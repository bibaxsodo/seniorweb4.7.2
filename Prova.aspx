﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Prova" CodeFile="Prova.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Login</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=10" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <style>
        div.centerpage {
            width: 895px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 40px;
            text-align: left;
            display: block;
        }

        div.latosinistro {
            float: left;
            width: 475px;
            height: 490px;
            display: block;
            background-color: #0072C6;
        }

        div.latodestro {
            float: right;
            width: 420px;
            height: 494px;
            display: block;
        }

        div.prepiede {
            display: inline;
            width: 895px;
            height: 50px;
        }

        div.piede {
            display: inline-table;
            border-top: 1px solid #CCC;
            width: 895px;
            height: 69px;
        }

        div.signUpFloat {
            position: relative;
            display: inline-table;
            margin-left: 120px;
            bottom: 0px;
            margin-top: 150px;
            width: 250px;
        }
    </style>
</head>

<body>
    <div class="centerpage">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div class="latosinistro">
            <img src="images/ImgLogin.png" />
            <div style="text-align: left; margin-left: 20px; font-size: 26pt; color: White;">
                Senior software per RSA
            </div>
            <div style="text-align: left; margin-left: 20px; font-size: 10pt; color: White;">
                Gestione Ospiti/Denaro/Stanze/Contabilità.
            </div>

            <div style="text-align: left; margin-left: 20px; font-size: 10pt; color: White; margin-top: 30px;">
                <a href="http://www.sodo.it" style="background-color: White; display: block; width: 100px; height: 20px; margin-top: 15px; text-align: center; font-style: normal; color: black; text-decoration: none;">Info</a>
            </div>
        </div>
        <div class="latodestro">
            <div style="height: 40px;"></div>
            <div style="height: 54px; margin-left: 100px;">
                <img src="images/seniormelograno.jpg" /><br />
            </div>
            <div style="height: 30px;"></div>
            <div style="float: left; width: 100px; height: 370px;"></div>
            <div style="float: right; width: 320px; vertical-align: top; line-height: 142%;">

                <br />
                <form id="form1" runat="server">
                    Login<br />
                    <asp:TextBox ID="Txt_Login" Width="311px" value="Utente" ForeColor="#888888" onclick="this.value=''; this.style.color='#000000';" onblur="if (this.value=='') { this.value='Utente'; this.style.color='#888888'; } " runat="server" TabIndex="1"></asp:TextBox><br />
                    <br />
                    <asp:TextBox ID="Txt_Password" Width="311px" Style="height: 25px;" runat="server" TextMode="Password" value="******" ForeColor="#888888" onclick="this.value=''; this.style.color='#000000';" onblur="if (this.value=='') { this.value='******'; this.style.color='#888888'; }"></asp:TextBox><br />
                    <br />
                    <br />
                    <asp:Button ID="BtnLogin" runat="server" Text="Login" Style='height: 2.142em; min-width: 6em; font-family: "Segoe UI Semibold", "Segoe UI Web Semibold", "Segoe UI Web Regular", "Segoe UI", "Segoe UI Symbol","HelveticaNeue-Medium", "Helvetica Neue", Arial, sans-serif; font-weight: normal; font-size: 100%; background-color: rgba(182,182,182,0.7); color: #212121; padding: 3px 12px 5px; border: 0px;' />

                </form>
                <div class="signUpFloat">Per informazioni <a href="mailto:info@sodo.it">info@sodo.it</a></div>
            </div>
        </div>

        <div class="prepiede">
            &nbsp;
	       <br />
            <br />
            <br />
        </div>

        <div class="piede">
            <div style="width: 100%; text-align: right; color: Gray;">Sodo Informatica S.r.l.</div>
        </div>
    </div>
</body>

</html>
