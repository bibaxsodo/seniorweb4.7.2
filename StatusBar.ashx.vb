﻿Public Class StatusBar
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        If Not IsNothing(System.Web.HttpContext.Current.Session("ProgressBar")) Then
            context.Response.ContentType = "text/plain"
            context.Response.Write(System.Web.HttpContext.Current.Session("ProgressBar"))
        Else
            context.Response.ContentType = "text/plain"
            context.Response.Write("")
        End If
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
End Class