﻿Imports System.Data.OleDb

Public Class Anagrafiche
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim sb As StringBuilder = New StringBuilder
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If RICERCA.IndexOf("%") >= 0 Then Exit Sub
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(context.Session("DC_TABELLE"))

        cn.Open()

        Dim cnO As OleDbConnection


        cnO = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cnO.Open()
        Dim cmd As New OleDbCommand()

        cmd.Connection = cn
        cmd.CommandText = ("select * from Rubrica where " &
          "Denominazione Like ?")
        cmd.Parameters.AddWithValue("@Descrizione", "%" & RICERCA & "%")

        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            sb.Append("R-" & myPOSTreader.Item("Id") & " " & myPOSTreader.Item("Denominazione")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop

        Dim cmd1 As New OleDbCommand()

        cmd1.Connection = cn
        cmd1.CommandText = ("select * from Rubrica where " &
          " Cognome + ' ' + Nome Like ?")
        cmd1.Parameters.AddWithValue("@CognomeNome", "%" & RICERCA & "%")

        Counter = 0
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While myPOSTreader1.Read

            sb.Append("R-" & myPOSTreader1.Item("Id") & " " & myPOSTreader1.Item("Cognome") & " " & myPOSTreader1.Item("Nome")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop
        myPOSTreader.Close





        Dim cmd5 As New OleDbCommand()

        cmd5.Connection = cnO
        cmd5.CommandText = ("select * from AnagraficaComune where TIPOLOGIA ='D' And " &
          " Nome Like ?")
        cmd5.Parameters.AddWithValue("@CognomeNome", "%" & RICERCA & "%")

        Counter = 0
        Dim Rd5 As OleDbDataReader = cmd5.ExecuteReader()
        Do While Rd5.Read

            sb.Append("C-D" & Rd5.Item("CODICEDEBITORECREDITORE") & " " & campodb(Rd5.Item("Nome"))).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop
        Rd5.Close


        Dim cmd6 As New OleDbCommand()

        cmd6.Connection = cnO
        cmd6.CommandText = ("select * from AnagraficaComune where TIPOLOGIA ='O' And " &
          " Nome Like ?")
        cmd6.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")

        Counter = 0
        Dim Rd6 As OleDbDataReader = cmd6.ExecuteReader()
        Do While Rd6.Read

            sb.Append("C-O" & Rd6.Item("CODICEOSPITE") & " " & campodb(Rd6.Item("Nome"))).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop
        Rd6.Close



        Dim cmd7 As New OleDbCommand()

        cmd7.Connection = cnO
        cmd7.CommandText = ("select * from AnagraficaComune where TIPOLOGIA ='P' And " &
          " Nome Like ?")
        cmd7.Parameters.AddWithValue("@CognomeNome", "%" & RICERCA & "%")

        Counter = 0
        Dim Rd7 As OleDbDataReader = cmd7.ExecuteReader()
        Do While Rd7.Read

            sb.Append("C-P" & Rd7.Item("CODICEOSPITE") & "-" & Rd7.Item("CODICEPARENTE") & " " & campodb(Rd7.Item("Nome"))).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop
        Rd7.Close

        cnO.Close
        cn.Close
        context.Response.Write(sb.ToString)

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class