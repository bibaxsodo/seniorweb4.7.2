﻿
Partial Class Protocollo_Rubrica
    Inherits System.Web.UI.Page



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Comune')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/autocompletecomune.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Cap')!= null) {  "
        MyJs = MyJs & " $(els[i]).focus(function() { if ($(els[i]).val() == '') {  __doPostBack(""Btn_RefreshPostali"", ""0""); } }); "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtConto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Denominazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Rubrica.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Cognome')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Rubrica.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then Exit Sub


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        DD_Report.Items.Add("Informativa")

        Dim TabAp As New Cls_Tabella_Appellativo

        TabAp.UpDropDownList(Session("DC_TABELLE"), DD_Appellativo)

        Dim k As New Cls_RaggruppamentoRubrica


        k.UpDropDown(Session("DC_TABELLE"), DD_Raggruppamento)
        Call EseguiJS()

        If Val(Request.Item("ID")) > 0 Then
            Txt_ID.Text = Val(Request.Item("ID"))

            'Lbl_Organizzazione.Text = "<a href=""#"" onclick=""DialogBoxMail('LogMailInviate.aspx?ID=" & Val(Request.Item("ID")) & "&TIPO=R')"" >Mail Inviate</a> "
            LeggiRubrica()
        End If
    End Sub


    Private Sub LeggiRubrica()

        Dim k As New Cls_Rubrica

        k.Id = Val(Txt_ID.Text)
        k.Leggi(Session("DC_TABELLE"))

        Txt_Denominazione.Text = k.Denominazione
        Txt_Cognome.Text = k.Cognome
        Txt_Nome.Text = k.Nome

        Txt_Indirizzo.Text = k.Indirizzo
        Txt_Cap.Text = k.Cap

        DD_Appellativo.SelectedValue = k.Appellativo

        Txt_Localita.Text = k.Localita

        Dim J As New ClsComune

        J.Provincia = k.Provincia
        J.Comune = k.Comune
        J.Leggi(Session("DC_OSPITE"))
        Txt_Comune.Text = Trim(k.Provincia & " " & k.Comune & " " & J.Descrizione)

        Txt_Note.Text = k.Note

        Txt_Telefono1.Text = k.Telefono1
        Txt_Telefono2.Text = k.Telefono2

        Txt_Mail1.Text = k.EMail1
        Txt_Mail2.Text = k.EMail2

        DD_Raggruppamento.SelectedValue = k.Ragruppamento.Replace("<", "").Replace(">", "")

        DD_InvioComunicazione.SelectedValue = k.InvioComunicazione


        If k.CONSENSOINSERIMENTO = 2 Then
            RB_NOConsenso1.Checked = True
        End If

        If k.CONSENSOINSERIMENTO = 1 Then
            RB_SIConsenso1.Checked = True
        End If



        If k.CONSENSOMARKETING = 0 Then
            RB_NOConsenso2.Checked = True
        End If

        If k.CONSENSOMARKETING = 1 Then
            RB_SIConsenso2.Checked = True
        End If



        'For i = 0 To Lst_Ragruppamento.Items.Count - 1
        ' If k.Ragruppamento.IndexOf("<" & Lst_Ragruppamento.Items(i).Value & ">") >= 0 Then
        'Lst_Ragruppamento.Items(i).Selected = True
        'End If
        'Next

    End Sub
    Private Sub ScriviRubrica()
        Dim k As New Cls_Rubrica

        k.Id = Val(Txt_ID.Text)

        k.Denominazione = Txt_Denominazione.Text

        k.Cognome = Txt_Cognome.Text
        k.Nome = Txt_Nome.Text

        k.Appellativo = Val(DD_Appellativo.SelectedValue)
        Try
            If Txt_Comune.Text.Length > 6 Then
                Dim Vettore(100) As String

                Vettore = SplitWords(Txt_Comune.Text)
                k.Provincia = Vettore(0)
                k.Comune = Vettore(1)
            End If
        Catch ex As Exception

        End Try

        k.Cap = Txt_Cap.Text

        k.Localita = Txt_Localita.Text

        k.Indirizzo = Txt_Indirizzo.Text
        k.Note = Txt_Note.Text
        k.Utente = Session("Utente")

        k.Telefono1 = Txt_Telefono1.Text
        k.Telefono2 = Txt_Telefono2.Text

        k.EMail1 = Txt_Mail1.Text
        k.EMail2 = Txt_Mail2.Text

        k.InvioComunicazione = DD_InvioComunicazione.SelectedValue

        Dim i As Integer

        k.Ragruppamento = "<" & DD_Raggruppamento.SelectedValue & ">"
        ' For i = 0 To Lst_Ragruppamento.Items.Count - 1
        '     If Lst_Ragruppamento.Items(i).Selected = True Then
        '        k.Ragruppamento = "<" & Lst_Ragruppamento.Items(i).Value & ">"
        '    End If
        'Next
        If RB_NOConsenso1.Checked = True Then
            k.CONSENSOINSERIMENTO = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            k.CONSENSOINSERIMENTO = 1
        End If


        If RB_NOConsenso2.Checked = True Then
            k.CONSENSOMARKETING = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            k.CONSENSOMARKETING = 1
        End If

        k.Scrivi(Session("DC_TABELLE"))



        Txt_ID.Text = 0

        Txt_Denominazione.Text = ""

        Txt_Cognome.Text = ""
        Txt_Nome.Text = ""

        DD_Appellativo.SelectedValue = ""
        Txt_Comune.Text = ""
        Txt_Cap.Text = ""

        Txt_Localita.Text = ""

        Txt_Indirizzo.Text = ""
        Txt_Note.Text = ""


        Txt_Telefono1.Text = ""
        Txt_Telefono2.Text = ""

        Txt_Mail1.Text = ""
        Txt_Mail2.Text = ""

        DD_Raggruppamento.SelectedValue = ""

        RB_NOConsenso1.Checked = True
        RB_SIConsenso1.Checked = False

        RB_NOConsenso2.Checked = True
        RB_SIConsenso2.Checked = False
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Denominazione.Text.Trim = "" And Txt_Cognome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Cognome o Denominazione</center>');", True)
            Exit Sub
        End If

        If DD_Raggruppamento.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Raggruppamento</center>');", True)
            Exit Sub
        End If

        If DD_InvioComunicazione.SelectedValue = "99" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Invio Comunicazione</center>');", True)
            Exit Sub
        End If

        If Txt_Cognome.Text.Trim <> "" Then
            If RB_SIConsenso1.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il consenso all inserimento dei dati in Iuvant');", True)
                Exit Sub
            End If
        End If

        ScriviRubrica()
        Dim MyJs As String
        MyJs = "alert('Dati Rubrica Inseriti ');"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("Elenco_Rubrica.aspx")
        Else
            Response.Redirect("Elenco_Rubrica.aspx?TIPO=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Txt_ID.Text = 0
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        If Val(Txt_ID.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Rubrica non registrata non posso eliminare</center>');", True)
            Exit Sub
        End If


        Dim K As New Cls_Rubrica
        K.Id = Txt_ID.Text
        K.Delete(Session("DC_TABELLE"))

        Response.Redirect("Elenco_Rubrica.aspx")
    End Sub




    Protected Sub Btn_RefreshPostali_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_RefreshPostali.Click
        If Txt_Cap.Text = "" Then
            Dim Xs As New ClsComune
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune.Text)

            Xs.Provincia = ""
            Xs.Comune = ""
            If Vettore.Length >= 2 Then
                Xs.Provincia = Vettore(0)
                Xs.Comune = Vettore(1)
                Xs.Leggi(Session("DC_OSPITE"))
                Txt_Cap.Text = Xs.RESIDENZACAP1
            End If
        End If

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Open1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Open1.Click
        Dim Testo As String

        Testo = Txt_Denominazione.Text

        If Len(Testo) > 3 And Testo.IndexOf(" ") > 0 Then
            Dim Codice As Long

            Codice = Val(Mid(Testo, 1, Testo.IndexOf(" ")))
            If Codice > 0 Then
                Response.Redirect("Rubrica.aspx?ID=" & Codice)
            End If
        End If


    End Sub

    Protected Sub Btn_Open2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Open2.Click
        Dim Testo As String

        Testo = Txt_Cognome.Text

        If Len(Testo) > 3 And Testo.IndexOf(" ") > 0 Then
            Dim Codice As Long

            Codice = Val(Mid(Testo, 1, Testo.IndexOf(" ")))
            If Codice > 0 Then
                Response.Redirect("Rubrica.aspx?ID=" & Codice)
            End If
        End If
    End Sub
End Class
