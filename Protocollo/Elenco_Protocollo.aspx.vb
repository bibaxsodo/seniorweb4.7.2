﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO


Partial Class Elenco_Protcollo
    Inherits System.Web.UI.Page
    Dim Tab_RicercaSoci As New System.Data.DataTable("Tab_RicercaSoci")

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("Anno", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Numero", GetType(String))
        Tab_RicercaSoci.Columns.Add("Protocollo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Data Arrivo/Invio", GetType(String))
        Tab_RicercaSoci.Columns.Add("Titolo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Oggetto", GetType(String))
        Tab_RicercaSoci.Columns.Add("Ragione Sociale", GetType(String))


        Dim cn As OleDbConnection
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Anno", Txt_Anno.Text)
        k1.Add("Chk_Entrata", Chk_Entrata.Checked)
        k1.Add("Chk_Uscita", Chk_Uscita.Checked)
        k1.Add("Txt_Numero", Txt_Numero.Text)
        k1.Add("Txt_DataArrivoDal", Txt_DataArrivoDal.Text)
        k1.Add("Txt_DataArrivoAl", Txt_DataArrivoAl.Text)
        k1.Add("Txt_oggetto", Txt_oggetto.Text)
        k1.Add("Txt_MittenteDestinatario", Txt_MittenteDestinatario.Text)
        k1.Add("RB_Tutti", RB_Tutti.Checked)
        k1.Add("RB_Presente", RB_Presente.Checked)
        k1.Add("RB_Assente", RB_Assente.Checked)



        Session("ElencoProtocolloSQLString") = k1

        Dim MySql As String
        MySql = "select * from Protocollo where "
        If Txt_Anno.Text <> "" Then
            MySql = MySql & " Anno = ?"
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        End If

        If Chk_Entrata.Checked = True And Chk_Uscita.Checked = True Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " (Tipo = 'E' or Tipo = 'U')"
        Else
            If Chk_Entrata.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'E'"
            End If
            If Chk_Uscita.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'U'"
            End If
        End If
        If Txt_Numero.Text <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Numero = ?"
            cmd.Parameters.AddWithValue("@Numero", Txt_Numero.Text)
        End If
        If IsDate(Txt_DataArrivoDal.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo >= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoDal.Text)
        End If
        If IsDate(Txt_DataArrivoAl.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo <= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoAl.Text)
        End If
        If Txt_oggetto.Text.Trim <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Oggetto Like ?"
            cmd.Parameters.AddWithValue("@Oggetto", Txt_oggetto.Text & "%")
        End If

        Dim CodiceInRicerca As Boolean = False
        If Len(Txt_MittenteDestinatario.Text) > 4 Then
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "R" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdRubrica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "A" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdAnagrafica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If

            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "C" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then

                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "D" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdClienteFornitore = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "O" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "P" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
            End If


        End If

        If CodiceInRicerca = False Then
            If Txt_MittenteDestinatario.Text <> "" And Val(Txt_Anno.Text) = 0 Then
                Dim cmdA As New OleDbCommand()
                Dim Condizione As String = ""
                cmdA.CommandText = "Select * From Anagrafica Where RagioneSociale Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdA.Connection = cn
                Dim Leggi As OleDbDataReader = cmdA.ExecuteReader()
                Do While Leggi.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(Leggi.Item("IdAnagrafica")))
                Loop
                Leggi.Close()
                Dim cmdP As New OleDbCommand()

                cmdP.CommandText = "Select * From Anagrafica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdP.Connection = cn
                Dim LeggiP As OleDbDataReader = cmdP.ExecuteReader()
                Do While LeggiP.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(LeggiP.Item("IdAnagrafica")))
                Loop
                LeggiP.Close()

                Dim cmdR As New OleDbCommand()

                cmdR.CommandText = "Select * From Rubrica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%' OR Denominazione Like '%" & Txt_MittenteDestinatario.Text & "%' "
                cmdR.Connection = cn
                Dim LeggiR As OleDbDataReader = cmdR.ExecuteReader()
                Do While LeggiR.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdRubrica = " & Val(campodb(LeggiR.Item("ID")))
                Loop
                LeggiR.Close()

                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                If Condizione = "" Then
                    MySql = MySql & " RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%'"
                Else
                    MySql = MySql & " ((Select count(*) From [ProtocolloAnagrafiche] Where Anno = Protocollo.Anno And Tipo = Protocollo.Tipo And Numero =Protocollo.Numero And (" & Condizione & ")) > 0 OR RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%')"
                End If
            End If
        End If

        If MySql.Trim = "select * from Protocollo where" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
            Exit Sub
        End If
        'DataArrivo Desc,
        cmd.CommandText = MySql & "  Order By Numero Desc "

        Dim nUMEROrIGHE As Long

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Session("Posizione") = 0
        Do While myPOSTreader.Read

            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim RagioneSociale As String = ""
            Dim ForSearchRagioneSociale As String = ""


            KlS.Anno = campodb(myPOSTreader.Item("Anno"))
            KlS.Numero = campodb(myPOSTreader.Item("Numero"))
            KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
            KlS.Leggi(Session("DC_TABELLE"))



            For I = 0 To 1000
                If KlS.IdRubrica(I) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(I)
                    Kppp.Leggi(Session("DC_TABELLE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                End If
                If KlS.IdClienteFornitore(I) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                    KpppS.Leggi(Session("DC_OSPITE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If
                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
                If KlS.IdParente(I) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.CodiceParente = KlS.IdParente(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
            Next
            If RagioneSociale = "" Then
                ForSearchRagioneSociale = Replace(campodb(myPOSTreader.Item("RagioneSociale")), "%", "")
                RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            End If
            If IsNothing(ForSearchRagioneSociale) Then
                ForSearchRagioneSociale = ""
            End If

            If CodiceInRicerca = True Or Txt_MittenteDestinatario.Text.Trim = "" Or (Txt_MittenteDestinatario.Text.Trim <> "" And ForSearchRagioneSociale.ToUpper.IndexOf(Txt_MittenteDestinatario.Text.ToUpper.Replace("%", "")) >= 0) Then
                Dim Aggiungi As Boolean = False

                If RB_Tutti.Checked = True Then
                    Aggiungi = True
                End If
                If RB_Presente.Checked = True Then
                    If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                        Dim aryItemsInfo() As FileSystemInfo
                        Dim objItem As FileSystemInfo

                        aryItemsInfo = objDI.GetFileSystemInfos()
                        If aryItemsInfo.Length > 0 Then
                            Aggiungi = True
                        End If
                    End If
                End If
                If RB_Assente.Checked = True Then
                    If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                        Dim aryItemsInfo() As FileSystemInfo
                        Dim objItem As FileSystemInfo

                        aryItemsInfo = objDI.GetFileSystemInfos()
                        If aryItemsInfo.Length <= 0 Then
                            Aggiungi = True
                        End If
                    Else
                        Aggiungi = True
                    End If

                End If
                If Aggiungi = True Then
                    Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
                    myrigaT(0) = campodb(myPOSTreader.Item("Anno"))
                    myrigaT(1) = campodb(myPOSTreader.Item("Numero"))
                    myrigaT(2) = campodb(myPOSTreader.Item("Tipo"))
                    myrigaT(3) = campodb(myPOSTreader.Item("DataArrivo"))
                    Dim k As New Cls_Titolo
                    k.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
                    k.Leggi(Session("DC_TABELLE"))
                    myrigaT(4) = k.Descrizione
                    myrigaT(5) = campodb(myPOSTreader.Item("Oggetto"))
                    myrigaT(6) = ""
                    myrigaT(6) = RagioneSociale

                    Tab_RicercaSoci.Rows.Add(myrigaT)
                    nUMEROrIGHE = nUMEROrIGHE + 1
                End If
            End If
            If nUMEROrIGHE > 20 Then
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("Posizione") = nUMEROrIGHE + 1
        Session("ElencoProtocollo") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()

        Call EseguiJS()

        If nUMEROrIGHE = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Nessundata", "alert('Nessun dato estratto');", True)
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Try
            If Session("ABILITAZIONI").IndexOf("<SOLOENTRATE>") >= 0 Then
                Chk_Entrata.Checked = True
                Chk_Uscita.Checked = False
                Chk_Entrata.Enabled = False
                Chk_Uscita.Enabled = False
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOUSCITE>") >= 0 Then
                Chk_Uscita.Checked = True
                Chk_Entrata.Checked = False
                Chk_Entrata.Enabled = False
                Chk_Uscita.Enabled = False
            End If
        Catch ex As Exception
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End Try

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Txt_Anno.Text = Year(Now)

        If Request.Item("TIPO") <> "RITORNO" Then
            Call CaricaUltimi10()
            Session("ElencoOrganizzazioniSQLString") = Nothing
        Else

            If Not IsNothing(Session("ElencoProtocolloSQLString")) Then
                Dim k As New Cls_SqlString
                Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("ElencoProtocolloSQLString")

                Txt_Anno.Text = k.GetValue("Txt_Anno")
                Chk_Entrata.Checked = k.GetValue("Chk_Entrata")
                Chk_Uscita.Checked = k.GetValue("Chk_Uscita")
                Txt_Numero.Text = k.GetValue("Txt_Numero")
                Txt_DataArrivoDal.Text = k.GetValue("Txt_DataArrivoDal")
                Txt_DataArrivoAl.Text = k.GetValue("Txt_DataArrivoAl")
                Txt_oggetto.Text = k.GetValue("Txt_oggetto")
                Txt_MittenteDestinatario.Text = k.GetValue("Txt_MittenteDestinatario")
                RB_Tutti.Checked = k.GetValue("RB_Tutti")
                RB_Presente.Checked = k.GetValue("RB_Presente")
                RB_Assente.Checked = k.GetValue("RB_Assente")

                Call Btn_Ricerca_Click(sender, Appoggio)
            Else
                Call CaricaUltimi10()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grd_Visualizzazione.PageCount Then
                    Grd_Visualizzazione.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tab_RicercaSoci = Session("ElencoProtocollo")

            Grd_Visualizzazione.AutoGenerateColumns = True
            Grd_Visualizzazione.DataSource = Tab_RicercaSoci
            Grd_Visualizzazione.DataBind()
        End If





        Call EseguiJS()
    End Sub

    Protected Sub Grd_Visualizzazione_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Visualizzazione.PageIndexChanging
        Tab_RicercaSoci = Session("ElencoProtocollo")

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.PageIndex = e.NewPageIndex
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()
    End Sub

    Protected Sub Grd_Visualizzazione_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Visualizzazione.RowCommand
        Try

            If (e.CommandName = "Select" Or e.CommandName = "Seleziona") Then

                Tab_RicercaSoci = Session("ElencoProtocollo")

                If Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(1).ToString) > 0 Then
                    Response.Redirect("Protocollo.aspx?Anno=" & Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(0).ToString) & "&Numero=" & Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(1).ToString) & "&Tipo=" & Tab_RicercaSoci.Rows(e.CommandArgument).Item(2).ToString & "&PAGINA=" & Grd_Visualizzazione.PageIndex)
                End If
            End If


            If (e.CommandName = "Stampa" Or e.CommandName = "Stampa") Then

                Tab_RicercaSoci = Session("ElencoProtocollo")


                Dim Kj As New Cls_Protocollo
                Dim RagioneSociale As String = ""
                Dim ForSearchRagioneSociale As String = ""
                Dim Indirizzo As String = ""
                Dim Cap As String = ""
                Dim Comune As String = ""
                Dim Provincia As String = ""

                Dim Mezzo As String = ""

                Dim KlS As New Cls_ProtocolloAnagrafiche


                Kj.Anno = Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(0).ToString)
                Kj.Numero = Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(1).ToString)
                Kj.Tipo = Tab_RicercaSoci.Rows(e.CommandArgument).Item(2).ToString
                Kj.Leggi(Session("DC_TABELLE"))

                KlS.Anno = Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(0).ToString)
                KlS.Numero = Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(1).ToString)
                KlS.Tipo = Tab_RicercaSoci.Rows(e.CommandArgument).Item(2).ToString
                KlS.Leggi(Session("DC_TABELLE"))



                For I = 0 To 1000
                    If KlS.IdRubrica(I) > 0 Then
                        Dim Kppp As New Cls_Rubrica
                        Kppp.Id = KlS.IdRubrica(I)
                        Kppp.Leggi(Session("DC_TABELLE"))
                        If RagioneSociale = "" Then
                            RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                            Indirizzo = Kppp.Indirizzo
                            Cap = Kppp.Cap

                            Dim DecTipo As New ClsComune

                            DecTipo.Provincia = Kppp.Provincia
                            DecTipo.Comune = Kppp.Comune
                            DecTipo.Leggi(Session("DC_OSPITE"))
                            Comune = DecTipo.Descrizione
                            Provincia = DecTipo.CodificaProvincia
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                    End If
                    If KlS.IdClienteFornitore(I) > 0 Then
                        Dim KpppS As New Cls_ClienteFornitore
                        KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                        KpppS.Leggi(Session("DC_OSPITE"))
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                            Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                            Cap = KpppS.RESIDENZACAP1

                            Dim DecTipo As New ClsComune

                            DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                            DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                            DecTipo.Leggi(Session("DC_OSPITE"))
                            Comune = DecTipo.Descrizione
                            Provincia = DecTipo.CodificaProvincia
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                    End If

                    If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                        Dim KpppS As New ClsOspite
                        KpppS.CodiceOspite = KlS.IdOspite(I)
                        KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                            Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                            Cap = KpppS.RESIDENZACAP1

                            Dim DecTipo As New ClsComune

                            DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                            DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                            DecTipo.Leggi(Session("DC_OSPITE"))
                            Comune = DecTipo.Descrizione
                            Provincia = DecTipo.CodificaProvincia
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                    End If
                    If KlS.IdOspite(I) > 0 And KlS.IdParente(I) > 0 Then
                        Dim KpppS As New Cls_Parenti
                        KpppS.CodiceOspite = KlS.IdOspite(I)
                        KpppS.CodiceParente = KlS.IdParente(I)
                        KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                            Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                            Cap = KpppS.RESIDENZACAP1

                            Dim DecTipo As New ClsComune

                            DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                            DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                            DecTipo.Leggi(Session("DC_OSPITE"))
                            Comune = DecTipo.Descrizione
                            Provincia = DecTipo.CodificaProvincia
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                    End If


                Next

                Dim TabProtcollo As New Xsd_StampaProtocollo
                Dim k As System.Data.DataRow = TabProtcollo.Tables("Protocollo").NewRow

                k.Item("Anno") = Kj.Anno
                k.Item("Numero") = Kj.Numero
                k.Item("Tipo") = Kj.Tipo
                k.Item("Data") = Format(Kj.DataArrivo, "dd/MM/yyyy")

                k.Item("Ora") = Format(Kj.OraArrivo, "hh:mm")

                Dim MyOf As New Cls_UfficioDestinatario

                MyOf.idUfficioDestinatario = Kj.UfficioDestinatario
                MyOf.Leggi(Session("DC_TABELLE"))

                k.Item("Ufficio") = MyOf.Descrizione


                Dim lk As New Cls_Titolo

                lk.idTitolo = Kj.Titolo
                lk.Leggi(Session("DC_TABELLE"))


                k.Item("Titolo") = lk.Descrizione
                k.Item("CodiceTitolo") = lk.Codice

                Dim lkClasse As New Cls_Classe

                lkClasse.idClasse = Kj.Classe
                lkClasse.Leggi(Session("DC_TABELLE"))
                k.Item("Classe") = lkClasse.Descrizione
                k.Item("IdClasse") = lkClasse.idClasse

                Dim lkIndice As New Cls_Indice


                lkIndice.idClasse = Kj.Indice
                lkIndice.Leggi(Session("DC_TABELLE"))
                k.Item("Indice") = lkIndice.Descrizione
                k.Item("idIndice") = lkIndice.idIndice

                k.Item("Fascicolo") = Kj.Fascicolo



                k.Item("Oggetto") = Kj.Oggetto

                k.Item("Destinatario") = RagioneSociale
                k.Item("Indirizzo") = Indirizzo
                k.Item("Comune") = Comune
                k.Item("Provincia") = Provincia
                k.Item("Cap") = Cap

                TabProtcollo.Tables("Protocollo").Rows.Add(k)


                Session("stampa") = TabProtcollo


                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=EtichettaProtocollo.rpt','Stampe','width=800,height=600');", True)


            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Grd_Visualizzazione_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Visualizzazione.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub

    Private Sub CaricaUltimi10()
        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("Anno", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Numero", GetType(String))
        Tab_RicercaSoci.Columns.Add("Protocollo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Data Arrivo/Invio", GetType(String))
        Tab_RicercaSoci.Columns.Add("Titolo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Oggetto", GetType(String))
        Tab_RicercaSoci.Columns.Add("Ragione Sociale", GetType(String))


        Dim cn As OleDbConnection
        Dim i As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        Dim MySql As String
        MySql = "select Top 20 * from Protocollo Where Anno = " & Val(Txt_Anno.Text)
        'DataArrivo Desc,


        Try
            If Session("ABILITAZIONI").IndexOf("<SOLOENTRATE>") >= 0 Then
                MySql = MySql & " And Tipo = 'E' "
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOUSCITE>") >= 0 Then
                MySql = MySql & " And  Tipo = 'U' "
            End If
        Catch ex As Exception
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End Try


        cmd.CommandText = MySql & "    Order By Numero Desc "

        cmd.Connection = cn
        Dim nUMEROrIGHE As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
            myrigaT(0) = campodb(myPOSTreader.Item("Anno"))
            myrigaT(1) = campodb(myPOSTreader.Item("Numero"))
            myrigaT(2) = campodb(myPOSTreader.Item("Tipo"))
            myrigaT(3) = campodb(myPOSTreader.Item("DataArrivo"))
            Dim k As New Cls_Titolo

            k.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
            k.Leggi(Session("DC_TABELLE"))

            myrigaT(4) = k.Descrizione
            myrigaT(5) = campodb(myPOSTreader.Item("Oggetto"))
            myrigaT(6) = ""


            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim RagioneSociale As String = ""


            KlS.Anno = campodb(myPOSTreader.Item("Anno"))
            KlS.Numero = campodb(myPOSTreader.Item("Numero"))
            KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
            KlS.Leggi(Session("DC_TABELLE"))



            For i = 0 To 1000
                If KlS.IdRubrica(i) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(i)
                    Kppp.Leggi(Session("DC_TABELLE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                    End If
                End If
                If KlS.IdClienteFornitore(i) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(i)
                    KpppS.Leggi(Session("DC_OSPITE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
                If KlS.IdOspite(i) > 0 And KlS.IdParente(i) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(i)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
                If KlS.IdParente(i) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(i)
                    KpppS.CodiceParente = KlS.IdParente(i)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If

                'If KlS.IdAnagrafica(i) > 0 Then
                '    Dim Kppp As New Cls_Anagrafica
                '    Kppp.Leggi(Session("DC_TABELLE"), KlS.IdAnagrafica(i))
                '    If RagioneSociale = "" Then
                '        RagioneSociale = Kppp.RagioneSociale
                '    End If
                'End If
            Next
            If RagioneSociale = "" Then
                RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            End If


            myrigaT(6) = RagioneSociale
            nUMEROrIGHE = nUMEROrIGHE + 1

            Tab_RicercaSoci.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("Posizione") = nUMEROrIGHE + 1
        Session("ElencoProtocollo") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else "
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_MittenteDestinatario')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Anagrafiche.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_ToExcel.Click
        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("Anno", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Numero", GetType(String))
        Tab_RicercaSoci.Columns.Add("Protocollo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Data Arrivo/Invio", GetType(String))
        Tab_RicercaSoci.Columns.Add("Titolo", GetType(String))
        Tab_RicercaSoci.Columns.Add("Oggetto", GetType(String))
        Tab_RicercaSoci.Columns.Add("Ragione Sociale", GetType(String))

        Dim cn As OleDbConnection
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Anno", Txt_Anno.Text)
        k1.Add("Chk_Entrata", Chk_Entrata.Checked)
        k1.Add("Chk_Uscita", Chk_Uscita.Checked)
        k1.Add("Txt_Numero", Txt_Numero.Text)
        k1.Add("Txt_DataArrivoDal", Txt_DataArrivoDal.Text)
        k1.Add("Txt_DataArrivoAl", Txt_DataArrivoAl.Text)
        k1.Add("Txt_oggetto", Txt_oggetto.Text)
        k1.Add("Txt_MittenteDestinatario", Txt_MittenteDestinatario.Text)
        k1.Add("RB_Tutti", RB_Tutti.Checked)
        k1.Add("RB_Presente", RB_Presente.Checked)
        k1.Add("RB_Assente", RB_Assente.Checked)



        Session("ElencoProtocolloSQLString") = k1

        Dim MySql As String
        MySql = "select * from Protocollo where "
        If Txt_Anno.Text <> "" Then
            MySql = MySql & " Anno = ?"
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        End If

        If Chk_Entrata.Checked = True And Chk_Uscita.Checked = True Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " (Tipo = 'E' or Tipo = 'U')"
        Else
            If Chk_Entrata.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'E'"
            End If
            If Chk_Uscita.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'U'"
            End If
        End If
        If Txt_Numero.Text <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Numero = ?"
            cmd.Parameters.AddWithValue("@Numero", Txt_Numero.Text)
        End If
        If IsDate(Txt_DataArrivoDal.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo >= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoDal.Text)
        End If
        If IsDate(Txt_DataArrivoAl.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo <= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoAl.Text)
        End If
        If Txt_oggetto.Text.Trim <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Oggetto Like ?"
            cmd.Parameters.AddWithValue("@Oggetto", Txt_oggetto.Text & "%")
        End If

        Dim CodiceInRicerca As Boolean = False
        If Len(Txt_MittenteDestinatario.Text) > 4 Then
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "R" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdRubrica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "A" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdAnagrafica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If

            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "C" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then

                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "D" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdClienteFornitore = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "O" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "P" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
            End If


        End If

        If CodiceInRicerca = False Then
            If Txt_MittenteDestinatario.Text <> "" And Val(Txt_Anno.Text) = 0 Then
                Dim cmdA As New OleDbCommand()
                Dim Condizione As String = ""
                cmdA.CommandText = "Select * From Anagrafica Where RagioneSociale Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdA.Connection = cn
                Dim Leggi As OleDbDataReader = cmdA.ExecuteReader()
                Do While Leggi.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(Leggi.Item("IdAnagrafica")))
                Loop
                Leggi.Close()
                Dim cmdP As New OleDbCommand()

                cmdP.CommandText = "Select * From Anagrafica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdP.Connection = cn
                Dim LeggiP As OleDbDataReader = cmdP.ExecuteReader()
                Do While LeggiP.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(LeggiP.Item("IdAnagrafica")))
                Loop
                LeggiP.Close()

                Dim cmdR As New OleDbCommand()

                cmdR.CommandText = "Select * From Rubrica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%' OR Denominazione Like '%" & Txt_MittenteDestinatario.Text & "%' "
                cmdR.Connection = cn
                Dim LeggiR As OleDbDataReader = cmdR.ExecuteReader()
                Do While LeggiR.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdRubrica = " & Val(campodb(LeggiR.Item("ID")))
                Loop
                LeggiR.Close()

                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                If Condizione = "" Then
                    MySql = MySql & " RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%'"
                Else
                    MySql = MySql & " ((Select count(*) From [ProtocolloAnagrafiche] Where Anno = Protocollo.Anno And Tipo = Protocollo.Tipo And Numero =Protocollo.Numero And (" & Condizione & ")) > 0 OR RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%')"
                End If
            End If
        End If

        If MySql.Trim = "select * from Protocollo where" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
            Exit Sub
        End If

        'DataArrivo Desc,
        cmd.CommandText = MySql & "  Order By Numero Desc "

        Dim nUMEROrIGHE As Long

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder
        Dim Indice As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            

            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim RagioneSociale As String = ""
            Dim ForSearchRagioneSociale As String = ""


            KlS.Anno = campodb(myPOSTreader.Item("Anno"))
            KlS.Numero = campodb(myPOSTreader.Item("Numero"))
            KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
            KlS.Leggi(Session("DC_TABELLE"))



            For I = 0 To 1000
                If KlS.IdRubrica(I) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(I)
                    Kppp.Leggi(Session("DC_TABELLE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                End If
                If KlS.IdClienteFornitore(I) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                    KpppS.Leggi(Session("DC_OSPITE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If
                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
                If KlS.IdParente(I) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.CodiceParente = KlS.IdParente(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                    End If
                End If
            Next
            If RagioneSociale = "" Then
                ForSearchRagioneSociale = Replace(campodb(myPOSTreader.Item("RagioneSociale")), "%", "")
                RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            End If
            If IsNothing(ForSearchRagioneSociale) Then
                ForSearchRagioneSociale = ""
            End If

            If CodiceInRicerca = True Or Txt_MittenteDestinatario.Text.Trim = "" Or (Txt_MittenteDestinatario.Text.Trim <> "" And ForSearchRagioneSociale.ToUpper.IndexOf(Txt_MittenteDestinatario.Text.ToUpper.Replace("%", "")) >= 0) Then
                Dim Aggiungi As Boolean = False

                If RB_Tutti.Checked = True Then
                    Aggiungi = True
                End If
                If RB_Presente.Checked = True Then
                    If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                        Dim aryItemsInfo() As FileSystemInfo
                        Dim objItem As FileSystemInfo

                        aryItemsInfo = objDI.GetFileSystemInfos()
                        If aryItemsInfo.Length > 0 Then
                            Aggiungi = True
                        End If
                    End If
                End If
                If RB_Assente.Checked = True Then
                    If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                        Dim aryItemsInfo() As FileSystemInfo
                        Dim objItem As FileSystemInfo

                        aryItemsInfo = objDI.GetFileSystemInfos()
                        If aryItemsInfo.Length <= 0 Then
                            Aggiungi = True
                        End If
                    Else
                        Aggiungi = True
                    End If

                End If
                If Aggiungi = True Then
                    Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
                    myrigaT(0) = campodb(myPOSTreader.Item("Anno"))
                    myrigaT(1) = campodb(myPOSTreader.Item("Numero"))
                    myrigaT(2) = campodb(myPOSTreader.Item("Tipo"))
                    myrigaT(3) = campodb(myPOSTreader.Item("DataArrivo"))
                    Dim k As New Cls_Titolo
                    k.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
                    k.Leggi(Session("DC_TABELLE"))
                    myrigaT(4) = k.Descrizione
                    myrigaT(5) = campodb(myPOSTreader.Item("Oggetto"))
                    myrigaT(6) = ""
                    myrigaT(6) = RagioneSociale

                    Tab_RicercaSoci.Rows.Add(myrigaT)
                    nUMEROrIGHE = nUMEROrIGHE + 1
                End If
            End If            
        Loop
        myPOSTreader.Close()
        cn.Close()



        Session("GrigliaSoloStampa") = Tab_RicercaSoci
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)
    End Sub

    Protected Sub Img_Espandi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Espandi.Click
        Tab_RicercaSoci = Session("ElencoProtocollo")


        Dim cn As OleDbConnection
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Anno", Txt_Anno.Text)
        k1.Add("Chk_Entrata", Chk_Entrata.Checked)
        k1.Add("Chk_Uscita", Chk_Uscita.Checked)
        k1.Add("Txt_Numero", Txt_Numero.Text)
        k1.Add("Txt_DataArrivoDal", Txt_DataArrivoDal.Text)
        k1.Add("Txt_DataArrivoAl", Txt_DataArrivoAl.Text)
        k1.Add("Txt_oggetto", Txt_oggetto.Text)
        k1.Add("Txt_MittenteDestinatario", Txt_MittenteDestinatario.Text)
        k1.Add("RB_Tutti", RB_Tutti.Checked)
        k1.Add("RB_Presente", RB_Presente.Checked)
        k1.Add("RB_Assente", RB_Assente.Checked)



        Session("ElencoProtocolloSQLString") = k1

        Dim MySql As String
        MySql = "select * from Protocollo where "
        If Txt_Anno.Text <> "" Then
            MySql = MySql & " Anno = ?"
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        End If

        If Chk_Entrata.Checked = True And Chk_Uscita.Checked = True Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " (Tipo = 'E' or Tipo = 'U')"
        Else
            If Chk_Entrata.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'E'"
            End If
            If Chk_Uscita.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'U'"
            End If
        End If
        If Txt_Numero.Text <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Numero = ?"
            cmd.Parameters.AddWithValue("@Numero", Txt_Numero.Text)
        End If
        If IsDate(Txt_DataArrivoDal.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo >= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoDal.Text)
        End If
        If IsDate(Txt_DataArrivoAl.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo <= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataArrivoAl.Text)
        End If
        If Txt_oggetto.Text.Trim <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Oggetto Like ?"
            cmd.Parameters.AddWithValue("@Oggetto", Txt_oggetto.Text & "%")
        End If

        Dim CodiceInRicerca As Boolean = False
        If Len(Txt_MittenteDestinatario.Text) > 4 Then
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "R" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdRubrica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If
            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "A" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If

                CodiceInRicerca = True
                MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdAnagrafica = " & Val(Mid(Txt_MittenteDestinatario.Text, 3, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
            End If

            If Mid(Txt_MittenteDestinatario.Text, 1, 1) = "C" And Mid(Txt_MittenteDestinatario.Text, 2, 1) = "-" Then

                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "D" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdClienteFornitore = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "O" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
                If Mid(Txt_MittenteDestinatario.Text, 3, 1) = "P" Then
                    If MySql <> "select * from Protocollo where " Then
                        MySql = MySql & " And "
                    End If

                    CodiceInRicerca = True
                    MySql = MySql & "(Select count(*) From [ProtocolloAnagrafiche] Where [ProtocolloAnagrafiche].Anno = Protocollo.Anno And [ProtocolloAnagrafiche].Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].Numero =Protocollo.Numero And  Tipo = Protocollo.Tipo And [ProtocolloAnagrafiche].IdOspite = " & Val(Mid(Txt_MittenteDestinatario.Text, 4, Txt_MittenteDestinatario.Text.IndexOf(" "))) & ") > 0"
                End If
            End If


        End If

        If CodiceInRicerca = False Then
            If Txt_MittenteDestinatario.Text <> "" And Val(Txt_Anno.Text) = 0 Then
                Dim cmdA As New OleDbCommand()
                Dim Condizione As String = ""
                cmdA.CommandText = "Select * From Anagrafica Where RagioneSociale Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdA.Connection = cn
                Dim Leggi As OleDbDataReader = cmdA.ExecuteReader()
                Do While Leggi.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(Leggi.Item("IdAnagrafica")))
                Loop
                Leggi.Close()
                Dim cmdP As New OleDbCommand()

                cmdP.CommandText = "Select * From Anagrafica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%'"
                cmdP.Connection = cn
                Dim LeggiP As OleDbDataReader = cmdP.ExecuteReader()
                Do While LeggiP.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdAnagrafica = " & Val(campodb(LeggiP.Item("IdAnagrafica")))
                Loop
                LeggiP.Close()

                Dim cmdR As New OleDbCommand()

                cmdR.CommandText = "Select * From Rubrica Where Cognome + ' ' + Nome Like '%" & Txt_MittenteDestinatario.Text & "%' OR Denominazione Like '%" & Txt_MittenteDestinatario.Text & "%' "
                cmdR.Connection = cn
                Dim LeggiR As OleDbDataReader = cmdR.ExecuteReader()
                Do While LeggiR.Read
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " IdRubrica = " & Val(campodb(LeggiR.Item("ID")))
                Loop
                LeggiR.Close()

                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                If Condizione = "" Then
                    MySql = MySql & " RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%'"
                Else
                    MySql = MySql & " ((Select count(*) From [ProtocolloAnagrafiche] Where Anno = Protocollo.Anno And Tipo = Protocollo.Tipo And Numero =Protocollo.Numero And (" & Condizione & ")) > 0 OR RagioneSociale  Like '%" & Txt_MittenteDestinatario.Text & "%')"
                End If
            End If
        End If

        If MySql.Trim = "select * from Protocollo where" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
            Exit Sub
        End If

        'DataArrivo Desc,
        cmd.CommandText = MySql & "  Order By Numero Desc "

        Dim nUMEROrIGHE As Long

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder
        Dim Indice As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Indice = Indice + 1

            If Indice > 5 + Session("Posizione") Then
                Exit Do
            End If
            If Indice >= Session("Posizione") Then

                Dim KlS As New Cls_ProtocolloAnagrafiche
                Dim RagioneSociale As String = ""
                Dim ForSearchRagioneSociale As String = ""


                KlS.Anno = campodb(myPOSTreader.Item("Anno"))
                KlS.Numero = campodb(myPOSTreader.Item("Numero"))
                KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
                KlS.Leggi(Session("DC_TABELLE"))



                For I = 0 To 1000
                    If KlS.IdRubrica(I) > 0 Then
                        Dim Kppp As New Cls_Rubrica
                        Kppp.Id = KlS.IdRubrica(I)
                        Kppp.Leggi(Session("DC_TABELLE"))
                        If RagioneSociale = "" Then
                            RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                    End If
                    If KlS.IdClienteFornitore(I) > 0 Then
                        Dim KpppS As New Cls_ClienteFornitore
                        KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                        KpppS.Leggi(Session("DC_OSPITE"))
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                        End If
                        ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                    End If
                    If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                        Dim KpppS As New ClsOspite
                        KpppS.CodiceOspite = KlS.IdOspite(I)
                        KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                        End If
                    End If
                    If KlS.IdParente(I) > 0 Then
                        Dim KpppS As New Cls_Parenti
                        KpppS.CodiceOspite = KlS.IdOspite(I)
                        KpppS.CodiceParente = KlS.IdParente(I)
                        KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                        If RagioneSociale = "" Then
                            RagioneSociale = KpppS.Nome
                        End If
                    End If
                Next
                If RagioneSociale = "" Then
                    ForSearchRagioneSociale = Replace(campodb(myPOSTreader.Item("RagioneSociale")), "%", "")
                    RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
                End If
                If IsNothing(ForSearchRagioneSociale) Then
                    ForSearchRagioneSociale = ""
                End If

                If CodiceInRicerca = True Or Txt_MittenteDestinatario.Text.Trim = "" Or (Txt_MittenteDestinatario.Text.Trim <> "" And ForSearchRagioneSociale.ToUpper.IndexOf(Txt_MittenteDestinatario.Text.ToUpper.Replace("%", "")) >= 0) Then
                    Dim Aggiungi As Boolean = False

                    If RB_Tutti.Checked = True Then
                        Aggiungi = True
                    End If
                    If RB_Presente.Checked = True Then
                        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                            Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                            Dim aryItemsInfo() As FileSystemInfo
                            Dim objItem As FileSystemInfo

                            aryItemsInfo = objDI.GetFileSystemInfos()
                            If aryItemsInfo.Length > 0 Then
                                Aggiungi = True
                            End If
                        End If
                    End If
                    If RB_Assente.Checked = True Then
                        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")), FileAttribute.Directory) <> "" Then
                            Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("CodiceCSV") & "\PRT_" & campodb(myPOSTreader.Item("Anno")) & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                            Dim aryItemsInfo() As FileSystemInfo
                            Dim objItem As FileSystemInfo

                            aryItemsInfo = objDI.GetFileSystemInfos()
                            If aryItemsInfo.Length <= 0 Then
                                Aggiungi = True
                            End If
                        Else
                            Aggiungi = True
                        End If

                    End If
                    If Aggiungi = True Then
                        Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
                        myrigaT(0) = campodb(myPOSTreader.Item("Anno"))
                        myrigaT(1) = campodb(myPOSTreader.Item("Numero"))
                        myrigaT(2) = campodb(myPOSTreader.Item("Tipo"))
                        myrigaT(3) = campodb(myPOSTreader.Item("DataArrivo"))
                        Dim k As New Cls_Titolo
                        k.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
                        k.Leggi(Session("DC_TABELLE"))
                        myrigaT(4) = k.Descrizione
                        myrigaT(5) = campodb(myPOSTreader.Item("Oggetto"))
                        myrigaT(6) = ""
                        myrigaT(6) = RagioneSociale

                        Tab_RicercaSoci.Rows.Add(myrigaT)
                        nUMEROrIGHE = nUMEROrIGHE + 1
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("Posizione") = Indice

        Session("ElencoProtocollo") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()

        REM Call EseguiJS()

        Dim MyJs As String = ""
        MyJs = "var $target = $('html,body');"
        REM MyJs = MyJs & "$target.animate({ scrollTop: $target.height() -300 }, 1);"
        MyJs = "setTimeout(function(){ $('html,body').animate({scrollTop: $(document).height() -1200}, 10);}, 300);"

        REM ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScrollDown", MyJs, True)

    End Sub
End Class
