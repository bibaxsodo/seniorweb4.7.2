﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Tabella_Ragruppamento" CodeFile="Tabella_Ragruppamento.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Tabella Raggruppamento</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Protocollo - Tabella Raggruppamento</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="images\salva.jpg" ToolTip="Modifica / Inserisci" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Rubrica.aspx">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a>
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="565px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Tabella Raggruppamento" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />

                                    <asp:GridView ID="GrdTabella" runat="server" CellPadding="4" Height="60px" Width="940px"
                                        ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="IB_Update" runat="Server" CommandName="Update" ImageUrl="~/images/aggiorna.png" />
                                                    <asp:ImageButton ID="IB_Cancel" runat="Server" CommandName="Cancel" ImageUrl="~/images/annulla.png" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Inserisci" ImageUrl="~/images/inserisci.png" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Modify" runat="Server" CommandName="Edit"
                                                        ImageUrl="~/images/modifica.png" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" runat="Server" CommandName="Delete"
                                                        ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ID">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtIDTipoIscrizione" onkeypress="return handleEnterSoloNumero(this, event)" runat="server" Width="100px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblIDTipoIscrizione" runat="server"
                                                        Text='<%# Eval("IdRaggruppamento") %>' Width="80px"> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="DESCRIZIONE">
                                                <EditItemTemplate>
                                                    <asp:TextBox onkeypress="return handleEnter(this, event)" ID="TxtDescrizione" MaxLength="50" runat="server" Width="200px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblDescrizione" runat="server"
                                                        Text='<%# Eval("Descrizione") %>' Width="200px"> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>


                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#58A4D5" Font-Bold="True" ForeColor="Black" />
                                    </asp:GridView>



                                </ContentTemplate>

                            </AJAX:TabPanel>

                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
