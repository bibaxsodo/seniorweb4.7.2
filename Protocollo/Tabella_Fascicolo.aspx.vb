﻿
Partial Class Protocollo_Tabella_Fascicolo
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("Tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub




        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim X As New Cls_TabelleFascicolo



        X.loaddati(Session("DC_TABELLE"), Tabella)
        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()
    End Sub

    Private Sub BindMovimentiTesseramento()

        Tabella = ViewState("Tabella")
        GrdTabella.AutoGenerateColumns = False
        GrdTabella.DataSource = Tabella
        GrdTabella.DataBind()
    End Sub



    Protected Sub GrdTabella_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GrdTabella.RowCancelingEdit
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdTabella.RowCommand
        If (e.CommandName = "Inserisci") Then

            Tabella = ViewState("Tabella")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)

            ViewState("Tabella") = Tabella
            Call BindMovimentiTesseramento()
        End If
    End Sub

    Protected Sub GrdTabella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdTabella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Tabella = ViewState("Tabella")



            Dim LblIDTipoIscrizione As Label = DirectCast(e.Row.FindControl("LblIDTipoIscrizione"), Label)
            If Not IsNothing(LblIDTipoIscrizione) Then

            Else
                Dim TxtIDTipoIscrizione As TextBox = DirectCast(e.Row.FindControl("TxtIDTipoIscrizione"), TextBox)
                TxtIDTipoIscrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString()

            End If


            Dim LblUfficioDestinatario As Label = DirectCast(e.Row.FindControl("LblUfficioDestinatario"), Label)
            If Not IsNothing(LblUfficioDestinatario) Then
                Dim K As New Cls_UfficioDestinatario

                K.idUfficioDestinatario = Val(Tabella.Rows(e.Row.RowIndex).Item(1).ToString())
                K.Leggi(Session("DC_TABELLE"))

                LblUfficioDestinatario.Text = K.Descrizione
            Else
                Dim DD_UfficioDestinatario As DropDownList = DirectCast(e.Row.FindControl("DD_UfficioDestinatario"), DropDownList)
                Dim K As New Cls_UfficioDestinatario

                K.UpDropDownList(Session("DC_TABELLE"), DD_UfficioDestinatario)
                DD_UfficioDestinatario.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString()
            End If


            Dim LblCodice As Label = DirectCast(e.Row.FindControl("LblCodice"), Label)
            If Not IsNothing(LblCodice) Then

            Else
                Dim TxtCodice As TextBox = DirectCast(e.Row.FindControl("TxtCodice"), TextBox)
                TxtCodice.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString()

            End If

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString()

            End If


            Dim LblChiuso As Label = DirectCast(e.Row.FindControl("LblChiuso"), Label)
            If Not IsNothing(LblChiuso) Then
                If Val(Tabella.Rows(e.Row.RowIndex).Item(4).ToString()) = 1 Then
                    LblChiuso.Text = "Chiuso"
                Else
                    LblChiuso.Text = ""
                End If
            Else
                Dim ChkChiuso As CheckBox = DirectCast(e.Row.FindControl("ChkChiuso"), CheckBox)


                If Val(Tabella.Rows(e.Row.RowIndex).Item(4).ToString()) = 1 Then
                    ChkChiuso.Checked = True
                Else
                    ChkChiuso.Checked = False
                End If

            End If

            Dim LblDataScadenza As Label = DirectCast(e.Row.FindControl("LblDataScadenza"), Label)
            If Not IsNothing(LblDataScadenza) Then

            Else
                Dim TxtDataScadenza As TextBox = DirectCast(e.Row.FindControl("TxtDataScadenza"), TextBox)
                TxtDataScadenza.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString()
            End If


        End If
    End Sub

    Protected Sub GrdTabella_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GrdTabella.RowDeleted

    End Sub

    Protected Sub GrdTabella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdTabella.RowDeleting
        Tabella = ViewState("Tabella")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        ViewState("Tabella") = Tabella

        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdTabella.RowEditing
        GrdTabella.EditIndex = e.NewEditIndex
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GrdTabella.RowUpdated
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub


    Protected Sub GrdTabella_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GrdTabella.RowUpdating
        Dim TxtIDTipoIscrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtIDTipoIscrizione"), TextBox)
        Dim DD_UfficioDestinatario As DropDownList = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("DD_UfficioDestinatario"), DropDownList)
        Dim TxtDescrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)

        Dim ChkChiuso As CheckBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("ChkChiuso"), CheckBox)

        Dim TxtDataScadenza As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtDataScadenza"), TextBox)

        Dim TxtCodice As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtCodice"), TextBox)




        Tabella = ViewState("Tabella")
        Dim row = GrdTabella.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = Val(TxtIDTipoIscrizione.Text)
        Tabella.Rows(row.DataItemIndex).Item(1) = DD_UfficioDestinatario.SelectedValue
        Tabella.Rows(row.DataItemIndex).Item(2) = TxtCodice.Text
        Tabella.Rows(row.DataItemIndex).Item(3) = TxtDescrizione.Text
        If ChkChiuso.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(4) = 1
        Else
            Tabella.Rows(row.DataItemIndex).Item(4) = 0
        End If
        If TxtDataScadenza.Text = "" Then
            Tabella.Rows(row.DataItemIndex).Item(5) = ""
        Else
            Tabella.Rows(row.DataItemIndex).Item(5) = TxtDataScadenza.Text
        End If
        ViewState("Tabella") = Tabella
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Session("ABILITAZIONI").ToString.IndexOf("<ABILMODIFICA>") = -1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non posso Modificare');", True)
            Exit Sub
        End If

        Dim X As New Cls_TabelleFascicolo

        Tabella = ViewState("Tabella")

        X.ScriviTabella(Session("DC_TABELLE"), Tabella)


        X.loaddati(Session("DC_TABELLE"), Tabella)
        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ||  (appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



End Class
