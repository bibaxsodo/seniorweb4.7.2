﻿
Partial Class Exporta_Excel
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Tabella = Session("Excel")
        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.DataBind()


        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Esportazione.xls")        

        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())

        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        form1.Controls.Clear()
        form1.Controls.Add(GridView1)

        form1.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class
