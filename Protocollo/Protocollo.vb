﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class Protocollo_Protocollo
    Inherits WebService

    <WebMethod()>
    Public Function InserimentoProtocollo(ByVal UTENTE As String, ByVal PASSWORD As String, ByVal ANNOPROTOCOLLO As Integer,
                                          ByVal TIPOPROTOCOLLO As String, ByVal UFFICIOMITTENTE As Integer, ByVal TITOLO As Integer,
                                          ByVal CLASSE As Integer, ByVal INDICE As Integer, ByVal FASCICOLO As Integer,
                                          ByVal DATAARRIVO As String, ByVal OGGETTO As String, ByVal MEZZO As Integer,
                                          ByVal DENOMINAZIONE As String, ByVal INDIRIZZO As String, ByVal CITTA As String, ByVal CAP As String, ByVal PROVINCIA As String, ByVal STATO As String, ByVal NOTE As String) As String
        Dim LOGIN As New Cls_Login


        LOGIN.Utente = UTENTE
        LOGIN.Chiave = PASSWORD
        LOGIN.Leggi(Application("SENIOR"))

        If LOGIN.TABELLE.Trim = "" Then
            Return -99
            Exit Function
        End If

        If TIPOPROTOCOLLO <> "E" And TIPOPROTOCOLLO = "U" Then
            Return -90
            Exit Function
        End If

        Dim UFdes As New Cls_UfficioDestinatario

        UFdes.idUfficioDestinatario = Val(UFFICIOMITTENTE)
        UFdes.Leggi(LOGIN.TABELLE)

        If UFdes.Descrizione.Trim = "" And Val(UFFICIOMITTENTE) > 0 Then
            Return -10
            Exit Function
        End If

        Dim DeTitolo As New Cls_Titolo

        DeTitolo.idTitolo = Val(TITOLO)
        DeTitolo.Leggi(LOGIN.TABELLE)

        If DeTitolo.Descrizione = "" Then
            Return -11
            Exit Function
        End If


        Dim DeClasse As New Cls_Classe

        DeClasse.idTitolo = Val(TITOLO)
        DeClasse.idClasse = Val(CLASSE)
        DeClasse.Leggi(LOGIN.TABELLE)

        If DeClasse.Descrizione = "" And Val(CLASSE) > 0 Then
            Return -12
            Exit Function
        End If


        Dim DeIndice As New Cls_Indice

        DeIndice.idTitolo = Val(TITOLO)
        DeIndice.idIndice = Val(INDICE)
        DeIndice.idClasse = Val(CLASSE)
        DeIndice.Leggi(LOGIN.TABELLE)

        If DeIndice.Descrizione = "" And Val(INDICE) > 0 Then
            Return -13
            Exit Function
        End If

        Dim DeFascicolo As New Cls_TabelleFascicolo

        DeFascicolo.IdUfficioDestinatario = Val(UFFICIOMITTENTE)
        DeFascicolo.IdFascicolo = Val(FASCICOLO)
        DeFascicolo.Leggi(LOGIN.TABELLE)

        If DeFascicolo.Descrizione = "" And Val(FASCICOLO) > 0 Then
            Return -14
            Exit Function
        End If

        Dim DeMezzo As New Cls_Mezzo

        DeMezzo.idMezzo = Val(MEZZO)
        DeMezzo.Leggi(LOGIN.TABELLE)

        If DeMezzo.Descrizione = "" Then
            Return -15
            Exit Function
        End If


        Dim PROTOCOLLO As New Cls_Protocollo

        PROTOCOLLO.Utente = UTENTE
        PROTOCOLLO.Anno = Val(ANNOPROTOCOLLO)
        PROTOCOLLO.Tipo = TIPOPROTOCOLLO
        PROTOCOLLO.UfficioDestinatario = Val(UFFICIOMITTENTE)
        PROTOCOLLO.Titolo = Val(TITOLO)
        PROTOCOLLO.Classe = Val(CLASSE)
        PROTOCOLLO.Indice = Val(INDICE)
        PROTOCOLLO.FascicoloId = Val(FASCICOLO)

        PROTOCOLLO.DataArrivo = Format(Now, "dd/MM/yyyy")
        PROTOCOLLO.DataDocumento = DATAARRIVO
        PROTOCOLLO.OraArrivo = Now
        PROTOCOLLO.Oggetto = OGGETTO
        PROTOCOLLO.Mezzo = Val(MEZZO)
        PROTOCOLLO.RagioneSociale = DENOMINAZIONE
        PROTOCOLLO.Indirizzo = INDIRIZZO
        PROTOCOLLO.Citta = CITTA
        PROTOCOLLO.Cap = CAP
        PROTOCOLLO.Provincia = PROVINCIA
        PROTOCOLLO.Stato = STATO
        PROTOCOLLO.Note = NOTE
        PROTOCOLLO.Scrivi(LOGIN.TABELLE)


        Return PROTOCOLLO.Numero
    End Function



    <WebMethod()>
    Public Function ModificaProtocollo(ByVal UTENTE As String, ByVal PASSWORD As String, ByVal ANNOPROTOCOLLO As Integer,
                                      ByVal TIPOPROTOCOLLO As String, ByVal NUMEROPROTOCOLLO As Integer, ByVal UFFICIOMITTENTE As Integer, ByVal TITOLO As Integer,
                                      ByVal CLASSE As Integer, ByVal INDICE As Integer, ByVal FASCICOLO As Integer,
                                      ByVal DATAARRIVO As String, ByVal OGGETTO As String, ByVal MEZZO As Integer,
                                      ByVal DENOMINAZIONE As String, ByVal INDIRIZZO As String, ByVal CITTA As String, ByVal CAP As String, ByVal PROVINCIA As String, ByVal STATO As String, ByVal NOTE As String) As String
        Dim LOGIN As New Cls_Login


        LOGIN.Utente = UTENTE
        LOGIN.Chiave = PASSWORD
        LOGIN.Leggi(Application("SENIOR"))

        If LOGIN.TABELLE.Trim = "" Then
            Return -99
            Exit Function
        End If



        If TIPOPROTOCOLLO <> "E" And TIPOPROTOCOLLO = "U" Then
            Return -90
            Exit Function
        End If

        Dim UFdes As New Cls_UfficioDestinatario

        UFdes.idUfficioDestinatario = Val(UFFICIOMITTENTE)
        UFdes.Leggi(LOGIN.TABELLE)

        If UFdes.Descrizione.Trim = "" And Val(UFFICIOMITTENTE) > 0 Then
            Return -10
            Exit Function
        End If

        Dim DeTitolo As New Cls_Titolo

        DeTitolo.idTitolo = Val(TITOLO)
        DeTitolo.Leggi(LOGIN.TABELLE)

        If DeTitolo.Descrizione = "" Then
            Return -11
            Exit Function
        End If


        Dim DeClasse As New Cls_Classe

        DeClasse.idTitolo = Val(TITOLO)
        DeClasse.idClasse = Val(CLASSE)
        DeClasse.Leggi(LOGIN.TABELLE)

        If DeClasse.Descrizione = "" And Val(CLASSE) > 0 Then
            Return -12
            Exit Function
        End If


        Dim DeIndice As New Cls_Indice

        DeIndice.idTitolo = Val(TITOLO)
        DeIndice.idIndice = Val(INDICE)
        DeIndice.idClasse = Val(CLASSE)
        DeIndice.Leggi(LOGIN.TABELLE)

        If DeIndice.Descrizione = "" And Val(INDICE) > 0 Then
            Return -13
            Exit Function
        End If

        Dim DeFascicolo As New Cls_TabelleFascicolo

        DeFascicolo.IdUfficioDestinatario = Val(UFFICIOMITTENTE)
        DeFascicolo.IdFascicolo = Val(FASCICOLO)
        DeFascicolo.Leggi(LOGIN.TABELLE)

        If DeFascicolo.Descrizione = "" And Val(FASCICOLO) > 0 Then
            Return -14
            Exit Function
        End If

        Dim DeMezzo As New Cls_Mezzo

        DeMezzo.idMezzo = Val(MEZZO)
        DeMezzo.Leggi(LOGIN.TABELLE)

        If DeMezzo.Descrizione = "" Then
            Return -15
            Exit Function
        End If



        Dim PROTOCOLLO As New Cls_Protocollo

        PROTOCOLLO.Utente = UTENTE
        PROTOCOLLO.Anno = Val(ANNOPROTOCOLLO)
        PROTOCOLLO.Tipo = TIPOPROTOCOLLO
        PROTOCOLLO.Numero = NUMEROPROTOCOLLO
        PROTOCOLLO.Leggi(LOGIN.TABELLE)
        If PROTOCOLLO.Titolo = 0 And PROTOCOLLO.Titolo = 0 Then
            Return -1
            Exit Function
        End If


        PROTOCOLLO.UfficioDestinatario = Val(UFFICIOMITTENTE)
        PROTOCOLLO.Titolo = Val(TITOLO)
        PROTOCOLLO.Classe = Val(CLASSE)
        PROTOCOLLO.Indice = Val(INDICE)
        PROTOCOLLO.FascicoloId = Val(FASCICOLO)
        PROTOCOLLO.DataArrivo = DATAARRIVO
        PROTOCOLLO.OraArrivo = Now
        PROTOCOLLO.Oggetto = OGGETTO
        PROTOCOLLO.Mezzo = Val(MEZZO)
        PROTOCOLLO.RagioneSociale = DENOMINAZIONE
        PROTOCOLLO.Indirizzo = INDIRIZZO
        PROTOCOLLO.Citta = CITTA
        PROTOCOLLO.Cap = CAP
        PROTOCOLLO.Provincia = PROVINCIA
        PROTOCOLLO.Stato = STATO
        PROTOCOLLO.Note = NOTE
        PROTOCOLLO.Scrivi(LOGIN.TABELLE)


        Return PROTOCOLLO.Numero
    End Function
End Class
