﻿
Partial Class Protocollo_Tabella_Indice
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("Tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim X As New Cls_Indice


        X.loaddati(Session("DC_TABELLE"), Tabella)

        Session("RIGAMODIFICATA") = 0


        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()
    End Sub

    Private Sub BindMovimentiTesseramento()

        Tabella = ViewState("Tabella")
        GrdTabella.AutoGenerateColumns = False
        GrdTabella.DataSource = Tabella
        GrdTabella.DataBind()
    End Sub



    Protected Sub GrdTabella_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GrdTabella.RowCancelingEdit
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdTabella.RowCommand
        If (e.CommandName = "Inserisci") Then

            Tabella = ViewState("Tabella")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)

            ViewState("Tabella") = Tabella
            Call BindMovimentiTesseramento()
        End If
    End Sub

    Protected Sub GrdTabella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdTabella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Tabella = ViewState("Tabella")



            Dim LblIDTipoIscrizione As Label = DirectCast(e.Row.FindControl("LblIDTipoIscrizione"), Label)
            If Not IsNothing(LblIDTipoIscrizione) Then

            Else
                Dim TxtIDTipoIscrizione As TextBox = DirectCast(e.Row.FindControl("TxtIDTipoIscrizione"), TextBox)
                TxtIDTipoIscrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString()

            End If


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString()

            End If


            Dim LblTitolo As Label = DirectCast(e.Row.FindControl("LblTitolo"), Label)
            If Not IsNothing(LblTitolo) Then
                Dim TpTitolo As New Cls_Titolo

                TpTitolo.idTitolo = Val(LblTitolo.Text)
                TpTitolo.Leggi(Session("DC_TABELLE"))

                LblTitolo.Text = Val(LblTitolo.Text) & " - " & TpTitolo.Descrizione
            Else
                Session("RIGAMODIFICATA") = e.Row.RowIndex
                Dim DDTitolo As DropDownList = DirectCast(e.Row.FindControl("DD_Titolo"), DropDownList)

                Dim TpTitolo As New Cls_Titolo

                TpTitolo.UpDropDownList(Session("DC_TABELLE"), DDTitolo)

                DDTitolo.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(2).ToString()

            End If



            Dim LblClasse As Label = DirectCast(e.Row.FindControl("LblClasse"), Label)
            If Not IsNothing(LblClasse) Then
                Dim TpTitolo As New Cls_Classe

                TpTitolo.idTitolo = Val(LblTitolo.Text)
                TpTitolo.idClasse = Val(LblClasse.Text)
                TpTitolo.Leggi(Session("DC_TABELLE"))

                LblClasse.Text = Val(LblClasse.Text) & " - " & TpTitolo.Descrizione
            Else
                Dim DD_Classe As DropDownList = DirectCast(e.Row.FindControl("DD_Classe"), DropDownList)

                Dim TpTitolo As New Cls_Classe

                TpTitolo.UpDropDownList(Session("DC_TABELLE"), DD_Classe)

                DD_Classe.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString()

            End If

        End If
    End Sub


    Protected Sub GrdTabella_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GrdTabella.RowDeleted

    End Sub

    Protected Sub GrdTabella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdTabella.RowDeleting
        Tabella = ViewState("Tabella")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        ViewState("Tabella") = Tabella

        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdTabella.RowEditing
        GrdTabella.EditIndex = e.NewEditIndex
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GrdTabella.RowUpdated
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub


    Protected Sub GrdTabella_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GrdTabella.RowUpdating
        Dim TxtIDTipoIscrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtIDTipoIscrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtData"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim DDTitolo As DropDownList = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("DD_Titolo"), DropDownList)
        Dim DD_Classe As DropDownList = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("DD_Classe"), DropDownList)




        Tabella = ViewState("Tabella")
        Dim row = GrdTabella.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = Val(TxtIDTipoIscrizione.Text)
        Tabella.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text

        Tabella.Rows(row.DataItemIndex).Item(2) = DDTitolo.SelectedValue
        Tabella.Rows(row.DataItemIndex).Item(3) = DD_Classe.SelectedValue


        ViewState("Tabella") = Tabella
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Session("ABILITAZIONI").ToString.IndexOf("<ABILMODIFICA>") = -1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non posso Modificare');", True)
            Exit Sub
        End If

        Dim X As New Cls_Indice

        Tabella = ViewState("Tabella")

        X.ScriviTabella(Session("DC_TABELLE"), Tabella)


        X.loaddati(Session("DC_TABELLE"), Tabella)
        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub

    Protected Sub DD_Classe_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim klasClasse As New Cls_Classe
        Dim DDTitolo As DropDownList = DirectCast(GrdTabella.Rows(Session("RIGAMODIFICATA")).FindControl("DD_Titolo"), DropDownList)
        Dim DD_Classe As DropDownList = DirectCast(GrdTabella.Rows(Session("RIGAMODIFICATA")).FindControl("DD_Classe"), DropDownList)


        klasClasse.UpDropDownListClasse(Session("DC_TABELLE"), DD_Classe, DDTitolo.SelectedValue)
    End Sub

End Class
