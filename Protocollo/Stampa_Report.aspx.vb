﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Partial Class Stampa_Report
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        With CrystalReportViewer1
            .HasDrillUpButton = True
            .HasExportButton = True
            .HasGotoPageButton = True
            .HasPageNavigationButtons = True
            .HasPrintButton = True
            .HasRefreshButton = True
            .HasSearchButton = True
            .HasToggleGroupTreeButton = True
            .HasViewList = True
            .HasZoomFactorList = True
        End With
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim rpt As New ReportDocument



        'x = Session("stampa")


        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & Request.Item("REPORT"))
        rpt.SetDataSource(Session("stampa"))


        CrystalReportViewer1.DisplayGroupTree = False

        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ReuseParameterValuesOnRefresh = True

        CrystalReportViewer1.Visible = True
    End Sub
End Class
