﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO
Imports System.Security.Cryptography


Partial Class Protocollo
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Anagrafica')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Anagrafiche.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_OraArrivo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99.99""); "
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim klasRag As New Cls_RaggruppamentoRubrica


        klasRag.UpDropDown(Session("DC_TABELLE"), DD_Ragruppamento)



        

        DD_Tipo.Items.Add("E")
        DD_Tipo.Items.Add("U")
        Lbl_TipoEU.Text = "Entrata"

        DD_TipoSuccessivo.Items.Add("E")
        DD_TipoSuccessivo.Items.Add("U")


        DD_TipoPrecedente.Items.Add("E")
        DD_TipoPrecedente.Items.Add("U")


        Dim klasTitolo As New Cls_Titolo


        klasTitolo.UpDropDownList(Session("DC_TABELLE"), DD_Titolo)

        Try
            If Session("ABILITAZIONI").IndexOf("<SOLOENTRATE>") >= 0 Then
                DD_Tipo.SelectedValue = "E"
                DD_Tipo.Enabled = False
                Call DD_Tipo_TextChanged(sender, e)
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOUSCITE>") >= 0 Then
                DD_Tipo.SelectedValue = "U"
                DD_Tipo.Enabled = False
                Call DD_Tipo_TextChanged(sender, e)
            End If
        Catch ex As Exception
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End Try

        DD_Report.Items.Add("Informativa")

        Dim kMezzo As New Cls_Mezzo

        kMezzo.UpDropDownList(Session("DC_TABELLE"), DD_Mezzo)


        Dim kUfficioDestinatario As New Cls_UfficioDestinatario

        kUfficioDestinatario.UpDropDownList(Session("DC_TABELLE"), DD_UfficioDestinatario)

        Txt_Anno.Text = Year(Now)

        Txt_DataArrivo.Text = UltimaData(Val(Txt_Anno.Text), DD_Tipo.SelectedValue)
        If Session("DC_TABELLE").IndexOf("AspCharitas") > 0 Then
            Txt_DataArrivo.Text = Format(Now, "dd/MM/yyyy")
        End If

        Txt_OraArrivo.Text = Format(Now, "HH:mm")

        If Val(Request.Item("Numero")) = 0 And Val(Request.Item("Anno")) = 0 Then
            'Txt_Fascicolo.Enabled = False
            'Txt_Fascicolo.BackColor = System.Drawing.Color.LightGray
        End If

        If Val(Request.Item("Numero")) > 0 And Val(Request.Item("Anno")) > 0 Then
            Dim l As New Cls_Protocollo

            l.Anno = Val(Request.Item("Anno"))
            l.Numero = Val(Request.Item("Numero"))
            l.Tipo = Request.Item("Tipo")
            l.Leggi(Session("DC_TABELLE"))

            Txt_Anno.Text = l.Anno
            Txt_Numero.Text = l.Numero
            DD_Tipo.SelectedValue = l.Tipo
            DD_Titolo.SelectedValue = l.Titolo

            Dim klasClasse As New Cls_Classe


            klasClasse.UpDropDownListClasse(Session("DC_TABELLE"), DD_Classe, Val(DD_Titolo.SelectedValue))

            Txt_DataLettere.Text = l.DataDocumento
            Txt_DataArrivo.Text = l.DataArrivo
            Txt_NumeroDocumento.Text = l.NumeroDocumento
            Txt_Oggetto.Text = l.Oggetto
            DD_Mezzo.SelectedValue = l.Mezzo
            DD_UfficioDestinatario.SelectedValue = l.UfficioDestinatario

            Dim K As New Cls_TabelleFascicolo

            K.UpDropDownListClasse(Session("DC_TABELLE"), DD_Fascicolo, DD_UfficioDestinatario.SelectedValue)

            DD_Fascicolo.SelectedValue = l.FascicoloId

            Txt_DatiAnagrafici.Text = l.RagioneSociale
            Txt_Indirizzo.Text = l.Indirizzo
            Txt_Citta.Text = l.Citta
            Txt_Cap.Text = l.Cap
            Txt_Provincia.Text = l.Provincia
            Txt_Stato.Text = l.Stato
            Txt_Note.Text = l.Note

            DD_Classe.SelectedValue = l.Classe



            If l.CONSENSOINSERIMENTO = 2 Then
                RB_NOConsenso1.Checked = True
            End If

            If l.CONSENSOINSERIMENTO = 1 Then
                RB_SIConsenso1.Checked = True
            End If



            If l.CONSENSOMARKETING = 0 Then
                RB_NOConsenso2.Checked = True
            End If

            If l.CONSENSOMARKETING = 1 Then
                RB_SIConsenso2.Checked = True
            End If


            Dim klasIndice As New Cls_Indice


            klasIndice.UpDropDownListClasse(Session("DC_TABELLE"), DD_Indice, Val(DD_Titolo.SelectedValue), Val(DD_Classe.SelectedValue))

            DD_Indice.SelectedValue = l.Indice
            Txt_Fascicolo.Text = l.Fascicolo

            If Hour(l.OraArrivo) < 10 Then
                Txt_OraArrivo.Text = "0" & Hour(l.OraArrivo) & "." & Minute(l.OraArrivo)
            Else
                Txt_OraArrivo.Text = Hour(l.OraArrivo) & "." & Minute(l.OraArrivo)
            End If


            If DD_Tipo.SelectedValue = "E" Then
                Lbl_TipoEU.Text = "Corrispondenza Ricevuta"
                Lbl_DataArrivo.Text = "Data Arrivo :"
                Lbl_ufficiodestinatario.Text = "Ufficio mittente :"
            Else
                Lbl_TipoEU.Text = "Corrispondenza Inviata"
                Lbl_DataArrivo.Text = "Data Invio :"
                Lbl_ufficiodestinatario.Text = "Ufficio destinatario :"
            End If

            Call LeggiDirectory()


            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim i As Integer


            KlS.Anno = Txt_Anno.Text
            KlS.Numero = Txt_Numero.Text
            KlS.Tipo = DD_Tipo.SelectedValue
            KlS.Leggi(Session("DC_TABELLE"))

            Dim ConvT As New Cls_DataTableToJson
            Dim AppoggioJS As String = ConvT.SerializeObject(l)
            Dim lOG As New Cls_LogPrivacy

            lOG.LogPrivacy(Session("DC_TABELLE"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "PROTOCOLLO", "V", "", AppoggioJS)

            Chk_Selezionati.Items.Clear()
            Lbl_Titolo.Text = ""
            For i = 0 To 1000
                If KlS.IdRubrica(i) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(i)
                    Kppp.Leggi(Session("DC_TABELLE"))

                    Dim MyHtml As String

                    Dim Vettore(100) As String

                    Vettore = SplitWords("R-" & KlS.IdRubrica(i) & " " & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome)
                    MyHtml = "<b>" & "R-" & KlS.IdRubrica(i) & " " & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & "</b><br/>"

                    Dim KRubrica As New Cls_Rubrica

                    KRubrica.Id = Val(Vettore(1))
                    KRubrica.Leggi(Session("DC_TABELLE"))

                    MyHtml = MyHtml & KRubrica.Indirizzo & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KRubrica.Provincia
                    KComune.Comune = KRubrica.Comune
                    KComune.Leggi(Session("DC_OSPITE"))

                    MyHtml = MyHtml & KRubrica.Cap & " " & KComune.Descrizione & " " & KComune.CodificaProvincia


                    Chk_Selezionati.Items.Add(MyHtml)


                    If Lbl_Titolo.Text = "" Then
                        Lbl_Titolo.Text = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                    End If
                End If
                If KlS.IdClienteFornitore(i) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(i)
                    KpppS.Leggi(Session("DC_OSPITE"))

                    Dim KAnagrafica As New Cls_ClienteFornitore
                    Dim MyHtml As String

                    Dim Vettore(100) As String

                    Vettore = SplitWords("C-D" & KlS.IdClienteFornitore(i) & " " & KpppS.Nome)
                    MyHtml = "<b>" & "C-D" & KlS.IdClienteFornitore(i) & " " & KpppS.Nome & "</b><br/>"




                    'Chk_Selezionati.Items.Add("C-" & KlS.IdClienteFornitore(i) & " " & KpppS.Nome)


                    Dim KCliFor As New Cls_ClienteFornitore

                    KCliFor.CODICEDEBITORECREDITORE = Val(Vettore(1))
                    KCliFor.Leggi(Session("DC_OSPITE"))

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    If IsNothing(KCliFor.RESIDENZACOMUNE1) Or IsNothing(KCliFor.RESIDENZAPROVINCIA1) Then
                    Else
                        KComune.Leggi(Session("DC_OSPITE"))
                    End If

                    MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia

                    Chk_Selezionati.Items.Add(MyHtml)

                    If Lbl_Titolo.Text = "" Then
                        Lbl_Titolo.Text = KpppS.Nome
                    End If
                End If

                If KlS.IdOspite(i) > 0 And KlS.IdParente(i) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(i)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)

                    Dim KAnagrafica As New Cls_ClienteFornitore
                    Dim MyHtml As String

                    Dim Vettore(100) As String

                    Vettore = SplitWords("C-O" & KlS.IdOspite(i) & " " & KpppS.Nome)
                    MyHtml = "<b>" & "C-O" & KlS.IdOspite(i) & " " & KpppS.Nome & "</b><br/>"

                    'Chk_Selezionati.Items.Add("C-" & KlS.IdClienteFornitore(i) & " " & KpppS.Nome)


                    Dim KCliFor As New ClsOspite

                    KCliFor.CodiceOspite = KlS.IdOspite(i)
                    KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite)

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    KComune.Leggi(Session("DC_OSPITE"))

                    MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia

                    Chk_Selezionati.Items.Add(MyHtml)

                    If Lbl_Titolo.Text = "" Then
                        Lbl_Titolo.Text = KpppS.Nome
                    End If
                End If

                If KlS.IdParente(i) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(i)
                    KpppS.CodiceParente = KlS.IdParente(i)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)

                    Dim KAnagrafica As New Cls_ClienteFornitore
                    Dim MyHtml As String

                    Dim Vettore(100) As String

                    Vettore = SplitWords("C-P" & KlS.IdOspite(i) & "-" & KlS.IdParente(i) & " " & KpppS.Nome)
                    MyHtml = "<b>" & "C-P" & KlS.IdOspite(i) & "-" & KlS.IdParente(i) & " " & KpppS.Nome & "</b><br/>"

                    'Chk_Selezionati.Items.Add("C-" & KlS.IdClienteFornitore(i) & " " & KpppS.Nome)


                    Dim KCliFor As New Cls_Parenti

                    KCliFor.CodiceOspite = KlS.IdOspite(i)
                    KCliFor.CodiceParente = KlS.IdParente(i)
                    KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite, KCliFor.CodiceParente)

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    KComune.Leggi(Session("DC_OSPITE"))

                    MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia

                    Chk_Selezionati.Items.Add(MyHtml)

                    If Lbl_Titolo.Text = "" Then
                        Lbl_Titolo.Text = KpppS.Nome
                    End If
                End If

            Next
            If Lbl_Titolo.Text = "" Then
                Lbl_Titolo.Text = Txt_DatiAnagrafici.Text
            End If


        End If

    End Sub

    Protected Sub Btn_EstraiRubrica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_EstraiRubrica.Click
        Dim k As New Cls_Rubrica

        k.UpCheckBoxList(Session("DC_TABELLE"), Val(DD_Ragruppamento.SelectedValue), Lst_Rubrica)


    End Sub

    Protected Sub Btn_SelectAllR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_SelectAllR.Click
        Dim I As Integer

        For I = 0 To Lst_Rubrica.Items.Count - 1
            Lst_Rubrica.Items(I).Selected = True
        Next
    End Sub

    Private Function UltimaData(ByVal Anno As Integer, ByVal Tipo As String) As Date
        Dim K As New Cls_Protocollo

        K.Anno = Anno
        K.Tipo = Tipo

        UltimaData = K.UltimaData(Session("DC_TABELLE"))
    End Function




    Protected Sub Btn_InvertiR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_InvertiR.Click
        Dim I As Integer

        For I = 0 To Lst_Rubrica.Items.Count - 1
            If Lst_Rubrica.Items(I).Selected = True Then
                Lst_Rubrica.Items(I).Selected = False
            Else
                Lst_Rubrica.Items(I).Selected = True
            End If
        Next
    End Sub

    Protected Sub Btn_EstraiCliFor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_EstraiCliFor.Click
        Dim k As New Cls_ClienteFornitore

        k.UpCheckBoxList(Session("DC_OSPITE"), Txt_RagioneSociale.Text, Lst_ClientiFornitori)

    End Sub

    Protected Sub Btn_AggiungiAnagrafica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_AggiungiAnagrafica.Click
        Dim MyHtml As String

        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_Anagrafica.Text)
        MyHtml = "<b>" & Txt_Anagrafica.Text & "</b><br/>"

        If Vettore.Length > 1 Then
            Dim Appoggio As String

            Appoggio = Vettore(0)


            If Mid(Appoggio, 1, 1) = "R" Then
                Dim KRubrica As New Cls_Rubrica

                KRubrica.Id = Val(Vettore(1))
                KRubrica.Leggi(Session("DC_TABELLE"))

                MyHtml = MyHtml & KRubrica.Indirizzo & "<br>"

                Dim KComune As New ClsComune

                KComune.Provincia = KRubrica.Provincia
                KComune.Comune = KRubrica.Comune
                KComune.Leggi(Session("DC_OSPITE"))

                MyHtml = MyHtml & KRubrica.Cap & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
            End If
            If Mid(Appoggio, 1, 1) = "C" Then
                If Mid(Vettore(1), 1, 1) = "D" Then
                    Dim KCliFor As New Cls_ClienteFornitore

                    KCliFor.CODICEDEBITORECREDITORE = Val(Vettore(1))
                    KCliFor.Leggi(Session("DC_OSPITE"))

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    If KComune.Comune <> "" And KComune.Provincia <> "" Then
                        KComune.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                    End If
                End If
                If Mid(Vettore(1), 1, 1) = "O" Then
                    Dim KCliFor As New ClsOspite

                    KCliFor.CodiceOspite = Val(Mid(Vettore(1), 2, Len(Vettore(1)) - 1))
                    KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite)

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    If KComune.Comune <> "" And KComune.Provincia <> "" Then
                        KComune.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                    End If
                End If


                If Mid(Vettore(1), 1, 1) = "P" Then
                    Dim KCliFor As New ClsOspite

                    KCliFor.CodiceOspite = Val(Mid(Vettore(1), 2, Len(Vettore(1)) - 1))
                    KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite)

                    MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                    Dim KComune As New ClsComune

                    KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                    KComune.Comune = KCliFor.RESIDENZACOMUNE1
                    If KComune.Comune <> "" And KComune.Provincia <> "" Then
                        KComune.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                    End If
                End If
            End If
        End If
        Chk_Selezionati.Items.Add(MyHtml)

        Txt_Anagrafica.Text = ""
    End Sub


    Protected Sub Btn_AggiungiAnagraficaR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_AggiungiAnagraficaR.Click
        Dim I As Integer

        For I = 0 To Lst_Rubrica.Items.Count - 1
            If Lst_Rubrica.Items(I).Selected = True Then
                Dim MyHtml As String

                Dim Vettore(100) As String
                Vettore = SplitWords("R" & "-" & Lst_Rubrica.Items(I).Value & " " & Lst_Rubrica.Items(I).Text)
                MyHtml = "<b>" & "R" & "-" & Lst_Rubrica.Items(I).Value & " " & Lst_Rubrica.Items(I).Text & "</b><br/>"

                If Vettore.Length > 1 Then
                    Dim Appoggio As String

                    Appoggio = Vettore(0)


                    If Mid(Appoggio, 1, 1) = "R" Then
                        Dim KRubrica As New Cls_Rubrica

                        KRubrica.Id = Val(Vettore(1))
                        KRubrica.Leggi(Session("DC_TABELLE"))

                        MyHtml = MyHtml & KRubrica.Indirizzo & "<br>"

                        Dim KComune As New ClsComune

                        KComune.Provincia = KRubrica.Provincia
                        KComune.Comune = KRubrica.Comune
                        KComune.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KRubrica.Cap & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                    End If
                    If Mid(Appoggio, 1, 1) = "C" Then
                        Dim KCliFor As New Cls_ClienteFornitore

                        KCliFor.CODICEDEBITORECREDITORE = Val(Vettore(1))
                        KCliFor.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                        Dim KComune As New ClsComune

                        KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                        KComune.Comune = KCliFor.RESIDENZACOMUNE1
                        KComune.Leggi(Session("DC_OSPITE"))

                        MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                    End If
                End If
                Chk_Selezionati.Items.Add(MyHtml)


                'Chk_Selezionati.Items.Add("R" & "-" & Lst_Rubrica.Items(I).Value & " " & Lst_Rubrica.Items(I).Text)
                Lst_Rubrica.Items(I).Selected = False
            End If
        Next
    End Sub

    Protected Sub Btn_AggiungiAnagraficaCF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_AggiungiAnagraficaCF.Click
        Dim I As Integer

        For I = 0 To Lst_ClientiFornitori.Items.Count - 1
            If Lst_ClientiFornitori.Items(I).Selected = True Then
                Dim MyHtml As String

                Dim Vettore(100) As String
                Vettore = SplitWords("C" & "-" & Lst_ClientiFornitori.Items(I).Value & " " & Lst_ClientiFornitori.Items(I).Text)
                MyHtml = "<b>" & "C" & "-" & Lst_ClientiFornitori.Items(I).Value & " " & Lst_ClientiFornitori.Items(I).Text & "</b><br/>"

                If Vettore.Length > 1 Then
                    Dim Appoggio As String

                    Appoggio = Vettore(0)

                    If Mid(Appoggio, 1, 1) = "R" Then
                        Dim KRubrica As New Cls_Rubrica

                        KRubrica.Id = Val(Vettore(1))
                        KRubrica.Leggi(Session("DC_TABELLE"))

                        MyHtml = MyHtml & KRubrica.Indirizzo & "<br>"

                        Dim KComune As New ClsComune

                        KComune.Provincia = KRubrica.Provincia
                        KComune.Comune = KRubrica.Comune
                        If KComune.Comune <> "" And KComune.Provincia <> "" Then
                            KComune.Leggi(Session("DC_OSPITE"))

                            MyHtml = MyHtml & KRubrica.Cap & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                        End If
                    End If
                    If Mid(Appoggio, 1, 1) = "C" Then
                        If Mid(Vettore(1), 1, 1) = "D" Then
                            Dim KCliFor As New Cls_ClienteFornitore

                            KCliFor.CODICEDEBITORECREDITORE = Val(Vettore(1))
                            KCliFor.Leggi(Session("DC_OSPITE"))

                            MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                            Dim KComune As New ClsComune

                            KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                            KComune.Comune = KCliFor.RESIDENZACOMUNE1
                            If KComune.Comune <> "" And KComune.Provincia <> "" Then
                                KComune.Leggi(Session("DC_OSPITE"))

                                MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                            End If
                        End If
                        If Mid(Vettore(1), 1, 1) = "O" Then
                            Dim KCliFor As New ClsOspite

                            KCliFor.CodiceOspite = Val(Mid(Vettore(1), 2, Len(Vettore(1)) - 1))
                            KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite)

                            MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                            Dim KComune As New ClsComune

                            KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                            KComune.Comune = KCliFor.RESIDENZACOMUNE1
                            If KComune.Comune <> "" And KComune.Provincia <> "" Then
                                KComune.Leggi(Session("DC_OSPITE"))

                                MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                            End If
                        End If


                        If Mid(Vettore(1), 1, 1) = "P" Then
                            Dim KCliFor As New ClsOspite

                            KCliFor.CodiceOspite = Val(Mid(Vettore(1), 2, Len(Vettore(1)) - 1))
                            KCliFor.Leggi(Session("DC_OSPITE"), KCliFor.CodiceOspite)

                            MyHtml = MyHtml & KCliFor.RESIDENZAINDIRIZZO1 & "<br>"

                            Dim KComune As New ClsComune

                            KComune.Provincia = KCliFor.RESIDENZAPROVINCIA1
                            KComune.Comune = KCliFor.RESIDENZACOMUNE1
                            If KComune.Comune <> "" And KComune.Provincia <> "" Then
                                KComune.Leggi(Session("DC_OSPITE"))

                                MyHtml = MyHtml & KCliFor.RESIDENZACAP1 & " " & KComune.Descrizione & " " & KComune.CodificaProvincia
                            End If
                        End If


                    End If


                End If
                Chk_Selezionati.Items.Add(MyHtml)

                'Chk_Selezionati.Items.Add("C" & "-" & Lst_ClientiFornitori.Items(I).Value & " " & Lst_ClientiFornitori.Items(I).Text)
                Lst_ClientiFornitori.Items(I).Selected = False
            End If
        Next
    End Sub

    Protected Sub Btn_Rimuovi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Rimuovi.Click
        Dim I As Integer

        For I = Chk_Selezionati.Items.Count - 1 To 0 Step -1
            If Chk_Selezionati.Items(I).Selected = True Then
                Chk_Selezionati.Items.RemoveAt(I)
            End If
        Next
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Session("ABILITAZIONI").ToString.IndexOf("<ABILMODIFICA>") = -1 Then
            If Grid.Rows.Count > 0 Then
                Dim LblNomeFile As Label = DirectCast(Grid.Rows(0).FindControl("LblNomeFile"), Label)
                If Grid.Rows.Count > 1 Or LblNomeFile.Text <> "" Then
                    If IsDate(Txt_DataArrivo.Text) Then
                        Dim Data As Date
                        Data = Txt_DataArrivo.Text
                        If DateDiff("d", Txt_DataArrivo.Text, Now) > 5 Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi modificare il protocollo</center>');", True)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If Month(Now) = 1 And Day(Now) < 15 And Session("NomeEPersonam").ToString.IndexOf("Siena") >= 0 Then
                If Val(Txt_Anno.Text) = Year(Now) And Val(Txt_Anno.Text) = Year(Now) - 1 Then
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Anno protocollo errato</center>');", True)
                    Exit Sub
                End If
            Else
                If Val(Txt_Anno.Text) < Year(Now) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Anno protocollo minore del corrente</center>');", True)
                    Exit Sub
                End If
            End If
            
        End If

        If Txt_DatiAnagrafici.Text <> "" Then
            If RB_SIConsenso1.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il consenso all inserimento dei dati in Iuvant');", True)
                Exit Sub
            End If
        End If

        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifica  anno protocollo</center>');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataArrivo.Text) Then
            If DD_Tipo.SelectedValue = "E" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifirare Data di Arrivo</center>');", True)
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifirare Data di Invio</center>');", True)
            End If
            Exit Sub
        End If

        If DD_Mezzo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Mezzo obbligatorio</center>');", True)
            Exit Sub
        End If
        If Chk_Selezionati.Items.Count < 1 And Txt_DatiAnagrafici.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare almeno un destinatario/mittente</center>');", True)
            Exit Sub
        End If

        If Trim(Txt_OraArrivo.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifirare ora arrivo</center>');", True)
            Exit Sub
        End If

        If Trim(Txt_Oggetto.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifirare oggetto</center>');", True)
            Exit Sub
        End If
        If Val(Txt_Numero.Text) = 0 Then
            Dim DataAppoggio As Date = Txt_DataArrivo.Text
            If Format(UltimaData(Val(Txt_Anno.Text), DD_Tipo.SelectedValue), "yyyyMMdd") > Format(DataAppoggio, "yyyyMMdd") Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Data Arrivo minore di ultimo protocollo</center>');", True)
                Exit Sub
            End If
        End If


        Dim k As New Cls_Protocollo

        k.Anno = Val(Txt_Anno.Text)
        k.Numero = Val(Txt_Numero.Text)
        k.Tipo = DD_Tipo.SelectedValue
        k.Titolo = Val(DD_Titolo.SelectedValue)
        k.DataDocumento = IIf(IsDate(Txt_DataLettere.Text), Txt_DataLettere.Text, Nothing)
        k.DataArrivo = Txt_DataArrivo.Text
        k.NumeroDocumento = Txt_NumeroDocumento.Text
        k.Oggetto = Txt_Oggetto.Text
        k.Mezzo = Val(DD_Mezzo.SelectedValue)
        k.UfficioDestinatario = Val(DD_UfficioDestinatario.SelectedValue)
        k.RagioneSociale = Txt_DatiAnagrafici.Text
        k.Indirizzo = Txt_Indirizzo.Text
        k.Citta = Txt_Citta.Text
        k.Cap = Txt_Cap.Text
        k.Provincia = Txt_Provincia.Text
        k.Stato = Txt_Stato.Text
        k.Note = Txt_Note.Text
        k.Utente = Session("UTENTE")

        If RB_NOConsenso1.Checked = True Then
            k.CONSENSOINSERIMENTO = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            k.CONSENSOINSERIMENTO = 1
        End If


        If RB_NOConsenso2.Checked = True Then
            k.CONSENSOMARKETING = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            k.CONSENSOMARKETING = 1
        End If

        k.FascicoloId = Val(DD_Fascicolo.SelectedValue)

        k.Classe = Val(DD_Classe.SelectedValue)
        k.Indice = Val(DD_Indice.SelectedValue)
        k.Fascicolo = Txt_Fascicolo.Text
        k.OraArrivo = DateSerial(1889, 12, 30).AddHours(Mid(Txt_OraArrivo.Text, 1, 2)).AddMinutes(Mid(Txt_OraArrivo.Text, 4, 2))
        k.Scrivi(Session("DC_TABELLE"))


        Txt_Numero.Text = k.Numero

        Dim Ks As New Cls_Allegati

        MyTable = Session("Documentazione")

        If Not IsNothing(MyTable) Then
            Ks.Anno = Txt_Anno.Text
            Ks.NumeroProtocollo = Txt_Numero.Text
            Ks.Tipo = DD_Tipo.SelectedValue
            Ks.Scrivi(Session("DC_TABELLE"), MyTable)
        End If


        Dim KlS As New Cls_ProtocolloAnagrafiche
        Dim I As Integer
        Dim Indice As Integer
        Dim IndSt As Integer
        Dim Stringa As String


        KlS.Anno = Txt_Anno.Text
        KlS.Numero = Val(Txt_Numero.Text)
        KlS.Tipo = DD_Tipo.SelectedValue

        For I = 0 To Chk_Selezionati.Items.Count - 1

            Indice = Indice + 1
            If Mid(Chk_Selezionati.Items(I).Text.Replace("<b>", ""), 1, 1) = "R" Then
                IndSt = Chk_Selezionati.Items(I).Text.IndexOf(" ")
                Stringa = Chk_Selezionati.Items(I).Text.Replace("<b>", "")
                KlS.IdRubrica(Indice) = Val(Mid(Stringa, 3, IndSt - 2))
            End If
            If Mid(Chk_Selezionati.Items(I).Text.Replace("<b>", ""), 1, 1) = "C" Then
                IndSt = Chk_Selezionati.Items(I).Text.IndexOf(" ")
                Stringa = Chk_Selezionati.Items(I).Text.Replace("<b>", "")
                If Mid(Stringa, 3, 1) = "D" Then
                    KlS.IdClienteFornitore(Indice) = Val(Mid(Stringa, 4, IndSt - 2))
                End If
                If Mid(Stringa, 3, 1) = "O" Then
                    KlS.IdOspite(Indice) = Val(Mid(Stringa, 4, IndSt - 2))
                End If
                If Mid(Stringa, 3, 1) = "P" Then
                    KlS.IdOspite(Indice) = Val(Mid(Stringa, 4, Stringa.IndexOf("-", 3) - 2))
                    KlS.IdParente(Indice) = Val(Mid(Stringa, Stringa.IndexOf("-", 3) + 2, Stringa.IndexOf(" ") - Stringa.IndexOf("-", 3)))
                End If

            End If
            If Mid(Chk_Selezionati.Items(I).Text.Replace("<b>", ""), 1, 1) = "A" Then
                IndSt = Chk_Selezionati.Items(I).Text.Replace("<b>", "").IndexOf(" ")
                Stringa = Chk_Selezionati.Items(I).Text.Replace("<b>", "")
                KlS.IdAnagrafica(Indice) = Val(Mid(Stringa, 3, IndSt - 2))
            End If

        Next
        KlS.Scrivi(Session("DC_TABELLE"))

        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String = ConvT.SerializeObject(k)
        Dim lOG As New Cls_LogPrivacy

        lOG.LogPrivacy(Session("DC_TABELLE"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "PROTOCOLLO", "M", "", AppoggioJS)



        Dim MyJs As String
        MyJs = "alert('Dati Protocollo Inseriti, NUMERO " & k.Numero & "   ');"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", MyJs, True)



        Exit Sub

        Txt_Anno.Text = Year(Now)
        Txt_Numero.Text = 0


        Txt_DataLettere.Text = ""
        Txt_DataArrivo.Text = ""
        Txt_NumeroDocumento.Text = ""
        Txt_Oggetto.Text = ""
        Lbl_Titolo.Text = ""

        Txt_DatiAnagrafici.Text = ""
        Txt_Indirizzo.Text = ""
        Txt_Citta.Text = ""
        Txt_Cap.Text = ""
        Txt_Provincia.Text = ""
        Txt_Stato.Text = ""
        Txt_Note.Text = ""
        Chk_Selezionati.Items.Clear()
        Lst_Rubrica.Items.Clear()

        Txt_RagioneSociale.Text = ""
        Txt_Anagrafica.Text = ""


        Try
            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("NomeFile", GetType(String))
            MyTable.Columns.Add("Descrizione", GetType(String))
            MyTable.Columns.Add("Data", GetType(String))



            Session("Documentazione") = MyTable
            Grid.AutoGenerateColumns = False
            Grid.DataSource = MyTable
            Grid.DataBind()

        Catch ex As Exception

        End Try


        Dim klasRag As New Cls_RaggruppamentoRubrica


        klasRag.UpDropDown(Session("DC_TABELLE"), DD_Ragruppamento)

        Dim klasTitolo As New Cls_Titolo


        klasTitolo.UpDropDownList(Session("DC_TABELLE"), DD_Titolo)




        Dim kMezzo As New Cls_Mezzo

        kMezzo.UpDropDownList(Session("DC_TABELLE"), DD_Mezzo)


        Dim kUfficioDestinatario As New Cls_UfficioDestinatario

        kUfficioDestinatario.UpDropDownList(Session("DC_TABELLE"), DD_UfficioDestinatario)

    End Sub
    Private Sub LeggiDirectory()


        Dim k As New Cls_Login
        Dim NomeSocieta As String

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = "Protocollo_" & k.RagioneSociale


        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("NomeFile", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue)
        End If



        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Console.WriteLine(objItem.Name)

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga(0) = objItem.Name

            Dim x As New Cls_Allegati

            x.Anno = Val(Txt_Anno.Text)
            x.NumeroProtocollo = Val(Txt_Numero.Text)
            x.Tipo = DD_Tipo.SelectedValue
            x.NomeFile = objItem.Name
            x.Leggi(Session("DC_TABELLE"))

            myriga(0) = objItem.Name
            myriga(1) = x.DESCRIZIONE
            If x.DATA = Nothing Then
                myriga(2) = ""
            Else
                myriga(2) = Format(x.DATA, "dd/MM/yyyy")
            End If

            MyTable.Rows.Add(myriga)

        Next

        Session("Documentazione") = MyTable
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()

    End Sub



    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpLoadFile.Click

        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErrore", "VisualizzaErrore('Devi prima inserire il protocollo');", True)
            Exit Sub
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErrore", "VisualizzaErrore('Devi prima inserire il protocollo');", True)
            Exit Sub
        End If

        If FileUpload1.FileName.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErroreFileNonSpecificato", "VisualizzaErrore('Specificare un file.');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login
        Dim NomeSocieta As String

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))



        If k.RagioneSociale = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErrore", "VisualizzaErrore('Non posso procedere');", True)
            Exit Sub
        End If
        NomeSocieta = "Protocollo_" & k.RagioneSociale

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue)
        End If




        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & FileUpload1.FileName)



        Encrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & FileUpload1.FileName, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\Crypt_" & FileUpload1.FileName)

        Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & FileUpload1.FileName)



        Call LeggiDirectory()
    End Sub

    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call BindDocumentazione()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "DOWNLOAD" Then

            Dim k As New Cls_Login
            Dim NomeSocieta As String

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = "Protocollo_" & k.RagioneSociale




            If e.CommandArgument.ToString.IndexOf("Crypt_") >= 0 Then
                Decrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & e.CommandArgument, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End If

            Try
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ClearHeaders()
                HttpContext.Current.Response.ClearContent()

                Response.Clear()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("Content-Disposition", _
                                    "attachment; filename=""" & "Copia_" & e.CommandArgument.ToString.Replace("Crypt_", "") & """")
                Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))

                Response.Flush()

            Finally
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End Try

            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.ClearHeaders()
            'HttpContext.Current.Response.ClearContent()
            'Response.Clear()
            'Response.ContentType = "application/octect-stream"
            'Response.AddHeader("Content-Disposition", _
            '                    "attachment; filename=""" & "Copia_" & e.CommandArgument & """")
            'Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & e.CommandArgument)
            'Response.End()
        End If
        If e.CommandName = "CANCELLAFILE" Then
            Dim k As New Cls_Login
            Dim NomeSocieta As String

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = "Protocollo_" & k.RagioneSociale


            MyTable = Session("Documentazione")


            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & Txt_Numero.Text & DD_Tipo.SelectedValue & "\" & MyTable.Rows(e.CommandArgument).Item(0))
            Catch ex As Exception

            End Try


            MyTable.Rows.RemoveAt(e.CommandArgument)

            If MyTable.Rows.Count = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                MyTable.Rows.Add(myriga)
            End If

            Session("Documentazione") = MyTable

            Call BindDocumentazione()

        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtDescrizione.Text = MyDv(1).ToString
            End If
            Dim LblData As Label = DirectCast(e.Row.FindControl("LblData"), Label)
            If Not IsNothing(LblData) Then

            Else
                Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtData.Text = MyDv(2).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", MyJs, True)
            End If
        End If
    End Sub

    Private Sub UpDateTableTab_Documenti()
        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("NomeFile", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))



        For i = 0 To Grid.Rows.Count - 1

            Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Dim TxtData As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtData"), TextBox)
            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga.Item(1) = TxtDescrizione.Text
            myriga.Item(2) = TxtData.Text

            MyTable.Rows.Add(myriga)
        Next

        Session("Documentazione") = MyTable

        Call BindDocumentazione()

    End Sub


    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call BindDocumentazione()
    End Sub



    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtData"), TextBox)

        If Not IsDate(TxtData.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Data formalmente errata</center>');", True)
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); });"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", MyJs, True)
            Exit Sub
        End If

        MyTable = Session("Documentazione")
        Dim row = Grid.Rows(e.RowIndex)
        MyTable.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text
        MyTable.Rows(row.DataItemIndex).Item(2) = TxtData.Text
        Session("Documentazione") = MyTable

        Grid.EditIndex = -1
        Call BindDocumentazione()
    End Sub


    Private Sub BindDocumentazione()
        MyTable = Session("Documentazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("Elenco_Protocollo.aspx")
        Else
            Response.Redirect("Elenco_Protocollo.aspx?TIPO=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_CercaSuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CercaSuc.Click


        Dim Ls As New Cls_Protocollo
        Dim DataDal As Date
        Dim DataAl As Date

        If Not IsDate(Txt_DataDalRSuc.Text) Then
            DataDal = Nothing
        Else
            DataDal = Txt_DataDalRSuc.Text
        End If
        If Not IsDate(Txt_DataAlRSuc.Text) Then
            DataAl = Nothing
        Else
            DataAl = Txt_DataAlRSuc.Text
        End If

        Ls.CaricaLista(Session("DC_TABELLE"), Lst_ListaSuc, Txt_OggettoRSuc.Text, DataDal, DataAl)
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "$(document).ready(function() { Abilita('Id_ProtSuccessivo'); });", True)

    End Sub

    Protected Sub Lst_ListaSuc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lst_ListaSuc.SelectedIndexChanged

        Dim APPOGGIO As String
        APPOGGIO = Lst_ListaSuc.SelectedItem.Text

        Dim Vettore(100) As String
        Vettore = SplitWords(APPOGGIO)

        If Vettore.Length > 3 Then
            Txt_AnnoSuccessivo.Text = Vettore(0)
            Txt_NumeroSuccessivo.Text = Vettore(1)
            If Vettore(2) = "E" Then
                DD_TipoSuccessivo.SelectedValue = "E"
            End If
            If Vettore(2) = "U" Then
                DD_TipoSuccessivo.SelectedValue = "U"
            End If
        End If


    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_CercaPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CercaPre.Click
        Dim Ls As New Cls_Protocollo
        Dim DataDal As Date
        Dim DataAl As Date

        If Not IsDate(Txt_DataDalRPrec.Text) Then
            DataDal = Nothing
        Else
            DataDal = Txt_DataDalRPrec.Text
        End If
        If Not IsDate(Txt_DataAlRPrec.Text) Then
            DataAl = Nothing
        Else
            DataAl = Txt_DataAlRPrec.Text
        End If

        Ls.CaricaLista(Session("DC_TABELLE"), Lst_ListaPrec, Txt_OggettoRPrec.Text, DataDal, DataAl)
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "$(document).ready(function() { Abilita('Id_ProtPrecedenti'); });", True)

    End Sub

    Protected Sub Lst_ListaPrec_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lst_ListaPrec.SelectedIndexChanged
        Dim APPOGGIO As String
        APPOGGIO = Lst_ListaPrec.SelectedItem.Text

        Dim Vettore(100) As String
        Vettore = SplitWords(APPOGGIO)

        If Vettore.Length > 3 Then
            Txt_AnnoPrecedente.Text = Vettore(0)
            Txt_NumeroPrecedente.Text = Vettore(1)
            If Vettore(2) = "E" Then
                DD_TipoPrecedente.SelectedValue = "E"
            End If
            If Vettore(2) = "U" Then
                DD_TipoPrecedente.SelectedValue = "U"
            End If
        End If

    End Sub



    Protected Sub DD_Tipo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Tipo.TextChanged
        If DD_Tipo.SelectedValue = "E" Then
            Lbl_TipoEU.Text = "Corrispondenza Ricevuta"
            Lbl_DataArrivo.Text = "Data Arrivo :"
            Lbl_ufficiodestinatario.Text = "Ufficio mittente :"            

            If Session("DC_TABELLE").IndexOf("AspCharitas") > 0 Then
            Else
                Txt_DataArrivo.Text = UltimaData(Val(Txt_Anno.Text), DD_Tipo.SelectedValue)
            End If
        Else
            Lbl_TipoEU.Text = "Corrispondenza Inviata"
            Lbl_DataArrivo.Text = "Data Invio :"
            Lbl_ufficiodestinatario.Text = "Ufficio destinatario :"
            If Session("DC_TABELLE").IndexOf("AspCharitas") > 0 Then
            Else
                Txt_DataArrivo.Text = UltimaData(Val(Txt_Anno.Text), DD_Tipo.SelectedValue)
            End If
        End If
    End Sub




    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Session("ABILITAZIONI").ToString.IndexOf("<ABILMODIFICA>") = -1 Then
            If Grid.Rows.Count > 0 Then
                Dim LblNomeFile As Label = DirectCast(Grid.Rows(0).FindControl("LblNomeFile"), Label)
                If Grid.Rows.Count > 1 Or LblNomeFile.Text <> "" Then
                    If IsDate(Txt_DataArrivo.Text) Then
                        Dim Data As Date
                        Data = Txt_DataArrivo.Text
                        If DateDiff("d", Txt_DataArrivo.Text, Now) > 5 Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi modificare il protocollo</center>');", True)
                            Exit Sub
                        End If
                    End If
                End If
            End If
        End If

        Dim k As New Cls_Protocollo

        k.Anno = Val(Txt_Anno.Text)
        k.Numero = Val(Txt_Numero.Text)
        k.Tipo = DD_Tipo.SelectedValue


        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String = ConvT.SerializeObject(k)
        Dim lOG As New Cls_LogPrivacy

        lOG.LogPrivacy(Session("DC_TABELLE"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "PROTOCOLLO", "E", "", AppoggioJS)


        k.Elimina(Session("DC_TABELLE"))



        If Request.Item("PAGINA") = "" Then
            Response.Redirect("Elenco_Protocollo.aspx")
        Else
            Response.Redirect("Elenco_Protocollo.aspx?TIPO=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub DD_Titolo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Titolo.SelectedIndexChanged
        Dim klasClasse As New Cls_Classe


        klasClasse.UpDropDownListClasse(Session("DC_TABELLE"), DD_Classe, DD_Titolo.SelectedValue)

        Dim klaIndice As New Cls_Indice


        klaIndice.UpDropDownListClasse(Session("DC_TABELLE"), DD_Indice, Val(DD_Titolo.SelectedValue), Val(DD_Classe.SelectedValue))
    End Sub

    Protected Sub DD_Classe_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Classe.TextChanged
        Dim klasClasse As New Cls_Indice


        klasClasse.UpDropDownListClasse(Session("DC_TABELLE"), DD_Indice, DD_Titolo.SelectedValue, DD_Classe.SelectedValue)
    End Sub

    Protected Sub DD_UfficioDestinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_UfficioDestinatario.SelectedIndexChanged
        Dim K As New Cls_TabelleFascicolo

        K.UpDropDownListClasse(Session("DC_TABELLE"), DD_Fascicolo, DD_UfficioDestinatario.SelectedValue)
    End Sub

    Protected Sub DD_Fascicolo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Fascicolo.SelectedIndexChanged
        Dim DF As New Cls_TabelleFascicolo

        DF.IdUfficioDestinatario = Val(DD_UfficioDestinatario.SelectedValue)
        DF.IdFascicolo = Val(DD_Fascicolo.SelectedValue)
        DF.Leggi(Session("DC_TABELLE"))

        Lbl_segnala.Text = ""
        Lbl_segnala.ForeColor = System.Drawing.Color.Black
        If DF.Chiuso = 1 Then
            Lbl_segnala.ForeColor = System.Drawing.Color.Red
            Lbl_segnala.Text = "Chiuso"
        End If


        Dim MiaDataArrivo As Date = Txt_DataArrivo.Text
        If Format(DF.DataScadenza, "yyyy-MM-dd") >= Format(MiaDataArrivo, "yyyy-MM-dd") Then

            Lbl_segnala.ForeColor = System.Drawing.Color.Red
            Lbl_segnala.Text = "Chiuso"
        End If
    End Sub


    Private Sub ExportPDF(ByVal NomeFile As String)
        Dim objStreamReader As StreamReader


        objStreamReader = File.OpenText(Server.MapPath(NomeFile))

        ' Leggo il contenuto del file sino alla fine (ReadToEnd)
        Dim contenuto As String = objStreamReader.ReadToEnd()

        objStreamReader.Close()

        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))



        contenuto = contenuto.Replace("@NOME@", Txt_DatiAnagrafici.Text)
        contenuto = contenuto.Replace("@NOMETITOLARE@", TabSoc.RagioneSociale)
        contenuto = contenuto.Replace("@INDIRIZZO@", TabSoc.Indirizzo)
        contenuto = contenuto.Replace("@CAP@", TabSoc.Cap)
        contenuto = contenuto.Replace("@CITTA@", TabSoc.Localita)
        contenuto = contenuto.Replace("@TEL@", TabSoc.Telefono)
        contenuto = contenuto.Replace("@EMAIL@", TabSoc.EMail)


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        contenuto = Server.UrlEncode(contenuto.Replace(vbNewLine, ""))

        Session("PdfDaHTML") = contenuto
    End Sub



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        If DD_Report.SelectedIndex = 0 Then
            ExportPDF("..\modulo\Informativa_GDPR.html")
        End If
    End Sub


    Private Sub Encrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(outputfilePath, FileMode.Create)
                Using cs As New CryptoStream(fs, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    Using fsInput As New FileStream(inputFilePath, FileMode.Open)
                        Dim data As Integer
                        While (Assign(data, fsInput.ReadByte())) <> -1
                            cs.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub Decrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(inputFilePath, FileMode.Open)
                Using cs As New CryptoStream(fs, encryptor.CreateDecryptor(), CryptoStreamMode.Read)
                    Using fsOutput As New FileStream(outputfilePath, FileMode.Create)
                        Dim data As Integer
                        While (Assign(data, cs.ReadByte())) <> -1
                            fsOutput.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub


    Private Shared Function Assign(Of T)(ByRef source As T, ByVal value As T) As T
        source = value
        Return value
    End Function
End Class
