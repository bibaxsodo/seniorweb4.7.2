﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO

Partial Class StampaRegistro_Protocollo
    Inherits System.Web.UI.Page



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Anagrafica')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Anagrafiche.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        If Request.Item("AUTOMATICO") = "SI" Then
            If Request.Item("USER") = "ASPMAGIERA" Then

                Dim k As New Cls_Login
                k.Utente = "protocollomagiera"
                k.Chiave = "12345678"
                k.Leggi(Application("SENIOR"))


                Session("UTENTE") = "protocollomagiera"

                Session("Password") = "12345678"
                Session("ChiaveCr") = k.ChiaveCr

                Session("DC_OSPITE") = k.Ospiti
                Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
                Session("DC_TABELLE") = k.TABELLE
                Session("DC_GENERALE") = k.Generale
                Session("DC_TURNI") = k.Turni
                Session("STAMPEOSPITI") = k.STAMPEOSPITI
                Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
                Session("ProgressBar") = ""
                Session("CODICEREGISTRO") = ""
                Session("NomeEPersonam") = k.NomeEPersonam
            End If
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Txt_Anno.Text = Year(Now)

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ParametriProtocollo ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Txt_Pagina.Text = campodb(myPOSTreader.Item("Pagina"))
            Txt_Riga.Text = campodb(myPOSTreader.Item("NumeroRiga"))
        End If
        myPOSTreader.Close()
        cn.Close()
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")

        If Request.Item("AUTOMATICO") = "SI" Then
            Dim kUI As New System.Web.UI.ImageClickEventArgs(0, 0)
            Call Btn_Stampa_Click(sender, kUI)
        End If


    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Dim TabProtcollo As New Xsd_StampaProtocollo

        Dim cn As OleDbConnection
        Dim RigaPartenza As Integer = Val(Txt_Riga.Text)

        Dim UltimaData As Date = DateSerial(Mid(Txt_DataAl.Text, 7, 4), Mid(Txt_DataAl.Text, 4, 2), Mid(Txt_DataAl.Text, 1, 2))
        Dim UltimaDataDal As Date = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))

        Dim kLogin As New Cls_Login
        Dim NomeSocieta As String

        kLogin.Utente = Session("UTENTE")
        kLogin.LeggiSP(Application("SENIOR"))

        NomeSocieta = "Protocollo_" & kLogin.RagioneSociale


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()

        If Chk_RegistroProva.Checked = False Then


            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = ("select * from ParametriProtocollo ")
            cmdR.Connection = cn


            Dim MyRead1 As OleDbDataReader = cmdR.ExecuteReader()
            If MyRead1.Read Then
                If Format(campodbd(MyRead1.Item("UltimaStampa")), "yyyyMMdd") > Format(UltimaData, "yyyyMMdd") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi stampare,periodo già stampato</center>');", True)
                    Exit Sub
                End If

                If Format(campodbd(MyRead1.Item("UltimaStampa")), "yyyyMMdd") > Format(UltimaDataDal, "yyyyMMdd") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi stampare,periodo già stampato</center>');", True)
                    Exit Sub
                End If
            End If

            MyRead1.Close()
        End If




        Dim cmd As New OleDbCommand()



        Dim MySql As String
        MySql = "select * from Protocollo where "
        If Txt_Anno.Text <> "" Then
            MySql = MySql & " Anno = ?"
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        End If


        If IsDate(Txt_DataDal.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo >= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataDal.Text)
        End If
        If IsDate(Txt_DataAl.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo <= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataAl.Text)
        End If


        If MySql.Trim = "select * from Protocollo where" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
            Exit Sub
        End If

        cmd.CommandText = MySql & "  Order By DataArrivo,Tipo"



        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim RagioneSociale As String = ""
            Dim ForSearchRagioneSociale As String = ""
            Dim Indirizzo As String = ""
            Dim Cap As String = ""
            Dim Comune As String = ""
            Dim Provincia As String = ""

            Dim Mezzo As String = ""




            KlS.Anno = campodb(myPOSTreader.Item("Anno"))
            KlS.Numero = campodb(myPOSTreader.Item("Numero"))
            KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
            KlS.Leggi(Session("DC_TABELLE"))



            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Comune = campodb(myPOSTreader.Item("Citta"))
            Provincia = ""
            Cap = campodb(myPOSTreader.Item("Cap"))

            For I = 0 To 1000
                If KlS.IdRubrica(I) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(I)
                    Kppp.Leggi(Session("DC_TABELLE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                        Indirizzo = Kppp.Indirizzo
                        Cap = Kppp.Cap

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = Kppp.Provincia
                        DecTipo.Comune = Kppp.Comune
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                End If
                If KlS.IdClienteFornitore(I) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                    KpppS.Leggi(Session("DC_OSPITE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If

                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If
                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.CodiceParente = KlS.IdParente(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If


            Next


            Dim k As System.Data.DataRow = TabProtcollo.Tables("Protocollo").NewRow

            k.Item("Anno") = campodb(myPOSTreader.Item("Anno"))
            k.Item("Numero") = campodb(myPOSTreader.Item("Numero"))
            k.Item("Tipo") = campodb(myPOSTreader.Item("Tipo"))
            k.Item("Data") = campodb(myPOSTreader.Item("DataArrivo"))

            k.Item("Ora") = Format(campodbd(myPOSTreader.Item("OraArrivo")), "HH:mm")

            Dim lk As New Cls_Titolo

            lk.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
            lk.Leggi(Session("DC_TABELLE"))


            k.Item("Titolo") = lk.Descrizione
            k.Item("CodiceTitolo") = lk.Codice

            Dim lkClasse As New Cls_Classe

            lkClasse.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
            lkClasse.idClasse = Val(campodb(myPOSTreader.Item("Classe")))
            lkClasse.Leggi(Session("DC_TABELLE"))
            k.Item("Classe") = lkClasse.Descrizione
            k.Item("IdClasse") = lkClasse.idClasse

            Dim lkIndice As New Cls_Indice


            lkIndice.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
            lkIndice.idClasse = Val(campodb(myPOSTreader.Item("Classe")))
            lkIndice.idIndice = Val(campodb(myPOSTreader.Item("Indice")))
            lkIndice.Leggi(Session("DC_TABELLE"))
            k.Item("Indice") = lkIndice.Descrizione

            k.Item("idIndice") = lkIndice.idIndice

            k.Item("Fascicolo") = campodb(myPOSTreader.Item("Fascicolo"))


            k.Item("Oggetto") = campodb(myPOSTreader.Item("Oggetto"))

            k.Item("Destinatario") = RagioneSociale
            k.Item("Indirizzo") = Indirizzo
            k.Item("Comune") = Comune
            k.Item("Provincia") = Provincia
            k.Item("Cap") = Cap
            k.Item("PaginaPartenza") = Val(Txt_Pagina.Text)
            RigaPartenza = RigaPartenza + 1
            k.Item("Riga") = RigaPartenza


            Dim XMezzo As New Cls_Mezzo

            XMezzo.idMezzo = Val(campodb(myPOSTreader.Item("Mezzo")))
            XMezzo.Leggi(Session("DC_TABELLE"))

            k.Item("Mezzo") = XMezzo.Descrizione

            Dim XUfficio As New Cls_UfficioDestinatario

            XUfficio.idUfficioDestinatario = Val(campodb(myPOSTreader.Item("UfficioDestinatario")))
            XUfficio.Leggi(Session("DC_TABELLE"))


            Dim kSoc As New Cls_TabellaSocieta



            k.Item("IntestaSocieta") = kSoc.DecodificaSocieta(Session("DC_TABELLE"))
            k.Item("Ufficio") = XUfficio.Descrizione

            REM  k.Item("Allegati") = 0
            Try



                'Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\PRT_" & Txt_Anno.Text & "_" & campodb(myPOSTreader.Item("Numero")) & campodb(myPOSTreader.Item("Tipo")) & "\")
                'Dim aryItemsInfo() As FileSystemInfo
                'Dim objItem As FileSystemInfo

                'aryItemsInfo = objDI.GetFileSystemInfos()
                'For Each objItem In aryItemsInfo
                '    k.Item("Allegati") = Val(k.Item("Allegati")) + 1
                'Next
            Catch ex As Exception

            End Try


            TabProtcollo.Tables("Protocollo").Rows.Add(k)

        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("stampa") = TabProtcollo




        Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\Stampa_RegistroProtocollo.rpt"

        Dim rpt As New ReportDocument

        rpt.Load(NomeSt)

        rpt.SetDataSource(Session("stampa"))



        rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")


        If Chk_RegistroProva.Checked = False Then

            cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

            cn.Open()
            Dim cmdW As New OleDbCommand()
            cmdW.CommandText = ("update ParametriProtocollo set Pagina = ?,NumeroRiga  = ?,UltimaStampa =?")
            cmdW.Parameters.AddWithValue("@Pagina", Val(Txt_Pagina.Text) + rpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext()))
            cmdW.Parameters.AddWithValue("@NumeroRiga", RigaPartenza)
            cmdW.Parameters.AddWithValue("@UltimaStampa", UltimaData)
            cmdW.Connection = cn
            cmdW.ExecuteNonQuery()
            cn.Close()
        End If

        If Request.Item("AUTOMATICO") <> "SI" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=Stampa_RegistroProtocollo.rpt','Stampe','width=800,height=600');", True)

        End If
        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=Stampa_RegistroProtocollo.rpt','Stampe','width=800,height=600');", True)
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub

    Protected Sub Txt_DataDal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DataDal.TextChanged
        Txt_DataAl.Text = Txt_DataDal.Text

        Call EseguiJS()
    End Sub
End Class
