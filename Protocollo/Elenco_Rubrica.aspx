﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Elenco_Rubrica" CodeFile="Elenco_Rubrica.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elenco Rubrica</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Protocollo - Elenco Rubrica</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">

                                    <table>
                                        <tr>
                                            <td style="color: #565151;">ID:</td>
                                            <td style="color: #565151;">
                                                <asp:TextBox ID="Txt_ID" MaxLength="10"
                                                    onkeypress="return handleEnterSoloNumero(this, event)" Width="60px" runat="server"></asp:TextBox></td>
                                            <td style="color: #565151;">Denominazione :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_Denominazione" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="250px" runat="server"></asp:TextBox></td>
                                            <td style="color: #565151;">Cognome :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_Cognome" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="250px" runat="server"></asp:TextBox></td>
                                            <td style="color: #565151;">Nome :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_Nome" onkeypress="return handleEnter(this, event)" MaxLength="30" runat="server" Width="250px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #565151;"></td>
                                            <td style="color: #565151;"></td>
                                            <td style="color: #565151;">Ragruppamento :</td>
                                            <td>
                                                <asp:DropDownList ID="DD_Raggruppamento" runat="server"></asp:DropDownList></td>
                                            <td style="color: #565151;"></td>
                                            <td></td>
                                            <td style="color: #565151;"></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="Btn_Ricerca" runat="server" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" BackColor="Transparent" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;"></td>
                    <td colspan="2" style="text-align: right;">
                        <a href="Rubrica.aspx?ID=0">
                            <img src="../images/nuovo.png" alt="Aggiungi Rubrica" title="Aggiungi Rubrica" /></a>
                    </td>
                </tr>


                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Rubrica.aspx">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a>
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <asp:ImageButton ID="Lnk_ToExcel" runat="server" ToolTip="Esporta griglia in excel" ImageUrl="images/BTNExel.jpg" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <asp:GridView ID="Grd_Visualizzazione" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical" PageSize="20" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                            ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                            CommandArgument="<%#   Container.DataItemIndex  %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                ForeColor="White" />
                            <AlternatingRowStyle BackColor="Gainsboro" />
                        </asp:GridView>


                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
