﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class Elenco_Rubrica
    Inherits System.Web.UI.Page
    Dim Tab_RicercaSoci As New System.Data.DataTable("Tab_RicercaSoci")

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("ID", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Denominazione", GetType(String))
        Tab_RicercaSoci.Columns.Add("Cognome", GetType(String))
        Tab_RicercaSoci.Columns.Add("Nome", GetType(String))


        Dim k As New Cls_SqlString

        k.Add("Txt_Denominazione", Txt_Denominazione.Text)
        k.Add("Txt_Cognome", Txt_Cognome.Text)
        k.Add("Txt_Nome", Txt_Nome.Text)
        k.Add("Txt_ID", Txt_ID.Text)
        k.Add("DD_Raggruppamento", DD_Raggruppamento.SelectedValue)

        Session("ElencoRubricaSQLString") = k

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Val(Txt_ID.Text) > 0 Then
            cmd.CommandText = ("select * from Rubrica  where ID = ?  ")
            cmd.Parameters.AddWithValue("@ID", Txt_ID.Text)
        Else
            Dim MySql As String
            MySql = "select * from Rubrica where "
            If Txt_Denominazione.Text.Trim <> "" Then
                MySql = MySql & " Denominazione Like ?"
                cmd.Parameters.AddWithValue("@Denominazione", Txt_Denominazione.Text.Trim & "%")
            End If

            If Txt_Cognome.Text <> "" Then
                MySql = MySql & " Cognome Like ?"
                cmd.Parameters.AddWithValue("@Cognome", Txt_Cognome.Text.Trim & "%")
            End If
            If Txt_Nome.Text <> "" Then
                If MySql <> "select * from Rubrica where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Nome Like ?"
                cmd.Parameters.AddWithValue("@Nome", Txt_Nome.Text.Trim & "%")
            End If
            If DD_Raggruppamento.SelectedValue <> "" Then
                If MySql <> "select * from Rubrica where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Ragruppamento = ?"
                cmd.Parameters.AddWithValue("@Ragruppamento", "<" & DD_Raggruppamento.SelectedValue & ">")
            End If

            If MySql.Trim = "select * from Rubrica where" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
                Exit Sub
            End If

            cmd.CommandText = MySql & "  Order By Denominazione,Cognome,Nome "
        End If


        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
            myrigaT(0) = campodb(myPOSTreader.Item("ID"))
            myrigaT(1) = campodb(myPOSTreader.Item("Denominazione"))
            myrigaT(2) = campodb(myPOSTreader.Item("Cognome"))
            myrigaT(3) = campodb(myPOSTreader.Item("Nome"))



            Tab_RicercaSoci.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("ElencoRubrica") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)
        Dim kr As New Cls_RaggruppamentoRubrica


        kr.UpDropDown(Session("DC_TABELLE"), DD_Raggruppamento)


        If Request.Item("TIPO") <> "RITORNO" Then
            Call CaricaUltimi10()
            Session("ElencoRubricaSQLString") = Nothing
        Else
            If Not IsNothing(Session("ElencoRubricaSQLString")) Then
                Dim k As New Cls_SqlString
                Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("ElencoRubricaSQLString")

                Txt_Denominazione.Text = k.GetValue("Txt_Denominazione")
                Txt_Cognome.Text = k.GetValue("Txt_Cognome")
                Txt_Nome.Text = k.GetValue("Txt_Nome")
                Txt_ID.Text = k.GetValue("Txt_ID")
                DD_Raggruppamento.SelectedValue = k.GetValue("DD_Raggruppamento")

                Call Btn_Ricerca_Click(sender, Appoggio)
            Else
                Call CaricaUltimi10()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grd_Visualizzazione.PageCount Then
                    Grd_Visualizzazione.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tab_RicercaSoci = Session("ElencoRubrica")

            Grd_Visualizzazione.AutoGenerateColumns = True
            Grd_Visualizzazione.DataSource = Tab_RicercaSoci
            Grd_Visualizzazione.DataBind()
        End If

    End Sub

    Protected Sub Grd_Visualizzazione_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Visualizzazione.PageIndexChanging
        Tab_RicercaSoci = Session("ElencoRubrica")

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.PageIndex = e.NewPageIndex
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()
    End Sub

    Protected Sub Grd_Visualizzazione_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Visualizzazione.RowCommand
        If (e.CommandName = "Select" Or e.CommandName = "Seleziona") Then

            Tab_RicercaSoci = Session("ElencoRubrica")

            If Val(Tab_RicercaSoci.Rows(e.CommandArgument).Item(0)) > 0 Then
                Response.Redirect("Rubrica.aspx?ID=" & Tab_RicercaSoci.Rows(e.CommandArgument).Item(0) & "&PAGINA=" & Grd_Visualizzazione.PageIndex)
            End If
        End If
    End Sub

    Protected Sub Grd_Visualizzazione_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Visualizzazione.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Rubrica.aspx")
    End Sub

    Private Sub CaricaUltimi10()
        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("ID", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Denominazione", GetType(String))
        Tab_RicercaSoci.Columns.Add("Cognome", GetType(String))
        Tab_RicercaSoci.Columns.Add("Nome", GetType(String))


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        If Val(Txt_ID.Text) > 0 Then
            cmd.CommandText = ("select * from Rubrica where " & _
                               "Id = ?  ")
            cmd.Parameters.AddWithValue("@ID", Txt_ID.Text)
        Else
            Dim MySql As String
            MySql = "select Top 20 * from Rubrica  "

            cmd.CommandText = MySql & "    Order By ID Desc"
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()

            myrigaT(0) = campodb(myPOSTreader.Item("ID"))
            myrigaT(1) = campodb(myPOSTreader.Item("Denominazione"))
            myrigaT(2) = campodb(myPOSTreader.Item("Cognome"))
            myrigaT(3) = campodb(myPOSTreader.Item("Nome"))

            Tab_RicercaSoci.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("ElencoRubrica") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_ToExcel.Click

        Session("GrigliaSoloStampa") = Session("ElencoRubrica")
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)
    End Sub
End Class
