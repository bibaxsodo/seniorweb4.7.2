﻿
Partial Class Tabella_Mezzo
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("Tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub




        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim X As New Cls_Mezzo


        X.loaddati(Session("DC_TABELLE"), Tabella)
        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()
    End Sub

    Private Sub BindMovimentiTesseramento()

        Tabella = ViewState("Tabella")
        GrdTabella.AutoGenerateColumns = False
        GrdTabella.DataSource = Tabella
        GrdTabella.DataBind()
    End Sub



    Protected Sub GrdTabella_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GrdTabella.RowCancelingEdit
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdTabella.RowCommand
        If (e.CommandName = "Inserisci") Then

            Tabella = ViewState("Tabella")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)

            ViewState("Tabella") = Tabella
            Call BindMovimentiTesseramento()
        End If
    End Sub

    Protected Sub GrdTabella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdTabella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Tabella = ViewState("Tabella")



            Dim LblIDTipoIscrizione As Label = DirectCast(e.Row.FindControl("LblIDTipoIscrizione"), Label)
            If Not IsNothing(LblIDTipoIscrizione) Then

            Else
                Dim TxtIDTipoIscrizione As TextBox = DirectCast(e.Row.FindControl("TxtIDTipoIscrizione"), TextBox)
                TxtIDTipoIscrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString()

            End If


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString()

            End If

        End If
    End Sub

    Protected Sub GrdTabella_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GrdTabella.RowDeleted

    End Sub

    Protected Sub GrdTabella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdTabella.RowDeleting
        Tabella = ViewState("Tabella")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        ViewState("Tabella") = Tabella

        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdTabella.RowEditing
        GrdTabella.EditIndex = e.NewEditIndex
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub GrdTabella_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GrdTabella.RowUpdated
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub


    Protected Sub GrdTabella_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GrdTabella.RowUpdating
        Dim TxtIDTipoIscrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtIDTipoIscrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtData"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtImporto As TextBox = DirectCast(GrdTabella.Rows(e.RowIndex).FindControl("TxtImporto"), TextBox)




        Tabella = ViewState("Tabella")
        Dim row = GrdTabella.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = Val(TxtIDTipoIscrizione.Text)
        Tabella.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text


        ViewState("Tabella") = Tabella
        GrdTabella.EditIndex = -1
        Call BindMovimentiTesseramento()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Session("ABILITAZIONI").ToString.IndexOf("<ABILMODIFICA>") = -1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non posso Modificare');", True)
            Exit Sub
        End If

        Dim X As New Cls_Mezzo

        Tabella = ViewState("Tabella")

        X.ScriviTabella(Session("DC_TABELLE"), Tabella)


        X.loaddati(Session("DC_TABELLE"), Tabella)
        ViewState("Tabella") = Tabella
        Call BindMovimentiTesseramento()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub


End Class
