﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Menu_Protocollo" CodeFile="Menu_Protocollo.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Protocollo</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;">
                    <td>
                        <div class="Titolo">Protocollo - Principale</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 900px;">

                            <tr>
                                <td style="text-align: center; width: 150px;"><a href="Elenco_Protocollo.aspx" id="ImgSoci">
                                    <img src="images/protocollo.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Stmp_Protocollo.aspx">
                                    <img src="images/Stampa.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;"><a href="StampaRegistro_Protocollo.aspx">
                                    <img src="images/Stampa.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Protocollo</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Stampa</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Registro</span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;"><a href="Menu_Rubrica.aspx" id="A5">
                                    <img src="images/rubrica.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="ImportaMetodo.aspx" id="A4">
                                    <img src="../images/Menu_Strumenti.png" style="border-width: 0;"></a></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Rubrica</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Importa Da Metodo</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;"><a href="Tabella_Titolo.aspx" id="A7">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="Tabella_Classe.aspx" id="A8">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="Tabella_Indice.aspx" id="A1">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="Tabella_Mezzo.aspx" id="A9">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="Tabella_UfficioDestinatario.aspx" id="A10">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="Tabella_Fascicolo.aspx" id="A11">
                                    <img src="images/GestioneTabelle.jpg" style="border-width: 0;"></a></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Titolo</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Classe</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Indice</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Mezzo</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Uff.Dest.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Tabella Fascicolo</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>
