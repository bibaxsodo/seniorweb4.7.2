﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="creautenti" CodeFile="creautenti.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Gestione Utenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Gestione Utenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci" />
                            <asp:ImageButton ID="Btn_Pulisci" runat="server" BackColor="Transparent" Height="38px" ImageUrl="~/images/PULISCI.JPG" Width="38px" ToolTip="Pulisci" />
                            <asp:ImageButton ID="Btn_Esci" runat="server" ImageUrl="~/GeneraleWeb/images/esci.jpg" />
                        </div>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <a href="Login.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    </td>
                    <td colspan="2" style="vertical-align: top;">

                        <div align="left">

                            <p>
                                <asp:GridView ID="GridView1" runat="server" BackColor="White"
                                    BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3"
                                    CellSpacing="1" GridLines="None" Width="686px">
                                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" SelectImageUrl="~/images/modifica.png"
                                            ShowSelectButton="True" ControlStyle-CssClass="EffettoBottoniTondi" />
                                        <asp:BoundField DataField="Utente" HeaderText="Utente" />
                                        <asp:CommandField CancelImageUrl="~/images/elimina.jpg" CancelText=""
                                            DeleteImageUrl="~/images/cancella.png"
                                            EditImageUrl="~/images/elimina.jpg" ControlStyle-CssClass="EffettoBottoniTondi" ShowDeleteButton="True"
                                            ButtonType="Image" />
                                        <asp:TemplateField ItemStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%#   Container.DataItemIndex  %>" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                </asp:GridView>
                            </p>
                            <p>
                                <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="472px"></asp:Label>&nbsp;
                            </p>
                            <table style="width: 50%;">
                                <tr>
                                    <td>
                                        <p>
                                            Utente :
            <asp:TextBox ID="Txt_Utente" runat="server" Width="151px"></asp:TextBox>
                                        </p>

                                        <p>
                                            Password :
                                            <asp:TextBox ID="Txt_Password" TextMode="Password" runat="server"></asp:TextBox><br />
                                            Conferma Password :
                                            <asp:TextBox ID="Txt_ConfermaPassword" TextMode="Password" runat="server"></asp:TextBox>
                                            (Password utente tecnico $3N!0R_secret! )  
                                        </p>
                                        <p>
                                            E-Mail : 
                                            <asp:TextBox ID="Txt_Email" runat="server"></asp:TextBox>
                                        </p>

                                        <p>
                                            Chiave E-Personam:
            <asp:TextBox ID="Txt_ChiaveCR" runat="server" Width="251px"></asp:TextBox>
                                            <asp:Button ID="Btn_Verifica" runat="server" Text="Verifica" />
                                            <asp:Button ID="Btn_Token" runat="server" Text="Token" />
                                        </p>
                                        <p>
                                            Abilitazioni :
                                        </p>
                                        <p>
                                            <br />
                                            <asp:CheckBox ID="Chk_EPersonamUser" runat="server" Text="E-Personam User" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_EPersonam" runat="server" Text="Collegamento E-Personam" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_EPersonamAut" runat="server" Text="Collegamento E-Personam Aut" /><br />


                                            <br />
                                            <asp:CheckBox ID="Chk_InserimentoOspite" runat="server" Text="Inserimento Ospite Anche con Epersonam" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_GestioneUtenti" runat="server" Text="Gestione Utenti" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_GestioneOspiti" runat="server" Text="Gestione Ospiti" /><br />
                                            <asp:CheckBox ID="Chk_Anagrafica" runat="server" Text="Anagrafica" /><br />
                                            <asp:CheckBox ID="Chk_Calcolo" runat="server" Text="Calcolo/Ricalcolo" /><br />
                                            <asp:CheckBox ID="Chk_Emissione" runat="server" Text="Emissione" /><br />
                                            <asp:CheckBox ID="Chk_AddebitiAccrediti" runat="server" Text="Addebiti/Accrediti" /><br />
                                            <asp:CheckBox ID="Chk_Movimenti" runat="server" Text="Movimenti/Diurno" /><br />
                                            <asp:CheckBox ID="Chk_Incasso" runat="server" Text="Incasso/Anticipo" /><br />
                                            <asp:CheckBox ID="Chk_Tabelle" runat="server" Text="Tabelle" /><br />
                                            <asp:CheckBox ID="Chk_Servizi" runat="server" Text="Servizi" /><br />
                                            <asp:CheckBox ID="Chk_Denaro" runat="server" Text="Denaro" /><br />
                                        </p>

                                        <p>
                                            <asp:CheckBox ID="Chk_GestioneGenerale" runat="server" Text="Gestione Generale" /><br />
                                            <asp:CheckBox ID="Chk_PianoConti" runat="server" Text="Piano dei conti" /><br />
                                            <asp:CheckBox ID="Chk_Registrazioni" runat="server" Text="Prima Nota/Doc/Pag/Inc" /><br />
                                            <asp:CheckBox ID="Chk_Budget" runat="server" Text="Budget" /><br />
                                            Tabelle<br />
                                            <asp:CheckBox ID="Chk_CausaliContabili" runat="server" Text="Causali Contabili" /><br />
                                            <asp:CheckBox ID="Chk_ModalitaPagamento" runat="server" Text="Modalita Pagamento" /><br />
                                            <asp:CheckBox ID="Chk_TipoRegistro" runat="server" Text="Tipo Registro" /><br />
                                            <asp:CheckBox ID="Chk_RegistroIVA" runat="server" Text="Registro IVA" /><br />
                                            <asp:CheckBox ID="Chk_Detraibilita" runat="server" Text="Detraibilita" /><br />
                                            <asp:CheckBox ID="Chk_PeriodoaConfronto" runat="server" Text="Periodoa Confronto" /><br />
                                            <asp:CheckBox ID="Chk_Ritenute" runat="server" Text="Ritenute" /><br />
                                            <asp:CheckBox ID="Chk_Spesometro2017" runat="server" Text="Dati Fattura" /><br />
                                        </p>
                                        <p>
                                            <asp:CheckBox ID="Chk_Richieste" runat="server" Text="Richieste Turni" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_abilmodifica" runat="server" Text="Abilita Modifica Protocollo" /><br />
                                        </p>
                                        <p>
                                            <asp:CheckBox ID="Chk_NonGenerale" runat="server" Text="Non Generale" /><br />
                                            <asp:CheckBox ID="Chk_SoloGenerale" runat="server" Text="Solo Generale" /><br />
                                            <asp:CheckBox ID="Chk_NonProtocollo" runat="server" Text="Non Protocollo" /><br />
                                            <asp:CheckBox ID="Chk_NonMailingList" runat="server" Text="Non Mailing-List" /><br />
                                            <asp:CheckBox ID="Chk_NonOspiti" runat="server" Text="Non Ospiti" /><br />
                                            <asp:CheckBox ID="Chk_Porteneria" runat="server" Text="Porteneria" /><br />
                                            <asp:CheckBox ID="Chk_Protocollo" runat="server" Text="Solo Protocollo" /><br />
                                            <asp:CheckBox ID="Chk_SoloUscite" runat="server" Text="Solo Uscite" /><br />
                                            <asp:CheckBox ID="Chk_SoloEntrate" runat="server" Text="Solo Entrate" /><br />
                                            <asp:CheckBox ID="Chk_Solo730" runat="server" Text="Solo 730" /><br />
                                            <asp:CheckBox ID="Chk_LogPrivacy" runat="server" Text="Log Privacy" /><br />
                                            <asp:CheckBox ID="Chk_Appalti" runat="server" Text="Appalti" /><br />
                                            <br />
                                            <asp:CheckBox ID="Chk_App" runat="server" Text="App" /><br />
                                            <input id="login_exchange" type="text" name="login" size="1" width="1" height="" autocomplete="off" style="z-index: -1000; height: 1px; width: 1px; border: none; position: absolute;">
                                            <input id="pswd_exchange" type="password" name="pswd" size="1" autocomplete="off" style="z-index: -1000; height: 1px; width: 1px; border: none; position: absolute;">
                                        </p>
                                    </td>
                                    <td style="vertical-align: top; width: 50%;">Personalizzazioni:<br />
                                        <asp:TextBox ID="Txt_Report" Text="" Rows="8" runat="server" TextMode="MultiLine" Width="400px" Height="400px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <p>
                                &nbsp;
                            </p>


                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
