﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class AbilitaProcedure
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_SODO") <> True Then

            Response.Redirect("login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), "", Page)



        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("RagioneSociale", GetType(String))
        Tabella.Columns.Add("ProcedureAbilitate", GetType(String))
        Tabella.Columns.Add("Tabelle", GetType(String))

        Dim cn As OleDbConnection




        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ArchivioClienti order by RAGIONESOCIALE")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("RagioneSociale"))

            Dim StringoConnessione As String = campodb(myPOSTreader.Item("TABELLE"))

            Try
                Dim CnTAbelle As Data.OleDb.OleDbConnection

                CnTAbelle = New Data.OleDb.OleDbConnection(StringoConnessione)

                CnTAbelle.Open()

                Dim cmdSocieta As New OleDbCommand()
                cmdSocieta.CommandText = ("select * from Societa")
                cmdSocieta.Connection = CnTAbelle


                Dim ReadSocieta As OleDbDataReader = cmdSocieta.ExecuteReader()
                If ReadSocieta.Read Then
                    Try
                        myriga(1) = campodb(ReadSocieta.Item("ProcedureAbilitate"))
                    Catch ex As Exception

                    End Try


                End If
                ReadSocieta.Close()

                CnTAbelle.Close()
            Catch ex As Exception

            End Try


            myriga(2) = StringoConnessione
            Tabella.Rows.Add(myriga)





        Loop
        myPOSTreader.Close()
        cn.Close()


        ViewState("AbilitaProcedure") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True


    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Abilitazioni As String = ""


        If Chk_Ospiti.Checked = True Then
            Abilitazioni = Abilitazioni & "<OSPITI>"
        End If

        If Chk_Generale.Checked = True Then
            Abilitazioni = Abilitazioni & "<GENERALE>"
        End If

        If Chk_Protocollo.Checked = True Then
            Abilitazioni = Abilitazioni & "<PROTOCOLLO>"
        End If

        If Chk_MailingList.Checked = True Then
            Abilitazioni = Abilitazioni & "<MAILING>"
        End If

        If Chk_Appalti.Checked = True Then
            Abilitazioni = Abilitazioni & "<APPALTI>"
        End If
        If Chk_Comunicazioni.Checked = True Then
            Abilitazioni = Abilitazioni & "<COMUNICAZIONI>"
        End If

                      
        If Chk_AllegatiXML.Checked = True Then
            Abilitazioni = Abilitazioni & "<ALLEGATIDOCUMENTI>"
        End If
              
        If Chk_Ambulatoriale.Checked = True Then
            Abilitazioni = Abilitazioni & "<AMBULATORIALE>"
        End If

        IF Chk_Ricoveri.Checked =True Then
            Abilitazioni = Abilitazioni & "<RICOVERI>"
        End If

        Tabella = ViewState("AbilitaProcedure")


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim StringoConnessione As String = Tabella.Rows(Riga).Item("Tabelle")

                Try
                    Dim CnTAbelle As Data.OleDb.OleDbConnection

                    CnTAbelle = New Data.OleDb.OleDbConnection(StringoConnessione)

                    CnTAbelle.Open()



                    Dim cmdSocietaVerifica As New OleDbCommand()
                    cmdSocietaVerifica.CommandText = ("select * from Societa")
                    cmdSocietaVerifica.Connection = CnTAbelle


                    Dim ReadSocieta As OleDbDataReader = cmdSocietaVerifica.ExecuteReader()
                    If Not ReadSocieta.Read Then
                        Dim cmdSocietaInsert As New OleDbCommand()
                        cmdSocietaInsert.CommandText = "INSERT INTO Societa (UTENTE) VALUES (?)"
                        cmdSocietaInsert.Connection = CnTAbelle
                        cmdSocietaInsert.Parameters.AddWithValue("@UTENTE", "Abilitazioni")
                        cmdSocietaInsert.ExecuteNonQuery()
                    End If
                    ReadSocieta.Close()



                    Dim cmdSocieta As New OleDbCommand()
                    cmdSocieta.CommandText = "Update Societa set ProcedureAbilitate = ? "
                    cmdSocieta.Connection = CnTAbelle
                    cmdSocieta.Parameters.AddWithValue("@ProcedureAbilitate", Abilitazioni)
                    cmdSocieta.ExecuteNonQuery()

                    CnTAbelle.Close()
                Catch ex As Exception

                End Try
            End If
        Next
        Response.Redirect("SelezionaSocieta.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            CheckBox.Checked = True
        Next
    End Sub

    Protected Sub Btn_Home_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Home.Click
        Response.Redirect("SelezionaSocieta.aspx")
    End Sub
End Class
