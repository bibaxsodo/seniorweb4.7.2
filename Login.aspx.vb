﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util


Partial Class Login
    Inherits System.Web.UI.Page

    Private Sub AutentificaLogin()
        Dim tkt As FormsAuthenticationTicket
        Dim cookiestr As String
        Dim ck As HttpCookie

        tkt = New FormsAuthenticationTicket(1, Txt_Login.Text, DateTime.Now, DateTime.Now.AddMinutes(30), True, "")
        cookiestr = FormsAuthentication.Encrypt(tkt)
        ck = New HttpCookie(FormsAuthentication.FormsCookieName, cookiestr)
        ck.Expires = tkt.Expiration
        ck.Path = FormsAuthentication.FormsCookiePath
        Response.Cookies.Add(ck)

    End Sub


    Protected Sub BtnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLogin.Click

        If Txt_Password.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
            Exit Sub
        End If



        Dim k As New Cls_Login

        Session("SOCIETALI") = ""
        Session("UTENTEIMPER") = ""
        Session("USER_SODO") = False

        

        If Txt_Login.Text = "SODO" And Txt_Password.Text = "MASTER76" Then
            Session("USER_SODO") = True
            AutentificaLogin()
            Response.Redirect("SelezionaSocieta.aspx")
            Exit Sub
        End If


        Session("TIPOAPP") = "RSA"

        '        Dim ss As New x

        k.Utente = UCase(Txt_Login.Text) & "<1>"
        k.Chiave = Txt_Password.Text
        k.Leggi(Application("SENIOR"))
        Session("Password") = Txt_Password.Text
        If k.Ospiti <> "" Then
            Session("UTENTE") = Txt_Login.Text.Trim
            AutentificaLogin()
            If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then                
                Response.Redirect("ModificaPassword.aspx")
                Exit Sub
            End If



            Session("SOCIETALI") = k.CaricaSocieta(Application("SENIOR"), Txt_Login.Text.Trim)

            AutentificaLogin()
            Response.Redirect("Menu_Societa.aspx")

            Exit Sub
        End If

        k.Utente = UCase(Txt_Login.Text.Trim)
        k.Chiave = Txt_Password.Text

        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.Leggi(Application("SENIOR"))


        If k.ABILITAZIONI = "<ADMIN>" Then
            Session("SOCIETALI") = k.CaricaSocietaaDMIN(Application("SENIOR"))
            Session("UTENTEIMPER") = Txt_Login.Text.Trim 'UTENTE IMPERSONIFICATORE
            Session("USER_SODO") = True
            Session("UTENTE") = ""

           If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then
                AutentificaLogin()
                Response.Redirect("ModificaPassword_UtenteImper.aspx")
                Exit Sub
            Else
                AutentificaLogin()
                Response.Redirect("SelezionaSocieta.aspx")
                Exit Sub
            End If            
        End If



        Session("Password") = Txt_Password.Text
        Session("ChiaveCr") = k.ChiaveCr
        Session("CLIENTE") = k.Cliente

        Session("SENIOR") = Application("SENIOR")
        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""
        Session("NomeEPersonam") = k.NomeEPersonam

        k.LeggiEpersonamUser(Application("SENIOR"))

        Session("EPersonamUser") = k.EPersonamUser.Replace("w", "")
        Session("EPersonamPSW") = k.EPersonamPSW
        Session("EPersonamPSWCRYPT") = k.EPersonamPSWCRYPT

        Session("GDPRAttivo") = k.GDPRAttivo



        If k.Ospiti <> "" Then


            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = Txt_Login.Text.ToLower
            Dim Log As New Cls_LogPrivacy

            Try
                Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "LOGIN", "", "", "")
            Catch ex As Exception

            End Try

            If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then
                AutentificaLogin()
                Response.Redirect("ModificaPassword.aspx")
                Exit Sub
            End If



            If Session("ABILITAZIONI").IndexOf("<PROTOCOLLO>") >= 0 Then
                AutentificaLogin()
                Response.Redirect("Protocollo/Menu_Protocollo.aspx")
                Exit Sub
            End If

            If Session("ABILITAZIONI").IndexOf("<STATISTICHE>") >= 0 Then
                AutentificaLogin()
                Response.Redirect("OspitiWeb\Menu_Visualizzazioni.aspx")
                Exit Sub
            End If

            If Session("ABILITAZIONI").IndexOf("<SOLO730>") >= 0 Then
                AutentificaLogin()
                Response.Redirect("Menu730.aspx")
                Exit Sub
            End If

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                AutentificaLogin()
                Response.Redirect("MainPortineria.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                AutentificaLogin()
                Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<RICHIESTE>") > 0 Then
                AutentificaLogin()
                Response.Redirect("TurniWeb\Menu_Richieste.aspx")
                Exit Sub
            End If
            AutentificaLogin()
            Response.Redirect("MainMenu.aspx")
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub

        If Request.Item("Logout") = "TRUE" Then
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "LOGOUT", "", "", "")

            FormsAuthentication.SignOut()

            Session.Clear()
            Session.Abandon()
            Session("UTENTE") = ""
            Session("ODV") = ""
            Session("UTENTEIMPER") = ""

        Else


        End If


        Dim Crypt As New Cls_Crypt


        Dim codicefiscale As String = Crypt.aesEncrypt("GABELLINI SAURO", "0bScUr3@Dv3n1@Sf0rS3CuR1Ty")
        codicefiscale = "<codicefiscale>" & codicefiscale & "</codiceficale>"
    End Sub

    Function DeCrypt(ByVal strEncrypted As String) As String
        Dim g_Key As String = "GIUV2SPBNI99212CON"

        Dim strDecrypted As String
        Dim iDeCryptChar As String
        Dim iKeyChar, iStringChar, i As Integer


        For i = 1 To Len(strEncrypted)
            iKeyChar = (Asc(Mid(g_Key, i, 1)))
            iStringChar = Asc(Mid(strEncrypted, i, 1))
            ' *** uncomment below to decrypt with subtraction	
            ' iDeCryptChar = iStringChar - iKeyChar 
            iDeCryptChar = iKeyChar Xor iStringChar
            strDecrypted = strDecrypted & Chr(iDeCryptChar)
        Next
        DeCrypt = strDecrypted
    End Function

    Function EnCrypt(ByVal strCryptThis As String) As String
        Dim g_Key As String = "GIUV2SPBNI99212CON"
        Dim iCryptChar As String
        Dim strEncrypted As String = ""
        Dim iKeyChar, iStringChar, i As Integer

        For i = 1 To Len(strCryptThis)
            iKeyChar = Asc(Mid(g_Key, i, 1))
            iStringChar = Asc(Mid(strCryptThis, i, 1))
            ' *** uncomment below to encrypt with addition,
            ' iCryptChar = iStringChar + iKeyChar
            iCryptChar = iKeyChar Xor iStringChar
            strEncrypted = strEncrypted & Chr(iCryptChar)
        Next
        EnCrypt = strEncrypted
    End Function




    Private Sub RichiediIDEpersonam(ByVal CodiceFiscale As String)

        Dim request As New NameValueCollection

        'request.Add("Authorization : Bearer", Session("access_token"))
        'System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim client As New WebClient()
        client.Headers.Add("Content-Type", "text")
        client.Headers.Add("Authorization", "Bearer " & Session("access_token"))

        Dim result As String = client.DownloadString("https://api-v0.e-personam.com/v0/guests/" & CodiceFiscale & "/relatives")
        'Dim stringa As String = Encoding.Default.GetString(result)

        'ModificaPerente()
    End Sub




    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
