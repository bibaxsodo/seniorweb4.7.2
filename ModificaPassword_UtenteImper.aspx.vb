﻿
Partial Class ModificaPassword_UtenteImper
    Inherits System.Web.UI.Page
    Protected Sub BtnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLogin.Click
        If Txt_Password.Text <> Txt_PasswordConferma.Text Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Le password non coincidono</b></center>');", True)
            Exit Sub
        End If
        If Len(Txt_Password.Text) < 6 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password troppo corta</b></center>');", True)
            Exit Sub
        End If
        Dim Kx As New Cls_Login
        Kx.Utente = ViewState("UTENTEIMPER")
        Kx.LeggiSP(Application("SENIOR"))
        If Kx.PasswordCriptata <> "" Then
            If BCrypt.Net.BCrypt.Verify(Txt_Password.Text, Kx.PasswordCriptata) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password già utilizzata</b></center>');", True)
                Exit Sub
            End If
        End If

        'If Kx.EmailUtente.Trim = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password troppo corta</b></center>');", True)
        '    Exit Sub
        'End If


        Dim ChiaveCriptata As String = BCrypt.Net.BCrypt.HashPassword(Txt_Password.Text)

        Kx.Utente = ViewState("UTENTEIMPER")
        Kx.LeggiSP(Application("SENIOR"))
        Kx.Chiave = Txt_Password.Text
        Kx.PasswordCriptata = ChiaveCriptata
        Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
        Kx.Scrivi(Application("SENIOR"))



        Response.Redirect("Login.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("UTENTEIMPER") = "" And (Request.Item("UTENTEIMPER") = "" Or IsNothing(Request.Item("UTENTEIMPER"))) Then
            Exit Sub
        End If

        If Not (Request.Item("UTENTEIMPER") = "" Or IsNothing(Request.Item("UTENTEIMPER"))) Then

            Dim M As New Cls_Login


            M.Utente = Request.Item("UTENTEIMPER")
            M.Ospiti = ""
            M.LeggiSP(Application("SENIOR"))
            If Request.Item("TOKEN") = M.PasswordCriptata Then
                ViewState("UTENTEIMPER") = Request.Item("UTENTEIMPER")
                Exit Sub
            End If

            Response.Redirect("Login.aspx")
        End If


        ViewState("UTENTEIMPER") = Session("UTENTEIMPER")

    End Sub
End Class
