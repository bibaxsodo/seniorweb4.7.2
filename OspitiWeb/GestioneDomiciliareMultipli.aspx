﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GestioneDomiciliareMultipli" CodeFile="GestioneDomiciliareMultipli.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Domiciliare</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">  
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Ospiti - Gestione Domiciliare</div>
                    <div class="SottoTitoloOSPITE">
                        <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right;">
                    <div class="DivTastiOspite">
                        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                    </div>
                </td>
            </tr>

            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    <div id="MENUDIV"></div>
                    <br />
                </td>
                <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                    <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                        Width="100%" BorderStyle="None" Style="margin-right: 39px">
                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                            <HeaderTemplate>
                                Gestione Domiciliare         
                            </HeaderTemplate>
                            <ContentTemplate>


                                <label style="display: block; float: left; width: 160px;">Data  :</label>
                                <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox>
                                <br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Ora Inizio  :</label>
                                <asp:TextBox ID="Txt_OraInizio" runat="server" Width="100px"></asp:TextBox>
                                <br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Ora Fine  :</label>
                                <asp:TextBox ID="Txt_OraFine" runat="server" Width="100px"></asp:TextBox>
                                <br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Descrizione :</label>
                                <asp:TextBox ID="Txt_Descrizione" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                                <br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Operatore :</label>
                                <asp:TextBox ID="Txt_Operatore" runat="server" onkeypress="return soloNumeri(event);" Width="100px" MaxLength="50"></asp:TextBox><br />
                                <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore1" AutoPostBack="true" runat="server" Visible="true"></asp:DropDownList><asp:Button ID="Btn_Plus1" runat="server" Text="+" /><br />
                                <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore2" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus2" runat="server" Text="+" Visible="false" /><asp:Button ID="Btn_Meno2" runat="server" Text="-" Visible="false" /><br />
                                <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore3" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus3" runat="server" Text="+" Visible="false" /><asp:Button ID="Btn_Meno3" runat="server" Text="-" Visible="false" /><br />
                                <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore4" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus4" runat="server" Text="+" Visible="false" /><asp:Button ID="Btn_Meno4" runat="server" Text="-" Visible="false" /><br />
                                <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore5" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus5" runat="server" Text="+" Visible="false" /><asp:Button ID="Btn_Meno5" runat="server" Text="-" Visible="false" /><br />
                                <br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Tipologia :</label>
                                <asp:DropDownList ID="DD_Tipologia" runat="server"></asp:DropDownList>
                                <br />
                                <br />


                                <label style="display: block; float: left; width: 160px;">Numero  :</label>
                                <asp:TextBox ID="Txt_Numero" runat="server" Width="100px" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)"></asp:TextBox>
                                <br />
                                <br />

                            </ContentTemplate>

                        </xasp:TabPanel>
                    </xasp:TabContainer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
