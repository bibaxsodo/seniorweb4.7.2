﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Imports org.jivesoftware.util

Public Class Import_DirunoEPersonam
    Implements System.Web.IHttpHandler, IRequiresSessionState


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim VettoreMovimenti(10000) As Movimenti
        Dim Numero As Integer = 0
        Dim LastMov As String = ""


        If context.Session("UTENTE") = "" Then
            Exit Sub
        End If


        Dim Token As String
        Dim OutPutMovimenti As String = ""



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()

        Try


            Token = LoginPersonam(context)

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(context.Session("DC_OSPITE"))




            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim cmdcs As New OleDbCommand()
            cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And EPersonam > 0")

            cmdcs.Connection = cn

            Dim RDcs As OleDbDataReader = cmdcs.ExecuteReader()
            Do While RDcs.Read


                '       " & RDcs.Item("Epersonam") & "
                Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/day_centers/" & RDcs.Item("Epersonam") & "/guests/presences/" & Param.AnnoFatturazione & "/" & Param.MeseFatturazione)

                client.Method = "GET"
                client.Headers.Add("Authorization", "Bearer " & Token)
                client.ContentType = "Content-Type: application/json"

                Dim reader As StreamReader
                Dim response As HttpWebResponse = Nothing

                response = DirectCast(client.GetResponse(), HttpWebResponse)

                reader = New StreamReader(response.GetResponseStream())


                Dim rawresp As String
                rawresp = reader.ReadToEnd()






                Dim jResults As JObject = JObject.Parse(rawresp)



                For Each jTok1 As JToken In jResults.Item("guests_presences").Children
                    Dim CodiceFiscale As String
                    CodiceFiscale = jTok1.Item("cf").ToString()

                    Dim Ospite As New ClsOspite

                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(context.Session("DC_OSPITE"), CodiceFiscale)
                    If Ospite.CodiceOspite > 0 Then
                        For Each jTok2 As JToken In jTok1.Item("presences").Children

                            Dim MiaData As Date

                            MiaData = jTok2.Item("date")

                            Dim CAUSALE As String = ""

                            If RDcs.Item("Epersonam") = 1157 Or RDcs.Item("Epersonam") = 1162 Or RDcs.Item("Epersonam") = 1166 Or RDcs.Item("Epersonam") = 1409 Then
                                'MAGIERA                                  


                                If jTok2.Item("trasporto").ToString = "True" Then
                                    CAUSALE = "D5"
                                End If

                                If jTok2.Item("cena").ToString = "True" Then
                                    CAUSALE = "D1"
                                End If

                                If (jTok2.Item("mattina").ToString = "False" And Not IsNothing(jTok2.Item("mattina_prev"))) Then
                                    If jTok2.Item("mattina_prev").ToString = "True" Then
                                        CAUSALE = "D2"
                                    End If
                                End If



                                If (jTok2.Item("pomeriggio").ToString = "False" And Not IsNothing(jTok2.Item("pomeriggio_prev"))) Then
                                    If jTok2.Item("pomeriggio_prev").ToString = "True" Then
                                        CAUSALE = "D2"
                                    End If
                                End If

                                If CAUSALE = "" Then
                                    If jTok2.Item("pomeriggio").ToString = "False" And jTok2.Item("mattina").ToString = "False" And jTok2.Item("notte").ToString = "False" Then
                                        CAUSALE = "C"
                                    End If
                                End If

                            End If

                            If RDcs.Item("Epersonam") = 1075 Then
                                If (jTok2.Item("mattina").ToString = "False" And Not IsNothing(jTok2.Item("mattina_prev"))) Then
                                    If jTok2.Item("mattina_prev").ToString = "True" Then
                                        CAUSALE = "A"
                                    End If
                                End If



                                If (jTok2.Item("pomeriggio").ToString = "False" And Not IsNothing(jTok2.Item("pomeriggio_prev"))) Then
                                    If jTok2.Item("pomeriggio_prev").ToString = "True" Then
                                        CAUSALE = "A"
                                    End If
                                End If

                                If CAUSALE = "" Then
                                    If jTok2.Item("cena").ToString = "False" And jTok2.Item("pranzo").ToString = "False" Then
                                        CAUSALE = "PP"
                                    End If
                                End If

                                If CAUSALE = "" Then
                                    If jTok2.Item("pomeriggio").ToString = "False" And jTok2.Item("mattina").ToString = "False" And jTok2.Item("notte").ToString = "False" Then
                                        CAUSALE = "C"
                                    End If
                                End If
                            End If

                            Dim CENTROSERVIZIO As String

                            CENTROSERVIZIO = RDcs.Item("CENTROSERVIZIO")

                            Dim UltCserv As New Cls_Movimenti



                            UltCserv.CENTROSERVIZIO = RDcs.Item("CENTROSERVIZIO")
                            UltCserv.CodiceOspite = Ospite.CodiceOspite
                            UltCserv.UltimaData(context.Session("DC_OSPITE"), UltCserv.CodiceOspite)
                            If UltCserv.CENTROSERVIZIO <> CENTROSERVIZIO Then
                                Dim cmdvc As New OleDbCommand()
                                cmdvc.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And EPersonamN= ?")
                                cmdvc.Connection = cn
                                cmdvc.Parameters.AddWithValue("@EPersonamn", RDcs.Item("Epersonam"))

                                Dim RDvs As OleDbDataReader = cmdvc.ExecuteReader()
                                If RDvs.Read Then
                                    If UltCserv.CENTROSERVIZIO = RDvs.Item("CENTROSERVIZIO") Then
                                        CENTROSERVIZIO = UltCserv.CENTROSERVIZIO
                                    End If
                                End If
                                RDvs.Close
                            End If



                            Dim cmdcData As New OleDbCommand()
                            cmdcData.CommandText = ("select * from Diurno_EPersonam Where CentroServizio = ? and CodiceOspite = ? And [Data] = ?")

                            cmdcData.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
                            cmdcData.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                            cmdcData.Parameters.AddWithValue("@Data", MiaData)
                            cmdcData.Connection = cn
                            Dim RDData As OleDbDataReader = cmdcData.ExecuteReader()
                            If RDData.Read Then

                                If CAUSALE <> "" Then
                                    Dim DEccau As New Cls_CausaliEntrataUscita

                                    DEccau.Codice = CAUSALE
                                    DEccau.LeggiCausale(context.Session("DC_OSPITE"))
                                    CAUSALE = DEccau.Descrizione
                                End If

                                VettoreMovimenti(Numero) = New Movimenti
                                VettoreMovimenti(Numero).Esito = "N"
                                VettoreMovimenti(Numero).CentroServizio = CENTROSERVIZIO
                                VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                VettoreMovimenti(Numero).Nome = Ospite.Nome
                                VettoreMovimenti(Numero).Tipo = CAUSALE
                                VettoreMovimenti(Numero).Data = MiaData
                                Numero = Numero + 1
                            Else

                                Dim Vercentroservizio As New Cls_Movimenti



                                Vercentroservizio.CENTROSERVIZIO = CENTROSERVIZIO
                                Vercentroservizio.CodiceOspite = Ospite.CodiceOspite
                                If Vercentroservizio.CServizioUsato(context.Session("DC_OSPITE")) Then

                                    Dim Diuro As New Cls_Diurno
                                    Diuro.Anno = 0
                                    Diuro.CENTROSERVIZIO = CENTROSERVIZIO
                                    Diuro.CodiceOspite = Ospite.CodiceOspite
                                    Diuro.Leggi(context.Session("DC_OSPITE"), Diuro.CodiceOspite, Diuro.CENTROSERVIZIO, Param.AnnoFatturazione, Param.MeseFatturazione)
                                    If Diuro.Anno = 0 Then
                                        Diuro.Giorno1 = "C"
                                        Diuro.Giorno2 = "C"
                                        Diuro.Giorno3 = "C"
                                        Diuro.Giorno4 = "C"
                                        Diuro.Giorno5 = "C"
                                        Diuro.Giorno6 = "C"
                                        Diuro.Giorno7 = "C"
                                        Diuro.Giorno8 = "C"
                                        Diuro.Giorno9 = "C"
                                        Diuro.Giorno10 = "C"
                                        Diuro.Giorno11 = "C"
                                        Diuro.Giorno12 = "C"
                                        Diuro.Giorno13 = "C"
                                        Diuro.Giorno14 = "C"
                                        Diuro.Giorno15 = "C"
                                        Diuro.Giorno16 = "C"
                                        Diuro.Giorno17 = "C"
                                        Diuro.Giorno18 = "C"
                                        Diuro.Giorno19 = "C"
                                        Diuro.Giorno20 = "C"
                                        Diuro.Giorno21 = "C"
                                        Diuro.Giorno22 = "C"
                                        Diuro.Giorno23 = "C"
                                        Diuro.Giorno24 = "C"
                                        Diuro.Giorno25 = "C"
                                        Diuro.Giorno26 = "C"
                                        Diuro.Giorno27 = "C"
                                        Diuro.Giorno28 = "C"
                                        Diuro.Giorno29 = "C"
                                        Diuro.Giorno30 = "C"
                                        Diuro.Giorno31 = "C"
                                    End If

                                    Diuro.Anno = Param.AnnoFatturazione
                                    Diuro.Mese = Param.MeseFatturazione
                                    Diuro.CENTROSERVIZIO = CENTROSERVIZIO
                                    Diuro.CodiceOspite = Ospite.CodiceOspite


                                    If Day(MiaData) = 1 Then Diuro.Giorno1 = CAUSALE
                                    If Day(MiaData) = 2 Then Diuro.Giorno2 = CAUSALE
                                    If Day(MiaData) = 3 Then Diuro.Giorno3 = CAUSALE
                                    If Day(MiaData) = 4 Then Diuro.Giorno4 = CAUSALE
                                    If Day(MiaData) = 5 Then Diuro.Giorno5 = CAUSALE
                                    If Day(MiaData) = 6 Then Diuro.Giorno6 = CAUSALE
                                    If Day(MiaData) = 7 Then Diuro.Giorno7 = CAUSALE
                                    If Day(MiaData) = 8 Then Diuro.Giorno8 = CAUSALE
                                    If Day(MiaData) = 9 Then Diuro.Giorno9 = CAUSALE
                                    If Day(MiaData) = 10 Then Diuro.Giorno10 = CAUSALE
                                    If Day(MiaData) = 11 Then Diuro.Giorno11 = CAUSALE
                                    If Day(MiaData) = 12 Then Diuro.Giorno12 = CAUSALE
                                    If Day(MiaData) = 13 Then Diuro.Giorno13 = CAUSALE
                                    If Day(MiaData) = 14 Then Diuro.Giorno14 = CAUSALE
                                    If Day(MiaData) = 15 Then Diuro.Giorno15 = CAUSALE
                                    If Day(MiaData) = 16 Then Diuro.Giorno16 = CAUSALE
                                    If Day(MiaData) = 17 Then Diuro.Giorno17 = CAUSALE
                                    If Day(MiaData) = 18 Then Diuro.Giorno18 = CAUSALE
                                    If Day(MiaData) = 19 Then Diuro.Giorno19 = CAUSALE
                                    If Day(MiaData) = 20 Then Diuro.Giorno20 = CAUSALE
                                    If Day(MiaData) = 21 Then Diuro.Giorno21 = CAUSALE
                                    If Day(MiaData) = 22 Then Diuro.Giorno22 = CAUSALE
                                    If Day(MiaData) = 23 Then Diuro.Giorno23 = CAUSALE
                                    If Day(MiaData) = 24 Then Diuro.Giorno24 = CAUSALE
                                    If Day(MiaData) = 25 Then Diuro.Giorno25 = CAUSALE
                                    If Day(MiaData) = 26 Then Diuro.Giorno26 = CAUSALE
                                    If Day(MiaData) = 27 Then Diuro.Giorno27 = CAUSALE
                                    If Day(MiaData) = 28 Then Diuro.Giorno28 = CAUSALE
                                    If Day(MiaData) = 29 Then Diuro.Giorno29 = CAUSALE
                                    If Day(MiaData) = 30 Then Diuro.Giorno30 = CAUSALE
                                    If Day(MiaData) = 31 Then Diuro.Giorno31 = CAUSALE

                                    Dim Mysql As String

                                    Mysql = "INSERT INTO Diurno_EPersonam (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,CAUSALE) VALUES (?,?,?,?,?,?)"
                                    Dim cmdw As New OleDbCommand()
                                    cmdw.CommandText = (Mysql)

                                    cmdw.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
                                    cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                    cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                                    cmdw.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                                    cmdw.Parameters.AddWithValue("@Data", MiaData)
                                    cmdw.Parameters.AddWithValue("@CAUSALE", CAUSALE)
                                    cmdw.Connection = cn
                                    cmdw.ExecuteNonQuery()

                                    Diuro.AggiornaDB(context.Session("DC_OSPITE"))


                                    VettoreMovimenti(Numero) = New Movimenti
                                    VettoreMovimenti(Numero).Esito = "I"
                                    VettoreMovimenti(Numero).CentroServizio = CENTROSERVIZIO
                                    VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                    VettoreMovimenti(Numero).Nome = Ospite.Nome

                                    If CAUSALE <> "" Then
                                        Dim DEccau As New Cls_CausaliEntrataUscita

                                        DEccau.Codice = CAUSALE
                                        DEccau.LeggiCausale(context.Session("DC_OSPITE"))
                                        CAUSALE = DEccau.Descrizione
                                    End If
                                    VettoreMovimenti(Numero).Tipo = CAUSALE
                                    VettoreMovimenti(Numero).Data = MiaData
                                    Numero = Numero + 1
                                Else
                                    VettoreMovimenti(Numero) = New Movimenti
                                    VettoreMovimenti(Numero).Esito = "E"
                                    VettoreMovimenti(Numero).CentroServizio = CENTROSERVIZIO
                                    VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                    VettoreMovimenti(Numero).Nome = Ospite.Nome
                                    VettoreMovimenti(Numero).Tipo = ""
                                    VettoreMovimenti(Numero).Data = MiaData
                                    Numero = Numero + 1
                                End If
                            End If
                            RDData.Close()
                        Next
                    End If
                Next
            Loop
        Catch ex As Exception

        End Try

        cn.Close()
        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(VettoreMovimenti)

        context.Response.Write(Serializzazione)
    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String


        If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = context.Session("ChiaveCr")
        Else
            request.Add("username", context.Session("EPersonamUser"))

            guarda = context.Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class




    Public Class Movimenti
        Public Esito As String
        Public CentroServizio As String
        Public CodiceOspite As String
        Public Nome As String
        Public Data As String
        Public Tipo As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class
