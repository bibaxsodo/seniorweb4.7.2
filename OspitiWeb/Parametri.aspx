﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Parametri" EnableEventValidation="false" CodeFile="Parametri.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Tabella Parametri</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .LabelCampo {
            width: 300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Tabella Parametri</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Generali       
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Periodo su Servizi : </label>
                                    <asp:CheckBox ID="Chk_periodoSuCserv" runat="server" />
                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Periodo Fatturazione : Anno : </label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server"></asp:TextBox>
                                    Mese : 
       <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
           <asp:ListItem Value="1">Gennaio</asp:ListItem>
           <asp:ListItem Value="2">Febbraio</asp:ListItem>
           <asp:ListItem Value="3">Marzo</asp:ListItem>
           <asp:ListItem Value="4">Aprile</asp:ListItem>
           <asp:ListItem Value="5">Maggio</asp:ListItem>
           <asp:ListItem Value="6">Giugno</asp:ListItem>
           <asp:ListItem Value="7">Luglio</asp:ListItem>
           <asp:ListItem Value="8">Agosto</asp:ListItem>
           <asp:ListItem Value="9">Settembre</asp:ListItem>
           <asp:ListItem Value="10">Ottobre</asp:ListItem>
           <asp:ListItem Value="11">Novembre</asp:ListItem>
           <asp:ListItem Value="12">Dicembre</asp:ListItem>
       </asp:DropDownList>
                                    <asp:CheckBox ID="Chk_GiaCalcolata" runat="server" />
                                    Già Calcolato
       <asp:Label ID="Lbl_UltimoOspiteElaborato" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">2° Importo:</label>
                                    <asp:RadioButton ID="Chk_NonAttivo" runat="server" GroupName="TipoImporto2" Text="NonAttivo" />
                                    <asp:RadioButton ID="Chk_AlberghieroAssistenziale" runat="server" GroupName="TipoImporto2" Text="Alberghiero + Assistenziale" />
                                    <asp:RadioButton ID="Chk_SocialeSanitario" runat="server" GroupName="TipoImporto2" Text="Sociale/Sanitario" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Accoglimento : </label>
                                    <asp:DropDownList ID="DD_CausaleAccoglimento" runat="server"></asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">Codice Iva : </label>
                                    <asp:DropDownList ID="DD_CodiceIVA" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">TipoOperazione :</label>
                                    <asp:DropDownList ID="DD_TipoOperazione" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">ModalitaPagamento :</label>
                                    <asp:DropDownList ID="DD_ModalitaPagamento" runat="server"></asp:DropDownList>
                                    Obbligatoria
                                    <asp:CheckBox ID="Chk_ModalitaPagamentoObligatoria" runat="server" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Controlla Deposito :</label>
                                    <asp:CheckBox ID="Chk_VerificaDeposito" runat="server" />(Non In Uso)<br />
                                    <br />


                                    <label class="LabelCampo">Descrizione Default Denaro : </label>
                                    <asp:TextBox ID="Txt_DescrizioneDefaultDenaro" MaxLength="50" Width="400px" runat="server"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Tipi Addebito allegato : </label>
                                    <asp:TextBox ID="Txt_AddebitiRettaInRendiconto" MaxLength="250" Width="400px" runat="server"></asp:TextBox>
                                    (Tipi addebito da sommare alle assenze in allegati,rendiconti)
        <br />
                                    <br />

                                    <label class="LabelCampo">Raggruppa Ricavi :</label>
                                    <asp:CheckBox ID="Chk_RaggruppaRicavi" runat="server" />(Fatture Ospiti)<br />
                                    <br />


                                    <label class="LabelCampo">Id Mandato(CF) :</label>
                                    <asp:CheckBox ID="Chk_IdMandatoDaCF" runat="server" />
                                    Codice SIA :
        <asp:TextBox ID="Txt_MandatoSIA" MaxLength="50" Width="300px" runat="server"></asp:TextBox>
                                    Progressivo:<asp:TextBox ID="Txt_ProgrerssivoID" MaxLength="50" Width="80px" runat="server"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Controllo Unicità IdMandato :</label>
                                    <asp:CheckBox ID="Chk_CheckIdMandato" runat="server" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Tipo Export:</label>
                                    <asp:DropDownList ID="DD_TipoExport" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />




                                    <label class="LabelCampo">Giorni Presenza Diurno:</label>
                                    <asp:CheckBox ID="Chk_GiorniPresenzaDiurno" runat="server" Text="Non visualizzare in anagrafica ospite" /><br />
                                    <br />

                                    <label class="LabelCampo">Ora in Movimenti:</label>
                                    <asp:CheckBox ID="Chk_OraSuMovimenti" runat="server" Text="" /><br />
                                    <br />


                                    <label class="LabelCampo">Controllo Indirizzo :</label>
                                    <asp:CheckBox ID="Chk_ContolloIndirizzo" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo">Fatture/NC nuova versione :</label>
                                    <asp:CheckBox ID="Chk_FatturaNC" runat="server" /><br />
                                    <br />


                                    <label class="LabelCampo">Fattura/NC Abilita Modifica Periodo :</label>
                                    <asp:CheckBox ID="Chk_ModificaPeriodoFatturaNC" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo">Abilita Qr Code AddAcc :</label>
                                    <asp:CheckBox ID="Chk_AbilitaQrCodeAddAcc" runat="server" /><br />
                                    <br />


                                    <label class="LabelCampo">Un solo Parente (intestatario) :</label>
                                    <asp:CheckBox ID="Chk_ParenteIntestatarioUnico" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo">Stampa Servizi Attivi :</label>
                                    <asp:CheckBox ID="Chk_StampaServiziAttivi" runat="server" /><br />
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Calcolo
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Giorno Uscita : </label>
                                    <asp:RadioButton ID="RB_Presente" GroupName="GGU" runat="server" />
                                    Presente
           <asp:RadioButton ID="RB_Assente" GroupName="GGU" runat="server" />
                                    Assente<br />
                                    <br />
                                    <label class="LabelCampo">Giorno Entrata : </label>
                                    <asp:RadioButton ID="RB_E_Presente" GroupName="GGE" runat="server" />
                                    Presente
           <asp:RadioButton ID="RB_E_Assente" GroupName="GGE" runat="server" />
                                    Assente<br />
                                    <br />
                                    <label class="LabelCampo">Calcolo Giorno :</label>
                                    <asp:RadioButton ID="Rb_GiorniMese" GroupName="CalcoloGiorni" runat="server" />Mensile/Giorni Mese
           <asp:RadioButton ID="Rb_Diviso30" GroupName="CalcoloGiorni" runat="server" />Mensile/ 30
           <asp:RadioButton ID="Rb_MeseAnno" GroupName="CalcoloGiorni" runat="server" />Mensile * 12 / 365
           <asp:RadioButton ID="RB_Anno12" GroupName="CalcoloGiorni" runat="server" />Mensile/ (GG Anno / 12)
           <asp:RadioButton ID="Rb_Diviso34" GroupName="CalcoloGiorni" runat="server" />Mensile/ 30,04<br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito/Accredito per Deposito : </label>
                                    <asp:DropDownList ID="DD_Deposito" runat="server"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Tipo Addebito/Accredito Variazione Retta : </label>
                                    <asp:DropDownList ID="DD_VariazioneRetta" runat="server"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Conguaglio Minimo : </label>
                                    <asp:TextBox ID="Txt_ConguagilioMinimo" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Non allineare differenza su mensile :</label>
                                    <asp:CheckBox ID="Chk_NonAllineareMensileAdifferenza" runat="server" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Cambio Servizio Senza Conguaglio : </label>
                                    <asp:DropDownList ID="DD_CausaleCambioSenzaConguaglio" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Denaro da Competenza :</label>
                                    <asp:CheckBox ID="Chk_DenaroDaCompetenza" runat="server" />
                                    <br />
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Emissione
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Ordine in Emissione :</label>
                                    <asp:RadioButton ID="RB_OrdinaOspite" runat="server" Text="Ordina per Ospite" GroupName="Ordine" />
                                    <asp:RadioButton ID="RB_OrdinaOspiteServizio" runat="server" Text="Ordine per Ospite (Servizio)" GroupName="Ordine" />
                                    <asp:RadioButton ID="RB_OrdinaPerDestinatario" runat="server" Text="Ordina Per Destinatario" GroupName="Ordine" />
                                    <asp:RadioButton ID="RB_OrdinaPerDestinatarioServizio" runat="server" Text="Ordina Per Destinatario (Servizio)" GroupName="Ordine" /><br />

                                    <br />
                                    <label class="LabelCampo">Non Emis. Comuni :</label>
                                    <asp:CheckBox ID="Chk_EmissioneComuni" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Non Emis. Regione :</label>
                                    <asp:CheckBox ID="Chk_EmissioneRegione" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Non Emis. Jolly :</label>
                                    <asp:CheckBox ID="Chk_EmissioneJolly" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Non Emis. Ospite :</label>
                                    <asp:CheckBox ID="Chk_EmissioneOspite" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Non Emis. Parente :</label>
                                    <asp:CheckBox ID="Chk_EmissioneParente" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Azzera < 0 in Calcolo:</label>
                                    <asp:CheckBox ID="Chk_AzzeraSeNegativo" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Add. Accumulati enti :</label>
                                    <asp:CheckBox ID="Chk_AddebitiAccumolati" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Mastro :</label>
                                    <asp:TextBox ID="Txt_Mastro" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server" Width="97px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto Comune :</label>
                                    <asp:TextBox ID="Txt_ContoComune" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto Regione :</label>
                                    <asp:TextBox ID="Txt_ContoRegione" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Mastro Anticipo :</label>
                                    <asp:TextBox ID="Txt_MastroAnticipo" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito in fattura :</label>
                                    <asp:CheckBox ID="Chk_TipoAddebitoInFattura" runat="server" />
                                    (Inserisce tipo addebito in descrizione della riga fattura)
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Separa Periodo :</label>
                                    <asp:CheckBox ID="Chk_SeparaPeriodoOspiteParente" runat="server" /><br />

                                    <br />
                                    <label class="LabelCampo">Split Payment Enti :</label>
                                    <asp:CheckBox ID="Chk_SplitEnti" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Esegui Ordinamento Protocollo su Enti :</label>
                                    <asp:CheckBox ID="Chk_OrdinamentoCR" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo">Causale Contabile SeparaIVA : </label>
                                    <asp:DropDownList ID="DD_CausaleContabileSeparaIVA" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Documenti / Incassi" ID="TabPanel4">
                                <HeaderTemplate>
                                    Documenti / Incassi
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Blocca Modifica Periodo :</label>
                                    <asp:CheckBox ID="Chk_DocumentoIncassi" runat="server" Text="" />(Domiciliari)<br />
                                    <br />


                                    <label class="LabelCampo">Periodo:</label>
                                    <asp:CheckBox ID="Chk_DocumentiIncassiNonPeriodo" runat="server" Text="(Disabilita Modifica Periodo)" /><br />
                                    <br />


                                    <label class="LabelCampo">Periodo Su Ultima Uscita Def:</label>
                                    <asp:CheckBox ID="Chk_UltimaUscitaDefinitiva" runat="server" Text="Periodo su Ultima Uscita Definitiva" />(Ambulatori)<br />
                                    <br />


                                    <label class="LabelCampo">Doc. Inc. mesi precedenti:</label>
                                    <asp:TextBox ID="Txt_DocumentoIncassiMesiPrecedenti" onkeypress="return soloNumeri(event);" MaxLength="2" Width="100px" runat="server"></asp:TextBox>(Conguaglia mesi precedenti - Ambulatori)<br />
                                    <br />



                                    <label class="LabelCampo">Descrizione in Incasso:</label>
                                    <asp:CheckBox ID="Chk_DescrizioneInIncasso" runat="server" Text="" />(Descrizione Automatica in Incassi Anticipi)<br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="EPersonam" ID="TabPanel3">
                                <HeaderTemplate>
                                    ePersonam
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Data Sosia:</label>
                                    <asp:CheckBox ID="Chk_DataSosia" runat="server" />(Data Sosia per Web Service per E-Personam)
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Non In Attesa:</label>
                                    <asp:CheckBox ID="Chk_NonInAtteasa" runat="server" />(E-Personam)
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Non Us/E Temporanee:</label>
                                    <asp:CheckBox ID="Chk_NonUsciteTemporanee" runat="server" />(E-Personam)
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Api V 1.0:</label>
                                    <asp:CheckBox ID="Chk_ApiV1" runat="server" />(Usare solo con utente tecnico)
                                    <br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Solo Modifica :</label>
                                    <asp:CheckBox ID="Chk_SoloModAnagraficaDaEpersonam" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Debug</label>
                                    <asp:CheckBox ID="Chk_Debug" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Verifica Movimenti da importare :</label>
                                    <asp:CheckBox ID="Chk_V1MovimentiEpersonam" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Import Listino:</label>
                                    <asp:CheckBox ID="Chk_ImportListino" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Movim. Stanze:</label>
                                    <asp:CheckBox ID="Chk_MovimentiStanze" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Cartella :</label>
                                    <asp:CheckBox ID="Chk_Cartella" runat="server" /><br />
                                    <br />



                                    <label class="LabelCampo">Check ePersonam :</label>
                                    <asp:TextBox ID="Txt_CheckePersonam" onkeypress="return soloNumeri(event);" Width="100px" runat="server"></asp:TextBox><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="EPersonam" ID="TabPanel5">
                                <HeaderTemplate>
                                    Fattura Elettronica
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo" style="margin-left: 50px;">Raggruppa Ricavi:</label>
                                    <asp:CheckBox ID="Chk_FE_RaggruppaRicavi" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Indica Mastro Partita:</label>
                                    <asp:CheckBox ID="Chk_FE_IndicaMastroPartita" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Competenza:</label>
                                    <asp:CheckBox ID="Chk_FE_Competenza" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Centro Servizio:</label>
                                    <asp:CheckBox ID="Chk_FE_CentroServizio" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Giorni in Descrizione:</label>
                                    <asp:CheckBox ID="Chk_FE_GiorniInDescrizione" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Tipo Retta:</label>
                                    <asp:CheckBox ID="Chk_FE_TipoRetta" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Solo Iniziali:</label>
                                    <asp:CheckBox ID="Chk_FE_SoloIniziali" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Note Fatture:</label>
                                    <asp:CheckBox ID="Chk_FE_NoteFatture" runat="server" /><br />
                                    <br />


                                    <label class="LabelCampo" style="margin-left: 50px;">Sez. Prima Prot.:</label>
                                    <asp:CheckBox ID="Chk_XMLSEZIONALEPRIMAPROTOCOLLO" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Usa Solo Descrizione Riga Fattura :</label>
                                    <asp:CheckBox ID="Chk_SoloDescrizioneRigaFattura" runat="server" /><br />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Operazioni Batch" ID="TabPanel6">
                                <HeaderTemplate>
                                    Operazioni Batch
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo" style="margin-left: 50px;">Allinea Anag.:</label>
                                    <asp:CheckBox ID="Chk_AllineaAnag" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Allinea Movimenti :</label>
                                    <asp:CheckBox ID="Chk_AllineaMovimenti" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Tabelle Presenze :</label>
                                    <asp:CheckBox ID="Chk_TabPresenze" runat="server" /><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
