﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System.Data.DataView

Partial Class TabelleRegioni
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Dim MyTableImp As New System.Data.DataTable("tabella")
    Dim MyDataSetImp As New System.Data.DataSet()


    Dim MyEccezioni As New System.Data.DataTable("Eccezioni")
    Dim DSEccezioni As New System.Data.DataSet()



    Dim TabComuniResidenza As New System.Data.DataTable("Eccezioni")




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then
            Call EseguiJS()
            Exit Sub
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim k As New ClsUSL


        k.UpDateDropBoxCodice(Session("DC_OSPITE"), DD_Raggruppato)


        Dim DDTipoOpe As New Cls_TipoOperazione
        DDTipoOpe.UpDateDropBoxTipo(ConnectionString, DD_TipoOperazione, "E")

        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(ConnectionString, DD_ModalitaPagamento)

        Dim DDIVA As New Cls_IVA
        DDIVA.UpDateDropBox(ConnectionStringTabelle, DD_IVA)

        If Request.Item("CODICE") = "" Then

            Txt_Codice.Text = k.MaxRegione(Session("DC_OSPITE"))


            Dim X1 As New Cls_ImportoRegione



            X1.loaddati(ConnectionString, "", MyTableImp)
            ViewState("Appoggio") = MyTableImp
            Grd_Importo.AutoGenerateColumns = False
            Grd_Importo.DataSource = MyTableImp
            Grd_Importo.DataBind()
            Exit Sub
        End If

        k.CodiceRegione = Request.Item("CODICE")
        k.Leggi(ConnectionString)
        Txt_Codice.Text = k.CodiceRegione

        Txt_Codice.Enabled = False

        Txt_Descrizione.Text = k.Nome

        If k.NonInUso = "S" Then
            Chk_NonInUso.Checked = True
        Else
            Chk_NonInUso.Checked = False
        End If


        Txt_NomeConiuge.Text = k.NonInVisualizzazione
        Txt_Indirizzo.Text = k.RESIDENZAINDIRIZZO1
        Txt_Cap.Text = k.RESIDENZACAP1
        Txt_Attenzione.Text = k.Attenzione
        Txt_Piva.Text = k.PARTITAIVA
        Txt_CodiceFiscale.Text = k.CodiceFiscale


        Dim MyRes As New ClsComune

        MyRes.Descrizione = ""
        MyRes.Provincia = k.RESIDENZAPROVINCIA1
        MyRes.Comune = k.RESIDENZACOMUNE1
        MyRes.Leggi(Session("DC_OSPITE"))
        If k.RESIDENZAPROVINCIA1 <> "" Then
            Txt_ComRes.Text = k.RESIDENZAPROVINCIA1 & " " & k.RESIDENZACOMUNE1 & " " & MyRes.Descrizione
        Else
            Txt_ComRes.Text = ""
        End If


        Txt_Int.Text = k.IntCliente

        Txt_NumCont.Text = k.NumeroControlloCliente

        Txt_Cab.Text = k.CABCLIENTE

        Txt_Abi.Text = k.ABICLIENTE
        Txt_Cin.Text = k.CINCLIENTE
        Txt_Ccbancario.Text = k.CCBANCARIOCLIENTE
        Txt_BancaCliente.Text = k.BancaCliente
        Txt_BancaCIG.Text = k.CodiceCig

        TxT_Mail.Text = k.RESIDENZATELEFONO3

        Txt_Esportazione.Text = k.CONTOPERESATTO

        Txt_Referente.Text = k.RecapitoNome
        Txt_TelefonoReferente.Text = k.RESIDENZATELEFONO4

        DD_TipoOperazione.SelectedValue = k.TipoOperazione
        DD_IVA.SelectedValue = k.CodiceIva
        DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento
        If k.Periodo = "M" Then
            RB_Mensile.Checked = True
        End If
        If k.Periodo = "B" Then
            RB_Bimestrale.Checked = True
        End If

        If k.Periodo = "T" Then
            RB_Trimestrale.Checked = True
        End If

        If k.Periodo = "K" Then
            RB_TreMesiD.Checked = True
        End If
        If k.Periodo = "N" Then
            RB_NonFatturare.Checked = True
        End If
        If k.Periodo = "X" Then
            RB_FatturaDa.Checked = True
            TxtAnno.Enabled = True
            TxtMese.Enabled = True
            TxtAnno.Text = k.AnnoFatturazione
            TxtMese.Text = k.MeseFatturazione

            TxtAnno.Enabled = True
            TxtAnno.BackColor = System.Drawing.Color.White
            TxtMese.Enabled = True
            TxtMese.BackColor = System.Drawing.Color.White
        End If

        If RB_SI.Checked = True Then
            k.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then            
            k.Compensazione = "N"
        End If


        If k.RotturaCserv = "S" Then
            Chk_RotturaCserv.Checked = True
        Else
            Chk_RotturaCserv.Checked = False
        End If

        If k.RotturaOspite = 1 Then
            Chk_RotturaOspite.Checked = True
        Else
            Chk_RotturaOspite.Checked = False
        End If

        If k.ImportoAZero = 1 Then
            Chk_ImportiAzero.Checked = True
        Else
            Chk_ImportiAzero.Checked = False
        End If

        If k.EGo = "" Or k.EGo = "N" Then
            DD_EGO.SelectedValue = "N"
        Else
            DD_EGO.SelectedValue = k.EGo
        End If



        Txt_NoteFe.Text = k.Note

        Txt_Cup.Text = k.CodiceCup
        Txt_IdDocumento.Text = k.IdDocumento

        Txt_DataIdDocumento.text = k.IdDocumentoData
        Txt_RiferimentoAmministrazione.Text = k.RiferimentoAmministrazione
        If k.NumeroFatturaDDT = 1 Then
            Chk_NFDtt.Checked = True
        Else
            Chk_NFDtt.Checked = False
        End If

        Rb_NonRagruppare.Checked = False
        Rb_GiorniInDesc.Checked = False
        Rb_GiorniInQty.Checked = False
        If k.RaggruppaInExport = 0 Then
            Rb_NonRagruppare.Checked = True
        End If
        If k.RaggruppaInExport = 1 Then
            Rb_GiorniInDesc.Checked = True
        End If
        If k.RaggruppaInExport = 2 Then
            Rb_GiorniInQty.Checked = True
        End If

        If k.RaggruppaInElaborazione = 1 Then
            Chk_RaggruppaInElab.Checked = True
        Else
            Chk_RaggruppaInElab.Checked = False
        End If


        Txt_Sconto.Text = k.ImportoSconto

        RB_SI.Checked = False
        RB_NO.Checked = False
        If k.Compensazione = "S" Then
            RB_SI.Checked = True
        End If
        If k.Compensazione = "N" Then
            RB_NO.Checked = True
        End If
        If k.Compensazione = "T" Then
            RB_Dettaglio.Checked = True
        End If
        DD_Raggruppato.SelectedValue = k.Raggruppamento

        Dim ks As New Cls_Pianodeiconti

        ks.Mastro = k.MastroCliente
        ks.Conto = k.ContoCliente
        ks.Sottoconto = k.SottoContoCliente
        ks.Decodfica(Session("DC_GENERALE"))

        Txt_Sottoconto.Text = ks.Mastro & " " & ks.Conto & " " & ks.Sottoconto & " " & ks.Descrizione


        Txt_CodiceDestinatario.Text = k.CodiceDestinatario


        Txt_NumItem.Text = k.NumItem

        Txt_CodiceCommessaConvezione.Text = k.CodiceCommessaConvezione

        Txt_PEC.Text = k.PEC




        If k.DescrizioneXML = "" Then
            Chk_Personalizzare.Checked = False
            Chk_FE_RaggruppaRicavi.Enabled = False
            Chk_FE_IndicaMastroPartita.Enabled = False
            Chk_FE_Competenza.Enabled = False
            Chk_FE_CentroServizio.Enabled = False
            Chk_FE_GiorniInDescrizione.Enabled = False
            Chk_FE_TipoRetta.Enabled = False
            Chk_FE_SoloIniziali.Enabled = False
            Chk_FE_NoteFatture.Enabled = False
            Chk_SoloDescrizioneRigaFattura.Enabled=False
        Else
            Chk_Personalizzare.Checked = True

            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            Chk_SoloDescrizioneRigaFattura.Enabled =True
           
            If k.DescrizioneXML.IndexOf("<FE_RaggruppaRicavi>") >= 0 Then
                Chk_FE_RaggruppaRicavi.Checked = True
            End If
            If k.DescrizioneXML.IndexOf("<FE_IndicaMastroPartita>") >= 0 Then
                Chk_FE_IndicaMastroPartita.Checked = True
            End If
            If k.DescrizioneXML.IndexOf("<FE_Competenza>") >= 0 Then
                Chk_FE_Competenza.Checked = True
            End If
            If k.DescrizioneXML.IndexOf("<FE_CentroServizio>") >= 0 Then
                Chk_FE_CentroServizio.Checked = True
            End If

            If k.DescrizioneXML.IndexOf("<FE_GiorniInDescrizione>") >= 0 Then
                Chk_FE_GiorniInDescrizione.Checked = True
            End If
            If k.DescrizioneXML.IndexOf("<FE_TipoRetta>") >= 0 Then
                Chk_FE_TipoRetta.Checked = True
            End If

            If k.DescrizioneXML.IndexOf("<FE_SoloIniziali>") >= 0 Then
                Chk_FE_SoloIniziali.Checked = True
            End If

            If k.DescrizioneXML.IndexOf("<FE_NoteFatture>") >= 0 Then
                Chk_FE_NoteFatture.Checked = True
            End If

            If k.DescrizioneXML.IndexOf("<SoloDescrizioneRigaFattura>") >= 0 Then
                Chk_SoloDescrizioneRigaFattura.Checked = True
            End If

        End If


        Dim X As New Cls_ImportoRegione

        X.loaddati(ConnectionString, k.CodiceRegione, MyTableImp)

        ViewState("Appoggio") = MyTableImp

        Grd_Importo.AutoGenerateColumns = False
        Grd_Importo.DataSource = MyTableImp.DefaultView
        Grd_Importo.DataBind()

        Dim IC As New Cls_DatiComnueRegioneServizio

        IC.CodiceRegione = k.CodiceRegione
        IC.loaddati(ConnectionString, MyEccezioni)


        ViewState("Eccezioni") = MyEccezioni

        Grd_Eccezioni.AutoGenerateColumns = False
        Grd_Eccezioni.DataSource = MyEccezioni.DefaultView
        Grd_Eccezioni.DataBind()



        Dim ComuniResidenza As New ClsComune


        ComuniResidenza.loaddati(ConnectionString, k.CodiceRegione, TabComuniResidenza)

        ViewState("TabComuniResidenza") = TabComuniResidenza

        Grd_Comuni.AutoGenerateColumns = False
        Grd_Comuni.DataSource = TabComuniResidenza
        Grd_Comuni.DataBind()



        'Dim m As New System.Data.DataView

        'm = MyTableImp.DefaultView
        'm.AllowEdit = True
        'm.Sort = "Data DESC"



        'Dim kSA As String
        'kSA = m(0)(1).ToString

        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If

    End Sub

    
  

    Protected Sub ModificaRegione()
        Dim k As New ClsUSL
        Dim ConnectionString As String = Session("DC_OSPITE")

        k.CodiceRegione = Txt_Codice.Text
        k.CodiceRegione = Txt_Codice.Text
        k.Nome = Txt_Descrizione.Text
        k.NonInVisualizzazione = Txt_NomeConiuge.Text
        k.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        k.RESIDENZACAP1 = Txt_Cap.Text
        k.Attenzione = Txt_Attenzione.Text
        k.PARTITAIVA = Val(Txt_Piva.Text)
        k.CodiceFiscale = Txt_CodiceFiscale.Text

        If Chk_NonInUso.Checked = True Then
            k.NonInUso = "S"
        Else
            k.NonInUso = ""
        End If


        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                k.RESIDENZAPROVINCIA1 = Vettore(0)
                k.RESIDENZACOMUNE1 = Vettore(1)
            End If
        Else
            k.RESIDENZAPROVINCIA1 = ""
            k.RESIDENZACOMUNE1 = ""
        End If

        If Chk_RotturaCserv.Checked = True Then
            k.RotturaCserv = "S"
        Else
            k.RotturaCserv = "N"
        End If

        If Chk_RotturaOspite.Checked = True Then
            k.RotturaOspite = 1
        Else
            k.RotturaOspite = 0
        End If


        If Chk_ImportiAzero.Checked = True Then
            k.ImportoAZero = 1
        Else
            k.ImportoAZero = 0
        End If


        k.EGo = DD_EGO.SelectedValue 

        k.CONTOPERESATTO = Txt_Esportazione.Text


        k.CodiceDestinatario = Txt_CodiceDestinatario.Text
        k.IntCliente = Txt_Int.Text
        k.NumeroControlloCliente = Txt_NumCont.Text
        k.CABCLIENTE = Txt_Cab.Text
        k.ABICLIENTE = Txt_Abi.Text
        k.CINCLIENTE = Txt_Cin.Text
        k.CCBANCARIOCLIENTE = Txt_Ccbancario.Text
        k.BancaCliente = Txt_BancaCliente.Text
        k.CodiceCig = Txt_BancaCIG.Text
        k.TipoOperazione = DD_TipoOperazione.SelectedValue
        k.CodiceIva = DD_IVA.SelectedValue
        k.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue
        If RB_Mensile.Checked = True Then
            k.Periodo = "M"
        End If
        If RB_Bimestrale.Checked = True Then
            k.Periodo = "B"
        End If

        If RB_Trimestrale.Checked = True Then
            k.Periodo = "T"
        End If

        If RB_TreMesiD.Checked = True Then
            k.Periodo = "K"
        End If
        If RB_NonFatturare.Checked = True Then
            k.Periodo = "N"
        End If

        If RB_SI.Checked = True Then
            k.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            k.Compensazione = "N"
        End If
        If RB_Dettaglio.Checked = True Then
            k.Compensazione = "T"
        End If

        If RB_FatturaDa.Checked = True Then
            k.Periodo = "X"

            k.AnnoFatturazione = TxtAnno.Text
            k.MeseFatturazione = TxtMese.Text
        End If

        If RB_NonFatturare.Checked = True Then
            k.Periodo = "N"
        End If


        k.Raggruppamento = DD_Raggruppato.SelectedValue

        k.Note = Txt_NoteFe.Text

        If Txt_Sconto.Text.Trim = "" Then
            Txt_Sconto.Text = "0"
        End If

        k.ImportoSconto = Txt_Sconto.Text


        k.NumItem = Txt_NumItem.Text

        k.CodiceCommessaConvezione = Txt_CodiceCommessaConvezione.Text

        k.PEC = Txt_PEC.Text

        If Txt_Sottoconto.Text.Replace("0", "").Trim <> "" Then
            k.MastroCliente = 0
            k.ContoCliente = 0
            k.SottoContoCliente = 0

            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                k.MastroCliente = Vettore(0)
                k.ContoCliente = Vettore(1)
                k.SottoContoCliente = Vettore(2)
            End If
        Else
            k.MastroCliente = 0
            k.ContoCliente = 0
            k.SottoContoCliente = 0
        End If

        If k.SottoContoCliente = 0 And DD_TipoOperazione.SelectedValue <> "" Then
            Dim TabellaParametri As New Cls_Parametri

            TabellaParametri.LeggiParametri(Session("DC_OSPITE"))

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = TabellaParametri.Mastro
            PianoConti.Conto = TabellaParametri.ContoRegione
            PianoConti.Sottoconto = k.CodiceRegione
            PianoConti.Decodfica(Session("DC_GENERALE"))

            If PianoConti.Descrizione = "" Then
                PianoConti.Mastro = TabellaParametri.Mastro
                PianoConti.Conto = TabellaParametri.ContoRegione
                PianoConti.Sottoconto = k.CodiceRegione
                PianoConti.Descrizione = Txt_Descrizione.Text
                PianoConti.Tipo = "A"
                PianoConti.TipoAnagrafica = "R"
                PianoConti.Scrivi(Session("DC_GENERALE"))

                k.MastroCliente = TabellaParametri.Mastro
                k.ContoCliente = TabellaParametri.ContoRegione
                k.SottoContoCliente = k.CodiceRegione
            Else
                k.MastroCliente = TabellaParametri.Mastro
                k.ContoCliente = TabellaParametri.ContoRegione
                k.SottoContoCliente = k.CodiceRegione
            End If
     
        End If



        k.RecapitoNome = Txt_Referente.Text
        k.RESIDENZATELEFONO4 = Txt_TelefonoReferente.Text



        k.RESIDENZATELEFONO3 = TxT_Mail.Text
        k.CodiceCup = Txt_Cup.Text
        k.IdDocumento = Txt_IdDocumento.Text

        k.IdDocumentoData = Txt_DataIdDocumento.text
        k.RiferimentoAmministrazione = Txt_RiferimentoAmministrazione.Text
        If Chk_NFDtt.Checked = True Then
            k.NumeroFatturaDDT = 1
        Else
            k.NumeroFatturaDDT = 0
        End If

        If Rb_NonRagruppare.Checked = True Then
            k.RaggruppaInExport = 0
        End If
        If Rb_GiorniInDesc.Checked = True Then
            k.RaggruppaInExport = 1
        End If
        If Rb_GiorniInQty.Checked = True Then
            k.RaggruppaInExport = 2
        End If

        If Chk_RaggruppaInElab.Checked = True Then
            k.RaggruppaInElaborazione = 1
        Else
            k.RaggruppaInElaborazione = 0
        End If

        If Chk_Personalizzare.Checked = False Then
            k.DescrizioneXML = ""
        Else
            k.DescrizioneXML = "<PERSONOLIZZA>"

            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            Chk_SoloDescrizioneRigaFattura.Enabled=True

            If Chk_FE_RaggruppaRicavi.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_RaggruppaRicavi>"
            End If
            If Chk_FE_IndicaMastroPartita.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_IndicaMastroPartita>"
            End If
            If Chk_FE_Competenza.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_Competenza>"
            End If
            If Chk_FE_CentroServizio.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_CentroServizio>"
            End If

            If Chk_FE_GiorniInDescrizione.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_GiorniInDescrizione>"
            End If
            If Chk_FE_TipoRetta.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_TipoRetta>"
            End If

            If Chk_FE_SoloIniziali.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_SoloIniziali>"
            End If

            If Chk_FE_NoteFatture.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_NoteFatture>"
            End If

            
            If Chk_SoloDescrizioneRigaFattura.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<SoloDescrizioneRigaFattura>"
            End If                        
        End If


        k.ScriviRegione(ConnectionString)



        Dim X1 As New Cls_ImportoRegione

        X1.Codice = Txt_Codice.Text

        X1.EliminaAll(Session("DC_OSPITE"))

        For i = 0 To Grd_Importo.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Importo.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Importo.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Importo.Rows(i).FindControl("DD_Tipo"), DropDownList)
            Dim DD_TipoRette As DropDownList = DirectCast(Grd_Importo.Rows(i).FindControl("DD_TipoRetta"), DropDownList)

            Dim X As New Cls_ImportoRegione

            X.Codice = Txt_Codice.Text


            If IsDate(TxtData.Text) And TxtImporto.Text <> "" Then
                X.Data = TxtData.Text
                X.Importo = TxtImporto.Text
                X.Tipo = DD_Tipo.SelectedValue
                X.TipoRetta = DD_TipoRette.Text
                X.Utente = Session("UTENTE")
                X.AggiornaDB(Session("DC_OSPITE"))
            End If
        Next



        Dim E1 As New Cls_DatiComnueRegioneServizio

        E1.CodiceRegione = Txt_Codice.Text

        E1.EliminaAll(Session("DC_OSPITE"))

        For i = 0 To Grd_Eccezioni.Rows.Count - 1



            Dim DD_CentroSerivizio As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_CentroSerivizio"), DropDownList)
            Dim DD_TipoOperazione As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)
            Dim DD_IVA As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_IVA"), DropDownList)

            Dim EX As New Cls_DatiComnueRegioneServizio

            EX.CodiceRegione = Txt_Codice.Text
            EX.CentroServizio = DD_CentroSerivizio.SelectedValue
            EX.TipoOperazione = DD_TipoOperazione.SelectedValue
            EX.AliquotaIva = DD_IVA.SelectedValue

            EX.Scrivi(Session("DC_OSPITE"))

        Next


        Dim Comune As New ClsComune

        Call UpDateTableComune()

        TabComuniResidenza = ViewState("TabComuniResidenza")



        Comune.RIFERIMENTOREGIONE = Txt_Codice.Text

        Comune.AggiornaDaTabella(Session("DC_OSPITE"), TabComuniResidenza)







    End Sub
    

    
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim k As New ClsUSL
            Dim ConnectionString As String = Session("DC_OSPITE")

            k.CodiceRegione = Txt_Codice.Text
            k.Leggi(ConnectionString)
            If k.Nome <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Regione già presente con questo codice');", True)
                Exit Sub
            End If
        End If


        If RB_SI.Checked = True Then
            Dim MS As New Cls_TipoOperazione

            MS.Codice = DD_TipoOperazione.SelectedValue
            MS.Leggi(Session("DC_OSPITE"), MS.Codice)

            If MS.SCORPORAIVA = 1 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Compensazione Si non compatibile con Scorporo IVA');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        Call UpDateTable()
        MyTableImp = ViewState("Appoggio")

        Dim m As New System.Data.DataView

        m = MyTableImp.DefaultView
        m.AllowEdit = True
        m.Sort = "Data DESC" 'ordinamento date come fossero testo


        Dim Appoggio As String = ""
        Dim AppoggioTipoRetta As String = ""
        Dim x As Integer
        For x = 0 To m.Table.Rows.Count - 1
            If Appoggio = m(x)(0).ToString And Appoggio <> "" And AppoggioTipoRetta = m(x)(3).ToString Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso inserire date uguali');", True)
                Exit Sub
            End If
            Appoggio = m(x)(0).ToString
            AppoggioTipoRetta = m(x)(3).ToString
        Next


        Call ModificaRegione()

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If

        Dim X As New ClsUSL

        X.CodiceRegione = Txt_Codice.Text
        If X.InUso(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If

        X.Leggi(Session("DC_OSPITE"))


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson

        Dim AppoggioJS As String = ConvT.SerializeObject(X)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "REGIONE", AppoggioJS)


        X.EliminaRegione(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTableImp = ViewState("Appoggio")
        Dim myriga As System.Data.DataRow = MyTableImp.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTableImp.Rows.Add(myriga)

        ViewState("Appoggio") = MyTableImp
        Grd_Importo.AutoGenerateColumns = False

        Grd_Importo.DataSource = MyTableImp

        Grd_Importo.DataBind()

        Call EseguiJS()
    End Sub



    Private Sub InserisciRigaEccezioni()
        UpDateTableEccezioni()
        MyEccezioni = ViewState("Eccezioni")
        Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyEccezioni.Rows.Add(myriga)

        ViewState("Eccezioni") = MyEccezioni
        Grd_Eccezioni.AutoGenerateColumns = False

        Grd_Eccezioni.DataSource = MyEccezioni

        Grd_Eccezioni.DataBind()

        Call EseguiJS()
    End Sub

    Protected Sub Grd_Importo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Importo.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grd_Importo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Importo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            MyTableImp = ViewState("Appoggio")

            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTableImp.Rows(e.Row.RowIndex).Item(0).ToString

            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
            TxtImporto.Text = MyTableImp.Rows(e.Row.RowIndex).Item(1).ToString
            Dim MyJs As String

            MyJs = "$('#" & TxtImporto.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImporto.ClientID & "').val(), true, true); } );  $('#" & TxtImporto.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImporto.ClientID & "').val(),2); $('#" & TxtImporto.ClientID & "').val(ap); }); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTableImp.Rows(e.Row.RowIndex).Item(2).ToString

            Dim DD_TipoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoRetta"), DropDownList)

            Dim K As New Cls_TabellaTipoImportoRegione

            K.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)

            DD_TipoRetta.SelectedValue = MyTableImp.Rows(e.Row.RowIndex).Item(3).ToString


            Call EseguiJS()

        End If
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_ComRes')!= null) || (appoggio.match('TxtComune')!= null))    {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTableEccezioni()
        Dim i As Integer

        MyEccezioni.Clear()
        MyEccezioni.Columns.Clear()
        MyEccezioni.Columns.Add("Servizio", GetType(String))
        MyEccezioni.Columns.Add("TipoOperazione", GetType(String))
        MyEccezioni.Columns.Add("IVA", GetType(String))

        For i = 0 To Grd_Eccezioni.Rows.Count - 1

            Dim DD_CentroSerivizio As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_CentroSerivizio"), DropDownList)
            Dim DD_TipoOperazione As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)
            Dim DD_IVA As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_IVA"), DropDownList)

            Dim myrigaR As System.Data.DataRow = MyEccezioni.NewRow()


            myrigaR(0) = DD_CentroSerivizio.SelectedValue
            myrigaR(1) = DD_TipoOperazione.SelectedValue
            myrigaR(2) = DD_IVA.SelectedValue

            MyEccezioni.Rows.Add(myrigaR)
        Next
        ViewState("Eccezioni") = MyEccezioni

    End Sub
    Private Sub UpDateTable()
        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("TipoRetta", GetType(String))

        For i = 0 To Grd_Importo.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Importo.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Importo.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Importo.Rows(i).FindControl("DD_Tipo"), DropDownList)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_Importo.Rows(i).FindControl("DD_TipoRetta"), DropDownList)

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text
            myrigaR(1) = TxtImporto.Text
            myrigaR(2) = DD_Tipo.Text
            myrigaR(3) = DD_TipoRetta.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("Appoggio") = MyTable

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub


    Private Sub PaginaPrecedente()
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoRegioni.aspx")
        Else
            Response.Redirect("ElencoRegioni.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Grd_Importo_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Importo.RowDeleted

    End Sub

    Private Function MaxCodReg() As String

        MaxCodReg = ""
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        cmd.CommandText = "Select max(CodiceRegione) From AnagraficaComune Where Tipologia ='R' "



        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxCodReg = campodbn(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()
        cn.Close()

        If MaxCodReg = "0" Then
            MaxCodReg = ""
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_Importo_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Importo.RowDeleting
        UpDateTable()
        MyTable = ViewState("Appoggio")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("Appoggio") = MyTable

        Grd_Importo.AutoGenerateColumns = False

        Grd_Importo.DataSource = MyTable

        Grd_Importo.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Dim Regione As New ClsUSL

        Txt_Codice.Text = ""

        Txt_Codice.Text = Regione.MaxRegione(Session("DC_OSPITE"))



        Txt_Codice.Enabled = True

        Txt_Sottoconto.Text = ""


        Call Txt_Codice_TextChanged(sender, e)
        Call Txt_Descrizione_TextChanged(sender, e)

    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub Grd_Eccezioni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Eccezioni.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaEccezioni()
        End If
    End Sub

    Protected Sub Grd_Eccezioni_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Eccezioni.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            MyEccezioni = ViewState("Eccezioni")


            Dim DD_CentroSerivizio As DropDownList = DirectCast(e.Row.FindControl("DD_CentroSerivizio"), DropDownList)

            Dim k As New Cls_CentroServizio

            k.UpDateDropBox(Session("DC_OSPITE"), DD_CentroSerivizio)

            DD_CentroSerivizio.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(0).ToString


            Dim DD_TipoOperazione As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperazione"), DropDownList)

            Dim k1 As New Cls_TipoOperazione

            k1.UpDateDropBoxTipo(Session("DC_OSPITE"), DD_TipoOperazione, "E")

            DD_TipoOperazione.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(1).ToString


            Dim DD_IVA As DropDownList = DirectCast(e.Row.FindControl("DD_IVA"), DropDownList)

            Dim k2 As New Cls_IVA

            k2.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

            DD_IVA.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(2).ToString




            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grd_Eccezioni_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Eccezioni.RowDeleting
        UpDateTableEccezioni()
        MyEccezioni = ViewState("Eccezioni")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyEccezioni.Rows.RemoveAt(e.RowIndex)
                    If MyEccezioni.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyEccezioni.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyEccezioni.Rows.RemoveAt(e.RowIndex)
                If MyEccezioni.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyEccezioni.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("Eccezioni") = MyEccezioni

        Grd_Eccezioni.AutoGenerateColumns = False

        Grd_Eccezioni.DataSource = MyEccezioni

        Grd_Eccezioni.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Txt_Piva_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Piva.TextChanged
        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If
    End Sub

    Protected Sub RB_FatturaDa_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_FatturaDa.CheckedChanged
        If RB_FatturaDa.Checked = True Then
            TxtAnno.Enabled = True
            TxtAnno.BackColor = System.Drawing.Color.White
            TxtMese.Enabled = True
            TxtMese.BackColor = System.Drawing.Color.White
            Dim cn As OleDbConnection
            Dim MySql As String

            MySql = ""

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("Select AnnoCompetenza,MeseCompetenza from MovimentiContabiliTesta Where Tipologia like ? Order by AnnoCompetenza Desc,MeseCompetenza Desc ")
            cmd.Parameters.AddWithValue("@Tipologia", "%" & Txt_Codice.Text & "%")
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                TxtAnno.Text = campodb(myPOSTreader.Item("AnnoCompetenza"))
                TxtMese.Text = campodb(myPOSTreader.Item("MeseCompetenza"))
            End If
            cn.Close()

        Else

            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub RB_NonFatturare_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NonFatturare.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_Mensile_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Mensile.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_NO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NO.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_SI_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_SI.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_TreMesiD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_TreMesiD.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_Trimestrale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Trimestrale.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub

    Protected Sub RB_Bimestrale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Bimestrale.CheckedChanged
        If RB_NonFatturare.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray

            TxtMese.Text = ""
            TxtAnno.Text = ""
        End If
    End Sub


    Protected Sub Chk_Personalizzare_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_Personalizzare.CheckedChanged
        If Chk_Personalizzare.Checked = False Then
            Chk_FE_RaggruppaRicavi.Enabled = False
            Chk_FE_IndicaMastroPartita.Enabled = False
            Chk_FE_Competenza.Enabled = False
            Chk_FE_CentroServizio.Enabled = False
            Chk_FE_GiorniInDescrizione.Enabled = False
            Chk_FE_TipoRetta.Enabled = False
            Chk_FE_SoloIniziali.Enabled = False
            Chk_FE_NoteFatture.Enabled = False
            Chk_SoloDescrizioneRigaFattura.Enabled=False
            Chk_FE_RaggruppaRicavi.Enabled = False
        Else
            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            Chk_SoloDescrizioneRigaFattura.Enabled=True
            Chk_FE_RaggruppaRicavi.Enabled = True

        End If
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Codice.Text.Trim <> "" Then
                Img_VerCodice.ImageUrl = "~/images/corretto.gif"

                Dim x As New ClsUSL

                x.CodiceRegione = Txt_Codice.Text
                x.Leggi(Session("DC_OSPITE"))

                If x.Nome <> "" Then
                    Img_VerCodice.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text.Trim <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

                Dim x As New ClsUSL

                x.Nome = Txt_Descrizione.Text
                x.LeggiNome(Session("DC_OSPITE"))

                If x.CodiceRegione <> "" Then
                    Img_VerificaDes.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub Grd_Comuni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Comuni.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaComune()
        End If
    End Sub


    Private Sub InserisciRigaComune()
        UpDateTableComune()
        TabComuniResidenza = ViewState("TabComuniResidenza")
        Dim myriga As System.Data.DataRow = TabComuniResidenza.NewRow()
        myriga(0) = ""

        TabComuniResidenza.Rows.Add(myriga)

        ViewState("TabComuniResidenza") = TabComuniResidenza
        Grd_Comuni.AutoGenerateColumns = False

        Grd_Comuni.DataSource = TabComuniResidenza

        Grd_Comuni.DataBind()

        Call EseguiJS()
    End Sub

    Protected Sub Grd_Comuni_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Comuni.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            TabComuniResidenza = ViewState("TabComuniResidenza")

            Dim TxtComune As TextBox = DirectCast(e.Row.FindControl("TxtComune"), TextBox)

            TxtComune.Text = TabComuniResidenza.Rows(e.Row.RowIndex).Item(0).ToString

            Call EseguiJS()

        End If
    End Sub

    Private Sub UpDateTableComune()
        Dim i As Integer

        TabComuniResidenza.Clear()
        TabComuniResidenza.Columns.Clear()
        TabComuniResidenza.Columns.Add("Comune", GetType(String))

        For i = 0 To Grd_Comuni.Rows.Count - 1

            Dim TxtComune As TextBox = DirectCast(Grd_Comuni.Rows(i).FindControl("TxtComune"), TextBox)

            Dim myrigaR As System.Data.DataRow = TabComuniResidenza.NewRow()

            myrigaR(0) = TxtComune.Text

            TabComuniResidenza.Rows.Add(myrigaR)
        Next
        ViewState("TabComuniResidenza") = TabComuniResidenza

    End Sub

    Protected Sub Grd_Comuni_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Comuni.RowDeleting
        UpDateTableComune()
        TabComuniResidenza = ViewState("TabComuniResidenza")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    TabComuniResidenza.Rows.RemoveAt(e.RowIndex)
                    If TabComuniResidenza.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = TabComuniResidenza.NewRow()
                        TabComuniResidenza.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                TabComuniResidenza.Rows.RemoveAt(e.RowIndex)
                If TabComuniResidenza.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = TabComuniResidenza.NewRow()
                    TabComuniResidenza.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("TabComuniResidenza") = TabComuniResidenza

        Grd_Comuni.AutoGenerateColumns = False

        Grd_Comuni.DataSource = TabComuniResidenza

        Grd_Comuni.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class

