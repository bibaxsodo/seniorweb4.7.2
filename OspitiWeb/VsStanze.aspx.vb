﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_VsStanze
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Request.Item("VILLA") = "" Then
            Call Esplodi()
            Exit Sub
        End If


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = ("select Villa from Stanze  Group by Villa ")

        cmd.Connection = cn

        Lbl_VsPianoConti.Text = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Request.Item("VILLA") = campodb(myPOSTreader.Item("VILLA")) Then
                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "VIL"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
                XVilla.Leggi(Session("DC_OSPITIACCESSORI"))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & """><img src=""images/arrow.gif"" /></a>" & XVilla.Descrizione & "<br>"

                Call Reparto()
            Else
                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "VIL"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
                XVilla.Leggi(Session("DC_OSPITIACCESSORI"))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & """><img src=""images/arrow.gif"" /></a>" & XVilla.Descrizione & "<br>"
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Private Sub Esplodi()
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = ("select Villa from Stanze  Group by Villa ")

        cmd.Connection = cn

        Lbl_VsPianoConti.Text = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

            XVilla.TipoTabella = "VIL"
            XVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
            XVilla.Leggi(Session("DC_OSPITIACCESSORI"))
            Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & """><img src=""images/arrow.gif"" /></a>" & XVilla.Descrizione & "<br>"

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select Reparto from Stanze  Where Villa = '" & campodb(myPOSTreader.Item("VILLA")) & "' Group by Reparto ")
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            Do While myPOSTreader1.Read

                Dim XPer As New Cls_TabelleDescrittiveOspitiAccessori

                XPer.TipoTabella = "REP"
                XPer.CodiceTabella = campodb(myPOSTreader1.Item("REPARTO"))
                XPer.Leggi(Session("DC_OSPITIACCESSORI"))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & "&REPARTO=" & campodb(myPOSTreader1.Item("REPARTO")) & """><img src=""images/arrow.gif"" /></a>" & XPer.Descrizione & "<br>"

                Dim cmd3 As New OleDbCommand()
                cmd3.CommandText = ("select Piano from Stanze  Where Villa = '" & campodb(myPOSTreader.Item("VILLA")) & "' And REPARTO = '" & campodb(myPOSTreader1.Item("REPARTO")) & "' Group by Piano ")
                cmd3.Connection = cn
                Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
                Do While myPOSTreader3.Read
                    Dim ValoreP As String
                    Dim Inserire As Boolean = True
                    ValoreP = campodb(myPOSTreader3.Item("PIANO"))
                    
                    If ValoreP = "" Then
                        ValoreP = "NON INDICATO"
                    End If

                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & "&REPARTO=" & campodb(myPOSTreader1.Item("REPARTO")) & "&PIANO=" & ValoreP & """><img src=""images/arrow.gif"" /></a>" & ValoreP & "(Piano)<br>"

                    Dim cmd4 As New OleDbCommand()
                    If ValoreP = "NON INDICATO" Then
                        cmd4.CommandText = ("select Stanza from Stanze  Where Villa = '" & campodb(myPOSTreader.Item("VILLA")) & "' And REPARTO = '" & campodb(myPOSTreader1.Item("REPARTO")) & "' And (Piano Is Null Or Piano ='') Group by Stanza ")
                    Else
                        cmd4.CommandText = ("select Stanza from Stanze  Where Villa = '" & campodb(myPOSTreader.Item("VILLA")) & "' And REPARTO = '" & campodb(myPOSTreader1.Item("REPARTO")) & "' And Piano = '" & ValoreP & "' Group by Stanza ")
                    End If
                    cmd4.Connection = cn
                    Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
                    Do While myPOSTreader4.Read
                        Dim Valore As String
                        Valore = campodb(myPOSTreader4.Item("Stanza"))

                        
                        If Valore = "" Then
                            Valore = "NON INDICATO"
                        End If

                        Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & campodb(myPOSTreader.Item("VILLA")) & "&REPARTO=" & campodb(myPOSTreader1.Item("REPARTO")) & "&PIANO=" & Request.Item("PIANO") & "&STANZA=" & Valore & """><img src=""images/arrow.gif"" /></a>" & Valore & "(Stanza)<br>"

                        Dim Condizione As String

                        Dim cmd5 As New OleDbCommand()
                        If ValoreP = "NON INDICATO" Then
                            Condizione = "(Piano Is Null Or Piano ='') "
                        Else
                            Condizione = " Piano = '" & campodb(myPOSTreader3.Item("PIANO")) & "'"
                        End If
                        If Valore = "NON INDICATO" Then
                            Condizione = Condizione & " And (Stanza Is Null Or Stanza ='') "
                        Else
                            Condizione = Condizione & " And Stanza = '" & campodb(myPOSTreader4.Item("Stanza")) & "'"
                        End If


                        cmd5.CommandText = ("select Letto from Stanze  Where Villa = '" & campodb(myPOSTreader.Item("VILLA")) & "' And REPARTO = '" & campodb(myPOSTreader1.Item("REPARTO")) & "' And " & Condizione & " Group by Letto ")
                        cmd5.Connection = cn
                        Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
                        Do While myPOSTreader5.Read

                            Valore = campodb(myPOSTreader5.Item("Letto"))

                            If Valore = "" Then
                                Inserire = False
                            End If

                            If EstraLettoOccupato(campodb(myPOSTreader.Item("VILLA")), campodb(myPOSTreader1.Item("REPARTO")), campodb(myPOSTreader3.Item("PIANO")), campodb(myPOSTreader4.Item("Stanza")), campodb(myPOSTreader5.Item("Letto"))) Then
                                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=""../images/redarrow.png"" />" & Valore & "(Letto)<br>"
                                Call EstraOccupante(campodb(myPOSTreader.Item("VILLA")), campodb(myPOSTreader1.Item("REPARTO")), campodb(myPOSTreader3.Item("PIANO")), campodb(myPOSTreader4.Item("Stanza")), campodb(myPOSTreader5.Item("Letto")))
                            Else
                                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=""../images/bluearrow.png"" />" & Valore & "(Letto)<br>"
                            End If
                        Loop
                    Loop

                Loop
            Loop
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Private Sub Reparto()
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select Reparto from Stanze  Where Villa = '" & Request.Item("VILLA") & "' Group by Reparto ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Request.Item("REPARTO") = campodb(myPOSTreader.Item("REPARTO")) Then
                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "REP"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
                XVilla.Leggi(Session("DC_OSPITIACCESSORI"))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & campodb(myPOSTreader.Item("REPARTO")) & """><img src=""images/arrow.gif"" /></a>" & XVilla.Descrizione & "<br>"

                Call Piano()
            Else
                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "REP"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
                XVilla.Leggi(Session("DC_OSPITIACCESSORI"))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & campodb(myPOSTreader.Item("REPARTO")) & """><img src=""images/arrow.gif"" /></a>" & XVilla.Descrizione & "<br>"
            End If

        Loop

        cn.Close()

    End Sub

    Private Sub Piano()
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select Piano from Stanze  Where Villa = '" & Request.Item("VILLA") & "' And REPARTO = '" & Request.Item("REPARTO") & "' Group by Piano ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Valore As String
            Dim Inserire As Boolean = True
            Valore = campodb(myPOSTreader.Item("PIANO"))

            If Valore = "" And Vuoto = True Then
                Inserire = False
            End If
            If Valore = "" Then
                Valore = "NON INDICATO"
                Vuoto = True
            End If
            If Inserire Then
                If Request.Item("PIANO") = Valore Then
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & Request.Item("REPARTO") & "&PIANO=" & Valore & """><img src=""images/arrow.gif"" /></a>" & Valore & "(Piano)<br>"
                    Call Stanza()
                Else
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & Request.Item("REPARTO") & "&PIANO=" & Valore & """><img src=""images/arrow.gif"" /></a>" & Valore & "(Piano)<br>"
                End If
            End If
        Loop

        cn.Close()

    End Sub

    Private Sub Stanza()
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        If Request.Item("PIANO") = "NON INDICATO" Then
            cmd.CommandText = ("select Stanza from Stanze  Where Villa = '" & Request.Item("VILLA") & "' And REPARTO = '" & Request.Item("REPARTO") & "' And (Piano Is Null Or Piano ='') Group by Stanza ")
        Else
            cmd.CommandText = ("select Stanza from Stanze  Where Villa = '" & Request.Item("VILLA") & "' And REPARTO = '" & Request.Item("REPARTO") & "' And Piano = '" & Request.Item("PIANO") & "' Group by Stanza ")
        End If
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Valore As String
            Dim Inserire As Boolean = True
            Valore = campodb(myPOSTreader.Item("Stanza"))

            If Valore = "" And Vuoto = True Then
                Inserire = False
            End If
            If Valore = "" Then
                Valore = "NON INDICATO"
                Vuoto = True
            End If
            If Inserire Then
                If Request.Item("STANZA") = Valore Then
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & Request.Item("REPARTO") & "&PIANO=" & Request.Item("PIANO") & "&STANZA=" & Valore & """><img src=""images/arrow.gif"" /></a>" & Valore & "(Stanza)<br>"
                    Call Letto()
                Else
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""VsStanze.aspx?VILLA=" & Request.Item("VILLA") & "&REPARTO=" & Request.Item("REPARTO") & "&PIANO=" & Request.Item("PIANO") & "&STANZA=" & Valore & """><img src=""images/arrow.gif"" /></a>" & Valore & "(Stanza)<br>"
                End If
            End If
        Loop

        cn.Close()
    End Sub


    Private Sub Letto()
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()
        Dim Condizione As String

        Dim cmd As New OleDbCommand()
        If Request.Item("PIANO") = "NON INDICATO" Then
            Condizione = "(Piano Is Null Or Piano ='') "
        Else
            Condizione = " Piano = '" & Request.Item("PIANO") & "'"
        End If
        If Request.Item("STANZA") = "NON INDICATO" Then
            Condizione = Condizione & " And (Stanza Is Null Or Stanza ='') "
        Else
            Condizione = Condizione & " And Stanza = '" & Request.Item("STANZA") & "'"
        End If


        cmd.CommandText = ("select Letto from Stanze  Where Villa = '" & Request.Item("VILLA") & "' And REPARTO = '" & Request.Item("REPARTO") & "' And " & Condizione & " Group by Letto ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Valore As String
            Dim Inserire As Boolean = True
            Valore = campodb(myPOSTreader.Item("Letto"))

            If Valore = "" And Vuoto = True Then
                Inserire = False
            End If
            If Valore = "" Then
                Valore = "NON INDICATO"
                Vuoto = True
            End If
            If Inserire Then
                If LettoOccupato(campodb(myPOSTreader.Item("Letto"))) Then
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=""../images/redarrow.png"" />" & Valore & "(Letto)<br>"
                    Call Occupante(campodb(myPOSTreader.Item("Letto")))
                Else
                    Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=""../images/bluearrow.png"" />" & Valore & "(Letto)<br>"
                End If
            End If
        Loop

        cn.Close()
    End Sub



    Private Sub Occupante(ByVal Letto As String)
        Dim Villa As String
        Dim Reparto As String
        Dim Piano As String
        Dim Stanza As String
        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0



        Villa = Request.Item("VILLA")
        Reparto = Request.Item("REPARTO")
        Piano = Request.Item("PIANO")
        Stanza = Request.Item("STANZA")

        If Piano = "NON INDICATO" And Stanza = "NON INDICATO" Then
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & Piano & "'" & _
                    " AND STANZA = '" & Stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()

        Dim Sw_PrimaVolta As Boolean = False

        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                Dim Ospite As New ClsOspite
                Ospite.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=""blue"">" & Ospite.CodiceOspite & " " & Ospite.Nome & " " & Ospite.DataNascita & "</font><br>"
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Private Function LettoOccupato(ByVal Letto As String) As Boolean
        Dim Villa As String
        Dim Reparto As String
        Dim Piano As String
        Dim Stanza As String
        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0

        LettoOccupato = False

        Villa = Request.Item("VILLA")
        Reparto = Request.Item("REPARTO")
        Piano = Request.Item("PIANO")
        Stanza = Request.Item("STANZA")

        If Piano = "NON INDICATO" And Stanza = "NON INDICATO" Then
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & Piano & "'" & _
                    " AND STANZA = '" & Stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                LettoOccupato = True
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function


    Private Sub EstraOccupante(ByVal Villa As String, ByVal Reparto As String, ByVal piano As String, ByVal stanza As String, ByVal Letto As String)
        
        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0



        
        If (piano = "NON INDICATO" Or piano = "") And (stanza = "NON INDICATO" Or stanza = "") Then
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & piano & "'" & _
                    " AND STANZA = '" & stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()

        Dim Sw_PrimaVolta As Boolean = False

        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                Dim Ospite As New ClsOspite
                Ospite.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color=""blue"">" & Ospite.CodiceOspite & " " & Ospite.Nome & " " & Ospite.DataNascita & "</font><br>"
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Private Function EstraLettoOccupato(ByVal Villa As String, ByVal Reparto As String, ByVal piano As String, ByVal stanza As String, ByVal Letto As String) As Boolean
        
        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0

        EstraLettoOccupato = False

        

        If (piano = "NON INDICATO" Or piano = "") And (stanza = "NON INDICATO" Or stanza = "") Then
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & piano & "'" & _
                    " AND STANZA = '" & stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                EstraLettoOccupato = True
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


End Class
