﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_GiornoDomiciliare
    Inherits System.Web.UI.Page




    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Ora')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99:99"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub OspitiWeb_GiornoDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim cn As OleDbConnection
        Dim Numero As Integer = 0


        Dim Ktipo As New Cls_TipoDomiciliare


        Ktipo.UpDateDropBox(Session("DC_OSPITE"), DD_Tipologia)
        Ktipo.UpDateDropBox(Session("DC_OSPITE"), DD_Tipologia_2)
        Ktipo.UpDateDropBox(Session("DC_OSPITE"), DD_Tipologia_3)


        Dim KOperatore As New Cls_Operatore


        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore1_1)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore2_1)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore3_1)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore4_1)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore5_1)

        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore1_2)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore2_2)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore3_2)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore4_2)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore5_2)

        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore1_3)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore2_3)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore3_3)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore4_3)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore5_3)




        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & Request.Item("CENTROSERVIZIO") & "' And CodiceOspite = " & Request.Item("CODICEOSPITE") & " And Data  = ? Order by ID")
        cmd.Parameters.AddWithValue("@DATA", DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")))
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Numero = Numero + 1
            If Numero = 1 Then
                Lbl_Id1.Text = Val(campodb(myPOSTreader.Item("id")))
                Txt_DataRegistrazione.Text = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
                Txt_Descrizione.Text = campodb(myPOSTreader.Item("DESCRIZIONE"))
                Txt_OraInizio.Text = Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm")
                Txt_OraFine.Text = Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm")
                Txt_Operatore1.Text = campodb(myPOSTreader.Item("Operatore"))
                DD_Tipologia.SelectedValue = campodb(myPOSTreader.Item("Tipologia"))

                Dim Oper As New Cls_MovimentiDomiciliare


                Oper.ID = Val(campodb(myPOSTreader.Item("id")))
                Oper.leggi(Session("DC_OSPITE"))
                If Not IsNothing(Oper.Righe(0)) Then
                    DD_Operatore1_1.SelectedValue = Oper.Righe(0).CodiceOperatore
                End If

                If Not IsNothing(Oper.Righe(1)) Then
                    DD_Operatore2_1.SelectedValue = Oper.Righe(1).CodiceOperatore
                    DD_Operatore2_1.Visible = True                    
                End If
                If Not IsNothing(Oper.Righe(2)) Then
                    DD_Operatore3_1.SelectedValue = Oper.Righe(2).CodiceOperatore
                    DD_Operatore3_1.Visible = True                    
                End If
                If Not IsNothing(Oper.Righe(3)) Then
                    DD_Operatore4_1.SelectedValue = Oper.Righe(3).CodiceOperatore
                    DD_Operatore4_1.Visible = True                    
                End If
                If Not IsNothing(Oper.Righe(4)) Then
                    DD_Operatore5_1.SelectedValue = Oper.Righe(4).CodiceOperatore
                    DD_Operatore5_1.Visible = True
                End If
            End If
            If Numero = 2 Then
                Lbl_Id2.Text = Val(campodb(myPOSTreader.Item("id")))
                Txt_DataRegistrazione_2.Text = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
                Txt_Descrizione_2.Text = campodb(myPOSTreader.Item("DESCRIZIONE"))
                Txt_OraInizio_2.Text = Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm")
                Txt_OraFine_2.Text = Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm")
                Txt_Operatore2.Text = campodb(myPOSTreader.Item("Operatore"))
                DD_Tipologia_2.SelectedValue = campodb(myPOSTreader.Item("Tipologia"))


                Dim Oper As New Cls_MovimentiDomiciliare


                Oper.ID = Val(campodb(myPOSTreader.Item("id")))
                Oper.leggi(Session("DC_OSPITE"))
                If Not IsNothing(Oper.Righe(0)) Then
                    DD_Operatore1_2.SelectedValue = Oper.Righe(0).CodiceOperatore
                End If

                If Not IsNothing(Oper.Righe(1)) Then
                    DD_Operatore2_2.SelectedValue = Oper.Righe(1).CodiceOperatore
                    DD_Operatore2_2.Visible = True                    
                End If
                If Not IsNothing(Oper.Righe(2)) Then
                    DD_Operatore3_2.SelectedValue = Oper.Righe(2).CodiceOperatore
                    DD_Operatore3_2.Visible = True

                End If
                If Not IsNothing(Oper.Righe(3)) Then
                    DD_Operatore4_2.SelectedValue = Oper.Righe(3).CodiceOperatore
                    DD_Operatore4_2.Visible = True

                End If
                If Not IsNothing(Oper.Righe(4)) Then
                    DD_Operatore5_2.SelectedValue = Oper.Righe(4).CodiceOperatore

                End If
            End If
            If Numero = 3 Then
                Lbl_Id3.Text = Val(campodb(myPOSTreader.Item("id")))
                Txt_DataRegistrazione_3.Text = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
                Txt_Descrizione_3.Text = campodb(myPOSTreader.Item("DESCRIZIONE"))
                Txt_OraInizio_3.Text = Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm")
                Txt_OraFine_3.Text = Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm")
                Txt_Operatore3.Text = campodb(myPOSTreader.Item("Operatore"))
                DD_Tipologia_3.SelectedValue = campodb(myPOSTreader.Item("Tipologia"))

                Dim Oper As New Cls_MovimentiDomiciliare


                Oper.ID = Val(campodb(myPOSTreader.Item("id")))
                Oper.leggi(Session("DC_OSPITE"))
                If Not IsNothing(Oper.Righe(0)) Then
                    DD_Operatore1_3.SelectedValue = Oper.Righe(0).CodiceOperatore
                End If

                If Not IsNothing(Oper.Righe(1)) Then
                    DD_Operatore2_3.SelectedValue = Oper.Righe(1).CodiceOperatore
                    DD_Operatore2_3.Visible = True

                End If
                If Not IsNothing(Oper.Righe(2)) Then
                    DD_Operatore3_3.SelectedValue = Oper.Righe(2).CodiceOperatore
                    DD_Operatore3_3.Visible = True

                End If
                If Not IsNothing(Oper.Righe(3)) Then
                    DD_Operatore4_3.SelectedValue = Oper.Righe(3).CodiceOperatore
                    DD_Operatore4_3.Visible = True

                End If
                If Not IsNothing(Oper.Righe(4)) Then
                    DD_Operatore5_3.SelectedValue = Oper.Righe(4).CodiceOperatore
                    DD_Operatore5_3.Visible = True
                End If
            End If

        Loop
        myPOSTreader.Close()

        If Numero = 0 Then
            Txt_DataRegistrazione.Text = Format(DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")), "dd/MM/yyyy")
        End If
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_OraInizio.Text <> "" Then
            Dim k As New Cls_MovimentiDomiciliare

            'cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & Request.Item("CENTROSERVIZIO") & "' And CodiceOspite = " & Request.Item("CODICEOSPITE") & " And Data  = ? Order by ID")
            'cmd.Parameters.AddWithValue("@DATA", DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")))

            Dim OraFine As String
            Dim MinutiFine As String
            Dim OraInizio As String
            Dim MinutiInizio As String

            If Not IsDate(Txt_DataRegistrazione.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Not IsDate(Txt_DataRegistrazione.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraInizio.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If
            If Txt_OraInizio.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraFine = Mid(Txt_OraFine.Text, 1, 2)
            If Val(OraFine) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If

            MinutiFine = Mid(Txt_OraFine.Text, 4, 2)
            If Val(MinutiFine) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraInizio = Mid(Txt_OraInizio.Text, 1, 2)
            If Val(OraInizio) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            MinutiInizio = Mid(Txt_OraInizio.Text, 4, 2)
            If Val(MinutiInizio) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If


            k.CodiceOspite = Val(Request.Item("CODICEOSPITE"))
            k.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
            k.Data = DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO"))

            If Val(Lbl_Id1.Text) > 0 Then
                k.ID = Val(Lbl_Id1.Text)
                k.leggi(Session("DC_OSPITE"))
            End If

            k.Data = Txt_DataRegistrazione.Text
            k.Descrizione = Txt_Descrizione.Text



            k.OraFine = TimeSerial(OraFine, MinutiFine, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.OraInizio = TimeSerial(OraInizio, MinutiInizio, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.Operatore = Txt_Operatore1.Text
            k.Tipologia = DD_Tipologia.SelectedValue


            k.Utente = Session("UTENTE")


            Dim i As Integer = 0
            For i = 0 To 40
                k.Righe(i) = Nothing
            Next
            k.Righe(0) = New Cls_MovimentiDomiciliare_Operatori
            k.Righe(0).CodiceOperatore = Val(DD_Operatore1_1.SelectedValue)
            k.Righe(0).IdMovimento = k.ID

            If DD_Operatore2_1.SelectedValue <> "" Then
                k.Righe(1) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(1).CodiceOperatore = Val(DD_Operatore2_1.SelectedValue)
                k.Righe(1).IdMovimento = k.ID
            End If

            If DD_Operatore3_1.SelectedValue <> "" Then
                k.Righe(2) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(2).CodiceOperatore = Val(DD_Operatore3_1.SelectedValue)
                k.Righe(2).IdMovimento = k.ID
            End If

            If DD_Operatore4_1.SelectedValue <> "" Then
                k.Righe(3) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(3).CodiceOperatore = Val(DD_Operatore4_1.SelectedValue)
                k.Righe(3).IdMovimento = k.ID
            End If

            If DD_Operatore5_1.SelectedValue <> "" Then
                k.Righe(4) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(4).CodiceOperatore = Val(DD_Operatore5_1.SelectedValue)
                k.Righe(4).IdMovimento = k.ID
            End If

            k.AggiornaDB(Session("DC_OSPITE"))


        End If

        If Txt_OraInizio_2.Text <> "" Then
            Dim k As New Cls_MovimentiDomiciliare

            'cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & Request.Item("CENTROSERVIZIO") & "' And CodiceOspite = " & Request.Item("CODICEOSPITE") & " And Data  = ? Order by ID")
            'cmd.Parameters.AddWithValue("@DATA", DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")))

            Dim OraFine As String
            Dim MinutiFine As String
            Dim OraInizio As String
            Dim MinutiInizio As String

            If Not IsDate(Txt_DataRegistrazione_2.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Not IsDate(Txt_DataRegistrazione_2.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraInizio_2.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine_2.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If
            If Txt_OraInizio_2.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine_2.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraFine = Mid(Txt_OraFine_2.Text, 1, 2)
            If Val(OraFine) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If

            MinutiFine = Mid(Txt_OraFine_2.Text, 4, 2)
            If Val(MinutiFine) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraInizio = Mid(Txt_OraInizio_2.Text, 1, 2)
            If Val(OraInizio) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            MinutiInizio = Mid(Txt_OraInizio_2.Text, 4, 2)
            If Val(MinutiInizio) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If


            k.CodiceOspite = Val(Request.Item("CODICEOSPITE"))
            k.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
            k.Data = DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO"))

            If Val(Lbl_Id2.Text) > 0 Then
                k.ID = Val(Lbl_Id2.Text)
                k.leggi(Session("DC_OSPITE"))
            End If

            k.Data = Txt_DataRegistrazione_2.Text
            k.Descrizione = Txt_Descrizione_2.Text



            k.OraFine = TimeSerial(OraFine, MinutiFine, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.OraInizio = TimeSerial(OraInizio, MinutiInizio, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.Operatore = Txt_Operatore2.Text
            k.Tipologia = DD_Tipologia_2.SelectedValue


            k.Utente = Session("UTENTE")


            Dim i As Integer = 0
            For i = 0 To 40
                k.Righe(i) = Nothing
            Next
            k.Righe(0) = New Cls_MovimentiDomiciliare_Operatori
            k.Righe(0).CodiceOperatore = Val(DD_Operatore1_2.SelectedValue)
            k.Righe(0).IdMovimento = k.ID

            If DD_Operatore2_2.SelectedValue <> "" Then
                k.Righe(1) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(1).CodiceOperatore = Val(DD_Operatore2_2.SelectedValue)
                k.Righe(1).IdMovimento = k.ID
            End If

            If DD_Operatore3_2.SelectedValue <> "" Then
                k.Righe(2) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(2).CodiceOperatore = Val(DD_Operatore3_2.SelectedValue)
                k.Righe(2).IdMovimento = k.ID
            End If

            If DD_Operatore4_2.SelectedValue <> "" Then
                k.Righe(3) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(3).CodiceOperatore = Val(DD_Operatore4_2.SelectedValue)
                k.Righe(3).IdMovimento = k.ID
            End If

            If DD_Operatore5_2.SelectedValue <> "" Then
                k.Righe(4) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(4).CodiceOperatore = Val(DD_Operatore5_2.SelectedValue)
                k.Righe(4).IdMovimento = k.ID
            End If

            k.AggiornaDB(Session("DC_OSPITE"))


        End If


        If Txt_OraInizio_3.Text <> "" Then
            Dim k As New Cls_MovimentiDomiciliare

            'cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & Request.Item("CENTROSERVIZIO") & "' And CodiceOspite = " & Request.Item("CODICEOSPITE") & " And Data  = ? Order by ID")
            'cmd.Parameters.AddWithValue("@DATA", DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")))

            Dim OraFine As String
            Dim MinutiFine As String
            Dim OraInizio As String
            Dim MinutiInizio As String

            If Not IsDate(Txt_DataRegistrazione_3.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Not IsDate(Txt_DataRegistrazione_3.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraInizio_3.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine_3.Text = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If
            If Txt_OraInizio_3.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            If Txt_OraFine_3.Text.Length < 5 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraFine = Mid(Txt_OraFine_3.Text, 1, 2)
            If Val(OraFine) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If

            MinutiFine = Mid(Txt_OraFine_3.Text, 4, 2)
            If Val(MinutiFine) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
                Exit Sub
            End If


            OraInizio = Mid(Txt_OraInizio_3.Text, 1, 2)
            If Val(OraInizio) > 24 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If

            MinutiInizio = Mid(Txt_OraInizio_3.Text, 4, 2)
            If Val(MinutiInizio) >= 60 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
                Exit Sub
            End If


            k.CodiceOspite = Val(Request.Item("CODICEOSPITE"))
            k.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
            k.Data = DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO"))

            If Val(Lbl_Id3.Text) > 0 Then
                k.ID = Val(Lbl_Id3.Text)
                k.leggi(Session("DC_OSPITE"))
            End If

            k.Data = Txt_DataRegistrazione_3.Text
            k.Descrizione = Txt_Descrizione_3.Text



            k.OraFine = TimeSerial(OraFine, MinutiFine, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.OraInizio = TimeSerial(OraInizio, MinutiInizio, 0).AddYears(1899).AddMonths(12).AddDays(30)
            k.Operatore = Txt_Operatore3.Text
            k.Tipologia = DD_Tipologia_3.SelectedValue


            k.Utente = Session("UTENTE")


            Dim i As Integer = 0
            For i = 0 To 40
                k.Righe(i) = Nothing
            Next
            k.Righe(0) = New Cls_MovimentiDomiciliare_Operatori
            k.Righe(0).CodiceOperatore = Val(DD_Operatore1_3.SelectedValue)
            k.Righe(0).IdMovimento = k.ID

            If DD_Operatore2_3.SelectedValue <> "" Then
                k.Righe(1) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(1).CodiceOperatore = Val(DD_Operatore2_3.SelectedValue)
                k.Righe(1).IdMovimento = k.ID
            End If

            If DD_Operatore3_3.SelectedValue <> "" Then
                k.Righe(2) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(2).CodiceOperatore = Val(DD_Operatore3_3.SelectedValue)
                k.Righe(2).IdMovimento = k.ID
            End If

            If DD_Operatore4_3.SelectedValue <> "" Then
                k.Righe(3) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(3).CodiceOperatore = Val(DD_Operatore4_3.SelectedValue)
                k.Righe(3).IdMovimento = k.ID
            End If

            If DD_Operatore5_3.SelectedValue <> "" Then
                k.Righe(4) = New Cls_MovimentiDomiciliare_Operatori
                k.Righe(4).CodiceOperatore = Val(DD_Operatore5_3.SelectedValue)
                k.Righe(4).IdMovimento = k.ID
            End If


            k.AggiornaDB(Session("DC_OSPITE"))


        End If

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "OK", "alert('Modifica Effettuata');", True)

    End Sub
End Class

