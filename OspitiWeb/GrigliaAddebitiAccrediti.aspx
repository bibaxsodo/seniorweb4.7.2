﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="true" Inherits="OspitiWeb_GrigliaAddebitiAccrediti" CodeFile="GrigliaAddebitiAccrediti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Griglia Addebiti/Accrediti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        $(document).click(function () {

            // This is the triggered action name
            switch ($(this).attr("data-action")) {

                // A case for each action. Your actions here    
                case "Anagrafica": break;
                case "Addebiti Accrediti": break;
                case "Movimenti": break;
                case "Diurno": break;
                case "Denaro": break;
                case "Documenti": break;
                case "Estratto Conto": break;
            }

            // Hide it AFTER the action was triggered
            $("#rightmenu").hide(100);
        });

        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }
        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');

            $("#pippo").css('visibility', 'visible');
            $("#Txt_DataAcco").mask("99/99/9999");
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 240px;
            position: absolute;
            top: 290px;
            left: 40%;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Elenco Addebiti/Accrediti</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right;">
                        <asp:ImageButton ID="Btn_Excel" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr style="height: 32px;">
                                <td align="left" style="width: 100px;">Data Dal :
                                </td>
                                <td align="left" style="width: 200px;">
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Text="" Width="90px"></asp:TextBox>
                                </td>
                                <td align="left" style="width: 100px;">Data Al :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Text="" Width="90px"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:RadioButton ID="RB_Tutti" runat="server" Text="Tutti" GroupName="Filtro" Checked="true" />
                                    <asp:RadioButton ID="RB_Ospite" runat="server" Text="Ospite" GroupName="Filtro" />
                                    <asp:RadioButton ID="RB_Perente" runat="server" Text="Parente" GroupName="Filtro" />
                                    <asp:RadioButton ID="RB_Comune" runat="server" Text="Comune" GroupName="Filtro" />
                                    <asp:RadioButton ID="RB_Jolly" runat="server" Text="Jolly" GroupName="Filtro" />
                                    <asp:RadioButton ID="RB_Regione" runat="server" Text="Regione" GroupName="Filtro" />
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <div id="MENUDIV"></div>
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="Btn_Nuovo" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi Addebiti/Accrediti" />
                        </div>
                        <asp:GridView ID="Grd_AddebitiAccrediti" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                            PageSize="50">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%# Container.DataItemIndex  %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="Id" />
                                <asp:BoundField DataField="Data" HeaderText="Data" />
                                <asp:BoundField DataField="TIPOMOV" HeaderText="Tipo Movimento" />
                                <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                                <asp:BoundField DataField="IMPORTO" HeaderText="Importo" />
                                <asp:BoundField DataField="RIFERIMENTO" HeaderText="Riferimento" />
                                <asp:BoundField DataField="PARENTE" HeaderText="Parente" />
                                <asp:BoundField DataField="REGIONE" HeaderText="Regione" />
                                <asp:BoundField DataField="COMUNE" HeaderText="Comune" />
                                <asp:BoundField DataField="PERIODO" HeaderText="Periodo" />
                                <asp:BoundField DataField="ELABORATO" HeaderText="Elaborato" />
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="QrCode" CommandName="QrCode" runat="Server" ImageUrl="~/images/sociale.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%# Container.DataItemIndex  %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                        </asp:GridView>

                        <div id="blur" style="visibility: hidden;">&nbsp;</div>
                        <div id="pippo" style="visibility: hidden;" class="wait">
                            <asp:Image runat="server" ID="ImgQrCode" Width="100%" />
                            <button id="btn" onclick="Chiudi();" style="width: 100%; height: 24px;">Chiudi</button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
