﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class OspitiWeb_ImportAddebitiDaFileExcel
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione Formalmente Errata');", True)
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1900 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno competenza errato');", True)
            Exit Sub
        End If

        If RB_FuoriRetta.Checked = False And Rb_Rendiconto.Checked = False And RB_Retta.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica tipo ripartizione');", True)
            Exit Sub
        End If

        Tabella = ViewState("Appoggio")

        Dim Indice As Integer

        For Indice = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportoOspite.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)

            If Chekc.Checked = True And Val(Grd_ImportoOspite.Rows(Indice).Cells(2).Text) > 0 Then
                Dim CodiceOspite As Integer
                Dim TipoAddebito As String
                Dim Importo As Double




                CodiceOspite = Grd_ImportoOspite.Rows(Indice).Cells(2).Text
                TipoAddebito = Grd_ImportoOspite.Rows(Indice).Cells(5).Text
                Importo = Grd_ImportoOspite.Rows(Indice).Cells(7).Text


                Dim MyAdddebito As New Cls_AddebitiAccrediti

                Dim DD_Cserv As DropDownList = DirectCast(Grd_ImportoOspite.Rows(Indice).FindControl("DD_Cserv"), DropDownList)

                MyAdddebito.CENTROSERVIZIO = DD_Cserv.SelectedValue
                MyAdddebito.CodiceOspite = CodiceOspite

                Dim MiOsp As New Cls_Modalita
                Dim CodiceParente As Integer = 0
                Dim TipoMod As String

                TipoMod = "O"

                Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                KCs.CentroServizio = DD_Cserv.SelectedValue
                KCs.CodiceOspite = CodiceOspite
                KCs.CodiceParente = 0
                KCs.Leggi(Session("DC_OSPITE"))
                If KCs.TipoAddebito1 = TipoAddebito Then
                    TipoMod = "C"                    
                End If

                If KCs.TipoAddebito2 = TipoAddebito Then
                    TipoMod = "C"
                End If
                If KCs.TipoAddebito3 = TipoAddebito Then
                    TipoMod = "C"
                End If
                If KCs.TipoAddebito4 = TipoAddebito Then
                    TipoMod = "C"
                End If

                If TipoMod = "C" Then
                    Dim m As New Cls_ImportoComune

                    m.CODICEOSPITE = CodiceOspite
                    m.CENTROSERVIZIO = DD_Cserv.SelectedValue
                    m.UltimaData(Session("DC_OSPITE"), m.CODICEOSPITE, m.CENTROSERVIZIO)

                    MyAdddebito.PROVINCIA = m.PROV
                    MyAdddebito.COMUNE = m.COMUNE
                End If

                MiOsp.CentroServizio = MyAdddebito.CENTROSERVIZIO
                MiOsp.CodiceOspite = MyAdddebito.CodiceOspite
                MiOsp.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio)
                If MiOsp.MODALITA = "P" Then
                    TipoMod = "P"
                    CodiceParente = 1
                End If

                Dim MyPar As New Cls_ImportoParente

                MyPar.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 1)
                If MyPar.IMPORTO > 0 Then
                    TipoMod = "P"
                    CodiceParente = 1
                End If

                Dim MyPar1 As New Cls_ImportoParente

                MyPar1.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 2)
                If MyPar1.IMPORTO > 0 Then
                    TipoMod = "P"
                    CodiceParente = 2
                End If

                Dim MyPar2 As New Cls_ImportoParente

                MyPar2.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 3)
                If MyPar2.IMPORTO > 0 Then
                    TipoMod = "P"
                    CodiceParente = 3
                End If

                MyAdddebito.RIFERIMENTO = TipoMod
                MyAdddebito.PARENTE = CodiceParente

                MyAdddebito.ANNOCOMPETENZA = Val(Txt_Anno.Text)
                MyAdddebito.MESECOMPETENZA = Dd_Mese.SelectedValue
                MyAdddebito.TipoMov = "AD"
                MyAdddebito.CodiceIva = TipoAddebito
                MyAdddebito.Data = Txt_DataRegistrazione.Text
                MyAdddebito.IMPORTO = Importo
                MyAdddebito.Descrizione = campodb(Tabella.Rows(Indice).Item(9))
                MyAdddebito.AggiornaDB(Session("DC_OSPITE"))

            End If
        Next

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Importazione Avvenuta');", True)
        Grd_ImportoOspite.Visible = False

    End Sub


    Protected Sub OspitiWeb_ImportAddebitiDaFileExcel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Txt_NonTrovati.Visible = False

        Dim x As New Cls_Addebito

        x.UpDateDropBox(Session("DC_OSPITE"), dd_TipoAddebito)

        Dim Par As New Cls_Parametri

        Par.LeggiParametri(Session("DC_OSPITE"))

        Dd_Mese.SelectedValue = Par.MeseFatturazione
        Txt_Anno.Text = Par.AnnoFatturazione

        RB_FuoriRetta.Checked = True
    End Sub

    Private Function ImportFileXLS(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim nomefileexcel As String

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
                Exit Function
            End If
        Else
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File excel non trovata');", True)
                Exit Function
            End If
        End If


        Tabella.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(String))
        Tabella.Columns.Add("CentroServizio", GetType(String))

        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("CognomeNome", GetType(String))
        Tabella.Columns.Add("TipoAddebito", GetType(String))
        Tabella.Columns.Add("Decodifica", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Segnalazione", GetType(String))
        Tabella.Columns.Add("Ripartizione", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        'cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then

            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")
        Else
            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        End If


        cn.Open()
        ''Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}\\;Extended Properties='Text;HDR=Yes;FMT=CSVDelimited(;)
        'Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=" & Txt_Rsl.text & ";"

        Dim cmd As New OdbcCommand()

        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")

            'CsvImport" & Appoggio & ".csv"
        Else
            cmd.CommandText = ("select * from [FOGLIO$]")
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim CodiceFiscale As String = ""
            Dim Importo As String = 0
            Dim TipoAddebito As String = ""
            Dim Cognome As String = ""
            Dim Nome As String = ""
            Dim Descrizione As String = ""

            If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                Dim Vettore(100) As String
                Dim CampoInput As String

                CampoInput = myPOSTreader.Item(0)
                Try
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                Catch ex As Exception

                End Try

                Vettore = SplitWords(CampoInput)
                Try
                    CodiceFiscale = Vettore(0)
                    Cognome = Vettore(1)
                    Nome = Vettore(2)
                    TipoAddebito = Vettore(3)
                    Importo = Vettore(4)
                    Descrizione = Vettore(5)
                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                    Exit Function
                End Try
            Else

                Try
                    Cognome = campodb(myPOSTreader.Item("Cognome")).Trim
                    Nome = campodb(myPOSTreader.Item("Nome")).Trim

                    CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))

                    CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
                    Importo = campodb(myPOSTreader.Item("Importo"))
                    TipoAddebito = campodb(myPOSTreader.Item("TipoAddebito"))

                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                    Exit Function

                End Try
                Try
                    Descrizione = campodb(myPOSTreader.Item("Descrizione"))

                Catch ex As Exception

                End Try
            End If

            If Importo = "" Then
                Importo = 0
            End If

            If CDbl(Importo) > 0 Then
                Dim CercaOspite As New ClsOspite

                CercaOspite.CODICEFISCALE = CodiceFiscale
                If CodiceFiscale = "" Then
                    If Trim(Cognome) <> "" Then
                        CercaOspite.Nome = Cognome.Replace(" ", "%") & "%" & Nome.Replace(" ", "%")
                        CercaOspite.LeggiNome(Session("DC_OSPITE"))
                    End If
                Else
                    CercaOspite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CercaOspite.CODICEFISCALE)
                End If
                If CercaOspite.CodiceOspite = 0 Then
                    CercaOspite.Nome = Cognome & " " & Nome
                    CercaOspite.LeggiNome(Session("DC_OSPITE"))
                End If

                If Cognome = "GRIGOLON ROMA" Then
                    Cognome = "GRIGOLON ROMA"
                End If
                If CercaOspite.CodiceOspite > 0 Then

                    Dim MovLas As New Cls_Movimenti

                    MovLas.CodiceOspite = CercaOspite.CodiceOspite
                    MovLas.UltimaData(Session("DC_OSPITE"), CercaOspite.CodiceOspite)

                    Dim CercaTipoAddebito As New Cls_Addebito

                    If TipoAddebito = "" Or TipoAddebito = "&nbsp;" Then
                        CercaTipoAddebito.Codice = dd_TipoAddebito.SelectedValue
                    Else
                        CercaTipoAddebito.Codice = TipoAddebito
                    End If
                    CercaTipoAddebito.Leggi(Session("DC_OSPITE"), CercaTipoAddebito.Codice)
                    If Trim(CercaTipoAddebito.Descrizione) <> "" Then

                        Dim TipoMod As String
                        TipoMod = "O"

                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                        KCs.CentroServizio = MovLas.CENTROSERVIZIO
                        KCs.CodiceOspite = CercaOspite.CodiceOspite
                        KCs.CodiceParente = 0
                        KCs.Leggi(Session("DC_OSPITE"))
                        If KCs.TipoAddebito1 = TipoAddebito Then
                            TipoMod = "C"
                        End If

                        If KCs.TipoAddebito2 = TipoAddebito Then
                            TipoMod = "C"
                        End If
                        If KCs.TipoAddebito3 = TipoAddebito Then
                            TipoMod = "C"
                        End If
                        If KCs.TipoAddebito4 = TipoAddebito Then
                            TipoMod = "C"
                        End If

                        Dim MiOsp As New Cls_Modalita


                        MiOsp.CentroServizio = MovLas.CENTROSERVIZIO
                        MiOsp.CodiceOspite = CercaOspite.CodiceOspite
                        MiOsp.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio)
                        If MiOsp.MODALITA = "P" Then
                            TipoMod = "P"
                        End If

                        Dim MyPar As New Cls_ImportoParente

                        MyPar.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 1)
                        If MyPar.IMPORTO > 0 Then
                            TipoMod = "P"
                        End If

                        Dim MyPar1 As New Cls_ImportoParente

                        MyPar1.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 2)
                        If MyPar1.IMPORTO > 0 Then
                            TipoMod = "P"
                        End If

                        Dim MyPar2 As New Cls_ImportoParente

                        MyPar2.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 3)
                        If MyPar2.IMPORTO > 0 Then
                            TipoMod = "P"
                        End If

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = CercaOspite.CodiceOspite

                        myriga(1) = MovLas.CENTROSERVIZIO
                        myriga(2) = CercaOspite.CODICEFISCALE
                        myriga(3) = CercaOspite.Nome
                        myriga(4) = CercaTipoAddebito.Codice
                        myriga(5) = CercaTipoAddebito.Descrizione
                        myriga(6) = Importo
                        myriga(7) = "OK"
                        myriga(8) = TipoMod
                        If Descrizione = "" Then
                            myriga(9) = CercaTipoAddebito.Descrizione
                        Else
                            myriga(9) = Descrizione
                        End If

                        Tabella.Rows.Add(myriga)
                    Else
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = CercaOspite.CodiceOspite
                        myriga(1) = MovLas.CENTROSERVIZIO
                        myriga(2) = CodiceFiscale
                        myriga(3) = CercaOspite.Nome
                        myriga(4) = TipoAddebito
                        myriga(5) = ""
                        myriga(6) = Importo
                        myriga(7) = "Tipo Addebito Non Trovato"
                        myriga(9) = Descrizione
                        Tabella.Rows.Add(myriga)
                    End If
                Else
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                    Txt_NonTrovati.Text = Txt_NonTrovati.Text & Cognome & " " & Nome & vbNewLine
                    myriga(0) = ""
                    myriga(1) = 0
                    myriga(2) = CodiceFiscale
                    myriga(3) = Cognome & " " & Nome
                    myriga(4) = TipoAddebito
                    myriga(5) = ""
                    myriga(6) = Importo
                    myriga(7) = "Ospite Non Trovato"
                    myriga(9) = Descrizione
                    Tabella.Rows.Add(myriga)
                End If
            End If

        Loop
        cn.Close()

        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = False

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()
        Grd_ImportoOspite.Visible = True

        If Txt_NonTrovati.Text.Trim <> "" Then
            Txt_NonTrovati.Visible = False
        End If

        Dim Indice As Integer

        For Indice = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportoOspite.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
            If Grd_ImportoOspite.Rows(Indice).BackColor <> Drawing.Color.Red Then
                Chekc.Checked = True
            End If
        Next



    End Function
    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            Dim NomeFile As String

            NomeFile = ImportFileXLS(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Grd_ImportoOspite_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_ImportoOspite.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)


            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(1).ToString
            Dim Chekc As CheckBox = DirectCast(e.Row.FindControl("Chk_Selezionato"), CheckBox)


            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "Ospite Non Trovato" Then
                e.Row.BackColor = Drawing.Color.Red
                Chekc.Enabled = False
                Chekc.Checked = False
            End If

            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "Tipo Addebito Non Trovato" Then
                e.Row.BackColor = Drawing.Color.Red
                Chekc.Enabled = False
                Chekc.Checked = False
            End If
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Protected Sub Img_CSV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_CSV.Click

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione Formalmente Errata');", True)
            Exit Sub
        End If



        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim ContaOspiti As Integer = 0
        Dim CSV As String = ""
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        CSV = "CodiceFiscale;Cognome;Nome;TipoAddebito;Importo;Descrizione;" & vbNewLine
        Dim cmdCS As New OleDbCommand()


        cmdCS.CommandText = "Select * From TABELLACENTROSERVIZIO"
        cmdCS.Connection = cn
        Dim ReadCS As OleDbDataReader = cmdCS.ExecuteReader()

        Do While ReadCS.Read


            Dim cmd As New OleDbCommand()

            MySql = "Select * From AnagraficaComune Where (NonInUso = '' or NonInUso  is null ) And CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "') > 0 OR " & _
                                                            " (" & _
                                                            " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                            " And " & _
                                                            " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' And TipoMov = '05') <>  0) " & _
                                                            " )" & _
                                                            " ) order by Nome"


            cmd.CommandText = MySql
            Dim Txt_DataDalText As Date = Txt_DataRegistrazione.Text
            Dim Txt_DataAlText As Date = Txt_DataRegistrazione.Text

            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CSV = CSV & campodb(myPOSTreader.Item("CODICEFISCALE")) & ";" & campodb(myPOSTreader.Item("CognomeOspite")) & ";" & campodb(myPOSTreader.Item("NomeOspite")) & ";"""";0;"""";" & vbNewLine
            Loop
            myPOSTreader.Close()
        Loop

        cn.Close()

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("content-disposition", "attachment;filename=ImportAddebitiAccrediti.csv")
        Response.Write(CSV)
        Response.End()
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, ";")
    End Function

End Class
