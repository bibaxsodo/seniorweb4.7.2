﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO
Imports System.Security.Cryptography

Partial Class OspitiWeb_DocumentazioneLocal
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")

    Protected Sub EncryptFile(ByVal sender As Object, ByVal e As EventArgs)
        'Get the Input File Name and Extension.  
        Dim fileName As String = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName)
        Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)

        'Build the File Path for the original (input) and the encrypted (output) file.  
        Dim input As String = Convert.ToString(Server.MapPath("~/Files/") & fileName) & fileExtension
        Dim output As String = Convert.ToString((Server.MapPath("~/Files/") & fileName) + "_enc") & fileExtension

        'Save the Input File, Encrypt it and save the encrypted file in output path.  
        FileUpload1.SaveAs(input)
        Me.Encrypt(input, output)

        'Download the Encrypted File.  
        Response.ContentType = FileUpload1.PostedFile.ContentType
        Response.Clear()
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output))
        Response.WriteFile(output)
        Response.Flush()

        'Delete the original (input) and the encrypted (output) file.  
        File.Delete(input)
        File.Delete(output)

        Response.End()
    End Sub

    Protected Sub DecryptFile(ByVal sender As Object, ByVal e As EventArgs)
        'Get the Input File Name and Extension  
        Dim fileName As String = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName)
        Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)

        'Build the File Path for the original (input) and the decrypted (output) file  
        Dim input As String = Convert.ToString(Server.MapPath("~/Files/") & fileName) & fileExtension
        Dim output As String = Convert.ToString((Server.MapPath("~/Files/") & fileName) + "_dec") & fileExtension

        'Save the Input File, Decrypt it and save the decrypted file in output path.  
        FileUpload1.SaveAs(input)
        Me.Decrypt(input, output)

        'Download the Decrypted File.  
        Response.Clear()
        Response.ContentType = FileUpload1.PostedFile.ContentType
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output))
        Response.WriteFile(output)
        Response.Flush()

        'Delete the original (input) and the decrypted (output) file.  
        File.Delete(input)
        File.Delete(output)

        Response.End()
    End Sub

    Private Shared Function Assign(Of T)(ByRef source As T, ByVal value As T) As T
        source = value
        Return value
    End Function

    Private Sub Encrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(outputfilePath, FileMode.Create)
                Using cs As New CryptoStream(fs, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    Using fsInput As New FileStream(inputFilePath, FileMode.Open)
                        Dim data As Integer
                        While (Assign(data, fsInput.ReadByte())) <> -1
                            cs.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub Decrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(inputFilePath, FileMode.Open)
                Using cs As New CryptoStream(fs, encryptor.CreateDecryptor(), CryptoStreamMode.Read)
                    Using fsOutput As New FileStream(outputfilePath, FileMode.Create)
                        Dim data As Integer
                        While (Assign(data, cs.ReadByte())) <> -1
                            fsOutput.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub LeggiDirectory()
        Dim i As Long = 0

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("NomeFile", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))

        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")))
        End If


        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Dim NomeFile As String

            NomeFile = objItem.Name

            If NomeFile.IndexOf(".") >= 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = objItem.Name
                myriga(0) = objItem.Name
                MyTable.Rows.Add(myriga)
                i = i + 1
            End If
        Next

        If i = 0 Then
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            MyTable.Rows.Add(myriga)
        End If
        Session("Documentazione") = MyTable
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()
    End Sub



    Protected Sub OspitiWeb_DocumentazioneLocal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then
            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Call LeggiDirectory()
        Call EseguiJS()
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "



        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Dim Ks As New Cls_Documentazione

        MyTable = Session("Documentazione")

        If Not IsNothing(MyTable) Then
            Ks.CODICEOSPITE = Val(Session("CODICEOSPITE"))
            Ks.Scrivi(Session("DC_OSPITE"), MyTable)
        End If


        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

        Call EseguiJS()
    End Sub


    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call BindDocumentazione()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "DOWNLOAD" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale

            If e.CommandArgument.ToString.IndexOf("Crypt_") >= 0 Then
                Decrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & e.CommandArgument, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End If

            Try
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ClearHeaders()
                HttpContext.Current.Response.ClearContent()

                Response.Clear()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("Content-Disposition", _
                                    "attachment; filename=""" & "Copia_" & e.CommandArgument & """")
                Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))

                Response.Flush()

            Finally
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End Try
        End If
        If e.CommandName = "CANCELLAFILE" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale

            MyTable = Session("Documentazione")


            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & MyTable.Rows(e.CommandArgument).Item(0))
            Catch ex As Exception

            End Try


            MyTable.Rows.RemoveAt(e.CommandArgument)

            If MyTable.Rows.Count = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                MyTable.Rows.Add(myriga)
            End If

            Session("Documentazione") = MyTable

            Call BindDocumentazione()

        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtDescrizione.Text = MyDv(1).ToString
            End If
            Dim LblData As Label = DirectCast(e.Row.FindControl("LblData"), Label)
            If Not IsNothing(LblData) Then

            Else
                Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtData.Text = MyDv(2).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999"");"
                MyJs = MyJs & " $('#" & TxtData.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);  });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", MyJs, True)
            End If
        End If
    End Sub

    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call BindDocumentazione()
    End Sub



    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtData"), TextBox)

        If Not IsDate(TxtData.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Data formalmente errata</center>');", True)
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); "


            MyJs = MyJs & " $('#" & TxtData.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]); });"

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", MyJs, True)
            Exit Sub
        End If

        MyTable = Session("Documentazione")
        Dim row = Grid.Rows(e.RowIndex)
        MyTable.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text
        MyTable.Rows(row.DataItemIndex).Item(2) = TxtData.Text
        Session("Documentazione") = MyTable

        Grid.EditIndex = -1
        Call BindDocumentazione()
    End Sub



    Private Sub BindDocumentazione()
        MyTable = Session("Documentazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()
    End Sub

    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpLoadFile.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If
        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        If FileUpload1.FileName.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErroreFileNonSpecificato", "VisualizzaErrore('Specificare un file.');", True)
            Exit Sub
        End If

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")))
        End If

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & FileUpload1.FileName)

        Encrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & FileUpload1.FileName, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\Crypt_" & FileUpload1.FileName)

        Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & FileUpload1.FileName)
        Call LeggiDirectory()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub

    
    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click

    End Sub
End Class
