﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading

Partial Class OspitiWeb_Export_FE
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyTable2 As New System.Data.DataTable("tabellaGriglia")
    Dim MyDataSet As New System.Data.DataSet()
    Dim CodiceATSUDO As String = "327"
    Dim TipologiaOfferta As String = "RSA"
    Dim CodiceStruttura As String = "327001795"



    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim OldOspite As Integer = 0
        Dim OldTipoExt As String = ""
        Dim OldCentroServizio As String = ""
        Dim oldRegione As String = ""

        Dim VettoreCentroServizio(1000) As String
        Dim VettoreCodiceOspite(1000) As Integer        
        Dim VettoreAssenzeOspedale(1000) As Integer
        Dim VettoreAssenzeFamiglia(1000) As Integer
        Dim VettorePresenzeOspite(1000) As Integer

        Dim VettoreCodiceRegione(1000) As String
        Dim VettoreTipoImporto(1000) As String


        Dim CentroServizio As String
        Dim NumeroOspiti As Integer = 0

        Dim CodiceIndividualeOspite As String = ""
        Dim DatadiNascita As String = ""
        Dim CodiceIstatComune As String = ""
        Dim CodiceATS As String = ""
        Dim Tipologiapresaincarico As String = ""
        Dim ClassificazionePosto As String = ""
        Dim Specificatipologiaposto As String = ""
        Dim Specificautente As String = ""
        Dim UtenteSolventeinProprio As String = ""
        Dim Codicedrg As String = ""
        Dim DataInizioCondizione As Date = DateSerial(2016, 10, 1)
        Dim DataFineCondizione As Date = DateSerial(2016, 12, 31)
        Dim Utentepresentfinecondizione As String = ""
        Dim GiornateAssenzanelperiodo As String = ""
        Dim TotalegiornateoneriFSR As String = ""
        Dim Giornatetrattamenticomplessivi As String = ""
        Dim Giornatetrattamenti As String = ""
        Dim Tariffa As String = ""
        Dim Tariffaaggiuntiva As String = ""
        Dim QuotaTrattamentobudget As String = ""
        Dim Importocomplessivobase As String = ""
        Dim Importocomplessivoaggiuntiva As String = ""
        Dim i As Integer

        Dim GiorniPresenza As Integer = 0
        Dim GiorniAssenza As Integer = 0


        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\FE1.Txt"


        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim Anno As Integer = Txt_Anno.Text
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim MySQl As String
        Dim X As Integer
        Dim Indice As Integer

        CentroServizio = Cmb_CServ.SelectedValue

        Dim TotGiorni As Integer = DateDiff(DateInterval.Day, DataInizioCondizione, DataFineCondizione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where CODICEOSPITE = 13 AND Tipologia ='O'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim CodiceOspite As Integer
            Dim VettorePresenze(100)
            Dim VettoreStato(100)
            Dim VettoreAts(100)
            Dim VettoreSosia(100)

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))

            Dim Tipo As String

            Dim cmdMov As New OleDbCommand()
            cmdMov.CommandText = ("select Top 1 * from Movimenti Where  CENTROSERVIZIO = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
            cmdMov.Connection = cn
            cmdMov.Parameters.AddWithValue("@DATA", DataInizioCondizione)
            Dim RdMovimenti As OleDbDataReader = cmdMov.ExecuteReader()
            If RdMovimenti.Read Then
                Tipo = campodb(RdMovimenti.Item("TIPOMOV"))
            End If
            RdMovimenti.Close()
            If Tipo <> "13" Then                
                For i = 0 To TotGiorni
                    VettorePresenze(i) = "P"
                Next
            Else
                For i = 0 To TotGiorni
                    VettorePresenze(i) = "C"
                Next
            End If


            Dim cmdMovL As New OleDbCommand()
            cmdMovL.CommandText = ("select Top 1 * from Movimenti Where  CENTROSERVIZIO = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And Data >= ? Order by Data Asc")
            cmdMovL.Connection = cn
            cmdMovL.Parameters.AddWithValue("@DATA", DataInizioCondizione)
            Dim RdMovimentiL As OleDbDataReader = cmdMovL.ExecuteReader()
            Do While RdMovimentiL.Read
                If campodb(RdMovimentiL.Item("TIPOMOV")) = "05" Then
                    For i = DateDiff(DateInterval.Day, DataInizioCondizione, campodbD(RdMovimentiL.Item("DATA"))) To TotGiorni
                        VettorePresenze(i) = "P"
                    Next
                End If
                If campodb(RdMovimentiL.Item("TIPOMOV")) = "13" Then
                    For i = DateDiff(DateInterval.Day, DataInizioCondizione, campodbD(RdMovimentiL.Item("DATA"))) To TotGiorni
                        VettorePresenze(i) = "N"
                    Next
                End If

                If campodb(RdMovimentiL.Item("TIPOMOV")) = "03" Then
                    For i = DateDiff(DateInterval.Day, DataInizioCondizione, campodbD(RdMovimentiL.Item("DATA"))) To TotGiorni
                        If campodb(RdMovimentiL.Item("CAUSALE")) = "02" Then
                            VettorePresenze(i) = "O"
                        Else
                            VettorePresenze(i) = "F"
                        End If
                    Next
                End If
                If campodb(RdMovimentiL.Item("TIPOMOV")) = "04" Then
                    For i = DateDiff(DateInterval.Day, DataInizioCondizione, campodbD(RdMovimentiL.Item("DATA"))) To TotGiorni
                        VettorePresenze(i) = "P"
                    Next
                End If
            Loop
            RdMovimenti.Close()

            Dim cmdStato As New OleDbCommand()
            cmdStato.CommandText = ("select Top 1 * from STATOAUTO Where  CENTROSERVIZIO = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
            cmdStato.Connection = cn
            cmdStato.Parameters.AddWithValue("@DATA", DataInizioCondizione)
            Dim RdStato As OleDbDataReader = cmdStato.ExecuteReader()
            If RdStato.Read Then
                Tipo = campodb(RdStato.Item("TIPORETTA"))

                For i = 0 To TotGiorni
                    VettoreStato(i) = Tipo
                    VettoreAts(i) = ""
                    VettoreSosia(i) = ""
                Next
            End If
            RdStato.Close()


            Dim cmdStatoL As New OleDbCommand()
            cmdStatoL.CommandText = ("select Top 1 * from STATOAUTO Where  CENTROSERVIZIO = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And Data >= ? Order by Data Asc")
            cmdStatoL.Connection = cn
            cmdStatoL.Parameters.AddWithValue("@DATA", DataInizioCondizione)
            Dim RdStatoL As OleDbDataReader = cmdStatoL.ExecuteReader()
            Do While RdStatoL.Read
                For i = DateDiff(DateInterval.Day, DataInizioCondizione, campodbD(RdStatoL.Item("DATA"))) To TotGiorni
                    VettoreStato(i) = campodb(RdStatoL.Item("TIPORETTA"))
                    VettoreAts(i) = ""
                    VettoreSosia(i) = ""
                Next
            Loop
            RdStatoL.Close()
            Dim Presenza As Integer
            Dim AssenzaOspedale As Integer
            Dim AssenzaFamiglia As Integer
            Dim Entrato As Boolean = False
            Dim VecchioTipo As String

            VecchioTipo = VettoreStato(0)

            For i = 0 To TotGiorni
                Entrato = False
                If VecchioTipo <> VettoreStato(i) Then
                    NumeroOspiti = NumeroOspiti + 1

                    VettoreCentroServizio(NumeroOspiti) = CentroServizio
                    VettoreCodiceOspite(NumeroOspiti) = CodiceOspite
                    VettorePresenzeOspite(NumeroOspiti) = Presenza
                    VettoreAssenzeOspedale(NumeroOspiti) = AssenzaOspedale
                    VettoreAssenzeFamiglia(NumeroOspiti) = AssenzaFamiglia
                    VettoreCodiceRegione(NumeroOspiti) = ""
                    VettoreTipoImporto(NumeroOspiti) = VecchioTipo
                    VettoreCodiceRegione(NumeroOspiti) = "1"
                    Presenza = 0
                    AssenzaOspedale = 0
                    AssenzaFamiglia = 0
                    VecchioTipo = VettoreStato(i)
                    Entrato = True
                End If
                If VettorePresenze(i) = "P" Then
                    Presenza = Presenza + 1
                End If
                If VettorePresenze(i) = "O" Then
                    AssenzaOspedale = AssenzaOspedale + 1
                End If
                If VettorePresenze(i) = "F" Then
                    AssenzaFamiglia = AssenzaFamiglia + 1
                End If
            Next
            If Entrato = False Then
                NumeroOspiti = NumeroOspiti + 1

                VettoreCentroServizio(NumeroOspiti) = CentroServizio
                VettoreCodiceOspite(NumeroOspiti) = CodiceOspite
                VettorePresenzeOspite(NumeroOspiti) = Presenza
                VettoreAssenzeOspedale(NumeroOspiti) = AssenzaOspedale
                VettoreAssenzeFamiglia(NumeroOspiti) = AssenzaFamiglia
                VettoreCodiceRegione(NumeroOspiti) = ""
                VettoreTipoImporto(NumeroOspiti) = VecchioTipo
                VettoreCodiceRegione(NumeroOspiti) = "1"
                Presenza = 0
                AssenzaOspedale = 0
                AssenzaFamiglia = 0
                VecchioTipo = VettoreStato(i)
            End If
        Loop
        myPOSTreader.Close()
        


        For X = 0 To NumeroOspiti
            If VettoreCodiceOspite(X) > 0 And VettoreTipoImporto(X) <> "" And VettoreCentroServizio(X) <> "" Then
                Dim DatiOspite As New ClsOspite

                DatiOspite.CodiceOspite = VettoreCodiceOspite(X)
                DatiOspite.Leggi(Session("DC_OSPITE"), DatiOspite.CodiceOspite)

                CodiceIndividualeOspite = DatiOspite.CODICEFISCALE

                If DatiOspite.CODICEFISCALE = "RSSTRS37B48I849Q" Then
                    DatiOspite.CODICEFISCALE = "RSSTRS37B48I849Q"
                End If
                CodiceIndividualeOspite = DatiOspite.CODICEFISCALE
                DatadiNascita = Format(DatiOspite.DataNascita, "yyyyMMdd")

                CodiceIstatComune = DatiOspite.RESIDENZAPROVINCIA1 & DatiOspite.RESIDENZACOMUNE1
                CodiceATS = DatiOspite.USL
                Tipologiapresaincarico = "1"
                ClassificazionePosto = "1"
                Specificatipologiaposto = "A"
                If VettoreTipoImporto(X) = "10" Then
                    Specificatipologiaposto = "B"
                End If

                Specificautente = ""
                If VettoreCentroServizio(X) = "CD07" Then
                    Specificautente = "CDITIPICACL0TEP"
                Else
                    If VettoreTipoImporto(X) = "10" Then
                        Specificautente = "RSAALZHEICL5TEP"
                    End If
                    If VettoreTipoImporto(X) = "01" Then
                        Specificautente = "RSATIPICACL1TEP"
                    End If
                    If VettoreTipoImporto(X) = "02" Then
                        Specificautente = "RSATIPICACL2TEP"
                    End If
                    If VettoreTipoImporto(X) = "03" Then
                        Specificautente = "RSATIPICACL3TEP"
                    End If
                    If VettoreTipoImporto(X) = "04" Then
                        Specificautente = "RSATIPICACL4TEP"
                    End If
                    If VettoreTipoImporto(X) = "05" Then
                        Specificautente = "RSATIPICACL5TEP"
                    End If
                    If VettoreTipoImporto(X) = "06" Then
                        Specificautente = "RSATIPICACL6TEP"
                    End If
                    If VettoreTipoImporto(X) = "07" Then
                        Specificautente = "RSATIPICACL7TEP"
                    End If
                    If VettoreTipoImporto(X) = "08" Then
                        Specificautente = "RSATIPICACL8TEP"
                    End If
                    If VettoreTipoImporto(X) = "09" Then
                        Specificautente = "RSATIPICACL8TEP"
                    End If
                End If


                UtenteSolventeinProprio = "2"
                Codicedrg = ""
                Dim InizioCondizione As New Cls_StatoAuto
                Dim AppDataInizioCondizione As String
                Dim AppDataFineCondizione As String

                InizioCondizione.CODICEOSPITE = VettoreCodiceOspite(X)
                InizioCondizione.CENTROSERVIZIO = VettoreCentroServizio(X)
                InizioCondizione.USL = VettoreCodiceRegione(X)
                InizioCondizione.TipoRetta = VettoreTipoImporto(X)
                InizioCondizione.UltimaDataConUslTipoExtra(Session("DC_OSPITE"), InizioCondizione.CODICEOSPITE, InizioCondizione.CENTROSERVIZIO)


                AppDataInizioCondizione = Format(InizioCondizione.Data, "yyyyMMdd")

                Dim StatoDopoData As New Cls_StatoAuto

                StatoDopoData.Data = InizioCondizione.Data
                StatoDopoData.StatoDopoData(Session("DC_OSPITE"), InizioCondizione.CODICEOSPITE, InizioCondizione.CENTROSERVIZIO)


                If Year(StatoDopoData.Data) > 1 Then
                    AppDataFineCondizione = Format(StatoDopoData.Data, "yyyyMMdd")
                Else
                    AppDataFineCondizione = Space(8)
                End If


                Dim Ospit As New Cls_Movimenti

                Ospit.CodiceOspite = OldOspite
                Ospit.CENTROSERVIZIO = OldCentroServizio
                Ospit.UltimaDataCserv(Session("DC_OSPITE"), InizioCondizione.CODICEOSPITE, InizioCondizione.CENTROSERVIZIO)
                If Ospit.TipoMov = "13" Then
                    Utentepresentfinecondizione = "2"
                Else
                    Utentepresentfinecondizione = "1"
                End If


                GiornateAssenzanelperiodo = VettoreAssenzeOspedale(X)
                TotalegiornateoneriFSR = 0
                Giornatetrattamenticomplessivi = VettorePresenzeOspite(X)
                Giornatetrattamenti = VettorePresenzeOspite(X)

                Dim Quota As New Cls_ImportoRegione
                Quota.UltimaData(Session("DC_OSPITE"), oldRegione, OldTipoExt)

                Tariffa = Math.Round(Quota.Importo * 100, 0)
                Tariffaaggiuntiva = 0
                QuotaTrattamentobudget = Math.Round(Quota.Importo * 100, 0)
                Importocomplessivobase = ""
                Importocomplessivoaggiuntiva = ""

                Dim Appoggio As String

                Appoggio = CodiceATSUDO
                Appoggio = Appoggio & TipologiaOfferta
                Appoggio = Appoggio & CodiceStruttura

                Appoggio = Appoggio & Txt_Anno.Text & "_0" & DD_Mese.SelectedValue
                Appoggio = Appoggio & CodiceIndividualeOspite & Space(16)
                Appoggio = Appoggio & DatadiNascita
                Appoggio = Appoggio & CodiceIstatComune
                Appoggio = Appoggio & AdattaLunghezza(CodiceATS, 3)
                Appoggio = Appoggio & Tipologiapresaincarico
                Appoggio = Appoggio & ClassificazionePosto
                Appoggio = Appoggio & Specificatipologiaposto
                Appoggio = Appoggio & AdattaLunghezza(Specificautente, 15)
                Appoggio = Appoggio & UtenteSolventeinProprio
                Appoggio = Appoggio & AdattaLunghezza(Codicedrg, 3)
                Appoggio = Appoggio & AppDataInizioCondizione
                Appoggio = Appoggio & AppDataFineCondizione
                Appoggio = Appoggio & Utentepresentfinecondizione
                Appoggio = Appoggio & AdattaLunghezzaNumero(GiornateAssenzanelperiodo, 3)
                Appoggio = Appoggio & "000"
                Appoggio = Appoggio & AdattaLunghezzaNumero(Giornatetrattamenti, 3)
                Appoggio = Appoggio & AdattaLunghezzaNumero(Giornatetrattamenticomplessivi, 3)
                Appoggio = Appoggio & Space(3) 'Giornate /trattamenti oltre soglia 1
                Appoggio = Appoggio & Space(3) 'Giornate /trattamenti oltre soglia 2
                Appoggio = Appoggio & AdattaLunghezzaNumero(Tariffa, 8)
                Appoggio = Appoggio & Space(8)
                Appoggio = Appoggio & Space(8)
                Appoggio = Appoggio & "00000000"
                Appoggio = Appoggio & AdattaLunghezzaNumero(QuotaTrattamentobudget, 8)
                Appoggio = Appoggio & "00000000"
                Appoggio = Appoggio & "00000000"

                tw.WriteLine(Appoggio)


                oldRegione = ""

                GiorniPresenza = 0
                GiorniAssenza = 0
            End If
        Next


        cn.Close()

        tw.Close()

    End Sub

    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function



    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If



    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()        
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)
        DD_Mese.SelectedValue = Month(Now)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub
End Class
