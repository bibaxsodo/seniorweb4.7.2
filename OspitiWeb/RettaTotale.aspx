﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="RettaTotale" ValidateRequest="false" EnableEventValidation="false" CodeFile="RettaTotale.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Retta Totale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />



    <script src="css/jquery-ui-1.8.14.custom.min.css" type="text/javascript"></script>

    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>



    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <style>
        #sortable1 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable2 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable3 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable4 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable5 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable6 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable7 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable8 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable9 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable10 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable11 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable12 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable13 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable14 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable15 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable16 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable17 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable18 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable19 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable20 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable21 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable22 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable23 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable24 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable25 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable26 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable27 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable28 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable29 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable30 {
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortableLista {
            border: 1px solid #0000FF;
            width: 220px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            text-align: left;
            float: right;
            margin-right: 10px;
        }

            #sortable1 li, #sortable2 li, #sortable3 li, #sortable4 li, #sortable5 li, #sortable6 li, #sortable7 li, #sortable8 li, #sortable9 li, #sortable10 li, #sortable11 li, #sortable12 li, #sortable13 li, #sortable14 li, #sortable15 li, #sortable16 li, #sortable17 li, #sortable18 li, #sortable19 li, #sortable20 li, #sortableLista li {
                border: 2px solid #6FA7D1;
                margin: 0 5px 5px 5px;
                padding: 5px;
                font-size: 0.9em;
                background-color: #A6C9E2;
                color: #ffffff;
                width: 200px;
            }
    </style>
    <script type="text/javascript">
        function replaceAll(find, replace, str) {
            return str.replace(new RegExp(find, 'g'), replace);
        }

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text="" Style="width: 100%;"></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 185px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Retta Totale</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" OnClientClick="Update();" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Retta Totale
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <asp:GridView ID="Grd_Retta" runat="server" CellPadding="4" Height="60px"
                                                            ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                            BorderStyle="Solid" BorderWidth="1px">
                                                            <RowStyle ForeColor="#6FA7D1" BackColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="IB_Delete" OnClientClick="Update();" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <div style="text-align: right">
                                                                            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" OnClientClick="Update();" runat="server" />
                                                                        </div>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Data">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Importo">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="80px" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Tipo">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="DD_Tipo" runat="server">
                                                                            <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                                            <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                                            <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TipoRetta">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="DD_TipoRetta" runat="server">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                    <ItemStyle Width="30px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Extra Fisso">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Lbl_Sortablelist" runat="server" Text=""></asp:Label>
                                                                        <asp:TextBox ID="TxtSort" Style="text-align: right; border-color: White;" Width="1px" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="#6FA7D1" />
                                                            <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" BorderColor="#6FA7D1" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td style="text-align: right; float: right; width: 250px; vertical-align: top;">
                                                        <div style="width: 230px; text-align: center;"><b>Tipo Extra</b></div>
                                                        <asp:RadioButton ID="RB_Ospite" runat="server" GroupName="TipoExtra" AutoPostBack="true" onclick="Update();" Text="Ospite" />
                                                        <asp:RadioButton ID="RB_Parente" runat="server" GroupName="TipoExtra" AutoPostBack="true" onclick="Update();" Text="Parente" />
                                                        <asp:RadioButton ID="RB_Comune" runat="server" GroupName="TipoExtra" AutoPostBack="true" onclick="Update();" Text="Comune" />
                                                        <br />
                                                        <asp:RadioButton ID="RB_iva4" runat="server" GroupName="TipoExtraIVA" AutoPostBack="true" onclick="Update();" Text="4%" />
                                                        <asp:RadioButton ID="RB_iva5" runat="server" GroupName="TipoExtraIVA" AutoPostBack="true" onclick="Update();" Text="5%" />
                                                        <asp:RadioButton ID="RB_ivaA" runat="server" GroupName="TipoExtraIVA" AutoPostBack="true" onclick="Update();" Text="Altro" />
                                                        <br />
                                                        <div id="MyID"></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
