﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class AddebitoAccredito
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub caricapagina()
        Dim ConnectionString As String = Session("DC_OSPITE")


        Dim o As New ClsOspite

        o.Leggi(ConnectionString, Session("CODICEOSPITE"))


        Dim P As New Cls_Parenti

        P.UpDateDropBox(ConnectionString, Session("CODICEOSPITE"), DD_Parente)


        Dim y As New ClsUSL

        y.UpDateDropBox(ConnectionString, DD_Regione)

        Dim x As New Cls_Addebito

        x.UpDateDropBoxCentroServizio(ConnectionString, dd_TipoAddebito, Session("CODICESERVIZIO"))


        Dim KComune As New ClsComune

        KComune.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)

        Dim d As Integer

        MyTable = ViewState("Appoggio")

        Dim MyADAC As New Cls_AddebitiAccrediti


        MyADAC.leggi(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Request.Item("id"))



        Lbl_Progressivo.Text = Request.Item("id")

        Txt_Data.Text = MyADAC.Data


        Dim x1 As New Cls_Addebito

        x1.UpDateDropBoxCentroServizio(ConnectionString, dd_TipoAddebito, Session("CODICESERVIZIO"), MyADAC.Data)


        RB_ADDEBITO.Checked = False
        RB_ACCREDITO.Checked = False


        If MyADAC.TipoMov = "AD" Then
            RB_ADDEBITO.Checked = True
        End If
        If MyADAC.TipoMov = "AC" Then
            RB_ACCREDITO.Checked = True
        End If

        Txt_Descrizione.Text = MyADAC.Descrizione
        Lbl_Progressivo.Text = MyADAC.Progressivo


        Txt_Importo.Text = Format(MyADAC.IMPORTO, "#,##0.00")

        RB_OSPITE.Checked = False
        RB_PARENTE.Checked = False
        RB_COMUNE.Checked = False
        RB_JOLLY.Checked = False
        Rb_Regione.Checked = False

        If MyADAC.RIFERIMENTO = "O" Then
            RB_OSPITE.Checked = True
        End If
        If MyADAC.RIFERIMENTO = "P" Then
            RB_PARENTE.Checked = True
        End If
        If MyADAC.RIFERIMENTO = "C" Then
            RB_COMUNE.Checked = True
        End If
        If MyADAC.RIFERIMENTO = "J" Then
            RB_JOLLY.Checked = True
        End If
        If MyADAC.RIFERIMENTO = "R" Then
            Rb_Regione.Checked = True
        End If


        DD_Parente.SelectedValue = MyADAC.PARENTE




        DD_Regione.SelectedValue = MyADAC.REGIONE

        Dim MyDec As New ClsComune

        MyDec.Provincia = campodb(MyADAC.PROVINCIA)
        MyDec.Comune = campodb(MyADAC.COMUNE)
        MyDec.Leggi(ConnectionString)


        DD_Comune.SelectedValue = MyADAC.PROVINCIA & " " & MyADAC.COMUNE

        dd_TipoAddebito.SelectedValue = MyADAC.CodiceIva

        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)

        If Mt.Importo > 0 Then
            Txt_Quantita.Visible = True
        Else
            Txt_Quantita.Visible = False
        End If

        Txt_Quantita.Text = Format(MyADAC.Quantita, "#,##0.00")

        Txt_Anno.Text = MyADAC.ANNOCOMPETENZA
        Dd_Mese.SelectedValue = MyADAC.MESECOMPETENZA

        RB_FuoriRetta.Checked = True
        RB_Retta.Checked = False
        Rb_Rendiconto.Checked = False


        If MyADAC.RETTA = "S" Then
            RB_Retta.Checked = True
        End If
        If MyADAC.RETTA = "N" Then
            RB_FuoriRetta.Checked = True
        End If
        If MyADAC.RETTA = "O" Then
            Rb_Rendiconto.Checked = True
        End If

        If MyADAC.Elaborato = 1 Then
            ChK_Elaborato.Checked = True
        Else
            ChK_Elaborato.Checked = False
        End If

        If MyADAC.raggruppa = 1 Then
            Chk_Ragruppa.Checked = True
        Else
            Chk_Ragruppa.Checked = False

        End If

        Txt_Registrazione.Text = MyADAC.NumeroRegistrazione


        Txt_Chiave.Text = MyADAC.chiaveselezione

        Try
            Dim NomeSocieta As String

            Dim k2 As New Cls_Login

            k2.Utente = Session("UTENTE")
            k2.LeggiSP(Application("SENIOR"))

            NomeSocieta = k2.RagioneSociale

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & "AdAC_" & Request.Item("id") & "\1.jpg") <> "" Then
                ImgAllegato.ImageUrl = "..\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & "AdAC_" & Request.Item("id") & "\1.jpg"
                ImgAllegato.Visible = True
            Else

                ImgAllegato.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If



        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select column_name, data_type, character_maximum_length      from information_schema.columns   where table_name = 'ADDACR' and column_name like  'DESCRIZIONE'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Txt_Descrizione.MaxLength = Val(campodb(myPOSTreader.Item("character_maximum_length")))
        End If
        cn.Close()

        If Page.IsPostBack = False Then



            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If
            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Dim Barra As New Cls_BarraSenior

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

            Call Pulisci()

            Dim ConnectionString As String = Session("DC_OSPITE")

            Dim o As New ClsOspite

            o.Leggi(ConnectionString, Session("CODICEOSPITE"))



            Dim P As New Cls_Parenti

            P.UpDateDropBox(ConnectionString, Session("CODICEOSPITE"), DD_Parente)


            Dim y As New ClsUSL

            y.UpDateDropBox(ConnectionString, DD_Regione)

            Dim x1 As New Cls_Addebito

            x1.UpDateDropBoxCentroServizio(ConnectionString, dd_TipoAddebito, Session("CODICESERVIZIO"))

            Dim KComune As New ClsComune

            KComune.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)


            Dim TipoMod As String

            TipoMod = "O"
            Dim MiOsp As New Cls_Modalita
            Dim CodiceParente As Integer = 0

            MiOsp.CentroServizio = Session("CODICESERVIZIO")
            MiOsp.CodiceOspite = Session("CODICEOSPITE")
            MiOsp.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio)
            If MiOsp.MODALITA = "P" Then
                TipoMod = "P"
                CodiceParente = 1
            End If

            Dim MyPar As New Cls_ImportoParente

            MyPar.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 1)
            If MyPar.IMPORTO > 0 Or MyPar.PERCENTUALE > 0 Then
                TipoMod = "P"
                CodiceParente = 1
            End If

            Dim MyPar1 As New Cls_ImportoParente


            MyPar1.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 2)
            If MyPar1.IMPORTO > 0 Or MyPar1.PERCENTUALE > 0 Then
                TipoMod = "P"
                CodiceParente = 2
            End If

            Dim MyPar2 As New Cls_ImportoParente

            MyPar2.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 3)
            If MyPar2.IMPORTO > 0 Or MyPar2.PERCENTUALE > 0 Then
                TipoMod = "P"
                CodiceParente = 3
            End If


            Dim MyPar3 As New Cls_ImportoParente

            MyPar3.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 4)
            If MyPar3.IMPORTO > 0 Or MyPar3.PERCENTUALE > 0 Then
                TipoMod = "P"
                CodiceParente = 4
            End If

            If TipoMod = "P" Then
                RB_OSPITE.Checked = False
                RB_PARENTE.Checked = True
                DD_Parente.SelectedValue = CodiceParente
            End If



            If Val(Request.Item("ID")) > 0 Then
                Call caricapagina()
            End If


            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        End If
        Call EseguiJS()
    End Sub


    Protected Sub Modifica()
        Dim ConnectionString As String = Session("DC_OSPITE")


        If Val(Lbl_Progressivo.Text) = 0 Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "I", "MOVIMENTI", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_AddebitiAccrediti

            OldDatiPar.CENTROSERVIZIO = Session("CODICESERVIZIO")
            OldDatiPar.CodiceOspite = Session("CODICEOSPITE")
            OldDatiPar.ID = Val(Request.Item("ID"))
            OldDatiPar.leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Val(Request.Item("ID")))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "MOVIMENTI", AppoggioJS)

        End If

        Dim x As New Cls_AddebitiAccrediti


        x.CodiceOspite = Session("CODICEOSPITE")
        x.CENTROSERVIZIO = Session("CODICESERVIZIO")

        If RB_ADDEBITO.Checked = True Then

            x.TipoMov = "AD"

        End If

        If RB_ACCREDITO.Checked = True Then

            x.TipoMov = "AC"

        End If

        If RB_OSPITE.Checked = True Then
            x.RIFERIMENTO = "O"
        End If
        If RB_PARENTE.Checked = True Then
            x.RIFERIMENTO = "P"
        End If
        If RB_COMUNE.Checked = True Then
            x.RIFERIMENTO = "C"
        End If
        If Rb_Regione.Checked = True Then
            x.RIFERIMENTO = "R"
        End If
        If RB_JOLLY.Checked = True Then
            x.RIFERIMENTO = "J"
        End If


        x.ANNOCOMPETENZA = Txt_Anno.Text
        x.MESECOMPETENZA = Dd_Mese.SelectedValue

        x.Data = Txt_Data.Text
        x.Descrizione = Txt_Descrizione.Text

        x.Progressivo = Val(Lbl_Progressivo.Text)

        x.PARENTE = DD_Parente.SelectedValue

        Dim Vettore(100) As String

        Vettore = SplitWords(DD_Comune.SelectedValue)

        If Vettore.Length >= 2 Then
            x.PROVINCIA = Vettore(0)
            x.COMUNE = Vettore(1)
        End If

        x.REGIONE = DD_Regione.SelectedValue

        x.CodiceIva = dd_TipoAddebito.SelectedValue

        x.Quantita = Val(Txt_Quantita.Text)

        If Chk_Ragruppa.Checked = True Then
            x.raggruppa = 1
        Else
            x.raggruppa = 0
        End If

        x.IMPORTO = CDbl(Txt_Importo.Text)
        If RB_FuoriRetta.Checked = True Then
            x.RETTA = "N"
        End If
        If RB_Retta.Checked = True Then
            x.RETTA = "S"
        End If
        If Rb_Rendiconto.Checked = True Then
            x.RETTA = "O"
        End If

        If ChK_Elaborato.Checked = True Then
            x.Elaborato = 1
        Else
            x.Elaborato = 0
        End If

        x.NumeroRegistrazione = Val(Txt_Registrazione.Text)

        x.chiaveselezione = Txt_Chiave.Text

        x.Utente = Session("UTENTE")

        Dim AppoggioJS2 As String
        Dim ConvT2 As New Cls_DataTableToJson
        Dim Log2 As New Cls_LogPrivacy

        AppoggioJS2 = ConvT2.SerializeObject(x)

        Log2.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "MOVIMENTI", AppoggioJS2)


        x.AggiornaDB(ConnectionString)




    End Sub

    Protected Sub Elimina()
        If Not IsDate(Txt_Data.Text) Then            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Selezionare movimento');", True)
            Exit Sub
        End If


        Dim xs As New Cls_AddebitiAccrediti
        Dim ConnectionString As String = Session("DC_OSPITE")

        xs.CENTROSERVIZIO = Session("CODICESERVIZIO")
        xs.CodiceOspite = Session("CODICEOSPITE")
        xs.Progressivo = Val(Lbl_Progressivo.Text)
        xs.Data = Txt_Data.Text

        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_AddebitiAccrediti

        OldDatiPar.CENTROSERVIZIO = Session("CODICESERVIZIO")
        OldDatiPar.CodiceOspite = Session("CODICEOSPITE")
        OldDatiPar.ID = Val(Request.Item("ID"))
        OldDatiPar.leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Val(Request.Item("ID")))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "D", "MOVIMENTI", AppoggioJS)


        xs.Elimina(ConnectionString)

    End Sub

    
    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Validità formalmente errata');", True)
            Exit Sub
        End If
        If Txt_Importo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specifica un importo');", True)
            Exit Sub
        End If
        If CDbl(Txt_Importo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specifica un importo');", True)
            Exit Sub
        End If

        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica ospite');", True)
            Exit Sub
        End If


        If dd_TipoAddebito.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare tipo addebito');", True)
            Exit Sub
        End If


        If RB_PARENTE.Checked = True Then
            If dd_TipoAddebito.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare tipo addebito');", True)
                Exit Sub
            End If
            If DD_Parente.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare parente');", True)
                Exit Sub
            End If
        End If
        If RB_COMUNE.Checked = True Then
            If DD_Comune.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare provincia comune');", True)
                Exit Sub
            End If
        End If
        If RB_JOLLY.Checked = True Then
            If DD_Comune.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare provincia comune');", True)
                Exit Sub
            End If
        End If
        If Rb_Regione.Checked = True Then
            If DD_Regione.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare la regione');", True)
                Exit Sub
            End If
        End If
        Call Modifica()

        Call PaginaPrecedente()

    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica ospite');", True)
            Exit Sub
        End If
        If Val(Lbl_Progressivo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Seleziona movimento');", True)
            Exit Sub
        End If

        Call Elimina()

        Call PaginaPrecedente()
    End Sub

    Private Sub Pulisci()
        RB_FuoriRetta.Checked = True
        RB_Retta.Checked = False
        Rb_Rendiconto.Checked = False
        RB_ADDEBITO.Checked = True
        RB_ACCREDITO.Checked = False
        Txt_Data.Text = Format(Now, "dd/MM/yyyy")
        Txt_Descrizione.Text = ""
        Lbl_Progressivo.Text = ""
        Txt_Quantita.Text = "0,00"
        DD_Parente.SelectedValue = 0        
        DD_Comune.SelectedValue = ""
        Txt_Importo.Text = "0,00"
        DD_Regione.SelectedValue = ""
        dd_TipoAddebito.SelectedValue = ""
        RB_OSPITE.Checked = True
        RB_PARENTE.Checked = False
        RB_COMUNE.Checked = False
        RB_JOLLY.Checked = False
        Rb_Regione.Checked = False
        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)
    End Sub
    
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"
        'MyJs = MyJs & " if ((appoggio.match('dd_TipoAddebito')!= null) ) {  "
        'MyJs = MyJs & "$(els[i]).chosen({"
        'MyJs = MyJs & "no_results_text: ""Tipo Addebito/Accredito non trovato"","
        'MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        'MyJs = MyJs & "width: ""350px"","
        'MyJs = MyJs & "placeholder_text_single: ""Seleziona Tipo Addebito/Accredito"""
        'MyJs = MyJs & "});"
        'MyJs = MyJs & "    }"
        'MyJs = MyJs & " if ((appoggio.match('DD_Comune')!= null) ) {  "
        ''MyJs = MyJs & "$(els[i]).chosen({"
        ''MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        ''MyJs = MyJs & "display_disabled_options: true,"
        ''MyJs = MyJs & "allow_single_deselect: true,"
        ''MyJs = MyJs & "width: ""350px"","
        ''MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        ''MyJs = MyJs & "});"
        'MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_ComRes')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

        MyJs = MyJs & "$(""#" & dd_TipoAddebito.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Tipo Addebito/Accredito Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""460px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Addebito/Accredito"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$(""#" & DD_Comune.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Comune Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$(""#" & DD_Regione.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Regione Non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""430px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Regione"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        Response.Redirect("GrigliaAddebitiAccrediti.aspx?CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))

    End Sub

    Protected Sub Txt_Data_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Data.TextChanged
        If Not IsDate(Txt_Data.Text) Then
            Exit Sub
        End If

        Dim Appoggio As String

        Appoggio = dd_TipoAddebito.SelectedValue

        Dim x1 As New Cls_Addebito

        x1.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, Session("CODICESERVIZIO"), Txt_Data.Text)


        Try
            dd_TipoAddebito.SelectedValue = Appoggio
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dd_TipoAddebito_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_TipoAddebito.SelectedIndexChanged
        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
        If Mt.Importo > 0 Then
            Txt_Quantita.Visible = True
            lblImporto.Text = "Quantità importo :"
            Txt_Quantita.Text = "1,00"
            Txt_Importo.Text = Format(Mt.Importo * Val(Txt_Quantita.Text), "#,##0.00")
        Else
            Txt_Quantita.Visible = False
            lblImporto.Text = "Importo :"
            Txt_Quantita.Text = "0,00"
        End If
    End Sub

    Protected Sub Txt_Quantita_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Quantita.TextChanged
        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
        If Mt.Importo > 0 Then
            Txt_Importo.Text = Format(Mt.Importo * Val(Txt_Quantita.Text), "#,##0.00")
        End If
    End Sub
End Class
