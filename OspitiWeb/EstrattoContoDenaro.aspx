﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_EstrattoContoDenaro" CodeFile="EstrattoContoDenaro.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Estratto Conto Denaro</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'apertura del popup da parte del browser");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Statistiche - Estratto Conto Denaro</div>
                                <div class="SottoTitoloOSPITE">
                                    <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <span class="BenvenutoText">Benvenuto
                                    <asp:Label ID="Lbl_Utente" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <div class="DivTastiOspite">
                                    <asp:ImageButton ID="ImageButton5" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                    <asp:ImageButton ID="ImageButton6" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                                    <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" Width="38px" ToolTip="Stampa" ID="imgBtnStampa"></asp:ImageButton>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />

                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Estratto Conto Denaro
                                        </HeaderTemplate>

                                        <ContentTemplate>
                                            <label class="LabelCampo">Estrazione :</label>
                                            &nbsp;&nbsp;
                                            <asp:RadioButton ID="Est_Data" runat="server" Text="Per Data" AutoPostBack="true" GroupName="Estrazione" />
                                            <asp:RadioButton ID="Est_Tipo" runat="server" Text="Periodo " AutoPostBack="true" GroupName="Estrazione" />
                                            <br />
                                            <br />
                                            <table>
                                                <tr style="vertical-align: top;">
                                                    <td>
                                                        <label class="LabelCampo">Data Dal :</label>
                                                        <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />
                                                        <br />

                                                        <label class="LabelCampo">Data Al :</label>
                                                        <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox><br />
                                                        <br />
                                                        <br />

                                                    </td>
                                                    <td>
                                                        <label class="LabelCampo">Periodo Dal :</label>
                                                        <asp:TextBox ID="Txt_AnnoCompetenzaDal" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                                        <asp:DropDownList ID="Dd_MeseCompetenzaDal" runat="server" Width="128px">
                                                            <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                            <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                            <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                            <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                            <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                            <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                            <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                            <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                            <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                            <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                        </asp:DropDownList><br />
                                                        <br />
                                                        <label class="LabelCampo">Periodo Al :</label>
                                                        <asp:TextBox ID="Txt_AnnoCompetenzaAl" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                                        <asp:DropDownList ID="Dd_MeseCompetenzaAl" runat="server" Width="128px">
                                                            <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                            <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                            <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                            <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                            <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                            <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                            <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                            <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                            <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                            <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                        </asp:DropDownList><br />

                                                    </td>
                                                </tr>
                                            </table>

                                            <label class="LabelCampo">Addebiti Programamti :</label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="Chk_ADP" runat="server" Text="" />
                                            <br />
                                            <br />
                                            <br />

                                            <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                                BorderWidth="1px" GridLines="Vertical">
                                                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                <AlternatingRowStyle BackColor="Gainsboro" />

                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                                    ForeColor="White" />
                                            </asp:GridView>
                                        </ContentTemplate>


                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ImageButton6" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>

                    <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                        Attendere prego.....<br />
                        <img height="30px" src="images/loading.gif">
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </div>
    </form>
</body>
</html>
