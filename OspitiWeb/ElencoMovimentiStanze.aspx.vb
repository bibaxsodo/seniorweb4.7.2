﻿
Partial Class OspitiWeb_ElencoMovimentiStanze
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim kX As New Cls_MovimentiStanze

        kX.CentroServizio = Session("CODICESERVIZIO")
        kX.loaddati(Session("DC_OSPITIACCESSORI"), Session("CODICEOSPITE"), Year(Now), MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Denaro.AutoGenerateColumns = True
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()
        Call ColoreGriglia()
    End Sub


    Private Sub ColoreGriglia()

        Dim k As Integer
        Dim i As Integer

        For i = 0 To Grd_Denaro.Rows.Count - 1
            If Grd_Denaro.Rows(i).Cells(3).Text = "Occupazione" Then
                For k = 0 To 4
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Green
                Next
            End If
            If Grd_Denaro.Rows(i).Cells(3).Text = "Liberazione" Then

                For k = 0 To 4
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Red
                Next k
            End If
        Next

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim kX As New Cls_MovimentiStanze

        kX.CentroServizio = Session("CODICESERVIZIO")
        kX.loaddati(Session("DC_OSPITIACCESSORI"), Session("CODICEOSPITE"), Year(Now), MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Denaro.AutoGenerateColumns = True
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()

        Call ColoreGriglia()
    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Denaro.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_Denaro.PageIndex = e.NewPageIndex
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()

        Call ColoreGriglia()
    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Denaro.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = MyTable.Rows(d).Item(0).ToString


            Response.Redirect("GestioneMovimentiStanza.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Denaro.SelectedIndexChanged

    End Sub

    

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=STANZE")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=STANZE&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_Occupazione_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Occupazione.Click
        Dim k As New Cls_MovimentiStanze


        k.CodiceOspite = Val(Session("CODICEOSPITE"))
        k.CentroServizio = Session("CODICESERVIZIO")
        k.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))

        If k.Tipologia <> "OC" Then
            Response.Redirect("GestioneMovimentiStanza.aspx?CODICE=0")
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite già occupa altro letto');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub Btn_Liberazione_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Liberazione.Click
        Dim k As New Cls_MovimentiStanze


        k.CodiceOspite = Val(Session("CODICEOSPITE"))
        k.CentroServizio = Session("CODICESERVIZIO")
        k.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))

        If k.Tipologia = "OC" Then
            Response.Redirect("GestioneMovimentiStanza.aspx?CODICE=0&TIPO=LIBERAZIONE")
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite non occupa nessun letto');", True)
            Exit Sub
        End If
    End Sub
End Class
