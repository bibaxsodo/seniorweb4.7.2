﻿Imports System.Web.Hosting

Partial Class OspitiWeb_AddebitoAccreditoProgrammati
    Inherits System.Web.UI.Page



    Protected Sub OspitiWeb_AddebitoAccreditoProgrammati_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"



        If Page.IsPostBack = False Then


            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Dim Barra As New Cls_BarraSenior

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

            Dim ParCl As New Cls_Parenti

            ParCl.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_Parente)

            Dim UslCl As New ClsUSL

            UslCl.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)

            Dim TipoAdd As New Cls_Addebito

            TipoAdd.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, Session("CODICESERVIZIO"))

            Dim KComune As New ClsComune

            KComune.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)



            If Val(Request.Item("ID")) > 0 Then
                Dim k As New Cls_AddebitoAccreditiProgrammati


                k.CentroServizio = Session("CODICESERVIZIO")
                k.CodiceOspite = Session("CODICEOSPITE")
                k.ID = Val(Request.Item("ID"))
                k.leggi(Session("DC_OSPITE"))

                Txt_Importo.Text = Format(k.Importo, "#,##0.00")
                Txt_DataInizio.Text = Format(k.DataInizio, "dd/MM/yyyy")
                Txt_DataFine.Text = Format(k.DataFine, "dd/MM/yyyy")
                DD_Parente.SelectedValue = k.Parente

                Txt_Descrizione.Text = k.Descrizione

                If k.Tipo = "AD" Then
                    RB_ADDEBITO.Checked = True
                    RB_ACCREDITO.Checked = False
                Else
                    RB_ACCREDITO.Checked = True
                    RB_ADDEBITO.Checked = False
                End If

                dd_TipoAddebito.SelectedValue = k.TipoAddebito


                DD_Regione.SelectedValue = k.CodiceRegione

                DD_Tipo.SelectedValue = k.Ripetizione

                Dim kCom As New ClsComune

                kCom.Provincia = k.Provincia
                kCom.Comune = k.Comune
                kCom.Leggi(Session("DC_OSPITE"))

                'Txt_ComRes.Text = kCom.Provincia & " " & kCom.Comune & " " & kCom.Descrizione
                DD_Comune.SelectedValue = kCom.Provincia & " " & kCom.Comune


                RB_OSPITE.Checked = False
                RB_PARENTE.Checked = False
                RB_COMUNE.Checked = False
                Rb_Regione.Checked = False

                If k.Riferimento = "O" Then
                    RB_OSPITE.Checked = True
                End If
                If k.Riferimento = "P" Then
                    RB_PARENTE.Checked = True
                End If
                If k.Riferimento = "C" Then
                    RB_COMUNE.Checked = True
                End If
                If k.Riferimento = "R" Then
                    Rb_Regione.Checked = True
                End If

                If k.Rendiconto = 1 Then
                    ChK_Rendiconto.Checked = True
                Else
                    ChK_Rendiconto.Checked = False
                End If
            End If
        End If


        Call EseguiJS()

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_ComRes')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "


        MyJs = MyJs & "$(""#" & dd_TipoAddebito.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Tipo Addebito/Accredito Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""460px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Addebito/Accredito"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$(""#" & DD_Comune.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Comune Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""430px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$(""#" & DD_Regione.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Regione Non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""430px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Regione"""
        MyJs = MyJs & "});"
        
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        If Not IsDate(Txt_DataInizio.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Inizio formalmente errata');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataFine.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Fine formalmente errata');", True)
            Exit Sub
        End If
        If Txt_Importo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specifica un importo');", True)
            Exit Sub
        End If
        If CDbl(Txt_Importo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specifica un importo');", True)
            Exit Sub
        End If


        Dim k As New Cls_AddebitoAccreditiProgrammati


        k.CentroServizio = Session("CODICESERVIZIO")
        k.CodiceOspite = Session("CODICEOSPITE")
        k.ID = Val(Request.Item("ID"))
        k.leggi(Session("DC_OSPITE"))


        k.Importo = Txt_Importo.Text
        k.DataInizio = Txt_DataInizio.Text
        k.DataFine = Txt_DataFine.Text
        k.Parente = DD_Parente.SelectedValue

        k.Descrizione = Txt_Descrizione.Text

        If RB_ADDEBITO.Checked = True Then
            k.Tipo = "AD"
        Else
            k.Tipo = "AC"
        End If

        k.TipoAddebito = dd_TipoAddebito.SelectedValue

        k.Ripetizione = DD_Tipo.SelectedValue

        k.CodiceRegione = DD_Regione.SelectedValue

        If RB_OSPITE.Checked = True Then
            k.Riferimento = "O"
        End If
        If RB_PARENTE.Checked = True Then
            k.Riferimento = "P"
        End If
        If RB_COMUNE.Checked = True Then
            k.Riferimento = "C"
        End If
        If Rb_Regione.Checked = True Then
            k.Riferimento = "R"
        End If


        If ChK_Rendiconto.Checked = True Then
            k.Rendiconto = 1
        Else
            k.Rendiconto = 0
        End If

        Dim Vettore(100) As String

        Vettore = SplitWords(DD_Comune.SelectedValue)

        If Vettore.Length >= 2 Then
            k.Provincia = Vettore(0)
            k.Comune = Vettore(1)
        End If

        k.AggiornaDB(Session("DC_OSPITE"))

        Response.Redirect("Elenco_AddebitoAccreditoProgrammati.aspx?CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Dim k As New Cls_AddebitoAccreditiProgrammati


        k.CentroServizio = Session("CODICESERVIZIO")
        k.CodiceOspite = Session("CODICEOSPITE")
        k.ID = Val(Request.Item("ID"))
        k.Elimina(Session("DC_OSPITE"))

        Response.Redirect("Elenco_AddebitoAccreditoProgrammati.aspx?CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("Elenco_AddebitoAccreditoProgrammati.aspx?CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub
End Class
