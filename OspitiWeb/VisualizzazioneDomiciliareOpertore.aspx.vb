﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_VisualizzazioneDomiciliareOpertore
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click



        Call CaricaTabella()

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Call CaricaTabella()


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Txt_Operatore.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare l'operatore');", True)
            Exit Sub
        End If
        
        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=AddebitiAccrediti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Operatore')!= null) || (appoggio.match('Txt_Operatore')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutoComplateOperatore.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim CondOspite As String
        Dim Ordine As String = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("IdMovimento", GetType(String))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Minuti", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Tipologia", GetType(String))
        MyTable.Columns.Add("Operatore", GetType(String))

        Dim CodiceOperatore As Integer = 0

        If Txt_Operatore.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Operatore.Text)
            If Not IsNothing(Vettore(0)) Then
                If Val(Vettore(0)) > 0 Then

                    CodiceOperatore = Val(Vettore(0))
                End If
            End If
        End If

        Dim cmd As New OleDbCommand()

        Condizione = ""
        Condizione = Condizione & "SELECT TOP (1000) [ID],[IdMovimento],"
        Condizione = Condizione & "(select nome from AnagraficaComune where AnagraficaComune.CODICEOSPITE = (select MovimentiDomiciliare.CodiceOspite from  MovimentiDomiciliare where IdMovimento =MovimentiDomiciliare.ID) and CODICEPARENTE = 0) as nome"
        Condizione = Condizione & ",[CodiceOperatore]"
        Condizione = Condizione & ",(select DATEDIFF ( ""n"",OraInizio,OraFine) from  MovimentiDomiciliare where IdMovimento = MovimentiDomiciliare.ID) as minuti "
        Condizione = Condizione & ",(select data  from  MovimentiDomiciliare where IdMovimento = MovimentiDomiciliare.ID) as data"
        Condizione = Condizione & ",(select tipologia  from  MovimentiDomiciliare  where IdMovimento = MovimentiDomiciliare.ID) as tipologia"
        Condizione = Condizione & "  FROM [MovimentiDomiciliare_Operatori]"
        If CodiceOperatore > 0 Then
            Condizione = Condizione & "  where codiceoperatore = " & CodiceOperatore & " and "
        Else
            Condizione = Condizione & "  Where "
        End If

        Condizione = Condizione & " (select year(data) from  MovimentiDomiciliare  where IdMovimento = MovimentiDomiciliare.ID) = " & Val(Txt_Anno.Text)
        Condizione = Condizione & " and (select MONTH(data) from  MovimentiDomiciliare  where IdMovimento = MovimentiDomiciliare.ID) = " & Val(Dd_Mese.Text)


        Dim TotMinuti As Integer = 0
        Dim TotRighe As Integer = 0

        cmd.CommandText = Condizione

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("IdMovimento"))
            myriga(1) = campodb(myPOSTreader.Item("nome"))
            myriga(2) = campodb(myPOSTreader.Item("minuti"))

            TotMinuti = TotMinuti + Val(campodb(myPOSTreader.Item("minuti")))
            TotRighe = TotRighe + 1
            myriga(3) = campodb(myPOSTreader.Item("data"))

            Dim TipoDom As New Cls_TipoDomiciliare

            TipoDom.Codice = campodb(myPOSTreader.Item("tipologia"))
            TipoDom.Leggi(Session("DC_OSPITE"), TipoDom.Codice)
            myriga(4) = TipoDom.Descrizione



            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = campodb(myPOSTreader.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))

            myriga(5) = Operatore.Nome

            MyTable.Rows.Add(myriga)
        Loop
        cn.Close()



        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        myriga1(0) = "Totali "
        myriga1(1) = "Minuti :"
        myriga1(2) = TotMinuti
        myriga1(3) = "Accessi :"
        myriga1(4) = TotRighe

        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_StatisticheDomiciliari.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)
    End Sub
End Class
