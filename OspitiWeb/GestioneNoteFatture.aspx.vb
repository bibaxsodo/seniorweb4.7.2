﻿
Partial Class OspitiWeb_GestioneNoteFatture
    Inherits System.Web.UI.Page

    Protected Sub OspitiWeb_GestioneNoteFatture_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim y As New ClsUSL

        y.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        If Val(Request.Item("ID")) > 0 Then
            Dim k As New Cls_NoteFattureOspiti

            k.id = Val(Request.Item("ID"))
            k.Leggi(ConnectionString, k.id)

            Txt_ID.Text = k.id
            Txt_CodiciOspiti.Text = k.CODICIOSPITI


            RB_OspitiParenti.Checked = False
            RB_ComuneRegioni.Checked = False
            RB_Entrambe.Checked = False
            If k.OspitiParentiComuniRegioni = "O" Then
                RB_OspitiParenti.Checked = True
            End If
            If k.OspitiParentiComuniRegioni = "C" Then
                RB_ComuneRegioni.Checked = True
            End If
            If k.OspitiParentiComuniRegioni = "E" Then
                RB_Entrambe.Checked = True
            End If

            Dim KComun As New ClsComune

            KComun.Provincia = k.CodiceProvincia
            KComun.Comune = k.CodiceComune
            KComun.Leggi(ConnectionString)
            Txt_ComRes.Text = k.CodiceProvincia & " " & k.CodiceComune & " " & KComun.Descrizione

            Txt_NoteDown.Text = k.NoteDown
            Txt_NoteUp.Text = k.NoteUp

            Txt_NumeroRegistrazione.Text = k.NumeroRegistrazione

            Dim Kcser As New Cls_CentroServizio

            Kcser.CENTROSERVIZIO = k.CodiceServizio

            Kcser.Leggi(ConnectionString, Kcser.CENTROSERVIZIO)

            DD_Struttura.SelectedValue = Kcser.Villa



            Call AggiornaCServ()

            Cmb_CServ.SelectedValue = Kcser.CENTROSERVIZIO

            DD_Regione.SelectedValue = k.CodiceRegione



        End If

        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
        Call EseguiJS()
    End Sub
    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()        
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_ComRes')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim k As New Cls_NoteFattureOspiti

        k.id = Val(Txt_ID.Text)
        k.Leggi(ConnectionString, k.id)

        k.id = Val(Txt_ID.Text)
        k.CODICIOSPITI = Txt_CodiciOspiti.Text

        If RB_OspitiParenti.Checked = True Then
            k.OspitiParentiComuniRegioni = "O"
        End If
        If RB_ComuneRegioni.Checked = True Then
            k.OspitiParentiComuniRegioni = "C"
        End If
        If RB_Entrambe.Checked = True Then
            k.OspitiParentiComuniRegioni = "E"
        End If

        Dim Vettore(100) As String

        Vettore = SplitWords(Txt_ComRes.Text)

        If Vettore.Length >= 3 Then
            k.CodiceProvincia = Vettore(0)
            k.CodiceComune = Vettore(1)
        End If


        k.NoteDown = Txt_NoteDown.Text
        k.NoteUp = Txt_NoteUp.Text

        k.NumeroRegistrazione = Val(Txt_NumeroRegistrazione.Text)

        
        k.CodiceServizio = Cmb_CServ.SelectedValue

        k.CodiceRegione = DD_Regione.SelectedValue

        k.Scrivi(ConnectionString)

        Response.Redirect("ElencoNoteFatture.aspx")
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoNoteFatture.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Txt_ID.Text = 0
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim k As New Cls_NoteFattureOspiti

        k.id = Val(Txt_ID.Text)
        k.Delete(ConnectionString, k.id)
        Response.Redirect("ElencoNoteFatture.aspx")
    End Sub
End Class

