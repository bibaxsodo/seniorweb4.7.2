﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="NuovoOspite" CodeFile="NuovoOspite.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Inserimento Nuovo Ospite</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                            CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Nuovo Ospite        
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="left"></div>     
    
      <label class="LabelCampo">Cognome Nome : </label>
                                                <asp:TextBox ID="Txt_Cognome" Style="border-color: Red;" runat="server" Width="240px" AutoPostBack="True"></asp:TextBox>
                                                <asp:TextBox ID="Txt_Nome" Style="border-color: Red;" runat="server" Width="240px" AutoPostBack="True"></asp:TextBox>
                                                <asp:Image runat="server" ImageUrl="~/images/Blanco.png" Height="18px" Width="18px" ID="Img_VerificaDes"></asp:Image>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Sesso : </label>
                                                <asp:RadioButton ID="RB_Maschile" Style="border-color: Red;" runat="server" GroupName="sesso"></asp:RadioButton>
                                                Maschile
     <asp:RadioButton ID="RB_Femminile" Style="border-color: Red;" runat="server" GroupName="sesso"></asp:RadioButton>
                                                Femminile<br />
                                                <br />
                                                <label class="LabelCampo">Data Nascita :</label>
                                                <asp:TextBox ID="Txt_DataNascita" Style="border-color: Red;" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Comune nascita :</label>
                                                <asp:TextBox ID="Txt_ComuneNascita" runat="server" Width="392px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Codice Fiscale :</label>
                                                <asp:TextBox ID="Txt_CodiceFiscale" runat="server" Width="192px"></asp:TextBox>
                                                <asp:Button ID="Button1" runat="server" BackColor="#33CCCC" Text="Calcola" />
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Centro Servizio : </label>
                                                <asp:DropDownList ID="Cmb_CServ" Style="border-color: Red;" AutoPostBack="true" runat="server" Width="288px"></asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Data Accoglimento : </label>
                                                <asp:TextBox ID="Txt_DataAccoglimento" Width="90px" Style="border-color: Red;" runat="server"></asp:TextBox>
                                                <br />
                                                <br />
                                                Consenso all’inserimento dei dati personali in Senior (l’informativa è stata compresa e firmata dall’interessato):
      <asp:RadioButton ID="RB_SIConsenso1" runat="server" Text="SI" GroupName="Consenso1" />
                                                <asp:RadioButton ID="RB_NOConsenso1" runat="server" Text="NO" GroupName="Consenso1" />
                                                <br />
                                                <br />
                                                Consenso al trattamento per finalita’ di marketing :
      <asp:RadioButton ID="RB_SIConsenso2" runat="server" Text="SI" GroupName="Consenso2" />
                                                <asp:RadioButton ID="RB_NOConsenso2" runat="server" Text="NO" GroupName="Consenso2" />
                                                <br />
                                                <br />
                                                <asp:Panel ID="PN_LISITINO" runat="server" Visible="false" Height="1024px">
                                                    <br />
                                                    <label class="LabelCampo">Listino :</label>
                                                    <asp:DropDownList ID="DD_Lisitino" runat="server"></asp:DropDownList><br />
                                                    <br />
                                                    <label class="LabelCampo">
                                                        <asp:Label ID="LblListinoPrivatoSinatario" runat="server" Text="Privato Sanitario :"></asp:Label></label>
                                                    <asp:DropDownList ID="DD_PrivatoSanitario" runat="server"></asp:DropDownList><br />
                                                    <br />
                                                    <label class="LabelCampo">
                                                        <asp:Label ID="lblListinoSan" runat="server" Text="Sanitario :"></asp:Label></label>
                                                    <asp:DropDownList ID="DD_LisitinoSanitario" runat="server"></asp:DropDownList><br />
                                                    <br />
                                                    <label class="LabelCampo">
                                                        <asp:Label ID="lblListinoSociale" runat="server" Text="Sociale :"></asp:Label></label>
                                                    <asp:DropDownList ID="DD_LisitinoSociale" runat="server"></asp:DropDownList><br />
                                                    <br />
                                                    <label class="LabelCampo">
                                                        <asp:Label ID="lblListinoJolly" runat="server" Text="Jolly :"></asp:Label></label>
                                                    <asp:DropDownList ID="DD_LisitinoJolly" runat="server"></asp:DropDownList><br />
                                                    <br />
                                                    <br />
                                                </asp:Panel>
                                                <asp:Panel ID="PN_DETTAGLIO" runat="server" Visible="true">
                                                    <br />
                                                    <label class="LabelCampo">Retta Totale :</label>
                                                    <asp:TextBox ID="Txt_RettaTotale" Style="border-color: Red; text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="112px"></asp:TextBox>
                                                    <asp:DropDownList ID="cmb_tiporettaT" runat="server">
                                                        <asp:ListItem Value="G">Giornaliera</asp:ListItem>
                                                        <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                        <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Regione :</label>
                                                    <asp:DropDownList ID="Cmb_regione" runat="server" Width="296px"></asp:DropDownList>
                                                    Tipo Retta :
      <asp:DropDownList ID="DD_TipoRetta" runat="server" Width="100px"></asp:DropDownList>
                                                    <font color="red">(indicare regione solo se non auto)</font>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Modalità di Calcolo : </label>
                                                    <asp:DropDownList ID="Cmb_Modalita" runat="server">
                                                        <asp:ListItem Value="O">Differenza Ospite</asp:ListItem>
                                                        <asp:ListItem Value="C">Differenza Comune</asp:ListItem>
                                                        <asp:ListItem Value="P">Differenza Parente</asp:ListItem>
                                                        <asp:ListItem Value="E">Differenza Ente</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Importo Retta Ospite : </label>
                                                    <asp:TextBox ID="Txt_RettaOspite" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="112px"></asp:TextBox>
                                                    <asp:DropDownList ID="cmb_TiporettaO" runat="server">
                                                        <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                        <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                        <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Comune : </label>
                                                    <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList>

                                                    Importo :<asp:TextBox ID="Txt_ImportoComune" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="80px"></asp:TextBox>

                                                    <asp:DropDownList ID="DD_TipoImportoC" runat="server">
                                                        <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                        <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                        <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    Dati Parente <font color="red">(Inserire solo se parente pagante)</font>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Cognome Nome : </label>
                                                    <asp:TextBox ID="Txt_CognomeNomeParente" runat="server" Width="288px"></asp:TextBox>
                                                    <br />
                                                    <br />
                                                    <label class="LabelCampo">Retta Parente :</label>
                                                    <asp:TextBox ID="Txt_ImportoParente" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="80px"></asp:TextBox>
                                                    <asp:DropDownList ID="Cmb_TipoImportoP" runat="server">
                                                        <asp:ListItem Value="G">Giornaliera</asp:ListItem>
                                                        <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                        <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    Consenso all’inserimento dei dati personali in Senior (l’informativa è stata compresa e firmata dall’interessato):
      <asp:RadioButton ID="RB_SIConsenso1P" runat="server" Text="SI" GroupName="Consenso1P" />
                                                    <asp:RadioButton ID="RB_NOConsenso1P" runat="server" Text="NO" GroupName="Consenso1P" />
                                                    <br />
                                                    <br />
                                                    Consenso al trattamento per finalita’ di marketing :
      <asp:RadioButton ID="RB_SIConsenso2P" runat="server" Text="SI" GroupName="Consenso2P" />
                                                    <asp:RadioButton ID="RB_NOConsenso2P" runat="server" Text="NO" GroupName="Consenso2P" />
                                                    <br />
                                                    <br />

                                                </asp:Panel>
                                                <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="504px"></asp:Label>
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
