﻿
Partial Class OspitiWeb_RaggruppamentoOperatori
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Causalifattura As New Cls_CausaleContabile

        Causalifattura.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_Causale)

        Dim Ritenute As New ClsRitenuta

        Ritenute.UpDateDropBox(Session("DC_TABELLE"), DD_TipoRitenuta)


        Dim AliquoteIVA As New Cls_IVA

        AliquoteIVA.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

        If Request.Item("CODICE") = "" Then
            Call EseguiJS()

            Dim MaxRaggruppamentoOperatori As New Cls_RaggruppamentoOperatori

            Txt_Codice.Text = MaxRaggruppamentoOperatori.MaxTipoExtra(Session("DC_OSPITE"))
            Exit Sub
        End If



        Dim TpAd As New Cls_RaggruppamentoOperatori


        TpAd.Codice = Request.Item("CODICE")
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Txt_Codice.Text = TpAd.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = TpAd.Descrizione

        DD_Causale.SelectedValue = TpAd.CausaleContabile
        DD_IVA.SelectedValue = TpAd.CodiceIVA
        DD_TipoRitenuta.SelectedValue = TpAd.CodiceRitenuta



        Dim PianoConti As New Cls_Pianodeiconti

        PianoConti.Mastro = TpAd.Mastro
        PianoConti.Conto = TpAd.Conto
        PianoConti.Sottoconto = TpAd.Sottoconto
        PianoConti.Decodfica(Session("DC_GENERALE"))

        Txt_Conto.Text = TpAd.Mastro & " " & TpAd.Conto & " " & TpAd.Sottoconto & " " & PianoConti.Descrizione


        Txt_Percentuale.Text = Format(TpAd.Percentuale, "#,##0.00")
        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If

        Dim TpAd As New Cls_RaggruppamentoOperatori


        TpAd.Codice = Txt_Codice.Text
        TpAd.Descrizione = Txt_Descrizione.Text

        TpAd.CausaleContabile = DD_Causale.SelectedValue
        TpAd.CodiceIVA = DD_IVA.SelectedValue
        TpAd.CodiceRitenuta = DD_TipoRitenuta.SelectedValue



        Dim Vettore(100) As String

        If Txt_Conto.Text <> "" Then
            Vettore = SplitWords(Txt_Conto.Text)
            If Vettore.Length > 1 Then
                TpAd.Mastro = Vettore(0)
                TpAd.Conto = Vettore(1)
                TpAd.Sottoconto = Vettore(2)
            End If
        End If

        Try
            TpAd.Percentuale = Txt_Percentuale.Text
        Catch ex As Exception
            TpAd.Percentuale = 0
        End Try


        TpAd.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If
        Dim TpAd As New Cls_RaggruppamentoOperatori

        TpAd.Codice = Txt_Codice.Text
        TpAd.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_RaggruppamentoOperatori.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True
        Dim MaxRaggruppamentoOperatori As New Cls_RaggruppamentoOperatori

        Txt_Codice.Text = MaxRaggruppamentoOperatori.MaxTipoExtra(Session("DC_OSPITE"))

        Call Txt_Codice_TextChanged(sender, e)

        Call Txt_Descrizione_TextChanged(sender, e)

        Call EseguiJS()
    End Sub




    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_RaggruppamentoOperatori

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_OSPITE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_RaggruppamentoOperatori

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_OSPITE"), x.Descrizione)

                If x.Codice <> "" Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub


End Class
