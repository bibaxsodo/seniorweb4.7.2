﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_MovimentiDiurniCentroServizio" CodeFile="MovimentiDiurniCentroServizio.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Movimenti Diurni Centro Servizio</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function deletebox(cella) {
            $("div").remove("#RegistrazioneViewer");
            $("#RegistrazioneViewer").remove();
        }
        function apribox(CodiceOspite, Cserv, Anno, Mese, cella) {
            var posizione = $("#" + cella).position().top + 30;

            if ($(document).height() < (posizione + 100)) {
                posizione = posizione - 200;
            }

            $.ajax({
                url: "DatiDiurno.ashx?CodiceOspite=" + CodiceOspite + "&Cserv=" + Cserv + "&Anno=" + Anno + "&Mese=" + Mese,
                success: function (data, stato) {

                    $("div").remove("#RegistrazioneViewer");
                    $("#RegistrazioneViewer").remove();

                    $("body").append(data);
                    $("#RegistrazioneViewer").css('top', posizione + 30);
                    $("#RegistrazioneViewer").css('left', 300);
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        var GlbColore = 0;

        function leggicella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA) {
            setTimeout(function () {
                AJAXleggicella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA)
            }, GIORNO * 100);
        }


        function AJAXleggicella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA) {

            $.ajax({
                url: "ColoreDiurno.ashx?CSERV=" + CSERV + "&CODOSP=" + CODOSP + "&ANNO=" + ANNO + "&MESE=" + MESE + "&GIORNO=" + GIORNO,
                success: function (data, stato) {
                    JsonDati = eval('(' + data + ')');
                    if (JsonDati.Colore == null) { JsonDati.Colore = 0; }

                    if (isNaN(JsonDati.Colore)) { JsonDati.Colore = 0; }

                    if (JsonDati.Colore != 0) {
                        Appoggio = "<label style='position: relative;'>" + JsonDati.Causale + "</label><br/><img src='images/Diurno_" + JsonDati.Colore + ".png' style='Width:20px; Height:20px;'  title='" + JsonDati.Descrizione + "' />";
                        $("#" + CELLA).html(Appoggio);

                    } else {
                        Appoggio = "";
                        if (JsonDati.Causale != "") {
                            if (JsonDati.Causale == null) { JsonDati.Causale = ''; }

                            if (isNaN(JsonDati.Causale)) { JsonDati.Causale = ''; }

                            Appoggio = "<span title='" + JsonDati.Descrizione + "' style='witdh:34px;'>" + JsonDati.Causale + "</span><img src='images/Diurno_Blanco.png' style='Width:20px; Height:20px;'  title='Presente' />";
                        }

                        if (Appoggio == '') {
                            Appoggio = '&nbsp;&nbsp;&nbsp;';
                        }
                        $("#" + CELLA).html(Appoggio);
                    }

                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }
        function selezionacolore(colore) {
            $("#Colore" + GlbColore).css("border-color", "black");
            GlbColore = colore;
            $("#Colore" + GlbColore).css("border-color", "red");
        }
        function apricella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA) {
            $.ajax({
                url: "CambiaCellaDiurno.ashx?CSERV=" + CSERV + "&CODOSP=" + CODOSP + "&ANNO=" + ANNO + "&MESE=" + MESE + "&GIORNO=" + GIORNO + "&COLORE=" + GlbColore,
                success: function (data, stato) {
                    leggicella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA);
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Servizio -  Movimenti Diurni Centro Servizio</div>
                                <div class="SottoTitolo">
                                    <br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <div class="DivTasti">
                                    <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                                <br />
                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                                    Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Movimenti Diurni Centro Servizio      
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="392px"></asp:Label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label class="LabelCampo">Anno :</label>
                                                        <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                                    </td>
                                                    <td>
                                                        <label class="LabelCampo">Mese :</label>
                                                        <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px" AutoPostBack="true">
                                                            <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                            <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                            <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                            <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                            <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                            <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                            <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                            <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                            <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                            <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                        </asp:DropDownList><br />
                                                    </td>
                                                    <td>
                                                        <label class="LabelCampo">Giorni Dal :</label>
                                                        <asp:TextBox ID="Txt_Dal" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                                        Al 
        <asp:TextBox ID="Txt_Al" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />

                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <label class="LabelCampo">Struttura:</label>
                                                        <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                                    </td>
                                                    <td>
                                                        <label class="LabelCampo">Centro Servizio : </label>
                                                        <asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <asp:Label ID="Lbl_Schema" runat="server"></asp:Label>
                                            <br />
                                        </ContentTemplate>
                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>
                    <div id="Div1">&nbsp;</div>
                    <div id="pippo" class="wait">
                        <br />
                        <img height="30px" src="images/loading.gif"><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
