﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data



Partial Class OspitiWeb_GeneraAllegati
    Inherits System.Web.UI.Page
    Public Allegati As New StampeOspiti


    Class RigaStampa
        Public CodiceServizio As String
        Public DecodificaCentroServizio As String
        Public CODICEOSPITE As String
        Public NOME As String
        Public DATANASCITA As String
        Public CODICEFISCALE As String
        Public MESE As String
        Public ANNO As String
        Public STATOAUTO As String
        Public GIORNIPRESENZA As String
        Public GIORNIASSENZA As String
        Public ImportoOspitePresenze As String
        Public ImportoOspiteAssenze As String
        Public ImportoParentiPresenze As String
        Public ImportoParentiAssenze As String
        Public CodiceComuneFatturazione As String
        Public DecodificaComuneFatturazione As String
        Public CodiceRegioneFatturazione As String
        Public DecodificaRegioneFatturazione As String
        Public ImportoComunePresenze As String
        Public ImportoComuneAssenze As String
        Public CodiceJolly As String
        Public DecodificaJolly As String
        Public ImportoJollyPresenze As String
        Public ImportoJollyAssenze As String
        Public ComuneResidenza As String
        Public Isee As String
        Public Accompagnamento As String
        Public DataAccoglimento As Date
        Public DataDimissione As Date
        Public MotivoDimissione As String
        Public Giorno1 As String
        Public Giorno2 As String
        Public Giorno3 As String
        Public Giorno4 As String
        Public Giorno5 As String
        Public Giorno6 As String
        Public Giorno7 As String
        Public Giorno8 As String
        Public Giorno9 As String
        Public Giorno10 As String
        Public Giorno11 As String
        Public Giorno12 As String
        Public Giorno13 As String
        Public Giorno14 As String
        Public Giorno15 As String
        Public Giorno16 As String
        Public Giorno17 As String
        Public Giorno18 As String
        Public Giorno19 As String
        Public Giorno20 As String
        Public Giorno21 As String
        Public Giorno22 As String
        Public Giorno23 As String
        Public Giorno24 As String
        Public Giorno25 As String
        Public Giorno26 As String
        Public Giorno27 As String
        Public Giorno28 As String
        Public Giorno29 As String
        Public Giorno30 As String
        Public Giorno31 As String
        Public GiorniPresTP As String
        Public QuotaPresTP As String
        Public GiorniAssTP As String
        Public QuotaAssTP As String
        Public GiorniPresPT As String
        Public QuotaPresPT As String
        Public GiorniAssPT As String
        Public QuotaAssPT As String
        Public ComuneNascita As String
        Public TipoRettautente As String
        Public TipoRettaRegione As String
        Public Uscita1 As String
        Public Causale1 As String
        Public Entrata1 As String
        Public Uscita2 As String
        Public Causale2 As String
        Public Entrata2 As String
        Public Uscita3 As String
        Public Causale3 As String
        Public Entrata3 As String
        Public CodiceCartellaClinica As String
        Public ExtraFissiOspiteParente As String
        Public ImportiExtraFissiOspiteParente As Double
        Public ExtraFissiComune As String
        Public ImportiExtraFissiComune As Double

        Public ImportoRegionePresenze As Double
        Public ImportoRegioneAssenze As Double
    End Class



    Class DatiRigaRegione
        Public CodiceRegione As String = ""
        Public TipoRetta As String = ""
        Public ImportoPresenze As Double = 0
        Public ImportoAssenze As Double = 0
        Public GiorniPresenze As Double = 0
        Public GiorniAssenza As Double = 0
    End Class

    Class DatiRigheRegione
        Public Riga(100) As DatiRigaRegione
    End Class





    Private Sub RiempiTabella()


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then Exit Sub


        Txt_Anno.Text = Year(Now)



        DD_MeseDa.Items.Add("Gennaio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 1
        DD_MeseDa.Items.Add("Febbraio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 2
        DD_MeseDa.Items.Add("Marzo")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 3
        DD_MeseDa.Items.Add("Aprile")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 4
        DD_MeseDa.Items.Add("Maggio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 5
        DD_MeseDa.Items.Add("Giugno")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 6
        DD_MeseDa.Items.Add("Luglio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 7
        DD_MeseDa.Items.Add("Agosto")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 8
        DD_MeseDa.Items.Add("Settembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 9
        DD_MeseDa.Items.Add("Ottobre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 10
        DD_MeseDa.Items.Add("Novembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 11
        DD_MeseDa.Items.Add("Dicembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 12


        DD_MeseA.Items.Add("Gennaio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 1
        DD_MeseA.Items.Add("Febbraio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 2
        DD_MeseA.Items.Add("Marzo")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 3
        DD_MeseA.Items.Add("Aprile")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 4
        DD_MeseA.Items.Add("Maggio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 5
        DD_MeseA.Items.Add("Giugno")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 6
        DD_MeseA.Items.Add("Luglio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 7
        DD_MeseA.Items.Add("Agosto")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 8
        DD_MeseA.Items.Add("Settembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 9
        DD_MeseA.Items.Add("Ottobre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 10
        DD_MeseA.Items.Add("Novembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 11
        DD_MeseA.Items.Add("Dicembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 12


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, Nothing)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)


        Dim Xcs As New Cls_CentroServizio

        Xcs.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)

        Dim XOs As New ClsUSL

        XOs.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)

        Dim XOc As New ClsComune


        XOc.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub Cerca()
        Dim MySql As String
        Dim Mese As Integer = 0
        Dim Stampa As New StampeOspiti


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        For Mese = DD_MeseDa.SelectedValue To DD_MeseA.SelectedValue

            DatiComune(Val(Txt_Anno.Text), Mese, cn)

        Next
        cn.Close()

    End Sub



    'Private Sub DatiRegione(ByVal CentroServizio As String, ByVal CodiceOspite As Integer, ByVal Anno As Integer, ByVal Mese As Integer, ByRef cn As OleDbConnection, ByRef RigheR As DatiRigheRegione)
    '    Dim CodiceRegione As String = ""
    '    Dim CodiceExtra As String = ""
    '    Dim ImportoPresenze As Double = ""
    '    Dim ImportoAssenze As Double = ""
    '    Dim GiorniPresenze As Double = ""
    '    Dim GiorniAssenza As Double = ""
    '    Dim Riga As Integer = 0

    '    Dim CmdRegione As New OleDbCommand()


    '    CmdRegione.CommandText = "Select * From RetteRegione Where CentroServizio =? And CodiceOspite= ? And Anno = ? And Mese = ? Order by REGIONE,CodiceExtra"
    '    CmdRegione.Connection = cn
    '    CmdRegione.Parameters.AddWithValue("@CentroServizio", CentroServizio)
    '    CmdRegione.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
    '    CmdRegione.Parameters.AddWithValue("@Anno", Anno)
    '    CmdRegione.Parameters.AddWithValue("@Mese", Mese)

    '    Dim Rs_Rette As OleDbDataReader = CmdRegione.ExecuteReader()
    '    Do While Rs_Rette.Read
    '        Dim VerificoRegione As String = ""
    '        Dim VerificaExtra As String = ""

    '        VerificoRegione = campodb(Rs_Rette.Item("REGIONE"))
    '        VerificaExtra = campodb(Rs_Rette.Item("CodiceExtra"))
    '        If VerificaExtra <> CodiceRegione Or VerificoRegione <> CodiceRegione Then
    '            Riga = Riga + 1
    '        End If

    '        CodiceRegione = campodb(Rs_Rette.Item("REGIONE"))
    '        CodiceExtra = campodb(Rs_Rette.Item("CodiceExtra"))

    '        RigheR.Riga(Riga).CodiceRegione = CodiceRegione
    '        RigheR.Riga(Riga).TipoRetta = CodiceExtra


    '        If campodb(Rs_Rette.Item("ELEMENTO")) = "RGP" Then
    '            RigheR.Riga(Riga).GiorniPresenze = RigheR.Riga(Riga).GiorniPresenze + campodbn(Rs_Rette.Item("GIORNI"))
    '            RigheR.Riga(Riga).ImportoPresenze = RigheR.Riga(Riga).ImportoPresenze + campodbn(Rs_Rette.Item("IMPORTO"))
    '        End If

    '        If campodb(Rs_Rette.Item("ELEMENTO")) = "RGA" Then
    '            RigheR.Riga(Riga).GiorniAssenza = RigheR.Riga(Riga).GiorniAssenza + campodbn(Rs_Rette.Item("GIORNI"))
    '            RigheR.Riga(Riga).ImportoAssenze = RigheR.Riga(Riga).ImportoAssenze + campodbn(Rs_Rette.Item("IMPORTO"))
    '        End If
    '    Loop
    '    Rs_Rette.Close()
    'End Sub

    Private Sub DatiComune(ByVal Anno As Integer, ByVal Mese As Integer, ByRef cn As OleDbConnection)
        Dim CodiceComune As String = ""
        Dim ImportoPresenze As Double = 0
        Dim ImportoAssenze As Double = 0
        Dim GiorniPresenze As Double = 0
        Dim GiorniAssenza As Double = 0
        Dim ImportiExtraFissiComune As Double = 0
        Dim ExtraFissiComune As String = ""
        Dim CodiceOspite As Integer = 0
        Dim CentroServizio As String = ""
        Dim First As Boolean = True

        Dim Riga As Integer = 0

        Dim RecordStampa As New StampeOspiti


        Dim Condizione As String = ""

        If DD_Comune.SelectedValue <> "" Then
            Dim StringaCodiceComune As String = ""

            StringaCodiceComune = DD_Comune.SelectedValue

            Dim Provincia As String
            Dim Comune As String

            Provincia = Mid(StringaCodiceComune & Space(6), 1, 3)
            Comune = Mid(StringaCodiceComune & Space(6), 5, 3)

            Condizione = " And Provincia = '" & Provincia & "' And Comune = '" & Comune & "'"
        End If

        If DD_CServ.SelectedValue <> "" Then
            Condizione = Condizione & " And CentroServizio = '" & DD_CServ.SelectedValue & "' "
        End If



        Dim CmdComune As New OleDbCommand()


        CmdComune.CommandText = "Select * From RetteComune Where Anno = ? And Mese = ? " & Condizione & " Order by CentroServizio,CodiceOspite,Provincia,Comune"
        CmdComune.Connection = cn
        CmdComune.Parameters.AddWithValue("@Anno", Anno)
        CmdComune.Parameters.AddWithValue("@Mese", Mese)

        Dim Rs_Rette As OleDbDataReader = CmdComune.ExecuteReader()
        Do While Rs_Rette.Read
            Dim VerificoComune As String = ""


            VerificoComune = campodb(Rs_Rette.Item("PROVINCIA")) & campodb(Rs_Rette.Item("COMUNE"))
            If Not First Then
                If VerificoComune <> CodiceComune Or CodiceOspite <> campodbn(Rs_Rette.Item("CodiceOspite")) Or CentroServizio <> campodb(Rs_Rette.Item("CentroServizio")) Then

                    Call EstraiDatiMeseOspite(CentroServizio, CodiceOspite, Anno, Mese, cn, CodiceComune, Math.Round(ImportoPresenze, 2), Math.Round(ImportoAssenze, 2), GiorniPresenze, GiorniAssenza, Math.Round(ImportiExtraFissiComune, 2), ExtraFissiComune, RecordStampa, "", 0, 0, 0, 0, "")
                    GiorniPresenze = 0
                    GiorniAssenza = 0
                    ImportoPresenze = 0
                    ImportoAssenze = 0
                    ExtraFissiComune = ""
                    ImportiExtraFissiComune = 0
                End If
            End If


            First = False


            CodiceComune = campodb(Rs_Rette.Item("PROVINCIA")) & campodb(Rs_Rette.Item("COMUNE"))

            CodiceOspite = campodbn(Rs_Rette.Item("CodiceOspite"))
            CentroServizio = campodb(Rs_Rette.Item("CentroServizio"))


            If Mid(campodb(Rs_Rette.Item("ELEMENTO")) & Space(10), 1, 1) = "E" Then
                Dim Extra As New Cls_TipoExtraFisso

                Extra.CODICEEXTRA = campodb(Rs_Rette.Item("CODICEEXTRA"))
                Extra.Leggi(Session("DC_OSPITE"))


                ExtraFissiComune = Extra.Descrizione

                ImportiExtraFissiComune = ImportiExtraFissiComune + campodbn(Rs_Rette.Item("Importo"))
            End If



            If campodb(Rs_Rette.Item("ELEMENTO")) = "RGP" Then
                GiorniPresenze = GiorniPresenze + campodbn(Rs_Rette.Item("GIORNI"))
                ImportoPresenze = ImportoPresenze + campodbn(Rs_Rette.Item("IMPORTO"))
            End If

            If campodb(Rs_Rette.Item("ELEMENTO")) = "RGA" Then
                GiorniAssenza = GiorniAssenza + campodbn(Rs_Rette.Item("GIORNI"))
                ImportoAssenze = ImportoAssenze + campodbn(Rs_Rette.Item("IMPORTO"))
            End If
        Loop

        If GiorniPresenze > 0 Or _
            GiorniAssenza > 0 Or _
            ImportoPresenze > 0 Or _
            ImportoAssenze > 0 Or _
            ExtraFissiComune <> "" Or _
            ImportiExtraFissiComune > 0 Then

            Call EstraiDatiMeseOspite(CentroServizio, CodiceOspite, Anno, Mese, cn, CodiceComune, Math.Round(ImportoPresenze, 2), Math.Round(ImportoAssenze, 2), GiorniPresenze, GiorniAssenza, Math.Round(ImportiExtraFissiComune, 2), ExtraFissiComune, RecordStampa, "", 0, 0, 0, 0, "")

        End If
        Rs_Rette.Close()



        Session("stampa") = RecordStampa

        Dim Tabella As New System.Data.DataTable("Tabella")

        Tabella = RecordStampa.EstrazioneAllegati



        Session("Tabelle") = Tabella


        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.Font.Size = 10
        GridView1.DataBind()


    End Sub

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub EstraiDatiMeseOspite(ByVal CentroServizio As String, ByVal CodiceOspite As Integer, ByVal Anno As Integer, ByVal Mese As Integer, ByRef cn As OleDbConnection, ByVal CodiceComune As String, ByVal ImportoPresenzeComune As Double, ByVal ImportoAssenzeComune As Double, ByVal GiorniPresenzeComune As Integer, ByVal GiorniAssenzaComune As Integer, ByVal ImportiExtraFissiComune As Double, ByVal ExtraFissiComune As String, ByRef RecordStampa As StampeOspiti, ByVal CodiceRegione As String, ByVal ImportoPresenzeRegione As Double, ByVal ImportoAssenzeRegione As Double, ByVal GiorniPresenzeRegione As Integer, ByVal GiorniAssenzaRegione As Integer, ByVal TipoImporto As String)

        Dim Righe As New RigaStampa

        Righe.CodiceServizio = CentroServizio
        Righe.CODICEOSPITE = CodiceOspite

        Dim CServ As New Cls_CentroServizio

        CServ.CENTROSERVIZIO = CentroServizio
        CServ.Leggi(Session("DC_OSPITE"), CServ.CENTROSERVIZIO)

        Righe.DecodificaCentroServizio = CServ.DESCRIZIONE


        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = CodiceOspite
        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

        Righe.NOME = Ospite.Nome

        Righe.DATANASCITA = Ospite.DataNascita
        Righe.CODICEFISCALE = Ospite.CODICEFISCALE
        Righe.MESE = Mese
        Righe.ANNO = Anno


        Dim ComuneNascita As New ClsComune

        ComuneNascita.Provincia = Ospite.ProvinciaDiNascita
        ComuneNascita.Comune = Ospite.ComuneDiNascita

        ComuneNascita.Leggi(Session("DC_OSPITE"))

        Righe.ComuneNascita = ComuneNascita.Descrizione


        Dim StatoAuto As New Cls_StatoAuto

        StatoAuto.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
        StatoAuto.CODICEOSPITE = CodiceOspite
        StatoAuto.CENTROSERVIZIO = CentroServizio
        StatoAuto.StatoPrimaData(Session("DC_OSPITE"), CodiceOspite, CentroServizio)


        Righe.STATOAUTO = StatoAuto.STATOAUTO

        Dim GiorniPresenzaOspite As Integer = 0


        Dim GiorniAssenzaOspite As Integer = 0

        Dim ImportoPresenzaOspite As Double = 0
        Dim ImportoAssenzaOspite As Double = 0

        Dim ImportoPresenzeJolly As Double = 0
        Dim ImportoAssenzeJolly As Double = 0
        Dim GiorniAssenzaJolly As Integer
        Dim GiorniPresenzaJolly As Integer
        Dim CodiceJolly As String = ""


        If ImportoPresenzeJolly = 0 And ImportoAssenzeJolly = 0 Then

            Dim CmdJolly As New OleDbCommand()


            CmdJolly.CommandText = "Select * From RetteJolly Where (CentroServizio =? And CodiceOspite= ? And Anno = ? And Mese = ?)  Order by ID"
            CmdJolly.Connection = cn
            CmdJolly.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            CmdJolly.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            CmdJolly.Parameters.AddWithValue("@Anno", Anno)
            CmdJolly.Parameters.AddWithValue("@Mese", Mese)


            Dim Rs_RetteJolly As OleDbDataReader = CmdJolly.ExecuteReader()
            Do While Rs_RetteJolly.Read

                If campodb(Rs_RetteJolly.Item("ELEMENTO")) = "RGP" Then
                    GiorniPresenzaJolly = GiorniPresenzaJolly + campodbn(Rs_RetteJolly.Item("GIORNI"))
                    ImportoPresenzeJolly = ImportoPresenzeJolly + campodbn(Rs_RetteJolly.Item("IMPORTO"))
                End If
                If campodb(Rs_RetteJolly.Item("ELEMENTO")) = "RGA" Then
                    GiorniAssenzaJolly = GiorniAssenzaJolly + campodbn(Rs_RetteJolly.Item("GIORNI"))
                    ImportoAssenzaOspite = ImportoAssenzaOspite + campodbn(Rs_RetteJolly.Item("IMPORTO"))
                End If

                CodiceJolly = campodb(Rs_RetteJolly.Item("PROVINCIA")) & " " & campodb(Rs_RetteJolly.Item("COMUNE"))
            Loop
            Rs_RetteJolly.Close()
        End If

        Righe.ImportoJollyAssenze = ImportoAssenzeJolly
        Righe.ImportoJollyPresenze = ImportoPresenzeJolly
        Righe.CodiceJolly = CodiceJolly



        If ImportoPresenzeComune = 0 And ImportoAssenzeComune = 0 Then

            Dim CmdComune As New OleDbCommand()


            CmdComune.CommandText = "Select * From RetteComune Where (CentroServizio =? And CodiceOspite= ? And Anno = ? And Mese = ?)  Order by ID"
            CmdComune.Connection = cn
            CmdComune.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            CmdComune.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            CmdComune.Parameters.AddWithValue("@Anno", Anno)
            CmdComune.Parameters.AddWithValue("@Mese", Mese)


            Dim Rs_RetteComune As OleDbDataReader = CmdComune.ExecuteReader()
            Do While Rs_RetteComune.Read
                If campodb(Rs_RetteComune.Item("ELEMENTO")) = "RPX" Then
                    GiorniPresenzeComune = GiorniPresenzeComune + campodbn(Rs_RetteComune.Item("GIORNI"))
                    ImportoPresenzeComune = ImportoPresenzeComune + campodbn(Rs_RetteComune.Item("IMPORTO"))
                End If
                If campodb(Rs_RetteComune.Item("ELEMENTO")) = "RGP" Then
                    GiorniPresenzeComune = GiorniPresenzeComune + campodbn(Rs_RetteComune.Item("GIORNI"))
                    ImportoPresenzeComune = ImportoPresenzeComune + campodbn(Rs_RetteComune.Item("IMPORTO"))
                End If
                If campodb(Rs_RetteComune.Item("ELEMENTO")) = "RGA" Then
                    GiorniAssenzaOspite = GiorniAssenzaOspite + campodbn(Rs_RetteComune.Item("GIORNI"))
                    ImportoAssenzaOspite = ImportoAssenzaOspite + campodbn(Rs_RetteComune.Item("IMPORTO"))
                End If

                If Mid(campodb(Rs_RetteComune.Item("ELEMENTO")) & Space(10), 1, 1) = "E" Then
                    Dim Extra As New Cls_TipoExtraFisso

                    Extra.CODICEEXTRA = campodb(Rs_RetteComune.Item("CODICEEXTRA"))
                    Extra.Leggi(Session("DC_OSPITE"))


                    Righe.ExtraFissiOspiteParente = Extra.Descrizione

                    Righe.ImportiExtraFissiOspiteParente = Righe.ImportiExtraFissiOspiteParente + campodbn(Rs_RetteComune.Item("Importo"))
                End If


                CodiceComune = campodb(Rs_RetteComune.Item("PROVINCIA")) & campodb(Rs_RetteComune.Item("COMUNE"))
            Loop
            Rs_RetteComune.Close()
        End If



        If ImportoPresenzeRegione = 0 And ImportoAssenzeRegione = 0 Then           
            Dim CmdRegione As New OleDbCommand()


            CmdRegione.CommandText = "Select * From RetteRegione Where (CentroServizio =? And CodiceOspite= ? And Anno = ? And Mese = ?)  Order by ID"
            CmdRegione.Connection = cn
            CmdRegione.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            CmdRegione.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            CmdRegione.Parameters.AddWithValue("@Anno", Anno)
            CmdRegione.Parameters.AddWithValue("@Mese", Mese)


            Dim Rs_RetteRegione As OleDbDataReader = CmdRegione.ExecuteReader()
            Do While Rs_RetteRegione.Read
                If campodb(Rs_RetteRegione.Item("ELEMENTO")) = "RPX" Then
                    GiorniPresenzeRegione = GiorniPresenzeRegione + campodbn(Rs_RetteRegione.Item("GIORNI"))
                    ImportoPresenzeRegione = ImportoPresenzeRegione + campodbn(Rs_RetteRegione.Item("IMPORTO"))
                End If
                If campodb(Rs_RetteRegione.Item("ELEMENTO")) = "RGP" Then
                    GiorniPresenzeRegione = GiorniPresenzeRegione + campodbn(Rs_RetteRegione.Item("GIORNI"))
                    ImportoPresenzeRegione = ImportoPresenzeRegione + campodbn(Rs_RetteRegione.Item("IMPORTO"))
                End If
                If campodb(Rs_RetteRegione.Item("ELEMENTO")) = "RGA" Then
                    GiorniAssenzaRegione = GiorniAssenzaRegione + campodbn(Rs_RetteRegione.Item("GIORNI"))
                    ImportoAssenzeRegione = ImportoAssenzeRegione + campodbn(Rs_RetteRegione.Item("IMPORTO"))
                End If

                CodiceRegione = campodb(Rs_RetteRegione.Item("REGIONE"))
            Loop
            Rs_RetteRegione.Close()


        End If

        Righe.CodiceRegioneFatturazione = CodiceRegione

        Dim Usl As New ClsUSL

        Usl.CodiceRegione = CodiceRegione
        Usl.Leggi(Session("DC_OSPITE"))

        Righe.DecodificaRegioneFatturazione = Usl.Nome


        Righe.ImportoRegioneAssenze = Math.Round(ImportoAssenzeRegione, 2)

        Righe.ImportoRegionePresenze = Math.Round(ImportoPresenzeRegione, 2)


        Dim CmdOspite As New OleDbCommand()

        GiorniPresenzaOspite = 0
        GiorniAssenzaOspite = 0
        ImportoPresenzaOspite = 0
        ImportoAssenzaOspite = 0

        CmdOspite.CommandText = "Select * From RetteOspite Where CentroServizio =? And CodiceOspite= ? And ((Anno = ? And Mese = ?) Or (Elemento = 'RPX' AND ANNOCOMPETENZA =? And  MESECOMPETENZA = ?)) Order by ID"
        CmdOspite.Connection = cn
        CmdOspite.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        CmdOspite.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        CmdOspite.Parameters.AddWithValue("@Anno", Anno)
        CmdOspite.Parameters.AddWithValue("@Mese", Mese)
        CmdOspite.Parameters.AddWithValue("@Anno", Anno)
        CmdOspite.Parameters.AddWithValue("@Mese", Mese)

        Dim Rs_Rette As OleDbDataReader = CmdOspite.ExecuteReader()
        Do While Rs_Rette.Read
            If campodb(Rs_Rette.Item("ELEMENTO")) = "RPX" And campodb(Rs_Rette.Item("MESECOMPETENZA")) = Mese And campodb(Rs_Rette.Item("ANNOCOMPETENZA")) = Anno Then
                GiorniPresenzaOspite = GiorniPresenzaOspite + campodbn(Rs_Rette.Item("GIORNI"))
                ImportoPresenzaOspite = ImportoPresenzaOspite + campodbn(Rs_Rette.Item("IMPORTO"))
            End If
            If campodb(Rs_Rette.Item("ELEMENTO")) = "RGP" Then
                GiorniPresenzaOspite = GiorniPresenzaOspite + campodbn(Rs_Rette.Item("GIORNI"))
                ImportoPresenzaOspite = ImportoPresenzaOspite + campodbn(Rs_Rette.Item("IMPORTO"))
            End If
            If campodb(Rs_Rette.Item("ELEMENTO")) = "RGA" Then
                GiorniAssenzaOspite = GiorniAssenzaOspite + campodbn(Rs_Rette.Item("GIORNI"))
                ImportoAssenzaOspite = ImportoAssenzaOspite + campodbn(Rs_Rette.Item("IMPORTO"))
            End If

            If Mid(campodb(Rs_Rette.Item("ELEMENTO")) & Space(10), 1, 1) = "E" Then
                Dim Extra As New Cls_TipoExtraFisso

                Extra.CODICEEXTRA = campodb(Rs_Rette.Item("CODICEEXTRA"))
                Extra.Leggi(Session("DC_OSPITE"))


                Righe.ExtraFissiOspiteParente = Extra.Descrizione

                Righe.ImportiExtraFissiOspiteParente = Righe.ImportiExtraFissiOspiteParente + campodbn(Rs_Rette.Item("Importo"))
            End If

        Loop
        Rs_Rette.Close()


        Dim GiorniPresenzaParente As Integer = 0
        Dim GiorniAssenzaParente As Integer = 0

        Dim ImportoPresenzaParente As Double = 0
        Dim ImportoAssenzaParente As Double = 0

        Dim CmdParente As New OleDbCommand()


        CmdParente.CommandText = "Select * From RetteParente Where (CentroServizio =? And CodiceOspite= ?) And  ((Anno = ? And Mese = ?) Or (Elemento = 'RPX' AND ANNOCOMPETENZA =? And  MESECOMPETENZA = ?)) Order by ID"
        CmdParente.Connection = cn
        CmdParente.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        CmdParente.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        CmdParente.Parameters.AddWithValue("@Anno", Anno)
        CmdParente.Parameters.AddWithValue("@Mese", Mese)
        CmdParente.Parameters.AddWithValue("@Anno", Anno)
        CmdParente.Parameters.AddWithValue("@Mese", Mese)

        Dim Rs_RetteParente As OleDbDataReader = CmdParente.ExecuteReader()
        Do While Rs_RetteParente.Read
            If campodb(Rs_RetteParente.Item("ELEMENTO")) = "RPX" And campodb(Rs_RetteParente.Item("MESECOMPETENZA")) = Mese And campodb(Rs_RetteParente.Item("ANNOCOMPETENZA")) = Anno Then
                GiorniPresenzaOspite = GiorniPresenzaOspite + campodbn(Rs_RetteParente.Item("GIORNI"))
                ImportoPresenzaOspite = ImportoPresenzaOspite + campodbn(Rs_RetteParente.Item("IMPORTO"))
            End If
            If campodb(Rs_RetteParente.Item("ELEMENTO")) = "RGP" Then
                GiorniPresenzaOspite = GiorniPresenzaOspite + campodbn(Rs_RetteParente.Item("GIORNI"))
                ImportoPresenzaOspite = ImportoPresenzaOspite + campodbn(Rs_RetteParente.Item("IMPORTO"))
            End If
            If campodb(Rs_RetteParente.Item("ELEMENTO")) = "RGA" Then
                GiorniAssenzaOspite = GiorniAssenzaOspite + campodbn(Rs_RetteParente.Item("GIORNI"))
                ImportoAssenzaOspite = ImportoAssenzaOspite + campodbn(Rs_RetteParente.Item("IMPORTO"))
            End If
            If Mid(campodb(Rs_RetteParente.Item("ELEMENTO")) & Space(10), 1, 1) = "E" Then
                Dim Extra As New Cls_TipoExtraFisso

                Extra.CODICEEXTRA = campodb(Rs_RetteParente.Item("CODICEEXTRA"))
                Extra.Leggi(Session("DC_OSPITE"))


                Righe.ExtraFissiOspiteParente = Extra.Descrizione

                Righe.ImportiExtraFissiOspiteParente = Righe.ImportiExtraFissiOspiteParente + Math.Round(campodbn(Rs_RetteParente.Item("Importo")), 2)
            End If
        Loop
        Rs_RetteParente.Close()


        Righe.GIORNIPRESENZA = GiorniPresenzaOspite
        Righe.GIORNIASSENZA = GiorniAssenzaOspite

        If GiorniAssenzaOspite = 0 And GiorniPresenzaOspite = 0 Then
            Righe.GIORNIPRESENZA = GiorniPresenzaParente
            Righe.GIORNIASSENZA = GiorniAssenzaParente
        End If


        Righe.ImportoOspitePresenze = Math.Round(ImportoPresenzaOspite, 2)
        Righe.ImportoOspiteAssenze = Math.Round(ImportoAssenzaOspite, 2)


        Righe.ImportoParentiPresenze = Math.Round(ImportoPresenzaParente, 2)
        Righe.ImportoParentiAssenze = Math.Round(ImportoAssenzaParente, 2)

        Dim DecodificaComuneResidenza As New ClsComune


        DecodificaComuneResidenza.Provincia = Ospite.RESIDENZAPROVINCIA1
        DecodificaComuneResidenza.Comune = Ospite.RESIDENZACOMUNE1
        DecodificaComuneResidenza.Leggi(Session("DC_OSPITE"))

        Righe.ComuneResidenza = DecodificaComuneResidenza.Descrizione

        Dim Ise As New Cls_Ise

        Ise.CODICEOSPITE = CodiceOspite
        Ise.CENTROSERVIZIO = CentroServizio
        Ise.UltimaData(Session("DC_OSPITE"), Ise.CODICEOSPITE, Ise.CENTROSERVIZIO)

        Righe.Isee = Ise.ISEE

        Dim Accompagnamento As New Cls_Accompagnamento

        Accompagnamento.CODICEOSPITE = CodiceOspite
        Accompagnamento.CENTROSERVIZIO = CentroServizio
        Accompagnamento.LeggiAData(Session("DC_OSPITE"), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

        Righe.Accompagnamento = Accompagnamento.Accompagnamento


        Dim Movimenti As New Cls_Movimenti

        Movimenti.Data = Nothing
        Movimenti.CodiceOspite = CodiceOspite
        Movimenti.CENTROSERVIZIO = CentroServizio
        Movimenti.Data = DateSerial(Anno, Mese, 1)
        Movimenti.UltimaDataAccoglimento(Session("DC_OSPITE"))



        Righe.DataAccoglimento = Movimenti.Data
        If IsNothing(Movimenti.Data) Then


            Movimenti.CodiceOspite = CodiceOspite
            Movimenti.CENTROSERVIZIO = CentroServizio
            Movimenti.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
            Movimenti.UltimaDataAccoglimento(Session("DC_OSPITE"))

            Righe.DataAccoglimento = Movimenti.Data
        End If



        Movimenti.Causale = ""
        Movimenti.Data = Nothing
        Movimenti.CodiceOspite = CodiceOspite
        Movimenti.CENTROSERVIZIO = CentroServizio
        Movimenti.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
        Movimenti.UltimaDataUscitaDefinitivaMese(Session("DC_OSPITE"), Anno, Mese)


        If Format(Righe.DataAccoglimento, "yyyyMMdd") < Format(Movimenti.Data, "yyyyMMdd") Then
            Righe.DataDimissione = Movimenti.Data
        End If


        If Movimenti.Causale <> "" Then

            Dim CauEU As New Cls_CausaliEntrataUscita

            CauEU.Codice = Movimenti.Causale
            CauEU.LeggiCausale(Session("DC_OSPITE"))

            Righe.MotivoDimissione = CauEU.Descrizione

        End If

        GioniMese(cn, CodiceOspite, CentroServizio, Mese, Anno, Righe)

        Dim RettaTotale As New Cls_rettatotale

        RettaTotale.CODICEOSPITE = CodiceOspite
        RettaTotale.CENTROSERVIZIO = CentroServizio
        RettaTotale.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        If RettaTotale.TipoRetta <> "" Then
            Dim TipoRetta As New Cls_TipoRetta

            TipoRetta.Codice = RettaTotale.TipoRetta
            TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Codice)

            Righe.TipoRettautente = TipoRetta.Descrizione
        End If



        UsciteTemporanee(cn, CodiceOspite, CentroServizio, Mese, Anno, Righe)

        Righe.CodiceCartellaClinica = Ospite.CartellaClinica



        Dim RigaStampa As System.Data.DataRow = RecordStampa.Tables("EstrazioneAllegati").NewRow


        Dim StringaCodiceComune As String = ""

        StringaCodiceComune = CodiceComune

        Dim Provincia As String
        Dim Comune As String

        Provincia = Mid(StringaCodiceComune & Space(6), 1, 3)
        Comune = Mid(StringaCodiceComune & Space(6), 4, 3)

        Dim LeggiEnte As New ClsComune

        LeggiEnte.Provincia = Provincia
        LeggiEnte.Comune = Comune
        LeggiEnte.Leggi(Session("DC_OSPITE"))


        RigaStampa.Item("CodiceComuneFatturazione") = CodiceComune
        RigaStampa.Item("DecodificaComuneFatturazione") = LeggiEnte.Descrizione




        StringaCodiceComune = CodiceJolly

        Provincia = Mid(StringaCodiceComune & Space(6), 1, 3)
        Comune = Mid(StringaCodiceComune & Space(6), 4, 3)


        LeggiEnte.Provincia = Provincia
        LeggiEnte.Comune = Comune
        LeggiEnte.Leggi(Session("DC_OSPITE"))


        RigaStampa.Item("CodiceJolly") = CodiceJolly
        RigaStampa.Item("DecodificaJolly") = LeggiEnte.Descrizione




        RigaStampa.Item("CodiceServizio") = Righe.CodiceServizio
        RigaStampa.Item("DecodificaCentroServizio") = Righe.DecodificaCentroServizio
        RigaStampa.Item("CODICEOSPITE") = Righe.CODICEOSPITE
        RigaStampa.Item("NOME") = Righe.NOME
        RigaStampa.Item("DATANASCITA") = Righe.DATANASCITA
        RigaStampa.Item("CODICEFISCALE") = Righe.CODICEFISCALE
        RigaStampa.Item("MESE") = Righe.MESE
        RigaStampa.Item("ANNO") = Righe.ANNO
        RigaStampa.Item("STATOAUTO") = Righe.STATOAUTO

        If GiorniAssenzaComune > 0 Or GiorniPresenzeComune > 0 Then
            RigaStampa.Item("GIORNIPRESENZA") = GiorniPresenzeComune
            RigaStampa.Item("GIORNIASSENZA") = GiorniAssenzaComune
        Else
            If GiorniPresenzaOspite > 0 Or GiorniAssenzaOspite > 0 Then
                RigaStampa.Item("GIORNIPRESENZA") = GiorniPresenzaOspite
                RigaStampa.Item("GIORNIASSENZA") = GiorniAssenzaOspite
            Else
                RigaStampa.Item("GIORNIPRESENZA") = GiorniPresenzaParente
                RigaStampa.Item("GIORNIASSENZA") = GiorniAssenzaParente
            End If
        End If

        RigaStampa.Item("ImportoOspitePresenze") = Righe.ImportoOspitePresenze
        RigaStampa.Item("ImportoOspiteAssenze") = Righe.ImportoOspiteAssenze
        RigaStampa.Item("ImportoParentiPresenze") = Righe.ImportoParentiPresenze
        RigaStampa.Item("ImportoParentiAssenze") = Righe.ImportoParentiAssenze
        RigaStampa.Item("CodiceRegioneFatturazione") = Righe.CodiceRegioneFatturazione
        RigaStampa.Item("DecodificaRegioneFatturazione") = Righe.DecodificaRegioneFatturazione

        RigaStampa.Item("ImportoRegionePresenze") = Righe.ImportoRegionePresenze
        RigaStampa.Item("ImportoRegioneAssenze") = Righe.ImportoRegioneAssenze

        RigaStampa.Item("ImportoComunePresenze") = ImportoPresenzeComune
        RigaStampa.Item("ImportoComuneAssenze") = ImportoAssenzeComune


        RigaStampa.Item("CodiceJolly") = Righe.CodiceJolly
        RigaStampa.Item("DecodificaJolly") = Righe.DecodificaJolly
        RigaStampa.Item("ImportoJollyPresenze") = Righe.ImportoJollyPresenze
        RigaStampa.Item("ImportoJollyAssenze") = Righe.ImportoJollyAssenze
        RigaStampa.Item("ComuneResidenza") = Righe.ComuneResidenza
        RigaStampa.Item("Isee") = Righe.Isee
        RigaStampa.Item("Accompagnamento") = Righe.Accompagnamento
        RigaStampa.Item("DataAccoglimento") = Righe.DataAccoglimento
        RigaStampa.Item("DataDimissione") = Righe.DataDimissione
        RigaStampa.Item("MotivoDimissione") = Righe.MotivoDimissione
        RigaStampa.Item("Giorno1") = Righe.Giorno1
        RigaStampa.Item("Giorno2") = Righe.Giorno2
        RigaStampa.Item("Giorno3") = Righe.Giorno3
        RigaStampa.Item("Giorno4") = Righe.Giorno4
        RigaStampa.Item("Giorno5") = Righe.Giorno5
        RigaStampa.Item("Giorno6") = Righe.Giorno6
        RigaStampa.Item("Giorno7") = Righe.Giorno7
        RigaStampa.Item("Giorno8") = Righe.Giorno8
        RigaStampa.Item("Giorno9") = Righe.Giorno9
        RigaStampa.Item("Giorno10") = Righe.Giorno10
        RigaStampa.Item("Giorno11") = Righe.Giorno11
        RigaStampa.Item("Giorno12") = Righe.Giorno12
        RigaStampa.Item("Giorno13") = Righe.Giorno13
        RigaStampa.Item("Giorno14") = Righe.Giorno14
        RigaStampa.Item("Giorno15") = Righe.Giorno15
        RigaStampa.Item("Giorno16") = Righe.Giorno16
        RigaStampa.Item("Giorno17") = Righe.Giorno17
        RigaStampa.Item("Giorno18") = Righe.Giorno18
        RigaStampa.Item("Giorno19") = Righe.Giorno19
        RigaStampa.Item("Giorno20") = Righe.Giorno20
        RigaStampa.Item("Giorno21") = Righe.Giorno21
        RigaStampa.Item("Giorno22") = Righe.Giorno22
        RigaStampa.Item("Giorno23") = Righe.Giorno23
        RigaStampa.Item("Giorno24") = Righe.Giorno24
        RigaStampa.Item("Giorno25") = Righe.Giorno25
        RigaStampa.Item("Giorno26") = Righe.Giorno26
        RigaStampa.Item("Giorno27") = Righe.Giorno27
        RigaStampa.Item("Giorno28") = Righe.Giorno28
        RigaStampa.Item("Giorno29") = Righe.Giorno29
        RigaStampa.Item("Giorno30") = Righe.Giorno30
        RigaStampa.Item("Giorno31") = Righe.Giorno31
        RigaStampa.Item("GiorniPresTP") = Righe.GiorniPresTP
        RigaStampa.Item("QuotaPresTP") = Righe.QuotaPresTP
        RigaStampa.Item("GiorniAssTP") = Righe.GiorniAssTP
        RigaStampa.Item("QuotaAssTP") = Righe.QuotaAssTP
        RigaStampa.Item("GiorniPresPT") = Righe.GiorniPresPT
        RigaStampa.Item("QuotaPresPT") = Righe.QuotaPresPT
        RigaStampa.Item("GiorniAssPT") = Righe.GiorniAssPT
        RigaStampa.Item("QuotaAssPT") = Righe.QuotaAssPT
        RigaStampa.Item("ComuneNascita") = Righe.ComuneNascita
        RigaStampa.Item("TipoRettautente") = Righe.TipoRettautente
        RigaStampa.Item("TipoRettaRegione") = Righe.TipoRettaRegione
        RigaStampa.Item("Uscita1") = Righe.Uscita1
        RigaStampa.Item("Causale1") = Righe.Causale1
        RigaStampa.Item("Entrata1") = Righe.Entrata1
        RigaStampa.Item("Uscita2") = Righe.Uscita2
        RigaStampa.Item("Causale2") = Righe.Causale2
        RigaStampa.Item("Entrata2") = Righe.Entrata2
        RigaStampa.Item("Uscita3") = Righe.Uscita3
        RigaStampa.Item("Causale3") = Righe.Causale3
        RigaStampa.Item("Entrata3") = Righe.Entrata3
        RigaStampa.Item("CodiceCartellaClinica") = Righe.CodiceCartellaClinica
        RigaStampa.Item("ExtraFissiOspiteParente") = Righe.ExtraFissiOspiteParente
        RigaStampa.Item("ImportiExtraFissiOspiteParente") = Math.Round(Righe.ImportiExtraFissiOspiteParente, 2)
        RigaStampa.Item("ExtraFissiComune") = ExtraFissiComune
        RigaStampa.Item("ImportiExtraFissiComune") = Math.Round(ImportiExtraFissiComune, 2)




        RecordStampa.Tables("EstrazioneAllegati").Rows.Add(RigaStampa)




    End Sub


    Private Sub UsciteTemporanee(ByRef cn As OleDbConnection, ByVal CodiceOspite As Integer, ByVal CentroServizio As String, ByVal Mese As Integer, ByVal Anno As Integer, ByRef Righe As RigaStampa)
        Dim Uscita1Osp As Date

        Dim Uscita2Osp As Date
        Dim UscitaPermesso As Date

        Dim Entrata1Osp As Date
        Dim Entrata2Osp As Date
        Dim EntrataPermesso As Date




        Dim PresenteUscitaTemp As Boolean = False



        Dim TipoUsc As String = ""


        Dim cmdIMPPrec As New OleDbCommand()

        cmdIMPPrec.Connection = cn

        cmdIMPPrec.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & CentroServizio & "' And CODICEOSPITE = " & CodiceOspite & " And DATA < ? Order by Data"
        cmdIMPPrec.Parameters.AddWithValue("@DATA", DateSerial(Anno, Mese, 1))

        Dim LeggiImpPrec As OleDbDataReader = cmdIMPPrec.ExecuteReader()
        Do While LeggiImpPrec.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImpPrec.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))

            If campodb(LeggiImpPrec.Item("TipoMov")) = "03" Then
                If Tipo.Descrizione.ToUpper.IndexOf("OSPEDA") >= 0 Then
                    TipoUsc = "O"
                Else
                    'PresenteUscitaTemp = True
                    TipoUsc = ""
                End If

            End If
            If campodb(LeggiImpPrec.Item("TipoMov")) = "04" Then
                TipoUsc = ""
            End If
        Loop
        LeggiImpPrec.Close()


        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn

        cmdIMP.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & CentroServizio & "' And CODICEOSPITE = " & CodiceOspite & " And Month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " Order by Data"


        Dim LeggiImp As OleDbDataReader = cmdIMP.ExecuteReader()
        Do While LeggiImp.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImp.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))

            If campodb(LeggiImp.Item("TipoMov")) = "03" Then
                If Tipo.Descrizione.ToUpper.IndexOf("OSPEDA") >= 0 Then
                    If Year(Uscita1Osp) < 2000 Then
                        Uscita1Osp = campodbD(LeggiImp.Item("Data"))
                    Else
                        Uscita2Osp = campodbD(LeggiImp.Item("Data"))
                    End If
                    TipoUsc = "O"
                Else
                    If Not PresenteUscitaTemp Then
                        UscitaPermesso = campodbD(LeggiImp.Item("Data"))
                    Else
                        EntrataPermesso = Nothing
                    End If
                    PresenteUscitaTemp = True
                    TipoUsc = ""
                End If

            End If
            If campodb(LeggiImp.Item("TipoMov")) = "04" Then
                If TipoUsc = "O" Then
                    If Year(Entrata1Osp) < 2000 Then
                        Entrata1Osp = campodbD(LeggiImp.Item("Data"))
                    Else
                        Entrata2Osp = campodbD(LeggiImp.Item("Data"))
                    End If
                Else
                    EntrataPermesso = campodbD(LeggiImp.Item("Data"))
                End If
            End If

        Loop
        LeggiImp.Close()




        Righe.Uscita1 = Uscita1Osp
        Righe.Uscita2 = Uscita2Osp
        Righe.Uscita3 = UscitaPermesso

        Righe.Entrata1 = Entrata1Osp
        Righe.Entrata2 = Entrata2Osp
        Righe.Entrata3 = EntrataPermesso


    End Sub


    Private Sub GioniMese(ByRef cn As OleDbConnection, ByVal CodiceOspite As Integer, ByVal CentroServizio As String, ByVal Mese As Integer, ByVal Anno As Integer, ByRef Righe As RigaStampa)

        Dim giorni As Integer
        Dim CausaleGiorno(31) As String

        giorni = GiorniMese(Mese, Anno)

        Dim Cs As New Cls_CentroServizio


        Cs.CENTROSERVIZIO = CentroServizio

        Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)
        If Cs.TIPOCENTROSERVIZIO = "D" Then


            Dim AccoltoNelMese As Integer = 0
            Dim Mov As New Cls_Movimenti

            Mov.CodiceOspite = CodiceOspite
            Mov.CENTROSERVIZIO = CentroServizio
            Mov.UltimaDataAccoglimento(Session("DC_OSPITE"))
            If Month(Mov.Data) = Mese And Year(Mov.Data) = Anno Then
                AccoltoNelMese = Day(Mov.Data)
            End If



            Dim UscitiNelMese As Integer = 0
            Dim MovU As New Cls_Movimenti

            MovU.CodiceOspite = CodiceOspite
            MovU.CENTROSERVIZIO = CentroServizio
            MovU.UltimaDataUscitaDefinitivaMese(Session("DC_OSPITE"), Anno, Mese)
            If Month(MovU.Data) = Mese And Year(MovU.Data) = Anno Then
                UscitiNelMese = Day(MovU.Data)
            End If

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = "Select * From AssenzeCentroDiurno where [CENTROSERVIZIO] = ? and  [CODICEOSPITE] = ? and anno = ? and mese = ?"
            cmd1.Connection = cn

            cmd1.Parameters.AddWithValue("@Cserv", CentroServizio)
            cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmd1.Parameters.AddWithValue("@Anno", Anno)
            cmd1.Parameters.AddWithValue("@Mese", Mese)

            Dim Read1 As OleDbDataReader = cmd1.ExecuteReader()
            If Read1.Read Then

                Dim K As New ClsOspite

                K.CodiceOspite = CodiceOspite
                K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)



                For I = 1 To giorni

                    CausaleGiorno(I) = campodb(Read1.Item("GIORNO" & I))

                    If AccoltoNelMese > 0 Then
                        If AccoltoNelMese > I Then
                            CausaleGiorno(I) = "C"
                        End If
                    End If
                    If UscitiNelMese > 0 Then
                        If UscitiNelMese < I Then
                            CausaleGiorno(I) = "C"
                        End If
                    End If
                Next
            Else
                Dim Presente As New Cls_Movimenti

                Presente.CENTROSERVIZIO = CentroServizio
                Presente.CodiceOspite = CodiceOspite
                Presente.TipoMov = ""
                Presente.UltimaMovimentoPrimaData(Session("DC_OSPITE"), Presente.CodiceOspite, Presente.CENTROSERVIZIO, DateSerial(Anno, Mese, 1))

                If Presente.TipoMov <> "13" And Presente.TipoMov <> "" Then

                    Dim Kx As New ClsOspite

                    Kx.CodiceOspite = CodiceOspite
                    Kx.Leggi(Session("DC_OSPITE"), Kx.CodiceOspite)




                    Dim TbCServ As New Cls_CentroServizio
                    Dim SETTIMANA As String
                    Dim Causale(32) As String

                    For I = 1 To 31
                        Causale(I) = ""
                    Next

                    Dim Gg As Integer
                    TbCServ.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)

                    Dim AnaOsp As New ClsOspite

                    AnaOsp.Leggi(Session("DC_OSPITE"), Presente.CodiceOspite)

                    Dim K As New Cls_DatiOspiteParenteCentroServizio


                    K.CentroServizio = Cs.CENTROSERVIZIO
                    K.CodiceOspite = Presente.CodiceOspite
                    K.Leggi(Session("DC_OSPITE"))
                    If K.TipoOperazione <> "" Then
                        AnaOsp.SETTIMANA = K.Settimana
                    End If



                    SETTIMANA = AnaOsp.SETTIMANA


                    For I = 1 To GiorniMese(Mese, Anno)
                        Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                        If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                            Causale(I) = Mid(SETTIMANA, Gg, 1)
                        End If
                    Next I


                    SETTIMANA = TbCServ.SETTIMANA
                    For I = 1 To GiorniMese(Mese, Anno)
                        Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                        If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                            Causale(I) = "C"
                        End If
                    Next I


                    If Mese = 1 Then
                        If TbCServ.GIORNO0101 <> "S" Then
                            Causale(1) = "C"
                        End If
                        If TbCServ.GIORNO0601 <> "S" Then
                            Causale(6) = "C"
                        End If
                    End If
                    If Mese = 3 Then
                        If TbCServ.GIORNO1903 <> "S" Then
                            Causale(19) = "C"
                        End If
                    End If
                    If Mese = 4 Then
                        If TbCServ.GIORNO2504 <> "S" Then
                            Causale(25) = "C"
                        End If
                    End If
                    If Mese = 5 Then
                        If TbCServ.GIORNO0105 <> "S" Then
                            Causale(1) = "C"
                        End If
                    End If

                    If Mese = 6 Then
                        If TbCServ.GIORNO0206 <> "S" Then
                            Causale(2) = "C"
                        End If
                        If TbCServ.GIORNO2906 <> "S" Then
                            Causale(29) = "C"
                        End If
                    End If
                    If Mese = 8 Then
                        If TbCServ.GIORNO1508 <> "S" Then
                            Causale(15) = "C"
                        End If
                    End If
                    If Mese = 11 Then
                        If TbCServ.GIORNO0111 <> "S" Then
                            Causale(1) = "C"
                        End If
                        If TbCServ.GIORNO0411 <> "S" Then
                            Causale(4) = "C"
                        End If
                    End If
                    If Mese = 12 Then
                        If TbCServ.GIORNO0812 <> "S" Then
                            Causale(8) = "C"
                        End If
                        If TbCServ.GIORNO2512 <> "S" Then
                            Causale(25) = "C"
                        End If
                        If TbCServ.GIORNO2612 <> "S" Then
                            Causale(26) = "C"
                        End If
                    End If
                    If TbCServ.GIORNO1ATTIVO <> "S" Then
                        If Not IsDBNull(TbCServ.GIORNO1) Then
                            If Val(Mid(TbCServ.GIORNO1, 4, 2)) = Mese Then
                                Causale(Val(Mid(TbCServ.GIORNO1, 1, 2))) = "C"
                            End If
                        End If
                    End If

                    If TbCServ.GIORNO2ATTIVO <> "S" Then
                        If Not IsDBNull(TbCServ.GIORNO2) Then
                            If Val(Mid(TbCServ.GIORNO2, 4, 2)) = Mese Then
                                Causale(Val(Mid(TbCServ.GIORNO2, 1, 2))) = "C"
                            End If
                        End If
                    End If

                    If TbCServ.GIORNO3ATTIVO <> "S" Then
                        If Not IsDBNull(TbCServ.GIORNO3) Then
                            If Val(Mid(TbCServ.GIORNO3, 4, 2)) = Mese Then
                                Causale(Val(Mid(TbCServ.GIORNO3, 1, 2))) = "C"
                            End If
                        End If
                    End If

                    If TbCServ.GIORNO4ATTIVO <> "S" Then
                        If Not IsDBNull(TbCServ.GIORNO4) Then
                            If Val(Mid(TbCServ.GIORNO4, 4, 2)) = Mese Then
                                Causale(Val(Mid(TbCServ.GIORNO4, 1, 2))) = "C"
                            End If
                        End If
                    End If

                    If TbCServ.GIORNO5ATTIVO <> "S" Then
                        If Not IsDBNull(TbCServ.GIORNO5) Then
                            If Val(Mid(TbCServ.GIORNO5, 4, 2)) = Mese Then
                                Causale(Val(Mid(TbCServ.GIORNO5, 1, 2))) = "C"
                            End If
                        End If
                    End If


                    For I = 1 To 31
                        CausaleGiorno(I) = Causale(I)
                    Next

                End If
            End If
            Read1.Close()
        Else

            For I = 1 To giorni
                Dim MySql As String = ""

                MySql = "SELECT codiceospite,nome,(select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as tipo,(select top 1 Data from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as Data,(select top 1 CAUSALE  from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as CAUSALE"
                MySql = MySql & " FROM AnagraficaComune "
                MySql = MySql & "where CodiceParente = 0 And (select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite  and ((data < ?  AND TIPOMOV  = '13') OR (data <= ?  AND (TIPOMOV  = '05' OR TIPOMOV  = '03' OR TIPOMOV  = '04'))) and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO desc ) <> '13' And (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And CodiceOSpite = " & CodiceOspite

                Dim cmd As New OleDbCommand()
                cmd.CommandText = MySql
                cmd.Connection = cn
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", CentroServizio)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", CentroServizio)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", CentroServizio)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", CentroServizio)
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Dim OspFind As Boolean


                    OspFind = False

                    OspFind = True
                    If campodb(myPOSTreader.Item("tipo")) = "05" Then
                        If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                            CausaleGiorno(I) = ""
                        Else
                            CausaleGiorno(I) = ""
                        End If
                    End If
                    If campodb(myPOSTreader.Item("tipo")) = "13" Then
                        If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                            CausaleGiorno(I) = myPOSTreader.Item("CAUSALE")  '"U"
                        End If
                    End If
                    If campodb(myPOSTreader.Item("tipo")) = "04" Then
                        CausaleGiorno(I) = ""
                    End If
                    If campodb(myPOSTreader.Item("tipo")) = "03" Then
                        CausaleGiorno(I) = myPOSTreader.Item("CAUSALE")  '"A"
                        If campodb(myPOSTreader.Item("CAUSALE")) = "01" Or campodb(myPOSTreader.Item("CAUSALE")) = "03" Then
                            CausaleGiorno(I) = myPOSTreader.Item("CAUSALE")  '"O"
                        End If
                    End If

                Loop
                myPOSTreader.Close()
            Next
        End If


        Righe.Giorno1 = CausaleGiorno(1)
        Righe.Giorno2 = CausaleGiorno(2)
        Righe.Giorno3 = CausaleGiorno(3)
        Righe.Giorno4 = CausaleGiorno(4)
        Righe.Giorno5 = CausaleGiorno(5)
        Righe.Giorno6 = CausaleGiorno(6)
        Righe.Giorno7 = CausaleGiorno(7)
        Righe.Giorno8 = CausaleGiorno(8)
        Righe.Giorno9 = CausaleGiorno(9)
        Righe.Giorno10 = CausaleGiorno(10)
        Righe.Giorno11 = CausaleGiorno(11)
        Righe.Giorno12 = CausaleGiorno(12)
        Righe.Giorno13 = CausaleGiorno(13)
        Righe.Giorno14 = CausaleGiorno(14)
        Righe.Giorno15 = CausaleGiorno(15)
        Righe.Giorno16 = CausaleGiorno(16)
        Righe.Giorno17 = CausaleGiorno(17)
        Righe.Giorno18 = CausaleGiorno(18)
        Righe.Giorno19 = CausaleGiorno(19)
        Righe.Giorno20 = CausaleGiorno(20)
        Righe.Giorno21 = CausaleGiorno(21)
        Righe.Giorno22 = CausaleGiorno(22)
        Righe.Giorno23 = CausaleGiorno(23)
        Righe.Giorno24 = CausaleGiorno(24)
        Righe.Giorno25 = CausaleGiorno(25)
        Righe.Giorno26 = CausaleGiorno(26)
        Righe.Giorno27 = CausaleGiorno(27)
        Righe.Giorno28 = CausaleGiorno(28)
        Righe.Giorno29 = CausaleGiorno(29)
        Righe.Giorno30 = CausaleGiorno(30)
        Righe.Giorno31 = CausaleGiorno(31)




        ' Quota a zero, codifica vbscript non inserita

        Righe.QuotaAssTP = 0
        Righe.QuotaAssPT = 0
        Righe.QuotaPresPT = 0
        Righe.QuotaPresTP = 0



        For i = 1 To giorni
            If CausaleGiorno(i) = Cs.CausaleTempoPieno Then
                Righe.GiorniPresTP = Righe.GiorniPresTP + 1
            End If
            If CausaleGiorno(i) = Cs.CausaleTempoPienoAss Then
                Righe.GiorniAssTP = Righe.GiorniAssTP + 1
            End If
            If CausaleGiorno(i) = Cs.CausalePartTime Then
                Righe.GiorniPresTP = Righe.GiorniPresTP + 1
            End If
            If CausaleGiorno(i) = Cs.CausaleTempoPienoAss Then
                Righe.GiorniAssPT = Righe.GiorniAssPT + 1
            End If
        Next
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Protected Sub DD_Struttura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.SelectedIndexChanged
        AggiornaCServ()
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Cerca()
    End Sub
End Class
