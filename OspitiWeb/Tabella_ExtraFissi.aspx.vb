﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_Tabella_ExtraFissi
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If

        If DD_IVA.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il codice iva');", True)
            Exit Sub
        End If

        If RB_Comune.Checked = False And RB_Ospite.Checked = False And RB_Parente.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Selezionare la ripartizione');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim Verifica As New Cls_TipoExtraFisso

            Verifica.CODICEEXTRA = Txt_Codice.Text

            Verifica.Leggi(Session("DC_OSPITE"))
            If Verifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già presente');", True)
                Exit Sub
            End If
        End If

        Dim K As New Cls_TipoExtraFisso

        K.CODICEEXTRA = Request.Item("CODICE")

        K.Leggi(Session("DC_OSPITE"))

        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(K)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "TipoExtraFisso", AppoggioJS)


        K.CODICEEXTRA = Txt_Codice.Text
        K.Descrizione = Txt_Descrizione.Text
        K.DescrizioneInterna = Txt_DescrizioneInterna.Text

        Dim Vettore(100) As String
        If Txt_Sottoconto.Text <> "" Then
            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                K.MASTRO = Vettore(0)
                K.CONTO = Vettore(1)
                K.SOTTOCONTO = Vettore(2)
            End If
        Else
            K.MASTRO = 0
            K.CONTO = 0
            K.SOTTOCONTO = 0
        End If


        K.CENTROSERVIZIO = Cmb_CServ.SelectedValue

        K.Struttura = DD_Struttura.SelectedValue


        K.CodiceIva = DD_IVA.SelectedValue

        If Txt_Importo.Text = "" Then
            Txt_Importo.Text = 0
        End If
        K.IMPORTO = Txt_Importo.Text

        K.TipoImporto = DD_Tipo.SelectedValue

        If RB_Comune.Checked = True Then
            K.Ripartizione = "C"
        End If
        If RB_Ospite.Checked = True Then
            K.Ripartizione = "O"
        End If
        If RB_Parente.Checked = True Then
            K.Ripartizione = "P"
        End If

        If Chk_Ancticipato.Checked Then
            K.Anticipato = "S"
        Else
            K.Anticipato = ""
        End If


        If Chk_FuoriRetta.Checked Then
            K.FuoriRetta = 1
        Else
            K.FuoriRetta = 0
        End If

        K.DataScadenza = IIf(Txt_DataScadenza.Text = "", Nothing, Txt_DataScadenza.Text)

        K.Scrivi(Session("DC_OSPITE"))

        Response.Redirect("ElencoExtraFissi.aspx")

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub OspitiWeb_Tabella_ModalitaPagamento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim kIva As New Cls_IVA

        kIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Request.Item("CODICE") <> "" Then
            Dim K As New Cls_TipoExtraFisso


            K.CODICEEXTRA = Request.Item("CODICE")

            K.Leggi(Session("DC_OSPITE"))

            Txt_Codice.Text = K.CODICEEXTRA
            Txt_Descrizione.Text = K.Descrizione

            Txt_DescrizioneInterna.Text = K.DescrizioneInterna

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = K.MASTRO
            PianoConti.Conto = K.CONTO
            PianoConti.Sottoconto = K.SOTTOCONTO
            PianoConti.Decodfica(Session("DC_GENERALE"))

            Txt_Sottoconto.Text = K.MASTRO & " " & K.CONTO & " " & K.SOTTOCONTO & " " & PianoConti.Descrizione

            DD_IVA.SelectedValue = K.CodiceIva

            Txt_Importo.Text = Format(K.IMPORTO, "#,##0.00")

            DD_Tipo.SelectedValue = K.TipoImporto

            If K.Anticipato = "S" Then
                Chk_Ancticipato.Checked = True
            Else
                Chk_Ancticipato.Checked = False
            End If



            If K.FuoriRetta = 1 Then
                Chk_FuoriRetta.Checked = True
            Else
                Chk_FuoriRetta.Checked = False
            End If

            DD_Struttura.SelectedValue = K.Struttura
            AggiornaCServ()

            Dim KCserv As New Cls_CentroServizio

            KCserv.CENTROSERVIZIO = K.CENTROSERVIZIO
            KCserv.Leggi(Session("DC_OSPITE"), K.CENTROSERVIZIO)
            If KCserv.DESCRIZIONE <> "" Then

                DD_Struttura.SelectedValue = KCserv.Villa
                AggiornaCServ()
                Cmb_CServ.SelectedValue = K.CENTROSERVIZIO
            End If

            Txt_DataScadenza.Text = K.DataScadenza

            Txt_Codice.Enabled = False

            RB_Comune.Checked = False
            RB_Ospite.Checked = False
            RB_Parente.Checked = False
            If K.Ripartizione = "C" Then
                RB_Comune.Checked = True
            End If
            If K.Ripartizione = "O" Then
                RB_Ospite.Checked = True
            End If
            If K.Ripartizione = "P" Then
                RB_Parente.Checked = True
            End If
            RB_Comune.Enabled = False
            RB_Ospite.Enabled = False
            RB_Parente.Enabled = False
        Else


            Dim TipoExtra As New Cls_TipoExtraFisso

            Txt_Codice.Text = TipoExtra.MaxTipoExtra(Session("DC_OSPITE"))
        End If

        Call EseguiJS()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If
        Dim kVer As New Cls_ExtraFisso

        kVer.CODICEEXTRA = Txt_Codice.Text

        If kVer.CodiceExtraUsato(Session("DC_OSPITE"), kVer.CODICEEXTRA) = True Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Extra in uso non posso eliminarlo');", True)
            Exit Sub
        End If


        Dim K As New Cls_TipoExtraFisso


        K.CODICEEXTRA = Request.Item("CODICE")
        K.Leggi(Session("DC_OSPITE"))

        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(K)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "TipoExtraFisso", AppoggioJS)


        K.CODICEEXTRA = Request.Item("CODICE")

        K.Elimina(Session("DC_OSPITE"))

        Response.Redirect("ElencoExtraFissi.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoExtraFissi.aspx")
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click

        Dim TipoExtra As New Cls_TipoExtraFisso

        Txt_Codice.Text = TipoExtra.MaxTipoExtra(Session("DC_OSPITE"))

        Txt_Codice.Enabled = True

        Call Txt_Codice_TextChanged(sender, e)


        RB_Comune.Enabled = True
        RB_Ospite.Enabled = True
        RB_Parente.Enabled = True

        Call Txt_Codice_TextChanged(sender, e)

        Call Txt_Descrizione_TextChanged(sender, e)

    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoExtraFisso

            x.CODICEEXTRA = Txt_Codice.Text
            x.Leggi(Session("DC_OSPITE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoExtraFisso

            x.CODICEEXTRA = ""
            x.Descrizione = Txt_Descrizione.Text.Trim & " " & Txt_DescrizioneInterna.Text.Trim
            x.Ripartizione = "O"
            If RB_Comune.Checked = True Then
                x.Ripartizione = "C"
            End If
            If RB_Ospite.Checked = True Then
                x.Ripartizione = "O"
            End If
            If RB_Parente.Checked = True Then
                x.Ripartizione = "P"
            End If

            x.LeggiDaDescrizioneEDescrizioneInternaRipartizione(Session("DC_OSPITE"))

            If x.CODICEEXTRA <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Txt_DescrizioneInterna_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DescrizioneInterna.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoExtraFisso

            x.CODICEEXTRA = ""
            x.Descrizione = Txt_Descrizione.Text.Trim & " " & Txt_DescrizioneInterna.Text.Trim
            x.Ripartizione = "O"
            If RB_Comune.Checked = True Then
                x.Ripartizione = "C"
            End If
            If RB_Ospite.Checked = True Then
                x.Ripartizione = "O"
            End If
            If RB_Parente.Checked = True Then
                x.Ripartizione = "P"
            End If

            x.LeggiDaDescrizioneEDescrizioneInternaRipartizione(Session("DC_OSPITE"))

            If x.CODICEEXTRA <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
