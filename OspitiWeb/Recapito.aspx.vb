﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util

Partial Class Recapito
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub


        Dim x As New ClsOspite
        Dim d As New ClsComune

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

  

        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        Txt_Nome.Text = x.RecapitoNome
        Txt_Indirizzo.Text = x.RecapitoIndirizzo        
        Txt_Telefono.Text = x.RESIDENZATELEFONO4

        Dim dc As New ClsComune

        dc.Comune = x.RecapitoComune
        dc.Provincia = x.RecapitoProvincia
        dc.DecodficaComune(ConnectionString)
        Txt_Comune.Text = dc.Provincia & " " & dc.Comune & " " & dc.Descrizione

        Call EseguiJS()

    End Sub


    Protected Sub Modifica()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))


        x.RecapitoNome = Txt_Nome.Text
        x.RecapitoIndirizzo = Txt_Indirizzo.Text

        If Txt_Comune.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune.Text)
            If Vettore.Length > 1 Then                
                x.RecapitoProvincia = Vettore(0)
                x.RecapitoComune = Vettore(1)
            End If
        End If


        x.RESIDENZATELEFONO4 = Txt_Telefono.Text

        x.ScriviOspite(ConnectionString)
    End Sub

    Protected Sub Btn_Modifica1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica1.Click

        Call Modifica()

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("RicercaAnagrafica.aspx")
    End Sub



End Class
