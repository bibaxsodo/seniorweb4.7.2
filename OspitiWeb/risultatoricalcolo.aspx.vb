﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class risultatoricalcolo
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub loadgriglia()
        Dim k As New Cls_Parametri
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        k.LeggiParametri(ConnectionString)
        Dim i As Integer
        Dim Appoggio As String
        Dim Vettore(1000) As Long
        Dim InMax As Integer = 0
        Appoggio = ""
        For i = 1 To Len(k.Errori)
            If Mid(k.Errori, i, 1) = "," Then
                Vettore(InMax) = Appoggio
                InMax = InMax + 1
                Appoggio = ""
            Else
                Appoggio = Appoggio & Mid(k.Errori, i, 1)
            End If
        Next
        MyTable.Clear()
        MyTable.Columns.Add("Numero", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Tipo Mov", GetType(String))
        MyTable.Columns.Add("Destinatario", GetType(String))
        MyTable.Columns.Add("Importo", GetType(Double))


        For i = 0 To InMax
            Dim cmd As New OleDbCommand()

            cmd.CommandText = ("select * from ADDACR Where PROGRESSIVO = " & Val(Vettore(i)))
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = myPOSTreader.Item("PROGRESSIVO")
                Dim Co As New ClsOspite
                Co.Leggi(ConnectionString, myPOSTreader.Item("CodiceOspite"))
                myriga(1) = Co.Nome
                myriga(2) = myPOSTreader.Item("TIPOMOV")
                myriga(3) = myPOSTreader.Item("RIFERIMENTO")
                myriga(4) = myPOSTreader.Item("IMPORTO")

                MyTable.Rows.Add(myriga)
            End If
            myPOSTreader.Close()

        Next


        Grid_AddAcr.AutoGenerateColumns = True

        Grid_AddAcr.DataSource = MyTable

        Grid_AddAcr.DataBind()

        Session("SalvaGrigliaADAC") = MyTable

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Call loadgriglia()
        End If
    End Sub

    Protected Sub Btn_EliminaOspiti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_EliminaOspiti.Click
        MyTable = Session("SalvaGrigliaADAC")
        Dim i As Integer
        For i = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(i).Item(3)) Then
                If MyTable.Rows(i).Item(3) = "O" Then
                    Dim ConnectionString As String = Session("DC_OSPITE")

                    Dim cn As OleDbConnection
                    Dim MySql As String
                    cn = New Data.OleDb.OleDbConnection(ConnectionString)
                    cn.Open()

                    MySql = "DELETE FROM ADDACR WHERE  PROGRESSIVO = ?"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", MyTable.Rows(i).Item(0))
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()
                    cn.Close()

                    Grid_AddAcr.Rows(i).Cells(1).Text = 0
                    Grid_AddAcr.Rows(i).Cells(2).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(3).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(4).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(5).Text = 0
                End If
            End If
        Next
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        MyTable = Session("SalvaGrigliaADAC")
        Dim i As Integer
        For i = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(i).Item(3)) Then
                If MyTable.Rows(i).Item(3) = "P" Then
                    Dim ConnectionString As String = Session("DC_OSPITE")

                    Dim cn As OleDbConnection
                    Dim MySql As String
                    cn = New Data.OleDb.OleDbConnection(ConnectionString)
                    cn.Open()

                    MySql = "DELETE FROM ADDACR WHERE  PROGRESSIVO = ?"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", MyTable.Rows(i).Item(0))
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()
                    cn.Close()

                    Grid_AddAcr.Rows(i).Cells(1).Text = 0
                    Grid_AddAcr.Rows(i).Cells(2).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(3).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(4).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(5).Text = 0
                End If
            End If
        Next

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        MyTable = Session("SalvaGrigliaADAC")
        Dim i As Integer
        For i = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(i).Item(3)) Then
                If MyTable.Rows(i).Item(3) = "C" Then
                    Dim ConnectionString As String = Session("DC_OSPITE")

                    Dim cn As OleDbConnection
                    Dim MySql As String
                    cn = New Data.OleDb.OleDbConnection(ConnectionString)
                    cn.Open()

                    MySql = "DELETE FROM ADDACR WHERE  PROGRESSIVO = ?"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", MyTable.Rows(i).Item(0))
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()
                    cn.Close()

                    Grid_AddAcr.Rows(i).Cells(1).Text = 0
                    Grid_AddAcr.Rows(i).Cells(2).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(3).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(4).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(5).Text = 0
                End If
            End If
        Next

    End Sub



    Protected Sub Grid_AddAcr_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid_AddAcr.RowCommand
        Dim d As Integer

        Dim ConnectionString As String = Session("DC_OSPITE")

        MyTable = Session("SalvaGrigliaADAC")

        d = Val(e.CommandArgument)


        Dim cn As OleDbConnection
        Dim MySql As String
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        MySql = "DELETE FROM ADDACR WHERE  PROGRESSIVO = ?"
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@PROGRESSIVO", MyTable.Rows(d).Item(0))
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()
        cn.Close()

        Grid_AddAcr.Rows(d).Cells(1).Text = 0
        Grid_AddAcr.Rows(d).Cells(2).Text = "***"
        Grid_AddAcr.Rows(d).Cells(3).Text = "***"
        Grid_AddAcr.Rows(d).Cells(4).Text = "***"
        Grid_AddAcr.Rows(d).Cells(5).Text = 0
    End Sub



    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        MyTable = Session("SalvaGrigliaADAC")
        Dim i As Integer
        For i = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(i).Item(3)) Then
                If MyTable.Rows(i).Item(3) = "R" Then
                    Dim ConnectionString As String = Session("DC_OSPITE")

                    Dim cn As OleDbConnection
                    Dim MySql As String
                    cn = New Data.OleDb.OleDbConnection(ConnectionString)
                    cn.Open()

                    MySql = "DELETE FROM ADDACR WHERE  PROGRESSIVO = ?"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", MyTable.Rows(i).Item(0))
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()
                    cn.Close()

                    Grid_AddAcr.Rows(i).Cells(1).Text = 0
                    Grid_AddAcr.Rows(i).Cells(2).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(3).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(4).Text = "***"
                    Grid_AddAcr.Rows(i).Cells(5).Text = 0
                End If
            End If
        Next
    End Sub
End Class
