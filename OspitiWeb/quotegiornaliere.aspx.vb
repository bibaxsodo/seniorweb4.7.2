﻿
Partial Class OspitiWeb_quotegiornaliere
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim x As New Cls_CalcoloRette
        Dim ImportoOspite As Double
        Dim ImportoParenti As Double
        Dim ImportoComune As Double
        Dim ImportoRegione As Double
        Dim ImportoJolly As Double
        Dim i As Integer

        If Request.Form("Txt_Login") <> "" And Request.Form("Txt_Enc_Password") <> "" Then
            Call ForzaLogin()

            Dim KS As New ClsOspite

            KS.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Request.Form("CODICEFISCALE"))

            Session("CODICEOSPITE") = KS.CodiceOspite

            Dim Cserv As New Cls_CentroServizio

            Cserv.EPersonam = Request.Form("NUCLEO")
            Cserv.LeggiEpersonam(Session("DC_OSPITE"), Request.Form("NUCLEO"), True)

            Session("CODICESERVIZIO") = Cserv.CENTROSERVIZIO

            If Cserv.CENTROSERVIZIO = "" Then
                Dim Cserv1 As New Cls_CentroServizio

                Cserv1.EPersonamN = Request.Form("NUCLEO")
                Cserv1.LeggiEpersonam(Session("DC_OSPITE"), Request.Form("NUCLEO"), False)

                Session("CODICESERVIZIO") = Cserv1.CENTROSERVIZIO
            End If

        End If


        x.STRINGACONNESSIONEDB = Session("DC_OSPITE")

        x.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

        Dim KO As New ClsOspite

        KO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        Lbl_NomeOspite.Text = "<b>Quote Giornaliere di : " & KO.Nome & "</b><br/><br/>"

        ImportoOspite = Math.Round(x.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O", 0, Now), 2)
        For i = 1 To 10
            ImportoParenti = ImportoParenti + Math.Round(CDbl(x.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", i, Now)), 2)
        Next
        ImportoComune = Math.Round(x.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "C", 0, Now), 2)
        ImportoRegione = Math.Round(x.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "R", 0, Now), 2)
        ImportoJolly = Math.Round(x.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "J", 0, Now), 2)
        x.ChiudiDB()


        Dim MyJs As String


        MyJs = "    $(document).ready(function(){"

        MyJs = MyJs & "var salesData=["
        MyJs = MyJs & "{label:""Ospite"", color:""#F2C485"",value: " & Replace(Math.Round(ImportoOspite, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Parente"", color:""#F28DB3"",value: " & Replace(Math.Round(ImportoParenti, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Comune"", color:""#BE87EB"",value: " & Replace(Math.Round(ImportoComune, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Regione"", color:""#8AB8F7"",value: " & Replace(Math.Round(ImportoRegione, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Jolly"", color:""#8AEFC9"",value: " & Replace(Math.Round(ImportoJolly, 2), ",", ".") & "}"
        MyJs = MyJs & "];"

        MyJs = MyJs & "var svg = d3.select(""#chart2"").append(""svg"").attr(""width"",300).attr(""height"",200);"

        MyJs = MyJs & "svg.append(""g"").attr(""id"",""quotesDonut"");"

        MyJs = MyJs & "Donut3D.draw(""quotesDonut"", salesData, 130, 90, 100, 80, 30, 0);"


        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)

        If ImportoOspite > 0 Then lblOSpite.Text = Format(ImportoOspite, "#,##0.00")
        If ImportoParenti > 0 Then lblParenti.Text = Format(ImportoParenti, "#,##0.00")
        If ImportoComune > 0 Then lblComune.Text = Format(ImportoComune, "#,##0.00")
        If ImportoRegione > 0 Then lblRegione.Text = Format(ImportoRegione, "#,##0.00")
        If ImportoJolly > 0 Then lblJolly.Text = Format(ImportoJolly, "#,##0.00")
    End Sub


    Private Sub ForzaLogin()

        Dim k As New Cls_Login


        Session("TIPOAPP") = "RSA"


        k.Utente = Request.Form("Txt_Login")
        k.Chiave = Request.Form("Txt_Enc_Password")

        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.Leggi(Application("SENIOR"))



        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""



        If k.Ospiti <> "" Then
            Session("UTENTE") = k.Utente
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr
            Session("ABILITAZIONI") = k.ABILITAZIONI

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                Response.Redirect("MainPortineria.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                Exit Sub
            End If
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub
End Class
