﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_ElencoCausali
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Causali Order By Descrizione"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("ColoreIcona", GetType(String))
        
        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Diurno")

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = False

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()


        For i = 0 To Grd_ImportoOspite.Rows.Count - 1

            If Val(Grd_ImportoOspite.Rows(i).Cells(3).Text) > 0 Then
                Grd_ImportoOspite.Rows(i).Cells(3).Text = "<img src=""images\Diurno_" & Grd_ImportoOspite.Rows(i).Cells(3).Text & ".png"" />"
            Else
                Grd_ImportoOspite.Rows(i).Cells(3).Text = ""
            End If

        Next

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()

        For i = 0 To Grd_ImportoOspite.Rows.Count - 1

            If Val(Grd_ImportoOspite.Rows(i).Cells(3).Text) > 0 Then
                Grd_ImportoOspite.Rows(i).Cells(3).Text = "<img src=""images\Diurno_" & Grd_ImportoOspite.Rows(i).Cells(3).Text & ".png"" />"
            Else
                Grd_ImportoOspite.Rows(i).Cells(3).Text = ""
            End If

        Next
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("CausaliEntrataUscita.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Config.aspx")
    End Sub


    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("CausaliEntrataUscita.aspx")
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Response.Redirect("Menu_Config.aspx")
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand




        cmd.CommandText = "Select * From Causali Order By Descrizione"

        cmd.Connection = cn
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("RaggruppamentoCausale", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Regole", GetType(String))
        Tabella.Columns.Add("TIPOMOVIMENTO", GetType(String))
        Tabella.Columns.Add("GiornoUscita", GetType(String))
        Tabella.Columns.Add("Diurno", GetType(String))
        Tabella.Columns.Add("ChekCausale", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("Codice"))
            myriga(1) = campodb(myPOSTreader.Item("RaggruppamentoCausale"))
            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(3) = campodb(myPOSTreader.Item("Regole"))
            myriga(4) = campodb(myPOSTreader.Item("TIPOMOVIMENTO"))
            myriga(5) = campodb(myPOSTreader.Item("GiornoUscita"))
            myriga(6) = campodb(myPOSTreader.Item("Diurno"))
            myriga(7) = campodb(myPOSTreader.Item("ChekCausale"))
            

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
