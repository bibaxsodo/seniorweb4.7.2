﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel

Partial Class barraricalcolo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        If f.PercEsec >= 100 Then
            Response.Redirect("risultatoricalcolo.aspx")
        Else
            Label1.Text = "<div style=""background-color:#FF0000; border-style:solid; border-color: white;  width:" & Int(f.PercEsec * 4) & "px; height:40px; "" > "
            Label1.Text = Label1.Text & "<H1>" & f.PercEsec & "%</H1><BR/><BR/></div>"
            Label1.Text = Label1.Text & "Elaborazione Ospite : " & f.CodiceOspite
        End If
    End Sub
End Class
