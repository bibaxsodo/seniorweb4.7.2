﻿
Partial Class OspitiWeb_TipoSconto
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim Y As New Cls_IVA

        Y.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)


        If Request.Item("CODICE") = "" Then
            Call EseguiJS()

            Dim TipoSconto As New Cls_Sconto
            Txt_Codice.Text = TipoSconto.MaxTipoSconto(Session("DC_OSPITE"))
            Exit Sub
        End If

        Dim TpAd As New Cls_Sconto


        TpAd.Codice = Request.Item("CODICE")
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Txt_Codice.Text = TpAd.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = TpAd.Descrizione

        DD_IVA.SelectedValue = TpAd.CodiceIVA

        RB_Percentuale.Checked = True
        Rb_GioniPresenza.Checked = False
        RB_Fisso.Checked = False
        If TpAd.Tipologia = "F" Then
            RB_Percentuale.Checked = False
            RB_Fisso.Checked = True
        End If
        If TpAd.Tipologia = "G" Then
            RB_Percentuale.Checked = False
            RB_Fisso.Checked = False
            Rb_GioniPresenza.Checked = True
        End If

        Dim DecConto As New Cls_Pianodeiconti

        Dim KCserv As New Cls_CentroServizio

        KCserv.CENTROSERVIZIO = TpAd.CENTROSERVIZIO
        KCserv.Leggi(Session("DC_OSPITE"), TpAd.CENTROSERVIZIO)
        If KCserv.DESCRIZIONE <> "" Then

            DD_Struttura.SelectedValue = KCserv.Villa
            AggiornaCServ()
            Cmb_CServ.SelectedValue = TpAd.CENTROSERVIZIO
        End If


        DecConto.Mastro = TpAd.Mastro
        DecConto.Conto = TpAd.Conto
        DecConto.Sottoconto = TpAd.Sottoconto

        DecConto.Decodfica(Session("DC_GENERALE"))
        Txt_Conto.Text = TpAd.Mastro & " " & TpAd.Conto & " " & TpAd.Sottoconto & " " & DecConto.Descrizione

        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If

        Dim TpAd As New Cls_Sconto
        Dim Vettore(100) As String


        TpAd.Codice = Txt_Codice.Text
        TpAd.Descrizione = Txt_Descrizione.Text

        Vettore = SplitWords(Txt_Conto.Text)

        TpAd.Mastro = 0
        TpAd.Conto = 0
        TpAd.Sottoconto = 0
        If Vettore.Length >= 3 Then
            TpAd.Mastro = Val(Vettore(0))
            TpAd.Conto = Val(Vettore(1))
            TpAd.Sottoconto = Val(Vettore(2))
        End If

        TpAd.Tipologia = ""
        If RB_Fisso.Checked = True Then
            TpAd.Tipologia = "F"
        End If
        If Rb_GioniPresenza.Checked = True Then
            TpAd.Tipologia = "G"
        End If


        TpAd.CENTROSERVIZIO = Cmb_CServ.Text
        TpAd.CodiceIVA = DD_IVA.SelectedValue

        TpAd.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If
        Dim TpAd As New Cls_Sconto

        TpAd.Codice = Txt_Codice.Text
        TpAd.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoTipoSconto.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True


        Dim TipoSconto As New Cls_Sconto
        Txt_Codice.Text = TipoSconto.MaxTipoSconto(Session("DC_OSPITE"))


        Call Txt_Codice_TextChanged(sender, e)
        Call Txt_Descrizione_TextChanged(sender, e)


        Call EseguiJS()
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub



    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Sconto

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_OSPITE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_Sconto

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_OSPITE"), x.Descrizione)

                If x.Codice <> "" Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub
End Class
