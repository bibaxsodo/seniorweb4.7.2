﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ListinoOspite" CodeFile="ListinoOspite.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Listino</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Listino</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <asp:Label ID="lbl_Lisitino" runat="server" Text=""></asp:Label>
                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Listino
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Lbl_PresaInCarico" runat="server" Text=""></asp:Label>
                                            <asp:GridView ID="Grd_Retta" runat="server" CellPadding="4" Height="60px" Width="80%"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:BoundField DataField="DATA" HeaderText="Data" ItemStyle-Width="100px" />
                                                    <asp:BoundField DataField="Listino" HeaderText="Tipo Listino" ItemStyle-Width="90%" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" OnClientClick="return window.confirm('Eliminare un listino provoca la cancellazione delle informazione generate dal listino nelle tab  Totale/Stato Auto/Modalita/Ospite/Comune/Jolly/Parente ,confermi ?');" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <div style="float: left; margin-top: 10px;">
                                                Data :               
         <asp:TextBox ID="Txt_Data" runat="server" Width="90px"></asp:TextBox>
                                            </div>
                                            <div style="margin-top: 10px;">
                                                &nbsp;&nbsp;<asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Esegui" ID="IB_Genera"></asp:ImageButton>
                                            </div>
                                            <br />
                                            <label class="LabelCampo">Listino :</label>
                                            <asp:DropDownList ID="DD_Lisitino" runat="server"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="LblListinoPrivatoSinatario" runat="server" Text="Privato Sanitario :"></asp:Label></label>
                                            <asp:DropDownList ID="DD_PrivatoSanitario" runat="server"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="lblListinoSan" runat="server" Text="Sanitario :"></asp:Label></label>
                                            <asp:DropDownList ID="DD_LisitinoSanitario" runat="server"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="lblListinoSociale" runat="server" Text="Sociale :"></asp:Label></label>
                                            <asp:DropDownList ID="DD_LisitinoSociale" runat="server"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="lblListinoJolly" runat="server" Text="Jolly :"></asp:Label></label>
                                            <asp:DropDownList ID="DD_LisitinoJolly" runat="server"></asp:DropDownList><br />
                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                    <asp:Label ID="Lbl_errori" runat="server" ForeColor="Red" Width="288px"></asp:Label>
                                    <br />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

