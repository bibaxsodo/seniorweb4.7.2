﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_allineaospitiparenti
    Inherits System.Web.UI.Page

    Private Delegate Sub DoWorkDelegate(ByRef data As Object)

    'USE [Senior_AspMagiera_Ospiti]
    'GO

    '/****** Object:  Table [dbo].[Log_AllineamentoAnagrafica]    Script Date: 08/30/2016 12:47:42 ******/
    'SET ANSI_NULLS ON
    'GO

    'SET QUOTED_IDENTIFIER ON
    'GO

    'SET ANSI_PADDING ON
    'GO

    'CREATE TABLE [dbo].[Log_AllineamentoAnagrafica](
    '	[CognomeOspite] [varchar](50) NULL,
    '	[NomeOspite] [varchar](50) NULL,
    '	[Nome] [varchar](50) NULL,
    '	[DataNascita] [date] NULL,
    '	[CODICEFISCALE] [varchar](50) NULL,
    '	[Sesso] [varchar](50) NULL,
    '	[ProvinciaDiNascita] [varchar](50) NULL,
    '	[ComuneDiNascita] [varchar](50) NULL,
    '	[RESIDENZATELEFONO1] [varchar](50) NULL,
    '	[RESIDENZAINDIRIZZO1] [varchar](50) NULL,
    '	[RESIDENZACAP1] [varchar](50) NULL,
    '	[RESIDENZAPROVINCIA1] [varchar](50) NULL,
    '	[RESIDENZACOMUNE1] [varchar](50) NULL,
    '	[CodiceOspite] [int] NULL,
    '	[CodiceParente] [int] NULL,
    '	[Data] [date] NULL,
    '	[ParenteIndirizzo] [int] NULL,
    '	[EMail] [varchar](50) NULL,
    '	[Cellulare] [varchar](50) NULL,
    '	[Fax] [varchar](50) NULL
    ') ON [PRIMARY]

    'GO

    'SET ANSI_PADDING OFF
    'GO


    'USE [Senior_AspMagiera_Ospiti]
    'GO

    '/****** Object:  Table [dbo].[Log_VariazioneDati]    Script Date: 08/30/2016 12:48:04 ******/
    'SET ANSI_NULLS ON
    'GO

    'SET QUOTED_IDENTIFIER ON
    'GO

    'SET ANSI_PADDING ON
    'GO

    'CREATE TABLE [dbo].[Log_VariazioneDati](
    '	[CodiceOspite] [int] NULL,
    '	[CodiceParente] [int] NULL,
    '	[Campo] [varchar](50) NULL,
    '	[ValoreSenior] [varchar](250) NULL,
    '	[ValoreEpersonam] [varchar](250) NULL
    ') ON [PRIMARY]

    'GO

    'SET ANSI_PADDING OFF
    'GO




    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Sub DoWork(ByVal data As Object)

        AllineaDB(data)
    End Sub

    Private Sub AllineaDB(ByRef data As Object)

        Dim Token As String = ""
        Dim SessionInt As System.Web.SessionState.HttpSessionState = data
        Dim AppoggioLOG As String = ""

        Try
            Token = LoginPersonam(data)
        Catch ex As Exception

        End Try

        If Token = "" Then Exit Sub

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim ConnectionString As String = SessionInt("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(SessionInt("DC_OSPITE"))

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        REM DataFatt = "2015-01-01"
        Dim Indice As Integer = 0
        Dim NumeroRighe As Integer = 0
        Dim Pagina = 1

        Dim UrlConnessione As String = ""




        Dim VettoreBu(100) As Integer
        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Sicurezza".ToUpper) > 0 Or Session("DC_OSPITE").ToString.ToUpper.IndexOf("ODA".ToUpper) > 0 Then
            ListaBussineUnit(Token, Context, VettoreBu)
        Else
            VettoreBu(0) = BusinessUnit(Token, SessionInt)
        End If


        Do While VettoreBu(Indice) > 0
            SessionInt("APPOGGIO") = ""
            SessionInt("APPOGGIOPARENTE") = ""
            Session("LETTOMOVIMENTI") = ""

            Pagina = 0
            AppoggioLOG = AppoggioLOG & " " & VettoreBu(Indice)
            Do
                NumeroRighe = 0
                If Pagina = 0 Then
                    ' UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/from/" & DataFatt
                    UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & VettoreBu(Indice) & "/guests/updated/" & DataFatt & "?per_page=500"
                Else
                    'UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/from/" & DataFatt & "?page=" & Indice
                    UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & VettoreBu(Indice) & "/guests/updated/" & DataFatt & "?per_page=500&page=" & Pagina

                End If
                Pagina = Pagina + 1

                Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

                client.Method = "GET"
                client.Headers.Add("Authorization", "Bearer " & Token)
                client.ContentType = "Content-Type: application/json"

                Dim reader As StreamReader
                Dim response As HttpWebResponse = Nothing

                response = DirectCast(client.GetResponse(), HttpWebResponse)

                reader = New StreamReader(response.GetResponseStream())


                Dim rawresp As String
                rawresp = reader.ReadToEnd()


                Dim jResults As JArray = JArray.Parse(rawresp)


                For Each jTok As JToken In jResults
                    Dim M As New ClsOspite
                    Dim TId As Integer
                    M.CodiceOspite = 0
                    M.CODICEFISCALE = jTok.Item("cf").ToString()

                    Try
                        TId = jTok.Item("id").ToString()
                    Catch ex As Exception

                    End Try

                    NumeroRighe = NumeroRighe + 1

                    M.LeggiPerCodiceFiscale(SessionInt("DC_OSPITE"), M.CODICEFISCALE)


                    If M.CODICEFISCALE = "FRNLCU77T05B936V" Then
                        M.CODICEFISCALE = "FRNLCU77T05B936V"
                    End If
                    If M.CodiceOspite = 0 Then
                        M.IdEpersonam = TId
                        M.LeggiPerIdEpersonam(SessionInt("DC_OSPITE"))
                    End If
                    If M.CodiceOspite > 0 Then
                        Dim AppoCognome As String
                        Dim AppoNome As String
                        Dim AppoDatascinta As Date
                        Dim AppoCf As String
                        Dim AppoSesso As String
                        Dim AppoProvinciaDiNascita As String
                        Dim AppoComuneDiNascita As String
                        Dim AppoRESIDENZATELEFONO1 As String
                        Dim AppoRESIDENZAINDIRIZZO1 As String
                        Dim AppoRESIDENZACAP1 As String
                        Dim AppoRESIDENZAPROVINCIA1 As String
                        Dim AppoRESIDENZACOMUNE1 As String

                        Dim AppoRESIDENZATELEFONO2 As String
                        Dim AppoRESIDENZATELEFONO3 As String
                        Dim AppoTELEFONO1 As String
                        Dim AppoAssistenteSociale As String
                        Dim AppoTesseraSanitaria As String

                        AppoCognome = M.CognomeOspite
                        AppoNome = M.NomeOspite
                        AppoDatascinta = M.DataNascita
                        AppoCf = M.CODICEFISCALE
                        AppoSesso = M.Sesso
                        AppoProvinciaDiNascita = M.ProvinciaDiNascita
                        AppoComuneDiNascita = M.ComuneDiNascita
                        AppoRESIDENZATELEFONO1 = M.RESIDENZATELEFONO1
                        AppoRESIDENZAINDIRIZZO1 = M.RESIDENZAINDIRIZZO1
                        AppoRESIDENZACAP1 = M.RESIDENZACAP1
                        AppoRESIDENZAPROVINCIA1 = M.RESIDENZAPROVINCIA1
                        AppoRESIDENZACOMUNE1 = M.RESIDENZACOMUNE1

                        AppoRESIDENZATELEFONO2 = M.RESIDENZATELEFONO2
                        AppoRESIDENZATELEFONO3 = M.RESIDENZATELEFONO3
                        AppoTELEFONO1 = M.TELEFONO1
                        AppoAssistenteSociale = M.AssistenteSociale
                        AppoTesseraSanitaria = M.TesseraSanitaria

                        M.CognomeOspite = jTok.Item("surname").ToString()
                        M.NomeOspite = jTok.Item("firstname").ToString()
                        M.Nome = M.CognomeOspite & " " & M.NomeOspite
                        Try
                            M.DataNascita = campodb(jTok.Item("birthdate").ToString())
                        Catch ex As Exception

                        End Try

                        If Year(M.DataNascita) < 1800 Then
                            M.DataNascita = Now
                        End If

                        M.CODICEFISCALE = jTok.Item("cf").ToString()

                        Try
                            If Val(jTok.Item("gender").ToString()) = 1 Then
                                M.Sesso = "M"

                            Else
                                M.Sesso = "F"
                            End If
                        Catch ex As Exception

                        End Try

                        Dim appoggio As String
                        Try
                            appoggio = jTok.Item("birthcity").ToString()

                            M.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
                            M.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
                        Catch ex As Exception

                        End Try

                        Try
                            M.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.TELEFONO1 = jTok.Item("fax").ToString()
                        Catch ex As Exception

                        End Try



                        Try
                            M.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.RESIDENZACAP1 = jTok.Item("rescap").ToString()
                        Catch ex As Exception

                        End Try


                        Try
                            M.NomeMedico = jTok.Item("doctor_fullname").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.AssistenteSociale = jTok.Item("ass_soc_fullname").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            M.TesseraSanitaria = jTok.Item("numero_identificazione_tessera_sanitaria").ToString()
                        Catch ex As Exception

                        End Try


                        Try
                            appoggio = jTok.Item("rescity").ToString()

                            M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
                            M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

                        Catch ex As Exception

                        End Try


                        Try
                            M.IdEpersonam = jTok.Item("id").ToString()
                        Catch ex As Exception

                        End Try

                        If SessionInt("SOLOVERIFICA") = "SI" Then

                            If Trim(AppoCognome) <> Trim(M.CognomeOspite) Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "CognomeOspite")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCognome)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CognomeOspite)
                                CmdI.ExecuteNonQuery()
                            End If
                            If Trim(AppoNome) <> Trim(M.NomeOspite) Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "NomeOspite")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoNome)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.NomeOspite)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoDatascinta <> M.DataNascita Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "DataNascita")
                                If AppoDatascinta.Year = 1 Then
                                    CmdI.Parameters.AddWithValue("@ValoreSenior", Now)
                                Else
                                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoDatascinta)
                                End If
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.DataNascita)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoCf <> M.CODICEFISCALE Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "CODICEFISCALE")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCf)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CODICEFISCALE)
                                CmdI.ExecuteNonQuery()
                            End If

                            If AppoSesso <> M.Sesso Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "SESSO")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoSesso)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.Sesso)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoProvinciaDiNascita <> M.ProvinciaDiNascita Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "ProvinciaDiNascita")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoProvinciaDiNascita)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ProvinciaDiNascita)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoComuneDiNascita <> M.ComuneDiNascita Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "ComuneDiNascita")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoComuneDiNascita)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ComuneDiNascita)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoRESIDENZATELEFONO1 <> M.RESIDENZATELEFONO1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO1)
                                CmdI.ExecuteNonQuery()
                            End If

                            If AppoRESIDENZATELEFONO2 <> M.RESIDENZATELEFONO2 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO2")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO2)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO2)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoRESIDENZATELEFONO3 <> M.RESIDENZATELEFONO3 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO3")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO3)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO3)
                                CmdI.ExecuteNonQuery()
                            End If

                            If AppoTELEFONO1 <> M.TELEFONO1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTELEFONO1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.TELEFONO1)
                                CmdI.ExecuteNonQuery()
                            End If

                            If AppoRESIDENZAINDIRIZZO1 <> M.RESIDENZAINDIRIZZO1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAINDIRIZZO1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAINDIRIZZO1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAINDIRIZZO1)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoRESIDENZACAP1 <> M.RESIDENZACAP1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACAP1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACAP1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACAP1)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoRESIDENZAPROVINCIA1 <> M.RESIDENZAPROVINCIA1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "AppoRESIDENZAPROVINCIA1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAPROVINCIA1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAPROVINCIA1)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoRESIDENZACOMUNE1 <> M.RESIDENZACOMUNE1 Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACOMUNE1")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACOMUNE1)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACOMUNE1)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoAssistenteSociale <> M.AssistenteSociale Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "AssistenteSociale")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoAssistenteSociale)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.AssistenteSociale)
                                CmdI.ExecuteNonQuery()
                            End If
                            If AppoTesseraSanitaria <> M.TesseraSanitaria Then
                                Dim CmdI As New OleDbCommand
                                CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                            " (?, ?, ?, ?,?)"
                                CmdI.Connection = cn
                                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                                CmdI.Parameters.AddWithValue("@Campo", "TesseraSanitaria")
                                CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTesseraSanitaria)
                                CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.TesseraSanitaria)
                                CmdI.ExecuteNonQuery()
                            End If

                        Else
                            M.ScriviOspite(SessionInt("DC_OSPITE"))


                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_AllineamentoAnagrafica (CognomeOspite ,NomeOspite ,Nome , DataNascita ,  CODICEFISCALE ,Sesso , ProvinciaDiNascita , ComuneDiNascita , RESIDENZATELEFONO1 , RESIDENZAINDIRIZZO1  ,RESIDENZACAP1 ,  RESIDENZAPROVINCIA1 , RESIDENZACOMUNE1, EMail, Cellulare, Fax ,CodiceOspite ,CodiceParente, Data) VALUES " & _
                                        " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CognomeOspite", AppoCognome)
                            CmdI.Parameters.AddWithValue("@NomeOspite", AppoNome)
                            CmdI.Parameters.AddWithValue("@Nome", AppoCognome & " " & AppoNome)
                            CmdI.Parameters.AddWithValue("@DataNascita", AppoDatascinta)
                            CmdI.Parameters.AddWithValue("@CODICEFISCALE", AppoCf)
                            CmdI.Parameters.AddWithValue("@Sesso", AppoSesso)
                            CmdI.Parameters.AddWithValue("@ProvinciaDiNascita", AppoProvinciaDiNascita)
                            CmdI.Parameters.AddWithValue("@ComuneDiNascita", AppoComuneDiNascita)
                            CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO1", AppoRESIDENZATELEFONO1)
                            CmdI.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", AppoRESIDENZAINDIRIZZO1)
                            CmdI.Parameters.AddWithValue("@RESIDENZACAP1", AppoRESIDENZACAP1)
                            CmdI.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", AppoRESIDENZAPROVINCIA1)
                            CmdI.Parameters.AddWithValue("@RESIDENZACOMUNE1", AppoRESIDENZACOMUNE1)

                            CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO2", AppoRESIDENZATELEFONO2)
                            CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO3", AppoRESIDENZATELEFONO3)
                            CmdI.Parameters.AddWithValue("@TELEFONO1", AppoTELEFONO1)

                            CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                            CmdI.Parameters.AddWithValue("@Data", Now)
                            Try
                                CmdI.ExecuteNonQuery()
                            Catch ex As Exception

                            End Try

                        End If



                        VerificaParente(M.CODICEFISCALE, M.CodiceOspite, Token, SessionInt("SOLOVERIFICA"))
                    Else
                        If SessionInt("SOLOVERIFICA") = "SI" Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", 0)
                            CmdI.Parameters.AddWithValue("@Campo", "CodiceFiscale")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", "")
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CODICEFISCALE)
                            CmdI.ExecuteNonQuery()
                        Else
                            Dim CodiceFiscale As String = M.CODICEFISCALE
                            Dim Letto As String

                            Dim CentroServizio As String = ""
                            Dim DataAcco As Date = Nothing
                            Dim DataAppoggio As String = Nothing


                            Dim kEp As New Cls_Epersonam
                            kEp.VisuallizzaAccoglimentoOspite(Token, SessionInt("DC_OSPITE"), True, HttpContext.Current, M.CODICEFISCALE, CentroServizio, DataAppoggio, VettoreBu(Indice))

                            If Len(DataAppoggio) > 10 Then
                                DataAcco = Mid(DataAppoggio, 1, 10)
                            Else
                                If Not IsNothing(DataAppoggio) Then
                                    If DataAppoggio = "" Then
                                        DataAcco = Now
                                    Else
                                        DataAcco = DataAppoggio
                                    End If
                                End If
                            End If

                            If Format(DataAcco, "yyyyMMdd") > Format(Now, "yyyyMMdd") Or Param.NonInAtteasa = 0 Then

                                Dim Cs As New Cls_CentroServizio

                                Cs.CENTROSERVIZIO = CentroServizio

                                Cs.Leggi(SessionInt("DC_OSPITE"), Cs.CENTROSERVIZIO)


                                If Cs.DESCRIZIONE <> "" Then

                                    Dim Anag As New ClsOspite

                                    M.CognomeOspite = jTok.Item("surname").ToString()
                                    M.NomeOspite = jTok.Item("firstname").ToString()
                                    M.Nome = M.CognomeOspite & " " & M.NomeOspite

                                    Try
                                        M.DataNascita = campodb(jTok.Item("birthdate").ToString())
                                    Catch ex As Exception
                                        M.DataNascita = Now
                                    End Try
                                    If Year(M.DataNascita) < 1900 Then
                                        M.DataNascita = Now
                                    End If


                                    M.InserisciOspite(SessionInt("DC_OSPITE"), M.CognomeOspite, M.NomeOspite, M.DataNascita)

                                    M.Leggi(SessionInt("DC_OSPITE"), M.CodiceOspite)

                                    M.CODICEFISCALE = jTok.Item("cf").ToString()

                                    Try
                                        If Val(jTok.Item("gender").ToString()) = 1 Then
                                            M.Sesso = "M"

                                        Else
                                            M.Sesso = "F"
                                        End If
                                    Catch ex As Exception

                                    End Try

                                    Dim appoggio As String
                                    Try
                                        appoggio = jTok.Item("birthcity").ToString()

                                        M.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
                                        M.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.TELEFONO1 = jTok.Item("fax").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.RESIDENZACAP1 = jTok.Item("rescap").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.NomeMedico = jTok.Item("doctor_fullname").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.AssistenteSociale = jTok.Item("ass_soc_fullname").ToString()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        M.TesseraSanitaria = jTok.Item("numero_identificazione_tessera_sanitaria").ToString()
                                    Catch ex As Exception

                                    End Try





                                    Try
                                        appoggio = jTok.Item("rescity").ToString()

                                        M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
                                        M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

                                    Catch ex As Exception

                                    End Try

                                    Try
                                        Letto = jTok.Item("bed").ToString()
                                    Catch ex As Exception

                                    End Try

                                    M.ScriviOspite(SessionInt("DC_OSPITE"))






                                    Dim Pc As New Cls_Pianodeiconti
                                    Pc.Mastro = Cs.MASTRO
                                    Pc.Conto = Cs.CONTO
                                    Pc.Sottoconto = M.CodiceOspite * 100
                                    Pc.Decodfica(SessionInt("DC_GENERALE"))
                                    Pc.Mastro = Cs.MASTRO
                                    Pc.Conto = Cs.CONTO
                                    Pc.Sottoconto = M.CodiceOspite * 100
                                    Pc.Descrizione = M.Nome
                                    Pc.Tipo = "A"
                                    Pc.TipoAnagrafica = "O"
                                    Pc.Scrivi(SessionInt("DC_GENERALE"))




                                    Dim Trovato As Boolean = False



                                    Dim TipoMovimentoSenior As String = ""
                                    Dim Progressivo As Long
                                    Dim MySql As String


                                    TipoMovimentoSenior = "05"

                                    Dim xtr As OleDbTransaction = cn.BeginTransaction()


                                    Dim MovLetto As New Cls_MovimentiStanze


                                    If Trim(Letto) <> "" Then
                                        MovLetto.CentroServizio = Cs.CENTROSERVIZIO
                                        MovLetto.CodiceOspite = M.CodiceOspite
                                        MovLetto.Data = DataAcco
                                        MovLetto.Tipologia = "OC"
                                        MovLetto.Villa = "01"
                                        If Mid(Letto, 1, 1) = "1" Then
                                            MovLetto.Reparto = "01"
                                        End If
                                        If Mid(Letto, 1, 1) = "2" Then
                                            MovLetto.Reparto = "02"
                                        End If
                                        If Mid(Letto, 1, 1) = "3" Then
                                            MovLetto.Reparto = "03"
                                        End If
                                        If Mid(Letto, 1, 1) = "4" Then
                                            MovLetto.Reparto = "04"
                                        End If
                                        MovLetto.Piano = ""
                                        MovLetto.Stanza = ""
                                        MovLetto.Letto = Letto
                                        MovLetto.AggiornaDB(SessionInt("DC_OSPITIACCESSORI"))
                                    End If


                                    Dim cmdIns As New OleDbCommand()
                                    cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                                    cmdIns.Connection = cn
                                    cmdIns.Transaction = xtr
                                    cmdIns.Parameters.AddWithValue("@IdEpersonam", 1)
                                    cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
                                    cmdIns.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
                                    cmdIns.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                    cmdIns.Parameters.AddWithValue("@Data", Format(DataAcco, "dd/MM/yyyy"))
                                    cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
                                    cmdIns.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
                                    cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                                    cmdIns.ExecuteNonQuery()

                                    Dim cmd1 As New OleDbCommand()
                                    cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & Cs.CENTROSERVIZIO & "' And  CodiceOspite = " & M.CodiceOspite)
                                    cmd1.Connection = cn
                                    cmd1.Transaction = xtr
                                    Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                                    If myPOSTreader1.Read Then
                                        If Not IsDBNull(myPOSTreader1.Item(0)) Then
                                            Progressivo = myPOSTreader1.Item(0) + 1
                                        Else
                                            Progressivo = 1
                                        End If
                                    Else
                                        Progressivo = 1
                                    End If

                                    Dim MyData As Date = DataAcco

                                    MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
                                    Dim cmdw As New OleDbCommand()
                                    cmdw.CommandText = (MySql)
                                    cmdw.Transaction = xtr
                                    cmdw.Parameters.AddWithValue("@Utente", SessionInt("UTENTE"))
                                    cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                    cmdw.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
                                    cmdw.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
                                    cmdw.Parameters.AddWithValue("@Data", DateSerial(Year(MyData), Month(MyData), Day(MyData)))
                                    cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
                                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
                                    cmdw.Parameters.AddWithValue("@CAUSALE", "")
                                    cmdw.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
                                    cmdw.Parameters.AddWithValue("@EPersonam", 1)
                                    cmdw.Connection = cn
                                    cmdw.ExecuteNonQuery()

                                    xtr.Commit()
                                End If
                            End If
                    End If
                    End If
                Next
            Loop While NumeroRighe > 0
            Indice = Indice + 1
        Loop
        cn.Close()



    End Sub


    Protected Sub OspitiWeb_allineaospitiparenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.Item("UTENTE") <> "" And Request.Item("PASSWORD") <> "" Then
            Dim k As New Cls_Login

            k.Utente = Request.Item("UTENTE")
            k.Chiave = Request.Item("PASSWORD")
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            If k.Ospiti <> "" Then
                Session("UTENTE") = k.Utente
            End If

            k.Utente = UCase(k.Utente)
            k.Chiave = k.Chiave

            k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
            k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr

            Session("DC_OSPITE") = k.Ospiti
            Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
            Session("DC_TABELLE") = k.TABELLE
            Session("DC_GENERALE") = k.Generale
            Session("DC_TURNI") = k.Turni
            Session("STAMPEOSPITI") = k.STAMPEOSPITI
            Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
            Session("ProgressBar") = ""
            Session("CODICEREGISTRO") = ""
            Session("NomeEPersonam") = k.NomeEPersonam
            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = k.Utente.ToLower

        End If

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If




        Session("SOLOVERIFICA") = Request.Item("SOLOVERIFICA")

        DoWork(Session)

        REM Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        REM t.Start(Session)


        If Request.Item("SOLOVERIFICA") = "SI" Then

            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim cn As OleDbConnection
            Dim Tabella As String = ""
            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()


            Tabella = "<tr><th>Codice Ospite</th><th>Codice Parente</th><th>Cognome Nome</th><ht>Campo</th><th>Valore Senior</th>Valore Epersonam</th></tr>"
            Dim Extrai As New OleDbCommand

            Extrai.Connection = cn
            Extrai.CommandText = "SELECT TOP 1000 [CodiceOspite],[CodiceParente],(SELECT top 1 NOME FROM AnagraficaComune WHERE AnagraficaComune.CODICEOSPITE =[Log_VariazioneDati].CodiceOspite AND AnagraficaComune.CodiceParente =[Log_VariazioneDati].CodiceParente) as CognomeNome ,[Campo] ,[ValoreSenior] ,[ValoreEpersonam]  FROM [Log_VariazioneDati] ORDER BY CognomeNome"


            Dim ReadDati As OleDbDataReader = Extrai.ExecuteReader()
            Do While ReadDati.Read
                Tabella = Tabella & "<tr><td>" & campodbN(ReadDati.Item("CodiceOspite")) & "</td><td>" & campodbN(ReadDati.Item("CodiceParente")) & "</td><td>" & campodb(ReadDati.Item("CognomeNome")) & "</td><td>" & campodb(ReadDati.Item("Campo")) & "</td><td>" & campodb(ReadDati.Item("ValoreSenior")) & "</td><td>" & campodb(ReadDati.Item("ValoreEpersonam")) & "</td></tr>"
            Loop
            cn.Close()


            Response.Clear()
            Response.ClearContent()
            Response.ClearHeaders()
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.AddHeader("content-disposition", "attachment;filename=DatiAnagrafici.xls")
            Response.ContentType = "application/vnd.xls"
            Response.Write("<table>" & Tabella & "</table>")
            Response.Flush()
            Response.End()
        End If


        If Request.Item("RICERCAOSPITI") = "SI" Then
            If Request.Item("TIPO") = "" Then
                Response.Redirect("RicercaAnagrafica.aspx")
            Else
                Response.Redirect("RicercaAnagrafica.aspx?TIPO=" & Request.Item("TIPO"))
            End If
        Else
            Response.Redirect("Menu_Epersonam.aspx")
        End If

    End Sub


    Private Sub VerificaParente(ByVal CodiceFiscale As String, ByVal CodiceOspite As Long, ByVal Token As String, ByVal SOLOVERIFICA As String)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        If CodiceFiscale = "" Then
            Exit Sub
        End If


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing
        Dim rawresp As String = ""


        Try
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & CodiceFiscale & "/relatives")

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()
        Catch ex As Exception

            Dim CmdI As New OleDbCommand
            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                        " (?, ?, ?, ?,?)"
            CmdI.Connection = cn
            CmdI.Parameters.AddWithValue("@CodiceOspite", 0)
            CmdI.Parameters.AddWithValue("@CodiceParente", 0)
            CmdI.Parameters.AddWithValue("@Campo", "ERRORE")
            CmdI.Parameters.AddWithValue("@ValoreSenior", "Errore " & "https://api-v0.e-personam.com/v0/guests/" & CodiceFiscale & "/relatives")
            CmdI.Parameters.AddWithValue("@ValoreEpersonam", ex.Message)
            CmdI.ExecuteNonQuery()
        End Try


        Dim jResults As JArray

        Try
            jResults = JArray.Parse(rawresp)
        Catch ex As Exception

        End Try


        Dim Appoggio As String
        Dim Modifica As Boolean

        If IsDBNull(rawresp) Or rawresp = "" Then
            Exit Sub
        End If


        If IsDBNull(jResults) Then
            Exit Sub
        End If

        For Each jTok2 As JToken In jResults

            'If jTok2.Item("signed_to_pay") = "true" Then
            Dim Prova As New Cls_Parenti
            If CodiceOspite = 1687 Then
                CodiceOspite = 1687
            End If

            Prova.CodiceOspite = CodiceOspite
            Try
                Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
            Catch ex As Exception

            End Try

            Try
                Prova.IdEpersonam = jTok2.Item("id").ToString()
            Catch ex As Exception

            End Try


            Dim kv6 As New Cls_CodiceFiscale

            If Prova.CODICEFISCALE <> "" Then
                If kv6.Check_CodiceFiscale(Prova.CODICEFISCALE) = True Then

                    Modifica = True
                    Prova.Nome = ""
                    Prova.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Prova.CODICEFISCALE)
                    If Prova.Nome = "" Then
                        Prova.LeggiPerIdEpersonam(Session("DC_OSPITE"), Prova.IdEpersonam)
                        If Prova.Nome = "" Then
                            Modifica = False
                        End If
                    End If

                    Dim AppoCognome As String
                    Dim AppoNome As String
                    Dim AppoDatascinta As Date
                    Dim AppoCf As String
                    Dim AppoSesso As String
                    Dim AppoProvinciaDiNascita As String
                    Dim AppoComuneDiNascita As String
                    Dim AppoTELEFONO1 As String
                    Dim AppoEmail As String
                    Dim AppoRESIDENZAINDIRIZZO1 As String
                    Dim AppoRESIDENZACAP1 As String
                    Dim AppoRESIDENZAPROVINCIA1 As String
                    Dim AppoRESIDENZACOMUNE1 As String
                    Dim AppoPARENTEINDIRIZZO As Integer


                    AppoCognome = Prova.CognomeParente
                    AppoNome = Prova.NomeParente
                    AppoDatascinta = Prova.DataNascita
                    AppoCf = Prova.CODICEFISCALE
                    AppoSesso = Prova.Sesso
                    AppoProvinciaDiNascita = Prova.ProvinciaDiNascita
                    AppoComuneDiNascita = Prova.ComuneDiNascita
                    AppoTELEFONO1 = Prova.Telefono1
                    AppoEmail = Prova.Telefono3
                    AppoRESIDENZAINDIRIZZO1 = Prova.RESIDENZAINDIRIZZO1
                    AppoRESIDENZACAP1 = Prova.RESIDENZACAP1
                    AppoRESIDENZAPROVINCIA1 = Prova.RESIDENZAPROVINCIA1
                    AppoRESIDENZACOMUNE1 = Prova.RESIDENZACOMUNE1
                    AppoPARENTEINDIRIZZO = Prova.ParenteIndirizzo




                    Prova.Nome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString
                    Prova.CognomeParente = jTok2.Item("surname").ToString
                    Prova.NomeParente = jTok2.Item("firstname").ToString

                    If Prova.CognomeParente = "DR SERNI" Then
                        Prova.CognomeParente = "DR SERNI"
                    End If
                    Try
                        Prova.Telefono1 = jTok2.Item("phone").ToString
                    Catch ex As Exception

                    End Try
                    Try
                        Prova.Telefono3 = jTok2.Item("email").ToString
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZACAP1 = jTok2.Item("rescap").ToString
                    Catch ex As Exception

                    End Try


                    Try
                        Appoggio = jTok2.Item("rescity_alpha6").ToString()

                        Prova.RESIDENZAPROVINCIA1 = Mid(Appoggio & Space(6), 1, 3)
                        Prova.RESIDENZACOMUNE1 = Mid(Appoggio & Space(6), 4, 3)
                    Catch ex As Exception

                    End Try


                    Try
                        If jTok2.Item("signed_to_receive_bill") = "true" Or jTok2.Item("signed_to_receive_bill") = "True" Then
                            Prova.ParenteIndirizzo = 1
                        Else
                            Prova.ParenteIndirizzo = 0
                        End If
                    Catch ex As Exception
                        Prova.ParenteIndirizzo = 0
                    End Try

                    Try
                        Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZAINDIRIZZO1 = jTok2.Item("resaddress").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.IdEpersonam = jTok2.Item("id").ToString()
                    Catch ex As Exception

                    End Try

                    If SOLOVERIFICA = "SI" Then
                        If AppoCognome <> Prova.CognomeParente Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "CognomeParente")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCognome)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.CognomeParente)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoNome <> Prova.NomeParente Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "NomeParente")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoNome)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.NomeParente)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoCf <> Prova.CODICEFISCALE Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "CODICEFISCALE")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCf)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.CODICEFISCALE)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZAINDIRIZZO1 <> Prova.RESIDENZAINDIRIZZO1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAINDIRIZZO1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAINDIRIZZO1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZAINDIRIZZO1)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZAPROVINCIA1 <> Prova.RESIDENZAPROVINCIA1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAPROVINCIA1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAPROVINCIA1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZAPROVINCIA1)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZACOMUNE1 <> Prova.RESIDENZACOMUNE1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACOMUNE1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACOMUNE1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZACOMUNE1)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoTELEFONO1 <> Prova.Telefono1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "Telefono1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTELEFONO1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.Telefono1)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoEmail <> Prova.Telefono3 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "Email")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoEmail)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.Telefono3)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoPARENTEINDIRIZZO <> Prova.ParenteIndirizzo Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "ParenteIndirizzo")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoPARENTEINDIRIZZO)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.ParenteIndirizzo)
                            CmdI.ExecuteNonQuery()
                        End If


                    Else
                        Dim CmdZ As New OleDbCommand
                        CmdZ.CommandText = "INSERT INTO  Log_AllineamentoAnagrafica (CognomeOspite ,NomeOspite ,Nome , CODICEFISCALE , RESIDENZAINDIRIZZO1  ,RESIDENZAPROVINCIA1 , RESIDENZACOMUNE1 ,CodiceOspite ,CodiceParente, Data) VALUES " & _
                                    " (? ,? ,?, ?, ? ,?, ? ,? ,?, ?)"
                        CmdZ.Connection = cn
                        CmdZ.Parameters.AddWithValue("@CognomeOspite", Prova.CognomeParente)
                        CmdZ.Parameters.AddWithValue("@NomeOspite", Prova.NomeParente)
                        CmdZ.Parameters.AddWithValue("@Nome", Prova.Nome)
                        CmdZ.Parameters.AddWithValue("@CODICEFISCALE", Prova.CODICEFISCALE)
                        CmdZ.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", Prova.RESIDENZAINDIRIZZO1)
                        CmdZ.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", Prova.RECAPITOPROVINCIA)
                        CmdZ.Parameters.AddWithValue("@RESIDENZACOMUNE1", Prova.RESIDENZACOMUNE1)
                        CmdZ.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                        CmdZ.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                        CmdZ.Parameters.AddWithValue("@Data", Now)
                        CmdZ.ExecuteNonQuery()


                        If Modifica = True Then

                            Prova.ScriviParente(Session("DC_OSPITE"))

                        Else

                            Prova.ScriviParente(Session("DC_OSPITE"))
                            Dim UltimoCs As New Cls_Movimenti

                            UltimoCs.UltimaData(Session("DC_OSPITE"), CodiceOspite)

                            Dim Cs As New Cls_CentroServizio

                            Cs.Leggi(Session("DC_OSPITE"), UltimoCs.CENTROSERVIZIO)

                            Dim Pc As New Cls_Pianodeiconti
                            Pc.Mastro = Cs.MASTRO
                            Pc.Conto = Cs.CONTO
                            Pc.Sottoconto = CodiceOspite * 100 + Prova.CodiceParente
                            Pc.Decodfica(Session("DC_GENERALE"))

                            Pc.Mastro = Cs.MASTRO
                            Pc.Conto = Cs.CONTO
                            Pc.Sottoconto = CodiceOspite * 100 + Prova.CodiceParente
                            Pc.Descrizione = Prova.Nome
                            Pc.Tipo = "A"
                            Pc.TipoAnagrafica = "P"
                            Pc.Scrivi(Session("DC_GENERALE"))

                        End If
                        REM Session("APPOGGIOPARENTE") = Session("APPOGGIOPARENTE") & CodiceFiscale & ";" & Prova.CognomeParente & ";" & Prova.NomeParente & ";" & Prova.Telefono1 & ";" & Prova.RESIDENZAPROVINCIA1 & ";" & Prova.RESIDENZACOMUNE1 & ";" & Prova.CODICEFISCALE & ";" & Prova.RESIDENZAINDIRIZZO1 & vbNewLine



                        Dim MyBill As String = ""


                        Try
                            MyBill = jTok2.Item("signed_to_receive_bill")
                        Catch ex As Exception

                        End Try
                        If UCase(MyBill) = "TRUE" Then
                            Dim Anag As New ClsOspite
                            Anag.Leggi(Session("DC_OSPITE"), CodiceOspite)
                            Anag.RecapitoNome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString

                            If CodiceOspite = 298 Then
                                CodiceOspite = 298
                            End If
                            Try
                                Appoggio = jTok2.Item("rescity_alpha6").ToString()
                                Anag.RecapitoProvincia = Mid(Appoggio & Space(6), 1, 3)
                                Anag.RecapitoComune = Mid(Appoggio & Space(6), 4, 3)
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.RecapitoIndirizzo = jTok2.Item("resaddress").ToString()
                            Catch ex As Exception

                            End Try


                            Try
                                Anag.RESIDENZACAP4 = jTok2.Item("rescap").ToString
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.RESIDENZATELEFONO4 = jTok2.Item("phone").ToString
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.TELEFONO3 = jTok2.Item("email").ToString
                            Catch ex As Exception

                            End Try

                            Dim cmdOspite As New OleDbCommand

                            cmdOspite.CommandText = "UPDATE AnagraficaComune Set RecapitoNome=?,RecapitoProvincia=?,RecapitoComune=?,RecapitoIndirizzo=?,RESIDENZATELEFONO4=?,RESIDENZACAP4=?,TELEFONO4=? Where CodiceOspite = ? And Tipologia = 'O'"
                            cmdOspite.Connection = cn

                            If Anag.RecapitoNome.Length > 50 Then
                                Anag.RecapitoNome = Mid(Anag.RecapitoNome, 1, 50)
                            End If
                            If Anag.RecapitoProvincia.Length > 3 Then
                                Anag.RecapitoProvincia = Mid(Anag.RecapitoProvincia, 1, 3)
                            End If
                            If Anag.RecapitoComune.Length > 3 Then
                                Anag.RecapitoComune = Mid(Anag.RecapitoComune, 1, 3)
                            End If
                            If Anag.RecapitoIndirizzo.Length > 50 Then
                                Anag.RecapitoIndirizzo = Mid(Anag.RecapitoIndirizzo, 1, 50)
                            End If
                            If Anag.RESIDENZATELEFONO4.Length > 100 Then
                                Anag.RESIDENZATELEFONO4 = Mid(Anag.RESIDENZATELEFONO4, 1, 100)
                            End If
                            If Anag.TELEFONO3.Length > 100 Then
                                Anag.TELEFONO3 = Mid(Anag.TELEFONO3, 1, 100)
                            End If

                            cmdOspite.Parameters.AddWithValue("@RecapitoNome", Anag.RecapitoNome)
                            cmdOspite.Parameters.AddWithValue("@RecapitoProvincia", Anag.RecapitoProvincia)
                            cmdOspite.Parameters.AddWithValue("@RecapitoComune", Anag.RecapitoComune)
                            cmdOspite.Parameters.AddWithValue("@RecapitoIndirizzo", Anag.RecapitoIndirizzo)
                            cmdOspite.Parameters.AddWithValue("@RESIDENZATELEFONO4", Anag.RESIDENZATELEFONO4)
                            cmdOspite.Parameters.AddWithValue("@RESIDENZACAP4", Anag.RESIDENZACAP4)
                            cmdOspite.Parameters.AddWithValue("@TELEFONO4", Anag.TELEFONO3)
                            cmdOspite.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
                            cmdOspite.ExecuteNonQuery()
                        Else
                            If AppoPARENTEINDIRIZZO = 1 Then
                                Dim cmdOspite As New OleDbCommand

                                cmdOspite.CommandText = "UPDATE AnagraficaComune Set RecapitoNome=?,RecapitoProvincia=?,RecapitoComune=?,RecapitoIndirizzo=?,RESIDENZATELEFONO4=?,RESIDENZACAP4=?,TELEFONO4=?  Where CodiceOspite = ? And Tipologia ='O' "
                                cmdOspite.Connection = cn
                                cmdOspite.Parameters.AddWithValue("@RecapitoNome", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoProvincia", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoComune", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoIndirizzo", "")
                                cmdOspite.Parameters.AddWithValue("@RESIDENZATELEFONO4", "")
                                cmdOspite.Parameters.AddWithValue("@RESIDENZACAP4", "")
                                cmdOspite.Parameters.AddWithValue("@TELEFONO4", "")
                                cmdOspite.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
                                cmdOspite.ExecuteNonQuery()
                            End If

                        End If
                    End If
                End If
            End If
        Next

        cn.Close()


    End Sub

    Private Function LoginPersonam(ByVal context As Object) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")




        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")
        'request.Add("password", "advenias2014")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String


        If Trim(Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function



    Private Function ElencoSottoStrutture(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As Integer
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()
        Try
            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("structures").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                    ElencoSottoStrutture = Val(jTok2.Item("id").ToString)
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try

    End Function

    Private Sub ListaBussineUnit(ByVal Token As String, ByVal context As Object, ByRef VettoreBU() As Integer)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()


        Dim Indice As Integer

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("id").ToString) >= 0 Then
                VettoreBU(Indice) = Val(jTok2.Item("id").ToString)
                Indice = Indice + 1
            End If
        Next
    End Sub
    Private Function BusinessUnit(ByVal Token As String, ByVal context As Object) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Dim appoggio As String = context("NomeEPersonam")

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            Else
                If context("NomeEPersonam") = "PROVA" Then
                    BusinessUnit = 1825
                End If
            End If
        Next



    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    'Dim cmd As New OleDbCommand()
    '    cmd.CommandText = ("Select * From AnagraficaComune Where CodiceOspite >0 And CodiceParente = 0")
    '    cmd.Connection = cn

    'Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
    '    Do While myPOSTreader.Read
    '        Try

    'Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/" & campodb(myPOSTreader.Item("CODICEFISCALE")))

    '            client.Method = "GET"
    '            client.Headers.Add("Authorization", "Bearer " & Token)
    '            client.ContentType = "Content-Type: application/json"

    'Dim reader As StreamReader
    'Dim response As HttpWebResponse = Nothing

    '            response = DirectCast(client.GetResponse(), HttpWebResponse)

    '            reader = New StreamReader(response.GetResponseStream())
    'Dim rawresp As String


    '            rawresp = reader.ReadToEnd()


    'Dim jTok As JObject = JObject.Parse(rawresp)

    'Dim M As New ClsOspite
    '            M.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
    '            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)

    'Dim AppoCognome As String
    'Dim AppoNome As String
    'Dim AppoDatascinta As Date
    'Dim AppoCf As String
    'Dim AppoSesso As String
    'Dim AppoProvinciaDiNascita As String
    'Dim AppoComuneDiNascita As String
    'Dim AppoRESIDENZATELEFONO1 As String
    'Dim AppoRESIDENZAINDIRIZZO1 As String
    'Dim AppoRESIDENZACAP1 As String
    'Dim AppoRESIDENZAPROVINCIA1 As String
    'Dim AppoRESIDENZACOMUNE1 As String

    'Dim AppoRESIDENZATELEFONO2 As String
    'Dim AppoRESIDENZATELEFONO3 As String
    'Dim AppoTELEFONO1 As String

    '            AppoCognome = M.CognomeOspite
    '            AppoNome = M.NomeOspite
    '            AppoDatascinta = M.DataNascita
    '            AppoCf = M.CODICEFISCALE
    '            AppoSesso = M.Sesso
    '            AppoProvinciaDiNascita = M.ProvinciaDiNascita
    '            AppoComuneDiNascita = M.ComuneDiNascita
    '            AppoRESIDENZATELEFONO1 = M.RESIDENZATELEFONO1
    '            AppoRESIDENZAINDIRIZZO1 = M.RESIDENZAINDIRIZZO1
    '            AppoRESIDENZACAP1 = M.RESIDENZACAP1
    '            AppoRESIDENZAPROVINCIA1 = M.RESIDENZAPROVINCIA1
    '            AppoRESIDENZACOMUNE1 = M.RESIDENZACOMUNE1

    '            AppoRESIDENZATELEFONO2 = M.RESIDENZATELEFONO2
    '            AppoRESIDENZATELEFONO3 = M.RESIDENZATELEFONO3
    '            AppoTELEFONO1 = M.TELEFONO1


    '            M.CognomeOspite = jTok.Item("surname").ToString()
    '            M.NomeOspite = jTok.Item("firstname").ToString()
    '            M.Nome = M.CognomeOspite & " " & M.NomeOspite
    '            Try
    '                M.DataNascita = campodb(jTok.Item("birthdate").ToString())
    '            Catch ex As Exception

    '            End Try

    '            M.CODICEFISCALE = jTok.Item("cf").ToString()

    '            Try
    '                If Val(jTok.Item("gender").ToString()) = 1 Then
    '                    M.Sesso = "M"

    '                Else
    '                    M.Sesso = "F"
    '                End If
    '            Catch ex As Exception

    '            End Try

    'Dim appoggio As String
    '            Try
    '                appoggio = jTok.Item("birthcity").ToString()

    '                M.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
    '                M.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.TELEFONO1 = jTok.Item("fax").ToString()
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
    '            Catch ex As Exception

    '            End Try

    '            Try
    '                M.RESIDENZACAP1 = jTok.Item("rescap").ToString()
    '            Catch ex As Exception

    '            End Try


    '            Try
    '                appoggio = jTok.Item("rescity").ToString()

    '                M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
    '                M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

    '            Catch ex As Exception

    '            End Try

    '            If Request.Item("SOLOVERIFICA") = "SI" Then

    '                If Trim(AppoCognome) <> Trim(M.CognomeOspite) Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "CognomeOspite")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCognome)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CognomeOspite)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If Trim(AppoNome) <> Trim(M.NomeOspite) Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "NomeOspite")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoNome)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.NomeOspite)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoDatascinta <> M.DataNascita Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "DataNascita")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoDatascinta)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.DataNascita)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoCf <> M.CODICEFISCALE Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "CODICEFISCALE")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCf)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CODICEFISCALE)
    '                    CmdI.ExecuteNonQuery()
    '                End If

    '                If AppoSesso <> M.Sesso Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "SESSO")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoSesso)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.Sesso)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoProvinciaDiNascita <> M.ProvinciaDiNascita Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "ProvinciaDiNascita")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoProvinciaDiNascita)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ProvinciaDiNascita)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoComuneDiNascita <> M.ComuneDiNascita Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "ComuneDiNascita")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoComuneDiNascita)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ComuneDiNascita)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZATELEFONO1 <> M.RESIDENZATELEFONO1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO1)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZATELEFONO2 <> M.RESIDENZATELEFONO2 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO2")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO2)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO2)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZATELEFONO3 <> M.RESIDENZATELEFONO3 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO3")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO3)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO3)
    '                    CmdI.ExecuteNonQuery()
    '                End If

    '                If AppoTELEFONO1 <> M.TELEFONO1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "TELEFONO1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTELEFONO1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.TELEFONO1)
    '                    CmdI.ExecuteNonQuery()
    '                End If


    '                If AppoRESIDENZAINDIRIZZO1 <> M.RESIDENZAINDIRIZZO1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAINDIRIZZO1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAINDIRIZZO1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAINDIRIZZO1)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZACAP1 <> M.RESIDENZACAP1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACAP1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACAP1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACAP1)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZAPROVINCIA1 <> M.RESIDENZAPROVINCIA1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAPROVINCIA1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAPROVINCIA1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAPROVINCIA1)
    '                    CmdI.ExecuteNonQuery()
    '                End If
    '                If AppoRESIDENZACOMUNE1 <> M.RESIDENZACOMUNE1 Then
    'Dim CmdI As New OleDbCommand
    '                    CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                                " (?, ?, ?, ?,?)"
    '                    CmdI.Connection = cn
    '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                    CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACOMUNE1")
    '                    CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACOMUNE1)
    '                    CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACOMUNE1)
    '                    CmdI.ExecuteNonQuery()
    '                End If


    '            Else
    '                M.ScriviOspite(Session("DC_OSPITE"))


    'Dim CmdI As New OleDbCommand
    '                CmdI.CommandText = "INSERT INTO  Log_AllineamentoAnagrafica (CognomeOspite ,NomeOspite ,Nome , DataNascita ,  CODICEFISCALE ,Sesso , ProvinciaDiNascita , ComuneDiNascita , RESIDENZATELEFONO1 , RESIDENZAINDIRIZZO1  ,RESIDENZACAP1 ,  RESIDENZAPROVINCIA1 , RESIDENZACOMUNE1, EMail, Cellulare, Fax ,CodiceOspite ,CodiceParente, Data) VALUES " & _
    '                            " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    '                CmdI.Connection = cn
    '                CmdI.Parameters.AddWithValue("@CognomeOspite", AppoCognome)
    '                CmdI.Parameters.AddWithValue("@NomeOspite", AppoNome)
    '                CmdI.Parameters.AddWithValue("@Nome", AppoCognome & " " & AppoNome)
    '                CmdI.Parameters.AddWithValue("@DataNascita", AppoDatascinta)
    '                CmdI.Parameters.AddWithValue("@CODICEFISCALE", AppoCf)
    '                CmdI.Parameters.AddWithValue("@Sesso", AppoSesso)
    '                CmdI.Parameters.AddWithValue("@ProvinciaDiNascita", AppoProvinciaDiNascita)
    '                CmdI.Parameters.AddWithValue("@ComuneDiNascita", AppoComuneDiNascita)
    '                CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO1", AppoRESIDENZATELEFONO1)
    '                CmdI.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", AppoRESIDENZAINDIRIZZO1)
    '                CmdI.Parameters.AddWithValue("@RESIDENZACAP1", AppoRESIDENZACAP1)
    '                CmdI.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", AppoRESIDENZAPROVINCIA1)
    '                CmdI.Parameters.AddWithValue("@RESIDENZACOMUNE1", AppoRESIDENZACOMUNE1)

    '                CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO2", AppoRESIDENZATELEFONO2)
    '                CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO3", AppoRESIDENZATELEFONO3)
    '                CmdI.Parameters.AddWithValue("@TELEFONO1", AppoTELEFONO1)

    '                CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
    '                CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '                CmdI.Parameters.AddWithValue("@Data", Now)
    '                CmdI.ExecuteNonQuery()
    '            End If



    '            VerificaParente(M.CODICEFISCALE, M.CodiceOspite, Token)


    '        Catch ex As Exception
    'Dim CmdI As New OleDbCommand
    '            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
    '                        " (?, ?, ?, ?,?)"
    '            CmdI.Connection = cn
    '            CmdI.Parameters.AddWithValue("@CodiceOspite", Val(campodb(myPOSTreader.Item("CodiceOspite"))))
    '            CmdI.Parameters.AddWithValue("@CodiceParente", 0)
    '            CmdI.Parameters.AddWithValue("@Campo", "ERRORE")
    '            CmdI.Parameters.AddWithValue("@ValoreSenior", ex.Message)
    '            CmdI.Parameters.AddWithValue("@ValoreEpersonam", "")
    '            CmdI.ExecuteNonQuery()

    '        End Try

    '    Loop
    '    myPOSTreader.Close()


End Class




