﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_MesiDiurno
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim xCls As New Cls_Diurno

        Dim KT As New Cls_Parametri

        KT.LeggiParametri(Session("DC_OSPITE"))


        Txt_anno.Text = KT.AnnoFatturazione

        Dim ConnectionString As String = Session("DC_OSPITE")

        xCls.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Val(Txt_anno.Text), MyTable)
        

        ViewState("Appoggio") = MyTable
        Grd_ImportoOspite.AutoGenerateColumns = True
        Grd_ImportoOspite.DataSource = MyTable
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("Diurno.aspx")
    End Sub


    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand
        Dim d As Integer
        Dim Mese As Integer
        Dim Anno As Integer


        MyTable = ViewState("Appoggio")

        d = Val(e.CommandArgument)
        Anno = MyTable.Rows(d).Item(0).ToString
        Mese = MyTable.Rows(d).Item(1).ToString



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("Diurno.aspx?Mese=" & Mese & "&Anno=" & Anno & "&CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("RicercaAnagrafica.aspx?TIPO=DIURNO")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim xCls As New Cls_Diurno


        Dim ConnectionString As String = Session("DC_OSPITE")

        xCls.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Val(Txt_anno.Text), MyTable)


        ViewState("Appoggio") = MyTable
        Grd_ImportoOspite.AutoGenerateColumns = True
        Grd_ImportoOspite.DataSource = MyTable
        Grd_ImportoOspite.DataBind()
    End Sub
End Class
