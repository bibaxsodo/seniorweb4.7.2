﻿
Partial Class OspitiWeb_ForzadescrizioneDocumento
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim M As New Cls_MovimentoContabile


        M.Leggi(Session("DC_GENERALE"), TextBox1.Text, False)

        AnniSereni(M, Session("DC_OSPITE"), Session("DC_TABELLE"), Session("DC_GENERALE"))

    End Sub

    Private Sub AnniSereni(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)


        Dim GiorniPresenza As Integer = 0
        Dim ImportoPresenza As Double
        Dim CodiceOspite As Integer = 0
        Dim Riga As Integer = 0



        For Riga = 0 To 300

            If Not IsNothing(Documento.Righe(Riga)) Then
                If Documento.Righe(Riga).RigaDaCausale <> 9 Then
                    If Documento.Tipologia = "" Or Documento.Tipologia = "O" Or Documento.Tipologia = "P" Then
                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)



                        If Documento.Righe(Riga).RigaDaCausale = 4 Or Documento.Righe(Riga).RigaDaCausale = 5 Or Documento.Righe(Riga).RigaDaCausale = 6 Then
                            Documento.Righe(Riga).Descrizione = Documento.Righe(Riga).Descrizione
                        Else
                            If Conto.Descrizione = Documento.Righe(Riga).Descrizione Then
                                Documento.Righe(Riga).Descrizione = Conto.Descrizione
                            Else
                                Documento.Righe(Riga).Descrizione = Conto.Descrizione & "  " & UCase(Documento.Righe(Riga).Descrizione)
                            End If
                        End If
                    Else
                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)


                        Dim ContoControPartita As New Cls_Pianodeiconti

                        ContoControPartita.Mastro = Documento.Righe(Riga).MastroContropartita
                        ContoControPartita.Conto = Documento.Righe(Riga).ContoContropartita
                        ContoControPartita.Sottoconto = Documento.Righe(Riga).SottocontoContropartita
                        ContoControPartita.Decodfica(ConnessioneGenerale)
                        If Mid(Documento.Tipologia & Space(10), 1, 1) = "C" Then
                            Dim DesRigaComune As New Cls_ImportoComune

                            DesRigaComune.CODICEOSPITE = Int(Documento.Righe(Riga).SottocontoContropartita / 100)
                            DesRigaComune.CENTROSERVIZIO = Documento.CentroServizio
                            DesRigaComune.UltimaData(ConnessioneOspite, DesRigaComune.CODICEOSPITE, DesRigaComune.CENTROSERVIZIO)



                            Documento.Righe(Riga).Descrizione = Conto.Descrizione & "  " & ContoControPartita.Descrizione & " " & Documento.Righe(Riga).Descrizione & " " & DesRigaComune.DescrizioneRiga
                        Else
                            Documento.Righe(Riga).Descrizione = Conto.Descrizione & " " & Documento.Righe(Riga).Descrizione & "  " & ContoControPartita.Descrizione
                        End If
                    End If
                End If
            End If
        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, False)
    End Sub

End Class
