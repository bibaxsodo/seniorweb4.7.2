﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading

Partial Class OspitiWeb_Export_MAV
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0



        MySql = "SELECT MIN(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MassimoDocumento = 0



        MySql = "SELECT MAX(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text


        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub DD_Mese_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mese.SelectedIndexChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_AnnoRif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_AnnoRif.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub


    Protected Sub OspitiWeb_Export_Documenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Session("CampoProgressBar") = 0

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Txt_DataScadenza.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Data Scadenza');", True)
            Exit Sub
        End If


        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))

        If TabSoc.CodiceMittente = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Codice Mittente');", True)
            Exit Sub
        End If

        If TabSoc.CodiceMittente.Length <> 5 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Codice Mittente deve essere lungo 5 caratteri');", True)
            Exit Sub
        End If


        If TabSoc.CodiceRicevente = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Codice Ricevente');", True)
            Exit Sub
        End If

        If TabSoc.CodiceRicevente.Length <> 5 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Codice Ricevente deve essere lungo 5 caratteri');", True)
            Exit Sub
        End If

        If TabSoc.CODICEABI = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Codice Ricevente');", True)
            Exit Sub
        End If

        If TabSoc.CODICEABI.Length <> 5 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Codice ABI deve essere lungo 5 caratteri');", True)
            Exit Sub
        End If


        If TabSoc.CODICECAB = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Codice Ricevente');", True)
            Exit Sub
        End If

        If TabSoc.CODICECAB.Length <> 5 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Codice CAB deve essere lungo 5 caratteri');", True)
            Exit Sub
        End If


        If TabSoc.Conto = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare Codice Conto');", True)
            Exit Sub
        End If

        If TabSoc.Conto.Length <> 12 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Codice Conto deve essere lungo 12 caratteri');", True)
            Exit Sub
        End If

        Call CreaGriglia()

        Btn_Salva.Visible = True

    End Sub




    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function






    Sub CreaGriglia()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String = ""

        MeseContr = DD_Mese.SelectedValue

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("CognomeNome", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("ImportoTotale", GetType(String))
            Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
            Tabella.Columns.Add("Indirizzo", GetType(String))
            Tabella.Columns.Add("Cap", GetType(String))
            Tabella.Columns.Add("Comune", GetType(String))            
            Tabella.Columns.Add("Descrizione1", GetType(String))
            Tabella.Columns.Add("Descrizione2", GetType(String))
            Tabella.Columns.Add("Descrizione3", GetType(String))
            Tabella.Columns.Add("Descrizione4", GetType(String))
            Tabella.Columns.Add("Descrizione5", GetType(String))

        Catch ex As Exception

        End Try

        Dim Condizione As String = ""

        If DD_Struttura.SelectedValue <> "" Then
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            Else
                Dim Indice As Integer

                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " OR "
                        End If
                        Condizione = Condizione & " CentroServizio =  '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = " And (" & Condizione & ") "
            End If
        Else
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            End If
        End If


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "
        MySql = MySql & Condizione



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn

        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0
        Dim DataScadenza As Date = Txt_DataScadenza.Text

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue

        AppoggioNumero = 0
        AppoggioTotale = 0

        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)





            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""
            Dim ImporotTotale As Double = 0            
            Dim DESCRIZIONE1 As String = ""
            Dim DESCRIZIONE2 As String = ""
            Dim DESCRIZIONE3 As String = ""
            Dim DESCRIZIONE4 As String = ""
            Dim DESCRIZIONE5 As String = ""


            If Txt_Descrizione.Text.Trim = "" Then
                Dim cmdRdDescrizione As New OleDbCommand()
                cmdRdDescrizione.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And RigaDaCausale >= 3 order by RigaDaCausale "
                cmdRdDescrizione.Connection = cn
                cmdRdDescrizione.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim ReadDescrizione As OleDbDataReader = cmdRdDescrizione.ExecuteReader()
                Do While ReadDescrizione.Read
                    Dim pC As New Cls_Pianodeiconti

                    pC.Mastro = campodbN(ReadDescrizione.Item("MastroPartita"))
                    pC.Conto = campodbN(ReadDescrizione.Item("ContoPartita"))
                    pC.Sottoconto = campodbN(ReadDescrizione.Item("SottocontoPartita"))
                    pC.Decodfica(Session("DC_GENERALE"))

                    Dim Quantita As String = ""
                    If campodbN(ReadDescrizione.Item("RigaDaCausale")) = 3 Then
                        Quantita = " " & campodb(ReadDescrizione.Item("Quantita"))
                    End If

                    Select Case True
                        Case DESCRIZIONE1 = ""
                            DESCRIZIONE1 = pC.Descrizione & " " & campodb(ReadDescrizione.Item("Descrizione")) & Quantita & " " & Format(campodbN(ReadDescrizione.Item("Importo")), "#,##0.00")
                        Case DESCRIZIONE2 = ""
                            DESCRIZIONE2 = pC.Descrizione & " " & campodb(ReadDescrizione.Item("Descrizione")) & Quantita & " " & Format(campodbN(ReadDescrizione.Item("Importo")), "#,##0.00")
                        Case DESCRIZIONE3 = ""
                            DESCRIZIONE3 = pC.Descrizione & " " & campodb(ReadDescrizione.Item("Descrizione")) & Quantita & " " & Format(campodbN(ReadDescrizione.Item("Importo")), "#,##0.00")
                        Case DESCRIZIONE4 = ""
                            DESCRIZIONE4 = pC.Descrizione & " " & campodb(ReadDescrizione.Item("Descrizione")) & Quantita & " " & Format(campodbN(ReadDescrizione.Item("Importo")), "#,##0.00")
                        Case DESCRIZIONE5 = ""
                            DESCRIZIONE5 = pC.Descrizione & " " & campodb(ReadDescrizione.Item("Descrizione")) & Quantita & " " & Format(campodbN(ReadDescrizione.Item("Importo")), "#,##0.00")
                    End Select
                Loop
                ReadDescrizione.Close()
            Else
                DESCRIZIONE1 = Txt_Descrizione.Text
            End If

            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

                ImporotTotale = campodbN(MyReadSC.Item("Importo"))
            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_FIRST As String = ""
            Dim DO11_CODICEOSPITE As Integer = 0
            Dim DO11_CODICEPARENTE As Integer = 0
            Dim DO11_TIPOFLUSSO As String = ""



            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                DO11_CODICEOSPITE = CodiceOspite
                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti


                DO11_CODICEOSPITE = CodiceOspite
                DO11_CODICEPARENTE = CodiceParente

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If
            End If


            If DO11_CONDPAG_CODICE_SEN = "" Then
                DO11_CONDPAG_CODICE_SEN = "XX"
            End If
            Dim M As New Cls_TipoPagamento
            M.Descrizione = ""
            M.Codice = DO11_CONDPAG_CODICE_SEN
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If


            If DO11_TIPOFLUSSO = "M" Then

                Dim AppoggioData As Date

                AppoggioTotale = AppoggioTotale + ImporotTotale
                AppoggioNumero = AppoggioNumero + 1


                Try
                    AppoggioData = DO11_BANCA_DATAMANDATO
                Catch ex As Exception

                End Try

                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = DO11_RAGIONESOCIALE
                myriga(1) = DO11_CF
                myriga(2) = Format(ImporotTotale, "0.00")
                myriga(3) = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                myriga(4) = DO11_INDIRIZZO
                myriga(5) = DO11_CAP
                myriga(6) = DO11_CITTA_NOME & " " & DO11_PROVINCIA
                myriga(7) = DESCRIZIONE1
                myriga(8) = DESCRIZIONE2
                myriga(9) = DESCRIZIONE3
                myriga(10) = DESCRIZIONE4
                myriga(11) = DESCRIZIONE5


                Tabella.Rows.Add(myriga)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        GridView1.AutoGenerateColumns = False



        Session("TabellaRid") = Tabella
        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
        Lbl_Totali.Text = "Numero Mav estratti " & AppoggioNumero & " per un importo di " & Format(AppoggioTotale, "#,##0.00")
        Lbl_Totali.Visible = True
        Btn_Seleziona.Visible = True


    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Export.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
                CheckBox.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Salva_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Salva.Click
        CreaMAV()
    End Sub


    Sub CreaMAV()


        Tabella = Session("TabellaRid")


        Dim ErroriNonCongruita As String = ""
        Dim TestoMAV As String
        Dim TestoMAV1 As String
        Dim TestoMAV2 As String
        Dim TestoMAV3 As String
        Dim TestoMAV4 As String
        Dim TestoMAV5 As String
        Dim TestoMAV6 As String
        Dim TestoMAV7 As String
        Dim TestoMAV8 As String


        Dim MavFile As String = ""
        Dim NumeroMav As Double
        Dim IMPORTOTOT As Double


        TestoMAV = ""
        TestoMAV1 = ""
        TestoMAV2 = ""
        TestoMAV3 = ""
        TestoMAV4 = ""
        TestoMAV5 = ""
        TestoMAV6 = ""
        TestoMAV7 = ""
        TestoMAV8 = ""

        Dim CodiceMittente As String
        Dim CodiceRicevente As String
        Dim NomeSupporto As String
        Dim NumeroRecord As Integer
        Dim RecordTesta As String

        Dim TabSoc As New Cls_TabellaSocieta

        Dim AppoggioNumero As Integer = 0
        Dim AppoggioTotale As Double = 0

        TabSoc.Leggi(Session("DC_TABELLE"))


        CodiceMittente = TabSoc.CodiceMittente

        CodiceRicevente = TabSoc.CodiceRicevente

        NomeSupporto = "MAV" & Format(Now, "ddMMyyyy")



        NumeroRecord = 0


        RecordTesta = Space(1)
        RecordTesta = RecordTesta & "IM"


        RecordTesta = RecordTesta & CodiceMittente
        RecordTesta = RecordTesta & CodiceRicevente
        RecordTesta = RecordTesta & Format(Now, "ddMMyy") 'data creazione
        RecordTesta = RecordTesta & AdattaLunghezza(NomeSupporto, 20)
        RecordTesta = RecordTesta & Space(6)
        RecordTesta = RecordTesta & Space(59)
        RecordTesta = RecordTesta & Space(7)
        RecordTesta = RecordTesta & Space(2)
        RecordTesta = RecordTesta & "E"
        RecordTesta = RecordTesta & Space(1)
        RecordTesta = RecordTesta & Space(5)
        NumeroRecord = NumeroRecord + 1
        'Print #1, RecordTesta
        MavFile = MavFile & RecordTesta & vbNewLine


        Dim DtDataScadenza As Date = Txt_DataScadenza.Text
        Dim DataScadenza As String = Format(DtDataScadenza, "ddMMyy")


        Dim CODICEABI As String = TabSoc.CODICEABI

        Dim CODICECAB As String = TabSoc.CODICECAB

        Dim conto As String = TabSoc.Conto ' Space(12)

        Dim Progressivo As Integer

        Progressivo = 0
        For Riga = GridView1.Rows.Count - 1 To 0 Step -1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then

                ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & Tabella.Rows(Riga).Item("CognomeNome") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("CodiceFiscale") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("Indirizzo") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("Cap") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("Comune") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;"">" & Tabella.Rows(Riga).Item("ImportoTotale") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "</tr>"

                AppoggioNumero = AppoggioNumero + 1
                AppoggioTotale = AppoggioTotale + Tabella.Rows(Riga).Item("ImportoTotale")

                Dim Importo As Double = Tabella.Rows(Riga).Item("ImportoTotale") * 100
                Dim NumeroDisposzione As String = ""

                NumeroDisposzione = Tabella.Rows(Riga).Item("NumeroRegistrazione")

                Dim CODICECLIENTEDEBITORE As String = ""
                Dim Record14 As String
                Dim Record20 As String
                Dim Record30 As String


                CODICECLIENTEDEBITORE = AdattaLunghezza(NumeroDisposzione, 16)
                IMPORTOTOT = IMPORTOTOT + Importo
                NumeroMav = NumeroMav + 1
                Progressivo = Progressivo + 1
                Record14 = Space(1)
                Record14 = Record14 & "14"
                Record14 = Record14 & AdattaLunghezzaNumero(Progressivo, 7)
                Record14 = Record14 & Space(12)
                Record14 = Record14 & DataScadenza
                Record14 = Record14 & "07000" 'causale
                Record14 = Record14 & AdattaLunghezzaNumero(Importo, 13) 'impotyo
                Record14 = Record14 & "-" 'segno
                Record14 = Record14 & CODICEABI 'CHE CI VA?
                Record14 = Record14 & CODICECAB 'CHE CI VA?
                Record14 = Record14 & conto 'CHE CI VA?
                Record14 = Record14 & " " & Space(21)
                Record14 = Record14 & Space(5)
                Record14 = Record14 & "4" '4
                Record14 = Record14 & CODICECLIENTEDEBITORE 'CHE CI VA?
                Record14 = Record14 & Space(6)
                Record14 = Record14 & "E"
                NumeroRecord = NumeroRecord + 1

                MavFile = MavFile & Record14 & vbNewLine
                '  Print #1, Record14

                Record20 = Space(1)
                Record20 = Record20 & "20"
                Record20 = Record20 & AdattaLunghezzaNumero(Progressivo, 7)
                Record20 = Record20 & AdattaLunghezza(TabSoc.RagioneSociale, 23)
                Record20 = Record20 & Space(87)
                NumeroRecord = NumeroRecord + 1

                MavFile = MavFile & Record20 & vbNewLine
                'Print #1, Record20

                Record30 = Space(1)
                Record30 = Record30 & "30"
                Record30 = Record30 & AdattaLunghezzaNumero(Progressivo, 7)
                Dim Intestatario As String = Tabella.Rows(Riga).Item("CognomeNome")
                Record30 = Record30 & Levaaccentate(AdattaLunghezza(Intestatario, 23))
                Record30 = Record30 & Space(87)
                NumeroRecord = NumeroRecord + 1
                'Print #1, Record30
                MavFile = MavFile & Record30 & vbNewLine


                Dim Indirizzo As String = Tabella.Rows(Riga).Item("Indirizzo")
                Dim Cap As String = Tabella.Rows(Riga).Item("Cap")
                Dim Comune As String = Tabella.Rows(Riga).Item("Comune")

                Dim Record40 As String = ""

                Record40 = Space(1)
                Record40 = Record40 & "40"
                Record40 = Record40 & AdattaLunghezzaNumero(Progressivo, 7)
                Record40 = Record40 & Levaaccentate(AdattaLunghezza(Indirizzo, 30))
                Record40 = Record40 & AdattaLunghezza(Cap, 5)
                Record40 = Record40 & Levaaccentate(AdattaLunghezza(Comune, 25))
                If Len(Indirizzo) > 30 Then
                    Record40 = Record40 & Levaaccentate(AdattaLunghezza(Mid(Indirizzo, 31, Len(Indirizzo) - 30), 28))
                Else
                    Record40 = Record40 & Space(28)
                End If
                Record40 = Record40 & "  "
                Record40 = Record40 & Space(20)
                NumeroRecord = NumeroRecord + 1
                'Print #1, Record40
                MavFile = MavFile & Record40 & vbNewLine


                Dim Record51 As String
                Record51 = Space(1)
                Record51 = Record51 & "51"
                Record51 = Record51 & AdattaLunghezzaNumero(Progressivo, 7)
                Record51 = Record51 & AdattaLunghezzaNumero(Progressivo, 10) 'id
                Record51 = Record51 & Space(100) ' TESTO DA BASTARE
                NumeroRecord = NumeroRecord + 1
                'Print #1, Record51
                MavFile = MavFile & Record51 & vbNewLine

                Dim Record50 As String
                Record50 = Space(1)
                Record50 = Record50 & "59"
                Record50 = Record50 & AdattaLunghezzaNumero(Progressivo, 7)                
                Record50 = Record50 & AdattaLunghezza(Tabella.Rows(Riga).Item("Descrizione1"), 110) 'Space(110)
                NumeroRecord = NumeroRecord + 1

                MavFile = MavFile & Record50 & vbNewLine



                If campodb(Tabella.Rows(Riga).Item("Descrizione2")) <> "" Then
                    Dim Record59_2 As String
                    Record59_2 = Space(1)
                    Record59_2 = Record59_2 & "59"
                    Record59_2 = Record59_2 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59_2 = Record59_2 & AdattaLunghezza(Tabella.Rows(Riga).Item("Descrizione2"), 110) 'Space(110)
                    NumeroRecord = NumeroRecord + 1

                    MavFile = MavFile & Record59_2 & vbNewLine
                End If

                If campodb(Tabella.Rows(Riga).Item("Descrizione3")) <> "" Then
                    Dim Record59_3 As String
                    Record59_3 = Space(1)
                    Record59_3 = Record59_3 & "59"
                    Record59_3 = Record59_3 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59_3 = Record59_3 & AdattaLunghezza(Tabella.Rows(Riga).Item("Descrizione3"), 110) 'Space(110)
                    NumeroRecord = NumeroRecord + 1

                    MavFile = MavFile & Record59_3 & vbNewLine
                End If

                If campodb(Tabella.Rows(Riga).Item("Descrizione4")) <> "" Then
                    Dim Record59_4 As String
                    Record59_4 = Space(1)
                    Record59_4 = Record59_4 & "59"
                    Record59_4 = Record59_4 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59_4 = Record59_4 & AdattaLunghezza(Tabella.Rows(Riga).Item("Descrizione4"), 110) 'Space(110)
                    NumeroRecord = NumeroRecord + 1

                    MavFile = MavFile & Record59_4 & vbNewLine
                End If

                If campodb(Tabella.Rows(Riga).Item("Descrizione5")) <> "" Then
                    Dim Record59_5 As String
                    Record59_5 = Space(1)
                    Record59_5 = Record59_5 & "59"
                    Record59_5 = Record59_5 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59_5 = Record59_5 & AdattaLunghezza(Tabella.Rows(Riga).Item("Descrizione5"), 110) 'Space(110)
                    NumeroRecord = NumeroRecord + 1

                    MavFile = MavFile & Record59_5 & vbNewLine
                End If


                If TestoMAV <> "" Then
                    Dim Record59 As String
                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV, 110)
                    NumeroRecord = NumeroRecord + 1
                    'Print #1, Record59
                    MavFile = MavFile & Record59 & vbNewLine

                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV1, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59

                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV2, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59

                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV3, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59


                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV4, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59


                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV5, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59


                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV6, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59

                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV7, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59

                    Record59 = Space(1)
                    Record59 = Record59 & "59"
                    Record59 = Record59 & AdattaLunghezzaNumero(Progressivo, 7)
                    Record59 = Record59 & AdattaLunghezza(TestoMAV8, 110)
                    NumeroRecord = NumeroRecord + 1
                    MavFile = MavFile & Record59 & vbNewLine
                    'Print #1, Record59
                End If
                Dim Chiavicontrollo As String
                Dim Record70 As String

                Chiavicontrollo = Space(20)
                Record70 = Space(1)
                Record70 = Record70 & "70"
                Record70 = Record70 & AdattaLunghezzaNumero(Progressivo, 7)
                Record70 = Record70 & Space(83)
                Record70 = Record70 & " " 'blank
                Record70 = Record70 & " "
                Record70 = Record70 & Space(5)
                Record70 = Record70 & Chiavicontrollo '?
                NumeroRecord = NumeroRecord + 1
                MavFile = MavFile & Record70 & vbNewLine
                'Print #1, Record70

            End If
        Next

        Dim RecordEF As String

        RecordEF = Space(1)
        RecordEF = RecordEF & "EF"
        RecordEF = RecordEF & CodiceMittente
        RecordEF = RecordEF & CodiceRicevente
        RecordEF = RecordEF & Format(Now, "ddMMyy") 'data creazione
        RecordEF = RecordEF & AdattaLunghezza(NomeSupporto, 20)
        RecordEF = RecordEF & Space(6)
        RecordEF = RecordEF & AdattaLunghezzaNumero(NumeroMav, 7)
        RecordEF = RecordEF & AdattaLunghezzaNumero(IMPORTOTOT * 100, 15)
        RecordEF = RecordEF & "00000" & "00000" & "00000"
        RecordEF = RecordEF & AdattaLunghezzaNumero(NumeroRecord + 1, 7)

        RecordEF = RecordEF & Space(24)
        RecordEF = RecordEF & "E"
        RecordEF = RecordEF & Space(6)

        MavFile = MavFile & RecordEF & vbNewLine
        'Print #1, RecordEF





        ErroriNonCongruita = ErroriNonCongruita & "<tr><td><b>NUMERO MAV ESTRATTI</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>" & AppoggioNumero & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>PER UN TOTALE DI: </b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;""><b>" & Format(AppoggioTotale, "#,##0.00") & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "</tr>"

        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)

        tw.WriteLine(MavFile)

        tw.Close()

        Session("NOMEFILE") = NomeFile

        Session("ERRORIIMPORT") = ErroriNonCongruita

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() { __doPostBack('Btn_Dowload', '0');  DialogBoxx('ReportMavCreati.aspx');  });", True)


    End Sub

    Protected Sub GridView1_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GridView1.RowDeleted

    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting

    End Sub

    Private Function Levaaccentate(ByVal sauro As String) As String
        Levaaccentate = Replace(sauro, "à", "a")
        Levaaccentate = Replace(sauro, "è", "e")
        Levaaccentate = Replace(sauro, "é", "e")
        Levaaccentate = Replace(sauro, "ì", "i")
        Levaaccentate = Replace(sauro, "ò", "o")
        Levaaccentate = Replace(sauro, "u", "u")
    End Function


    Protected Sub Btn_Dowload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Dowload.Click
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "text/plain"
        Response.AppendHeader("content-disposition", "attachment;filename=Mav" & Format(Now, "ddMMyyyy") & ".txt")
        Response.WriteFile(Session("NOMEFILE"))
        Response.Flush()
        Response.End()
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub
End Class
