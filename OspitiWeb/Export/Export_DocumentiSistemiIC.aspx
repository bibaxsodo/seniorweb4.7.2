﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Export_Export_DocumentiSistemiIC" CodeFile="Export_DocumentiSistemiIC.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Export Sistemi</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="../ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="../js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="../js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="../dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../dialogbox/redips-dialog-min.js"></script>
    <script src="/js/listacomuni.js" type="text/javascript"></script>
    <script src="../js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="../js/JSErrore.js" type="text/javascript"></script>
    <script src="../js/NoEnter.js" type="text/javascript"></script>


    <link rel="stylesheet" href="../dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="../dialogbox/script.js"></script>

    <link rel="stylesheet" href="../js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="../js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="../js/chosen/chosen.css">

    <link rel="stylesheet" href="../jqueryui/jquery-ui.css" type="text/css">
    <script src="../jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="../jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="../js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="../js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export - Export Sistemi</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Export" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Export" Visible="false" />
                            <asp:ImageButton ID="Btn_Estrai" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Export Sistemi
         
         
                                </HeaderTemplate>


                                <ContentTemplate>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Dal Documento :</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="84px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Al Documento :</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="84px"></asp:TextBox>
                                    <br />
                                    <br />



                                    <label class="LabelCampo">
                                        <asp:Label ID="lbl_prova" runat="server" Text="Prova"></asp:Label></label>
                                    <asp:CheckBox ID="Chk_Prova" Text="" runat="server" Width="84px"></asp:CheckBox>
                                    <br />
                                    <br />

                                    <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona Tutto" />
                                    <br />
                                    <br />

                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />

                                        <Columns>



                                            <asp:TemplateField HeaderText="Importa">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NumeroRegistrazione" HeaderText="Numero Registrazione" />
                                            <asp:BoundField DataField="DataRegistrazione" HeaderText="Data Registrazione" />
                                            <asp:BoundField DataField="NumeroDocumento" HeaderText="Numero Documento" />
                                            <asp:BoundField DataField="Causale" HeaderText="Causale Contabile" />
                                            <asp:BoundField DataField="Intestatario" HeaderText="Causale Contabile" />
                                            <asp:BoundField DataField="Importo" HeaderText="Importo" />
                                        </Columns>

                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </sau:TabPanel>

                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tb_Errori">
                                <HeaderTemplate>
                                    Errori / Export
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Button ID="Btn_DownloadMOVIM" runat="server" Text="Download Zip" Visible="false" /><br />
                                    <br />

                                    <asp:GridView ID="GridView2" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
