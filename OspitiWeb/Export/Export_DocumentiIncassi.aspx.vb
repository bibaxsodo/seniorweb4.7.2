﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression

Partial Class OspitiWeb_Export_Export_DocumentiIncassi
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        Dim Condizione As String

        Condizione = ""
        If RB_Documenti.Checked = True Then
            Condizione = " And AnnoProtocollo > 0"
        End If
        If RB_Incassi.Checked = True Then
            Condizione = " And AnnoProtocollo = 0"
        End If

        If Param.CausaleRettifica = "" Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1 or RegistroIva is null) order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione " & Condizione & "  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CausaleContabile <> '" & Param.CausaleRettifica & "' And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1 or RegistroIva is null) " & Condizione & " order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        End If
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione.Replace(vbTab, "").Replace(vbNewLine, "")

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            Btn_Export.ImageUrl = "~/images/sendmail.png"
            Btn_Export.Visible = True
        Else
            Btn_Export.ImageUrl = "~/images/download.png"
            Btn_Export.Visible = True
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click
        Estrazione()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        Dim K1 As New Cls_SqlString


        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub


    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Dim MyRs As New ADODB.Recordset
        Dim RsIva As New ADODB.Recordset

        Dim RsSt As New ADODB.Recordset
        Dim UtilVb6 As New Cls_FunzioniVB6
        Dim Errori As String = ""
        Dim VerificaCodiceFiscale As New Cls_CodiceFiscale
        Dim IndiceRiga As Integer = 0

        Dim NomeFile As String
        Dim NomeFile2 As String
        Dim VettoreExport(1000) As String




        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then

            If Session("NomeEPersonam") = "SMARIAMONTE" Then
                If FtpFileExists("ftp://194.79.58.9/AdveniasFDG/malnate/fat114.txt", "AdveniasFtpUser", "R35upTf541?") = FtpResult.Exists Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione file esiste non posso procedere');", True)
                    Exit Function
                End If

            End If
            If Session("NomeEPersonam") = "GNOCCHI" Then
                If FtpFileExists("ftp://194.79.58.9/AdveniasFDG/salice/fat110.txt", "AdveniasFtpUser", "R35upTf541?") = FtpResult.Exists Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione file esiste non posso procedere');", True)
                    Exit Function
                End If
            End If
            If Session("NomeEPersonam") = "SantaMariaProvvidenza" Then
                If FtpFileExists("ftp://194.79.58.9/AdveniasFDG/roma/fat125.txt", "AdveniasFtpUser", "R35upTf541?") = FtpResult.Exists Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione file esiste non posso procedere');", True)
                    Exit Function
                End If
            End If
            If Session("NomeEPersonam") = "PoloSpRiabilitativo" Then
                If FtpFileExists("ftp://194.79.58.9/AdveniasFDG/tricarico/fat128.txt", "AdveniasFtpUser", "R35upTf541?") = FtpResult.Exists Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione file esiste non posso procedere');", True)
                    Exit Function
                End If
            End If

            If Session("NomeEPersonam") = "RonzoniVilla" Then
                If FtpFileExists("ftp://194.79.58.9/AdveniasFDG/seregno/fat130.txt", "AdveniasFtpUser", "R35upTf541?") = FtpResult.Exists Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione file esiste non posso procedere');", True)
                    Exit Function
                End If
            End If
        End If

        Session("MYDATE") = Now

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"
        NomeFile2 = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".csv"

        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()


        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True


        IndiceRiga = 0
        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Riga As New Cls_CampiExport
                Dim Registrazione As New Cls_MovimentoContabile
                Dim RegistrazioneIncasso As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Dim CausaleContabileDocumentoCollegato As New Cls_CausaleContabile




                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)



                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If


                Riga.Tipodoc = 0
                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Then
                    Riga.Tipodoc = 0
                End If
                If CausaleContabile.TipoDocumento = "NC" Then
                    Riga.Tipodoc = 2
                End If
                If CausaleContabile.Tipo = "P" Then
                    Riga.Tipodoc = 3

                    RegistrazioneIncasso.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                    RegistrazioneIncasso.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                    If RegistrazioneIncasso.NumeroRegistrazione = 246 Then
                        RegistrazioneIncasso.NumeroRegistrazione = 246
                    End If

                    Dim lk As New Cls_Legami


                    lk.Leggi(Session("DC_GENERALE"), 0, Registrazione.NumeroRegistrazione)

                    Registrazione.NumeroRegistrazione = 0
                    Dim IndDocu As Integer
                    For IndDocu = 0 To 100
                        If lk.NumeroDocumento(IndDocu) > 0 Then
                            Registrazione.Leggi(Session("DC_GENERALE"), lk.NumeroDocumento(IndDocu))

                            CausaleContabileDocumentoCollegato.Codice = Registrazione.CausaleContabile
                            CausaleContabileDocumentoCollegato.Leggi(Session("DC_TABELLE"), CausaleContabileDocumentoCollegato.Codice)

                            Exit For
                        End If
                    Next

                    If Registrazione.NumeroRegistrazione = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Incasso non collegato a documento, non posso procedere registrazione : " & RegistrazioneIncasso.NumeroRegistrazione & "');", True)
                        Exit Function
                    End If
                End If




                Riga.Numdoc = Registrazione.NumeroDocumento
                Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni

                'GESTIRE LA TABELLA DI TRASCODIFICA TIPO REGISTRO
                TabTrascodifica.TIPOTAB = "RI"
                TabTrascodifica.SENIOR = Registrazione.RegistroIVA
                TabTrascodifica.EXPORT = 0
                TabTrascodifica.Leggi(Session("DC_TABELLE"))
                Riga.seziva = TabTrascodifica.EXPORT

                Riga.datadoc = Format(Registrazione.DataRegistrazione, "ddMMyyyy")
                If CausaleContabile.Tipo = "P" Then
                    Riga.datadoc = Format(RegistrazioneIncasso.DataRegistrazione, "ddMMyyyy")
                End If
                Dim TotaleDocumento As Double

                TotaleDocumento = Math.Abs(Registrazione.ImportoDocumento(Session("DC_TABELLE")))

                Riga.totDoc = Format(TotaleDocumento, "0.00")
                If CausaleContabile.Tipo = "P" Then
                    Riga.totDoc = Format(Math.Abs(RegistrazioneIncasso.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))), "0.00")
                End If

                Riga.imponib2 = "0,00"
                Riga.iva2 = "0,00"
                Riga.imponib3 = "0,00"
                Riga.iva3 = "0,00"
                Riga.imponib4 = "0,00"
                Riga.iva4 = "0,00"
                Riga.imponib5 = "0,00"
                Riga.iva5 = "0,00"
                Riga.imponib6 = "0,00"
                Riga.iva6 = "0,00"
                Riga.imponib7 = "0,00"
                Riga.iva7 = "0,00"
                Riga.imponib8 = "0,00"
                Riga.iva8 = "0,00"
                Riga.imponib9 = "0,00"
                Riga.iva9 = "0,00"
                Riga.imponib10 = "0,00"
                Riga.iva10 = "0,00"


                REM ****************** LETTURA RIGHE IVA
                Dim Indice As Integer = 0
                Dim IndiceUso As Integer = 0
                Dim DareAvereDefaultIVA As String = ""
                Dim TotaleImponibile As Double = 0
                Dim TotaleIVA As Double = 0

                If CausaleContabile.TipoDocumento = "NC" Then
                    DareAvereDefaultIVA = "D"
                Else
                    DareAvereDefaultIVA = "A"
                End If
                If CausaleContabile.Tipo = "P" Then
                    If CausaleContabileDocumentoCollegato.TipoDocumento = "NC" Then
                        DareAvereDefaultIVA = "D"
                    Else
                        DareAvereDefaultIVA = "A"
                    End If
                End If


                For Indice = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(Indice)) Then
                        If Registrazione.Righe(Indice).Tipo = "IV" And (Registrazione.Righe(Indice).Imponibile > 0 Or Registrazione.Righe(Indice).Importo > 0) Then
                            IndiceUso = IndiceUso + 1
                            If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                TotaleImponibile = TotaleImponibile + Registrazione.Righe(Indice).Imponibile
                                TotaleIVA = TotaleIVA + Registrazione.Righe(Indice).Importo
                            Else
                                TotaleImponibile = TotaleImponibile - Registrazione.Righe(Indice).Imponibile
                                TotaleIVA = TotaleIVA - Registrazione.Righe(Indice).Importo
                            End If
                            If IndiceUso = 1 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva1 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib1 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva1 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib1 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva1 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 2 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva2 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib2 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva2 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib2 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva2 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 3 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva3 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib3 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva3 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib3 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva3 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 4 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva4 = TabTrascodifica.EXPORT


                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib4 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva4 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib4 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva4 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 5 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva5 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib5 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva5 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib5 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva5 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 6 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva6 = TabTrascodifica.EXPORT


                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib6 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva6 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib6 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva6 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 7 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva7 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib7 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva7 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib7 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva7 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                            If IndiceUso = 8 Then
                                TabTrascodifica.TIPOTAB = "IV"
                                TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                                Riga.codiva8 = TabTrascodifica.EXPORT

                                If DareAvereDefaultIVA = Registrazione.Righe(Indice).DareAvere Then
                                    Riga.imponib8 = Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva8 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                                Else
                                    Riga.imponib8 = "-" & Format(Registrazione.Righe(Indice).Imponibile, "0.00")
                                    Riga.iva8 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                                End If
                            End If
                        End If
                    End If
                Next

                REM ****************** LETTURA RIGHE RICAVO

                Riga.ricavo1 = "0,00"
                Riga.ricavo2 = "0,00"
                Riga.ricavo3 = "0,00"
                Riga.ricavo4 = "0,00"
                Riga.ricavo5 = "0,00"
                Riga.ricavo6 = "0,00"
                Riga.ricavo7 = "0,00"
                Riga.ricavo8 = "0,00"
                Riga.ricavo9 = "0,00"
                Riga.ricavo10 = "0,00"


                Dim DareAvereDefaultRicavo As String = ""
                Dim TotaleRicavi As Double = 0

                If CausaleContabile.TipoDocumento = "NC" Then
                    DareAvereDefaultRicavo = "D"
                Else
                    DareAvereDefaultRicavo = "A"
                End If
                If CausaleContabile.Tipo = "P" Then
                    If CausaleContabileDocumentoCollegato.TipoDocumento = "NC" Then
                        DareAvereDefaultRicavo = "D"
                    Else
                        DareAvereDefaultRicavo = "A"
                    End If
                End If

                IndiceUso = 0

                Dim cmdRicavi As New OleDbCommand()

                'cmdRicavi.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,DareAvere,sum(importo) As ImpTotale From MovimentiContabiliRiga where Numero = ? And (Tipo = '' Or Tipo Is Null) Group by MastroPartita,ContoPartita,SottocontoPartita,DareAvere "

                If CausaleContabile.TipoDocumento = "NC" Then
                    cmdRicavi.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,sum(CASE WHEN DareAvere = 'D' THEN Importo ELSE Importo * -1 END) As ImpTotale From MovimentiContabiliRiga where Numero = ? And (Tipo = '' Or Tipo Is Null) Group by MastroPartita,ContoPartita,SottocontoPartita "
                Else
                    cmdRicavi.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,sum(CASE WHEN DareAvere = 'A' THEN Importo ELSE Importo * -1 END) As ImpTotale From MovimentiContabiliRiga where Numero = ? And (Tipo = '' Or Tipo Is Null) Group by MastroPartita,ContoPartita,SottocontoPartita"
                End If
                cmdRicavi.Parameters.AddWithValue("@NumeroRegistrazione", Registrazione.NumeroRegistrazione)
                cmdRicavi.Connection = cn
                Dim RicaviDB As OleDbDataReader = cmdRicavi.ExecuteReader()
                Do While RicaviDB.Read

                    IndiceUso = IndiceUso + 1
                    'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                    TotaleRicavi = TotaleRicavi + campodbN(RicaviDB.Item("ImpTotale"))

                    'Else
                    '    TotaleRicavi = TotaleRicavi - campodbN(RicaviDB.Item("ImpTotale"))
                    'End If

                    If IndiceUso = 1 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric1 = TabTrascodifica.EXPORT


                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto1 = TabTrascodifica.EXPORT

                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto1 = TabTrascodifica.EXPORT
                        End If

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo1 = Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'Else
                        '    Riga.ricavo1 = "-" & Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'End If
                    End If
                    If IndiceUso = 2 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric2 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto2 = TabTrascodifica.EXPORT

                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto2 = TabTrascodifica.EXPORT
                        End If
                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo2 = Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'Else
                        '    Riga.ricavo2 = "-" & Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'End If
                    End If
                    If IndiceUso = 3 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric3 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto3 = TabTrascodifica.EXPORT


                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto3 = TabTrascodifica.EXPORT
                        End If

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo3 = Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'Else
                        '    Riga.ricavo3 = "-" & Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'End If
                    End If
                    If IndiceUso = 4 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric4 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto4 = TabTrascodifica.EXPORT

                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto4 = TabTrascodifica.EXPORT
                        End If


                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo4 = Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'Else
                        '    Riga.ricavo4 = "-" & Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'End If
                    End If
                    If IndiceUso = 5 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric5 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto5 = TabTrascodifica.EXPORT


                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto5 = TabTrascodifica.EXPORT
                        End If

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo5 = Format(campodbN(RicaviDB.Item("ImpTotale")), "0.00")
                        'Else
                        '    Riga.ricavo5 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'End If
                    End If
                    If IndiceUso = 6 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric6 = TabTrascodifica.EXPORT


                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto6 = TabTrascodifica.EXPORT


                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto6 = TabTrascodifica.EXPORT
                        End If

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo6 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'Else
                        '    Riga.ricavo6 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'End If
                    End If
                    If IndiceUso = 7 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric7 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto7 = TabTrascodifica.EXPORT


                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto7 = TabTrascodifica.EXPORT
                        End If

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo7 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'Else
                        '    Riga.ricavo7 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'End If
                    End If
                    If IndiceUso = 8 Then
                        'GESTIRE LA TABELLA DI TRASCODIFICA CONTO RICAVO
                        TabTrascodifica.TIPOTAB = "CC"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.contoric8 = TabTrascodifica.EXPORT

                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = campodbN(RicaviDB.Item("MastroPartita")) & "." & campodbN(RicaviDB.Item("ContoPartita")) & "." & campodbN(RicaviDB.Item("SottocontoPartita"))
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))

                        Riga.TipoConto8 = TabTrascodifica.EXPORT

                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.TIPOTAB = "CT"
                        TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                        TabTrascodifica.EXPORT = ""
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        If TabTrascodifica.EXPORT <> "" Then
                            Riga.TipoConto8 = TabTrascodifica.EXPORT
                        End If


                        Riga.ricavo8 = Format(Registrazione.Righe(Indice).Importo, "0.00")

                        'If DareAvereDefaultRicavo = campodb(RicaviDB.Item("DareAvere")) Then
                        Riga.ricavo8 = Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'Else
                        '    Riga.ricavo8 = "-" & Format(Registrazione.Righe(Indice).Importo, "0.00")
                        'End If
                    End If
                Loop
                RicaviDB.Close()


                'GESTIRE LA TABELLA DI TRASCODIFICA MODALITA PAGAMENTO
                TabTrascodifica.TIPOTAB = "MP"
                TabTrascodifica.SENIOR = Registrazione.CodicePagamento
                TabTrascodifica.EXPORT = 0
                TabTrascodifica.Leggi(Session("DC_TABELLE"))
                Riga.Modpag = TabTrascodifica.EXPORT


                TabTrascodifica.TIPOTAB = "SC"
                TabTrascodifica.SENIOR = Registrazione.CodicePagamento
                TabTrascodifica.EXPORT = 0
                TabTrascodifica.Leggi(Session("DC_TABELLE"))


                Dim Scadenziario As New Cls_Scadenziario

                Scadenziario.NumeroRegistrazioneContabile = Registrazione.NumeroRegistrazione
                Scadenziario.Numero = 0
                Scadenziario.DataScadenza = Nothing
                Scadenziario.Leggi(Session("DC_GENERALE"))


                If Scadenziario.DataScadenza = Nothing Then

                    Riga.Scadpag = TabTrascodifica.EXPORT
                    Riga.datascad1 = Format(Registrazione.DataRegistrazione, "ddMMyyyy")
                    Riga.datascad2 = "00000000"
                    Riga.datascad3 = "00000000"
                    Riga.datascad4 = "00000000"
                Else
                    Riga.Scadpag = TabTrascodifica.EXPORT
                    Riga.datascad1 = Format(Scadenziario.DataScadenza, "ddMMyyyy")
                    Riga.datascad2 = "00000000"
                    Riga.datascad3 = "00000000"
                    Riga.datascad4 = "00000000"
                End If
                Riga.codcliente = 0

                Dim Cliente As String = ""
                Dim CodiceOspite As Integer
                Dim CodiceParente As Integer = 0
                Dim Tipo As String
                Dim CodiceProvincia As String = ""
                Dim CodiceComune As String = ""
                Dim CodiceRegione As String = ""

                Tipo = Registrazione.TipoDocumento

                Dim cmdRd As New OleDbCommand()
                cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
                cmdRd.Connection = cn
                cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
                If MyReadSC.Read Then
                    Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                    If Mid(Tipo & Space(10), 1, 1) = " " Then
                        CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                        CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                        If CodiceParente = 0 And CodiceOspite > 0 Then
                            Tipo = "O" & Space(10)
                        End If
                        If CodiceParente > 0 And CodiceOspite > 0 Then
                            Tipo = "P" & Space(10)
                        End If
                    End If
                    If Mid(Tipo & Space(10), 1, 1) = "C" Then
                        CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                        CodiceComune = Mid(Tipo & Space(10), 5, 3)
                    End If
                    If Mid(Tipo & Space(10), 1, 1) = "J" Then
                        CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                        CodiceComune = Mid(Tipo & Space(10), 5, 3)
                    End If
                    If Mid(Tipo & Space(10), 1, 1) = "R" Then
                        CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                    End If

                End If
                MyReadSC.Close()

                Dim DO11_RAGIONESOCIALE As String = ""
                Dim DO11_PIVA As String = ""
                Dim DO11_CF As String = ""
                Dim DO11_INDIRIZZO As String = ""
                Dim DO11_CAP As String = ""
                Dim DO11_CITTA_NOME As String = ""
                Dim DO11_CITTA_ISTAT As String = ""
                Dim DO11_PROVINCIA As String = ""
                Dim DO11_CONDPAG_CODICE_SEN As String = ""
                Dim DO11_CONDPAG_NOME_SEN As String = ""
                Dim DO11_BANCA_CIN As String = ""
                Dim DO11_BANCA_ABI As String = ""
                Dim DO11_BANCA_CAB As String = ""
                Dim DO11_BANCA_CONTO As String = ""
                Dim DO11_BANCA_IBAN As String = ""
                Dim DO11_BANCA_IDMANDATO As String = ""
                Dim DO11_BANCA_DATAMANDATO As String = ""
                Dim AddDescrizione As String

                AddDescrizione = ""
                If Mid(Tipo & Space(10), 1, 1) = "O" Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = CodiceOspite
                    Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                    DO11_RAGIONESOCIALE = Ospite.CognomeOspite & ", " & Ospite.NomeOspite
                    DO11_PIVA = ""
                    DO11_CF = Ospite.CODICEFISCALE
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia


                    Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                    Kl.CodiceOspite = CodiceOspite
                    Kl.CodiceParente = 0
                    Kl.CentroServizio = Registrazione.CentroServizio
                    Kl.Leggi(Session("DC_OSPITE"))
                    If Kl.ModalitaPagamento <> "" Then
                        Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                    End If


                    DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.MODALITAPAGAMENTO
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                    Dim ModPag As New Cls_DatiPagamento

                    ModPag.CodiceOspite = Ospite.CodiceOspite
                    ModPag.CodiceParente = 0
                    ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                    DO11_BANCA_CIN = ModPag.Cin
                    DO11_BANCA_ABI = ModPag.Abi
                    DO11_BANCA_CAB = ModPag.Cab
                    DO11_BANCA_CONTO = ModPag.CCBancario
                    DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                    DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                    DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                End If

                If Mid(Tipo & Space(10), 1, 1) = "P" Then
                    Dim Ospite As New Cls_Parenti

                    Ospite.CodiceOspite = CodiceOspite
                    Ospite.CodiceParente = CodiceParente
                    Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                    DO11_RAGIONESOCIALE = Ospite.CognomeParente & ", " & Ospite.NomeParente
                    DO11_PIVA = ""
                    DO11_CF = Ospite.CODICEFISCALE
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia
                    DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                    Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                    Kl.CodiceOspite = CodiceOspite
                    Kl.CodiceParente = CodiceParente
                    Kl.CentroServizio = Registrazione.CentroServizio
                    Kl.Leggi(Session("DC_OSPITE"))
                    If Kl.ModalitaPagamento <> "" Then
                        Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                    End If

                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.MODALITAPAGAMENTO
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                    Dim ModPag As New Cls_DatiPagamento

                    ModPag.CodiceOspite = Ospite.CodiceOspite
                    ModPag.CodiceParente = Ospite.CodiceParente
                    ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                    DO11_BANCA_CIN = ModPag.Cin
                    DO11_BANCA_ABI = ModPag.Abi
                    DO11_BANCA_CAB = ModPag.Cab
                    DO11_BANCA_CONTO = ModPag.CCBancario
                    DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                    DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                    DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    Dim Ospite As New ClsComune

                    Ospite.Provincia = CodiceProvincia
                    Ospite.Comune = CodiceComune
                    Ospite.Leggi(Session("DC_OSPITE"))
                    DO11_RAGIONESOCIALE = Ospite.Descrizione
                    DO11_PIVA = Ospite.PartitaIVA
                    DO11_CF = Ospite.CodiceFiscale
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia
                    DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.ModalitaPagamento
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                    Dim cnOspiti As OleDbConnection

                    cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                    cnOspiti.Open()

                    Dim cmdC As New OleDbCommand()
                    cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                    cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                    cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                    cmdC.Connection = cnOspiti
                    Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                    If RdCom.Read Then
                        AddDescrizione = campodb(RdCom.Item("NoteUp"))
                    End If
                    RdCom.Close()
                    cnOspiti.Close()
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    Dim Ospite As New ClsComune

                    Ospite.Provincia = CodiceProvincia
                    Ospite.Comune = CodiceComune
                    Ospite.Leggi(Session("DC_OSPITE"))
                    DO11_RAGIONESOCIALE = Ospite.Descrizione
                    DO11_PIVA = Ospite.PartitaIVA
                    DO11_CF = Ospite.CodiceFiscale
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia
                    DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.ModalitaPagamento
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                    Dim cnOspiti As OleDbConnection

                    cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                    cnOspiti.Open()

                    Dim cmdC As New OleDbCommand()
                    cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                    cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                    cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                    cmdC.Connection = cnOspiti
                    Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                    If RdCom.Read Then
                        AddDescrizione = campodb(RdCom.Item("NoteUp"))
                    End If
                    RdCom.Close()
                    cnOspiti.Close()
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    Dim Ospite As New ClsUSL

                    Ospite.CodiceRegione = CodiceRegione
                    Ospite.Leggi(Session("DC_OSPITE"))
                    DO11_RAGIONESOCIALE = Ospite.Nome
                    DO11_PIVA = Ospite.PARTITAIVA
                    DO11_CF = Ospite.CodiceFiscale
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia
                    DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.ModalitaPagamento
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
                End If


                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per ospite "
                    MyTable.Rows.Add(myrigaerrore)
                    Export_DocumentiIncassi = False
                End If


                Riga.denomin1 = DO11_RAGIONESOCIALE.ToUpper
                Riga.denomin2 = ""
                Riga.vian = DO11_INDIRIZZO.ToUpper
                Riga.cap = DO11_CAP

                If IsNothing(DO11_CITTA_NOME) Then
                    DO11_CITTA_NOME = ""
                End If
                If IsNothing(DO11_PROVINCIA) Then
                    DO11_PROVINCIA = ""
                End If

                If IsNothing(DO11_CF) Then
                    DO11_CF = ""
                End If
                Riga.citta = DO11_CITTA_NOME.ToUpper
                Riga.prov = DO11_PROVINCIA.ToUpper
                Riga.piva = DO11_PIVA
                Riga.codfisc = DO11_CF.ToUpper
                Riga.abi = ""
                Riga.cab = ""



                If Riga.TipoConto1 = "" Then
                    If Riga.contoric1 <> "" Then Riga.TipoConto1 = "IS" Else Riga.TipoConto1 = "00"
                End If
                If Riga.TipoConto2 = "" Then
                    If Riga.contoric2 <> "" Then Riga.TipoConto2 = "IS" Else Riga.TipoConto2 = "00"
                End If
                If Riga.TipoConto3 = "" Then
                    If Riga.contoric3 <> "" Then Riga.TipoConto3 = "IS" Else Riga.TipoConto3 = "00"
                End If
                If Riga.TipoConto4 = "" Then
                    If Riga.contoric4 <> "" Then Riga.TipoConto4 = "IS" Else Riga.TipoConto4 = "00"
                End If
                If Riga.TipoConto5 = "" Then
                    If Riga.contoric5 <> "" Then Riga.TipoConto5 = "IS" Else Riga.TipoConto5 = "00"
                End If
                If Riga.TipoConto6 = "" Then
                    If Riga.contoric6 <> "" Then Riga.TipoConto6 = "IS" Else Riga.TipoConto6 = "00"
                End If
                If Riga.TipoConto7 = "" Then
                    If Riga.contoric7 <> "" Then Riga.TipoConto7 = "IS" Else Riga.TipoConto7 = "00"
                End If
                If Riga.TipoConto8 = "" Then
                    If Riga.contoric8 <> "" Then Riga.TipoConto8 = "IS" Else Riga.TipoConto8 = "00"
                End If

                If Riga.contoric9 <> "" Then Riga.TipoConto9 = "IS" Else Riga.TipoConto9 = "00"
                If Riga.contoric10 <> "" Then Riga.TipoConto10 = "IS" Else Riga.TipoConto10 = "00"

                If Riga.ricavo2 = 0 Then
                    Riga.TipoConto2 = "00"
                End If
                If Riga.ricavo3 = 0 Then
                    Riga.TipoConto3 = "00"
                End If
                If Riga.ricavo4 = 0 Then
                    Riga.TipoConto4 = "00"
                End If
                If Riga.ricavo5 = 0 Then
                    Riga.TipoConto5 = "00"
                End If
                If Riga.ricavo6 = 0 Then
                    Riga.TipoConto6 = "00"
                End If
                If Riga.ricavo7 = 0 Then
                    Riga.TipoConto7 = "00"
                End If
                If Riga.ricavo8 = 0 Then
                    Riga.TipoConto8 = "00"
                End If
                If Riga.ricavo9 = 0 Then
                    Riga.TipoConto9 = "00"
                End If
                If Riga.ricavo10 = 0 Then
                    Riga.TipoConto10 = "00"
                End If


                Riga.ContoCredito = 14051001


                If Riga.Tipodoc = 3 Then
                    TabTrascodifica.TIPOTAB = "CC"
                    TabTrascodifica.SENIOR = RegistrazioneIncasso.Righe(1).MastroPartita & "." & RegistrazioneIncasso.Righe(1).ContoPartita & "." & RegistrazioneIncasso.Righe(1).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    Riga.CassaBanca = TabTrascodifica.EXPORT
                Else
                    Riga.CassaBanca = 0
                End If
                Riga.Datanascita = Space(8)

                Riga.Capnascita = Nothing
                Riga.Protocolloiva = ""
                If RegistrazioneIncasso.NumeroRegistrazione > 0 Then
                    TabTrascodifica.TIPOTAB = "CI"
                    TabTrascodifica.SENIOR = RegistrazioneIncasso.Righe(1).MastroPartita & "." & RegistrazioneIncasso.Righe(1).ContoPartita & "." & RegistrazioneIncasso.Righe(1).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))


                    Riga.Causale = TabTrascodifica.EXPORT 'tabella trascodifica CI 
                    Riga.Dataincasso = Format(RegistrazioneIncasso.DataRegistrazione, "ddMMyyyy")
                    Riga.Descr1 = RegistrazioneIncasso.Descrizione.Replace(vbTab, "").Replace(vbNewLine, "")
                    Riga.Descr2 = ""
                Else
                    Riga.Causale = "" 'Che inserisco nella causale incasso
                    Riga.Dataincasso = Space(8) ' non capisco a che serve a incasso
                    Riga.Descr1 = ""
                    Riga.Descr2 = ""
                End If

                If CausaleContabile.TipoDocumento = "NC" Then
                    Riga.Causaledoc = "NA"
                Else
                    Riga.Causaledoc = "FC"
                End If
                If CausaleContabile.Tipo = "P" Then
                    If CausaleContabileDocumentoCollegato.TipoDocumento = "NC" Then
                        Riga.Causaledoc = "NA"
                    Else
                        Riga.Causaledoc = "FC"
                    End If
                End If


                If RegistrazioneIncasso.NumeroRegistrazione > 0 Then
                    Riga.Dataregistr = Format(RegistrazioneIncasso.DataRegistrazione, "ddMMyyyy")
                Else
                    Riga.Dataregistr = Format(Registrazione.DataRegistrazione, "ddMMyyyy")
                End If
                Riga.NumDocpartic = ""
                Riga.DataCompetIniziale = "00000000"
                Riga.DataCompetFinale = "00000000"
                Riga.Contoanalit1 = ""
                Riga.Contoanalit2 = ""
                Riga.Contoanalit3 = ""
                Riga.Contoanalit4 = ""
                Riga.Contoanalit5 = ""
                Riga.Contoanalit6 = ""
                Riga.Contoanalit7 = ""
                Riga.Contoanalit8 = ""
                Riga.Contoanalit9 = ""
                Riga.Contoanalit10 = ""

                Riga.Centrocosto1 = ""
                Riga.Centrocosto2 = ""
                Riga.Centrocosto3 = ""
                Riga.Centrocosto4 = ""
                Riga.Centrocosto5 = ""
                Riga.Centrocosto6 = ""
                Riga.Centrocosto7 = ""
                Riga.Centrocosto8 = ""
                Riga.Centrocosto9 = ""
                Riga.Centrocosto10 = ""

                Riga.ImpAnalit1 = "0,00"
                Riga.ImpAnalit2 = "0,00"
                Riga.ImpAnalit3 = "0,00"
                Riga.ImpAnalit4 = "0,00"
                Riga.ImpAnalit5 = "0,00"
                Riga.ImpAnalit6 = "0,00"
                Riga.ImpAnalit7 = "0,00"
                Riga.ImpAnalit8 = "0,00"
                Riga.ImpAnalit9 = "0,00"
                Riga.ImpAnalit10 = "0,00"

                Riga.FlagOpposiz = ""

                If Riga.Tipodoc = 0 Or Riga.Tipodoc = 2 Then
                    Riga.FlagOpposiz = "N"
                    Riga.AnnoIncasso = 0
                    Riga.IDIncasso = 0
                Else
                    Riga.AnnoIncasso = Year(RegistrazioneIncasso.DataRegistrazione)


                    TabTrascodifica.TIPOTAB = "PR"
                    TabTrascodifica.SENIOR = "1"
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))




                    Riga.IDIncasso = TabTrascodifica.EXPORT & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000")
                End If

                Dim CentroServizio As New Cls_CentroServizio

                CentroServizio.CENTROSERVIZIO = Registrazione.CentroServizio
                CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)




                Riga.Codregione = ""
                Riga.CodASL = ""
                Riga.CodStruttura = ""

                If Riga.Tipodoc = 0 Or Riga.Tipodoc = 2 Then

                    Riga.Codregione = CentroServizio.CODREGIONE
                    Riga.CodASL = CentroServizio.CODASL
                    Riga.CodStruttura = CentroServizio.CODSSA

                    TabTrascodifica.TIPOTAB = "CS"
                    TabTrascodifica.SENIOR = Registrazione.CausaleContabile
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    Riga.TipoSpesa = TabTrascodifica.EXPORT
                Else
                    Riga.Codregione = ""
                    Riga.CodASL = ""
                    Riga.Codregione = ""
                    Riga.TipoSpesa = ""
                End If

                If Registrazione.RifNumero = "" Then
                    Riga.RifNumero = ""
                Else
                    TabTrascodifica.TIPOTAB = "RI"
                    TabTrascodifica.SENIOR = Registrazione.RegistroIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    Riga.RifNumero = TabTrascodifica.EXPORT & "/" & Registrazione.RifNumero

                    'If Session("DC_OSPITE").ToString.ToUpper.IndexOf("MARIA") >= 0 Then
                    '    Riga.RifNumero = "17/" & Registrazione.RifNumero
                    'Else
                    '    Riga.RifNumero = "88/" & Registrazione.RifNumero
                    'End If
                End If

                If Not IsDate(Registrazione.RifData) Then
                    Riga.RifData = "00000000"
                Else
                    If Year(Registrazione.RifData) > 1900 Then
                        Dim AppoggioDate As Date = Registrazione.RifData

                        Riga.RifData = Format(AppoggioDate, "ddMMyyyy")
                    Else
                        Riga.RifData = "00000000"
                    End If
                End If

                If Riga.Tipodoc = 0 Or Riga.Tipodoc = 1 Or Riga.Tipodoc = 2 Then
                    If Math.Round(TotaleImponibile, 2) <> Math.Round(TotaleRicavi, 2) Then
                        Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                        myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                        myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                        myrigaerrore.Item("Segnalazione") = " Registrazione non quadra"
                        MyTable.Rows.Add(myrigaerrore)
                        Export_DocumentiIncassi = False
                    End If
                End If

                ' - NS. FATTURA” O “NS. NOTA DI CREDITO”+ il nome del cliente + il tipo spesa 730  
                If CausaleContabile.TipoDocumento = "NC" Then
                    Riga.Descrizionedoc = "NS. NOTA DI CREDITO " & Riga.denomin1.Replace(vbTab, "").Replace(vbNewLine, "") & " " & Riga.TipoSpesa.Replace(vbTab, "").Replace(vbNewLine, "")
                Else
                    Riga.Descrizionedoc = "NS. FATTURA " & Riga.denomin1.Replace(vbTab, "").Replace(vbNewLine, "") & " " & Riga.TipoSpesa.Replace(vbTab, "").Replace(vbNewLine, "")
                End If
                If CausaleContabile.Tipo = "P" Then
                    If CausaleContabileDocumentoCollegato.TipoDocumento = "NC" Then
                        Riga.Descrizionedoc = "NS. NOTA DI CREDITO " & Riga.denomin1.Replace(vbTab, "").Replace(vbNewLine, "") & " " & Riga.TipoSpesa.Replace(vbTab, "").Replace(vbNewLine, "")
                    Else
                        Riga.Descrizionedoc = "NS. FATTURA " & Riga.denomin1.Replace(vbTab, "").Replace(vbNewLine, "") & " " & Riga.TipoSpesa.Replace(vbTab, "").Replace(vbNewLine, "")
                    End If
                End If

                Dim Stringa As String = Riga.ExportStringa()

                VettoreExport(IndiceRiga) = Stringa
                IndiceRiga = IndiceRiga + 1
            End If
        Next

        cn.Close()

        For i = 0 To IndiceRiga - 1
            For x = i + 1 To IndiceRiga
                Dim NumeroX As String = ""
                Dim NumeroI As String = ""

                NumeroX = Mid(VettoreExport(x), 3, 20) & VettoreExport(x)
                NumeroI = Mid(VettoreExport(i), 3, 20) & VettoreExport(i)

                If NumeroI > NumeroX Then
                    Dim MySwap As String
                    MySwap = VettoreExport(i)
                    VettoreExport(i) = VettoreExport(x)
                    VettoreExport(x) = MySwap
                End If
            Next
        Next

        REM Array.Sort(VettoreExport, 0, IndiceRiga, revComparer)

        Dim tw2 As System.IO.TextWriter = System.IO.File.CreateText(NomeFile2)
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        For i = 0 To IndiceRiga
            If Not IsNothing(VettoreExport(i)) Then
                For x = 3 To Len(VettoreExport(i))
                    If Mid(VettoreExport(i), x, 1) <> "0" Then Exit For

                    Mid(VettoreExport(i), x, 1) = Space(1)
                Next

                tw.WriteLine(VettoreExport(i))

                Dim Appoggio As String = VettoreExport(i) & Space(200)
                Dim CSV As String = ""
                If Mid(Appoggio, 1, 1) = "0" Then
                    CSV = "Fattura"
                End If
                If Mid(Appoggio, 1, 1) = "2" Then
                    CSV = "NC"
                End If
                If Mid(Appoggio, 1, 1) = "3" Then
                    CSV = "Incassi"
                End If
                CSV = CSV & Mid(Appoggio, 3, 20) & ";"
                Dim DataSt As String
                Dim Giorni As String = ""
                Dim Mese As String = ""
                Dim Anno As String = ""
                DataSt = Mid(Appoggio, 29, 8)

                Giorni = Mid(DataSt, 1, 2)
                Mese = Mid(DataSt, 3, 2)
                Anno = Mid(DataSt, 5, 4)


                CSV = CSV & Format(DateSerial(Anno, Mese, Giorni), "dd/MM/yyyy") & ";"
                CSV = CSV & Mid(Appoggio, 702, 35) & ";"
                CSV = CSV & Format(CDbl(Mid(Appoggio, 38, 20)), "0.00") & ";"

                CSV = CSV & Mid(Appoggio.Replace(";", ""), 1131, 50) & ";"

                tw2.WriteLine(CSV)
            End If
        Next
        tw.Close()
        tw2.Close()

        If Export_DocumentiIncassi = False Then
            GridView2.AutoGenerateColumns = True
            GridView2.DataSource = MyTable
            GridView1.Font.Size = 10
            GridView2.DataBind()
            GridView2.Visible = True
            Btn_Download.Visible = False
            TabContainer1.ActiveTabIndex = 1
        Else
            GridView2.Visible = False
            TabContainer1.ActiveTabIndex = 1
            Btn_Download.Visible = True
        End If




        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            If Chk_Prova.Checked = False Then
                If Session("NomeEPersonam") = "SMARIAMONTE" Then
                    UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/malnate/fat114.txt", "AdveniasFtpUser", "R35upTf541?")

                End If
                If Session("NomeEPersonam") = "GNOCCHI" Then
                    UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/salice/fat110.txt", "AdveniasFtpUser", "R35upTf541?")
                End If

                If Session("NomeEPersonam") = "SantaMariaProvvidenza" Then
                    UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/romasmp/fat125.txt", "AdveniasFtpUser", "R35upTf541?")
                End If

                If Session("NomeEPersonam") = "PoloSpRiabilitativo" Then
                    UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/tricarico/fat128.txt", "AdveniasFtpUser", "R35upTf541?")
                End If

                If Session("NomeEPersonam") = "RonzoniVilla" Then
                    UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/seregno/fat130.txt", "AdveniasFtpUser", "R35upTf541?")
                End If
            End If
        Else
            Btn_DownloadFile.Visible = True
        End If


        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If
    End Function


    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function


    Public Function FtpFileExists(ByVal Address As String, ByVal UserName As String, ByVal Password As String) As FtpResult
        Try
            Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(Address), System.Net.FtpWebRequest)
            request.Credentials = New System.Net.NetworkCredential(UserName, Password)
            request.GetResponse()
            Return FtpResult.Exists
        Catch ex As Exception
            If UserName = "" Then Return FtpResult.BlankUserName
            If Password = "" Then Return FtpResult.BlankPassword
            If ex.Message.IndexOf("530") > 0 Then Return FtpResult.InvalidLogin
            If Address.ToLower.IndexOf("ftp://ftp") = -1 Then Return FtpResult.InvalidUrl
            If Address = String.Empty Then Return FtpResult.InvalidUrl
            Return FtpResult.DoesNotExist
        End Try
    End Function

    Enum FtpResult
        Exists
        DoesNotExist
        InvalidLogin
        InvalidUrl
        BlankPassword
        BlankUserName
    End Enum


    Public Sub UploadFile(ByVal _FileName As String, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)
        Dim _FileInfo As New System.IO.FileInfo(_FileName)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile

        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = _FileInfo.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte

        ' Opens a file stream (System.IO.FileStream) to read the file to be uploaded
        Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()


        ' Stream to which the file to be upload is written
        Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

        ' Read from the file stream 2kb at a time
        Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)

        ' Till Stream content ends
        Do While contentLen <> 0
            ' Write Content from the file stream to the FTP Upload Stream
            _Stream.Write(buff, 0, contentLen)
            contentLen = _FileStream.Read(buff, 0, buffLength)
        Loop

        ' Close the file stream and the Request Stream
        _Stream.Close()
        _Stream.Dispose()
        _FileStream.Close()
        _FileStream.Dispose()

    End Sub

    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Export_DocumentiIncassi()
    End Sub

    Protected Sub Btn_Download_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Download.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".csv"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=DocumentiIncassi.csv")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("../Menu_Export.aspx")
    End Sub

    Protected Sub Btn_DownloadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadFile.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=DocumentiIncassi.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try

    End Sub
End Class
