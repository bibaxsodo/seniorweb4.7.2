﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression
Partial Class OspitiWeb_Export_Export_IstitutoCiechi
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()
    Dim Vt_Cliente(1000) As String
    'luca.destefani@saserviziassociati.it

    Class Anagrafica
        Public CODICEEXPORT As String = ""
        Public DO11_RAGIONESOCIALE As String = ""
        Public DO11_PIVA As String = ""
        Public DO11_CF As String = ""
        Public DO11_INDIRIZZO As String = ""
        Public DO11_CAP As String = ""
        Public DO11_CITTA_NOME As String = ""
        Public DO11_CITTA_ISTAT As String = ""
        Public DO11_PROVINCIA As String = ""
        Public DO11_CONDPAG_CODICE_SEN As String = ""
        Public DO11_CONDPAG_NOME_SEN As String = ""
        Public DO11_BANCA_CIN As String = ""
        Public DO11_BANCA_ABI As String = ""
        Public DO11_BANCA_CAB As String = ""
        Public DO11_BANCA_CONTO As String = ""
        Public DO11_BANCA_IBAN As String = ""
        Public DO11_BANCA_IDMANDATO As String = ""
        Public DO11_BANCA_DATAMANDATO As String = ""
        Public DO11_OPPOSIZIONE As String = ""



        Public DES_RAGIONESOCIALE As String = ""
        Public DES_PIVA As String = ""
        Public DES_CF As String = ""
        Public DES_INDIRIZZO As String = ""
        Public DES_CAP As String = ""
        Public DES_CITTA_NOME As String = ""
        Public DES_CITTA_ISTAT As String = ""
        Public DES_PROVINCIA As String = ""


        Public DES_COGNOME As String = ""
        Public DES_NOME As String = ""

        Public AddDescrizione As String
    End Class


    Class DocumentoTesta
        Public TipoRecord As String
        Public TipoDocumento As String
        Public DataDocumento As String
        Public NumeroRegistrazione As String
        Public CodiceFiscale As String
        Public Codicecommessa As String
        Public TipoRiga As String
        Public CodiceServizio As String
        Public Descrizioneservizio As String
        Public Quantita As String
        Public Prezzo As String
        Public Importo As String
    End Class

    Class DocumentoRiga
        Public TipoRecord As String
        Public TipoDocumento As String
        Public DataDocumento As String
        Public NumeroRegistrazione As String
        Public CodiceFiscale As String
        Public Codicecommessa As String
        Public TipoRiga As String
        Public CodiceServizio As String
        Public Descrizioneservizio As String
        Public Quantita As String
        Public Prezzo As String
        Public Importo As String
    End Class

    Private Sub ScriviDocumentoTesta(ByVal Dati As DocumentoTesta, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""
        Stringa = ""
        Stringa = Stringa & Dati.TipoRecord & ";"
        Stringa = Stringa & Dati.TipoDocumento & ";"
        Stringa = Stringa & Dati.DataDocumento & ";"
        Stringa = Stringa & Dati.NumeroRegistrazione & ";"
        Stringa = Stringa & Dati.CodiceFiscale & ";"
        Stringa = Stringa & Dati.Codicecommessa & ";"
        Stringa = Stringa & Dati.TipoRiga & ";"
        Stringa = Stringa & Dati.CodiceServizio & ";"
        Stringa = Stringa & Dati.Descrizioneservizio & ";"
        Stringa = Stringa & Dati.Quantita & ";"
        Stringa = Stringa & Dati.Prezzo & ";"
        Stringa = Stringa & Dati.Importo

        File.WriteLine(Stringa)

    End Sub



    Private Sub ScriviDocumentoRiga(ByVal Dati As DocumentoRiga, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & Dati.TipoRecord & ";"
        Stringa = Stringa & Dati.TipoDocumento & ";"
        Stringa = Stringa & Dati.DataDocumento & ";"
        Stringa = Stringa & Dati.NumeroRegistrazione & ";"
        Stringa = Stringa & Dati.CodiceFiscale & ";"
        Stringa = Stringa & Dati.Codicecommessa & ";"
        Stringa = Stringa & Dati.TipoRiga & ";"
        Stringa = Stringa & Dati.CodiceServizio & ";"
        Stringa = Stringa & Dati.Descrizioneservizio & ";"
        Stringa = Stringa & Dati.Quantita & ";"
        Stringa = Stringa & Dati.Prezzo & ";"
        Stringa = Stringa & Dati.Importo

        File.WriteLine(Stringa)

    End Sub




    Private Sub DecodificaAnagrafica(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile)
        Dim cn As OleDbConnection
        Dim Cliente As String
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim CodiceProvincia As String
        Dim CodiceComune As String
        Dim CodiceRegione As String
        Dim Tipo As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Tipo = Registrazione.Tipologia

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
            If Mid(Tipo & Space(10), 1, 1) = " " Then
                CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                If CodiceParente = 0 And CodiceOspite > 0 Then
                    Tipo = "O" & Space(10)
                End If
                If CodiceParente > 0 And CodiceOspite > 0 Then
                    Tipo = "P" & Space(10)
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                CodiceRegione = Mid(Tipo & Space(10), 2, 4)
            End If

        End If
        MyReadSC.Close()
        cn.Close()

        Dim Servizio As New Cls_CentroServizio

        Servizio.CENTROSERVIZIO = Registrazione.CentroServizio
        Servizio.Leggi(Session("DC_OSPITE"), Servizio.CENTROSERVIZIO)


        Dati.AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10)
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeOspite & Space(30), 1, 30) & Ospite.NomeOspite
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dati.DES_COGNOME = Ospite.CognomeOspite
            Dati.DES_NOME = Ospite.NomeOspite

            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia


            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = 0
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If


            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = 0
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

            If Ospite.RecapitoNome <> "" Then
                Dati.DES_RAGIONESOCIALE = Ospite.RecapitoNome

                Dati.DES_INDIRIZZO = Ospite.RecapitoIndirizzo
                Dati.DES_CAP = Ospite.RESIDENZACAP4

                Dim DestCom As New ClsComune

                DestCom.Provincia = Ospite.RecapitoProvincia
                DestCom.Comune = Ospite.RecapitoComune
                DestCom.Leggi(Session("DC_OSPITE"))

                Dati.DES_CITTA_NOME = DestCom.Descrizione
                Dati.DES_CITTA_ISTAT = DestCom.Provincia & DcCom.Comune

                Dim DestProv As New ClsComune

                DestProv.Provincia = Ospite.RecapitoProvincia
                DestProv.Comune = ""
                DestProv.Leggi(Session("DC_OSPITE"))
                Dati.DES_PROVINCIA = DestProv.CodificaProvincia

            End If

        End If
        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10)
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeParente & Space(30), 1, 30) & Ospite.NomeParente
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1


            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
        End If
        If Mid(Tipo & Space(10), 1, 1) = "C" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CC" & CodiceConto
            End If


            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "J" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))


            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CJ" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "R" Then
            Dim Ospite As New ClsUSL

            Ospite.CodiceRegione = CodiceRegione
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO


            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.CodiceRegione
                Dati.CODICEEXPORT = "CR" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Ospite.Nome
            Dati.DO11_PIVA = Ospite.PARTITAIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
        End If

        cn.Close()
    End Sub

    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Session("MYDATE") = Now
        'ADCLIFOR.XXXXX

        'ADDOCUME.XXXXX
        Dim TwDocumento As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documento_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwCSV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim Param As New Cls_DatiGenerali



        Param.LeggiDati(Session("DC_TABELLE"))

        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True

        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                Dim Anagrafiche As New Anagrafica


                DecodificaAnagrafica(Anagrafiche, Registrazione)

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If

                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Or CausaleContabile.TipoDocumento = "NC" Then
                    Call FattureNC(Anagrafiche, Registrazione, CausaleContabile, TwDocumento)


                End If

                Dim Riga As String

                Riga = Registrazione.NumeroRegistrazione & ";" & Registrazione.DataRegistrazione & ";" & Registrazione.DataDocumento & ";" & Registrazione.NumeroDocumento & ";" & Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

                TwCSV.WriteLine(Riga)
            End If
        Next
        cn.Close()


        TwDocumento.Close()
        TwCSV.Close()




        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If



        TabContainer1.ActiveTabIndex = 1
    End Function


    'TwADDOCUME, TwADDOCUMERighe, TwADCLIFOR
    Private Sub FattureNC(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal TwADDOCUME As System.IO.TextWriter)
        Dim DocTesta As New DocumentoTesta
        Dim DocRiga As New DocumentoRiga


        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        Dim DatiSocieta As New Cls_TabellaSocieta

        DatiSocieta.Leggi(Session("DC_TABELLE"))


        
        Dim RigaInterna As Integer = 0
        Dim RigaRicavo As Integer

        For RigaRicavo = 0 To 100
            If Not IsNothing(Reg.Righe(RigaRicavo)) Then
                If Reg.Righe(RigaRicavo).RigaDaCausale = 3 Or Reg.Righe(RigaRicavo).RigaDaCausale = 4 Or Reg.Righe(RigaRicavo).RigaDaCausale = 5 Then
                    If RigaInterna = 0 Then
                        DocTesta.TipoRecord = "T+R"

                        if Reg.CentroServizio ="CDD" then
                            DocTesta.TipoDocumento = "804"
                        else
                            DocTesta.TipoDocumento = "801"
                        End If
                        
                        DocTesta.DataDocumento = Format(Reg.DataRegistrazione, "dd/MM/yyyy")
                        DocTesta.NumeroRegistrazione = Reg.NumeroProtocollo
                        DocTesta.CodiceFiscale = Dati.DO11_CF

                        TabTrascodifica.TIPOTAB = "CS"
                        TabTrascodifica.SENIOR = Reg.CentroServizio
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        DocTesta.Codicecommessa = TabTrascodifica.EXPORT

                        DocTesta.TipoRiga = "85"
                        DocTesta.CodiceServizio = Reg.CentroServizio '???
                        DocTesta.Descrizioneservizio = CentroServizio.DESCRIZIONE
                        DocTesta.Quantita = 1
                        DocTesta.Prezzo = Reg.Righe(RigaRicavo).Importo
                        DocTesta.Importo = Reg.Righe(RigaRicavo).Importo



                        ScriviDocumentoTesta(DocTesta, TwADDOCUME)

                    Else
                        DocRiga.TipoRecord = "RIG"


                        
                        if Reg.CentroServizio ="CDD" then
                            DocRiga.TipoDocumento = "804"
                        else
                            DocRiga.TipoDocumento = "801"
                        End If
                        
                        DocRiga.DataDocumento = Format(Reg.DataRegistrazione, "dd/MM/yyyy")
                        DocRiga.NumeroRegistrazione = Reg.NumeroProtocollo
                        DocRiga.CodiceFiscale = Dati.DO11_CF

                        TabTrascodifica.TIPOTAB = "CS"
                        TabTrascodifica.SENIOR = Reg.CentroServizio
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        DocRiga.Codicecommessa = TabTrascodifica.EXPORT


                        DocRiga.TipoRiga = "85"
                        DocRiga.CodiceServizio = Reg.CentroServizio '???
                        DocRiga.Descrizioneservizio = CentroServizio.DESCRIZIONE
                        DocRiga.Quantita = 1
                        DocRiga.Prezzo = Reg.Righe(RigaRicavo).Importo
                        DocRiga.Importo = Reg.Righe(RigaRicavo).Importo


                        ScriviDocumentoRiga(DocRiga, TwADDOCUME)


                    End If

                    RigaInterna = RigaInterna + 1

                    
                End If
            End If
        Next






    End Sub



    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        If Param.CausaleRettifica = "" Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0)  order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CausaleContabile <> '" & Param.CausaleRettifica & "' And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0)  order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        End If
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            Btn_Export.ImageUrl = "~/images/sendmail.png"
            Btn_Export.Visible = True
        Else
            Btn_Export.ImageUrl = "~/images/download.png"
            Btn_Export.Visible = True
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click
        Estrazione()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        Dim K1 As New Cls_SqlString


        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function



    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Call Export_DocumentiIncassi()

        Btn_DownloadMOVIM.Visible = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Export.aspx")
    End Sub


    Protected Sub Btn_DownloadMOVIM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadMOVIM.Click
        Dim NomeFile As String

        Try

            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti.txt")
        Catch ex As Exception

        End Try


        Dim Zipfile As New Ionic.Zip.ZipFile

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documento_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti.txt")

        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti.txt", "\")



        Zipfile.Save(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=documenti.zip")
            Response.WriteFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub

End Class
