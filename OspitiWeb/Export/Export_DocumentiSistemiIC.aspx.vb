﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression

Partial Class OspitiWeb_Export_Export_DocumentiSistemiIC
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()



    Class Anagrafica
        Public DO11_RAGIONESOCIALE As String = ""
        Public DO11_PIVA As String = ""
        Public DO11_CF As String = ""
        Public DO11_INDIRIZZO As String = ""
        Public DO11_CAP As String = ""
        Public DO11_CITTA_NOME As String = ""
        Public DO11_CITTA_ISTAT As String = ""
        Public DO11_PROVINCIA As String = ""
        Public DO11_CONDPAG_CODICE_SEN As String = ""
        Public DO11_CONDPAG_NOME_SEN As String = ""
        Public DO11_BANCA_CIN As String = ""
        Public DO11_BANCA_ABI As String = ""
        Public DO11_BANCA_CAB As String = ""
        Public DO11_BANCA_CONTO As String = ""
        Public DO11_BANCA_IBAN As String = ""
        Public DO11_BANCA_IDMANDATO As String = ""
        Public DO11_BANCA_DATAMANDATO As String = ""
        Public DO11_OPPOSIZIONE As String = ""



        Public DES_RAGIONESOCIALE As String = ""
        Public DES_PIVA As String = ""
        Public DES_CF As String = ""
        Public DES_INDIRIZZO As String = ""
        Public DES_CAP As String = ""
        Public DES_CITTA_NOME As String = ""
        Public DES_CITTA_ISTAT As String = ""
        Public DES_PROVINCIA As String = ""

        Public AddDescrizione As String

        Public TIPO As String = ""
    End Class

    Private Function CercaRegione(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Integer) As String
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cn.Open()

        CercaRegione = ""

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From AnagraficaComune Where MastroCliente = ?  And ContoCliente = ? And SottocontoCliente = ?  And Tipologia = 'R'  "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Mastro", Mastro)
        cmdRd.Parameters.AddWithValue("@Conto", Conto)
        cmdRd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            CercaRegione = campodb(MyReadSC.Item("CodiceRegione"))
        End If
        cn.Close()
    End Function

    Private Function CercaComune(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Integer) As String
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cn.Open()

        CercaComune = ""

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From AnagraficaComune Where MastroCliente = ?  And ContoCliente = ? And SottocontoCliente = ?  And Tipologia = 'C'  "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Mastro", Mastro)
        cmdRd.Parameters.AddWithValue("@Conto", Conto)
        cmdRd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            CercaComune = campodb(MyReadSC.Item("CODICEPROVINCIA")) & campodb(MyReadSC.Item("CODICECOMUNE"))
        End If
        cn.Close()
    End Function

    Private Sub DecodificaAnagrafica(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile)
        Dim cn As OleDbConnection
        Dim Cliente As String
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim CodiceProvincia As String
        Dim CodiceComune As String
        Dim CodiceRegione As String
        Dim Tipo As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Tipo = Registrazione.Tipologia

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))

            If Tipo <> "" Then
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If
            Else

                Dim PianoConti As New Cls_Pianodeiconti

                PianoConti.Mastro = campodb(MyReadSC.Item("MastroPartita"))
                PianoConti.Conto = campodb(MyReadSC.Item("ContoPartita"))
                PianoConti.Sottoconto = campodb(MyReadSC.Item("SottocontoPartita"))
                PianoConti.Decodfica(Session("DC_GENERALE"))
                If PianoConti.TipoAnagrafica = "R" Then
                    Tipo = "R" & CercaRegione(PianoConti.Mastro, PianoConti.Conto, PianoConti.Sottoconto)
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If
                If PianoConti.TipoAnagrafica = "C" Then
                    Tipo = "C" & CercaComune(PianoConti.Mastro, PianoConti.Conto, PianoConti.Sottoconto)

                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If


                If PianoConti.TipoAnagrafica = "P" Or PianoConti.TipoAnagrafica = "O" Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    Tipo = PianoConti.TipoAnagrafica
                End If

            End If

        End If
        MyReadSC.Close()
        cn.Close()
        Dati.TIPO = Mid(Tipo & Space(10), 1, 1)


        Dati.AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
            Dati.DO11_RAGIONESOCIALE = Ospite.CognomeOspite & " " & Ospite.NomeOspite
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1

            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia


            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = 0
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If


            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = 0
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

            If Ospite.RecapitoNome <> "" Then
                Dati.DES_RAGIONESOCIALE = Ospite.RecapitoNome

                Dati.DES_INDIRIZZO = Ospite.RecapitoIndirizzo
                Dati.DES_CAP = Ospite.RESIDENZACAP4

                Dim DestCom As New ClsComune

                DestCom.Provincia = Ospite.RecapitoProvincia
                DestCom.Comune = Ospite.RecapitoComune
                DestCom.Leggi(Session("DC_OSPITE"))

                Dati.DES_CITTA_NOME = DestCom.Descrizione
                Dati.DES_CITTA_ISTAT = DestCom.Provincia & DcCom.Comune

                Dim DestProv As New ClsComune

                DestProv.Provincia = Ospite.RecapitoProvincia
                DestProv.Comune = ""
                DestProv.Leggi(Session("DC_OSPITE"))
                Dati.DES_PROVINCIA = DestProv.CodificaProvincia

            End If

        End If
        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
            Dati.DO11_RAGIONESOCIALE = Ospite.CognomeParente & " " & Ospite.NomeParente
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1


            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
        End If
        If Mid(Tipo & Space(10), 1, 1) = "C" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "J" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "R" Then
            Dim Ospite As New ClsUSL

            Ospite.CodiceRegione = CodiceRegione
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Nome
            Dati.DO11_PIVA = Ospite.PARTITAIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
        End If

        cn.Close()
    End Sub

    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Dim VerificaCodiceFiscale As New Cls_CodiceFiscale
        Session("MYDATE") = Now


        Dim TwIncassi As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Incassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwFatture As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwCSV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim Param As New Cls_DatiGenerali
        Dim Errore As Boolean = False



        Param.LeggiDati(Session("DC_TABELLE"))



        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))


        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True

        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                Dim Anagrafiche As New Anagrafica


                DecodificaAnagrafica(Anagrafiche, Registrazione)

                If Anagrafiche.TIPO <> "R" And Anagrafiche.TIPO <> "C" Then
                    If VerificaCodiceFiscale.Check_CodiceFiscale(Anagrafiche.DO11_CF) = False And (Registrazione.Tipologia = "" Or Mid(Registrazione.Tipologia & Space(10), 1, 1) = "O" Or Mid(Registrazione.Tipologia & Space(10), 1, 1) = "P") Then
                        Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                        myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                        myrigaerrore.Item("Cliente") = Anagrafiche.DO11_RAGIONESOCIALE
                        myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per ospite/parente "
                        MyTable.Rows.Add(myrigaerrore)

                        Errore = True
                    End If
                End If

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If

                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Or CausaleContabile.TipoDocumento = "NC" Then
                    Call FattureNC(Anagrafiche, Registrazione, CausaleContabile, TwFatture)
                End If
                
                Dim Riga As String

                Riga = Registrazione.NumeroRegistrazione & ";" & Registrazione.DataRegistrazione & ";" & Registrazione.DataDocumento & ";" & Registrazione.NumeroDocumento & ";" & Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

                TwCSV.WriteLine(Riga)
            End If
        Next
        cn.Close()


        TwFatture.Close()

        TwIncassi.Close()
        TwCSV.Close()

        If Errore = True Then
            GridView2.AutoGenerateColumns = True
            GridView2.DataSource = MyTable
            GridView1.Font.Size = 10
            GridView2.DataBind()
            GridView2.Visible = True

            Btn_DownloadMOVIM.Visible = False
            TabContainer1.ActiveTabIndex = 1
        Else
            GridView2.Visible = False
        End If


        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If

        Btn_DownloadMOVIM.Visible = True
        TabContainer1.ActiveTabIndex = 1
    End Function

    Private Sub Incassi(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal File As System.IO.TextWriter)
        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni
        Dim Legami As New Cls_Legami
        Dim RegistrazioneIncasso As New Cls_MovimentoContabile
        Dim CausaleContabileDocumentoCollegato As New Cls_CausaleContabile
        Dim X As Integer

        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Registrazione.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)


        'Riga.Tipodoc = 3

        RegistrazioneIncasso.NumeroRegistrazione = Registrazione.NumeroRegistrazione
        RegistrazioneIncasso.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        Dim lk As New Cls_Legami
        Dim RigaRecordIncasso As String = ""

        lk.Leggi(Session("DC_GENERALE"), 0, Registrazione.NumeroRegistrazione)

        Dim TabTrascodificaCausale As New Cls_TabellaTrascodificheEsportazioni

        Dim IndDocu As Integer
        Dim DocumentiLegati As Boolean = False

        For IndDocu = 0 To 100
            If lk.NumeroDocumento(IndDocu) > 0 Then
                Registrazione.Leggi(Session("DC_GENERALE"), lk.NumeroDocumento(IndDocu))

                CausaleContabileDocumentoCollegato.Codice = Registrazione.CausaleContabile

                CausaleContabileDocumentoCollegato.Leggi(Session("DC_TABELLE"), CausaleContabileDocumentoCollegato.Codice)
                DocumentiLegati = True

                RigaRecordIncasso = ""

                If IsNothing(Dati.DO11_CF) Then
                    Dati.DO11_CF = ""
                End If
                If campodb(Dati.DO11_CF).Trim = "" Then
                    If IsNothing(Dati.DO11_PIVA) Then
                        Dati.DO11_PIVA = 0
                    End If
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
                Else
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
                End If

                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")
                Dim TipoCausale As String = "CRD"




                TabTrascodificaCausale.TIPOTAB = "CS"
                TabTrascodificaCausale.SENIOR = RegistrazioneIncasso.CausaleContabile
                TabTrascodificaCausale.EXPORT = "0"
                TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                If TabTrascodificaCausale.EXPORT = "0" Or TabTrascodificaCausale.EXPORT = "" Then
                    RigaRecordIncasso = RigaRecordIncasso & "CRD"
                Else
                    RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale.EXPORT
                End If
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                If lk.Importo(IndDocu) > 0 Then
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(lk.Importo(IndDocu), 2) * 100, 9)
                    RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(0).DareAvere
                Else
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Abs(Math.Round(lk.Importo(IndDocu), 2)) * 100, 9)
                    If RegistrazioneIncasso.Righe(0).DareAvere = "D" Then
                        RigaRecordIncasso = RigaRecordIncasso & "A"
                    Else
                        RigaRecordIncasso = RigaRecordIncasso & "D"
                    End If
                End If


                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero("123015000001", 12)
                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                File.WriteLine(RigaRecordIncasso)



            End If
        Next

        If DocumentiLegati = False Then


            RigaRecordIncasso = ""

            If IsNothing(Dati.DO11_CF) Then
                Dati.DO11_CF = ""
            End If
            If campodb(Dati.DO11_CF).Trim = "" Then
                If IsNothing(Dati.DO11_PIVA) Then
                    Dati.DO11_PIVA = 0
                End If
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
            Else
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
            End If

            RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")
            Dim TipoCausale As String = "CRD"




            TabTrascodificaCausale.TIPOTAB = "CS"
            TabTrascodificaCausale.SENIOR = RegistrazioneIncasso.CausaleContabile
            TabTrascodificaCausale.EXPORT = "0"
            TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

            If TabTrascodificaCausale.EXPORT = "0" Or TabTrascodificaCausale.EXPORT = "" Then
                RigaRecordIncasso = RigaRecordIncasso & "CRD"
            Else
                RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale.EXPORT
            End If
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 4)
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 8)

            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Abs(Math.Round(RegistrazioneIncasso.Righe(0).Importo, 2)) * 100, 9)
            RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(0).DareAvere


            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero("123015000001", 12)
            RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

            File.WriteLine(RigaRecordIncasso)

        End If

        Dim TabTrascodificaCC As New Cls_TabellaTrascodificheEsportazioni

        If RegistrazioneIncasso.Righe(1).Importo > 0 Then
            RigaRecordIncasso = ""

            If IsNothing(Dati.DO11_CF) Then
                Dati.DO11_CF = ""
            End If
            If campodb(Dati.DO11_CF).Trim = "" Then
                If IsNothing(Dati.DO11_PIVA) Then
                    Dati.DO11_PIVA = 0
                End If
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
            Else
                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
            End If

            RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")



            TabTrascodificaCausale.TIPOTAB = "CS"
            TabTrascodificaCausale.SENIOR = RegistrazioneIncasso.CausaleContabile
            TabTrascodificaCausale.EXPORT = "0"
            TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

            If TabTrascodificaCausale.EXPORT = "0" Or TabTrascodificaCausale.EXPORT = "" Then
                RigaRecordIncasso = RigaRecordIncasso & "CRD"
            Else
                RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale.EXPORT
            End If
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 4)
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 8)
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(1).Importo, 2) * 100, 9)
            RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(1).DareAvere

            TabTrascodificaCC.TIPOTAB = "CC"
            TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(1).MastroPartita & "." & RegistrazioneIncasso.Righe(1).ContoPartita & "." & RegistrazioneIncasso.Righe(1).SottocontoPartita
            TabTrascodificaCC.EXPORT = 0
            TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 12)
            RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

            File.WriteLine(RigaRecordIncasso)
        End If


        RigaRecordIncasso = ""


        Dim InRiga As Integer
        For InRiga = 2 To 100
            If Not IsNothing(RegistrazioneIncasso.Righe(InRiga)) Then
                If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 3 Then
                    If RegistrazioneIncasso.Righe(InRiga).Importo <> 0 Then
                        RigaRecordIncasso = ""

                        If IsNothing(Dati.DO11_CF) Then
                            Dati.DO11_CF = ""
                        End If
                        If campodb(Dati.DO11_CF).Trim = "" Then
                            If IsNothing(Dati.DO11_PIVA) Then
                                Dati.DO11_PIVA = 0
                            End If
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")

                        Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                        TabTrascodificaCausale1.TIPOTAB = "CS"
                        TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                        TabTrascodificaCausale1.EXPORT = "0"
                        TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                        If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                            RigaRecordIncasso = RigaRecordIncasso & "CRD"
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                        End If
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                        RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                        TabTrascodificaCC.TIPOTAB = "CC"
                        TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                        TabTrascodificaCC.EXPORT = 0
                        TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 12)
                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                        File.WriteLine(RigaRecordIncasso)
                    End If
                End If
                If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 4 Then
                    If RegistrazioneIncasso.Righe(InRiga).Importo > 0 Then
                        RigaRecordIncasso = ""

                        If IsNothing(Dati.DO11_CF) Then
                            Dati.DO11_CF = ""
                        End If
                        If campodb(Dati.DO11_CF).Trim = "" Then
                            If IsNothing(Dati.DO11_PIVA) Then
                                Dati.DO11_PIVA = 0
                            End If
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")


                        Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                        TabTrascodificaCausale1.TIPOTAB = "CS"
                        TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                        TabTrascodificaCausale1.EXPORT = 0
                        TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                        If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                            RigaRecordIncasso = RigaRecordIncasso & "CRD"
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                        RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                        TabTrascodificaCC.TIPOTAB = "CC"
                        TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                        TabTrascodificaCC.EXPORT = 0
                        TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 12)
                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                        File.WriteLine(RigaRecordIncasso)
                    End If
                End If
                If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 6 Then
                    If RegistrazioneIncasso.Righe(InRiga).Importo > 0 Then
                        RigaRecordIncasso = ""

                        If IsNothing(Dati.DO11_CF) Then
                            Dati.DO11_CF = ""
                        End If
                        If campodb(Dati.DO11_CF).Trim = "" Then
                            If IsNothing(Dati.DO11_PIVA) Then
                                Dati.DO11_PIVA = 0
                            End If
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(Dati.DO11_PIVA), 11) & Space(5)
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(Dati.DO11_CF, 16)
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")

                        Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                        TabTrascodificaCausale1.TIPOTAB = "CS"
                        TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                        TabTrascodificaCausale1.EXPORT = 0
                        TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                        If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                            RigaRecordIncasso = RigaRecordIncasso & "CRD"
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                        RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                        TabTrascodificaCC.TIPOTAB = "CC"
                        TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                        TabTrascodificaCC.EXPORT = 0
                        TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 12)
                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                        File.WriteLine(RigaRecordIncasso)
                    End If
                End If
            End If
        Next

    End Sub

    Private Sub FattureNC(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal File As System.IO.TextWriter)
        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni
        Dim Tipo As String = ""
        Dim VerificaCodiceFiscale As New Cls_CodiceFiscale


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Registrazione.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        Tipo = Registrazione.Tipologia

        Dim RigaRecord As String
        Dim Entrato As Boolean = False



        For I = 0 To 100

            If IsNothing(Registrazione.Righe(I)) Then

                If Registrazione.Righe(I).RigaDaCausale > 2 Then

                    If Entrato = False Then
                        RigaRecord = "T+R;"
                    Else
                        RigaRecord = "RIG;"
                    End If

                    RigaRecord = RigaRecord & "801" & ";"
                    RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "dd/MM/yyyy") & ";"
                    RigaRecord = RigaRecord & Registrazione.NumeroProtocollo
                    If Dati.DO11_CF <> "" Then
                        RigaRecord = RigaRecord & Dati.DO11_CF & ";"
                    Else
                        RigaRecord = RigaRecord & Dati.DO11_PIVA & ";"
                    End If

                    
                    TabTrascodifica.TIPOTAB = "CS"
                    TabTrascodifica.SENIOR = Registrazione.CentroServizio
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))


                    RigaRecord = RigaRecord & TabTrascodifica.EXPORT & ";"

                    RigaRecord = RigaRecord & "85" & ";"

                    RigaRecord = RigaRecord & Registrazione.Righe(I).MastroPartita & "." & Registrazione.Righe(I).ContoPartita & "." & Registrazione.Righe(I).SottocontoPartita & ";"

                    RigaRecord = RigaRecord & Registrazione.Righe(I).Descrizione & ";"


                    If Math.Round(Registrazione.Righe(I).Importo / Registrazione.Righe(I).Quantita, 10) = Math.Round(Registrazione.Righe(I).Importo / Registrazione.Righe(I).Quantita, 0) Then
                        RigaRecord = RigaRecord & Registrazione.Righe(I).Quantita & ";"
                    End If
                    RigaRecord = RigaRecord & Registrazione.Righe(I).Importo & ";"




                    File.WriteLine(RigaRecord)

                End If

            End If
        Next




    End Sub



    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        
        cmd.CommandText = "Select * From MovimentiContabiliTesta where NumeroProtocollo = 0 And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0)  order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"

        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        Btn_Export.ImageUrl = "~/images/elabora.png"
        Btn_Export.Visible = True

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click
        Estrazione()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        Dim K1 As New Cls_SqlString


        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function



    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Call Export_DocumentiIncassi()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Export.aspx")
    End Sub


    
    Protected Sub Btn_DownloadMOVIM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadMOVIM.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=EXPORT.CSV")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
