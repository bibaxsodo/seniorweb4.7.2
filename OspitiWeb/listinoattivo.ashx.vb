﻿Public Class listinoattivo
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim Listino As New Cls_Tabella_Listino

        Try
            If Listino.EsisteListino(HttpContext.Current.Session("DC_OSPITE")) Then
                context.Response.Write("SI")
            Else
                context.Response.Write("NO")
            End If
        Catch ex As Exception
            context.Response.Write("NO")
        End Try


    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class