﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Config" CodeFile="Menu_Config.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Configurazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Menu' Configurazione</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" valign="top">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center;">
                                    <a href="ElencoCentroServizio.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Centro Servizio" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoCausali.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Causali Entrata/Uscita" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Parametri.aspx">
                                        <img alt="Parametri" src="../images/Menu_Tabelle.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoTipoOperazione.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Operazione" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoModalitapagamento.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tipo Addebito/Accredito" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CENTRO SERV.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CAUSALI E/U</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PARAMETRI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO OPER.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOD.PAG.</span></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="ElencoCausaliEpersonam.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Listino" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoNucleiCentroServizi.aspx">
                                        <img alt="Nuclei-Cserv" src="../images/Menu_Tabelle.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_CausaliEpersonamDomiciliare.aspx">
                                        <img alt="SENIOR-EPERSONAM DOM" src="../images/Menu_Tabelle.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SENIOR-EPERSONAM</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">NULCEI-CSERV</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SENIOR-EP.DOM.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;"></td>

                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>


                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>


                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
