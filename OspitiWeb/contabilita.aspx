﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="contabilita" CodeFile="contabilita.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Ospiti Web</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0b1/jquery.mobile-1.0b1.min.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/mobile/1.0b1/jquery.mobile-1.0b1.min.js"></script>
    <script type="text/javascript">


        (function ($, undefined) {
            $.mobile.currentMenu = '';
            $.widget("mobile.dualColumn", $.mobile.widget, {
                _create: function () {
                    if ($('html').hasClass('min-width-768px')) { //if there's enough place
                        var M = function (e) { //builds the menu everywhere
                            var $t = $(this), $n = $t.find(":jqmData(role='menu')");

                            $t.children('.ui-header,.ui-footer').width('auto');
                            var c = $t.children().eq(0);

                            if ($n.length) {
                                $.mobile.currentMenu = $n;
                                $n.menu();
                            } else {
                                if ($.mobile.currentMenu.length) c.before($.mobile.currentMenu = $.mobile.currentMenu.clone());
                            }
                        }
                        //builds it the first time. It must be bound to a good event so that it's not needed. Waiting for the next jqm release
                        M.apply(this.element);
                        $('div').live('pagebeforeshow', M);

                    }
                }
            });

            $.widget("mobile.menu", $.mobile.widget, {
                options: {
                    width: "20%"
                },
                _create: function () {
                    var $h = $('html'),
                        $m = this.element,
                        $p = $m.parents(":jqmData(role='page')"),
                        o = this.options;
                    if ($m.is(":jqmData(role='menu')")) { //if there's enough place
                        if ($m.hasClass('ui-menu')) { //if it's a menu I reproduced
                            return;
                        }
                        $p.children('.ui-header,.ui-footer').width('auto');
                        var c = $p.children().eq(0);

                        c.before($m);
                        $m.css({ "width": o.width, "height": "100%", "float": "left" }).addClass('ui-menu ui-content').page();

                    }
                }
            });

            $(function () { $("[data-role='page']:first").dualColumn(); });
        })(jQuery);
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div data-role="page">
            <div data-role="menu" data-width="25%">
                <div data-role="header" data-nobackbtn="true">
                    <span class="ui-li-count"></span>
                    <h1>Senior Contabilità Web</h1>
                </div>
                <!-- /header -->
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

                <div data-role="footer">
                    <span class="ui-li-count"></span>
                    <h1>Senior Contabilità Web</h1>
                </div>
            </div>
            <!-- /menu -->

            <div data-role="content">
                <asp:Label ID="Lbl_Registrazione" runat="server" Text=""></asp:Label>
                <asp:Label ID="lbl_container" runat="server" Text="Label"></asp:Label>
            </div>
            <!-- /content -->
        </div>
    </form>
</body>
</html>
