﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_TipoAddebito
    Inherits System.Web.UI.Page
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select column_name, data_type, character_maximum_length      from information_schema.columns   where table_name like 'TabTipoAddebito' and column_name like  'Descrizione'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Txt_Descrizione.MaxLength = Val(campodb(myPOSTreader.Item("character_maximum_length")))
        End If
        cn.Close()


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim M As New Cls_ModalitaPagamento


        M.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPamento1)
        M.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPamento2)
        M.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPamento3)
        M.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPamento4)




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim Y As New Cls_IVA

        Y.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)


        If Request.Item("CODICE") = "" Then
            Dim MaxTpAd As New Cls_Addebito

            Txt_Codice.Text = MaxTpAd.MaxTipoAddebito(Session("DC_OSPITE"))
            Exit Sub
        End If

        Dim TpAd As New Cls_Addebito


        TpAd.Codice = Request.Item("CODICE")
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Txt_Codice.Text = TpAd.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = TpAd.Descrizione

        DD_IVA.SelectedValue = TpAd.CodiceIVA
        Dim DecConto As New Cls_Pianodeiconti


        DD_Struttura.SelectedValue = TpAd.Struttura
        AggiornaCServ()
        Dim KCserv As New Cls_CentroServizio

        KCserv.CENTROSERVIZIO = TpAd.CENTROSERVIZIO
        KCserv.Leggi(Session("DC_OSPITE"), TpAd.CENTROSERVIZIO)
        If KCserv.DESCRIZIONE <> "" Then

            DD_Struttura.SelectedValue = KCserv.Villa
            AggiornaCServ()
            Cmb_CServ.SelectedValue = TpAd.CENTROSERVIZIO
        End If

        Txt_DataScadenza.Text = TpAd.DataScadenza

        DecConto.Mastro = TpAd.Mastro
        DecConto.Conto = TpAd.Conto
        DecConto.Sottoconto = TpAd.Sottoconto

        DecConto.Decodfica(Session("DC_GENERALE"))
        Txt_Conto.Text = TpAd.Mastro & " " & TpAd.Conto & " " & TpAd.Sottoconto & " " & DecConto.Descrizione


        If TpAd.Deposito = 1 Then
            Chk_DepositoCauzionale.Checked = True
        Else
            Chk_DepositoCauzionale.Checked = False
        End If


        If TpAd.FuoriRetta = 1 Then
            Chk_FuoriRetta.Checked = True
        Else
            Chk_FuoriRetta.Checked = False
        End If

        DD_ModalitaPamento1.SelectedValue = TpAd.ModalitaPagamentoEpersonam1
        If TpAd.ModalitaPagamentoEpersonam2 <> "" Then
            DD_ModalitaPamento2.SelectedValue = TpAd.ModalitaPagamentoEpersonam2
            DD_ModalitaPamento2.Visible = True
            Btn_Plus1.Visible = False
            Btn_Plus2.Visible = True
        End If
        If TpAd.ModalitaPagamentoEpersonam3 <> "" Then
            DD_ModalitaPamento3.SelectedValue = TpAd.ModalitaPagamentoEpersonam3
            DD_ModalitaPamento3.Visible = True
            Btn_Plus2.Visible = False
            Btn_Plus3.Visible = True
        End If

        If TpAd.ModalitaPagamentoEpersonam4 <> "" Then
            DD_ModalitaPamento4.SelectedValue = TpAd.ModalitaPagamentoEpersonam4
            DD_ModalitaPamento3.Visible = True
            Btn_Plus2.Visible = False
            Btn_Plus3.Visible = True
        End If


        Txt_Importo.Text = Format(TpAd.Importo, "#,##0.00")


        Call EseguiJS()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If


        If Txt_Codice.Enabled = True Then
            Dim TpVr As New Cls_Addebito

            TpVr.Codice = Txt_Codice.Text
            TpVr.Leggi(Session("DC_OSPITE"), TpVr.Codice)

            If TpVr.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                Exit Sub
            End If
        End If


        Dim TpAd As New Cls_Addebito
        Dim Vettore(100) As String


        TpAd.Codice = Txt_Codice.Text
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(TpAd)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "Addebito", AppoggioJS)


        TpAd.Descrizione = Txt_Descrizione.Text

        Vettore = SplitWords(Txt_Conto.Text)

        TpAd.Mastro = 0
        TpAd.Conto = 0
        TpAd.Sottoconto = 0
        If Vettore.Length >= 3 Then
            TpAd.Mastro = Val(Vettore(0))
            TpAd.Conto = Val(Vettore(1))
            TpAd.Sottoconto = Val(Vettore(2))
        End If


        TpAd.ModalitaPagamentoEpersonam1 = DD_ModalitaPamento1.SelectedValue
        TpAd.ModalitaPagamentoEpersonam2 = DD_ModalitaPamento2.SelectedValue
        TpAd.ModalitaPagamentoEpersonam3 = DD_ModalitaPamento3.SelectedValue
        TpAd.ModalitaPagamentoEpersonam4 = DD_ModalitaPamento4.SelectedValue


        TpAd.Struttura = DD_Struttura.SelectedValue
        TpAd.CENTROSERVIZIO = Cmb_CServ.Text
        TpAd.CodiceIVA = DD_IVA.SelectedValue

        If Txt_Importo.Text = "" Then
            Txt_Importo.Text = "0"
        End If
        TpAd.Importo = Txt_Importo.Text

        TpAd.DataScadenza = IIf(Txt_DataScadenza.Text = "", Nothing, Txt_DataScadenza.Text)
        If Chk_DepositoCauzionale.Checked = True Then
            TpAd.Deposito = 1
        Else
            TpAd.Deposito = 0
        End If
        If Chk_FuoriRetta.Checked = True Then
            TpAd.FuoriRetta = 1
        Else
            TpAd.FuoriRetta = 0
        End If

        TpAd.Scrivi(Session("DC_OSPITE"))
        
        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If



        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select * from ADDACR  " & _
                               "  Where CodiceIva =  ?")
        cmd.Connection = cn

        cmd.Parameters.AddWithValue("@CodiceIva", Txt_Codice.Text.Trim)


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Tipo Addebito usato non posso eliminarlo');", True)
            Exit Sub
        End If
        myPOSTreader.Close()
        cn.Close()


        Dim TpAd As New Cls_Addebito

        TpAd.Codice = Txt_Codice.Text
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(TpAd)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "Addebito", AppoggioJS)


        TpAd.Codice = Txt_Codice.Text
        TpAd.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
   

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoTipoAddebito.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim MaxTpAd As New Cls_Addebito

        Txt_Codice.Text = MaxTpAd.MaxTipoAddebito(Session("DC_OSPITE"))

        Txt_Codice.Enabled = True

        Call EseguiJS()

        Call Txt_Codice_TextChanged(sender, e)

        Call Txt_Descrizione_TextChanged(sender, e)

    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then            
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Addebito

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_OSPITE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Addebito

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_OSPITE"), x.Descrizione)

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Plus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus1.Click
        DD_ModalitaPamento2.Visible = True
        Btn_Plus1.Visible = False
        Btn_Plus2.Visible = True
    End Sub

    Protected Sub Btn_Plus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus2.Click
        DD_ModalitaPamento3.Visible = True
        Btn_Plus2.Visible = False
        Btn_Plus3.Visible = True
    End Sub

    Protected Sub Btn_Plus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus3.Click
        DD_ModalitaPamento4.Visible = True
        Btn_Plus3.Visible = False
    End Sub
End Class
