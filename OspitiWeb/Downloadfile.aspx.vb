﻿Imports System.Web.Hosting

Partial Class OspitiWeb_Downloadfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.ClearHeaders()
        HttpContext.Current.Response.ClearContent()
        Response.Clear()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("Content-Disposition", _
                            "attachment; filename=""" & "Copia_" & Request.Item("FileName") & """")
        Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "FileUpLoad\" & Request.Item("MyCasualDir") & "\" & Request.Item("FileName"))
        Response.End()
    End Sub
End Class
