﻿
Partial Class OspitiWeb_Export_RID_Step2
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        LblErroriImportazione.Text = "<table class=""tabella"">"

        LblErroriImportazione.Text = LblErroriImportazione.Text & "<tr class=""miotr""><th class=""miaintestazione"">Cognome Nome</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">Iban</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">CodiceFiscale</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">DataMandato</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">IdMandato</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">ImportoTotale</th></tr>"

        LblErroriImportazione.Text = LblErroriImportazione.Text & Session("ERRORIIMPORT")

        LblErroriImportazione.Text = LblErroriImportazione.Text & "</table>"


    End Sub


    Protected Sub Btn_Dowload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Download.Click
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "text/plain"
        Response.AppendHeader("content-disposition", "attachment;filename=Rid" & Request.Item("ID") & ".xml")
        Response.WriteFile(Session("NOMEFILE"))
        Response.Flush()
        Response.End()
    End Sub



    Protected Sub BTN_CSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_CSV.Click
        Tabella = Session("TabellaRid")

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("content-disposition", "attachment;filename=RidInviati.csv")

        If Not IsNothing(Session("CSVFILE")) Then

            Response.Write(Session("CSVFILE"))
            Response.End()
        Else
            Dim sb As New StringBuilder()

            For i As Integer = 0 To Tabella.Columns.Count - 1
                sb.Append(Tabella.Columns(i).ColumnName + ",")
            Next
            sb.Append(Environment.NewLine)

            For j As Integer = 0 To Tabella.Rows.Count - 1
                For k As Integer = 0 To Tabella.Columns.Count - 1
                    sb.Append(Tabella.Rows(j)(k).ToString().Replace(",", ".") + ",")
                Next
                sb.Append(Environment.NewLine)
            Next
            Response.Write(sb.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Try
            Kill(Session("NOMEFILE"))
        Catch ex As Exception

        End Try
        Response.Redirect("Export_RID.ASPX")
    End Sub

    
 
End Class
