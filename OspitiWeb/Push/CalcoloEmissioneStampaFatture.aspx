﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Push_CalcoloEmissioneStampaFatture" CodeFile="CalcoloEmissioneStampaFatture.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Elaborazione Fatture</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="../ospiti.css?ver=10" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="../js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <link rel="manifest" href="manifest.json">
    <script src="../js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="../js/JSErrore.js" type="text/javascript"></script>
    <script src="/js/listacomuni.js" type="text/javascript"></script>
    <script src="../js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="../dialogbox/script.js"></script>

    <link rel="stylesheet" href="../jqueryui/jquery-ui.css" type="text/css">
    <script src="../jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="../jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager2" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Elaborazione Fatture</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/rettatotale.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Test Notifica" ID="Img_TestNotifica"></asp:ImageButton>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi"
                 Height="38px" ToolTip="Esegui" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="~/images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Elaborazione Fatture              
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div>
                                        <asp:Button ID="Btn_Test" runat="server" Text="Button" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">Anno :</label>
                                        <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="64px"></asp:TextBox><br />
                                        <br />
                                        <label class="LabelCampo">Mese :</label>
                                        <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                            <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                            <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                            <asp:ListItem Value="4">Aprile</asp:ListItem>
                                            <asp:ListItem Value="5">Maggio</asp:ListItem>
                                            <asp:ListItem Value="6">Giugno</asp:ListItem>
                                            <asp:ListItem Value="7">Luglio</asp:ListItem>
                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                            <asp:ListItem Value="9">Settembre</asp:ListItem>
                                            <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                            <asp:ListItem Value="11">Novembre</asp:ListItem>
                                            <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                        </asp:DropDownList><br />
                                        <br />
                                    </div>
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Data Registrazione Posticipato :</label>
                                    <asp:TextBox ID="Txt_DataP" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Data Registrazione Anticipato :</label>
                                    <asp:TextBox ID="Txt_DataA" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Data Scadenza :</label>
                                    <asp:TextBox ID="Txt_DataScadenza" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Data Registrazione Ente :</label>
                                    <asp:TextBox ID="Txt_DataEnte" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Data Scadenza Ente :</label>
                                    <asp:TextBox ID="Txt_ScadenzaEnte" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <asp:Button ID="Btn_Aggiorna" runat="server" Text="Aggiorna" /><br />
                                    <br />
                                    <asp:GridView ID="Grd_Registri" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Height="160px" ShowFooter="True" Width="612px">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/annulla.png" EditImageUrl="~/images/modifica.png"
                                                ShowEditButton="True" UpdateImageUrl="~/images/aggiorna.png" />
                                            <asp:TemplateField HeaderText="Registro">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblRegistro" runat="server" Text='<%# Eval("Registro") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblRegistro" runat="server" Text='<%# Eval("Registro") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Protcolllo1">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtProtcollo1" runat="server" Style="text-align: right;" Width="200px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblProtocollo1" Style="text-align: right;" runat="server" Text='<%# Eval("Protocollo1") %>' Width="200px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Protcolllo2">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtProtcollo2" Style="text-align: right;" runat="server" Width="200px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblProtocollo2" Style="text-align: right;" runat="server" Text='<%# Eval("Protocollo2") %>' Width="200px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>


        </div>

        <script src="config.js"></script>
        <script src="main.js"></script>
        <script>
            /* jshint ignore:start */
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-53563471-1', 'auto');
            ga('send', 'pageview');
      /* jshint ignore:end */
        </script>

    </form>
</body>
</html>
