﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_GestioneStanza
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If




        If Page.IsPostBack = False Then

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



            Dim KVilla As New Cls_TabelleDescrittiveOspitiAccessori
            Dim KReparto As New Cls_TabelleDescrittiveOspitiAccessori
            Dim KArredo As New Cls_TabelleDescrittiveOspitiAccessori
            Dim KTipologia As New Cls_TabelleDescrittiveOspitiAccessori

            KVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Villa)

            KReparto.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "REP", Dd_Reparto)

            KArredo.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "TAR", DD_TipoArredo)

            KTipologia.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "TST", DD_Tipologia)

            Dim d As Integer


            d = Val(Request.Item("CODICE"))

            Dim K As New Cls_Stanze

            K.Id = Val(Request.Item("CODICE"))
            K.Leggi(Session("DC_OSPITIACCESSORI"))

            DD_Villa.SelectedValue = K.Villa
            Dd_Reparto.SelectedValue = K.Reparto

            Txt_Piano.Text = K.Piano
            Txt_Stanza.Text = K.Stanza
            Txt_Letto.Text = K.Letto

            DD_Tipologia.SelectedValue = K.Tipologia
            DD_TipoArredo.SelectedValue = K.TipoArredo
            Txt_NumeroBiancheria.Text = K.NumeroBiancheria
            Txt_NumeroTelefono.Text = K.NumeroTelefono

            Call EseguiJS()
        End If
    End Sub








    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click





        Dim K As New Cls_Stanze


        K.Id = Val(Request.Item("CODICE"))

        K.Villa = DD_Villa.SelectedValue
        K.Reparto = Dd_Reparto.SelectedValue

        K.Piano = Txt_Piano.Text
        K.Stanza = Txt_Stanza.Text
        K.Letto = Txt_Letto.Text

        K.Tipologia = DD_Tipologia.SelectedValue
        K.TipoArredo = DD_TipoArredo.SelectedValue
        K.NumeroBiancheria = Txt_NumeroBiancheria.Text
        K.NumeroTelefono = Txt_NumeroTelefono.Text

        K.Utente = Session("UTENTE")

        K.AggiornaDB(Session("DC_OSPITIACCESSORI"))


        Response.Redirect("Elenco_Stanze.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click


        Dim K As New Cls_Stanze



        K.Id = Val(Request.Item("CODICE"))


        K.Elimina(Session("DC_OSPITIACCESSORI"))
        Response.Redirect("Elenco_Stanze.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Stanze.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


End Class
