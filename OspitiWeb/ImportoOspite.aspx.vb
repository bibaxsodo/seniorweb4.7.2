﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class ImportoOspite
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub CaricaPagina()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))




        dd_TipoSconto.SelectedValue = x.TipoSconto
        Txt_ContoEsportazione.Text = x.CONTOPERESATTO

        Txt_CodiceCup.Text = x.CodiceCup

        DD_TipoOperazione.Items.Clear()
        DD_ModalitaPagamento.Items.Clear()
        DD_IVA.Items.Clear()

        Dim DDTipoOpe As New Cls_TipoOperazione
        DDTipoOpe.UpDateDropBoxCentroServizio(ConnectionString, DD_TipoOperazione, Session("CODICESERVIZIO"), "O")

        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(ConnectionString, DD_ModalitaPagamento)

        Dim DDIVA As New Cls_IVA
        DDIVA.UpDateDropBox(ConnectionStringTabelle, DD_IVA)

        DD_TipoOperazione.SelectedValue = x.TIPOOPERAZIONE
        DD_ModalitaPagamento.SelectedValue = x.MODALITAPAGAMENTO
        DD_IVA.SelectedValue = x.CODICEIVA


        Txt_Int.Text = x.IntCliente
        Txt_Abi.Text = x.ABICLIENTE
        Txt_Cab.Text = x.CABCLIENTE
        Txt_Ccbancario.Text = x.CCBANCARIOCLIENTE
        Txt_Cin.Text = x.CINCLIENTE
        Txt_NumCont.Text = x.NumeroControlloCliente
        Txt_BancaCliente.Text = x.BancaCliente

        Txt_ContoEsportazione.Text = x.CONTOPERESATTO


        Txt_CodiceCup.Text = x.CodiceCup

        Txt_ImportoSconto.Text = Format(x.ImportoSconto, "#,##0.00")




        If x.RotturaOspite = 1 Then
            Chk_RotturaOspite.Checked = True
        Else
            Chk_RotturaOspite.Checked = False
        End If

        If x.PERIODO = "M" Then
            RB_Mensile.Checked = True
        End If
        If x.PERIODO = "B" Then
            RB_Bimestrale.Checked = True
        End If
        If x.PERIODO = "T" Then
            RB_Trimestrale.Checked = True
        End If
        If x.PERIODO = "F" Then
            RB_FinePeriodoAss.Checked = True
        End If

        Chk_ExtraFuoriFattura.Visible = False
        If x.Compensazione = "S" Then
            RB_SI.Checked = True
        End If
        If x.Compensazione = "N" Then
            Chk_ExtraFuoriFattura.Visible = True
            RB_NO.Checked = True
        End If
        If x.Compensazione = "D" Then
            RB_Dettaglio.Checked = True
        End If

        If x.ExtraFuoriRetta = 1 Then
            Chk_ExtraFuoriFattura.Checked = True
        Else
            Chk_ExtraFuoriFattura.Checked = False
        End If

        Txt_IntestarioCC.Text = x.IntestatarioCC
        Txt_CodiceFiscaleCC.Text = x.CodiceFiscaleCC

        If x.FattAnticipata = "S" Then
            Chk_Anticipata.Checked = True
        Else
            Chk_Anticipata.Checked = False
        End If

        If x.EmOspiteParente = 0 Then
            Chk_NonEmettere.Checked = False
        Else
            Chk_NonEmettere.Checked = True
        End If

        Dim appoggio As String

        appoggio = x.SETTIMANA & Space(7)

        If Mid(appoggio, 1, 1) = "A" Then
            Chk_Lunedi.Checked = True
        End If
        If Mid(appoggio, 2, 1) = "A" Then
            Chk_Martedi.Checked = True
        End If
        If Mid(appoggio, 3, 1) = "A" Then
            Chk_Mercoledi.Checked = True
        End If
        If Mid(appoggio, 4, 1) = "A" Then
            Chk_Giovedi.Checked = True
        End If
        If Mid(appoggio, 5, 1) = "A" Then
            Chk_Venerdi.Checked = True
        End If
        If Mid(appoggio, 6, 1) = "A" Then
            Chk_Sabato.Checked = True
        End If
        If Mid(appoggio, 7, 1) = "A" Then
            Chk_Domenica.Checked = True
        End If

        Dim Pr As New Cls_ImportoOspite

        Pr.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        Session("Appoggio") = MyTable

        Grd_ImportoOspite.AutoGenerateColumns = False
        'Grd_Classifica.DataSource = MyDataSet
        'Grd_Classifica.DataMember = "tabella"
        Grd_ImportoOspite.DataSource = MyTable

        Call EtichetteImporti()

        Grd_ImportoOspite.DataBind()


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(Session("DC_OSPITE"))

        If KCs.CodiceOspite <> 0 Then
            DD_TipoOperazione.SelectedValue = KCs.TipoOperazione
            DD_IVA.SelectedValue = KCs.AliquotaIva
            DD_ModalitaPagamento.SelectedValue = KCs.ModalitaPagamento
            If KCs.Anticipata = "S" Then
                Chk_Anticipata.Checked = True
            Else
                Chk_Anticipata.Checked = False                
            End If

            If KCs.Compensazione = "S" Then
                RB_SI.Checked = True
                RB_NO.Checked = False
                RB_Dettaglio.Checked = False
            End If
            If KCs.Compensazione = "N" Then
                RB_SI.Checked = False
                RB_NO.Checked = True
                RB_Dettaglio.Checked = False
            End If
            If KCs.Compensazione = "D" Then
                RB_SI.Checked = False
                RB_Dettaglio.Checked = True
                RB_NO.Checked = False
            End If



            appoggio = KCs.Settimana & Space(7)

            Chk_Lunedi.Checked = False
            Chk_Martedi.Checked = False
            Chk_Mercoledi.Checked = False
            Chk_Giovedi.Checked = False
            Chk_Venerdi.Checked = False
            Chk_Sabato.Checked = False
            Chk_Domenica.Checked = False

            If Mid(appoggio, 1, 1) = "A" Then
                Chk_Lunedi.Checked = True
            End If
            If Mid(appoggio, 2, 1) = "A" Then
                Chk_Martedi.Checked = True
            End If
            If Mid(appoggio, 3, 1) = "A" Then
                Chk_Mercoledi.Checked = True
            End If
            If Mid(appoggio, 4, 1) = "A" Then
                Chk_Giovedi.Checked = True
            End If
            If Mid(appoggio, 5, 1) = "A" Then
                Chk_Venerdi.Checked = True
            End If
            If Mid(appoggio, 6, 1) = "A" Then
                Chk_Sabato.Checked = True
            End If
            If Mid(appoggio, 7, 1) = "A" Then
                Chk_Domenica.Checked = True
            End If

            DDTipoAddebito1.SelectedValue = KCs.TipoAddebito1
            DDTipoAddebito2.SelectedValue = KCs.TipoAddebito2
            DDTipoAddebito3.SelectedValue = KCs.TipoAddebito3
            DDTipoAddebito4.SelectedValue = KCs.TipoAddebito4
        End If

        Call EseguiJS()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim MioPar As New Cls_Parametri

        MioPar.LeggiParametri(Session("DC_OSPITE"))




        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If
            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

            Dim Lp As New Cls_Sconto

            Lp.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoSconto, Session("CODICESERVIZIO"))


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



            Dim TipoAddebito As New Cls_Addebito

            TipoAddebito.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DDTipoAddebito1, Session("CODICESERVIZIO"))
            TipoAddebito.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DDTipoAddebito2, Session("CODICESERVIZIO"))
            TipoAddebito.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DDTipoAddebito3, Session("CODICESERVIZIO"))
            TipoAddebito.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DDTipoAddebito4, Session("CODICESERVIZIO"))



            Lbl_Metodo.Text = ""
            If Session("NomeEPersonam") = "MAGIERA" Then
                'Lbl_Metodo.Text = LeggiDatiIbanMetodo(x.CODICEFISCALE) & "<br/>"
            End If

            If MioPar.HideDefaultDiurnoOspite = 1 Or cs.TIPOCENTROSERVIZIO <> "D" Then
                'If Session("ABILITAZIONI").IndexOf("<EPERSONAM-AUT>") > 0 Then
                lbl_GiorniSettimana.Visible = False
                Chk_Lunedi.Visible = False
                Chk_Martedi.Visible = False
                Chk_Mercoledi.Visible = False
                Chk_Giovedi.Visible = False
                Chk_Venerdi.Visible = False
                Chk_Sabato.Visible = False
                Chk_Domenica.Visible = False
            End If


            Dim m As New Cls_Parametri

            m.LeggiParametri(Session("DC_OSPITE"))

            If m.SeparaPeriodoOspiteParente = 1 Then
                DD_TipoOperazione.Enabled = False
            End If

            Call CaricaPagina()
        End If

        If DD_ModalitaPagamento.SelectedValue = "" Then

            DD_ModalitaPagamento.SelectedValue = MioPar.ModalitaPagamento
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function LeggiDatiIbanMetodo(ByVal CodiceFiscale As String) As String

        Dim cnMetodo As OleDbConnection
        Dim DataBaseMetodo As String = ""
        Dim Codice As String = ""

        LeggiDatiIbanMetodo = ""

        If Session("NomeEPersonam") = "MAGIERA" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPMAGANS;User Id=sa;Password=advenias2012;"
        End If



        cnMetodo = New Data.OleDb.OleDbConnection(DataBaseMetodo)

        cnMetodo.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from ANAGRAFICACF  where CODFISCALE = ? order by DataModifica")
        cmd.Connection = cnMetodo
        cmd.Parameters.AddWithValue("@CODFISCALE", CodiceFiscale)        

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("CODCONTO"))
        End If
        myPOSTreader.Close()

        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = ("select * from BANCAAPPCF  where CODCONTO = ? order by DataModifica")
        cmd1.Connection = cnMetodo
        cmd1.Parameters.AddWithValue("@CODCONTO", Codice)

        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            LeggiDatiIbanMetodo = campodb(myPOSTreader1.Item("BANCAAPPOGGIO"))
            LeggiDatiIbanMetodo = LeggiDatiIbanMetodo & " " & campodb(myPOSTreader1.Item("CODICEIBAN"))
        End If
        myPOSTreader1.Close()

        cnMetodo.Close()

    End Function

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0
        myriga(3) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_ImportoOspite.AutoGenerateColumns = False

        Grd_ImportoOspite.DataSource = MyTable

        Call EtichetteImporti()

        Grd_ImportoOspite.DataBind()

        Call EseguiJS()

        Dim TxtData As TextBox = DirectCast(Grd_ImportoOspite.Rows(Grd_ImportoOspite.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)

    End Sub
    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub





    Private Function CercaData(ByVal Data As String) As Boolean
        Dim MyTable As New System.Data.DataTable("tabella")
        Dim i As Long
        MyTable = Session("Appoggio")
        CercaData = False
        For i = 0 To MyTable.Rows.Count - 1
            If MyTable.Rows(i).Item(0).ToString = Data Then
                CercaData = True
                Exit Function
            End If
        Next
    End Function




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If


        If DD_TipoOperazione.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il tipo operazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_IVA.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice iva');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If DD_ModalitaPagamento.SelectedValue = "" Then
            Dim MioPar As New Cls_Parametri

            MioPar.LeggiParametri(Session("DC_OSPITE"))

            DD_ModalitaPagamento.SelectedValue = MioPar.ModalitaPagamento
        End If

        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("DD_Tipo"), DropDownList)


            If Not IsDate(TxtData.Text) And CDbl(TxtImporto.Text) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            For T = 0 To Grd_ImportoOspite.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_ImportoOspite.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next

        Dim Param As New Cls_Parametri


        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.ModalitaPagamentoObligatoria = 1 Then
            If DD_ModalitaPagamento.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Modalità pagamento obbligatoria');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")


        Dim Log As New Cls_LogPrivacy

        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_ImportoOspite

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "RETTEOSPITE", AppoggioJS)


        Dim OldDatiCS As New Cls_DatiOspiteParenteCentroServizio

        OldDatiCS.CodiceOspite = Session("CODICEOSPITE")
        OldDatiCS.CentroServizio = Session("CODICESERVIZIO")
        OldDatiCS.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJSds As String = ConvT.SerializeObject(OldDatiCS)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "DATICS", AppoggioJSds)



        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))




        If Chk_Anticipata.Checked = True Then
            x.FattAnticipata = "S"
        Else
            x.FattAnticipata = ""
        End If

        If RB_Mensile.Checked = True Then
            x.PERIODO = "M"
        End If
        If RB_Bimestrale.Checked = True Then
            x.PERIODO = "B"
        End If
        If RB_Trimestrale.Checked = True Then
            x.PERIODO = "T"
        End If
        If RB_FinePeriodoAss.Checked = True Then
            x.PERIODO = "F"
        End If

        Dim appoggio As String

        x.IntCliente = Txt_Int.Text
        x.ABICLIENTE = Txt_Abi.Text
        x.CABCLIENTE = Txt_Cab.Text
        x.CCBANCARIOCLIENTE = Txt_Ccbancario.Text
        x.CINCLIENTE = Txt_Cin.Text
        x.NumeroControlloCliente = Txt_NumCont.Text
        x.BancaCliente = Txt_BancaCliente.Text

        x.IntestatarioCC = Txt_IntestarioCC.Text
        x.CodiceFiscaleCC = Txt_CodiceFiscaleCC.Text

        x.TIPOOPERAZIONE = DD_TipoOperazione.SelectedValue

        If Chk_RotturaOspite.Checked = True Then
            x.RotturaOspite = 1
        Else
            x.RotturaOspite = 0
        End If

        If RB_SI.Checked = True Then
            x.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            x.Compensazione = "N"
        End If
        If RB_Dettaglio.Checked = True Then
            x.Compensazione = "D"
        End If

        If Chk_ExtraFuoriFattura.Checked = True Then
            x.ExtraFuoriRetta = 1
        Else
            x.ExtraFuoriRetta = 0
        End If

        x.ImportoSconto = Txt_ImportoSconto.Text

        x.TipoSconto = dd_TipoSconto.SelectedValue

        x.CONTOPERESATTO = Txt_ContoEsportazione.Text

        x.CodiceCup = Txt_CodiceCup.Text

        If Chk_NonEmettere.Checked = True Then
            x.EmOspiteParente = 1
        Else
            x.EmOspiteParente = 0
        End If


        appoggio = ""
        If Chk_Lunedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Martedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Mercoledi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Giovedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Venerdi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Sabato.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If

        If Chk_Domenica.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If


        x.SETTIMANA = appoggio


        x.MODALITAPAGAMENTO = DD_ModalitaPagamento.SelectedValue

        x.UTENTE = Session("UTENTE")
        x.DATAAGGIORNAMENTO = Now

        x.ScriviOspite(ConnectionString)


        Dim X1 As New Cls_ImportoOspite

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")
        X1.Utente = Session("UTENTE")


        Call UpDateTable()
        MyTable = ViewState("App_Retta")

        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(Session("DC_OSPITE"))

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.TipoOperazione = DD_TipoOperazione.SelectedValue

        KCs.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue



        KCs.TipoAddebito1 = DDTipoAddebito1.SelectedValue
        KCs.TipoAddebito2 = DDTipoAddebito2.SelectedValue
        KCs.TipoAddebito3 = DDTipoAddebito3.SelectedValue
        KCs.TipoAddebito4 = DDTipoAddebito4.SelectedValue



        KCs.AliquotaIva = DD_IVA.SelectedValue
        If RB_SI.Checked = True Then
            KCs.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            KCs.Compensazione = "N"
        End If
        If RB_Dettaglio.Checked = True Then
            KCs.Compensazione = "D"
        End If

        If Chk_Anticipata.Checked = True Then
            KCs.Anticipata = "S"
        Else
            KCs.Anticipata = ""
        End If




        KCs.Settimana = appoggio
        KCs.Scrivi(Session("DC_OSPITE"))


        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)
        Call EseguiJS()
    End Sub

    Protected Sub Grd_ImportoOspite_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_ImportoOspite.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString

            Dim TxtImporto_2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto_2"), TextBox)

            TxtImporto_2.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString

            Dim TxtImporto1 As TextBox = DirectCast(e.Row.FindControl("TxtImporto1"), TextBox)

            TxtImporto1.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString


            Dim TxtImporto2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto2"), TextBox)

            TxtImporto2.Text = MyTable.Rows(e.Row.RowIndex).Item(5).ToString


            Dim TxtImporto3 As TextBox = DirectCast(e.Row.FindControl("TxtImporto3"), TextBox)

            TxtImporto3.Text = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtImporto4 As TextBox = DirectCast(e.Row.FindControl("TxtImporto4"), TextBox)

            TxtImporto4.Text = MyTable.Rows(e.Row.RowIndex).Item(7).ToString


            Dim DdTipoOperazione As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperazione"), DropDownList)

            Dim m As New Cls_Parametri

            m.LeggiParametri(Session("DC_OSPITE"))

            If m.SeparaPeriodoOspiteParente = 1 Then
                Dim ml As New Cls_TipoOperazione

                'ml.UpDateDropBox(Session("DC_OSPITE"), DdTipoOperazione)
                ml.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DdTipoOperazione, Session("CODICESERVIZIO"), "O")

                DdTipoOperazione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(9).ToString
            End If


            Call EseguiJS()
        End If
    End Sub

    Private Sub UpDateTable()

        

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Importo_2", GetType(String))        
        MyTable.Columns.Add("Importo1", GetType(String))
        MyTable.Columns.Add("Importo2", GetType(String))
        MyTable.Columns.Add("Importo3", GetType(String))
        MyTable.Columns.Add("Importo4", GetType(String))
        MyTable.Columns.Add("MensileFisso", GetType(String))
        MyTable.Columns.Add("TipoOperazione", GetType(String))
        For i = 0 To Grd_ImportoOspite.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("DD_Tipo"), DropDownList)

            Dim TxtImporto_2 As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto_2"), TextBox)
            Dim TxtImporto1 As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto1"), TextBox)
            Dim TxtImporto2 As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto2"), TextBox)
            Dim TxtImporto3 As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto3"), TextBox)
            Dim TxtImporto4 As TextBox = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("TxtImporto4"), TextBox)

            Dim DD_Tipooperazione As DropDownList = DirectCast(Grd_ImportoOspite.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)
            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()

            myrigaR(0) = TxtData.Text
            myrigaR(1) = TxtImporto.Text
            myrigaR(2) = DD_Tipo.SelectedValue

            If DD_Tipo.SelectedValue = "N" Then
                myrigaR(2) = "M"
                myrigaR(8) = 1
            Else
                myrigaR(2) = DD_Tipo.SelectedValue
                myrigaR(8) = 0
            End If


            myrigaR(3) = IIf(TxtImporto_2.Text = "", 0, TxtImporto_2.Text)
            myrigaR(4) = IIf(TxtImporto1.Text = "", 0, TxtImporto1.Text)
            myrigaR(5) = IIf(TxtImporto2.Text = "", 0, TxtImporto2.Text)
            myrigaR(6) = IIf(TxtImporto3.Text = "", 0, TxtImporto3.Text)
            myrigaR(7) = IIf(TxtImporto4.Text = "", 0, TxtImporto4.Text)

            myrigaR(9) = DD_Tipooperazione.SelectedValue

            MyTable.Rows.Add(myrigaR)

        Next

        ViewState("App_Retta") = MyTable
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"        
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ||  (appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    
    Protected Sub Grd_ImportoOspite_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_ImportoOspite.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_ImportoOspite.AutoGenerateColumns = False

        Grd_ImportoOspite.DataSource = MyTable

        Call EtichetteImporti()
        Grd_ImportoOspite.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub


    Private Sub EtichetteImporti()
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AlberghieroAssistenziale = 1 Then
            Grd_ImportoOspite.Columns(4).HeaderText = "Importo Assistenziale"
            Grd_ImportoOspite.Columns(2).HeaderText = "Importo Alberghiero"
        End If
        If Param.SocialeSanitario = 1 Then
            Grd_ImportoOspite.Columns(4).HeaderText = "Importo Sanitario"
            Grd_ImportoOspite.Columns(2).HeaderText = "Importo Sociale"
        End If
        If Param.AlberghieroAssistenziale = 0 And Param.SocialeSanitario = 0 Then
            Grd_ImportoOspite.Columns(4).Visible = False
            Grd_ImportoOspite.Columns(2).HeaderText = "Importo"        
        End If

        Dim m As New Cls_Parametri

        m.LeggiParametri(Session("DC_OSPITE"))

        If m.SeparaPeriodoOspiteParente = 0 Then
            Grd_ImportoOspite.Columns(9).Visible = False
        End If

        Dim k As New Cls_Tabelle

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "01"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoOspite.Columns(5).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoOspite.Columns(5).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "02"
        k.Leggi(Session("DC_OSPITE"))

        Grd_ImportoOspite.Columns(6).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoOspite.Columns(6).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "03"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoOspite.Columns(7).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoOspite.Columns(7).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "04"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoOspite.Columns(8).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoOspite.Columns(8).Visible = False
        End If

    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub DD_TipoOperazione_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_TipoOperazione.SelectedIndexChanged

    End Sub


    Protected Sub DD_TipoOperazioneChange(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim appoggio As String

        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To Grd_ImportoOspite.Rows.Count - 1
            If sender.clientid = Grd_ImportoOspite.Rows(I).Cells(9).Controls.Item(1).ClientID Then
                riga = I
            End If
        Next

        If riga = Grd_ImportoOspite.Rows.Count - 1 Then
            DD_TipoOperazione.SelectedValue = sender.SelectedValue
        End If

    End Sub

    Protected Sub RB_NO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NO.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub

    Protected Sub RB_SI_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_SI.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub

    Protected Sub RB_Dettaglio_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Dettaglio.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub
End Class


