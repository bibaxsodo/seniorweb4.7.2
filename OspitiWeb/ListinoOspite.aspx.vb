﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Collections.Generic
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_ListinoOspite
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MyJS As String

        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        MyJS = "$('#" & Txt_Data.ClientID & "').mask(""99/99/9999"");"
        MyJS = MyJS & "$('#" & Txt_Data.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 2 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Anagrafica", MyJS, True)

        If Page.IsPostBack = True Then Exit Sub
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"


        If Session("EPersonamUser").ToString <> "" And x1.IdEpersonam > 0 Then

            Dim Token As String = ""

            Try
                Token = LoginPersonam(Context)

            Catch ex As Exception

            End Try

            If Token <> "" Then
                Try
                    Dim BuDaUtente As String = Session("EPersonamUser").ToString.Replace("senioruser_", "").Replace("w", "")

                    Dim ePersonamID As Integer = 0
                    If cs.EPersonam > 0 Then
                        ePersonamID = cs.EPersonam
                    Else
                        ePersonamID = cs.EPersonamN
                    End If

                    Lbl_PresaInCarico.Text = "Presa in Carico :" & PresaInCarico(Token, ePersonamID, x1.IdEpersonam, Context) & "<br/>"

                Catch ex As Exception

                End Try
                
            End If
        End If


        Dim k As New Cls_Tabella_Listino


        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, Session("CODICESERVIZIO"), "")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_PrivatoSanitario, Session("CODICESERVIZIO"), "P")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSanitario, Session("CODICESERVIZIO"), "R")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSociale, Session("CODICESERVIZIO"), "C")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoJolly, Session("CODICESERVIZIO"), "J")


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AlberghieroAssistenziale = 1 Then
            LblListinoPrivatoSinatario.Text = "Assistenziale Privato"
        End If

        If DD_PrivatoSanitario.Items.Count <= 1 Then
            DD_PrivatoSanitario.Visible = False
            LblListinoPrivatoSinatario.Visible = False

        End If

        If DD_LisitinoSanitario.Items.Count <= 1 Then
            DD_LisitinoSanitario.Visible = False
            lblListinoSan.Visible = False
        End If
        If DD_LisitinoSociale.Items.Count <= 1 Then
            DD_LisitinoSociale.Visible = False
            lblListinoSociale.Visible = False
        End If
        If DD_LisitinoJolly.Items.Count <= 1 Then
            DD_LisitinoJolly.Visible = False

            lblListinoJolly.Visible = False
        End If

        CaricaPagina()

    End Sub


    Private Function PresaInCarico(ByVal Token As String, ByVal BU As Integer, ByVal IdOspite As Integer, ByVal context As HttpContext) As String
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        ' /api/v1.0/business_units/:business_unit_id/guests/:id_or_cf/periods
        Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BU & "/guests/" & IdOspite & "/periods")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        Dim Descrizione As String = ""



        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Dim Periodi As JArray = JArray.Parse(rawresp)

        For Each jPeriodo As JToken In Periodi


            Try
                Descrizione = jPeriodo.Item("intype").Item("description")
            Catch ex As Exception

            End Try


        Next



        Return Descrizione


    End Function



    Private Sub CaricaPagina()

        Dim ListinoOspite As New Cls_Listino



        ListinoOspite.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        Grd_Retta.AutoGenerateColumns = False
        Grd_Retta.DataSource = MyTable
        Grd_Retta.Font.Size = 10
        Grd_Retta.DataBind()


        Dim I As Integer
        If Grd_Retta.Rows.Count > 1 Then
            For I = 0 To Grd_Retta.Rows.Count - 2
                Dim IB_DELETE As ImageButton = DirectCast(Grd_Retta.Rows(I).FindControl("IB_Delete"), ImageButton)

                IB_DELETE.Enabled = False
            Next
        End If

        Dim m As New Cls_Movimenti

        m.CENTROSERVIZIO = Session("CODICESERVIZIO")
        m.CodiceOspite = Session("CODICEOSPITE")

        m.UltimaDataAccoglimento(Session("DC_OSPITE"))


        Txt_Data.Text = Format(m.Data, "dd/MM/yyyy")
    End Sub

    Protected Sub IB_Genera_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Genera.Click

        If Not IsDate(Txt_Data.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Exit Sub
        End If

        If DD_Lisitino.SelectedValue = "" And DD_LisitinoSanitario.Visible = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il listino');", True)
            Exit Sub
        End If

        If DD_Lisitino.SelectedValue = "" And DD_LisitinoSanitario.Visible = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare almeno il listino privati');", True)
            Exit Sub
        End If

        If Val(Session("CODICEOSPITE")) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Codice ospite non valorizzato');", True)
            Exit Sub
        End If

        Dim Listino As New Cls_Tabella_Listino


        Listino.Codice = DD_Lisitino.SelectedValue
        Listino.LeggiCausale(Session("DC_OSPITE"))

        If Listino.USLDACOMUNERESIDENZA = 1 Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = Session("CODICEOSPITE")
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
            If Ospite.RESIDENZACOMUNE1.Trim = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso assegnare questo listino ad un ospite senza comune residenza');", True)
                Exit Sub
            End If

        End If
        If DD_LisitinoSanitario.SelectedValue <> "" Or DD_LisitinoSociale.SelectedValue <> "" Or DD_LisitinoJolly.SelectedValue <> "" Then
            If Listino.TIPOLISTINO = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non puoi un listino composto con listini sanitario,sociale o jolly');", True)
                Exit Sub
            End If
        End If

        If Listino.MODALITA = "P" Then
            Dim ParImp As New Cls_Parenti

            If ParImp.ParenteIntestatario(Session("DC_OSPITE"), Val(Session("CODICEOSPITE"))) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non indicato parente intestatario');", True)
                Exit Sub
            End If
        End If


        If DD_LisitinoSanitario.SelectedValue <> "" Then

            Dim ListinoSan As New Cls_Tabella_Listino


            ListinoSan.Codice = DD_LisitinoSanitario.SelectedValue
            ListinoSan.LeggiCausale(Session("DC_OSPITE"))

            Dim ImpReg As New Cls_ImportoRegione


            If ListinoSan.USL = "" Then
                ImpReg.UltimaDataSenzaRegione(Session("DC_OSPITE"), ListinoSan.TIPORETTAUSL)
            Else
                ImpReg.UltimaData(Session("DC_OSPITE"), ListinoSan.USL, ListinoSan.TIPORETTAUSL)
            End If

            If ImpReg.Tipo <> Listino.TIPORETTATOTALE Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non puoi un listino composto con listini sanitario,sociale con tipo importo differenti');", True)
                Exit Sub
            End If
        End If

        Dim VerRettaTotale As New Cls_rettatotale


        VerRettaTotale.CENTROSERVIZIO = Session("CODICESERVIZIO")
        VerRettaTotale.CODICEOSPITE = Session("CODICEOSPITE")
        VerRettaTotale.UltimaData(Session("DC_OSPITE"), VerRettaTotale.CODICEOSPITE, VerRettaTotale.CENTROSERVIZIO)

        Dim DataAppoggio As Date = Txt_Data.Text

        If Format(VerRettaTotale.Data, "yyyyMMdd") >= Format(DataAppoggio, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data in retta totale successiva alla data inserita');", True)
            Exit Sub
        End If



        Dim ListinoOspite As New Cls_Listino

        ListinoOspite.InserisciRette(Session("DC_OSPITE"), Session("CODICESERVIZIO"), Session("CODICEOSPITE"), DD_Lisitino.SelectedValue, Txt_Data.Text, DD_PrivatoSanitario.SelectedValue, DD_LisitinoSanitario.SelectedValue, DD_LisitinoSociale.SelectedValue, DD_LisitinoJolly.SelectedValue)

        CaricaPagina()

        DD_Lisitino.SelectedValue = ""
        DD_PrivatoSanitario.SelectedValue = ""
        DD_LisitinoSanitario.SelectedValue = ""
        DD_LisitinoSociale.SelectedValue = ""
        DD_LisitinoJolly.SelectedValue = ""
        DD_PrivatoSanitario.SelectedValue = ""

    End Sub



    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        Dim Data As Date
        Dim DataStringa As String

        DataStringa = Grd_Retta.Rows(e.RowIndex).Cells(0).Text

        If IsDate(DataStringa) Then
            Data = DataStringa

            EliminaListino(Session("DC_OSPITE"), Session("CODICESERVIZIO"), Session("CODICEOSPITE"), Data)


            CaricaPagina()
        End If
    End Sub


    Private Sub EliminaListino(ByVal StringaConnessione As String, ByVal CentroServizio As String, ByVal CodiceOspite As Integer, ByVal Data As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTORETTA Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from EXTRAOSPITE Where CENTROSERVIZIO = '" & CentroServizio & "' And  CODICEOSPITE = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from MODALITA Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from STATOAUTO Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOOSPITE Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOJOLLY Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOCOMUNE Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOPARENTI Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from Listino Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click

    End Sub




    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

End Class
