﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ModificaRetteUtenti.aspx.vb" Inherits="ModificaRetteUtenti" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modifica Rette Utenti</title>
        <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
   <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" /> <link rel="shortcut icon" href="images/SENIOR.ico"/>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
       

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="js/convertinumero.js" type="text/javascript"></script>
    
        <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
	<script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/formatnumer.js" type="text/javascript"></script>  
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>    
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>   
    
             <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
	<script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
        
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
    
        <link rel="stylesheet" href="jqueryui/jquery-ui.css"  type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script> 


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript">
        function DialogBox(Path) {
            alert('s');

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }

        function DialogBoxx(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
            document.documentElement &&
            document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }   


    $(document).ready( function() {
      if (window.innerHeight>0) { $("#BarraLaterale").css("height",(window.innerHeight - 94) + "px"); } else
        { $("#BarraLaterale").css("height",(document.documentElement.offsetHeight - 94) + "px");  }
    });
    </script>     
        <style>
.chosen-container-single .chosen-single
{
	 height:30px;
	 background: white;
	 color:Black;
	   font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 16px;
}
.chosen-container 
{
	   font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 16px;  
  
} 
 .wait  { 
border:solid 1px #2d2d2d;  text-align:center; vertical-align:top; background:white; width:20%; height:180px; top:30%; left:40%;   position:absolute;  -moz-border-radius: 5px;  -webkit-border-radius: 5px; border-radius: 5px;



box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
-moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
-webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
}

     
    #blur
    {

        width: 100%;

        background-color: black;

        moz-opacity: 0.5;

        khtml-opacity: .5;

        opacity: .5;

        filter: alpha(opacity=50);

        z-index: 120;

        height: 100%;

        position: absolute;

        top: 0;

        left: 0;

    }

    #progress

    {

        z-index: 200;

        background-color: White;

        position: absolute;

        top: 0pt;

        left: 0pt;

        border: solid 1px black;

        padding: 5px 5px 5px 5px;

        text-align: center;

    }
</style>
       
</head>
<body>
    <form id="form1" runat="server">    
    <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label> 
    <div align="left">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
    </asp:ScriptManager>
    <table style="width:100%;" cellpadding="0" cellspacing="0">
    <tr>
    <td style="width:160px; background-color:#F0F0F0;"></td>
    <td>
    <div class="Titolo">Ospiti - Strumenti - Modifica Rette Utenti</div>
    <div class="SottoTitolo">
        <br />
        <br />
    </div>
    </td>
    <td style="text-align:right;">
        <div class="DivTasti">     
        <asp:ImageButton ID="Btn_Esegui" runat="server" Height="38px" ImageUrl="~/images/esegui.png"  class="EffettoBottoniTondi"  Width="38px" ToolTip="Modifica" />                      
        <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg"  class="EffettoBottoniTondi"  Width="38px" ToolTip="Modifica" />                      
        </div>
     </td>
    </tr>
   
    <tr>
    <td style="width:160px; background-color:#F0F0F0; vertical-align:top; text-align:center;"  id="BarraLaterale">       
     <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png"  class="Effetto" ToolTip="Chiudi"  />

     <br />          
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
    </td>    
    <td colspan="2" style="background-color: #FFFFFF; vertical-align:top;" > 
     <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"  
            Width="100%" BorderStyle="None" style="margin-right: 39px">               
     <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica"><HeaderTemplate>Addebiti/Accrediti</HeaderTemplate>
     

<ContentTemplate>  
            
      <br />       
       <label class="LabelCampo">Struttura:</label>
       <asp:DropDownList runat="server" ID="DD_Struttura" 
          AutoPostBack="True" ></asp:DropDownList>        
       Centro Servizio :
       <asp:DropDownList ID="DD_CentroServizio" AutoPostBack="True"  runat="server"></asp:DropDownList><br />
       <br />
                    
       
       <label class="LabelCampo">Anno :</label>
       <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>    
       Mese :
       <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
             <asp:ListItem Value="1">Gennaio</asp:ListItem>
             <asp:ListItem Value="2">Febbraio</asp:ListItem>
             <asp:ListItem Value="3">Marzo</asp:ListItem>
             <asp:ListItem Value="4">Aprile</asp:ListItem>
             <asp:ListItem Value="5">Maggio</asp:ListItem>
             <asp:ListItem Value="6">Giugno</asp:ListItem>
             <asp:ListItem Value="7">Luglio</asp:ListItem>
             <asp:ListItem Value="8">Agosto</asp:ListItem>
             <asp:ListItem Value="9">Settembre</asp:ListItem>
             <asp:ListItem Value="10">Ottobre</asp:ListItem>
             <asp:ListItem Value="11">Novembre</asp:ListItem>
             <asp:ListItem Value="12">Dicembre</asp:ListItem>
       </asp:DropDownList>                 
       <br />
         
         
        
        <asp:GridView ID="Grd_AggiornaRette" runat="server" CellPadding="4" Height="60px"  
                ShowFooter="True" BackColor="White"  BorderColor="#6FA7D1" 
                BorderStyle="Dotted" BorderWidth="1px"   >
               <RowStyle ForeColor="#333333" BackColor="White" /><Columns>

                <asp:TemplateField>
                <ItemTemplate>
                <div style="text-align:center"   >
                <asp:ImageButton ID="IB_Delete" CommandName="Delete" Runat="Server" ImageUrl="~/images/cancella.png" />
                </div>
                </ItemTemplate>
                <FooterTemplate>
                <div style="text-align:center"   >
                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci"  runat="server" />
                </div>
                </FooterTemplate>
                </asp:TemplateField>
                
               <asp:TemplateField HeaderText="Ospite">
               <ItemTemplate>
               <asp:TextBox ID="Txt_Ospite" style="text-align:left;" Width="90%" runat="server"></asp:TextBox>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="50%" />
               </asp:TemplateField>  
               
               <asp:TemplateField HeaderText="Regione">
               <ItemTemplate>               
               <asp:DropDownList ID="DD_Regione" style="text-align:right;" onkeypress="return handleEnter(this, event)"  Width="100%" runat="server"></asp:DropDownList>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="800px" />
               </asp:TemplateField>
               
               <asp:TemplateField HeaderText="Tipo Importo Retta">
               <ItemTemplate>               
               <asp:DropDownList ID="DD_TipoImportoRetta" style="text-align:right;" onkeypress="return handleEnter(this, event)"  Width="100%" runat="server"></asp:DropDownList>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="1000px" />
               </asp:TemplateField>
               
               <asp:TemplateField HeaderText="Tipo Retta">
               <ItemTemplate>               
               <asp:DropDownList ID="DD_TipoRetta" style="text-align:right;" onkeypress="return handleEnter(this, event)"  Width="100%" runat="server">
               <asp:ListItem Value="RGP" Text="Presenza"></asp:ListItem>
               <asp:ListItem Value="RGA" Text="Assenza"></asp:ListItem>
               <asp:ListItem Value="ADD" Text="Addebito"></asp:ListItem>
               <asp:ListItem Value="ACC" Text="Accredito"></asp:ListItem>
               </asp:DropDownList>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="700px" />
               </asp:TemplateField>
               
               
               <asp:TemplateField HeaderText="Quantita">
               <ItemTemplate>               
               <asp:TextBox ID="Txt_Quantita" style="text-align:right;" onkeypress="return handleEnter(this, event)"  Width="50%" runat="server"></asp:TextBox>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="500px" />
               </asp:TemplateField>
               
               
                            
               <asp:TemplateField HeaderText="Importo">
               <ItemTemplate>               
               <asp:TextBox ID="Txt_Importo" style="text-align:right;" onkeypress="return handleEnter(this, event)"  Width="50%" runat="server"></asp:TextBox>
               </ItemTemplate>
               <HeaderStyle Font-Bold="False" Font-Italic="False" />
               <ItemStyle Width="900px" />
               </asp:TemplateField>
               
               
               </Columns>         
               <FooterStyle BackColor="White" ForeColor="#023102" />
               <HeaderStyle BackColor="#A6C9E2" Font-Bold="False"  ForeColor="White" BorderColor="#6FA7D1"  BorderWidth="1px"  />
               <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
               <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
               </asp:GridView>
         </ContentTemplate>            
        </xasp:TabPanel>
    </xasp:TabContainer>         
     </td>
     </tr>     
     </table>    
                               
    </div>
    </form>
</body>
</html>

