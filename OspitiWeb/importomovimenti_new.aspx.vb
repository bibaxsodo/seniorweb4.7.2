﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq


Partial Class OspitiWeb_importomovimenti_new
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Private Sub StartImport()
        


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim Token As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Token = LoginPersonam(Context)

        BusinessUnit(Token, Context)
        cn.Close()
    End Sub

    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler




        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0



        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("Cognome", GetType(String))
            Tabella.Columns.Add("Nome", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("Centroservizio", GetType(String))
            Tabella.Columns.Add("DataMovimento", GetType(String))
            Tabella.Columns.Add("Tipo", GetType(String))
            Tabella.Columns.Add("TipoInt", GetType(String))
            Tabella.Columns.Add("Seganlazione", GetType(String))
            Tabella.Columns.Add("IdEpersonam", GetType(Long))
            Tabella.Columns.Add("CodiceOspite", GetType(Long))
            Tabella.Columns.Add("Letto", GetType(String))
            Tabella.Columns.Add("Doppio", GetType(String))
            Tabella.Columns.Add("Causale", GetType(String))

            Tabella.Columns.Add("Ora", GetType(String))

        Catch ex As Exception

        End Try
        Dim jResults As JObject = JObject.Parse(rawresp)


        If Cmb_CServ.SelectedValue = "" Then
            For Each jTok2 As JToken In jResults.Item("business_units").Children



                BusinessUnit = Val(jTok2.Item("id").ToString)

                ImportaMovimenti(BusinessUnit, Token)
                'End If
            Next
        Else
            Dim MCs As New Cls_CentroServizio

            MCs.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            MCs.Leggi(Session("DC_OSPITE"), MCs.CENTROSERVIZIO)


            If MCs.DESCRIZIONE = "CD COSTA ARGENTO" Then
                MCs.EPersonam = 1891
                MCs.EPersonamN = 1891
            End If
            If MCs.DESCRIZIONE = "CD COSTA ARGENTO" Then
                MCs.EPersonam = 1891
                MCs.EPersonamN = 1891
            End If
            If MCs.EPersonam = MCs.EPersonamN Then
                ImportaMovimenti(MCs.EPersonam, Token)
            Else
                ImportaMovimenti(MCs.EPersonamN, Token)
                ImportaMovimenti(MCs.EPersonam, Token)
            End If
        End If



        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Function

    Private Sub ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String)
        Dim request As New NameValueCollection
        Dim rawresp As String
        Dim cn As OleDbConnection




        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessID & "/guests/movements/from/" & DataFatt)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        rawresp = reader.ReadToEnd()

        Dim LastMov As String

        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim SalvaPrecedenteTipo As String = ""
        Dim SalvaPrecedenteData As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim CmdDeleteTemp As New OleDbCommand

        CmdDeleteTemp.Connection = cn
        CmdDeleteTemp.CommandText = "Delete From TempMovimentiEpersonam "
        CmdDeleteTemp.ExecuteNonQuery()


        For Each jTok As JToken In jResults
            rawresp = rawresp & jTok.Item("guests_movements").ToString()
            For Each jTok1 As JToken In jTok.Item("guests_movements").Children
                Dim CodiceFiscale As String
                Dim ward_id As Integer
                Dim Fullname As String


                CodiceFiscale = jTok1.Item("cf").ToString()
                ward_id = jTok1.Item("ward_id").ToString()
                Fullname = jTok1.Item("fullname").ToString()

                Dim Ospite As New ClsOspite



                SalvaPrecedenteTipo = ""

                Ospite.CODICEFISCALE = CodiceFiscale
                Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), CodiceFiscale)

                If CodiceFiscale = "NDRLJA44H60A343O" Then
                    CodiceFiscale = "NDRLJA44H60A343O"
                End If
                REM If Ospite.CodiceOspite > 0 Then
                For Each jTok2 As JToken In jTok1.Item("movements").Children


                    Dim DescrizioneMovimento As String = ""

                    Try
                        DescrizioneMovimento = jTok2.Item("description").ToString.ToUpper
                    Catch ex As Exception

                    End Try

                    Dim conv As String = ""

                    Try
                        conv = jTok2.Item("conv").ToString.ToUpper()
                    Catch ex As Exception

                    End Try


                    Dim bed As String = ""
                    Try
                        bed = jTok2.Item("bed").ToString.ToUpper
                    Catch ex As Exception

                    End Try


                    Dim idEpersonam As String = ""
                    Try
                        idEpersonam = jTok2.Item("id").ToString.ToUpper
                    Catch ex As Exception

                    End Try

                    Dim dataepersonam As String = ""
                    Try
                        dataepersonam = jTok2.Item("date").ToString.ToUpper
                    Catch ex As Exception

                    End Try

                    Dim typeepersonam As String = ""
                    Try
                        typeepersonam = jTok2.Item("type").ToString.ToUpper
                    Catch ex As Exception

                    End Try

                    Dim description As String = ""
                    Try
                        description = jTok2.Item("description").ToString.ToUpper
                    Catch ex As Exception

                    End Try

                    Dim outtype As String = ""
                    Try
                        outtype = jTok2.Item("outtype").ToString.ToUpper
                    Catch ex As Exception

                    End Try


                    Dim MCd As New OleDbCommand

                    MCd.Connection = cn
                    MCd.CommandText = "INSERT INTO TempMovimentiEpersonam ([CodiceFiscale],[ward_id],[conv],[bed],[idepersonam],[dataepersonam],[description],[type],outtype,Cognome)  VALUES (?,?,?,?,?,?,?,?,?,?)"
                    MCd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
                    MCd.Parameters.AddWithValue("@WardId", ward_id)
                    MCd.Parameters.AddWithValue("@conv", conv)
                    MCd.Parameters.AddWithValue("@bed", bed)
                    MCd.Parameters.AddWithValue("@idepersonam", idEpersonam)
                    MCd.Parameters.AddWithValue("@dataepersonam", dataepersonam)
                    MCd.Parameters.AddWithValue("@description", description)
                    MCd.Parameters.AddWithValue("@type", typeepersonam)
                    MCd.Parameters.AddWithValue("@outtype", outtype)
                    MCd.Parameters.AddWithValue("@Cognome", Fullname)
                    MCd.ExecuteNonQuery()
                Next
                REM End If
            Next
        Next



        Dim CmdCleanTemp As New OleDbCommand
        Dim UltimoType As String = ""
        CmdCleanTemp.Connection = cn
        CmdCleanTemp.CommandText = "Select * From TempMovimentiEpersonam Order by CodiceFiscale,dataepersonam,idepersonam"


        Dim ReadCleanTemp As OleDbDataReader = CmdCleanTemp.ExecuteReader
        Do While ReadCleanTemp.Read
            Dim Eliminato As Boolean = False

            If campodb(ReadCleanTemp.Item("CodiceFiscale")) = "NDRLJA44H60A343O" Then
                Eliminato = False
            End If
            If (campodb(ReadCleanTemp.Item("type")) = "9" Or campodb(ReadCleanTemp.Item("type")) = "8") And Eliminato = False Then
                Dim CS As New Cls_CentroServizio
                CS.DESCRIZIONE = ""
                CS.EPersonam = campodb(ReadCleanTemp.Item("ward_id"))

                Dim AppoggioBool As Boolean
                AppoggioBool = True
                Dim Convenzionato As String

                Convenzionato = campodb(ReadCleanTemp.Item("conv"))


                If Convenzionato.ToUpper = "TRUE" Then
                    AppoggioBool = True
                End If
                If Convenzionato.ToUpper = "FALSE" Then
                    AppoggioBool = False
                End If

                CS.LeggiEpersonam(Context.Session("DC_OSPITE"), CS.EPersonam, AppoggioBool)

                If CS.TIPOCENTROSERVIZIO = "D" Then
                    Dim CmdDelTemp As New OleDbCommand

                    CmdDelTemp.Connection = cn
                    CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ?"
                    CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                    CmdDelTemp.ExecuteNonQuery()
                    Eliminato = True
                End If
            End If

            If campodb(ReadCleanTemp.Item("type")) = "12" And Eliminato = False Then
                Dim CmdVerTemp As New OleDbCommand

                CmdVerTemp.Connection = cn
                CmdVerTemp.CommandText = "Select * From TempMovimentiEpersonam Where dataepersonam = ? And codiceFiscale = ? order by dataepersonam,idepersonam "
                CmdVerTemp.Parameters.AddWithValue("@idepersonam", campodb(ReadCleanTemp.Item("dataepersonam")))
                CmdVerTemp.Parameters.AddWithValue("@codiceFiscale", campodb(ReadCleanTemp.Item("codiceFiscale")))
                Dim ReadVerTemp As OleDbDataReader = CmdVerTemp.ExecuteReader
                Do While ReadVerTemp.Read
                    If campodb(ReadVerTemp.Item("type")) = "11" And campodb(ReadVerTemp.Item("codiceFiscale")) = campodb(ReadCleanTemp.Item("CodiceFiscale")) Then
                        Dim DataUscita As String = campodb(ReadCleanTemp.Item("dataepersonam"))
                        Dim DataEntrata As String = campodb(ReadVerTemp.Item("dataepersonam"))

                        Dim VCserv As New Cls_Epersonam

                        Dim TipoCS As New Cls_CentroServizio

                        TipoCS.EPersonam = VCserv.RendiWardid(campodb(ReadVerTemp.Item("ward_id")), Context.Session("DC_OSPITE"))
                        TipoCS.LeggiEpersonam(Session("DC_OSPITE"), TipoCS.EPersonam, campodb(ReadVerTemp.Item("conv")))



                        If TipoCS.EPersonam <> TipoCS.EPersonamN Then
                            If Mid(DataUscita & Space(10), 1, 10) = Mid(DataEntrata & Space(10), 1, 10) And VCserv.RendiWardid(campodb(ReadCleanTemp.Item("ward_id")), Context.Session("DC_OSPITE")) = VCserv.RendiWardid(campodb(ReadVerTemp.Item("ward_id")), Context.Session("DC_OSPITE")) And campodb(ReadCleanTemp.Item("conv")) = campodb(ReadVerTemp.Item("conv")) Then
                                Dim CmdDelTemp As New OleDbCommand

                                CmdDelTemp.Connection = cn
                                CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ? OR idepersonam = ? "
                                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerTemp.Item("idepersonam"))))
                                CmdDelTemp.ExecuteNonQuery()
                                Eliminato = True
                            End If
                        Else
                            If Mid(DataUscita & Space(10), 1, 10) = Mid(DataEntrata & Space(10), 1, 10) And VCserv.RendiWardid(campodb(ReadCleanTemp.Item("ward_id")), Context.Session("DC_OSPITE")) = VCserv.RendiWardid(campodb(ReadVerTemp.Item("ward_id")), Context.Session("DC_OSPITE")) Then
                                Dim CmdDelTemp As New OleDbCommand

                                CmdDelTemp.Connection = cn
                                CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ? OR idepersonam = ? "
                                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerTemp.Item("idepersonam"))))
                                CmdDelTemp.ExecuteNonQuery()
                                Eliminato = True
                            End If
                        End If
                    End If
                Loop
                ReadVerTemp.Close()
            End If

            If campodb(ReadCleanTemp.Item("type")) = "9" And Eliminato = False Then
                Dim CmdVerTemp As New OleDbCommand

                CmdVerTemp.Connection = cn
                CmdVerTemp.CommandText = "Select * From TempMovimentiEpersonam Where dataepersonam > ? And codiceFiscale = ? order by dataepersonam,idepersonam "
                CmdVerTemp.Parameters.AddWithValue("@idepersonam", campodb(ReadCleanTemp.Item("dataepersonam")))
                CmdVerTemp.Parameters.AddWithValue("@codiceFiscale", campodb(ReadCleanTemp.Item("codiceFiscale")))
                Dim ReadVerTemp As OleDbDataReader = CmdVerTemp.ExecuteReader
                Do While ReadVerTemp.Read
                    If campodb(ReadVerTemp.Item("type")) = "8" And campodb(ReadVerTemp.Item("codiceFiscale")) = campodb(ReadCleanTemp.Item("CodiceFiscale")) Then
                        Dim DataUscita As String = campodb(ReadCleanTemp.Item("dataepersonam"))
                        Dim DataEntrata As String = campodb(ReadVerTemp.Item("dataepersonam"))

                        If Mid(DataUscita & Space(10), 1, 10) = Mid(DataEntrata & Space(10), 1, 10) Then
                            Dim CmdDelTemp As New OleDbCommand

                            CmdDelTemp.Connection = cn
                            CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ? OR idepersonam = ? "
                            CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                            CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerTemp.Item("idepersonam"))))
                            CmdDelTemp.ExecuteNonQuery()
                            Eliminato = True
                        End If
                    End If
                Loop
                ReadVerTemp.Close()
            End If

            If campodb(ReadCleanTemp.Item("description")) = "CHANGE_TUTOR_BU_SOAS" Or campodb(ReadCleanTemp.Item("description")) = "CHANGE_TUTOR_BU" Or campodb(ReadCleanTemp.Item("description")) = "CHANGE_DOCTOR_BU" Then
                Dim CmdDelTemp As New OleDbCommand

                CmdDelTemp.Connection = cn
                CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ?"
                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                CmdDelTemp.ExecuteNonQuery()
            End If

            '(campodb(ReadTemp.Item("description")).ToUpper = "CHANGE_BED" Or campodb(ReadTemp.Item("description")).ToUpper = "NEW_BED") Then
            If campodb(ReadCleanTemp.Item("type")) = "8" And Not (campodb(ReadCleanTemp.Item("description")).ToUpper = "ENTER" Or campodb(ReadCleanTemp.Item("description")).ToUpper = "CHANGE_BED" Or campodb(ReadCleanTemp.Item("description")).ToUpper = "NEW_BED") Then
                Dim CmdDelTemp As New OleDbCommand

                CmdDelTemp.Connection = cn
                CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ?"
                CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                CmdDelTemp.ExecuteNonQuery()
            End If

            If campodb(ReadCleanTemp.Item("type")) = "8" And campodb(ReadCleanTemp.Item("description")).ToUpper = "NEW_UNIT" And Eliminato = False Then

                Dim Ospite As New ClsOspite


                Ospite.CODICEFISCALE = campodb(ReadCleanTemp.Item("CodiceFiscale"))
                Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), Ospite.CODICEFISCALE)
                Dim Convenzionato As String

                Convenzionato = campodb(ReadCleanTemp.Item("conv"))



                Dim RagWardID As New Cls_Epersonam

                Dim MovUltAccogl As New Cls_Movimenti

                MovUltAccogl.CodiceOspite = Ospite.CodiceOspite
                MovUltAccogl.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                Dim CservUltAccogl As New Cls_CentroServizio

                CservUltAccogl.CENTROSERVIZIO = MovUltAccogl.CENTROSERVIZIO
                CservUltAccogl.Leggi(Session("DC_OSPITE"), CservUltAccogl.CENTROSERVIZIO)

                Dim MovUltCserv As New Cls_Movimenti

                MovUltCserv.CENTROSERVIZIO = MovUltAccogl.CENTROSERVIZIO
                MovUltCserv.CodiceOspite = Ospite.CodiceOspite
                MovUltCserv.UltimaDataCserv(Session("DC_OSPITE"), Ospite.CodiceOspite, MovUltAccogl.CENTROSERVIZIO)
                Dim EpersonamDaVerifica As Integer


                EpersonamDaVerifica = CservUltAccogl.EPersonam
                If Convenzionato = "TRUE" Then
                    EpersonamDaVerifica = CservUltAccogl.EPersonam
                Else
                    EpersonamDaVerifica = CservUltAccogl.EPersonamN
                End If


                If EpersonamDaVerifica <> RagWardID.RendiWardid(Val(campodb(ReadCleanTemp.Item("ward_id"))), Context.Session("DC_OSPITE")) And MovUltCserv.TipoMov <> "13" Then
                    Dim CmdModTemp As New OleDbCommand

                    CmdModTemp.Connection = cn
                    CmdModTemp.CommandText = "UPDATE TempMovimentiEpersonam SET type  =  11,CONV ='' Where idepersonam = ?"
                    CmdModTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                    CmdModTemp.ExecuteNonQuery()

                    Dim MCd As New OleDbCommand

                    MCd.Connection = cn
                    MCd.CommandText = "INSERT INTO TempMovimentiEpersonam ([CodiceFiscale],[ward_id],[conv],[bed],[idepersonam],[dataepersonam],[description],[type],outtype,Cognome)  VALUES (?,?,?,?,?,?,?,?,?,?)"
                    MCd.Parameters.AddWithValue("@CodiceFiscale", campodb(ReadCleanTemp.Item("CodiceFiscale")))
                    MCd.Parameters.AddWithValue("@WardId", CservUltAccogl.EPersonam)
                    MCd.Parameters.AddWithValue("@conv", campodb(ReadCleanTemp.Item("conv")))
                    MCd.Parameters.AddWithValue("@bed", campodb(ReadCleanTemp.Item("bed")))
                    MCd.Parameters.AddWithValue("@idepersonam", campodb(ReadCleanTemp.Item("idepersonam")))
                    Dim DataAppoggio As Date

                    DataAppoggio = campodb(ReadCleanTemp.Item("dataepersonam"))

                    MCd.Parameters.AddWithValue("@dataepersonam", Format(DataAppoggio.AddDays(-1), "dd/MM/yyyy"))
                    MCd.Parameters.AddWithValue("@description", campodb(ReadCleanTemp.Item("description")))
                    MCd.Parameters.AddWithValue("@type", 12)
                    MCd.Parameters.AddWithValue("@outtype", campodb(ReadCleanTemp.Item("outtype")))
                    MCd.Parameters.AddWithValue("@cognome", campodb(ReadCleanTemp.Item("Cognome")))
                    MCd.ExecuteNonQuery()
                Else
                    Dim CmdDelTemp As New OleDbCommand

                    CmdDelTemp.Connection = cn
                    CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ?"
                    CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))))
                    CmdDelTemp.ExecuteNonQuery()
                    Eliminato = True
                End If
            End If
            If campodb(ReadCleanTemp.Item("type")) = "11" And Eliminato = False Then
                Dim CmdVerTemp As New OleDbCommand

                CmdVerTemp.Connection = cn
                CmdVerTemp.CommandText = "Select * From TempMovimentiEpersonam Where idepersonam = ? "
                CmdVerTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))) + 1)
                Dim ReadVerTemp As OleDbDataReader = CmdVerTemp.ExecuteReader
                If ReadVerTemp.Read Then
                    If campodb(ReadVerTemp.Item("type")) = "8" Then
                        Dim CmdDelTemp As New OleDbCommand

                        CmdDelTemp.Connection = cn
                        CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET CONV ='DELETE' Where idepersonam = ?"
                        CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadCleanTemp.Item("idepersonam"))) + 1)
                        CmdDelTemp.ExecuteNonQuery()
                    End If
                End If
                ReadVerTemp.Close()

            End If

            If Eliminato = True Then
                UltimoType = ""
            Else
                UltimoType = campodb(ReadCleanTemp.Item("type"))
            End If
        Loop

        ReadCleanTemp.Close()


        If Param.NonUsciteTemporanee = 1 Then
            Dim CmdDelete_NonUsciteTemporanee As New OleDbCommand

            CmdDelete_NonUsciteTemporanee.Connection = cn
            CmdDelete_NonUsciteTemporanee.CommandText = "delete from TempMovimentiEpersonam Where type = 8 or type = 9 "
            CmdDelete_NonUsciteTemporanee.ExecuteNonQuery()
        End If



        Dim CmdDelete_DefinitivaTemp As New OleDbCommand

        CmdDelete_DefinitivaTemp.Connection = cn
        CmdDelete_DefinitivaTemp.CommandText = "delete from TempMovimentiEpersonam Where CONV ='DELETE' "
        CmdDelete_DefinitivaTemp.ExecuteNonQuery()



        Dim CmdTempMov As New OleDbCommand

        CmdTempMov.Connection = cn
        CmdTempMov.CommandText = "Select CodiceFiscale From TempMovimentiEpersonam group by CodiceFiscale"

        Dim ReadTempMOV As OleDbDataReader = CmdTempMov.ExecuteReader
        Do While ReadTempMOV.Read

            Dim MCosp As New ClsOspite

            MCosp.CODICEFISCALE = campodb(ReadTempMOV.Item("CodiceFiscale"))
            MCosp.LeggiPerCodiceFiscale(Session("DC_OSPITE"), MCosp.CODICEFISCALE)

            If MCosp.CODICEFISCALE = "GNNCHR93P42L833E" Then
                MCosp.CODICEFISCALE = "GNNCHR93P42L833E"
            End If

            If MCosp.CodiceOspite > 0 Then

                Dim CmdMov As New OleDbCommand

                CmdMov.Connection = cn
                CmdMov.CommandText = "Select * From Movimenti Where CodiceOSpite = ?"
                CmdMov.Parameters.AddWithValue("@CodiceOspite", MCosp.CodiceOspite)

                Dim ReadMovTemp As OleDbDataReader = CmdMov.ExecuteReader
                Do While ReadMovTemp.Read

                    Dim CentroServizio As New Cls_CentroServizio

                    CentroServizio.CENTROSERVIZIO = campodb(ReadMovTemp.Item("CENTROSERVIZIO"))
                    CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)


                    Dim MCd As New OleDbCommand

                    MCd.Connection = cn
                    MCd.CommandText = "INSERT INTO TempMovimentiEpersonam ([CodiceFiscale],[ward_id],[conv],[bed],[idepersonam],[dataepersonam],[description],[type],outtype,Cognome)  VALUES (?,?,?,?,?,?,?,?,?,?)"
                    MCd.Parameters.AddWithValue("@CodiceFiscale", MCosp.CODICEFISCALE)
                    MCd.Parameters.AddWithValue("@WardId", CentroServizio.EPersonam)
                    MCd.Parameters.AddWithValue("@conv", "DELETE")
                    MCd.Parameters.AddWithValue("@bed", "")
                    MCd.Parameters.AddWithValue("@idepersonam", campodb(ReadMovTemp.Item("EPersonam")))

                    MCd.Parameters.AddWithValue("@dataepersonam", Format(ReadMovTemp.Item("DATA"), "dd/MM/yyyy"))
                    MCd.Parameters.AddWithValue("@description", campodb(ReadMovTemp.Item("descrizione")))

                    Dim Tipo As String = ""
                    If campodb(ReadMovTemp.Item("TIPOMOV")) = "05" Then
                        Tipo = "11"
                    End If
                    If campodb(ReadMovTemp.Item("TIPOMOV")) = "03" Then
                        Tipo = "9"
                    End If
                    If campodb(ReadMovTemp.Item("TIPOMOV")) = "04" Then
                        Tipo = "8"
                    End If

                    If campodb(ReadMovTemp.Item("TIPOMOV")) = "13" Then
                        Tipo = "12"
                    End If

                    MCd.Parameters.AddWithValue("@type", Tipo)
                    MCd.Parameters.AddWithValue("@outtype", "")
                    MCd.Parameters.AddWithValue("@cognome", MCosp.Nome)
                    MCd.ExecuteNonQuery()
                Loop
                ReadMovTemp.Close()
            End If
        Loop
        ReadTempMOV.Close()


        Dim CmdVerificaIntegrita As New OleDbCommand
        Dim LastCodiceFiscale As String = ""
        Dim Lastward_id As String = ""
        Dim NonCoerente As Boolean = False
        CmdVerificaIntegrita.Connection = cn

        CmdVerificaIntegrita.CommandText = "Select * From TempMovimentiEpersonam Where (([description] <> 'NEW_BED') AND ([description] <> 'CHANGE_BED')) Order by ward_id,CodiceFiscale,dataepersonam,[idepersonam]"

        Dim ReadVerificaIntegrita As OleDbDataReader = CmdVerificaIntegrita.ExecuteReader
        Do While ReadVerificaIntegrita.Read
            If LastCodiceFiscale <> campodb(ReadVerificaIntegrita.Item("CodiceFiscale")) Or Lastward_id <> campodb(ReadVerificaIntegrita.Item("ward_id")) Then
                NonCoerente = False
            End If


            If LastCodiceFiscale = campodb(ReadVerificaIntegrita.Item("CodiceFiscale")) And Lastward_id = campodb(ReadVerificaIntegrita.Item("ward_id")) Then
                If NonCoerente = True Or (LastMov = campodb(ReadVerificaIntegrita.Item("type")) Or (LastMov = "12" And (campodb(ReadVerificaIntegrita.Item("type")) = "8" Or campodb(ReadVerificaIntegrita.Item("type")) = "9"))) Then
                    If Val(campodb(ReadVerificaIntegrita.Item("idepersonam"))) > 1 Then
                        Dim Presente As Boolean = False
                        If campodb(ReadVerificaIntegrita.Item("type")) = "11" Then

                            Dim OspiteV As New ClsOspite


                            OspiteV.CODICEFISCALE = campodb(ReadVerificaIntegrita.Item("CodiceFiscale"))
                            OspiteV.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), OspiteV.CODICEFISCALE)


                            Dim CmdVerMovimenti_EPersonam As New OleDbCommand

                            CmdVerMovimenti_EPersonam.Connection = cn
                            CmdVerMovimenti_EPersonam.CommandText = ("select * from Movimenti_EPersonam Where (CodiceOspite = " & OspiteV.CodiceOspite & " And Data = ? And [TipoMovimento] = 'A') or  IdEpersonam = ? ")
                            CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@Data", Mid(campodb(ReadVerificaIntegrita.Item("dataepersonam")), 1, 10))
                            CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerificaIntegrita.Item("idepersonam"))))
                            Dim ReadVerMovimenti_EPersonam As OleDbDataReader = CmdVerMovimenti_EPersonam.ExecuteReader
                            If ReadVerMovimenti_EPersonam.Read Then
                                Presente = True
                            End If
                            ReadVerMovimenti_EPersonam.Close()
                        Else
                            Dim CmdVerMovimenti_EPersonam As New OleDbCommand

                            CmdVerMovimenti_EPersonam.Connection = cn
                            CmdVerMovimenti_EPersonam.CommandText = "Select * From Movimenti_EPersonam Where [IdEpersonam] = ?"
                            CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerificaIntegrita.Item("idepersonam"))))
                            Dim ReadVerMovimenti_EPersonam As OleDbDataReader = CmdVerMovimenti_EPersonam.ExecuteReader
                            If ReadVerMovimenti_EPersonam.Read Then
                                Presente = True
                            End If
                            ReadVerMovimenti_EPersonam.Close()
                        End If

                        If Presente = False Then
                            Dim CmdDelTemp As New OleDbCommand

                            CmdDelTemp.Connection = cn
                            CmdDelTemp.CommandText = "UPDATE TempMovimentiEpersonam SET [outtype] ='ERRATO' Where idepersonam = ?"
                            CmdDelTemp.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadVerificaIntegrita.Item("idepersonam"))))
                            CmdDelTemp.ExecuteNonQuery()
                            NonCoerente = True
                        End If
                    End If
                End If
            End If

            LastCodiceFiscale = campodb(ReadVerificaIntegrita.Item("CodiceFiscale"))
            LastMov = campodb(ReadVerificaIntegrita.Item("type"))
            Lastward_id = campodb(ReadVerificaIntegrita.Item("ward_id"))
            If LastCodiceFiscale = "DNIRNA48R31B455C" Then
                LastCodiceFiscale = "DNIRNA48R31B455C"
            End If
        Loop
        ReadVerificaIntegrita.Close()


        Dim CmdDelete_DefinitivaTemp2 As New OleDbCommand

        CmdDelete_DefinitivaTemp2.Connection = cn
        CmdDelete_DefinitivaTemp2.CommandText = "delete from TempMovimentiEpersonam Where CONV ='DELETE' "
        CmdDelete_DefinitivaTemp2.ExecuteNonQuery()




        Dim CmdTemp As New OleDbCommand

        CmdTemp.Connection = cn
        CmdTemp.CommandText = "Select * From TempMovimentiEpersonam Order by Cognome"

        Dim ReadTemp As OleDbDataReader = CmdTemp.ExecuteReader
        Do While ReadTemp.Read

            Dim Ospite As New ClsOspite


            Ospite.CODICEFISCALE = campodb(ReadTemp.Item("CodiceFiscale"))
            Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), Ospite.CODICEFISCALE)

            If Ospite.CODICEFISCALE = "PRSNNA30C51Z110F" Then
                Ospite.CODICEFISCALE = "PRSNNA30C51Z110F"
            End If

            Dim SK As New Cls_Epersonam
            Dim CS As New Cls_CentroServizio
            CS.DESCRIZIONE = ""
            CS.EPersonam = SK.RendiWardid(campodb(ReadTemp.Item("ward_id")), Context.Session("DC_OSPITE"))

            Dim AppoggioBool As Boolean
            AppoggioBool = True
            Dim Convenzionato As String

            Convenzionato = campodb(ReadTemp.Item("conv"))


            If Convenzionato.ToUpper = "TRUE" Then
                AppoggioBool = True
            End If
            If Convenzionato.ToUpper = "FALSE" Then
                AppoggioBool = False
            End If
            CS.CENTROSERVIZIO = ""

            CS.LeggiEpersonam(Context.Session("DC_OSPITE"), CS.EPersonam, AppoggioBool)


            If (Cmb_CServ.SelectedValue = CS.CENTROSERVIZIO And Cmb_CServ.SelectedValue <> "") Or (Cmb_CServ.SelectedValue = "" And Chk_IncludiMovimentiSoloSe.Checked = False) Or (Chk_IncludiMovimentiSoloSe.Checked = True And CS.CENTROSERVIZIO <> "") Then

                Dim Tipo As String
                Dim Letto As String

                If campodb(ReadTemp.Item("type")) = "11" Then
                    Tipo = "05"
                End If
                If campodb(ReadTemp.Item("type")) = "9" Then
                    Tipo = "03"
                End If
                If campodb(ReadTemp.Item("type")) = "8" Then
                    Tipo = "04"
                End If


                If campodb(ReadTemp.Item("type")) = "8" And (campodb(ReadTemp.Item("description")).ToUpper = "CHANGE_BED" Or campodb(ReadTemp.Item("description")).ToUpper = "NEW_BED") Then
                    Tipo = "99"
                    Letto = campodb(ReadTemp.Item("bed"))
                End If

                If campodb(ReadTemp.Item("type")) = "12" Then
                    Tipo = "13"
                End If


                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Ospite.CognomeOspite

                If Ospite.NonInUso = "S" Then
                    myriga(1) = Ospite.NomeOspite & " (OSPITE NON IN USO ATTIVARLO)"
                Else
                    myriga(1) = Ospite.NomeOspite
                End If
                If Ospite.Nome = "" Then
                    myriga(1) = campodb(ReadTemp.Item("Cognome"))
                End If


                myriga(2) = Ospite.CODICEFISCALE


                myriga(3) = CS.CENTROSERVIZIO
                myriga(4) = Mid(campodb(ReadTemp.Item("dataepersonam")), 1, 10)

                If Tipo = "05" Then
                    myriga(5) = "Accoglimento"
                End If
                If Tipo = "13" Then
                    myriga(5) = "Uscita Definitiva"
                End If
                If Tipo = "03" Then
                    myriga(5) = "Uscita Temporanea"
                End If
                If Tipo = "04" Then
                    myriga(5) = "Entrate"
                End If
                If Tipo = "99" Then
                    myriga(5) = "Cambio Letto"
                End If


                myriga(6) = Tipo
                myriga(7) = ""
                myriga(8) = campodb(ReadTemp.Item("idepersonam"))
                myriga(9) = Ospite.CodiceOspite
                myriga(10) = campodb(ReadTemp.Item("bed"))
                myriga(11) = ""

                myriga(13) = campodb(ReadTemp.Item("dataepersonam"))


                If campodb(ReadTemp.Item("type")) = "11" Then

                    Dim CmdVerMovimenti_EPersonam As New OleDbCommand

                    CmdVerMovimenti_EPersonam.Connection = cn
                    CmdVerMovimenti_EPersonam.CommandText = ("select * from Movimenti_EPersonam Where (CodiceOspite = " & Ospite.CodiceOspite & " And Data = ? And [TipoMovimento] = 'A') or  IdEpersonam = ? ")
                    CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@Data", Mid(campodb(ReadTemp.Item("dataepersonam")), 1, 10))
                    CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadTemp.Item("idepersonam"))))
                    Dim ReadVerMovimenti_EPersonam As OleDbDataReader = CmdVerMovimenti_EPersonam.ExecuteReader
                    If ReadVerMovimenti_EPersonam.Read Then
                        myriga(7) = "ERROREPRE"
                    End If
                    ReadVerMovimenti_EPersonam.Close()
                Else
                    Dim CmdVerMovimenti_EPersonam As New OleDbCommand

                    CmdVerMovimenti_EPersonam.Connection = cn
                    CmdVerMovimenti_EPersonam.CommandText = "Select * From Movimenti_EPersonam Where [IdEpersonam] = ?"
                    CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@idepersonam", Val(campodb(ReadTemp.Item("idepersonam"))))
                    Dim ReadVerMovimenti_EPersonam As OleDbDataReader = CmdVerMovimenti_EPersonam.ExecuteReader
                    If ReadVerMovimenti_EPersonam.Read Then
                        myriga(7) = "ERROREPRE"
                    End If
                    ReadVerMovimenti_EPersonam.Close()
                End If


                If Ospite.Nome = "" Then
                    myriga(7) = "ERRORE"
                End If
                If campodb(ReadTemp.Item("outtype")) = "ERRATO" Then
                    myriga(7) = "ERRORECS"
                End If

                myriga(12) = "" ' causale    

                If Tipo = "03" Or Tipo = "13" Then
                    Dim CmdVerTemp As New OleDbCommand

                    CmdVerTemp.Connection = cn
                    CmdVerTemp.CommandText = "Select * From EPersonam Where CausaleEPersonam like ? And (CentroServizio = ? Or CentroServizio IS null or CentroServizio ='')"
                    CmdVerTemp.Parameters.AddWithValue("@idepersonam", campodb(ReadTemp.Item("outtype")))
                    If IsNothing(CS.CENTROSERVIZIO) Then
                        CmdVerTemp.Parameters.AddWithValue("@CentroServizio", "")
                    Else
                        CmdVerTemp.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
                    End If
                    Dim ReadVerTemp As OleDbDataReader = CmdVerTemp.ExecuteReader
                    If ReadVerTemp.Read Then
                        myriga(12) = campodb(ReadVerTemp.Item("CausaleOspiti"))
                    End If
                    ReadVerTemp.Close()
                End If


                Tabella.Rows.Add(myriga)
            End If
        Loop
        cn.Close()





    End Sub


    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Function campodbN(ByVal oggetto As Object) As Integer
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try

            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE"))            
            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String


            If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
                request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

                guarda = context.Session("ChiaveCr")
            Else
                request.Add("username", context.Session("EPersonamUser"))

                guarda = context.Session("EPersonamPSWCRYPT")
            End If


            request.Add("code", guarda)
            REM request.Add("password", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)
            REM Dim result As Object = client.UploadValues("http://api-v1.e-personam.com/v0/oauth/token", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()


            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)
            Dim DDCausale As DropDownList = DirectCast(e.Row.FindControl("DDCausale"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)

            Dim Causale As New Cls_CausaliEntrataUscita


            Causale.UpDateDropBox(Session("DC_OSPITE"), "99", DDCausale)
            DDCausale.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(12).ToString


            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERRORE" Then
                DD_Cserv.BackColor = Drawing.Color.Red
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Red

            End If




            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERRORECS" Then
                DD_Cserv.BackColor = Drawing.Color.Orange
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                REM CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Orange

            End If

            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "NONIMP" Then
                DD_Cserv.BackColor = Drawing.Color.Green
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Green

            End If

            If Tabella.Rows(e.Row.RowIndex).Item(5).ToString = "Cambio Letto" Then
                DD_Cserv.BackColor = Drawing.Color.Green
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = True

                e.Row.BackColor = Drawing.Color.Green

            End If
            If Tabella.Rows(e.Row.RowIndex).Item(2).ToString = "TSSDNC36D54B499N" Then
                Dim i As Integer
                i = 0
            End If


            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERROREPRE" Then
                DD_Cserv.BackColor = Drawing.Color.Aqua
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)


                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua

            End If



            If Tabella.Rows(e.Row.RowIndex).Item(11).ToString = "S" Then
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)
                CheckBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Call StartImport()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.Item("UTENTE") <> "" And Request.Item("PASSWORD") <> "" Then
            Dim k As New Cls_Login

            k.Utente = Request.Item("UTENTE")
            k.Chiave = Request.Item("PASSWORD")
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            If k.Ospiti <> "" Then
                Session("UTENTE") = k.Utente
            End If

            k.Utente = UCase(k.Utente)
            k.Chiave = k.Chiave

            k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
            k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr

            Session("DC_OSPITE") = k.Ospiti
            Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
            Session("DC_TABELLE") = k.TABELLE
            Session("DC_GENERALE") = k.Generale
            Session("DC_TURNI") = k.Turni
            Session("STAMPEOSPITI") = k.STAMPEOSPITI
            Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
            Session("ProgressBar") = ""
            Session("CODICEREGISTRO") = ""
            Session("NomeEPersonam") = k.NomeEPersonam
            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = k.Utente.ToLower

        End If

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If



        End If
    End Sub
    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

    End Sub
    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer
        Dim MySql As String


        Btn_Modifica.Enabled = False
        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Context.Session("DC_OSPITE"))

        cn.Open()


        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)
            Dim DDCausale As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DDCausale"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                If DD_Cserv.SelectedValue = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi importare i movimenti senza cserv');", True)
                    Exit Sub
                End If
            End If

            If CheckBox.Checked = True Then
                Dim Progressivo As Long


                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                cmdIns.Connection = cn
                cmdIns.Parameters.AddWithValue("@IdEpersonam", Val(Tabella.Rows(Riga).Item(8)))
                cmdIns.Parameters.AddWithValue("@CodiceFiscale", Tabella.Rows(Riga).Item(2))
                cmdIns.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                cmdIns.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(9)))
                cmdIns.Parameters.AddWithValue("@Data", Tabella.Rows(Riga).Item(4))
                cmdIns.Parameters.AddWithValue("@TipoMovimento", "M")
                cmdIns.Parameters.AddWithValue("@Descrizione", "")
                cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                cmdIns.ExecuteNonQuery()


                If Tabella.Rows(Riga).Item(6) = "99" Then

                    Dim MovDim As New Cls_MovimentiStanze
                    Dim MyData As Date = DateSerial(Val(Mid(Tabella.Rows(Riga).Item(4), 7, 4)), Val(Mid(Tabella.Rows(Riga).Item(4), 4, 2)), Val(Mid(Tabella.Rows(Riga).Item(4), 1, 2)))

                    MovDim.Villa = ""
                    MovDim.CentroServizio = DD_Cserv.SelectedValue
                    MovDim.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                    MovDim.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
                    If MovDim.Villa <> "" Then
                        MovDim.Id = 0
                        MovDim.Data = MyData.AddDays(-1)
                        MovDim.Tipologia = "LI"
                        MovDim.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                    End If

                    Dim MovLetto As New Cls_MovimentiStanze
                    Dim Letto As String = Tabella.Rows(Riga).Item(10)


                    MovLetto.CentroServizio = DD_Cserv.SelectedValue
                    MovLetto.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                    MovLetto.Data = MyData
                    MovLetto.Tipologia = "OC"
                    MovLetto.Villa = "01"
                    If Mid(Letto, 1, 1) = "1" Then
                        MovLetto.Reparto = "01"
                    End If
                    If Mid(Letto, 1, 1) = "2" Then
                        MovLetto.Reparto = "02"
                    End If
                    If Mid(Letto, 1, 1) = "3" Then
                        MovLetto.Reparto = "03"
                    End If
                    If Mid(Letto, 1, 1) = "4" Then
                        MovLetto.Reparto = "04"
                    End If
                    MovLetto.Piano = ""
                    MovLetto.Stanza = ""
                    MovLetto.Letto = Letto
                    MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))

                Else




                    Dim cmd1 As New OleDbCommand()
                    cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & DD_Cserv.SelectedValue & "' And  CodiceOspite = " & Val(Tabella.Rows(Riga).Item(9)))
                    cmd1.Connection = cn
                    Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                    If myPOSTreader1.Read Then
                        If Not IsDBNull(myPOSTreader1.Item(0)) Then
                            Progressivo = myPOSTreader1.Item(0) + 1
                        Else
                            Progressivo = 1
                        End If
                    Else
                        Progressivo = 1
                    End If



                    Dim MyData As Date = DateSerial(Val(Mid(Tabella.Rows(Riga).Item(4), 7, 4)), Val(Mid(Tabella.Rows(Riga).Item(4), 4, 2)), Val(Mid(Tabella.Rows(Riga).Item(4), 1, 2)))

                    MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam,DataOra) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)

                    cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
                    cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                    cmdw.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                    cmdw.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(9)))
                    cmdw.Parameters.AddWithValue("@Data", DateSerial(Year(MyData), Month(MyData), Day(MyData)))
                    cmdw.Parameters.AddWithValue("@TIPOMOV", Tabella.Rows(Riga).Item(6))
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)                    
                    cmdw.Parameters.AddWithValue("@CAUSALE", DDCausale.SelectedValue)
                    cmdw.Parameters.AddWithValue("@Descrizione", "")
                    cmdw.Parameters.AddWithValue("@EPersonam", 1)
                    cmdw.Parameters.AddWithValue("@DataOra", Tabella.Rows(Riga).Item(13))
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()

                    Dim Letto As String = Tabella.Rows(Riga).Item(10)

                    If Letto <> "" And (Tabella.Rows(Riga).Item(6) = "13" Or Tabella.Rows(Riga).Item(6) = "05") Then
                        Dim MovLetto As New Cls_MovimentiStanze

                        MovLetto.CentroServizio = DD_Cserv.SelectedValue
                        MovLetto.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        MovLetto.Data = MyData
                        If Tabella.Rows(Riga).Item(6) = "13" Then
                            MovLetto.Tipologia = "LI"
                        Else
                            MovLetto.Tipologia = "OC"
                        End If
                        MovLetto.Villa = "01"
                        If Mid(Letto, 1, 1) = "1" Then
                            MovLetto.Reparto = "01"
                        End If
                        If Mid(Letto, 1, 1) = "2" Then
                            MovLetto.Reparto = "02"
                        End If
                        If Mid(Letto, 1, 1) = "3" Then
                            MovLetto.Reparto = "03"
                        End If
                        If Mid(Letto, 1, 1) = "4" Then
                            MovLetto.Reparto = "04"
                        End If
                        MovLetto.Piano = ""
                        MovLetto.Stanza = ""
                        MovLetto.Letto = Letto
                        MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                    End If

                    If Tabella.Rows(Riga).Item(6) = "05" Then

                        Dim Cserv As New Cls_CentroServizio

                        Cserv.CENTROSERVIZIO = DD_Cserv.SelectedValue
                        Cserv.Leggi(Session("DC_OSPITE"), DD_Cserv.SelectedValue)
                        Dim s As New Cls_Pianodeiconti

                        s.Descrizione = ""
                        s.Mastro = Cserv.MASTRO
                        s.Conto = Cserv.CONTO
                        s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100
                        s.Decodfica(Session("DC_GENERALE"))
                        If s.Descrizione = "" Then
                            Dim xOsp As New ClsOspite

                            xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                            xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)))
                            s.Descrizione = xOsp.Nome
                            s.Tipo = "A"
                            s.TipoAnagrafica = "O"
                            s.Scrivi(Session("DC_GENERALE"))
                        End If

                        Dim KPar As New Cls_Parametri

                        KPar.LeggiParametri(Session("DC_OSPITE"))

                        If KPar.MastroAnticipo > 0 Then
                            s.Descrizione = ""
                            s.Mastro = KPar.MastroAnticipo
                            s.Conto = Cserv.CONTO
                            s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100
                            s.Decodfica(Session("DC_GENERALE"))
                            If s.Descrizione = "" Then
                                Dim xOsp As New ClsOspite

                                xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)))
                                s.Descrizione = xOsp.Nome
                                s.Tipo = "P"
                                s.TipoAnagrafica = "O"
                                s.Scrivi(Session("DC_GENERALE"))
                            End If
                        End If

                        Dim Par As Integer
                        Dim OldCserv As New Cls_CentroServizio

                        OldCserv.CENTROSERVIZIO = DD_Cserv.SelectedValue
                        OldCserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

                        For Par = 1 To 10
                            Dim sPar As New Cls_Pianodeiconti
                            Dim Parente As New Cls_Parenti

                            Parente.Nome = ""
                            Parente.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                            Parente.CodiceParente = Par
                            Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                            If Parente.Nome <> "" Then
                                sPar.Descrizione = ""
                                sPar.Mastro = Cserv.MASTRO
                                sPar.Conto = Cserv.CONTO
                                sPar.Sottoconto = (Val(Tabella.Rows(Riga).Item(9)) * 100) + Par
                                sPar.Decodfica(Session("DC_GENERALE"))
                                If sPar.Descrizione = "" Then
                                    Dim xOsp As New Cls_Parenti


                                    xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                    xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)), Par)
                                    sPar.Tipo = "A"
                                    sPar.TipoAnagrafica = "P"
                                    sPar.Descrizione = xOsp.Nome
                                    sPar.Scrivi(Session("DC_GENERALE"))
                                End If
                                If KPar.MastroAnticipo > 0 Then
                                    s.Descrizione = ""
                                    s.Mastro = KPar.MastroAnticipo
                                    s.Conto = Cserv.CONTO
                                    s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100 + Par
                                    s.Decodfica(Session("DC_GENERALE"))
                                    If s.Descrizione = "" Then
                                        Dim xOsp As New Cls_Parenti

                                        xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                        xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)), Par)
                                        s.Descrizione = xOsp.Nome
                                        s.Tipo = "A"
                                        s.TipoAnagrafica = "P"
                                        s.Scrivi(Session("DC_GENERALE"))
                                    End If
                                End If

                            End If
                        Next
                    End If

                    If Tabella.Rows(Riga).Item(6) = "13" Then


                        Dim MovDim As New Cls_MovimentiStanze

                        MovDim.CentroServizio = DD_Cserv.SelectedValue
                        MovDim.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        MovDim.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
                        If MovDim.Id > 0 Then
                            MovDim.Id = 0
                            MovDim.Data = MyData
                            MovDim.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                        End If
                    End If
                End If
            End If
        Next

        Dim ErroriNonCongruita As String = ""
        Dim AnagOspite As New ClsOspite

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)            
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim CmdVerifica As New OleDbCommand
                Dim VerificaUltimoTipo As String = ""
                Dim VerificaUltimaData As Date = Nothing
                CmdVerifica.CommandText = "Select  * From Movimenti Where CentroServizio = ? And CodiceOspite = ? Order by Data,Progressivo"
                CmdVerifica.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                CmdVerifica.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(9)))
                CmdVerifica.Connection = cn

                Dim RdVerifica As OleDbDataReader = CmdVerifica.ExecuteReader()
                Do While RdVerifica.Read
                    If VerificaUltimoTipo = "" And campodb(RdVerifica.Item("TIPOMOV")) <> "05" Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        'ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Il primo movimento non è un accoglimento</td></tr>"
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If
                    If campodb(RdVerifica.Item("TIPOMOV")) = "05" Then
                        If Not (VerificaUltimoTipo = "" Or VerificaUltimoTipo = "13") Then
                            AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                            AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                            ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                        End If
                    End If


                    If VerificaUltimoTipo = "04" And campodb(RdVerifica.Item("TIPOMOV")) = "05" Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        'ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Non può esserci un accoglimento dopo un entrata</td></tr>"
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If

                    If VerificaUltimoTipo = "13" And campodb(RdVerifica.Item("TIPOMOV")) = "03" Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        'ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Non può esserci un uscita temporanea dopo un uscita definitiva</td></tr>"
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If

                    If VerificaUltimoTipo = "13" And campodb(RdVerifica.Item("TIPOMOV")) = "04" Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If



                    If VerificaUltimoTipo = campodb(RdVerifica.Item("TIPOMOV")) Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        'ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Non possono esserci movimenti dello stesso tipo</td></tr>"
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If


                    If VerificaUltimaData = campodb(RdVerifica.Item("DATA")) Then
                        AnagOspite.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)
                        'ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Errore non vi posso essere due movimenti con la stessa data</td></tr>"
                        ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & DD_Cserv.SelectedValue & "</td><td>" & Val(Tabella.Rows(Riga).Item(9)) & "</td><td>" & AnagOspite.Nome & "</td><td>Verificare i movimenti per questo ospite</td></tr>"
                    End If

                    VerificaUltimoTipo = campodb(RdVerifica.Item("TIPOMOV"))
                    VerificaUltimaData = campodb(RdVerifica.Item("DATA"))
                Loop
                RdVerifica.Close()
            End If

        Next


        cn.Close()
        If ErroriNonCongruita = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Movimenti Importati');", True)
        End If

        Call Btn_Movimenti_Click(sender, e)

        Btn_Modifica.Enabled = True
        If ErroriNonCongruita <> "" Then
            Session("ERRORIIMPORT") = ErroriNonCongruita




            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxx('ReportErroriImport.aspx');  });", True)
        End If

    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True And GridView1.Rows(Riga).BackColor <> System.Drawing.Color.Orange Then
                CheckBox.Checked = True
                If GridView1.Rows(Riga).Cells(5).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                    CheckBox.Checked = True
                End If
            End If
        Next        
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Epersonam.aspx")
    End Sub
End Class
