﻿
Partial Class contabilita
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Label1.Text = ""
        lbl_container.Text = "<h1>Gestione Contabilità Web<br/></h1><table><tr><td><img src=images\qdocumenti.jpg></td><td bgcolor=#808080></td><td valign=top align=right>Gestione Documenti<br />Prima Nota<br />Incassi Pagamenti<br />Bilancio<br /></td><tr><td bgcolor=#C0C0C0></td><td><img src=images\qincassipagamenti.jpg></td><td bgcolor=#808080></td></tr><tr><td valign=bottom>Sodo Informatica S.r.l.<br/><a href=http://www.sodo.it>www.sodo.it</a></td><td bgcolor=#C0C0C0></td><td><img src=images\qmastrini.jpg></td></tr></table>"

        If Request.Item("TIPO") = "PrimaNota" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PrimaNota"" data-role=""button"" data-theme=""a""  rel=""external"">Prima Nota</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PrimaNota"" data-role=""button"" rel=""external"">Prima Nota</a> "
        End If
        If Request.Item("TIPO") = "Documenti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Documenti"" data-role=""button"" data-theme=""a""  rel=""external"">Documenti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Documenti"" data-role=""button"" rel=""external"">Documenti</a> "
        End If
        If Request.Item("TIPO") = "IncassiPagamenti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncassiPagamenti"" data-role=""button"" data-theme=""a""  rel=""external"">Incassi/Pagamenti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncassiPagamenti"" data-role=""button"" rel=""external"">Incassi/Pagamenti</a> "
        End If
        If Request.Item("TIPO") = "PianoConti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PianoConti"" data-role=""button"" data-theme=""a""  rel=""external"">Piano Conti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PianoConti"" data-role=""button"" rel=""external"">Piano Conti</a> "
        End If
        If Request.Item("TIPO") = "Anagrafica" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Anagrafica"" data-role=""button"" data-theme=""a""  rel=""external"">Anagrafica Cli/For</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Anagrafica"" data-role=""button"" rel=""external"">Anagrafica Cli/For</a> "
        End If
        If Request.Item("TIPO") = "Ricerca" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Ricerca"" data-role=""button"" data-theme=""a""  rel=""external"">Ricerca</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Ricerca"" data-role=""button"" rel=""external"">Ricerca</a> "
        End If
    End Sub
End Class
