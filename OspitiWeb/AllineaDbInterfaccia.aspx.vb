﻿
Partial Class OspitiWeb_AllineaDbInterfaccia
    Inherits System.Web.UI.Page

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.ApiV1 = 1 Then
            Response.Redirect("ApiV1Epersonam/allineaospitiparentiv1.aspx")
        Else
            If Chk_SoloVerifica.Checked = True Then
                Response.Redirect("AllineaOspitiParenti.aspx?SOLOVERIFICA=SI")
            Else
                Response.Redirect("AllineaOspitiParenti.aspx?SOLOVERIFICA=NO")
            End If
        End If

        
    End Sub

    Protected Sub OspitiWeb_AllineaDbInterfaccia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("UsciteSicurezza".ToUpper) > 0 Then
            Lbl_NomeOspite.Text = "<b>1</b>"
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Epersonam.aspx")
    End Sub
End Class
