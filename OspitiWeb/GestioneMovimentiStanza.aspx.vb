﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_GestioneMovimentiStanza
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"




        If Page.IsPostBack = False Then

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



            Dim KVilla As New Cls_TabelleDescrittiveOspitiAccessori
            Dim KReparto As New Cls_TabelleDescrittiveOspitiAccessori

            KVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Villa)

            KReparto.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "REP", Dd_Reparto)

            Dim d As Integer


            d = Val(Request.Item("CODICE"))

            Dim K As New Cls_MovimentiStanze


            K.CodiceOspite = Session("CODICEOSPITE")
            K.CentroServizio = Session("CODICESERVIZIO")
            K.Id = Val(Request.Item("CODICE"))

            If Request.Item("TIPO") = "LIBERAZIONE" Then
                K.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
            Else
                K.Leggi(Session("DC_OSPITIACCESSORI"))
            End If

            If Val(Request.Item("CODICE")) > 0 Or Request.Item("TIPO") = "LIBERAZIONE" Then


                DD_Villa.SelectedValue = K.Villa
                Dd_Reparto.SelectedValue = K.Reparto

                Txt_Piano.Text = K.Piano
                Txt_Stanza.Text = K.Stanza
                Txt_Letto.Text = K.Letto

                If K.Tipologia = "OC" Then
                    DD_Occupazione.Checked = True
                    DD_Liberazione.Checked = False
                Else
                    DD_Occupazione.Checked = False
                    DD_Liberazione.Checked = True
                End If
                If Year(K.Data) > 1910 Then
                    Txt_Data.Text = Format(K.Data, "dd/MM/yyyy")
                Else
                    Txt_Data.Text = Format(Now, "dd/MM/yyyy")
                End If
                If Request.Item("TIPO") = "LIBERAZIONE" Then
                    Txt_Data.Text = Format(Now, "dd/MM/yyyy")
                    DD_Occupazione.Checked = False
                    DD_Liberazione.Checked = True

                End If
            Else
                Txt_Data.Text = Format(Now, "dd/MM/yyyy")
                DD_Occupazione.Checked = True
                DD_Liberazione.Checked = False
            End If

            Call EseguiJS()
        End If
    End Sub








    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Not IsDate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If


        Dim s As New Cls_Stanze

        s.Villa = DD_Villa.SelectedValue
        s.Reparto = Dd_Reparto.SelectedValue
        s.Piano = Txt_Piano.Text
        s.Stanza = Txt_Stanza.Text
        s.Letto = Txt_Letto.Text
        s.Id = 0
        s.LeggiLetto(Session("DC_OSPITIACCESSORI"))
        If s.Id = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Stanza non presente, non posso procedere');", True)
            Exit Sub
        End If

        Dim K As New Cls_MovimentiStanze



        K.CodiceOspite = Session("CODICEOSPITE")
        K.CentroServizio = Session("CODICESERVIZIO")
        K.Id = Val(Request.Item("CODICE"))        

        K.Villa = DD_Villa.SelectedValue
        K.Reparto = Dd_Reparto.SelectedValue

        K.Piano = Txt_Piano.Text
        K.Stanza = Txt_Stanza.Text
        K.Letto = Txt_Letto.Text

        If DD_Occupazione.Checked = True Then
            K.Tipologia = "OC"
        Else
            K.Tipologia = "LI"            
        End If
        K.Data = Txt_Data.Text
        K.Utente = Session("UTENTE")

        K.AggiornaDB(Session("DC_OSPITIACCESSORI"))


        Response.Redirect("ElencoMovimentiStanze.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click


        Dim K As New Cls_MovimentiStanze



        K.CodiceOspite = Session("CODICEOSPITE")
        K.CentroServizio = Session("CODICESERVIZIO")
        K.Id = Val(Request.Item("CODICE"))


        K.Elimina(Session("DC_OSPITIACCESSORI"))
        Response.Redirect("ElencoMovimentiStanze.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoMovimentiStanze.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"


        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
