﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="TabelleRegioni" EnableEventValidation="false" CodeFile="TabelleRegioni.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Tabella Regione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function DialogBox(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }

    </script>
    <style>
        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(0, 0, 0);
            display: block;
            height: 455px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }

        .cornicegrigia2 {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(0, 0, 0);
            display: block;
            height: 325px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Regione</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Btn_Duplica"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server"
                                OnClientClick="return window.confirm('Eliminare?');"
                                ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina"
                                ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <label class="MenuDestra"><a href="#" onclick="DialogBox('../GeneraleWeb/PianoConti.aspx');">Piano Conti</a></label>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Codice :</label>
                                                <asp:TextBox ID="Txt_Codice" MaxLength="4" AutoPostBack="true" runat="server" Width="50px"></asp:TextBox>
                                                <asp:Image ID="Img_VerCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                                <asp:CheckBox ID="Chk_NonInUso" runat="server" Text="Non In Uso" />
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Descrizione :</label>
                                                <asp:TextBox ID="Txt_Descrizione" AutoPostBack="true" runat="server" Width="352px" MaxLength="50"></asp:TextBox>
                                                <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                                &nbsp;Nome In Visualizzazione&nbsp;
        <asp:TextBox ID="Txt_NomeConiuge" runat="server" Width="200px" MaxLength="50" AutoPostBack="true"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">All' attenzione di :</label>
                                                <asp:TextBox ID="Txt_Attenzione" runat="server" Width="320px" MaxLength="40"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Indirizzo :</label>
                                                <asp:TextBox ID="Txt_Indirizzo" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Cap :</label>
                                                <asp:TextBox ID="Txt_Cap" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="112px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Comune :</label>
                                                <asp:TextBox ID="Txt_ComRes" runat="server" Width="300px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Partita IVA :</label>
                                                <asp:TextBox ID="Txt_Piva" MaxLength="11" onkeypress="return soloNumeri(event);" runat="server" Width="120px" AutoPostBack="true"></asp:TextBox>
                                                <asp:Image ID="Img_VerPIVA" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                                Codice Fiscale :
       <asp:TextBox ID="Txt_CodiceFiscale" MaxLength="16" runat="server" Width="112px"></asp:TextBox>
                                                <br />
                                                <br />


                                                <label class="LabelCampo">Mail :</label>
                                                <asp:TextBox ID="TxT_Mail" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />

                                                <br />
                                                <label class="LabelCampo">Referente :</label>
                                                <asp:TextBox ID="Txt_Referente" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Telefono :</label>
                                                <asp:TextBox ID="Txt_TelefonoReferente" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>


                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia2">

                                                <br />
                                                <label class="LabelCampo">IBAN: Int</label>
                                                <asp:TextBox ID="Txt_Int" MaxLength="2" runat="server" Width="24px"></asp:TextBox>
                                                Num Cont
       <asp:TextBox ID="Txt_NumCont" MaxLength="2" onkeypress="return soloNumeri(event);" runat="server" Width="32px"></asp:TextBox>
                                                Cin
       <asp:TextBox ID="Txt_Cin" MaxLength="1" runat="server" Width="40px"></asp:TextBox>
                                                Abi
       <asp:TextBox ID="Txt_Abi" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="48px"></asp:TextBox>
                                                Cab
       <asp:TextBox ID="Txt_Cab" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="40px"></asp:TextBox>
                                                C/C Bancario&nbsp;
       <asp:TextBox ID="Txt_Ccbancario" MaxLength="12" runat="server" Width="328px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Banca Cliente:</label>
                                                <asp:TextBox ID="Txt_BancaCliente" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">CIG :</label>
                                                <asp:TextBox ID="Txt_BancaCIG" runat="server" Width="104px" MaxLength="11"></asp:TextBox>
                                                Cup :
       <asp:TextBox ID="Txt_Cup" runat="server" Width="104px" MaxLength="11"></asp:TextBox>
                                                Codice Destinatario :
       <asp:TextBox ID="Txt_CodiceDestinatario" runat="server" Width="104px" MaxLength="11"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">IdDocumento :</label>
                                                <asp:TextBox ID="Txt_IdDocumento" runat="server" Width="104px" MaxLength="20"></asp:TextBox>
                                                Data Id Documento
       <asp:TextBox ID="Txt_DataIdDocumento" runat="server" Width="104px" MaxLength="20"></asp:TextBox>
                                                Riferimento Amministrazione :
       <asp:TextBox ID="Txt_RiferimentoAmministrazione" runat="server" Width="104px" MaxLength="20"></asp:TextBox>

                                                Numero Fattura DDT :
       <asp:CheckBox ID="Chk_NFDtt" runat="server" Text=""></asp:CheckBox><br />
                                                <br />
                                                <label class="LabelCampo">Note FE:</label>
                                                <asp:TextBox ID="Txt_NoteFe" runat="server" Width="304px" MaxLength="100"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Tipo XML. :</label>
                                                <asp:DropDownList ID="DD_EGO" runat="server">
                                                    <asp:ListItem Value="C" Text="Contratto"></asp:ListItem>
                                                    <asp:ListItem Value="Z" Text="Convenzione"></asp:ListItem>
                                                    <asp:ListItem Value="O" Text="Ordine"></asp:ListItem>
                                                    <asp:ListItem Value="N" Text="Non Indicato"></asp:ListItem>
                                                </asp:DropDownList>
                                                Numero Item :
       <asp:TextBox ID="Txt_NumItem" runat="server" Width="204px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Com. Convezione :</label>
                                                <asp:TextBox ID="Txt_CodiceCommessaConvezione" runat="server" Width="104px" MaxLength="50"></asp:TextBox>
                                                PEC :
       <asp:TextBox ID="Txt_PEC" runat="server" Width="304px" MaxLength="150"></asp:TextBox><br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </xasp:TabPanel>



                            <xasp:TabPanel runat="server" HeaderText="FE.XML" ID="TabPanel3">
                                <HeaderTemplate>
                                    FE.XML
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo" style="margin-left: 50px;">Configurazione :</label>
                                    <asp:CheckBox ID="Chk_Personalizzare" runat="server" AutoPostBack="true" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Raggruppa Ricavi:</label>
                                    <asp:CheckBox ID="Chk_FE_RaggruppaRicavi" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Mastro Partita:</label>
                                    <asp:CheckBox ID="Chk_FE_IndicaMastroPartita" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Competenza:</label>
                                    <asp:CheckBox ID="Chk_FE_Competenza" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Centro Servizio:</label>
                                    <asp:CheckBox ID="Chk_FE_CentroServizio" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Giorni in Des.:</label>
                                    <asp:CheckBox ID="Chk_FE_GiorniInDescrizione" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Tipo Retta:</label>
                                    <asp:CheckBox ID="Chk_FE_TipoRetta" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Solo Iniziali Ospite:</label>
                                    <asp:CheckBox ID="Chk_FE_SoloIniziali" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo" style="margin-left: 50px;">Note Fatture:</label>
                                    <asp:CheckBox ID="Chk_FE_NoteFatture" runat="server" /><br />
                                    <br />


                                    <label class="LabelCampo" style="margin-left: 50px;">Usa Solo Descrizione Riga Fattura :</label>
                                    <asp:CheckBox ID="Chk_SoloDescrizioneRigaFattura" runat="server" /><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>



                            <xasp:TabPanel runat="server" HeaderText="Importi" ID="TabPanel1">
                                <HeaderTemplate>Importi</HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Tipo Operazione :</label>
                                    <asp:DropDownList ID="DD_TipoOperazione" runat="server" Width="248px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">IVA :</label>
                                    <asp:DropDownList ID="DD_IVA" runat="server" Width="176px"></asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">Modalità Pagamento:</label>
                                    <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="248px"></asp:DropDownList>
                                    Periodo
       <asp:RadioButton ID="RB_Mensile" runat="server" AutoPostBack="true" GroupName="Periodo" Text="M" />
                                    <asp:RadioButton ID="RB_Bimestrale" runat="server" AutoPostBack="true" GroupName="Periodo" Text="B" />
                                    <asp:RadioButton ID="RB_Trimestrale" runat="server" AutoPostBack="true" GroupName="Periodo" Text="T" />
                                    <asp:RadioButton ID="RB_TreMesiD" runat="server" AutoPostBack="true" GroupName="Periodo" Text="-3M" />
                                    <asp:RadioButton ID="RB_NonFatturare" runat="server" AutoPostBack="true" GroupName="Periodo" Text="Non Fatturare" />

                                    <asp:RadioButton ID="RB_FatturaDa" runat="server" GroupName="Periodo" Text="Fatturare Da" AutoPostBack="true" />
                                    Anno
                                    <asp:TextBox ID="TxtAnno" runat="server" Text="" Enabled="false" BackColor="Gray" Width="60px"></asp:TextBox>
                                    Mese
                                    <asp:TextBox ID="TxtMese" runat="server" Text="" Enabled="false" BackColor="Gray" Width="60px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Compensazione :</label>
                                    <asp:RadioButton ID="RB_SI" runat="server" GroupName="compesazione" Text="SI" />
                                    <asp:RadioButton ID="RB_NO" runat="server" GroupName="compesazione" Text="NO" />
                                    <asp:RadioButton ID="RB_Dettaglio" runat="server" GroupName="compesazione" Text="Dettaglio" /><br />
                                    <br />
                                    <label class="LabelCampo">Raggruppato :</label>
                                    <asp:DropDownList ID="DD_Raggruppato" runat="server" Width="300px">
                                    </asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Conto Contabilità : </label>
                                    <asp:TextBox ID="Txt_Sottoconto" runat="server" Width="300px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Conto Esportazione : </label>
                                    <asp:TextBox ID="Txt_Esportazione" runat="server" Width="300px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Rott. Centro Servizio :</label>
                                    <asp:CheckBox ID="Chk_RotturaCserv" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Rott. Ospite :</label>
                                    <asp:CheckBox ID="Chk_RotturaOspite" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Non includere importi a zero :</label>
                                    <asp:CheckBox ID="Chk_ImportiAzero" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Sconto : </label>
                                    <asp:TextBox ID="Txt_Sconto" runat="server" Width="300px"></asp:TextBox><br />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Raggruppa in Export :</label>
                                    <asp:RadioButton ID="Rb_NonRagruppare" runat="server" Text="Non Raggruppare" GroupName="RAGGRUPPA" />
                                    <asp:RadioButton ID="Rb_GiorniInDesc" runat="server" Text="Giorni in descrizione" GroupName="RAGGRUPPA" />
                                    <asp:RadioButton ID="Rb_GiorniInQty" runat="server" Text="Giorni in Quantita" GroupName="RAGGRUPPA" />
                                    (coll. Alyante) (coll. Alyante)
         <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Raggruppa in Elaborazione :</label>
                                    <asp:CheckBox ID="Chk_RaggruppaInElab" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <br />

                                    <asp:GridView ID="Grd_Importo" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Data">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Importo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tipo">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Tipo" runat="server">
                                                        <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                        <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                        <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tipo Retta">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoRetta" runat="server">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>

                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>


                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Importi" ID="TabPanel2">
                                <HeaderTemplate>Eccezioni</HeaderTemplate>
                                <ContentTemplate>


                                    <asp:GridView ID="Grd_Eccezioni" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Servizio">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_CentroSerivizio" runat="server">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tipo Oparazione">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoOperazione" runat="server">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="IVA">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_IVA" runat="server">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>




                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </xasp:TabPanel>


                            <xasp:TabPanel runat="server" HeaderText="Importi" ID="TabPanel4">
                                <HeaderTemplate>Comuni Residenza</HeaderTemplate>
                                <ContentTemplate>


                                    <asp:GridView ID="Grd_Comuni" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Comune">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtComune" Width="300px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </xasp:TabPanel>

                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
