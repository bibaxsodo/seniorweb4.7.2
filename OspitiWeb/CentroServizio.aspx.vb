﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports Microsoft.VisualBasic
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class CentroServizio
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()





    Private Sub DecodificaCodiceEpersonam()
        Dim Token As String

        Token = LoginPersonam(Context)
        Dim Url As String
        Dim rawresp As String

        Url = "https://api.e-personam.com/api/v1.0/business_units"

        Dim client As HttpWebRequest = WebRequest.Create(Url)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing



        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        rawresp = reader.ReadToEnd()



        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim IndiceRighe As Integer = 0
        Dim VtTipo(1000) As String
        Dim VtTipoCodice(1000) As String
        Dim VtPadre(1000) As Integer



        For Each jTok1 As JToken In jResults

            If jTok1.Item("structure").ToString.ToUpper = "true".ToUpper Then
                Dim Cerca As Integer
                Dim trovato As Boolean = False

                For Cerca = 0 To IndiceRighe - 1
                    If VtTipoCodice(Cerca) = jTok1.Item("id") Then
                        trovato = True
                    End If
                Next

                If Not trovato Then
                    IndiceRighe = IndiceRighe + 1
                    VtTipo(IndiceRighe) = jTok1.Item("name").ToString
                    VtTipoCodice(IndiceRighe) = jTok1.Item("id")
                    VtPadre(IndiceRighe) = 0
                End If
            End If


            If Not IsNothing(jTok1.Item("substructures")) Then
                For Each jTok2 As JToken In jTok1.Item("substructures")

                    Dim Cerca As Integer
                    Dim trovato As Boolean = False

                    For Cerca = 0 To IndiceRighe - 1
                        If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                            VtPadre(Cerca) = jTok1.Item("id")
                            trovato = True
                        End If
                    Next
                    If Not trovato Then
                        IndiceRighe = IndiceRighe + 1
                        VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                        VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                        VtPadre(IndiceRighe) = jTok1.Item("id")
                    End If

                    If Not IsNothing(jTok2.Item("units")) Then
                        For Each jTok3 As JToken In jTok2.Item("units")
                            '5214
                            If Val(jTok3.Item("id").ToString) = 5214 Then
                                trovato = False

                            End If

                            trovato = False
                            For Cerca = 0 To IndiceRighe - 1
                                If VtTipoCodice(Cerca) = jTok3.Item("id") Then
                                    VtPadre(Cerca) = jTok3.Item("id")
                                    trovato = True
                                End If
                            Next
                            If Not trovato Then
                                IndiceRighe = IndiceRighe + 1
                                VtTipo(IndiceRighe) = jTok3.Item("name").ToString
                                VtTipoCodice(IndiceRighe) = jTok3.Item("id")
                                VtPadre(IndiceRighe) = jTok2.Item("id")
                            End If

                        Next
                    End If
                Next
            Else
                If Not IsNothing(jTok1.Item("units")) Then
                    For Each jTok2 As JToken In jTok1.Item("units")

                        Dim Cerca As Integer
                        Dim trovato As Boolean = False

                        For Cerca = 0 To IndiceRighe - 1
                            If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                                VtPadre(Cerca) = jTok1.Item("id")
                                trovato = True
                            End If
                        Next
                        If Not trovato Then
                            IndiceRighe = IndiceRighe + 1
                            VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                            VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                            VtPadre(IndiceRighe) = jTok1.Item("id")
                        End If
                    Next
                End If
            End If
        Next

        Dim Ordina As Integer
        Dim OrdinaX As Integer

        For Ordina = 1 To IndiceRighe
            If VtPadre(Ordina) = 0 Then
                DD_EPersonam.Items.Add(VtTipo(Ordina))
                DD_EPersonam.Items(DD_EPersonam.Items.Count - 1).Value = VtTipoCodice(Ordina)
                Dim Indicex As Integer

                For Indicex = 0 To IndiceRighe
                    If VtPadre(Indicex) = VtTipoCodice(Ordina) Then
                        DD_EPersonam.Items.Add("---" & VtTipo(Indicex))
                        DD_EPersonam.Items(DD_EPersonam.Items.Count - 1).Value = VtTipoCodice(Indicex)
                        Dim Indicey As Integer

                        For Indicey = 0 To IndiceRighe
                            If VtPadre(Indicey) = VtTipoCodice(Indicex) Then
                                DD_EPersonam.Items.Add("------" & VtTipo(Indicey))
                                DD_EPersonam.Items(DD_EPersonam.Items.Count - 1).Value = VtTipoCodice(Indicey)

                                Dim Indicez As Integer

                                For Indicez = 0 To IndiceRighe
                                    If VtPadre(Indicez) = VtTipoCodice(Indicey) Then
                                        DD_EPersonam.Items.Add("---------" & VtTipo(Indicez))
                                        DD_EPersonam.Items(DD_EPersonam.Items.Count - 1).Value = VtTipoCodice(Indicez)

                                    End If
                                Next

                            End If
                        Next
                    End If
                Next

            End If

        Next


        DD_EPersonam.SelectedValue = Txt_EPersonam.Text


        For Ordina = 0 To DD_EPersonam.Items.Count - 1
            DD_EPersonamN.Items.Add(DD_EPersonam.Items(Ordina))
        Next

        DD_EPersonamN.SelectedValue = Txt_EPersonamNon.Text
    End Sub

    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Private Sub Pulisci()
        Txt_Codice.Text = ""
        Txt_Descrizione.Text = ""
        Txt_Mastro.Text = 0
        Txt_Conto.Text = 0
        Txt_Regole.Text = ""
        Chk_Calcolare.Checked = False

        Chk_NonEmettere.Checked = False
        Chk_Domiciliare.Checked = False
        Chk_CentroDiurno.Checked = False
        Chk_Permanente.Checked = True
        Chk_Lunedi.Checked = False
        Chk_Martedi.Checked = False
        Chk_Mercoledi.Checked = False
        Chk_Giovedi.Checked = False
        Chk_Venerdi.Checked = False
        Chk_Sabato.Checked = False
        Chk_Domenica.Checked = False
        Chk_NonEmettereOP.Checked = False

        Rb_GiorniMese.Checked = False
        Rb_Diviso30.Checked = False
        Rb_MeseAnno.Checked = False
        RB_Anno12.Checked = False
        RB_Anno7.Checked = False
        Rb_Diviso34.Checked = False
        Chk_IncrementaNumeroCartella.Checked = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        DD_Personalizzazione.Items.Clear()
        DD_Personalizzazione.Items.Add("")
        DD_Personalizzazione.Items.Add("Conto Retta")
        DD_Personalizzazione.Items.Add("Descrizione")
        DD_Personalizzazione.Items.Add("Conto Retta - descrizione")
        DD_Personalizzazione.Items.Add("Conto Retta - contropartita - Descrizione")
        DD_Personalizzazione.Items.Add("Conto Retta - ControPartita - Dt Nas - TabAnagrafica")
        DD_Personalizzazione.Items.Add("R/C Contropartita - Descrizione O/P Retta Descrizione")
        DD_Personalizzazione.Items.Add("R/C Contropartita - Descrizione O/P Retta Descrizione - DataNascita")
        DD_Personalizzazione.Items.Add("Mese Compentenza - Stato Autosufficenza - Fatt. Riferimento")
        DD_Personalizzazione.Items.Add("Stato Autosufficenza - gg - Retta Ospite")
        DD_Personalizzazione.Items.Add("GG - Retta Ospite")        
        DD_Personalizzazione.Items.Add("Residenziale Magiera")
        DD_Personalizzazione.Items.Add("Diurno Magiera")
        DD_Personalizzazione.Items.Add("Domiciliare Magiera")
        DD_Personalizzazione.Items.Add("Residenziali Asp Charitas")
        DD_Personalizzazione.Items.Add("Residenziale Cittadella")
        DD_Personalizzazione.Items.Add("Virginia Borgheri")
        DD_Personalizzazione.Items.Add("Conto Descrizione CodiceFiscale")
        DD_Personalizzazione.Items.Add("Conto Retta - ! descrizione")
        DD_Personalizzazione.Items.Add("Diurno Soresina")
        DD_Personalizzazione.Items.Add("Conto Retta - descrizione - Betulle")
        DD_Personalizzazione.Items.Add("Med Service")
        DD_Personalizzazione.Items.Add("(O) Descrizione - (E) Conto-contropartita-Descrizione")
        DD_Personalizzazione.Items.Add("Villa Amelia")
        DD_Personalizzazione.Items.Add("Villa Jole")
        DD_Personalizzazione.Items.Add("Virginia")
        DD_Personalizzazione.Items.Add("Uscita Sicurezza")
        DD_Personalizzazione.Items.Add("Prealpina")
        DD_Personalizzazione.Items.Add("Diurno Cittadella")
        DD_Personalizzazione.Items.Add("Invita")
        DD_Personalizzazione.Items.Add("Asp Siena")
        DD_Personalizzazione.Items.Add("Sissa")
        DD_Personalizzazione.Items.Add("ODA")
        DD_Personalizzazione.Items.Add("Don Gnocchi")
        DD_Personalizzazione.Items.Add("Lusan")
        DD_Personalizzazione.Items.Add("Chiara Luce")
        DD_Personalizzazione.Items.Add("NEFESH")
        DD_Personalizzazione.Items.Add("San Martino")
        DD_Personalizzazione.Items.Add("Moscati")
        DD_Personalizzazione.Items.Add("Italcliniche")
        DD_Personalizzazione.Items.Add("ANNISERENI")
        DD_Personalizzazione.Items.Add("Don Gnocchi Tricarico")
        DD_Personalizzazione.Items.Add("Residenza Paradiso")
        DD_Personalizzazione.Items.Add("Gabbiano")
        DD_Personalizzazione.Items.Add("MezzaSelva Res")
        DD_Personalizzazione.Items.Add("ANFFAS Carrara")
        DD_Personalizzazione.Items.Add("CoopSanLorenzo")
        DD_Personalizzazione.Items.Add("Don Gnocchi Ronzoni")
        DD_Personalizzazione.Items.Add("IlFaro")
        DD_Personalizzazione.Items.Add("DonMoschetta")
        DD_Personalizzazione.Items.Add("IstitutoCiechi")
        DD_Personalizzazione.Items.Add("OspizioMarino")
        DD_Personalizzazione.Items.Add("Libera")

        'OspizioMarino


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)

        Dim KCausa As New Cls_CausaliEntrataUscita


        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzaPT)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzaPT1)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzaPT2)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_PresenzaPT)
        
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzeTP)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzeTP1)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_AssenzeTP2)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_PresenzeTP)
        KCausa.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_Chiuso)


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim k As New Cls_CentroServizio


        REM MyTable = Session("Appoggio")


        If Request.Item("CODICE") <> "" Then
            k.CENTROSERVIZIO = Request.Item("CODICE")


            k.Leggi(ConnectionString, k.CENTROSERVIZIO)

            Txt_Codice.Text = k.CENTROSERVIZIO
            Txt_Descrizione.Text = k.DESCRIZIONE
            Txt_Mastro.Text = k.MASTRO
            Txt_Conto.Text = k.CONTO

            If k.TIPOCENTROSERVIZIO = "F" Then
                Chk_CentroDiurno.Checked = False
                Chk_Permanente.Checked = False
                Chk_Domiciliare.Checked = False

            Else

                If k.TIPOCENTROSERVIZIO = "D" Then
                    Chk_CentroDiurno.Checked = True
                    Chk_Permanente.Checked = False
                    Chk_Domiciliare.Checked = False
                Else
                    If k.TIPOCENTROSERVIZIO = "A" Then
                        Chk_CentroDiurno.Checked = False
                        Chk_Permanente.Checked = False
                        Chk_Domiciliare.Checked = True
                    Else
                        Chk_Domiciliare.Checked = False
                        Chk_CentroDiurno.Checked = False
                        Chk_Permanente.Checked = True
                    End If
                End If
            End If
            If k.Emissione = "" Then
                k.Emissione = 0
            End If

            If k.Emissione = 0 Then
                Chk_NonEmettere.Checked = False
            Else
                Chk_NonEmettere.Checked = True
            End If

            If k.NonCalcolare = 0 Then
                Chk_Calcolare.Checked = False
            Else
                Chk_Calcolare.Checked = True
            End If

            If k.EmissioneOP = 0 Then
                Chk_NonEmettereOP.Checked = False
            Else
                Chk_NonEmettereOP.Checked = True
            End If

            Dim APPOGGIO As String
            APPOGGIO = k.SETTIMANA

            If Mid(APPOGGIO, 1, 1) = "A" Then
                Chk_Lunedi.Checked = True
            End If
            If Mid(APPOGGIO, 2, 1) = "A" Then
                Chk_Martedi.Checked = True
            End If
            If Mid(APPOGGIO, 3, 1) = "A" Then
                Chk_Mercoledi.Checked = True
            End If
            If Mid(APPOGGIO, 4, 1) = "A" Then
                Chk_Giovedi.Checked = True
            End If
            If Mid(APPOGGIO, 5, 1) = "A" Then
                Chk_Venerdi.Checked = True
            End If
            If Mid(APPOGGIO, 6, 1) = "A" Then
                Chk_Sabato.Checked = True
            End If
            If Mid(APPOGGIO, 7, 1) = "A" Then
                Chk_Domenica.Checked = True
            End If


            If k.GIORNO0101 = "S" Then
                Chk_0101.Checked = True
            Else
                Chk_0101.Checked = False
            End If

            If k.GIORNO1508 = "S" Then
                Chk_1508.Checked = True
            Else
                Chk_1508.Checked = False
            End If




            If k.GIORNO0601 = "S" Then
                Chk_0601.Checked = True
            Else
                Chk_0601.Checked = False
            End If


            If k.GIORNO0111 = "S" Then
                Chk_0111.Checked = True
            Else
                Chk_0111.Checked = False
            End If



            If k.GIORNO1903 = "S" Then
                Chk_1903.Checked = True
            Else
                Chk_1903.Checked = False
            End If


            If k.GIORNO0411 = "S" Then
                Chk_0411.Checked = True
            Else
                Chk_0411.Checked = False
            End If



            If k.GIORNO2504 = "S" Then
                Chk_2504.Checked = True
            Else
                Chk_2504.Checked = False
            End If


            If k.GIORNO0812 = "S" Then
                Chk_0812.Checked = True
            Else
                Chk_0812.Checked = False
            End If



            If k.GIORNO0105 = "S" Then
                Chk_0105.Checked = True
            Else
                Chk_0105.Checked = False
            End If


            If k.GIORNO2512 = "S" Then
                Chk_2512.Checked = True
            Else
                Chk_2512.Checked = False
            End If

            If k.GIORNO0206 = "S" Then
                Chk_0206.Checked = True
            Else
                Chk_0206.Checked = False
            End If


            If k.GIORNO2612 = "S" Then
                Chk_2612.Checked = True
            Else
                Chk_2612.Checked = False
            End If

            If k.GIORNO2906 = "S" Then
                Chk_2906.Checked = True
            Else
                Chk_2906.Checked = False
            End If



            Txt_LunediPasqua.Text = k.GIORNO1
            If k.GIORNO1ATTIVO = "S" Then
                Chk_LunediPasqua.Checked = True
            Else
                Chk_LunediPasqua.Checked = False
            End If

            Txt_SantoPatrono.Text = k.GIORNO4

            If k.GIORNO4ATTIVO = "S" Then
                Chk_SantoPatrono.Checked = True
            Else
                Chk_SantoPatrono.Checked = False
            End If

            Txt_Regole.Text = k.Regole

            DD_Struttura.SelectedValue = k.Villa

            DD_Personalizzazione.Text = k.DescrizioneRigaInFattura


            Txt_EPersonam.Text = k.EPersonam
            Txt_EPersonamNon.Text = k.EPersonamN

            Txt_CodiceRegione.Text = k.CODREGIONE
            Txt_CodiceAsl.Text = k.CODASL
            Txt_CodiceStruttura.Text = k.CODSSA



            DD_AssenzaPT.SelectedValue = k.CausalePartTimeAss
            DD_AssenzaPT1.SelectedValue = k.CausalePartTimeAss1
            DD_AssenzaPT2.SelectedValue = k.CausalePartTimeAss2
            DD_PresenzaPT.SelectedValue = k.CausalePartTime
            DD_AssenzeTP.SelectedValue = k.CausaleTempoPienoAss
            DD_AssenzeTP1.SelectedValue = k.CausaleTempoPienoAss1
            DD_AssenzeTP2.SelectedValue = k.CausaleTempoPienoAss2
            DD_PresenzeTP.SelectedValue = k.CausaleTempoPieno
            DD_Chiuso.SelectedValue = k.CausaleChiuso

            Dd_Mese.SelectedValue = k.MeseFatturazione
            Txt_Anno.Text = k.AnnoFatturazione


            If k.IncrementaNumeroCartella = 1 Then
                Chk_IncrementaNumeroCartella.Checked = True
            Else
                Chk_IncrementaNumeroCartella.Checked = False
            End If

            If k.ImportoMensile = -1 Then
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = False
                RB_Anno7.Checked = False
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = True
            End If
            If k.ImportoMensile = 0 Then
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = True
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = False
                RB_Anno7.Checked = False
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = False
            End If
            If k.ImportoMensile = 30 Then
                Rb_Diviso30.Checked = True
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = False
                RB_Anno7.Checked = False
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = False
            End If

            If k.ImportoMensile = 34 Then
                Rb_Diviso34.Checked = True
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = False
                RB_Anno7.Checked = False
                Rb_DaParametri.Checked = False
            End If


            If k.ImportoMensile = 12 Then
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = True
                RB_Anno12.Checked = False
                RB_Anno7.Checked = False
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = False
            End If
            If k.ImportoMensile = 365 Then
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = True
                RB_Anno7.Checked = False
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = False
            End If
            If k.ImportoMensile = 7 Then
                Rb_Diviso30.Checked = False
                Rb_GiorniMese.Checked = False
                Rb_MeseAnno.Checked = False
                RB_Anno12.Checked = False
                RB_Anno7.Checked = True
                Rb_Diviso34.Checked = False
                Rb_DaParametri.Checked = False
            End If
            Txt_Codice.Enabled = False
        End If

        Try
            DecodificaCodiceEpersonam()
        Catch ex As Exception

        End Try

    End Sub




    Protected Sub Modifica()
        Dim K As New Cls_CentroServizio
        Dim ConnectionString As String = Session("DC_OSPITE")

        K.CENTROSERVIZIO = Txt_Codice.Text
        K.Leggi(ConnectionString, K.CENTROSERVIZIO)

        K.CENTROSERVIZIO = Txt_Codice.Text
        K.DESCRIZIONE = Txt_Descrizione.Text
        K.MASTRO = Val(Txt_Mastro.Text)
        K.CONTO = Val(Txt_Conto.Text)

        
        If Chk_CentroDiurno.Checked = True Then
            K.TIPOCENTROSERVIZIO = "D"
        Else
            If Chk_Domiciliare.Checked = True Then
                K.TIPOCENTROSERVIZIO = "A"
            Else
                K.TIPOCENTROSERVIZIO = ""
            End If
        End If

        Dim Giorno As String

        Giorno = ""
        If Chk_Lunedi.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Martedi.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Mercoledi.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Giovedi.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Venerdi.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Sabato.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If
        If Chk_Domenica.Checked = True Then
            Giorno = Giorno & "A"
        Else
            Giorno = Giorno & " "
        End If

        K.Regole = Txt_Regole.Text
        If Chk_NonEmettere.Checked = True Then
            K.Emissione = 1
        Else
            K.Emissione = 0
        End If


        If Chk_Calcolare.Checked = True Then
            K.NonCalcolare = 1
        Else
            K.NonCalcolare = 0
        End If


        If Chk_NonEmettereOP.Checked = True Then
            K.EmissioneOP = 1
        Else
            K.EmissioneOP = 0
        End If


        K.Villa = DD_Struttura.SelectedValue
        K.SETTIMANA = Giorno


        If Rb_DaParametri.Checked = True Then
            K.ImportoMensile = -1
        End If
        If Rb_GiorniMese.Checked = True Then
            K.ImportoMensile = 0
        End If
        If Rb_Diviso30.Checked = True Then
            K.ImportoMensile = 30
        End If
        If Rb_MeseAnno.Checked = True Then
            K.ImportoMensile = 12
        End If
        If RB_Anno12.Checked = True Then
            K.ImportoMensile = 365
        End If
        If RB_Anno7.Checked = True Then
            K.ImportoMensile = 7
        End If

        If Rb_Diviso34.Checked = True Then
            K.ImportoMensile = 34
        End If


        If Chk_0101.Checked = True Then
            K.GIORNO0101 = "S"
        Else
            K.GIORNO0101 = ""
        End If

        If Chk_1508.Checked = True Then
            K.GIORNO1508 = "S"
        Else
            K.GIORNO1508 = ""
        End If




        If Chk_0601.Checked = True Then
            K.GIORNO0601 = "S"
        Else
            K.GIORNO0601 = ""
        End If


        If Chk_0111.Checked = True Then
            K.GIORNO0111 = "S"
        Else
            K.GIORNO0111 = ""
        End If



        If Chk_1903.Checked = True Then
            K.GIORNO1903 = "S"
        Else
            K.GIORNO1903 = ""
        End If


        If Chk_0411.Checked = True Then
            K.GIORNO0411 = "S"
        Else
            K.GIORNO0411 = ""
        End If



        If Chk_2504.Checked = True Then
            K.GIORNO2504 = "S"
        Else
            K.GIORNO2504 = ""
        End If


        If Chk_0812.Checked = True Then
            K.GIORNO0812 = "S"
        Else
            K.GIORNO0812 = ""
        End If



        If Chk_0105.Checked = True Then
            K.GIORNO0105 = "S"
        Else
            K.GIORNO0105 = ""
        End If


        If Chk_2512.Checked = True Then
            K.GIORNO2512 = "S"
        Else
            K.GIORNO2512 = ""
        End If

        If Chk_0206.Checked = True Then
            K.GIORNO0206 = "S"
        Else
            K.GIORNO0206 = ""
        End If

        If Chk_2612.Checked = True Then
            K.GIORNO2612 = "S"
        Else
            K.GIORNO2612 = ""
        End If

        If Chk_2906.Checked = True Then
            K.GIORNO2906 = "S"
        Else
            K.GIORNO2906 = ""
        End If

        K.GIORNO1 = ""
        K.GIORNO1ATTIVO = ""

        K.GIORNO4 = ""
        K.GIORNO4ATTIVO = ""

        If Txt_LunediPasqua.Text <> "" Then
            K.GIORNO1 = Txt_LunediPasqua.Text
        End If
        If Chk_LunediPasqua.Checked = True Then
            K.GIORNO1ATTIVO = "S"
        End If


        If Txt_SantoPatrono.Text <> "" Then
            K.GIORNO4 = Txt_SantoPatrono.Text
        End If
        If Chk_SantoPatrono.Checked = True Then
            K.GIORNO4ATTIVO = "S"
        End If



        K.EPersonam = Val(Txt_EPersonam.Text)
        K.EPersonamN = Val(Txt_EPersonamNon.Text)

        K.CausalePartTimeAss = DD_AssenzaPT.SelectedValue
        K.CausalePartTimeAss1 = DD_AssenzaPT1.SelectedValue
        K.CausalePartTimeAss2 = DD_AssenzaPT2.SelectedValue
        K.CausalePartTime = DD_PresenzaPT.SelectedValue
        K.CausaleTempoPienoAss = DD_AssenzeTP.SelectedValue
        K.CausaleTempoPienoAss1 = DD_AssenzeTP1.SelectedValue
        K.CausaleTempoPienoAss2 = DD_AssenzeTP2.SelectedValue
        K.CausaleTempoPieno = DD_PresenzeTP.SelectedValue
        K.CausaleChiuso = DD_Chiuso.SelectedValue


        K.CODREGIONE = Txt_CodiceRegione.Text
        K.CODASL = Txt_CodiceAsl.Text
        K.CODSSA = Txt_CodiceStruttura.Text


        K.DescrizioneRigaInFattura = DD_Personalizzazione.SelectedItem.Text
        K.Utente = Session("UTENTE")

        If Chk_IncrementaNumeroCartella.Checked = True Then
            K.IncrementaNumeroCartella = 1
        Else
            K.IncrementaNumeroCartella = 0
        End If

        K.MeseFatturazione = Dd_Mese.SelectedValue
        K.AnnoFatturazione = Txt_Anno.Text


        K.ScriviCentroServizio(ConnectionString)

        'Txt_Codice.Enabled = False
        'Dim MyJs As String
        'MyJs = "alert('Modificato Tabella');"
        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

    End Sub


    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice centroservizio');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Exit Sub
        End If

        Dim k As New Cls_CentroServizio

        k.Elimina(Session("DC_OSPITE"), Txt_Codice.Text)

        Call PaginaPrecedente()
    End Sub

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice centroservizio');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Exit Sub
        End If

        If Trim(Request.Item("CODICE")) = "" Then
            Dim K As New Cls_CentroServizio

            K.CENTROSERVIZIO = Txt_Codice.Text
            K.Leggi(Session("DC_OSPITE"), K.CENTROSERVIZIO)

            If Trim(K.DESCRIZIONE) <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già usato');", True)
                Exit Sub
            End If

        End If


        If Val(Txt_Mastro.Text) = 0 Or Val(Txt_Conto.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare conto di riferimento del servizio');", True)
            Exit Sub
        End If


        Call Modifica()


        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True
        Img_VerificaDes.ImageUrl = "~/images/Blanco.png"
        If Txt_Descrizione.Text.Trim <> "" Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CentroServizio

            x.CENTROSERVIZIO = ""
            x.Descrizione = Txt_Descrizione.Text
            x.LeggiDescrizione(Session("DC_OSPITE"))

            If x.CENTROSERVIZIO <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoCentroServizio.aspx")
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text.Trim <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_CentroServizio

                x.CENTROSERVIZIO = ""
                x.Descrizione = Txt_Descrizione.Text
                x.LeggiDescrizione(Session("DC_OSPITE"))

                If x.CENTROSERVIZIO <> "" Then
                    Img_VerificaDes.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then            
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CentroServizio

            x.CENTROSERVIZIO = Txt_Codice.Text.Trim
            x.DESCRIZIONE = ""
            x.Leggi(Session("DC_OSPITE"), x.CENTROSERVIZIO)

            If x.DESCRIZIONE <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub DD_EPersonam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_EPersonam.SelectedIndexChanged
        Txt_EPersonam.Text = DD_EPersonam.SelectedValue
    End Sub

    Protected Sub Txt_EPersonamNon_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_EPersonamNon.TextChanged

    End Sub

    Protected Sub DD_EPersonamN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_EPersonamN.SelectedIndexChanged
        Txt_EPersonamNon.Text = DD_EPersonamN.SelectedValue
    End Sub

    Protected Sub BtnEpersonam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEpersonam.Click

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BtnEpersonam", "$(document).ready(function() { setTimeout(function(){  DialogBoxW('NucleiEpersonam.aspx?CODICE=" & Val(Txt_EPersonam.Text) & "');}, 1000);  });", True)

        'DialogBoxW('NucleiEpersonam.aspx');
    End Sub

    Protected Sub BtnEpersonamN_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEpersonamN.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BtnEpersonam", "$(document).ready(function() { setTimeout(function(){  DialogBoxW('NucleiEpersonam.aspx?CODICE=" & Val(Txt_EPersonamNon.Text) & "');}, 1000);  });", True)

    End Sub
End Class
