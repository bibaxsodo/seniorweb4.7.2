﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_MovimentiDomiciliariCentroServizio" CodeFile="MovimentiDomiciliariCentroServizio.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="axex" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Movimenti Domiciliari Servizio</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        var GlbCella = '';
        var GlbCSERV = '';
        var GlbCODOSP = 0;
        var GlbANNO = 0;
        var GlbMESE = 0;
        var GlbGIORNO = 0;

        function leggicella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA) {
            $.ajax({
                url: "GiornoDomiciliare.ashx?CSERV=" + CSERV + "&CODOSP=" + CODOSP + "&ANNO=" + ANNO + "&MESE=" + MESE + "&GIORNO=" + GIORNO,
                success: function (data, stato) {

                    $("#" + CELLA).html(data);


                },
                error: function (richiesta, statoa, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }

        function apricella(CSERV, CODOSP, ANNO, MESE, GIORNO, CELLA) {
            GlbCella = CELLA;
            GlbCSERV = CSERV;
            GlbCODOSP = CODOSP;
            GlbANNO = ANNO;
            GlbMESE = MESE;
            GlbGIORNO = GIORNO;
            DialogBox("GiornoDomiciliare.aspx?CENTROSERVIZIO=" + CSERV + "&CODICEOSPITE=" + CODOSP + "&ANNO=" + ANNO + "&MESE=" + MESE + "&GIORNO=" + GIORNO);
        }


        function apriwiz(CSERV, CODOSP, ANNO, MESE) {
            GlbCella = "ALL";
            GlbCSERV = CSERV;
            GlbCODOSP = CODOSP;
            GlbANNO = ANNO;
            GlbMESE = MESE;
            DialogBox("WizardDomiciliare.aspx?CENTROSERVIZIO=" + CSERV + "&CODICEOSPITE=" + CODOSP + "&ANNO=" + ANNO + "&MESE=" + MESE);
        }
        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.function_call = 'chiudiredisp';

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
        function chiudiredisp() {
            if (GlbCella == "ALL") {
                var giorno = 1;
                for (giorno = 1; giorno <= 31; giorno++) {
                    leggicella(GlbCSERV, GlbCODOSP, GlbANNO, GlbMESE, giorno, "Cella" + giorno + "O" + GlbCODOSP);
                }
            } else {
                leggicella(GlbCSERV, GlbCODOSP, GlbANNO, GlbMESE, GlbGIORNO, GlbCella);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Servizio -  Movimenti DomiciliariServizio</div>
                                <div class="SottoTitolo">
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <span class="BenvenutoText">Benvenuto
                                    <asp:Label ID="Lbl_Utente" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <div class="DivTasti">
                                    <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                                <br />
                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <axex:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                                    Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <axex:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Movimenti Domiciliari Servizio
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="392px"></asp:Label>
                                            <br />
                                            <label class="LabelCampo">Anno :</label>
                                            <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Mese :</label>

                                            <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px" AutoPostBack="true">
                                                <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Giorni Dal :</label>
                                            <asp:TextBox ID="Txt_Dal" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                            Al 
        <asp:TextBox ID="Txt_Al" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Struttura:</label>
                                            <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Centro Servizio : </label>
                                            <asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
                                            <br />
                                            <br />
                                            <asp:Label ID="Lbl_Schema" runat="server"></asp:Label>
                                            <br />
                                        </ContentTemplate>
                                    </axex:TabPanel>
                                </axex:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>




            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>
                    <div id="Div1">&nbsp;</div>
                    <div id="pippo" class="wait">
                        <br />
                        <img height="30px" src="images/loading.gif" alt=""><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </div>
    </form>
</body>
</html>
