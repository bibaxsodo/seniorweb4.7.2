﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_ImportDiurnoNew
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub OspitiWeb_ImportDiurno_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then
            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Try
                If Session("NomeEPersonam") = "MAGIERA" Then
                    Chk_SoloNonAuto.Visible = True
                End If

                If Session("DC_OSPITE").ToString.IndexOf("UsciteSicurezza") > 0 Then
                    Chk_SoloNonAuto.Visible = True
                End If

            Catch ex As Exception

            End Try
        End If

    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String
        Dim CodiceFiscale As String

        Token = LoginPersonam(Context)
        If Token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
            Exit Sub
        End If

        Dim OldCF As String = ""


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("Centroservizio", GetType(String))
        Dim Ind As Integer
        For Ind = 1 To 31
            Tabella.Columns.Add("G" & Ind, GetType(String))
        Next
        Tabella.Columns.Add("Segnalazione", GetType(String))
        Tabella.Columns.Add("CodiceOspite", GetType(String))


        Dim Data() As Byte

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Token = LoginPersonam(Context)


        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        Dim MyVtRiga(40) As String

        For Indice = 0 To 36
            If Indice > 3 And Indice - 3 <= GiorniMese(Param.MeseFatturazione, Param.AnnoFatturazione) Then
                MyVtRiga(Indice) = "C"
            End If
        Next

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim cmdcs As New OleDbCommand()


        Dim Entrato As Boolean = False

        If Chk_SoloNonAuto.Visible = True Then
            If Chk_SoloNonAuto.Checked = True Then
                cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And EPersonamN > 0")
            Else
                cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And EPersonam > 0 ")
            End If
        Else
            cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And (EPersonam > 0 OR  EPersonamN > 0)")
        End If
        If Session("DC_OSPITE").ToString.IndexOf("UsciteSicurezza") > 0 Then
            cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And (EPersonam > 0 OR  EPersonamN > 0)")
        End If


        cmdcs.Connection = cn

        Dim RDcs As OleDbDataReader = cmdcs.ExecuteReader()
        Do While RDcs.Read

            Dim rawresp As String

            Try
                Dim Nucleo As Integer
                '       " & RDcs.Item("Epersonam") & "
                If RDcs.Item("Epersonam") = 0 Then
                    Nucleo = RDcs.Item("EpersonamN")
                Else
                    Nucleo = RDcs.Item("Epersonam")
                End If
                If Session("DC_OSPITE").ToString.IndexOf("UsciteSicurezza") > 0 Then
                    Nucleo = RDcs.Item("Epersonam")
                    If Chk_SoloNonAuto.Checked = True Then
                        Nucleo = RDcs.Item("EpersonamN")
                    End If
                End If
                Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/day_centers/" & Nucleo & "/guests/presences/" & Param.AnnoFatturazione & "/" & Param.MeseFatturazione)

                client.Method = "GET"
                client.Headers.Add("Authorization", "Bearer " & Token)
                client.ContentType = "Content-Type: application/json"

                Dim reader As StreamReader
                Dim response As HttpWebResponse = Nothing

                response = DirectCast(client.GetResponse(), HttpWebResponse)

                reader = New StreamReader(response.GetResponseStream())



                rawresp = reader.ReadToEnd()

            Catch ex As Exception
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                Exit Sub
            End Try


            If rawresp <> "{}" Then



                'Dim jResults As JObject = JObject.Parse(rawresp)

                Dim jResults As JArray = JArray.Parse(rawresp)

                Dim Accreditato As String = ""

                For Each jTok1 As JToken In jResults.Children

                    CodiceFiscale = jTok1.Item("cf").ToString()

                    If CodiceFiscale = "PLCMGR34M55I622K" Then
                        CodiceFiscale = "PLCMGR34M55I622K"
                    End If
                    If OldCF <> CodiceFiscale And OldCF <> "" Then
                        If Entrato = True Then
                            Dim CercaIndice As Integer = 0
                            Dim TrovatoIndice As Integer = -1

                            For CercaIndice = 0 To Tabella.Rows.Count - 1
                                If Tabella.Rows(CercaIndice).Item(2) = OldCF Then
                                    TrovatoIndice = CercaIndice
                                End If
                            Next
                            Dim myriga As System.Data.DataRow
                            If TrovatoIndice = -1 Then
                                myriga = Tabella.NewRow()
                            End If


                            Dim Indice As Integer
                            For Indice = 0 To 36
                                If TrovatoIndice = -1 Then
                                    myriga(Indice) = MyVtRiga(Indice)
                                Else
                                    If campodb(Tabella.Rows(TrovatoIndice).Item(Indice)) = "" Or campodb(MyVtRiga(Indice)) <> "C" Then
                                        Tabella.Rows(TrovatoIndice).Item(Indice) = MyVtRiga(Indice)
                                    End If
                                End If
                            Next
                            If TrovatoIndice = -1 Then
                                Tabella.Rows.Add(myriga)
                            End If
                        End If
                        Entrato = False
                        For Indice = 0 To 36
                            If Indice > 3 And Indice - 3 <= GiorniMese(Param.MeseFatturazione, Param.AnnoFatturazione) Then
                                MyVtRiga(Indice) = "C"
                            End If
                        Next
                        MyVtRiga(35) = ""
                    End If
                    OldCF = CodiceFiscale


                    Dim Ospite As New ClsOspite

                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), CodiceFiscale)



                    If Ospite.CodiceOspite > 0 Then


                        MyVtRiga(0) = Ospite.CognomeOspite
                        MyVtRiga(1) = Ospite.NomeOspite
                        MyVtRiga(2) = jTok1.Item("cf").ToString()
                        MyVtRiga(36) = Ospite.CodiceOspite

                        If jTok1.Item("cf").ToString = "RFFVSS95P41A657R" Then

                            CodiceFiscale = "RFFVSS95P41A657R"

                        End If

                        For Each jTok2 As JToken In jTok1.Item("presences").Children
                            Dim Procedi As Boolean = True

                            ' MAGIERA
                            If RDcs.Item("Epersonam") = 1157 Or RDcs.Item("Epersonam") = 1162 Or RDcs.Item("Epersonam") = 1166 Or RDcs.Item("Epersonam") = 1409 Or _
                               RDcs.Item("EpersonamN") = 1157 Or RDcs.Item("EpersonamN") = 1162 Or RDcs.Item("EpersonamN") = 1166 Or RDcs.Item("EpersonamN") = 1409 Then
                                If jTok2.Item("conv").ToString = "True" And RDcs.Item("EPersonamN") > 0 Then
                                    Procedi = False
                                End If
                                If jTok2.Item("conv").ToString = "False" And RDcs.Item("EPersonamN") = 0 Then
                                    Procedi = False
                                End If
                            End If

                            If Procedi = True Then
                                Dim MiaData As Date
                                Entrato = True

                                MiaData = jTok2.Item("data")

                                If Day(MiaData) = 28 Then
                                    MiaData = jTok2.Item("data")
                                End If
                                If Day(MiaData) = 22 Then
                                    If jTok1.Item("cf").ToString = "CRGNNT21A57B455Q" Then

                                        CodiceFiscale = "CRGNNT21A57B455Q"

                                    End If
                                End If
                                Dim GlbPranzo As String = "False"
                                Dim GlbCena As String = "False"
                                Dim GlbTrasporto As String = "False"
                                Dim Glbpomeriggio_prev As String = "False"
                                Dim Glbmattina_prev As String = "False"
                                Dim Glbnotte_prev As String = "False"

                                Dim Glbpomeriggio As String = "False"
                                Dim Glbmattina As String = "False"
                                Dim Glbnotte As String = "False"

                                Dim Glbcauseid As String = "False"
                                Dim Glbconv As String = "False"

                                Dim GlbNumeroTrasporti As String = ""


                                Glbcauseid = jTok2.Item("cause_id").ToString
                                Glbconv = jTok2.Item("conv").ToString



                                For Each jTok3 As JToken In jTok2.Item("meals").Children
                                    If jTok3.Item("type").ToString.ToUpper = "lunch".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            GlbPranzo = "True"
                                        End If
                                    End If
                                    If jTok3.Item("type").ToString.ToUpper = "dinner".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            GlbCena = "True"
                                        End If
                                    End If
                                Next

                                For Each jTok3 As JToken In jTok2.Item("mod").Children
                                    If jTok3.Item("type").ToString.ToUpper = "morning".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            Glbmattina = "True"
                                        End If
                                        If jTok3.Item("plan").ToString.ToUpper = "True".ToUpper Then
                                            Glbmattina_prev = "True"
                                        End If
                                    End If
                                    If jTok3.Item("type").ToString.ToUpper = "afternoon".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            Glbpomeriggio = "True"
                                        End If
                                        If jTok3.Item("plan").ToString.ToUpper = "True".ToUpper Then
                                            Glbpomeriggio_prev = "True"
                                        End If
                                    End If
                                    If jTok3.Item("type").ToString.ToUpper = "night".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            Glbnotte = "True"
                                        End If
                                        If jTok3.Item("plan").ToString.ToUpper = "True".ToUpper Then
                                            Glbnotte_prev = "True"
                                        End If
                                    End If
                                Next

                                For Each jTok3 As JToken In jTok2.Item("others").Children
                                    If jTok3.Item("type").ToString.ToUpper = "transport".ToUpper Then
                                        If jTok3.Item("actual").ToString.ToUpper = "True".ToUpper Then
                                            GlbTrasporto = "True"
                                        End If
                                    End If
                                Next



                                Try
                                    If jTok2.ToString.ToUpper.IndexOf("""type_description_actual"": ""2 viaggi""".ToUpper) >= 0 Then
                                        GlbNumeroTrasporti = "2"
                                    End If
                                Catch ex As Exception

                                End Try


                                Dim CAUSALE As String = ""

                                If RDcs.Item("Epersonam") = 1157 Or RDcs.Item("Epersonam") = 1162 Or RDcs.Item("Epersonam") = 1166 Or RDcs.Item("Epersonam") = 1409 Or _
                                   RDcs.Item("EpersonamN") = 1157 Or RDcs.Item("EpersonamN") = 1162 Or RDcs.Item("EpersonamN") = 1166 Or RDcs.Item("EpersonamN") = 1409 Then
                                    'MAGIERA                                  

                                    If GlbTrasporto = "True" Then
                                        If RDcs.Item("Epersonam") = 1409 Or RDcs.Item("EpersonamN") = 1409 Then
                                            CAUSALE = "D5"
                                        Else
                                            CAUSALE = "D7"
                                        End If

                                    End If

                                    If GlbCena = "True" Then
                                        CAUSALE = "D1"
                                        If GlbTrasporto = "True" Then
                                            If RDcs.Item("Epersonam") = 1409 Or RDcs.Item("EpersonamN") = 1409 Then
                                                CAUSALE = "D1"
                                            Else
                                                CAUSALE = "D8"
                                            End If
                                        End If
                                    End If

                                    If Glbmattina = "False" Then
                                        If Glbmattina_prev = "True" Then
                                            CAUSALE = "D2"
                                        End If
                                    End If

                                    If Glbpomeriggio = "False" Then
                                        If Glbpomeriggio_prev = "False" And Glbmattina = "True" Then
                                            If CAUSALE = "" Then
                                                CAUSALE = "DT"
                                                If GlbPranzo = "True" Or GlbCena = "True" Then
                                                    CAUSALE = "DX"
                                                End If
                                            End If
                                            If CAUSALE = "D1" Then
                                                CAUSALE = "DY"
                                            End If
                                        End If
                                    End If

                                    If CAUSALE = "" Then
                                        If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" Then
                                            CAUSALE = "C"
                                        End If
                                    End If

                                    If Glbcauseid <> "" Then
                                        If Val(Glbcauseid) = 3 Then
                                            CAUSALE = "C"
                                        End If
                                    End If
                                    If Glbcauseid <> "" Then
                                        If Val(Glbcauseid) = 3 Then
                                            CAUSALE = "C"
                                        End If
                                        If Val(Glbcauseid) = 7 Then
                                            CAUSALE = "D6"
                                        End If
                                        If Val(Glbcauseid) = 1 Then
                                            CAUSALE = "D6"
                                        End If

                                        If Val(Glbcauseid) = 4 Then
                                            CAUSALE = "D2"
                                        End If
                                        If Val(Glbcauseid) = 6 Then
                                            CAUSALE = "D3"
                                        End If
                                    End If
                                End If

                                '18
                                If RDcs.Item("Epersonam") = 18 Or RDcs.Item("Epersonam") = 997 Then
                                    Dim pomeriggio_prev As String = ""
                                    Dim mattina_prev As String = ""
                                    Dim notte_prev As String = ""

                                    Dim pomeriggio As String = ""
                                    Dim mattina As String = ""
                                    Dim notte As String = ""





                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                             (Glbpomeriggio_prev = "True" And Glbmattina_prev = "True" And Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If


                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       (Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" Or _
                                        Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False") Then
                                        CAUSALE = "C"
                                    End If

                                    If Glbcauseid = "6" Then  'assenza giustificata (6 ?)
                                        CAUSALE = "A"
                                    End If

                                    If Glbcauseid = "7" Then  'assenza non giustificata (7 ?)
                                        CAUSALE = "AN"
                                    End If

                                    If Glbcauseid = "8" Then  'assenza per scambio (8 ?)
                                        CAUSALE = "C"
                                    End If
                                End If


                                ' USCITA SICUREZZA
                                If RDcs.Item("Epersonam") = 2513 Or RDcs.Item("Epersonam") = 2512 Then
                                    Dim pomeriggio_prev As String = ""
                                    Dim mattina_prev As String = ""
                                    Dim notte_prev As String = ""

                                    Dim pomeriggio As String = ""
                                    Dim mattina As String = ""
                                    Dim notte As String = ""




                                    If Glbmattina = "False" Then
                                        If Glbmattina_prev = "True" Then
                                            CAUSALE = "A"
                                        End If
                                    End If



                                    If Glbpomeriggio = "False" Then
                                        If Glbpomeriggio_prev = "True" And Glbmattina = "False" Then
                                            CAUSALE = "A"
                                        End If
                                    End If

                                    If CAUSALE = "" Then
                                        If GlbCena = "False" And GlbPranzo = "False" Then
                                            CAUSALE = "A"
                                        End If
                                    End If


                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       (Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" Or _
                                        Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False") Then
                                        CAUSALE = "C"
                                    End If

                                    If CAUSALE = "" Then
                                        If Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" And Glbpomeriggio = "" And Glbmattina = "" And Glbnotte = "" Then
                                            CAUSALE = "C"
                                        End If
                                        If Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False" And Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" Then
                                            CAUSALE = "C"
                                        End If
                                    End If
                                End If


                                If RDcs.Item("Epersonam") = 1075 Then
                                    Dim pomeriggio_prev As String = ""
                                    Dim mattina_prev As String = ""
                                    Dim notte_prev As String = ""

                                    Dim pomeriggio As String = ""
                                    Dim mattina As String = ""
                                    Dim notte As String = ""




                                    If Glbmattina = "False" Then
                                        If Glbmattina_prev = "True" Then
                                            CAUSALE = "A"
                                        End If
                                    End If



                                    If Glbpomeriggio = "False" Then
                                        If Glbpomeriggio_prev = "True" And Glbmattina = "False" Then
                                            CAUSALE = "A"
                                        End If
                                    End If

                                    If CAUSALE = "" Then
                                        If Glbmattina = "True" And Glbpomeriggio = "False" And Glbpomeriggio = "Pranzo" Then
                                            CAUSALE = "PP"
                                        End If
                                    End If

                                    If Glbcauseid = "6" And CAUSALE = "" Then
                                        CAUSALE = "PP"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       (Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" Or _
                                        Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False") Then
                                        CAUSALE = "C"
                                    End If

                                    If CAUSALE = "" Then
                                        If Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" And Glbpomeriggio = "" And Glbmattina = "" And Glbnotte = "" Then
                                            CAUSALE = "C"
                                        End If
                                        If Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False" And Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" Then
                                            CAUSALE = "C"
                                        End If
                                    End If

                                    If CAUSALE = "A" Then
                                        If Glbpomeriggio_prev = "False" Or Glbmattina_prev = "False" Then
                                            CAUSALE = "AP"
                                        End If
                                    End If
                                End If

                                If RDcs.Item("Epersonam") = 2104 Then

                                    If Glbmattina = "False" Then
                                        If Glbmattina_prev = "True" Then
                                            CAUSALE = "A"
                                        End If
                                    End If



                                    If Glbpomeriggio = "False" Then
                                        If Glbpomeriggio_prev = "True" Then
                                            CAUSALE = "A"
                                        End If
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       (Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" Or _
                                        Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False") Then
                                        CAUSALE = "C"
                                    End If

                                    If CAUSALE = "" Then
                                        If Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" And Glbpomeriggio = "" And Glbmattina = "" And Glbnotte = "" Then
                                            CAUSALE = "C"
                                        End If
                                        If Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False" And Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" Then
                                            CAUSALE = "C"
                                        End If
                                    End If

                                    If CAUSALE = "C" And GlbPranzo = "True" Then
                                        CAUSALE = ""
                                    End If
                                End If


                                If RDcs.Item("Epersonam") = 2035 Or RDcs.Item("Epersonam") = 2036 Or RDcs.Item("Epersonam") = 2037 Or RDcs.Item("Epersonam") = 2038 Or RDcs.Item("Epersonam") = 2042 Then


                                    If Glbmattina = "True" Then
                                        If Glbpomeriggio = "" Or Glbpomeriggio = "False" Then
                                            CAUSALE = "PP"
                                        End If
                                    End If
                                    If Glbpomeriggio = "True" Then
                                        If Glbmattina = "" Or Glbmattina = "False" Then
                                            CAUSALE = "PP"
                                        End If
                                    End If
                                    If (Glbmattina_prev = "True" And Glbmattina = "False") Then
                                        If (Glbpomeriggio = "" Or Glbpomeriggio = "False") Then
                                            CAUSALE = "PA"
                                            'If Glbcauseid = 1007 Then
                                            '    CAUSALE = "PA"
                                            'Else
                                            '    CAUSALE = "A"
                                            'End If
                                            If Glbcauseid = 4 Then  '4 Altro
                                                CAUSALE = "AP"
                                            End If
                                        End If
                                    End If
                                    If (Glbpomeriggio_prev = "True" And Glbpomeriggio = "False") Then
                                        If (Glbmattina = "" Or Glbmattina = "False") Then
                                            CAUSALE = "PA"
                                            'If Glbcauseid = 1007 Then
                                            '    CAUSALE = "PA"
                                            'Else
                                            '    CAUSALE = "A"
                                            'End If
                                            If Glbcauseid = 4 Then  '4 Altro
                                                CAUSALE = "AP"
                                            End If
                                        End If
                                    End If

                                    If (Glbpomeriggio_prev = "" Or Glbpomeriggio_prev = "False") And (Glbpomeriggio = "False" And Glbpomeriggio = "") Then
                                        If (Glbmattina = "" Or Glbmattina = "False") Then
                                            If Glbcauseid = 1007 Then
                                                CAUSALE = "PA"
                                            Else
                                                CAUSALE = "AP"
                                            End If
                                        End If
                                    End If

                                    If Glbmattina_prev = "True" And Glbmattina = "False" Then
                                        If Glbpomeriggio = "False" And Glbpomeriggio_prev = "True" Then
                                            CAUSALE = "A"
                                        End If
                                    End If

                                    If CAUSALE = "A" Then
                                        If Glbcauseid = 1008 Then '1008 Giorno Festivo
                                            CAUSALE = "AF"
                                        End If
                                        If Glbcauseid = 4 Then  '4 Altro
                                            CAUSALE = "F"
                                        End If
                                        If Glbcauseid = 1 Then '1 Malattia
                                            CAUSALE = "M"
                                        End If
                                        If Glbcauseid = 1007 Then ' 1007 Ricovero Ospedaliero
                                            CAUSALE = "A"
                                        End If
                                    End If

                                    'If RDcs.Item("Epersonam") = 2035 Or RDcs.Item("Epersonam") = 2036 Or RDcs.Item("Epersonam") = 2037 Then
                                    '    If CAUSALE = "M" Then
                                    '        CAUSALE = "F"
                                    '    End If
                                    'End If

                                    If CAUSALE = "" Then
                                        If Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "" And Glbpomeriggio = "" And Glbmattina = "" And Glbnotte = "" Then
                                            CAUSALE = "C"
                                        End If
                                        If Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False" And Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" Then
                                            CAUSALE = "C"
                                        End If
                                    End If

                                    If CAUSALE = "C" And GlbPranzo = "True" Then
                                        CAUSALE = ""
                                    End If

                                    If Glbcauseid = "1008" Then
                                        CAUSALE = "AF"
                                    End If
                                    If (Glbpomeriggio_prev = "False" And Glbmattina_prev = "True") Or (Glbpomeriggio_prev = "True" And Glbmattina_prev = "False") Then
                                        If CAUSALE = "A" Or CAUSALE = "AF" Or CAUSALE = "M" Or CAUSALE = "F" Then
                                            If RDcs.Item("Epersonam") <> 2042 Then
                                                CAUSALE = "AP"
                                            End If
                                        End If
                                    End If

                                    If RDcs.Item("Epersonam") = 2035 Then
                                        If CAUSALE = "PP" Then
                                            If Glbpomeriggio_prev = "True" And Glbmattina_prev = "True" Then
                                                CAUSALE = ""
                                            End If
                                        End If
                                    End If


                                    REM  CAUSALE  = mattina & "-" & pomeriggio
                                End If

                                REM CITTADELLA
                                If RDcs.Item("Epersonam") = 18 Then

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                        (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If

                                    'If Glbcauseid = "1005" Then
                                    'CAUSALE = "AN"
                                    'End If

                                    If GlbTrasporto = "True" Then
                                        CAUSALE = "T"
                                    End If

                                End If


                                REM MED SERVICE SORESINA



                                If RDcs.Item("Epersonam") = 2438 Then

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                        (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If

                                    If GlbTrasporto = "True" And GlbNumeroTrasporti = "2" Then
                                        CAUSALE = "t2"
                                    End If

                                    If GlbTrasporto = "True" And GlbNumeroTrasporti = "" Then
                                        CAUSALE = "t1"
                                    End If

                                End If

                                REM jole

                                If RDcs.Item("Epersonam") = 2533 Then

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                        (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If

                                    If UCase(GlbTrasporto) = UCase("True") Then

                                    End If


                                End If


                                'Lieto soggirno

                                If RDcs.Item("Epersonam") = 2482 Then

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                        (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If

                                    If UCase(GlbTrasporto) = UCase("True") Then

                                    End If
                                End If

                                'naviglio

                                If RDcs.Item("Epersonam") = 2539 Then

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                        (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If

                                    If GlbTrasporto = "True" Then
                                        CAUSALE = "T"
                                    End If



                                End If


                                REM betulle
                                If RDcs.Item("Epersonam") = 1653 Then
                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                     (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If



                                End If


                                REM asp siena
                                If RDcs.Item("Epersonam") = 2979 Then
                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                     (Glbpomeriggio_prev = "True" Or Glbmattina_prev = "True" Or Glbnotte_prev = "True") Then
                                        CAUSALE = "A"
                                    End If

                                    If Glbpomeriggio = "False" And Glbmattina = "False" And Glbnotte = "False" And _
                                       ((Glbpomeriggio_prev = "" And Glbmattina_prev = "" And Glbnotte_prev = "") Or _
                                        (Glbpomeriggio_prev = "False" And Glbmattina_prev = "False" And Glbnotte_prev = "False")) Then
                                        CAUSALE = "C"
                                    End If



                                End If



                                Dim CENTROSERVIZIO As String

                                CENTROSERVIZIO = RDcs.Item("CENTROSERVIZIO")

                                Dim UltCserv As New Cls_Movimenti



                                UltCserv.CENTROSERVIZIO = RDcs.Item("CENTROSERVIZIO")
                                UltCserv.CodiceOspite = Ospite.CodiceOspite
                                UltCserv.UltimaData(Context.Session("DC_OSPITE"), UltCserv.CodiceOspite)
                                If UltCserv.CENTROSERVIZIO <> CENTROSERVIZIO Then
                                    Dim cmdvc As New OleDbCommand()
                                    cmdvc.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'D' And EPersonamN= ?")
                                    cmdvc.Connection = cn
                                    cmdvc.Parameters.AddWithValue("@EPersonamn", RDcs.Item("Epersonam"))

                                    Dim RDvs As OleDbDataReader = cmdvc.ExecuteReader()
                                    If RDvs.Read Then
                                        If UltCserv.CENTROSERVIZIO = RDvs.Item("CENTROSERVIZIO") Then
                                            CENTROSERVIZIO = UltCserv.CENTROSERVIZIO
                                        End If
                                    End If
                                    RDvs.Close()
                                End If



                                Dim cmdcData As New OleDbCommand()
                                cmdcData.CommandText = ("select * from Diurno_EPersonam Where CentroServizio = ? and CodiceOspite = ? And year([Data]) = ? And Month([Data]) = ?")

                                cmdcData.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
                                cmdcData.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                                cmdcData.Parameters.AddWithValue("@Year", Year(MiaData))
                                cmdcData.Parameters.AddWithValue("@Month", Month(MiaData))
                                cmdcData.Connection = cn
                                Dim RDData As OleDbDataReader = cmdcData.ExecuteReader()
                                If RDData.Read Then
                                    MyVtRiga(3) = CENTROSERVIZIO
                                    MyVtRiga(Day(MiaData) + 3) = CAUSALE
                                    MyVtRiga(35) = "ERROREPRE"
                                    MyVtRiga(36) = Ospite.CodiceOspite

                                    If Ospite.CodiceOspite = 26 Then
                                        Ospite.CodiceOspite = 26
                                    End If
                                Else

                                    Dim Vercentroservizio As New Cls_Movimenti



                                    Vercentroservizio.CENTROSERVIZIO = CENTROSERVIZIO
                                    Vercentroservizio.CodiceOspite = Ospite.CodiceOspite
                                    If Vercentroservizio.CServizioUsato(Context.Session("DC_OSPITE")) Then
                                        MyVtRiga(3) = CENTROSERVIZIO
                                        MyVtRiga(Day(MiaData) + 3) = CAUSALE
                                        MyVtRiga(36) = Ospite.CodiceOspite
                                    Else
                                        MyVtRiga(3) = CENTROSERVIZIO
                                        MyVtRiga(35) = "ERRORECS"
                                        MyVtRiga(36) = Ospite.CodiceOspite
                                    End If
                                End If
                                RDData.Close()
                            End If
                        Next
                    Else
                        REM  Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        MyVtRiga(0) = jTok1.Item("fullname").ToString()
                        MyVtRiga(1) = ""
                        MyVtRiga(2) = jTok1.Item("cf").ToString()
                        MyVtRiga(35) = "ERRORE"
                        MyVtRiga(36) = 0
                        REM Tabella.Rows.Add(myriga)
                    End If
                Next
            End If
        Loop

        Dim CercaIndice1 As Integer = 0
        Dim TrovatoIndice1 As Integer = -1

        For CercaIndice1 = 0 To Tabella.Rows.Count - 1
            If Tabella.Rows(CercaIndice1).Item(2) = OldCF Then
                TrovatoIndice1 = CercaIndice1
            End If
        Next
        Dim myriga1 As System.Data.DataRow
        If TrovatoIndice1 = -1 Then
            myriga1 = Tabella.NewRow()
        End If

        Dim Indice1 As Integer
        For Indice1 = 0 To 36
            If TrovatoIndice1 = -1 Then
                myriga1(Indice1) = MyVtRiga(Indice1)
            Else
                If campodb(Tabella.Rows(TrovatoIndice1).Item(Indice1)) = "" Or campodb(MyVtRiga(Indice1)) <> "C" Then
                    Tabella.Rows(TrovatoIndice1).Item(Indice1) = MyVtRiga(Indice1)
                End If
            End If
        Next
        If TrovatoIndice1 = -1 Then
            Tabella.Rows.Add(myriga1)
        End If


        For Indice1 = 0 To 36
            If Indice1 > 3 And Indice1 - 3 <= GiorniMese(Param.MeseFatturazione, Param.AnnoFatturazione) Then
                MyVtRiga(Indice1) = "C"
            End If
        Next
        MyVtRiga(35) = ""



        cn.Close()


        If Cmb_CServ.SelectedValue <> "" Then
            For Riga = Tabella.Rows.Count - 1 To 0 Step -1
                If Tabella.Rows(Riga).Item(3).ToString <> Cmb_CServ.SelectedValue Then
                    Tabella.Rows.RemoveAt(Riga)
                End If
            Next
        End If


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)

            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            If Tabella.Rows(e.Row.RowIndex).Item(35).ToString = "ERRORE" Then
                DD_Cserv.BackColor = Drawing.Color.Red
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Red

            End If


            If Tabella.Rows(e.Row.RowIndex).Item(35).ToString = "ERRORECS" Then
                DD_Cserv.BackColor = Drawing.Color.Orange
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = True

                e.Row.BackColor = Drawing.Color.Orange

            End If


            If Tabella.Rows(e.Row.RowIndex).Item(35).ToString = "ERROREPRE" Then
                DD_Cserv.BackColor = Drawing.Color.Aqua
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua

            End If



        End If
    End Sub



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-staging.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try

            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE"))

            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String

            If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
                request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

                guarda = context.Session("ChiaveCr")
            Else
                request.Add("username", context.Session("EPersonamUser"))

                guarda = context.Session("EPersonamPSWCRYPT")
            End If


            request.Add("code", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try

    End Function
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer
        Dim MySql As String

        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Context.Session("DC_OSPITE"))

        cn.Open()

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Diuro As New Cls_Diurno
                Diuro.Anno = 0
                Diuro.CENTROSERVIZIO = DD_Cserv.SelectedValue
                Diuro.CodiceOspite = Val(Tabella.Rows(Riga).Item(36))
                Diuro.Leggi(Context.Session("DC_OSPITE"), Diuro.CodiceOspite, Diuro.CENTROSERVIZIO, Param.AnnoFatturazione, Param.MeseFatturazione)
                If Diuro.Anno = 0 Then
                    Diuro.Giorno1 = "C"
                    Diuro.Giorno2 = "C"
                    Diuro.Giorno3 = "C"
                    Diuro.Giorno4 = "C"
                    Diuro.Giorno5 = "C"
                    Diuro.Giorno6 = "C"
                    Diuro.Giorno7 = "C"
                    Diuro.Giorno8 = "C"
                    Diuro.Giorno9 = "C"
                    Diuro.Giorno10 = "C"
                    Diuro.Giorno11 = "C"
                    Diuro.Giorno12 = "C"
                    Diuro.Giorno13 = "C"
                    Diuro.Giorno14 = "C"
                    Diuro.Giorno15 = "C"
                    Diuro.Giorno16 = "C"
                    Diuro.Giorno17 = "C"
                    Diuro.Giorno18 = "C"
                    Diuro.Giorno19 = "C"
                    Diuro.Giorno20 = "C"
                    Diuro.Giorno21 = "C"
                    Diuro.Giorno22 = "C"
                    Diuro.Giorno23 = "C"
                    Diuro.Giorno24 = "C"
                    Diuro.Giorno25 = "C"
                    Diuro.Giorno26 = "C"
                    Diuro.Giorno27 = "C"
                    Diuro.Giorno28 = "C"
                    Diuro.Giorno29 = "C"
                    Diuro.Giorno30 = "C"
                    Diuro.Giorno31 = "C"
                End If

                Diuro.Anno = Param.AnnoFatturazione
                Diuro.Mese = Param.MeseFatturazione
                Diuro.CENTROSERVIZIO = Diuro.CENTROSERVIZIO
                Diuro.CodiceOspite = Val(Tabella.Rows(Riga).Item(36))


                Diuro.Giorno1 = campodb(Tabella.Rows(Riga).Item(4))
                Diuro.Giorno2 = campodb(Tabella.Rows(Riga).Item(5))
                Diuro.Giorno3 = campodb(Tabella.Rows(Riga).Item(6))
                Diuro.Giorno4 = campodb(Tabella.Rows(Riga).Item(7))
                Diuro.Giorno5 = campodb(Tabella.Rows(Riga).Item(8))
                Diuro.Giorno6 = campodb(Tabella.Rows(Riga).Item(9))
                Diuro.Giorno7 = campodb(Tabella.Rows(Riga).Item(10))
                Diuro.Giorno8 = campodb(Tabella.Rows(Riga).Item(11))
                Diuro.Giorno9 = campodb(Tabella.Rows(Riga).Item(12))
                Diuro.Giorno10 = campodb(Tabella.Rows(Riga).Item(13))
                Diuro.Giorno11 = campodb(Tabella.Rows(Riga).Item(14))
                Diuro.Giorno12 = campodb(Tabella.Rows(Riga).Item(15))
                Diuro.Giorno13 = campodb(Tabella.Rows(Riga).Item(16))
                Diuro.Giorno14 = campodb(Tabella.Rows(Riga).Item(17))
                Diuro.Giorno15 = campodb(Tabella.Rows(Riga).Item(18))
                Diuro.Giorno16 = campodb(Tabella.Rows(Riga).Item(19))
                Diuro.Giorno17 = campodb(Tabella.Rows(Riga).Item(20))
                Diuro.Giorno18 = campodb(Tabella.Rows(Riga).Item(21))
                Diuro.Giorno19 = campodb(Tabella.Rows(Riga).Item(22))
                Diuro.Giorno20 = campodb(Tabella.Rows(Riga).Item(23))
                Diuro.Giorno21 = campodb(Tabella.Rows(Riga).Item(24))
                Diuro.Giorno22 = campodb(Tabella.Rows(Riga).Item(25))
                Diuro.Giorno23 = campodb(Tabella.Rows(Riga).Item(26))
                Diuro.Giorno24 = campodb(Tabella.Rows(Riga).Item(27))
                Diuro.Giorno25 = campodb(Tabella.Rows(Riga).Item(28))
                Diuro.Giorno26 = campodb(Tabella.Rows(Riga).Item(29))
                Diuro.Giorno27 = campodb(Tabella.Rows(Riga).Item(30))
                Diuro.Giorno28 = campodb(Tabella.Rows(Riga).Item(31))
                Diuro.Giorno29 = campodb(Tabella.Rows(Riga).Item(32))
                Diuro.Giorno30 = campodb(Tabella.Rows(Riga).Item(33))
                Diuro.Giorno31 = campodb(Tabella.Rows(Riga).Item(34))



                For i = 1 To 31
                    If campodb(Tabella.Rows(Riga).Item(i + 3)) <> "" Then
                        MySql = "INSERT INTO Diurno_EPersonam (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,CAUSALE) VALUES (?,?,?,?,?,?)"
                        Dim cmdw As New OleDbCommand()
                        cmdw.CommandText = (MySql)

                        cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
                        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                        cmdw.Parameters.AddWithValue("@CentroServizio", Diuro.CENTROSERVIZIO)
                        cmdw.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(36)))
                        cmdw.Parameters.AddWithValue("@Data", DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, i))
                        cmdw.Parameters.AddWithValue("@CAUSALE", Tabella.Rows(Riga).Item(i + 3))
                        cmdw.Connection = cn
                        cmdw.ExecuteNonQuery()
                    End If
                Next
                Diuro.AggiornaDB(Context.Session("DC_OSPITE"))

                CheckBox.Checked = False
            End If
        Next

        cn.Close()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Diurno Importato');", True)

        Call Btn_Movimenti_Click(sender, e)
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ, "D")
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue, "D")
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub



End Class



