﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO


Partial Class OspitiWeb_GestioneDenaro
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()



    Private Sub Pulisci()        
        RB_VERSAMENTO.Checked = True
        RB_PRELIEVO.Checked = False
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_Importo.Text = "0"
        Txt_Descrizione.Text = ""
        DD_Parenti.SelectedValue = 0
        RB_NonRipartito.Checked = True
        RB_Ospite.Checked = False
        RB_Parenti.Checked = False
        dd_TipoAddebito.SelectedValue = ""
        Lbl_Numero.Text = 0

        Dd_MeseCompetenza.SelectedValue = Now.Month
        Txt_AnnoCompetenza.Text = Now.Year
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"




        If Page.IsPostBack = False Then

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")


            Call Pulisci()

            Dim Parametro As New Cls_Parametri

            Parametro.LeggiParametri(Session("DC_OSPITE"))

            txt_descrizione.text = Parametro.DescrizioneDefaultDenaro

            Dim Lp As New Cls_Parenti

            Lp.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_Parenti)

            Dim TipAdd As New Cls_Addebito

            TipAdd.UpDateDropBox(Session("DC_OSPITE"), dd_TipoAddebito)

            Dim d As Integer

            Try
                MyTable = Session("Appoggio")
            Catch ex As Exception

            End Try


            d = Val(Request.Item("CODICE"))


            Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
            Txt_Anno.Text = Val(Request.Item("ANNO"))

            Dd_MeseCompetenza.SelectedValue = Now.Month
            Txt_AnnoCompetenza.Text = Now.Year

            Dim K As New Cls_Denaro



            K.CodiceOspite = Session("CODICEOSPITE")
            K.AnnoMovimento = Val(Request.Item("ANNO"))
            K.NumeroMovimento = Val(Request.Item("CODICE"))

            Lbl_Saldo.Text = Format(K.Saldo(Session("DC_OSPITIACCESSORI")), "#,##0.00")
            K.Leggi(Session("DC_OSPITIACCESSORI"))

            If K.Importo > 0 Then
                Txt_Anno.Text = K.AnnoMovimento
                Lbl_Numero.Text = K.NumeroMovimento

                If Year(K.DataRegistrazione) > 1 Then
                    Txt_DataRegistrazione.Text = Format(K.DataRegistrazione, "dd/MM/yyyy")
                Else
                    Txt_DataRegistrazione.Text = ""
                End If
                If K.TipoOperazione = "V" Then
                    RB_VERSAMENTO.Checked = True
                    RB_PRELIEVO.Checked = False
                Else
                    RB_VERSAMENTO.Checked = False
                    RB_PRELIEVO.Checked = True
                End If
                Txt_Importo.Text = Format(K.Importo, "#,##0.00")
                Txt_Descrizione.Text = K.Descrizione

                RB_NonRipartito.Checked = False
                RB_Ospite.Checked = False
                RB_Parenti.Checked = False
                If K.Ripartizione = "" Then
                    RB_NonRipartito.Checked = True
                End If
                If K.Ripartizione = "O" Then
                    RB_Ospite.Checked = True
                End If
                If K.Ripartizione = "P" Then
                    RB_Parenti.Checked = True
                End If

                DD_Parenti.SelectedValue = K.Parente

                Lbl_Numero.Text = K.NumeroMovimento

                dd_TipoAddebito.SelectedValue = K.TipoAddebito


                Txt_AnnoCompetenza.Text = K.AnnoCompetenza
                Dd_MeseCompetenza.SelectedValue = K.MeseCompetenza


                Dim NomeSocieta As String


                Dim k2 As New Cls_Login

                k2.Utente = Session("UTENTE")
                k2.LeggiSP(Application("SENIOR"))

                NomeSocieta = k2.RagioneSociale

                Try

                    Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & "\Mov" & K.AnnoMovimento & "_" & K.NumeroMovimento)
                    Dim aryItemsInfo() As FileSystemInfo
                    Dim objItem As FileSystemInfo

                    aryItemsInfo = objDI.GetFileSystemInfos()
                    For Each objItem In aryItemsInfo

                        LBL_Allegato.text = "<a href=""" & "..\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\Mov" & K.AnnoMovimento & "_" & K.NumeroMovimento & "\" & objItem.Name & """>" & objItem.Name & "</a>"

                    Next

                Catch ex As Exception

                End Try

            End If

            Call EseguiJS()
        End If
    End Sub








    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If


        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If


        If Val(Txt_Anno.Text) <> Year(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno e data registrazione non coincidenti');", True)
            Exit Sub
        End If



        If CDbl(Txt_Importo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare un importo');", True)
            Exit Sub
        End If

        If RB_Parenti.Checked = True And DD_Parenti.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi selezionare un parente');", True)
            Exit Sub
        End If

        Dim KS As New Cls_Denaro

        KS.AnnoMovimento = Txt_Anno.Text

        KS.NumeroMovimento = Val(Lbl_Numero.Text)

        If RB_VERSAMENTO.Checked = True Then
            KS.TipoOperazione = "V"
        Else
            KS.TipoOperazione = "P"
        End If
        KS.CodiceOspite = Session("CODICEOSPITE")
        KS.DataRegistrazione = Txt_DataRegistrazione.Text
        KS.Descrizione = Txt_Descrizione.Text
        KS.Importo = Txt_Importo.Text
        KS.Parente = 0
        If RB_NonRipartito.Checked = True Then
            KS.Ripartizione = ""
        End If
        If RB_Ospite.Checked = True Then
            KS.Ripartizione = "O"
        End If
        If RB_Parenti.Checked = True Then
            KS.Ripartizione = "P"
            KS.Parente = DD_Parenti.SelectedValue
        End If

        KS.TipoAddebito = dd_TipoAddebito.SelectedValue
        KS.Utente = Session("UTENTE")

        KS.AnnoCompetenza = Txt_AnnoCompetenza.Text
        KS.MeseCompetenza = Dd_MeseCompetenza.SelectedValue

        KS.AggiornaDB(Session("DC_OSPITIACCESSORI"))


        If FileUpload1.FileName.Trim <> "" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale


            Dim Appo As String

            Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
            End If


            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")), FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")))
            End If

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\Mov" & KS.AnnoMovimento & "_" & KS.NumeroMovimento, FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\Mov" & KS.AnnoMovimento & "_" & KS.NumeroMovimento)
            End If

            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\Mov" & KS.AnnoMovimento & "_" & KS.NumeroMovimento & "\" & FileUpload1.FileName)

        End If

        Response.Redirect("ElencoMovimentiDenaro.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        Dim KS As New Cls_Denaro

        KS.AnnoMovimento = Txt_Anno.Text

        KS.NumeroMovimento = Val(Lbl_Numero.Text)

        If RB_VERSAMENTO.Checked = True Then
            KS.TipoOperazione = "V"
        Else
            KS.TipoOperazione = "P"
        End If
        KS.CodiceOspite = Session("CODICEOSPITE")

        KS.Elimina(Session("DC_OSPITIACCESSORI"))

        Response.Redirect("ElencoMovimentiDenaro.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoMovimentiDenaro.aspx?CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        
        

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class
