﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GeneraAllegati" CodeFile="GeneraAllegati.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Genera Allegati</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
    <style>
        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript"> 
        function verificastao() {
            var url = "interrogaparametri.ashx?";
            url += "&casuale=" + Math.floor(Math.random() * 1000);

            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari    
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5    
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.responseText != '') {
                        document.getElementById("msg").innerHTML = '<font color=white>Elaborando :' + xmlhttp.responseText + '</font>';
                    }
                }
            }
            xmlhttp.open("GET", url, true);
            xmlhttp.send(null);
        }


        var t = setInterval("verificastao()", 1000);

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function Stampa() {
            if ($("#OpzioniStampa").css("visibility") != "hidden") {
                $("#OpzioniStampa").css('visibility', 'hidden');
                localStorage.setItem("OpzioniStampa", "invisibile");
            } else {
                $("#OpzioniStampa").css('visibility', 'visible');
                localStorage.setItem("OpzioniStampa", "visibile");
            }
        }

        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Genera Allegati</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Stampa Sanitaria" ID="ImageButton1"></asp:ImageButton>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />


                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Genera Allegati
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="80px" MaxLength="4"></asp:TextBox>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Mese Da :</label>
                                    <asp:DropDownList ID="DD_MeseDa" runat="server" Width="168px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Mese A :</label>
                                    <asp:DropDownList ID="DD_MeseA" runat="server" Width="168px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="True"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Centro Servizio :</label>
                                    <asp:DropDownList ID="DD_CServ" runat="server" Width="354px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Regione :</label>
                                    <asp:DropDownList ID="DD_Regione" runat="server" class="chosen-select" Width="354px"></asp:DropDownList><br />
                                    <br />


                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />

                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                    <br />
                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                                        AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <div id="blur">&nbsp;</div>
                                            <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                                                Attendere prego.....<br />
                                                <img height="30px" src="images/loading.gif">
                                            </div>

                                        </ProgressTemplate>
                                    </asp:UpdateProgress>


                                </ContentTemplate>


                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
