﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class OspitiWeb_Stampa_ReportXSD
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        With CrystalReportViewer1
            .HasDrillUpButton = True
            .HasExportButton = True
            .HasGotoPageButton = True
            .HasPageNavigationButtons = True
            .HasPrintButton = True
            .HasRefreshButton = True
            .HasSearchButton = True
            .HasToggleGroupTreeButton = True
            .HasViewList = True
            .HasZoomFactorList = True
        End With
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        REM If Page.IsPostBack = True Then Exit Sub

        Dim XS As New Cls_Login

        Dim rpt As New ReportDocument


        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))
        'x = Session("stampa")

        If XS.ReportPersonalizzato(Request.Item("REPORT")) = "" Then
            'window.close()

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Report non indicato'); window.close();", True)

            Exit Sub
        End If

        Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT"))


        If Not System.IO.File.Exists(NomeSt) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Report non presente'); window.close();", True)

            Exit Sub
        End If

        Try
            rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT")))
        Catch ex As Exception
        End Try



        If Request.Item("FORMULA") = "SI" Then
            rpt.RecordSelectionFormula = Session("SelectionFormula")
        End If


        Try
            rpt.SetDataSource(Session("stampa"))
        Catch ex As Exception

        End Try


        If Request.Item("EXPORT") = "PDF" Then
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Request.Item("NOME"))
            Exit Sub
        End If


        CrystalReportViewer1.DisplayGroupTree = False

        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.RefreshReport()

        CrystalReportViewer1.ReuseParameterValuesOnRefresh = True


        CrystalReportViewer1.Visible = True

                

    End Sub
End Class
