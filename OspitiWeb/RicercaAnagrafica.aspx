﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="RicercaAnagrafica" CodeFile="RicercaAnagrafica.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Ricerca</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">  
        var timeout = 0;
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() + 1 >= $(document).height()) {
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    $("#Img_Espandi").trigger("click");
                }, 50);
            }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Ricerca</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td style="color: #565151;">Struttura:</td>
                                            <td style="color: #565151;">
                                                <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList></td>
                                            <td style="color: #565151;">Centro Servizio:</td>
                                            <td style="color: #565151;">
                                                <asp:DropDownList runat="server" ID="DD_CServ"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #565151;">Codice Ospite :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_CodiceOspite" onkeypress="return soloNumeri(event);" runat="server" Width="120px"></asp:TextBox><br />
                                            </td>
                                            <td style="color: #565151;">Cognome Nome :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_CognomeNome" runat="server" Width="440px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #565151;">Includi Non In Uso:<asp:CheckBox ID="Chk_NonInUso" runat="server" Text="" /></td>
                                            <td style="color: #565151;" colspan="2">Ordina per Cod.Ospite Desc:<asp:CheckBox ID="Chk_OrdinePerCodice" runat="server" Text="" /></td>
                                        </tr>

                                    </table>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Esci" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="Btn_Nuovo" Style="border-width: 0px;" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi Ospite" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="Grd_ImportoOspite" runat="server" CellPadding="3"
                                    Width="100%" BackColor="White" BorderColor="#008CD6" BorderStyle="None"
                                    BorderWidth="1px" GridLines="Horizontal" AllowPaging="True"
                                    PageSize="1210">
                                    <RowStyle ForeColor="#000000" BackColor="#EEEEEE" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%#   Container.DataItemIndex  %>" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="False" ForeColor="White" HorizontalAlign="Left" />
                                    <HeaderStyle BackColor="#F9F9F9" Font-Size="16px" ForeColor="#000000" BorderColor="#008CD6" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                </asp:GridView>
                                <asp:ImageButton ID="Img_Espandi" runat="server" ImageUrl="../images/Blanco.png" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
