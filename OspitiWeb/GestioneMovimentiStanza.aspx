﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GestioneMovimentiStanza" CodeFile="GestioneMovimentiStanza.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Movimenti Stanze</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script type="text/javascript">  
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Ospiti - Gestione Movimento Stanza</div>
                    <div class="SottoTitoloOSPITE">
                        <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right;">
                    <div class="DivTastiOspite">
                        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                    </div>
                </td>
            </tr>

            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    <br />
                    <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsStanze.aspx','Stampe','width=450,height=600,scrollbars=yes');"><img src="images/arrow.gif" />Stanze</a></label>
                    <br />
                    <div id="MENUDIV"></div>
                    <br />
                </td>
                <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                    <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                        Width="100%" BorderStyle="None" Style="margin-right: 39px">
                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                            <HeaderTemplate>
                                Gestione Movimenti Stanze         
                            </HeaderTemplate>
                            <ContentTemplate>

                                <br />

                                <label style="display: block; float: left; width: 160px;">Villa :</label>
                                <asp:DropDownList ID="DD_Villa" runat="server"></asp:DropDownList><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Reparto:</label>
                                <asp:DropDownList ID="Dd_Reparto" runat="server"></asp:DropDownList><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Piano</label>
                                <asp:TextBox ID="Txt_Piano" runat="server" MaxLength="2" Width="100px"></asp:TextBox><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Stanza</label>
                                <asp:TextBox ID="Txt_Stanza" runat="server" MaxLength="2" Width="100px"></asp:TextBox><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Letto</label>
                                <asp:TextBox ID="Txt_Letto" runat="server" MaxLength="10" Width="100px"></asp:TextBox><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Data</label>
                                <asp:TextBox ID="Txt_Data" runat="server" MaxLength="2" Width="90px"></asp:TextBox><br />
                                <br />

                                <label style="display: block; float: left; width: 160px;">Tipo Movimento</label>
                                <asp:RadioButton runat="server" ID="DD_Occupazione" Text="Occupazione" runat="server" GroupName="TIPO"></asp:RadioButton>
                                <asp:RadioButton runat="server" ID="DD_Liberazione" Text="Liberazione" runat="server" GroupName="TIPO"></asp:RadioButton>
                                <br />
                                <br />
                            </ContentTemplate>
                        </xasp:TabPanel>
                    </xasp:TabContainer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
