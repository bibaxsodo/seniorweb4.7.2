﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_GestioneListino
    Inherits System.Web.UI.Page




    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Seleziona"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)


        If Request.Item("TIPOLISTINO") = "S" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)


        End If

        If Request.Item("TIPOLISTINO") = "C" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSociale", "$(document).ready(function(){  OpenSociale() }); ", True)

            Pan_GiorniSettimana.Visible = False
        End If

        If Request.Item("TIPOLISTINO") = "P" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)


            Pan_OspiteSOC.Visible = False

            Pan_GiorniSettimana.Visible = False

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSociale", "$(document).ready(function(){  OpenOspite() }); ", True)
        End If

        If Request.Item("TIPOLISTINO") = "J" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)


            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenJolly", "$(document).ready(function(){  OpenJolly() }); ", True)

            Pan_GiorniSettimana.Visible = False
        End If

        If Request.Item("TIPOLISTINO") = "R" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSanitario", "$(document).ready(function(){  OpenSanitario() }); ", True)
            Pan_GiorniSettimana.Visible = False
        End If
    End Sub

    Private Sub ApriRettaTotale()
        Dim MyJs As String

        EseguiJS()

        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "OpenRettaTotale();"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ApriRettaTotale", MyJs, True)
    End Sub

    Protected Sub OspitiWeb_GestioneListino_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        Txt_ImportoRettaTotale.Text = "0,00"


        Txt_Jolly.Text = "0,00"



        Txt_ImportoOspite.Text = "0,00"
        Txt_ImportoOspite2.Text = "0,00"


        Txt_ImportoSociale.Text = "0,00"



        Dim MPag As New Cls_ModalitaPagamento


        MPag.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPagamento)


        Dim NOper As New Cls_TipoOperazione

        NOper.UpDateDropBoxTipo(Session("DC_OSPITE"), DD_TipoOperazione, "O")

        Dim NIva As New Cls_IVA

        NIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

        Dim TipoRetta As New Cls_TipoRetta

        TipoRetta.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)


        Dim Nregione As New ClsUSL

        Nregione.UpDateDropBox(Session("DC_OSPITE"), DD_Usl)
        

        Dim MyE As New Cls_TipoExtraFisso

        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso1)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso2)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso3)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso4)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso5)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso6)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso7)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso8)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso9)
        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_ExtraFisso10)

        Dim TipoRettaUSl As New Cls_TabellaTipoImportoRegione

        TipoRettaUSl.UpDateDropBox(Session("DC_OSPITE"), DD_SanitarioTipoRetta)
        DD_SanitarioTipoRetta.Items.Add("")
        DD_SanitarioTipoRetta.Items(DD_SanitarioTipoRetta.Items.Count - 1).Value = ""



        Dim K As New ClsComune

        K.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)

        K.UpDateDropBox(Session("DC_OSPITE"), DD_ComuneJolly)

        If Request.Item("TIPOLISTINO") = "S" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)


            RB_Ospite.Checked = True
            RB_Comune.Enabled = False
            RB_Ente.Enabled = False
            'RB_Parente.Enabled = False

            Txt_ImportoOspite.Enabled = False
            DD_TipoRettaOspite.Enabled = False


            Txt_ImportoOspite2.Visible = False
            Lbl_ImportoOspite2.Visible = False
        End If
        If Request.Item("TIPOLISTINO") = "P" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)


            Txt_ImportoOspite.Enabled = False
            DD_TipoRettaOspite.Enabled = False
            DD_TipoOperazione.Enabled = False
            Chk_Anticipata.Enabled = False
            DD_ModalitaPagamento.Enabled = False
            DD_IVA.Enabled = False
            RB_NO.Enabled = False
            RB_SI.Enabled = False

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSociale", "$(document).ready(function(){  OpenOspite() }); ", True)
        End If


        If Request.Item("TIPOLISTINO") = "C" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSociale", "$(document).ready(function(){  OpenSociale() }); ", True)
        End If

        If Request.Item("TIPOLISTINO") = "J" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenJolly", "$(document).ready(function(){  OpenJolly() }); ", True)
        End If

        If Request.Item("TIPOLISTINO") = "R" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaTotale", "$(document).ready(function(){  $(""#IdRettaTotale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaOspite", "$(document).ready(function(){  $(""#IdRettaOspite"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenSanitario", "$(document).ready(function(){  OpenSanitario() }); ", True)

            Img_SanitariaPrivata.Visible = True
        End If

        Txt_Codice.Text = Request.Item("CODICE")
        Dim Lisitino As New Cls_Tabella_Listino

        Lisitino.Codice = Txt_Codice.Text
        Lisitino.LeggiCausale(Session("DC_OSPITE"))

        If Txt_Codice.Text = "" Then


            Dim MaxListino As New Cls_Tabella_Listino

            Txt_Codice.Text = MaxListino.MaxTipoListino(Session("DC_OSPITE"))
        End If





        If Lisitino.Descrizione <> "" Then

            Txt_CodiceEPersonam.Text = Lisitino.CodiceEpersonam

            Txt_Codice.Enabled = False
            Txt_Descrizione.Text = Lisitino.Descrizione
            Txt_ImportoRettaTotale.Text = Format(Lisitino.IMPORTORETTATOTALE, "#,##0.00")
            DD_TipoRettaTotale.SelectedValue = Lisitino.TIPORETTATOTALE

            If Year(Lisitino.DATA) > 1900 Then
                Txt_DataScadenza.Text = Format(Lisitino.DATA, "dd/MM/yyyy")
            Else
                Txt_DataScadenza.Text = ""
            End If

            DD_TipoRetta.SelectedValue = Lisitino.TipoRetta

            If Lisitino.MODALITA = "O" Then
                RB_Ospite.Checked = True
            End If

            If Lisitino.MODALITA = "P" Then
                RB_Parente.Checked = True
            End If

            If Lisitino.MODALITA = "C" Then
                RB_Comune.Checked = True
            End If

            If Lisitino.MODALITA = "E" Then
                RB_Ente.Checked = True
            End If

            DD_ExtraFisso1.SelectedValue = Lisitino.CODICEEXTRA1
            DD_ExtraFisso2.SelectedValue = Lisitino.CODICEEXTRA2
            DD_ExtraFisso3.SelectedValue = Lisitino.CODICEEXTRA3
            DD_ExtraFisso4.SelectedValue = Lisitino.CODICEEXTRA4
            DD_ExtraFisso5.SelectedValue = Lisitino.CODICEEXTRA5
            DD_ExtraFisso6.SelectedValue = Lisitino.CODICEEXTRA6
            DD_ExtraFisso7.SelectedValue = Lisitino.CODICEEXTRA7
            DD_ExtraFisso8.SelectedValue = Lisitino.CODICEEXTRA8
            DD_ExtraFisso9.SelectedValue = Lisitino.CODICEEXTRA9
            DD_ExtraFisso10.SelectedValue = Lisitino.CODICEEXTRA10
            If DD_ExtraFisso2.SelectedValue <> "" Then
                DD_ExtraFisso2.Visible = True
                Btn_Plus2.Visible = True
                Btn_Plus1.Visible = False
            End If
            If DD_ExtraFisso3.SelectedValue <> "" Then
                DD_ExtraFisso3.Visible = True
                Btn_Plus3.Visible = True
                Btn_Plus2.Visible = False
            End If
            If DD_ExtraFisso4.SelectedValue <> "" Then
                DD_ExtraFisso4.Visible = True
                Btn_Plus4.Visible = True
                Btn_Plus3.Visible = False
            End If
            If DD_ExtraFisso5.SelectedValue <> "" Then
                DD_ExtraFisso5.Visible = True
                Btn_Plus5.Visible = True
                Btn_Plus4.Visible = False
            End If
            If DD_ExtraFisso6.SelectedValue <> "" Then
                DD_ExtraFisso6.Visible = True
                Btn_Plus6.Visible = True
                Btn_Plus5.Visible = False
            End If
            If DD_ExtraFisso7.SelectedValue <> "" Then
                DD_ExtraFisso7.Visible = True
                Btn_Plus7.Visible = True
                Btn_Plus6.Visible = False
            End If
            If DD_ExtraFisso8.SelectedValue <> "" Then
                DD_ExtraFisso8.Visible = True
                Btn_Plus8.Visible = True
                Btn_Plus7.Visible = False
            End If
            If DD_ExtraFisso9.SelectedValue <> "" Then
                DD_ExtraFisso9.Visible = True
                Btn_Plus9.Visible = True
                Btn_Plus8.Visible = False
            End If
            If DD_ExtraFisso10.SelectedValue <> "" Then
                DD_ExtraFisso10.Visible = True
                Btn_Plus10.Visible = True
                Btn_Plus9.Visible = False
            End If


            DD_IVA.SelectedValue = Lisitino.CODICEIVA
            DD_ModalitaPagamento.SelectedValue = Lisitino.MODALITAPAGAMENTO
            DD_TipoOperazione.SelectedValue = Lisitino.TIPOOPERAIZONE

            DD_Struttura.SelectedValue = Lisitino.Struttura
            AggiornaCServ()

            Dim KCserv As New Cls_CentroServizio

            KCserv.CENTROSERVIZIO = Lisitino.CENTROSERVIZIO
            KCserv.Leggi(Session("DC_OSPITE"), Lisitino.CENTROSERVIZIO)
            If KCserv.DESCRIZIONE <> "" Then

                DD_Struttura.SelectedValue = KCserv.Villa
                AggiornaCServ()
                Cmb_CServ.SelectedValue = Lisitino.CENTROSERVIZIO
            End If

            If Lisitino.COMPENSAZIONE = "S" Then
                RB_SI.Checked = True
            End If
            If Lisitino.COMPENSAZIONE = "N" Then
                RB_NO.Checked = True
            End If
            If Lisitino.COMPENSAZIONE = "D" Then
                RB_Dettaglio.Checked = True
            End If


            Dim appoggio As String


            appoggio = Lisitino.SETTIMANA & Space(7)

            If Mid(appoggio, 1, 1) = "A" Then
                Chk_Lunedi.Checked = True
            End If
            If Mid(appoggio, 2, 1) = "A" Then
                Chk_Martedi.Checked = True
            End If
            If Mid(appoggio, 3, 1) = "A" Then
                Chk_Mercoledi.Checked = True
            End If
            If Mid(appoggio, 4, 1) = "A" Then
                Chk_Giovedi.Checked = True
            End If
            If Mid(appoggio, 5, 1) = "A" Then
                Chk_Venerdi.Checked = True
            End If
            If Mid(appoggio, 6, 1) = "A" Then
                Chk_Sabato.Checked = True
            End If
            If Mid(appoggio, 7, 1) = "A" Then
                Chk_Domenica.Checked = True
            End If


            RB_Mensile.Checked = False
            RB_Bimestrale.Checked = False
            RB_Trimestrale.Checked = False
            RB_FinePeriodoAss.Checked = False

            If Lisitino.PeriodoFatturazione = "M" Then
                RB_Mensile.Checked = True
            End If

            If Lisitino.PeriodoFatturazione = "B" Then
                RB_Bimestrale.Checked = True
            End If

            If Lisitino.PeriodoFatturazione = "T" Then
                RB_Trimestrale.Checked = True
            End If

            If Lisitino.PeriodoFatturazione = "F" Then
                RB_FinePeriodoAss.Checked = True
            End If



            If Lisitino.ANTICIPATA = "S" Then
                Chk_Anticipata.Checked = True
            Else
                Chk_Anticipata.Checked = False
            End If


            Txt_ImportoOspite.Text = Format(Lisitino.IMPORTORETTTAOSPITE1, "#,##0.00")
            Txt_ImportoOspite2.Text = Format(Lisitino.IMPORTORETTTAOSPITE2, "#,##0.00")
            DD_TipoRettaOspite.SelectedValue = Lisitino.TIPORETTTAOSPITE


            DD_Usl.SelectedValue = Lisitino.USL

            DD_SanitarioTipoRetta.SelectedValue = Lisitino.TIPORETTAUSL

            DD_Comune.SelectedValue = Lisitino.SOCIALEPROVINCIA & " " & Lisitino.SOCIALECOMUNE


            Txt_ImportoSociale.Text = Lisitino.IMPORTORETTTASOCIALE

            DD_TipoRettaSociale.SelectedValue = Lisitino.TIPORETTASOCIALE



            DD_ComuneJolly.SelectedValue = Lisitino.JOLLYPROVINCIA & " " & Lisitino.JOLLYCOMUNE

            Txt_Jolly.Text = Lisitino.IMPORTORETTTAJOLLY

            DD_TipoRettaJolly.SelectedValue = Lisitino.TIPORETTAJOLLY


            If Lisitino.USLDACOMUNERESIDENZA = 1 Then
                Chk_CodRegDaComRes.Checked = True
            Else
                Chk_CodRegDaComRes.Checked = False
            End If

            Txt_ImportoRicoveriMaggioreZero.Text = format(Lisitino.ImportoRicoveriMaggioreZero, "#,##0.00")
            txt_GiornateSoglia.Text= Lisitino.GiornateSoglia 
            txt_ImportoIncremento.Text =format(Lisitino.ImportoIncremento, "#,##0.00")
            Txt_ImportoDiurno.Text = format(Lisitino.ImportoDiurno, "#,##0.00")
        End If


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AlberghieroAssistenziale = 1 Then
            Lbl_ImportoOspite1.Text = "Importo Alberghiero"
            Lbl_ImportoOspite2.Text = "Importo Assistenziale"
        End If
        If Param.SocialeSanitario = 1 Then
            Lbl_ImportoOspite1.Text = "Importo Sociale"
            Lbl_ImportoOspite2.Text = "Importo Sanitario"
        End If
        If Param.AlberghieroAssistenziale = 0 And Param.SocialeSanitario = 0 Then
            Lbl_ImportoOspite2.Visible = False
            Txt_ImportoOspite2.Visible = False
            Lbl_ImportoOspite1.Text = "Importo"
        End If



        Call EseguiJS()
    End Sub

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare un codice listino');", True)
            EseguiJS()
            Exit Sub
        End If

        Dim VettoreVerifica(10) As String

        VettoreVerifica(1) = DD_ExtraFisso1.SelectedValue
        VettoreVerifica(2) = DD_ExtraFisso2.SelectedValue
        VettoreVerifica(3) = DD_ExtraFisso3.SelectedValue
        VettoreVerifica(4) = DD_ExtraFisso4.SelectedValue
        VettoreVerifica(5) = DD_ExtraFisso5.SelectedValue
        VettoreVerifica(6) = DD_ExtraFisso6.SelectedValue
        VettoreVerifica(7) = DD_ExtraFisso7.SelectedValue
        VettoreVerifica(8) = DD_ExtraFisso8.SelectedValue
        VettoreVerifica(9) = DD_ExtraFisso9.SelectedValue
        VettoreVerifica(10) = DD_ExtraFisso10.SelectedValue

        Dim Indice As Integer
        Dim IndiceInterno As Integer

        For Indice = 1 To 10
            For IndiceInterno = 1 To 10
                If Indice <> IndiceInterno Then
                    If VettoreVerifica(Indice) <> "" Then
                        If VettoreVerifica(Indice) = VettoreVerifica(IndiceInterno) Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi usare due extra uguali');", True)
                            ApriRettaTotale()
                            EseguiJS()
                            Exit Sub
                        End If
                    End If
                End If
            Next
        Next


        If Txt_ImportoOspite.Text.Trim = "" Then
            Txt_ImportoOspite.Text = "0"
        End If
        If Txt_ImportoSociale.Text.Trim = "" Then
            Txt_ImportoSociale.Text = "0"
        End If
        If Txt_Jolly.Text.Trim = "" Then
            Txt_Jolly.Text = "0"
        End If


        If RB_Ospite.Checked = True Then
            
            If CDbl(Txt_ImportoOspite.Text) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare modalità ospite e indicare importo su ospite');", True)
                EseguiJS()

                Exit Sub
            End If
        End If

        If RB_Comune.Checked = True Then
            
            If CDbl(Txt_ImportoSociale.Text) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare modalità comune e indicare importo su comue');", True)
                EseguiJS()

                Exit Sub
            End If
            If DD_Comune.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare modalità comune e non indicare il comue');", True)
                EseguiJS()

                Exit Sub
            End If
        End If

        If CDbl(Txt_ImportoSociale.Text) > 0 Then
            If DD_Comune.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare importo sul comune e non indicare il comue');", True)
                EseguiJS()

                Exit Sub
            End If
        End If

        If CDbl(Txt_Jolly.Text) > 0 Then
            If DD_ComuneJolly.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare importo su jolly e non indicare quale ente');", True)
                EseguiJS()


                Exit Sub
            End If
        End If

        Dim Lisitino As New Cls_Tabella_Listino





        Lisitino.Codice = Txt_Codice.Text
        Lisitino.Descrizione = Txt_Descrizione.Text
        Lisitino.IMPORTORETTATOTALE = Txt_ImportoRettaTotale.Text
        Lisitino.TIPORETTATOTALE = DD_TipoRettaTotale.SelectedValue

        Lisitino.TipoRetta = DD_TipoRetta.SelectedValue


        Lisitino.TIPOLISTINO = Request.Item("TIPOLISTINO")

        If RB_Ospite.Checked = True Then
            Lisitino.MODALITA = "O"
        End If

        If RB_Parente.Checked = True Then
            Lisitino.MODALITA = "P"
        End If

        If RB_Comune.Checked = True Then
            Lisitino.MODALITA = "C"
        End If

        If RB_Ente.Checked = True Then
            Lisitino.MODALITA = "E"
        End If

        Lisitino.CODICEEXTRA1 = DD_ExtraFisso1.SelectedValue
        Lisitino.CODICEEXTRA2 = DD_ExtraFisso2.SelectedValue
        Lisitino.CODICEEXTRA3 = DD_ExtraFisso3.SelectedValue
        Lisitino.CODICEEXTRA4 = DD_ExtraFisso4.SelectedValue
        Lisitino.CODICEEXTRA5 = DD_ExtraFisso5.SelectedValue

        Lisitino.DATA = IIf(IsDate(Txt_DataScadenza.Text), Txt_DataScadenza.Text, Nothing)

        Lisitino.CODICEEXTRA6 = DD_ExtraFisso6.SelectedValue
        Lisitino.CODICEEXTRA7 = DD_ExtraFisso7.SelectedValue
        Lisitino.CODICEEXTRA8 = DD_ExtraFisso8.SelectedValue
        Lisitino.CODICEEXTRA9 = DD_ExtraFisso9.SelectedValue
        Lisitino.CODICEEXTRA10 = DD_ExtraFisso10.SelectedValue

        Lisitino.CODICEIVA = DD_IVA.SelectedValue
        Lisitino.MODALITAPAGAMENTO = DD_ModalitaPagamento.SelectedValue
        Lisitino.TIPOOPERAIZONE = DD_TipoOperazione.SelectedValue

        
        If RB_Mensile.Checked = True Then
            Lisitino.PeriodoFatturazione = "M"
        End If

        If RB_Bimestrale.Checked = True Then
            Lisitino.PeriodoFatturazione = "B"
        End If

        If RB_Trimestrale.Checked = True Then
            Lisitino.PeriodoFatturazione = "T"
        End If

        If RB_FinePeriodoAss.Checked = True Then
            Lisitino.PeriodoFatturazione = "F"
        End If



        Lisitino.CodiceEpersonam = Val(Txt_CodiceEPersonam.Text)



        Lisitino.CENTROSERVIZIO = Cmb_CServ.SelectedValue

        Lisitino.Struttura = DD_Struttura.SelectedValue


        If RB_SI.Checked = True Then
            Lisitino.COMPENSAZIONE = "S"
        End If
        If RB_NO.Checked = True Then
            Lisitino.COMPENSAZIONE = "N"
        End If
        If RB_Dettaglio.Checked = True Then
            Lisitino.COMPENSAZIONE = "D"
        End If
        Dim appoggio As String


        appoggio = ""
        If Chk_Lunedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Martedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Mercoledi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Giovedi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Venerdi.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If
        If Chk_Sabato.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If

        If Chk_Domenica.Checked = True Then
            appoggio = appoggio & "A"
        Else
            appoggio = appoggio & " "
        End If

        Lisitino.SETTIMANA = appoggio

        If Chk_Anticipata.Checked = True Then
            Lisitino.ANTICIPATA = "S"
        Else
            Lisitino.ANTICIPATA = ""
        End If


        Lisitino.IMPORTORETTTAOSPITE1 = Txt_ImportoOspite.Text
        Lisitino.IMPORTORETTTAOSPITE2 = Txt_ImportoOspite2.Text
        Lisitino.TIPORETTTAOSPITE = DD_TipoRettaOspite.SelectedValue


        Lisitino.USL = DD_Usl.SelectedValue

        Lisitino.TIPORETTAUSL = DD_SanitarioTipoRetta.SelectedValue

        Dim Vettore(100) As String
        appoggio = DD_Comune.SelectedValue

        Vettore = SplitWords(appoggio)
        If Vettore.Length >= 2 Then
            Lisitino.SOCIALEPROVINCIA = Vettore(0)
            Lisitino.SOCIALECOMUNE = Vettore(1)
        End If


        Lisitino.IMPORTORETTTASOCIALE = Txt_ImportoSociale.Text

        Lisitino.TIPORETTASOCIALE = DD_TipoRettaSociale.SelectedValue



        appoggio = DD_ComuneJolly.SelectedValue

        Vettore = SplitWords(appoggio)
        If Vettore.Length >= 2 Then
            Lisitino.JOLLYPROVINCIA = Vettore(0)
            Lisitino.JOLLYCOMUNE = Vettore(1)
        End If



        If Chk_CodRegDaComRes.Checked = True Then
            Lisitino.USLDACOMUNERESIDENZA = 1
        Else
            Lisitino.USLDACOMUNERESIDENZA = 0
        End If


        Lisitino.IMPORTORETTTAJOLLY = Txt_Jolly.Text

        Lisitino.TIPORETTAJOLLY = DD_TipoRettaJolly.SelectedValue


        Lisitino.ImportoRicoveriMaggioreZero  =Txt_ImportoRicoveriMaggioreZero.Text 
        Lisitino.GiornateSoglia  = txt_GiornateSoglia.Text
        Lisitino.ImportoIncremento  =txt_ImportoIncremento.Text
        Lisitino.ImportoDiurno  =Txt_ImportoDiurno.Text


        Lisitino.Scrivi(Session("DC_OSPITE"))

        Response.Redirect("Elenco_Listino.aspx")

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Listino.aspx")

    End Sub

    Protected Sub Btn_Plus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus1.Click
        Btn_Plus1.Visible = False
        Btn_Plus2.Visible = True
        DD_ExtraFisso2.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus2.Click
        Btn_Plus2.Visible = False
        Btn_Plus3.Visible = True
        DD_ExtraFisso3.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus3.Click
        Btn_Plus3.Visible = False
        Btn_Plus4.Visible = True
        DD_ExtraFisso4.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus4.Click
        Btn_Plus4.Visible = False
        Btn_Plus5.Visible = True
        DD_ExtraFisso5.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus5.Click
        Btn_Plus5.Visible = False
        Btn_Plus6.Visible = True
        DD_ExtraFisso6.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus6.Click
        Btn_Plus6.Visible = False
        Btn_Plus7.Visible = True
        DD_ExtraFisso7.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus7.Click
        Btn_Plus7.Visible = False
        Btn_Plus8.Visible = True
        DD_ExtraFisso8.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus8.Click
        Btn_Plus8.Visible = False
        Btn_Plus9.Visible = True
        DD_ExtraFisso9.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Btn_Plus9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus9.Click
        Btn_Plus9.Visible = False
        Btn_Plus10.Visible = True
        DD_ExtraFisso10.Visible = True
        ApriRettaTotale()
    End Sub

    Protected Sub Imb_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Duplica.Click
        Txt_Codice.Text = ""
        If Request.Item("TIPOLISTINO") = "S" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSanitario", "$(document).ready(function(){  $(""#IdRettaSanitario"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaSociale", "$(document).ready(function(){  $(""#IdRettaSociale"").css('visibility', 'hidden'); }); ", True)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdRettaJolly", "$(document).ready(function(){  $(""#IdRettaJolly"").css('visibility', 'hidden'); }); ", True)


            RB_Ospite.Checked = True
            RB_Comune.Enabled = False
            RB_Ente.Enabled = False
            'RB_Parente.Enabled = False

            Txt_ImportoOspite.Enabled = False
            DD_TipoRettaOspite.Enabled = False
        End If
        Txt_Codice.Enabled = True


        Dim MaxListino As New Cls_Tabella_Listino

        Txt_Codice.Text = MaxListino.MaxTipoListino(Session("DC_OSPITE"))

        Txt_Codice_TextChanged(sender, e)
        Txt_Descrizione_TextChanged(sender, e)

        EseguiJS()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click

        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from Listino Where CodiceListino = '" & Txt_Codice.Text & "' OR CodiceListinoSanitario = '" & Txt_Codice.Text & "' OR CodiceListinoSociale = '" & Txt_Codice.Text & "' OR CodiceListinoJolly = '" & Txt_Codice.Text & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Listino presente su alcuni ospiti non posso eliminare');", True)
            EseguiJS()

            Exit Sub
        End If
        cn.Close()

        Dim L As New Cls_Tabella_Listino


        L.Codice = Txt_Codice.Text
        L.Elimina(Session("DC_OSPITE"))


        Response.Redirect("Elenco_Listino.aspx")

    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Protected Sub Img_SanitariaPrivata_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_SanitariaPrivata.Click
        Dim Codice As String = ""
        Codice = Request.Item("CODICE")

        Response.Redirect("GestioneListino.aspx?CODICE=" & Codice & "&TIPOLISTINO=P")
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCod.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Tabella_Listino

            x.Codice = Txt_Codice.Text
            x.LeggiCausale(Session("DC_OSPITE"))

            If x.Descrizione <> "" Then
                Img_VerificaCod.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub



    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Tabella_Listino

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text
            x.LeggiDescrizione(Session("DC_OSPITE"))

            If x.Codice <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub
End Class



