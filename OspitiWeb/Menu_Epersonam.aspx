﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Epersonam" CodeFile="Menu_Epersonam.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Menu ePersonam</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 60%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - ePersonam</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 100%;">


                            <tr>

                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_AllineDb" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_ImportaMovimenti" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_Diurno" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_Domiciliare" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_Extra" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                            </tr>

                            <tr>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ANAGRAFICHE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOVIMENTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DIURNO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DOMICILIARE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="lblExtra" runat="SERVER" Text=""></asp:Label></span></td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>



                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_Listino" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_ListinoAggironamento" runat="server" ImageUrl="../images/Menu_EPersonam.PNG" class="Effetto" Style="border-width: 0;" />
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Listino" runat="SERVER" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_ListinoAggironamento" runat="SERVER" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
