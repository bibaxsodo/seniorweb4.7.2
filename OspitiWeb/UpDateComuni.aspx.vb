﻿
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes


Partial Class OspitiWeb_UpDateComuni
    Inherits System.Web.UI.Page

    Protected Sub Img_AllineaComuni_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_AllineaComuni.Click
        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Application("SENIOR")

        cn = New Data.OleDb.OleDbConnection(ConnectionString.Replace("=SENIOR;", "=COMUNI;"))

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Comuni Order By Codice"
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim appoggio As String
            Dim Comuni As String
            Dim Provincia As String

            appoggio = campodb(myPOSTreader.Item("Codice"))
            If Len(appoggio) = 5 Then
                appoggio = "0" & appoggio
            End If
            If Len(appoggio) = 4 Then
                appoggio = "00" & appoggio
            End If

            Provincia = Mid(appoggio, 1, 3)
            Comuni = Mid(appoggio, 4, 3)


            Dim M As New ClsComune

            M.Provincia = Provincia
            M.Comune = Comuni
            M.Leggi(Session("DC_OSPITE"))
            If campodb(M.TipoOperazione) = "" Then
                M.Provincia = Provincia
                M.Comune = Comuni
                M.Descrizione = campodb(myPOSTreader.Item("Descrizione"))
                M.CodificaProvincia = campodb(myPOSTreader.Item("Prov"))
                M.CODXCODF = campodb(myPOSTreader.Item("CodxCF"))
                M.ScriviComune(Session("DC_OSPITE"))
            End If

        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Strumenti.aspx")
    End Sub
End Class
