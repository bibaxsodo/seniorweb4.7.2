﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class OspitiWeb_CreaDocumentoDaAddebito
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Dim cmd As New OleDbCommand()
        Dim MySql As String

        cmd.CommandText = "Select * From ADDACR where ID =?"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@id", Request.Item("ID"))
        
        Dim Addebito As OleDbDataReader = cmd.ExecuteReader()
        If Addebito.Read Then
            If Addebito.Item("RIFERIMENTO") = "O" Then

                Dim TipoAddebito As New Cls_Addebito

                TipoAddebito.Codice = campodb(Addebito.Item("CodiceIva"))
                TipoAddebito.Leggi(Session("DC_OSPITE"), TipoAddebito.Codice)


                If campodb(Addebito.Item("TIPOMOV")) = "AD" Then
                    CreaDocumentoOspite(campodb(Addebito.Item("CENTROSERVIZIO")), campodb(Addebito.Item("CODICEOSPITE")), Year(campodb(Addebito.Item("DATA"))), Month(campodb(Addebito.Item("DATA"))), campodbN(Addebito.Item("IMPORTO")), campodb(Addebito.Item("Descrizione")), TipoAddebito.Codice)
                Else
                    CreaDocumentoOspite(campodb(Addebito.Item("CENTROSERVIZIO")), campodb(Addebito.Item("CODICEOSPITE")), Year(campodb(Addebito.Item("DATA"))), Month(campodb(Addebito.Item("DATA"))), campodbN(Addebito.Item("IMPORTO")) * -1, campodb(Addebito.Item("Descrizione")), TipoAddebito.Codice)
                End If

            End If

            If Addebito.Item("RIFERIMENTO") = "P" Then

                Dim TipoAddebito As New Cls_Addebito

                TipoAddebito.Codice = campodb(Addebito.Item("CodiceIva"))
                TipoAddebito.Leggi(Session("DC_OSPITE"), TipoAddebito.Codice)


                If campodb(Addebito.Item("TIPOMOV")) = "AD" Then
                    CreaDocumentoParente(campodb(Addebito.Item("CENTROSERVIZIO")), campodb(Addebito.Item("CODICEOSPITE")), Year(campodb(Addebito.Item("DATA"))), Month(campodb(Addebito.Item("DATA"))), campodbN(Addebito.Item("IMPORTO")), campodb(Addebito.Item("Descrizione")), TipoAddebito.Codice, campodb(Addebito.Item("PARENTE")))
                Else
                    CreaDocumentoParente(campodb(Addebito.Item("CENTROSERVIZIO")), campodb(Addebito.Item("CODICEOSPITE")), Year(campodb(Addebito.Item("DATA"))), Month(campodb(Addebito.Item("DATA"))), campodbN(Addebito.Item("IMPORTO")) * -1, campodb(Addebito.Item("Descrizione")), TipoAddebito.Codice, campodb(Addebito.Item("PARENTE")))
                End If

            End If
        End If


        cn.Close()

        Response.Redirect("StampaFattureProva.aspx?TIPO=NONEMISSIONE&URL=GrigliaAddebitiAccrediti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub CreaDocumentoOspite(ByVal CentroServizio As String, ByVal CodiceOspite As Integer, ByVal Anno As Integer, ByVal Mese As Integer, ByVal Importo As Double, ByVal Descrizione As String, ByVal CodiceIVA As String)
        Dim xs As New ClsOspite

        xs.Leggi(Session("DC_OSPITE"), CodiceOspite)

        Dim EM_Retta As New Cls_EmissioneRetta
        Dim BolloImpo As Double
        Dim Bollo As Double
        Dim IVABollo As String


        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

        TipoCserv.CentroServizio = CentroServizio
        TipoCserv.CodiceOspite = xs.CodiceOspite
        TipoCserv.Leggi(Session("DC_OSPITE"))
        If TipoCserv.TipoOperazione <> "" Then
            xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
            xs.CODICEIVA = TipoCserv.AliquotaIva
        End If


        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), CentroServizio)

        If xs.TIPOOPERAZIONE.Trim <> "" Then
            Dim TipoOperasazione As New Cls_TipoOperazione

            TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)
            EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE


            Dim AliquotaIva As New Cls_IVA
            Dim AliquotaBollo As New Cls_IVA

            AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

            If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And Importo < 0) Then
                BolloImpo = 0
                Bollo = 0
                IVABollo = ""
            Else
                Dim KBollo As New Cls_bolli

                KBollo.Leggi(Session("DC_TABELLE"), Now)
                BolloImpo = KBollo.ImportoBollo
                Bollo = KBollo.ImportoBollo
                IVABollo = KBollo.CodiceIVA


                AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

            End If
            For x = 0 To 40
                EM_Retta.Emr_ImpAddebito(x) = 0
                EM_Retta.Emr_DesAddebito(x) = ""
                EM_Retta.Emr_CodivaAddebito(x) = ""
                EM_Retta.Emr_ImpAccredito(x) = 0
                EM_Retta.Emr_DesAccredito(x) = ""
                EM_Retta.Emr_CodivaAccredito(x) = ""
            Next

            If Importo > 0 Then
                EM_Retta.Emr_ImpAddebito(1) = Importo
                EM_Retta.Emr_DesAddebito(1) = Descrizione
                EM_Retta.Emr_CodivaAddebito(1) = CodiceIVA
            Else
                EM_Retta.Emr_ImpAccredito(1) = Math.Abs(Importo)
                EM_Retta.Emr_DesAccredito(1) = Descrizione
                EM_Retta.Emr_CodivaAccredito(1) = CodiceIVA
            End If

            If Importo < 0 Then
                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                EM_Retta.CreaRegistrazioneContabile(Anno, Mese, CentroServizio, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, CodiceOspite * 100, 0, 0, 0, 0, Math.Abs(Importo), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, DateSerial(Year(Now), Month(Now), Day(Now)), 0, 0, 0, 0, Mese, Anno, TipoOperasazione.Codice, 0, 0, Math.Abs(Importo), 0, 0, 0, 0, 0, 1)
            Else
                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                EM_Retta.CreaRegistrazioneContabile(Anno, Mese, CentroServizio, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, CodiceOspite * 100, 0, 0, 0, Math.Abs(Importo), 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, DateSerial(Year(Now), Month(Now), Day(Now)), 0, 0, 0, 0, Mese, Anno, TipoOperasazione.Codice, 0, 0, Math.Abs(Importo), 0, 0, 0, 0, 0, 1)
            End If



        End If

    End Sub


    Private Sub CreaDocumentoParente(ByVal CentroServizio As String, ByVal CodiceOspite As Integer, ByVal Anno As Integer, ByVal Mese As Integer, ByVal Importo As Double, ByVal Descrizione As String, ByVal CodiceIVA As String, ByVal CodiceParente As Integer)
        Dim xs As New Cls_Parenti

        xs.Leggi(Session("DC_OSPITE"), CodiceOspite, CodiceParente)

        Dim EM_Retta As New Cls_EmissioneRetta
        Dim BolloImpo As Double
        Dim Bollo As Double
        Dim IVABollo As String


        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

        TipoCserv.CentroServizio = CentroServizio
        TipoCserv.CodiceOspite = xs.CodiceOspite
        TipoCserv.CodiceParente = CodiceParente
        TipoCserv.Leggi(Session("DC_OSPITE"))
        If TipoCserv.TipoOperazione <> "" Then
            xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
            xs.CODICEIVA = TipoCserv.AliquotaIva
        End If


        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), CentroServizio)

        If xs.TIPOOPERAZIONE.Trim <> "" Then
            Dim TipoOperasazione As New Cls_TipoOperazione

            TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)
            EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE


            Dim AliquotaIva As New Cls_IVA
            Dim AliquotaBollo As New Cls_IVA

            AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

            If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And Importo < 0) Then
                BolloImpo = 0
                Bollo = 0
                IVABollo = ""
            Else
                Dim KBollo As New Cls_bolli

                KBollo.Leggi(Session("DC_TABELLE"), Now)
                BolloImpo = KBollo.ImportoBollo
                Bollo = KBollo.ImportoBollo
                IVABollo = KBollo.CodiceIVA


                AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

            End If
            For x = 0 To 40
                EM_Retta.Emr_ImpAddebito(x) = 0
                EM_Retta.Emr_DesAddebito(x) = ""
                EM_Retta.Emr_CodivaAddebito(x) = ""
                EM_Retta.Emr_ImpAccredito(x) = 0
                EM_Retta.Emr_DesAccredito(x) = ""
                EM_Retta.Emr_CodivaAccredito(x) = ""
            Next

            If Importo > 0 Then
                EM_Retta.Emr_ImpAddebito(1) = Importo
                EM_Retta.Emr_DesAddebito(1) = Descrizione
                EM_Retta.Emr_CodivaAddebito(1) = CodiceIVA
            Else
                EM_Retta.Emr_ImpAccredito(1) = Math.Abs(Importo)
                EM_Retta.Emr_DesAccredito(1) = Descrizione
                EM_Retta.Emr_CodivaAccredito(1) = CodiceIVA
            End If

            If Importo < 0 Then
                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                EM_Retta.CreaRegistrazioneContabile(Anno, Mese, CentroServizio, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, CodiceOspite * 100 + CodiceParente, 0, 0, 0, 0, Math.Abs(Importo), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, DateSerial(Year(Now), Month(Now), Day(Now)), 0, 0, 0, 0, Mese, Anno, TipoOperasazione.Codice, 0, 0, Math.Abs(Importo), 0, 0, 0, 0, 0, 1)
            Else
                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                EM_Retta.CreaRegistrazioneContabile(Anno, Mese, CentroServizio, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, CodiceOspite * 100 + CodiceParente, 0, 0, 0, Math.Abs(Importo), 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, DateSerial(Year(Now), Month(Now), Day(Now)), 0, 0, 0, 0, Mese, Anno, TipoOperasazione.Codice, 0, 0, Math.Abs(Importo), 0, 0, 0, 0, 0, 1)
            End If



        End If

    End Sub
End Class
