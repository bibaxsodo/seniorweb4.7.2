﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.AsymmetricAlgorithm
Imports System.Security.Cryptography.RSA
Imports System.Security.Cryptography.RSACryptoServiceProvider
Imports System.Text

Partial Class OspitiWeb_ImportCSVDomiciliari
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim NomeSocieta As String = "xml"
        Dim Appo As String
        Randomize()
        Dim NomeFile As String = Format(Now, "ddMMyyyyhhmmss") & Int(Rnd(1) * 10)

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".csv")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".csv") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
            Exit Sub
        End If


        Dim cnO As OleDbConnection

        cnO = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnO.Open()

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        Dim SalvaDb As New OleDbCommand

        SalvaDb.CommandText = "DELETE  From MovimentiDomiciliare WHERE YEAR(DATA) = ? And month(Data) = ?"
        SalvaDb.Connection = cnO
        SalvaDb.Parameters.AddWithValue("@Anno", Param.AnnoFatturazione)
        SalvaDb.Parameters.AddWithValue("@Mese", Param.MeseFatturazione)
        SalvaDb.ExecuteNonQuery()

        cnO.Close()


        Tabella.Clear()
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Attivita", GetType(String))
        Tabella.Columns.Add("Totale", GetType(String))
        Tabella.Columns.Add("Segnalazioni", GetType(String))

        Dim cn As OdbcConnection


        cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\" & NomeSocieta & "\;HDR=Yes;")

        cn.Open()
        Dim cmd As New OdbcCommand()

        cmd.CommandText = ("select * from [ExcelImport" & NomeFile & ".csv" & "]")

        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim CodiceFiscale As String = ""
            Dim Operatore As String = ""
            Dim Data As String = ""
            Dim Attivita As String = ""
            Dim OraInizio As String = ""
            Dim OraFine As String = ""
            Dim Totale As String = ""
            Dim Stringa As String = ""
            Dim Errore As Boolean = False

            Stringa = campodb(myPOSTreader.Item(0))

            Dim Vettore(100) As String

            Vettore = SplitWords(Stringa)


            CodiceFiscale = Vettore(1)
            Operatore = Vettore(3)
            Data = Vettore(4)
            Attivita = Vettore(5)
            Totale = Vettore(8)
            OraInizio = Vettore(6)
            OraFine = Vettore(7)


            Dim Ospite As New ClsOspite

            Ospite.CODICEFISCALE = CodiceFiscale
            Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CodiceFiscale)

            Dim Movimenti As New Cls_Movimenti

            If Ospite.CodiceOspite > 0 Then


                Movimenti.CodiceOspite = Ospite.CodiceOspite
                Movimenti.UltimaData(Session("DC_OSPITE"), Movimenti.CodiceOspite)

            Else
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = CodiceFiscale
                myriga(1) = Data           
                myriga(2) = Attivita
                myriga(3) = Totale
                myriga(4) = "Utente non trovato"
                Tabella.Rows.Add(myriga)
                Errore = True
            End If

            Dim Operat As New Cls_Operatore

            Operat.Nome = Operatore
            Operat.LeggiPerNOME(Session("DC_OSPITE"))

            Dim TipoDom As New Cls_TipoDomiciliare

            TipoDom.LeggiDescrizione(Session("DC_OSPITE"), Attivita)

            If TipoDom.Descrizione = "" Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = CodiceFiscale
                myriga(1) = Data
                myriga(2) = Attivita
                myriga(3) = Totale
                myriga(4) = "Attivita non trovata"
                Tabella.Rows.Add(myriga)
                Errore = True
            End If

            
            If Not Errore Then
                Dim Domiciliare As New Cls_MovimentiDomiciliare

                Domiciliare.CodiceOspite = Ospite.CodiceOspite
                Domiciliare.Tipologia = TipoDom.Codice
                Domiciliare.CENTROSERVIZIO = Movimenti.CENTROSERVIZIO
                Domiciliare.Data = Data
                Domiciliare.Descrizione = Attivita
                Domiciliare.Operatore = Operat.CodiceMedico
                Domiciliare.OraInizio = TimeSerial(Mid(OraInizio, 1, 2), Mid(OraInizio, 4, 2), 0).AddYears(1899).AddMonths(12).AddDays(30)
                Domiciliare.OraFine = TimeSerial(Mid(OraFine, 1, 2), Mid(OraFine, 4, 2), 0).AddYears(1899).AddMonths(12).AddDays(30)
                Domiciliare.Operatore = 1
                Domiciliare.Utente = Session("UTENTE")

                Domiciliare.AggiornaDB(Session("DC_OSPITE"))
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()



        ViewState("Appoggio") = Tabella


        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella

        GridView1.DataBind()
    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, ";")
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub
End Class
