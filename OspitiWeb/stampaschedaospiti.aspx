﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_stampaschedaospiti" CodeFile="stampaschedaospiti.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stampa Scheda/Etichette</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />

    <script type="text/javascript">
        var windowObjectReference;

        function openRequestedPopup() {

            windowObjectReference = window.open("PdfDaHtml.aspx",
                "DescriptiveWindowName",
                "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");

        }
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div style="text-align: center;">
            <center><b>STAMPA ETICHETTE</b></center>
            <br />
            <br />
            <br />
            <br />
            Numero Etichette: 
        <asp:DropDownList ID="DD_NumeroEtichette" runat="server">
        </asp:DropDownList>
            <asp:Button ID="Btn_Etichetta" runat="server" Height="28px" Width="148px" ToolTip="Etichette" BackColor="#007DC4" ForeColor="White" Text="Stampa Etichette" />
            <br />
            <br />
            <hr />
            <center><b>STAMPA SCHEDA</b></center>
            <br />
            <br />
            <asp:Button ID="Btn_Scheda" runat="server" Height="28px" Width="148px" ToolTip="Scheda" BackColor="#007DC4" ForeColor="White" Text="Stampa Scheda" />
            <br />
            <br />
            <hr />
            <center><b>STAMPA CONTRATTO</b></center>
            <br />
            <br />
            <br />

            Tipo Contratto :
        <asp:DropDownList ID="DD_TipoContratto" runat="server">
        </asp:DropDownList>
            <asp:Button ID="Btn_Contratto" runat="server" Height="28px" Width="148px" ToolTip="Contratto" BackColor="#007DC4" ForeColor="White" Text="Contratto PDF" OnClientClick="javascript:return openRequestedPopup();" />
            <asp:Button ID="Btn_Word" runat="server" Height="28px" Width="148px" ToolTip="Word" BackColor="#007DC4" ForeColor="White" Text="Contratto Word" />
            <br />
            Campo Libero 1 :
            <asp:TextBox ID="Txt_Libero1" runat="server"></asp:TextBox>
            <br />
            Campo Libero 2 :
            <asp:TextBox ID="Txt_Libero2" runat="server"></asp:TextBox>
            <br />
            Campo Libero 3 :
            <asp:TextBox ID="Txt_Libero3" runat="server"></asp:TextBox>
            <br />

            <br />

            <asp:Label ID="lblexportcontratto" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
