﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq


Partial Class OspitiWeb_ApiV1Epersonam_ImportDomiciliareV1
    Inherits System.Web.UI.Page


    Dim Tabella As New System.Data.DataTable("tabella")

    Function TabellaTrascodifica() As Boolean
        Dim cn As OleDbConnection

        TabellaTrascodifica = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DomiciliareEPersonam")

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TabellaTrascodifica = True
        End If
        cn.Close()
    End Function


    Function DecodificaTabellaTrascodifica(ByVal CentroServizio As String, ByVal activity_id As Integer, ByVal Convenzione As String) As String
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        DecodificaTabellaTrascodifica = ""

        If Convenzione.ToUpper = "FALSE" Then
            Convenzione = "N"
        End If
        If Convenzione.ToUpper = "TRUE" Then
            Convenzione = "S"
        End If

        Dim cmd As New OleDbCommand()
        If CentroServizio = "" Then
            cmd.CommandText = ("select * from DomiciliareEPersonam where activity_id = ?")
            cmd.Parameters.AddWithValue("@activity_id", activity_id)
        Else
            cmd.CommandText = ("select * from DomiciliareEPersonam where activity_id = ? And (CentroServizio = ? or (CentroServizio = '' OR CentroServizio is null)) And (Convenzione = ? or (Convenzione = '' OR Convenzione is null))")
            cmd.Parameters.AddWithValue("@activity_id", activity_id)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmd.Parameters.AddWithValue("@Convenzione", Convenzione)

        End If
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DecodificaTabellaTrascodifica = campodb(myPOSTreader.Item("TipologiaDomiciliare"))
        End If
        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub OspitiWeb_ImportDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception

                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Txt_Dal.Text = "1"
            Txt_Al.Text = GiorniMese(Param.MeseFatturazione, Param.AnnoFatturazione)

            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim SalvaDb As New OleDbCommand

            Chk_RicaricaDati.Checked = True
            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Periodo = ? And Tipo = 'D' And Nucleo = ?"
            SalvaDb.Connection = cn
            SalvaDb.Parameters.AddWithValue("@Periodo", Format(Param.MeseFatturazione, "00") & Param.AnnoFatturazione)
            SalvaDb.Parameters.AddWithValue("@Nucleo", 0)
            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim appoggio As Date = Nothing
                Try
                    appoggio = campodb(VerificaDB.Item("UltimaModifica"))
                Catch ex As Exception

                End Try
                If Format(appoggio, "yyyyMMdd") = Format(Now, "yyyyMMdd") Then
                    Chk_RicaricaDati.Checked = False
                End If
            End If
            VerificaDB.Close()

            cn.Close()
        End If

    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String = ""
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        If Chk_RicaricaDati.Checked = True Then
            Token = LoginPersonam(Context)

            

            If Param.DebugV1 = 1 Then
                Dim NomeFile As String

                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Token_" & Format(Now, "yyyyMMdd") & ".xml"
                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                tw.Write(Token)
                tw.Close()
            End If


            If Token = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                Exit Sub
            End If
        End If
        Session("TOKEN") = Token
        Session("PAGINA") = 0
        Call ImportDomiciliare(Token)
    End Sub


    Function RichiediJSONEpersonam(ByVal Token As String, ByVal BU As Long, ByRef Pagina As Integer, ByVal Anno As Integer, ByVal Mese As Integer) As String
        Dim rawresp As String = ""
        Dim Url As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



       
        If Pagina = 0 Then
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese '& "?per_page=10"
            Pagina = Pagina + 1
        Else
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese & "?page=" & Pagina ' & "&per_page=100"
        End If
        Pagina = Pagina + 1

        If Chk_RicaricaDati.Checked = True Then
            Try

                Dim client As HttpWebRequest = WebRequest.Create(Url)

                client.Method = "GET"
                client.Headers.Add("Authorization", "Bearer " & Token)
                client.ContentType = "Content-Type: application/json"

                client.KeepAlive = True
                client.ReadWriteTimeout = 62000
                client.MaximumResponseHeadersLength = 262144
                client.Timeout = 62000


                Dim reader As StreamReader
                Dim response As HttpWebResponse = Nothing



                response = DirectCast(client.GetResponse(), HttpWebResponse)

                reader = New StreamReader(response.GetResponseStream())

                rawresp = reader.ReadToEnd()

            Catch ex As Exception
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam " & BU & " ');", True)
                Return ""
                Exit Function
            End Try

            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = " & Pagina - 1 & " And Periodo = ? And Tipo = 'D' And Nucleo = ? "
            SalvaDb.Parameters.AddWithValue("@Periodo", Format(Anno, "00") & Anno)
            SalvaDb.Parameters.AddWithValue("@Nucleo", BU)
            SalvaDb.Connection = cn

            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'D' And Nucleo =  ? "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", Pagina - 1)
                UpDb.Parameters.AddWithValue("@Periodo", Format(Mese, "00") & Anno)
                UpDb.Parameters.AddWithValue("@Nucleo", BU)
                UpDb.ExecuteNonQuery()
            Else
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo,Nucleo) values (?,?,?,?,?,'D',?) "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", Pagina - 1)
                UpDb.Parameters.AddWithValue("@Periodo", Format(Mese, "00") & Anno)
                UpDb.Parameters.AddWithValue("@Nucleo", BU)
                UpDb.ExecuteNonQuery()
            End If
            VerificaDB.Close()

        Else
            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = " & Pagina - 1 & " And Periodo = ? And Tipo ='D' And Nucleo = ? "
            SalvaDb.Connection = cn
            SalvaDb.Parameters.AddWithValue("@Periodo", Format(Mese, "00") & Anno)
            SalvaDb.Parameters.AddWithValue("@Nucleo", BU)
            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                rawresp = campodb(VerificaDB.Item("Dati"))
            End If
            VerificaDB.Close()


            'Dim objStreamReader As StreamReader
            'objStreamReader = File.OpenText(Server.MapPath("..\Public\dom_" & cS.EPersonam & ".json"))
            'rawresp = objStreamReader.ReadToEnd().Trim
            'objStreamReader.Close()
        End If

        cn.Close()

        Return rawresp
    End Function


    Public Sub ImportDomiciliare(ByVal Token As String)
        Dim NumeroRighe As Integer = 0
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler




        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Cognome", GetType(String)) ' 0
        Tabella.Columns.Add("Nome", GetType(String)) '1
        Tabella.Columns.Add("CodiceFiscale", GetType(String)) '2
        Tabella.Columns.Add("Centroservizio", GetType(String)) '3
        Tabella.Columns.Add("DataMovimento", GetType(String)) '4
        Tabella.Columns.Add("OrarioDal", GetType(String)) '5
        Tabella.Columns.Add("OrarioAl", GetType(String)) '6
        Tabella.Columns.Add("Operatore", GetType(String)) '7
        Tabella.Columns.Add("Tipologia", GetType(String)) '8
        Tabella.Columns.Add("IdEpersonam", GetType(Long)) '9
        Tabella.Columns.Add("CodiceOspite", GetType(Long)) '10
        Tabella.Columns.Add("Segnalazioni", GetType(String)) '11        
        Tabella.Columns.Add("Durata", GetType(String)) '12

        Tabella.Columns.Add("NOperatore1", GetType(String)) '12
        Tabella.Columns.Add("NomeOperatore1", GetType(String)) '12

        Tabella.Columns.Add("NOperatore2", GetType(String)) '12
        Tabella.Columns.Add("NomeOperatore2", GetType(String)) '12

        Tabella.Columns.Add("NOperatore3", GetType(String)) '12
        Tabella.Columns.Add("NomeOperatore3", GetType(String)) '12

        Tabella.Columns.Add("NOperatore4", GetType(String)) '12
        Tabella.Columns.Add("NomeOperatore4", GetType(String)) '12

        Tabella.Columns.Add("NOperatore5", GetType(String)) '12
        Tabella.Columns.Add("NomeOperatore5", GetType(String)) '12


        Dim Data() As Byte

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim cS As New Cls_CentroServizio
        If Cmb_CServ.SelectedValue <> "" Then
            cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)
        End If

        
        Dim Pagina As Integer = 0

        Dim NucleiDb As New OleDbCommand


        If Session("DC_OSPITE").ToString.IndexOf("AspMagiera") > 0 Then
            NucleiDb.CommandText = "Select [EPersonam] AS IdEpersonam  From TABELLACENTROSERVIZIO Where CENTROSERVIZIO= '" & Cmb_CServ.SelectedValue & "'"
        Else

            If Session("DC_OSPITE").ToString.IndexOf("AQUA") > 0 Then
                NucleiDb.CommandText = "Select * From NucleiCentroServizi Where IdSenior = 3769 or IdSenior = 4404 or  IdSenior = 4388"
            Else
                If Cmb_CServ.SelectedValue <> "" Then
                    NucleiDb.CommandText = "Select * From NucleiCentroServizi Where IdSenior = " & cS.EPersonam
                Else
                    NucleiDb.CommandText = "Select * From NucleiCentroServizi "
                End If
            End If
        End If

        NucleiDb.Connection = cn

        Dim NucleiRB As OleDbDataReader = NucleiDb.ExecuteReader()


        Do While NucleiRB.Read
            Do
                NumeroRighe = 0


                Pagina = 0

                Dim rawresp As String = ""

                If NucleiRB.Item("IdEpersonam") = 4959 Then
                    rawresp = RichiediJSONEpersonam(Token, 4955, Pagina, Param.AnnoFatturazione, Param.MeseFatturazione)
                Else
                    rawresp = RichiediJSONEpersonam(Token, NucleiRB.Item("IdEpersonam"), Pagina, Param.AnnoFatturazione, Param.MeseFatturazione)

                End If

                
                If rawresp = "" Then Exit Do
                If rawresp = "[]" Then Exit Do
                If rawresp = "{}" Then Exit Do

                Dim jResults As JArray = JArray.Parse(rawresp)


                Dim IndiceRighe As Integer = 0

                For Each jTok1 As JToken In jResults
                    Dim CodiceFiscale As String


                    CodiceFiscale = jTok1.Item("cf").ToString()

                    If CodiceFiscale = "CNISCL35A52C310B" Then
                        CodiceFiscale = "CNISCL35A52C310B"
                    End If

                    Dim Ospite As New ClsOspite

                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), CodiceFiscale)
                    If Ospite.CodiceOspite = 0 Then
                        For Each jTok2 As JToken In jTok1.Item("domestic_activities").Children
                            NumeroRighe = NumeroRighe + 1
                        Next


                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = jTok1.Item("fullname").ToString()
                        myriga(1) = ""

                        myriga(2) = jTok1.Item("cf").ToString()

                        myriga(3) = ""
                        myriga(4) = ""



                        myriga(5) = ""
                        myriga(6) = ""
                        myriga(7) = 0
                        myriga(8) = 0
                        myriga(9) = 0
                        myriga(11) = "ERRORE"
                        Tabella.Rows.Add(myriga)

                    Else
                        For Each jTok2 As JToken In jTok1.Item("domestic_activities").Children

                            Dim MiaData As Date
                            Dim Motivation As Integer

                            Dim CentroServizio As String =""

                            NumeroRighe = NumeroRighe + 1


                            'CentroServizio = RDcs.Item("CENTROSERVIZIO")

                            Dim MMov As New Cls_Movimenti


                            Dim UnitID As Integer
                            Try
                                UnitID = jTok2.Item("unit_id").ToString()
                            Catch ex As Exception

                            End Try

                            Dim ErroreCS As Boolean = False
                            Dim UnitStruttura As New Cls_Epersonam

                            Dim ServizioUnitID As New Cls_CentroServizio
                            Dim UnitTrascodifica As Integer

                            UnitTrascodifica = UnitStruttura.RendiWardid(UnitID, Session("DC_OSPITE"))

                            If UnitTrascodifica > 0 Then
                                ServizioUnitID.EPersonam = UnitTrascodifica
                                ServizioUnitID.LeggiEpersonam(Session("DC_OSPITE"), UnitTrascodifica, True)
                            Else
                                ServizioUnitID.EPersonam = UnitID
                                ServizioUnitID.LeggiEpersonam(Session("DC_OSPITE"), UnitID, True)
                            End If

                            MMov.CodiceOspite = Ospite.CodiceOspite
                            MMov.CENTROSERVIZIO = ServizioUnitID.CENTROSERVIZIO
                            If MMov.CServizioUsato(Session("DC_OSPITE")) = False Then
                                ErroreCS = True
                            Else
                                CentroServizio = ServizioUnitID.CENTROSERVIZIO
                            End If


                            If ErroreCS = True and ServizioUnitID.CENTROSERVIZIO <> ""  Then
                                'crea utente su cserv

                                Dim Servizio As New Cls_Movimenti

                                MMov.CodiceOspite = Ospite.CodiceOspite
                                MMov.PrimaData(Session("DC_OSPITE"), MMov.CodiceOspite)

                                Servizio.CENTROSERVIZIO = ServizioUnitID.CENTROSERVIZIO
                                Servizio.CodiceOspite = Ospite.CodiceOspite
                                Servizio.Data = MMov.Data
                                Servizio.Progressivo = 1
                                Servizio.TipoMov = "05"
                                Servizio.Descrizione = "Accoglimento Domiciliare"
                                Servizio.InserisciDB(Session("DC_OSPITE"))
                                ErroreCS = False
                            End If



                            MiaData = jTok2.Item("date_done").ToString

                            Try
                                If IsDBNull(jTok2.Item("done").ToString) Or jTok2.Item("done").ToString = "" Then
                                    Motivation = 1 ' Eseguito se Null??
                                Else
                                    Motivation = jTok2.Item("done").ToString
                                End If

                            Catch ex As Exception
                                Motivation = 99
                            End Try

                            iF Motivation>= 1 THEN
                                ImportDomiciliare(NumeroRighe, Param, cn, IndiceRighe, jTok1, Ospite, jTok2, MiaData, CentroServizio, MMov, ErroreCS)
                            End If
                        Next
                    End If
                Next


            Loop While NumeroRighe >= 90000


        Loop
        NucleiRB.Close()


        cn.Close()


        Dim TabApp As System.Data.DataTable = Tabella.Clone

        Tabella.Select("", "Cognome,Nome,DataMovimento")

        Dim foundRowsT As System.Data.DataRow()
        foundRowsT = Tabella.Select("", "Cognome,Nome,DataMovimento")

        For XO = 0 To foundRowsT.Length - 1
            TabApp.ImportRow(foundRowsT(XO))
        Next


        ViewState("App_AddebitiMultiplo") = TabApp ' TabApp

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = TabApp
        GridView1.DataBind()
        GridView1.Visible = True

    End Sub

    Private Sub ImportDomiciliare(ByRef NumeroRighe As Integer, Param As Cls_Parametri, cn As OleDbConnection, ByRef IndiceRighe As Integer, jTok1 As JToken, Ospite As ClsOspite, jTok2 As JToken, MiaData As Date, ByRef CentroServizio As String, MMov As Cls_Movimenti, ErroreCS As Boolean)
        Dim Appoggio As Date = Mid(MiaData, 1, 10)

        If (Cmb_CServ.SelectedValue <> CentroServizio And Cmb_CServ.SelectedValue <> "") Or ErroreCS Then
            Dim UltMov As New Cls_Movimenti


            'If UltMov.PresenzaInSAD(Session("DC_OSPITE"), Ospite.CodiceOspite, Param.AnnoFatturazione, Param.MeseFatturazione) = False Then
            For Each jTokQ As JToken In jTok1.Item("domestic_activities").Children
                NumeroRighe = NumeroRighe + 1
            Next

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = jTok1.Item("fullname").ToString()
            myriga(1) = ""

            myriga(2) = jTok1.Item("cf").ToString()

            myriga(3) = ""
            myriga(4) = ""



            myriga(5) = ""
            myriga(6) = ""
            myriga(7) = 0
            myriga(8) = 0
            myriga(9) = 0
            myriga(11) = "ERRORECS"
            Tabella.Rows.Add(myriga)
            'End If
        Else
            If MMov.TipoMov = "13" And Format(MMov.Data, "yyyyMMDD") < Format(DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, 1), "yyyyMMDD") Then
                For Each jTokQ As JToken In jTok1.Item("domestic_activities").Children
                    NumeroRighe = NumeroRighe + 1
                Next

                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = jTok1.Item("fullname").ToString()
                myriga(1) = ""

                myriga(2) = jTok1.Item("cf").ToString()

                myriga(3) = ""
                myriga(4) = ""



                myriga(5) = ""
                myriga(6) = ""
                myriga(7) = 0
                myriga(8) = 0
                myriga(9) = 0
                myriga(11) = "ERRORE"
                Tabella.Rows.Add(myriga)
                CentroServizio = ""
            End If
        End If


        If (Cmb_CServ.SelectedValue = "" Or Cmb_CServ.SelectedValue = CentroServizio) And Format(Appoggio, "yyyyMMdd") >= Format(DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, Val(Txt_Dal.Text)), "yyyyMMdd") And Format(Appoggio, "yyyyMMdd") <= Format(DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, Val(Txt_Al.Text)), "yyyyMMdd") Then

            Dim cmdcData As New OleDbCommand()
            cmdcData.CommandText = ("select * from Domiciliare_EPersonam Where CentroServizio = ? and CodiceOspite = ? And ID_Epersonam = ?")

            cmdcData.Parameters.AddWithValue("@CENTROSERVIZIO", CentroServizio)
            cmdcData.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
            cmdcData.Parameters.AddWithValue("@Id", jTok2.Item("id").ToString)
            cmdcData.Connection = cn
            Dim RDData As OleDbDataReader = cmdcData.ExecuteReader()
            If RDData.Read Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Ospite.CognomeOspite
                myriga(1) = Ospite.NomeOspite
                myriga(2) = Ospite.CODICEFISCALE

                myriga(3) = CentroServizio
                myriga(4) = Mid(MiaData, 1, 10)




                Dim Ore As Integer = Mid(jTok2.Item("date_done").ToString, 12, 2)
                Dim Minuti As Integer = Mid(jTok2.Item("date_done").ToString, 15, 2)
                Dim OraInizio As Date
                Dim OraFine As Date
                Dim Tipologia As String

                OraInizio = TimeSerial(Ore, Minuti, 0).AddYears(1899).AddMonths(12).AddDays(30)


                REM invertito controllo per aqua (da parametrizzarE)
                Dim Durata As Long = Val(jTok2.Item("duration").ToString)

                If Val(Durata) = 0 Then
                    Durata = Val(jTok2.Item("duration_prev").ToString)
                End If


                REM Import minuti effettivi non a 15  min. per aqua (da parametrizzarE)
                ' If Durata > 0 Then
                'Durata = Math.Round(Durata / 15, 0) * 15
                'End If


                Dim NumeroOperatori As Integer = 0
                For Each jTok3 As JToken In jTok2.Item("operators").Children
                    NumeroOperatori = NumeroOperatori + 1

                    If NumeroOperatori = 1 Then
                        myriga(13) = jTok3.Item("id")
                        myriga(14) = jTok3.Item("fullname")
                    End If
                    If NumeroOperatori = 2 Then
                        myriga(15) = jTok3.Item("id")
                        myriga(16) = jTok3.Item("fullname")
                    End If
                    If NumeroOperatori = 3 Then
                        myriga(17) = jTok3.Item("id")
                        myriga(18) = jTok3.Item("fullname")
                    End If
                    If NumeroOperatori = 4 Then
                        myriga(19) = jTok3.Item("id")
                        myriga(20) = jTok3.Item("fullname")
                    End If
                    If NumeroOperatori = 5 Then
                        myriga(21) = jTok3.Item("id")
                        myriga(22) = jTok3.Item("fullname")
                    End If
                Next

                If NumeroOperatori > 2 Then
                    NumeroOperatori = 2
                End If
                myriga(12) = Durata

                OraFine = TimeSerial(Ore, Minuti, 0).AddMinutes(Durata).AddYears(1899).AddMonths(12).AddDays(30)
                myriga(5) = Format(OraInizio, "HH:mm")
                myriga(6) = Format(OraFine, "HH:mm")
                myriga(7) = NumeroOperatori


                If Not TabellaTrascodifica() Then

                    If jTok2.Item("conv").ToString = "True" Then
                        Tipologia = "Accreditato"  ' "01"
                    Else
                        Tipologia = "Non Accreditato" ' "02"
                    End If
                    If Val(jTok2.Item("activity_id").ToString) = 4064 Then
                        Tipologia = "Pasto" ' "03" ' Pasto
                    End If
                    If Val(jTok2.Item("activity_id").ToString) = 4155 Or Val(jTok2.Item("activity_id").ToString) = 3290 Then
                        Tipologia = "DIMISSIONE PROTETTA" ' "06" ' DIMISSIONE PROTETTA
                    End If
                    If Val(jTok2.Item("activity_id").ToString) = 3632 Then
                        Tipologia = "Pasto + Cena Freda"  ' "04" ' Pasto + Cena Fredda
                    End If
                    If Val(jTok2.Item("activity_id").ToString) = 4224 Or Val(jTok2.Item("activity_id").ToString) = 3282 Or Val(jTok2.Item("activity_id").ToString) = 4156 Then
                        Tipologia = "Tutoring" ' "05" ' Tutoring
                    End If
                Else
                    Tipologia = DecodificaTabellaTrascodifica(CentroServizio, Val(jTok2.Item("activity_id").ToString), campodb(jTok2.Item("conv").ToString))
                    Dim mTp As New Cls_TipoDomiciliare

                    mTp.Codice = Tipologia
                    mTp.Leggi(Session("DC_OSPITE"), mTp.Codice)

                    Tipologia = mTp.Descrizione
                End If

                myriga(8) = Tipologia



                myriga(9) = jTok2.Item("id").ToString
                myriga(10) = Ospite.CodiceOspite
                myriga(11) = "ERROREPRE"
                If Chk_Importa.Checked = False Then
                    Tabella.Rows.Add(myriga)
                End If

                IndiceRighe = IndiceRighe + 1
            Else

                Dim Vercentroservizio As New Cls_Movimenti



                Vercentroservizio.CENTROSERVIZIO = CentroServizio
                Vercentroservizio.CodiceOspite = Ospite.CodiceOspite
                If Vercentroservizio.CServizioUsato(Context.Session("DC_OSPITE")) Then
                    Dim RaddopiaRiga As Boolean = False
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = Ospite.CognomeOspite
                    myriga(1) = Ospite.NomeOspite
                    myriga(2) = Ospite.CODICEFISCALE

                    myriga(3) = CentroServizio
                    myriga(4) = Mid(MiaData, 1, 10)




                    Dim Ore As Integer = Mid(jTok2.Item("date_done").ToString, 12, 2)
                    Dim Minuti As Integer = Mid(jTok2.Item("date_done").ToString, 15, 2)
                    Dim OraInizio As Date
                    Dim OraFine As Date
                    Dim Tipologia As String
                    Dim Warning As String = ""

                    OraInizio = TimeSerial(Ore, Minuti, 0).AddYears(1899).AddMonths(12).AddDays(30)

                    REM invertito controllo per aqua (da parametrizzarE)
                    Dim Durata As Long = Val(jTok2.Item("duration").ToString)

                    If Val(Durata) = 0 Then
                        Durata = Val(jTok2.Item("duration_prev").ToString)
                    End If

                    REM Import minuti effettivi non a 15  min. per aqua (da parametrizzarE)
                    'If Durata > 0 Then
                    'Durata = Math.Round(Durata / 15, 0) * 15
                    'End If

                    Dim NumeroOperatori As Integer = 0
                    For Each jTok3 As JToken In jTok2.Item("operators").Children
                        NumeroOperatori = NumeroOperatori + 1
                        If NumeroOperatori = 1 Then
                            myriga(13) = jTok3.Item("id")
                            myriga(14) = jTok3.Item("fullname")
                        End If
                        If NumeroOperatori = 2 Then
                            myriga(15) = jTok3.Item("id")
                            myriga(16) = jTok3.Item("fullname")

                        End If
                        If NumeroOperatori = 3 Then
                            myriga(17) = jTok3.Item("id")
                            myriga(18) = jTok3.Item("fullname")

                        End If
                        If NumeroOperatori = 4 Then
                            myriga(19) = jTok3.Item("id")
                            myriga(20) = jTok3.Item("fullname")

                        End If
                        If NumeroOperatori = 5 Then
                            myriga(21) = jTok3.Item("id")
                            myriga(22) = jTok3.Item("fullname")
                        End If
                        Dim Operatore As New Cls_Operatore

                        Operatore.CodiceMedico = jTok3.Item("id")
                        Operatore.Leggi(Session("DC_OSPITE"))
                        If Operatore.Nome <> "" Then
                            If Operatore.Nome.ToUpper.Trim <> jTok3.Item("fullname").ToString.ToUpper.Trim Then
                                Warning = "Operatore " & Operatore.CodiceMedico & " diverso su epersonam " & jTok3.Item("fullname").ToString
                            End If
                        End If
                    Next

                    If NumeroOperatori > 2 Then
                        NumeroOperatori = 2
                    End If



                    OraFine = TimeSerial(Ore, Minuti, 0).AddMinutes(Durata).AddYears(1899).AddMonths(12).AddDays(30)
                    myriga(5) = Format(OraInizio, "HH:mm")
                    myriga(6) = Format(OraFine, "HH:mm")
                    myriga(7) = NumeroOperatori
                    myriga(12) = Durata

                    If Not TabellaTrascodifica() Then
                        If jTok2.Item("conv").ToString = "True" Then
                            Tipologia = "Accreditato"  ' "01"
                        Else
                            Tipologia = "Non Accreditato" ' "02"
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4064 Or Val(jTok2.Item("activity_id").ToString) = 6384 Then
                            Tipologia = "Pasto" ' "03" ' Pasto
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4155 Or Val(jTok2.Item("activity_id").ToString) = 3290 Then
                            Tipologia = "DIMISSIONE PROTETTA" ' "06" ' DIMISSIONE PROTETTA
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 6383 Then
                            RaddopiaRiga = True 'doppio pasto
                            Tipologia = "Pasto" ' "03" ' Pasto
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 3632 Then
                            Tipologia = "Pasto + Cena Freda"  ' "04" ' Pasto + Cena Fredda
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4224 Or Val(jTok2.Item("activity_id").ToString) = 3282 Or Val(jTok2.Item("activity_id").ToString) = 4156 Then
                            Tipologia = "Tutoring" ' "05" ' Tutoring
                        End If
                    Else
                        Tipologia = DecodificaTabellaTrascodifica(CentroServizio, Val(jTok2.Item("activity_id").ToString), campodb(jTok2.Item("conv").ToString))
                        Dim mTp As New Cls_TipoDomiciliare

                        mTp.Codice = Tipologia
                        mTp.Leggi(Session("DC_OSPITE"), mTp.Codice)
                        If mTp.Descrizione = "" Then
                            Warning = "Trascodifica non possibile " & Val(jTok2.Item("activity_id").ToString) & " " & jTok2.Item("description").ToString
                        End If
                        Tipologia = mTp.Descrizione
                    End If

                    myriga(8) = Tipologia



                    myriga(9) = jTok2.Item("id").ToString
                    myriga(10) = Ospite.CodiceOspite
                    myriga(11) = Warning

                    Dim Inizio As Date = OraInizio
                    Dim MinutiInCentesimiInizio As Double
                    Dim MinutiInCentesimiFine As Double


                    MinutiInCentesimiInizio = Math.Round((Minute(Inizio) * 100 / 60) / 100, 2)
                    MinutiInCentesimiFine = Math.Round((Durata * 100 / 60) / 100, 2)


                    If Hour(Inizio) + MinutiInCentesimiInizio + MinutiInCentesimiFine > 24 Then
                        myriga(6) = "23:59"
                        If Chk_Importa.Checked = True And Warning = "" Then
                            ScriviRiga(CentroServizio, myriga)
                        Else
                            Tabella.Rows.Add(myriga)
                        End If


                        Dim IndiceCpy As Integer
                        Dim myrigaCopy As System.Data.DataRow = Tabella.NewRow()

                        For IndiceCpy = 0 To Tabella.Columns.Count - 1
                            myrigaCopy(IndiceCpy) = myriga(IndiceCpy)
                        Next

                        Dim DataGiornoDopo As Date = Mid(MiaData, 1, 10)

                        myrigaCopy(4) = DataGiornoDopo.AddDays(1)


                        myrigaCopy(5) = "00:00"
                        myrigaCopy(6) = Format(OraFine, "HH:mm")

                        If Chk_Importa.Checked = True And Warning = "" Then
                            ScriviRiga(CentroServizio, myrigaCopy)
                        Else
                            Tabella.Rows.Add(myrigaCopy)
                        End If


                    Else
                        If Chk_Importa.Checked = True And Warning = "" Then
                            ScriviRiga(CentroServizio, myriga)
                        Else
                            Tabella.Rows.Add(myriga)
                        End If

                    End If





                    If RaddopiaRiga Then
                        Dim IndiceCpy As Integer
                        Dim myrigaCopy As System.Data.DataRow = Tabella.NewRow()

                        For IndiceCpy = 0 To Tabella.Columns.Count - 1
                            myrigaCopy(IndiceCpy) = myriga(IndiceCpy)
                        Next

                        Tabella.Rows.Add(myrigaCopy)
                    End If
                    IndiceRighe = IndiceRighe + 1
                Else
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = Ospite.CognomeOspite
                    myriga(1) = Ospite.NomeOspite
                    myriga(2) = Ospite.CODICEFISCALE

                    myriga(3) = CentroServizio
                    myriga(4) = Mid(jTok2.Item("date_done").ToString, 1, 10)




                    Dim Ore As Integer = Mid(jTok2.Item("date_done").ToString, 12, 2)
                    Dim Minuti As Integer = Mid(jTok2.Item("date_done").ToString, 15, 2)
                    Dim OraInizio As Date
                    Dim OraFine As Date
                    Dim Tipologia As String

                    OraInizio = TimeSerial(Ore, Minuti, 0).AddYears(1899).AddMonths(12).AddDays(30)

                    REM invertito controllo per aqua (da parametrizzarE)
                    Dim Durata As Long = Val(jTok2.Item("duration").ToString)

                    If Val(Durata) = 0 Then
                        Durata = Val(jTok2.Item("duration_prev").ToString)
                    End If

                    REM Import minuti effettivi non a 15  min. per aqua (da parametrizzarE)
                    'If Durata > 0 Then
                    'Durata = Math.Round(Durata / 15, 0) * 15
                    'End If

                    Dim NumeroOperatori As Integer = 0
                    For Each jTok3 As JToken In jTok2.Item("operators").Children
                        NumeroOperatori = NumeroOperatori + 1
                        If NumeroOperatori = 1 Then
                            myriga(13) = jTok3.Item("id")
                            myriga(14) = jTok3.Item("fullname")
                        End If
                        If NumeroOperatori = 2 Then
                            myriga(15) = jTok3.Item("id")
                            myriga(16) = jTok3.Item("fullname")
                        End If
                        If NumeroOperatori = 3 Then
                            myriga(17) = jTok3.Item("id")
                            myriga(18) = jTok3.Item("fullname")
                        End If
                        If NumeroOperatori = 4 Then
                            myriga(19) = jTok3.Item("id")
                            myriga(20) = jTok3.Item("fullname")
                        End If
                        If NumeroOperatori = 5 Then
                            myriga(21) = jTok3.Item("id")
                            myriga(22) = jTok3.Item("fullname")
                        End If
                    Next


                    myriga(12) = Durata

                    OraFine = TimeSerial(Ore, Minuti, 0).AddMinutes(Durata).AddYears(1899).AddMonths(12).AddDays(30)
                    myriga(5) = Format(OraInizio, "HH:mm")
                    myriga(6) = Format(OraFine, "HH:mm")
                    myriga(7) = NumeroOperatori

                    If Not TabellaTrascodifica() Then
                        If jTok2.Item("conv").ToString = "True" Then
                            Tipologia = "Accreditato"  ' "01"
                        Else
                            Tipologia = "Non Accreditato" ' "02"
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4064 Then
                            Tipologia = "Pasto" ' "03" ' Pasto
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4155 Or Val(jTok2.Item("activity_id").ToString) = 3290 Then
                            Tipologia = "DIMISSIONE PROTETTA" ' "06" ' DIMISSIONE PROTETTA
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 3632 Then
                            Tipologia = "Pasto + Cena Freda"  ' "04" ' Pasto + Cena Fredda
                        End If
                        If Val(jTok2.Item("activity_id").ToString) = 4224 Or Val(jTok2.Item("activity_id").ToString) = 3282 Or Val(jTok2.Item("activity_id").ToString) = 4156 Then
                            Tipologia = "Tutoring" ' "05" ' Tutoring
                        End If
                    Else
                        Tipologia = DecodificaTabellaTrascodifica(CentroServizio, Val(jTok2.Item("activity_id").ToString), campodb(jTok2.Item("conv").ToString))
                        Dim mTp As New Cls_TipoDomiciliare

                        mTp.Codice = Tipologia
                        mTp.Leggi(Session("DC_OSPITE"), mTp.Codice)

                        If mTp.Descrizione = "" Then
                            myriga(11) = "ERRORECS"
                        End If

                        Tipologia = mTp.Descrizione
                    End If
                    myriga(8) = Tipologia


                    myriga(9) = jTok2.Item("id").ToString
                    myriga(10) = Ospite.CodiceOspite
                    myriga(11) = "ERRORECS"
                    Tabella.Rows.Add(myriga)
                    IndiceRighe = IndiceRighe + 1

                End If
            End If
            RDData.Close()
        End If
    End Sub

    Private Sub ScriviRiga(ByVal Cserv As String, ByVal myriga As System.Data.DataRow)
        Dim Riga As Integer
        Dim MySql As String

        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim Progressivo As Long


        MySql = "INSERT INTO Domiciliare_EPersonam (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,Durata,ID_Epersonam) VALUES (?,?,?,?,?,?,?)"
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)

        cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdw.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdw.Parameters.AddWithValue("@CodiceOspite", myriga(10))

        Dim DataAppoggio As Date = Now
        Try
            DataAppoggio = myriga(4)
        Catch ex As Exception

        End Try
        cmdw.Parameters.AddWithValue("@Data", Format(DataAppoggio, "dd/MM/yyyy"))

        Dim AppoD1 As Date
        Dim AppoD2 As Date

        AppoD1 = myriga(5)
        AppoD2 = myriga(6)

        cmdw.Parameters.AddWithValue("@Durata", DateDiff(DateInterval.Minute, AppoD2, AppoD1))


        cmdw.Parameters.AddWithValue("@ID_Epersonam", Val(myriga(9)))
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()


        Dim Dom As New Cls_MovimentiDomiciliare

        Dom.CENTROSERVIZIO = Cserv
        Dom.CodiceOspite = myriga(10)

        Dom.Data = myriga(4)

        Dom.OraInizio = AppoD1.AddYears(1899).AddMonths(12).AddDays(30)

        Dom.OraFine = AppoD2.AddYears(1899).AddMonths(12).AddDays(30)


        Dom.Operatore = myriga(7)
        If Not TabellaTrascodifica() Then
            If myriga(8) = "Accreditato" Then
                Dom.Tipologia = "01"
            End If
            If myriga(8) = "Non Accreditato" Then
                Dom.Tipologia = "02"
            End If
            If myriga(8) = "Pasto" Then
                Dom.Tipologia = "03"
            End If
            If myriga(8) = "Pasto + Cena Freda" Then
                Dom.Tipologia = "04"
            End If
            If myriga(8) = "Tutoring" Then
                Dom.Tipologia = "05"
            End If
            If myriga(8) = "DIMISSIONE PROTETTA" Then
                Dom.Tipologia = "06"
            End If
        Else
            Dim Mp As New Cls_TipoDomiciliare


            Mp.Descrizione = campodb(myriga(8))
            Mp.LeggiDescrizione(Session("DC_OSPITE"), Mp.Descrizione)

            Dom.Tipologia = Mp.Codice
        End If


        If Val(campodb(myriga(13))) > 0 Then
            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = Val(campodb(myriga(13)))
            Operatore.Leggi(Session("DC_OSPITE"))

            If Operatore.Nome = "" Then
                Operatore.Nome = campodb(myriga(14))
                Operatore.Scrivi(Session("DC_OSPITE"))
            End If

            Dom.Righe(0) = New Cls_MovimentiDomiciliare_Operatori

            Dom.Righe(0).CodiceOperatore = Val(campodb(myriga(13)))
            Dom.Righe(0).IdMovimento = Dom.ID
        End If

        If Val(campodb(myriga(15))) > 0 Then
            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = Val(campodb(myriga(15)))
            Operatore.Leggi(Session("DC_OSPITE"))

            If Operatore.Nome = "" Then
                Operatore.Nome = campodb(myriga(16))
                Operatore.Scrivi(Session("DC_OSPITE"))
            End If

            Dom.Righe(1) = New Cls_MovimentiDomiciliare_Operatori

            Dom.Righe(1).CodiceOperatore = Val(campodb(myriga(15)))
            Dom.Righe(1).IdMovimento = Dom.ID
        End If

        If Val(campodb(myriga(17))) > 0 Then
            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = Val(campodb(myriga(17)))
            Operatore.Leggi(Session("DC_OSPITE"))

            If Operatore.Nome = "" Then
                Operatore.Nome = campodb(myriga(18))
                Operatore.Scrivi(Session("DC_OSPITE"))
            End If

            Dom.Righe(2) = New Cls_MovimentiDomiciliare_Operatori

            Dom.Righe(2).CodiceOperatore = Val(campodb(myriga(17)))
            Dom.Righe(2).IdMovimento = Dom.ID
        End If


        If Val(campodb(myriga(19))) > 0 Then
            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = Val(campodb(myriga(19)))
            Operatore.Leggi(Session("DC_OSPITE"))

            If Operatore.Nome = "" Then
                Operatore.Nome = campodb(myriga(20))
                Operatore.Scrivi(Session("DC_OSPITE"))
            End If

            Dom.Righe(3) = New Cls_MovimentiDomiciliare_Operatori

            Dom.Righe(3).CodiceOperatore = Val(campodb(myriga(19)))
            Dom.Righe(3).IdMovimento = Dom.ID
        End If

        If Val(campodb(myriga(21))) > 0 Then
            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = Val(campodb(myriga(21)))
            Operatore.Leggi(Session("DC_OSPITE"))

            If Operatore.Nome = "" Then
                Operatore.Nome = campodb(myriga(22))
                Operatore.Scrivi(Session("DC_OSPITE"))
            End If

            Dom.Righe(4) = New Cls_MovimentiDomiciliare_Operatori

            Dom.Righe(4).CodiceOperatore = Val(campodb(myriga(21)))
            Dom.Righe(4).IdMovimento = Dom.ID
        End If

        Dom.Utente = Context.Session("UTENTE")
        Dom.AggiornaDB(Context.Session("DC_OSPITE"))

        cn.Close()
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TabApp As System.Data.DataTable
            TabApp = ViewState("App_AddebitiMultiplo")

            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)

            DD_Cserv.SelectedValue = TabApp.Rows(e.Row.RowIndex).Item(3).ToString
            If TabApp.Rows(e.Row.RowIndex).Item(11).ToString = "ERRORE" Then
                DD_Cserv.BackColor = Drawing.Color.Red
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Red

            End If


            If TabApp.Rows(e.Row.RowIndex).Item(11).ToString = "ERRORECS" Then
                DD_Cserv.BackColor = Drawing.Color.Orange
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Orange

            End If


            If TabApp.Rows(e.Row.RowIndex).Item(11).ToString = "ERROREPRE" Then
                DD_Cserv.BackColor = Drawing.Color.Aqua
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua

            End If
            If Val(TabApp.Rows(e.Row.RowIndex).Item(7).ToString) = 0 And TabApp.Rows(e.Row.RowIndex).Item(11).ToString = "" Then

                e.Row.ForeColor = Drawing.Color.Red

            End If

        End If
    End Sub



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/v1/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer
        Dim MySql As String

        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Progressivo As Long


                MySql = "INSERT INTO Domiciliare_EPersonam (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,Durata,ID_Epersonam) VALUES (?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (Mysql)

                cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdw.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                cmdw.Parameters.AddWithValue("@CodiceOspite", Tabella.Rows(Riga).Item(10))

                Dim DataAppoggio As Date = Now
                Try
                    DataAppoggio = Tabella.Rows(Riga).Item(4)
                Catch ex As Exception

                End Try
                cmdw.Parameters.AddWithValue("@Data", Format(DataAppoggio, "dd/MM/yyyy"))

                Dim AppoD1 As Date
                Dim AppoD2 As Date

                AppoD1 = Tabella.Rows(Riga).Item(5)
                AppoD2 = Tabella.Rows(Riga).Item(6)

                cmdw.Parameters.AddWithValue("@Durata", DateDiff(DateInterval.Minute, AppoD2, AppoD1))


                cmdw.Parameters.AddWithValue("@ID_Epersonam", Val(Tabella.Rows(Riga).Item(9)))
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()


                Dim Dom As New Cls_MovimentiDomiciliare

                Dom.CENTROSERVIZIO = DD_Cserv.SelectedValue
                Dom.CodiceOspite = Tabella.Rows(Riga).Item(10)

                Dom.Data = Tabella.Rows(Riga).Item(4)

                Dom.OraInizio = AppoD1.AddYears(1899).AddMonths(12).AddDays(30)

                Dom.OraFine = AppoD2.AddYears(1899).AddMonths(12).AddDays(30)


                Dom.Operatore = Tabella.Rows(Riga).Item(7)
                If Not TabellaTrascodifica() Then
                    If Tabella.Rows(Riga).Item(8) = "Accreditato" Then
                        Dom.Tipologia = "01"
                    End If
                    If Tabella.Rows(Riga).Item(8) = "Non Accreditato" Then
                        Dom.Tipologia = "02"
                    End If
                    If Tabella.Rows(Riga).Item(8) = "Pasto" Then
                        Dom.Tipologia = "03"
                    End If
                    If Tabella.Rows(Riga).Item(8) = "Pasto + Cena Freda" Then
                        Dom.Tipologia = "04"
                    End If
                    If Tabella.Rows(Riga).Item(8) = "Tutoring" Then
                        Dom.Tipologia = "05"
                    End If
                    If Tabella.Rows(Riga).Item(8) = "DIMISSIONE PROTETTA" Then
                        Dom.Tipologia = "06"
                    End If
                Else
                    Dim Mp As New Cls_TipoDomiciliare


                    Mp.Descrizione = campodb(Tabella.Rows(Riga).Item(8))
                    Mp.LeggiDescrizione(Session("DC_OSPITE"), Mp.Descrizione)

                    Dom.Tipologia = Mp.Codice
                End If


                If Val(campodb(Tabella.Rows(Riga).Item(13))) > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = Val(campodb(Tabella.Rows(Riga).Item(13)))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    If Operatore.Nome = "" Then
                        Operatore.Nome = campodb(Tabella.Rows(Riga).Item(14))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    Dom.Righe(0) = New Cls_MovimentiDomiciliare_Operatori

                    Dom.Righe(0).CodiceOperatore = Val(campodb(Tabella.Rows(Riga).Item(13)))
                    Dom.Righe(0).IdMovimento = Dom.ID
                End If

                If Val(campodb(Tabella.Rows(Riga).Item(15))) > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = Val(campodb(Tabella.Rows(Riga).Item(15)))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    If Operatore.Nome = "" Then
                        Operatore.Nome = campodb(Tabella.Rows(Riga).Item(16))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    Dom.Righe(1) = New Cls_MovimentiDomiciliare_Operatori

                    Dom.Righe(1).CodiceOperatore = Val(campodb(Tabella.Rows(Riga).Item(15)))
                    Dom.Righe(1).IdMovimento = Dom.ID
                End If

                If Val(campodb(Tabella.Rows(Riga).Item(17))) > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = Val(campodb(Tabella.Rows(Riga).Item(17)))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    If Operatore.Nome = "" Then
                        Operatore.Nome = campodb(Tabella.Rows(Riga).Item(18))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    Dom.Righe(2) = New Cls_MovimentiDomiciliare_Operatori

                    Dom.Righe(2).CodiceOperatore = Val(campodb(Tabella.Rows(Riga).Item(17)))
                    Dom.Righe(2).IdMovimento = Dom.ID
                End If


                If Val(campodb(Tabella.Rows(Riga).Item(19))) > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = Val(campodb(Tabella.Rows(Riga).Item(19)))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    If Operatore.Nome = "" Then
                        Operatore.Nome = campodb(Tabella.Rows(Riga).Item(20))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    Dom.Righe(3) = New Cls_MovimentiDomiciliare_Operatori

                    Dom.Righe(3).CodiceOperatore = Val(campodb(Tabella.Rows(Riga).Item(19)))
                    Dom.Righe(3).IdMovimento = Dom.ID
                End If

                If Val(campodb(Tabella.Rows(Riga).Item(21))) > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = Val(campodb(Tabella.Rows(Riga).Item(21)))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    If Operatore.Nome = "" Then
                        Operatore.Nome = campodb(Tabella.Rows(Riga).Item(22))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    Dom.Righe(4) = New Cls_MovimentiDomiciliare_Operatori

                    Dom.Righe(4).CodiceOperatore = Val(campodb(Tabella.Rows(Riga).Item(21)))
                    Dom.Righe(4).IdMovimento = Dom.ID
                End If

                Dom.Utente = Context.Session("UTENTE")
                Dom.AggiornaDB(Context.Session("DC_OSPITE"))

                CheckBox.Checked = False
            End If
        Next

        cn.Close()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Domiciliare Importato');", True)

        Call Btn_Movimenti_Click(sender, e)
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ, "A")
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue, "A")
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If GridView1.Rows(Riga).Cells(11).Text = "&nbsp;" Then
                CheckBox.Checked = True

            End If


        Next
    End Sub

    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim SalvaDb As New OleDbCommand

        Chk_RicaricaDati.Checked = True
        SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Periodo = ? And Tipo = 'D' And Nucleo = ?"
        SalvaDb.Connection = cn
        SalvaDb.Parameters.AddWithValue("@Periodo", Format(Param.MeseFatturazione, "00") & Param.AnnoFatturazione)
        SalvaDb.Parameters.AddWithValue("@Nucleo", cS.EPersonam)
        Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
        If VerificaDB.Read Then
            Dim appoggio As Date = Nothing
            Try
                appoggio = campodb(VerificaDB.Item("UltimaModifica"))
            Catch ex As Exception

            End Try
            If Format(appoggio, "yyyyMMdd") = Format(Now, "yyyyMMdd") Then
                Chk_RicaricaDati.Checked = False
            End If
        End If
        VerificaDB.Close()

        cn.Close()

    End Sub

End Class
