﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class OspitiWeb_ApiV1Epersonam_ImportExtra
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Call StartImport()
    End Sub


    Private Sub StartImport()



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim Token As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Token = LoginPersonam(Context)

        BusinessUnit(Token, Context)
        cn.Close()
    End Sub

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        Dim rawresp As String
        Dim IDutenteTecnico As String = Session("EPersonamUser").ToString.Replace("w", "").Replace("senioruser_", "")

        BusinessUnit = 0
        Try



            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("Cognome", GetType(String))
            Tabella.Columns.Add("Nome", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("Centroservizio", GetType(String))
            Tabella.Columns.Add("Data", GetType(String))
            Tabella.Columns.Add("Tipo", GetType(String))
            Tabella.Columns.Add("Importo", GetType(Double))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("Presente", GetType(String))
            Tabella.Columns.Add("IDEPERSONAM", GetType(String))



            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()


            Dim jArr As JArray = JArray.Parse(rawresp)


            For Each jResults As JToken In jArr


                If BusinessUnit <> Val(jResults.Item("id").ToString) Then
                    BusinessUnit = Val(jResults.Item("id").ToString)
                    If IDutenteTecnico = Val(jResults.Item("id").ToString) Then
                        ImportaMovimenti(BusinessUnit, Token)
                    End If
                End If
            Next
        Catch ex As Exception

        End Try

        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Function



    Private Sub ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String)
        Dim request As New NameValueCollection
        Dim rawresp As String
        Dim cn As OleDbConnection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        Else
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        End If

        Try

            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BusinessID & "/extras/" & DataFatt)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            rawresp = reader.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)


            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()

            If Param.DebugV1 = 1 Then
                Dim NomeFile As String

                Dim serializer As JavaScriptSerializer
                serializer = New JavaScriptSerializer()

                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & BusinessID & "_" & Format(Now, "yyyyMMddHHmmss") & "OSP.xml"
                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                tw.Write(rawresp)
                tw.Close()
            End If



            For Each jTok As JToken In jResults
                rawresp = rawresp & jTok.Item("extras").ToString
                For Each jTok1 As JToken In jTok.Item("extras").Children


                    Dim Ospiti As New ClsOspite

                    Ospiti.CODICEFISCALE = jTok.Item("cf")
                    Ospiti.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospiti.CODICEFISCALE)
                    If Ospiti.CodiceOspite = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga.Item(0) = "ERRORE"
                        myriga.Item(1) = jTok.Item("fullname")
                        myriga.Item(2) = jTok.Item("cf")

                        Tabella.Rows.Add(myriga)
                    Else
                        Dim CentroServizio As New Cls_Movimenti

                        CentroServizio.CodiceOspite = Ospiti.CodiceOspite
                        CentroServizio.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))


                        If CentroServizio.CENTROSERVIZIO = Cmb_CServ.SelectedValue Or Cmb_CServ.SelectedValue = "" Then
                            Dim myriga As System.Data.DataRow = Tabella.NewRow()


                            myriga.Item(0) = Ospiti.CognomeOspite
                            myriga.Item(1) = Ospiti.NomeOspite
                            myriga.Item(2) = Ospiti.CODICEFISCALE


                            myriga.Item(3) = CentroServizio.CENTROSERVIZIO

                            Try
                                Dim DataAppoggio As Date = jTok1.Item("data")
                                myriga.Item(4) = Format(DataAppoggio, "dd/MM/yyyy")
                            Catch ex As Exception

                            End Try

                            Try
                                myriga.Item(5) = jTok1.Item("tipo_extra").Item("cod")
                            Catch ex As Exception

                            End Try

                            Try
                                myriga.Item(6) = jTok1.Item("importo")
                            Catch ex As Exception

                            End Try
                            'Descrizione

                            Dim Tipo As New Cls_Addebito


                            Tipo.Codice = myriga.Item(5)
                            Tipo.Leggi(Session("DC_OSPITE"), Tipo.Codice)

                            If myriga.Item(6) = 0 Then
                                If Tipo.Importo > 0 Then
                                    myriga.Item(6) = Tipo.Importo
                                End If
                            End If


                            Try
                                myriga.Item(7) = jTok1.Item("description")
                            Catch ex As Exception

                            End Try


                            Try
                                myriga.Item(9) = jTok1.Item("created_at")
                            Catch ex As Exception

                            End Try

                            Try
                                Dim cmd As New OleDbCommand()
                                cmd.CommandText = "select * from ADDACR where centroservizio = '" & CentroServizio.CENTROSERVIZIO & "' And CodiceOspite = " & Ospiti.CodiceOspite & " And IDEPERSONAM= ?"
                                cmd.Connection = cn
                                cmd.Parameters.AddWithValue("@DATA", myriga.Item(9))
                                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                If myPOSTreader.Read Then
                                    myriga.Item(8) = "PRESENTE"
                                End If
                                myPOSTreader.Close()
                            Catch ex As Exception

                            End Try


                            Tabella.Rows.Add(myriga)

                        End If
                    End If
                Next
            Next

        
            cn.Close()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)
            Dim DDTipo As DropDownList = DirectCast(e.Row.FindControl("DDTipo"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)


            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString

            Dim TpAdd As New Cls_Addebito


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("Txt_Data"), TextBox)


            TxtData.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString

            TpAdd.UpDateDropBox(Session("DC_OSPITE"), DDTipo)

            DDTipo.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(5).ToString

            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("Txt_Importo"), TextBox)


            TxtImporto.Text = Tabella.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)


            TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(7).ToString

            Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

            If Tabella.Rows(e.Row.RowIndex).Item(0).ToString = "ERRORE" Then
                e.Row.BackColor = Drawing.Color.Red
                CheckBox.Enabled = False
            End If

            If Tabella.Rows(e.Row.RowIndex).Item(8).ToString = "PRESENTE" Then
                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua
            End If



        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Indice As Integer
        Tabella = ViewState("App_AddebitiMultiplo")

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        For Indice = 0 To Tabella.Rows.Count - 1
            Dim TipoAddebito As New Cls_Addebito

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Indice).FindControl("DD_Cserv"), DropDownList)
            Dim DDTipo As DropDownList = DirectCast(GridView1.Rows(Indice).FindControl("DDTipo"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Indice).FindControl("ChkImporta"), CheckBox)

            Dim TxtImporto As TextBox = DirectCast(GridView1.Rows(Indice).FindControl("Txt_Importo"), TextBox)
            If CheckBox.Checked = True Then

                TipoAddebito.Descrizione = ""
                TipoAddebito.Codice = DDTipo.SelectedValue
                TipoAddebito.Leggi(Session("DC_OSPITE"), TipoAddebito.Codice)
                If TipoAddebito.Descrizione = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi importare i movimenti senza TIPO ADDEBITO');", True)
                    Exit Sub
                End If

                If DD_Cserv.SelectedValue = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi importare i movimenti senza centro servizio');", True)
                    Exit Sub
                End If
            End If

        Next

        For Indice = 0 To Tabella.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Indice).FindControl("DD_Cserv"), DropDownList)
            Dim DDTipo As DropDownList = DirectCast(GridView1.Rows(Indice).FindControl("DDTipo"), DropDownList)

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Indice).FindControl("ChkImporta"), CheckBox)

            Dim TxtImporto As TextBox = DirectCast(GridView1.Rows(Indice).FindControl("Txt_Importo"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(GridView1.Rows(Indice).FindControl("TxtDescrizione"), TextBox)
            Dim TxtData As TextBox = DirectCast(GridView1.Rows(Indice).FindControl("Txt_Data"), TextBox)


            If CheckBox.Checked = True Then
                Dim Ospiti As New ClsOspite

                Ospiti.CODICEFISCALE = Tabella.Rows(Indice).Item(2).ToString
                Ospiti.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospiti.CODICEFISCALE)

                Dim DatiCsO As New Cls_DatiOspiteParenteCentroServizio

                DatiCsO.TipoOperazione = ""
                DatiCsO.CodiceOspite = Ospiti.CodiceOspite
                DatiCsO.CentroServizio = DD_Cserv.SelectedValue
                DatiCsO.Leggi(Session("DC_OSPITE"))
                If DatiCsO.TipoOperazione <> "" Then
                    Ospiti.MODALITAPAGAMENTO = DatiCsO.ModalitaPagamento
                End If






                Dim AddAcr As New Cls_AddebitiAccrediti

                AddAcr.CENTROSERVIZIO = DD_Cserv.SelectedValue
                AddAcr.CodiceOspite = Ospiti.CodiceOspite
                AddAcr.TipoMov = "AD"
                AddAcr.CodiceIva = DDTipo.SelectedValue
                AddAcr.IMPORTO = TxtImporto.Text
                AddAcr.Data = TxtData.Text
                AddAcr.Descrizione = TxtDescrizione.Text

                AddAcr.MESECOMPETENZA = Param.MeseFatturazione
                AddAcr.ANNOCOMPETENZA = Param.AnnoFatturazione
                AddAcr.RETTA = "N"

                AddAcr.PARENTE = 0
                AddAcr.RIFERIMENTO = "O"

                Dim CalcolaRette As New Cls_CalcoloRette
                CalcolaRette.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                CalcolaRette.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                If CalcolaRette.QuoteGiornaliere(DD_Cserv.SelectedValue, Ospiti.CodiceOspite, "P", 1, Now) > 0 Then
                    AddAcr.RIFERIMENTO = "P"
                    AddAcr.PARENTE = 1
                End If
                If CalcolaRette.QuoteGiornaliere(DD_Cserv.SelectedValue, Ospiti.CodiceOspite, "P", 2, Now) > 0 Then
                    AddAcr.RIFERIMENTO = "P"
                    AddAcr.PARENTE = 2
                End If
                If CalcolaRette.QuoteGiornaliere(DD_Cserv.SelectedValue, Ospiti.CodiceOspite, "P", 3, Now) > 0 Then
                    AddAcr.RIFERIMENTO = "P"
                    AddAcr.PARENTE = 3
                End If
                If CalcolaRette.QuoteGiornaliere(DD_Cserv.SelectedValue, Ospiti.CodiceOspite, "P", 4, Now) > 0 Then
                    AddAcr.RIFERIMENTO = "P"
                    AddAcr.PARENTE = 4
                End If
                If CalcolaRette.QuoteGiornaliere(DD_Cserv.SelectedValue, Ospiti.CodiceOspite, "P", 5, Now) > 0 Then
                    AddAcr.RIFERIMENTO = "P"
                    AddAcr.PARENTE = 5
                End If
                Dim TipoAdd As New Cls_Addebito


                TipoAdd.Codice = AddAcr.CodiceIva
                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                If AddAcr.RIFERIMENTO = "O" Then
                    If Ospiti.MODALITAPAGAMENTO <> "" Then
                        If Ospiti.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                           Ospiti.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                           Ospiti.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                          Ospiti.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 Then
                            AddAcr.RETTA = "O"
                        End If
                    End If
                End If


                If AddAcr.RIFERIMENTO = "P" Then
                    Dim Parente As New Cls_Parenti

                    Parente.CodiceOspite = Ospiti.CodiceOspite
                    Parente.CodiceParente = AddAcr.PARENTE
                    Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)


                    Dim DatiCservPar As New Cls_DatiOspiteParenteCentroServizio


                    DatiCservPar.TipoOperazione = ""
                    DatiCservPar.CodiceOspite = Parente.CodiceOspite
                    DatiCservPar.CodiceOspite = Parente.CodiceParente
                    DatiCservPar.CentroServizio = DD_Cserv.SelectedValue
                    DatiCservPar.Leggi(Session("DC_OSPITE"))

                    If DatiCservPar.TipoOperazione <> "" Then
                        Parente.MODALITAPAGAMENTO = DatiCservPar.ModalitaPagamento
                    End If




                    If Parente.MODALITAPAGAMENTO <> "" Then
                        If Parente.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                           Parente.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                           Parente.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 And _
                           Parente.MODALITAPAGAMENTO <> TipoAdd.ModalitaPagamentoEpersonam1 Then
                            AddAcr.RETTA = "O"
                        End If
                    End If
                End If





                CalcolaRette.ChiudiDB()


                AddAcr.IDEPERSONAM = Tabella.Rows(Indice).Item(9).ToString
                AddAcr.AggiornaDB(Session("DC_OSPITE"))
            End If
        Next

        StartImport()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception
                    K1 = Nothing
                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If



        End If
    End Sub

    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

    End Sub
    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click

    End Sub
End Class

