﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_ApiV1Epersonam_AllineaCartella
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim VSession As New VariabiliSesssione


        VSession.DC_OSPITE = Session("DC_OSPITE")
        VSession.DC_OSPITIACCESSORI = Session("DC_OSPITIACCESSORI")
        VSession.DC_TABELLE = Session("DC_TABELLE")
        VSession.DC_GENERALE = Session("DC_GENERALE")
        VSession.UTENTE = "senioruser_3044" ' Session("EPersonamUser")
        VSession.PASSWORD = Session("EPersonamPSW")
        VSession.RICERCAOSPITI = ""

        Dim Token As String
        Token = LoginPersonam(VSession)

        '4430
        LeggiCartella(Token, VSession.UTENTE.Replace("senioruser_", ""), "BRNRRT43P08E582P")
        LeggiCartella(Token, 3469, "BRNRRT43P08E582P")
        LeggiCartella(Token, 3044, "BRNRRT43P08E582P")
        LeggiCartella(Token, 4429, "BRNRRT43P08E582P")


        Exit Sub

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From AnagraficaComune where codiceospite > 0 And CodiceParente = 0"
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Cartella As String = ""

            Cartella = LeggiCartella(Token, VSession.UTENTE.Replace("senioruser_", ""), campodb(myPOSTreader.Item("CODICEFISCALE")))
            If Cartella <> "" Then
                Dim M As New ClsOspite

                M.CodiceOspite = Val(campodb(myPOSTreader.Item("codiceospite")))
                M.Leggi(VSession.DC_OSPITE, M.CodiceOspite)

                M.CartellaClinica = Cartella
                M.ScriviOspite(VSession.DC_OSPITE)

            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Private Function LeggiCartella(ByVal Token As String, ByVal BussinesUnit As String, ByVal CodiceFiscale As String) As String
        Try

            Dim UrlConnessione As String



            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/1771/guests?from=2018-02-04T00:00:00+00:00

            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/3302/guests/updated/2018-04-10T00:00:00+00:00

            UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/" & CodiceFiscale & "/periods"

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)
            Dim CartellaAppo As String = ""

            For Each jTok As JToken In jResults
                CartellaAppo = jTok.Item("nr_cartella")
            Next
            Return CartellaAppo

        Catch ex As Exception
            Return ""
        End Try


    End Function



    Private Function LoginPersonam(ByVal data As VariabiliSesssione) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", data.UTENTE)



            request.Add("password", data.PASSWORD)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Class VariabiliSesssione
        Public DC_OSPITE As String
        Public DC_OSPITIACCESSORI As String
        Public DC_TABELLE As String
        Public DC_GENERALE As String
        Public UTENTE As String
        Public PASSWORD As String
        Public RICERCAOSPITI As String
    End Class

    'https://api.e-personam.com/api/v1.0/business_units

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

End Class
