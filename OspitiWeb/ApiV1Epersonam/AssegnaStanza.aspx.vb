﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_ApiV1Epersonam_AssegnaStanza
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim VSession As New VariabiliSesssione


        VSession.DC_OSPITE = Session("DC_OSPITE")
        VSession.DC_OSPITIACCESSORI = Session("DC_OSPITIACCESSORI")
        VSession.DC_TABELLE = Session("DC_TABELLE")
        VSession.DC_GENERALE = Session("DC_GENERALE")
        VSession.UTENTE = Session("EPersonamUser")
        VSession.PASSWORD = Session("EPersonamPSW")
        VSession.RICERCAOSPITI = ""

        Dim Token As String
        Token = LoginPersonam(VSession)


        Dim UrlConnessione As String
        Dim rawresp As String

        UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & VSession.UTENTE.Replace("senioruser_", "") & "/guests?per_page=500"        




        Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing
        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())



        rawresp = reader.ReadToEnd()
        Dim jResults As JArray = JArray.Parse(rawresp)

        For Each jTok As JToken In jResults
            Dim M As New ClsOspite
            M.CodiceOspite = 0
            M.CODICEFISCALE = jTok.Item("cf").ToString()
            M.LeggiPerCodiceFiscale(Session("DC_OSPITE"), M.CODICEFISCALE)

            If M.CodiceOspite > 0 Then
                Dim tipo As String = ""

                Try
                    tipo = jTok.Item("current_status").Item("room").ToString
                Catch ex As Exception

                End Try

                Dim Mov As New Cls_Movimenti

                Mov.CodiceOspite = M.CodiceOspite
                Mov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                Dim MovimentoStanza As New Cls_MovimentiStanze


                MovimentoStanza.CentroServizio = Mov.CENTROSERVIZIO
                MovimentoStanza.CodiceOspite = M.CodiceOspite
                MovimentoStanza.Data = Mov.Data
                MovimentoStanza.Villa = "01"
                MovimentoStanza.Reparto = "0" & Int(Val(tipo) / 100)
                MovimentoStanza.Stanza = ""
                MovimentoStanza.Piano = "0"
                MovimentoStanza.Letto = tipo
                MovimentoStanza.Tipologia = "OC"
                MovimentoStanza.AggiornaDB(Session("DC_OSPITIACCESSORI"))


            End If
        Next


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Function LoginPersonam(ByVal data As VariabiliSesssione) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", data.UTENTE)



            request.Add("password", data.PASSWORD)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Class VariabiliSesssione
        Public DC_OSPITE As String
        Public DC_OSPITIACCESSORI As String
        Public DC_TABELLE As String
        Public DC_GENERALE As String
        Public UTENTE As String
        Public PASSWORD As String
        Public RICERCAOSPITI As String
    End Class

    'https://api.e-personam.com/api/v1.0/business_units

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function
End Class
