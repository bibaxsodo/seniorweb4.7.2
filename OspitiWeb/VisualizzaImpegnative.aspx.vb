﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_VisualizzaImpegnative
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Centro Servizio", GetType(String))
        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Data Nascita", GetType(String))
        MyTable.Columns.Add("Data Inizio", GetType(String))
        MyTable.Columns.Add("Data Fine", GetType(String))
        'MyTable.Columns.Add("Ise", GetType(String))
        'MyTable.Columns.Add("Isee", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        Dim cmd As New OleDbCommand()


        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  IMPEGNATIVE.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
            If RD_TipoEsportazioneF.Checked = True Then
                MySql = "Select * From IMPEGNATIVE Where [DATAFINE] >= ? And [DATAFINE] <= ?  Order By DATAFINE "
            Else
                MySql = "Select * From IMPEGNATIVE Where [DATAINIZIO] >= ? And [DATAINIZIO] <= ?  Order By DATAINIZIO "
            End If
        Else
            If RD_TipoEsportazioneF.Checked = True Then
                MySql = "Select * From IMPEGNATIVE Where [DATAFINE] >= ? And [DATAFINE] <= ?  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " Order By DATAFINE "
            Else
                MySql = "Select * From IMPEGNATIVE Where [DATAINIZIO] >= ? And [DATAINIZIO] <= ?  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " Order By DATAINIZIO "
            End If
        End If

            cmd.CommandText = MySql
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text

            cmd.Parameters.AddWithValue("@DataDal", DataDal)
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim DecOsp As New ClsOspite

                DecOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            myriga(1) = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            myriga(2) = DecOsp.Nome
            myriga(3) = Format(DecOsp.DataNascita, "dd/MM/yyyy")

            myriga(4) = Format(myPOSTreader.Item("DATAINIZIO"), "dd/MM/yyyy")
            If Not IsDBNull(myPOSTreader.Item("DATAFINE")) Then
                myriga(5) = Format(myPOSTreader.Item("DATAFINE"), "dd/MM/yyyy")
            End If

            'myriga(6) = campodb(myPOSTreader.Item("Ise"))
            'myriga(7) = campodb(myPOSTreader.Item("Isee"))

            myriga(6) = campodb(myPOSTreader.Item("Descrizione"))

            MyTable.Rows.Add(myriga)
        Loop
        cn.Close()

            Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
            MyTable.Rows.Add(myriga1)

            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = MyTable
            GridView1.Font.Size = 10
            GridView1.DataBind()


            Dim i As Integer

            For i = 0 To GridView1.Rows.Count - 1

                Dim jk As Integer
                jk = Val(GridView1.Rows(i).Cells(1).Text)

                Dim Csk As String
                Csk = GridView1.Rows(i).Cells(0).Text

                If jk > 0 Then
                    GridView1.Rows(i).Cells(1).Text = "<a href=""#"" onclick=""DialogBox('Anagrafica.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Csk & "');"" >" & GridView1.Rows(i).Cells(1).Text & "</a>"
                End If
            Next
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If

        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VisualizzaImpegnative.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        'Call EseguiJS()
        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
End Class

