﻿
Partial Class OspitiWeb_DatiPagamento
    Inherits System.Web.UI.Page

    Protected Sub OspitiWeb_DatiPagamento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Call EseguiJS()

        If Page.IsPostBack = False Then

            Dim Barra As New Cls_BarraSenior

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)
            Dim P As New Cls_Parenti

            P.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_Parente)

            Dim Bn As New Cls_Banche

            Bn.UpDateDropBox(Session("DC_OSPITE"), DD_Banca)


            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))                
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

            If Request.Item("DATA") <> "" Then
                Dim MyData As New Cls_DatiPagamento

                MyData.CodiceOspite = Session("CODICEOSPITE")
                MyData.CodiceParente = Val(Request.Item("CodiceParente"))
                MyData.Data = Request.Item("DATA")
                MyData.Leggi(Session("DC_OSPITE"))


                DD_Parente.SelectedValue = Val(Request.Item("CodiceParente"))

                Txt_Data.Text = Format(MyData.Data, "dd/MM/yyyy")

                Txt_Abi.Text = MyData.Abi
                Txt_BancaCliente.Text = MyData.Banca
                Txt_Cab.Text = MyData.Cab
                Txt_Ccbancario.Text = MyData.CCBancario
                Txt_Cin.Text = MyData.Cin
                Txt_Int.Text = MyData.CodiceInt

                If Year(MyData.DataMandatoDebitore) < 1901 Then
                    Txt_DataMandato.Text = ""
                Else
                    Txt_DataMandato.Text = Format(MyData.DataMandatoDebitore, "dd/MM/yyyy")
                End If
                Txt_IdMandato.Text = MyData.IdMandatoDebitore
                Txt_CodiceFiscaleCC.Text = MyData.IntestatarioCFConto
                Txt_IntestarioCC.Text = MyData.IntestatarioConto
                Txt_NumCont.Text = MyData.NumeroControllo


                DD_Banca.SelectedValue = MyData.CodiceBanca

                If MyData.First = 1 Then
                    Chk_First.Checked = True
                Else
                    Chk_First.Checked = False
                End If


                Txt_Data.Enabled = False
                DD_Parente.Enabled = False
            Else
                Dim Param As New Cls_Parametri

                Param.LeggiParametri(Session("DC_OSPITE"))

                If Param.IdMandatoDaCF = 0 And Param.MandatoSIA <> "" Then
                    Txt_IdMandato.Text = Param.MandatoSIA
                    If Param.ProgressivoIDMandato > 0 Then
                        Dim xDP As New Cls_DatiPagamento

                        xDP.CodiceOspite = Session("CODICEOSPITE")
                        xDP.CodiceParente = 0
                        xDP.LeggiUltimaData(Session("DC_OSPITE"))

                        Dim UltimoAccoglimento As New Cls_Movimenti
                        UltimoAccoglimento.CodiceOspite = Session("CODICEOSPITE")
                        UltimoAccoglimento.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                        If Format(xDP.Data, "yyyyMMdd") >= Format(UltimoAccoglimento.Data, "yyyyMMdd") Then
                            Txt_IdMandato.Text = xDP.IdMandatoDebitore
                        Else
                            If Session("DC_OSPITE").ToString.IndexOf("SMARIAMONTE") >= 0 Then
                                Txt_IdMandato.Text = Param.MandatoSIA & Format(Param.ProgressivoIDMandato + 1, "0000")
                            Else
                                Txt_IdMandato.Text = Param.MandatoSIA & Format(Param.ProgressivoIDMandato + 1, "00000")
                            End If

                        End If
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare la data di validità');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim VerificaMandato As New Cls_DatiPagamento
        VerificaMandato.CodiceOspite = Session("CODICEOSPITE")
        VerificaMandato.CodiceParente = Val(Request.Item("CodiceParente"))
        If Request.Item("DATA") <> "" Then
            VerificaMandato.Data = Request.Item("DATA")
        Else
            VerificaMandato.Data = Txt_Data.Text
        End If

        VerificaMandato.IdMandatoDebitore = Txt_IdMandato.Text
        If VerificaMandato.VerificaSeUsato(Session("DC_OSPITE")) And Txt_IdMandato.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Id Mandato già inserito');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Txt_IdMandato.Text = "" Then
            If Param.IdMandatoDaCF = 1 Then
                If Val(DD_Parente.SelectedValue) = 0 Then
                    Dim AnagOspite As New ClsOspite

                    AnagOspite.CodiceOspite = Session("CODICEOSPITE")
                    AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)

                    Txt_IdMandato.Text = Param.MandatoSIA & AnagOspite.CODICEFISCALE
                Else
                    Dim AnagParente As New Cls_Parenti

                    AnagParente.CodiceOspite = Session("CODICEOSPITE")
                    AnagParente.CodiceParente = Val(DD_Parente.SelectedValue)
                    AnagParente.Leggi(Session("DC_OSPITE"), AnagParente.CodiceOspite, AnagParente.CodiceParente)

                    Txt_IdMandato.Text = Param.MandatoSIA & AnagParente.CODICEFISCALE
                End If
            End If
        End If

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)


        If Request.Item("DATA") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, x.Nome, "I", "DATIPAGAMENTO", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim MyDataSave As New Cls_DatiPagamento

            MyDataSave.CodiceOspite = Session("CODICEOSPITE")
            MyDataSave.CodiceParente = Val(Request.Item("CodiceParente"))
            MyDataSave.Data = Request.Item("DATA")
            MyDataSave.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(MyDataSave)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, x.Nome, "M", "DATIPAGAMENTO", AppoggioJS)

        End If

        Dim MyData As New Cls_DatiPagamento

        If Request.Item("DATA") <> "" Then
            MyData.CodiceOspite = Session("CODICEOSPITE")
            MyData.CodiceParente = Val(Request.Item("CodiceParente"))
            MyData.Data = Request.Item("DATA")
            MyData.Leggi(Session("DC_OSPITE"))
        End If

        MyData.CodiceOspite = Session("CODICEOSPITE")
        MyData.CodiceParente = DD_Parente.SelectedValue
        MyData.Data = Txt_Data.Text

        MyData.Abi = Txt_Abi.Text
        MyData.Banca = Txt_BancaCliente.Text
        MyData.Cab = Txt_Cab.Text
        MyData.CCBancario = Txt_Ccbancario.Text
        MyData.Cin = Txt_Cin.Text
        MyData.CodiceInt = Txt_Int.Text

        If Txt_DataMandato.Text = "" Then
            MyData.DataMandatoDebitore = Nothing
        Else
            MyData.DataMandatoDebitore = Txt_DataMandato.Text
        End If
        MyData.IdMandatoDebitore = Txt_IdMandato.Text
        MyData.IntestatarioCFConto = Txt_CodiceFiscaleCC.Text
        MyData.IntestatarioConto = Txt_IntestarioCC.Text
        MyData.NumeroControllo = Txt_NumCont.Text
        If Chk_First.Checked = True Then
            MyData.First = 1
        Else
            MyData.First = 0
        End If
        MyData.CodiceBanca = DD_Banca.SelectedValue

        MyData.Utente = Session("UTENTE")

        MyData.Scrivi(Session("DC_OSPITE"))

        Dim Appoggio As New Cls_DatiPagamento
        Appoggio.CodiceOspite = Session("CODICEOSPITE")
        Appoggio.CodiceParente = DD_Parente.SelectedValue
        Appoggio.Data = Txt_Data.Text
        Appoggio.LeggiUltimaData(Session("DC_OSPITE"))

        If Val(DD_Parente.SelectedValue) = 0 Then
            Dim Anagrafica As New ClsOspite

            Anagrafica.CodiceOspite = Session("CODICEOSPITE")
            Anagrafica.Leggi(Session("DC_OSPITE"), Anagrafica.CodiceOspite)
            Anagrafica.ABICLIENTE = Appoggio.Abi
            Anagrafica.BancaCliente = Appoggio.Banca
            Anagrafica.CABCLIENTE = Appoggio.Cab
            Anagrafica.CCBANCARIOCLIENTE = Appoggio.CCBancario
            Anagrafica.CINCLIENTE = Appoggio.Cin
            Anagrafica.IntCliente = Appoggio.CodiceInt
            Anagrafica.IntestatarioCC = Appoggio.IntestatarioConto
            Anagrafica.CodiceFiscaleCC = Appoggio.IntestatarioCFConto
            Anagrafica.NumeroControlloCliente = Appoggio.NumeroControllo

            Dim DO11_BANCA_CIN As String
            Dim DO11_BANCA_ABI As String
            Dim DO11_BANCA_CAB As String
            Dim DO11_BANCA_CONTO As String
            Dim DO11_BANCA_IBAN As String
            Dim DO11_BANCA_IDMANDATO As String

            Dim DO11_BANCA_DATAMANDATO As String


            DO11_BANCA_CIN = Appoggio.Cin
            DO11_BANCA_ABI = Appoggio.Abi
            DO11_BANCA_CAB = Appoggio.Cin
            DO11_BANCA_CONTO = Appoggio.CCBancario
            DO11_BANCA_IBAN = Appoggio.CodiceInt & Format(Val(Appoggio.NumeroControllo), "00") & Appoggio.Cin & Format(Val(Appoggio.Abi), "00000") & Format(Val(Appoggio.Cab), "00000") & AdattaLunghezzaNumero(Appoggio.CCBancario, 12)

            DO11_BANCA_IDMANDATO = Appoggio.IdMandatoDebitore

            DO11_BANCA_DATAMANDATO = Appoggio.DataMandatoDebitore

            Dim MyCheckIban As New CheckIban

            If Appoggio.CodiceInt = "IT" Then
                If MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN) <> "Codice Corretto" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Iban formalmente errato ');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If

            If Param.IdMandatoDaCF = 0 Then
                If DO11_BANCA_IDMANDATO = "" Then

                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('ID MANDATO formalmente errato ');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If



            If Not IsDate(DO11_BANCA_DATAMANDATO) Or DO11_BANCA_DATAMANDATO = "00:00:00" Or DO11_BANCA_DATAMANDATO = "00.00.00" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare DATA MANDATO');", True)
                Call EseguiJS()
                Exit Sub
            End If
            Anagrafica.ScriviOspite(Session("DC_OSPITE"))
        Else
            Dim Anagrafica As New Cls_Parenti

            Anagrafica.CodiceOspite = Session("CODICEOSPITE")
            Anagrafica.CodiceParente = DD_Parente.SelectedValue
            Anagrafica.Leggi(Session("DC_OSPITE"), Anagrafica.CodiceOspite, Anagrafica.CodiceParente)
            Anagrafica.ABICLIENTE = Appoggio.Abi
            Anagrafica.BancaCliente = Appoggio.Banca
            Anagrafica.CABCLIENTE = Appoggio.Cab
            Anagrafica.CCBANCARIOCLIENTE = Appoggio.CCBancario
            Anagrafica.CINCLIENTE = Appoggio.Cin
            Anagrafica.IntCliente = Appoggio.CodiceInt
            Anagrafica.IntestatarioCC = Appoggio.IntestatarioConto
            Anagrafica.CodiceFiscaleCC = Appoggio.IntestatarioCFConto
            Anagrafica.NumeroControlloCliente = Appoggio.NumeroControllo

            Dim DO11_BANCA_CIN As String
            Dim DO11_BANCA_ABI As String
            Dim DO11_BANCA_CAB As String
            Dim DO11_BANCA_CONTO As String
            Dim DO11_BANCA_IBAN As String
            Dim DO11_BANCA_IDMANDATO As String

            Dim DO11_BANCA_DATAMANDATO As String


            DO11_BANCA_CIN = Appoggio.Cin
            DO11_BANCA_ABI = Appoggio.Abi
            DO11_BANCA_CAB = Appoggio.Cin
            DO11_BANCA_CONTO = Appoggio.CCBancario
            DO11_BANCA_IBAN = Appoggio.CodiceInt & Format(Val(Appoggio.NumeroControllo), "00") & Appoggio.Cin & Format(Val(Appoggio.Abi), "00000") & Format(Val(Appoggio.Cab), "00000") & AdattaLunghezzaNumero(Appoggio.CCBancario, 12)

            DO11_BANCA_IDMANDATO = Appoggio.IdMandatoDebitore

            DO11_BANCA_DATAMANDATO = Appoggio.DataMandatoDebitore

            Dim MyCheckIban As New CheckIban

            If Appoggio.CodiceInt = "IT" Then
                If MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN) <> "Codice Corretto" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Iban formalmente errato ');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If

            If Param.IdMandatoDaCF = 0 Then
                If DO11_BANCA_IDMANDATO = "" Then

                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('ID MANDATO formalmente errato ');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If


            If Not IsDate(DO11_BANCA_DATAMANDATO) Or DO11_BANCA_DATAMANDATO = "00:00:00" Or DO11_BANCA_DATAMANDATO = "00.00.00" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare DATA MANDATO');", True)
                Call EseguiJS()
                Exit Sub
            End If

            Anagrafica.ScriviParente(Session("DC_OSPITE"))
        End If


        
        If Param.IdMandatoDaCF = 0 And Param.MandatoSIA <> "" Then
            If Param.ProgressivoIDMandato > 0 Then
                If Txt_IdMandato.Text = Param.MandatoSIA & Format(Param.ProgressivoIDMandato + 1, "00000") Or Txt_IdMandato.Text = Param.MandatoSIA & Format(Param.ProgressivoIDMandato + 1, "0000") Then
                    Param.ProgressivoIDMandato = Param.ProgressivoIDMandato + 1
                    Param.ScriviParametri(Session("DC_OSPITE"))
                End If
            End If
        End If

        Response.Redirect("Elenco_DatiPagamento.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then
            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        Response.Redirect("Elenco_DatiPagamento.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Data.Enabled = True Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Puoi eliminare solo righe inserite');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim MyData As New Cls_DatiPagamento

        If Request.Item("DATA") <> "" Then
            MyData.CodiceOspite = Session("CODICEOSPITE")
            MyData.CodiceParente = Val(Request.Item("CodiceParente"))
            MyData.Data = Request.Item("DATA")
            MyData.Elimina(Session("DC_OSPITE"))


            Dim Appoggio As New Cls_DatiPagamento
            Appoggio.CodiceOspite = Session("CODICEOSPITE")
            Appoggio.CodiceParente = DD_Parente.SelectedValue
            Appoggio.Data = Txt_Data.Text
            Appoggio.LeggiUltimaData(Session("DC_OSPITE"))

            If Val(DD_Parente.SelectedValue) = 0 Then
                Dim Anagrafica As New ClsOspite

                Anagrafica.CodiceOspite = Session("CODICEOSPITE")
                Anagrafica.Leggi(Session("DC_OSPITE"), Anagrafica.CodiceOspite)
                Anagrafica.ABICLIENTE = Appoggio.Abi
                Anagrafica.BancaCliente = Appoggio.Banca
                Anagrafica.CABCLIENTE = Appoggio.Cab
                Anagrafica.CCBANCARIOCLIENTE = Appoggio.CCBancario
                Anagrafica.CINCLIENTE = Appoggio.Cin
                Anagrafica.IntCliente = Appoggio.CodiceInt
                Anagrafica.IntestatarioCC = Appoggio.IntestatarioConto
                Anagrafica.CodiceFiscaleCC = Appoggio.IntestatarioCFConto
                Anagrafica.NumeroControlloCliente = Appoggio.NumeroControllo

                Dim DO11_BANCA_CIN As String
                Dim DO11_BANCA_ABI As String
                Dim DO11_BANCA_CAB As String
                Dim DO11_BANCA_CONTO As String
                Dim DO11_BANCA_IBAN As String
                Dim DO11_BANCA_IDMANDATO As String

                Dim DO11_BANCA_DATAMANDATO As String


                DO11_BANCA_CIN = Appoggio.Cin
                DO11_BANCA_ABI = Appoggio.Abi
                DO11_BANCA_CAB = Appoggio.Cin
                DO11_BANCA_CONTO = Appoggio.CCBancario
                DO11_BANCA_IBAN = Appoggio.CodiceInt & Format(Val(Appoggio.NumeroControllo), "00") & Appoggio.Cin & Format(Val(Appoggio.Abi), "00000") & Format(Val(Appoggio.Cab), "00000") & AdattaLunghezzaNumero(Appoggio.CCBancario, 12)

                DO11_BANCA_IDMANDATO = Appoggio.IdMandatoDebitore

                DO11_BANCA_DATAMANDATO = Appoggio.DataMandatoDebitore


                Anagrafica.ScriviOspite(Session("DC_OSPITE"))            
        Else
                Dim Anagrafica As New Cls_Parenti

                Anagrafica.CodiceOspite = Session("CODICEOSPITE")
                Anagrafica.CodiceParente = DD_Parente.SelectedValue
                Anagrafica.Leggi(Session("DC_OSPITE"), Anagrafica.CodiceOspite, Anagrafica.CodiceParente)
                Anagrafica.ABICLIENTE = Appoggio.Abi
                Anagrafica.BancaCliente = Appoggio.Banca
                Anagrafica.CABCLIENTE = Appoggio.Cab
                Anagrafica.CCBANCARIOCLIENTE = Appoggio.CCBancario
                Anagrafica.CINCLIENTE = Appoggio.Cin
                Anagrafica.IntCliente = Appoggio.CodiceInt
                Anagrafica.IntestatarioCC = Appoggio.IntestatarioConto
                Anagrafica.CodiceFiscaleCC = Appoggio.IntestatarioCFConto
                Anagrafica.NumeroControlloCliente = Appoggio.NumeroControllo


                Dim DO11_BANCA_CIN As String
                Dim DO11_BANCA_ABI As String
                Dim DO11_BANCA_CAB As String
                Dim DO11_BANCA_CONTO As String
                Dim DO11_BANCA_IBAN As String
                Dim DO11_BANCA_IDMANDATO As String

                Dim DO11_BANCA_DATAMANDATO As String


                DO11_BANCA_CIN = Appoggio.Cin
                DO11_BANCA_ABI = Appoggio.Abi
                DO11_BANCA_CAB = Appoggio.Cin
                DO11_BANCA_CONTO = Appoggio.CCBancario
                DO11_BANCA_IBAN = Appoggio.CodiceInt & Format(Val(Appoggio.NumeroControllo), "00") & Appoggio.Cin & Format(Val(Appoggio.Abi), "00000") & Format(Val(Appoggio.Cab), "00000") & AdattaLunghezzaNumero(Appoggio.CCBancario, 12)

                DO11_BANCA_IDMANDATO = Appoggio.IdMandatoDebitore

                DO11_BANCA_DATAMANDATO = Appoggio.DataMandatoDebitore


                Anagrafica.ScriviParente(Session("DC_OSPITE"))
            End If

            Response.Redirect("Elenco_DatiPagamento.aspx")
        End If

    End Sub



    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function

    Protected Sub DD_Parente_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Parente.TextChanged
        Dim xDP As New Cls_DatiPagamento

        xDP.CodiceOspite = Session("CODICEOSPITE")
        xDP.CodiceParente = DD_Parente.SelectedValue
        xDP.LeggiUltimaData(Session("DC_OSPITE"))

        Dim UltimoAccoglimento As New Cls_Movimenti
        UltimoAccoglimento.CodiceOspite = Session("CODICEOSPITE")
        UltimoAccoglimento.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

        If Format(xDP.Data, "yyyyMMdd") >= Format(UltimoAccoglimento.Data, "yyyyMMdd") Then
            Txt_IdMandato.Text = xDP.IdMandatoDebitore
        End If
    End Sub
End Class
