﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes



Partial Class OspitiWeb_ElencoMedici
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='M' Order By Nome"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("CodiceMedico")
            myriga(1) = myPOSTreader.Item("NOME")


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub


    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("GestioneMedici.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='M' And Nome Like ? Order By Nome"
        cmd.Parameters.AddWithValue("Descrizione", Txt_Descrizione.Text)
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("CodiceMedico")
            myriga(1) = myPOSTreader.Item("NOME")

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()


    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestioneMedici.aspx")
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand




        cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='M' Order By Nome"

        cmd.Connection = cn
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("RESIDENZAINDIRIZZO1", GetType(String))
        Tabella.Columns.Add("RESIDENZACOMUNE1", GetType(String))
        Tabella.Columns.Add("RESIDENZAPROVINCIA1", GetType(String))
        Tabella.Columns.Add("Telefono1", GetType(String))
        Tabella.Columns.Add("Telefono2", GetType(String))
        Tabella.Columns.Add("Specializazione", GetType(String))
        Tabella.Columns.Add("Note", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CodiceMedico"))
            myriga(1) = campodb(myPOSTreader.Item("Nome"))
            myriga(2) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            myriga(3) = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            myriga(4) = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            myriga(5) = campodb(myPOSTreader.Item("Telefono1"))
            myriga(6) = campodb(myPOSTreader.Item("Telefono2"))
            myriga(7) = campodb(myPOSTreader.Item("Specializazione"))
            myriga(8) = campodb(myPOSTreader.Item("Note"))
            myriga(9) = campodb(myPOSTreader.Item("CodiceFiscale"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
