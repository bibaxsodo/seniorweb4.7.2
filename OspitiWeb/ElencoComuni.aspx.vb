﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes


Partial Class OspitiWeb_ElencoComuni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim Barra As New Cls_BarraSenior
        Dim k1 As New Cls_SqlString
  

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Call Ricerca()
            Session("ElencoComuniSQLString") = Nothing
        Else
            If Not IsNothing(Session("ElencoComuniSQLString")) Then


                k1 = Session("ElencoComuniSQLString")

                Txt_Descrizione.Text = k1.GetValue("Txt_Descrizione")

                Call Ricerca()
            Else
                Call Ricerca()
            End If
        End If

        Btn_Nuovo.Visible = True
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim CodiceProvincia As String

            CodiceProvincia = Tabella.Rows(d).Item(0).ToString

            Dim CodiceComune As String

            CodiceComune = Tabella.Rows(d).Item(1).ToString


            Response.Redirect("TabellaComuni.aspx?PROVINCIA=" & CodiceProvincia & "&COMUNE=" & CodiceComune & "&PAGINA=" & Grd_ImportoOspite.PageIndex)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Private Sub Ricerca()
        Dim cn As New OleDbConnection


        'Response.Cookies("UserSettings")("PROVINCIA") = Txt_Provincia.Text

        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Descrizione", Txt_Descrizione.Text)

        Session("ElencoComuniSQLString") = k1

        Dim MySql As String
        Dim CondizioneNonInUso As String = " And (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')"

        If Chk_NonInUso.Checked = True Then
            CondizioneNonInUso = ""
        End If


        MySql = "Select * From AnagraficaComune Where Tipologia ='C' "
        If Txt_Descrizione.Text.Trim <> "" Then
            MySql = MySql & " And Nome Like ? "

        End If
        If Txt_Provincia.Text <> "" Then
            MySql = MySql & " And CodiceProvincia = '" & Txt_Provincia.Text & "' "
        End If
        If Chk_TipoOperazione.Checked = True Then
            MySql = MySql & " And (not TipoOperazione IS Null And TipoOperazione <> '') "
        End If


        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.Parameters.AddWithValue("Descrizione", Txt_Descrizione.Text)
        End If

        cmd.CommandText = MySql & CondizioneNonInUso & " Order By Nome"
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Provincia", GetType(String))
        Tabella.Columns.Add("Comune", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("CODICEPROVINCIA")
            myriga(1) = myPOSTreader.Item("CODICECOMUNE")
            Dim AppoggioCom As String

            AppoggioCom = campodb(myPOSTreader.Item("NomeConiuge"))
            If Trim(AppoggioCom) <> "" Then
                AppoggioCom = " (" & AppoggioCom & ")"
            End If

            myriga(2) = campodb(myPOSTreader.Item("NOME")) & AppoggioCom

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call Ricerca()

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("TabellaComuni.aspx")
    End Sub

    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='C' And Nome Like ? Order By Nome"
            cmd.Parameters.AddWithValue("Descrizione", Txt_Descrizione.Text)
        Else
            Dim Condizione As String = ""
            If Txt_Provincia.Text <> "" Then
                Condizione = " And CodiceProvincia = '" & Txt_Provincia.Text & "' "
            End If
            If Chk_TipoOperazione.Checked = True Then
                If Condizione = "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " Not TipoOperazione Is Null "
            End If

            cmd.CommandText = "Select Top 100 * From AnagraficaComune Where Tipologia ='C' " & Condizione & " Order By Nome"
        End If

        cmd.Connection = cn

            Tabella.Clear()
            Tabella.Columns.Add("Provincia", GetType(String))
            Tabella.Columns.Add("Comune", GetType(String))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("RESIDENZAINDIRIZZO1", GetType(String))
            Tabella.Columns.Add("Attenzione", GetType(String))
            Tabella.Columns.Add("RESIDENZACAP1", GetType(String))
            Tabella.Columns.Add("PartitaIVA", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("RESIDENZAPROVINCIA1", GetType(String))
            Tabella.Columns.Add("RESIDENZACOMUNE1", GetType(String))
            Tabella.Columns.Add("CABCLIENTE", GetType(String))
            Tabella.Columns.Add("ABICLIENTE", GetType(String))
            Tabella.Columns.Add("CODXCODF", GetType(String))
            Tabella.Columns.Add("CINCLIENTE", GetType(String))
            Tabella.Columns.Add("CCBANCARIOCLIENTE", GetType(String))
            Tabella.Columns.Add("BANCACLIENTE", GetType(String))
            Tabella.Columns.Add("IntCliente", GetType(String))
            Tabella.Columns.Add("NumeroControlloCliente", GetType(String))
            Tabella.Columns.Add("CodiceCig", GetType(String))
            Tabella.Columns.Add("ImportoSconto", GetType(String))
            Tabella.Columns.Add("NomeConiuge", GetType(String))
            Tabella.Columns.Add("ImportoAZero", GetType(String))
            Tabella.Columns.Add("SeparaInFattura", GetType(String))
            Tabella.Columns.Add("Specializazione", GetType(String))
            Tabella.Columns.Add("TipoOperazione", GetType(String))
            Tabella.Columns.Add("ModalitaPagamento", GetType(String))
            Tabella.Columns.Add("CodiceIva", GetType(String))
            Tabella.Columns.Add("Compensazione", GetType(String))
            Tabella.Columns.Add("CONTOPERESATTO", GetType(String))
            Tabella.Columns.Add("PERIODO", GetType(String))
            Tabella.Columns.Add("Vincolo", GetType(String))
            Tabella.Columns.Add("MastroCliente", GetType(String))
            Tabella.Columns.Add("ContoCliente", GetType(String))
            Tabella.Columns.Add("SottocontoCliente", GetType(String))
            Tabella.Columns.Add("CodiceDestinatario", GetType(String))
            Tabella.Columns.Add("CodificaProvincia", GetType(String))
            Tabella.Columns.Add("RotturaOspite", GetType(String))
            Tabella.Columns.Add("CodiceCup", GetType(String))
            Tabella.Columns.Add("IdDocumento", GetType(String))
            Tabella.Columns.Add("RiferimentoAmministrazione", GetType(String))
            Tabella.Columns.Add("NumeroFatturaDDT", GetType(String))
            Tabella.Columns.Add("Note", GetType(String))


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("CODICEPROVINCIA"))
                myriga(1) = campodb(myPOSTreader.Item("CODICECOMUNE"))
                myriga(2) = campodb(myPOSTreader.Item("NOME"))
            myriga(3) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            myriga(4) = campodb(myPOSTreader.Item("Attenzione"))
            myriga(5) = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            myriga(6) = campodb(myPOSTreader.Item("PartitaIVA"))
            myriga(7) = campodb(myPOSTreader.Item("CodiceFiscale"))
            myriga(8) = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            myriga(9) = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            myriga(10) = campodb(myPOSTreader.Item("CABCLIENTE"))
            myriga(11) = campodb(myPOSTreader.Item("ABICLIENTE"))
            myriga(12) = campodb(myPOSTreader.Item("CODXCODF"))
            myriga(13) = campodb(myPOSTreader.Item("CINCLIENTE"))
            myriga(14) = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            myriga(15) = campodb(myPOSTreader.Item("BANCACLIENTE"))
            myriga(16) = campodb(myPOSTreader.Item("IntCliente"))
            myriga(17) = campodb(myPOSTreader.Item("NumeroControlloCliente"))
            myriga(18) = campodb(myPOSTreader.Item("CodiceCig"))
            myriga(19) = campodb(myPOSTreader.Item("ImportoSconto"))
            myriga(20) = campodb(myPOSTreader.Item("NomeConiuge"))
            myriga(21) = campodb(myPOSTreader.Item("ImportoAZero"))
            myriga(22) = campodb(myPOSTreader.Item("SeparaInFattura"))
            myriga(23) = campodb(myPOSTreader.Item("Specializazione"))
            myriga(24) = campodb(myPOSTreader.Item("TipoOperazione"))
            myriga(25) = campodb(myPOSTreader.Item("ModalitaPagamento"))
            myriga(26) = campodb(myPOSTreader.Item("CodiceIva"))
            myriga(27) = campodb(myPOSTreader.Item("Compensazione"))
            myriga(28) = campodb(myPOSTreader.Item("CONTOPERESATTO"))
            myriga(29) = campodb(myPOSTreader.Item("PERIODO"))
            myriga(30) = campodb(myPOSTreader.Item("Vincolo"))
            myriga(31) = campodb(myPOSTreader.Item("MastroCliente"))
            myriga(32) = campodb(myPOSTreader.Item("ContoCliente"))
            myriga(33) = campodb(myPOSTreader.Item("SottocontoCliente"))
            myriga(34) = campodb(myPOSTreader.Item("CodiceDestinatario"))
            myriga(35) = campodb(myPOSTreader.Item("CodificaProvincia"))
            myriga(36) = campodb(myPOSTreader.Item("RotturaOspite"))
            myriga(37) = campodb(myPOSTreader.Item("CodiceCup"))
            myriga(38) = campodb(myPOSTreader.Item("IdDocumento"))
            myriga(39) = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            myriga(40) = campodb(myPOSTreader.Item("NumeroFatturaDDT"))
            myriga(41) = campodb(myPOSTreader.Item("Note"))
                Tabella.Rows.Add(myriga)
            Loop

            myPOSTreader.Close()
            cn.Close()


            Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub
End Class
