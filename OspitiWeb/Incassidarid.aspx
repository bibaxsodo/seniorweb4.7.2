﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Incassidarid" EnableEventValidation="false" CodeFile="Incassidarid.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajtsauro" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Incassi da Rid</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function DialogBox(Path) {


            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        function DialogBoxx(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }

        #left {
            float: left;
            width: 65%;
            overflow: hidden;
        }

        #right {
            overflow: hidden;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <asp:Button ID="Btn_Dowload" runat="server" Text="Download" Visible="false" />
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Incassi Rid</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Esegui" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton ID="Btn_Salva" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Scarica" Visible="false" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />


                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <ajtsauro:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica" Visible="false">
                                <HeaderTemplate>
                                    Per Periodo
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_AnnoRif" AutoPostBack="True" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" AutoPostBack="True" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Mese :</label>
                                    <asp:DropDownList ID="DD_Mese" OnSelectedIndexChanged="DD_Mese_SelectedIndexChanged" AutoPostBack="True"
                                        runat="server" Width="128px">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Registro :</label>
                                    <asp:DropDownList ID="DD_Registro" OnTextChanged="DD_Registro_TextChanged" AutoPostBack="True" runat="server"></asp:DropDownList>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">Dal Documento :</label>
                                    <asp:TextBox ID="Txt_DalDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Al Documento :</label>
                                    <asp:TextBox ID="Txt_AlDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                </ContentTemplate>
                            </ajtsauro:TabPanel>

                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1" Visible="false">
                                <HeaderTemplate>
                                    Per Data           
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Data Dal :</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="124px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Al :</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="124px"></asp:TextBox>
                                    <br />
                                </ContentTemplate>
                            </ajtsauro:TabPanel>

                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel3">
                                <HeaderTemplate>
                                    Per Data Scadenza e ID           
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Seleziona :</label>
                                    <asp:DropDownList runat="server" ID="DD_SelezionaInvia"></asp:DropDownList><br />
                                    <br />
                                </ContentTemplate>
                            </ajtsauro:TabPanel>

                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Incasso Rid
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>

                                    <asp:DropDownList runat="server" ID="DD_Struttura"
                                        AutoPostBack="True">
                                    </asp:DropDownList><br />

                                    <br />

                                    <label class="LabelCampo">Centro Servizio :</label>
                                    <asp:DropDownList ID="DD_CServ" runat="server" Width="354px"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Banca :</label>
                                    <asp:DropDownList ID="DD_Banca" runat="server" Width="264px"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Data Incasso :</label>
                                    <asp:TextBox ID="Txt_DataIncasso" runat="server" Width="94px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Incasso :</label><asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="408px"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <asp:FileUpload ID="FileUpload1" runat="server" Visible="false" />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server"></asp:Label>
                                    <br />
                                    <div id="msg"></div>
                                    <br />
                                    <div id="left">
                                        <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona" Visible="False" />
                                    </div>
                                    <div id="right">
                                    </div>
                                    <br />

                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />

                                        <Columns>



                                            <asp:TemplateField HeaderText="Seleziona">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NumeroRegistrazione" HeaderText="Numero Registrazione" />
                                            <asp:BoundField DataField="NumeroProtocollo" HeaderText="Numero Protocollo" />
                                            <asp:BoundField DataField="AnnoProtocollo" HeaderText="Anno Protocollo" />
                                            <asp:BoundField DataField="CognomeNome" HeaderText="Cognome Nome" />
                                            <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice Fiscale" />
                                            <asp:BoundField DataField="Iban" HeaderText="Iban" />
                                            <asp:BoundField DataField="First" HeaderText="First" />
                                            <asp:BoundField DataField="DataMandato" HeaderText="Data Mandato" />
                                            <asp:BoundField DataField="IdMandato" HeaderText="Id Mandato" />
                                            <asp:BoundField DataField="ImportoTotale" HeaderText="Importo" />
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>

                                    <asp:Label ID="Lbl_Totali" runat="server"></asp:Label>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                            </asp:Timer>
                                            <asp:Label ID="Lbl_Waiting" runat="server" Text="Label"></asp:Label>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>


                            </ajtsauro:TabPanel>
                        </ajtsauro:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
