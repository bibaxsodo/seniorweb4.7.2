﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GestioneDenaro" CodeFile="GestioneDenaro.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Denaro</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">

        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        var selectedFiles;

        $(document).ready(function () {
            $("input:file").change(
                function () {
                    $("#upload-file-container").css("background", "url(images/UPLOADok.jpg) no-repeat");
                    $("#nomefile").html($("input:file").val().split('/').pop().split('\\').pop());
                }
            );


            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }




        });

    </script>
    <style>
        #upload-file-container {
            background: url(images/UPLOAD.jpg) no-repeat;
            height: 178px;
        }

            #upload-file-container input {
                filter: alpha(opacity=0);
                height: 178px;
                width: 300px;
                opacity: 0;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Ospiti - Gestione Denaro</div>
                    <div class="SottoTitoloOSPITE">
                        <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right;">
                    <div class="DivTastiOspite">
                        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                    </div>
                </td>
            </tr>

            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    <div id="MENUDIV"></div>
                    <br />
                </td>
                <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                    <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                        Width="100%" BorderStyle="None" Style="margin-right: 39px">
                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                            <HeaderTemplate>
                                Gestione Denaro         
                            </HeaderTemplate>
                            <ContentTemplate>

                                <label style="display: block; float: left; width: 160px; color: Gray;">Sado Attuale :</label>
                                <asp:Label ID="Lbl_Saldo" runat="server" ForeColor="Gray"></asp:Label><br />
                                <br />

                                <label class="LabelCampo">Anno :</label>
                                <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="58px"></asp:TextBox>
                                <br />
                                <br />



                                <label class="LabelCampo">Tipo Movimento :</label>
                                &nbsp;<asp:RadioButton ID="RB_VERSAMENTO" GroupName="mTIPOAD" runat="server"
                                    Text="Versamento" ForeColor="Red" Checked="True" />
                                <asp:RadioButton ID="RB_PRELIEVO" GroupName="mTIPOAD" runat="server" Text="Prelievo"
                                    ForeColor="Green" />
                                <br />


                                <br />


                                <label class="LabelCampo">Importo : </label>
                                <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" MaxLength="8" Width="100px"></asp:TextBox>

                                <br />
                                <br />


                                <label class="LabelCampo">Data Registrazione :</label>
                                <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox>
                                <asp:Label ID="Lbl_Numero" runat="server" Visible="False"></asp:Label>
                                <br />
                                <br />

                                <label class="LabelCampo">Tipo Addebito :</label>
                                <asp:DropDownList ID="dd_TipoAddebito" runat="server" Width="216px"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Descrizione :</label>
                                <asp:TextBox ID="Txt_Descrizione" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                                <br />

                                <label class="LabelCampo">Criterio di Ripartizione : </label>
                                <asp:RadioButton ID="RB_NonRipartito" GroupName="Ripartizione" runat="server" />
                                Non Ripartito
     <asp:RadioButton ID="RB_Ospite" GroupName="Ripartizione" runat="server" />
                                Ospite
     <asp:RadioButton ID="RB_Parenti" GroupName="Ripartizione" runat="server" />
                                Parenti<br />
                                <br />
                                <br />
                                <label class="LabelCampo">Parente : </label>
                                <asp:DropDownList ID="DD_Parenti" runat="server" Width="208px"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Periodo:</label>
                                <asp:TextBox ID="Txt_AnnoCompetenza" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                <asp:DropDownList ID="Dd_MeseCompetenza" runat="server" Width="128px">

                                    <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                    <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                    <asp:ListItem Value="4">Aprile</asp:ListItem>
                                    <asp:ListItem Value="5">Maggio</asp:ListItem>
                                    <asp:ListItem Value="6">Giugno</asp:ListItem>
                                    <asp:ListItem Value="7">Luglio</asp:ListItem>
                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                    <asp:ListItem Value="9">Settembre</asp:ListItem>
                                    <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                    <asp:ListItem Value="11">Novembre</asp:ListItem>
                                    <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <br />
                                <asp:Label ID="LBL_Allegato" runat="server" Text=""></asp:Label>
                                <br />
                                <div id="upload-file-container">
                                    <div style="text-align: center; width: 300px; padding-top: 15px;" id="nomefile"></div>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </div>
                                <br />
                                <br />

                            </ContentTemplate>
                        </xasp:TabPanel>
                    </xasp:TabContainer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
