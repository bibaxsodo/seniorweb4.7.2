﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ElencoRegistrazione" EnableEventValidation="false" CodeFile="ElencoRegistrazione.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Elenco Registrazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js?Ver=1"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script type="text/javascript">
        function DialogBox(Path) {
            REDIPS.dialog.myhandler_closed = function () {
                alert('Dialog closed');
            };
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        // event handler called after dialog is closed

        function DialogBoxx(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }


            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');

            $("#IdCloseModal").click(function () {
                __doPostBack("ImPanello", "0");
            });

            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }
        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');

            $("#pippo").css('visibility', 'visible');
            $("#Txt_DataAcco").mask("99/99/9999");
        }
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>

    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 200px;
            height: 240px;
            position: absolute;
            top: 290px;
            right: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Elenco Registrazioni</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">Parente :     
                                    <asp:DropDownList runat="server" Width="381px" ID="DD_OspiteParenti"></asp:DropDownList>
                                    Filtro :
                                    <asp:RadioButton ID="RD_Tutte" runat="server" Text="Tutte" GroupName="Filtro" Checked />
                                    <asp:RadioButton ID="RD_Legate" runat="server" Text="Solo Completamente Chiuse" GroupName="Filtro" />
                                    <asp:RadioButton ID="RD_NonLegate" runat="server" Text="Solo Non Chiuse o Parzialmente Chiuse" GroupName="Filtro" />
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                        <div id="MENUDIV"></div>
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="width: 100%;">
                            <div style="text-align: left; float: left; width: 40%; position: relative; width: auto; margin-left: 5px; top: 10px;" align="left">
                                <label class="LabelCampo">Seleziona Report:</label>
                                <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>
                            </div>
                            <div style="text-align: right; float: right; width: 40%; position: relative; width: auto; margin-right: 5px;" align="right">
                                <a href="#" onclick="VisualizzaDivRegistrazione();">
                                    <img src="../images/nuovo.png" title="Nuovo Incasso/Documento" class="EffettoBottoniTondi" /></a><br />
                            </div>
                        </div>
                        <br />
                        <label class="LabelCampo">
                            <asp:Label ID="Lbl_LabelDeposito" runat="server" Text=""></asp:Label></label>
                        <asp:Label ID="lbl_Deposito" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="Grd_Denaro" runat="server" CellPadding="3"
                                    Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                    BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                                    ShowFooter="True" PageSize="20">
                                    <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="IB_Printer" CommandName="Printer" runat="Server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%#  Eval("Registrazione") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="IB_Delete" CommandName="Richiama" runat="Server" ImageUrl="~/images/edit.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%#  Eval("Registrazione") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Registrazione" HeaderText="N.Reg." />
                                        <asp:BoundField DataField="DataRegistrazione" HeaderText="Data" />
                                        <asp:BoundField DataField="NumeroDocumento" HeaderText="N. Doc." />
                                        <asp:BoundField DataField="DataDocumento" HeaderText="Dt Doc." />
                                        <asp:BoundField DataField="NumeroProtocollo" HeaderText="Protocollo" />
                                        <asp:BoundField DataField="AnnoProtocollo" HeaderText="Anno P." />
                                        <asp:BoundField DataField="CausaleContabile" HeaderText="Causale" />
                                        <asp:BoundField DataField="Descrizione" HeaderText="Desc." />
                                        <asp:BoundField DataField="Dare" HeaderText="Dare" />
                                        <asp:BoundField DataField="Avere" HeaderText="Avere" />
                                        <asp:BoundField DataField="Saldo" HeaderText="Saldo" />
                                        <asp:BoundField DataField="Legato" HeaderText="Legato" />
                                        <asp:BoundField DataField="NumeroBolletta" HeaderText="N. Bol." />
                                        <asp:BoundField DataField="DataBolletta" HeaderText="Dt Bol." />
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                </asp:GridView>

                                <asp:ImageButton ID="ImPanello" runat="server" BackColor="Transparent" ImageUrl="images/diurno_Blanco.png" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div id="blur" style="visibility: hidden;">&nbsp;</div>
        <div id="pippo" style="visibility: hidden;" class="wait">
            <div style="text-align: left; display: inline-table;"><a href="#" onclick="Chiudi();">
                <img src="../images/annulla.png" title="Chiudi" /></a></div>
            <br />
            <br />
            <table>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_Nuovo" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi Movimento" />
                    </td>
                    <td>Incasso / Anticipo </td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_Deposito" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Deposito Cauzionale" />
                    </td>
                    <td>Deposito Cauzionale
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_Caparra" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Caparra Confirmatoria" />
                    </td>
                    <td>Caparra Confirmatoria
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_Documento" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Documento Incasso Retta" />
                    </td>
                    <td>Documento Incasso
                    </td>
                </tr>

            </table>
        </div>
    </form>
</body>
</html>
