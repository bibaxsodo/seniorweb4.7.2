﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes


Partial Class OspitiWeb_ElencoRegioni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Call Ricerca()
            Session("ElencoRegioniSQLString") = Nothing
        Else
            If Not IsNothing(Session("ElencoRegioniSQLString")) Then
                Dim k As New Cls_SqlString

                k = Session("ElencoRegioniSQLString")

                Txt_Descrizione.Text = k.GetValue("Txt_Descrizione")

                Call Ricerca()
            Else
                Call Ricerca()
            End If
        End If

        Btn_Nuovo.Visible = True
    End Sub


    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = Tabella.Rows(d).Item(1).ToString

            
            Response.Redirect("TabelleRegioni.aspx?CODICE=" & Codice & "&PAGINA=" & Grd_ImportoOspite.PageIndex)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Private Sub Ricerca()

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Descrizione", Txt_Descrizione.Text)

        Session("ElencoRegioniSQLString") = k1

        Dim CondizioneNonInUso As String = " And (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')"

        If Chk_NonInUso.Checked = True Then
            CondizioneNonInUso = ""
        End If

        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='R' And Nome Like ?  " & CondizioneNonInUso & " Order By Raggruppamento,Nome"
            cmd.Parameters.AddWithValue("Descrizione", Txt_Descrizione.Text)
        Else
            cmd.CommandText = "Select Top 100 * From AnagraficaComune Where Tipologia ='R'  " & CondizioneNonInUso & " Order By Raggruppamento,Nome"
        End If

        cmd.Connection = cn

        Dim OldRaggruppamento As String = ""

        Tabella.Clear()
        Tabella.Columns.Add("Raggruppamento", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = "*"
            If campodb(myPOSTreader.Item("Raggruppamento")) <> "" Then
                myriga(0) = ""
            End If
            If OldRaggruppamento <> campodb(myPOSTreader.Item("Raggruppamento")) Then
                myriga(0) = myPOSTreader.Item("Raggruppamento")
            End If
            myriga(1) = myPOSTreader.Item("CodiceRegione")

            Dim AppoggioCom As String

            AppoggioCom = campodb(myPOSTreader.Item("NomeConiuge"))
            If Trim(AppoggioCom) <> "" Then
                AppoggioCom = " (" & AppoggioCom & ")"
            End If

            myriga(2) = campodb(myPOSTreader.Item("NOME")) & AppoggioCom
            Dim M As New Cls_ImportoRegione


            M.UltimaData(Session("DC_OSPITE"), myPOSTreader.Item("CodiceRegione"), "")
            If M.Codice = myPOSTreader.Item("CodiceRegione") Then
                myriga(3) = M.UltimiImporti(Session("DC_OSPITE"), myPOSTreader.Item("CodiceRegione"))
            End If
            OldRaggruppamento = campodb(myPOSTreader.Item("Raggruppamento"))
            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Call Ricerca()

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("TabelleRegioni.aspx")
    End Sub



    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

 

        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='R' And Nome Like ? Order By Raggruppamento,Nome"
            cmd.Parameters.AddWithValue("Descrizione", Txt_Descrizione.Text)
        Else
            cmd.CommandText = "Select Top 100 * From AnagraficaComune Where Tipologia ='R' Order By Raggruppamento,Nome"
        End If

        cmd.Connection = cn
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("CODICEREGIONE")
        Tabella.Columns.Add("Descrizione")        
        Tabella.Columns.Add("Tipologia")
        Tabella.Columns.Add("Note")
        Tabella.Columns.Add("CINCLIENTE")
        Tabella.Columns.Add("CCBANCARIOCLIENTE")
        Tabella.Columns.Add("BancaCliente")
        Tabella.Columns.Add("RESIDENZAPROVINCIA1")
        Tabella.Columns.Add("RESIDENZACOMUNE1")
        Tabella.Columns.Add("RESIDENZAINDIRIZZO1")
        Tabella.Columns.Add("Attenzione")
        Tabella.Columns.Add("RESIDENZACAP1")
        Tabella.Columns.Add("PARTITAIVA")
        Tabella.Columns.Add("CodiceFiscale")
        Tabella.Columns.Add("CONTOPERESATTO")
        Tabella.Columns.Add("FuoriRegione")
        Tabella.Columns.Add("CodiceCig")
        Tabella.Columns.Add("CABCLIENTE")
        Tabella.Columns.Add("ABICLIENTE")
        Tabella.Columns.Add("IntCliente")
        Tabella.Columns.Add("NumeroControlloCliente")
        Tabella.Columns.Add("Periodo")
        Tabella.Columns.Add("ImportoAZero")
        Tabella.Columns.Add("ImportoSconto")
        Tabella.Columns.Add("TipoOperazione")
        Tabella.Columns.Add("ModalitaPagamento")
        Tabella.Columns.Add("CodiceIva")
        Tabella.Columns.Add("Raggruppamento")
        Tabella.Columns.Add("SeparaInFattura")
        Tabella.Columns.Add("Compensazione")
        Tabella.Columns.Add("MastroCliente")
        Tabella.Columns.Add("ContoCliente")
        Tabella.Columns.Add("SottoContoCliente")
        Tabella.Columns.Add("RotturaCserv")
        Tabella.Columns.Add("CodiceDestinatario")
        Tabella.Columns.Add("RotturaOspite")
        Tabella.Columns.Add("CodiceCup")
        Tabella.Columns.Add("IdDocumento")
        Tabella.Columns.Add("RiferimentoAmministrazione")
        Tabella.Columns.Add("NumeroFatturaDDT")


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICEREGIONE"))
            myriga(1) = campodb(myPOSTreader.Item("Nome"))            
            myriga(2) = campodb(myPOSTreader.Item("Tipologia"))
            myriga(3) = campodb(myPOSTreader.Item("Note"))
            myriga(4) = campodb(myPOSTreader.Item("CINCLIENTE"))
            myriga(5) = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            myriga(6) = campodb(myPOSTreader.Item("BancaCliente"))
            myriga(7) = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            myriga(8) = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            myriga(9) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            myriga(10) = campodb(myPOSTreader.Item("Attenzione"))
            myriga(11) = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            myriga(12) = campodb(myPOSTreader.Item("PARTITAIVA"))
            myriga(13) = campodb(myPOSTreader.Item("CodiceFiscale"))
            myriga(14) = campodb(myPOSTreader.Item("CONTOPERESATTO"))
            myriga(15) = campodb(myPOSTreader.Item("FuoriRegione"))
            myriga(16) = campodb(myPOSTreader.Item("CodiceCig"))
            myriga(17) = campodb(myPOSTreader.Item("CABCLIENTE"))
            myriga(18) = campodb(myPOSTreader.Item("ABICLIENTE"))
            myriga(19) = campodb(myPOSTreader.Item("IntCliente"))
            myriga(20) = campodb(myPOSTreader.Item("NumeroControlloCliente"))
            myriga(21) = campodb(myPOSTreader.Item("Periodo"))
            myriga(22) = campodb(myPOSTreader.Item("ImportoAZero"))
            myriga(23) = campodb(myPOSTreader.Item("ImportoSconto"))
            myriga(24) = campodb(myPOSTreader.Item("TipoOperazione"))
            myriga(25) = campodb(myPOSTreader.Item("ModalitaPagamento"))
            myriga(26) = campodb(myPOSTreader.Item("CodiceIva"))
            myriga(27) = campodb(myPOSTreader.Item("Raggruppamento"))
            myriga(28) = campodb(myPOSTreader.Item("SeparaInFattura"))
            myriga(29) = campodb(myPOSTreader.Item("Compensazione"))
            myriga(30) = campodb(myPOSTreader.Item("MastroCliente"))
            myriga(31) = campodb(myPOSTreader.Item("ContoCliente"))
            myriga(32) = campodb(myPOSTreader.Item("SottoContoCliente"))
            myriga(33) = campodb(myPOSTreader.Item("RotturaCserv"))
            myriga(34) = campodb(myPOSTreader.Item("CodiceDestinatario"))
            myriga(35) = campodb(myPOSTreader.Item("RotturaOspite"))
            myriga(36) = campodb(myPOSTreader.Item("CodiceCup"))
            myriga(37) = campodb(myPOSTreader.Item("IdDocumento"))
            myriga(38) = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            myriga(39) = campodb(myPOSTreader.Item("NumeroFatturaDDT"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)


    End Sub
End Class
