﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Frm_EstrazionePer730" CodeFile="Frm_EstrazionePer730.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Esporta 730</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="3600" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Statistiche - Esporta 730</div>
                                <div class="SottoTitoloOSPITE">
                                    <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <span class="BenvenutoText">Benvenuto
                                    <asp:Label ID="Lbl_Utente" runat="server"></asp:Label></span>
                                <div class="DivTastiOspite">
                                    <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                    <asp:ImageButton ID="ImageButton4" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />

                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Dati Fatturato

                                        </HeaderTemplate>



                                        <ContentTemplate>

                                            <label class="LabelCampo">Struttura:</label>
                                            <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Centro Servizio :</label>
                                            <asp:DropDownList ID="DD_CServ" runat="server" Width="354px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Data Dal :</label>
                                            <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Data Al :</label>
                                            <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Conto Contabile :</label>
                                            <asp:TextBox ID="Txt_ContoContabile" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 2 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile2" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 3 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile3" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 4 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile4" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 5 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile5" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />


                                            <label class="LabelCampo">Conto Contabile 6 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile6" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 7 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile7" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 8 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile8" runat="server" Width="341px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento1" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento2" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />



                                            <label class="LabelCampo">Filtra per :</label>
                                            <asp:RadioButton ID="RB_Tutti" runat="server" GroupName="FILTRO" Text="Tutti" Checked="true" />
                                            <asp:RadioButton ID="RB_Auto" runat="server" GroupName="FILTRO" Text="Auto" Checked="false" />
                                            <asp:RadioButton ID="Rb_NonAuto" runat="server" GroupName="FILTRO" Text="Non Auto" Checked="false" />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Dati Ospite :</label>
                                            <asp:CheckBox ID="Chk_DatiAggiuntivi" runat="server" Text="" AutoPostBack="true" /><br />
                                            <br />

                                            <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                                BorderWidth="1px" GridLines="Vertical">
                                                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                <AlternatingRowStyle BackColor="Gainsboro" />
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                                    ForeColor="White" />
                                            </asp:GridView>
                                            <br />
                                        </ContentTemplate>
                                    </xasp:TabPanel>

                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                        <HeaderTemplate>
                                            Dati Incassato
                                        </HeaderTemplate>
                                        <ContentTemplate>

                                            <label class="LabelCampo">Struttura:</label>
                                            <asp:DropDownList runat="server" ID="DD_Struttura2" AutoPostBack="true"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Centro Servizio :</label>
                                            <asp:DropDownList ID="DD_CServ2" runat="server" Width="354px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Data Dal :</label>
                                            <asp:TextBox ID="Txt_DataDalIncasso" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Data Al :</label>
                                            <asp:TextBox ID="Txt_DataAlIncasso" runat="server" Width="90px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Causale :</label>
                                            <asp:DropDownList ID="DD_CausaleIncasso" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale 1 :</label>
                                            <asp:DropDownList ID="DD_CausaleIncasso1" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale 2 :</label>
                                            <asp:DropDownList ID="DD_CausaleIncasso2" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Causale 3 :</label>
                                            <asp:DropDownList ID="DD_CausaleIncasso3" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale 4 :</label>
                                            <asp:DropDownList ID="DD_CausaleIncasso4" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Causale Documento :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Causale Documento 2 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc2" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento 3 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc3" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento 4 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc4" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Causale Documento 5 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc5" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento 6 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc6" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Causale Documento 7 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc7" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Causale Documento 8 :</label>
                                            <asp:DropDownList ID="DD_CausaleDocumento_Inc8" runat="server"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Conto Contabile :</label>
                                            <asp:TextBox ID="Txt_ContoContabile_1" runat="server" Width="341px"></asp:TextBox>
                                            <asp:CheckBox ID="Chk_IncludiSoloSeEsente" runat="server" Text="Includi solo se esente iva" />
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 2 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile2_1" runat="server" Width="341px"></asp:TextBox>
                                            <asp:CheckBox ID="Chk_IncludiSoloSeEsente1" runat="server" Text="Includi solo se esente iva" />
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabile 3 :</label>
                                            <asp:TextBox ID="Txt_ContoContabile3_1" runat="server" Width="341px"></asp:TextBox>
                                            <asp:CheckBox ID="Chk_IncludiSoloSeEsente2" runat="server" Text="Includi solo se esente iva" />
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Dati Ospite :</label>
                                            <asp:CheckBox ID="Chk_Sosia" runat="server" Text="" /><br />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Escludi bollo :</label>
                                            <asp:CheckBox ID="Chk_Bollo" runat="server" Text="Usare solo senza filtro su conto" /><br />
                                            <br />
                                            <br />

                                            <asp:Label ID="lbl_errori" runat="server" Text="">
            <br />
          <br />     

                                            </asp:Label>
                                            <asp:GridView ID="GridView2" runat="server" CellPadding="3"
                                                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                                BorderWidth="1px" GridLines="Vertical">
                                                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                <AlternatingRowStyle BackColor="Gainsboro" />
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                                    ForeColor="White" />
                                            </asp:GridView>
                                            <br />
                                        </ContentTemplate>
                                    </xasp:TabPanel>

                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ImageButton4" />
                </Triggers>
            </asp:UpdatePanel>


            <br />
            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>

                    <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                        Attendere prego.....<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="30px" src="images/loading.gif">
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <br />

        </div>
    </form>
</body>
</html>
