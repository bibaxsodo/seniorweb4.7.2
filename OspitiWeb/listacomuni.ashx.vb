﻿Imports System.Data.OleDb

Public Class listacomuni
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim prefixText As String = context.Request.QueryString("q")
        Dim TipoQ As String = context.Request.QueryString("Tipo")
        Dim Utente As String = context.Request.QueryString("UTENTE")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim DbC As New Cls_Login

        DbC.Utente = context.Session("UTENTE")
        DbC.LeggiSP(context.Application("SENIOR"))

        If (prefixText = "") Then Exit Sub


        Dim cn As OleDbConnection


        Dim ConnectionString As String = DbC.Ospiti

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from anagraficacomune where " &
                           "nome like ? + '%' and TIPOLOGIA ='C'")
        cmd.Parameters.AddWithValue("@SearchText", prefixText)

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder
        Dim MioTesto As String

        MioTesto = "<div id='elencobambini' style='width:400px; height:300px;' >"
        MioTesto = MioTesto + "<table  width='400px' >"
        MioTesto = MioTesto + "<td  width='20' background='images/sfondo.jpg' ></td>"
        MioTesto = MioTesto + "<td width='200' background='images/sfondo.jpg'><font size='2' >"
        MioTesto = MioTesto + "<strong>Descrizione</strong></font></td>"
        MioTesto = MioTesto + "</tr>"

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        While myPOSTreader.Read
            sb.Append(myPOSTreader("nome")) _
                .Append(Environment.NewLine)
            Dim CodiceAPPO As String

            CodiceAPPO = "<tr >"
            CodiceAPPO = CodiceAPPO + "<td  width='20' background='images/sfondo.jpg' ><img src='images/figura.jpg' onclick='return ritornacodice(" & Chr(34) & myPOSTreader("CodiceProvincia") & Chr(34) & "," & Chr(34) & myPOSTreader("CodiceComune") & Chr(34) & "," & Chr(34) & myPOSTreader("Nome") & Chr(34) & ")'> </td>"
            CodiceAPPO = CodiceAPPO + "<td width='200' background='images/sfondo.jpg'><font size='2' >" + myPOSTreader("nome") + "</font></td>"
            CodiceAPPO = CodiceAPPO + "</tr>"

            If TipoQ = "1" Then
                CodiceAPPO = "<tr >"
                CodiceAPPO = CodiceAPPO + "<td  width='20' background='images/sfondo.jpg' ><img src='images/figura.jpg' onclick='return ritornacodice_2(" & Chr(34) & myPOSTreader("CodiceProvincia") & Chr(34) & "," & Chr(34) & myPOSTreader("CodiceComune") & Chr(34) & "," & Chr(34) & myPOSTreader("Nome") & Chr(34) & ")'> </td>"
                CodiceAPPO = CodiceAPPO + "<td width='200' background='images/sfondo.jpg'><font size='2' >" + myPOSTreader("nome") + "</font></td>"
                CodiceAPPO = CodiceAPPO + "</tr>"
            End If
            If TipoQ = "2" Then
                CodiceAPPO = "<tr >"
                CodiceAPPO = CodiceAPPO + "<td  width='20' background='images/sfondo.jpg' ><img src='images/figura.jpg' onclick='return ritornacodice_3(" & Chr(34) & myPOSTreader("CodiceProvincia") & Chr(34) & "," & Chr(34) & myPOSTreader("CodiceComune") & Chr(34) & "," & Chr(34) & myPOSTreader("Nome") & Chr(34) & ")'> </td>"
                CodiceAPPO = CodiceAPPO + "<td width='200' background='images/sfondo.jpg'><font size='2' >" + myPOSTreader("nome") + "</font></td>"
                CodiceAPPO = CodiceAPPO + "</tr>"
            End If

            MioTesto = MioTesto + CodiceAPPO
        End While
        cn.Close()

        context.Response.Write(MioTesto)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class