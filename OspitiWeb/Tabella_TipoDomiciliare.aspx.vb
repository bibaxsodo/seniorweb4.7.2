﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_Tabella_TipoDomiciliare
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        If Request.Item("CODICE") = "" Then

            Dim MaxTipoDomiciliare As New Cls_TipoDomiciliare

            Txt_Codice.Text = MaxTipoDomiciliare.MaxTipoDomiciliare(Session("DC_OSPITE"))

            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("TipoAddebito", GetType(String))
            MyTable.Columns.Add("TipoRetta", GetType(String))
            MyTable.Columns.Add("TipoOperatore", GetType(String))
            MyTable.Columns.Add("Ripartizione", GetType(String))
            MyTable.Columns.Add("Tipologia", GetType(String))
            MyTable.Columns.Add("Importo1", GetType(String))
            MyTable.Columns.Add("Importo2", GetType(String))
            MyTable.Columns.Add("Importo3", GetType(String))
            MyTable.Columns.Add("Operatori", GetType(String))
            MyTable.Columns.Add("PrimoIngresso", GetType(String))
            MyTable.Columns.Add("Festivo", GetType(String))
            MyTable.Columns.Add("FasciaOraria", GetType(String))
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = ""
            myriga(1) = ""
            myriga(2) = ""
            myriga(3) = ""
            myriga(4) = ""
            myriga(5) = "0,00"
            myriga(6) = "0,00"
            myriga(7) = "0,00"
            myriga(8) = 0
            myriga(9) = 0
            myriga(10) = 0
            myriga(11) = 0
            MyTable.Rows.Add(myriga)
            ViewState("App_Retta") = MyTable

            Grd_DatiDomicliare.AutoGenerateColumns = False
            Grd_DatiDomicliare.DataSource = MyTable
            Grd_DatiDomicliare.DataBind()

            Call EseguiJS()
            Exit Sub
        End If

        Dim TpAd As New Cls_TipoDomiciliare


        TpAd.Codice = Request.Item("CODICE")
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Txt_Codice.Text = TpAd.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = TpAd.Descrizione

        If TpAd.ConsideraIngresso = 1 Then
            Chk_ConsideraIngressso.Checked = True
        Else
            Chk_ConsideraIngressso.Checked = False
        End If

        If TpAd.FatturazioneFinePeriodo = 1 Then
            Chk_FatturaFineMese.Checked = True
        Else
            Chk_FatturaFineMese.Checked = False
        End If

        Dim InseritoRiga As Boolean = False

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("TipoAddebito", GetType(String))
        MyTable.Columns.Add("TipoRetta", GetType(String))
        MyTable.Columns.Add("TipoOperatore", GetType(String))
        MyTable.Columns.Add("Ripartizione", GetType(String))
        MyTable.Columns.Add("Tipologia", GetType(String))
        MyTable.Columns.Add("Importo1", GetType(String))
        MyTable.Columns.Add("Importo2", GetType(String))
        MyTable.Columns.Add("Importo3", GetType(String))
        MyTable.Columns.Add("Operatori", GetType(String))
        MyTable.Columns.Add("PrimoIngresso", GetType(String))
        MyTable.Columns.Add("Festivo", GetType(String))
        MyTable.Columns.Add("FasciaOraria", GetType(String))


        If Txt_Codice.Text.Trim <> "" Then
            Dim cn As OleDbConnection



            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from TipoDomiciliare_Addebiti where " & _
                               "CodiceTipoDomiciliare = ?  Order by TipoAddebito,TipoRetta,TipoOperatore,Ripartizione, Tipologia,NumeroOperatori,PrimoIngresso")
            cmd.Parameters.AddWithValue("@CodiceTipoDomiciliare", Txt_Codice.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = campodb(myPOSTreader.Item("TipoAddebito"))
                myriga(1) = campodb(myPOSTreader.Item("TipoRetta"))
                myriga(2) = campodb(myPOSTreader.Item("TipoOperatore"))
                myriga(3) = campodb(myPOSTreader.Item("Ripartizione"))
                myriga(4) = campodb(myPOSTreader.Item("Tipologia"))
                myriga(5) = Format(campodbn(myPOSTreader.Item("IMPORTO1")), "#,##0.000000")
                myriga(6) = Format(campodbn(myPOSTreader.Item("IMPORTO2")), "#,##0.000000")
                myriga(7) = Format(campodbn(myPOSTreader.Item("IMPORTO3")), "#,##0.000000")
                myriga(8) = campodbn(myPOSTreader.Item("NumeroOperatori"))
                myriga(9) = campodbn(myPOSTreader.Item("PrimoIngresso"))
                myriga(10) = campodbn(myPOSTreader.Item("Festivo"))
                myriga(11) = campodbn(myPOSTreader.Item("FasciaOraria"))
                MyTable.Rows.Add(myriga)
                InseritoRiga = True
            Loop
            myPOSTreader.Close()
            cn.Close()
        End If
        If Not InseritoRiga Then
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = ""
            myriga(1) = ""
            myriga(2) = ""
            myriga(3) = ""
            myriga(4) = ""
            myriga(5) = "0,00"
            myriga(6) = "0,00"
            myriga(7) = "0,00"
            myriga(8) = "0"
            myriga(9) = "0"
            myriga(10) = "0"
            myriga(11) = "0"
            MyTable.Rows.Add(myriga)
        End If

        ViewState("App_Retta") = MyTable

        Grd_DatiDomicliare.AutoGenerateColumns = False
        Grd_DatiDomicliare.DataSource = MyTable
        Grd_DatiDomicliare.DataBind()

        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If

        Call UpDateTable()


        For i = 0 To MyTable.Rows.Count - 1
            If MyTable.Rows(i).Item(4) <> "I" Then
                If CDbl(MyTable.Rows(i).Item(5)) = 0 Or CDbl(MyTable.Rows(i).Item(6)) = 0 Or CDbl(MyTable.Rows(i).Item(7)) = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Se tipo non fisso devi indicare i 3 importi');", True)
                    Exit Sub
                End If
            End If
        Next


        Dim TpAd As New Cls_TipoDomiciliare
        Dim Vettore(100) As String


        TpAd.Codice = Txt_Codice.Text
        TpAd.Descrizione = Txt_Descrizione.Text

        If Chk_ConsideraIngressso.Checked = True Then
            TpAd.ConsideraIngresso = 1
        Else
            TpAd.ConsideraIngresso = 0
        End If


        If Chk_FatturaFineMese.Checked = True Then
            TpAd.FatturazioneFinePeriodo = 1
        Else
            TpAd.FatturazioneFinePeriodo = 0
        End If


        TpAd.Scrivi(Session("DC_OSPITE"))




        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(TpAd)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "TipoDomiciliare", AppoggioJS)




        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmd.Parameters.AddWithValue("@CodiceTipoDomiciliare", Txt_Codice.Text)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        For i = 0 To MyTable.Rows.Count - 1
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = ("INSERT INTO TipoDomiciliare_Addebiti " & _
                               "(CodiceTipoDomiciliare,TipoAddebito,TipoRetta,TipoOperatore,Ripartizione,Tipologia,Importo1,Importo2,Importo3,NumeroOperatori,PrimoIngresso,Festivo, FasciaOraria) VALUES " & _
                               "(                    ?,           ?,        ?,            ?,            ?,       ?,       ?,       ?,       ?,              ?,            ?,      ?,            ?) ")
            cmdIns.Parameters.AddWithValue("@CodiceTipoDomiciliare", Txt_Codice.Text)
            cmdIns.Parameters.AddWithValue("@TipoAddebito", MyTable.Rows(i).Item(0))
            cmdIns.Parameters.AddWithValue("@TipoRetta", MyTable.Rows(i).Item(1))
            cmdIns.Parameters.AddWithValue("@TipoOperatore", MyTable.Rows(i).Item(2))
            cmdIns.Parameters.AddWithValue("@Ripartizione", MyTable.Rows(i).Item(3))
            cmdIns.Parameters.AddWithValue("@Tipologia", MyTable.Rows(i).Item(4))
            cmdIns.Parameters.AddWithValue("@IMPORTO1", CDbl(MyTable.Rows(i).Item(5)))
            cmdIns.Parameters.AddWithValue("@IMPORTO2", CDbl(MyTable.Rows(i).Item(6)))            
            cmdIns.Parameters.AddWithValue("@IMPORTO3", CDbl(MyTable.Rows(i).Item(7)))
            cmdIns.Parameters.AddWithValue("@NumeroOperatori", CDbl(MyTable.Rows(i).Item(8)))
            cmdIns.Parameters.AddWithValue("@PrimoIngresso", CDbl(MyTable.Rows(i).Item(9)))
            cmdIns.Parameters.AddWithValue("@Festivo", CDbl(MyTable.Rows(i).Item(10)))
            cmdIns.Parameters.AddWithValue("@Fasciaoraria", CDbl(MyTable.Rows(i).Item(11)))
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        Next
        cn.Close()

        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If


        Dim TpAd As New Cls_TipoDomiciliare

        TpAd.Codice = Txt_Codice.Text
        TpAd.Leggi(Session("DC_OSPITE"), TpAd.Codice)


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(TpAd)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "TipoDomiciliare", AppoggioJS)




       
        TpAd.Elimina(Session("DC_OSPITE"))

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmd.Parameters.AddWithValue("@CodiceTipoDomiciliare", Txt_Codice.Text)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),6); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_TipoDomiciliare.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True

        Dim MaxTipoDomiciliare As New Cls_TipoDomiciliare

        Txt_Codice.Text = MaxTipoDomiciliare.MaxTipoDomiciliare(Session("DC_OSPITE"))


        Call Txt_Codice_TextChanged(sender, e)

        Call Txt_Descrizione_TextChanged(sender, e)


        Call EseguiJS()
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoDomiciliare

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_OSPITE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_TipoDomiciliare

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_OSPITE"), x.Descrizione)

                If x.Codice <> "" Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("TipoAddebito", GetType(String))
        MyTable.Columns.Add("TipoRetta", GetType(String))
        MyTable.Columns.Add("TipoOperatore", GetType(String))
        MyTable.Columns.Add("Ripartizione", GetType(String))
        MyTable.Columns.Add("Tipologia", GetType(String))
        MyTable.Columns.Add("Importo1", GetType(String))
        MyTable.Columns.Add("Importo2", GetType(String))
        MyTable.Columns.Add("Importo3", GetType(String))
        MyTable.Columns.Add("Operatori", GetType(String))
        MyTable.Columns.Add("PrimoIngresso", GetType(String))
        MyTable.Columns.Add("Festivo", GetType(String))
        MyTable.Columns.Add("FasciaOraria", GetType(String))

        For i = 0 To Grd_DatiDomicliare.Rows.Count - 1



            Dim DD_TipoAddebito As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_TipoAddebito"), DropDownList)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_TipoRetta"), DropDownList)
            Dim DD_TipoOperatore As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_TipoOperatore"), DropDownList)

            Dim DD_Tipo As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_Tipo"), DropDownList)
            
            Dim DD_Ripartizione As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_Ripartizione"), DropDownList)


            Dim TxtImporto1 As TextBox = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("TxtImporto1"), TextBox)


            Dim TxtImporto2 As TextBox = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("TxtImporto2"), TextBox)


            Dim TxtImporto3 As TextBox = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("TxtImporto3"), TextBox)

            Dim DD_Operatori As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_Operatori"), DropDownList)

            Dim DD_Ingressi As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_Ingressi"), DropDownList)

            Dim DD_Festivo As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_Festivo"), DropDownList)
            Dim DD_FasciaOraria As DropDownList = DirectCast(Grd_DatiDomicliare.Rows(i).FindControl("DD_FasciaOraria"), DropDownList)



            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = DD_TipoAddebito.SelectedValue
            myriga(1) = DD_TipoRetta.SelectedValue
            myriga(2) = DD_TipoOperatore.SelectedValue
            myriga(3) = DD_Ripartizione.SelectedValue
            myriga(4) = DD_Tipo.SelectedValue
            myriga(5) = TxtImporto1.Text
            myriga(6) = TxtImporto2.Text
            myriga(7) = TxtImporto3.Text
            myriga(8) = Val(DD_Operatori.SelectedValue)
            myriga(9) = Val(DD_Ingressi.SelectedValue)
            myriga(10) = Val(DD_Festivo.SelectedValue)
            myriga(11) = Val(DD_FasciaOraria.SelectedValue)

            'DD_FasciaOraria

            MyTable.Rows.Add(myriga)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Grd_DatiDomicliare_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_DatiDomicliare.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = ""
        myriga(2) = ""
        myriga(3) = ""
        myriga(4) = ""
        myriga(5) = "0,00"
        myriga(6) = "0,00"
        myriga(7) = "0,00"
        myriga(8) = 0
        myriga(9) = 0
        myriga(10) = 0
        myriga(11) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_DatiDomicliare.AutoGenerateColumns = False

        Grd_DatiDomicliare.DataSource = MyTable
        'Call EtichetteImporti()
        Grd_DatiDomicliare.DataBind()



        Call EseguiJS()
    End Sub


    Protected Sub Grd_DatiDomicliare_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_DatiDomicliare.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim DD_TipoAddebito As DropDownList = DirectCast(e.Row.FindControl("DD_TipoAddebito"), DropDownList)

            Dim M As New Cls_Addebito

            M.UpDateDropBox(Session("DC_OSPITE"), DD_TipoAddebito)
            DD_TipoAddebito.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(0).ToString


            Dim DD_TipoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoRetta"), DropDownList)

            Dim MR As New Cls_TipoRetta

            MR.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)
            DD_TipoRetta.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(1).ToString

            Dim DD_TipoOperatore As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperatore"), DropDownList)

            Dim MO As New Cls_TipoOperatore

            MO.UpDateDropBox(Session("DC_OSPITE"), DD_TipoOperatore)
            DD_TipoOperatore.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString

            Dim DD_Ripartizione As DropDownList = DirectCast(e.Row.FindControl("DD_Ripartizione"), DropDownList)

            DD_Ripartizione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(4).ToString


            Dim TxtImporto1 As TextBox = DirectCast(e.Row.FindControl("TxtImporto1"), TextBox)

            TxtImporto1.Text = MyTable.Rows(e.Row.RowIndex).Item(5).ToString


            Dim TxtImporto2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto2"), TextBox)

            TxtImporto2.Text = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtImporto3 As TextBox = DirectCast(e.Row.FindControl("TxtImporto3"), TextBox)

            TxtImporto3.Text = MyTable.Rows(e.Row.RowIndex).Item(7).ToString

            Dim DD_operatori As DropDownList = DirectCast(e.Row.FindControl("DD_operatori"), DropDownList)

            DD_operatori.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(8).ToString

            Dim DD_Ingressi As DropDownList = DirectCast(e.Row.FindControl("DD_Ingressi"), DropDownList)

            DD_Ingressi.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(9).ToString

            Dim DD_Festivo As DropDownList = DirectCast(e.Row.FindControl("DD_Festivo"), DropDownList)

            DD_Festivo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(10).ToString


            Dim DD_FasciaOraria As DropDownList = DirectCast(e.Row.FindControl("DD_FasciaOraria"), DropDownList)

            DD_FasciaOraria.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(11).ToString

            Call EseguiJS()
        End If
    End Sub

    Protected Sub Grd_DatiDomicliare_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_DatiDomicliare.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = ""
                        myriga(1) = ""
                        myriga(2) = ""
                        myriga(3) = ""
                        myriga(4) = ""
                        myriga(5) = "0,00"
                        myriga(6) = "0,00"
                        myriga(7) = "0,00"
                        myriga(8) = 0
                        myriga(9) = 0
                        myriga(10) = 0
                        myriga(11) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = ""
                    myriga(1) = ""
                    myriga(2) = ""
                    myriga(3) = ""
                    myriga(4) = ""
                    myriga(5) = "0,00"
                    myriga(6) = "0,00"
                    myriga(7) = "0,00"
                    myriga(8) = 0
                    myriga(9) = 0
                    myriga(10) = 0
                    myriga(11) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_DatiDomicliare.AutoGenerateColumns = False

        Grd_DatiDomicliare.DataSource = MyTable        
        Grd_DatiDomicliare.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class
