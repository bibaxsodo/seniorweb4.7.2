﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_GestioneDomiciliare
    Inherits System.Web.UI.Page

    Protected Sub OspitiWeb_GestioneDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")



        Dim Ktipo As New Cls_TipoDomiciliare


        Ktipo.UpDateDropBox(Session("DC_OSPITE"), DD_Tipologia)


        Dim KOperatore As New Cls_Operatore


        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore1)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore2)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore3)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore4)
        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore5)


        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Txt_Operatore.Text = "0"

        If Val(Request.Item("ID")) > 0 Then
            Dim kMD As New Cls_MovimentiDomiciliare

            kMD.CENTROSERVIZIO = Session("CODICESERVIZIO")
            kMD.CodiceOspite = Session("CODICEOSPITE")
            kMD.ID = Val(Request.Item("ID"))
            kMD.leggi(Session("DC_OSPITE"))

            Txt_DataRegistrazione.Text = Format(kMD.Data, "dd/MM/yyyy")
            Txt_Descrizione.Text = kMD.Descrizione
            Txt_OraInizio.Text = Format(kMD.OraInizio, "HH:mm")
            Txt_OraFine.Text = Format(kMD.OraFine, "HH:mm")
            Txt_Operatore.Text = kMD.Operatore
            DD_Tipologia.SelectedValue = kMD.Tipologia

            If kMD.CreatoDocumentoOspiti = 1 Then
                Chk_CreatoDocumentoOspiti.Checked = True
            Else
                Chk_CreatoDocumentoOspiti.Checked = False
            End If

            If Not IsNothing(kMD.Righe(0)) Then
                DD_Operatore1.SelectedValue = kMD.Righe(0).CodiceOperatore
            End If

            If Not IsNothing(kMD.Righe(1)) Then
                DD_Operatore2.SelectedValue = kMD.Righe(1).CodiceOperatore
                DD_Operatore2.Visible = True

            End If
            If Not IsNothing(kMD.Righe(2)) Then
                DD_Operatore3.SelectedValue = kMD.Righe(2).CodiceOperatore
                DD_Operatore3.Visible = True

            End If
            If Not IsNothing(kMD.Righe(3)) Then
                DD_Operatore4.SelectedValue = kMD.Righe(3).CodiceOperatore
                DD_Operatore4.Visible = True

            End If
            If Not IsNothing(kMD.Righe(4)) Then
                DD_Operatore5.SelectedValue = kMD.Righe(4).CodiceOperatore
                DD_Operatore5.Visible = True
            End If

            Btn_Plus1.Visible = False


            If DD_Operatore5.Visible = True Then
                Btn_Plus5.Visible = True
                Exit Sub
            End If

            If DD_Operatore4.Visible = True Then
                Btn_Meno4.Visible = True
                Btn_Plus4.Visible = True
                Exit Sub
            End If

            If DD_Operatore3.Visible = True Then
                Btn_Meno3.Visible = True
                Btn_Plus3.Visible = True
                Exit Sub
            End If



            If DD_Operatore2.Visible = True Then
                Btn_Meno2.Visible = True
                Btn_Plus2.Visible = True
                Exit Sub
            Else
                Btn_Plus1.Visible = True
                Exit Sub
            End If

            


        End If

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Ora')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99:99"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim OraFine As String
        Dim MinutiFine As String
        Dim OraInizio As String
        Dim MinutiInizio As String


        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If

        If Txt_OraInizio.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Txt_OraFine.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If
        If Txt_OraInizio.Text.Length < 5 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Txt_OraFine.Text.Length < 5 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If


        OraFine = Mid(Txt_OraFine.Text, 1, 2)
        If Val(OraFine) > 24 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If

        MinutiFine = Mid(Txt_OraFine.Text, 4, 2)
        If Val(MinutiFine) >= 60 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If


        OraInizio = Mid(Txt_OraInizio.Text, 1, 2)
        If Val(OraInizio) > 24 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        MinutiInizio = Mid(Txt_OraInizio.Text, 4, 2)
        If Val(MinutiInizio) >= 60 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If


        If Val(Txt_Operatore.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare un operatore domiciliare');", True)
            Exit Sub
        End If


        If DD_Tipologia.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare un tipo domiciliare');", True)
            Exit Sub
        End If


        Dim KS As New Cls_MovimentiDomiciliare


        KS.CENTROSERVIZIO = Session("CODICESERVIZIO")
        KS.CodiceOspite = Session("CODICEOSPITE")

        If Val(Request.Item("ID")) > 0 Then
            KS.ID = Val(Request.Item("ID"))
            KS.leggi(Session("DC_OSPITE"))
        End If

        KS.Data = Txt_DataRegistrazione.Text
        KS.Descrizione = Txt_Descrizione.Text



        KS.OraFine = TimeSerial(OraFine, MinutiFine, 0).AddYears(1899).AddMonths(12).AddDays(30)
        KS.OraInizio = TimeSerial(OraInizio, MinutiInizio, 0).AddYears(1899).AddMonths(12).AddDays(30)
        KS.Operatore = Val(Txt_Operatore.Text)
        KS.Tipologia = DD_Tipologia.SelectedValue

        If Chk_CreatoDocumentoOspiti.Checked = True Then
            KS.CreatoDocumentoOspiti = 1
        Else
            KS.CreatoDocumentoOspiti = 0
        End If

        KS.Utente = Session("UTENTE")

        Dim i As Integer = 0
        For i = 0 To 40
            KS.Righe(i) = Nothing
        Next
        KS.Righe(0) = New Cls_MovimentiDomiciliare_Operatori
        KS.Righe(0).CodiceOperatore = Val(DD_Operatore1.SelectedValue)
        KS.Righe(0).IdMovimento = KS.ID

        If DD_Operatore2.SelectedValue <> "" Then
            KS.Righe(1) = New Cls_MovimentiDomiciliare_Operatori
            KS.Righe(1).CodiceOperatore = Val(DD_Operatore2.SelectedValue)
            KS.Righe(1).IdMovimento = KS.ID
        End If

        If DD_Operatore3.SelectedValue <> "" Then
            KS.Righe(2) = New Cls_MovimentiDomiciliare_Operatori
            KS.Righe(2).CodiceOperatore = Val(DD_Operatore3.SelectedValue)
            KS.Righe(2).IdMovimento = KS.ID
        End If

        If DD_Operatore4.SelectedValue <> "" Then
            KS.Righe(3) = New Cls_MovimentiDomiciliare_Operatori
            KS.Righe(3).CodiceOperatore = Val(DD_Operatore4.SelectedValue)
            KS.Righe(3).IdMovimento = KS.ID
        End If

        If DD_Operatore5.SelectedValue <> "" Then
            KS.Righe(4) = New Cls_MovimentiDomiciliare_Operatori
            KS.Righe(4).CodiceOperatore = Val(DD_Operatore5.SelectedValue)
            KS.Righe(4).IdMovimento = KS.ID
        End If


        KS.AggiornaDB(Session("DC_OSPITE"))


        Response.Redirect("Elenco_MovimentiDomiciliare.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_MovimentiDomiciliare.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Dim KS As New Cls_MovimentiDomiciliare


        KS.CENTROSERVIZIO = Session("CODICESERVIZIO")
        KS.CodiceOspite = Session("CODICEOSPITE")

        If Val(Request.Item("ID")) > 0 Then
            KS.ID = Val(Request.Item("ID"))
            KS.leggi(Session("DC_OSPITE"))
            KS.Elimina(Session("DC_OSPITE"))
            Response.Redirect("Elenco_MovimentiDomiciliare.aspx")
        End If
    End Sub

    Protected Sub Btn_Plus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus3.Click
        If Btn_Plus3.Text = "+" Then
            DD_Operatore4.Visible = True
            Btn_Plus3.Visible = False
            Btn_Plus4.Visible = True
            Txt_Operatore.Text = "4"
            Btn_Meno3.Visible = False
            Btn_Meno4.Visible = True
        End If
    End Sub

    Protected Sub Btn_Plus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus1.Click
        If Btn_Plus1.Text = "+" Then
            DD_Operatore2.Visible = True
            Btn_Plus1.Visible = False
            Btn_Plus2.Visible = True
            Txt_Operatore.Text = "2"
            Btn_Meno2.Visible = True
        End If
    End Sub

    Protected Sub Btn_Plus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus2.Click
        If Btn_Plus2.Text = "+" Then
            DD_Operatore3.Visible = True
            Btn_Plus2.Visible = False
            Btn_Plus3.Visible = True
            Txt_Operatore.Text = "3"
            Btn_Meno2.Visible = False
            Btn_Meno3.Visible = True
        End If
    End Sub

    Protected Sub Btn_Plus4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Plus4.Click
        If Btn_Plus4.Text = "+" Then
            DD_Operatore5.Visible = True
            Btn_Plus4.Visible = False
            Txt_Operatore.Text = "5"
            Btn_Meno4.Visible = False
            Btn_Meno5.Visible = True
        End If
    End Sub

    Protected Sub Btn_Meno2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Meno2.Click
        DD_Operatore2.Visible = False
        DD_Operatore2.SelectedValue = ""
        Btn_Meno2.Visible = False
        Btn_Plus2.Visible = False
        Btn_Plus1.Visible = True
        Btn_Meno2.Visible = False
    End Sub

    Protected Sub Btn_Meno3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Meno3.Click
        DD_Operatore3.Visible = False
        DD_Operatore3.SelectedValue = ""
        Btn_Meno3.Visible = False
        Btn_Plus3.Visible = False
        Btn_Plus2.Visible = True
        Btn_Meno2.Visible = True
    End Sub

    Protected Sub Btn_Meno4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Meno4.Click
        DD_Operatore4.Visible = False
        DD_Operatore4.SelectedValue = ""
        Btn_Meno4.Visible = False
        Btn_Plus4.Visible = False
        Btn_Plus3.Visible = True
        Btn_Meno3.Visible = True
    End Sub

    Protected Sub Btn_Meno5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Meno5.Click
        DD_Operatore5.Visible = False
        DD_Operatore5.SelectedValue = ""
        Btn_Meno5.Visible = False
        Btn_Plus5.Visible = False
        Btn_Plus4.Visible = True
        Btn_Meno4.Visible = True
    End Sub

    Protected Sub DD_Operatore1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Operatore1.TextChanged
        If DD_Operatore1.Text <> "" Then
            If Val(Txt_Operatore.Text) = 0 Then
                Txt_Operatore.Text = 1
            End If
        End If
    End Sub
End Class
