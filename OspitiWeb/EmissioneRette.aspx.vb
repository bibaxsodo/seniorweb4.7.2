﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.Data.OleDb


Partial Class EmissioneRette
    Inherits System.Web.UI.Page


    Private myRequest As System.Net.WebRequest
    Public StDataCond As String


    Private DataP As Date
    Private DataA As Date

    Private AttivaEnte As Date
    Private DataEnte As Date
    Private ScadenzaEnte As Date


    Private ConnessioneGenerale As String
    Private ConnessioneOspiti As String
    Private ConnessioneTabelle As String
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoEstrazione As String
    Private CampoProva As String
    Private CampoErrori As String
    Private CampoWr As String

    Dim Vt_List2(100) As String
    Public DiProva As Boolean
    Dim TabRegistri As New System.Data.DataTable("TabRegistri")




    Private Function LeggiRegistriIVAMETODO(ByVal Anno As Integer, ByVal Numero As Integer) As Integer

        Return 0
        Exit Function

        Dim cnMetodo As OleDbConnection
        Dim DataBaseMetodo As String = ""
        Dim Codice As String = ""

        LeggiRegistriIVAMETODO = 0

        If Session("NomeEPersonam") = "MAGIERA" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPMAGANS;User Id=sa;Password=advenias2012;"
        Else
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPCHARITAS;User Id=sa;Password=advenias2012;"
        End If



        cnMetodo = New Data.OleDb.OleDbConnection(DataBaseMetodo)

        cnMetodo.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from [Y_VistaTabRegistriIvaVenditeProgressivi]  where Tipo = ? And ESERCIZIO = ?")
        cmd.Connection = cnMetodo
        cmd.Parameters.AddWithValue("@Tipo", Numero)
        cmd.Parameters.AddWithValue("@Anno", Anno)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            LeggiRegistriIVAMETODO = campodb(myPOSTreader.Item("NRMAXPROTO"))
        End If
        myPOSTreader.Close()


        cnMetodo.Close()

    End Function

    Public Sub DoWork(ByVal data As Object)

        Call EmissioneR(DataP, DataA, data, DataEnte, ScadenzaEnte)
    End Sub


    Protected Sub EseguiEm()
        If Not IsDate(Txt_DataP.Text) Then

            Exit Sub
        End If
        If Not IsDate(Txt_DataA.Text) Then
            Txt_DataA.Text = Txt_DataP.Text
        End If

        DataP = Txt_DataP.Text
        DataA = Txt_DataA.Text

        If IsDate(Txt_DataEnte.Text) Then
            DataEnte = Txt_DataEnte.Text
        Else
            DataEnte = Nothing
        End If

        If IsDate(Txt_ScadenzaEnte.Text) Then
            ScadenzaEnte = Txt_ScadenzaEnte.Text
        Else
            ScadenzaEnte = Nothing
        End If



        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(Session("DC_OSPITE"))

        SyncLock (Session.SyncRoot())
            Session("CampoProva") = ""
            Session("CampoProva") = "P"

            Session("CampoProgressBar") = "Inizio elaborazione..."
            Session("EmissioneMese") = Dd_Mese.SelectedValue
            Session("EmissioneAnno") = Txt_Anno.Text
        End SyncLock

        Cache("CampoProva" + Session.SessionID) = "P"
        Cache("CampoProgressBar" + Session.SessionID) = "Inizio elaborazione..."
        Cache("EmissioneMese" + Session.SessionID) = Dd_Mese.SelectedValue
        Cache("EmissioneAnno" + Session.SessionID) = Txt_Anno.Text

        'DoWork(Session)


        'Exit Sub
        If Request.Item("EMETTI") = "PROVA" Then
            DoWork(Session)

            Session("CampoErrori") = Cache("CampoErrori" + Session.SessionID) & "Emissione di Prova"
            Session("CampoWr") = Cache("CampoWr" + Session.SessionID)
            If Request.Item("EMETTI") = "PROVA" Then
                Response.Redirect("StampaFattureProva.aspx?RegistrazioneDal=" & Cache("RegistrazioneDal" + Session.SessionID) & "&RegistrazioneAl=" & Cache("RegistrazioneAl" + Session.SessionID) & "&URL=Calcolo.aspx")
            Else
                Response.Redirect("StampaFattureProva.aspx?RegistrazioneDal=" & Cache("RegistrazioneDal" + Session.SessionID) & "&RegistrazioneAl=" & Cache("RegistrazioneAl" + Session.SessionID) & "&URL=EmissioneRette.aspx")
            End If
        Else
            Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

            t.Start(Session)
        End If
    End Sub
    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim Appoggio As String

        Appoggio = Cache("CampoProgressBar" + Session.SessionID) 
        If Appoggio = "FINE" Then
            Lbl_Waiting.Text = ""
            Application(CampoProgressBar) = ""
            Me.Title = "Emissione Retta"


            Session("CampoErrori") = Cache("CampoErrori" + Session.SessionID)
            Session("CampoWr") = Cache("CampoWr" + Session.SessionID)

            If DD_CServ.SelectedValue = "" Then
                Response.Redirect("StampaFattureProva.aspx?RegistrazioneDal=" & Cache("RegistrazioneDal" + Session.SessionID) & "&RegistrazioneAl=" & Cache("RegistrazioneAl" + Session.SessionID) & "&URL=EmissioneRette.aspx")
            Else
                Response.Redirect("StampaFattureProva.aspx?RegistrazioneDal=" & Cache("RegistrazioneDal" + Session.SessionID) & "&RegistrazioneAl=" & Cache("RegistrazioneAl" + Session.SessionID) & "&URL=EmissioneRette.aspx&CENTROSERVIZIO=" & DD_CServ.SelectedValue)
            End If


            REM Cache("RegistrazioneDal" + Session.SessionID) = EmC.BeginNumeroRegistraione
            REM Cache("RegistrazioneAl" + Session.SessionID) = EmC.MaxRegistrazione()
            Exit Sub
        End If

        If Appoggio <> "" Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""5"">" & Appoggio & "</font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & " <br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & Cache("RagioneSocialeFattura" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
            Me.Title = Cache("RagioneSocialeFattura" + Session.SessionID)

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        ConnessioneOspiti = Session("DC_OSPITE")
        ConnessioneTabelle = Session("DC_TABELLE")
        ConnessioneGenerale = Session("DC_GENERALE")


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim NumeroRegistrazioniIni As Integer = 0
        Dim NumeroRegistrazioniFin As Integer = 0
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()




        Session("CampoProgressBar") = ""


        Cache("CampoProgressBar" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                Session("RicercaAnagraficaSQLString")=Nothing
            End Try   
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_DataP.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataA.Text = Format(Now, "dd/MM/yyyy")

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        Call CaricaTabellaRegistri()

        Application(CampoProgressBar) = ""
        Application(CampoProva) = ""
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

        Dd_Mese.Enabled = False
        Txt_Anno.Enabled = False

        If f.Calcolato <> "S" Then
            Btn_Modifica.Enabled = False
            Lbl_NomeOspite.Text = "C'è un errore nel calcolo, o non è stato eseguito"
        End If

        Call EseguiJS()
        If Request.Item("EMETTI") = "PROVA" Then
            Timer1.Enabled = False
            EseguiEm()
        End If
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Private Sub CaricaTabellaRegistri()

        TabRegistri.Clear()
        TabRegistri.Columns.Clear()
        TabRegistri.Columns.Add("Numero", GetType(String))
        TabRegistri.Columns.Add("Registro", GetType(String))
        TabRegistri.Columns.Add("Protocollo1", GetType(String))
        TabRegistri.Columns.Add("Protocollo2", GetType(String))
       

        Dim cn As OleDbConnection
        Dim cnGen As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnGen = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        cnGen.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TipoRegistro Where TipoIVA = 'V' OR TipoIVA = 'C'"

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = TabRegistri.NewRow()
            myriga(0) = campodbN(myPOSTreader.Item("Tipo"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            Dim cmdProg As New OleDbCommand()
            cmdProg.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_DataP.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
            cmdProg.Connection = cnGen
            Dim RdProg As OleDbDataReader = cmdProg.ExecuteReader()
            If RdProg.Read Then
                myriga(2) = campodbN(RdProg.Item(0))
            End If
            RdProg.Close()


            If IsDate(Txt_DataA.Text) Then
                If Year(Txt_DataP.Text) <> Year(Txt_DataA.Text) Then
                    Dim cmdProgA As New OleDbCommand()
                    cmdProgA.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_DataA.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
                    cmdProgA.Connection = cnGen
                    Dim RdProgA As OleDbDataReader = cmdProgA.ExecuteReader()
                    If RdProgA.Read Then
                        myriga(3) = campodbN(RdProgA.Item(0))
                    End If
                    RdProgA.Close()
                Else
                    myriga(3) = 0
                End If
            Else
                myriga(3) = 0
            End If
            Try
                'Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=Senior_UsciteSicurezza_Generale;User Id=sa;Password=advenias2012;
                If cnGen.ConnectionString.IndexOf("Senior_UsciteSicurezza_Generale") >= 0 And campodbN(myPOSTreader.Item("Tipo")) = 12 Then
                    Dim NumeroX As Integer = MaxInt(Year(Txt_DataP.Text), campodbN(myPOSTreader.Item("Tipo")))



                    If Val(myriga(2)) < NumeroX Then
                        myriga(2) = NumeroX

                    End If

                End If

            Catch ex As Exception

            End Try
            
            If Session("NomeEPersonam") = "MAGIERA" Or Session("NomeEPersonam") = "CHARITAS" Then
                Dim NumeroX As Integer = LeggiRegistriIVAMETODO(Year(Txt_DataA.Text), campodbN(myPOSTreader.Item("Tipo")))

                If Val(myriga(3)) < NumeroX Then
                    myriga(3) = NumeroX
                End If
            End If

            TabRegistri.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
        cnGen.Close()

        Session("TabRegistri") = TabRegistri
        BindGen()
    End Sub

    Function MaxInt(ByVal Anno As Integer, ByVal registroiva As Integer) As Long
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection("Provider=SQLOLEDB;Data Source=localhost\SQLEXPRESS;Initial Catalog=Senior_AppaltiUscita_Generale;User Id=sa;Password=advenias2012;")

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select max(NumeroProtocollo) As NmProcollo From MovimentiContabiliTesta Where AnnoProtocollo = ? And RegistroIVA = " & registroiva
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            MaxInt = campodbN(myPOSTreader.Item("NmProcollo"))
        End If

        cn.Close()
    End Function

    Private Sub BindGen()
        TabRegistri = Session("TabRegistri")
        Grd_Registri.AutoGenerateColumns = False
        Grd_Registri.DataSource = TabRegistri


        Grd_Registri.Columns(2).HeaderText = "Protocollo " & Year(Txt_DataP.Text)
        If IsDate(Txt_DataA.Text) Then
            If Year(Txt_DataP.Text) <> Year(Txt_DataA.Text) Then
                Grd_Registri.Columns(3).HeaderText = "Protocollo " & Year(Txt_DataA.Text)
                Grd_Registri.Columns(3).Visible = True
            Else
                Grd_Registri.Columns(3).Visible = False
            End If
        Else
            Grd_Registri.Columns(3).Visible = False
        End If

        Grd_Registri.DataBind()
    End Sub


    Protected Sub Grd_Registri_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grd_Registri.RowCancelingEdit
        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Registri.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblProtocollo1 As Label = DirectCast(e.Row.FindControl("LblProtocollo1"), Label)
            If Not IsNothing(LblProtocollo1) Then

            Else
                Dim TxtProtocollo1 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo1"), TextBox)
                TxtProtocollo1.Text = TabRegistri.Rows(e.Row.RowIndex).Item(2).ToString
            End If


            Dim LblProtocollo2 As Label = DirectCast(e.Row.FindControl("LblProtocollo2"), Label)
            If Not IsNothing(LblProtocollo2) Then

            Else
                Dim TxtProtocollo2 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo2"), TextBox)
                TxtProtocollo2.Text = TabRegistri.Rows(e.Row.RowIndex).Item(3).ToString
            End If
        End If
    End Sub
    Protected Sub Grd_Registri_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grd_Registri.RowEditing
        Grd_Registri.EditIndex = e.NewEditIndex
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grd_Registri.RowUpdated

    End Sub

    Protected Sub Grd_Registri_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grd_Registri.RowUpdating
        Dim TxtProtcollo1 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo1"), TextBox)
        Dim TxtProtcollo2 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo2"), TextBox)


        TabRegistri = Session("TabRegistri")
        Dim row = Grd_Registri.Rows(e.RowIndex)

        TabRegistri.Rows(row.DataItemIndex).Item(2) = TxtProtcollo1.Text
        TabRegistri.Rows(row.DataItemIndex).Item(3) = TxtProtcollo2.Text



        Session("TabRegistri") = TabRegistri

        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()

    End Sub


    Public Sub EmissioneR(ByVal DataPr As Date, ByVal DataAn As Date, ByRef data As Object, ByVal DataEnte As Date, ByVal ScadenzaEnte As Date)
        Dim MyRs As New ADODB.Recordset
        Dim DocumentoAl As Long, DocumentoDal As Long

        Dim Mese As Integer
        Dim InizioVerificaRegistrazione As Long
        Dim BeginNumeroRegistraione As Long
        Dim EmC As New Cls_EmissioneRetta

        Dim StringaErrore As String
        Dim StringaWarning As String
        Dim MeseContr As Integer
        Dim Anno As Integer

        Dim CentroServizio As String = ""

        If DD_CServ.SelectedValue.Trim <> "" Then
            CentroServizio = DD_CServ.SelectedValue
        End If

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data

        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(ConnessioneOspiti)

        Dim ParametriGenerale As New Cls_ParametriGenerale


        ParametriGenerale.Elaborazione(ConnessioneGenerale)

        LeggiParametri.Elaborazione(ConnessioneOspiti)

        Mese = LeggiParametri.MeseFatturazione
        MeseContr = LeggiParametri.MeseFatturazione
        Anno = LeggiParametri.AnnoFatturazione
        EmC.ConnessioneGenerale = ConnessioneGenerale
        EmC.ConnessioneOspiti = ConnessioneOspiti
        EmC.ConnessioneTabelle = ConnessioneTabelle

        EmC.ApriDB()

        Session("CampoProva") = "P"
        EmC.EliminaTemporanei()
        EmC.EmissioneDiProva = "P"


        TabRegistri = SessioneTP("TabRegistri")

        Dim Indice As Integer



        For Indice = 0 To TabRegistri.Rows.Count - 1


            EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = Val(TabRegistri.Rows(Indice).Item(2))

            If Val(TabRegistri.Rows(Indice).Item(3)) > 0 Then
                EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = Val(TabRegistri.Rows(Indice).Item(3))
            End If
        Next


        InizioVerificaRegistrazione = EmC.MaxRegistrazione()

        BeginNumeroRegistraione = EmC.MaxRegistrazione()



        StringaErrore = ""
        StringaWarning = ""

        If LeggiParametri.OrdinamentoCR = 1 Then
            SessioneTP("CampoProgressBar") = "Crea punto ordinamento..."
            SessioneTP("RagioneSocialeFattura") = ""


            Cache("CampoProgressBar" + Session.SessionID) = "Crea punto ordinamento..."
            Cache("RagioneSocialeFattura" + Session.SessionID) = ""

            For Indice = 0 To TabRegistri.Rows.Count - 1
                EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataPr), Val(TabRegistri.Rows(Indice).Item(0)))
                EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataAn), Val(TabRegistri.Rows(Indice).Item(0)))
            Next
        End If

        SessioneTP("CampoProgressBar") = "Elaborazione Comune..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Comune..."
        If LeggiParametri.EmissioneComuni = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaComune(MeseContr, Anno, CentroServizio, DataPr, DataAn, data)
                Else
                    Call EmC.CreaRettaComune(MeseContr, Anno, CentroServizio, DataAn, DataAn, data)
                End If
            Else
                Call EmC.CreaRettaComune(MeseContr, Anno, CentroServizio, DataEnte, DataAn, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Regione..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Regione..."
        If LeggiParametri.EmissioneRegione = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaRegione(MeseContr, Anno, CentroServizio, DataPr, data)
                Else
                    Call EmC.CreaRettaRegione(MeseContr, Anno, CentroServizio, DataAn, data)
                End If
            Else
                Call EmC.CreaRettaRegione(MeseContr, Anno, CentroServizio, DataEnte, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Regione Raggruppata..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Regione Raggruppata..."
        If LeggiParametri.EmissioneRegione = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, CentroServizio, DataPr, data)
                Else
                    Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, CentroServizio, DataAn, data)
                End If
            Else
                Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, CentroServizio, DataEnte, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Jolly..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Jolly..."
        If LeggiParametri.EmissioneJolly = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaJolly(MeseContr, Anno, CentroServizio, DataPr, DataPr, data)
                Else
                    Call EmC.CreaRettaJolly(MeseContr, Anno, CentroServizio, DataPr, DataPr, data)
                End If
            Else
                Call EmC.CreaRettaJolly(MeseContr, Anno, CentroServizio, DataPr, DataPr, data)
            End If
        End If



        If LeggiParametri.OrdinamentoCR = 0 Then
            SessioneTP("CampoProgressBar") = "Crea punto ordinamento..."
            SessioneTP("RagioneSocialeFattura") = ""


            Cache("CampoProgressBar" + Session.SessionID) = "Crea punto ordinamento..."
            Cache("RagioneSocialeFattura" + Session.SessionID) = ""

            For Indice = 0 To TabRegistri.Rows.Count - 1
                EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataPr), Val(TabRegistri.Rows(Indice).Item(0)))
                EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataAn), Val(TabRegistri.Rows(Indice).Item(0)))
            Next
        End If



        SessioneTP("CampoProgressBar") = "Elaborazione Ospiti..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Ospiti..."

        If LeggiParametri.EmissioneOspite = 0 Then
            If IsDate(DataPr) Then
                Call EmC.CreaRettaOspite(MeseContr, Anno, CentroServizio, DataAn, DataPr, data)
            Else
                Call EmC.CreaRettaOspite(MeseContr, Anno, CentroServizio, DataAn, DataPr, data)
            End If
        End If

        SessioneTP("CampoProgressBar") = "Elaborazione Parenti..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Parenti..."
        If LeggiParametri.EmissioneParente = 0 Then
            If IsDate(DataPr) Then
                Call EmC.CreaRettaParente(MeseContr, Anno, CentroServizio, DataAn, DataPr, data)
            Else
                Call EmC.CreaRettaParente(MeseContr, Anno, CentroServizio, DataAn, DataPr, data)
            End If
        End If
        LeggiParametri.InElaborazione(ConnessioneOspiti)

        If EmC.ComuniIntestatiOspiti() Then
            EmC.ModificaFatturaComuniIntestatario()
        End If

        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)
        Call EmC.AssemblaFattureOspitiParenti()

        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)
        If LeggiParametri.RaggruppaRicavi = 1 Then
            Call EmC.RaggruppaContiRicavo()
        End If

        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)

        SessioneTP("CampoProgressBar") = "Ordina Protocolli..."
        SessioneTP("RagioneSocialeFattura") = ""

        Cache("CampoProgressBar" + Session.SessionID) = "Ordina Protocolli..."
        Cache("RagioneSocialeFattura" + Session.SessionID) = ""
        LeggiParametri.InElaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)

        For Indice = 0 To TabRegistri.Rows.Count - 1
            DocumentoAl = EmC.CalcolaProgressivo(Year(DataAn), Val(TabRegistri.Rows(Indice).Item(0)))
            DocumentoDal = EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) + 1
            If DocumentoAl > DocumentoDal Then
                LeggiParametri.Elaborazione(ConnessioneOspiti)
                ParametriGenerale.Elaborazione(ConnessioneGenerale)
                Call EmC.RiordinaFatture(DocumentoDal, DocumentoAl, Val(TabRegistri.Rows(Indice).Item(0)), Anno, Mese, EmC.StringaErrore, Year(DataA), ConnessioneGenerale, ConnessioneOspiti)
            End If

            If Format(DataAn, "ddMMyyyy") <> Format(DataPr, "ddMMyyyy") Then
                DocumentoAl = EmC.CalcolaProgressivo(Year(DataPr), Val(TabRegistri.Rows(Indice).Item(0)))
                DocumentoDal = EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) + 1
                If DocumentoAl > DocumentoDal Then
                    LeggiParametri.Elaborazione(ConnessioneOspiti)
                    ParametriGenerale.Elaborazione(ConnessioneGenerale)
                    Call EmC.RiordinaFatture(DocumentoDal, DocumentoAl, Val(TabRegistri.Rows(Indice).Item(0)), Anno, Mese, EmC.StringaErrore, Year(DataPr), ConnessioneGenerale, ConnessioneOspiti)
                End If
            End If
        Next
        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)

        If Txt_DataScadenza.Text <> "" Then
            SessioneTP("CampoProgressBar") = "Inserisci Scadenze Fatture..."
            Cache("CampoProgressBar" + Session.SessionID) = "Inserisci Scadenze Fatture..."

            'Call ScadenzaFatture()
            Call EmC.ScadenzaFatture(Txt_DataScadenza.Text, ConnessioneGenerale, ScadenzaEnte, SessioneTP("UTENTE"))
        End If
        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)

        SessioneTP("CampoProgressBar") = "Termine operazione..."

        SessioneTP("RegistrazioneDal") = EmC.BeginNumeroRegistraione
        SessioneTP("RegistrazioneAl") = EmC.MaxRegistrazione()


        Cache("CampoProgressBar" + Session.SessionID) = "Termine operazione..."
        Cache("RegistrazioneDal" + Session.SessionID) = EmC.BeginNumeroRegistraione
        Cache("RegistrazioneAl" + Session.SessionID) = EmC.MaxRegistrazione()
        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)




        SessioneTP("CampoErrori") = EmC.StringaErrore
        SessioneTP("CampoWr") = EmC.StringaWarning
        SessioneTP("CampoProgressBar") = "FINE"

        If IsNothing(EmC.StringaErrore) Then
            EmC.StringaErrore = ""
        End If
        If IsNothing(EmC.StringaWarning) Then
            EmC.StringaWarning = ""
        End If
        Cache("CampoErrori" + Session.SessionID) = EmC.StringaErrore
        Cache("CampoWr" + Session.SessionID) = EmC.StringaWarning
        Cache("CampoProgressBar" + Session.SessionID) = "FINE"

        LeggiParametri.Elaborazione(ConnessioneOspiti)
        ParametriGenerale.Elaborazione(ConnessioneGenerale)
        'EmC.ConnessioneGenerale = ConnessioneGenerale
        'EmC.ConnessioneOspiti = ConnessioneOspiti
        'EmC.ConnessioneTabelle = ConnessioneTabelle
        NoteSuDocumenti(ConnessioneOspiti, ConnessioneTabelle, ConnessioneGenerale)


        If LeggiParametri.SplitSuEnti = 1 Then
            EmC.SpliSuEnti()
        End If

        EmC.CloseDB()

        LeggiParametri.FineElaborazione(ConnessioneOspiti)
        ParametriGenerale.FineElaborazione(ConnessioneGenerale)

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function




    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Not IsDate(Txt_DataP.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Exit Sub
        End If
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno formalmente errato');", True)
            Exit Sub
        End If

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.PeriodoFatturazioneSuServizio = 1 Then
            If DD_CServ.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il centro servizio');", True)
                Exit Sub
            End If
        End If
        If Param.PeriodoFatturazioneSuServizio = 0 Then
            If DD_CServ.SelectedValue <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare il centro servizio');", True)
                Exit Sub
            End If
        End If
        If Param.Calcolato <> "S" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Eseguire Calcolo Rette');", True)
            Exit Sub
        End If

        If Param.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If

        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from notifiche ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()

        Call EseguiEm()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Btn_Aggiorna_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Aggiorna.Click

        Call CaricaTabellaRegistri()
        BindGen()
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub



    Public Sub NoteSuDocumenti(ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select *  from Temp_MovimentiContabiliTesta ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim PostRead As OleDbDataReader = cmd.ExecuteReader()

        Do While PostRead.Read
            Dim Documento As New Cls_MovimentoContabile

            Documento.NumeroRegistrazione = campodbN(PostRead.Item("NumeroRegistrazione"))
            Documento.Leggi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)

            Dim M As New Cls_DescrizioneSuDocumento

            M.ModificaDescrizioneDocumento(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
        Loop
        PostRead.Close()

        cn.Close()
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub DD_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CServ.SelectedIndexChanged
        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))


        If f.PeriodoFatturazioneSuServizio = 1 Then
            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = DD_CServ.SelectedValue
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            If Cserv.MeseFatturazione > 0 Then
                Dd_Mese.SelectedValue = Cserv.MeseFatturazione
                Txt_Anno.Text = Cserv.AnnoFatturazione
            Else
                Dd_Mese.SelectedValue = f.MeseFatturazione
                Txt_Anno.Text = f.AnnoFatturazione
            End If
        End If
    End Sub
End Class
