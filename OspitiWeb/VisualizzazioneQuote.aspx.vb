﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports BCrypt.Net
Imports System.Web.Script.Serialization

Partial Class OspitiWeb_VisualizzazioneQuote
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Lbl_Utente.Text = Session("UTENTE")


        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub DD_Struttura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.SelectedIndexChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub ProspettoPresenze(ByVal Cancella As Boolean)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand()


        MyTable.Clear()
        MyTable.Columns.Clear()        
        MyTable.Columns.Add("Nome", GetType(String)) '1
        MyTable.Columns.Add("Data Nascita", GetType(String)) '0
        MyTable.Columns.Add("Codice Fiscale", GetType(String)) '0


        MyTable.Columns.Add("Data", GetType(String)) '2
        If Chk_Regione.Checked = True Then
            MyTable.Columns.Add("Regione Ragione Sociale", GetType(String)) '3
            MyTable.Columns.Add("Regione Tipo Importo", GetType(String)) '4
            MyTable.Columns.Add("Regione Importo", GetType(String)) '5
        End If

        If Chk_Ospite.Checked = True Then
            MyTable.Columns.Add("Ospite Importo", GetType(String)) '6
        End If
        If Chk_Parente.Checked = True Then
            MyTable.Columns.Add("Parenti Importo", GetType(String)) '7
        End If
        If Chk_Comune.Checked = True Then
            MyTable.Columns.Add("Comune Importo", GetType(String)) '8
            MyTable.Columns.Add("Comune Decodifica", GetType(String)) '9
        End If



        MyTable.Columns.Add("Listino", GetType(String)) '10 



        Dim k As New Cls_CalcoloRette
        k.STRINGACONNESSIONEDB = Session("DC_OSPITE")
        k.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))



        MySql = "Select * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite >0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "') > 0 OR " & _
                                                        " (" & _
                                                        " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                        " And " & _
                                                        " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                        " )" & _
                                                        " ) order by Nome"


        cmd.CommandText = MySql
        Dim DataInizio As Date = Txt_DataDal.Text
        Dim DataFine As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataInizio)
        cmd.Parameters.AddWithValue("@DataAl", DataFine)
        cmd.Parameters.AddWithValue("@DataDal", DataInizio)
        cmd.Parameters.AddWithValue("@DataDal", DataInizio)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim Indice As Integer = 0
            Dim i As Integer = 0
            Dim XS As New QuoteOspitiPeriodoCFs


            Dim Centro1 As String = Cmb_CServ.SelectedValue
            Dim Anag As New ClsOspite
            Anag.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Anag.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite)


            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTORETTA]  where codiceospite = ? and centroservizio = ?   union " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOOSPITE] where codiceospite = ? and centroservizio = ?  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOCOMUNE] where codiceospite = ? and centroservizio = ?  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOJOLLY] where codiceospite = ? and centroservizio = ? union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTOPARENTI where codiceospite = ? and centroservizio = ?  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTORETTA  where codiceospite = ? and centroservizio = ?  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  STATOAUTO  where codiceospite = ? and centroservizio = ?  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MOVIMENTI  where codiceospite = ? and centroservizio = ? AND ( TIPOMOV = '05') union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MODALITA where codiceospite = ? and centroservizio = ?   ORDER BY DATA ")

            cmd1.Connection = cn
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
            Dim PassatoData As String = ""
            Dim PassatoCentroServizio As String = ""
            Dim UltimaCentroServizio As String = ""
            Dim Trovato As Boolean = False
            Dim UltimeQuoteO As Double = 0
            Dim UltimeQuoteP As Double = 0
            Dim UltimeQuoteC As Double = 0
            Dim UltimeQuoteR As Double = 0
            Dim UltimeQuoteJ As Double = 0
            Dim UltimaData As Date
            Dim Entrato As Boolean = False
            Dim CS As New Cls_CentroServizio

            Entrato = True
            Dim Reader As OleDbDataReader = cmd1.ExecuteReader()
            Do While Reader.Read
                CS.CENTROSERVIZIO = campodb(Reader.Item("CENTROSERVIZIO"))

                If Format(UltimaData, "yyyyMMdd") < Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataFine, "yyyyMMdd") And Entrato = False Then
                    Dim Appoggio As New QuoteOspitiPeriodo


                    If Anag.NonInUso = "S" Then
                        Appoggio.CF = Anag.NonInUso
                    Else
                        Appoggio.CF = Anag.CODICEFISCALE
                    End If

                    Appoggio.Nome = Anag.Nome
                    Appoggio.DataNascita = Anag.DataNascita

                    Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                    Appoggio.QuotaOspite = UltimeQuoteO
                    Appoggio.QuotaUsl = UltimeQuoteR

                    Dim UltStato As New Cls_StatoAuto

                    UltStato.CODICEOSPITE = Anag.CodiceOspite
                    UltStato.CENTROSERVIZIO = Centro1
                    UltStato.USL = ""
                    UltStato.TipoRetta = ""
                    UltStato.Data = UltimaData
                    UltStato.StatoPrimaData(Session("DC_OSPITE"), UltStato.CODICEOSPITE, UltStato.CENTROSERVIZIO)

                    Dim DecRegione As New ClsUSL

                    DecRegione.CodiceRegione = UltStato.USL
                    DecRegione.Nome = ""
                    DecRegione.Leggi(Session("DC_OSPITE"))

                    Appoggio.Usl = DecRegione.Nome

                    Dim DecTipoImporto As New Cls_TabellaTipoImportoRegione

                    DecTipoImporto.Codice = UltStato.TipoRetta
                    DecTipoImporto.Descrizione = ""
                    DecTipoImporto.Leggi(Session("DC_OSPITE"), DecTipoImporto.Codice)


                    Appoggio.TipoUSl = DecTipoImporto.Descrizione



                    Appoggio.QuotaComune = UltimeQuoteC


                    Dim ImportoComune As New Cls_ImportoComune

                    ImportoComune.CODICEOSPITE = Anag.CodiceOspite
                    ImportoComune.CENTROSERVIZIO = Centro1
                    ImportoComune.UltimaDataAData(Session("DC_OSPITE"), ImportoComune.CODICEOSPITE, ImportoComune.CENTROSERVIZIO, UltimaData)

                    Dim DecodificaComune As New ClsComune

                    DecodificaComune.Provincia = ImportoComune.PROV
                    DecodificaComune.Comune = ImportoComune.COMUNE
                    DecodificaComune.Leggi(Session("DC_OSPITE"))

                    Appoggio.Comune = DecodificaComune.Descrizione



                    Appoggio.QuotaAltriEnti = UltimeQuoteJ
                    Appoggio.QuotaParenti = UltimeQuoteP

                    Dim Lisitino As New Cls_Listino

                    Lisitino.CODICEOSPITE = Anag.CodiceOspite
                    Lisitino.CENTROSERVIZIO = Centro1
                    Lisitino.LeggiAData(Session("DC_OSPITE"), Appoggio.Data)

                    If Lisitino.CodiceListino <> "" Then
                        Dim DecListino As New Cls_Tabella_Listino

                        DecListino.Codice = Lisitino.CodiceListino
                        DecListino.LeggiCausale(Session("DC_OSPITE"))
                        Appoggio.Listino = DecListino.Descrizione
                    Else
                        Appoggio.Listino = ""
                    End If

                    XS.Singolo.Add(Appoggio)
                    i = i + 1
                    Entrato = True
                End If

                If Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") And Entrato = False Then
                    If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                    Else
                        Dim Appoggio As New QuoteOspitiPeriodo


                        If Anag.NonInUso = "S" Then
                            Appoggio.CF = Anag.NonInUso
                        Else
                            Appoggio.CF = Anag.CODICEFISCALE
                        End If
                        Appoggio.Nome = Anag.Nome
                        Appoggio.DataNascita = Anag.DataNascita
                        Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                        Appoggio.QuotaOspite = UltimeQuoteO
                        Appoggio.QuotaUsl = UltimeQuoteR

                        Dim UltStato As New Cls_StatoAuto

                        UltStato.CODICEOSPITE = Anag.CodiceOspite
                        UltStato.CENTROSERVIZIO = Centro1
                        UltStato.USL = ""
                        UltStato.TipoRetta = ""
                        UltStato.Data = UltimaData
                        UltStato.StatoPrimaData(Session("DC_OSPITE"), UltStato.CODICEOSPITE, UltStato.CENTROSERVIZIO)

                        Dim DecRegione As New ClsUSL

                        DecRegione.CodiceRegione = UltStato.USL
                        DecRegione.Nome = ""
                        DecRegione.Leggi(Session("DC_OSPITE"))

                        Appoggio.Usl = DecRegione.Nome

                        Dim DecTipoImporto As New Cls_TabellaTipoImportoRegione

                        DecTipoImporto.Codice = UltStato.TipoRetta
                        DecTipoImporto.Descrizione = ""
                        DecTipoImporto.Leggi(Session("DC_OSPITE"), DecTipoImporto.Codice)


                        Appoggio.TipoUSl = DecTipoImporto.Descrizione


                        Appoggio.QuotaComune = UltimeQuoteC


                        Dim ImportoComune As New Cls_ImportoComune

                        ImportoComune.CODICEOSPITE = Anag.CodiceOspite
                        ImportoComune.CENTROSERVIZIO = Centro1
                        ImportoComune.UltimaDataAData(Session("DC_OSPITE"), ImportoComune.CODICEOSPITE, ImportoComune.CENTROSERVIZIO, UltimaData)

                        Dim DecodificaComune As New ClsComune

                        DecodificaComune.Provincia = ImportoComune.PROV
                        DecodificaComune.Comune = ImportoComune.COMUNE
                        DecodificaComune.Leggi(Session("DC_OSPITE"))

                        Appoggio.Comune = DecodificaComune.Descrizione



                        Appoggio.QuotaAltriEnti = UltimeQuoteJ
                        Appoggio.QuotaParenti = UltimeQuoteP

                        Dim Lisitino As New Cls_Listino

                        Lisitino.CODICEOSPITE = Anag.CodiceOspite
                        Lisitino.CENTROSERVIZIO = Centro1
                        Lisitino.LeggiAData(Session("DC_OSPITE"), Appoggio.Data)

                        If Lisitino.CodiceListino <> "" Then
                            Dim DecListino As New Cls_Tabella_Listino

                            DecListino.Codice = Lisitino.CodiceListino
                            DecListino.LeggiCausale(Session("DC_OSPITE"))
                            Appoggio.Listino = DecListino.Descrizione
                        Else
                            Appoggio.Listino = ""
                        End If

                        XS.Singolo.Add(Appoggio)
                        i = i + 1
                        Entrato = True
                    End If
                End If

                Entrato = False
                UltimaData = campodb(Reader.Item("Data"))
                UltimaCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))


                UltimeQuoteO = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data"))

                If Anag.ImportoSconto > 0 Then
                    UltimeQuoteO = UltimeQuoteO - Modulo.MathRound(UltimeQuoteO * Anag.ImportoSconto / 100, 2)
                End If

                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")) > 0 Then
                    Dim ImportoMensile As Double = k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data"))

                    Dim Parente As New Cls_Parenti

                    Parente.CodiceOspite = Anag.CodiceOspite
                    Parente.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite, 1)

                    ImportoMensile = ImportoMensile - (ImportoMensile * Parente.ImportoSconto / 100)

                    UltimeQuoteO = Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                End If

                UltimeQuoteP = 0

                Dim TotaleMensile As Double = 0
                Dim Par As New Cls_Parenti

                Par.CodiceOspite = Anag.CodiceOspite
                Par.CodiceParente = 1
                Par.Leggi(Session("DC_OSPITE"), Par.CodiceOspite, Par.CodiceParente)

                If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                    UltimeQuoteP = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data"))
                    If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data")) > 0 Then
                        Dim ImportoMensile As Double = k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data"))

                        Dim Parente As New Cls_Parenti

                        Parente.CodiceOspite = Anag.CodiceOspite
                        Parente.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite, 1)

                        ImportoMensile = ImportoMensile - (ImportoMensile * Parente.ImportoSconto / 100)

                        TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                    End If

                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If

                Par.Nome = ""
                Par.CodiceOspite = Anag.CodiceOspite
                Par.CodiceParente = 2
                Par.Leggi(Session("DC_OSPITE"), Par.CodiceOspite, Par.CodiceParente)

                If Not IsNothing(Par.Nome) And Par.Nome <> "" Then

                    UltimeQuoteP = UltimeQuoteP + k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data"))

                    If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data")) > 0 Then
                        Dim ImportoMensile As Double = k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data"))

                        ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                        TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                    End If
                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If

                Par.Nome = ""
                Par.CodiceOspite = Anag.CodiceOspite
                Par.CodiceParente = 3
                Par.Leggi(Session("DC_OSPITE"), Par.CodiceOspite, Par.CodiceParente)

                If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                    UltimeQuoteP = UltimeQuoteP + k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data"))

                    If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data")) > 0 Then
                        Dim ImportoMensile As Double = k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data"))

                        ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                        TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                    End If
                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If
                If TotaleMensile > 0 Then
                    UltimeQuoteP = TotaleMensile
                End If

                UltimeQuoteC = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, Reader.Item("Data"))
                UltimeQuoteR = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, Reader.Item("Data"))
                If UltimeQuoteR = 0 Then
                    UltimeQuoteR = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DataInizio)
                End If
                UltimeQuoteJ = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, Reader.Item("Data"))


                If Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") Then
                    If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                    Else
                        Dim Appoggio As New QuoteOspitiPeriodo

                        If Anag.NonInUso = "S" Then
                            Appoggio.CF = Anag.NonInUso
                        Else
                            Appoggio.CF = Anag.CODICEFISCALE
                        End If
                        Appoggio.Nome = Anag.Nome
                        Appoggio.DataNascita = Anag.DataNascita
                        Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                        Appoggio.QuotaOspite = UltimeQuoteO
                        Appoggio.QuotaUsl = UltimeQuoteR

                        Dim UltStato As New Cls_StatoAuto

                        UltStato.CODICEOSPITE = Anag.CodiceOspite
                        UltStato.CENTROSERVIZIO = Centro1
                        UltStato.USL = ""
                        UltStato.TipoRetta = ""
                        UltStato.Data = UltimaData
                        UltStato.StatoPrimaData(Session("DC_OSPITE"), UltStato.CODICEOSPITE, UltStato.CENTROSERVIZIO)

                        Dim DecRegione As New ClsUSL

                        DecRegione.CodiceRegione = UltStato.USL
                        DecRegione.Nome = ""
                        DecRegione.Leggi(Session("DC_OSPITE"))

                        Appoggio.Usl = DecRegione.Nome

                        Dim DecTipoImporto As New Cls_TabellaTipoImportoRegione

                        DecTipoImporto.Codice = UltStato.TipoRetta
                        DecTipoImporto.Descrizione = ""
                        DecTipoImporto.Leggi(Session("DC_OSPITE"), DecTipoImporto.Codice)


                        Appoggio.TipoUSl = DecTipoImporto.Descrizione


                        Appoggio.QuotaComune = UltimeQuoteC


                        Dim ImportoComune As New Cls_ImportoComune

                        ImportoComune.CODICEOSPITE = Anag.CodiceOspite
                        ImportoComune.CENTROSERVIZIO = Centro1
                        ImportoComune.UltimaDataAData(Session("DC_OSPITE"), ImportoComune.CODICEOSPITE, ImportoComune.CENTROSERVIZIO, UltimaData)

                        Dim DecodificaComune As New ClsComune

                        DecodificaComune.Provincia = ImportoComune.PROV
                        DecodificaComune.Comune = ImportoComune.COMUNE
                        DecodificaComune.Leggi(Session("DC_OSPITE"))

                        Appoggio.Comune = DecodificaComune.Descrizione



                        Appoggio.QuotaAltriEnti = UltimeQuoteJ
                        Appoggio.QuotaParenti = UltimeQuoteP


                        Dim Lisitino As New Cls_Listino

                        Lisitino.CODICEOSPITE = Anag.CodiceOspite
                        Lisitino.CENTROSERVIZIO = Centro1
                        Lisitino.LeggiAData(Session("DC_OSPITE"), Appoggio.Data)

                        If Lisitino.CodiceListino <> "" Then
                            Dim DecListino As New Cls_Tabella_Listino

                            DecListino.Codice = Lisitino.CodiceListino
                            DecListino.LeggiCausale(Session("DC_OSPITE"))
                            Appoggio.Listino = DecListino.Descrizione
                        Else
                            Appoggio.Listino = ""
                        End If

                        XS.Singolo.Add(Appoggio)
                        i = i + 1

                        PassatoData = Format(Reader.Item("Data"), "yyyyMMdd")
                        PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))


                        Entrato = True
                    End If
                End If


                If Format(Reader.Item("Data"), "yyyyMMdd") = Format(DataInizio, "yyyyMMdd") Then
                    If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                    Else
                        If Format(UltimaData, "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") Then
                            Dim Appoggio As New QuoteOspitiPeriodo

                            If Anag.NonInUso = "S" Then
                                Appoggio.CF = Anag.NonInUso
                            Else
                                Appoggio.CF = Anag.CODICEFISCALE
                            End If
                            Appoggio.Nome = Anag.Nome
                            Appoggio.DataNascita = Anag.DataNascita
                            Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                            Appoggio.QuotaOspite = UltimeQuoteO
                            Appoggio.QuotaUsl = UltimeQuoteR

                            Dim UltStato As New Cls_StatoAuto

                            UltStato.CODICEOSPITE = Anag.CodiceOspite
                            UltStato.CENTROSERVIZIO = Centro1
                            UltStato.USL = ""
                            UltStato.TipoRetta = ""
                            UltStato.Data = UltimaData
                            UltStato.StatoPrimaData(Session("DC_OSPITE"), UltStato.CODICEOSPITE, UltStato.CENTROSERVIZIO)

                            Dim DecRegione As New ClsUSL

                            DecRegione.CodiceRegione = UltStato.USL
                            DecRegione.Nome = ""
                            DecRegione.Leggi(Session("DC_OSPITE"))

                            Appoggio.Usl = DecRegione.Nome

                            Dim DecTipoImporto As New Cls_TabellaTipoImportoRegione

                            DecTipoImporto.Codice = UltStato.TipoRetta
                            DecTipoImporto.Descrizione = ""
                            DecTipoImporto.Leggi(Session("DC_OSPITE"), DecTipoImporto.Codice)


                            Appoggio.TipoUSl = DecTipoImporto.Descrizione



                            Appoggio.QuotaComune = UltimeQuoteC


                            Dim ImportoComune As New Cls_ImportoComune

                            ImportoComune.CODICEOSPITE = Anag.CodiceOspite
                            ImportoComune.CENTROSERVIZIO = Centro1
                            ImportoComune.UltimaDataAData(Session("DC_OSPITE"), ImportoComune.CODICEOSPITE, ImportoComune.CENTROSERVIZIO, UltimaData)

                            Dim DecodificaComune As New ClsComune

                            DecodificaComune.Provincia = ImportoComune.PROV
                            DecodificaComune.Comune = ImportoComune.COMUNE
                            DecodificaComune.Leggi(Session("DC_OSPITE"))

                            Appoggio.Comune = DecodificaComune.Descrizione


                            Appoggio.QuotaAltriEnti = UltimeQuoteJ
                            Appoggio.QuotaParenti = UltimeQuoteP


                            Dim Lisitino As New Cls_Listino

                            Lisitino.CODICEOSPITE = Anag.CodiceOspite
                            Lisitino.CENTROSERVIZIO = Centro1
                            Lisitino.LeggiAData(Session("DC_OSPITE"), Appoggio.Data)

                            If Lisitino.CodiceListino <> "" Then
                                Dim DecListino As New Cls_Tabella_Listino

                                DecListino.Codice = Lisitino.CodiceListino
                                DecListino.LeggiCausale(Session("DC_OSPITE"))
                                Appoggio.Listino = DecListino.Descrizione
                            Else
                                Appoggio.Listino = ""
                            End If

                            XS.Singolo.Add(Appoggio)
                            i = i + 1

                            PassatoData = Format(Reader.Item("Data"), "yyyyMMdd")
                            PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))

                            Entrato = True
                        End If
                    End If
                End If

            Loop
            Reader.Close()

            For i = 0 To XS.Singolo.Count - 1
                If XS.Singolo(i).CF <> "S" Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = XS.Singolo(i).Nome
                    myriga(1) = XS.Singolo(i).DataNascita
                    myriga(2) = XS.Singolo(i).CF


                    myriga(3) = XS.Singolo(i).Data
                    If Chk_Regione.Checked = True Then
                        myriga(4) = XS.Singolo(i).Usl
                        myriga(5) = XS.Singolo(i).TipoUSl
                        myriga(6) = Math.Round(XS.Singolo(i).QuotaUsl, 2)
                    End If

                    If Chk_Ospite.Checked = True Then
                        myriga("Ospite Importo") = Math.Round(XS.Singolo(i).QuotaOspite, 2)
                    End If

                    If Chk_Parente.Checked = True Then
                        myriga("Parenti Importo") = Math.Round(XS.Singolo(i).QuotaParenti, 2)
                    End If
                    If Chk_Comune.Checked = True Then
                        myriga("Comune Importo") = Math.Round(XS.Singolo(i).QuotaComune, 2)
                        myriga("Comune Decodifica") = XS.Singolo(i).Comune
                    End If

                    myriga(MyTable.Columns.Count - 1) = XS.Singolo(i).Listino

                    MyTable.Rows.Add(myriga)
                End If
            Next

        Loop
        cn.Close()




        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" And DD_Struttura.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If

        If Cmb_CServ.SelectedValue <> "" Then
            Call ProspettoPresenze(True)
        Else
            Dim k As Integer

            For k = 0 To Cmb_CServ.Items.Count - 1
                Cmb_CServ.SelectedIndex = k
                If k = 0 Then
                    Call ProspettoPresenze(True)
                Else
                    Call ProspettoPresenze(False)
                End If
            Next
        End If
    End Sub

    Private Class QuoteOspitiPeriodoCFs
        Public Singolo As New List(Of QuoteOspitiPeriodo)
    End Class
    Private Class QuoteOspitiPeriodo
        Public Nome As String
        Public DataNascita As String
        Public CF As String
        Public Data As String
        Public Comune As String
        Public Usl As String
        Public TipoUSl As String
        Public AltroEnte As String
        Public QuotaOspite As Double
        Public QuotaParenti As Double
        Public QuotaComune As Double
        Public QuotaUsl As Double
        Public QuotaAltriEnti As Double
        Public Listino As String
    End Class

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" And DD_Struttura.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue <> "" Then
            Call ProspettoPresenze(True)
        Else
            Dim k As Integer

            For k = 0 To Cmb_CServ.Items.Count - 1
                Cmb_CServ.SelectedIndex = k
                If k = 0 Then
                    Call ProspettoPresenze(True)
                Else
                    Call ProspettoPresenze(False)
                End If
            Next
        End If

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CronologiaQuote.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" And DD_Struttura.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue <> "" Then
            Call ProspettoPresenze(True)
        Else
            Dim k As Integer

            For k = 0 To Cmb_CServ.Items.Count - 1
                Cmb_CServ.SelectedIndex = k
                If k = 0 Then
                    Call ProspettoPresenze(True)
                Else
                    Call ProspettoPresenze(False)
                End If
            Next
        End If


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
End Class
