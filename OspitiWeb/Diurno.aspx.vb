﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class Diurno
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Protected Sub Btn_CaricaMese_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CaricaMese.Click

        Call CaricaMese(Txt_Anno.Text, Dd_Mese.SelectedValue)

    End Sub

    Private Function GiornoDefault(ByVal Anno As Long, ByVal mese As Long, ByVal Giorni As Long) As String
        Dim CsD As New Cls_CentroServizio

        Dim mdata1 As Date = DateSerial(Anno, mese, Giorni)
        GiornoDefault = ""
        If Month(mdata1) <> mese Then
            GiornoDefault = ""
            Exit Function
        End If

        CsD.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Dim Settimana As String = CsD.SETTIMANA

        If mdata1.DayOfWeek = DayOfWeek.Monday And Mid(Settimana, 1, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Tuesday And Mid(Settimana, 2, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Wednesday And Mid(Settimana, 3, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Thursday And Mid(Settimana, 4, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Friday And Mid(Settimana, 5, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Saturday And Mid(Settimana, 6, 1) = "X" Then
            GiornoDefault = "C"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Sunday And Mid(Settimana, 7, 1) = "X" Then
            GiornoDefault = "C"
        End If
        

        Dim CsO As New ClsOspite

        CsO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

        Dim m As New Cls_DatiOspiteParenteCentroServizio

        m.CodiceOspite = CsO.CodiceOspite
        m.CentroServizio = Session("CODICESERVIZIO")
        m.CodiceParente = 0
        m.Leggi(Session("DC_OSPITE"))
        If Trim(m.Settimana) <> "" Then
            Settimana = m.Settimana
        Else
            Settimana = CsO.SETTIMANA
        End If


        If mdata1.DayOfWeek = DayOfWeek.Monday And Mid(Settimana, 1, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Tuesday And Mid(Settimana, 2, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Wednesday And Mid(Settimana, 3, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Thursday And Mid(Settimana, 4, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Friday And Mid(Settimana, 5, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Saturday And Mid(Settimana, 6, 1) = "1" Then
            GiornoDefault = "A"
        End If
        If mdata1.DayOfWeek = DayOfWeek.Sunday And Mid(Settimana, 7, 1) = "1" Then
            GiornoDefault = "A"
        End If
    End Function

    Private Sub CaricaMese(ByVal anno As Long, ByVal mese As Long)
        Dim I As Integer
        Dim mdata As Date = DateSerial(anno, mese, 1)
        Dim GiornoInizio As Long

        For I = 1 To 42
            ViewState("Giorni" & I) = ""
        Next


        For I = 1 To 42

            Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

            MyTxt.Enabled = True

        Next


        GiornoInizio = 1
        If mdata.DayOfWeek = DayOfWeek.Monday Then
            GiornoInizio = 1
        End If
        If mdata.DayOfWeek = DayOfWeek.Tuesday Then
            GiornoInizio = 2
        End If
        If mdata.DayOfWeek = DayOfWeek.Wednesday Then
            GiornoInizio = 3
        End If
        If mdata.DayOfWeek = DayOfWeek.Thursday Then
            GiornoInizio = 4
        End If
        If mdata.DayOfWeek = DayOfWeek.Friday Then
            GiornoInizio = 5
        End If
        If mdata.DayOfWeek = DayOfWeek.Saturday Then
            GiornoInizio = 6
        End If
        If mdata.DayOfWeek = DayOfWeek.Sunday Then
            GiornoInizio = 7
        End If



        For I = 1 To GiornoInizio - 1

            Try
                Dim MyLbl As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Giorno" & I), Label)

                If Not IsNothing(MyLbl) Then
                    MyLbl.Text = ""
                End If

                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                MyTxt.ImageUrl = "images/diurno_grigio.png"
                MyTxt.Enabled = False

            Catch ex As Exception

            End Try

        Next


        Dim SettimanaOspite As New ClsOspite

        SettimanaOspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

        Dim SettimanaCserv As New Cls_CentroServizio

        SettimanaCserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Dim m As New Cls_DatiOspiteParenteCentroServizio

        m.CodiceOspite = Session("CODICEOSPITE")
        m.CodiceParente = 0
        m.CentroServizio = Session("CODICESERVIZIO")
        m.Leggi(Session("DC_OSPITE"))
        If Trim(m.Settimana) <> "" Then
            SettimanaOspite.SETTIMANA = m.Settimana
        Else
            SettimanaOspite.SETTIMANA = SettimanaOspite.SETTIMANA
        End If


        Dim Giorno As Integer
        Giorno = 1
        For I = GiornoInizio To GiorniMese(mese, anno) + GiornoInizio - 1
            Dim MyLbl As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Giorno" & I), Label)


            If Not IsNothing(MyLbl) Then
                MyLbl.Text = Giorno
            End If

            Try
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                MyTxt.ImageUrl = "images/diurno_blanco.png"

                Dim Gg As Integer

                Gg = Weekday(DateSerial(anno, mese, Giorno), vbMonday)
                If Trim(Mid(SettimanaOspite.SETTIMANA, Gg, 1)) <> "" Then
                    Dim Causale As New Cls_CausaliEntrataUscita
                    Causale.Codice = "A"
                    Causale.LeggiCausale(Session("DC_OSPITE"))
                    MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                    MyTxt.ToolTip = Causale.Descrizione
                    ViewState("Giorni" & I) = "A"
                End If

                If Trim(Mid(SettimanaCserv.SETTIMANA, Gg, 1)) <> "" Then
                    Dim Causale As New Cls_CausaliEntrataUscita
                    Causale.Codice = "C"
                    Causale.LeggiCausale(Session("DC_OSPITE"))
                    MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                    MyTxt.ToolTip = Causale.Descrizione
                    ViewState("Giorni" & I) = "C"
                End If


                MyTxt.Visible = True

            Catch ex As Exception

            End Try

            Giorno = Giorno + 1
        Next

        Dim ClCserv As New Cls_CentroServizio

        ClCserv.CENTROSERVIZIO = Session("CODICESERVIZIO")
        ClCserv.Leggi(Session("DC_OSPITE"), ClCserv.CENTROSERVIZIO)
        If ClCserv.GIORNO0101 <> "S" Then
            If mese = 1 Then

                I = GiornoInizio
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If

        If ClCserv.GIORNO0601 <> "S" Then
            If mese = 1 Then

                I = GiornoInizio + 5
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If


        If ClCserv.GIORNO1903 <> "S" Then
            If mese = 3 Then

                I = GiornoInizio + 18
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO2504 <> "S" Then
            If mese = 4 Then

                I = GiornoInizio + 24
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO0105 <> "S" Then
            If mese = 5 Then

                I = GiornoInizio
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO0206 <> "S" Then
            If mese = 6 Then

                I = GiornoInizio + 1
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO2906 <> "S" Then
            If mese = 6 Then

                I = GiornoInizio + 28
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO0111 <> "S" Then
            If mese = 11 Then

                I = GiornoInizio
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If

        If ClCserv.GIORNO0411 <> "S" Then
            If mese = 11 Then

                I = GiornoInizio + 3
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If

        If ClCserv.GIORNO0812 <> "S" Then
            If mese = 12 Then

                I = GiornoInizio + 7
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO2512 <> "S" Then
            If mese = 12 Then

                I = GiornoInizio + 24
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If
        If ClCserv.GIORNO2612 <> "S" Then
            If mese = 12 Then

                I = GiornoInizio + 25
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If

        If ClCserv.GIORNO1ATTIVO <> "S" And ClCserv.GIORNO1 <> "" Then
            If mese = Val(Mid(ClCserv.GIORNO1, 4, 2)) Then

                I = GiornoInizio + Val(Mid(ClCserv.GIORNO1, 1, 2)) - 1
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If

        If ClCserv.GIORNO4ATTIVO <> "S" And ClCserv.GIORNO4 <> "" Then
            If mese = Val(Mid(ClCserv.GIORNO4, 4, 2)) Then

                I = GiornoInizio + Val(Mid(ClCserv.GIORNO4, 1, 2)) - 1
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                Dim Causale As New Cls_CausaliEntrataUscita
                Causale.Codice = "C"
                Causale.LeggiCausale(Session("DC_OSPITE"))
                MyTxt.ImageUrl = "images/diurno_" & Causale.Diurno & ".png"
                MyTxt.ToolTip = Causale.Descrizione
                ViewState("Giorni" & I) = "C"
            End If
        End If



        For I = GiorniMese(mese, anno) + GiornoInizio To 42
            Dim MyLbl As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Giorno" & I), Label)

            If Not IsNothing(MyLbl) Then
                MyLbl.Text = ""
            End If
            Try
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & I), ImageButton)

                MyTxt.ImageUrl = "images/diurno_grigio.png"
                MyTxt.ToolTip = ""
                MyTxt.Enabled = False

            Catch ex As Exception

            End Try
        Next

        Dim Z As Integer


        Dim MyDiurno As New Cls_Diurno




        Dim Appo As String

        Dim ConnectionString As String = Session("DC_OSPITE")


        Appo = MyDiurno.LeggiMese(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), anno, mese)
        Giorno = 1
        If Appo <> "" Then
            For Z = GiornoInizio To GiorniMese(mese, anno) + GiornoInizio
                Try
                    Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & Z), ImageButton)

                    Dim Codice As String
                    Dim VettoreCodici(100) As String


                    MyTxt.ImageUrl = "images/Diurno_Blanco.png"
                    MyTxt.ToolTip = ""


                    VettoreCodici = ViewState("VettoreCodice")

                    Codice = Trim(Mid(Appo, ((Giorno - 1) * 2) + 1, 2))
                    Dim x As Integer

                    If Codice.Trim <> "" Then
                        Dim Causal As New Cls_CausaliEntrataUscita

                        Causal.Codice = Codice.Trim
                        Causal.LeggiCausale(Session("DC_OSPITE"))


                        MyTxt.ImageUrl = "images/Diurno_" & Causal.Diurno & ".png"
                        MyTxt.ToolTip = Causal.Descrizione
                    End If
                    ViewState("Giorni" & Z) = Codice.Trim

                Catch ex As Exception

                End Try
                Giorno = Giorno + 1
            Next
        End If
        For Z = GiornoInizio To GiorniMese(mese, anno) + GiornoInizio
            Try
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & Z), ImageButton)


                Dim cn As OleDbConnection



                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cn.Open()

                Dim KCausale As New Cls_CausaliEntrataUscita

                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' And Data = ? And (Causale <> '' and not Causale  is null)")
                cmd.Parameters.AddWithValue("@Data", DateSerial(anno, mese, Z - GiornoInizio + 1))
                cmd.Connection = cn

                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                If myPOSTreader.Read Then
                    If MyTxt.ImageUrl = "images/Diurno_Blanco.png" Then
                        MyTxt.ImageUrl = "images/Diurno_Blanco.png"

                        MyTxt.ToolTip = campodb(myPOSTreader.Item("Descrizione"))
                        If campodb(myPOSTreader.Item("TipoMov")) = "05" Then
                            If MyTxt.ToolTip = "" Then
                                MyTxt.ToolTip = MyTxt.ToolTip & "Accoglimento"
                            End If
                        End If
                        If campodb(myPOSTreader.Item("TipoMov")) = "13" Then
                            If MyTxt.ToolTip = "" Then
                                MyTxt.ToolTip = MyTxt.ToolTip & "Uscita Definitiva"
                            End If
                        End If
                        Dim Codice As String
                        Codice = campodb(myPOSTreader.Item("Causale"))

                        If Codice.Trim <> "" Then
                            Dim Causal As New Cls_CausaliEntrataUscita

                            Causal.Codice = Codice.Trim
                            Causal.LeggiCausale(Session("DC_OSPITE"))


                            If Causal.Diurno > 0 Then
                                MyTxt.ImageUrl = "images/Diurno_" & Causal.Diurno & ".png"
                                MyTxt.ToolTip = Causal.Descrizione
                            End If
                        End If
                        ViewState("Giorni" & Z) = Codice.Trim
                    End If
                End If
                myPOSTreader.Close()
                cn.Close()
            Catch ex As Exception

            End Try
        Next

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

        If Val(Session("CODICEOSPITE")) = 0 Then
            Response.Redirect("RicercaAnagrafica.aspx")
            Exit Sub
        End If


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim KT As New Cls_Parametri

        KT.LeggiParametri(Session("DC_OSPITE"))


        Txt_Anno.Text = KT.AnnoFatturazione
        Dd_Mese.SelectedValue = KT.MeseFatturazione

        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim VettoreCodici(100) As String

        cmd.CommandText = ("select * from Causali where Diurno >= 1 ORDER BY Codice,Descrizione")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim i As Integer = 1

        For i = 1 To 42
            ViewState("Giorni" & i) = ""
        Next

        Dim x As Integer
        i = 1
        For x = 25 To 40
            Dim LblCau As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & x), Label)
            LblCau.Visible = False
            Dim ImCau As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Img_Causale" & x), ImageButton)
            ImCau.Visible = False
        Next

        x = 25
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If Val(myPOSTreader.Item("Diurno")) > 24 Then
                Dim LblCau As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & x), Label)

                LblCau.Text = myPOSTreader.Item("Descrizione")
                VettoreCodici(x) = myPOSTreader.Item("Codice")
                LblCau.Visible = True
                Dim ImCau As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Img_Causale" & x), ImageButton)
                ImCau.ImageUrl = "images\Diurno_" & Val(myPOSTreader.Item("Diurno")) & ".png"
                ImCau.Visible = True

                x = x + 1
            Else
                Dim LblCau As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & i), Label)

                LblCau.Text = myPOSTreader.Item("Descrizione")
                VettoreCodici(i) = myPOSTreader.Item("Codice")

                Dim ImCau As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Img_Causale" & i), ImageButton)
                ImCau.ImageUrl = "images\Diurno_" & Val(myPOSTreader.Item("Diurno")) & ".png"


                i = i + 1

            End If
            If i > 20 Then Exit Do
        Loop

        For x = i To 20
            Dim LblCau As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & x), Label)
            LblCau.Visible = False
            Dim ImCau As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Img_Causale" & x), ImageButton)
            ImCau.Visible = False
        Next

        ViewState("DESCRIZIONECLICK") = ""
        ViewState("VettoreCodice") = VettoreCodici
        cn.Close()

        If Val(Request.Item("Mese")) = 0 And Val(Request.Item("Anno")) = 0 Then
            Call CaricaMese(Txt_Anno.Text, Dd_Mese.SelectedValue)
        Else
            Call CaricaMese(Val(Request.Item("Anno")), Val(Request.Item("Mese")))
            Txt_Anno.Text = Val(Request.Item("Anno"))
            Dd_Mese.SelectedValue = Val(Request.Item("Mese"))

            Txt_Anno.Enabled = False
            Dd_Mese.Enabled = False
            Btn_CaricaMese.Enabled = False
        End If

        Call CalcoloTotale()
    End Sub

    

    Private Function Testo(ByVal Giorno As Long) As String
        Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & Giorno), ImageButton)
        Dim VettoreCodici(100) As String

        Testo = Replace(MyTxt.ImageUrl, "images/Diurno_", "")
        Testo = Replace(Testo, ".png", "")

        If Testo.ToUpper = "BLANCO" Then
            Testo = ""
        Else
            VettoreCodici = ViewState("VettoreCodice")
            Testo = VettoreCodici(Val(Testo))
        End If
        
        Testo = ViewState("Giorni" & Giorno)
    End Function

    Private Sub CalcoloTotale()

        Dim mdata As Date = DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1)

        Dim GiornoInizio As Long
        GiornoInizio = 1
        If mdata.DayOfWeek = DayOfWeek.Monday Then
            GiornoInizio = 1
        End If
        If mdata.DayOfWeek = DayOfWeek.Tuesday Then
            GiornoInizio = 2
        End If
        If mdata.DayOfWeek = DayOfWeek.Wednesday Then
            GiornoInizio = 3
        End If
        If mdata.DayOfWeek = DayOfWeek.Thursday Then
            GiornoInizio = 4
        End If
        If mdata.DayOfWeek = DayOfWeek.Friday Then
            GiornoInizio = 5
        End If
        If mdata.DayOfWeek = DayOfWeek.Saturday Then
            GiornoInizio = 6
        End If
        If mdata.DayOfWeek = DayOfWeek.Sunday Then
            GiornoInizio = 7
        End If

        Dim VettoreCodice(100) As String
        Dim VettoreString(100) As String
        Dim VettoreValore(100) As String
        Dim i As Integer
        Dim Cerca As Integer
        Dim Max As Integer = 0
        For i = 0 To 30
            Dim Trovato As Boolean = False
            For Cerca = 0 To Max
                If VettoreCodice(Cerca) = Testo(GiornoInizio + i) Then
                    VettoreValore(Cerca) = VettoreValore(Cerca) + 1
                    Trovato = True
                End If
            Next
            If Not Trovato Then
                VettoreCodice(Max) = Testo(GiornoInizio + i)
                Dim Causale As New Cls_CausaliEntrataUscita

                Causale.Codice = Testo(GiornoInizio + i)
                Causale.LeggiCausale(Session("DC_OSPITE"))

                VettoreString(Max) = Causale.Descrizione
                VettoreValore(Max) = 1
                Max = Max + 1
            End If
        Next

        lblTotale.Text = ""
        For i = 0 To Max
            If Trim(VettoreCodice(i)) = "" Then
                lblTotale.Text = lblTotale.Text & " Presenze " & VettoreValore(i) & "<br/>"
            Else
                lblTotale.Text = lblTotale.Text & VettoreCodice(i) & " " & VettoreString(i) & " " & VettoreValore(i) & "<br/>"
            End If
        Next

    End Sub

    Protected Sub Modifica()
        Dim ConnectionString As String = Session("DC_OSPITE")



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        If Request.Item("MESE") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, x.Nome, "I", "DIURNO", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")

            Dim KjSON As New Cls_Diurno

            KjSON.Anno = Txt_Anno.Text
            KjSON.Mese = Dd_Mese.SelectedValue

            KjSON.CodiceOspite = Session("CODICEOSPITE")
            KjSON.CENTROSERVIZIO = Session("CODICESERVIZIO")

            KjSON.Leggi(Session("DC_OSPITE"), KjSON.CodiceOspite, KjSON.CENTROSERVIZIO, KjSON.Anno, KjSON.Mese)
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(KjSON)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, x.Nome, "M", "DIURNO", AppoggioJS)

        End If


        Dim K As New Cls_Diurno

        K.Anno = Txt_Anno.Text
        K.Mese = Dd_Mese.SelectedValue

        K.CodiceOspite = Session("CODICEOSPITE")
        K.CENTROSERVIZIO = Session("CODICESERVIZIO")

        Dim mdata As Date = DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1)

        Dim GiornoInizio As Long
        GiornoInizio = 1
        If mdata.DayOfWeek = DayOfWeek.Monday Then
            GiornoInizio = 1
        End If
        If mdata.DayOfWeek = DayOfWeek.Tuesday Then
            GiornoInizio = 2
        End If
        If mdata.DayOfWeek = DayOfWeek.Wednesday Then
            GiornoInizio = 3
        End If
        If mdata.DayOfWeek = DayOfWeek.Thursday Then
            GiornoInizio = 4
        End If
        If mdata.DayOfWeek = DayOfWeek.Friday Then
            GiornoInizio = 5
        End If
        If mdata.DayOfWeek = DayOfWeek.Saturday Then
            GiornoInizio = 6
        End If
        If mdata.DayOfWeek = DayOfWeek.Sunday Then
            GiornoInizio = 7
        End If


        K.Giorno1 = Testo(GiornoInizio)
        K.Giorno2 = Testo(GiornoInizio + 1)
        K.Giorno3 = Testo(GiornoInizio + 2)
        K.Giorno4 = Testo(GiornoInizio + 3)
        K.Giorno5 = Testo(GiornoInizio + 4)
        K.Giorno6 = Testo(GiornoInizio + 5)
        K.Giorno7 = Testo(GiornoInizio + 6)
        K.Giorno8 = Testo(GiornoInizio + 7)
        K.Giorno9 = Testo(GiornoInizio + 8)
        K.Giorno10 = Testo(GiornoInizio + 9)
        K.Giorno11 = Testo(GiornoInizio + 10)
        K.Giorno12 = Testo(GiornoInizio + 11)
        K.Giorno13 = Testo(GiornoInizio + 12)
        K.Giorno14 = Testo(GiornoInizio + 13)
        K.Giorno15 = Testo(GiornoInizio + 14)
        K.Giorno16 = Testo(GiornoInizio + 15)
        K.Giorno17 = Testo(GiornoInizio + 16)
        K.Giorno18 = Testo(GiornoInizio + 17)
        K.Giorno19 = Testo(GiornoInizio + 18)
        K.Giorno20 = Testo(GiornoInizio + 19)
        K.Giorno21 = Testo(GiornoInizio + 20)
        K.Giorno22 = Testo(GiornoInizio + 21)
        K.Giorno23 = Testo(GiornoInizio + 22)
        K.Giorno24 = Testo(GiornoInizio + 23)
        K.Giorno25 = Testo(GiornoInizio + 24)
        K.Giorno26 = Testo(GiornoInizio + 25)
        K.Giorno27 = Testo(GiornoInizio + 26)
        K.Giorno28 = Testo(GiornoInizio + 27)
        K.Giorno29 = Testo(GiornoInizio + 28)
        K.Giorno30 = Testo(GiornoInizio + 29)
        K.Giorno31 = Testo(GiornoInizio + 30)

        K.AggiornaDB(ConnectionString)

        Dim Mese, Anno As Integer

        Dim KT As New Cls_Parametri

        KT.LeggiParametri(Session("DC_OSPITE"))


        Anno = KT.AnnoFatturazione
        Mese = KT.MeseFatturazione

        Call CaricaMese(Anno, Mese)

        


    End Sub

    

    Private Function VerificaGiorno(ByVal Giorno As String, ByVal Numero As Long) As Boolean

        If Giorno.Trim = "" Then
            VerificaGiorno = False
            Exit Function
        End If
        VerificaGiorno = False

        Dim k As New Cls_CausaliEntrataUscita

        k.Codice = Giorno
        k.LeggiCausale(Session("DC_OSPITE"))
        If k.Descrizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale giorno " & Numero & " formalmente errata');", True)
            VerificaGiorno = True
        End If
    End Function

    

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If


        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica anno');", True)
            Exit Sub
        End If

        Dim i As Long
        Dim Giorno As Long = 1
        For i = 1 To 42
            Try
                Dim MyTxt As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & i), ImageButton)

                If MyTxt.Enabled = True Then
                    
                    Giorno = Giorno + 1
                End If
            Catch ex As Exception

            End Try
        Next



        Dim UltUscit As New Cls_Movimenti

        UltUscit.CodiceOspite = Val(Session("CODICEOSPITE"))
        UltUscit.CENTROSERVIZIO = Session("CODICESERVIZIO")
        UltUscit.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))


        Dim UltAcco As New Cls_Movimenti

        UltAcco.CodiceOspite = Val(Session("CODICEOSPITE"))
        UltAcco.CENTROSERVIZIO = Session("CODICESERVIZIO")
        UltAcco.UltimaDataAccoglimento(Session("DC_OSPITE"))

        If Format(UltAcco.Data, "yyyyMMdd") < Format(UltUscit.Data, "yyyyMMdd") Then
            If Year(UltUscit.Data) < Val(Txt_Anno.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite uscito non posso proseguire');", True)
                Exit Sub
            End If
            If Year(UltUscit.Data) = Val(Txt_Anno.Text) And Month(UltUscit.Data) < Dd_Mese.SelectedValue Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite uscito non posso proseguire');", True)
                Exit Sub
            End If
            If Year(UltUscit.Data) = Val(Txt_Anno.Text) And Month(UltUscit.Data) = Dd_Mese.SelectedValue Then
                Dim mdata As Date = DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1)

                Dim GiornoInizio As Long
                GiornoInizio = 1
                If mdata.DayOfWeek = DayOfWeek.Monday Then
                    GiornoInizio = 1
                End If
                If mdata.DayOfWeek = DayOfWeek.Tuesday Then
                    GiornoInizio = 2
                End If
                If mdata.DayOfWeek = DayOfWeek.Wednesday Then
                    GiornoInizio = 3
                End If
                If mdata.DayOfWeek = DayOfWeek.Thursday Then
                    GiornoInizio = 4
                End If
                If mdata.DayOfWeek = DayOfWeek.Friday Then
                    GiornoInizio = 5
                End If
                If mdata.DayOfWeek = DayOfWeek.Saturday Then
                    GiornoInizio = 6
                End If
                If mdata.DayOfWeek = DayOfWeek.Sunday Then
                    GiornoInizio = 7
                End If

                For i = 1 To 31
                    If Testo(i) <> "" Then
                        If i - GiornoInizio + 1 > Day(UltUscit.Data) Then
                            'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite uscito non posso proseguire');", True)
                            'Exit Sub
                        End If
                    End If
                Next
                'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ospite uscito non posso proseguire');", True)
            End If
        End If


        Call Modifica()

        Call PaginaPrecedente()
    End Sub

    Private Sub ClickCausale(ByVal Numero As Integer)
        Dim X As Integer
        For X = 1 To 20
            Dim LblCau1 As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & X), Label)
            LblCau1.BackColor = Drawing.Color.White
        Next
        For X = 25 To 40
            Dim LblCau1 As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & X), Label)
            LblCau1.BackColor = Drawing.Color.White
        Next
        Dim LblCauP As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale99"), Label)
        LblCauP.BackColor = Drawing.Color.White

        Dim LblCau As Label = DirectCast(Tab_Anagrafica.FindControl("Lbl_Causale" & Numero), Label)
        LblCau.BackColor = Drawing.Color.Yellow



        If Numero <> 99 Then
            Dim Causale As New Cls_CausaliEntrataUscita
            Dim VettoreCodici(200) As String
            VettoreCodici = ViewState("VettoreCodice")


            Causale.Codice = VettoreCodici(Numero)
            Causale.LeggiCausale(Session("DC_OSPITE"))

            ViewState("COLORCLICK") = Causale.Diurno
            ViewState("DESCRIZIONECLICK") = Causale.Descrizione
            ViewState("CAUSALECLICK") = VettoreCodici(Numero)
        Else
            ViewState("COLORCLICK") = "99"
            ViewState("CAUSALECLICK") = ""
            ViewState("DESCRIZIONECLICK") = ""
        End If


        'Dim ImCau As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Img_Causale" & Numero), ImageButton)
        'ImCau.Visible = False
    End Sub

    Protected Sub Img_Causale1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale1.Click
        ClickCausale(1)
    End Sub
    Protected Sub Img_Causale2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale2.Click
        ClickCausale(2)
    End Sub
    Protected Sub Img_Causale3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale3.Click
        ClickCausale(3)
    End Sub
    Protected Sub Img_Causale4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale4.Click
        ClickCausale(4)
    End Sub
    Protected Sub Img_Causale5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale5.Click
        ClickCausale(5)
    End Sub
    Protected Sub Img_Causale6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale6.Click
        ClickCausale(6)
    End Sub
    Protected Sub Img_Causale7_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale7.Click
        ClickCausale(7)
    End Sub
    Protected Sub Img_Causale8_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale8.Click
        ClickCausale(8)
    End Sub
    Protected Sub Img_Causale9_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale9.Click
        ClickCausale(9)
    End Sub
    Protected Sub Img_Causale10_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale10.Click
        ClickCausale(10)
    End Sub
    Protected Sub Img_Causale11_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale11.Click
        ClickCausale(11)
    End Sub
    Protected Sub Img_Causale12_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale12.Click
        ClickCausale(12)
    End Sub
    Protected Sub Img_Causale13_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale13.Click
        ClickCausale(13)
    End Sub
    Protected Sub Img_Causale14_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale14.Click
        ClickCausale(14)
    End Sub
    Protected Sub Img_Causale15_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale15.Click
        ClickCausale(15)
    End Sub
    Protected Sub Img_Causale16_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale16.Click
        ClickCausale(16)
    End Sub




    Private Sub ClickGiorno(ByVal Numero As Integer)

        If Val(ViewState("COLORCLICK")) = 0 Then
            Exit Sub
        End If

        Dim ImDia As ImageButton = DirectCast(Tab_Anagrafica.FindControl("Lnk_Giorno" & Numero), ImageButton)

        If Not IsNothing(ImDia) Then            
            If ViewState("COLORCLICK") = 99 Then
                ImDia.ImageUrl = "images/Diurno_blanco.png"
                ImDia.ToolTip = ""
                ViewState("Giorni" & Numero) = ""
            Else
                ImDia.ImageUrl = "images/Diurno_" & ViewState("COLORCLICK") & ".png"
                ImDia.ToolTip = ViewState("DESCRIZIONECLICK")
                ViewState("Giorni" & Numero) = ViewState("CAUSALECLICK")
            End If
        End If

        Call CalcoloTotale()
    End Sub

    Protected Sub Lnk_Giorno1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno1.Click
        ClickGiorno(1)
    End Sub
    Protected Sub Lnk_Giorno2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno2.Click
        ClickGiorno(2)
    End Sub
    Protected Sub Lnk_Giorno3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno3.Click
        ClickGiorno(3)
    End Sub
    Protected Sub Lnk_Giorno4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno4.Click
        ClickGiorno(4)
    End Sub
    Protected Sub Lnk_Giorno5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno5.Click
        ClickGiorno(5)
    End Sub
    Protected Sub Lnk_Giorno6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno6.Click
        ClickGiorno(6)
    End Sub
    Protected Sub Lnk_Giorno7_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno7.Click
        ClickGiorno(7)
    End Sub
    Protected Sub Lnk_Giorno8_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno8.Click
        ClickGiorno(8)
    End Sub
    Protected Sub Lnk_Giorno9_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno9.Click
        ClickGiorno(9)
    End Sub
    Protected Sub Lnk_Giorno10_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno10.Click
        ClickGiorno(10)
    End Sub
    Protected Sub Lnk_Giorno11_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno11.Click
        ClickGiorno(11)
    End Sub
    Protected Sub Lnk_Giorno12_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno12.Click
        ClickGiorno(12)
    End Sub
    Protected Sub Lnk_Giorno13_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno13.Click
        ClickGiorno(13)
    End Sub
    Protected Sub Lnk_Giorno14_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno14.Click
        ClickGiorno(14)
    End Sub
    Protected Sub Lnk_Giorno15_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno15.Click
        ClickGiorno(15)
    End Sub
    Protected Sub Lnk_Giorno16_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno16.Click
        ClickGiorno(16)
    End Sub
    Protected Sub Lnk_Giorno17_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno17.Click
        ClickGiorno(17)
    End Sub
    Protected Sub Lnk_Giorno18_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno18.Click
        ClickGiorno(18)
    End Sub
    Protected Sub Lnk_Giorno19_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno19.Click
        ClickGiorno(19)
    End Sub
    Protected Sub Lnk_Giorno20_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno20.Click
        ClickGiorno(20)
    End Sub
    Protected Sub Lnk_Giorno21_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno21.Click
        ClickGiorno(21)
    End Sub
    Protected Sub Lnk_Giorno22_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno22.Click
        ClickGiorno(22)
    End Sub
    Protected Sub Lnk_Giorno23_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno23.Click
        ClickGiorno(23)
    End Sub
    Protected Sub Lnk_Giorno24_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno24.Click
        ClickGiorno(24)
    End Sub
    Protected Sub Lnk_Giorno25_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno25.Click
        ClickGiorno(25)
    End Sub
    Protected Sub Lnk_Giorno26_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno26.Click
        ClickGiorno(26)
    End Sub
    Protected Sub Lnk_Giorno27_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno27.Click
        ClickGiorno(27)
    End Sub
    Protected Sub Lnk_Giorno28_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno28.Click
        ClickGiorno(28)
    End Sub
    Protected Sub Lnk_Giorno29_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno29.Click
        ClickGiorno(29)
    End Sub
    Protected Sub Lnk_Giorno30_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno30.Click
        ClickGiorno(30)
    End Sub
    Protected Sub Lnk_Giorno31_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno31.Click
        ClickGiorno(31)
    End Sub
    Protected Sub Lnk_Giorno32_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno32.Click
        ClickGiorno(32)
    End Sub
    Protected Sub Lnk_Giorno33_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno33.Click
        ClickGiorno(33)
    End Sub
    Protected Sub Lnk_Giorno34_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno34.Click
        ClickGiorno(34)
    End Sub
    Protected Sub Lnk_Giorno35_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno35.Click
        ClickGiorno(35)
    End Sub
    Protected Sub Lnk_Giorno36_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno36.Click
        ClickGiorno(36)
    End Sub
    Protected Sub Lnk_Giorno37_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno37.Click
        ClickGiorno(37)
    End Sub
    Protected Sub Lnk_Giorno38_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno38.Click
        ClickGiorno(38)
    End Sub
    Protected Sub Lnk_Giorno39_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno39.Click
        ClickGiorno(39)
    End Sub
    Protected Sub Lnk_Giorno40_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno40.Click
        ClickGiorno(40)
    End Sub
    Protected Sub Lnk_Giorno41_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno41.Click
        ClickGiorno(41)
    End Sub
    Protected Sub Lnk_Giorno42_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_Giorno42.Click
        ClickGiorno(42)
    End Sub






    Protected Sub Img_Causale99_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale99.Click
        ClickCausale(99)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("MesiDiurno.aspx")
    End Sub

    Protected Sub Img_Causale17_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale17.Click
        ClickCausale(17)
    End Sub

    Protected Sub Img_Causale18_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale18.Click
        ClickCausale(18)
    End Sub

    Protected Sub Img_Causale19_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale19.Click
        ClickCausale(19)
    End Sub

    Protected Sub Img_Causale20_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale20.Click
        ClickCausale(20)
    End Sub



    Protected Sub Img_Causale25_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale25.Click
        ClickCausale(25)
    End Sub

    Protected Sub Img_Causale26_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale26.Click
        ClickCausale(26)
    End Sub

    Protected Sub Img_Causale27_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale27.Click
        ClickCausale(27)
    End Sub

    Protected Sub Img_Causale28_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale28.Click
        ClickCausale(28)
    End Sub

    Protected Sub Img_Causale29_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale29.Click
        ClickCausale(29)
    End Sub

    Protected Sub Img_Causale30_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale30.Click
        ClickCausale(30)
    End Sub

    Protected Sub Img_Causale31_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale31.Click
        ClickCausale(31)
    End Sub

    Protected Sub Img_Causale32_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale32.Click
        ClickCausale(32)
    End Sub

    Protected Sub Img_Causale33_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale33.Click
        ClickCausale(33)
    End Sub

    Protected Sub Img_Causale34_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale34.Click
        ClickCausale(34)
    End Sub

    Protected Sub Img_Causale35_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale35.Click
        ClickCausale(35)
    End Sub

    Protected Sub Img_Causale36_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale36.Click
        ClickCausale(36)
    End Sub

    Protected Sub Img_Causale37_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale37.Click
        ClickCausale(37)
    End Sub

    Protected Sub Img_Causale38_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale38.Click
        ClickCausale(38)
    End Sub

    Protected Sub Img_Causale39_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale39.Click
        ClickCausale(39)
    End Sub
End Class


