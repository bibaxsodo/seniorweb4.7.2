﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Elenco_MovimentiDomiciliare" CodeFile="Elenco_MovimentiDomiciliare.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Movimenti Domiciliari</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">                 
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;
        }
        function DialogBoxx(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;
        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }
        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');

            $("#pippo").css('visibility', 'visible');
            $("#Txt_DataAcco").mask("99/99/9999");
        }

        function ChiudiTipologia() {
            $("#ModificaTipologia").css('visibility', 'hidden');
        }
        function VisualizzaDivTipologia() {
            $("#blur").css('visibility', 'visible');

            $("#ModificaTipologia").css('visibility', 'visible');
        }
        function ChiudiOperatore() {
            $("#ModificaOperatore").css('visibility', 'hidden');
        }
        function VisualizzaDivOperatore() {
            $("#blur").css('visibility', 'visible');

            $("#ModificaOperatore").css('visibility', 'visible');
        }
    </script>

    <style>
        .modifica {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 400px;
            height: 200px;
            position: absolute;
            top: 290px;
            left: 40%;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 200px;
            height: 260px;
            position: absolute;
            top: 290px;
            right: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Elenco Domiciliari</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr style="height: 32px;">
                                <td align="left" style="width: 100px;">Data Dal :
                                </td>
                                <td align="left" style="width: 200px;">
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px" Text=""></asp:TextBox>
                                </td>
                                <td align="left" style="width: 100px;">Data Al :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px" Text=""></asp:TextBox>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="Imb_ImportEPersonam" runat="server" BackColor="Transparent" ImageUrl="~/images/EPersonam.png" class="EffettoBottoniTondi" />
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <div id="MENUDIV"></div>
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                                <a href="#" onclick="VisualizzaDivRegistrazione();">
                                    <img src="../images/nuovo.png" title="Aggiungi Movimento Domiciliare" class="EffettoBottoniTondi" /></a><br />
                            </div>
                        </div>
                        <asp:GridView ID="Grd_AddebitiAccrediti" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                            PageSize="50">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%# Container.DataItemIndex  %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="Chk_Seleziona" runat="server" Text="" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                        </asp:GridView>


                    </td>
                </tr>
            </table>
        </div>
        <div id="blur" style="visibility: hidden;">&nbsp;</div>
        <div id="pippo" style="visibility: hidden;" class="wait">
            <div style="text-align: left; display: inline-table;">
                <a href="#" onclick="Chiudi();">
                    <img src="../images/annulla.png" title="Chiudi" /></a>
            </div>
            <br />
            <br />
            <table>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_Nuovo" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi Movimento Domiciliare" />
                    </td>
                    <td>Nuovo Movimento</td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="Btn_MultiMovimenti" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi Piu Movimenti Per lo stesso giorno" />
                    </td>
                    <td>Movimenti Mult.</td>
                </tr>
                <tr>
                    <td>
                        <a href="#" onclick="VisualizzaDivOperatore();">
                            <img src="../images/edit.png" title="Modifica Operatore" class="EffettoBottoniTondi" /></a>
                    </td>
                    <td>Modifica Operatore
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#" onclick="VisualizzaDivTipologia();">
                            <img src="../images/edit.png" title="Modifica Tipologia" class="EffettoBottoniTondi" /></a>
                    </td>
                    <td>Modifica Tipologia
                    </td>
                </tr>
            </table>
        </div>
        <div id="ModificaOperatore" style="visibility: hidden;" class="modifica">
            <br />
            <br />
            <label style="display: block; float: left; width: 160px; color: Gray;">N.Operatori :</label>
            <asp:TextBox ID="Txt_Operatori" runat="server" Text="0"></asp:TextBox>
            <br />
            <br />
            <br />
            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_ModificaOperatore"></asp:ImageButton>
            <a href="#" onclick="ChiudiOperatore();">
                <img src="../images/annulla.png" title="Chiudi" /></a>
        </div>
        <div id="ModificaTipologia" style="visibility: hidden;" class="modifica">
            <br />
            <br />
            <label style="display: block; float: left; width: 160px; color: Gray;">Operatore :</label>
            <asp:DropDownList ID="DD_Tipologia" runat="server">
            </asp:DropDownList>
            <br />
            <br />
            <br />
            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_ModificaTiplogia"></asp:ImageButton>
            <a href="#" onclick="ChiudiTipologia();">
                <img src="../images/annulla.png" title="Chiudi" /></a>
        </div>
    </form>
</body>
</html>
