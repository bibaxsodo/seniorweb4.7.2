﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq



Partial Class OspitiWeb_CaricaOspitiDaTabella
    Inherits System.Web.UI.Page

    Protected Sub OspitiWeb_CaricaOspitiDaTabella_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Dim Token As String
        Token = LoginPersonam(Context)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))
        If Param.MeseFatturazione = 1 Then
            Param.MeseFatturazione = 12
            Param.AnnoFatturazione = Param.AnnoFatturazione - 1
        Else
            Param.MeseFatturazione = Param.MeseFatturazione - 1
        End If

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        Dim Indice As Integer = 0
        Dim NumeroRighe As Integer = 0


        Dim UrlConnessione As String = ""



        Session("APPOGGIO") = ""
        Session("APPOGGIOPARENTE") = ""

        Dim CmdListaOspiti As New OleDbCommand
        CmdListaOspiti.CommandText = "Select * From Lista_Ospiti where (Select COUNT(*) From AnagraficaComune Where CODICEFISCALE  = Cf) = 0 Order by CF"
        CmdListaOspiti.Connection = cn

        Dim RdListaOspiti As OleDbDataReader = CmdListaOspiti.ExecuteReader()

        Do While RdListaOspiti.Read
            
            UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/" & RdListaOspiti.Item("CF")

            Indice = Indice + 1


            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            Dim Anag As New ClsOspite

            Dim jTok As JObject = JObject.Parse(rawresp)


            Anag.CognomeOspite = jTok.Item("surname").ToString()
            Anag.NomeOspite = jTok.Item("firstname").ToString()
            If Not IsNothing(jTok.Item("birthdate")) Then
                Anag.DataNascita = campodb(jTok.Item("birthdate").ToString())
            End If


            Anag.Nome = Anag.CognomeOspite & " " & Anag.NomeOspite

            Anag.UTENTE = Session("UTENTE")
            Anag.InserisciOspite(Session("DC_OSPITE"), Anag.CognomeOspite, Anag.NomeOspite, Anag.DataNascita)
            Anag.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite)


            If Not IsNothing(jTok.Item("cf")) Then
                Anag.CODICEFISCALE = jTok.Item("cf").ToString()
            End If

            Dim IDEpersonam As String

            Dim DataAcco As String = Nothing
            Dim Cserv As String = ""
            Dim ward_id As Integer = campodbN(RdListaOspiti.Item("Nucleo"))

            'Cittadella
            If ward_id = 12 Or ward_id = 13 Or ward_id = 11 Or ward_id = 10 Or ward_id = 9 Or ward_id = 8 Then
                ward_id = 8
            End If

            If ward_id = 23 Or ward_id = 24 Then
                ward_id = 23
            End If


            If ward_id = 17 Or ward_id = 19 Or ward_id = 20 Or ward_id = 985 Or ward_id = 987 Or ward_id = 988 Or ward_id = 989 Then
                ward_id = 989
            End If

            If ward_id = 999 Or ward_id = 21 Or ward_id = 22 Or ward_id = 998 Then
                ward_id = 999
            End If

            If ward_id = 14 Or ward_id = 984 Then
                ward_id = 14
            End If


            Dim Progressivo As Long
            Dim MySql As String
            Dim CS As New Cls_CentroServizio
            CS.DESCRIZIONE = ""

            Dim Accred As Boolean

            If campodb(RdListaOspiti.Item("Accred")) = "Si" Then
                Accred = True
            Else
                Accred = False
            End If


            Cs.EPersonam = ward_id
            Cs.LeggiEpersonam(Context.Session("DC_OSPITE"), Cs.EPersonam, Accred)
            DataAcco = campodbd(RdListaOspiti.Item("DataIngresso"))
            Cserv = Cs.CENTROSERVIZIO


            Try
                If Val(jTok.Item("gender").ToString()) = 1 Then
                    Anag.Sesso = "M"
                Else
                    Anag.Sesso = "F"
                End If
            Catch ex As Exception

            End Try

            Dim appoggio As String
            Try
                appoggio = jTok.Item("birthcity").ToString()

                Anag.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
                Anag.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
            Catch ex As Exception

            End Try

            Try
                Anag.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
            Catch ex As Exception

            End Try

            Try
                Anag.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
            Catch ex As Exception

            End Try

            Try
                Anag.RESIDENZACAP1 = jTok.Item("rescap").ToString()
            Catch ex As Exception

            End Try

            Try
                IDEpersonam = jTok.Item("id").ToString()
            Catch ex As Exception

            End Try


            Try
                appoggio = jTok.Item("rescity").ToString()

                Anag.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
                Anag.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

            Catch ex As Exception

            End Try




            Try
                Anag.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
            Catch ex As Exception

            End Try

            Try
                Anag.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
            Catch ex As Exception

            End Try

            Try
                Anag.TELEFONO1 = jTok.Item("fax").ToString()
            Catch ex As Exception

            End Try


            Anag.Nome = Anag.CognomeOspite & " " & Anag.NomeOspite

            Anag.UTENTE = Session("UTENTE")

            Anag.ScriviOspite(Session("DC_OSPITE"))


            Dim client1 As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & Anag.CODICEFISCALE & "/relatives")

            client1.Method = "GET"
            client1.Headers.Add("Authorization", "Bearer " & Token)
            client1.ContentType = "Content-Type: application/json"

            Dim reader1 As StreamReader
            Dim response1 As HttpWebResponse = Nothing

            response1 = DirectCast(client1.GetResponse(), HttpWebResponse)

            reader1 = New StreamReader(response1.GetResponseStream())


            rawresp = reader1.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)


            For Each jTok2 As JToken In jResults
                Dim VCODICEFISCALE As String = ""

                Try
                    VCODICEFISCALE = jTok2.Item("cf").ToString()
                Catch ex As Exception

                End Try

                If Trim(VCODICEFISCALE) = "" Then


                    'If jTok2.Item("signed_to_pay") = "true" Then
                    Dim Prova As New Cls_Parenti

                    Prova.CodiceOspite = Anag.CodiceOspite
                    Prova.CodiceParente = 0
                    Prova.Nome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString
                    Try
                        Prova.Telefono1 = jTok2.Item("phone").ToString
                    Catch ex As Exception

                    End Try


                    Try
                        Prova.Telefono3 = jTok2.Item("email").ToString
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZACAP1 = jTok2.Item("rescap").ToString()

                    Catch ex As Exception

                    End Try

                    Try
                        appoggio = jTok2.Item("rescity").ToString()

                        Prova.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
                        Prova.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZAINDIRIZZO1 = jTok2.Item("resaddress").ToString()
                    Catch ex As Exception

                    End Try

                    Prova.IdEpersonam = Val(jTok2.Item("id").ToString())


                    'End If

                    Dim MyBill As String = ""


                    Try
                        MyBill = jTok2.Item("signed_to_receive_bill")
                    Catch ex As Exception

                    End Try
                    If MyBill = "true" Then
                        Prova.ParenteIndirizzo = 1
                    End If


                    Prova.ScriviParente(Session("DC_OSPITE"))

                    If MyBill = "true" Then
                        Anag.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite)

                        Anag.RecapitoNome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString

                        Try
                            appoggio = jTok2.Item("rescity").ToString()

                            Anag.RecapitoProvincia = Mid(appoggio & Space(6), 1, 3)
                            Anag.RecapitoComune = Mid(appoggio & Space(6), 4, 3)
                        Catch ex As Exception

                        End Try

                        Try
                            Anag.RecapitoIndirizzo = jTok2.Item("resaddress").ToString()
                        Catch ex As Exception

                        End Try

                        Anag.ScriviOspite(Session("DC_OSPITE"))
                    End If
                End If
            Next


            Cs.CENTROSERVIZIO = Cserv
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


            Dim Pc As New Cls_Pianodeiconti
            Pc.Mastro = Cs.MASTRO
            Pc.Conto = Cs.CONTO
            Pc.Sottoconto = Anag.CodiceOspite * 100
            Pc.Decodfica(Session("DC_GENERALE"))
            Pc.Mastro = Cs.MASTRO
            Pc.Conto = Cs.CONTO
            Pc.Sottoconto = Anag.CodiceOspite * 100
            Pc.Descrizione = Anag.Nome
            Pc.Tipo = "A"
            Pc.TipoAnagrafica = "O"
            Pc.Scrivi(Session("DC_GENERALE"))


            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
            cmdIns.Connection = cn
            cmdIns.Parameters.AddWithValue("@IdEpersonam", Val(IDEpersonam))
            cmdIns.Parameters.AddWithValue("@CodiceFiscale", Anag.CODICEFISCALE)
            cmdIns.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
            cmdIns.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
            cmdIns.Parameters.AddWithValue("@Data", campodbd(RdListaOspiti.Item("DataIngresso")))
            cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
            cmdIns.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
            cmdIns.Parameters.AddWithValue("@DataModifica", Now)
            cmdIns.ExecuteNonQuery()

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & Cs.CENTROSERVIZIO & "' And  CodiceOspite = " & Anag.CodiceOspite)
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                If Not IsDBNull(myPOSTreader1.Item(0)) Then
                    Progressivo = myPOSTreader1.Item(0) + 1
                Else
                    Progressivo = 1
                End If
            Else
                Progressivo = 1
            End If

            Dim MyData As Date = campodbd(RdListaOspiti.Item("DataIngresso"))

            MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Utente", Session("UTENTE"))
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
            cmdw.Parameters.AddWithValue("@Data", MyData)
            cmdw.Parameters.AddWithValue("@TIPOMOV", "05")
            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
            cmdw.Parameters.AddWithValue("@CAUSALE", "")
            cmdw.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
            cmdw.Parameters.AddWithValue("@EPersonam", 1)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()

        Loop
        cn.Close()

    End Sub

    Private Function LoginPersonam(ByVal context As HttpContext) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")

        'request.Add("password", "advenias2014")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String

        If Trim(Session("EPersonamPSWCRYPT")) = "" Then

            Dim AppoggioUtente As String = Session("UTENTE")

            For i = 1 To 20
                AppoggioUtente = AppoggioUtente.Replace("<" & i & ">", "")
            Next i


            request.Add("username", AppoggioUtente)

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function


    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Sub VisuallizzaAccoglimentoOspite(ByVal Token As String, ByVal codicefiscale As String, ByRef CServ As String, ByRef DataAccoglimento As String)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))
        If Param.MeseFatturazione = 1 Then
            Param.MeseFatturazione = 12
            Param.AnnoFatturazione = Param.AnnoFatturazione - 1
        Else
            Param.MeseFatturazione = Param.MeseFatturazione - 1
        End If

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        Dim rawresp As String = ""

        If Session("LETTOMOVIMENTI") = "" Then
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/movements/from/" & DataFatt)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())



            rawresp = reader.ReadToEnd()

            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = 0 And Periodo = ? And Tipo = 'M'"
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            SalvaDb.Connection = cn

            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'M'"
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            Else
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo) values (?,?,?,?,?,'M') "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            End If
            VerificaDB.Close()

            Session("LETTOMOVIMENTI") = rawresp
        Else
            rawresp = Session("LETTOMOVIMENTI")
        End If


        Dim jResults As JArray = JArray.Parse(rawresp)
        Dim LastMov As String

        Dim Ospite As New ClsOspite

        Ospite.CODICEFISCALE = codicefiscale
        Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospite.CODICEFISCALE)



        For Each jTok As JToken In jResults
            rawresp = rawresp & jTok.Item("guests_movements").ToString()
            For Each jTok1 As JToken In jTok.Item("guests_movements").Children

                Dim ward_id As Integer


                ward_id = jTok1.Item("ward_id").ToString()

                If codicefiscale = jTok1.Item("cf").ToString() Then
                    LastMov = ""
                    For Each jTok2 As JToken In jTok1.Item("movements").Children
                        Dim IdEpersonam As String

                        IdEpersonam = jTok2.Item("id").ToString


                        If LastMov = "11" And jTok2.Item("type").ToString = "8" Then

                        Else

                            LastMov = jTok2.Item("type").ToString

                            Dim cmd As New OleDbCommand()

                            If jTok2.Item("type").ToString = "11" Then
                                cmd.CommandText = ("select * from Movimenti_EPersonam Where  CodiceOspite = " & Ospite.CodiceOspite & " And Data = ?")
                                cmd.Parameters.AddWithValue("@Data", Mid(jTok2.Item("date").ToString, 1, 10))

                            Else
                                cmd.CommandText = ("select * from Movimenti_EPersonam Where  IdEpersonam = " & IdEpersonam)
                            End If

                            cmd.Connection = cn

                            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                            If Not myPOSTreader.Read Then
                                Dim Tipo As String = ""
                                Dim DataMov As String = ""

                                If jTok2.Item("type").ToString = "11" Then
                                    Tipo = "05"
                                End If
                                If jTok2.Item("type").ToString = "9" Then
                                    Tipo = "03"
                                End If
                                If jTok2.Item("type").ToString = "8" Then
                                    Tipo = "04"
                                End If
                                If jTok2.Item("type").ToString = "12" Then
                                    Tipo = "13"
                                End If

                                DataMov = jTok2.Item("date").ToString

                                ' ASP CARITAS - Personalizzazione
                                If ward_id = 1071 Or ward_id = 1072 Or ward_id = 1073 Or ward_id = 1074 Then
                                    ward_id = 1071
                                End If
                                If ward_id = 1168 Or ward_id = 1169 Then
                                    ward_id = 1168
                                End If

                                If ward_id = 1498 Or ward_id = 1499 Or ward_id = 1500 Or ward_id = 1501 Then
                                    ward_id = 1498
                                End If

                                'Virginia Borgheri

                                If ward_id = 1562 Or ward_id = 1563 Or ward_id = 1564 Then
                                    ward_id = 1561
                                End If


                                'Cittadella
                                If ward_id = 12 Or ward_id = 13 Or ward_id = 11 Or ward_id = 10 Or ward_id = 9 Or ward_id = 8 Then
                                    ward_id = 8
                                End If

                                If ward_id = 23 Or ward_id = 24 Then
                                    ward_id = 23
                                End If


                                If ward_id = 17 Or ward_id = 19 Or ward_id = 20 Or ward_id = 985 Or ward_id = 987 Or ward_id = 988 Or ward_id = 989 Then
                                    ward_id = 989
                                End If

                                If ward_id = 999 Or ward_id = 21 Or ward_id = 22 Or ward_id = 998 Then
                                    ward_id = 999
                                End If

                                If ward_id = 14 Or ward_id = 984 Then
                                    ward_id = 14
                                End If


                                Dim Progressivo As Long
                                Dim MySql As String
                                Dim CS As New Cls_CentroServizio
                                CS.DESCRIZIONE = ""

                                If ward_id = 1561 Or ward_id = 1562 Then
                                    ward_id = 1560
                                End If

                                CS.EPersonam = ward_id
                                CS.LeggiEpersonam(Context.Session("DC_OSPITE"), CS.EPersonam, jTok2.Item("conv").ToString)
                                DataAccoglimento = jTok2.Item("date").ToString
                                CServ = CS.CENTROSERVIZIO
                                myPOSTreader.Close()
                                Trovato = True
                                Exit For
                            End If
                            myPOSTreader.Close()
                        End If
                    Next
                End If
                If Trovato = True Then
                    Exit For
                End If
            Next
            If Trovato = True Then
                Exit For
            End If
        Next

        cn.Close()

    End Sub

End Class
