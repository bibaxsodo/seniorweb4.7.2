﻿
Partial Class OspitiWeb_Parametri
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Not IsNumeric(Txt_Anno.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno fatturazione');", True)
            Exit Sub
        End If


        Dim k As New Cls_Parametri



        If RB_Presente.Checked = True Then
            k.GIORNOUSCITA = "P"            
        Else
            k.GIORNOUSCITA = ""
        End If

        If RB_E_Presente.Checked = True Then
            k.GIORNOENTRATA = "P"            
        Else
            k.GIORNOENTRATA = ""            
        End If

        k.AnnoFatturazione = Txt_Anno.Text

        If Chk_GiaCalcolata.Checked = True Then
            k.Calcolato = "S"
        Else
            k.Calcolato = ""
        End If

        k.MeseFatturazione = Dd_Mese.SelectedValue


        If Rb_GiorniMese.Checked = True Then
            k.ImportoMensile = 0
        End If
        If Rb_Diviso30.Checked = True Then
            k.ImportoMensile = 30
        End If
        If Rb_Diviso34.Checked = True Then
            k.ImportoMensile = 34
        End If
        If Rb_MeseAnno.Checked = True Then
            k.ImportoMensile = 12
        End If
        If RB_Anno12.Checked = True Then
            k.ImportoMensile = 365
        End If


        If RB_OrdinaPerDestinatario.Checked = True Then
            k.OrdinaPerDestinatario = 3
        End If

        If RB_OrdinaOspiteServizio.Checked = True Then
            k.OrdinaPerDestinatario = 2
        End If

        If RB_OrdinaPerDestinatarioServizio.Checked = True Then
            k.OrdinaPerDestinatario = 1
        End If

        If RB_OrdinaOspite.Checked = True Then
            k.OrdinaPerDestinatario = 0
        End If


        k.CAUSALEACCOGLIMENTO = DD_CausaleAccoglimento.SelectedValue
        k.CausaleCambioServizioSenzaCognuaglio = DD_CausaleCambioSenzaConguaglio.SelectedValue


        k.CodiceIva = DD_CodiceIVA.SelectedValue

        k.TipoOperazione = DD_TipoOperazione.SelectedValue


        k.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue

        If Chk_EmissioneOspite.Checked = True Then
            k.EmissioneOspite = 1
        Else
            k.EmissioneOspite = 0
        End If
        If Chk_EmissioneParente.Checked = True Then
            k.EmissioneParente = 1
        Else
            k.EmissioneParente = 0
        End If
        If Chk_EmissioneComuni.Checked = True Then
            k.EmissioneComuni = 1
        Else
            k.EmissioneComuni = 0
        End If
        If Chk_EmissioneRegione.Checked = True Then
            k.EmissioneRegione = 1
        Else
            k.EmissioneRegione = 0
        End If

        If Chk_AddebitiAccumolati.Checked = True Then
            k.AddebitiAccreditiComulati = 1
        Else
            k.AddebitiAccreditiComulati = 0
        End If

        k.MastroAnticipo = Txt_MastroAnticipo.Text

        k.ConguaglioMinimo = Txt_ConguagilioMinimo.Text

        k.Mastro = Txt_Mastro.Text
        k.ContoComune = Txt_ContoComune.Text
        k.ContoRegione = Txt_ContoRegione.Text


        k.Deposito = DD_Deposito.SelectedValue

        k.NCDeposito = DD_VariazioneRetta.SelectedValue

        If Chk_VerificaDeposito.Checked = True Then
            k.ControllaInserimentoDeposito = 1
        Else
            k.ControllaInserimentoDeposito = 0
        End If

        If Chk_AzzeraSeNegativo.Checked = True Then
            k.AzzeraSeNegativo = 1
        Else
            k.AzzeraSeNegativo = 0
        End If

        k.DescrizioneDefaultDenaro = Txt_DescrizioneDefaultDenaro.Text

        If Chk_NonAllineareMensileAdifferenza.Checked = True Then
            k.NonAllineareMensileAdifferenza = 1
        Else
            k.NonAllineareMensileAdifferenza = 0
        End If

        If Chk_RaggruppaRicavi.Checked = True Then
            k.RaggruppaRicavi = 1
        Else
            k.RaggruppaRicavi = 0
        End If

        If Chk_NonInAtteasa.Checked = True Then
            k.NonInAtteasa = 1
        Else
            k.NonInAtteasa = 0
        End If

        If Chk_NonUsciteTemporanee.Checked = True Then
            k.NonUsciteTemporanee = 1
        Else
            k.NonUsciteTemporanee = 0
        End If

        If Chk_ModalitaPagamentoObligatoria.Checked = True Then
            k.ModalitaPagamentoObligatoria = 1
        Else
            k.ModalitaPagamentoObligatoria = 0
        End If

        If Chk_AlberghieroAssistenziale.Checked = True Then
            k.AlberghieroAssistenziale = 1
        Else
            k.AlberghieroAssistenziale = 0
        End If

        If Chk_SocialeSanitario.Checked = True Then
            k.SocialeSanitario = 1
        Else
            k.SocialeSanitario = 0
        End If

        If Chk_IdMandatoDaCF.Checked = True Then
            k.IdMandatoDaCF = 1
        Else
            k.IdMandatoDaCF = 0
        End If

        k.MandatoSIA = Txt_MandatoSIA.Text

        k.TipoExport = DD_TipoExport.SelectedItem.Text

        If Chk_TipoAddebitoInFattura.Checked = True Then
            k.TipoAddebitoFatturePrivati = 1
        Else
            k.TipoAddebitoFatturePrivati = 0
        End If

        If Chk_DocumentoIncassi.Checked = True Then
            k.DocumentiIncassiBloccato = 1
        Else
            k.DocumentiIncassiBloccato = 0
        End If



        If Chk_DocumentiIncassiNonPeriodo.Checked = True Then
            k.DocumentiIncassiNonPeriodo = 1
        Else
            k.DocumentiIncassiNonPeriodo = 0
        End If

        If Chk_UltimaUscitaDefinitiva.Checked = True Then
            k.DocumentiIncassiUltimaUscitaDefinitiva = 1
        Else
            k.DocumentiIncassiUltimaUscitaDefinitiva = 0
        End If

        k.DocumentiIncassiMesiPrecedenti = Txt_DocumentoIncassiMesiPrecedenti.Text

        If Chk_GiorniPresenzaDiurno.Checked = True Then
            k.HideDefaultDiurnoOspite = 1
        Else
            k.HideDefaultDiurnoOspite = 0
        End If

        If Chk_ApiV1.Checked = True Then
            k.ApiV1 = 1
        Else
            k.ApiV1 = 0
        End If

        If Chk_SoloModAnagraficaDaEpersonam.Checked = True Then
            k.SoloModAnagraficaDaEpersonam = 1
        Else
            k.SoloModAnagraficaDaEpersonam = 0
        End If


        If Chk_Debug.Checked = True Then
            k.DebugV1 = 1
        Else
            k.DebugV1 = 0
        End If

        If Chk_SeparaPeriodoOspiteParente.Checked = True Then
            k.SeparaPeriodoOspiteParente = 1
        Else
            k.SeparaPeriodoOspiteParente = 0
        End If

        If Chk_CheckIdMandato.Checked = True Then
            k.CheckIdMandato = 1
        Else
            k.CheckIdMandato = 0
        End If

        If Chk_V1MovimentiEpersonam.Checked = True Then
            k.V1MovimentiEpersonam = 1
        Else
            k.V1MovimentiEpersonam = 0
        End If

        If Chk_OraSuMovimenti.Checked = True Then
            k.OraSuMovimenti = 1
        Else
            k.OraSuMovimenti = 0
        End If

        k.ProgressivoIDMandato = Val(Txt_ProgrerssivoID.Text)


        If Chk_FE_RaggruppaRicavi.Checked = True Then
            k.FE_RaggruppaRicavi = 1
        Else
            k.FE_RaggruppaRicavi = 0
        End If


        If Chk_FE_CentroServizio.Checked = True Then
            k.FE_CentroServizio = 1
        Else
            k.FE_CentroServizio = 0
        End If

        If Chk_FE_Competenza.Checked = True Then
            k.FE_Competenza = 1
        Else
            k.FE_Competenza = 0
        End If


        If Chk_FE_IndicaMastroPartita.Checked = True Then
            k.FE_IndicaMastroPartita = 1
        Else
            k.FE_IndicaMastroPartita = 0
        End If


        If Chk_FE_TipoRetta.Checked = True Then
            k.FE_TipoRetta = 1
        Else
            k.FE_TipoRetta = 0
        End If

        If Chk_FE_SoloIniziali.Checked = True Then
            k.FE_SoloIniziali = 1
        Else
            k.FE_SoloIniziali = 0
        End If

        If Chk_FE_GiorniInDescrizione.Checked = True Then
            k.FE_GiorniInDescrizione = 1
        Else
            k.FE_GiorniInDescrizione = 0
        End If


        If Chk_AllineaAnag.Checked = True Then
            k.AllineaAnag = 1
        Else
            k.AllineaAnag = 0
        End If

        If Chk_TabPresenze.Checked = True Then
            k.TabPresenze = 1
        Else
            k.TabPresenze = 0
        End If

        If Chk_AllineaMovimenti.Checked = True Then
            k.AllineaMovimenti = 1
        Else
            k.AllineaMovimenti = 0
        End If

        If Chk_DataSosia.Checked = True Then
            k.DataSosia = 1
        Else
            k.DataSosia = 0
        End If

        If Chk_ImportListino.Checked = True Then
            k.ImportListino = 1
        Else
            k.ImportListino = 0
        End If

        If Chk_FE_NoteFatture.Checked = True Then
            k.FE_NoteFatture = 1
        Else
            k.FE_NoteFatture = 0
        End If

        If Chk_DescrizioneInIncasso.Checked = True Then
            k.DescrizioneAutomaticaIncassi = 1
        Else
            k.DescrizioneAutomaticaIncassi = 0
        End If

        If Chk_ContolloIndirizzo.Checked = True Then
            k.IndirizzoObbligatorioInt = 1
        Else
            k.IndirizzoObbligatorioInt = 0
        End If


        If Chk_SplitEnti.Checked = True Then
            k.SplitSuEnti = 1
        Else
            k.SplitSuEnti = 0
        End If

        If Chk_FatturaNC.Checked = True Then
            k.FattureNCNuovo = 1
        Else
            k.FattureNCNuovo = 0
        End If

        If Chk_DenaroDaCompetenza.Checked = True Then
            k.DenaroDaPeriodo = 1
        Else
            k.DenaroDaPeriodo = 0
        End If


        If Chk_OrdinamentoCR.Checked = True Then
            k.OrdinamentoCR = 1
        Else
            k.OrdinamentoCR = 0
        End If

        If Chk_MovimentiStanze.Checked = True Then
            k.MovimentiStanze = 1
        Else
            k.MovimentiStanze = 0
        End If


        If Chk_Cartella.Checked = True Then
            k.CartellaEpersonam = 1
        Else
            k.CartellaEpersonam = 0
        End If


        If Chk_ModificaPeriodoFatturaNC.Checked = True Then
            k.ModificaPeriodoFatturaNC = 1
        Else
            k.ModificaPeriodoFatturaNC = 0
        End If

        'Chk_AbilitaQrCodeAddAcc

        If Chk_AbilitaQrCodeAddAcc.Checked = True Then
            k.AbilitaQrCodeAddAcc = 1
        Else
            k.AbilitaQrCodeAddAcc = 0
        End If

        If Chk_ParenteIntestatarioUnico.Checked = True Then
            k.ParenteIntestatarioUnico = 1
        Else
            k.ParenteIntestatarioUnico = 0
        End If

        If Chk_StampaServiziAttivi.Checked = True Then
            k.StampaSoloCSAttivi = 1
        Else
            k.StampaSoloCSAttivi = 0
        End If


        If Chk_XMLSEZIONALEPRIMAPROTOCOLLO.Checked = True  Then
            k.XMLSEZIONALEPRIMAPROTOCOLLO = 1
        Else
            k.XMLSEZIONALEPRIMAPROTOCOLLO =0
        End If

        If Chk_SoloDescrizioneRigaFattura.Checked = True Then
            k.SoloDescrizioneRigaFattura = 1
        Else
            k.SoloDescrizioneRigaFattura = 0
        End If

        If Chk_periodoSuCserv.Checked = True Then
            k.PeriodoFatturazioneSuServizio = 1
        Else
            k.PeriodoFatturazioneSuServizio = 0
        End If

        k.CausaleContabileSeparaIVA = DD_CausaleContabileSeparaIVA.SelectedValue


        k.DataCheckEpersonam = Txt_CheckePersonam.Text

        k.ScriviParametri(Session("DC_OSPITE"))

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Modificato", "alert('Modifica Effettuata');", True)


        RB_Presente.Enabled = False
        RB_Assente.Enabled = False
        RB_E_Presente.Enabled = False
        RB_E_Assente.Enabled = False
        Txt_Anno.Enabled = False


        Chk_GiaCalcolata.Enabled = False


        Dd_Mese.Enabled = False



        Rb_Diviso30.Enabled = False
        Rb_Diviso34.Enabled = False
        Rb_GiorniMese.Enabled = False
        Rb_MeseAnno.Enabled = False
        RB_Anno12.Enabled = False



        RB_OrdinaOspite.Enabled = False
        RB_OrdinaOspiteServizio.Enabled = False
        RB_OrdinaPerDestinatario.Enabled = False
        RB_OrdinaPerDestinatarioServizio.Enabled = False


        DD_CausaleAccoglimento.Enabled = False
        DD_CausaleCambioSenzaConguaglio.Enabled = False


        DD_CodiceIVA.Enabled = False


        DD_TipoOperazione.Enabled = False

        DD_ModalitaPagamento.Enabled = False
        Chk_EmissioneOspite.Enabled = False

        Chk_EmissioneParente.Enabled = False


        Chk_EmissioneComuni.Enabled = False

        Chk_EmissioneRegione.Enabled = False
        Chk_NonInAtteasa.Checked = False


        Chk_AddebitiAccumolati.Enabled = False

        Txt_MastroAnticipo.Enabled = False

        Txt_Mastro.Enabled = False
        Txt_ContoComune.Enabled = False
        Txt_ContoRegione.Enabled = False


        DD_Deposito.Enabled = False

        DD_VariazioneRetta.Enabled = False


        Chk_VerificaDeposito.Enabled = False
        Chk_AzzeraSeNegativo.Enabled = False

        Txt_ConguagilioMinimo.Enabled = False

        Chk_NonAllineareMensileAdifferenza.Enabled = False
        Chk_NonUsciteTemporanee.Enabled = False
        Chk_AlberghieroAssistenziale.Enabled = False
        Chk_SocialeSanitario.Enabled = False

        Chk_IdMandatoDaCF.Enabled = False

        Txt_MandatoSIA.Enabled = False

        Chk_TipoAddebitoInFattura.Enabled = False

        Chk_DocumentoIncassi.Enabled = False


        Chk_DocumentiIncassiNonPeriodo.Enabled = False

        Chk_UltimaUscitaDefinitiva.Enabled = False

        Txt_DocumentoIncassiMesiPrecedenti.Enabled = False

        Chk_GiorniPresenzaDiurno.Enabled = False

        Chk_ApiV1.Enabled = False
        Chk_SoloModAnagraficaDaEpersonam.Enabled = False
        Chk_Debug.Enabled = False

        Chk_IdMandatoDaCF.Enabled = False

        Txt_ProgrerssivoID.Enabled = False



        Chk_FE_RaggruppaRicavi.Enabled = False
        Chk_FE_CentroServizio.Enabled = False
        Chk_FE_Competenza.Enabled = False

        Chk_FE_IndicaMastroPartita.Enabled = False
        Chk_FE_TipoRetta.Enabled = False
        Chk_FE_SoloIniziali.Enabled = False
        Chk_FE_GiorniInDescrizione.Enabled = False

        Chk_AllineaAnag.Enabled = False
        Chk_TabPresenze.Enabled = False

        Chk_AllineaMovimenti.Enabled = False
        Chk_DataSosia.Enabled = False

        Chk_ImportListino.Enabled = False

        Chk_DenaroDaCompetenza.Enabled = False

        Chk_MovimentiStanze.Enabled = False

        Chk_AbilitaQrCodeAddAcc.Enabled = False



        Chk_XMLSEZIONALEPRIMAPROTOCOLLO.Checked = False

        
        Chk_SoloDescrizioneRigaFattura.Checked = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Write("<script>")
            Response.Write("window.open('..\Login.aspx','_blank')")
            Response.Write("</script>")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        DD_TipoExport.Items.Clear()
        DD_TipoExport.Items.Add("")
        DD_TipoExport.Items.Add("ALYANTE")
        DD_TipoExport.Items.Add("Seac")
        DD_TipoExport.Items.Add("Multi")
        DD_TipoExport.Items.Add("Gamma")
        DD_TipoExport.Items.Add("AdHoc")
        DD_TipoExport.Items.Add("IpSoa")
        DD_TipoExport.Items.Add("PassePartout")
        DD_TipoExport.Items.Add("ADS SYSTEMATICA")
        DD_TipoExport.Items.Add("Documenti Incassi")
        DD_TipoExport.Items.Add("POLIFARMA")
        DD_TipoExport.Items.Add("ZUCCHETTI")
        DD_TipoExport.Items.Add("Documenti Incassi NO FTP")
        DD_TipoExport.Items.Add("EUSIS")
        DD_TipoExport.Items.Add("BPOINT")

        DD_TipoExport.Items.Add("SISTEMI")

        DD_TipoExport.Items.Add("ADHOC GABBIANO")

        DD_TipoExport.Items.Add("SISTEMA UNO IST.C.")

        'ZUCCHETTI
        'POLIFARMA


        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))

        If k.GIORNOUSCITA = "P" Then
            RB_Presente.Checked = True
            RB_Assente.Checked = False
        Else
            RB_Presente.Checked = False
            RB_Assente.Checked = True
        End If

        If k.GIORNOENTRATA = "P" Then
            RB_E_Presente.Checked = True
            RB_E_Assente.Checked = False
        Else
            RB_E_Presente.Checked = False
            RB_E_Assente.Checked = True
        End If

        Txt_Anno.Text = k.AnnoFatturazione

        If k.Calcolato = "S" Then
            Chk_GiaCalcolata.Checked = True
        Else
            Chk_GiaCalcolata.Checked = False
        End If

        Dd_Mese.SelectedValue = k.MeseFatturazione


        If k.ImportoMensile = 0 Then
            Rb_Diviso30.Checked = False
            Rb_GiorniMese.Checked = True
            Rb_MeseAnno.Checked = False
            RB_Anno12.Checked = False
            Rb_Diviso34.Checked = False
        End If
        If k.ImportoMensile = 30 Then
            Rb_Diviso30.Checked = True
            Rb_GiorniMese.Checked = False
            Rb_MeseAnno.Checked = False
            RB_Anno12.Checked = False
            Rb_Diviso34.Checked = False
        End If
        If k.ImportoMensile = 34 Then
            Rb_Diviso30.Checked = False
            Rb_Diviso34.Checked = True
            Rb_GiorniMese.Checked = False
            Rb_MeseAnno.Checked = False
            RB_Anno12.Checked = False
            Rb_Diviso34.Checked = False
        End If
        If k.ImportoMensile = 12 Then
            Rb_Diviso30.Checked = False
            Rb_GiorniMese.Checked = False
            Rb_MeseAnno.Checked = True
            RB_Anno12.Checked = False
            Rb_Diviso34.Checked = False
        End If
        If k.ImportoMensile = 365 Then
            Rb_Diviso30.Checked = False
            Rb_GiorniMese.Checked = False
            Rb_MeseAnno.Checked = False
            RB_Anno12.Checked = True
            Rb_Diviso34.Checked = False
        End If


        If k.OrdinaPerDestinatario = 3 Then
            RB_OrdinaPerDestinatario.Checked = True
        End If

        If k.OrdinaPerDestinatario = 2 Then
            RB_OrdinaOspiteServizio.Checked = True
        End If

        If k.OrdinaPerDestinatario = 1 Then
            RB_OrdinaPerDestinatarioServizio.Checked = True
        End If

        If k.OrdinaPerDestinatario = 0 Then
            RB_OrdinaOspite.Checked = True
        End If


        Dim CaricaCausali As New Cls_CausaliEntrataUscita

        CaricaCausali.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_CausaleAccoglimento)
        CaricaCausali.UpDateDropBoxCodiceDescrizione(Session("DC_OSPITE"), DD_CausaleCambioSenzaConguaglio)


        DD_CausaleCambioSenzaConguaglio.SelectedValue = k.CausaleCambioServizioSenzaCognuaglio
        DD_CausaleAccoglimento.SelectedValue = k.CAUSALEACCOGLIMENTO

        Dim CaricaIva As New Cls_IVA

        CaricaIva.UpDateDropBox(Session("DC_TABELLE"), DD_CodiceIVA)

        DD_CodiceIVA.SelectedValue = k.CodiceIva

        Dim CaricaTipoOperazione As New Cls_TipoOperazione

        CaricaTipoOperazione.UpDateDropBox(Session("DC_OSPITE"), DD_TipoOperazione)

        DD_TipoOperazione.SelectedValue = k.TipoOperazione

        Dim CaricaModalitaPagamento As New Cls_ModalitaPagamento

        CaricaModalitaPagamento.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPagamento)

        DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleContabileSeparaIVA)


        If k.OrdinaPerDestinatario = 1 Then
            RB_OrdinaPerDestinatarioServizio.Checked = True
        End If
        If k.OrdinaPerDestinatario = 0 Then
            RB_OrdinaOspite.Checked = True
        End If
        If k.OrdinaPerDestinatario = 3 Then
            RB_OrdinaOspiteServizio.Checked = True
        End If
        If k.OrdinaPerDestinatario = 4 Then
            RB_OrdinaPerDestinatario.Checked = True
        End If

        If k.EmissioneOspite = 1 Then
            Chk_EmissioneOspite.Checked = True
        Else
            Chk_EmissioneOspite.Checked = False
        End If
        If k.EmissioneParente = 1 Then
            Chk_EmissioneParente.Checked = True
        Else
            Chk_EmissioneParente.Checked = False
        End If
        If k.EmissioneComuni = 1 Then
            Chk_EmissioneComuni.Checked = True
        Else
            Chk_EmissioneComuni.Checked = False
        End If
        If k.EmissioneRegione = 1 Then
            Chk_EmissioneRegione.Checked = True
        Else
            Chk_EmissioneRegione.Checked = False
        End If

        If k.AddebitiAccreditiComulati = 1 Then
            Chk_AddebitiAccumolati.Checked = True
        Else
            Chk_AddebitiAccumolati.Checked = False
        End If

        Txt_MastroAnticipo.Text = k.MastroAnticipo

        Txt_Mastro.Text = k.Mastro
        Txt_ContoComune.Text = k.ContoComune
        Txt_ContoRegione.Text = k.ContoRegione


        Dim CaricaDeposito As New Cls_Addebito

        CaricaDeposito.UpDateDropBox(Session("DC_OSPITE"), DD_Deposito)
        CaricaDeposito.UpDateDropBox(Session("DC_OSPITE"), DD_VariazioneRetta)

        DD_Deposito.SelectedValue = k.Deposito

        DD_VariazioneRetta.SelectedValue = k.NCDeposito

        If k.ControllaInserimentoDeposito = 1 Then
            Chk_VerificaDeposito.Checked = True
        Else
            Chk_VerificaDeposito.Checked = False
        End If

        If k.AzzeraSeNegativo = 1 Then
            Chk_AzzeraSeNegativo.Checked = True
        Else
            Chk_AzzeraSeNegativo.Checked = False
        End If

        Txt_ConguagilioMinimo.Text = Format(k.ConguaglioMinimo, "#,##0.00")

        Txt_DescrizioneDefaultDenaro.Text = k.DescrizioneDefaultDenaro

        If k.NonAllineareMensileAdifferenza = 1 Then
            Chk_NonAllineareMensileAdifferenza.Checked = True
        Else
            Chk_NonAllineareMensileAdifferenza.Checked = False
        End If

        If k.RaggruppaRicavi = 1 Then
            Chk_RaggruppaRicavi.Checked = True
        Else
            Chk_RaggruppaRicavi.Checked = False
        End If

        If k.NonInAtteasa = 1 Then
            Chk_NonInAtteasa.Checked = True
        Else
            Chk_NonInAtteasa.Checked = False
        End If

        If k.NonUsciteTemporanee = 1 Then
            Chk_NonUsciteTemporanee.Checked = True
        Else
            Chk_NonUsciteTemporanee.Checked = False
        End If

        If k.ModalitaPagamentoObligatoria = 1 Then
            Chk_ModalitaPagamentoObligatoria.Checked = True
        Else
            Chk_ModalitaPagamentoObligatoria.Checked = False
        End If

        If k.AlberghieroAssistenziale = 1 Then
            Chk_AlberghieroAssistenziale.Checked = True
        Else
            Chk_AlberghieroAssistenziale.Checked = False
        End If

        If k.SocialeSanitario = 1 Then
            Chk_SocialeSanitario.Checked = True
        Else
            Chk_SocialeSanitario.Checked = False
        End If
        If k.AlberghieroAssistenziale = 0 And k.SocialeSanitario = 0 Then
            Chk_NonAttivo.Checked = True
        Else
            Chk_NonAttivo.Checked = False
        End If

        If k.IdMandatoDaCF = 1 Then
            Chk_IdMandatoDaCF.Checked = True
        Else
            Chk_IdMandatoDaCF.Checked = False
        End If

        Txt_MandatoSIA.Text = k.MandatoSIA

        If k.TipoAddebitoFatturePrivati = 1 Then
            Chk_TipoAddebitoInFattura.Checked = True
        Else
            Chk_TipoAddebitoInFattura.Checked = False
        End If

        DD_TipoExport.SelectedItem.Text = k.TipoExport

        If k.DocumentiIncassiBloccato = 1 Then
            Chk_DocumentoIncassi.Checked = True
        Else
            Chk_DocumentoIncassi.Checked = False
        End If


        If k.DocumentiIncassiNonPeriodo = 1 Then
            Chk_DocumentiIncassiNonPeriodo.Checked = True
        Else
            Chk_DocumentiIncassiNonPeriodo.Checked = False
        End If


        If k.DocumentiIncassiUltimaUscitaDefinitiva = 1 Then
            Chk_UltimaUscitaDefinitiva.Checked = True
        Else
            Chk_UltimaUscitaDefinitiva.Checked = False
        End If


        Txt_DocumentoIncassiMesiPrecedenti.Text = k.DocumentiIncassiMesiPrecedenti


        If k.HideDefaultDiurnoOspite = 1 Then
            Chk_GiorniPresenzaDiurno.Checked = True
        Else
            Chk_GiorniPresenzaDiurno.Checked = False
        End If


        If k.ApiV1 = 1 Then
            Chk_ApiV1.Checked = True
        Else
            Chk_ApiV1.Checked = False
        End If

        If k.SoloModAnagraficaDaEpersonam = 1 Then
            Chk_SoloModAnagraficaDaEpersonam.Checked = True
        Else
            Chk_SoloModAnagraficaDaEpersonam.Checked = False
        End If


        If k.DebugV1 = 1 Then
            Chk_Debug.Checked = True
        Else
            Chk_Debug.Checked = False
        End If


        If k.SeparaPeriodoOspiteParente = 1 Then
            Chk_SeparaPeriodoOspiteParente.Checked = True
        Else
            Chk_SeparaPeriodoOspiteParente.Checked = False
        End If


        If k.CheckIdMandato = 1 Then
            Chk_CheckIdMandato.Checked = True
        Else
            Chk_CheckIdMandato.Checked = False
        End If

        If k.V1MovimentiEpersonam = 1 Then
            Chk_V1MovimentiEpersonam.Checked = True
        Else
            Chk_V1MovimentiEpersonam.Checked = False
        End If


        If k.OraSuMovimenti = 1 Then
            Chk_OraSuMovimenti.Checked = True
        Else
            Chk_OraSuMovimenti.Checked = False
        End If


        Txt_ProgrerssivoID.Text = k.ProgressivoIDMandato


        If k.FE_RaggruppaRicavi = 1 Then
            Chk_FE_RaggruppaRicavi.Checked = True
        Else
            Chk_FE_RaggruppaRicavi.Checked = False
        End If


        If k.FE_IndicaMastroPartita = 1 Then
            Chk_FE_IndicaMastroPartita.Checked = True
        Else
            Chk_FE_IndicaMastroPartita.Checked = False
        End If


        If k.FE_GiorniInDescrizione = 1 Then
            Chk_FE_GiorniInDescrizione.Checked = True
        Else
            Chk_FE_GiorniInDescrizione.Checked = False
        End If



        If k.FE_Competenza = 1 Then
            Chk_FE_Competenza.Checked = True
        Else
            Chk_FE_Competenza.Checked = False
        End If

        If k.FE_CentroServizio = 1 Then
            Chk_FE_CentroServizio.Checked = True
        Else
            Chk_FE_CentroServizio.Checked = False
        End If

        If k.FE_TipoRetta = 1 Then
            Chk_FE_TipoRetta.Checked = True
        Else
            Chk_FE_TipoRetta.Checked = False
        End If


        If k.FE_SoloIniziali = 1 Then
            Chk_FE_SoloIniziali.Checked = True
        Else
            Chk_FE_SoloIniziali.Checked = False
        End If


        If k.AllineaAnag = 1 Then
            Chk_AllineaAnag.Checked = True
        Else
            Chk_AllineaAnag.Checked = False
        End If

        If k.TabPresenze = 1 Then
            Chk_TabPresenze.Checked = True
        Else
            Chk_TabPresenze.Checked = False
        End If

        If k.AllineaMovimenti = 1 Then
            Chk_AllineaMovimenti.Checked = True
        Else
            Chk_AllineaMovimenti.Checked = False
        End If


        If k.DataSosia = 1 Then
            Chk_DataSosia.Checked = True
        Else
            Chk_DataSosia.Checked = False
        End If

        If k.ImportListino = 1 Then
            Chk_ImportListino.Checked = True
        Else
            Chk_ImportListino.Checked = False
        End If


        If k.FE_NoteFatture = 1 Then
            Chk_FE_NoteFatture.Checked = True
        Else
            Chk_FE_NoteFatture.Checked = False
        End If

        Lbl_UltimoOspiteElaborato.Text = ""
        If k.CodiceOspite > 0 Then
            Lbl_UltimoOspiteElaborato.Text = " (Elaborazione " & k.CodiceOspite & ")"
        End If



        If k.DescrizioneAutomaticaIncassi = 1 Then
            Chk_DescrizioneInIncasso.Checked = True
        Else
            Chk_DescrizioneInIncasso.Checked = False
        End If


        If k.IndirizzoObbligatorioInt = 1 Then
            Chk_ContolloIndirizzo.Checked = True
        Else
            Chk_ContolloIndirizzo.Checked = False
        End If

        If k.SplitSuEnti = 1 Then
            Chk_SplitEnti.Checked = True
        Else
            Chk_SplitEnti.Checked = False
        End If

        If k.FattureNCNuovo = 1 Then
            Chk_FatturaNC.Checked = True
        Else
            Chk_FatturaNC.Checked = False
        End If

        If k.DenaroDaPeriodo = 1 Then
            Chk_DenaroDaCompetenza.Checked = True
        Else
            Chk_DenaroDaCompetenza.Checked = False
        End If

        If k.OrdinamentoCR = 1 Then
            Chk_OrdinamentoCR.Checked = True
        Else
            Chk_OrdinamentoCR.Checked = False
        End If

        If k.MovimentiStanze = 1 Then
            Chk_MovimentiStanze.Checked = True
        Else
            Chk_MovimentiStanze.Checked = False
        End If



        If k.CartellaEpersonam = 1 Then
            Chk_Cartella.Checked = True
        Else
            Chk_Cartella.Checked = False
        End If


        If k.ModificaPeriodoFatturaNC = 1 Then
            Chk_ModificaPeriodoFatturaNC.Checked = True
        Else
            Chk_ModificaPeriodoFatturaNC.Checked = False
        End If

        If k.AbilitaQrCodeAddAcc = 1 Then
            Chk_AbilitaQrCodeAddAcc.Checked = True
        Else
            Chk_AbilitaQrCodeAddAcc.Checked = False
        End If

        If k.ParenteIntestatarioUnico = 1 Then
            Chk_ParenteIntestatarioUnico.Checked = True
        Else
            Chk_ParenteIntestatarioUnico.Checked = False
        End If

        If k.StampaSoloCSAttivi = 1 Then
            Chk_StampaServiziAttivi.Checked = True
        Else
            Chk_StampaServiziAttivi.Checked = False
        End If

        If k.XMLSEZIONALEPRIMAPROTOCOLLO = 1 Then
            Chk_XMLSEZIONALEPRIMAPROTOCOLLO.Checked = True 
        Else
            Chk_XMLSEZIONALEPRIMAPROTOCOLLO.Checked = False
        End If

        
        If k.SoloDescrizioneRigaFattura = 1 Then
            Chk_SoloDescrizioneRigaFattura.Checked = True
        Else
            Chk_SoloDescrizioneRigaFattura.Checked = false
        End If

        If k.PeriodoFatturazioneSuServizio = 1 Then
            Chk_periodoSuCserv.Checked = True
        Else
            Chk_periodoSuCserv.Checked = False
        End If


        DD_CausaleContabileSeparaIVA.SelectedValue = k.CausaleContabileSeparaIVA

        Txt_CheckePersonam.Text = Format(k.DataCheckEpersonam, "dd/MM/yyyy")

        Call EseguiJS()
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_ConguagilioMinimo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Config.aspx")
    End Sub
End Class
