﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class ModalitaCalcolo
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Sub loadpagina()
        Dim x As New ClsOspite
        Dim d As New Cls_Modalita


        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If
            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Call loadpagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


            Dim RB_Ospite As RadioButton = DirectCast(Grd_Retta.Rows(0).FindControl("RB_Ospite"), RadioButton)
            Dim RB_Parente As RadioButton = DirectCast(Grd_Retta.Rows(0).FindControl("RB_Parente"), RadioButton)


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If Param.SocialeSanitario = 1 Then
                RB_Ospite.Text = "Ospite (Quota Sociale)"
                RB_Parente.Text = "Parente (Quota Sociale)"

                Grd_Retta.Columns(2).ItemStyle.Width = 700
            End If

            If Param.AlberghieroAssistenziale = 1 Then
                RB_Ospite.Text = "Ospite (Quota Alberghiera)"
                RB_Parente.Text = "Parente (Quota Alberghiera)"

                Grd_Retta.Columns(2).ItemStyle.Width = 700
            End If

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

        Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("TxtData"), TextBox)
        Dim RB_Ospite As RadioButton = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("RB_Ospite"), RadioButton)
        Dim RB_Parente As RadioButton = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("RB_Parente"), RadioButton)


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.SocialeSanitario = 1 Then
            RB_Ospite.Text = "Ospite (Quota Sociale)"
            RB_Parente.Text = "Parente (Quota Sociale)"
        End If

        If Param.AlberghieroAssistenziale = 1 Then
            RB_Ospite.Text = "Ospite (Quota Alberghiera)"
            RB_Parente.Text = "Parente (Quota Alberghiera)"
        End If

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)

        Call EseguiJS()
    End Sub
    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If

    End Sub






    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim RB_Ospite As RadioButton = DirectCast(e.Row.FindControl("RB_Ospite"), RadioButton)
            Dim RB_Parente As RadioButton = DirectCast(e.Row.FindControl("RB_Parente"), RadioButton)
            Dim RB_Comune As RadioButton = DirectCast(e.Row.FindControl("RB_Comune"), RadioButton)
            Dim RB_Ente As RadioButton = DirectCast(e.Row.FindControl("RB_Ente"), RadioButton)

            
            RB_Ospite.Checked = False
            RB_Parente.Checked = False
            RB_Comune.Checked = False
            RB_Ente.Checked = False

            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "O" Then
                RB_Ospite.Checked = True
            End If
            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "P" Then
                RB_Parente.Checked = True
            End If
            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "C" Then
                RB_Comune.Checked = True
            End If

            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "E" Then
                RB_Ente.Checked = True
            End If

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If Param.SocialeSanitario = 1 Then
                RB_Ospite.Text = "Ospite (Quota Sociale)"
                RB_Parente.Text = "Parente (Quota Sociale)"

                Grd_Retta.Columns(2).ItemStyle.Width = 500
            End If

            If Param.AlberghieroAssistenziale = 1 Then
                RB_Ospite.Text = "Ospite (Quota Alberghiera)"
                RB_Parente.Text = "Parente (Quota Alberghiera)"

                Grd_Retta.Columns(2).ItemStyle.Width = 500
            End If


            Call EseguiJS()

        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"


        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("CODICEEXTRA", GetType(String))
        MyTable.Columns.Add("RIPARTIZIONE", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim RB_Ospite As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ospite"), RadioButton)
            Dim RB_Parente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Parente"), RadioButton)

            Dim RB_Comune As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Comune"), RadioButton)
            Dim RB_Ente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ente"), RadioButton)


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If Param.SocialeSanitario = 1 Then
                RB_Ospite.Text = "Ospite (Quota Sociale)"
                RB_Parente.Text = "Parente (Quota Sociale)"
            End If

            If Param.AlberghieroAssistenziale = 1 Then
                RB_Ospite.Text = "Ospite (Quota Alberghiera)"
                RB_Parente.Text = "Parente (Quota Alberghiera)"
            End If


            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text

            If RB_Ospite.Checked = True Then
                myrigaR(1) = "O"
            End If
            If RB_Parente.Checked = True Then
                myrigaR(1) = "P"
            End If
            If RB_Comune.Checked = True Then
                myrigaR(1) = "C"
            End If
            If RB_Ente.Checked = True Then
                myrigaR(1) = "E"
            End If




            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)

            Dim RB_Ospite As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ospite"), RadioButton)
            Dim RB_Parente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Parente"), RadioButton)

            Dim RB_Comune As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Comune"), RadioButton)
            Dim RB_Ente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ente"), RadioButton)

            If Not IsDate(TxtData.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If RB_Ospite.Checked = False And RB_Parente.Checked = False And RB_Comune.Checked = False And RB_Ente.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare differenza per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If


            For T = 0 To Grd_Retta.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_Retta.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next

        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")

        Dim Log As New Cls_LogPrivacy

        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Modalita

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "MODALITA", AppoggioJS)


        Dim X1 As New Cls_Modalita

        X1.CodiceOspite = Session("CODICEOSPITE")
        X1.CentroServizio = Session("CODICESERVIZIO")

        X1.EliminaAll(Session("DC_OSPITE"))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)

            Dim RB_Ospite As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ospite"), RadioButton)
            Dim RB_Parente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Parente"), RadioButton)

            Dim RB_Comune As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Comune"), RadioButton)
            Dim RB_Ente As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Ente"), RadioButton)


            Dim X As New Cls_Modalita

            X.CODICEOSPITE = Session("CODICEOSPITE")
            X.CENTROSERVIZIO = Session("CODICESERVIZIO")

            If IsDate(TxtData.Text) Then
                X.Data = TxtData.Text

                If RB_Ospite.Checked = True Then
                    X.MODALITA = "O"
                End If
                If RB_Parente.Checked = True Then
                    X.MODALITA = "P"
                End If
                If RB_Comune.Checked = True Then
                    X.MODALITA = "C"
                End If
                If RB_Ente.Checked = True Then
                    X.MODALITA = "E"
                End If

                X.AggiornaDB(Session("DC_OSPITE"))
            End If
        Next

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

    End Sub

    

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub
End Class
