﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_WizardDomiciliare" CodeFile="WizardDomiciliare.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="axex" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <div style="float: right;">
                <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
            </div>
            <axex:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                Width="100%" BorderStyle="None" Style="margin-right: 39px">
                <axex:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                    <HeaderTemplate>
                        Invervallo 1     
                    </HeaderTemplate>
                    <ContentTemplate>
                        <h1>
                            <asp:Label ID="Lbl_NomeOspite" runat="server" Text=""></asp:Label>
                        </h1>
                        <br />
                        <br />


                        <label style="display: block; float: left; width: 160px;">Ora Inizio  :</label>
                        <asp:TextBox ID="Txt_OraInizio" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Fine  :</label>
                        <asp:TextBox ID="Txt_OraFine" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Descrizione :</label>
                        <asp:TextBox ID="Txt_Descrizione" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Operatore :</label>
                        <asp:DropDownList ID="DD_Operatore" runat="server"></asp:DropDownList>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Tipologia :</label>
                        <asp:DropDownList ID="DD_Tipologia" runat="server"></asp:DropDownList>
                        <br />
                        <br />

                        <table style="border-width: 1px; border-color: Black;">
                            <tr>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                                <td>6</td>
                                <td>7</td>
                                <td>8</td>
                                <td>9</td>
                                <td>10</td>
                                <td>11</td>
                                <td>12</td>
                                <td>13</td>
                                <td>14</td>
                                <td>15</td>
                                <td>16</td>
                                <td>17</td>
                                <td>18</td>
                                <td>19</td>
                                <td>20</td>
                                <td>21</td>
                                <td>22</td>
                                <td>23</td>
                                <td>24</td>
                                <td>25</td>
                                <td>26</td>
                                <td>27</td>
                                <td>28</td>
                                <td>29</td>
                                <td>30</td>
                                <td>31</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="GG_1" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_2" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_3" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_4" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_5" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_6" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_7" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_8" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_9" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_10" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_11" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_12" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_13" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_14" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_15" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_16" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_17" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_18" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_19" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_20" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_21" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_22" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_23" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_24" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_25" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_26" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_27" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_28" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_29" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_30" runat="server" Text="" /></td>
                                <td>
                                    <asp:CheckBox ID="GG_31" runat="server" Text="" /></td>
                            </tr>
                        </table>
                    </ContentTemplate>

                </axex:TabPanel>


            </axex:TabContainer>
        </div>
    </form>
</body>
</html>
