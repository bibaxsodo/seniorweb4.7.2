﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_CausaliEntrataUscita" EnableEventValidation="false" CodeFile="CausaliEntrataUscita.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="x-ua-compatible" content="IE=9" />
	<title>Causal Entrata Uscita</title>
	<asp:PlaceHolder runat="server">
		<%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
	</asp:PlaceHolder>
	<link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="images/SENIOR.ico" />
	<script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
	<script src="/js/formatnumer.js" type="text/javascript"></script>
	<script src="js/JSErrore.js" type="text/javascript"></script>
	<link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery.autocomplete.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('html').keyup(function (event) {

				if (event.keyCode == 113) {
					__doPostBack("Btn_Modifica", "0");
				}


			});
		});

		$(document).ready(function () {
			if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
		});
	</script>
</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
			<Scripts>
				<asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
			</Scripts>
		</asp:ScriptManager>
		<asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
		<div style="text-align: left;">
			<table style="width: 100%;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="width: 160px; background-color: #F0F0F0;"></td>
					<td>
						<div class="Titolo">Ospiti - Tabella - Causali Entrata Uscita</div>
						<div class="SottoTitolo">
							<br />
							<br />
						</div>
					</td>
					<td style="text-align: right;">
						<div class="DivTasti">
							<asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
			<asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
							<asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
						</div>
					</td>
				</tr>
			</table>
			<table style="width: 100%;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
						<a href="Menu_Ospiti.aspx" style="border-width: 0px;">
							<img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
						<asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
					</td>
					<td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
						<xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
							<xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
								<HeaderTemplate>
									Causale              
								</HeaderTemplate>
								<ContentTemplate>


									<label class="LabelCampo">Codice :</label>
									<asp:TextBox ID="Txt_Codice" runat="server" MaxLength="2" Width="47px" AutoPostBack="true"></asp:TextBox>
									<asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
									<br />
									<br />
									<label class="LabelCampo">Descrizione :</label>
									<asp:TextBox ID="Txt_Descrizione" runat="server" MaxLength="30" Width="445px" AutoPostBack="true"></asp:TextBox>
									<asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
									<br />
									<br />
									<label class="LabelCampo">Raggruppamento :</label>
									<asp:DropDownList ID="DD_Raggruppamento" runat="server" Width="240px" AutoPostBack="True"></asp:DropDownList><br />


									<br />
									<label class="LabelCampo">Giorno della Causale :</label>
									<asp:RadioButton ID="RB_Presente" runat="server" GroupName="Giorno" />Presente
			<asp:RadioButton ID="RB_Assente" runat="server" GroupName="Giorno" />Assente
			<asp:RadioButton ID="RB_NonCalcolato" runat="server" GroupName="Giorno" />Non Calcolare
			<asp:RadioButton ID="RB_CalcolaImp" runat="server" GroupName="Giorno" />Calcolare solo Importo<br />
									<br />
									<label class="LabelCampo">Tipo Movimento : </label>
									<asp:RadioButton ID="RB_TMPresente" runat="server" GroupName="TIPO" />Presente
			<asp:RadioButton ID="RB_TMAssente" runat="server" GroupName="TIPO" />Assente
			<asp:RadioButton ID="RB_TMNonContare" runat="server" GroupName="TIPO" />Non Calcolare
			<asp:RadioButton ID="RB_TMCalcolaImp" runat="server" GroupName="TIPO" />Calcolare solo Importo<br />
									<br />
									<table width="90%">
										<tr>
											<td width="60%">Regola</td>
											<td width="30%">Variabili</td>
										</tr>
										<tr>
											<td width="70%">
												<asp:TextBox ID="Txt_Regola" runat="server" Height="280px" TextMode="MultiLine" Width="100%"></asp:TextBox>
											</td>
											<td width="30%">
												<div id="idRegola" style="width: 100%; height: 280px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
													Cserv          <i style="font-size: x-small;">CENTRO SERVIZIO FATTURAZIONE</i><br />
													CodiceOspite   <i style="font-size: x-small;">CODICE OSPITE FATTURAZIONE</i><br />
													Anno           <i style="font-size: x-small;">ANNO FATTURAZIONE</i><br />
													Mese           <i style="font-size: x-small;">MESE</i><br />
													GiornoElab <i style="font-size: x-small;">GIORNO ELABORAZIONE IN FORMATO DATA</i><br />
													TipoMov <i style="font-size: x-small;">TIPO MOVIMENTO A-P</i><br />
													TipoRetta <i style="font-size: x-small;">TIPO RETTA OSPITE</i><br />
													ComuneResidenza <i style="font-size: x-small;">CODICE PROVINCIA COMUNE</i><br />
													Stato <i style="font-size: x-small;">STATO AUTOSUFFICENZA</i><br />
													SalvaOspite   <i style="font-size: x-small;">RETTA OSPITE GIORNO</i><br />
													SalvaOspite2  <i style="font-size: x-small;">RETTA 2 OSPITE GIORNO</i><br />
													SalvaComune    <i style="font-size: x-small;">RETTA COMUNE GIORNO</i><br />
													SalvaJolly     <i style="font-size: x-small;">RETTA JOLLY GIORNO</i><br />
													SalvaRegione   <i style="font-size: x-small;">RETTA REGIONE</i><br />
													SalvaParente () <i style="font-size: x-small;">RETTA PARENTE GIORNO</i><br />
													SalvaParente2() <i style="font-size: x-small;">RETTA 2 PARENTE GIORNO</i><br />
													SalvaPerc()     <i style="font-size: x-small;">PERCENTUALE  PARENTE GIORNO</i><br />
													SalvaEnte       <i style="font-size: x-small;">RETTA PARENTE</i><br />
													NumeroComune  <i style="font-size: x-small;">NUMERO COMUNE GIORNO</i><br />
													NumeroJolly <i style="font-size: x-small;">NUMERO JOLLY GIORNO</i><br />
													NumeroRegione <i style="font-size: x-small;">NUMERO REGIONE GIORNO</i><br />
													SalvaImpExtrOspite()  <i style="font-size: x-small;">IMPORTO EXTRA FISSO OSPITE</i><br />
													CodiceExtrOspite()    <i style="font-size: x-small;">CODICE EXTRA FISSO OSPITE</i><br />
													SalvaImpExtrComune()  <i style="font-size: x-small;">IMPORTO EXTRA FISSO COMUNE</i><br />
													CodiceExtrComune()   <i style="font-size: x-small;">CODICE EXTRA FISSO COMUNE</i><br />
													SalvaImpExtrJolly() <i style="font-size: x-small;">IMPORTO EXTRA FISSO JOLLY</i><br />
													SalvaImpExtrParent()  <i style="font-size: x-small;">IMPORTO EXTRA FISSO PAR</i><br />
													CodiceExtrParent()    <i style="font-size: x-small;">CODICE EXTRA FISSO JOLLY</i><br />
													GiorniPres    <i style="font-size: x-small;">GIORNI PRESENZA OSPITE MESE PRIMO PERIODO</i><br />
													GiorniAss     <i style="font-size: x-small;">GIORNI ASSENZA OSPITE MESE PRIMO PERIODO</i><br />
													NonGiorniPres <i style="font-size: x-small;">GIORNI PRESENZA OSPITE MESE SECONDO PERIODO</i><br />
													NonGiorniAss  <i style="font-size: x-small;">GIORNI PRESENZA OSPITE MESE SECONDO PERIODO</i><br />
													GiorniPresEnte <i style="font-size: x-small;">GIORNI PRESENZA ENTE MESE AUTO</i><br />
													GiorniAssEnte  <i style="font-size: x-small;">GIORNI ASSENZA ENTE MESE AUTO</i><br />
													NonGiorniPresEnte <i style="font-size: x-small;">GIORNI PRESENZA ENTE MESE NON AUTO</i><br />
													NonGiorniAssEnte  <i style="font-size: x-small;">GIORNI ASSENZA ENTE MESE NON AUTO</i><br />
													GiorniPresParente()   <i style="font-size: x-small;">GIORNI PRESENZA PRIMO PERIODO</i><br />
													GiorniAssParente ()    <i style="font-size: x-small;">GIORNI ASSENZA PRIMO PERIODO</i><br />
													NonGiorniPresParente () <i style="font-size: x-small;">GIORNI PRESENZA SECONDO PERIODO</i><br />
													NonGiorniAssParente () <i style="font-size: x-small;">GIORNI ASSENZA SECONDO PERIODO</i><br />
													GiorniPresComune () <i style="font-size: x-small;">GIORNI PRESENZA AUTO</i><br />
													GiorniAssComune () <i style="font-size: x-small;">GIORNI ASSENZA AUTO</i><br />
													NonGiorniPresComune () <i style="font-size: x-small;">GIORNI PRESENZA NON AUTO</i><br />
													NonGiorniAssComune ()  <i style="font-size: x-small;">GIORNI ASSENZA NON AUTO</i><br />
													GiorniPresJolly () <i style="font-size: x-small;">GIORNI PRESENZA JOLLY</i><br />
													GiorniAssJolly () <i style="font-size: x-small;">GIORNI ASSENZA JOLLY</i><br />
													NonGiorniPresJolly ()  <i style="font-size: x-small;">GIORNI PRESENZA JOLLY  NON AUTO</i><br />
													NonGiorniAssJolly () <i style="font-size: x-small;">GIORNI ASSENZA JOLLY  NON AUTO</i><br />
													GiorniPresRegione () <i style="font-size: x-small;">GIORNI PRESENZA REGIONE</i><br />
													GiorniAssRegione () <i style="font-size: x-small;">GIORNI ASSENZA REGIONE</i><br />
													NonGiorniPresRegione () <i style="font-size: x-small;">GIORNI PRESENZA REGIONE NON AUTO</i><br />
													NonGiorniAssRegione () <i style="font-size: x-small;">GIORNI ASSENZA REGIONE NON AUTO</i><br />
													CodiceComune() <i style="font-size: x-small;">COMUNI ATTIVI NEL MESE</i><br />
													CodiceJolly()   <i style="font-size: x-small;">JOLLY ATTIVI NEL MESE</i><br />
													CodiceRegione()  <i style="font-size: x-small;">REGIONI ATTIVI NEL MESE</i><br />
													pXSalvaExtImporto1()  <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU PARENTI</i><br />
													pXSalvaExtImporto2()  <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU PARENTI</i><br />
													pXSalvaExtImporto3()  <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU PARENTI</i><br />
													pXSalvaExtImporto4()  <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU PARENTI</i><br />
													XSalvaExtImporto1 <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU OSPITE</i><br />
													XSalvaExtImporto2 <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU OSPITE</i><br />
													XSalvaExtImporto3 <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU OSPITE</i><br />
													XSalvaExtImporto4 <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU OSPITE</i><br />
													XSalvaExtImporto1C <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU COMUNE</i><br />
													XSalvaExtImporto2C <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU COMUNE</i><br />
													XSalvaExtImporto3C <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU COMUNE</i><br />
													XSalvaExtImporto4C <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU COMUNE</i><br />
													ImpExtrOspMan1 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													ImpExtrOspMan2 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													ImpExtrOspMan3 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													ImpExtrOspMan4 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													NumExtrOspMan1 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													NumExtrOspMan2 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													NumExtrOspMan3 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													NumExtrOspMan4 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													TipoAddExtrOspMan1 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													TipoAddExtrOspMan2 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													TipoAddExtrOspMan3 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													TipoAddExtrOspMan4 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
													ImpExtrParMan1()  <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													ImpExtrParMan2()  <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													ImpExtrParMan3()  <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													ImpExtrParMan4()  <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													NumExtrParMan1()  <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													NumExtrParMan2()  <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													NumExtrParMan3()  <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													NumExtrParMan4()  <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													TipoExtrParMan1() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													TipoExtrParMan2()  <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													TipoExtrParMan3() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													TipoExtrParMan4() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
													ImpExtrComMan1 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  COMUNI</i><br />
													ImpExtrComMan2 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  COMUNI</i><br />
													ImpExtrComMan3 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  COMUNI</i><br />
													ImpExtrComMan4 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  COMUNI</i><br />
													NumExtrComMan1 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													NumExtrComMan2 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													NumExtrComMan3 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													NumExtrComMan4 <i style="font-size: x-small;">QUANTITA ADDEBITO GESTITO IN CALCOLO SU COMUNE</i><br />
													TipoExtrComMan1() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													TipoExtrComMan2() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													TipoExtrComMan3() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													TipoExtrComMan4() <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU COMUNI</i><br />
													Variabile1 <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													Variabile2 <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													Variabile3 <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													Variabile4 <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													VariabileA <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													VariabileB <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													VariabileC <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													VariabileD <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
													GiorniAssenza  <i style="font-size: x-small;">GIORNI DA ULTIMA PRESENZA</i><br />
													GiorniRipetizioneCausale   <i style="font-size: x-small;">GIORNI DI RIPETIZIONE DELLA CAUSALE</i><br />
													GiorniRipetizioneCausaleNC  <i style="font-size: x-small;">GIORNI DI RIPET. DELLA CAUSALE NON CONS. [C]</i><br />
													AssenzaNellAnno  <i style="font-size: x-small;">NUMERO ASSENZE FATTE NELL'ANNO</i><br />
													AssenzaDiurnoNellAnno <i style="font-size: x-small;">NUMERO ASSENZE FATTE NELL'ANNO ESCLUDENDO CAUSALE C</i><br />
													AssenzaDiurnoNellAnnoEscludiAssenza <i style="font-size: x-small;">NUMERO ASSENZE FATTE NELL'ANNO ESCLUDENDO CAUSALE A E C</i><br />
													DomOre()  <i style="font-size: x-small;">ASSISTENZA DOMICILIARE DURATA INTERVENTO IN MINUTI</i><br />
													DomOpertore() <i style="font-size: x-small;">ASSISTENZA DOMICILIARE OPERATORE INTERVENTO</i><br />
													DomTipo() <i style="font-size: x-small;">ASSISTENZA DOMICILIARE TIPO INTERVENTO</i><br />
													EtaUtente <i style="font-size: x-small;">Anni Ospite</i><br />
													TipoImportoRegione <i style="font-size: x-small;">Tipo importo regione a data</i><br />
													ModificaTipoRegione <i style="font-size: x-small;">Modifica tipo importo regione</i><br />
												</div>
											</td>
										</tr>
									</table>
								</ContentTemplate>
							</xasp:TabPanel>
							<xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
								<HeaderTemplate>
									Diurno
								</HeaderTemplate>
								<ContentTemplate>
									<table>
										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale1" ImageUrl="images/diurno_1.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale2" ImageUrl="images/diurno_2.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale3" ImageUrl="images/diurno_3.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale4" ImageUrl="images/diurno_4.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale5" ImageUrl="images/diurno_5.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale6" ImageUrl="images/diurno_6.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>
										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale7" ImageUrl="images/diurno_7.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale8" ImageUrl="images/diurno_8.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale9" ImageUrl="images/diurno_9.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale10" ImageUrl="images/diurno_10.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale11" ImageUrl="images/diurno_11.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale12" ImageUrl="images/diurno_12.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>
										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale13" ImageUrl="images/diurno_13.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale14" ImageUrl="images/diurno_14.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale15" ImageUrl="images/diurno_15.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale16" ImageUrl="images/diurno_16.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale17" ImageUrl="images/diurno_17.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale18" ImageUrl="images/diurno_18.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>
										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale19" ImageUrl="images/diurno_19.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale20" ImageUrl="images/diurno_20.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale21" ImageUrl="images/diurno_21.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale22" ImageUrl="images/diurno_22.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale23" ImageUrl="images/diurno_23.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale24" ImageUrl="images/diurno_24.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>
										<tr>
											<td colspan="5"><font size="xx-small">Icone</font></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale25" ImageUrl="images/diurno_25.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale26" ImageUrl="images/diurno_26.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale27" ImageUrl="images/diurno_27.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale28" ImageUrl="images/diurno_28.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale29" ImageUrl="images/diurno_29.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale30" ImageUrl="images/diurno_30.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale31" ImageUrl="images/diurno_31.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale32" ImageUrl="images/diurno_32.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale33" ImageUrl="images/diurno_33.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale34" ImageUrl="images/diurno_34.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale35" ImageUrl="images/diurno_35.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale36" ImageUrl="images/diurno_36.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale37" ImageUrl="images/diurno_37.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale38" ImageUrl="images/diurno_38.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale39" ImageUrl="images/diurno_39.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale40" ImageUrl="images/diurno_40.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale41" ImageUrl="images/diurno_41.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale42" ImageUrl="images/diurno_42.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale43" ImageUrl="images/diurno_43.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale44" ImageUrl="images/diurno_44.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale45" ImageUrl="images/diurno_45.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale46" ImageUrl="images/diurno_46.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale47" ImageUrl="images/diurno_47.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale48" ImageUrl="images/diurno_48.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>


										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale49" ImageUrl="images/diurno_49.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale50" ImageUrl="images/diurno_50.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale51" ImageUrl="images/diurno_51.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale52" ImageUrl="images/diurno_52.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale53" ImageUrl="images/diurno_53.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale54" ImageUrl="images/diurno_54.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale55" ImageUrl="images/diurno_55.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale56" ImageUrl="images/diurno_56.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale57" ImageUrl="images/diurno_57.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale58" ImageUrl="images/diurno_58.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale59" ImageUrl="images/diurno_59.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale60" ImageUrl="images/diurno_60.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

										<tr>
											<td>
												<asp:ImageButton ID="Img_Causale61" ImageUrl="images/diurno_61.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale62" ImageUrl="images/diurno_62.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale63" ImageUrl="images/diurno_63.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale64" ImageUrl="images/diurno_64.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale65" ImageUrl="images/diurno_65.png" runat="server" Width="20px" Height="20px" /></td>
											<td>
												<asp:ImageButton ID="Img_Causale66" ImageUrl="images/diurno_66.png" runat="server" Width="20px" Height="20px" /></td>
										</tr>

									</table>
									<asp:Image ID="Img_ColoreDiurno" runat="server" Width="100px" Height="100px" BorderColor="Black" BorderWidth="1px" />
								</ContentTemplate>
							</xasp:TabPanel>
						</xasp:TabContainer>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
