﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_Menu_Contabile
    Inherits System.Web.UI.Page

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        'Try
        '    If Session("UTENTEIMPER").ToString <> "" Then
        '        Lbl_test.Text = Lbl_test.Text & "        <tr>"
        '        Lbl_test.Text = Lbl_test.Text & "    <td style=""text-align:center; vertical-align:top;""><a href=""push/CalcoloEmissioneStampaFatture.aspx"">EMISSIONE NOTIFICA (BETA)</a></td>"
        '        Lbl_test.Text = Lbl_test.Text & "    <td style=""text-align:center; vertical-align:top;""></td>"
        '        Lbl_test.Text = Lbl_test.Text & "     <td style=""text-align:center; width:20%;""></td>"
        '        Lbl_test.Text = Lbl_test.Text & "     <td style=""text-align:center; width:20%;""></td>"
        '        Lbl_test.Text = Lbl_test.Text & "     <td style=""text-align:center; width:20%;""></td>      "
        '        Lbl_test.Text = Lbl_test.Text & "    </tr> "
        '    End If
        'Catch ex As Exception

        'End Try


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
