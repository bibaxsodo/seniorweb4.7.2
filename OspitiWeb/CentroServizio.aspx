﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CentroServizio" EnableEventValidation="false" CodeFile="CentroServizio.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Centro Servizio</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });



        function DialogBoxW(Path) {
            var tot = 0;

            tot = document.body.offsetWidth - 100;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }

        function DialogBoxBig(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 185px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Centro Servizio</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Imb_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 185px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Centro Servizio              
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Codice :</label>
                                    <asp:TextBox ID="Txt_Codice" MaxLength="4" runat="server" Width="50px" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="352px" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Mastro :</label>
                                    <asp:TextBox ID="Txt_Mastro" onkeypress="return soloNumeri(event);" runat="server" Width="88px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto :</label>
                                    <asp:TextBox ID="Txt_Conto" onkeypress="return soloNumeri(event);" runat="server" Width="96px"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Tipo Centro Servizio :</label>
                                    <asp:RadioButton ID="Chk_Permanente" GroupName="TIPOCS" runat="server" />Permanente 
        <asp:RadioButton ID="Chk_CentroDiurno" GroupName="TIPOCS" runat="server" />Diurno 
        <asp:RadioButton ID="Chk_Domiciliare" GroupName="TIPOCS" runat="server" />Domiciliare<br />
                                    <br />

                                    <label class="LabelCampo">Giorni Settimana :</label>
                                    <asp:CheckBox ID="Chk_Lunedi" runat="server" Text="L" />
                                    <asp:CheckBox ID="Chk_Martedi" runat="server" Text="M" />
                                    <asp:CheckBox ID="Chk_Mercoledi" runat="server" Text="M" />
                                    <asp:CheckBox ID="Chk_Giovedi" runat="server" Text="G" />
                                    <asp:CheckBox ID="Chk_Venerdi" runat="server" Text="V" />
                                    <asp:CheckBox ID="Chk_Sabato" runat="server" Text="S" />
                                    <asp:CheckBox ID="Chk_Domenica" runat="server" Text="D" /><br />
                                    <br />


                                    <label class="LabelCampo">Non Emettere Documenti</label>
                                    <asp:CheckBox ID="Chk_NonEmettere" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Non Calcolare </label>
                                    <asp:CheckBox ID="Chk_Calcolare" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Non Emettere Doc. O/P</label>
                                    <asp:CheckBox ID="Chk_NonEmettereOP" runat="server" Text="" /><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                    <br />


                                    <table width="90%">
                                        <tr>
                                            <td width="60%">Regola</td>
                                            <td width="30%">Variabili</td>
                                        </tr>
                                        <tr>
                                            <td width="70%">
                                                <asp:TextBox ID="Txt_Regole" runat="server" Height="280px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                            </td>
                                            <td width="30%">
                                                <div id="idRegola" style="width: 100%; height: 280px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                    Cserv <i style="font-size: x-small;">CENTRO SERVIZIO CALCOLO</i><br />
                                                    CodiceOspite <i style="font-size: x-small;">CODICE OSPITE</i><br />
                                                    Anno  <i style="font-size: x-small;">ANNO CALCOLO</i><br />
                                                    Mese  <i style="font-size: x-small;">ANNO CALCOLO</i><br />
                                                    GiorniMese <i style="font-size: x-small;">GIORNI MESE</i><br />
                                                    TipoRetta  <i style="font-size: x-small;">TIPO RETTA OSPITE</i><br />
                                                    Modalita  <i style="font-size: x-small;">MODALITA CALCOLO</i><br />
                                                    Accolto <i style="font-size: x-small;">ACCOLTO NEL MESE = 1</i><br />
                                                    Dimesso <i style="font-size: x-small;">DIMESSO NEL MESE = 1</i><br />
                                                    QuotaOspite <i style="font-size: x-small;">QUOTA OSPITE</i><br />
                                                    QuotaParente()  <i style="font-size: x-small;">QUOTA PARENTE GIORNALIERA</i><br />
                                                    Stato <i style="font-size: x-small;">STATO AUTO</i><br />
                                                    ParentePagante  <i style="font-size: x-small;">PARENTE PAGANTE = 1</i><br />
                                                    ForzaRPXOspite    <i style="font-size: x-small;">FORZA IMPORTO ANTICIPATO OSPITE</i><br />
                                                    ForzaRPXParente() <i style="font-size: x-small;">FORZA IMPORTO ANTICIPATO PARENTE</i><br />
                                                    ImportoPresOspite     <i style="font-size: x-small;">IMPORTO PRES. OSPITE. PRIMO PERIODO</i><br />
                                                    ImportoAssOspite      <i style="font-size: x-small;">IMPORTO ASS. OSPITE. PRIMO PERIODO</i><br />
                                                    NonImportoPresOspite  <i style="font-size: x-small;">IMPORTO PRES. OSPITE. SECONDO PERIODO</i><br />
                                                    NonImportoAssOspite   <i style="font-size: x-small;">IMPORTO ASS. OSPITE. SECONDO PERIODO</i><br />
                                                    ImportoPresOspite2     <i style="font-size: x-small;">IMPORTO 2 PRES. OSPITE. PRIMO PERIODO</i><br />
                                                    ImportoAssOspite2      <i style="font-size: x-small;">IMPORTO 2 ASS. OSPITE. PRIMO PERIODO</i><br />
                                                    NonImportoPresOspite2  <i style="font-size: x-small;">IMPORTO 2 PRES. OSPITE. SECONDO PERIODO</i><br />
                                                    NonImportoAssOspite2   <i style="font-size: x-small;">IMPORTO 2 ASS. OSPITE. SECONDO PERIODO</i><br />
                                                    ImportoPresParente() <i style="font-size: x-small;">IMPORTO PRES. PARENT. PRIMO PERIODO</i><br />
                                                    ImportoAssParente()  <i style="font-size: x-small;">IMPORTO ASS. PARENT. PRIMO PERIODO</i><br />
                                                    NonImportoPresParente()  <i style="font-size: x-small;">NON IMPORTO PRES. PARENT. SECONDO PERIODO</i><br />
                                                    NonImportoAssParente()   <i style="font-size: x-small;">NON IMPORTO ASS. PARENT. SECONDO PERIODO</i><br />
                                                    ImportoPresParente2() <i style="font-size: x-small;">IMPORTO2 PRES. PARENT. PRIMO PERIODO</i><br />
                                                    ImportoAssParente2()  <i style="font-size: x-small;">IMPORTO2 ASS. PARENT. PRIMO PERIODO</i><br />
                                                    NonImportoPresParente2() <i style="font-size: x-small;">NON IMPORTO 2 PRES. PARENT. SECONDO PERIODO</i><br />
                                                    NonImportoAssParente2()  <i style="font-size: x-small;">NON IMPORTO 2 ASS. PARENT. SECONDO PERIODO</i><br />
                                                    ImportoPresComune() <i style="font-size: x-small;">IMPORTO PRES. COMUNE</i><br />
                                                    ImportoAssComune()   <i style="font-size: x-small;">IMPORTO ASS. COMUNE</i><br />
                                                    NonImportoPresComune()  <i style="font-size: x-small;">IMPORTO NON PRES. COMUNE</i><br />
                                                    NonImportoAssComune()   <i style="font-size: x-small;">IMPORTO NON ASS. COMUNE</i><br />
                                                    ImportoPresJolly() <i style="font-size: x-small;">IMPORTO PRES. JOLLY</i><br />
                                                    ImportoAssJolly()  <i style="font-size: x-small;">IMPORTO ASS. JOLLY</i><br />
                                                    NonImportoPresJolly()  <i style="font-size: x-small;">IMPORTO NON PRES. JOLLY</i><br />
                                                    NonImportoAssJolly()   <i style="font-size: x-small;">IMPORTO ASS. JOLLY</i><br />
                                                    ImportoPresRegione() <i style="font-size: x-small;">IMPORTO PRES. REGIONE</i><br />
                                                    ImportoAssRegione()  <i style="font-size: x-small;">IMPORTO ASS. REGIONE</i><br />
                                                    NonImportoPresRegione()  <i style="font-size: x-small;">IMPORTO NON PRES. REGIONE</i><br />
                                                    NonImportoAssRegione()   <i style="font-size: x-small;">IMPORTO NON ASS. REGIONE</i><br />
                                                    NonImportoPresEnte  <i style="font-size: x-small;">IMPORTO PRES. ENTE</i><br />
                                                    NonImportoAssEnte   <i style="font-size: x-small;">IMPORTO ASS. ENTE</i><br />
                                                    ImportoPresEnte     <i style="font-size: x-small;">IMPORTO PRES. ENTE</i><br />
                                                    ImportoAssEnte      <i style="font-size: x-small;">IMPORTO ASS. ENTE</i><br />
                                                    pXSalvaExtImporto1()  <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU PARENTI</i><br />
                                                    pXSalvaExtImporto2()  <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU PARENTI</i><br />
                                                    pXSalvaExtImporto3()  <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU PARENTI</i><br />
                                                    pXSalvaExtImporto4()  <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU PARENTI</i><br />
                                                    ImpExtrParMan1()   <i style="font-size: x-small;">IMPORTO 1 ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
                                                    ImpExtrParMan2()   <i style="font-size: x-small;">IMPORTO 2 ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
                                                    ImpExtrParMan3()   <i style="font-size: x-small;">IMPORTO 3 ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
                                                    ImpExtrParMan4()   <i style="font-size: x-small;">IMPORTO 4 ADDEBITO GESTITO IN CALCOLO SU  PARENTE</i><br />
                                                    OspiteMensile  <i style="font-size: x-small;">IMPORTO MENSILE OSPITE</i><br />
                                                    ParenteMensile() <i style="font-size: x-small;">IMPORTO PARENTI MENSILE</i><br />
                                                    ImportoMensileComune  <i style="font-size: x-small;">IMPORTO COMUNE MENSILE</i><br />
                                                    TotaleMensileRetta  <i style="font-size: x-small;">TOTALE RETTE MENSILE</i><br />
                                                    XSalvaExtImporto1  <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU OSPITE</i><br />
                                                    XSalvaExtImporto2  <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU OSPITE</i><br />
                                                    XSalvaExtImporto3  <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU OSPITE</i><br />
                                                    XSalvaExtImporto4  <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU OSPITE</i><br />
                                                    mGiorniPres <i style="font-size: x-small;">GIORNI SENZA CAUSALE</i><br />
                                                    mGiorniAss  <i style="font-size: x-small;">GIORNI CON CAUSALE</i><br />
                                                    GiorniPres <i style="font-size: x-small;">GIORNI PRESENZA OSPITE PRIMO PERIODO</i><br />
                                                    GiorniAss   <i style="font-size: x-small;">GIORNI ASSENZA OSPITE PRIMO PERIODO</i><br />
                                                    NonGiorniPres  <i style="font-size: x-small;">GIORNI PRESENZA OSPITE SECONDO PERIODO</i><br />
                                                    NonGiorniAss   <i style="font-size: x-small;">GIORNI ASSENZA OSPITE SECONDO PERIODO</i><br />
                                                    Variabile1  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    Variabile2  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    Variabile3  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    Variabile4  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    VariabileA  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    VariabileB  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    VariabileC  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    VariabileD  <i style="font-size: x-small;">VARIBILE INTERNA REGOLA LIBERA</i><br />
                                                    GiorniAssenza  <i style="font-size: x-small;">GIORNI DA ULTIMA PRESENZA FINE MESE</i><br />
                                                    GiorniRipetizioneCausale  <i style="font-size: x-small;">GIORNI DI RIPETIZIONE DELLA CAUSALE FINE MESE</i><br />
                                                    GiorniRipetizioneCausaleNC  <i style="font-size: x-small;">GIORNI DI RIPETIZIONE DELLA CAUSALE NON CONSIDERANDO <i>C</i><br />
                                                        FINE MESE</i><br />
                                                    UscitoPrimaMese <i style="font-size: x-small;">USCITA TEMPORANE PRIMA INIZIO MESE</i><br />
                                                    UscitoPenultimoGiorno  <i style="font-size: x-small;">USCITA TEMPORANE PENULTIMO GIORNO MESE</i><br />
                                                    CodiceComune()  <i style="font-size: x-small;">COMUNI ATTIVI NEL MESE</i><br />
                                                    CodiceJolly()   <i style="font-size: x-small;">JOLLY ATTIVI NEL MESE</i><br />
                                                    CodiceRegione() <i style="font-size: x-small;">REGIONE ATTIVI NEL MESE</i><br />
                                                    TipoExtrParMan1() <i style="font-size: x-small;">TIPO EXTRA PARENTE MANUALE 1</i><br />
                                                    TipoExtrParMan2() <i style="font-size: x-small;">TIPO EXTRA PARENTE MANUALE 2</i><br />
                                                    TipoExtrParMan3() <i style="font-size: x-small;">TIPO EXTRA PARENTE MANUALE 3</i><br />
                                                    TipoExtrParMan4() <i style="font-size: x-small;">TIPO EXTRA PARENTE MANUALE 3</i><br />
                                                    XSalvaExtImporto1C  <i style="font-size: x-small;">IMPORTO 1 PERSONALIZZABILI SU COMUNE</i><br />
                                                    XSalvaExtImporto2C  <i style="font-size: x-small;">IMPORTO 2 PERSONALIZZABILI SU COMUNE</i><br />
                                                    XSalvaExtImporto3C  <i style="font-size: x-small;">IMPORTO 3 PERSONALIZZABILI SU COMUNE</i><br />
                                                    XSalvaExtImporto4C  <i style="font-size: x-small;">IMPORTO 4 PERSONALIZZABILI SU COMUNE</i><br />
                                                    ImportoAddebitoPrimoComune <i style="font-size: x-small;">ADDEBITO COMUNE DA CALCOLO TIPO DA TIPO OPERAZIONE</i><br />
                                                    ImportoAddebitoPrimoJolly  <i style="font-size: x-small;">ADDEBITO JOLLY DA CALCOLO TIPO DA TIPO OPERAZIONE</i><br />
                                                    ImportoAddebitoPrimoRegione <i style="font-size: x-small;">ADDEBITO REGIONE DA CALCOLO</i><br />
                                                    TipoAddebitoPrimoRegione   <i style="font-size: x-small;">TIPO ADDEBITO REGIONE DA CALCOLO</i><br />
                                                    ImpExtrOspMan1 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    ImpExtrOspMan2 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    ImpExtrOspMan3 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    ImpExtrOspMan4 <i style="font-size: x-small;">IMPORTO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    TipoAddExtrOspMan1 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    TipoAddExtrOspMan2 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    TipoAddExtrOspMan3 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    TipoAddExtrOspMan4 <i style="font-size: x-small;">TIPO ADDEBITO GESTITO IN CALCOLO SU  OSPITE</i><br />
                                                    ImpExtraOspite()  <i style="font-size: x-small;">IMPORTO EXTRA OSPITE PRIMO PERIODO</i><br />
                                                    ImportoExtrComune()  <i style="font-size: x-small;">verificare</i><br />
                                                    ImportoExtrJolly()   <i style="font-size: x-small;">verificare</i><br />
                                                    ImportoExtrParent()  <i style="font-size: x-small;">verificare</i><br />
                                                    ImportoExtrOspite()  <i style="font-size: x-small;">verificare</i><br />
                                                    NonImpExtraOspite()  <i style="font-size: x-small;">verificare</i><br />

                                                </div>
                                            </td>
                                        </tr>
                                    </table>



                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Opzioni
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Personalizzazione</label>
                                    <asp:DropDownList ID="DD_Personalizzazione" runat="server"></asp:DropDownList>
                                    <br />
                                    <label class="LabelCampo">Calcolo Giorno :</label>
                                    <asp:RadioButton ID="Rb_DaParametri" GroupName="CalcoloGiorni" runat="server" />Da Parametri
            <asp:RadioButton ID="Rb_GiorniMese" GroupName="CalcoloGiorni" runat="server" />Mensile/Giorni Mese
            <asp:RadioButton ID="Rb_Diviso30" GroupName="CalcoloGiorni" runat="server" />Mensile/ 30
            <asp:RadioButton ID="Rb_MeseAnno" GroupName="CalcoloGiorni" runat="server" />Mensile * 12 / 365
            <asp:RadioButton ID="RB_Anno12" GroupName="CalcoloGiorni" runat="server" />Mensile/ (GG Anno / 12)
            <asp:RadioButton ID="RB_Anno7" GroupName="CalcoloGiorni" runat="server" />Mensile/ (GG Lun-Ver)
            <asp:RadioButton ID="Rb_Diviso34" GroupName="CalcoloGiorni" runat="server" />Mensile/ 30,40<br />
                                    <br />

                                    <br />
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_0101" Text="01/01 Primo Giorno Dell'Anno" runat="server" />

                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_1508" Text="15/08 Assunzione B.V. Maria" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_0601" Text="06/01 Giorno Dell'Epifania" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_0111" Text="01/11 Giorno di Ognissanti" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_1903" Text="19/03 Giorno Di San Giuseppe" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_0411" Text="04/11 Giorno Dell'unita Nazionale" runat="server" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_2504" Text="25/04 Aniversario Liberazione" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_0812" Text="08/12 Immacolata Concezione" runat="server" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_0105" Text="01/05 Festa Del Lavoro" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_2512" Text="25/12 Giorno di Natale" runat="server" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_0206" Text="02/06 Proclamazione Repubblica" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="Chk_2612" Text="26/12 Santo Stefano" runat="server" />
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_2906" Text="29/06 Apostoli Pietro e Paolo" runat="server" />
                                            </td>
                                            <td></td>
                                        </tr>

                                    </table>
                                    <br />
                                    <label class="LabelCampo">Lunedì Dopo Pasqua :</label>
                                    <asp:TextBox ID="Txt_LunediPasqua" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_LunediPasqua" Text="" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Santo Patrono :</label>
                                    <asp:TextBox ID="Txt_SantoPatrono" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_SantoPatrono" Text="" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">EPersonam:</label>
                                    <asp:TextBox ID="Txt_EPersonam" runat="server"></asp:TextBox>
                                    <asp:DropDownList ID="DD_EPersonam" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    <asp:Button ID="BtnEpersonam" runat="server" Text="Associa Nuclei" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">EPersonam Non:</label>
                                    <asp:TextBox ID="Txt_EPersonamNon" runat="server"></asp:TextBox>
                                    <asp:DropDownList ID="DD_EPersonamN" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    <asp:Button ID="BtnEpersonamN" runat="server" Text="Associa Nuclei" />
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Cod.Regione :</label>
                                    <asp:TextBox ID="Txt_CodiceRegione" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Cod. Asl:</label>
                                    <asp:TextBox ID="Txt_CodiceAsl" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Cod. SSa:</label>
                                    <asp:TextBox ID="Txt_CodiceStruttura" runat="server"></asp:TextBox>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Numero Cartella:</label>
                                    <asp:CheckBox ID="Chk_IncrementaNumeroCartella" runat="server" Text="Incrementa automatico per anno" /><br />
                                    <br />

                                    <label class="LabelCampo">Periodo Fatturazione : Anno : </label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server"></asp:TextBox>
                                    Mese : 
           <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
               <asp:ListItem Value="1">Gennaio</asp:ListItem>
               <asp:ListItem Value="2">Febbraio</asp:ListItem>
               <asp:ListItem Value="3">Marzo</asp:ListItem>
               <asp:ListItem Value="4">Aprile</asp:ListItem>
               <asp:ListItem Value="5">Maggio</asp:ListItem>
               <asp:ListItem Value="6">Giugno</asp:ListItem>
               <asp:ListItem Value="7">Luglio</asp:ListItem>
               <asp:ListItem Value="8">Agosto</asp:ListItem>
               <asp:ListItem Value="9">Settembre</asp:ListItem>
               <asp:ListItem Value="10">Ottobre</asp:ListItem>
               <asp:ListItem Value="11">Novembre</asp:ListItem>
               <asp:ListItem Value="12">Dicembre</asp:ListItem>
           </asp:DropDownList>

                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Rendiconto
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Presenze Tempo Pieno :</label>
                                    <asp:DropDownList ID="DD_PresenzeTP" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Assenze Tempo Pieno :</label>
                                    <asp:DropDownList ID="DD_AssenzeTP" runat="server"></asp:DropDownList>
                                    <asp:DropDownList ID="DD_AssenzeTP1" runat="server"></asp:DropDownList>
                                    <asp:DropDownList ID="DD_AssenzeTP2" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Presenze Part Time :</label>
                                    <asp:DropDownList ID="DD_PresenzaPT" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Assenze Part Time :</label>
                                    <asp:DropDownList ID="DD_AssenzaPT" runat="server"></asp:DropDownList>
                                    <asp:DropDownList ID="DD_AssenzaPT1" runat="server"></asp:DropDownList>
                                    <asp:DropDownList ID="DD_AssenzaPT2" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Centro Chiuso :</label>
                                    <asp:DropDownList ID="DD_Chiuso" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />



                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
