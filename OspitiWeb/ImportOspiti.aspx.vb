﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_ImportOspiti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim Token As String
        Dim rawresp As String

        Dim UltimoCodiceFiscale As String = ""

        If Chk_RicaricaDati.Checked = True Then
            Token = LoginPersonam(Context)
            If Token = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                Exit Sub
            End If
        End If
        Session("LETTOMOVIMENTI") = ""


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        'REM EPERSONAM NON RENDE GLI OSPITI DEL PRIMO GIORNO NEL MESE QUINDI PARTO DALL'ULTIMO GIORNO DEL MESE PRECEDENTE

        Dim DataFatt As String
        Dim MyMese As Integer = Param.MeseFatturazione
        Dim MyAnno As Integer = Param.AnnoFatturazione

        If Param.MeseFatturazione > 1 Then
            MyMese = MyMese - 1
        Else
            MyMese = 12
            MyAnno = MyAnno - 1
        End If

        If MyMese > 9 Then
            DataFatt = MyAnno & "-" & MyMese & "-" & GiorniMese(MyMese, MyAnno)
        Else
            DataFatt = MyAnno & "-0" & MyMese & "-" & GiorniMese(MyMese, MyAnno)
        End If



        Dim Indice As Integer = 0
        Dim NumeroRighe As Integer = 0


        Dim UrlConnessione As String = ""

        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("DataNascita", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("Cserv", GetType(String))
        Tabella.Columns.Add("DataAccoglimento", GetType(String))
        Tabella.Columns.Add("Sesso", GetType(String))
        Tabella.Columns.Add("ProvinciaNascita", GetType(String))
        Tabella.Columns.Add("ComuneNascita", GetType(String))
        Tabella.Columns.Add("Telefono", GetType(String))


        Tabella.Columns.Add("Residenza", GetType(String))
        Tabella.Columns.Add("Cap", GetType(String))
        Tabella.Columns.Add("ProvinciaResidenze", GetType(String))
        Tabella.Columns.Add("ComuneResidenze", GetType(String))
        Tabella.Columns.Add("Letto", GetType(String))
        Tabella.Columns.Add("id", GetType(String))

        Tabella.Columns.Add("Cellulare", GetType(String))
        Tabella.Columns.Add("Mail", GetType(String))
        Tabella.Columns.Add("Fax", GetType(String))


        Session("LETTOMOVIMENTI") = ""

        Dim BUCser As New Cls_CentroServizio
        Dim IDBusinessUnit As Integer = BusinessUnit(Token, Context)

        If Cmb_CServ.SelectedValue <> "" Then
            BUCser.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            BUCser.Leggi(Session("DC_OSPITE"), BUCser.CENTROSERVIZIO)
            IDBusinessUnit = BUCser.EPersonam
        End If

        If Cmb_CServ.SelectedValue = "BD" Then
            IDBusinessUnit = 994
        End If
        If Cmb_CServ.SelectedValue = "VB" Then
            IDBusinessUnit = 16
        End If
        If Cmb_CServ.SelectedValue = "BB" Then
            IDBusinessUnit = 1000
        End If
        If Cmb_CServ.SelectedValue = "CC" Then
            IDBusinessUnit = 995
        End If

        If Cmb_CServ.SelectedValue = "CP" Then
            IDBusinessUnit = 1002
        End If
        If Cmb_CServ.SelectedValue = "CDB" Then
            IDBusinessUnit = 1001
        End If
        If Cmb_CServ.SelectedValue = "H" Then
            IDBusinessUnit = 993
        End If


        '**** APPOGGIO CREA

        If Cmb_CServ.SelectedValue = "CC" Then
            IDBusinessUnit = 2032
        End If
        If Cmb_CServ.SelectedValue = "CG" Then
            IDBusinessUnit = 2033
        End If

        If Cmb_CServ.SelectedValue = "CP" Then
            IDBusinessUnit = 2031
        End If

        If Cmb_CServ.SelectedValue = "CA" Then
            IDBusinessUnit = 2034
        End If

        If Cmb_CServ.SelectedValue = "RSA" Then
            IDBusinessUnit = 2040
        End If

        If Cmb_CServ.SelectedValue = "CD" Then
            IDBusinessUnit = 2041
        End If
        '1001


        If DD_Struttura.SelectedItem.Text = "SAN MARTINO IN RIO" Then
            IDBusinessUnit = 1163
        End If
        If DD_Struttura.SelectedItem.Text = "ROLO" Then
            IDBusinessUnit = 1408
        End If
        If DD_Struttura.SelectedItem.Text = "RIO SALICETO" Then
            IDBusinessUnit = 1158
        End If
        If DD_Struttura.SelectedItem.Text = "FABBRICO" Then
            IDBusinessUnit = 1053
        End If
        If DD_Struttura.SelectedItem.Text = "CAMPAGNOLA EMILIA" Then
            IDBusinessUnit = 1153
        End If

        Try
            If Cmb_CServ.SelectedItem.Text = "CD COSTA ARGENTO" Then
                IDBusinessUnit = 1891
            End If
        Catch ex As Exception

        End Try
        


        'FABBRICO
        Indice = 1
        Do
            NumeroRighe = 0
            If Chk_RicaricaDati.Checked = True Then
                If Indice = 0 Then
                    UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & IDBusinessUnit & "/guests/updated/" & DataFatt & "?per_page=500"
                    REM Indice = Indice + 1
                Else
                    UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & IDBusinessUnit & "/guests/updated/" & DataFatt & "?per_page=500&page=" & Indice
                End If
            End If

            Indice = Indice + 1


            If Chk_RicaricaDati.Checked = True Then

                Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

                client.Method = "GET"
                client.Headers.Add("Authorization", "Bearer " & Token)
                client.ContentType = "Content-Type: application/json"

                Dim reader As StreamReader
                Dim response As HttpWebResponse = Nothing
                client.KeepAlive = True
                client.ReadWriteTimeout = 62000
                client.MaximumResponseHeadersLength = 262144
                client.Timeout = 62000

                response = DirectCast(client.GetResponse(), HttpWebResponse)

                reader = New StreamReader(response.GetResponseStream())



                rawresp = reader.ReadToEnd()





                Dim SalvaDb As New OleDbCommand

                SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = " & Indice - 1 & " And Periodo = ? And Tipo = 'O'"
                SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
                SalvaDb.Connection = cn

                Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
                If VerificaDB.Read Then
                    Dim UpDb As New OleDbCommand
                    UpDb.Connection = cn
                    UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'O'"
                    UpDb.Parameters.AddWithValue("@Dati", rawresp)
                    UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                    UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                    UpDb.Parameters.AddWithValue("@Pagina", Indice - 1)
                    UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                    UpDb.ExecuteNonQuery()
                Else
                    Dim UpDb As New OleDbCommand
                    UpDb.Connection = cn
                    UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo) values (?,?,?,?,?,'O') "
                    UpDb.Parameters.AddWithValue("@Dati", rawresp)
                    UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                    UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                    UpDb.Parameters.AddWithValue("@Pagina", Indice - 1)
                    UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                    UpDb.ExecuteNonQuery()
                End If
                VerificaDB.Close()
            Else
                Dim SalvaDb As New OleDbCommand

                SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = " & Indice - 1 & " And Periodo = ? And Tipo ='O'"
                SalvaDb.Connection = cn
                SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
                Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
                If VerificaDB.Read Then
                    rawresp = campodb(VerificaDB.Item("Dati"))
                End If
                VerificaDB.Close()
            End If



            Dim jResults As JArray = JArray.Parse(rawresp)

            For Each jTok As JToken In jResults
                Dim M As New ClsOspite
                M.CodiceOspite = 0
                M.CODICEFISCALE = jTok.Item("cf").ToString()

                NumeroRighe = NumeroRighe + 1
                If M.CODICEFISCALE = "CVLMTT48R17G187T" Then
                    M.CODICEFISCALE = "CVLMTT48R17G187T"
                End If

                M.LeggiPerCodiceFiscale(Session("DC_OSPITE"), M.CODICEFISCALE)

                If M.CodiceOspite = 0 And Session("UTENTE").ToString.ToUpper = "petrini riccardo".ToUpper Then
                    Dim DataAcco As String = Nothing
                    Dim Cserv As String = ""

                    Dim kEp As New Cls_Epersonam

                    kEp.VisuallizzaAccoglimentoOspite(Token, Session("DC_OSPITE"), Chk_RicaricaDati.Checked, HttpContext.Current, M.CODICEFISCALE, Cserv, DataAcco)
                    If Cserv = "GIU" Then
                        M.CodiceOspite = 1
                    End If
                End If
                If M.CodiceOspite = 0 And Session("UTENTE").ToString.ToUpper = "borri luciano".ToUpper Then
                    Dim DataAcco As String = Nothing
                    Dim Cserv As String = ""

                    Dim kEp As New Cls_Epersonam

                    kEp.VisuallizzaAccoglimentoOspite(Token, Session("DC_OSPITE"), Chk_RicaricaDati.Checked, HttpContext.Current, M.CODICEFISCALE, Cserv, DataAcco)

                    If Cserv = "FRA" Then
                        M.CodiceOspite = 1
                    End If
                End If

                'And Not IsNothing(jTok.Item("birthdate"))
                If M.CodiceOspite = 0 And UltimoCodiceFiscale <> jTok.Item("cf").ToString() Then

                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = jTok.Item("surname").ToString()
                    myriga(1) = jTok.Item("firstname").ToString()
                    If Not IsNothing(jTok.Item("birthdate")) Then
                        myriga(2) = campodb(jTok.Item("birthdate").ToString())
                    Else
                        myriga(2) = DateSerial(1900, 1, 1)
                    End If
                    If Not IsNothing(jTok.Item("cf")) Then
                        myriga(3) = jTok.Item("cf").ToString()
                    End If

                    UltimoCodiceFiscale = myriga(3)
                    Dim DataAcco As String = Nothing
                    Dim Cserv As String = ""

                    Dim kEp As New Cls_Epersonam

                    kEp.VisuallizzaAccoglimentoOspite(Token, Session("DC_OSPITE"), Chk_RicaricaDati.Checked, HttpContext.Current, M.CODICEFISCALE, Cserv, DataAcco)


                    myriga(4) = Cserv
                    myriga(5) = DataAcco

                    Try
                        If Val(jTok.Item("gender").ToString()) = 1 Then
                            myriga(6) = "M"
                        Else
                            myriga(6) = "F"
                        End If
                    Catch ex As Exception

                    End Try

                    Dim appoggio As String
                    Try
                        appoggio = jTok.Item("birthcity").ToString()

                        myriga(7) = Mid(appoggio & Space(6), 1, 3)
                        myriga(8) = Mid(appoggio & Space(6), 4, 3)
                    Catch ex As Exception

                    End Try

                    Try
                        myriga(9) = jTok.Item("phone").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        myriga(10) = jTok.Item("resaddress").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        myriga(11) = jTok.Item("rescap").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        appoggio = jTok.Item("rescity").ToString()

                        myriga(12) = Mid(appoggio & Space(6), 1, 3)
                        myriga(13) = Mid(appoggio & Space(6), 4, 3)

                    Catch ex As Exception

                    End Try



                    Try
                        myriga(14) = jTok.Item("bed").ToString()
                    Catch ex As Exception

                    End Try

                    myriga(15) = jTok.Item("id").ToString()


                    Try
                        myriga(16) = jTok.Item("mobile").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        myriga(17) = jTok.Item("email").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        myriga(18) = jTok.Item("fax").ToString()
                    Catch ex As Exception

                    End Try


                    Tabella.Rows.Add(myriga)
                End If
            Next




        Loop While NumeroRighe >= 1


        'If Cmb_CServ.SelectedValue <> "" Then
        For Riga = Tabella.Rows.Count - 1 To 0 Step -1
            If Tabella.Rows(Riga).Item(4).ToString = "" Then
                Tabella.Rows.RemoveAt(Riga)
            End If
        Next
        'End If

        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True



    End Sub




    Private Function LoginPersonam(ByVal context As HttpContext) As String

        Try


            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))


            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String

            If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
                request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

                guarda = context.Session("ChiaveCr")
            Else
                request.Add("username", context.Session("EPersonamUser"))

                guarda = context.Session("EPersonamPSWCRYPT")
            End If





            request.Add("code", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)
            REM Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Private Function ElencoNuclei(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As String
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler
        businessID = 2029


        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Return rawresp


    End Function


    Private Function ElencoSottoStrutture(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As Integer
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Try
            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("structures").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                    ElencoSottoStrutture = Val(jTok2.Item("id").ToString)
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try

    End Function

    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()


        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            Else

                REM Dim BU As Integer = Val(jTok2.Item("id").ToString)

                REM BusinessUnit = ElencoSottoStrutture(Token, context, BU)

                If context.Session("NomeEPersonam") = "PROVA" Then
                    BusinessUnit = 1825
                End If
            End If
        Next



    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)

            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(4).ToString
            If ViewState("IMPORTATO") = 1 Then
                Dim ChkImporta As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)


                ChkImporta.Enabled = False
            End If



        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer

        Dim Token As String
        Token = LoginPersonam(Context)

        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then

                Dim CodiceFiscale As String = Tabella.Rows(Riga).Item(3)
                Dim CentroServizio As String = DD_Cserv.SelectedValue

                Dim Cs As New Cls_CentroServizio

                Cs.CENTROSERVIZIO = CentroServizio

                Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)



                Dim Anag As New ClsOspite

                Anag.CognomeOspite = Tabella.Rows(Riga).Item(0)
                Anag.NomeOspite = Tabella.Rows(Riga).Item(1)
                Anag.Nome = Anag.CognomeOspite & " " & Anag.NomeOspite
                Anag.DataNascita = campodbd(Tabella.Rows(Riga).Item(2))
                Anag.UTENTE = Session("UTENTE")
                Anag.InserisciOspite(Session("DC_OSPITE"), Anag.CognomeOspite, Anag.NomeOspite, Anag.DataNascita)
                Anag.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite)

                Anag.CODICEFISCALE = CodiceFiscale



                Anag.Sesso = campodb(Tabella.Rows(Riga).Item(6))

                If campodb(Tabella.Rows(Riga).Item(7)) = "" Or campodb(Tabella.Rows(Riga).Item(8)) = "" Then

                Else
                    If Tabella.Rows(Riga).Item(7) <> "*" Then
                        Anag.ProvinciaDiNascita = campodb(Tabella.Rows(Riga).Item(7))
                    End If
                    If Tabella.Rows(Riga).Item(8) <> "*" Then
                        Anag.ComuneDiNascita = campodb(Tabella.Rows(Riga).Item(8))
                    End If
                End If


                'If NumeroDocumento <> "*" Then
                '    Anag.NumeroDocumento = NumeroDocumento
                'End If
                'If DataDocumento <> "*" Then
                '    If IsDate(DataDocumento) Then
                '        Anag.DataDocumento = DataDocumento
                '    End If
                'End If
                'If TipoDocumento <> "*" Then
                '    Anag.TipoDocumento = TipoDocumento
                'End If
                'If Emettitore <> "*" Then
                '    Anag.EmettitoreDocumento = Emettitore
                'End If
                'If Scadenza <> "*" Then
                '    If IsDate(Scadenza) Then
                '        Anag.ScadenzaDocumento = Scadenza
                '    End If
                'End If

                Anag.RESIDENZATELEFONO1 = campodb(Tabella.Rows(Riga).Item(9))

                'If Telefono2 <> "*" Then
                '    Anag.RESIDENZATELEFONO2 = Telefono2
                'End If

                'If DataDecesso <> "*" Then
                '    If IsDate(DataDecesso) Then
                '        Anag.DATAMORTE = DataDecesso
                '    End If
                'End If
                'If LuogoDecesso <> "*" Then
                '    Anag.LuogoMorte = LuogoDecesso
                'End If
                'If StrutturaProvenienza <> "*" Then
                '    Anag.StrutturaProvenienza = StrutturaProvenienza
                'End If
                'If AssistenteSociale <> "*" Then
                '    Anag.AssistenteSociale = AssistenteSociale
                'End If
                'If Distretto <> "*" Then
                '    Anag.Distretto = Distretto
                'End If
                'If CodiceMedico <> "*" Then
                '    Anag.CodiceMedico = CodiceMedico
                'End If
                'If codicesanitario <> "*" Then
                '    Anag.CodiceSanitario = codicesanitario
                'End If
                'If codiceesenzioneticket <> "*" Then
                '    Anag.CodiceTicket = codiceesenzioneticket
                'End If
                'If datadalesenzioneticket <> "*" Then
                '    If IsDate(datadalesenzioneticket) Then
                '        Anag.DataRilascio = datadalesenzioneticket
                '    End If
                'End If
                'If dataalesenzioneticket <> "*" Then
                '    If IsDate(dataalesenzioneticket) Then
                '        Anag.DataScadenza = dataalesenzioneticket
                '    End If
                'End If

                Anag.RESIDENZAINDIRIZZO1 = campodb(Tabella.Rows(Riga).Item(10))


                Anag.RESIDENZACAP1 = campodb(Tabella.Rows(Riga).Item(11))


                Anag.RESIDENZAPROVINCIA1 = campodb(Tabella.Rows(Riga).Item(12))


                Anag.RESIDENZACOMUNE1 = campodb(Tabella.Rows(Riga).Item(13))

                Anag.RESIDENZATELEFONO2 = campodb(Tabella.Rows(Riga).Item(16))
                Anag.RESIDENZATELEFONO3 = campodb(Tabella.Rows(Riga).Item(17))
                Anag.TELEFONO1 = campodb(Tabella.Rows(Riga).Item(18))



                Anag.ScriviOspite(Session("DC_OSPITE"))


                Try

                    'https://api-v0.e-personam.com/v0/guests/SLCNDR81C18A944O/relatives/4
                    Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & Anag.CODICEFISCALE & "/relatives")

                    client.Method = "GET"
                    client.Headers.Add("Authorization", "Bearer " & Token)
                    client.ContentType = "Content-Type: application/json"

                    Dim reader As StreamReader
                    Dim response As HttpWebResponse = Nothing

                    response = DirectCast(client.GetResponse(), HttpWebResponse)

                    reader = New StreamReader(response.GetResponseStream())
                    Dim rawresp As String


                    rawresp = reader.ReadToEnd()

                    Dim jResults As JArray = JArray.Parse(rawresp)

                    Dim Appoggio As String

                    For Each jTok2 As JToken In jResults

                        'If jTok2.Item("signed_to_pay") = "true" Then
                        Dim Prova As New Cls_Parenti

                        Prova.CodiceOspite = Anag.CodiceOspite
                        Prova.CodiceParente = 0
                        Prova.Nome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString
                        Try
                            Prova.Telefono1 = jTok2.Item("phone").ToString
                        Catch ex As Exception

                        End Try


                        Try
                            Prova.Telefono3 = jTok2.Item("email").ToString
                        Catch ex As Exception

                        End Try

                        Try
                            Prova.RESIDENZACAP1 = jTok2.Item("rescap").ToString()

                        Catch ex As Exception

                        End Try

                        Try
                            Appoggio = jTok2.Item("rescity_alpha6").ToString()

                            Prova.RESIDENZAPROVINCIA1 = Mid(Appoggio & Space(6), 1, 3)
                            Prova.RESIDENZACOMUNE1 = Mid(Appoggio & Space(6), 4, 3)
                        Catch ex As Exception

                        End Try

                        Try
                            Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
                        Catch ex As Exception

                        End Try

                        Try
                            Prova.RESIDENZAINDIRIZZO1 = jTok2.Item("resaddress").ToString()
                        Catch ex As Exception

                        End Try

                        Prova.IdEpersonam = Val(jTok2.Item("id").ToString())


                        'End If

                        Dim MyBill As String = ""


                        Try
                            MyBill = jTok2.Item("signed_to_receive_bill")
                        Catch ex As Exception

                        End Try
                        If MyBill = "true" Or MyBill = "True" Then
                            Prova.ParenteIndirizzo = 1
                        End If


                        Prova.ScriviParente(Session("DC_OSPITE"))

                        If MyBill = "true" Or MyBill = "True" Then
                            Anag.Leggi(Session("DC_OSPITE"), Anag.CodiceOspite)

                            Anag.RecapitoNome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString

                            Try
                                Appoggio = jTok2.Item("rescity_alpha6").ToString()

                                Anag.RecapitoProvincia = Mid(Appoggio & Space(6), 1, 3)
                                Anag.RecapitoComune = Mid(Appoggio & Space(6), 4, 3)
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.RecapitoIndirizzo = jTok2.Item("resaddress").ToString()
                            Catch ex As Exception

                            End Try


                            Try
                                Anag.TELEFONO4 = jTok2.Item("email").ToString()
                            Catch ex As Exception

                            End Try


                            Try
                                Anag.RESIDENZACAP4 = jTok2.Item("rescap").ToString()
                            Catch ex As Exception

                            End Try

                            Anag.ScriviOspite(Session("DC_OSPITE"))
                        End If
                    Next

                Catch ex As Exception

                End Try



                Dim Pc As New Cls_Pianodeiconti
                Pc.Mastro = Cs.MASTRO
                Pc.Conto = Cs.CONTO
                Pc.Sottoconto = Anag.CodiceOspite * 100
                Pc.Decodfica(Session("DC_GENERALE"))
                Pc.Mastro = Cs.MASTRO
                Pc.Conto = Cs.CONTO
                Pc.Sottoconto = Anag.CodiceOspite * 100
                Pc.Descrizione = Anag.Nome
                Pc.Tipo = "A"
                Pc.TipoAnagrafica = "O"
                Pc.Scrivi(Session("DC_GENERALE"))



                Dim cn As OleDbConnection
                Dim Trovato As Boolean = False

                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cn.Open()



                Dim TipoMovimentoSenior As String = ""
                Dim Progressivo As Long
                Dim MySql As String


                TipoMovimentoSenior = "05"

                Dim xtr As OleDbTransaction = cn.BeginTransaction()


                Dim MovLetto As New Cls_MovimentiStanze
                Dim Letto As String = campodb(Tabella.Rows(Riga).Item(14))

                If Trim(Letto) <> "" Then
                    MovLetto.CentroServizio = Cs.CENTROSERVIZIO
                    MovLetto.CodiceOspite = Anag.CodiceOspite
                    MovLetto.Data = Mid(Tabella.Rows(Riga).Item(5).ToString, 1, 10)
                    MovLetto.Tipologia = "OC"
                    MovLetto.Villa = "01"
                    If Mid(Letto, 1, 1) = "1" Then
                        MovLetto.Reparto = "01"
                    End If
                    If Mid(Letto, 1, 1) = "2" Then
                        MovLetto.Reparto = "02"
                    End If
                    If Mid(Letto, 1, 1) = "3" Then
                        MovLetto.Reparto = "03"
                    End If
                    If Mid(Letto, 1, 1) = "4" Then
                        MovLetto.Reparto = "04"
                    End If
                    MovLetto.Piano = ""
                    MovLetto.Stanza = ""
                    MovLetto.Letto = Letto
                    MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                End If


                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                cmdIns.Connection = cn
                cmdIns.Transaction = xtr
                cmdIns.Parameters.AddWithValue("@IdEpersonam", Val(campodb(Tabella.Rows(Riga).Item(14))))
                cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
                cmdIns.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
                cmdIns.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
                cmdIns.Parameters.AddWithValue("@Data", Mid(Tabella.Rows(Riga).Item(5).ToString, 1, 10))
                cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
                cmdIns.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
                cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                cmdIns.ExecuteNonQuery()

                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & Cs.CENTROSERVIZIO & "' And  CodiceOspite = " & Anag.CodiceOspite)
                cmd1.Connection = cn
                cmd1.Transaction = xtr
                Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                If myPOSTreader1.Read Then
                    If Not IsDBNull(myPOSTreader1.Item(0)) Then
                        Progressivo = myPOSTreader1.Item(0) + 1
                    Else
                        Progressivo = 1
                    End If
                Else
                    Progressivo = 1
                End If

                Dim MyData As Date = DateSerial(Val(Mid(Tabella.Rows(Riga).Item(5), 7, 4)), Val(Mid(Tabella.Rows(Riga).Item(5), 4, 2)), Val(Mid(Tabella.Rows(Riga).Item(5), 1, 2)))

                MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Transaction = xtr
                cmdw.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdw.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
                cmdw.Parameters.AddWithValue("@Data", MyData)
                cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
                cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
                cmdw.Parameters.AddWithValue("@CAUSALE", "")
                cmdw.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
                cmdw.Parameters.AddWithValue("@EPersonam", 1)
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()

                xtr.Commit()

                cn.Close()

            End If
        Next



        ViewState("IMPORTATO") = 1

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Movimenti Importati');", True)
    End Sub

    Protected Sub OspitiWeb_ImportOspiti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then



            Dim K1 As New Cls_SqlString


            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))

            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If


            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If

            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim SalvaDb As New OleDbCommand

            Chk_RicaricaDati.Checked = True
            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Periodo = ? And Tipo = 'O'"
            SalvaDb.Connection = cn
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim appoggio As Date = Nothing
                Try
                    appoggio = campodb(VerificaDB.Item("UltimaModifica"))
                Catch ex As Exception

                End Try
                If Format(appoggio, "yyyyMMdd") = Format(Now, "yyyyMMdd") Then
                    Chk_RicaricaDati.Checked = False
                End If
            End If
            VerificaDB.Close()

            cn.Close()

            Dim TOKEN As String

            Token = LoginPersonam(Context)



            Try
                Lbl_NomeOspite.Text = BusinessUnit(TOKEN, Context) & "-" & Session("NomeEPersonam")
            Catch ex As Exception

            End Try

        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Epersonam.aspx")
    End Sub


    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)
            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)

            If CheckBox.Enabled = True And DD_Cserv.SelectedValue <> "" Then
                REM If GridView1.Rows(Riga).Cells(6).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                CheckBox.Checked = True
                REM End If
            End If
        Next
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

End Class



