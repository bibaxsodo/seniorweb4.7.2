﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ImportDiurnoNew" CodeFile="ImportDiurnoNew.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Import E-Personam</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });

        function importmov() {
            $("#MovimentiImportati").html("");
            $.ajax({
                url: "Import_MovimentiEPersonam.ashx",
                success: function (data, stato) {


                    JsonDati = eval('(' + data + ')');
                    var indice = 0;
                    var myhtml = "";
                    do {
                        if (JsonDati[indice].Nome != null) {
                            var tipo = "";

                            if (JsonDati[indice].Tipo == '05') {
                                tipo = "Accoglimento";
                            }
                            if (JsonDati[indice].Tipo == '13') {
                                tipo = "Uscita Definitiva";
                            }
                            if (JsonDati[indice].Tipo == '03') {
                                tipo = "Uscita Temporanea";
                            }
                            if (JsonDati[indice].Tipo == '04') {
                                tipo = "Entrata";
                            }

                            var opendialog = "DialogBox('Anagrafica.aspx?CodiceOspite=" + JsonDati[indice].CodiceOspite + "&CentroServizio=" + JsonDati[indice].CentroServizio + "');"
                            if (JsonDati[indice].Esito == 'I') {
                                myhtml = myhtml + 'Entrate/Uscite :  <b><a href="#" onclick="' + opendialog + '">' + JsonDati[indice].Nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</b><br/>';
                            }
                            else {
                                myhtml = myhtml + 'Entrate/Uscite : <font color=orange><a href="#" onclick="' + opendialog + '">' + JsonDati[indice].Nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</font><br/>';
                            }
                        }
                        indice++;
                    } while ((JsonDati[indice] != null) || indice > 100)

                    $("#MovimentiImportati").append(myhtml);

                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });

        }

        function ImportDiurno() {
            $.ajax({
                url: "Import_DirunoEPersonam.ashx",
                success: function (data, stato) {
                    alert(data);

                    JsonDati = eval('(' + data + ')');
                    var indice = 0;
                    var myhtml = "";
                    do {
                        var nome = "";

                        try {
                            nome = JsonDati[indice].Nome;
                        }
                        catch (e) {
                            nome = "vuoto";
                        }

                        if ((nome != "vuoto") && (nome != '')) {
                            var tipo = "";


                            tipo = JsonDati[indice].Tipo;




                            var opendialog = "DialogBox('Anagrafica.aspx?CodiceOspite=" + JsonDati[indice].CodiceOspite + "&CentroServizio=" + JsonDati[indice].CentroServizio + "');"

                            if (JsonDati[indice].Esito == 'I') {
                                myhtml = myhtml + 'Diruno : <b><a href="#" onclick="' + opendialog + '">' + nome + '</a> ' + nome + " " + JsonDati[indice].Data + ' ' + tipo + '</b><br/>';
                            }
                            if (JsonDati[indice].Esito == 'E') {
                                myhtml = myhtml + 'Diruno :  <font color=red><a href="#" onclick="' + opendialog + '">' + nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</font><br/>';
                            }
                            if (JsonDati[indice].Esito == 'N') {
                                myhtml = myhtml + 'Diruno :  <font color=orange><a href="#" onclick="' + opendialog + '">' + nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</font><br/>';
                            }
                        }
                        indice++;
                    } while ((JsonDati[indice] != null) && indice < 1000)

                    $("#MovimentiImportati").append(myhtml);

                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }


        function ImportDomiciliare() {
            $.ajax({
                url: "Import_DomiciliareEPersonam.ashx",
                success: function (data, stato) {

                    alert(data);
                    JsonDati = eval('(' + data + ')');
                    var indice = 0;
                    var myhtml = "";
                    do {
                        if (JsonDati[indice].Nome != null) {
                            var tipo = "";

                            tipo = JsonDati[indice].Tipo;


                            var opendialog = "DialogBox('Anagrafica.aspx?CodiceOspite=" + JsonDati[indice].CodiceOspite + "&CentroServizio=" + JsonDati[indice].CentroServizio + "');"

                            if (JsonDati[indice].Esito == 'I') {
                                myhtml = myhtml + 'Diruno : <b><a href="#" onclick="' + opendialog + '">' + JsonDati[indice].Nome + '</a> ' + JsonDati[indice].Nome + " " + JsonDati[indice].Data + ' ' + tipo + '</b><br/>';
                            }
                            if (JsonDati[indice].Esito == 'E') {
                                myhtml = myhtml + 'Diruno :  <font color=red><a href="#" onclick="' + opendialog + '">' + JsonDati[indice].Nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</font><br/>';
                            }
                            if (JsonDati[indice].Esito == 'N') {
                                myhtml = myhtml + 'Diruno :  <font color=orange><a href="#" onclick="' + opendialog + '">' + JsonDati[indice].Nome + '</a> ' + JsonDati[indice].Data + ' ' + tipo + '</font><br/>';
                            }
                        }
                        indice++;
                    } while ((JsonDati[indice] != null) || indice > 100)

                    $("#MovimentiImportati").append(myhtml);

                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Import Diurno ePersonam</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto
                            <asp:Label ID="Lbl_Utente" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                            <asp:ImageButton ID="Btn_Movimenti" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />

                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Import ePersonam</HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Centro Servizio :</label>
                                    <asp:DropDownList ID="Cmb_CServ" runat="server" AutoPostBack="true" Width="358px"></asp:DropDownList><br />
                                    <br />
                                    <br />
                                    <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona Tutto" />
                                    <br />
                                    <br />
                                    <asp:CheckBox ID="Chk_SoloNonAuto" runat="server" Text="Non accreditati" Visible="false" /><br />
                                    <br />
                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Importa">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice Fiscale" />
                                            <asp:TemplateField HeaderText="Centro Servizio">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Cserv" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="G1" HeaderText="1" />
                                            <asp:BoundField DataField="G2" HeaderText="2" />
                                            <asp:BoundField DataField="G3" HeaderText="3" />
                                            <asp:BoundField DataField="G4" HeaderText="4" />
                                            <asp:BoundField DataField="G5" HeaderText="5" />
                                            <asp:BoundField DataField="G6" HeaderText="6" />
                                            <asp:BoundField DataField="G7" HeaderText="7" />
                                            <asp:BoundField DataField="G8" HeaderText="8" />
                                            <asp:BoundField DataField="G9" HeaderText="9" />
                                            <asp:BoundField DataField="G10" HeaderText="10" />
                                            <asp:BoundField DataField="G11" HeaderText="11" />
                                            <asp:BoundField DataField="G12" HeaderText="12" />
                                            <asp:BoundField DataField="G13" HeaderText="13" />
                                            <asp:BoundField DataField="G14" HeaderText="14" />
                                            <asp:BoundField DataField="G15" HeaderText="15" />
                                            <asp:BoundField DataField="G16" HeaderText="16" />
                                            <asp:BoundField DataField="G17" HeaderText="17" />
                                            <asp:BoundField DataField="G18" HeaderText="18" />
                                            <asp:BoundField DataField="G19" HeaderText="19" />
                                            <asp:BoundField DataField="G20" HeaderText="20" />
                                            <asp:BoundField DataField="G21" HeaderText="21" />
                                            <asp:BoundField DataField="G22" HeaderText="22" />
                                            <asp:BoundField DataField="G23" HeaderText="23" />
                                            <asp:BoundField DataField="G24" HeaderText="24" />
                                            <asp:BoundField DataField="G25" HeaderText="25" />
                                            <asp:BoundField DataField="G26" HeaderText="26" />
                                            <asp:BoundField DataField="G27" HeaderText="27" />
                                            <asp:BoundField DataField="G28" HeaderText="28" />
                                            <asp:BoundField DataField="G29" HeaderText="29" />
                                            <asp:BoundField DataField="G30" HeaderText="30" />
                                            <asp:BoundField DataField="G31" HeaderText="31" />
                                            <asp:BoundField DataField="Segnalazione" HeaderText="Segnalazione" />
                                            <asp:BoundField DataField="CodiceOspite" HeaderText="CodiceOspite" />
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>
                                    <ul>
                                        <li>
                                            <font color="Aqua">*</font>Movimento già importato in Senior
                                        </li>
                                        <li>
                                            <font color="orange">*</font>Ospiti non presenti nello stesso centro servizio/nucleo di ePersonam
                                        </li>
                                        <li>
                                            <font color="red">*</font>Ospiti non presenti in Senior
                                        </li>
                                    </ul>
                                    <div id="MovimentiImportati"></div>
                                </ContentTemplate>
                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
