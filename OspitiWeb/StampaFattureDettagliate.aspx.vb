﻿
Partial Class StampaFattureDettagliate
    Inherits System.Web.UI.Page

   

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function


    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select
    End Sub


    Public Function QuoteGiornaliere(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Tipo As String, ByVal CodPar As Long, Optional ByVal DataImporti As Date = #1/1/1971#) As Double
        Dim Rs_Myrec As New ADODB.Recordset
        Dim Rs_Ospite As New ADODB.Recordset
        Dim Rs_Regioni As New ADODB.Recordset
        Dim CodiciParente(100) As Boolean
        Dim MySql As String
        Dim Data As Date
        Dim i As Integer
        Dim Modalita As String = ""
        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection
        Dim ImpRetta As Double
        Dim ImpComune As Double
        Dim ImpJolly As Double
        Dim ImpOspite As Double
        Dim ImportoRegione As Double
        Dim AutoSufficente As String
        Dim MiaRegione As String = ""
        Dim ImpParenti As Double
        Dim ImpMia As Double
        Dim ImpEnte As Double



        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDb.Open(Session("DC_GENERALE"))


        Data = DataImporti

        For i = 0 To 100 : CodiciParente(i) = False : Next i

        If Format(DataImporti, "ddMMyyyy") = 1011971 Then
            Data = Now
        Else
            Data = DataImporti
        End If


        MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            Modalita = MoveFromDbWC(Rs_Myrec, "Modalita")
        End If
        Rs_Myrec.Close()

        MySql = "Select * From ImportoRetta Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'} Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "M" Then
                ImpRetta = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "A" Then
                ImpRetta = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "G" Or IsDBNull(Rs_Myrec.Fields("TipoImporto")) Then
                ImpRetta = MoveFromDbWC(Rs_Myrec, "Importo")
            End If
        End If

        Rs_Myrec.Close()

        MySql = "Select * From ImportoComune Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "M" Then
                ImpComune = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "A" Then
                ImpComune = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "G" Or MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "" Then
                ImpComune = MoveFromDbWC(Rs_Myrec, "Importo")
            End If
        End If
        Rs_Myrec.Close()


        MySql = "Select * From ImportoJolly Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "M" Then
                ImpJolly = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "A" Then
                ImpJolly = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "G" Or MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "" Then
                ImpJolly = MoveFromDbWC(Rs_Myrec, "Importo")
            End If
        End If
        Rs_Myrec.Close()


        MySql = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "M" Then
                ImpOspite = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "A" Then
                ImpOspite = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "G" Or MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "" Then
                ImpOspite = MoveFromDbWC(Rs_Myrec, "Importo")
            End If
        End If
        Rs_Myrec.Close()

        MySql = "Select * From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            MiaRegione = MoveFromDbWC(Rs_Myrec, "USL")
            AutoSufficente = MoveFromDbWC(Rs_Myrec, "STATOAUTO")
        End If
        Rs_Myrec.Close()

        MySql = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data"
        Rs_Regioni.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Regioni.EOF Then
            Rs_Regioni.MoveLast()
            ImportoRegione = MoveFromDbWC(Rs_Regioni, "Importo")
        End If
        Rs_Regioni.Close()

        MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        ImpParenti = 0
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
        End If
        Do While Not Rs_Myrec.EOF
            ImpMia = 0
            If Not CodiciParente(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) Then
                CodiciParente(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) = True
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "M" Then
                    ImpMia = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
                End If
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "A" Then
                    ImpMia = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
                End If
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "G" Or MoveFromDbWC(Rs_Myrec, "TipoImporto") = "" Then
                    ImpMia = MoveFromDbWC(Rs_Myrec, "Importo")
                End If
                ImpParenti = ImpParenti + ImpMia
            End If
            Rs_Myrec.MoveNext()
        Loop
        Rs_Myrec.Close()

        If Modalita = "O" Then
            ImpOspite = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly
        End If
        If Modalita = "E" Then
            ImpEnte = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpOspite - ImpJolly
        End If
        If Modalita = "C" Then
            ImpComune = ImpRetta - ImportoRegione - ImpParenti - ImpOspite - ImpJolly
        End If
        If Modalita = "P" Then
            ImpParenti = ImpRetta - ImpComune - ImportoRegione - ImpOspite - ImpJolly
        End If
        If Tipo = "O" Then
            QuoteGiornaliere = ImpOspite
        End If
        If Tipo = "C" Then
            QuoteGiornaliere = ImpComune
        End If
        If Tipo = "R" Then
            QuoteGiornaliere = ImportoRegione
        End If
        If Tipo = "E" Then
            QuoteGiornaliere = ImpEnte
        End If
        If Tipo = "P" Then
            If CodPar = 0 Then
                QuoteGiornaliere = ImpParenti
            Else
                MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  And CodiceParente = " & CodPar & " Order by Data Desc"
                Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not Rs_Myrec.EOF Then
                    If IsDBNull(Rs_Myrec.Fields("Percentuale")) Then 'Or Rs_Myrec!Percentuale = 0
                        QuoteGiornaliere = ImpParenti
                    Else
                        If MoveFromDbWC(Rs_Myrec, "Importo") > 0 Then
                            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "M" Then
                                QuoteGiornaliere = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data))
                            End If
                            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "A" Then
                                QuoteGiornaliere = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
                            End If
                            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "G" Or MoveFromDbWC(Rs_Myrec, "TipoImporto") = "" Then
                                QuoteGiornaliere = MoveFromDbWC(Rs_Myrec, "Importo")
                            End If
                        Else
                            QuoteGiornaliere = ImpParenti * MoveFromDbWC(Rs_Myrec, "Percentuale")
                        End If
                    End If
                End If
                Rs_Myrec.Close()
            End If
        End If
        QuoteGiornaliere = Math.Round(QuoteGiornaliere, 2)

    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function Mensile(ByVal Importo As Double, ByVal Mese As Byte, ByVal Anno As Integer) As Double
        Dim Para As New Cls_Parametri

        Para.LeggiParametri(Session("DC_OSPITE"))

        If Para.ImportoMensile = 0 Then
            If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
               Mese = 10 Or Mese = 12 Then
                Mensile = Math.Round(Importo / 31, 10) 'DecEuro)
            Else
                If Mese <> 2 Then
                    Mensile = Math.Round(Importo / 30, 10) ' DecEuro)
                Else
                    If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                        Mensile = Math.Round(Importo / 29, 10) 'DecEuro)
                    Else
                        Mensile = Math.Round(Importo / 28, 10) 'DecEuro)
                    End If
                End If
            End If
        End If
        If Para.ImportoMensile = 30 Then
            Mensile = Math.Round(Importo / 30, 10)  'DecEuro)
        End If
        If Para.ImportoMensile = 12 Then
            Mensile = Math.Round(Importo * 12 / 365, 2)  'DecEuro)
        End If
        If Para.ImportoMensile = 365 Then
            If Day(DateSerial(Anno, 2, 29)) = 29 Then
                Mensile = Math.Round(Importo / (366 / 12), 2)   'DecEuro)
            Else
                Mensile = Math.Round(Importo / (365 / 12), 2)  'DecEuro)
            End If
        End If
    End Function

    Public Function Annuale(ByVal Importo As Double, ByVal Anno As Integer) As Double
        If Day(DateSerial(Anno, 2, 29)) = 29 Then
            Annuale = Math.Round(Importo / 366, 2)
        Else
            Annuale = Math.Round(Importo / 365, 2)
        End If
    End Function

    Protected Sub StampaSociale()
        Lbl_Errore.Text = ""
        If Val(Txt_Anno.Text) = 0 Then
            Lbl_Errore.Text = "Digitare anno"
            Exit Sub
        End If





        Dim MyRs As New ADODB.Recordset
        Dim LEGGIrs As New ADODB.Recordset

        Dim RsDocumenti As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection


        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        Dim Anno As Integer
        Dim Mese As Integer
        Dim TestMeseAnno As String
        Dim MySql As String
        Dim DESMOTIVO As String
        Dim TotImportoAssenza As Double
        Dim TotImportoAss As Double
        Dim TotGiorniAss As Double
        Dim TotGiorniPre As Double
        Dim ImportoAssenzaRegione As Double
        Dim ImportoAssenzaOspite As Double
        Dim ImportoAssenzaParente As Double
        Dim TotImportoOspite As Double
        Dim TotImportoParenti As Double
        Dim RpxTotImportoOspite As Double
        Dim TotImporto As Double
        Dim RpxTotImportoParenti As Double
        Dim TotImportoRegione As Double
        Dim Quota As Double
        Dim DATAUSCITA As Date
        Dim Uscita As Date
        Dim ENTRATA As Date
        Dim USCITADEFINITIVA As Date
        Dim MOTIVO As String = ""
        Dim IMPEGNATIVA As String
        Dim accoglimento As Date
        Dim NumeroRegistrazione As Long

        Dim Cserv As String = ""
        Dim Provincia As String = ""
        Dim Comune As String = ""
        Dim TestMeseAnnoRpx As String = ""

        Dim Stampa As New StampeOspiti




        Anno = Txt_Anno.Text
        Mese = dd_mese.SelectedIndex + 1

        TestMeseAnno = " Mese = " & Mese & "  And Anno = " & Anno
        If Mese = 1 Then
            TestMeseAnnoRpx = " Mese = 12  And Anno = " & Anno - 1
        Else
            TestMeseAnnoRpx = " Mese = " & Mese - 1 & "  And Anno = " & Anno
        End If
        'StampeDb.Open(Session("STAMPEFINANZIARIA"))
        'StampeDb.Execute("Delete From RendicontoComuneMilano")

        'WrRs.Open("Select * From RendicontoComuneMilano", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)



        MySql = "SELECT RETTECOMUNE.CENTROSERVIZIO,RETTECOMUNE.CODICEOSPITE,RETTECOMUNE.PROVINCIA,RETTECOMUNE.COMUNE From RETTECOMUNE WHERE (((RETTECOMUNE.ELEMENTO)='RGP') OR ((RETTECOMUNE.ELEMENTO)='RGA') OR ((RETTECOMUNE.ELEMENTO)='ACC')) And " & TestMeseAnno & " GROUP BY RETTECOMUNE.CENTROSERVIZIO,RETTECOMUNE.CODICEOSPITE,RETTECOMUNE.PROVINCIA,RETTECOMUNE.COMUNE"

        MyRs.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        Do While Not MyRs.EOF
            ID = MoveFromDbWC(MyRs, "CodiceOspite")
            DESMOTIVO = ""
            TotImportoAssenza = 0
            TotImportoAss = 0

            Dim MyOspiti As New ClsOspite

            MyOspiti.Leggi(Session("DC_OSPITE"), ID)

            Application("ELABORANDO") = MyOspiti.Nome


            TotGiorniAss = 0
            LEGGIrs.Open("Select sum(Giorni) as t From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotGiorniAss = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()

            TotGiorniPre = 0
            LEGGIrs.Open("Select sum(Giorni) as t From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGP' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotGiorniPre = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()

            If Val(TotGiorniPre) = 0 And Val(TotGiorniAss) = 0 Then
                LEGGIrs.Open("Select sum(Giorni) From RETTEREGIONE WHERE  CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGP' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                        TotGiorniPre = LEGGIrs.Fields(0).Value
                    End If
                End If
                LEGGIrs.Close()

                LEGGIrs.Open("Select sum(Giorni) From RETTEREGIONE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                        TotGiorniAss = LEGGIrs.Fields(0).Value
                    End If
                End If
                LEGGIrs.Close()

                If Val(TotGiorniPre) = 0 And Val(TotGiorniAss) = 0 Then
                    TotGiorniPre = 0
                    LEGGIrs.Open("Select sum(Giorni) From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RPX' And " & Replace(Replace(TestMeseAnno, "Mese", "MeseCompetenza"), "Anno", "AnnoCompetenza"), OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not LEGGIrs.EOF Then
                        If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                            TotGiorniPre = LEGGIrs.Fields(0).Value
                        End If
                    End If
                    LEGGIrs.Close()

                    LEGGIrs.Open("Select sum(Giorni) From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGP' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not LEGGIrs.EOF Then
                        If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                            TotGiorniPre = TotGiorniPre + LEGGIrs.Fields(0).Value
                        End If
                    End If
                    LEGGIrs.Close()

                    LEGGIrs.Open("Select sum(Giorni) From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not LEGGIrs.EOF Then
                        If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                            TotGiorniAss = LEGGIrs.Fields(0).Value
                        End If
                    End If
                    LEGGIrs.Close()

                End If
            End If

            LEGGIrs.Open("Select sum(IMPORTO) as TOTALE From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND  CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGP'  And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                If IsDBNull(LEGGIrs.Fields("TOTALE")) Then
                    TotImporto = 0
                Else
                    TotImporto = MoveFromDbWC(LEGGIrs, "TOTALE")
                End If
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("Select sum(IMPORTO) as t From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoAss = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()



            ImportoAssenzaRegione = 0
            LEGGIrs.Open("Select sum(IMPORTO) as t From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoAssenza = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
                ImportoAssenzaRegione = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()



            ImportoAssenzaOspite = 0
            LEGGIrs.Open("Select sum(IMPORTO) as t From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoAssenza = TotImportoAssenza + Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
                ImportoAssenzaOspite = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()

            ImportoAssenzaParente = 0
            LEGGIrs.Open("Select sum(IMPORTO) as t From RETTEPARENTE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoAssenza = TotImportoAssenza + Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
                ImportoAssenzaParente = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()


            LEGGIrs.Open("Select sum(IMPORTO) AS T From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'RGP' OR Elemento = 'RGA') And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoOspite = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("Select sum(IMPORTO) AS T From RETTEPARENTE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'RGP' OR Elemento = 'RGA') And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoParenti = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()


            LEGGIrs.Open("Select sum(IMPORTO) AS T  From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'RPX') And " & TestMeseAnnoRpx, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                RpxTotImportoOspite = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()
            If RpxTotImportoOspite > 0 Then
                'If Chk_IncludiAccrediti.Value = 1 Then
                LEGGIrs.Open("Select sum(IMPORTO) as t From RETTEOSPITE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And Elemento = 'ACC'  And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                        RpxTotImportoOspite = RpxTotImportoOspite - MoveFromDbWC(LEGGIrs, "t")
                    End If
                    'If LEGGIrs.Fields(0).Value > 0 Then
                    '    RpxTotImportoOspite = 0
                    ' End If
                End If
                LEGGIrs.Close()
                'End If
            End If

            If ID = 135 Then

                ID = 135
            End If



            LEGGIrs.Open("Select sum(IMPORTO) AS T  From RETTEPARENTE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'RPX' ) And " & TestMeseAnnoRpx, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                RpxTotImportoParenti = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()
            If RpxTotImportoParenti > 0 Then
                LEGGIrs.Open("Select sum(IMPORTO) as t From RETTEPARENTE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'ACC' ) And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    If Not IsDBNull(LEGGIrs.Fields(0).Value) Then
                        RpxTotImportoParenti = RpxTotImportoParenti - MoveFromDbWC(LEGGIrs, "t")
                    End If

                End If
                LEGGIrs.Close()
            End If

            If Chk_NonAddebiti.Checked = False Then
                LEGGIrs.Open("Select sum(IMPORTO) AS T From ADDACR WHERE [RIFERIMENTO] = 'O' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And TIPOMOV = 'AD' And [MESECOMPETENZA] = " & Val(Mese) & " And [ANNOCOMPETENZA] = " & Val(Anno), OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    TotImportoOspite = TotImportoOspite + MoveFromDbWC(LEGGIrs, "t")
                    RpxTotImportoOspite = RpxTotImportoOspite + MoveFromDbWC(LEGGIrs, "t")
                End If
                LEGGIrs.Close()

                LEGGIrs.Open("Select sum(IMPORTO) AS T From ADDACR WHERE [RIFERIMENTO] = 'O' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And TIPOMOV = 'AC' And MESECOMPETENZA = " & Val(Mese) & " And [ANNOCOMPETENZA] = " & Val(Anno), OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not LEGGIrs.EOF Then
                    TotImportoOspite = TotImportoOspite - MoveFromDbWC(LEGGIrs, "t")
                    RpxTotImportoOspite = RpxTotImportoOspite - MoveFromDbWC(LEGGIrs, "t")
                End If
                LEGGIrs.Close()
            End If

            If RpxTotImportoOspite > 0 Then
                TotImportoOspite = RpxTotImportoOspite
            End If
            If RpxTotImportoParenti > 0 Then
                TotImportoParenti = RpxTotImportoParenti
            End If

            LEGGIrs.Open("Select sum(IMPORTO) AS T From RETTEREGIONE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And (Elemento = 'RGP' OR Elemento = 'RGA') And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImportoRegione = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()



            Quota = 73000
            Quota = QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "R", 0, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '13' ")
            If Not LEGGIrs.EOF Then
                USCITADEFINITIVA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()




            DATAUSCITA = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TipoMov = '03' ORDER BY DATA DESC")
            If Not LEGGIrs.EOF Then
                DATAUSCITA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()

            Uscita = DateSerial(1899, 12, 30)

            ENTRATA = DateSerial(1899, 12, 30)
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >=  {ts '" & Format(DATAUSCITA, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA")
            Do While Not LEGGIrs.EOF
                If MoveFromDbWC(LEGGIrs, "TipoMov") = "03" And (MoveFromDbWC(LEGGIrs, "Causale") <> "ZZ" And MoveFromDbWC(LEGGIrs, "Causale") <> "99" And MoveFromDbWC(LEGGIrs, "Causale") <> "ZA") Then
                    If Format(MoveFromDbWC(LEGGIrs, "Data"), "yyyymmdd") > Format(DateSerial(Anno, Mese - 2, 1), "yyyymmdd") Then
                        Uscita = MoveFromDbWC(LEGGIrs, "Data")
                        MOTIVO = MoveFromDbWC(LEGGIrs, "Causale")
                    End If
                    If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
                    If Not LEGGIrs.EOF Then
                        If MoveFromDbWC(LEGGIrs, "TipoMov") = "04" Then
                            ENTRATA = MoveFromDbWC(LEGGIrs, "Data")
                        End If
                    End If
                End If
                If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
            Loop
            LEGGIrs.Close()

            LEGGIrs.Open("SELECT * FROM CAUSALI WHERE CODICE = '" & MOTIVO & "'")
            If Not LEGGIrs.EOF Then
                DESMOTIVO = MoveFromDbWC(LEGGIrs, "Descrizione")
            End If
            LEGGIrs.Close()


            USCITADEFINITIVA = DateSerial(1989, 12, 30)
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '13' ", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                USCITADEFINITIVA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '03' And CAUSALE = 'ZA'", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                USCITADEFINITIVA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()


            accoglimento = DateSerial(1989, 12, 30)
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '05' ", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                accoglimento = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}   And (TIPOMOV = '03' or  TIPOMOV = '04') ORDER BY DATA", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not LEGGIrs.EOF
                If MoveFromDbWC(LEGGIrs, "TipoMov") = "03" And MoveFromDbWC(LEGGIrs, "Causale") = "ZZ" Then
                    If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
                    If Not LEGGIrs.EOF Then
                        accoglimento = MoveFromDbWC(LEGGIrs, "Data")
                        Exit Do
                    End If
                End If
                If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
            Loop
            LEGGIrs.Close()

            Quota = 80000


            Uscita = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
            If Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "YYYYMMDD") > Format(USCITADEFINITIVA, "YYYYMMDD") And USCITADEFINITIVA <> DateSerial(1989, 12, 30) Then
                Uscita = USCITADEFINITIVA
            End If

            ENTRATA = DateSerial(Anno, Mese - 2, 1)
            If Format(DateSerial(Anno, Mese, 1), "YYYYMMDD") > Format(accoglimento, "YYYYMMDD") And accoglimento <> DateSerial(1989, 12, 30) Then
                ENTRATA = accoglimento
            End If

            If IsDBNull(TotGiorniAss) Then
                DESMOTIVO = ""
            End If

            IMPEGNATIVA = ""
            LEGGIrs.Open("SELECT * FROM IMPEGNATIVE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " ORDER BY DATAINIZIO,DATAFINE DESC", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                IMPEGNATIVA = MoveFromDbWC(LEGGIrs, "Descrizione")
            End If
            LEGGIrs.Close()

            MySql = "SELECT * From RETTECOMUNE WHERE PROVINCIA = '" & MoveFromDbWC(MyRs, "Provincia") & "' AND Comune = '" & MoveFromDbWC(MyRs, "Comune") & "' AND CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And " & TestMeseAnno & " And CodiceOspite = " & ID 'And (RETTECOMUNE.ELEMENTO ='RGP'
            LEGGIrs.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                Cserv = MoveFromDbWC(LEGGIrs, "CentroServizio")
                Provincia = MoveFromDbWC(LEGGIrs, "Provincia")
                Comune = MoveFromDbWC(LEGGIrs, "Comune")
            End If
            LEGGIrs.Close()

            Dim WrRs As System.Data.DataRow = Stampa.Tables("RendicontoComuneMilano").NewRow
            WrRs.Item("Numero") = IMPEGNATIVA
            Dim x As New ClsOspite

            x.Leggi(Session("DC_OSPITE"), ID)

            WrRs.Item("NomeCognome") = x.Nome
            WrRs.Item("CodiceFiscale") = x.CODICEFISCALE
            WrRs.Item("DataDal") = ENTRATA
            WrRs.Item("DataAl") = Uscita
            WrRs.Item("Giorni") = Math.Round(TotGiorniPre, 2)
            'If Chk_mensile.Value = 1 Then

            WrRs.Item("QUOTAGIORNALIERA") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "C", 0) + QuoteGiornaliere(x.CentroServizio(Session("DC_OSPITE")), ID, "O", 0) + QuoteGiornaliere(x.CentroServizio(Session("DC_OSPITE")), ID, "P", 1) + QuoteGiornaliere(x.CentroServizio(Session("DC_OSPITE")), ID, "P", 2) + QuoteGiornaliere(x.CentroServizio(Session("DC_OSPITE")), ID, "P", 3) + QuoteGiornaliere(x.CentroServizio(Session("DC_OSPITE")), ID, "P", 4), 2)
            'Else
            'MoveToDb(WrRs.Fields("QUOTAGIORNALIERA"), Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "C", 0), 2))
            ' End If
            WrRs.Item("totaleimporto") = Math.Round(TotImporto, 2)
            WrRs.Item("TOTALEIMPORTOAssenze") = Math.Round(TotImportoAss, 2)
            WrRs.Item("MOTIVO") = DESMOTIVO
            WrRs.Item("GIORNIASSENZA") = TotGiorniAss
            WrRs.Item("QuotaGiornalieraOspite") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "O", 0), 2)
            WrRs.Item("QuotaGiornalieraParente1") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "P", 1), 2)
            WrRs.Item("QuotaGiornalieraParente2") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "P", 2), 2)
            WrRs.Item("QuotaGiornalieraParente3") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "P", 3), 2)
            WrRs.Item("QuotaGiornalieraParente4") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "P", 4), 2)
            WrRs.Item("QuotaGiornalieraParente5") = Math.Round(QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "P", 5), 2)

            Dim ks As New ClsComune

            If Trim(x.RESIDENZAPROVINCIA2) <> "" Then
                ks.Comune = x.RESIDENZACOMUNE2
                ks.Provincia = x.RESIDENZAPROVINCIA2
            Else
                ks.Comune = x.RESIDENZACOMUNE1
                ks.Provincia = x.RESIDENZAPROVINCIA1
            End If

            ks.Leggi(Session("DC_OSPITE"))
            WrRs.Item("ComuneResidenza") = ks.Descrizione

            'If Chk_mensile.Value = 1 Then
            WrRs.Item("Periodo") = "Periodo : " & Mese & " / " & Anno
            'Else
            'MoveToDb(WrRs.Fields("Periodo"), "Periodo : " & Format(DateSerial(Anno, Mese - 2, 1), "dd/mm/yyyy") & " - " & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/mm/yyyy"))
            'End If
            WrRs.Item("ImportoOspite") = Math.Round(TotImportoOspite, 2)
            WrRs.Item("ImportoParente") = Math.Round(TotImportoParenti, 2)
            WrRs.Item("ImportoUsl") = Math.Round(TotImportoRegione, 2)
            WrRs.Item("ImportoAssenza") = Math.Round(TotImportoAssenza, 2)

            WrRs.Item("ImportoAssenzaOspite") = ImportoAssenzaOspite
            WrRs.Item("ImportoAssenzaParente") = ImportoAssenzaParente
            WrRs.Item("ImportoAssenzaRegione") = ImportoAssenzaRegione


            WrRs.Item("RettaGiornalieraAssenza") = Math.Round(TotGiorniAss, 2)


            Dim XS As New Cls_FunzioniVB6

            XS.StringaOspiti = Session("DC_OSPITE")
            XS.StringaTabelle = Session("DC_TABELLE")
            XS.StringaGenerale = Session("DC_GENERALE")

            XS.ApriDB()

            WrRs.Item("DescrizioneUp") = XS.LeggiNote(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "Provincia"), MoveFromDbWC(MyRs, "Comune"), "", True, 0, 0, XS.StringaOspiti)
            WrRs.Item("DescrizioneDown") = XS.LeggiNote(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "Provincia"), MoveFromDbWC(MyRs, "Comune"), "", False, 0, 0, XS.StringaOspiti)

            XS.ChiudiDB()


            Dim xh As New Cls_CentroServizio

            xh.Leggi(Session("DC_OSPITE"), MoveFromDbWC(MyRs, "CentroServizio"))

            WrRs.Item("CentroServizio") = xh.DESCRIZIONE

            ks.Comune = Comune
            ks.Provincia = Provincia
            ks.Leggi(Session("DC_OSPITE"))

            Dim DecodificaKs As New ClsComune

            DecodificaKs.Provincia = ks.RESIDENZAPROVINCIA1
            DecodificaKs.Comune = ks.RESIDENZACOMUNE1
            If Not IsNothing(DecodificaKs.Provincia) Then
                DecodificaKs.Leggi(Session("DC_OSPITE"))
            End If

            If IsNothing(ks.Descrizione) Then
                WrRs.Item("Comune") = "Comune non decodificato"
            Else
                WrRs.Item("Comune") = ks.Descrizione
            End If


            WrRs.Item("Indirizzo") = ks.RESIDENZAINDIRIZZO1
            WrRs.Item("Cap") = ks.RESIDENZACAP1 & " " & DecodificaKs.Descrizione
            WrRs.Item("CODFISCALE") = ks.CodiceFiscale & " " & ks.PartitaIVA

            If Trim(Provincia) <> "" And Trim(Comune) <> "" Then
                Dim NuEs As New Cls_MovimentoContabile

                NumeroRegistrazione = NuEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), ks.MastroCliente, ks.ContoCliente, ks.SottocontoCliente, Cserv, Anno, Mese, "C" & Provincia & Comune, Anno, Mese)

                RsDocumenti.Open("Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & NumeroRegistrazione, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                If Not RsDocumenti.EOF Then
                    WrRs.Item("Documento") = MoveFromDbWC(RsDocumenti, "NumeroProtocollo") & "/A" & "/" & MoveFromDbWC(RsDocumenti, "AnnoProtocollo") & " del " & Format(MoveFromDbWC(RsDocumenti, "DataRegistrazione"), "dd/MM/yyyy")
                End If
                RsDocumenti.Close()
            End If
            WrRs.Item("Attenzione") = ks.Attenzione
            WrRs.Item("Descrizione") = ""

            If Year(accoglimento) = Anno And Month(accoglimento) = Mese Then
                WrRs.Item("Descrizione") = "A " & Format(accoglimento, "dd/MM/yyyy")
            End If
            If Year(USCITADEFINITIVA) = Anno And Month(accoglimento) = Mese Then
                WrRs.Item("Descrizione") = WrRs.Item("Descrizione") & " U " & Format(USCITADEFINITIVA, "dd/MM/yyyy")
            End If

            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And (TIPOMOV = '03' or TIPOMOV = '04') ", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)


            Do While Not LEGGIrs.EOF
                If MoveFromDbWC(MyRs, "TipoMov") = "13" Then
                    WrRs.Item("Descrizione") = MoveFromDbWC(WrRs, "Descrizione") & " Uscito il " & MoveFromDbWC(LEGGIrs, "Data")
                End If
                If MoveFromDbWC(MyRs, "TipoMov") = "03" Then
                    WrRs.Item("Descrizione") = MoveFromDbWC(WrRs, "Descrizione") & " Uscito il " & MoveFromDbWC(LEGGIrs, "Data")
                End If
                If MoveFromDbWC(MyRs, "TipoMov") = "04" Then
                    WrRs.Item("Descrizione") = MoveFromDbWC(WrRs, "Descrizione") & " Entrato il " & MoveFromDbWC(LEGGIrs, "Data")
                End If
                LEGGIrs.MoveNext()
            Loop
            LEGGIrs.Close()


            Stampa.Tables("RendicontoComuneMilano").Rows.Add(WrRs)

            MyRs.MoveNext()

        Loop
        MyRs.Close()


        Dim i As Integer
        Dim oldcomune As String
        Dim OldCENTROSERVIZIO As String

        i = 1

        oldcomune = ""
        OldCENTROSERVIZIO = ""
        'MySql = "Select * From RendicontoComuneMilano Order By CENTROSERVIZIO,Comune,ComuneResidenza,NOMECOGNOME"
        'WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        'Stampa.Tables("RendicontoComuneMilano").DefaultView.Sort = "CENTROSERVIZIO +.Comune + ComuneResidenza + NOMECOGNOME"

        ',CENTROSERVIZIO , ComuneResidenza , NOMECOGNOME


        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("SARA") > 0 Then
            Dim foundRows As System.Data.DataRow()

            foundRows = Stampa.Tables("RendicontoComuneMilano").Select("", "CENTROSERVIZIO,Comune ,  NOMECOGNOME")
            For MInd = 0 To foundRows.Length - 1
                If foundRows(MInd).Item("Comune") <> oldcomune Or foundRows(MInd).Item("CENTROSERVIZIO") <> OldCENTROSERVIZIO Then
                    oldcomune = foundRows(MInd).Item("Comune")
                    OldCENTROSERVIZIO = foundRows(MInd).Item("CENTROSERVIZIO")
                    i = 1
                End If
                foundRows(MInd).Item("NumeroRiga") = i
                i = i + 1
            Next MInd
        Else
            Dim foundRows As System.Data.DataRow()

            foundRows = Stampa.Tables("RendicontoComuneMilano").Select("", "CENTROSERVIZIO,Comune , ComuneResidenza , NOMECOGNOME")
            For MInd = 0 To foundRows.Length - 1
                If foundRows(MInd).Item("Comune") <> oldcomune Or foundRows(MInd).Item("CENTROSERVIZIO") <> OldCENTROSERVIZIO Then
                    oldcomune = foundRows(MInd).Item("Comune")
                    OldCENTROSERVIZIO = foundRows(MInd).Item("CENTROSERVIZIO")
                    i = 1
                End If
                foundRows(MInd).Item("NumeroRiga") = i
                i = i + 1
            Next MInd
        End If

        'Response.Redirect("StampaReport.aspx?REPORT=FATTUREDETTAGLIATESOCIALE")


        Session("stampa") = Stampa ' "" '"{Mastrino.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=FATTUREDETTAGLIATESOCIALE&PRINTERKEY=OFF','Stampe','width=800,height=600');", True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Application("ELABORANDO") = ""

        Dim K As New Cls_Parametri

        K.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = K.AnnoFatturazione

        dd_mese.Items.Clear()
        dd_mese.Items.Add("Gennaio")
        dd_mese.Items.Add("Febbraio")
        dd_mese.Items.Add("Marzo")
        dd_mese.Items.Add("Aprile")
        dd_mese.Items.Add("Maggio")
        dd_mese.Items.Add("Giugno")
        dd_mese.Items.Add("Luglio")
        dd_mese.Items.Add("Agosto")
        dd_mese.Items.Add("Settembre")
        dd_mese.Items.Add("Ottobre")
        dd_mese.Items.Add("Novembre")
        dd_mese.Items.Add("Dicembre")

        dd_mese.SelectedIndex = K.MeseFatturazione - 1

    End Sub

    Protected Sub StampaSanitaria()

        Lbl_Errore.Text = ""
        If Val(Txt_Anno.Text) = 0 Then
            Lbl_Errore.Text = "Digitare anno"
            Exit Sub
        End If


        Dim Stampa As New StampeOspiti

        Dim MyRs As New ADODB.Recordset
        Dim LEGGIrs As New ADODB.Recordset

        Dim RdRag As New ClsUSL

        Dim RsDocumenti As New ADODB.Recordset
        Dim Rs_Myrec As New ADODB.Recordset
        Dim Rs_Ospite As New ADODB.Recordset
        Dim Rs_Regioni As New ADODB.Recordset
        Dim CodiciParente(100) As Boolean
        Dim MySql As String        
        Dim i As Integer
        Dim Modalita As String = ""
        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection

        Dim GiorniPresenze As Integer
        Dim ImpRetta As Double = 0
        Dim ImpComune As Double = 0
        Dim ImpJolly As Double = 0
        Dim ImpOspite As Double = 0
        Dim ImportoRegione As Double = 0
        Dim AutoSufficente As String = ""
        Dim MiaRegione As String = ""
        Dim ImpParenti As Double = 0
        Dim ImpMia As Double = 0
        Dim ImpEnte As Double = 0
        Dim Mese As Integer
        Dim Anno As Integer
        Dim TestMeseAnno As String = ""
        Dim Cserv As String = ""
        Dim Regione As String = ""
        Dim Asl As String = ""
        Dim TOTGIORNI As Long
        Dim TotImporto As Double
        Dim TotAssente As Double

        Dim USCITADEFINITIVA As Date
        Dim ACCOGLIMENTO As Date
        Dim Uscita As Date
        Dim ENTRATA As Date
        Dim Quota As Double


        Dim NumeroRegistrazione As Long
        Dim ReD As New ClsUSL



        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        Anno = Txt_Anno.Text
        Mese = dd_mese.SelectedIndex + 1



        TestMeseAnno = " Mese = " & Mese & "  And Anno = " & Txt_Anno.Text


        

        MySql = "SELECT RETTEREGIONE.CENTROSERVIZIO,RETTEREGIONE.CODICEOSPITE From RETTEREGIONE WHERE (((RETTEREGIONE.ELEMENTO)='RGP' or (RETTEREGIONE.ELEMENTO)='RGA')) AND " & TestMeseAnno & " GROUP BY RETTEREGIONE.CENTROSERVIZIO,RETTEREGIONE.CODICEOSPITE"
        MyRs.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        Do While Not MyRs.EOF

            ID = MyRs.Fields("CodiceOspite").Value

            'If ID = 17 Then
            '   MsgBox "J"
            'End If
            Cserv = ""
            Regione = ""
            LEGGIrs.Open("Select * From RETTEREGIONE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then

                ReD.CodiceRegione = MoveFromDbWC(LEGGIrs, "Regione")
                ReD.Leggi(Session("DC_OSPITE"))

                RdRag.Nome = ""
                Asl = ReD.Nome
                If ReD.Raggruppamento <> "" Then


                    RdRag.CodiceRegione = ReD.Raggruppamento
                    RdRag.Leggi(Session("DC_OSPITE"))
                    Asl = RdRag.Nome
                End If

                Regione = MoveFromDbWC(LEGGIrs, "Regione")
                Cserv = MoveFromDbWC(LEGGIrs, "CentroServizio")
            End If
            LEGGIrs.Close()


            LEGGIrs.Open("Select sum(Giorni) AS T From RETTEREGIONE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & " And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TOTGIORNI = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("Select sum(Giorni) AS T From RETTEREGIONE WHERE  CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And (Elemento = 'RGP' OR Elemento = 'RPX') And CodiceOspite = " & ID & "  And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                GiorniPresenze = MoveFromDbWC(LEGGIrs, "t")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("Select sum(Importo) AS T From RETTEREGIONE WHERE  CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX') And CodiceOspite = " & ID & "  And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotImporto = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("Select sum(Giorni) AS T From RETTEREGIONE WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' And CodiceOspite = " & ID & "  And Elemento = 'RGA' And " & TestMeseAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                TotAssente = Math.Round(MoveFromDbWC(LEGGIrs, "t"), 2)
            End If
            LEGGIrs.Close()


            USCITADEFINITIVA = DateSerial(1899, 12, 30)
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' AND CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  And TIPOMOV = '13' ", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                USCITADEFINITIVA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()

            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' AND CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '03' And CAUSALE = 'ZA'", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                USCITADEFINITIVA = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()


            ACCOGLIMENTO = DateSerial(1899, 12, 30)
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' AND CODICEOSPITE = " & ID & " AND DATA >= {ts '" & Format(DateSerial(Anno, Mese - 2, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And TIPOMOV = '05' ", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not LEGGIrs.EOF Then
                ACCOGLIMENTO = MoveFromDbWC(LEGGIrs, "Data")
            End If
            LEGGIrs.Close()


            'LeggiRs.Open "SELECT * FROM MOVIMENTI WHERE CODICEOSPITE = " & ID & " AND DATA >= #" & Format(DateSerial(Anno, Mese - 2, 1), "mm/dd/yyyy") & "# And DATA <= #" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "mm/dd/yyyy") & "#   And (TIPOMOV = '03' or  TIPOMOV = '04') ORDER BY DATA,TIPOMOV", OspitiDb, adOpenKeyset, adLockOptimistic

            Quota = QuoteGiornaliere(MoveFromDbWC(MyRs, "CentroServizio"), ID, "R", 0, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
            LEGGIrs.Open("SELECT * FROM MOVIMENTI WHERE CENTROSERVIZIO = '" & MoveFromDbWC(MyRs, "CentroServizio") & "' AND CODICEOSPITE = " & ID & " And DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}   And (TIPOMOV = '03' or  TIPOMOV = '04') ORDER BY DATA,TIPOMOV", OspitiDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not LEGGIrs.EOF
                If MoveFromDbWC(LEGGIrs, "TipoMov") = "03" And MoveFromDbWC(LEGGIrs, "Causale") = "ZZ" Then
                    If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
                    If Not LEGGIrs.EOF Then
                        ACCOGLIMENTO = MoveFromDbWC(LEGGIrs, "Data")
                        Exit Do
                    End If
                End If
                If Not LEGGIrs.EOF Then LEGGIrs.MoveNext()
            Loop
            LEGGIrs.Close()

            Uscita = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
            If Val(Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd")) > Val(Format(USCITADEFINITIVA, "yyyyMMdd")) And USCITADEFINITIVA <> DateSerial(1899, 12, 30) Then
                Uscita = USCITADEFINITIVA
            End If

            ENTRATA = DateSerial(Anno, Mese - 2, 1)
            If Val(Format(DateSerial(Anno, Mese - 2, 1), "yyyyMMdd")) < Val(Format(ACCOGLIMENTO, "yyyyMMdd")) And ACCOGLIMENTO <> DateSerial(1899, 12, 30) Then
                ENTRATA = ACCOGLIMENTO
            End If



            Dim WrRs As System.Data.DataRow = Stampa.Tables("RendicontoAslMilano").NewRow


            Dim IDOsp As New ClsOspite

            IDOsp.Leggi(Session("DC_OSPITE"), ID)

            Application("ELABORANDO") = IDOsp.Nome

            WrRs.Item("ID") = ID
            WrRs.Item("NomeCognome") = IDOsp.Nome
            WrRs.Item("CodiceFiscale") = IDOsp.CODICEFISCALE
            WrRs.Item("Usl") = Asl
            WrRs.Item("DataDal") = ENTRATA
            WrRs.Item("DataAl") = Uscita
            If Quota = 0 Then
                WrRs.Item("Giorni") = 0
            Else
                WrRs.Item("Giorni") = Math.Round(TotImporto / Quota, 2)
            End If
            WrRs.Item("GiorniPresenze") = GiorniPresenze

            WrRs.Item("ImportoGiornaliero") = Quota
            WrRs.Item("totaleimporto") = TotImporto
            WrRs.Item("ExtraRegione") = ReD.FuoriRegione

            WrRs.Item("Periodo") = "Periodo : " & Mese & " / " & Anno

            WrRs.Item("QGGASSENZA") = TotAssente

            Dim XCser As New Cls_CentroServizio

            XCser.Leggi(Session("DC_OSPITE"), Cserv)


            Dim DecodificaKs As New ClsComune

            DecodificaKs.Provincia = ReD.RESIDENZAPROVINCIA1
            DecodificaKs.Comune = ReD.RESIDENZACOMUNE1
            DecodificaKs.Leggi(Session("DC_OSPITE"))


            WrRs.Item("CentroServizio") = XCser.DESCRIZIONE
            WrRs.Item("Regione") = Asl

            If RdRag.Nome = "" Then
                WrRs.Item("Indirizzo") = ReD.RESIDENZAINDIRIZZO1
                WrRs.Item("Cap") = ReD.RESIDENZACAP1 & " " & DecodificaKs.Descrizione
                WrRs.Item("CODFISCALE") = ReD.CodiceFiscale & " " & ReD.PARTITAIVA

                Dim Jk As New ClsComune

                Jk.Comune = ReD.RESIDENZACOMUNE1
                Jk.Provincia = ReD.RESIDENZAPROVINCIA1
                Jk.Leggi(Session("DC_OSPITE"))
                WrRs.Item("ComuneResidenza") = Jk.Descrizione

                WrRs.Item("Attenzione") = ReD.Attenzione
            Else
                WrRs.Item("Indirizzo") = RdRag.RESIDENZAINDIRIZZO1
                WrRs.Item("Cap") = RdRag.RESIDENZACAP1 & " " & DecodificaKs.Descrizione
                WrRs.Item("CODFISCALE") = RdRag.CodiceFiscale & " " & RdRag.PARTITAIVA

                Dim Jk As New ClsComune

                Jk.Comune = RdRag.RESIDENZACOMUNE1
                Jk.Provincia = RdRag.RESIDENZAPROVINCIA1
                Jk.Leggi(Session("DC_OSPITE"))
                WrRs.Item("ComuneResidenza") = Jk.Descrizione

                WrRs.Item("Attenzione") = RdRag.Attenzione
            End If

            WrRs.Item("Descrizione") = ""

            If Year(ACCOGLIMENTO) = Txt_Anno.Text And Month(ACCOGLIMENTO) = Mese Then
                WrRs.Item("Descrizione") = "A " & Format(ACCOGLIMENTO, "dd/MM/yyyy")
            End If
            If Year(USCITADEFINITIVA) = Txt_Anno.Text And Month(USCITADEFINITIVA) = Mese Then
                WrRs.Item("Descrizione") = WrRs.Item("Descrizione") & " U " & Format(USCITADEFINITIVA, "dd/MM/yyyy")
            End If
            Dim DocEs As New Cls_MovimentoContabile


            NumeroRegistrazione = DocEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), ReD.MastroCliente, ReD.ContoCliente, ReD.SottoContoCliente, Cserv, Anno, Mese, "R" & Regione, Anno, Mese)
            RsDocumenti.Open("Select * From MovimentiContabiliTesta Where NumerorEGISTRAZIONE = " & NumeroRegistrazione, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not RsDocumenti.EOF Then
                WrRs.Item("Documento") = MoveFromDbWC(RsDocumenti, "NumeroProtocollo") & "/A" & "/" & MoveFromDbWC(RsDocumenti, "AnnoProtocollo") & " del " & Format(MoveFromDbWC(RsDocumenti, "DataRegistrazione"), "dd/MM/yyyy")
            Else
                WrRs.Item("Documento") = ""
            End If
            RsDocumenti.Close()





            Dim XS As New Cls_FunzioniVB6

            XS.StringaOspiti = Session("DC_OSPITE")
            XS.StringaTabelle = Session("DC_TABELLE")
            XS.StringaGenerale = Session("DC_GENERALE")

            XS.ApriDB()

            'Regione & "-" & MoveFromDbWC(MyRs, "CentroServizio") &
            WrRs.Item("DescrizioneUp") = XS.LeggiNote(MoveFromDbWC(MyRs, "CentroServizio"), "", "", Regione, True, 9999999, 9999999, XS.StringaOspiti)
            WrRs.Item("DescrizioneDown") = XS.LeggiNote(MoveFromDbWC(MyRs, "CentroServizio"), "", "", Regione, False, 9999999, 9999999, XS.StringaOspiti)

            XS.ChiudiDB()

            Stampa.Tables("RendicontoAslMilano").Rows.Add(WrRs)


            MyRs.MoveNext()
        Loop
        MyRs.Close()


        Dim OldUSL As String
        Dim OldCserv As String

        i = 1
        OldUSL = ""
        OldCserv = ""
        'MySql = "Select * From RendicontoAslMilano Order By CENTROSERVIZIO,USL,NOMECOGNOME"

        Dim foundRows As System.Data.DataRow()

        foundRows = Stampa.Tables("RendicontoAslMilano").Select("", "CENTROSERVIZIO,USL,NOMECOGNOME")
        For MInd = 0 To foundRows.Length - 1
            If foundRows(MInd).Item("USL") <> OldUSL Or foundRows(MInd).Item("CENTROSERVIZIO") <> OldCserv Then
                OldUSL = foundRows(MInd).Item("USL")
                OldCserv = foundRows(MInd).Item("CENTROSERVIZIO")
                i = 1
            End If
            foundRows(MInd).Item("NumeroRiga") = i
            i = i + 1
        Next MInd

        'Response.Redirect("StampaReport.aspx?REPORT=FATTUREDETTAGLIATESANITARIO")


        Session("stampa") = Stampa '"{Mastrino.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=FATTUREDETTAGLIATESANITARIO&PRINTERKEY=OFF','Stampe','width=800,height=600');", True)
    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        Call StampaSociale()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        Call StampaSanitaria()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub
End Class

