﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Threading
Imports System.Collections.Generic


Partial Class Calcolo
    Inherits System.Web.UI.Page

    Dim myRequest As System.Net.WebRequest


    Private Delegate Sub DoWorkDelegate(ByRef data As Object)    
    Public StDataCond As String
    Private ConessioniOspiti As String
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoErrori As String
    Private CServInS As String
    Private CodOsp As Long


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Boolean
        Dim MySql As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(ConnessioniOspiti)

        cn.Open()

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim RsMovimenti As OleDbDataReader = cmd.ExecuteReader()


        If RsMovimenti.Read Then
            OspitePresente = False
            If campodb(RsMovimenti.Item("TipoMov")) = "13" And Format(campodbd(RsMovimenti.Item("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If campodb(RsMovimenti.Item("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        Else
            OspitePresente = False
        End If
        RsMovimenti.Close()
        cn.Close()
    End Function


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Protected Sub calcolo(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal txtanno As Long, ByVal ddmese As Long, ByVal ConnessioneOspiti As String, ByRef data As Object)




        Dim ConnectionString As String = ConnessioneOspiti
        Dim TipoCentro As String
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long
        Dim Mese As Integer
        Dim Anno As Integer
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim cn As OleDbConnection
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data
        
        'System.Web.SessionState.HttpSessionState = data


        
        cn = New Data.OleDb.OleDbConnection(ConnessioneOspiti)

        cn.Open()


        Dim UsoExtraParente As Integer = 0



        Dim cmdParenteRetta1 As New OleDbCommand()
        cmdParenteRetta1.CommandText = "SELECT count(*) as usati FROM [EXTRAOSPITE] where RIPARTIZIONE = 'P' "
        cmdParenteRetta1.Connection = cn
        Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
        If RDParenteRetta1.Read() Then
            If Val(campodb(RDParenteRetta1.Item(0))) > 0 Then
                UsoExtraParente = 1
            End If
        End If
        RDParenteRetta1.Close()


        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette
        SyncLock (SessioneTP.SyncRoot())
            SessioneTP("CampoErrori") = ""
            SessioneTP("CampoProgressBar") = 0
        End SyncLock


        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.ConguaglioMinimo = f.ConguaglioMinimo
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo
        XS.ApriDB(ConnessioneOspiti, Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = ConnessioneOspiti
        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.CaricaCausali()
        Mese = ddmese
        Anno = txtanno
        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"

        Dim ListinoPresente As Boolean = False
        Dim VerificaListino As New Cls_Tabella_Listino

        ListinoPresente = VerificaListino.EsisteListino(ConnessioneOspiti)


        VarCentroServizio = "0005"
        varcodiceospite = 371

        Dim Mysql As String
        Dim MysqlCount As String


        If CodiceOspite = 0 Then
            Mysql = " SELECT MOVIMENTI.CENTROSERVIZIO, MOVIMENTI.CODICEOSPITE FROM MOVIMENTI "
            Mysql = Mysql & " WHERE ("
            Mysql = Mysql & "(SELECT TOP 1 TIPOMOV FROM MOVIMENTI AS SUBMOV WHERE SUBMOV.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBMOV.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND DATA <= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC,PROGRESSIVO DESC ) <> '13' "
            Mysql = Mysql & " OR "
            Mysql = Mysql & " (SELECT COUNT(TIPOMOV) FROM MOVIMENTI AS SUBMOV WHERE SUBMOV.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBMOV.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND SUBMOV.DATA >={ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'}  AND SUBMOV.DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} )  > 0"
            Mysql = Mysql & " OR"

            Mysql = Mysql & " (SELECT COUNT(TIPOMOV) FROM ADDACR AS SUBADC WHERE  SUBADC.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBADC.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND SUBADC.DATA >={ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'}   AND SUBADC.DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} )  > 0"

            Mysql = Mysql & " ) "
            Mysql = Mysql & " AND (SELECT count(CodiceOspite)  From AnagraficaComune Where AnagraficaComune.CodiceOspite = MOVIMENTI.CODICEOSPITE And CodiceParente = 0 And NonInUso = 'S') < 1"

            If DD_CServ.SelectedValue <> "" Then
                Mysql = Mysql & " AND CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'"
            Else
                If DD_Struttura.SelectedValue <> "" Then
                    Mysql = Mysql & " And ("

                    Dim Indice As Integer
                    Call AggiornaCServ()
                    For Indice = 0 To DD_CServ.Items.Count - 1
                        If Indice >= 1 Then                            
                            Mysql = Mysql & " Or "                        
                        End If
                        Mysql = Mysql & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                    Next
                    Mysql = Mysql & ") "
                End If
            End If

            Mysql = Mysql & " GROUP BY  MOVIMENTI.CENTROSERVIZIO, MOVIMENTI.CODICEOSPITE "

            MysqlCount = "select count(*) as Totale From (" & Mysql & ") As TabAppo"

        Else
            Mysql = "SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE "
            MysqlCount = "select count(*) as Totale From (SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05') As TabAppo"
        End If

        Dim xMax As Long
        Dim xNum As Long = 0
        Dim VettoreTempi(100) As Double

        xMax = 0

        Dim cmdConta As New OleDbCommand()
        cmdConta.CommandText = MysqlCount
        cmdConta.Connection = cn
        Dim RdOspitiConta As OleDbDataReader = cmdConta.ExecuteReader()
        If RdOspitiConta.Read() Then
            xMax = RdOspitiConta.Item("Totale")
        End If
        RdOspitiConta.Close()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = Mysql
        cmd.Connection = cn
        Dim RdOspiti As OleDbDataReader = cmd.ExecuteReader()

        If CodiceOspite <> 0 Then
            XS.EliminaDatiRette(Mese, Anno, Cserv, CodiceOspite)
        Else
            If Cserv <> "" Then
                XS.EliminaDatiRette(Mese, Anno, Cserv, 0)
            Else
                If DD_Struttura.SelectedValue <> "" Then
                    Dim Indice As Integer
                    For Indice = 0 To DD_CServ.Items.Count - 1
                        XS.EliminaDatiRette(Mese, Anno, DD_CServ.Items(Indice).Value, 0)
                    Next
                Else
                    XS.EliminaDatiRette(Mese, Anno, "", 0)
                End If
            End If
        End If
        If Chk_EliminaCalcolato.checked = True Then
            XS.EliminaDatiRette(Mese, Anno, DD_CServ.SelectedValue, 0)
        End If


        Do While RdOspiti.Read


            Dim MyCserv As New Cls_CentroServizio

            MyCserv.Leggi(ConnessioneOspiti, campodb(RdOspiti.Item("CentroServizio")))

            f.Elaborazione(ConnessioneOspiti)

            If MyCserv.NonCalcolare = 0 Then

                VarCentroServizio = campodb(RdOspiti.Item("CentroServizio"))
                varcodiceospite = campodb(RdOspiti.Item("CodiceOspite"))

                Dim LeggiOspite As New ClsOspite

                LeggiOspite.Leggi(ConnessioneOspiti, varcodiceospite)


                If LeggiOspite.CONSENSOINSERIMENTO = 2 Then
                    XS.StringaDegliErrori = XS.StringaDegliErrori & "Consenso non dato al trattamento dei dati personali per ospite " & LeggiOspite.Nome & vbNewLine
                End If

                

                xNum = xNum + 1


                Try
                    Cache("CampoProgressBar" + Session.SessionID) = Math.Round(100 / xMax * xNum, 2)
                    Cache("NomeOspite" + Session.SessionID) = campodb(LeggiOspite.Nome)
                    Session("CampoProgressBar") = Math.Round(100 / xMax * xNum, 2)
                    Session("NomeOspite") = campodb(LeggiOspite.Nome)
                Catch ex As Exception

                End Try

                SyncLock (SessioneTP.SyncRoot())
                    SessioneTP("CampoProgressBar") = Math.Round(100 / xMax * xNum, 2)
                    SessioneTP("NomeOspite") = LeggiOspite.Nome
                End SyncLock

                Dim cmdAddOsp As New OleDbCommand()
                cmdAddOsp.CommandText = "Update TabellaParametri Set CodiceOspite = " & Val(campodb(RdOspiti.Item("CodiceOspite")))
                cmdAddOsp.Connection = cn
                cmdAddOsp.ExecuteNonQuery()


                If LeggiOspite.PERIODO = "F" Then
                    Dim DataUscita As Date
                    Dim CalcFineMese As New Cls_CalcoloOspitiFineMese



                    DataUscita = CalcFineMese.OspiteDimessonelMese(VarCentroServizio, varcodiceospite, Mese, Anno, Session("DC_OSPITE"))



                    If Month(DataUscita) = Mese And Year(DataUscita) = Anno Then
                        CalcFineMese.GeneraPeriodo(VarCentroServizio, varcodiceospite, Mese, Anno, DataUscita, Session("DC_OSPITE"), sc2)
                    End If

                    XS.StringaDegliErrori = XS.StringaDegliErrori & CalcFineMese.StringaDegliErrori
                Else
                    If Mese = Val(MyCserv.Mese1) Or Mese = Val(MyCserv.Mese2) Or Mese = Val(MyCserv.Mese3) Or Mese = Val(MyCserv.Mese4) Or Mese = Val(MyCserv.Mese5) Then

                    Else
                        If Not OspitePresente(VarCentroServizio, varcodiceospite, Mese, Anno, ConnessioneOspiti) Then

                            Call XS.AzzeraTabella()

                            XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                            XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                            XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                            XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                            XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                            XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                            XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

                            XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)
                            xMese = Mese
                            xAnno = Anno
                            If xMese = 12 Then
                                xMese = 1
                                xAnno = Anno + 1
                            Else
                                xMese = Mese + 1
                                xAnno = Anno
                            End If
                            Dim XOsp As New ClsOspite

                            XOsp.Leggi(ConnessioneOspiti, varcodiceospite)
                            If XOsp.FattAnticipata = "S" Then


                                Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese, xAnno)
                                Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese, xAnno)


                                Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese, xAnno)

                                Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese, xAnno)

                                Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese, xAnno)
                                Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese, xAnno)



                                Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese, xAnno, False)
                                Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese, xAnno)
                            End If
                            TipoCentro = XS.CampoCentroServizio(VarCentroServizio, "TIPOCENTROSERVIZIO")
                            Call XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                        Else
                            Dim Inizio As Long = 0



                            Call XS.AzzeraTabella()


                            If ListinoPresente Then
                                Dim MOs As New Cls_Listino

                                MOs.CENTROSERVIZIO = VarCentroServizio
                                MOs.CODICEOSPITE = varcodiceospite
                                MOs.LeggiAData(ConnessioneOspiti, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))


                                If MOs.CodiceListino <> "" Then
                                    Dim DeCListi As New Cls_Tabella_Listino

                                    DeCListi.Codice = MOs.CodiceListino
                                    DeCListi.LeggiCausale(ConnessioneOspiti)

                                    If Year(DeCListi.DATA) > 1900 Then
                                        If Format(DeCListi.DATA, "yyyMMdd") <= Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                            Dim DecOspite As New ClsOspite

                                            DecOspite.CodiceOspite = varcodiceospite
                                            DecOspite.Leggi(ConnessioneOspiti, DecOspite.CodiceOspite)

                                            XS.StringaDegliErrori = XS.StringaDegliErrori & "Ospite " & varcodiceospite & " " & DecOspite.Nome & " ha un listino scaduto " & DeCListi.Descrizione & vbNewLine
                                        End If
                                    End If
                                End If

                            End If

                            XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)
                            XS.PrimoDataAccoglimentoOspite = XS.PrimaDataAccoglimento(VarCentroServizio, varcodiceospite, Mese, Anno)

                            If UsoExtraParente = 1 Then
                                XS.VerificaPrimaCalcolo(VarCentroServizio, varcodiceospite)
                            End If

                            XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                            XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                            XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                            XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                            XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                            XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                            If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                                XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
                            End If

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(0) = VettoreTempi(0) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            If MyCserv.TIPOCENTROSERVIZIO = "A" Then
                                XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
                            End If

                            If Val(MyCserv.VerificaImpegnativa) = 1 Then
                                Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
                            End If

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(1) = VettoreTempi(1) + (Now.TimeOfDay.TotalMilliseconds - Inizio)


                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(2) = VettoreTempi(2) + (Now.TimeOfDay.TotalMilliseconds - Inizio)


                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(3) = VettoreTempi(3) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(4) = VettoreTempi(4) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(5) = VettoreTempi(5) + (Now.TimeOfDay.TotalMilliseconds - Inizio)


                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(6) = VettoreTempi(6) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)
                            VettoreTempi(7) = VettoreTempi(7) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(8) = VettoreTempi(8) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(9) = VettoreTempi(9) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)
                            VettoreTempi(10) = VettoreTempi(10) + (Now.TimeOfDay.TotalMilliseconds - Inizio)


                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(11) = VettoreTempi(11) + (Now.TimeOfDay.TotalMilliseconds - Inizio)

                            Inizio = Now.TimeOfDay.TotalMilliseconds
                            XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)
                            VettoreTempi(12) = VettoreTempi(12) + (Now.TimeOfDay.TotalMilliseconds - Inizio)
                            If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
                                REM  Errori = True
                            Else
                                Inizio = Now.TimeOfDay.TotalMilliseconds
                                XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                                VettoreTempi(13) = VettoreTempi(13) + (Now.TimeOfDay.TotalMilliseconds - Inizio)
                            End If
                        End If
                    End If
                End If
            End If
        Loop
        RdOspiti.Close()

        If f.IndirizzoObbligatorioInt = 1 Then
            Dim cmdRO As New OleDbCommand()
            cmdRO.CommandText = "Select * From  RetteOSpite where Mese = ? and Anno = ?"
            cmdRO.Connection = cn
            cmdRO.Parameters.AddWithValue("@Mese", Mese)
            cmdRO.Parameters.AddWithValue("@Anno", Anno)
            Dim RdRO As OleDbDataReader = cmdRO.ExecuteReader()
            Do While RdRO.Read()
                Dim LeggiOspite As New ClsOspite

                LeggiOspite.CodiceOspite = campodb(RdRO.Item("CodiceOspite"))
                LeggiOspite.Leggi(Session("DC_OSPITE"), LeggiOspite.CodiceOspite)
                If LeggiOspite.CodiceOspite > 0 Then

                    If LeggiOspite.RESIDENZACAP1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Cap non indicato per ospite " & LeggiOspite.Nome & vbNewLine
                    End If
                    If LeggiOspite.RESIDENZACOMUNE1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Comune non indicato per ospite " & LeggiOspite.Nome & vbNewLine
                    End If

                    If LeggiOspite.RESIDENZAINDIRIZZO1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Indirizzo non indicato per ospite " & LeggiOspite.Nome & vbNewLine
                    End If
                End If
            Loop
            RdRO.Close()

            Dim cmdRP As New OleDbCommand()
            cmdRP.CommandText = "Select * From  RetteParente where Mese = ? and Anno = ?"
            cmdRP.Connection = cn
            cmdRP.Parameters.AddWithValue("@Mese", Mese)
            cmdRP.Parameters.AddWithValue("@Anno", Anno)
            Dim RdRP As OleDbDataReader = cmdRP.ExecuteReader()
            Do While RdRP.Read()
                Dim LeggiParente As New Cls_Parenti

                LeggiParente.CodiceOspite = campodb(RdRP.Item("CodiceOspite"))
                LeggiParente.CodiceParente = campodb(RdRP.Item("CodiceParente"))
                LeggiParente.Leggi(Session("DC_OSPITE"), LeggiParente.CodiceOspite, LeggiParente.CodiceParente)


                If LeggiParente.CodiceOspite > 0 Then

                    If LeggiParente.RESIDENZACAP1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Cap non indicato per parente " & LeggiParente.Nome & vbNewLine
                    End If
                    If LeggiParente.RESIDENZACOMUNE1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Comune non indicato per parente " & LeggiParente.Nome & vbNewLine
                    End If

                    If LeggiParente.RESIDENZAINDIRIZZO1 = "" Then
                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Indirizzo non indicato per parente " & LeggiParente.Nome & vbNewLine
                    End If
                End If
            Loop
            RdOspitiConta.Close()


        End If

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = "Update TabellaParametri Set CodiceOspite = 0"
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()
        f.FineElaborazione(ConnessioneOspiti)
        cn.Close()
        SyncLock (SessioneTP.SyncRoot())
            SessioneTP("CampoProgressBar") = 101
            SessioneTP("CampoErrori") = XS.StringaDegliErrori
        End SyncLock

        Cache("CampoProgressBar" + Session.SessionID) = 101
        If IsNothing(XS.StringaDegliErrori) Then
            Cache("CampoErrori" + Session.SessionID) = ""
        Else
            Cache("CampoErrori" + Session.SessionID) = XS.StringaDegliErrori
        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<CALCOLO>") < 1 Then
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End If


        If Val(Request.Item("CodiceOspite")) > 0 Then
            Btn_Home.Visible = True            
        Else
            Btn_Home.Visible = False
        End If

        CServInS = Session("CODICESERVIZIO")
        CodOsp = Session("CODICEOSPITE")

        ConessioniOspiti = Session("DC_OSPITE")


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        

        If Val(Session("CODICEOSPITE")) = 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {  $('#MENUDIV').html(''); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DISATTIVAMENU", MyJs, True)
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



    


        Dim KBegin As New Cls_Parametri

        KBegin.LeggiParametri(Session("DC_OSPITE"))


        Try
            If Request.UrlReferrer.ToString.ToUpper.IndexOf("Menu_Ospiti.aspx".ToUpper) > 0 Then
                Session("CODICESERVIZIO") = ""
                Session("CODICEOSPITE") = 0
                ViewState("CODICESERVIZIO") = ""
                ViewState("CODICEOSPITE") = 0

                CServInS = Session("CODICESERVIZIO")
                CodOsp = Session("CODICEOSPITE")
            Else
                ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
                ViewState("CODICEOSPITE") = Session("CODICEOSPITE")
            End If
        Catch ex As Exception

        End Try
 


        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        If Val(Session("CODICEOSPITE")) > 0 Then
            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


            DD_Struttura.SelectedValue = cs.Villa
            Call AggiornaCServ()
            DD_CServ.SelectedValue = cs.CENTROSERVIZIO


            Dd_Mese.SelectedValue = cs.MeseFatturazione
            Txt_Anno.Text = cs.AnnoFatturazione
            DD_Struttura.Enabled = False
            DD_CServ.Enabled = False
        End If






        If Val(Session("CODICEOSPITE")) > 0 Then
            Chk_EliminaCalcolato.Visible = True
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim ConnectionString As String = Session("DC_OSPITE")



        Session("CampoErrori") = ""
        Session("CampoProgressBar") = -1

        Dim f As New Cls_Parametri
        Dim SaltaVerifica As Boolean = False

        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione
        Txt_Anno.Enabled = True
        Dd_Mese.Enabled = True

        If KBegin.PeriodoFatturazioneSuServizio = 1 Then
            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = DD_CServ.SelectedValue
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            If Cserv.MeseFatturazione > 0 Then
                Dd_Mese.SelectedValue = Cserv.MeseFatturazione
                Txt_Anno.Text = Cserv.AnnoFatturazione
            End If

            Dd_Mese.Enabled = False
            Txt_Anno.Enabled = False
        End If


        'Session("CalcCodiceOspite") = f.CodiceOspite

        'If f.CodiceOspite = 0 And KBegin.CodiceOspite = 0 Then
        '    SaltaVerifica = True
        'End If

        f.PercEsec = 0
        f.Errori = ""
        f.CodiceOspite = 0
        f.ScriviParametri(ConnectionString)

        Lbl_Waiting.Text = ""
        
        Timer1.Enabled = False


        'If Not SaltaVerifica Then
        '    Threading.Thread.Sleep(1500)


        '    Dim KAfter As New Cls_Parametri

        '    KAfter.LeggiParametri(Session("DC_OSPITE"))

        '    If KAfter.CodiceOspite > 0 Then
        '        If KAfter.CodiceOspite <> KBegin.CodiceOspite Then
        '            Response.Redirect("Menu_Ospiti.aspx")
        '            Exit Sub
        '        End If
        '    End If
        '    Session("CalcCodiceOspite") = KAfter.CodiceOspite
        'End If



    End Sub


    Public Sub DoWork(ByVal data As Object)

        calcolo(CServInS, CodOsp, Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), ConessioniOspiti,  data)
    End Sub



    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 100 Then
            Timer1.Enabled = False
            Dim Appoggio As String = ""
            Dim NumeroErrori As Long = 0
            Dim AppoggioCpy As String = Cache("CampoErrori" + Session.SessionID)

            Me.Title = "Calcolo Retta"

            Appoggio = "<table class=""style1"" style=""width: 100%"">"
            Appoggio = Appoggio & "<tr>"
            Appoggio = Appoggio & "<td class=""style2"" style=""width: 65px"">&nbsp;</td>"
            Appoggio = Appoggio & "<td class=""style3"">Errori Calcolo Rette</td>"
            Appoggio = Appoggio & "</tr>"
            Dim i As Integer
            For i = 1 To Len(AppoggioCpy)
                If Mid(AppoggioCpy, i, 1) = Chr(13) Then
                    Appoggio = Appoggio & "</b></td></tr>"
                    NumeroErrori = NumeroErrori + 1
                    Appoggio = Appoggio & "<tr>"
                    Appoggio = Appoggio & "<td class=""style5"" style=""width: 65px"">"
                    Appoggio = Appoggio & NumeroErrori
                    Appoggio = Appoggio & "</td>"
                    Appoggio = Appoggio & "<td class=""style5"" style=""text-align:left;""><b>"

                End If
                If Mid(AppoggioCpy, i, 1) = Chr(13) Or Mid(AppoggioCpy, i, 1) = Chr(10) Then
                Else
                    If NumeroErrori = 0 Then
                        NumeroErrori = NumeroErrori + 1
                        Appoggio = Appoggio & "<tr>"
                        Appoggio = Appoggio & "<td class=""style5"" style=""width: 65px"">"
                        Appoggio = Appoggio & NumeroErrori
                        Appoggio = Appoggio & "</td>"
                        Appoggio = Appoggio & "<td class=""style5"" style=""text-align:left;""><b>"
                    End If
                    Appoggio = Appoggio & Mid(AppoggioCpy, i, 1)
                End If
            Next
            Appoggio = Appoggio & "</b></td></tr></table>"
            If IsNothing(AppoggioCpy) Then
                Appoggio = "<table class=""style1"" style=""width: 41%; height: 62px"">"
                Appoggio = Appoggio & "<tr>"
                Appoggio = Appoggio & "<td class=""style2"" style=""width: 65px; height: 73px;""></td>"
                Appoggio = Appoggio & "<td class=""style3"" style=""height: 73px"">CALCOLO ESEGUITO CORRETTAMENTE</td>"
                Appoggio = Appoggio & "<td class=""style2"" style=""width: 65px; height: 73px;""></td>		"
                Appoggio = Appoggio & "</tr>"
                Appoggio = Appoggio & "</table>"

                Dim Param As New Cls_Parametri



                Param.LeggiParametri(Session("DC_OSPITE"))
                If (CodOsp > 0 Or CServInS <> "") Then
                    Param.Calcolato = ""
                Else
                    Param.Calcolato = "S"
                End If
                If Chk_EliminaCalcolato.visible = True Then
                    Param.Calcolato = "S"
                End If
                Param.ScriviParametri(Session("DC_OSPITE"))
                Btn_Modifica.Enabled = True

                If Chk_EliminaCalcolato.Checked = True Then
                    If DD_CServ.SelectedValue.Trim = "" Then
                        Response.Redirect("EmissioneRette.aspx?EMETTI=PROVA")
                    Else
                        Response.Redirect("EmissioneRette.aspx?EMETTI=PROVA&CENTROSERVIZIO=" & DD_CServ.SelectedValue.Trim)
                    End If
                End If
            Else
                If AppoggioCpy = "" Then
                    Appoggio = "<table class=""style1"" style=""width: 41%; height: 62px"">"
                    Appoggio = Appoggio & "<tr>"
                    Appoggio = Appoggio & "<td class=""style2"" style=""width: 65px; height: 73px;""></td>"
                    Appoggio = Appoggio & "<td class=""style3"" style=""height: 73px"">CALCOLO ESEGUITO CORRETTAMENTE</td>"
                    Appoggio = Appoggio & "<td class=""style2"" style=""width: 65px; height: 73px;""></td>		"
                    Appoggio = Appoggio & "</tr>"
                    Appoggio = Appoggio & "</table>"



                    Dim Param As New Cls_Parametri



                    Param.LeggiParametri(Session("DC_OSPITE"))
                    If (CodOsp > 0 Or CServInS <> "") Then
                        Param.Calcolato = ""
                    Else
                        Param.Calcolato = "S"
                    End If
                    If Chk_EliminaCalcolato.Visible = True Then
                        Param.Calcolato = "S"
                    End If

                    Param.ScriviParametri(Session("DC_OSPITE"))

                    If Chk_EliminaCalcolato.Checked = True Then
                        If DD_CServ.SelectedValue.Trim = "" Then
                            Response.Redirect("EmissioneRette.aspx?EMETTI=PROVA")
                        Else
                            Response.Redirect("EmissioneRette.aspx?EMETTI=PROVA&CENTROSERVIZIO=" & DD_CServ.SelectedValue.Trim)
                        End If

                    End If
                End If
            End If
            Lbl_Waiting.Text = Appoggio
            Session("CampoErrori") = ""
            Session("CampoProgressBar") = 0
            Exit Sub
        End If
        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 0 Then



            If Session("LastOspite") <> Cache("NomeOspite" + Session.SessionID) And Session("LastOspite") <> "" Then
                Session("NomeOspite1") = Session("NomeOspite2")
                Session("NomeOspite2") = Session("LastOspite")
            End If

            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Elaborazione..... <br /><br />"
            If Session("NomeOspite1") = "" Then
                Lbl_Waiting.Text = Lbl_Waiting.Text & " <br />"
            Else
                Lbl_Waiting.Text = Lbl_Waiting.Text & " ...... <br />"
            End If
            Lbl_Waiting.Text = Lbl_Waiting.Text & Session("NomeOspite1") & " <br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & Session("NomeOspite2") & " <br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & Cache("NomeOspite" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"

            Me.Title = Cache("CampoProgressBar" + Session.SessionID)

            Me.Title = Cache("CampoProgressBar" + Session.SessionID)
            Session("LastOspite") = Cache("NomeOspite" + Session.SessionID)
        End If
    End Sub




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        CServInS = Session("CODICESERVIZIO")
        CodOsp = Session("CODICEOSPITE")


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))



        If f.PeriodoFatturazioneSuServizio = 1 And DD_CServ.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Devi indicare il servizio da calcolare');", True)
            Exit Sub
        End If


        If f.PeriodoFatturazioneSuServizio = 0 Then
            If Val(Txt_Anno.Text) < f.AnnoFatturazione Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi calcolare un periodo precedente');", True)
                Exit Sub
            End If
            If Val(Dd_Mese.SelectedValue) < f.MeseFatturazione And Val(Txt_Anno.Text) = f.AnnoFatturazione Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi calcolare un periodo precedente');", True)
                Exit Sub
            End If
        Else
            Dim CentroServizio As New Cls_CentroServizio

            CentroServizio.CENTROSERVIZIO = DD_CServ.SelectedValue
            CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

            If Val(Txt_Anno.Text) < CentroServizio.AnnoFatturazione Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi calcolare un periodo precedente');", True)
                Exit Sub
            End If
            If Val(Dd_Mese.SelectedValue) < CentroServizio.MeseFatturazione And Val(Txt_Anno.Text) = f.AnnoFatturazione Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi calcolare un periodo precedente');", True)
                Exit Sub
            End If
        End If

        Btn_Modifica.Enabled = False

        'DoWork(Session)

        'Exit Sub

        Dim KAfter As New Cls_Parametri

        KAfter.LeggiParametri(Session("DC_OSPITE"))

        If KAfter.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If


        If Chk_EliminaCalcolato.Checked = True Then
            Dim ParametriGenerale As New Cls_ParametriGenerale

            If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
                Exit Sub
            End If

        End If




        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from notifiche ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()


        SyncLock (Session.SyncRoot())
            Session("CampoErrori") = ""
            Session("NomeOspite") = ""
            Session("NomeOspite1") = ""
            Session("NomeOspite2") = ""
            Session("LastOspite") = ""
            Session("CampoProgressBar") = 0
        End SyncLock

        'DoWork(Session)


        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)

        Timer1.Enabled = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Val(Request.Item("CODICEOSPITE")) > 0 Then
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("RicercaAnagrafica.aspx")
            Else
                Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
            End If
        Else
            Response.Redirect("Menu_Ospiti.aspx")
        End If
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub


    Protected Sub Btn_Home_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Home.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub DD_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CServ.SelectedIndexChanged
        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))


        If f.PeriodoFatturazioneSuServizio = 1 Then
            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = DD_CServ.SelectedValue
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            If Cserv.MeseFatturazione > 0 Then
                Dd_Mese.SelectedValue = Cserv.MeseFatturazione
                Txt_Anno.Text = Cserv.AnnoFatturazione
            Else
                Dd_Mese.SelectedValue = f.MeseFatturazione
                Txt_Anno.Text = f.AnnoFatturazione
            End If
        End If
    End Sub
End Class