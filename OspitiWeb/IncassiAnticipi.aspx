﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_IncassiAnticipi" EnableEventValidation="false" CodeFile="IncassiAnticipi.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Incassi e Pagamenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">                 
        function CloseMese() {
            if ($("#UltimoMese").css("visibility") != "hidden") {
                $("#UltimoMese").css('visibility', 'hidden');
                localStorage.setItem("UltimoMese", "invisibile");
            } else {
                $("#UltimoMese").css('visibility', 'visible');
                localStorage.setItem("UltimoMese", "visibile");
            }
        }

        function DialogBox(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });

            if (localStorage.UltimoMese == 'invisibile') {
                $("#UltimoMese").css('visibility', 'hidden');
            }

        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function CurrencyFormatted(number) {
            var numberStr = parseFloat(number).toFixed(2).toString();
            var numFormatDec = numberStr.slice(-2); /*decimal 00*/
            numberStr = numberStr.substring(0, numberStr.length - 3); /*cut last 3 strings*/
            var numFormat = new Array;
            while (numberStr.length > 3) {
                numFormat.unshift(numberStr.slice(-3));
                numberStr = numberStr.substring(0, numberStr.length - 3);
            }
            numFormat.unshift(numberStr);
            return numFormat.join('.') + ',' + numFormatDec; /*format 000.000.000,00 */
        }

        function copiaimporto(Riga) {
            var table = document.getElementById('TabContainer1_Tab_Anagrafica_Grid');


            var els = document.getElementsByTagName("*");

            Riga++;
            Riga++;

            var StrRiga = '' + Riga;
            if (Riga < 10) StrRiga = '0' + Riga;

            var totale = 0;
            for (var i = 0; i < els.length; i++) {
                if (els[i].id) {
                    var appoggio = els[i].id;
                    if (appoggio.match("ctl" + StrRiga + "_Txt_ImportoPagato") != null) {

                        if (($(els[i]).val() == 0) || ($(els[i]).val() == '0,00')) {
                            $(els[i]).val(table.rows[Riga - 1].cells[4].innerHTML);
                        } else {
                            $(els[i]).val('0,00');
                        }

                    }

                    if (appoggio.match("_Txt_ImportoPagato") != null) {
                        if ($(els[i]).val() != null) {
                            var testo = $(els[i]).val();

                            testo = testo.replace('.', '');
                            testo = testo.replace(',', '.');
                            if (testo != '') {
                                totale = totale + parseFloat(testo);
                            }
                        }
                    }

                }

            }


            var sttole = '' + (Math.round(totale * 100) / 100);

            //alert(sttole);
            //sttole = sttole.replace('.', ',');
            //alert(sttole);
            //alert(CurrencyFormatted(sttole));
            $("#TabContainer1_Tab_Anagrafica_Txt_Importo").val(CurrencyFormatted(sttole));
            $("#TabContainer1_Tab_Anagrafica_Txt_ImportoCassa").val(CurrencyFormatted(sttole));
        }
    </script>
    <style>
        #UltimoMese {
            position: absolute;
            right: 40px;
            top: 238px;
        }

        #ApriMese {
            position: absolute;
            right: 40px;
            top: 225px;
        }

        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 100%;
            box-shadow: 10px 10px 5px #888888;
            border: 1px solid #000000;
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

            .CSSTableGenerator table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                height: 100%;
                margin: 0px;
                padding: 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                -moz-border-radius-bottomright: 0px;
                -webkit-border-bottom-right-radius: 0px;
                border-bottom-right-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:first-child {
                -moz-border-radius-topleft: 0px;
                -webkit-border-top-left-radius: 0px;
                border-top-left-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:last-child {
                -moz-border-radius-topright: 0px;
                -webkit-border-top-right-radius: 0px;
                border-top-right-radius: 0px;
            }

            .CSSTableGenerator tr:last-child td:first-child {
                -moz-border-radius-bottomleft: 0px;
                -webkit-border-bottom-left-radius: 0px;
                border-bottom-left-radius: 0px;
            }

            .CSSTableGenerator tr:hover td {
            }

            .CSSTableGenerator tr:nth-child(odd) {
                background-color: #aad4ff;
            }

            .CSSTableGenerator tr:nth-child(even) {
                background-color: #ffffff;
            }

            .CSSTableGenerator td {
                vertical-align: middle;
                border: 1px solid #000000;
                border-width: 0px 1px 1px 0px;
                text-align: left;
                padding: 7px;
                font-size: 10px;
                font-family: Arial;
                font-weight: normal;
                color: #000000;
            }

            .CSSTableGenerator tr:last-child td {
                border-width: 0px 1px 0px 0px;
            }

            .CSSTableGenerator tr td:last-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                border-width: 0px 0px 0px 0px;
            }

            .CSSTableGenerator tr:first-child td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
                border: 0px solid #000000;
                text-align: center;
                border-width: 0px 0px 1px 1px;
                font-size: 14px;
                font-family: Arial;
                font-weight: bold;
                color: #ffffff;
            }

            .CSSTableGenerator tr:first-child:hover td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
            }

            .CSSTableGenerator tr:first-child td:first-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:first-child td:last-child {
                border-width: 0px 0px 1px 1px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Incassi Anticipi</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="ImageButton1" runat="server" Width="38px" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Incassi Anticipi</HeaderTemplate>
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="Lbl_ID" runat="server" Visible="False"></asp:Label>
                                                <label class="LabelCampo">Data :</label><asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Data Bolletta :</label><asp:TextBox ID="Txt_DataBolletta" runat="server" Width="90px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Numero Bolletta :</label><asp:TextBox ID="Txt_NumeroBolletta" runat="server" Width="104px" MaxLength="16"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Causale Contabile :</label><asp:DropDownList ID="Dd_CausaleContabile" runat="server" AutoPostBack="True" Width="408px"></asp:DropDownList><br />
                                                <br />
                                                <label class="LabelCampo">Modalita Pagamento :</label><asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="408px"></asp:DropDownList><br />
                                                <br />
                                                <label class="LabelCampo">Descrizione:</label><asp:TextBox ID="Txt_Descrizione" runat="server" Height="72px" TextMode="MultiLine" Width="680px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Ospite - Parente :</label>
                                                <asp:DropDownList ID="DD_OspiteParenti" runat="server" Width="381px"></asp:DropDownList><asp:Button ID="Button1" runat="server" Text="Refresh" /><br />
                                                <br />
                                                <label class="LabelCampo">Importo :</label>
                                                <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" AutoPostBack="True" Width="104px"></asp:TextBox><br />
                                                <br />
                                                <asp:GridView ID="Grid" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                                    Width="808px">
                                                    <Columns>
                                                        <asp:BoundField DataField="Copia" HeaderText="" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Registrazione" HeaderText="Numero Registrazione" />
                                                        <asp:BoundField DataField="DataDocumento" HeaderText="Data Documento" />
                                                        <asp:BoundField DataField="NumeroDocumento" HeaderText="Numero Documento" />
                                                        <asp:BoundField DataField="ImportoDocumento" HeaderText="Importo Documento" />
                                                        <asp:TemplateField HeaderText="Importo Pagato">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="Txt_ImportoPagato" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" onblur="this.value = formatNumber (this.value,2);  return true;" runat="server" Text='<%# Eval("ImportoPagato") %>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <EditRowStyle BackColor="#2461BF" />
                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#EFF3FB" />
                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                </asp:GridView>


                                                <div id="ApriMese">
                                                    <a href="#" onclick="CloseMese();">
                                                        <img src="../images/Icon_Importi.png" alt="Chiudi Mese" /></a>
                                                </div>
                                                <div id="UltimoMese">

                                                    <div class="CSSTableGenerator">
                                                        <table>
                                                            <tr>
                                                                <td>Periodo :
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Periodo" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Importo Retta Presenza :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ImportoRettaPresenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:Label>
                                                                </td>
                                                                <td>Giorni :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_GiorniPresenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Importo Retta Assenza :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ImportoRettaAssenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:Label></td>
                                                                <td>Giorni :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_GiorniAssenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:Label></td>
                                                            </tr>
                                                            <tr>

                                                                <td>Importo Extra :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ExtraFissi" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>

                                                                <td>Importo Addebiti :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ImportoAddibiti" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>

                                                                <td>Importo Accrediti :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ImportoAccrediti" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>

                                                            </tr>
                                                            <tr>

                                                                <td>Importo IVA :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_ImportoIVA" Enabled="false" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>

                                                                <td>Bollo :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_Bollo" Enabled="false" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Totale :</td>
                                                                <td>
                                                                    <asp:Label ID="Txt_Totale" Enabled="false" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Totale :</td>
                                                                <td>
                                                                    <asp:Label ID="Label1" Enabled="false" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>


                                                            <tr>
                                                                <td>Debito :</td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Debito" ForeColor="Red" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Saldo Anticipo :</td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Anticipo" ForeColor="Blue" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Incassi Non Chiusi :</td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_IncassiNonInChiuso" ForeColor="Blue" Style="text-align: right;" runat="server" Width="104px"></asp:Label></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                        </table>


                                                    </div>
                                                </div>
                                                <asp:Label ID="Lbl_errori" runat="server" ForeColor="Red" Width="424px"></asp:Label><br />

                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
