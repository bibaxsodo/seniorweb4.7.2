﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_DocumentazioneLocal" CodeFile="DocumentazioneLocal.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Documentazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {

            $("input:file").change(
                function () {
                    $("#upload-file-container").css("background", "url(images/UPLOADok.jpg) no-repeat");
                    $("#nomefile").html($("input:file").val().split('/').pop().split('\\').pop());
                }
            );

            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        #upload-file-container {
            background: url(images/UPLOAD.jpg) no-repeat;
            height: 178px;
        }

            #upload-file-container input {
                filter: alpha(opacity=0);
                height: 178px;
                width: 300px;
                opacity: 0;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Documentazione</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Documentazione</HeaderTemplate>
                                <ContentTemplate>
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Modify" CommandName="Edit" runat="Server" ImageUrl="~/images/modifica.png" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="IB_Update" CommandName="Update" runat="Server" ImageUrl="~/images/aggiorna.png" />
                                                    <asp:ImageButton ID="IB_Cancel" CommandName="Cancel" runat="Server" ImageUrl="~/images/annulla.png" />
                                                </EditItemTemplate>
                                                <ItemStyle Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Printer" CommandName="DOWNLOAD" runat="Server" ToolTip="Download File" ImageUrl="~/images/download.png" CommandArgument='<%#  Eval("NomeFile") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="40px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NOME FILE">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="286px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="286px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="DESCRIZIONE">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtDescrizione" onkeypress="return handleEnter(this, event)" MaxLength="100" Width="420px" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="420px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="440px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DATA">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblData" runat="server" Text='<%# Eval("Data") %>' Width="90px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_DELETEFILE" CommandName="CANCELLAFILE" ToolTip="Elimina File" OnClientClick="return window.confirm('Eliminare?');" runat="Server" ImageUrl="../images/elimina.jpg" CommandArgument='<%#  Container.DataItemIndex  %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="40px" />
                                            </asp:TemplateField>
                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White"
                                            BorderColor="#6FA7D1" BorderWidth="1px" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    Carica un file :<br />
                                    <div id="upload-file-container">
                                        <div style="text-align: center; width: 300px; padding-top: 15px;" id="nomefile"></div>
                                        <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" />
                                    </div>
                                    <br />
                                    <br />
                                    <asp:Button ID="UpLoadFile" runat="server" Height="28px" Width="88px" ToolTip="Up Load File" BackColor="#007DC4" ForeColor="White" Text="Carica File" />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
