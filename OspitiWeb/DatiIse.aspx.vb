﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class DatiIse
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Sub loadpagina()
        Dim x As New ClsOspite
        Dim d As New Cls_Ise


        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = False Then


            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")



            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Call loadpagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

        Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("TxtDataInizio"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)


        Call EseguiJS()
    End Sub
    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If

    End Sub






    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtDataInizio As TextBox = DirectCast(e.Row.FindControl("TxtDataInizio"), TextBox)

            TxtDataInizio.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim TxtDataFine As TextBox = DirectCast(e.Row.FindControl("TxtDataFine"), TextBox)

            TxtDataFine.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString



            Dim TxtISE As TextBox = DirectCast(e.Row.FindControl("TxtISE"), TextBox)

            TxtISE.Text = MyTable.Rows(e.Row.RowIndex).Item(2).ToString

            Dim TxtISEE As TextBox = DirectCast(e.Row.FindControl("TxtISEE"), TextBox)

            TxtISEE.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString

            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString

            Call EseguiJS()

        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Add("DataInizio", GetType(String))
        MyTable.Columns.Add("DataFine", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("ISE", GetType(String))
        MyTable.Columns.Add("ISEE", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtDataInizio As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataInizio"), TextBox)
            Dim TxtDataFine As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataFine"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Dim TxtISE As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtISE"), TextBox)
            Dim TxtISEE As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtISEE"), TextBox)

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()

            myrigaR(0) = TxtDataInizio.Text
            myrigaR(1) = TxtDataFine.Text
            myrigaR(2) = TxtISE.Text
            myrigaR(3) = TxtISEE.Text
            myrigaR(4) = TxtDescrizione.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtDataFine As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataFine"), TextBox)
            Dim TxtDataInizio As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataInizio"), TextBox)



            If Not IsDate(TxtDataInizio.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If TxtDataFine.Text <> "" Then
                If IsDate(TxtDataFine.Text) Then

                    Dim MyDateI As Date
                    Dim MyDateF As Date

                    MyDateI = TxtDataInizio.Text
                    MyDateF = TxtDataFine.Text
                    If Format(MyDateI, "yyyyMMdd") > Format(MyDateF, "yyyyMMdd") Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data inizio maggiore data fine per riga " & i + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            End If


            For T = 0 To Grd_Retta.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_Retta.Rows(T).FindControl("TxtDataInizio"), TextBox)

                    If TxtDatax.Text = TxtDataInizio.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next

        Call UpDateTable()
        MyTable = ViewState("App_Retta")

        Dim X1 As New Cls_ISE

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")


        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub
End Class
