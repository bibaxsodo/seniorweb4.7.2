﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.IO

Partial Class OspitiWeb_ImportRidDaXML
    Inherits System.Web.UI.Page


    Private Function LeggiFile() As String
        Dim line As String = ""
        Dim reader As StreamReader

        
        'Read to the end of file Peek method return -1 if the end of file is reach
        Try
            reader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "public/rid.xml")
            Do While reader.Peek <> -1

                line = line & reader.ReadLine().ToString.Replace(Chr(10), "").Replace(Chr(13), "").Replace(" ", "").Replace(vbTab, "")
            Loop

            reader.Close()

        Catch ex As Exception

        End Try

        Return line
    End Function


    Private Sub ImportXML()
        Dim FileXML As String
        Dim Indice As Integer = 0
        FileXML = LeggiFile()


        Dim LunghezzaRicerca As Integer = "<DrctDbtTxInf".Length
        Indice = FileXML.ToUpper.IndexOf("<DrctDbtTxInf".ToUpper)
        Do While Indice > 0
            Dim CodiceFiscale As String
            Dim InizoCodiceFiscale As Integer


            'InizoCodiceFiscale = FileXML.ToUpper.IndexOf("<InstrId>".ToUpper) + "<InstrId>".Length + 1
            'CodiceFiscale = Mid(FileXML, InizoCodiceFiscale, 16)


            Dim InizioOthr As String
            Dim FineOthr As String
            Dim cercaOthr As String

            InizioOthr = FileXML.ToUpper.IndexOf("<Othr>".ToUpper) + "<Othr>".Length
            FineOthr = FileXML.ToUpper.IndexOf("</Othr>".ToUpper)

            cercaOthr = Mid(FileXML, InizioOthr, FineOthr - InizioOthr)

            InizoCodiceFiscale = cercaOthr.ToUpper.IndexOf("<Id>".ToUpper) + "<Id>".Length + 1
            CodiceFiscale = Mid(cercaOthr, InizoCodiceFiscale, 16)


            Dim InizioIdMAndato As Integer
            Dim MndtId As String = ""
            Dim DataMandato As String = ""
            Dim DataInizioMandato As Integer
            Dim Iban As String = ""
            Dim InzioIban As Integer = 0

            If CodiceFiscale = "BNOGNI28A63F728Y" Then
                CodiceFiscale = "BNOGNI28A63F728Y"
            End If
            InizioIdMAndato = FileXML.ToUpper.IndexOf("<MndtId>".ToUpper) + "<MndtId>".Length + 1
            DataInizioMandato = FileXML.ToUpper.IndexOf("<DtOfSgntr>".ToUpper) + "<DtOfSgntr>".Length + 1

            MndtId = Mid(FileXML, InizioIdMAndato, FileXML.ToUpper.IndexOf("</MndtId>".ToUpper) - InizioIdMAndato + 1)
            DataMandato = Mid(FileXML, DataInizioMandato, FileXML.ToUpper.IndexOf("</DtOfSgntr>".ToUpper) - DataInizioMandato + 1)

            InzioIban = FileXML.ToUpper.IndexOf("<IBAN>".ToUpper) + "<IBAN>".Length + 1


            Iban = Mid(FileXML, InzioIban, FileXML.ToUpper.IndexOf("</IBAN>".ToUpper) - InzioIban + 1)


            FileXML = Mid(FileXML, FileXML.ToUpper.IndexOf("</DrctDbtTxInf>".ToUpper) + "</DrctDbtTxInf>".Length, FileXML.Length - FileXML.ToUpper.IndexOf("</DrctDbtTxInf>".ToUpper) - "</DrctDbtTxInf>".Length + 1)

            Dim DatiPagamento As New Cls_DatiPagamento
            Dim DatiOSpite As New ClsOspite

            DatiOSpite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CodiceFiscale)

            If DatiOSpite.CodiceOspite > 0 Then



                DatiPagamento.CodiceOspite = DatiOSpite.CodiceOspite
                DatiPagamento.CodiceParente = 0
                DatiPagamento.Data = DataMandato
                DatiPagamento.IdMandatoDebitore = MndtId
                DatiPagamento.DataMandatoDebitore = DataMandato

                DatiPagamento.CodiceInt = Mid(Iban, 1, 2)
                DatiPagamento.NumeroControllo = Mid(Iban, 3, 2)
                DatiPagamento.Cin = Mid(Iban, 5, 1)
                DatiPagamento.Abi = Mid(Iban, 6, 5)
                DatiPagamento.Cab = Mid(Iban, 11, 5)
                DatiPagamento.CCBancario = Mid(Iban, 16, 12)
                DatiPagamento.Scrivi(Session("DC_OSPITE"))
            Else
                Dim pAR As New Cls_Parenti


                pAR.LeggiPerCodiceFiscaleSenzaOspite(Session("DC_OSPITE"), CodiceFiscale)

                If pAR.CodiceOspite > 0 Then

                    DatiPagamento.CodiceOspite = pAR.CodiceOspite
                    Dim Calcolo As New Cls_CalcoloRette
                    Dim MQuote As New Cls_Movimenti

                    MQuote.CodiceOspite = pAR.CodiceOspite
                    MQuote.UltimaData(Session("DC_OSPITE"), MQuote.CodiceOspite)

                    

                    Calcolo.ConnessioneGenerale = Session("DC_GENERALE")
                    Calcolo.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                    Calcolo.ApriDB(Calcolo.STRINGACONNESSIONEDB, Session("DC_OSPITIACCESSORI"))
                    If Calcolo.QuoteGiornaliere(MQuote.CENTROSERVIZIO, MQuote.CodiceOspite, "O", 0, Now) > 0 Then
                        DatiPagamento.CodiceParente = 0
                    Else
                        DatiPagamento.CodiceParente = pAR.CodiceParente
                    End If

                    DatiPagamento.Data = DataMandato
                    DatiPagamento.IdMandatoDebitore = MndtId
                    DatiPagamento.DataMandatoDebitore = DataMandato

                    DatiPagamento.CodiceInt = Mid(Iban, 1, 2)
                    DatiPagamento.NumeroControllo = Mid(Iban, 3, 2)
                    DatiPagamento.Cin = Mid(Iban, 5, 1)
                    DatiPagamento.Abi = Mid(Iban, 6, 5)
                    DatiPagamento.Cab = Mid(Iban, 11, 5)
                    DatiPagamento.CCBancario = Mid(Iban, 16, 12)
                    DatiPagamento.Scrivi(Session("DC_OSPITE"))
                End If
            End If

            Indice = FileXML.ToUpper.IndexOf("<DrctDbtTxInf".ToUpper)
        Loop

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btn_clicca_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_clicca.Click
        ImportXML()
    End Sub
End Class
