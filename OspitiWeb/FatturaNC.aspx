﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_FatturaNC" CodeFile="FatturaNC.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Fattura NC</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">    
        $(document).ready(function () {

            $('#<%=Txt_DataRegistrazione.ClientID%>').mask("99/99/9999");

        });
        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 280px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 160px; background-color: #F0F0F0;"></td>
                        <td>
                            <div class="Titolo">Ospiti - Fattura/Nota Credito</div>
                            <div class="SottoTitoloOSPITE">
                                <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                                <br />
                            </div>
                        </td>
                        <td style="text-align: right; vertical-align: top;">
                            <div class="DivTasti">
                                <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 160px; background-color: #F0F0F0;"></td>
                        <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <table>
                                            <tr>
                                                <td style="color: #565151;">Anno :</td>
                                                <td>
                                                    <asp:TextBox ID="Txt_Anno" runat="server"
                                                        onkeypress="return soloNumeri(event);" Width="64px"></asp:TextBox>
                                                </td>
                                                <td style="color: #565151;">Mese :</td>
                                                <td>
                                                    <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox>
                                                </td>
                                                <td>Numero Protocollo:
                                                    <asp:TextBox ID="Txt_NumeroProtocollo" runat="server" Text="0" Width="90px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="Chk_SoloMese" runat="server" />
                                                    Solo Mese corrente                                        
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent"
                                            ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="LabelCampo">Ospite - Parente :</label>
                                        <asp:DropDownList ID="DD_OspiteParenti" runat="server" AutoPostBack="true" Width="381px"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RdRetta" runat="server" Text="Retta" GroupName="RettaFuoriRetta" Visible="false" />
                                        <asp:RadioButton ID="RdFuoriRetta" runat="server" Text="Fuori Retta" GroupName="RettaFuoriRetta" Visible="false" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;">
                            <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                                <img alt="Menù" src="images/Home.jpg" class="Effetto" /></a><br />
                            <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent"
                                ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                            <div id="MENUDIV">
                            </div>
                        </td>
                        <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Label ID="Lbl_DecodificaCausale" runat="server" ForeColor="Blue"></asp:Label>
                            <asp:GridView ID="GridV" runat="server" BackColor="White" BorderColor="#6FA7D1"
                                BorderStyle="Dotted" BorderWidth="1px" CellPadding="4" Height="60px"
                                ShowFooter="False">
                                <RowStyle BackColor="#EEEEEE" ForeColor="#565151" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>

                                    <asp:TemplateField HeaderText="DescrizioneRiga">
                                        <ItemTemplate>
                                            <asp:Label ID="LblDescrizioneRiga" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="LblId" runat="server" Visible="false" Text=""></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Importo">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtImporto" Style="text-align: right;" runat="server" Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Giorni">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtGiorni" runat="server" Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IVA">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDIva" AutoPostBack="True" Width="150px" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descrizione">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtDescrizione" runat="server" Width="300px"></asp:TextBox>

                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conto Ricavo">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtContoRicavo" runat="server" Width="300px"></asp:TextBox>

                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                <HeaderStyle BackColor="#A6C9E2" BorderColor="#6FA7D1" BorderWidth="1"
                                    Font-Bold="False" ForeColor="White" />
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>

                            <br />
                            <label style="display: block; float: left; width: 250px;">
                                <asp:Label ID="Lbl_Riferimento730" runat="server" Text="Riferimento per 730"></asp:Label></label>
                            <label style="display: block; float: left; width: 200px;">
                                <asp:Label ID="Lbl_RiferimentoNumero" runat="server" Text="Riferimento Numero:"></asp:Label></label>
                            <asp:TextBox ID="Txt_RifNumero" autocomplete="off" runat="server" Width="150px"></asp:TextBox>
                            <asp:Label ID="Lbl_RiferimentoData" runat="server" Text="Riferimento Data:"></asp:Label>
                            <asp:TextBox ID="Txt_DataRif" autocomplete="off" runat="server" Width="150px"></asp:TextBox>
                            <br />
                            <asp:LinkButton ID="Lb_GestioneDocumento" runat="server">Gestione Documento</asp:LinkButton><br />
                            <asp:LinkButton ID="Lb_StampaDocumento" runat="server">Stampa Documento</asp:LinkButton><br />

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="GridV" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdateProgress ID="UpdateProgress2" runat="server"
            AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div id="blur">&nbsp;</div>

                <div id="progress" style="width: 200px; height: 80px; left: 40%; position: absolute; top: 372px; text-align: center;">
                    Attendere prego.....<br />
                    <img height="30px" src="images/loading.gif">
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
