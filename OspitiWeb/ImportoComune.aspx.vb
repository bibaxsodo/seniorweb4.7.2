﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class ImportoComune
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Sub loadpagina()
        Dim x As New ClsOspite
        Dim d As New Cls_ImportoComune




        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Call EtichetteImporti()

        Grd_Retta.DataBind()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If
            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")
            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Call loadpagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"



            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(Session("DC_OSPITE"))

            If KCs.NonCalcoloComune = 1 Then
                Chk_calcolare.Checked = True
            Else
                Chk_calcolare.Checked = False
            End If

        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable
        Call EtichetteImporti()
        Grd_Retta.DataBind()


        Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)


        Call EseguiJS()
    End Sub

    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()            
        End If

    End Sub






    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString





            Dim TxtImporto1 As TextBox = DirectCast(e.Row.FindControl("TxtImporto1"), TextBox)

            TxtImporto1.Text = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtImporto2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto2"), TextBox)

            TxtImporto2.Text = MyTable.Rows(e.Row.RowIndex).Item(7).ToString


            Dim TxtImporto3 As TextBox = DirectCast(e.Row.FindControl("TxtImporto3"), TextBox)

            TxtImporto3.Text = MyTable.Rows(e.Row.RowIndex).Item(8).ToString


            Dim TxtImporto4 As TextBox = DirectCast(e.Row.FindControl("TxtImporto4"), TextBox)

            TxtImporto4.Text = MyTable.Rows(e.Row.RowIndex).Item(9).ToString



            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = MyTable.Rows(e.Row.RowIndex).Item(10).ToString



            Dim TxtDataSosia As TextBox = DirectCast(e.Row.FindControl("TxtDataSosia"), TextBox)

            TxtDataSosia.Text = MyTable.Rows(e.Row.RowIndex).Item(12).ToString


            REM Dim TxtComune As TextBox = DirectCast(e.Row.FindControl("TxtComune"), TextBox)

            REM TxtComune.Text = Trim(MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(5).ToString)

            Dim DD_Comune As DropDownList = DirectCast(e.Row.FindControl("DD_Comune"), DropDownList)
            Dim K As New ClsComune

            K.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)

            DD_Comune.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString



            If MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString <> "" And DD_Comune.SelectedValue = "" Then
                Dim TrovaCodice As Boolean = False
                Dim Indice As Integer


                For Indice = 0 To DD_Comune.Items.Count - 1
                    If DD_Comune.Items(Indice).Value = MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString Then
                        TrovaCodice = True
                    End If
                Next

                If TrovaCodice = False Then

                    Dim Comune As New ClsComune

                    Comune.Descrizione = ""
                    Comune.Provincia = MyTable.Rows(e.Row.RowIndex).Item(3).ToString
                    Comune.Comune = MyTable.Rows(e.Row.RowIndex).Item(4).ToString()
                    Comune.Leggi(Session("DC_OSPITE"))

                    If Comune.Descrizione <> "" Then
                        DD_Comune.Items.Add(Comune.Descrizione & " (" & Comune.NomeConiuge & ")")
                        DD_Comune.Items(DD_Comune.Items.Count - 1).Value = MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString

                        DD_Comune.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString & " " & MyTable.Rows(e.Row.RowIndex).Item(4).ToString
                    End If
                End If
            End If



            Call EseguiJS()

        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("PROV", GetType(String))
        MyTable.Columns.Add("COMUNE", GetType(String))
        MyTable.Columns.Add("DECODIFICACOMUNE", GetType(String))
        MyTable.Columns.Add("Importo1", GetType(String))
        MyTable.Columns.Add("Importo2", GetType(String))
        MyTable.Columns.Add("Importo3", GetType(String))
        MyTable.Columns.Add("Importo4", GetType(String))
        MyTable.Columns.Add("DescrizioneRiga", GetType(String))
        MyTable.Columns.Add("MensileFisso", GetType(String))
        MyTable.Columns.Add("DataSosia", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Tipo"), DropDownList)

            'Dim TxtComune As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtComune"), TextBox)

            Dim DD_Comune As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Comune"), DropDownList)

            Dim TxtImporto1 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto1"), TextBox)
            Dim TxtImporto2 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto2"), TextBox)
            Dim TxtImporto3 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto3"), TextBox)
            Dim TxtImporto4 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto4"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Dim TxtDataSosia As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataSosia"), TextBox)


            Dim Vettore(100) As String
            Dim Appoggio As String

            Appoggio = DD_Comune.SelectedValue
            

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text

            If TxtImporto.Text = "" Then
                TxtImporto.Text = "0,00"
            End If
            myrigaR(1) = TxtImporto.Text
            If DD_Tipo.SelectedValue = "N" Then
                myrigaR(2) = "M"
                myrigaR(11) = 1
            Else
                myrigaR(2) = DD_Tipo.SelectedValue
                myrigaR(11) = 0
            End If


            REM If Vettore.Length >= 2 Then
            Vettore = SplitWords(Appoggio)
            If Vettore.Length >= 2 Then
                myrigaR(3) = Vettore(0)
                myrigaR(4) = Vettore(1)
            End If
            REM End If

            'Dim Vettore(100) As String

            'If TxtComune.Text <> "" Then
            '    Vettore = SplitWords(TxtComune.Text)

            '    If Vettore.Length >= 3 Then
            '        myrigaR(3) = Vettore(0)
            '        myrigaR(4) = Vettore(1)
            '        myrigaR(5) = Trim(Replace(TxtComune.Text, Vettore(0) & " " & Vettore(1), ""))
            '    End If
            'End If

            myrigaR(5) = DD_Comune.Text
            '        myrigaR(4) = Vettore(1)
            '        myrigaR(5) = Trim(Replace(TxtComune.Text, Vettore(0) & " " & Vettore(1), ""))


            If TxtImporto1.Text = "" Then
                TxtImporto1.Text = "0,00"
            End If
            If TxtImporto2.Text = "" Then
                TxtImporto2.Text = "0,00"
            End If
            If TxtImporto3.Text = "" Then
                TxtImporto3.Text = "0,00"
            End If
            If TxtImporto4.Text = "" Then
                TxtImporto4.Text = "0,00"
            End If
            myrigaR(6) = TxtImporto1.Text
            myrigaR(7) = TxtImporto2.Text
            myrigaR(8) = TxtImporto3.Text
            myrigaR(9) = TxtImporto4.Text
            myrigaR(10) = TxtDescrizione.Text
            myrigaR(12) = TxtDataSosia.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable
        Call EtichetteImporti()
        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Tipo"), DropDownList)

            'Dim TxtComune As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtComune"), TextBox)
            Dim DD_Comune As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Comune"), DropDownList)

            Dim TxtImporto1 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto1"), TextBox)
            Dim TxtImporto2 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto2"), TextBox)
            Dim TxtImporto3 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto3"), TextBox)
            Dim TxtImporto4 As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto4"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Dim TxtDataSosia As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtDataSosia"), TextBox)

            If Not IsDate(TxtData.Text) And CDbl(TxtImporto.Text) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If Len(TxtDescrizione.Text) > 100 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione riga troppo lunga per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If CDbl(TxtImporto.Text) > 0 And DD_Comune.SelectedValue.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il comune per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            'If TxtComune.Text.Trim <> "" Then
            '    Dim Vettore(100) As String

            '    Vettore = SplitWords(TxtComune.Text)
            '    If Vettore.Length < 3 Then
            '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice comune errato per riga " & i + 1 & "');", True)
            '        Call EseguiJS()
            '        Exit Sub
            '    End If                
            'End If



            For T = 0 To Grd_Retta.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_Retta.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")

        Dim Log As New Cls_LogPrivacy

        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_ImportoComune

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "IMPORTOCOMUNE", AppoggioJS)


        Call UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim X1 As New Cls_ImportoComune

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")

        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(Session("DC_OSPITE"))


        If KCs.CodiceOspite = 0 Then

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0

            Dim Ospite As New ClsOspite


            Ospite.CodiceOspite = Session("CODICEOSPITE")

            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)


            KCs.Anticipata = Ospite.FattAnticipata
            KCs.TipoOperazione = Ospite.TIPOOPERAZIONE
            KCs.AliquotaIva = Ospite.CODICEIVA
            KCs.Compensazione = Ospite.Compensazione
            KCs.ModalitaPagamento = Ospite.MODALITAPAGAMENTO
            KCs.Settimana = Ospite.SETTIMANA


        End If


        If Chk_calcolare.Checked = True Then
            KCs.NonCalcoloComune = 1
        Else
            KCs.NonCalcoloComune = 0
        End If
        KCs.Scrivi(Session("DC_OSPITE"))


        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Private Sub EtichetteImporti()

        Dim k As New Cls_Tabelle

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "01"
        k.Leggi(Session("DC_OSPITE"))
        Grd_Retta.Columns(6).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_Retta.Columns(6).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "02"
        k.Leggi(Session("DC_OSPITE"))

        Grd_Retta.Columns(7).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_Retta.Columns(7).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "03"
        k.Leggi(Session("DC_OSPITE"))
        Grd_Retta.Columns(8).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_Retta.Columns(8).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "04"
        k.Leggi(Session("DC_OSPITE"))
        Grd_Retta.Columns(9).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_Retta.Columns(9).Visible = False
        End If

        Dim MPar As New Cls_Parametri

        MPar.LeggiParametri(Session("DC_OSPITE"))

        If MPar.DataSosia = 0 Then
            Grd_Retta.Columns(10).Visible = False
        End If
    End Sub
End Class

