﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="AddebitoAccredito" EnableEventValidation="false" CodeFile="AddebitoAccredito.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Addebiti Accrediti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica0", "0");
                }

            });
        });

        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Addebiti Accrediti</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Addebiti Accrediti
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <label class="LabelCampo">Tipo Movimento : </label>
                                            <asp:RadioButton ID="RB_ADDEBITO" GroupName="mTIPOAD" runat="server" Text="Addebito" ForeColor="red" Checked="True" />
                                            <asp:RadioButton ID="RB_ACCREDITO" GroupName="mTIPOAD" runat="server" Text="Accredito" ForeColor="green" /><br />
                                            <br />

                                            <label class="LabelCampo">Data Validità: </label>
                                            <asp:TextBox ID="Txt_Data" runat="server" Width="90px" AutoPostBack="true"></asp:TextBox>
                                            <asp:Label ID="Lbl_Progressivo" runat="server" Text="0" Width="136px"></asp:Label><br />
                                            <br />

                                            <label class="LabelCampo">Tipo Addebito :</label>
                                            <asp:DropDownList ID="dd_TipoAddebito" class="chosen-select" AutoPostBack="true" runat="server" Width="450px"></asp:DropDownList>
                                            <asp:CheckBox ID="Chk_Ragruppa" runat="server" Text="Raggruppa in Emissione" />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="lblImporto" runat="server" Text="Importo :"></asp:Label></label>
                                            <asp:TextBox ID="Txt_Quantita" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Visible="false" runat="server" Width="60px"></asp:TextBox>
                                            <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="120px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Descrizione :</label>
                                            <asp:TextBox ID="Txt_Descrizione" MaxLength="150" runat="server" Width="450px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Periodo :</label>
                                            <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                            <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                                <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RadioButton ID="RB_FuoriRetta" runat="server" Text="Fuori Retta" GroupName="mTIPO" />
                                            <asp:RadioButton ID="RB_Retta" runat="server" Text="Retta" GroupName="mTIPO" />
                                            <asp:RadioButton ID="Rb_Rendiconto" runat="server" Text="Rendiconto" GroupName="mTIPO" /><br />
                                            <br />


                                            <label class="LabelCampo">Ospite :</label>
                                            <asp:RadioButton ID="RB_OSPITE" runat="server" GroupName="TIPO" Checked="True" /><br />
                                            <br />

                                            <label class="LabelCampo">Parente :</label>
                                            <asp:RadioButton ID="RB_PARENTE" runat="server" GroupName="TIPO" />
                                            : 
         <asp:DropDownList ID="DD_Parente" runat="server" Width="430px"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Comune :</label>
                                            <asp:RadioButton ID="RB_COMUNE" runat="server" GroupName="TIPO" />, JOLLY : 
         <asp:RadioButton ID="RB_JOLLY" runat="server" GroupName="TIPO" />
                                            : 
         <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Regione :</label>
                                            <asp:RadioButton ID="Rb_Regione" runat="server" GroupName="TIPO" />:&nbsp;
         <asp:DropDownList ID="DD_Regione" runat="server" class="chosen-select"></asp:DropDownList><br />
                                            <br />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Elaborato :</label>
                                            <asp:CheckBox ID="ChK_Elaborato" runat="server" Text="" />
                                            <asp:TextBox ID="Txt_Registrazione" MaxLength="30" runat="server" Width="80px"></asp:TextBox><br />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Chiave :</label>
                                            <asp:TextBox ID="Txt_Chiave" MaxLength="30" runat="server" Width="80px"></asp:TextBox><br />
                                            <br />
                                            <br />

                                            <asp:Image ID="ImgAllegato" runat="server" Width="240px" Style="float: right; position: absolute; top: 240px; right: 10px;" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </ContentTemplate>


                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>


        </div>

    </form>
</body>
</html>
