﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_ElencoRegistrazione
    Inherits System.Web.UI.Page

    Dim TabellaDoc As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call CaricaGriglia(Val(DD_OspiteParenti.SelectedValue))
    End Sub

    Private Sub CaricaGriglia(ByVal Parente As Integer)
        Grd_Denaro.Visible = True

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If Parente = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + Parente
        End If


        If Mastro = 0 Or Sottoconto = 0 Then Exit Sub



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,MovimentiContabiliTesta.NumeroBolletta,MovimentiContabiliTesta.DataBolletta " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        TabellaDoc.Clear()
        TabellaDoc.Columns.Clear()
        TabellaDoc.Columns.Add("Registrazione", GetType(Long))
        TabellaDoc.Columns.Add("DataRegistrazione", GetType(String))
        TabellaDoc.Columns.Add("NumeroDocumento", GetType(String))
        TabellaDoc.Columns.Add("DataDocumento", GetType(String))
        TabellaDoc.Columns.Add("NumeroProtocollo", GetType(String))
        TabellaDoc.Columns.Add("AnnoProtocollo", GetType(String))
        TabellaDoc.Columns.Add("CausaleContabile", GetType(String))
        TabellaDoc.Columns.Add("Descrizione", GetType(String))
        TabellaDoc.Columns.Add("Dare", GetType(String))
        TabellaDoc.Columns.Add("Avere", GetType(String))
        TabellaDoc.Columns.Add("Saldo", GetType(String))
        TabellaDoc.Columns.Add("Legato", GetType(String))
        TabellaDoc.Columns.Add("NumeroBolletta", GetType(String))
        TabellaDoc.Columns.Add("DataBolletta", GetType(String))

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim Saldo As Double
        Saldo = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim ImportoLegame As Double = 0
            Dim DecoCaus As New Cls_CausaleContabile

            DecoCaus.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))


            If DecoCaus.Tipo <> "I" And DecoCaus.Tipo <> "R" Then
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodicePagamento = " & campodb(myPOSTreader.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cn
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegame = Format(CDbl(IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))), "#,##0.00")
                End If
                RDRL.Close()
            Else
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodiceDocumento = " & campodb(myPOSTreader.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cn
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegame = Format(CDbl(IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))), "#,##0.00")
                End If
                RDRL.Close()
            End If

            If RD_Tutte.Checked = True Or (RD_NonLegate.Checked = True And ImportoLegame < Math.Round(campodbN(myPOSTreader.Item("Importo")), 2)) Or (RD_Legate.Checked = True And ImportoLegame = Math.Round(campodbN(myPOSTreader.Item("Importo")), 2)) Then
                Dim myriga As System.Data.DataRow = TabellaDoc.NewRow()
                myriga(0) = campodb(myPOSTreader.Item("NumeroRegistrazione"))
                If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                    myriga(1) = ""
                Else
                    myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
                End If
                myriga(2) = campodb(myPOSTreader.Item("NumeroDocumento"))
                If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                    myriga(3) = ""
                Else
                    myriga(3) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
                End If
                myriga(4) = campodb(myPOSTreader.Item("NumeroProtocollo"))
                myriga(5) = campodb(myPOSTreader.Item("AnnoProtocollo"))
                myriga(6) = DecoCaus.Descrizione
                myriga(7) = myPOSTreader.Item("Descrizione")

                If myPOSTreader.Item("DareAvere") = "D" Then
                    myriga(8) = Format(Math.Round(campodbN(myPOSTreader.Item("Importo")), 2), "#,##.00")
                    Saldo = 0 ' Saldo + Math.Round(campodbN(myPOSTreader.Item("Importo")), 2)
                    myriga(9) = ",00"
                Else
                    myriga(8) = ",00"
                    myriga(9) = Format(Math.Round(campodbN(myPOSTreader.Item("Importo")), 2), "#,##.00")
                    Saldo = 0 ' Saldo - Math.Round(campodbN(myPOSTreader.Item("Importo")), 2)
                End If

                myriga(10) = Format(Math.Round(Saldo, 2), "#,##.00")

                myriga(11) = Format(Math.Abs(Math.Round(ImportoLegame, 2)), "#,##.00")

                myriga(12) = campodb(myPOSTreader.Item("NumeroBolletta"))
                If IsDBNull(myPOSTreader.Item("DataBolletta")) Then
                    myriga(13) = ""
                Else
                    myriga(13) = Format(myPOSTreader.Item("DataBolletta"), "dd/MM/yyyy")
                End If

                TabellaDoc.Rows.Add(myriga)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        Dim i As Integer

        Saldo = 0
        For i = TabellaDoc.Rows.Count - 1 To 0 Step -1
            Saldo = Saldo + TabellaDoc.Rows(i).Item(8) - TabellaDoc.Rows(i).Item(9)
            TabellaDoc.Rows(i).Item(10) = Format(Math.Round(Saldo, 2), "#,##0.00")
        Next

        Session("DocumentiRichiamati") = TabellaDoc

        Grd_Denaro.AutoGenerateColumns = False
        Grd_Denaro.DataSource = TabellaDoc
        Grd_Denaro.DataBind()
        Call ColoreGriglia()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<INCASSO>") < 1 Then
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)


        If x.ImportoDeposito > 0 And Year(x.DataDeposito) > 1900 Then
            lbl_Deposito.Text = "<font color=""red"">Data " & Format(x.DataDeposito, "dd/MM/yyyy") & " - " & Format(x.ImportoDeposito, "#,##0.00") & "€</font>"
            If Year(x.DataAccreditoDeposito) > 1950 Then
                lbl_Deposito.Text = "Data " & Format(x.DataDeposito, "dd/MM/yyyy") & " - " & Format(x.ImportoDeposito, "#,##0.00") & "€ - Restituzione il " & Format(x.DataAccreditoDeposito, "dd/MM/yyyy")
            End If
            Lbl_LabelDeposito.Text = "Deposito :"
        End If

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim CParenti As New Cls_Parenti
        Dim COspite As New ClsOspite

        COspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        CParenti.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_OspiteParenti)
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Text = ""
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Value = 99
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Selected = True

        If Val(Request.Item("CodiceParente")) > 0 Then
            DD_OspiteParenti.SelectedValue = Val(Request.Item("CodiceParente"))
            Call CaricaGriglia(Val(Request.Item("CodiceParente")))
        Else
            Call CaricaGriglia(99)
        End If



        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))



        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO1"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO2"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO3"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO4"
        End If


    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Denaro.PageIndexChanging
        TabellaDoc = Session("DocumentiRichiamati")
        Grd_Denaro.PageIndex = e.NewPageIndex
        Grd_Denaro.DataSource = TabellaDoc
        Grd_Denaro.DataBind()
        Call ColoreGriglia()
    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Denaro.RowCommand
        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                Session("NumeroRegistrazione") = Registrazione
                'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxx('../GeneraleWeb/Documenti.aspx?NONMENU=OK');  });", True)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>DialogBoxx('../GeneraleWeb/Documenti.aspx?NONMENU=OK');</script>", False)
                Exit Sub
            End If
            If CauCon.Tipo = "P" Then
                Session("NumeroRegistrazione") = Registrazione
                'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxx('../GeneraleWeb/incassipagamenti.aspx?NONMENU=OK');  });", True)


                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>DialogBoxx('../GeneraleWeb/incassipagamenti.aspx?NONMENU=OK');</script>", False)
                Exit Sub
            End If
            Session("NumeroRegistrazione") = Registrazione
            'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxx('../GeneraleWeb/primanota.aspx?NONMENU=OK');  });", True)

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>DialogBoxx('../GeneraleWeb/primanota.aspx?NONMENU=OK');</script>", False)

            Exit Sub
        End If
        If (e.CommandName = "Printer") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)
            Session("ReportPersonalizzato") = DD_Report.SelectedValue
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('StampaRicevutaIncasso.aspx?NumeroRegistrazione=" & Registrazione & "&OspiteParenti=" & DD_OspiteParenti.SelectedValue & "','Stampe','width=800,height=600');", True)

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>openPopUp('StampaRicevutaIncasso.aspx?NumeroRegistrazione=" & Registrazione & "&OspiteParenti=" & DD_OspiteParenti.SelectedValue & "','Stampe','width=800,height=600');</script>", False)
        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Denaro.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If



        Response.Redirect("IncassiAnticipi.aspx?CodiceParente=" & DD_OspiteParenti.SelectedValue & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=INCASSIANTICIPI")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=INCASSIANTICIPI&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Deposito_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Deposito.Click
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("DepositoCauzionaleCaparra.aspx?CodiceParente=" & DD_OspiteParenti.SelectedValue & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Btn_Caparra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Caparra.Click

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("DepositoCauzionaleCaparra.aspx?CodiceParente=" & DD_OspiteParenti.SelectedValue & "&TIPO=CAPARRA" & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Btn_Documento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Documento.Click
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("CreaDocumentoRetta.aspx?CodiceParente=" & DD_OspiteParenti.SelectedValue & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Private Sub ColoreGriglia()

        Dim k As Integer
        Dim i As Integer

        For i = 0 To Grd_Denaro.Rows.Count - 1
            If Grd_Denaro.Rows(i).Cells(5).Text.Trim <> "&nbsp;" Then
                For k = 0 To 15
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Red
                Next
            End If
            If Grd_Denaro.Rows(i).Cells(5).Text.Trim = "&nbsp;" Then

                For k = 0 To 15
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Green
                Next k
            End If
        Next

    End Sub

    Protected Sub ImPanello_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImPanello.Click
        ImageButton1_Click(sender, e)
    End Sub
End Class
