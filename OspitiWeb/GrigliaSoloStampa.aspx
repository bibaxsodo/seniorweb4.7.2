﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GrigliaSoloStampa" CodeFile="GrigliaSoloStampa.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stampa</title>
    <link rel="shortcut icon" href="../images/SENIOR.ico"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:Label ID="Txt_Parametri" runat="server" Text=""></asp:Label>
            <asp:GridView ID="GridView1" runat="server" CellPadding="3"
            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" 
                BorderWidth="1px" GridLines="Vertical">
            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
            <AlternatingRowStyle BackColor="Gainsboro" />
    
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small" 
                ForeColor="White" />
        </asp:GridView>
    
    <script  type="text/javascript"  >
      window.print();
    </script>        
    </div>
    
    </form>
</body>
</html>
