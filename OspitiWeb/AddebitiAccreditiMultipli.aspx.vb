﻿
Partial Class OspitiWeb_AddebitiAccreditiMultipli
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Appoggio As String = Session("ABILITAZIONI")

        If Appoggio.IndexOf("<STR=") > 0 Then
            Dim CentroServizioAbilitato As String
            Dim Posizione As Integer = Appoggio.IndexOf("<STR=") + 5

            CentroServizioAbilitato = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)

            DD_Struttura.SelectedValue = CentroServizioAbilitato
            DD_Struttura.Enabled = False
            Call AggiornaCServ()
        End If


        If Appoggio.IndexOf("<CSERV=") > 0 Then
            Dim CentroServizioAbilitato As String
            Dim Posizione As Integer = Appoggio.IndexOf("<CSERV=") + 7

            CentroServizioAbilitato = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)

            DD_CentroServizio.SelectedValue = CentroServizioAbilitato
            DD_CentroServizio.Enabled = False
        End If



        Dim x As New Cls_Addebito

        x.UpDateDropBox(Session("DC_OSPITE"), dd_TipoAddebito, Now)


        
        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Double))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        Tabella.Rows.Add(myriga)

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        If DD_CentroServizio.SelectedValue = "" And DD_Struttura.Text = "" Then
            MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) ) {  "
            MyJs = MyJs & " $(els[i]).autocomplete('AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            MyJs = MyJs & "    }"
        Else
            If DD_CentroServizio.SelectedValue <> "" Then
                MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) ) {  "
                MyJs = MyJs & " $(els[i]).autocomplete('AutoCompleteOspiti.ashx?Utente=" & Session("UTENTE") & "&CSERV=" & DD_CentroServizio.SelectedValue & "', {delay:5,minChars:3});"
                MyJs = MyJs & "    }"
            End If
        End If

        MyJs = MyJs & "} "


        MyJs = MyJs & "$(""#" & dd_TipoAddebito.ClientID & """).chosen({"
        MyJs = MyJs & "no_results_text: ""Tipo Addebito/Accredito Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Addebito/Accredito"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub DD_CentroServizio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CentroServizio.TextChanged
        Dim KAS As New Cls_Addebito

        If IsDate(Txt_DataRegistrazione.Text) Then
            KAS.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, DD_CentroServizio.SelectedValue, Txt_DataRegistrazione.Text)
        Else
            KAS.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, DD_CentroServizio.SelectedValue)
        End If

        Call EseguiJS()
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        Tabella = ViewState("App_AddebitiMultiplo")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = ""
        myriga(1) = 0
        myriga(2) = 0
        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
        If Mt.Importo > 0 Then
            myriga(1) = 1
            myriga(2) = Mt.Importo
        End If

        Tabella.Rows.Add(myriga)

        ViewState("App_AddebitiMultiplo") = Tabella
        Grd_AggiornaRette.AutoGenerateColumns = False

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()


        Dim TxtOspite As TextBox = DirectCast(Grd_AggiornaRette.Rows(Grd_AggiornaRette.Rows.Count - 1).FindControl("Txt_Ospite"), TextBox)

        TxtOspite.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtOspite.ClientID + "').focus(); }); }, 500);", True)
        'document.body.scrollTop = document.body.scrollHeight;
    End Sub

    Protected Sub Grd_AggiornaRette_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_AggiornaRette.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub


    Private Sub UpDateTable()



        Dim i As Integer

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Double))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        For i = 0 To Grd_AggiornaRette.Rows.Count - 1

            Dim TxtOspite As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Ospite"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Importo"), TextBox)
            Dim Txt_Quantita As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Quantita"), TextBox)
            Dim Txt_Descrizione As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Descrizione"), TextBox)



            Dim myrigaR As System.Data.DataRow = Tabella.NewRow()

            myrigaR(0) = TxtOspite.Text
            myrigaR(1) = Txt_Quantita.Text
            myrigaR(2) = TxtImporto.Text
            myrigaR(3) = Txt_Descrizione.Text

            Tabella.Rows.Add(myrigaR)
        Next
        ViewState("App_AddebitiMultiplo") = Tabella
    End Sub

    Protected Sub Grd_AggiornaRette_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_AggiornaRette.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim Txt_Ospite As TextBox = DirectCast(e.Row.FindControl("Txt_Ospite"), TextBox)

            Txt_Ospite.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString



            Dim Txt_Importo As TextBox = DirectCast(e.Row.FindControl("Txt_Importo"), TextBox)

            Dim TxtQuantita As TextBox = DirectCast(e.Row.FindControl("Txt_Quantita"), TextBox)

            If Tabella.Rows(e.Row.RowIndex).Item(1).ToString <> "" Then
                Txt_Importo.Text = Format(CDbl(Tabella.Rows(e.Row.RowIndex).Item(2).ToString), "#,##0.00")
            Else
                Txt_Importo.Text = "0,00"
            End If


            Dim Txt_Descrizione As TextBox = DirectCast(e.Row.FindControl("Txt_Descrizione"), TextBox)

            Txt_Descrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString



            Dim Mt As New Cls_Addebito

            Mt.Codice = dd_TipoAddebito.SelectedValue
            Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
            If Mt.Importo > 0 Then
                TxtQuantita.Visible = True
                TxtQuantita.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString
                Txt_Importo.Width = System.Web.UI.WebControls.Unit.Percentage(50)
            Else
                TxtQuantita.Visible = False
                TxtQuantita.Text = "0,00"
                Txt_Importo.Width = System.Web.UI.WebControls.Unit.Percentage(90)
            End If


            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grd_AggiornaRette_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_AggiornaRette.RowDeleted

    End Sub

    Protected Sub Grd_AggiornaRette_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_AggiornaRette.RowDeleting

        UpDateTable()
        Tabella = ViewState("App_AddebitiMultiplo")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        myriga(3) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    myriga(3) = 0
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_AddebitiMultiplo") = Tabella

        Grd_AggiornaRette.AutoGenerateColumns = False

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click

        'If DD_CentroServizio.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
        '    Call EseguiJS()
        '    Exit Sub
        'End If

        If dd_TipoAddebito.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica tipo addebito');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If
        Session("ADDEBITIIMPORTATI") = ""

        Call UpDateTable()
        Dim i As Integer
        Dim TipoMod As String
        Dim Parametri As New Cls_Parametri

        Parametri.LeggiParametri(Session("DC_OSPITE"))


        Tabella = ViewState("App_AddebitiMultiplo")

        For i = 0 To Tabella.Rows.Count - 1
            Dim NomeOspite As String

            NomeOspite = Tabella.Rows(i).Item(0).ToString

            Dim vettore(100) As String

            vettore = SplitWords(NomeOspite)
            Dim Verifica As Boolean = False
            Try

                If (Val(vettore(0)) > 0 Or DD_CentroServizio.SelectedValue <> "") Then
                    Verifica = True
                End If

                If DD_CentroServizio.SelectedValue = "" Then
                    If Val(vettore(1)) > 0 Then
                        Verifica = True
                    End If
                End If


            Catch ex As Exception

            End Try
            If Verifica Then
                If DD_CentroServizio.SelectedValue = "" Then

                    If Val(vettore(1)) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('attenzione  ospite non trovato in riga " & i & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If

                Else
                    If Val(vettore(0)) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('attenzione  ospite non trovato in riga " & i & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('attenzione  ospite non trovato in riga " & i & "');", True)
                Call EseguiJS()
                Exit Sub
            End If
        Next

        Dim Errore As String = ""

        For i = 0 To Tabella.Rows.Count - 1
            Dim NomeOspite As String

            NomeOspite = Tabella.Rows(i).Item(0).ToString

            Dim vettore(100) As String

            vettore = SplitWords(NomeOspite)
            Dim Verifica As Boolean = False
            Try

                If (Val(vettore(0)) > 0 Or DD_CentroServizio.SelectedValue <> "") Then
                    Verifica = True
                End If

                If DD_CentroServizio.SelectedValue = "" Then
                    If Val(vettore(1)) > 0 Then
                        Verifica = True
                    End If
                End If


            Catch ex As Exception

            End Try

            If Verifica Then
                If Tabella.Rows(i).Item(2) > 0 Then
                    Dim NewAdd As New Cls_AddebitiAccrediti

                    If DD_CentroServizio.SelectedValue = "" Then

                        NewAdd.CodiceOspite = Val(vettore(1))
                        NewAdd.CENTROSERVIZIO = vettore(0)
                    Else
                        NewAdd.CodiceOspite = Val(vettore(0))
                        NewAdd.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                    End If

                    NewAdd.CodiceIva = dd_TipoAddebito.SelectedValue
                    NewAdd.TipoMov = "AD"
                    NewAdd.RIFERIMENTO = "O"
                    NewAdd.Data = Txt_DataRegistrazione.Text
                    NewAdd.Descrizione = dd_TipoAddebito.SelectedItem.Text
                    NewAdd.Elaborato = 0
                    If Chk_Ragruppa.Checked = True Then
                        NewAdd.raggruppa = 1
                    Else
                        NewAdd.raggruppa = 0
                    End If

                    If RB_FuoriRetta.Checked = True Then
                        NewAdd.RETTA = "N"
                    End If
                    If RB_Retta.Checked = True Then
                        NewAdd.RETTA = "S"
                    End If
                    If Rb_Rendiconto.Checked = True Then
                        NewAdd.RETTA = "O"
                    End If


                    TipoMod = "O"
                    Dim MiOsp As New Cls_Modalita
                    Dim CodiceParente As Integer = 0


                    Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                    KCs.CentroServizio = NewAdd.CENTROSERVIZIO
                    KCs.CodiceOspite = NewAdd.CodiceOspite
                    KCs.CodiceParente = 0
                    KCs.Leggi(Session("DC_OSPITE"))
                    If KCs.TipoAddebito1 = dd_TipoAddebito.SelectedValue Then
                        TipoMod = "C"
                    End If
                    If KCs.TipoAddebito2 = dd_TipoAddebito.SelectedValue Then
                        TipoMod = "C"
                    End If
                    If KCs.TipoAddebito3 = dd_TipoAddebito.SelectedValue Then
                        TipoMod = "C"
                    End If
                    If KCs.TipoAddebito4 = dd_TipoAddebito.SelectedValue Then
                        TipoMod = "C"
                    End If
                    If TipoMod = "C" Then
                        Dim m As New Cls_ImportoComune

                        m.CODICEOSPITE = NewAdd.CodiceOspite
                        m.CENTROSERVIZIO = NewAdd.CENTROSERVIZIO
                        m.UltimaData(Session("DC_OSPITE"), m.CODICEOSPITE, m.CENTROSERVIZIO)

                        NewAdd.PROVINCIA = m.PROV
                        NewAdd.COMUNE = m.COMUNE
                    End If

                    Dim CalcolaRette As New Cls_CalcoloRette
                    CalcolaRette.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                    CalcolaRette.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                    If CalcolaRette.QuoteGiornaliere(NewAdd.CENTROSERVIZIO, NewAdd.CodiceOspite, "P", 1, Now) > 0 Then
                        TipoMod = "P"
                        CodiceParente = 1
                    End If
                    If CalcolaRette.QuoteGiornaliere(NewAdd.CENTROSERVIZIO, NewAdd.CodiceOspite, "P", 2, Now) > 0 Then
                        TipoMod = "P"
                        CodiceParente = 2
                    End If
                    If CalcolaRette.QuoteGiornaliere(NewAdd.CENTROSERVIZIO, NewAdd.CodiceOspite, "P", 3, Now) > 0 Then
                        TipoMod = "P"
                        CodiceParente = 3
                    End If
                    If CalcolaRette.QuoteGiornaliere(NewAdd.CENTROSERVIZIO, NewAdd.CodiceOspite, "P", 4, Now) > 0 Then
                        TipoMod = "P"
                        CodiceParente = 4
                    End If
                    If CalcolaRette.QuoteGiornaliere(NewAdd.CENTROSERVIZIO, NewAdd.CodiceOspite, "P", 5, Now) > 0 Then
                        TipoMod = "P"
                        CodiceParente = 5
                    End If
                    CalcolaRette.ChiudiDB()

                    NewAdd.RIFERIMENTO = TipoMod
                    NewAdd.PARENTE = CodiceParente

                    NewAdd.Descrizione = Tabella.Rows(i).Item(3).ToString
                    If NewAdd.Descrizione = "" And Parametri.TipoAddebitoFatturePrivati = 0 Then
                        NewAdd.Descrizione = dd_TipoAddebito.SelectedItem.Text
                    End If

                    NewAdd.MESECOMPETENZA = Dd_Mese.SelectedValue
                    NewAdd.ANNOCOMPETENZA = Txt_Anno.Text
                    NewAdd.Utente = Session("UTENTE")
                    NewAdd.Quantita = Tabella.Rows(i).Item(1)
                    NewAdd.IMPORTO = Tabella.Rows(i).Item(2)
                    Dim ClOspite As New ClsOspite

                    Try
                        NewAdd.InserisciAddebito(Session("DC_OSPITE"))

                        ClOspite.Nome = ""
                        If NewAdd.CodiceOspite > 0 Then
                            ClOspite.Leggi(Session("DC_OSPITE"), NewAdd.CodiceOspite)
                        End If
                        NewAdd.Descrizione = Trim(Mid(NewAdd.Descrizione & Space(20), 1, 20)) & "..."
                        Session("ADDEBITIIMPORTATI") = Session("ADDEBITIIMPORTATI") & "<tr><td>" & NewAdd.CENTROSERVIZIO & "</td><td>" & NewAdd.CodiceOspite & "</td><td>" & ClOspite.Nome & "</td><td>" & NewAdd.Descrizione & "</td><td>" & NewAdd.IMPORTO & "</td><td></td></tr>"
                    Catch ex As Exception


                        ClOspite.Nome = ""
                        If NewAdd.CodiceOspite > 0 Then
                            ClOspite.Leggi(Session("DC_OSPITE"), NewAdd.CodiceOspite)
                        End If
                        NewAdd.Descrizione = Trim(Mid(NewAdd.Descrizione & Space(20), 1, 20)) & "..."
                        Session("ADDEBITIIMPORTATI") = Session("ADDEBITIIMPORTATI") & "<tr><td>" & NewAdd.CENTROSERVIZIO & "</td><td>" & NewAdd.CodiceOspite & "</td><td>" & ClOspite.Nome & "</td><td>" & NewAdd.Descrizione & "</td><td>" & NewAdd.IMPORTO & "</td><td><font color=""red"">ERRORE NON CARICATO</font></td></tr>"
                    End Try

                End If
                If Tabella.Rows(i).Item(2) < 0 Then
                    Dim NewAdd As New Cls_AddebitiAccrediti

                    NewAdd.CodiceOspite = Val(vettore(0))
                    NewAdd.CENTROSERVIZIO = NewAdd.CENTROSERVIZIO
                    NewAdd.CodiceIva = dd_TipoAddebito.SelectedValue
                    NewAdd.TipoMov = "AC"
                    NewAdd.RIFERIMENTO = "O"
                    NewAdd.Data = Txt_DataRegistrazione.Text
                    NewAdd.Descrizione = dd_TipoAddebito.SelectedItem.Text
                    NewAdd.Elaborato = 0
                    If Chk_Ragruppa.Checked = True Then
                        NewAdd.raggruppa = 1
                    Else
                        NewAdd.raggruppa = 0
                    End If
                    If RB_FuoriRetta.Checked = True Then
                        NewAdd.RETTA = "N"
                    End If
                    If RB_Retta.Checked = True Then
                        NewAdd.RETTA = "S"
                    End If
                    If Rb_Rendiconto.Checked = True Then
                        NewAdd.RETTA = "O"
                    End If

                    TipoMod = "O"
                    Dim MiOsp As New Cls_Modalita
                    Dim CodiceParente As Integer = 0

                    MiOsp.CentroServizio = NewAdd.CENTROSERVIZIO
                    MiOsp.CodiceOspite = NewAdd.CodiceOspite
                    MiOsp.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio)
                    If MiOsp.MODALITA = "P" Then
                        TipoMod = "P"
                        CodiceParente = 1
                    End If

                    Dim MyPar As New Cls_ImportoParente

                    MyPar.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 1)
                    If MyPar.IMPORTO > 0 Then
                        TipoMod = "P"
                        CodiceParente = 1
                    End If

                    Dim MyPar1 As New Cls_ImportoParente

                    MyPar1.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 2)
                    If MyPar1.IMPORTO > 0 Then
                        TipoMod = "P"
                        CodiceParente = 2
                    End If

                    Dim MyPar2 As New Cls_ImportoParente

                    MyPar2.UltimaData(Session("DC_OSPITE"), MiOsp.CodiceOspite, MiOsp.CentroServizio, 3)
                    If MyPar2.IMPORTO > 0 Then
                        TipoMod = "P"
                        CodiceParente = 3
                    End If


                    NewAdd.RIFERIMENTO = TipoMod
                    NewAdd.PARENTE = CodiceParente
                    NewAdd.Descrizione = Tabella.Rows(i).Item(3).ToString
                    If NewAdd.Descrizione = "" And Parametri.TipoAddebitoFatturePrivati = 0 Then
                        NewAdd.Descrizione = dd_TipoAddebito.SelectedItem.Text
                    End If


                    NewAdd.MESECOMPETENZA = Dd_Mese.SelectedValue
                    NewAdd.ANNOCOMPETENZA = Txt_Anno.Text
                    NewAdd.Utente = Session("UTENTE")
                    NewAdd.Quantita = Tabella.Rows(i).Item(1)
                    NewAdd.IMPORTO = Math.Abs(Tabella.Rows(i).Item(2))
                    Dim ClOspite As New ClsOspite

                    Try
                        NewAdd.InserisciAddebito(Session("DC_OSPITE"))

                        ClOspite.Nome = ""
                        If NewAdd.CodiceOspite > 0 Then
                            ClOspite.Leggi(Session("DC_OSPITE"), NewAdd.CodiceOspite)
                        End If
                        NewAdd.Descrizione = Trim(Mid(NewAdd.Descrizione & Space(20), 1, 20)) & "..."
                        Session("ADDEBITIIMPORTATI") = Session("ADDEBITIIMPORTATI") & "<tr><td>" & NewAdd.CENTROSERVIZIO & "</td><td>" & NewAdd.CodiceOspite & "</td><td>" & ClOspite.Nome & "</td><td>" & NewAdd.Descrizione & "</td><td>" & NewAdd.IMPORTO & "</td><td></td></tr>"
                    Catch ex As Exception

                        ClOspite.Nome = ""
                        If NewAdd.CodiceOspite > 0 Then
                            ClOspite.Leggi(Session("DC_OSPITE"), NewAdd.CodiceOspite)
                        End If
                        NewAdd.Descrizione = Trim(Mid(NewAdd.Descrizione & Space(20), 1, 20)) & "..."
                        Session("ADDEBITIIMPORTATI") = Session("ADDEBITIIMPORTATI") & "<tr><td>" & NewAdd.CENTROSERVIZIO & "</td><td>" & NewAdd.CodiceOspite & "</td><td>" & ClOspite.Nome & "</td><td>" & NewAdd.Descrizione & "</td><td>" & NewAdd.IMPORTO & "</td><td><font color=""red"">ERRORE NON CARICATO</font></td></tr>"
                    End Try
                End If
            End If
        Next



        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Double))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        Tabella.Rows.Add(myriga)

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()

        If Errore <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Errore & "');", True)
            Call EseguiJS()
            Exit Sub
        End If

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxx('AddebitiImportati.aspx');  });", True)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Txt_DataRegistrazione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DataRegistrazione.TextChanged
        Dim KAS As New Cls_Addebito

        If IsDate(Txt_DataRegistrazione.Text) Then
            KAS.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, DD_CentroServizio.SelectedValue, Txt_DataRegistrazione.Text)
        Else
            KAS.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, DD_CentroServizio.SelectedValue)
        End If
    End Sub


    Protected Sub Txt_Quantita_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To Grd_AggiornaRette.Rows.Count - 1
            If sender.clientid = Grd_AggiornaRette.Rows(I).Cells(3).Controls.Item(1).ClientID Then
                riga = I


            End If
        Next

        Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(riga).FindControl("Txt_Importo"), TextBox)
        Dim TxtQuantita As TextBox = DirectCast(Grd_AggiornaRette.Rows(riga).FindControl("Txt_Quantita"), TextBox)

        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
        If Mt.Importo > 0 Then
            TxtImporto.Text = Format(Mt.Importo * Val(TxtQuantita.Text), "#,##0.00")
        End If

        Call EseguiJS()
    End Sub

    Protected Sub dd_TipoAddebito_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_TipoAddebito.SelectedIndexChanged
        Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(0).FindControl("Txt_Importo"), TextBox)
        Dim TxtQuantita As TextBox = DirectCast(Grd_AggiornaRette.Rows(0).FindControl("Txt_Quantita"), TextBox)

        Dim Mt As New Cls_Addebito

        Mt.Codice = dd_TipoAddebito.SelectedValue
        Mt.Leggi(Session("DC_OSPITE"), Mt.Codice)
        If Mt.Importo > 0 Then
            TxtQuantita.Visible = True
            TxtQuantita.Text = "1,00"
            TxtImporto.Text = Format(Mt.Importo * Val(TxtQuantita.Text), "#,##0.00")
        Else
            TxtQuantita.Visible = False
            TxtQuantita.Text = "0,00"
        End If

        Call EseguiJS()
    End Sub
End Class
