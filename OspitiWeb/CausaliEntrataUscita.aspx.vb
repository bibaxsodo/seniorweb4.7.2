﻿
Partial Class OspitiWeb_CausaliEntrataUscita
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim k As New Cls_TabellaRaggruppamento

        k.UpDateDropBox(Session("DC_OSPITE"), DD_Raggruppamento)

        Call Pulisci()

        If Request.Item("CODICE") <> "" Then

            RB_Assente.Checked = False
            RB_Presente.Checked = False
            RB_CalcolaImp.Checked = False
            RB_NonCalcolato.Checked = False

            RB_TMAssente.Checked = False
            RB_TMCalcolaImp.Checked = False
            RB_TMNonContare.Checked = False
            RB_TMPresente.Checked = False

            Dim X As New Cls_CausaliEntrataUscita


            X.Codice = Request.Item("CODICE")
            X.LeggiCausale(Session("DC_OSPITE"))

            Txt_Codice.Text = X.Codice
            Txt_Codice.Enabled = False
            Txt_Descrizione.Text = X.Descrizione
            Txt_Regola.Text = X.Regole

            If X.GiornoUscita = "P" Then
                RB_Presente.Checked = True
            End If
            If X.GiornoUscita = "A" Then
                RB_Assente.Checked = True
            End If
            If X.GiornoUscita = "N" Then
                RB_NonCalcolato.Checked = True
            End If
            If X.GiornoUscita = "" Then
                RB_CalcolaImp.Checked = True
            End If

            If X.TIPOMOVIMENTO = "P" Then
                RB_TMPresente.Checked = True
            End If
            If X.TIPOMOVIMENTO = "A" Then
                RB_TMAssente.Checked = True
            End If

            If X.TIPOMOVIMENTO = "N" Then
                RB_TMNonContare.Checked = True
            End If
            If X.TIPOMOVIMENTO = "" Then
                RB_TMCalcolaImp.Checked = True
            End If
            If X.Raggruppamento = "" Then
                DD_Raggruppamento.SelectedValue = "99"
            Else
                DD_Raggruppamento.SelectedValue = X.Raggruppamento
            End If

            ViewState("SetCausale") = X.Diurno
            If X.Diurno = 0 Then
                Img_ColoreDiurno.ImageUrl = "images/Diurno_Blanco.png"
            Else
                Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
            End If

        End If
    End Sub
    Private Sub Pulisci()
        Txt_Codice.Text = ""
        Txt_Descrizione.Text = ""
        Txt_Regola.Text = "Sub Calcolo()" & vbNewLine & "End Sub" & vbNewLine
        DD_Raggruppamento.SelectedValue = "99"
        RB_Assente.Checked = True
        RB_Presente.Checked = False
        RB_CalcolaImp.Checked = False
        RB_NonCalcolato.Checked = False

        RB_TMAssente.Checked = True
        RB_TMCalcolaImp.Checked = False
        RB_TMNonContare.Checked = False
        RB_TMPresente.Checked = False
    End Sub





    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then

            Dim m As New Cls_CausaliEntrataUscita

            m.Codice = Txt_Codice.Text
            m.LeggiCausale(Session("DC_OSPITE"))

            If m.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già usato');", True)
                Exit Sub
            End If
        End If

        Dim X As New Cls_CausaliEntrataUscita


        X.Codice = Txt_Codice.Text
        X.Descrizione = Txt_Descrizione.Text
        X.Regole = Txt_Regola.Text

        If RB_Presente.Checked = True Then
            X.GiornoUscita = "P"
        End If
        If RB_Assente.Checked = True Then        
            X.GiornoUscita = "A"
        End If
        If RB_NonCalcolato.Checked = True Then
            X.GiornoUscita = "N"
        End If
        If RB_CalcolaImp.Checked = True Then
            X.GiornoUscita = ""
        End If

        If RB_TMPresente.Checked = True Then
            X.TIPOMOVIMENTO = "P"
        End If
        If RB_TMAssente.Checked = True Then
            X.TIPOMOVIMENTO = "A"
        End If

        If RB_TMNonContare.Checked = True Then
            X.TIPOMOVIMENTO = "N"
        End If
        If RB_TMCalcolaImp.Checked = True Then
            X.TIPOMOVIMENTO = ""
        End If
        If DD_Raggruppamento.SelectedValue = "99" Then
            X.Raggruppamento = ""
        Else
            X.Raggruppamento = DD_Raggruppamento.SelectedValue
        End If


        X.Diurno = Val(ViewState("SetCausale"))


        X.CheckCausale = X.CheckCausaleFunction(X.Regole)
        X.Utente = Session("UTENTE")
        X.Scrivi(Session("DC_OSPITE"))

        Img_VerificaDes.ImageUrl = "~/images/Blanco.png"

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Exit Sub
        End If

        Dim X As New Cls_CausaliEntrataUscita

        X.Codice = Txt_Codice.Text

        If X.VerificaElimina(Session("DC_OSPITE")) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare causale utilizzata');", True)
            Exit Sub
        End If

        X.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoCausali.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True
        Img_VerificaDes.ImageUrl = "~/images/Blanco.png"
        If Txt_Descrizione.Text.Trim <> "" Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CausaliEntrataUscita

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text
            x.LeggiCausaleDescrizione(Session("DC_OSPITE"))

            If x.Codice <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text.Trim <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_CausaliEntrataUscita

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiCausaleDescrizione(Session("DC_OSPITE"))

                If x.Codice <> "" Then
                    Img_VerificaDes.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub Img_Causale1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale1.Click
        ViewState("SetCausale") = 1
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub



    Protected Sub Img_Causale11_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale11.Click
        ViewState("SetCausale") = 11
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub


    Protected Sub Img_Causale12_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale12.Click
        ViewState("SetCausale") = 12
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale13_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale13.Click
        ViewState("SetCausale") = 13
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale14_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale14.Click
        ViewState("SetCausale") = 14
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale15_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale15.Click
        ViewState("SetCausale") = 15
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale16_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale16.Click
        ViewState("SetCausale") = 16
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub


    Protected Sub Img_Causale17_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale17.Click
        ViewState("SetCausale") = 17
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale18_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale18.Click
        ViewState("SetCausale") = 18
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale19_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale19.Click
        ViewState("SetCausale") = 19
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale20_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale20.Click
        ViewState("SetCausale") = 20
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale2.Click
        ViewState("SetCausale") = 2
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale3.Click
        ViewState("SetCausale") = 3
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale4.Click
        ViewState("SetCausale") = 4
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale5.Click
        ViewState("SetCausale") = 5
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale6.Click
        ViewState("SetCausale") = 6
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale7_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale7.Click
        ViewState("SetCausale") = 7
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale8_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale8.Click
        ViewState("SetCausale") = 8
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale9_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale9.Click
        ViewState("SetCausale") = 9
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale10_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale10.Click
        ViewState("SetCausale") = 10
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub
    Protected Sub Img_Causale21_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale21.Click
        ViewState("SetCausale") = 21
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub
    Protected Sub Img_Causale22_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale22.Click
        ViewState("SetCausale") = 22
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub
    Protected Sub Img_Causale23_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale23.Click
        ViewState("SetCausale") = 23
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub
    Protected Sub Img_Causale24_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale24.Click
        ViewState("SetCausale") = 24
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale25_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale25.Click
        ViewState("SetCausale") = 25
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale26_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale26.Click
        ViewState("SetCausale") = 26
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale27_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale27.Click
        ViewState("SetCausale") = 27
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale28_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale28.Click
        ViewState("SetCausale") = 28
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale29_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale29.Click
        ViewState("SetCausale") = 29
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale30_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale30.Click
        ViewState("SetCausale") = 30
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale31_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale31.Click
        ViewState("SetCausale") = 31
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale32_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale32.Click
        ViewState("SetCausale") = 32
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale33_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale33.Click
        ViewState("SetCausale") = 33
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale34_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale34.Click
        ViewState("SetCausale") = 34
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale35_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale35.Click
        ViewState("SetCausale") = 35
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale36_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale36.Click
        ViewState("SetCausale") = 36
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub


    Protected Sub Img_Causale37_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale37.Click
        ViewState("SetCausale") = 37
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale38_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale38.Click
        ViewState("SetCausale") = 38
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale39_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale39.Click
        ViewState("SetCausale") = 39
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale40_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale40.Click
        ViewState("SetCausale") = 40
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale41_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale41.Click
        ViewState("SetCausale") = 41
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale42_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale42.Click
        ViewState("SetCausale") = 42
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale43_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale43.Click
        ViewState("SetCausale") = 43
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale44_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale44.Click
        ViewState("SetCausale") = 44
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale45_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale45.Click
        ViewState("SetCausale") = 45
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale46_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale46.Click
        ViewState("SetCausale") = 46
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale47_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale47.Click
        ViewState("SetCausale") = 47
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"

    End Sub

    Protected Sub Img_Causale48_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale48.Click
        ViewState("SetCausale") = 48
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale49_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale49.Click
        ViewState("SetCausale") = 49
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale50_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale50.Click
        ViewState("SetCausale") = 50
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale51_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale51.Click
        ViewState("SetCausale") = 51
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale52_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale52.Click
        ViewState("SetCausale") = 52
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale53_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale53.Click
        ViewState("SetCausale") = 53
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale54_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale54.Click
        ViewState("SetCausale") = 54
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale55_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale55.Click
        ViewState("SetCausale") = 55
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale56_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale56.Click
        ViewState("SetCausale") = 56
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale57_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale57.Click
        ViewState("SetCausale") = 57
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale58_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale58.Click
        ViewState("SetCausale") = 58
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale59_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale59.Click
        ViewState("SetCausale") = 59
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale60_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale60.Click
        ViewState("SetCausale") = 60
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale61_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale61.Click
        ViewState("SetCausale") = 61
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale62_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale62.Click
        ViewState("SetCausale") = 62
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale63_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale63.Click
        ViewState("SetCausale") = 63
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale64_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale64.Click
        ViewState("SetCausale") = 64
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale65_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale65.Click
        ViewState("SetCausale") = 65
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Img_Causale66_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Causale66.Click
        ViewState("SetCausale") = 66
        Img_ColoreDiurno.ImageUrl = "images/Diurno_" & ViewState("SetCausale") & ".png"
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then            
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CausaliEntrataUscita

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.LeggiCausale(Session("DC_OSPITE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If        
    End Sub
End Class