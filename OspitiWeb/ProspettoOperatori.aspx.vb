﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_ProspettoOperatori
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Operatore')!= null) || (appoggio.match('Txt_Operatore')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutoComplateOperatore.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_StatisticheDomiciliari.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim RsRagr As New Cls_RaggruppamentoOperatori

        RsRagr.UpDateDropBox(Session("DC_OSPITE"), DD_Raggruppamento)

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")


        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))

        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub


    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
       


        Call ProspettoOperatori(False)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Private Function QueryOperatoriUtentiChiusi() As String
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim Sql As String
        


        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        QueryOperatoriUtentiChiusi = ""


        Sql = "Select * From Movimenti where Data >= ? And Data <= ? And TipoMov = '13' Order by CodiceOspite"
        Dim cmd As New OleDbCommand()
        cmd.CommandText = Sql
        cmd.Parameters.AddWithValue("@Data", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@Data", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim ReaderUtentiUsciti As OleDbDataReader = cmd.ExecuteReader()
        Do While ReaderUtentiUsciti.Read

            Dim Accoglimento As Date


            Sql = "Select * From Movimenti where Data <= ? And TipoMov = '05' And CodiceOspite = ? And CentroServizio = ? Order by Data Desc"
            Dim cmdAccoglimento As New OleDbCommand()
            cmdAccoglimento.CommandText = Sql
            cmdAccoglimento.Parameters.AddWithValue("@Data", campodbd(ReaderUtentiUsciti.Item("Data")))
            cmdAccoglimento.Parameters.AddWithValue("@CodiceOspite", campodbN(ReaderUtentiUsciti.Item("CodiceOspite")))
            cmdAccoglimento.Parameters.AddWithValue("@CentroServizio", campodb(ReaderUtentiUsciti.Item("CentroServizio")))
            cmdAccoglimento.Connection = cn
            Dim ReaderAccoglimento As OleDbDataReader = cmdAccoglimento.ExecuteReader()
            If ReaderAccoglimento.Read Then
                Accoglimento = campodbd(ReaderAccoglimento.Item("Data"))
            End If
            ReaderAccoglimento.Close()


            Sql = "Select * From [MovimentiDomiciliare] where Data >= ? And Data <?  And CodiceOspite =  ? and (select top  1 FatturazioneFinePeriodo  from TipoDomiciliare where TipoDomiciliare.Codice = MovimentiDomiciliare.Tipologia) = 1"
            Dim cmdPrestazioni As New OleDbCommand

            cmdPrestazioni.CommandText = Sql
            cmdPrestazioni.Parameters.AddWithValue("@Data", Accoglimento)
            cmdPrestazioni.Parameters.AddWithValue("@Data", campodbd(ReaderUtentiUsciti.Item("Data")))
            cmdPrestazioni.Parameters.AddWithValue("@CodiceOspite", campodbN(ReaderUtentiUsciti.Item("CodiceOspite")))
            cmdPrestazioni.Connection = cn
            Dim Prestazioni As OleDbDataReader = cmdPrestazioni.ExecuteReader()
            Do While Prestazioni.Read
                If QueryOperatoriUtentiChiusi <> "" Then
                    QueryOperatoriUtentiChiusi = QueryOperatoriUtentiChiusi & " OR "
                End If
                QueryOperatoriUtentiChiusi = QueryOperatoriUtentiChiusi & " [MovimentiDomiciliare].ID = " & campodbN(Prestazioni.Item("ID"))
            Loop
            Prestazioni.Close()



        Loop

        ReaderUtentiUsciti.Close()
        cn.Close()



    End Function

    Private Sub ProspettoOperatori(ByVal excel As Boolean)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim ContaOspiti As Integer = 0



        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("CodiceOperatore", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Tipo Intervento", GetType(String))
        MyTable.Columns.Add("Descrizione Tipo Intervento", GetType(String))
        MyTable.Columns.Add("Totale Ore (Cent/s)/Accessi", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Proforma", GetType(String))

        Dim VettoreCodice(500) As String
        Dim VettoreImporto(500) As Double
        Dim VettoreDescrizione(500) As String
        Dim VettoreTipo(500) As String
        Dim VettoreTipologia(500) As String
        Dim VettoreOre(500) As Double
        Dim MaxAddebito As Integer = 0
        Dim OldOperatore As Integer = 0
        Dim Sql As String



        If Txt_Operatore.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Operatore.Text)
            If Not IsNothing(Vettore(0)) Then
                If Val(Vettore(0)) > 0 Then

                    Condizione = Condizione & " And "

                    Condizione = Condizione & "  CodiceOperatore = " & Val(Vettore(0))
                End If
            End If
        End If

        If DD_Raggruppamento.SelectedValue <> "" Then
            Condizione = Condizione & " And "

            Condizione = Condizione & "  (Select TOP  1 TipoOspite From AnagraficaComune Where CodiceMedico  = MovimentiDomiciliare_Operatori.CodiceOperatore And AnagraficaComune.Tipologia = 'A') = '" & DD_Raggruppamento.SelectedValue & "' "
        End If

        If Chk_EmOspiteParente.Checked = True Then
            Condizione = Condizione & " And "

            Condizione = Condizione & "  (Select TOP  1 EmOspiteParente From AnagraficaComune Where CodiceMedico  = MovimentiDomiciliare_Operatori.CodiceOperatore And AnagraficaComune.Tipologia = 'A') = 1"
        End If

        If DD_CServ.SelectedValue <> "" Then
            Condizione = Condizione & " And "

            Condizione = Condizione & "  CentroServizio = '" & DD_CServ.SelectedValue & "'"
        End If

        Condizione = Condizione & " And "

        Condizione = Condizione & "  ((select top  1 FatturazioneFinePeriodo  from TipoDomiciliare where TipoDomiciliare.Codice = MovimentiDomiciliare.Tipologia) = 0 or (select top  1 FatturazioneFinePeriodo  from TipoDomiciliare where TipoDomiciliare.Codice = MovimentiDomiciliare.Tipologia) is null)"


        If QueryOperatoriUtentiChiusi() <> "" Then
            Condizione = Condizione & " ) OR "


            Condizione = Condizione & "(" & QueryOperatoriUtentiChiusi() & ")"
            Sql = "Select * From MovimentiDomiciliare INNER JOIN  MovimentiDomiciliare_Operatori ON [MovimentiDomiciliare].Id =  MovimentiDomiciliare_Operatori.IdMovimento Where (data >= ? And Data <= ? " & Condizione & "  Order by CodiceOperatore,Tipologia "

        Else
            Sql = "Select * From MovimentiDomiciliare INNER JOIN  MovimentiDomiciliare_Operatori ON [MovimentiDomiciliare].Id =  MovimentiDomiciliare_Operatori.IdMovimento Where data >= ? And Data <= ? " & Condizione & "  Order by CodiceOperatore,Tipologia "

        End If

        Dim cmd As New OleDbCommand()
        cmd.CommandText = Sql
        cmd.Parameters.AddWithValue("@Data", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@Data", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()
        Do While ReaderAssenzeCentroDiurno.Read

            If OldOperatore <> Val(campodb(ReaderAssenzeCentroDiurno.Item("CodiceOperatore"))) And OldOperatore <> 0 Then
                Dim Indice As Integer
                Dim Codice As String = ""
                Dim TotaleOre As Double
                Dim TotaleImporto As Double

                Dim TotaleOreOperatore As Double = 0
                Dim TotaleImportoOperatore As Double = 0

                For Indice = 0 To MaxAddebito
                    If Codice <> VettoreTipologia(Indice) And Codice <> "" Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = OldOperatore

                        Dim Operatore As New Cls_Operatore

                        Operatore.CodiceMedico = OldOperatore
                        Operatore.Leggi(Session("DC_OSPITE"))

                        myriga(1) = Operatore.Nome

                        myriga(2) = Codice
                        Dim DeEn As New Cls_TipoDomiciliare
                        DeEn.Codice = Codice
                        DeEn.Leggi(Session("DC_OSPITE"), DeEn.Codice)
                        myriga(3) = DeEn.Descrizione

                        myriga(4) = Math.Round(TotaleOre / 60, 2)
                        myriga(5) = Format(TotaleImporto, "#,##0.00")

                        MyTable.Rows.Add(myriga)
                        TotaleOre = 0
                        TotaleImporto = 0
                    End If

                    Codice = VettoreTipologia(Indice)
                    If VettoreTipo(Indice) = "TI" Then
                        TotaleOre = TotaleOre + (VettoreOre(Indice) * 60)
                        TotaleOreOperatore = TotaleOreOperatore + VettoreOre(Indice) * 60
                    Else
                        TotaleOre = TotaleOre + VettoreOre(Indice)
                        TotaleOreOperatore = TotaleOreOperatore + VettoreOre(Indice)
                    End If
                    TotaleImporto = TotaleImporto + VettoreImporto(Indice)


                    TotaleImportoOperatore = TotaleImportoOperatore + VettoreImporto(Indice)
                Next
                If TotaleOre > 0 Or TotaleImporto > 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = OldOperatore

                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = OldOperatore
                    Operatore.Leggi(Session("DC_OSPITE"))

                    myriga(1) = Operatore.Nome

                    myriga(2) = Codice
                    Dim DeEn As New Cls_TipoDomiciliare
                    DeEn.Codice = Codice
                    DeEn.Leggi(Session("DC_OSPITE"), DeEn.Codice)
                    myriga(3) = DeEn.Descrizione

                    myriga(4) = Math.Round(TotaleOre / 60, 2)
                    myriga(5) = Format(TotaleImporto, "#,##0.00")

                    MyTable.Rows.Add(myriga)
                    TotaleOre = 0
                    TotaleImporto = 0
                End If

                If TotaleImportoOperatore > 0 Or TotaleOreOperatore > 0 Then
                    Dim myriga1 As System.Data.DataRow = MyTable.NewRow()

                    myriga1(1) = ""

                    myriga1(2) = ""
                    myriga1(3) = "Totale "

                    myriga1(4) = Math.Round(TotaleOreOperatore / 60, 2)
                    myriga1(5) = Format(TotaleImportoOperatore, "#,##0.00")
                    myriga1(6) = "FATTURA"

                    MyTable.Rows.Add(myriga1)
                End If
                For Indice = 0 To MaxAddebito
                    VettoreCodice(Indice) = ""
                    VettoreTipologia(Indice) = ""
                    VettoreImporto(Indice) = 0
                    VettoreDescrizione(Indice) = ""
                    VettoreTipo(Indice) = ""
                    VettoreOre(Indice) = 0
                Next
                MaxAddebito = 0
            End If
            Dim Minuti As Integer
            Minuti = DateDiff("n", campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio")), campodbd(ReaderAssenzeCentroDiurno.Item("OraFine")))

            Call CreaAddebitiDomicliari(campodb(ReaderAssenzeCentroDiurno.Item("Tipologia")), Minuti, VettoreTipo, VettoreCodice, VettoreDescrizione, VettoreImporto, VettoreOre, VettoreTipologia, MaxAddebito, campodbN(ReaderAssenzeCentroDiurno.Item("CreatoDocumentoOspiti")), campodb(ReaderAssenzeCentroDiurno.Item("CentroServizio")), campodbN(ReaderAssenzeCentroDiurno.Item("CodiceOspite")), campodbd(ReaderAssenzeCentroDiurno.Item("Data")), Val(campodb(ReaderAssenzeCentroDiurno.Item("Operatore"))), Val(campodb(ReaderAssenzeCentroDiurno.Item("IDmovimento"))))

            OldOperatore = Val(campodb(ReaderAssenzeCentroDiurno.Item("CodiceOperatore")))
        Loop
        ReaderAssenzeCentroDiurno.Close()

        If MaxAddebito > 0 Then
            Dim Indice As Integer
            Dim Codice As String = ""
            Dim TotaleOre As Double
            Dim TotaleImporto As Double
            Dim TotaleOreOperatore As Double = 0
            Dim TotaleImportoOperatore As Double = 0

            For Indice = 0 To MaxAddebito
                If Codice <> VettoreTipologia(Indice) And Codice <> "" Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = OldOperatore

                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = OldOperatore
                    Operatore.Leggi(Session("DC_OSPITE"))

                    myriga(1) = Operatore.Nome

                    myriga(2) = Codice
                    Dim DeEn As New Cls_TipoDomiciliare
                    DeEn.Codice = Codice
                    DeEn.Leggi(Session("DC_OSPITE"), DeEn.Codice)
                    myriga(3) = DeEn.Descrizione

                    myriga(4) = Math.Round(TotaleOre / 60, 2)
                    myriga(5) = Format(TotaleImporto, "#,##0.00")

                    MyTable.Rows.Add(myriga)
                    TotaleOre = 0
                    TotaleImporto = 0
                End If

                Codice = VettoreTipologia(Indice)

                If VettoreTipo(Indice) = "TI" Then
                    TotaleOre = TotaleOre + VettoreOre(Indice) * 60
                    TotaleOreOperatore = TotaleOreOperatore + VettoreOre(Indice) * 60
                Else
                    TotaleOre = TotaleOre + VettoreOre(Indice)
                    TotaleOreOperatore = TotaleOreOperatore + VettoreOre(Indice)
                End If
                TotaleImporto = TotaleImporto + VettoreImporto(Indice)


                TotaleImportoOperatore = TotaleImportoOperatore + VettoreImporto(Indice)
            Next
            If TotaleOre > 0 Or TotaleImporto > 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = OldOperatore

                Dim Operatore As New Cls_Operatore

                Operatore.CodiceMedico = OldOperatore
                Operatore.Leggi(Session("DC_OSPITE"))

                myriga(1) = Operatore.Nome

                myriga(2) = Codice
                Dim DeEn As New Cls_TipoDomiciliare
                DeEn.Codice = Codice
                DeEn.Leggi(Session("DC_OSPITE"), DeEn.Codice)
                myriga(3) = DeEn.Descrizione

                
                myriga(4) = Math.Round(TotaleOre / 60, 2)
                myriga(5) = Format(TotaleImporto, "#,##0.00")

                MyTable.Rows.Add(myriga)
                TotaleOre = 0
                TotaleImporto = 0
            End If

            If TotaleImportoOperatore > 0 Or TotaleOreOperatore > 0 Then
                Dim myriga2 As System.Data.DataRow = MyTable.NewRow()

                myriga2(1) = ""

                myriga2(2) = ""
                myriga2(3) = "Totale "

                myriga2(4) = Math.Round(TotaleOreOperatore / 60, 2)
                myriga2(5) = Format(TotaleImportoOperatore, "#,##0.00")

                myriga2(6) = "FATTURA"
                MyTable.Rows.Add(myriga2)
            End If

            For Indice = 0 To MaxAddebito
                VettoreCodice(Indice) = ""
                VettoreTipologia(Indice) = ""
                VettoreImporto(Indice) = 0
                VettoreDescrizione(Indice) = ""
                VettoreTipo(Indice) = ""
                VettoreOre(Indice) = 0
            Next
            MaxAddebito = 0
        End If



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



        Session("MYTABLE") = MyTable

        If Not excel Then
            Dim GeneraleCon As OleDbConnection


            GeneraleCon = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            GeneraleCon.Open()



            Dim i As Integer
            OldOperatore = 0
            For i = 0 To GridView1.Rows.Count - 1

                Dim jk As Integer


                GridView1.Rows(i).Cells(1).Text = "<b>" & GridView1.Rows(i).Cells(1).Text & "</b>"
                If GridView1.Rows(i).Cells(6).Text = "FATTURA" Then
                    Dim SOper As New Cls_Operatore

                    SOper.CodiceMedico = OldOperatore
                    SOper.Leggi(Session("DC_OSPITE"))

                    If SOper.TipoOperatore <> "" Then
                        MySql = "Select DataDocumento,NumeroDocumento  FROM [MovimentiContabiliTesta]  where (select COUNT(*) from MovimentiContabiliRiga  Where MovimentiContabiliRiga.MastroPartita = ? and MovimentiContabiliRiga.ContoPartita = ? and MovimentiContabiliRiga.SottocontoPartita = ? AND RigaDaCausale = 1 And MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) > 0 AND AnnoProtocollo > 0 Order by DataRegistrazione Desc"
                        Dim cmdVerificaTA As New OleDbCommand()
                        cmdVerificaTA.CommandText = MySql
                        cmdVerificaTA.Connection = GeneraleCon
                        cmdVerificaTA.Parameters.AddWithValue("@MastroFornitore", SOper.MastroFornitore)
                        cmdVerificaTA.Parameters.AddWithValue("@ContoFornitore", SOper.ContoFornitore)
                        cmdVerificaTA.Parameters.AddWithValue("@SottoContoFornitore", SOper.SottoContoFornitore)
                        Dim ReadVerificaTA As OleDbDataReader = cmdVerificaTA.ExecuteReader()
                        If ReadVerificaTA.Read Then
                            GridView1.Rows(i).Cells(1).Text = "<i>" & campodb(ReadVerificaTA.Item("DataDocumento")) & " " & campodb(ReadVerificaTA.Item("NumeroDocumento")) & "</i>"
                        End If
                        ReadVerificaTA.Close()

                        GridView1.Rows(i).Cells(3).Text = "<font color=""#007DC4""><b>" & GridView1.Rows(i).Cells(3).Text & "</b></font>"
                        GridView1.Rows(i).Cells(4).Text = "<font color=""#007DC4""><b>" & GridView1.Rows(i).Cells(4).Text & "</b></font>"
                        GridView1.Rows(i).Cells(5).Text = "<font color=""#007DC4""><b>" & GridView1.Rows(i).Cells(5).Text & "</b></font>"
                        GridView1.Rows(i).Cells(6).Text = "<a href=""#"" onclick=""DialogBox('FatturaDomiciliare.aspx?RIGA=" & i & "');"" ><img src=""../images/elabora.png"" title=""CREA FATTURA"" /a></a>"
                    End If
                End If
                OldOperatore = Val(GridView1.Rows(i).Cells(0).Text)
            Next


            GeneraleCon.Close()


            If DD_Raggruppamento.SelectedValue <> "" Then
                lbl_fattura.Text = "<label class=""LabelCampo"">Crea proforma per raggruppamento :</label><a href=""#"" onclick=""DialogBox('FatturaDomiciliare.aspx?RIGA=ALL');"" ><img src=""../images/elabora.png"" title=""CREA PROFORMA""></a><br />"
            End If
        End If
    End Sub

    Private Sub CreaAddebitiDomicliari(ByVal Codice As String, ByVal Minuti As Integer, ByRef VettoreTipo() As String, ByRef VettoreCodice() As String, ByRef VettoreDescrizione() As String, ByRef VettoreImporto() As Double, ByRef VettoreOre() As Double, ByRef VettoreTipologia() As String, ByRef MaxAddebito As Integer, ByVal CreatoDocumentoOspiti As Integer, ByVal Cserv As String, ByVal CodOsp As Integer, ByVal Data As Date, ByVal NumeroOperatori As Integer, ByVal ID As Integer)
        Dim X As Integer
        Dim STRINGACONNESSIONEDB As String = Session("DC_OSPITE")
        Dim OspitiCon As OleDbConnection
        Dim ContaOspiti As Integer = 0

        OspitiCon = New Data.OleDb.OleDbConnection(STRINGACONNESSIONEDB)

        OspitiCon.Open()



        Dim cmdVerificaTA As New OleDbCommand()
        cmdVerificaTA.CommandText = ("select * from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmdVerificaTA.Parameters.AddWithValue("@CodiceTipoDomiciliare", Codice)
        cmdVerificaTA.Connection = OspitiCon
        Dim ReadVerificaTA As OleDbDataReader = cmdVerificaTA.ExecuteReader()
        Do While ReadVerificaTA.Read
            Dim InserisciAdAC As Boolean = True
            If campodb(ReadVerificaTA.Item("TipoRetta")) <> "" Then
                Dim Ospite As New Cls_rettatotale

                Ospite.CENTROSERVIZIO = Cserv
                Ospite.CODICEOSPITE = CodOsp
                Ospite.Data = Data
                Ospite.UltimaData(STRINGACONNESSIONEDB, Ospite.CODICEOSPITE, Ospite.CENTROSERVIZIO)
                If campodb(ReadVerificaTA.Item("TipoRetta")) <> Ospite.TipoRetta Then
                    InserisciAdAC = False
                End If
            End If


            If campodb(ReadVerificaTA.Item("TipoOperatore")) <> "" Then
                Dim MovDom As New Cls_MovimentiDomiciliare_Operatori

                MovDom.IdMovimento = ID
                If Not MovDom.LegggiTipoOperatore(STRINGACONNESSIONEDB, campodb(ReadVerificaTA.Item("TipoOperatore"))) Then
                    InserisciAdAC = False
                End If
            End If


            If campodbN(ReadVerificaTA.Item("NumeroOperatori")) > 0 Then
                If campodbN(ReadVerificaTA.Item("NumeroOperatori")) = 1 And NumeroOperatori <> 1 Then
                    InserisciAdAC = False
                End If
                If campodbN(ReadVerificaTA.Item("NumeroOperatori")) = 2 And NumeroOperatori = 1 Then
                    InserisciAdAC = False
                End If
            End If



            If InserisciAdAC = True Then
                If CreatoDocumentoOspiti = 0 Then
                    If campodb(ReadVerificaTA.Item("Tipologia")) = "I" Then
                        If campodb(ReadVerificaTA.Item("Ripartizione")) = "T" Then
                            Dim DesT As New Cls_Addebito

                            DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                            DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                            Dim Trovato As Boolean = False
                            For X = 1 To MaxAddebito
                                If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "TI" Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                    VettoreDescrizione(X) = DesT.Descrizione
                                    VettoreCodice(X) = DesT.Codice
                                    VettoreTipologia(X) = Codice
                                    VettoreOre(X) = VettoreOre(X) + 1
                                    Trovato = True
                                End If
                            Next
                            If Trovato = False Then
                                MaxAddebito = MaxAddebito + 1
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                                VettoreOre(MaxAddebito) = VettoreOre(MaxAddebito) + 1

                                VettoreDescrizione(MaxAddebito) = DesT.Descrizione
                                VettoreCodice(MaxAddebito) = DesT.Codice
                                VettoreTipologia(MaxAddebito) = Codice
                                VettoreTipo(MaxAddebito) = "TI"
                            End If
                        End If
                    End If



                    If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Or campodb(ReadVerificaTA.Item("Tipologia")) = "M" Or campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                        If campodb(ReadVerificaTA.Item("Ripartizione")) = "T" Then
                            Dim DesT As New Cls_Addebito
                            Dim UnitaTempo As Integer

                            If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Then
                                UnitaTempo = Math.Round(Minuti / 60)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "M" Then
                                UnitaTempo = Math.Round(Minuti / 30)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                                UnitaTempo = Math.Round(Minuti / 15)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                                UnitaTempo = Math.Round(Minuti / 5)
                            End If

                            DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                            DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                            Dim Trovato As Boolean = False
                            For X = 1 To MaxAddebito
                                If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "O" Then
                                    If UnitaTempo = 1 Then
                                        VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                    End If
                                    If UnitaTempo = 2 Then
                                        VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                    End If
                                    If UnitaTempo > 2 Then
                                        VettoreImporto(X) = VettoreImporto(X) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                    End If
                                    VettoreOre(X) = VettoreOre(X) + Minuti
                                    VettoreDescrizione(X) = DesT.Descrizione
                                    VettoreCodice(X) = DesT.Codice
                                    VettoreTipologia(X) = Codice
                                    Trovato = True
                                End If
                            Next
                            If Trovato = False Then
                                MaxAddebito = MaxAddebito + 1
                                If UnitaTempo = 1 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                                End If
                                If UnitaTempo = 2 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If
                                If UnitaTempo > 2 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If

                                VettoreOre(MaxAddebito) = VettoreOre(MaxAddebito) + Minuti
                                VettoreDescrizione(MaxAddebito) = DesT.Descrizione
                                VettoreCodice(MaxAddebito) = DesT.Codice
                                VettoreTipologia(MaxAddebito) = Codice
                                VettoreTipo(MaxAddebito) = "T"
                            End If
                        End If
                    End If
                End If
            End If
        Loop
        ReadVerificaTA.Close()

        OspitiCon.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        Dim Indice As Integer
        Dim IndiceT As Integer
        Dim Riga As Integer
        Dim Totale As Double
        Dim CodiceOperatore As Integer
        Dim VtTipo(100) As String
        Dim VtOre(100) As Double
        Dim VtImporto(100) As Double
        Dim X As Integer = 0

        Dim NumeroProgressivo As Integer = 0

        If IsNothing(Session("MYTABLE")) Then
            Exit Sub
        End If

        MyTable = Session("MYTABLE")



        Dim Stampa As New StampeGenerale


        Dim MyRecordStampaT As System.Data.DataRow

        Dim MyRecordStampaR As System.Data.DataRow




        For Riga = MyTable.Rows.Count - 1 To 0 Step -1

            If Val(campodb(MyTable.Rows(Riga).Item(0))) = 0 Then
                Totale = MyTable.Rows(Riga).Item(5).ToString
                IndiceT = 0
                For X = 0 To 200
                    CodiceOperatore = 0
                    VtTipo(IndiceT) = ""
                    VtOre(IndiceT) = 0
                    VtImporto(IndiceT) = 0
                Next


                For Indice = Riga - 1 To 0 Step -1
                    If Val(campodb(MyTable.Rows(Indice).Item(0))) > 0 Then
                        CodiceOperatore = Val(MyTable.Rows(Indice).Item(0))
                        VtTipo(IndiceT) = MyTable.Rows(Indice).Item(2)
                        VtOre(IndiceT) = MyTable.Rows(Indice).Item(4)
                        VtImporto(IndiceT) = MyTable.Rows(Indice).Item(5)
                        IndiceT = IndiceT + 1
                    Else
                        Exit For
                    End If
                Next

                If CodiceOperatore > 0 Then
                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = CodiceOperatore
                    Operatore.Leggi(Session("DC_OSPITE"))

                    Dim TipoOper As New Cls_TipoOperatore

                    TipoOper.Codice = Operatore.TipoOperatore
                    TipoOper.Leggi(Session("DC_OSPITE"), TipoOper.Codice)


                    Dim TipoRaggr As New Cls_RaggruppamentoOperatori

                    TipoRaggr.Codice = Operatore.RaggruppamentoOperatori
                    TipoRaggr.Leggi(Session("DC_OSPITE"), TipoRaggr.Codice)


                    If TipoRaggr.Codice = "" Or TipoOper.Codice = "" Then
                        Exit For
                    End If

                    MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow

                    Dim CliFor As New Cls_ClienteFornitore


                    CliFor.CODICEDEBITORECREDITORE = 0
                    CliFor.MastroFornitore = Operatore.MastroFornitore
                    CliFor.ContoFornitore = Operatore.ContoFornitore
                    CliFor.SottoContoFornitore = Operatore.SottoContoFornitore

                    CliFor.Leggi(Session("DC_OSPITE"))


                    MyRecordStampaT.Item("RagioneSociale") = CliFor.Nome
                    MyRecordStampaT.Item("Indirizzo") = CliFor.RESIDENZAINDIRIZZO1
                    MyRecordStampaT.Item("CAP") = CliFor.RESIDENZACAP1

                    Dim Comune As New ClsComune

                    Comune.Provincia = CliFor.RESIDENZAPROVINCIA1
                    Comune.Comune = CliFor.RESIDENZACOMUNE1
                    Comune.Leggi(Session("DC_OSPITE"))

                    MyRecordStampaT.Item("Localita") = Comune.Descrizione


                    Dim Provincia As New ClsComune

                    Provincia.Provincia = CliFor.RESIDENZAPROVINCIA1
                    Provincia.Comune = ""
                    Provincia.Leggi(Session("DC_OSPITE"))

                    MyRecordStampaT.Item("Provincia") = Provincia.CodificaProvincia

                    NumeroProgressivo = NumeroProgressivo + 1
                    MyRecordStampaT.Item("NumeroFattura") = NumeroProgressivo

                    MyRecordStampaT.Item("DataFattura") = Format(Now, "dd/MM/yyyy")

                    If CliFor.PARTITAIVA > 0 Then
                        MyRecordStampaT.Item("PartitaIVACodiceFiscale") = CliFor.PARTITAIVA
                    Else
                        MyRecordStampaT.Item("PartitaIVACodiceFiscale") = CliFor.CodiceFiscale
                    End If



                    Dim CauIva As New Cls_IVA


                    CauIva.Codice = TipoRaggr.CodiceIVA
                    CauIva.Leggi(Session("DC_TABELLE"), CauIva.Codice)

                    MyRecordStampaT.Item("Imposta1") = 0
                    MyRecordStampaT.Item("AliquotaIVA1") = ""
                    MyRecordStampaT.Item("Imponibile1") = 0


                    MyRecordStampaT.Item("Imposta2") = 0
                    MyRecordStampaT.Item("AliquotaIVA2") = ""
                    MyRecordStampaT.Item("Imponibile2") = 0

                    MyRecordStampaT.Item("Imposta3") = 0
                    MyRecordStampaT.Item("AliquotaIVA3") = ""
                    MyRecordStampaT.Item("Imponibile3") = 0


                    MyRecordStampaT.Item("Imposta4") = 0
                    MyRecordStampaT.Item("AliquotaIVA4") = ""
                    MyRecordStampaT.Item("Imponibile4") = 0


                    MyRecordStampaT.Item("Imposta5") = 0
                    MyRecordStampaT.Item("AliquotaIVA5") = ""
                    MyRecordStampaT.Item("Imponibile5") = 0

                    Dim CassaProfe As Double = 0
                    If TipoRaggr.Mastro > 0 And TipoRaggr.Percentuale > 0 Then
                        CassaProfe = Math.Round(Totale * TipoRaggr.Percentuale / 100, 2)

                        Totale = Totale + CassaProfe
                    End If


                    MyRecordStampaT.Item("Imposta1") = Math.Round(Totale * CauIva.Aliquota, 2)
                    MyRecordStampaT.Item("AliquotaIVA1") = TipoRaggr.CodiceIVA & " " & CauIva.Descrizione
                    MyRecordStampaT.Item("Imponibile1") = Totale

                    If TipoRaggr.CodiceRitenuta <> "" Then
                        Dim TipoRitenuto As New ClsRitenuta

                        TipoRitenuto.Codice = TipoRaggr.CodiceRitenuta
                        TipoRitenuto.Leggi(Session("DC_TABELLE"))

                        MyRecordStampaT.Item("Ritenuta1") = TipoRaggr.CodiceRitenuta
                        MyRecordStampaT.Item("ImportoRitenuta1") = Math.Round(Totale * TipoRitenuto.Percentuale, 2)
                    End If


                    For X = 0 To IndiceT - 1

                        MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow


                        MyRecordStampaR.Item("NumeroFattura") = NumeroProgressivo
                        MyRecordStampaR.Item("DataFattura") = Format(Now, "dd/MM/yyyy")

                        Dim tp As New Cls_TipoDomiciliare

                        tp.Codice = VtTipo(X)
                        tp.Descrizione = ""
                        tp.Leggi(Session("DC_OSPITE"), tp.Codice)




                        MyRecordStampaR.Item("DescrizioneRiga") = tp.Descrizione
                        MyRecordStampaR.Item("Quantita") = VtOre(X) * 100
                        MyRecordStampaR.Item("Importo") = VtImporto(X)
                        MyRecordStampaR.Item("CodiceIVA") = TipoRaggr.CodiceIVA

                        Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
                    Next

                    If CassaProfe > 0 Then
                        MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow


                        MyRecordStampaR.Item("NumeroFattura") = NumeroProgressivo
                        MyRecordStampaR.Item("DataFattura") = Format(Now, "dd/MM/yyyy")

                        Dim tp As New Cls_Pianodeiconti

                        tp.Mastro = TipoRaggr.Mastro
                        tp.Conto = TipoRaggr.Conto
                        tp.Sottoconto = TipoRaggr.Sottoconto
                        tp.Decodfica(Session("DC_GENERALE"))


                        MyRecordStampaR.Item("DescrizioneRiga") = tp.Descrizione
                        MyRecordStampaR.Item("Quantita") = 100
                        MyRecordStampaR.Item("Importo") = CassaProfe
                        MyRecordStampaR.Item("CodiceIVA") = TipoRaggr.CodiceIVA

                        Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
                    End If

                    Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)
                End If
            End If

        Next


        Session("stampa") = Stampa
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=PROFORMAOPERATORI&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        EseguiJS()

    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If



        Call ProspettoOperatori(True)

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ProspettoOperatore.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub
End Class
