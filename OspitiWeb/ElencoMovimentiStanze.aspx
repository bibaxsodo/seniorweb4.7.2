﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ElencoMovimentiStanze" CodeFile="ElencoMovimentiStanze.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Elenco Movimento Stanza</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');
            $("#pippo").css('visibility', 'visible');
        }
        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });
    </script>

    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 200px;
            height: 200px;
            position: absolute;
            right: 0%;
            top: 200px;
            margin-right: 70px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <div id="blur" style="visibility: hidden;">&nbsp;</div>
            <div id="pippo" style="visibility: hidden;" class="wait">
                <div style="text-align: left; display: inline-table;">
                    <a href="#" onclick="Chiudi();">
                        <img src="../images/annulla.png" title="Chiudi" /></a>
                </div>
                <br />
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="Btn_Occupazione" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Occupazione Stanza" />
                        </td>
                        <td>Occupazione </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="Btn_Liberazione" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Liberazione Stanza" />
                        </td>
                        <td>Liberazione
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Ospiti - Elenco Movimenti Stanza</div>
                    <div class="SottoTitoloOSPITE">
                        <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right; vertical-align: top;">
                    <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </td>
            </tr>
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                    <table width="100%">
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="right">
                                <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    <div id="MENUDIV"></div>
                    <br />
                </td>
                <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                    <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                        <a href="#" onclick="VisualizzaDivRegistrazione();">
                            <img src="../images/nuovo.png" title="Nuovo Movimento" /></a><br />
                    </div>
                    <asp:GridView ID="Grd_Denaro" runat="server" CellPadding="3"
                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                        BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                        ShowFooter="True" PageSize="20">
                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%#   Container.DataItemIndex  %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
