﻿
Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_SituazioneContabileCentroServizio
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Private Sub CaricaGriglia()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")
        Dim StringaConnessioneGenerale As String = Session("DC_GENERALE")


        Dim MySql As String = ""
        Dim Saldo As Double

        Dim DisponibileSuDocumenti As Double


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cnGenerale As OleDbConnection

        cnGenerale = New Data.OleDb.OleDbConnection(StringaConnessioneGenerale)

        cnGenerale.Open()


        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(StringaConnessioneTabelle)

        cnTabelle.Open()

        Dim Mastro As Long = 0
        Dim Conto As Long = 0
        Dim SottoConto As Long = 0

        Dim CServizio As New Cls_CentroServizio

        CServizio.Leggi(ConnectionString, Cmb_CServ.SelectedValue)
        Mastro = CServizio.MASTRO
        Conto = CServizio.CONTO
        SottoConto = 0


        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Ospite", GetType(String))        
        MyTable.Columns.Add("Intestatario", GetType(String))
        MyTable.Columns.Add("CodiceFiscale", GetType(String))
        MyTable.Columns.Add("Indirizzo", GetType(String))
        MyTable.Columns.Add("Comune", GetType(String))
        MyTable.Columns.Add("Cap", GetType(String))
        MyTable.Columns.Add("Mese", GetType(String))
        MyTable.Columns.Add("Anno", GetType(String))
        MyTable.Columns.Add("NumeroRegistrazione", GetType(String))
        MyTable.Columns.Add("NumeroDocumento", GetType(String))
        MyTable.Columns.Add("DataDocumento", GetType(String))
        MyTable.Columns.Add("CausaleContabile", GetType(String))
        MyTable.Columns.Add("Importo Documento", GetType(String))
        MyTable.Columns.Add("Importo Legato", GetType(String))
        MyTable.Columns.Add("Giorno", GetType(String))

        Dim NumeroDocumento As String
        Dim DataDocumento As String
        Dim CausaleContabile As String
        Dim TipoDocumento As String
        If Chk_DataRegistrazione.Checked = False Then
            If Dd_Mese.SelectedValue = 13 Then
                MySql = "Select *  FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero =  MovimentiContabiliTesta.NumeroRegistrazione WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Val(Txt_Anno.Text) & "  and MovimentiContabiliTesta.CentroServizio = '" & Cmb_CServ.SelectedValue & "' And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & "  Order by SottocontoPartita,DataRegistrazione"
            Else
                MySql = "Select *  FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero =  MovimentiContabiliTesta.NumeroRegistrazione WHERE MovimentiContabiliTesta.MeseCompetenza = " & Dd_Mese.SelectedValue & " and MovimentiContabiliTesta.AnnoCompetenza = " & Val(Txt_Anno.Text) & "  and MovimentiContabiliTesta.CentroServizio = '" & Cmb_CServ.SelectedValue & "' And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & "  Order by SottocontoPartita,DataRegistrazione"
            End If
        Else
            MySql = "Select *  FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero =  MovimentiContabiliTesta.NumeroRegistrazione WHERE MovimentiContabiliTesta.DataRegistrazione >= ? And MovimentiContabiliTesta.DataRegistrazione <= ? And MovimentiContabiliTesta.CentroServizio = '" & Cmb_CServ.SelectedValue & "' And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & "  Order by SottocontoPartita,DataRegistrazione"
        End If
        Dim cmdCC As New OleDbCommand()
        cmdCC.CommandText = (MySql)
        cmdCC.Connection = cnGenerale
        If Chk_DataRegistrazione.Checked = True Then
            If Dd_Mese.SelectedValue = 13 Then
                cmdCC.Parameters.AddWithValue("@DataDal", DateSerial(Val(Txt_Anno.Text), 1, 1))
                cmdCC.Parameters.AddWithValue("@DataAl", DateSerial(Val(Txt_Anno.Text), 12, 31))
            Else
                cmdCC.Parameters.AddWithValue("@DataDal", DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, 1))
                cmdCC.Parameters.AddWithValue("@DataAl", DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text))))
            End If
        End If
        Dim RdCC As OleDbDataReader = cmdCC.ExecuteReader()
        Do While RdCC.Read()
            Dim TotImp As Double
            Dim ImportoLegato As Double
            Dim Giorni As Integer = 0

            If campodb(RdCC.Item("NumeroDocumento")) = "" Then
                NumeroDocumento = 0
            Else
                NumeroDocumento = campodb(RdCC.Item("NumeroDocumento"))
            End If

            DataDocumento = campodb(RdCC.Item("DataDocumento"))
            If Not IsDate(DataDocumento) Then
                DataDocumento = campodb(RdCC.Item("DataRegistrazione"))
            End If

            Dim Mov As New Cls_MovimentoContabile


            Mov.NumeroRegistrazione = campodb(RdCC.Item("NumeroRegistrazione"))
            Mov.Leggi(StringaConnessioneGenerale, Mov.NumeroRegistrazione)

            TotImp = Mov.ImportoDocumento(StringaConnessioneTabelle)

            Giorni = 0
            MySql = "Select sum(quantita) as totimp From MovimentiContabiliRiga Where  Numero = " & campodb(RdCC.Item("NumeroRegistrazione"))
            Dim cmdG As New OleDbCommand()
            cmdG.CommandText = (MySql)
            cmdG.Connection = cnGenerale

            Dim RdG As OleDbDataReader = cmdG.ExecuteReader()
            Do While RdG.Read
                If IsDBNull(RdG.Item("totimp")) Then
                    Giorni = 0
                Else
                    Giorni = RdG.Item("totimp")
                End If
            Loop
            RdG.Close()


            CausaleContabile = campodb(RdCC.Item("CausaleContabile"))
            TipoDocumento = ""

            MySql = "Select * From CausaliContabiliTesta Where Codice = '" & CausaleContabile & "'"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = (MySql)
            cmdCau.Connection = cnTabelle

            Dim RdCau As OleDbDataReader = cmdCau.ExecuteReader()
            If RdCau.Read Then
                TipoDocumento = campodb(RdCau.Item("TipoDocumento"))
                If campodb(RdCau.Item("Tipo")) = "P" Then
                    TipoDocumento = "INC"
                    If Mov.Righe(0).DareAvere = "A" Then
                        TotImp = TotImp * -1
                    Else
                        TotImp = TotImp
                    End If

                End If
            End If
            RdCau.Close()



            If TipoDocumento = "INC" Then
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodicePagamento = " & campodb(RdCC.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()
            Else
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodiceDocumento = " & campodb(RdCC.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()
            End If

            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            Dim OspiteParente As String = ""

            If Int(campodb(RdCC.Item("SottocontoPartita")) / 100) = campodb(RdCC.Item("SottocontoPartita")) / 100 Then
                OspiteParente = "O"
            Else
                OspiteParente = "P"
            End If

            Dim MP As New Cls_Pianodeiconti
            MP.Mastro = Mastro
            MP.Conto = Conto
            MP.Sottoconto = Int(campodb(RdCC.Item("SottocontoPartita")) / 100) * 100
            MP.Decodfica(StringaConnessioneGenerale)
            myriga(0) = MP.Descrizione

            If OspiteParente = "P" Then
                MP.Mastro = Mastro
                MP.Conto = Conto
                MP.Sottoconto = campodb(RdCC.Item("SottocontoPartita"))
                MP.Decodfica(StringaConnessioneGenerale)
                myriga(1) = MP.Descrizione
            Else
                myriga(1) = ""
            End If

            If OspiteParente = "O" Then
                Dim DatiOsp As New ClsOspite

                DatiOsp.Leggi(Session("DC_OSPITE"), Int(campodb(RdCC.Item("SottocontoPartita")) / 100))

                myriga(2) = DatiOsp.CODICEFISCALE
                myriga(3) = DatiOsp.RESIDENZAINDIRIZZO1
                myriga(5) = DatiOsp.RESIDENZACAP1

                Dim DatiComune As New ClsComune

                DatiComune.Comune = DatiOsp.RESIDENZACOMUNE1
                DatiComune.Provincia = DatiOsp.RESIDENZAPROVINCIA1
                DatiComune.DecodficaComune(Session("DC_OSPITE"))

                myriga(4) = DatiComune.Descrizione & " " & DatiComune.CodificaProvincia
            End If

            If OspiteParente = "P" Then
                Dim DatiOsp As New Cls_Parenti

                DatiOsp.Leggi(Session("DC_OSPITE"), Int(campodb(RdCC.Item("SottocontoPartita")) / 100), campodb(RdCC.Item("SottocontoPartita")) - (Int(campodb(RdCC.Item("SottocontoPartita")) / 100) * 100))

                myriga(2) = DatiOsp.CODICEFISCALE
                myriga(3) = DatiOsp.RESIDENZAINDIRIZZO1
                myriga(5) = DatiOsp.RESIDENZACAP1

                Dim DatiComune As New ClsComune

                DatiComune.Comune = DatiOsp.RESIDENZACOMUNE1
                DatiComune.Provincia = DatiOsp.RESIDENZAPROVINCIA1
                DatiComune.DecodficaComune(Session("DC_OSPITE"))

                myriga(4) = DatiComune.Descrizione & " " & DatiComune.CodificaProvincia
            End If



            myriga(6) = campodb(RdCC.Item("MeseCompetenza"))
            myriga(7) = campodb(RdCC.Item("AnnoCompetenza"))
            myriga(8) = campodb(RdCC.Item("NumeroRegistrazione"))
            myriga(9) = NumeroDocumento
            myriga(10) = DataDocumento

            Dim CauCont As New Cls_CausaleContabile
            CauCont.Codice = CausaleContabile
            CauCont.Leggi(StringaConnessioneTabelle, CausaleContabile)
            myriga(11) = CauCont.Descrizione

            If TotImp > 0 Then
                myriga(12) = Format(TotImp, "#,##0.00")
                myriga(13) = Format(ImportoLegato, "#,##0.00")
                myriga(14) = Giorni
            Else
                If TipoDocumento = "INC" Then
                    myriga(12) = "-" & Format(TotImp, "#,##0.00")
                Else
                    myriga(12) = Format(TotImp, "#,##0.00")
                End If

                myriga(13) = Format(ImportoLegato, "#,##0.00")
                myriga(14) = Giorni
            End If
            MyTable.Rows.Add(myriga)
        Loop
        RdCC.Close()

        cn.Close()
        cnGenerale.Close()
        cnTabelle.Close()



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"


        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "


        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"        

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton3_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton3.Click

        If Dd_Mese.SelectedValue = 14 Then
            Call CaricaGrigliaCompetenze()
        Else
            Call CaricaGriglia()
        End If

        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Dd_Mese.SelectedValue = 14 Then
            Call CaricaGrigliaCompetenze()
        Else
            Call CaricaGriglia()
        End If


        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & Cmb_CServ.Text & "_Periodo_" & Dd_Mese.SelectedItem.Text & "_" & Txt_Anno.Text & ".xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub



    Private Sub CaricaGrigliaCompetenze()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")
        Dim StringaConnessioneGenerale As String = Session("DC_GENERALE")


        Dim MySql As String = ""
        

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cnGenerale As OleDbConnection

        cnGenerale = New Data.OleDb.OleDbConnection(StringaConnessioneGenerale)

        cnGenerale.Open()


        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(StringaConnessioneTabelle)

        cnTabelle.Open()

        Dim Mastro As Long = 0
        Dim Conto As Long = 0
        Dim SottoConto As Long = 0

        Dim CServizio As New Cls_CentroServizio

        CServizio.Leggi(ConnectionString, Cmb_CServ.SelectedValue)
        Mastro = CServizio.MASTRO
        Conto = CServizio.CONTO
        SottoConto = 0


        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice Ospite", GetType(String))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Gennaio", GetType(String))
        MyTable.Columns.Add("Febbraio", GetType(String))
        MyTable.Columns.Add("Marzo", GetType(String))
        MyTable.Columns.Add("Aprile", GetType(String))
        MyTable.Columns.Add("Maggio", GetType(String))
        MyTable.Columns.Add("Giugno", GetType(String))
        MyTable.Columns.Add("Luglio", GetType(String))
        MyTable.Columns.Add("Agosto", GetType(String))
        MyTable.Columns.Add("Settembre", GetType(String))
        MyTable.Columns.Add("Ottobre", GetType(String))
        MyTable.Columns.Add("Novembre", GetType(String))
        MyTable.Columns.Add("Dicembre", GetType(String))


        Dim NumeroDocumento As String
        Dim DataDocumento As String
        Dim CausaleContabile As String
        Dim TipoDocumento As String
        If Chk_DataRegistrazione.Checked = False Then
            MySql = "Select *  FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero =  MovimentiContabiliTesta.NumeroRegistrazione WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Val(Txt_Anno.Text) & "  and MovimentiContabiliTesta.CentroServizio = '" & Cmb_CServ.SelectedValue & "' And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & "  Order by SottocontoPartita,DataRegistrazione"        
        Else
            MySql = "Select *  FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero =  MovimentiContabiliTesta.NumeroRegistrazione WHERE MovimentiContabiliTesta.DataRegistrazione >= ? And MovimentiContabiliTesta.DataRegistrazione <= ? And MovimentiContabiliTesta.CentroServizio = '" & Cmb_CServ.SelectedValue & "' And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & "  Order by SottocontoPartita,DataRegistrazione"
        End If
        Dim cmdCC As New OleDbCommand()
        cmdCC.CommandText = (MySql)
        cmdCC.Connection = cnGenerale
        If Chk_DataRegistrazione.Checked = True Then
            cmdCC.Parameters.AddWithValue("@DataDal", DateSerial(Val(Txt_Anno.Text), 1, 1))
            cmdCC.Parameters.AddWithValue("@DataAl", DateSerial(Val(Txt_Anno.Text), 12, 31))
        End If
        Dim RdCC As OleDbDataReader = cmdCC.ExecuteReader()
        Do While RdCC.Read()
            Dim TotImp As Double
            Dim ImportoLegato As Double
            Dim Giorni As Integer = 0

            If campodb(RdCC.Item("NumeroDocumento")) = "" Then
                NumeroDocumento = 0
            Else
                NumeroDocumento = campodb(RdCC.Item("NumeroDocumento"))
            End If

            DataDocumento = campodb(RdCC.Item("DataDocumento"))
            If Not IsDate(DataDocumento) Then
                DataDocumento = campodb(RdCC.Item("DataRegistrazione"))
            End If

            Dim Mov As New Cls_MovimentoContabile


            Mov.NumeroRegistrazione = campodb(RdCC.Item("NumeroRegistrazione"))
            Mov.Leggi(StringaConnessioneGenerale, Mov.NumeroRegistrazione)

            TotImp = Mov.ImportoDocumento(StringaConnessioneTabelle)

            CausaleContabile = campodb(RdCC.Item("CausaleContabile"))
            TipoDocumento = ""

            MySql = "Select * From CausaliContabiliTesta Where Codice = '" & CausaleContabile & "'"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = (MySql)
            cmdCau.Connection = cnTabelle

            Dim RdCau As OleDbDataReader = cmdCau.ExecuteReader()
            If RdCau.Read Then
                TipoDocumento = campodb(RdCau.Item("TipoDocumento"))
                If campodb(RdCau.Item("Tipo")) = "P" Then
                    TipoDocumento = "INC"
                    If Mov.Righe(0).DareAvere = "A" Then
                        TotImp = TotImp * -1
                    Else
                        TotImp = TotImp
                    End If
                End If
            End If
            RdCau.Close()



            If TipoDocumento = "INC" Then
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodicePagamento = " & campodb(RdCC.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()
            Else
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodiceDocumento = " & campodb(RdCC.Item("NumeroRegistrazione"))
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()
            End If



            Dim OspiteParente As String = ""

            If Int(campodb(RdCC.Item("SottocontoPartita")) / 100) = campodb(RdCC.Item("SottocontoPartita")) / 100 Then
                OspiteParente = "O"
            Else
                OspiteParente = "P"
            End If

            Dim MP As New Cls_Pianodeiconti
            MP.Mastro = Mastro
            MP.Conto = Conto
            MP.Sottoconto = Int(campodb(RdCC.Item("SottocontoPartita")) / 100) * 100
            MP.Decodfica(StringaConnessioneGenerale)


            Dim Trovato As Boolean = False
            Dim x As Integer

            For x = 0 To MyTable.Rows.Count - 1
                If MyTable.Rows(x).Item(0) = Int(campodb(RdCC.Item("SottocontoPartita")) / 100) Then
                    If campodb(RdCC.Item("AnnoCompetenza")) = Txt_Anno.Text Then
                        Dim Mese As Integer = campodb(RdCC.Item("MeseCompetenza"))
                        MyTable.Rows(x).Item(1 + Mese) = Math.Round(campodbn(MyTable.Rows(x).Item(1 + Mese)) + TotImp, 2)
                        Trovato = True
                        Exit For
                    End If
                End If
            Next
            If Not Trovato Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = Int(campodb(RdCC.Item("SottocontoPartita")) / 100)
                myriga(1) = MP.Descrizione

                If campodb(RdCC.Item("AnnoCompetenza")) = Txt_Anno.Text Then
                    Dim Mese As Integer = campodb(RdCC.Item("MeseCompetenza"))
                    myriga(1 + Mese) = Math.Round(TotImp, 2)
                End If

                MyTable.Rows.Add(myriga)
            End If
        Loop
        RdCC.Close()



        Dim cmd As New OleDbCommand()

        MySql = "Select * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "') > 0 OR " & _
                                                        " (" & _
                                                        " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                        " And " & _
                                                        " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                        " )" & _
                                                        " ) order by Nome"


        cmd.CommandText = MySql

        Dim Txt_DataDalText As Date = DateSerial(Val(Txt_Anno.Text), 1, 1)
        Dim Txt_DataAlText As Date = DateSerial(Val(Txt_Anno.Text), 12, 31)


        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Trovato As Boolean = False
            Dim x As Integer

            For x = 0 To MyTable.Rows.Count - 1
                If MyTable.Rows(x).Item(0) = Int(campodb(myPOSTreader.Item("CodiceOspite"))) Then
                    Trovato = True
                    Exit For
                End If
            Next
            If Trovato = False Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = Int(campodb(myPOSTreader.Item("CodiceOspite")))

                Dim MP As New ClsOspite


                MP.CodiceOspite = Int(campodb(myPOSTreader.Item("CodiceOspite")))
                MP.Leggi(Session("DC_OSPITE"), MP.CodiceOspite)
                myriga(1) = MP.Nome & "*"

                MyTable.Rows.Add(myriga)

            End If
        Loop
        myPOSTreader.Close()

        cn.Close()
        cnGenerale.Close()
        cnTabelle.Close()



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub
End Class
