﻿
Partial Class NuovoOspite
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim MyJS As String

        MyJS = "$('#" & Txt_DataAccoglimento.ClientID & "').mask(""99/99/9999"");"

        MyJS = MyJS & " $('#" & Txt_DataAccoglimento.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJS = MyJS & "$('#" & Txt_DataNascita.ClientID & "').mask(""99/99/9999"");"        
        MyJS = MyJS & "$('#" & Txt_DataNascita.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"

        MyJS = MyJS & "$('#" & Txt_RettaTotale.ClientID & "').blur(function() {       "
        MyJS = MyJS & "var ap = formatNumber ($('#" & Txt_RettaTotale.ClientID & "').val(),2);"
        MyJS = MyJS & "$('#" & Txt_RettaTotale.ClientID & "').val(ap);"
        MyJS = MyJS & "});"

        MyJS = MyJS & "$('#" & Txt_RettaOspite.ClientID & "').blur(function() {       "
        MyJS = MyJS & "var ap = formatNumber ($('#" & Txt_RettaOspite.ClientID & "').val(),2);"
        MyJS = MyJS & "$('#" & Txt_RettaOspite.ClientID & "').val(ap);"
        MyJS = MyJS & "});"

        MyJS = MyJS & "$('#" & Txt_ImportoParente.ClientID & "').blur(function() {       "
        MyJS = MyJS & "var ap = formatNumber ($('#" & Txt_ImportoParente.ClientID & "').val(),2);"
        MyJS = MyJS & "$('#" & Txt_ImportoParente.ClientID & "').val(ap);"
        MyJS = MyJS & "});"

        MyJS = MyJS & "$('#" & Txt_ImportoComune.ClientID & "').blur(function() {       "
        MyJS = MyJS & "var ap = formatNumber ($('#" & Txt_ImportoComune.ClientID & "').val(),2);"
        MyJS = MyJS & "$('#" & Txt_ImportoComune.ClientID & "').val(ap);"
        MyJS = MyJS & "});"
        MyJS = MyJS & "$(" & Chr(34) & "#" & Txt_ComuneNascita.ClientID & Chr(34) & ").autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"


        MyJS = MyJS & "$("".chosen-select"").chosen({"
        MyJS = MyJS & "no_results_text: ""Comune non trovato"","
        MyJS = MyJS & "display_disabled_options: true,"
        MyJS = MyJS & "allow_single_deselect: true,"
        MyJS = MyJS & "width: ""350px"","
        MyJS = MyJS & "placeholder_text_single: ""Seleziona Comune"""
        MyJS = MyJS & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NuovoOspiteLoad", MyJS, True)


        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("..\Login.aspx")

            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim Cserv As New Cls_CentroServizio
        Dim Regione As New ClsUSL

        Cserv.UpDateDropBox(ConnectionString, Cmb_CServ)

        Regione.UpDateDropBox(ConnectionString, Cmb_regione)

        Dim Comune As New ClsComune

        Comune.UpDateDropBox(ConnectionString, DD_Comune)


        Dim TRetta As New Cls_TabellaTipoImportoRegione


        TRetta.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)


        Dim Listino As New Cls_Tabella_Listino


        If Listino.EsisteListino(HttpContext.Current.Session("DC_OSPITE")) Then
            PN_LISITINO.Visible = True
            PN_DETTAGLIO.Visible = False


            Listino.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, Cmb_CServ.SelectedValue)
            Listino.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_PrivatoSanitario, Cmb_CServ.SelectedValue, "P")
            Listino.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSanitario, Cmb_CServ.SelectedValue, "R")
            Listino.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSociale, Cmb_CServ.SelectedValue, "C")
            Listino.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoJolly, Cmb_CServ.SelectedValue, "J")



            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If Param.AlberghieroAssistenziale = 1 Then
                LblListinoPrivatoSinatario.Text = "Assistenziale Privato"
            End If

            If DD_PrivatoSanitario.Items.Count <= 1 Then
                DD_PrivatoSanitario.Visible = False
                LblListinoPrivatoSinatario.Visible = False

            End If

            If DD_LisitinoSanitario.Items.Count <= 1 Then
                DD_LisitinoSanitario.Visible = False
                lblListinoSan.Visible = False
            End If
            If DD_LisitinoSociale.Items.Count <= 1 Then
                DD_LisitinoSociale.Visible = False
                lblListinoSociale.Visible = False
            End If
            If DD_LisitinoJolly.Items.Count <= 1 Then
                DD_LisitinoJolly.Visible = False

                lblListinoJolly.Visible = False
            End If
        Else
            PN_LISITINO.Visible = False
            PN_DETTAGLIO.Visible = True
        End If



        If Val(Session("GDPRAttivo")) = 0 Then
            RB_NOConsenso1.Enabled = False
            RB_NOConsenso2.Enabled = False
            RB_SIConsenso1.Enabled = False
            RB_SIConsenso2.Enabled = False


            RB_NOConsenso1P.Enabled = False
            RB_NOConsenso2P.Enabled = False
            RB_SIConsenso1P.Enabled = False
            RB_SIConsenso2P.Enabled = False

        End If
    End Sub


    Protected Sub Inserisci()
        Dim X As New ClsOspite
        Dim XParametri As New Cls_Parametri
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim Appoggio As String



        If Not IsDate(Txt_DataNascita.Text) Then
            Appoggio = "Data nascita obbligatoria per inserimento ospite "
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If


        If Not IsDate(Txt_DataAccoglimento.Text) Then
            Appoggio = "Data accoglimento obbligatoria per inserimento ospite "
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If Cmb_CServ.SelectedValue = "" Then
            Appoggio = "Specifica centro servizio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If


        If Val(Session("GDPRAttivo")) = 1 Then
            If RB_SIConsenso1.Checked <> True Then
                Appoggio = "Non posso procedere senza il consenso all inserimento dell ospite"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                Exit Sub
            End If


            If Txt_CognomeNomeParente.Text <> "" Then
                If RB_SIConsenso1P.Checked <> True Then
                    Appoggio = "Non posso procedere senza il consenso all inserimento del parente"
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                    Exit Sub
                End If
            End If
        End If

        If PN_LISITINO.Visible = False Then

            If Txt_RettaTotale.Text.Trim = "" Then
                Appoggio = "Retta Totale Obbligatoria per inserimento ospite "
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                Exit Sub
            End If

            If Txt_RettaOspite.Text.Trim = "" Then
                Txt_RettaOspite.Text = 0
            End If

            If Txt_ImportoParente.Text.Trim = "" Then
                Txt_ImportoParente.Text = 0
            End If
            If Txt_ImportoComune.Text.Trim = "" Then
                Txt_ImportoComune.Text = 0
            End If



            If Cmb_Modalita.SelectedValue = "O" Then
                If CDbl(Txt_RettaOspite.Text) > 0 Then
                    Appoggio = "Se differenza ospite, importo ospite deve essere a zero "
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                    Exit Sub
                End If
            End If

            If Cmb_Modalita.SelectedValue = "P" Then
                If CDbl(Txt_ImportoParente.Text) > 0 Then
                    Appoggio = "Se differenza ospite, importo ospite deve essere a zero "
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                    Exit Sub
                End If
            End If

            If Cmb_Modalita.SelectedValue = "C" Then
                If CDbl(Txt_ImportoComune.Text) > 0 Then
                    Appoggio = "Se differenza comune, importo comune deve essere a zero "
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
                    Exit Sub
                End If
            End If


            Dim NumeroTot As Double
            NumeroTot = 0
            If Txt_RettaOspite.Text <> "" Then
                If cmb_TiporettaO.SelectedValue = "G" Then
                    NumeroTot = NumeroTot + CDbl(Txt_RettaOspite.Text)
                End If
                If cmb_TiporettaO.SelectedValue = "M" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_RettaOspite.Text) / 30)
                End If
                If cmb_TiporettaO.SelectedValue = "A" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_RettaOspite.Text) / 365)
                End If
            End If
            If Txt_ImportoParente.Text <> "" Then
                If Cmb_TipoImportoP.SelectedValue = "G" Then
                    NumeroTot = NumeroTot + CDbl(Txt_ImportoParente.Text)
                End If
                If Cmb_TipoImportoP.SelectedValue = "M" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_ImportoParente.Text) / 30)
                End If
                If Cmb_TipoImportoP.SelectedValue = "A" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_ImportoParente.Text) / 365)
                End If
            End If
            If Txt_ImportoComune.Text <> "" Then
                If DD_TipoImportoC.SelectedValue = "G" Then
                    NumeroTot = NumeroTot + CDbl(Txt_ImportoComune.Text)
                End If
                If DD_TipoImportoC.SelectedValue = "M" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_ImportoComune.Text) / 30)
                End If
                If DD_TipoImportoC.SelectedValue = "A" Then
                    NumeroTot = NumeroTot + (CDbl(Txt_ImportoComune.Text) / 365)
                End If
            End If

            Dim XUsl As New Cls_ImportoRegione

            XUsl.UltimaData(ConnectionString, Cmb_regione.SelectedValue, DD_TipoRetta.SelectedValue)

            If XUsl.Tipo = "G" Then
                NumeroTot = NumeroTot + CDbl(XUsl.Importo)
            End If
            If XUsl.Tipo = "M" Then
                NumeroTot = NumeroTot + (CDbl(XUsl.Importo) / 30)
            End If
            If XUsl.Tipo = "A" Then
                NumeroTot = NumeroTot + (CDbl(XUsl.Importo) / 365)
            End If
            Dim Miototale As Double
            Miototale = 0
            If DD_TipoImportoC.SelectedValue = "G" Then
                Miototale = CDbl(Txt_RettaTotale.Text)
            End If
            If DD_TipoImportoC.SelectedValue = "M" Then
                Miototale = (CDbl(Txt_RettaTotale.Text) / 30)
            End If
            If DD_TipoImportoC.SelectedValue = "A" Then
                NumeroTot = (CDbl(Txt_RettaTotale.Text) / 365)
            End If
            If Miototale < NumeroTot Then
                Appoggio = "Retta totale inferiore a rette distribuite"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)

                Exit Sub
            End If
        End If


        XParametri.LeggiParametri(ConnectionString)

        X.Pulisci()

        X.InserisciOspite(ConnectionString, Txt_Cognome.Text, Txt_Nome.Text, Txt_DataNascita.Text)

        X.CognomeOspite = Txt_Cognome.Text
        X.NomeOspite = Txt_Nome.Text

        X.Nome = Txt_Cognome.Text & " " & Txt_Nome.Text


        If RB_NOConsenso1.Checked = True Then
            X.CONSENSOINSERIMENTO = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            X.CONSENSOINSERIMENTO = 1
        End If


        If RB_NOConsenso2.Checked = True Then
            X.CONSENSOMARKETING = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            X.CONSENSOMARKETING = 1
        End If


        X.CODICEIVA = XParametri.CodiceIva
        X.DataNascita = Txt_DataNascita.Text
        X.TIPOLOGIA = "O"
        X.PERIODO = "M"
        X.MODALITAPAGAMENTO = XParametri.ModalitaPagamento
        X.TIPOOPERAZIONE = XParametri.TipoOperazione
        X.FattAnticipata = XParametri.Anticipato
        If RB_Maschile.Checked = True Then
            X.Sesso = "M"
        Else
            X.Sesso = "F"
        End If
        Dim VettoreNS(100) As String

        VettoreNS = SplitWords(Txt_ComuneNascita.Text)
        If VettoreNS.Length > 2 Then
            X.ComuneDiNascita = VettoreNS(1)
            X.ProvinciaDiNascita = VettoreNS(0)

            If X.ComuneDiNascita.Length > 3 Then
                X.ComuneDiNascita = Mid(X.ComuneDiNascita, 1, 3)
            End If
            If X.ProvinciaDiNascita.Length > 3 Then
                X.ProvinciaDiNascita = Mid(X.ProvinciaDiNascita, 1, 3)
            End If
        End If

        X.CODICEFISCALE = Txt_CodiceFiscale.Text

        X.Compensazione = "S"

        Dim MyCserv As New Cls_CentroServizio

        MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)


        If MyCserv.IncrementaNumeroCartella = 1 Then
            X.CodiceCentroServizio = MyCserv.CENTROSERVIZIO
            X.CartellaClinica = X.MaxTesseraSanitaria(ConnectionString)
        End If


        X.ScriviOspite(ConnectionString)


        Dim xs As New Cls_Movimenti

        xs.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        xs.CodiceOspite = X.CodiceOspite
        xs.Causale = ""
        xs.Data = Txt_DataAccoglimento.Text
        xs.Descrizione = ""
        xs.TipoMov = "05"


        xs.AggiornaDB(ConnectionString)

        If PN_LISITINO.Visible = False Then
            Dim XL As New Cls_rettatotale

            XL.CODICEOSPITE = X.CodiceOspite
            XL.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            XL.Data = Txt_DataAccoglimento.Text
            XL.Importo = Txt_RettaTotale.Text
            XL.Tipo = cmb_tiporettaT.Text
            XL.TipoRetta = DD_TipoRetta.SelectedValue
            XL.AggiornaDB(ConnectionString)

            Dim XSTATO As New Cls_StatoAuto

            XSTATO.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            XSTATO.CODICEOSPITE = X.CodiceOspite
            XSTATO.Data = Txt_DataAccoglimento.Text
            XSTATO.STATOAUTO = "A"
            If Cmb_regione.SelectedValue <> "" Then
                XSTATO.USL = Cmb_regione.SelectedValue
                XSTATO.STATOAUTO = "N"
                XSTATO.TipoRetta = DD_TipoRetta.SelectedValue
            End If
            XSTATO.AggiornaDB(ConnectionString)

            Dim XMODALITA As New Cls_Modalita

            XMODALITA.CodiceOspite = X.CodiceOspite
            XMODALITA.CentroServizio = Cmb_CServ.SelectedValue
            XMODALITA.Data = Txt_DataAccoglimento.Text
            XMODALITA.MODALITA = Cmb_Modalita.SelectedValue

            XMODALITA.AggiornaDB(ConnectionString)


            If Txt_RettaOspite.Text > 0 Then
                Dim XOSPITE As New Cls_ImportoOspite

                XOSPITE.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                XOSPITE.CODICEOSPITE = X.CodiceOspite
                XOSPITE.Data = Txt_DataAccoglimento.Text
                XOSPITE.Importo = CDbl(Txt_RettaOspite.Text)
                XOSPITE.Tipo = cmb_TiporettaO.SelectedValue

                XOSPITE.AggiornaDB(ConnectionString)
            End If

            If Txt_CognomeNomeParente.Text <> "" Then
                Dim Xparente As New Cls_Parenti
                Xparente.Pulisci()
                Xparente.Nome = Txt_CognomeNomeParente.Text
                Xparente.CODICEIVA = XParametri.CodiceIva
                Xparente.PERIODO = "M"
                Xparente.MODALITAPAGAMENTO = XParametri.ModalitaPagamento
                Xparente.TIPOOPERAZIONE = XParametri.TipoOperazione
                Xparente.FattAnticipata = XParametri.Anticipato
                Xparente.CodiceOspite = X.CodiceOspite


                If RB_NOConsenso1P.Checked = True Then
                    Xparente.CONSENSOINSERIMENTO = 2
                End If

                If RB_SIConsenso1P.Checked = True Then
                    Xparente.CONSENSOINSERIMENTO = 1
                End If


                If RB_NOConsenso2P.Checked = True Then
                    Xparente.CONSENSOMARKETING = 2
                End If

                If RB_SIConsenso1P.Checked = True Then
                    Xparente.CONSENSOMARKETING = 1
                End If


                Xparente.CodiceParente = 0
                Xparente.Compensazione = "S"
                Xparente.ScriviParente(ConnectionString)

                Dim XImpPar As New Cls_ImportoParente

                XImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                XImpPar.CODICEOSPITE = X.CodiceOspite
                XImpPar.CODICEPARENTE = Xparente.CodiceParente
                XImpPar.DATA = Txt_DataAccoglimento.Text
                XImpPar.TIPOIMPORTO = Cmb_TipoImportoP.SelectedValue
                XImpPar.IMPORTO = Txt_ImportoParente.Text
                XImpPar.AggiornaDB(ConnectionString)

                Dim PartX As New Cls_Pianodeiconti

                Dim CentroServizioP As New Cls_CentroServizio

                CentroServizioP.Leggi(ConnectionString, Cmb_CServ.SelectedValue)

                PartX.Mastro = CentroServizioP.MASTRO
                PartX.Conto = CentroServizioP.CONTO
                PartX.Sottoconto = (X.CodiceOspite * 100) + 1
                PartX.Descrizione = Txt_CognomeNomeParente.Text
                PartX.TipoAnagrafica = "P"
                PartX.Scrivi(ConnectionStringGenerale)

                Dim ParXpAR As New Cls_Parametri

                ParXpAR.LeggiParametri(Session("DC_OSPITE"))

                If ParXpAR.MastroAnticipo > 0 Then
                    Dim PartX1 As New Cls_Pianodeiconti

                    Dim CentroServiziop1 As New Cls_CentroServizio

                    CentroServiziop1.Leggi(ConnectionString, Cmb_CServ.SelectedValue)

                    PartX1.Mastro = ParXpAR.MastroAnticipo
                    PartX1.Conto = CentroServiziop1.CONTO
                    PartX1.Sottoconto = (X.CodiceOspite * 100) + 1
                    PartX1.Descrizione = Txt_CognomeNomeParente.Text
                    PartX1.TipoAnagrafica = "P"
                    PartX1.Scrivi(ConnectionStringGenerale)
                End If
            End If

            If DD_Comune.SelectedValue <> "" Then
                Dim Vettore(100) As String

                Vettore = SplitWords(DD_Comune.SelectedValue)

                If Vettore.Length >= 2 Then

                    If Txt_ImportoComune.Text = "" Then
                        Txt_ImportoComune.Text = "0"
                    End If

                    Dim XComune As New Cls_ImportoComune

                    XComune.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    XComune.CODICEOSPITE = X.CodiceOspite
                    XComune.Data = Txt_DataAccoglimento.Text
                    XComune.COMUNE = Vettore(1)
                    XComune.PROV = Vettore(0)
                    XComune.Importo = CDbl(Txt_ImportoComune.Text)
                    XComune.Tipo = DD_TipoImportoC.SelectedValue
                    XComune.AggiornaDB(ConnectionString)


                    If XComune.COMUNE.Length > 3 Then
                        XComune.COMUNE = Mid(XComune.COMUNE, 1, 3)
                    End If
                    If XComune.PROV.Length > 3 Then
                        X.ProvinciaDiNascita = Mid(X.ProvinciaDiNascita, 1, 3)
                    End If
                End If
            End If
        Else
            Dim ListinoOspite As New Cls_Listino

            ListinoOspite.InserisciRette(Session("DC_OSPITE"), Cmb_CServ.SelectedValue, X.CodiceOspite, DD_Lisitino.SelectedValue, Txt_DataAccoglimento.Text, DD_PrivatoSanitario.SelectedValue, DD_LisitinoSanitario.SelectedValue, DD_LisitinoSociale.SelectedValue, DD_LisitinoJolly.SelectedValue)

        End If


        Dim tX As New Cls_Pianodeiconti

        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(ConnectionString, Cmb_CServ.SelectedValue)

        tX.Mastro = CentroServizio.MASTRO
        tX.Conto = CentroServizio.CONTO
        tX.Sottoconto = X.CodiceOspite * 100
        tX.Descrizione = X.Nome
        tX.Tipo = "A"
        tX.TipoAnagrafica = "O"

        tX.Scrivi(ConnectionStringGenerale)

        Dim XpAR As New Cls_Parametri

        XpAR.LeggiParametri(Session("DC_OSPITE"))

        If XpAR.MastroAnticipo > 0 Then
            Dim tX1 As New Cls_Pianodeiconti

            Dim CentroServizio1 As New Cls_CentroServizio

            CentroServizio1.Leggi(ConnectionString, Cmb_CServ.SelectedValue)

            tX1.Mastro = XpAR.MastroAnticipo
            tX1.Conto = CentroServizio.CONTO
            tX1.Sottoconto = X.CodiceOspite * 100
            tX1.Descrizione = X.Nome
            tX1.TipoAnagrafica = "O"
            tX1.Tipo = "P"
            tX1.Scrivi(ConnectionStringGenerale)
        End If

        REM Btn_Inserisci.Enabled = False
        REM Lbl_Errori.Text = "Ospite inserito correttamente"
        Session("CodiceOspite") = X.CodiceOspite
        Session("CODICESERVIZIO") = Cmb_CServ.SelectedValue


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "I", "OSPITE", "")

        If Txt_CognomeNomeParente.Text <> "" Then
            Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 1, "", "", 0, "", "I", "PARENTE", "")
        End If


        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Ospite Inserito');", True)
        'Call PaginaPrecedente()
        Response.Redirect("Anagrafica.aspx?PAGINA=0&CentroServizio=" & Cmb_CServ.SelectedValue & "&CODICEOSPITE=" & X.CodiceOspite)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Call Inserisci()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Txt_Cognome_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Cognome.TextChanged
        If Txt_Cognome.Text.Trim <> "" Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsOspite

            x.CodiceOspite = 0
            x.Nome = Txt_Cognome.Text & " " & Txt_Nome.Text
            x.LeggiNome(Session("DC_OSPITE"))

            If x.CodiceOspite <> 0 Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Nome_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Nome.TextChanged
        If Txt_Nome.Text.Trim <> "" Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsOspite

            x.CodiceOspite = 0
            x.Nome = Txt_Cognome.Text & " " & Txt_Nome.Text
            x.LeggiNome(Session("DC_OSPITE"))

            If x.CodiceOspite <> 0 Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim k As New Cls_CodiceFiscale
        Dim Nome As String
        Dim Cognome As String
        Dim FineCognome As Integer
        Dim Appoggio As String

        If Txt_Cognome.Text = "" Then
            Appoggio = "Necessari il cognome e nome per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Txt_Nome.Text = "" Then
            Appoggio = "Necessari il cognome e nome per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataNascita.Text) Then
            Appoggio = "Necessaria data nascita per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If Txt_ComuneNascita.Text = "" Then
            Appoggio = "Necessario comune nascita per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If RB_Maschile.Checked = False And RB_Femminile.Checked = False Then
            Appoggio = "Specificare il sesso per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        

        Cognome = Txt_Cognome.Text
        Nome = Txt_Nome.Text

        Dim VettoreNS(100) As String

        VettoreNS = SplitWords(Txt_ComuneNascita.Text)
        If VettoreNS.Length > 2 Then
            k.Comune = VettoreNS(1)
            k.Provincia = VettoreNS(0)
        End If

        k.Nome = Nome
        k.Cognome = Cognome
        k.DataNascita = Txt_DataNascita.Text
        If RB_Maschile.Checked = True Then
            k.Sesso = "M"
        Else
            k.Sesso = "F"
        End If
        k.StringaConnessione = Session("DC_OSPITE")
        Txt_CodiceFiscale.Text = k.Make_CodiceFiscale()
    End Sub


    Private Sub PaginaPrecedente()
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged
        Dim k As New Cls_Tabella_Listino


        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, Cmb_CServ.SelectedValue, "")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_PrivatoSanitario, Cmb_CServ.SelectedValue, "P")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSanitario, Cmb_CServ.SelectedValue, "R")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSociale, Cmb_CServ.SelectedValue, "C")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoJolly, Cmb_CServ.SelectedValue, "J")


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AlberghieroAssistenziale = 1 Then
            LblListinoPrivatoSinatario.Text = "Assistenziale Privato"
        End If

        If DD_PrivatoSanitario.Items.Count <= 1 Then
            DD_PrivatoSanitario.Visible = False
            LblListinoPrivatoSinatario.Visible = False

        End If

        If DD_LisitinoSanitario.Items.Count <= 1 Then
            DD_LisitinoSanitario.Visible = False
            lblListinoSan.Visible = False
        End If
        If DD_LisitinoSociale.Items.Count <= 1 Then
            DD_LisitinoSociale.Visible = False
            lblListinoSociale.Visible = False
        End If
        If DD_LisitinoJolly.Items.Count <= 1 Then
            DD_LisitinoJolly.Visible = False

            lblListinoJolly.Visible = False
        End If
    End Sub
End Class
