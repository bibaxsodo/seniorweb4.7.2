﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class OspitiWeb_FatturaNCNew
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim Tabella2 As New System.Data.DataTable("tabella2")

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Boolean
        Dim MySql As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(ConnessioniOspiti)

        cn.Open()

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim RsMovimenti As OleDbDataReader = cmd.ExecuteReader()


        If RsMovimenti.Read Then
            OspitePresente = False
            If campodb(RsMovimenti.Item("TipoMov")) = "13" And Format(campodbd(RsMovimenti.Item("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If campodb(RsMovimenti.Item("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        Else
            OspitePresente = False
        End If
        RsMovimenti.Close()
        cn.Close()
    End Function

    Private Sub Calcolo(ByVal ConnessioneOspiti As String, ByVal CodiceOspite As Integer, ByVal Cserv As String, ByRef Quota As Double, ByRef Quota2 As Double)
        Dim ConnectionString As String = ConnessioneOspiti
        Dim TipoCentro As String
        Dim StDataCond As String
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long
        Dim Mese As Integer
        Dim Anno As Integer
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim MySql As String

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnessioneOspiti)

        cn.Open()


        Dim UsoExtraParente As Integer = 0



        Dim cmdParenteRetta1 As New OleDbCommand()
        cmdParenteRetta1.CommandText = "SELECT count(*) as usati FROM [EXTRAOSPITE] where RIPARTIZIONE = 'P' "
        cmdParenteRetta1.Connection = cn
        Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
        If RDParenteRetta1.Read() Then
            If Val(campodb(RDParenteRetta1.Item(0))) > 0 Then
                UsoExtraParente = 1
            End If
        End If
        RDParenteRetta1.Close()

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette

        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.ConguaglioMinimo = f.ConguaglioMinimo
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo
        XS.ApriDB(ConnessioneOspiti, Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = ConnessioneOspiti
        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.ConnessioneTabelle = Session("DC_TABELLE")
        XS.CaricaCausali()
        Mese = Dd_Mese.SelectedValue
        Anno = Txt_Anno.Text
        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"

        Dim ListinoPresente As Boolean = False
        Dim VerificaListino As New Cls_Tabella_Listino

        ListinoPresente = VerificaListino.EsisteListino(ConnessioneOspiti)


        If DD_OspiteParenti.SelectedValue = 0 Then
            Quota = XS.QuoteGiornaliere(Cserv, CodiceOspite, "O1", 0, Now)
            Quota2 = XS.QuoteGiornaliere(Cserv, CodiceOspite, "O2", 0, Now)

            
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(ConnessioneOspiti,CodiceOspite )

            Dim DatiOspPareCServ As New Cls_DatiOspiteParenteCentroServizio

            DatiOspPareCServ.CodiceOspite = CodiceOspite
            DatiOspPareCServ.CodiceParente =0
            DatiOspPareCServ.CentroServizio= Cserv
            DatiOspPareCServ.Leggi( ConnessioneOspiti)
            If DatiOspPareCServ.TipoOperazione<>"" Then
                Ospite.TIPOOPERAZIONE = DatiOspPareCServ.TipoOperazione
                Ospite.CODICEIVA = DatiOspPareCServ.AliquotaIva
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(ConnessioneOspiti,TipoOperazione.Codice)

            If TipoOperazione.SCORPORAIVA = 1 Then
                Dim MyIVA As New Cls_IVA                

                MyIVA.Codice = Ospite.CODICEIVA
                MyIVA.Leggi(XS.ConnessioneTabelle,MyIVA.Codice)

                Quota =  Math.Round(Quota * 100 / (100 + (MyIVA.Aliquota * 100)),2)
                Quota2 = Math.Round(Quota2 * 100 / (100 + (MyIVA.Aliquota * 100)),2)
            End If
        Else
            Quota = XS.QuoteGiornaliere(Cserv, CodiceOspite, "P1", DD_OspiteParenti.SelectedValue, Now)
            Quota2 = XS.QuoteGiornaliere(Cserv, CodiceOspite, "P2", DD_OspiteParenti.SelectedValue, Now)


            Dim Parente As New Cls_Parenti

            Parente.CodiceOspite = CodiceOspite
            Parente.CodiceParente =DD_OspiteParenti.SelectedValue
            Parente.Leggi(ConnessioneOspiti,CodiceOspite,DD_OspiteParenti.SelectedValue)

            Dim DatiOspPareCServ As New Cls_DatiOspiteParenteCentroServizio

            DatiOspPareCServ.CodiceOspite = CodiceOspite
            DatiOspPareCServ.CodiceParente =DD_OspiteParenti.SelectedValue
            DatiOspPareCServ.CentroServizio= Cserv
            DatiOspPareCServ.Leggi( ConnessioneOspiti)
            If DatiOspPareCServ.TipoOperazione<>"" Then
                Parente.TIPOOPERAZIONE = DatiOspPareCServ.TipoOperazione
                Parente.CODICEIVA = DatiOspPareCServ.AliquotaIva
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Parente.TIPOOPERAZIONE
            TipoOperazione.Leggi(ConnessioneOspiti,TipoOperazione.Codice)

            If TipoOperazione.SCORPORAIVA = 1 Then
                Dim MyIVA As New Cls_IVA

                MyIVA.Codice = Parente.CODICEIVA
                MyIVA.Leggi(XS.ConnessioneTabelle,MyIVA.Codice)
                Quota =  Math.Round(Quota * 100 / (100 + (MyIVA.Aliquota * 100)),2)
                Quota2 = Math.Round(Quota2 * 100 / (100 + (MyIVA.Aliquota * 100)),2)
            End If
        End If


        MySql = "SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE "

        Dim xMax As Long
        Dim xNum As Long = 0



        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim RdOspiti As OleDbDataReader = cmd.ExecuteReader()

        XS.EliminaDatiRette(Mese, Anno, Cserv, 0)

        Do While RdOspiti.Read

            VarCentroServizio = campodb(RdOspiti.Item("CentroServizio"))
            varcodiceospite = campodb(RdOspiti.Item("CodiceOspite"))

            Dim MyCserv As New Cls_CentroServizio

            MyCserv.Leggi(ConnessioneOspiti, campodb(RdOspiti.Item("CentroServizio")))

            f.Elaborazione(ConnessioneOspiti)
            If Not OspitePresente(VarCentroServizio, varcodiceospite, Mese, Anno, ConnessioneOspiti) Then

                Call XS.AzzeraTabella()

                XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

                XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)
                xMese = Mese
                xAnno = Anno
                If xMese = 12 Then
                    xMese = 1
                    xAnno = Anno + 1
                Else
                    xMese = Mese + 1
                    xAnno = Anno
                End If
                Dim XOsp As New ClsOspite

                XOsp.Leggi(ConnessioneOspiti, varcodiceospite)
                If XOsp.FattAnticipata = "S" Then
                    Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese, xAnno)
                    Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese, xAnno)


                    Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese, xAnno)

                    Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese, xAnno)

                    Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese, xAnno)
                    Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese, xAnno)



                    Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese, xAnno, False)
                    Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese, xAnno)
                End If
                TipoCentro = XS.CampoCentroServizio(VarCentroServizio, "TIPOCENTROSERVIZIO")
                Call XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
            Else
                Call XS.AzzeraTabella()

                If ListinoPresente Then
                    Dim MOs As New Cls_Listino

                    MOs.CENTROSERVIZIO = VarCentroServizio
                    MOs.CODICEOSPITE = varcodiceospite
                    MOs.LeggiAData(ConnessioneOspiti, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))


                    If MOs.CodiceListino <> "" Then
                        Dim DeCListi As New Cls_Tabella_Listino

                        DeCListi.Codice = MOs.CodiceListino
                        DeCListi.LeggiCausale(ConnessioneOspiti)

                        If Year(DeCListi.DATA) > 1900 Then
                            If Format(DeCListi.DATA, "yyyMMdd") <= Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                Dim DecOspite As New ClsOspite

                                DecOspite.CodiceOspite = varcodiceospite
                                DecOspite.Leggi(ConnessioneOspiti, DecOspite.CodiceOspite)

                                XS.StringaDegliErrori = XS.StringaDegliErrori & "Ospite " & varcodiceospite & " " & DecOspite.Nome & " ha un listino scaduto " & DeCListi.Descrizione & vbNewLine
                            End If
                        End If
                    End If

                End If

                XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)
                XS.PrimoDataAccoglimentoOspite = XS.PrimaDataAccoglimento(VarCentroServizio, varcodiceospite, Mese, Anno)

                If UsoExtraParente = 1 Then
                    XS.VerificaPrimaCalcolo(VarCentroServizio, varcodiceospite)
                End If

                XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                    XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
                End If


                XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

                If MyCserv.TIPOCENTROSERVIZIO = "A" Then
                    XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
                End If

                If Val(MyCserv.VerificaImpegnativa) = 1 Then
                    Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
                End If

                If Chk_EscludiProgrammati.Checked = False Then
                    XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, Mese, Anno)
                End If

                XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



                XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
                XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
                XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



                XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

                XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

                XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)



                XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

                XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



                XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

                XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

                If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then

                Else
                    XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno, True)
                End If
            End If

        Loop


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = "Update TabellaParametri Set CodiceOspite = 0"
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()
        f.FineElaborazione(ConnessioneOspiti)
        cn.Close()
    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Private Sub Emissione(ByVal ConnessioneOspiti As String, ByVal ConnessioneGenerale As String, ByVal ConnessioneTabelle As String)
        Dim MyRs As New ADODB.Recordset

        Dim EmC As New Cls_EmissioneRetta

        Dim MeseContr As Integer
        Dim Anno As Integer

        
        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(ConnessioneOspiti)

        LeggiParametri.Elaborazione(ConnessioneOspiti)

        Anno = LeggiParametri.AnnoFatturazione
        EmC.ConnessioneGenerale = ConnessioneGenerale
        EmC.ConnessioneOspiti = ConnessioneOspiti
        EmC.ConnessioneTabelle = ConnessioneTabelle

        EmC.ApriDB()

        Session("CampoProva") = "P"
        EmC.EliminaTemporanei()
        EmC.EmissioneDiProva = "P"


        'Call EmC.CreaRettaOspite(Dd_Mese.SelectedValue, Txt_Anno.Text, "", Txt_DataRegistrazione.Text, Txt_DataRegistrazione.Text, Session, 0, False)

        If DD_OspiteParenti.SelectedValue = 0 Then
            Call EmC.CreaRettaOspite(Dd_Mese.SelectedValue, Txt_Anno.Text, "", Txt_DataRegistrazione.Text, Txt_DataRegistrazione.Text, Session, Session("CODICEOSPITE"), False)
        Else
            Call EmC.CreaRettaParente(Dd_Mese.SelectedValue, Txt_Anno.Text, "", Txt_DataRegistrazione.Text, Txt_DataRegistrazione.Text, Session, Session("CODICEOSPITE"), False)
        End If

    End Sub


    Private Sub NumeraRiferimentoFattura2()
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String


        Txt_DataRif_2.Text = ""
        Txt_RifNumero_2.Text = ""

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If


        If Mastro = 0 Or Sottoconto = 0 Then Exit Sub



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,MovimentiContabiliTesta.NumeroBolletta,MovimentiContabiliTesta.DataBolletta " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim Saldo As Double
        Saldo = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)
            If Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "RE" Then
                Txt_DataRif_2.Text = Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy")
                Txt_RifNumero_2.Text = campodb(myPOSTreader.Item("NumeroProtocollo"))
                Exit Sub
            End If


        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Private Sub NumeraRiferimentoFattura()
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String


        Txt_DataRif.Text = ""
        Txt_RifNumero.Text = ""

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If


        If Mastro = 0 Or Sottoconto = 0 Then Exit Sub



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,MovimentiContabiliTesta.NumeroBolletta,MovimentiContabiliTesta.DataBolletta " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim Saldo As Double
        Saldo = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)
            If Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "RE" Then
                Txt_DataRif.Text = Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy")
                Txt_RifNumero.Text = campodb(myPOSTreader.Item("NumeroProtocollo"))
                Exit Sub
            End If


        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Private Sub EliminaMeseSuccessivo()
        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(Session("DC_OSPITE"))

        Dim OspitiCon As OleDbConnection
        Dim MeseSuccessivo As Integer = 0
        Dim AnnoSuccessivo As Integer = 0


        If LeggiParametri.MeseFatturazione = 12 Then
            MeseSuccessivo = 1
            AnnoSuccessivo = LeggiParametri.AnnoFatturazione + 1
        Else
            MeseSuccessivo = LeggiParametri.MeseFatturazione + 1
            AnnoSuccessivo = LeggiParametri.AnnoFatturazione
        End If

        OspitiCon = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        OspitiCon.Open()
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = "Delete RetteOspite Where CodiceOspite= ? And CENTROSERVIZIO = ? And Mese = ? And Anno = ? And ANNOCOMPETENZA = ? And MESECOMPETENZA = ?"
        cmd1.Connection = OspitiCon
        cmd1.Parameters.AddWithValue("@CODICEOSPITE", Session("CODICEOSPITE"))
        cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", Session("CODICESERVIZIO"))
        cmd1.Parameters.AddWithValue("@Mese", LeggiParametri.MeseFatturazione)
        cmd1.Parameters.AddWithValue("@Anno", LeggiParametri.AnnoFatturazione)
        cmd1.Parameters.AddWithValue("@Anno", AnnoSuccessivo)
        cmd1.Parameters.AddWithValue("@Mese", MeseSuccessivo)
        cmd1.ExecuteNonQuery()


        Dim cmdP As New OleDbCommand()
        cmdP.CommandText = "Delete RetteParente Where CodiceOspite= ? And CENTROSERVIZIO = ? And Mese = ? And Anno = ? And ANNOCOMPETENZA = ? And MESECOMPETENZA = ?"
        cmdP.Connection = OspitiCon
        cmdP.Parameters.AddWithValue("@CODICEOSPITE", Session("CODICEOSPITE"))
        cmdP.Parameters.AddWithValue("@CENTROSERVIZIO", Session("CODICESERVIZIO"))
        cmdP.Parameters.AddWithValue("@Mese", LeggiParametri.MeseFatturazione)
        cmdP.Parameters.AddWithValue("@Anno", LeggiParametri.AnnoFatturazione)
        cmdP.Parameters.AddWithValue("@Anno", AnnoSuccessivo)
        cmdP.Parameters.AddWithValue("@Mese", MeseSuccessivo)
        cmdP.ExecuteNonQuery()
        OspitiCon.Close()



    End Sub
    Private Sub EstraiFTNC()
        Dim Quota As Double = 0
        Dim Quota2 As Double = 0
        Dim LeggiOSpite As New ClsOspite
        Dim CalcoloFineMese As New Cls_CalcoloOspitiFineMese

        LeggiOSpite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

        If LeggiOSpite.PERIODO = "F" Then
            Dim Xs As New Cls_CalcoloRette
            Xs.ConnessioneGenerale = Session("DC_GENERALE")
            Xs.ConnessioneTabelle = Session("DC_TABELLE")

            Xs.STRINGACONNESSIONEDB = Session("DC_OSPITE")
            Xs.ApriDB(Xs.STRINGACONNESSIONEDB, Session("DC_OSPITIACCESSORI"))
            Xs.EliminaDatiRette(Dd_Mese.SelectedValue, Val(Txt_Anno.Text), "", 0)

            Xs.ChiudiDB()

            Dim DataUscita As Date
            DataUscita = CalcoloFineMese.OspiteDimessonelMese(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), Dd_Mese.SelectedValue, Txt_Anno.Text, Session("DC_OSPITE"))


            Dim sc2 As New MSScriptControl.ScriptControl

            sc2.Language = "vbscript"

            If Month(DataUscita) = Dd_Mese.SelectedValue And Year(DataUscita) = Txt_Anno.Text Then
                CalcoloFineMese.GeneraPeriodo(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), Dd_Mese.SelectedValue, Txt_Anno.Text, DataUscita, Session("DC_OSPITE"), sc2)
            End If

        Else
            Calcolo(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), Quota, Quota2)
        End If
        If Chk_SoloMese.Checked Then
            EliminaMeseSuccessivo()
        End If
        Emissione(Session("DC_OSPITE"), Session("DC_GENERALE"), Session("DC_TABELLE"))



        Tabella2.Clear()
        Tabella2.Columns.Clear()
        Tabella2.Columns.Add("DescrizioneRiga", GetType(String))
        Tabella2.Columns.Add("Importo", GetType(String))
        Tabella2.Columns.Add("Giorni", GetType(String))
        Tabella2.Columns.Add("IVA", GetType(String))
        Tabella2.Columns.Add("Descrizione", GetType(String))
        Tabella2.Columns.Add("ID", GetType(String))
        Tabella2.Columns.Add("ContoRicavo", GetType(String))


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DescrizioneRiga", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Giorni", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("ContoRicavo", GetType(String))


        GridV2.AutoGenerateColumns = False

        GridV2.DataSource = Tabella2

        GridV2.DataBind()


        GridV.AutoGenerateColumns = False

        GridV.DataSource = Tabella

        GridV.DataBind()


        Dim GeneraleCon As OleDbConnection
        Dim Causale As String = ""
        Dim NumeroRegistrazione As Integer = 0


        Dim Causale_2 As String = ""
        Dim NumeroRegistrazione_2 As Integer = 0

        GeneraleCon = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        GeneraleCon.Open()



        Dim cmdTesta As New OleDbCommand()
        cmdTesta.CommandText = ("select * from Temp_MovimentiContabiliTesta order by NumeroRegistrazione")
        cmdTesta.Connection = GeneraleCon

        Dim ReadTesta As OleDbDataReader = cmdTesta.ExecuteReader()
        If ReadTesta.Read Then
            Causale = campodb(ReadTesta.Item("CausaleContabile"))
            NumeroRegistrazione = campodbn(ReadTesta.Item("NumeroRegistrazione"))
            Dim M As New Cls_DescrizioneSuDocumento
            Dim Documento As New Cls_MovimentoContabile

            Documento.NumeroRegistrazione = NumeroRegistrazione
            Documento.Leggi(Session("DC_GENERALE"), NumeroRegistrazione, True)

            If Val(Txt_NumeroProtocollo.Text) > 0 Then
                If Documento.MaxProtocollo(Session("DC_GENERALE"), Documento.AnnoProtocollo, Documento.RegistroIVA) < Val(Txt_NumeroProtocollo.Text) Then
                    Documento.NumeroProtocollo = Val(Txt_NumeroProtocollo.Text)
                    Documento.NumeroDocumento = Val(Txt_NumeroProtocollo.Text)
                    Documento.Scrivi(Session("DC_GENERALE"), NumeroRegistrazione, True)
                End If
            End If

            M.ModificaDescrizioneDocumento(Documento, Session("DC_OSPITE"), Session("DC_TABELLE"), Session("DC_GENERALE"))

            If ReadTesta.Read Then
                Causale_2 = campodb(ReadTesta.Item("CausaleContabile"))
                NumeroRegistrazione_2 = campodbn(ReadTesta.Item("NumeroRegistrazione"))

                Documento.NumeroRegistrazione = NumeroRegistrazione_2
                Documento.Leggi(Session("DC_GENERALE"), NumeroRegistrazione_2, True)

                M.ModificaDescrizioneDocumento(Documento, Session("DC_OSPITE"), Session("DC_TABELLE"), Session("DC_GENERALE"))

            End If
        End If

        ReadTesta.Close()



        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Codice = Causale
        MyCausale.Leggi(Session("DC_TABELLE"), MyCausale.Codice)

        Lbl_DecodificaCausale.Text = "<br><br>Causale Contabile " & MyCausale.Descrizione & "<br/>"

        If MyCausale.TipoDocumento = "NC" Then
            Lbl_Riferimento730.Visible = True
            Lbl_RiferimentoNumero.Visible = True
            Txt_DataRif.Visible = True
            Txt_RifNumero.Visible = True
            NumeraRiferimentoFattura()
        Else
            Lbl_Riferimento730.Visible = False
            Lbl_RiferimentoNumero.Visible = False
            Txt_DataRif.Visible = False
            Txt_RifNumero.Visible = False
        End If

        Dim MyCausale_2 As New Cls_CausaleContabile

        If Causale_2 <> "" Then
            MyCausale_2.Codice = Causale_2
            MyCausale_2.Leggi(Session("DC_TABELLE"), MyCausale_2.Codice)

            Lbl_DecodificaCausale2.Text = "<br><br>Causale Contabile " & MyCausale_2.Descrizione & "<br/>"

            If MyCausale_2.TipoDocumento = "NC" Then
                Lbl_Riferimento730_2.Visible = True
                Lbl_RiferimentoNumero_2.Visible = True
                Txt_DataRif_2.Visible = True
                Txt_RifNumero_2.Visible = True
                NumeraRiferimentoFattura2()
            Else
                Lbl_Riferimento730_2.Visible = False
                Lbl_RiferimentoNumero_2.Visible = False
                Txt_DataRif_2.Visible = False
                Txt_RifNumero_2.Visible = False
            End If
        Else
            Lbl_Riferimento730_2.Visible = False
            Lbl_RiferimentoNumero_2.Visible = False
            Txt_DataRif_2.Visible = False
            Txt_RifNumero_2.Visible = False
        End If

        Dim TipoOperazione As String = ""
        If DD_OspiteParenti.SelectedValue = 0 Then
            Dim Ospite As New ClsOspite

            Ospite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Ospite.CodiceOspite
            KCs.Leggi(Session("DC_OSPITE"))


            If KCs.CodiceOspite <> 0 Then
                Ospite.TIPOOPERAZIONE = KCs.TipoOperazione
            End If
            TipoOperazione = Ospite.TIPOOPERAZIONE
        Else
            Dim Parenti As New Cls_Parenti

            Parenti.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_OspiteParenti.SelectedValue)


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Parenti.CodiceOspite
            KCs.CodiceParente = DD_OspiteParenti.SelectedValue
            KCs.Leggi(Session("DC_OSPITE"))


            If KCs.CodiceOspite <> 0 Then
                Parenti.TIPOOPERAZIONE = KCs.TipoOperazione
            End If
            TipoOperazione = Parenti.TIPOOPERAZIONE
        End If


        If NumeroRegistrazione > 0 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from Temp_MovimentiContabiliRiga where ( TIPO = '' OR TIPO IS NULL) And Numero = ? ORDER BY RIGADACAUSALE")
            cmd.Connection = GeneraleCon
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 3 Then
                    myriga(0) = "Retta"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 4 Then
                    myriga(0) = "Extra Fissi"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                    myriga(0) = "Addebiti"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                    myriga(0) = "Accrediti"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 9 Then
                    myriga(0) = "Bollo"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 12 Then
                    myriga(0) = "Sconto"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 13 Then
                    myriga(0) = "Retta 2"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 14 Then
                    myriga(0) = "Retta Percentuale"
                End If

                If MyCausale.TipoDocumento = "NC" Then
                    If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                    Else
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                    End If
                Else
                    If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                    Else
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                    End If
                End If

                myriga(2) = campodb(myPOSTreader.Item("Quantita"))

                If campodbn(myPOSTreader.Item("Quantita")) < 0 And Math.Abs(Math.Round(campodbn(myPOSTreader.Item("Importo")), 2)) = 0 Then
                    myriga(2) = 0
                End If

                If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                    Dim ClTipoOperazione As New Cls_TipoOperazione

                    ClTipoOperazione.Codice = TipoOperazione
                    ClTipoOperazione.Leggi(Session("DC_OSPITE"), ClTipoOperazione.Codice)

                    If campodb(myPOSTreader.Item("TipoExtra")) = "A" & ClTipoOperazione.TipoAddebitoAccredito Then
                        If MyCausale.TipoDocumento <> "NC" Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = Session("CODICEOSPITE")
                            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")

                            Mov.UltimaDataAccoglimento(Session("DC_OSPITE"))
                            If Year(Mov.Data) = Txt_Anno.Text And Month(Mov.Data) = Dd_Mese.SelectedValue Then
                                myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                            Else
                                myriga(2) = 0
                            End If
                            If Quota > 0 Then
                                If Modulo.MathRound(myriga(1) / Quota, 2) = Int(myriga(1) / Quota) Then
                                    myriga(2) = Math.Abs(Int(myriga(1) / Quota))
                                End If
                            End If
                        End If
                    End If

                    
                    If campodb(myPOSTreader.Item("TipoExtra")) = "A" & ClTipoOperazione.TipoAddebitoAccredito2 Then
                        If MyCausale.TipoDocumento <> "NC" Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = Session("CODICEOSPITE")
                            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")

                            Mov.UltimaDataAccoglimento(Session("DC_OSPITE"))
                            If Year(Mov.Data) = Txt_Anno.Text And Month(Mov.Data) = Dd_Mese.SelectedValue Then
                                myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                            Else
                                myriga(2) = 0
                            End If
                            If Quota > 0 Then
                                If Modulo.MathRound(myriga(1) / Quota2, 2) = Int(myriga(1) / Quota2) And Quota2 > 0 Then
                                    myriga(2) = Math.Abs(Int(myriga(1) / Quota2))
                                End If
                            End If
                        End If
                    End If
                End If

                If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                    Dim ClTipoOperazione As New Cls_TipoOperazione


                    ClTipoOperazione.Codice = TipoOperazione
                    ClTipoOperazione.Leggi(Session("DC_OSPITE"), ClTipoOperazione.Codice)

                    Dim TipoExtraRegistrazione As String = campodb(myPOSTreader.Item("TipoExtra")) & Space(10)

                    If TipoExtraRegistrazione.Trim = "A" & ClTipoOperazione.TipoAddebitoAccredito.Trim Or TipoExtraRegistrazione.Trim = "A" & ClTipoOperazione.TipoAddebitoAccredito2.Trim Or  Mid(TipoExtraRegistrazione, 1, 1) = "E" Then
                        If MyCausale.TipoDocumento = "NC" Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = Session("CODICEOSPITE")
                            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")

                            Mov.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                            If Mov.Causale <> "" Then
                                Dim CausaleEU As New Cls_CausaliEntrataUscita

                                CausaleEU.Codice = Mov.Causale
                                CausaleEU.LeggiCausale(Session("DC_OSPITE"))

                                If CausaleEU.GiornoUscita = "P" Then
                                    myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                                Else
                                    myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text))) + 1
                                End If
                            Else
                                myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                            End If


                            If Mid(TipoExtraRegistrazione, 1, 1) = "E" Then

                                Dim TipoExtra As New Cls_TipoExtraFisso

                                TipoExtra.CODICEEXTRA = Mid(TipoExtraRegistrazione, 2, 2)
                                TipoExtra.Leggi(Session("DC_OSPITE"))

                                If TipoExtra.IMPORTO > 0 And TipoExtra.TipoImporto = "G" Then
                                    If Modulo.MathRound(myriga(1) / Modulo.MathRound(TipoExtra.IMPORTO, 2), 5) = Int(myriga(1) / Modulo.MathRound(TipoExtra.IMPORTO, 2)) Then
                                        myriga(2) = Math.Abs(Int(myriga(1) / Modulo.MathRound(TipoExtra.IMPORTO, 2)))
                                    End If
                                End If
                            Else
                                If TipoExtraRegistrazione.Trim = "A" & ClTipoOperazione.TipoAddebitoAccredito2.Trim  THEN
                                    If Quota2 > 0 Then
                                        If Modulo.MathRound(myriga(1) / Quota2, 2) = Int(myriga(1) / Quota2) Then
                                            myriga(2) = Math.Abs(Int(myriga(1) / Quota2))
                                        End If
                                    End If
                                   Else
                                    If Quota > 0 Then
                                        If Modulo.MathRound(myriga(1) / Quota, 2) = Int(myriga(1) / Quota) Then
                                            myriga(2) = Math.Abs(Int(myriga(1) / Quota))
                                        End If
                                    End If
                                End If                                
                            End If
                        End If
                    End If
                End If

                myriga(3) = campodb(myPOSTreader.Item("CodiceIva"))
                myriga(4) = campodb(myPOSTreader.Item("Descrizione"))
                myriga(5) = campodb(myPOSTreader.Item("id"))

                Dim MS As New Cls_Pianodeiconti
                MS.Mastro = campodb(myPOSTreader.Item("MastroPartita"))
                MS.Conto = campodb(myPOSTreader.Item("ContoPartita"))
                MS.Sottoconto = campodb(myPOSTreader.Item("SottocontoPartita"))
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(6) = MS.Mastro & "  " & MS.Conto & " " & MS.Sottoconto & " " & MS.Descrizione

                Tabella.Rows.Add(myriga)
            Loop
            myPOSTreader.Close()

            GridV.AutoGenerateColumns = False

            GridV.DataSource = Tabella

            GridV.DataBind()
        End If
        If NumeroRegistrazione_2 > 0 Then

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from Temp_MovimentiContabiliRiga where ( TIPO = '' OR TIPO IS NULL) And Numero = ? ORDER BY RIGADACAUSALE")
            cmd.Connection = GeneraleCon
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione_2)

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim myriga As System.Data.DataRow = Tabella2.NewRow()
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 3 Then
                    myriga(0) = "Retta"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 4 Then
                    myriga(0) = "Extra Fissi"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                    myriga(0) = "Addebiti"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                    myriga(0) = "Accrediti"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 9 Then
                    myriga(0) = "Bollo"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 12 Then
                    myriga(0) = "Sconto"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 13 Then
                    myriga(0) = "Retta 2"
                End If
                If campodb(myPOSTreader.Item("RigaDaCausale")) = 14 Then
                    myriga(0) = "Retta Percentuale"
                End If

                If MyCausale.TipoDocumento = "NC" Then
                    If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                    Else
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                    End If
                Else
                    If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                    Else
                        myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                    End If
                End If

                myriga(2) = campodb(myPOSTreader.Item("Quantita"))

                If campodbn(myPOSTreader.Item("Quantita")) < 0 And Math.Abs(Math.Round(campodbn(myPOSTreader.Item("Importo")), 2)) = 0 Then
                    myriga(2) = 0
                End If


                If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                    Dim ClTipoOperazione As New Cls_TipoOperazione


                    ClTipoOperazione.Codice = TipoOperazione
                    ClTipoOperazione.Leggi(Session("DC_OSPITE"), ClTipoOperazione.Codice)

                    If campodb(myPOSTreader.Item("TipoExtra")) = "A" & ClTipoOperazione.TipoAddebitoAccredito2 Then
                        If MyCausale_2.TipoDocumento <> "NC" Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = Session("CODICEOSPITE")
                            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")

                            Mov.UltimaDataAccoglimento(Session("DC_OSPITE"))

                            myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                            If Quota2 > 0 Then
                                If Modulo.MathRound(myriga(1) / Quota2, 5) = Int(myriga(1) / Quota2) Then
                                    myriga(2) = Math.Abs(Int(myriga(1) / Quota2))
                                End If
                            End If
                        End If
                    End If
                End If

                If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                    Dim ClTipoOperazione As New Cls_TipoOperazione


                    ClTipoOperazione.Codice = TipoOperazione
                    ClTipoOperazione.Leggi(Session("DC_OSPITE"), ClTipoOperazione.Codice)

                    Dim TipoExtraRegistrazione As String = campodb(myPOSTreader.Item("TipoExtra")) & Space(10)

                    If TipoExtraRegistrazione.Trim = "A" & ClTipoOperazione.TipoAddebitoAccredito.Trim Or Mid(TipoExtraRegistrazione, 1, 1) = "E" Then
                        If MyCausale_2.TipoDocumento = "NC" Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = Session("CODICEOSPITE")
                            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")

                            Mov.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                            myriga(2) = DateDiff(DateInterval.Day, Mov.Data, DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
                            If Mid(TipoExtraRegistrazione, 1, 1) = "E" Then

                                Dim TipoExtra As New Cls_TipoExtraFisso

                                TipoExtra.CODICEEXTRA = Mid(TipoExtraRegistrazione, 2, 2)
                                TipoExtra.Leggi(Session("DC_OSPITE"))

                                If TipoExtra.IMPORTO > 0 And TipoExtra.TipoImporto = "G" Then
                                    If Modulo.MathRound(myriga(1) / TipoExtra.IMPORTO, 5) = Int(myriga(1) / TipoExtra.IMPORTO) Then
                                        myriga(2) = Math.Abs(Int(myriga(1) / TipoExtra.IMPORTO))
                                    End If
                                End If
                            Else
                                If Quota2 > 0 Then
                                    If Modulo.MathRound(myriga(1) / Quota2, 5) = Int(myriga(1) / Quota2) Then
                                        myriga(2) = Math.Abs(Int(myriga(1) / Quota2))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If


                myriga(3) = campodb(myPOSTreader.Item("CodiceIva"))
                myriga(4) = campodb(myPOSTreader.Item("Descrizione"))
                myriga(5) = campodb(myPOSTreader.Item("id"))

                Dim MS As New Cls_Pianodeiconti
                MS.Mastro = campodb(myPOSTreader.Item("MastroPartita"))
                MS.Conto = campodb(myPOSTreader.Item("ContoPartita"))
                MS.Sottoconto = campodb(myPOSTreader.Item("SottocontoPartita"))
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(6) = MS.Mastro & "  " & MS.Conto & " " & MS.Sottoconto & " " & MS.Descrizione

                Tabella2.Rows.Add(myriga)
            Loop
            myPOSTreader.Close()


            GridV2.AutoGenerateColumns = False

            GridV2.DataSource = Tabella2

            GridV2.DataBind()
        End If
        GeneraleCon.Close()
        Dim Parametri As New Cls_Parametri

        Parametri.FineElaborazione(Session("DC_OSPITE"))

    End Sub


    Protected Sub GridV_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridV.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblID As Label = DirectCast(e.Row.FindControl("LblID"), Label)

            LblID.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizioneRiga"), Label)

            LblDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString


            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString


            Dim TxtGiorni As TextBox = DirectCast(e.Row.FindControl("TxtGiorni"), TextBox)


            TxtGiorni.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString

            Dim DDIva As DropDownList = DirectCast(e.Row.FindControl("DDIva"), DropDownList)

            Dim MyIva As New Cls_IVA

            MyIva.UpDateDropBox(Session("DC_TABELLE"), DDIva)
            DDIva.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString



            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString

            Dim TxtContoRicavo As TextBox = DirectCast(e.Row.FindControl("TxtContoRicavo"), TextBox)

            TxtContoRicavo.Text = Tabella.Rows(e.Row.RowIndex).Item(6).ToString


            Dim AppImporto As Double = 0
            Dim AppQuantita As Double = 0
            If TxtImporto.Text = "" Then
                AppImporto = 0
            Else
                AppImporto = TxtImporto.Text
            End If
            If TxtGiorni.Text = "" Then
                AppQuantita = 0
            Else
                AppQuantita = TxtGiorni.Text
            End If
            If AppImporto = 0 And AppQuantita = 0 Then
                TxtImporto.Enabled = False
                TxtGiorni.Enabled = False
                TxtDescrizione.Enabled = False
                LblID.Enabled = False
                LblDescrizione.Enabled = False
                DDIva.Enabled = False
                TxtContoRicavo.Enabled = False
                e.Row.Visible = False
            End If




            Call EseguiJS()

        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Session("CampoWr") = ""
        Session("CampoErrori") = ""
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))

        End If



        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

        Dim CParenti As New Cls_Parenti
        Dim COspite As New ClsOspite

        COspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        CParenti.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_OspiteParenti)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then            
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Lbl_Errori.Text = ""
        Dim ConnectionString As String = Session("DC_OSPITE")

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

        If f.PeriodoFatturazioneSuServizio = 1 Then
            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            If Cserv.MeseFatturazione > 0 Then
                Dd_Mese.SelectedValue = Cserv.MeseFatturazione
                Txt_Anno.Text = Cserv.AnnoFatturazione
            Else
                Dd_Mese.SelectedValue = f.MeseFatturazione
                Txt_Anno.Text = f.AnnoFatturazione
            End If
        End If

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)



        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Lb_GestioneDocumento.Visible = False
        Lb_StampaDocumento.Visible = False

        Lbl_Riferimento730.Visible = False
        Lbl_RiferimentoNumero.Visible = False
        Txt_DataRif.Visible = False
        Txt_RifNumero.Visible = False




        If f.ModificaPeriodoFatturaNC = 0 Then
            Dd_Mese.Enabled = False
        End If

        If x.PERIODO = "F" Then
            Chk_SoloMese.Visible = False
            Chk_EscludiProgrammati.Visible = False
        End If

        Call EseguiJS()

        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String


        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"
        MyJs = MyJs & "if ($('#MENUDIV').html().length < 50) { ImpostaMenuAgraficaOspiti(); }"
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('ContoRicavo')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"


        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("RicercaAnagrafica.aspx?TIPO=FATTURANC")
    End Sub

 

    Protected Sub Btn_EstraiDatiFatture_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_EstraiDatiFatture.Click
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            REM Lbl_Errori.Text = "Data formalmente errata"

            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            REM Lbl_Errori.Text = "Digitare Anno"
            Call EseguiJS()
            Exit Sub
        End If

        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))


        Dim ParametriGenerale As New Cls_ParametriGenerale
        

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If f.InElaborazione(Session("DC_OSPITE")) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim AppoggioPar As String
        Dim AppoggioInput As String
        AppoggioPar = f.AnnoFatturazione & Format(f.MeseFatturazione, "00")

        AppoggioInput = Txt_Anno.Text & Format(Val(Dd_Mese.SelectedValue), "00")
        If AppoggioInput < AppoggioPar Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Periodo già calcolato non posso procedere');", True)
            REM Lbl_Errori.Text = "Digitare Anno"
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))

        End If

        If Session("CODICESERVIZIO") = "" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice servizio');", True)
            REM Lbl_Errori.Text = "Specificare codice servizio "
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Session("CODICEOSPITE")) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice ospite');", True)
            REM Lbl_Errori.Text = "Specificare codice ospite"
            Call EseguiJS()
            Exit Sub
        End If

        Dim m As New Cls_Parametri



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from notifiche ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")

        Lbl_Errori.Text = ""

        Call EstraiFTNC()

        Call EseguiJS()


        m.FineElaborazione(Session("DC_OSPITE"))

        ParametriGenerale.FineElaborazione(Session("DC_GENERALE"))


    End Sub

    Protected Sub GridV2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridV2.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblID As Label = DirectCast(e.Row.FindControl("LblID"), Label)

            LblID.Text = Tabella2.Rows(e.Row.RowIndex).Item(5).ToString


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizioneRiga"), Label)

            LblDescrizione.Text = Tabella2.Rows(e.Row.RowIndex).Item(0).ToString


            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = Tabella2.Rows(e.Row.RowIndex).Item(1).ToString


            Dim TxtGiorni As TextBox = DirectCast(e.Row.FindControl("TxtGiorni"), TextBox)


            TxtGiorni.Text = Tabella2.Rows(e.Row.RowIndex).Item(2).ToString

            Dim DDIva As DropDownList = DirectCast(e.Row.FindControl("DDIva"), DropDownList)

            Dim MyIva As New Cls_IVA

            MyIva.UpDateDropBox(Session("DC_TABELLE"), DDIva)
            DDIva.SelectedValue = Tabella2.Rows(e.Row.RowIndex).Item(3).ToString



            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = Tabella2.Rows(e.Row.RowIndex).Item(4).ToString

            Dim TxtContoRicavo As TextBox = DirectCast(e.Row.FindControl("TxtContoRicavo"), TextBox)

            TxtContoRicavo.Text = Tabella2.Rows(e.Row.RowIndex).Item(6).ToString


            Dim AppImporto As Double = 0
            Dim AppQuantita As Double = 0
            If TxtImporto.Text = "" Then
                AppImporto = 0
            Else
                AppImporto = TxtImporto.Text
            End If
            If TxtGiorni.Text = "" Then
                AppQuantita = 0
            Else
                AppQuantita = TxtGiorni.Text
            End If
            If AppImporto = 0 And AppQuantita = 0 Then
                TxtImporto.Enabled = False
                TxtGiorni.Enabled = False
                TxtDescrizione.Enabled = False
                LblID.Enabled = False
                LblDescrizione.Enabled = False
                DDIva.Enabled = False
                TxtContoRicavo.Enabled = False
                e.Row.Visible = False
            End If




            Call EseguiJS()

        End If
    End Sub

    Protected Sub Btn_Registra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Registra.Click
        Dim I As Integer
        Dim NumeroRegistrazione As Long = 0
        Dim Causale As String = ""

        Dim NumeroRegistrazione_2 As Long = 0
        Dim Causale_2 As String = ""
        Dim GeneraleCon As New OleDb.OleDbConnection
        Dim MastroCF As Long
        Dim ContoCF As Long
        Dim SottoContoCF As Long



        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))

        Dim AppoggioPar As String
        Dim AppoggioInput As String
        AppoggioPar = f.AnnoFatturazione & Format(f.MeseFatturazione, "00")

        AppoggioInput = Txt_Anno.Text & Format(Val(Dd_Mese.SelectedValue), "00")
        If AppoggioInput < AppoggioPar Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Periodo già calcolato non posso procedere');", True)
            REM Lbl_Errori.Text = "Digitare Anno"
            Call EseguiJS()
            Exit Sub
        End If


        If f.InElaborazione(Session("DC_OSPITE")) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If

        ParametriGenerale.Elaborazione(Session("DC_GENERALE"))
        f.Elaborazione(Session("DC_OSPITE"))


        Dim m1 As New Cls_Parametri


        If Txt_DataRif_2.Text = "" Then
            Txt_DataRif_2.Text = Format(Now, "dd/MM/yyyy")
        End If

        If Not IsDate(Txt_DataRif_2.Text) Then
            Txt_DataRif_2.Text = Format(Now, "dd/MM/yyyy")
        End If


        If Txt_DataRif.Text = "" Then
            Txt_DataRif.Text = Format(Now, "dd/MM/yyyy")
        End If

        If Not IsDate(Txt_DataRif.Text) Then
            Txt_DataRif.Text = Format(Now, "dd/MM/yyyy")
        End If


        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")

        GeneraleCon.ConnectionString = Session("DC_GENERALE")
        GeneraleCon.Open()

        Dim cmdREAD As New OleDbCommand()
        cmdREAD.CommandText = ("select * from Temp_MovimentiContabiliTesta  order by NumeroRegistrazione")
        cmdREAD.Connection = GeneraleCon


        Dim myPOSTreader As OleDbDataReader = cmdREAD.ExecuteReader()
        If myPOSTreader.Read Then
            NumeroRegistrazione = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            Causale = campodb(myPOSTreader.Item("CausaleContabile"))
            If myPOSTreader.Read Then
                NumeroRegistrazione_2 = campodb(myPOSTreader.Item("NumeroRegistrazione"))
                Causale_2 = campodb(myPOSTreader.Item("CausaleContabile"))
            End If
        End If
        myPOSTreader.Close()



        If NumeroRegistrazione > 0 Then
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = Causale
            CausaleContabile.Leggi(Session("DC_TABELLE"), Causale)
            If CausaleContabile.TipoDocumento = "NC" Then
                Dim cmdWrite As New OleDbCommand()
                cmdWrite.CommandText = ("UPDATE Temp_MovimentiContabiliTesta set RifNumero = ?,RifData = ? Where NumeroRegistrazione =?  ")
                cmdWrite.Connection = GeneraleCon
                cmdWrite.Parameters.AddWithValue("RifNumero", Txt_RifNumero.Text)
                Dim DataRiferimento As Date

                DataRiferimento = Txt_DataRif.Text
                cmdWrite.Parameters.AddWithValue("RifData", DataRiferimento)
                cmdWrite.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
                cmdWrite.ExecuteNonQuery()
            End If




            Dim cmdREADCF As New OleDbCommand()
            cmdREADCF.CommandText = ("select * from Temp_MovimentiContabiliRiga where TIPO = 'CF' And Numero  =? ")
            cmdREADCF.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            cmdREADCF.Connection = GeneraleCon

            Dim myPOSTreaderCF As OleDbDataReader = cmdREADCF.ExecuteReader()
            If myPOSTreaderCF.Read Then
                MastroCF = campodb(myPOSTreaderCF.Item("MastroPartita"))
                ContoCF = campodb(myPOSTreaderCF.Item("ContoPartita"))
                SottoContoCF = campodb(myPOSTreaderCF.Item("SottocontoPartita"))
            End If
            myPOSTreaderCF.Close()


            Dim MyCausale As New Cls_CausaleContabile

            MyCausale.Leggi(Session("DC_TABELLE"), Causale)

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("DELETE from Temp_MovimentiContabiliRiga where TIPO = 'IV' And Numero  =?")
            cmd.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            cmd.Connection = GeneraleCon
            cmd.ExecuteNonQuery()


            For I = 0 To GridV.Rows.Count - 1

                Dim TxtImporto As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtImporto"), TextBox)
                Dim TxtGiorni As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtGiorni"), TextBox)
                Dim DDIva As DropDownList = DirectCast(GridV.Rows(I).FindControl("DDIva"), DropDownList)
                Dim TxtDescrizione As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtDescrizione"), TextBox)
                Dim LblDescrizione As Label = DirectCast(GridV.Rows(I).FindControl("LblDescrizioneRiga"), Label)
                Dim LblID As Label = DirectCast(GridV.Rows(I).FindControl("LblID"), Label)
                Dim TxtContoRicavo As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtContoRicavo"), TextBox)

                Dim Imponibile As Double
                Dim Imposta As Double

                If TxtImporto.Text = "" Then
                    TxtImporto.Text = "0"
                End If
                If TxtGiorni.Text = "" Then
                    TxtGiorni.Text = "0"
                End If


                If TxtGiorni.Text = "0" And TxtImporto.Text = "0" Then
                    Dim cmdMod As New OleDbCommand()
                    cmdMod.CommandText = ("DELETE Temp_MovimentiContabiliRiga where id = ? ")
                    cmdMod.Connection = GeneraleCon
                    cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                    cmdMod.ExecuteNonQuery()
                Else
                    Imponibile = TxtImporto.Text
                    Dim MyIva As New Cls_IVA
                    MyIva.Codice = DDIva.SelectedValue
                    MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)

                    Imposta = Modulo.MathRound(Imponibile * MyIva.Aliquota, 2)


                    Dim cmdIns As New OleDbCommand()
                    cmdIns.CommandText = ("INSERT INTO Temp_MovimentiContabiliRiga (Utente,DataAggiornamento,Numero,MastroPartita,ContoPartita,SottocontoPartita,MastroContropartita,ContoContropartita,SottocontoContropartita,Importo,DareAvere,Segno,Tipo,CodiceIVA,Imponibile,RigaDaCausale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    cmdIns.Connection = GeneraleCon
                    cmdIns.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                    cmdIns.Parameters.AddWithValue("@DataAggiornamento", Now)
                    cmdIns.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
                    cmdIns.Parameters.AddWithValue("@MastroPartita", MyCausale.Righe(1).Mastro)
                    cmdIns.Parameters.AddWithValue("@ContoPartita", MyCausale.Righe(1).Conto)
                    cmdIns.Parameters.AddWithValue("@SottocontoPartita", MyCausale.Righe(1).Sottoconto)

                    cmdIns.Parameters.AddWithValue("@MastroContropartita", MastroCF)
                    cmdIns.Parameters.AddWithValue("@ContoContropartita", ContoCF)
                    cmdIns.Parameters.AddWithValue("@SottocontoContropartita", SottoContoCF)

                    cmdIns.Parameters.AddWithValue("@Importo", Math.Abs(Imposta))
                    If MyCausale.TipoDocumento = "NC" Then
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    Else
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    End If
                    cmdIns.Parameters.AddWithValue("@Segno", "+")
                    cmdIns.Parameters.AddWithValue("@Tipo", "IV")
                    cmdIns.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                    cmdIns.Parameters.AddWithValue("@Imponibile", Math.Abs(Imponibile))
                    cmdIns.Parameters.AddWithValue("@RigaDaCausale", 2)


                    cmdIns.ExecuteNonQuery()

                    Dim Vettore(100) As String





                    If TxtContoRicavo.Text = "" Then
                        Dim cmdMod As New OleDbCommand()
                        cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?  where id = ? ")
                        cmdMod.Connection = GeneraleCon

                        cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                        cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                        cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                        cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                        If MyCausale.TipoDocumento = "NC" Then
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        Else
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        End If


                        cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                        cmdMod.ExecuteNonQuery()
                    Else

                        Vettore = SplitWords(TxtContoRicavo.Text)


                        Dim cmdMod As New OleDbCommand()
                        cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?,MastroPartita = ?,ContoPartita =?,SottocontoPartita =?  where id = ? ")
                        cmdMod.Connection = GeneraleCon

                        cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                        cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                        cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                        cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                        If MyCausale.TipoDocumento = "NC" Then
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        Else
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        End If

                        cmdMod.Parameters.AddWithValue("@MastroPartita", Vettore(0))
                        cmdMod.Parameters.AddWithValue("@ContoPartita", Vettore(1))
                        cmdMod.Parameters.AddWithValue("@SottoContoPartita", Vettore(2))

                        cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                        cmdMod.ExecuteNonQuery()
                    End If
                End If

            Next
            Dim DareAvere As Double = 0

            Dim cmdSUM As New OleDbCommand()
            cmdSUM.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'D' And  (TIPO <> 'CF' or TIPO IS NULL) And Numero  =?")
            cmdSUM.Connection = GeneraleCon
            cmdSUM.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            Dim RDSUM As OleDbDataReader = cmdSUM.ExecuteReader()
            If RDSUM.Read Then
                DareAvere = campodbn(RDSUM.Item(0))
            End If
            RDSUM.Close()


            Dim cmdSUMA As New OleDbCommand()
            cmdSUMA.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'A' And  (TIPO <> 'CF' or TIPO IS NULL) And Numero  =?")
            cmdSUMA.Connection = GeneraleCon
            cmdSUMA.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            Dim RDSUMA As OleDbDataReader = cmdSUMA.ExecuteReader()
            If RDSUMA.Read Then
                DareAvere = DareAvere - campodbn(RDSUMA.Item(0))
            End If
            RDSUMA.Close()

            Dim cmdModcf As New OleDbCommand()
            cmdModcf.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?   where TIPO= 'CF' And Numero  =?")
            cmdModcf.Connection = GeneraleCon
            cmdModcf.Parameters.AddWithValue("@Importo", Math.Abs(Modulo.MathRound(DareAvere, 2)))
            cmdModcf.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            cmdModcf.ExecuteNonQuery()
        End If


        If NumeroRegistrazione_2 > 0 Then
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = Causale
            CausaleContabile.Leggi(Session("DC_TABELLE"), Causale)
            If CausaleContabile.TipoDocumento = "NC" Then
                Dim cmdWrite As New OleDbCommand()
                cmdWrite.CommandText = ("UPDATE Temp_MovimentiContabiliTesta set RifNumero = ?,RifData = ? Where NumeroRegistrazione =?  ")
                cmdWrite.Connection = GeneraleCon
                cmdWrite.Parameters.AddWithValue("RifNumero", Txt_RifNumero_2.Text)
                Dim DataRiferimento As Date

                DataRiferimento = Txt_DataRif_2.Text
                cmdWrite.Parameters.AddWithValue("RifData", DataRiferimento)
                cmdWrite.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
                cmdWrite.ExecuteNonQuery()
            End If




            Dim cmdREADCF As New OleDbCommand()
            cmdREADCF.CommandText = ("select * from Temp_MovimentiContabiliRiga where TIPO = 'CF' And Numero =? ")
            cmdREADCF.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
            cmdREADCF.Connection = GeneraleCon

            Dim myPOSTreaderCF As OleDbDataReader = cmdREADCF.ExecuteReader()
            If myPOSTreaderCF.Read Then
                MastroCF = campodb(myPOSTreaderCF.Item("MastroPartita"))
                ContoCF = campodb(myPOSTreaderCF.Item("ContoPartita"))
                SottoContoCF = campodb(myPOSTreaderCF.Item("SottocontoPartita"))
            End If
            myPOSTreaderCF.Close()


            Dim MyCausale As New Cls_CausaleContabile

            MyCausale.Leggi(Session("DC_TABELLE"), Causale)

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("DELETE from Temp_MovimentiContabiliRiga where TIPO = 'IV'  And Numero =?")
            cmd.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
            cmd.Connection = GeneraleCon
            cmd.ExecuteNonQuery()


            For I = 0 To GridV2.Rows.Count - 1

                Dim TxtImporto As TextBox = DirectCast(GridV2.Rows(I).FindControl("TxtImporto"), TextBox)
                Dim TxtGiorni As TextBox = DirectCast(GridV2.Rows(I).FindControl("TxtGiorni"), TextBox)
                Dim DDIva As DropDownList = DirectCast(GridV2.Rows(I).FindControl("DDIva"), DropDownList)
                Dim TxtDescrizione As TextBox = DirectCast(GridV2.Rows(I).FindControl("TxtDescrizione"), TextBox)
                Dim LblDescrizione As Label = DirectCast(GridV2.Rows(I).FindControl("LblDescrizioneRiga"), Label)
                Dim LblID As Label = DirectCast(GridV2.Rows(I).FindControl("LblID"), Label)
                Dim TxtContoRicavo As TextBox = DirectCast(GridV2.Rows(I).FindControl("TxtContoRicavo"), TextBox)

                Dim Imponibile As Double
                Dim Imposta As Double

                If TxtImporto.Text = "" Then
                    TxtImporto.Text = "0"
                End If
                If TxtGiorni.Text = "" Then
                    TxtGiorni.Text = "0"
                End If


                If TxtGiorni.Text = "0" And TxtImporto.Text = "0" Then
                    Dim cmdMod As New OleDbCommand()
                    cmdMod.CommandText = ("DELETE Temp_MovimentiContabiliRiga where id = ? ")
                    cmdMod.Connection = GeneraleCon
                    cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                    cmdMod.ExecuteNonQuery()
                Else
                    Imponibile = TxtImporto.Text
                    Dim MyIva As New Cls_IVA
                    MyIva.Codice = DDIva.SelectedValue
                    MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)

                    Imposta = Modulo.MathRound(Imponibile * MyIva.Aliquota, 2)


                    Dim cmdIns As New OleDbCommand()
                    cmdIns.CommandText = ("INSERT INTO Temp_MovimentiContabiliRiga (Utente,DataAggiornamento,Numero,MastroPartita,ContoPartita,SottocontoPartita,MastroContropartita,ContoContropartita,SottocontoContropartita,Importo,DareAvere,Segno,Tipo,CodiceIVA,Imponibile,RigaDaCausale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    cmdIns.Connection = GeneraleCon
                    cmdIns.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                    cmdIns.Parameters.AddWithValue("@DataAggiornamento", Now)
                    cmdIns.Parameters.AddWithValue("@Numero", NumeroRegistrazione_2)
                    cmdIns.Parameters.AddWithValue("@MastroPartita", MyCausale.Righe(1).Mastro)
                    cmdIns.Parameters.AddWithValue("@ContoPartita", MyCausale.Righe(1).Conto)
                    cmdIns.Parameters.AddWithValue("@SottocontoPartita", MyCausale.Righe(1).Sottoconto)

                    cmdIns.Parameters.AddWithValue("@MastroContropartita", MastroCF)
                    cmdIns.Parameters.AddWithValue("@ContoContropartita", ContoCF)
                    cmdIns.Parameters.AddWithValue("@SottocontoContropartita", SottoContoCF)

                    cmdIns.Parameters.AddWithValue("@Importo", Math.Abs(Imposta))
                    If MyCausale.TipoDocumento = "NC" Then
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    Else
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    End If
                    cmdIns.Parameters.AddWithValue("@Segno", "+")
                    cmdIns.Parameters.AddWithValue("@Tipo", "IV")
                    cmdIns.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                    cmdIns.Parameters.AddWithValue("@Imponibile", Math.Abs(Imponibile))
                    cmdIns.Parameters.AddWithValue("@RigaDaCausale", 2)


                    cmdIns.ExecuteNonQuery()

                    Dim Vettore(100) As String





                    If TxtContoRicavo.Text = "" Then
                        Dim cmdMod As New OleDbCommand()
                        cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?  where id = ? ")
                        cmdMod.Connection = GeneraleCon

                        cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                        cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                        cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                        cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                        If MyCausale.TipoDocumento = "NC" Then
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        Else
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        End If


                        cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                        cmdMod.ExecuteNonQuery()
                    Else

                        Vettore = SplitWords(TxtContoRicavo.Text)


                        Dim cmdMod As New OleDbCommand()
                        cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?,MastroPartita = ?,ContoPartita =?,SottocontoPartita =?  where id = ? ")
                        cmdMod.Connection = GeneraleCon

                        cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                        cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                        cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                        cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                        If MyCausale.TipoDocumento = "NC" Then
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        Else
                            If CDbl(TxtImporto.Text) < 0 Then
                                cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                            Else
                                cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                            End If
                        End If

                        cmdMod.Parameters.AddWithValue("@MastroPartita", Vettore(0))
                        cmdMod.Parameters.AddWithValue("@ContoPartita", Vettore(1))
                        cmdMod.Parameters.AddWithValue("@SottoContoPartita", Vettore(2))

                        cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                        cmdMod.ExecuteNonQuery()
                    End If
                End If

            Next
            Dim DareAvere As Double = 0

            Dim cmdSUM As New OleDbCommand()
            cmdSUM.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'D' And  (TIPO <> 'CF' or TIPO IS NULL) And Numero  =?")
            cmdSUM.Connection = GeneraleCon
            cmdSUM.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
            Dim RDSUM As OleDbDataReader = cmdSUM.ExecuteReader()
            If RDSUM.Read Then
                DareAvere = campodbn(RDSUM.Item(0))
            End If
            RDSUM.Close()


            Dim cmdSUMA As New OleDbCommand()
            cmdSUMA.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'A' And  (TIPO <> 'CF' or TIPO IS NULL) And Numero =?")
            cmdSUMA.Connection = GeneraleCon
            cmdSUMA.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
            Dim RDSUMA As OleDbDataReader = cmdSUMA.ExecuteReader()
            If RDSUMA.Read Then
                DareAvere = DareAvere - campodbn(RDSUMA.Item(0))
            End If
            RDSUMA.Close()

            Dim cmdModcf As New OleDbCommand()
            cmdModcf.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?   where TIPO= 'CF' And Numero =?")
            cmdModcf.Connection = GeneraleCon
            cmdModcf.Parameters.AddWithValue("@Importo", Math.Abs(Modulo.MathRound(DareAvere, 2)))
            cmdModcf.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione_2)
            cmdModcf.ExecuteNonQuery()
        End If

        GeneraleCon.Close()


        Dim Em As New Cls_EmissioneRetta

        Em.ConnessioneGenerale = Session("DC_GENERALE")
        Em.ConnessioneOspiti = Session("DC_OSPITE")
        Em.ConnessioneTabelle = Session("DC_TABELLE")
        Em.ApriDB()

        Call Em.VerificaRegistrazioneIVA(NumeroRegistrazione, False)

        Dim LeggiParametri As New Cls_Parametri


        LeggiParametri.LeggiParametri(Em.ConnessioneOspiti)

        If LeggiParametri.RaggruppaRicavi = 1 Then
            Call Em.RaggruppaContiRicavo()
        End If


        Em.CloseDB()


        Response.Redirect("StampaFattureProva.aspx?TIPO=NONEMISSIONE&URL=FatturaNC.aspx")
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub DD_OspiteParenti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_OspiteParenti.SelectedIndexChanged
        Call EseguiJS()
    End Sub
End Class
