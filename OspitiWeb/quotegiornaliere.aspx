﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_quotegiornaliere" CodeFile="quotegiornaliere.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Quote Ospite</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>

    <script src="../js/d3.v3.min.js" type="text/javascript"></script>

    <script src="../js/Donut3D.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            font-family: "Source Sans Pro",sans-serif;
            font-size: 12px;
            line-height: 16px;
            margin: 20px 40px;
        }

        .percent {
            color: White;
            text-align: center;
        }

        h1 {
            font-size: 24px;
            margin: 5px 0 0 0;
            padding: 0 0 12px 0;
        }

        a {
            color: #690;
            text-decoration: none;
        }

            a:hover {
                color: #e60;
                text-decoration: none;
            }

        p {
            margin: 0 0 9px 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_NomeOspite" runat="server" Text=""></asp:Label>
            <div style="display: flex;">
                <div id="chart2" style="height: 200px; width: 300px; float: left;"></div>
                <div style="width: 120px;">
                    <div style="width: 20px; height: 20px; background-color: #F2C485; float: left;"></div>
                    <font style="padding-left: 5px;">Ospite <asp:Label ID="lblOSpite" runat="server" Text=""></asp:Label></font>
                    <br />
                    <br />
                    <div style="width: 20px; height: 20px; background-color: #F28DB3; float: left;"></div>
                    <font style="padding-left: 5px;">Parenti <asp:Label ID="lblParenti" runat="server" Text=""></asp:Label></font>
                    <br />
                    <br />
                    <div style="width: 20px; height: 20px; background-color: #BE87EB; float: left;"></div>
                    <font style="padding-left: 5px;">Comune <asp:Label ID="lblComune" runat="server" Text=""></asp:Label></font>
                    <br />
                    <br />
                    <div style="width: 20px; height: 20px; background-color: #8AB8F7; float: left;"></div>
                    <font style="padding-left: 5px;">Regione <asp:Label ID="lblRegione" runat="server" Text=""></asp:Label></font>
                    <br />
                    <br />
                    <div style="width: 20px; height: 20px; background-color: #8AEFC9; float: left;"></div>
                    <font style="padding-left: 5px;">Jolly <asp:Label ID="lblJolly" runat="server" Text=""></asp:Label></font>
                    <br />
                    <br />
                </div>
            </div>

        </div>
    </form>
</body>
</html>
