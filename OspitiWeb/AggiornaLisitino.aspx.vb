﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_AggiornaLisitino
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Exit Sub
        End If


        If DD_Lisitino.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il Lisitino');", True)
            Exit Sub
        End If


        If DD_ALisitino.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il Lisitino di destinazione');", True)
            Exit Sub
        End If


        Dim mListino As New Cls_Tabella_Listino


        mListino.Codice = DD_Lisitino.SelectedValue
        mListino.LeggiCausale(Session("DC_OSPITE"))


        Dim mListino2 As New Cls_Tabella_Listino


        mListino2.Codice = DD_ALisitino.SelectedValue
        mListino2.LeggiCausale(Session("DC_OSPITE"))

        If mListino2.TIPOLISTINO <> mListino.TIPOLISTINO Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('I tipi listino Da-A devono coincidere');", True)
            Exit Sub
        End If

        Call Ricerca()

    End Sub

    Private Sub Ricerca()
        Dim mListino As New Cls_Tabella_Listino


        mListino.Codice = DD_Lisitino.SelectedValue
        mListino.LeggiCausale(Session("DC_OSPITE"))



        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim MySql As String
        Dim DataRegistrazione As Date


        DataRegistrazione = Txt_DataRegistrazione.Text

        MySql = "SELECT     CODICEOSPITE,"
        MySql = MySql & "(SELECT     MAX(DATA) AS Expr1"
        MySql = MySql & " FROM          MOVIMENTI"
        MySql = MySql & "   WHERE      (CODICEOSPITE = AnagraficaComune.CODICEOSPITE) AND (TIPOMOV = '13') AND (CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "')) "
        MySql = MySql & " AS DataUscitaDefinitiva,"
        MySql = MySql & "(SELECT     MAX(DATA) AS Expr1"
        MySql = MySql & " FROM          MOVIMENTI AS MOVIMENTI_1"
        MySql = MySql & "    WHERE      (CODICEOSPITE = AnagraficaComune.CODICEOSPITE) AND (TIPOMOV = '05') AND (CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "')) "
        MySql = MySql & " AS DataAccoglimento"
        MySql = MySql & " FROM         AnagraficaComune WHERE     (TIPOLOGIA = 'O') Order by Nome"

        cmd.CommandText = MySql
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(Integer))
        Tabella.Columns.Add("NomeOspite", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("ImportoAttuale", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("StessaData", GetType(String))
        Tabella.Columns.Add("QSANITARIA", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If campodb(myPOSTreader.Item("DataAccoglimento")) <> "" Then
                Dim CodiceOSpite As Long
                Dim DataUscita As Date
                Dim DataAccoglimento As Date

                CodiceOSpite = campodb(myPOSTreader.Item("CODICEOSPITE"))


                If campodb(myPOSTreader.Item("DataUscitaDefinitiva")) = "" Then
                    DataUscita = DateSerial(2099, 1, 1)
                Else
                    DataUscita = campodb(myPOSTreader.Item("DataUscitaDefinitiva"))
                End If

                If campodb(myPOSTreader.Item("DataAccoglimento")) = "" Then
                    DataAccoglimento = DateSerial(1900, 1, 1)
                Else
                    DataAccoglimento = campodb(myPOSTreader.Item("DataAccoglimento"))
                End If

                If CodiceOSpite = 29 Then
                    CodiceOSpite = 29
                End If



                If Format(DataUscita, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd") Or (Format(DataAccoglimento, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd")) Or (Format(DataAccoglimento, "yyyyMMdd") > Format(DataUscita, "yyyyMMdd") And Format(DataAccoglimento, "yyyyMMdd") < Format(DataRegistrazione, "yyyyMMdd")) Then

                    Dim Listino As New Cls_Listino

                    Listino.CODICEOSPITE = CodiceOSpite
                    Listino.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                    Listino.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)
                    Dim Includi As Boolean = False

                    If mListino.TIPOLISTINO = "" Then
                        If Listino.CodiceListino = DD_Lisitino.SelectedValue Then
                            Includi = True
                        End If
                    End If

                    If mListino.TIPOLISTINO = "S" Then
                        If Listino.CodiceListino = DD_Lisitino.SelectedValue Then
                            Includi = True
                        End If
                    End If


                    If mListino.TIPOLISTINO = "R" Then
                        If Listino.CodiceListinoSanitario = DD_Lisitino.SelectedValue Then
                            Includi = True
                        End If
                    End If


                    If mListino.TIPOLISTINO = "C" Then
                        If Listino.CodiceListinoSociale = DD_Lisitino.SelectedValue Then
                            Includi = True
                        End If
                    End If

                    If mListino.TIPOLISTINO = "J" Then
                        If Listino.CodiceListinoJolly = DD_Lisitino.SelectedValue Then
                            Includi = True
                        End If
                    End If

                    If Includi Then



                        Dim KAuto As New Cls_StatoAuto

                        KAuto.UltimaData(Session("DC_OSPITE"), CodiceOSpite, DD_CentroServizio.SelectedValue)




                        Dim XAna As New ClsOspite

                        XAna.Leggi(Session("DC_OSPITE"), CodiceOSpite)

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()


                        myriga(0) = CodiceOSpite
                        myriga(1) = XAna.Nome

                        Dim Qusl As New Cls_CalcoloRette


                        Qusl.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                        Qusl.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                        Dim KCS As New Cls_DatiOspiteParenteCentroServizio


                        KCS.CentroServizio = DD_CentroServizio.SelectedValue
                        KCS.CodiceOspite = CodiceOSpite
                        KCS.CodiceParente = 0
                        KCS.Leggi(Session("DC_OSPITE"))

                        If KCS.AliquotaIva <> "" Then
                            XAna.CODICEIVA = KCS.AliquotaIva
                        End If


                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 1, Now) > 0 Then


                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 1)

                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 1
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If


                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 2, Now) > 0 Then

                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 2)


                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 2
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If



                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 3, Now) > 0 Then

                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 3)

                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 3
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If


                        Dim k As New Cls_rettatotale

                        k.UltimaData(Session("DC_OSPITE"), CodiceOSpite, DD_CentroServizio.SelectedValue)

                        If Format(DataAccoglimento, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd") Then
                            myriga(2) = Format(DataAccoglimento, "dd/MM/yyyy")
                        Else
                            myriga(2) = Format(DataRegistrazione, "dd/MM/yyyy")
                        End If
                        myriga(3) = Format(k.Importo, "#,##0.00")
                        If Format(k.Data, "yyyyMMdd") = Format(DataRegistrazione, "yyyyMMdd") Then
                            myriga(4) = "S"
                        End If




                        myriga(6) = Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "R", 0, Now)

                        Dim kDec As New Cls_IVA

                        kDec.Codice = XAna.CODICEIVA
                        kDec.Leggi(Session("DC_TABELLE"), kDec.Codice)


                        myriga(7) = kDec.Descrizione


                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()


        ViewState("SalvaTabella") = Tabella


        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()


        For i = 0 To Grd_AggiornaRette.Rows.Count - 1
            If campodb(Tabella.Rows(i).Item(4)) = "S" Then
                Grd_AggiornaRette.Rows(i).Cells(0).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(1).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(2).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(3).ForeColor = Drawing.Color.Violet

            End If
        Next



        Call EseguiJS()


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)



        Dim ddlisitno As New Cls_Tabella_Listino

        ddlisitno.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, "A")




        ddlisitno.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_ALisitino, "A")

        Call EseguiJS()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Private Sub UpDateTable()
        Dim i As Integer

        Tabella = ViewState("SalvaTabella")



    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Exit Sub
        End If


        If DD_Lisitino.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il Lisitino');", True)
            Exit Sub
        End If


        If DD_ALisitino.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il Lisitino di destinazione');", True)
            Exit Sub
        End If


        Dim mListinoChk As New Cls_Tabella_Listino


        mListinoChk.Codice = DD_Lisitino.SelectedValue
        mListinoChk.LeggiCausale(Session("DC_OSPITE"))


        Dim mListinoChk2 As New Cls_Tabella_Listino


        mListinoChk2.Codice = DD_ALisitino.SelectedValue
        mListinoChk2.LeggiCausale(Session("DC_OSPITE"))

        If mListinoChk2.TIPOLISTINO <> mListinoChk.TIPOLISTINO Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('I tipi listino Da-A devono coincidere');", True)
            Exit Sub
        End If

        Dim mListino As New Cls_Tabella_Listino


        mListino.Codice = DD_Lisitino.SelectedValue
        mListino.LeggiCausale(Session("DC_OSPITE"))

        Dim cn As OleDbConnection

        Dim DataRegistrazione As Date

        Dim DataAccoglimento As Date


        DataRegistrazione = Txt_DataRegistrazione.Text


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim i As Integer

        Tabella = ViewState("SalvaTabella")

        For i = 0 To Grd_AggiornaRette.Rows.Count - 1


            Dim ChkAggiornamento As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("chk_listino"), CheckBox)

            If ChkAggiornamento.Checked = True Then
                Dim Listi As New Cls_Listino
                Dim CODICEOSPITE As Integer

                CODICEOSPITE = Tabella.Rows(i).Item(0)

                Try
                    If mListino.TIPOLISTINO = "" Then
                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, DD_ALisitino.SelectedValue, Txt_DataRegistrazione.Text)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CodiceListino = DD_ALisitino.SelectedValue
                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If


                    If mListino.TIPOLISTINO = "S" Then

                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)

                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, DD_ALisitino.SelectedValue, Txt_DataRegistrazione.Text, Listi.CodiceListinoPrivatoSanitario, Listi.CodiceListinoSanitario, Listi.CodiceListinoSociale, Listi.CodiceListinoJolly)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CodiceListino = DD_ALisitino.SelectedValue

                        Listi.CodiceListinoPrivatoSanitario = Listi.CodiceListinoPrivatoSanitario
                        Listi.CodiceListinoSanitario = Listi.CodiceListinoSanitario
                        Listi.CodiceListinoSociale = Listi.CodiceListinoSociale
                        Listi.CodiceListinoJolly = Listi.CodiceListinoJolly

                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If

                    If mListino.TIPOLISTINO = "P" Then

                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)

                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, Listi.CodiceListino, Txt_DataRegistrazione.Text, DD_ALisitino.SelectedValue, Listi.CodiceListinoSanitario, Listi.CodiceListinoSociale, Listi.CodiceListinoJolly)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE

                        Listi.CodiceListino = Listi.CodiceListino
                        Listi.CodiceListinoPrivatoSanitario = Listi.CodiceListinoPrivatoSanitario
                        Listi.CodiceListinoSanitario = DD_ALisitino.SelectedValue
                        Listi.CodiceListinoSociale = Listi.CodiceListinoSociale
                        Listi.CodiceListinoJolly = Listi.CodiceListinoJolly
                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If


                    If mListino.TIPOLISTINO = "R" Then

                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)

                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, Listi.CodiceListino, Txt_DataRegistrazione.Text, Listi.CodiceListinoPrivatoSanitario, DD_ALisitino.SelectedValue, Listi.CodiceListinoSociale, Listi.CodiceListinoJolly)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE

                        Listi.CodiceListino = Listi.CodiceListino

                        Listi.CodiceListinoPrivatoSanitario = Listi.CodiceListinoPrivatoSanitario
                        Listi.CodiceListinoSanitario = DD_ALisitino.SelectedValue
                        Listi.CodiceListinoSociale = Listi.CodiceListinoSociale
                        Listi.CodiceListinoJolly = Listi.CodiceListinoJolly
                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If

                    If mListino.TIPOLISTINO = "C" Then

                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)

                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, Listi.CodiceListino, Txt_DataRegistrazione.Text, Listi.CodiceListinoPrivatoSanitario, Listi.CodiceListinoSanitario, DD_ALisitino.SelectedValue, Listi.CodiceListinoJolly)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CodiceListino = Listi.CodiceListino
                        Listi.CodiceListinoPrivatoSanitario = Listi.CodiceListinoPrivatoSanitario
                        Listi.CodiceListinoSanitario = Listi.CodiceListinoSanitario
                        Listi.CodiceListinoSociale = DD_ALisitino.SelectedValue
                        Listi.CodiceListinoJolly = Listi.CodiceListinoJolly


                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If

                    If mListino.TIPOLISTINO = "J" Then

                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.LeggiAData(Session("DC_OSPITE"), Txt_DataRegistrazione.Text)

                        Listi.InserisciRette(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue, CODICEOSPITE, Listi.CodiceListino, Txt_DataRegistrazione.Text, Listi.CodiceListinoPrivatoSanitario, Listi.CodiceListinoSanitario, Listi.CodiceListinoSociale, DD_ALisitino.SelectedValue)


                        Listi.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        Listi.CODICEOSPITE = CODICEOSPITE
                        Listi.CodiceListino = Listi.CodiceListino
                        Listi.CodiceListinoPrivatoSanitario = Listi.CodiceListinoPrivatoSanitario
                        Listi.CodiceListinoSanitario = Listi.CodiceListinoSanitario
                        Listi.CodiceListinoSociale = Listi.CodiceListinoSociale
                        Listi.CodiceListinoJolly = DD_ALisitino.SelectedValue

                        Listi.DATA = Txt_DataRegistrazione.Text
                        Listi.AggiornaDB(Session("DC_OSPITE"))
                    End If


                    ChkAggiornamento.Checked = False
                Catch ex As Exception
                    ChkAggiornamento.Checked = True
                End Try

            End If

        Next
        cn.Close()

        Call Ricerca()


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", "alert('Operazione effettuata');", True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    
    Protected Sub Grd_AggiornaRette_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_AggiornaRette.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




        End If
    End Sub

    Protected Sub DD_CentroServizio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CentroServizio.SelectedIndexChanged

    End Sub

    Protected Sub DD_CentroServizio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CentroServizio.TextChanged

        Dim ddlisitno As New Cls_Tabella_Listino

        ddlisitno.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, DD_CentroServizio.SelectedValue)




        ddlisitno.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_ALisitino, DD_CentroServizio.SelectedValue)


    End Sub


    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To Grd_AggiornaRette.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(Grd_AggiornaRette.Rows(Riga).FindControl("chk_listino"), CheckBox)


            CheckBox.Checked = True
        Next
    End Sub
End Class
