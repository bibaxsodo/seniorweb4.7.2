﻿Imports System.IO
Imports System.Security.Cryptography.SHA1Managed
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression

Partial Class OspitiWeb_Gdpr_DatiSensibili
    Inherits System.Web.UI.Page


    Private Sub ExportPDF(ByVal NomeFile As String)
        Dim objStreamReader As StreamReader


        objStreamReader = File.OpenText(Server.MapPath(NomeFile))

        ' Leggo il contenuto del file sino alla fine (ReadToEnd)
        Dim contenuto As String = objStreamReader.ReadToEnd()

        objStreamReader.Close()

        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))

        contenuto = contenuto.Replace("@NOMETITOLARE@", TabSoc.RagioneSociale)
        contenuto = contenuto.Replace("@INDIRIZZO@", TabSoc.Indirizzo)
        contenuto = contenuto.Replace("@CAP@", TabSoc.Cap)
        contenuto = contenuto.Replace("@CITTA@", TabSoc.Localita)
        contenuto = contenuto.Replace("@TEL@", TabSoc.Telefono)
        contenuto = contenuto.Replace("@EMAIL@", TabSoc.Telefono)


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        contenuto = Server.UrlEncode(contenuto.Replace(vbNewLine, ""))
        Response.Clear()

        Dim sb As New StringBuilder()
        sb.Append("<html>")
        sb.AppendFormat("<body onload='document.forms[""form""].submit()'>")
        sb.AppendFormat("<form name='form' action='http://94.177.206.89/DaHtmlaPdf/dagetapdf.aspx?&VERIFICA=" & SHA1("SAURO" & contenuto & "GABELLINI") & "' method='post'>")
        sb.AppendFormat("<textarea name='URLHTML' style='display:none;'>" & contenuto & "</textarea>")

        sb.Append("</form>")
        sb.Append("</body>")
        sb.Append("</html>")

        Response.Write(sb.ToString())

        Response.End()
    End Sub



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If
        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        DD_Report.Items.Add("Informativa Ospite Fornita da Ospite")
        DD_Report.Items.Add("Informativa Ospite Fornita da Parente")
        DD_Report.Items.Add("Informativa Parente Fornita da Ospite")
        DD_Report.Items.Add("Informativa Parente Forniti da Parente")
    End Sub

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click

        If DD_Report.SelectedIndex = 0 Then
            ExportPDF("..\modulo\Infomartiva_GDPR_DatiOspitefornitiDaOspite.html")
        End If
        If DD_Report.SelectedIndex = 1 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiOspitefornitiDaParente.html")
        End If
        If DD_Report.SelectedIndex = 2 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiParentefornitiDaOspite.html")
        End If
        If DD_Report.SelectedIndex = 3 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiParentefornitiDaParente.html")
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub
End Class
