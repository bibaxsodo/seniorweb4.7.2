﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_EstraiPerSociale
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub OspitiWeb_EstraiPerUSLSanitario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        Dim Stampa As New StampeOspiti
        Dim Cserv As String = ""
        Dim Regione As String = ""

        Stampa = Session("stampa")


        'Dim Stampa1 As New StampeOspiti

        'For i = 0 To Stampa.RendicontoUSl.Rows.Count - 1
        '    Dim myriga As System.Data.DataRow = Stampa1.RendicontoUSl.NewRow
        '    For x = 0 To Stampa.RendicontoUSl.Columns.Count - 1
        '        myriga(x) = Stampa.RendicontoUSl.Rows(i).Item(x)
        '    Next
        '    Stampa1.RendicontoUSl.Rows.Add(myriga)
        'Next

        'Session("Salvastampa1") = Stampa1

        Tabella.Clear()
        Tabella.Columns.Add("CentroServizio", GetType(String))
        Tabella.Columns.Add("Comune", GetType(String))


        For i = 0 To Stampa.RendicontoComune.Rows.Count - 1

            If Stampa.RendicontoComune.Rows(i).Item("DECODIFICACOMUNE").ToString <> Regione Or Stampa.RendicontoComune.Rows(i).Item("CODICESERVIZIO").ToString <> Cserv Then
                Regione = Stampa.RendicontoComune.Rows(i).Item("DECODIFICACOMUNE").ToString()
                Cserv = Stampa.RendicontoComune.Rows(i).Item("CODICESERVIZIO").ToString
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = Cserv
                myriga(1) = Regione
                Tabella.Rows.Add(myriga)

            End If
        Next


        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
        ViewState("Appoggio") = Tabella
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging

    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Cserv As String = ""
            Dim Regione As String = ""

            Cserv = Tabella.Rows(d).Item(0).ToString
            Regione = Tabella.Rows(d).Item(1).ToString

            Dim Stampa As New StampeOspiti


            Session("SelectionFormula") = "{RendicontoComune.DECODIFICACOMUNE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoComune.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34)



            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNE&FORMULA=SI','StampeRendiconto_2','width=800,height=600');", True)
        End If

        If e.CommandName = "Export" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Cserv As String = ""
            Dim Regione As String = ""

            Cserv = Tabella.Rows(d).Item(0).ToString
            Regione = Tabella.Rows(d).Item(1).ToString

            Dim Stampa As New StampeOspiti


            Session("SelectionFormula") = "{RendicontoComune.DECODIFICACOMUNE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoComune.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34)



            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNE&FORMULA=SI&EXPORT=PDF&NOME=" & Cserv & Regione & "','StampeRendiconto_2','width=700,height=100,top=300');", True)
        End If
    End Sub
End Class
