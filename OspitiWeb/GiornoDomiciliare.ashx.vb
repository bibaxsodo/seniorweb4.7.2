﻿Imports System.Data.OleDb

Public Class GiornoDomiciliare
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim CODOSP As String = context.Request.QueryString("CODOSP")

        Dim MESE As String = context.Request.QueryString("MESE")
        Dim GIORNO As String = context.Request.QueryString("GIORNO")

        Dim ANNO As String = context.Request.QueryString("ANNO")


        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim DbC As New Cls_Login


        If Trim(context.Session("UTENTE")) = "" Then
            Exit Sub
        End If


        DbC.Utente = context.Session("UTENTE")
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim Appoggio As String
        Appoggio = "<p  style=""font-size: small"">"


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim KCausale As New Cls_CausaliEntrataUscita
        Dim MovimentiTipo As String = ""
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiDomiciliare where CodiceOspite = " & CODOSP & " And CentroServizio = '" & CSERV & "' And Data = ? ")
        cmd.Parameters.AddWithValue("@Data", DateSerial(ANNO, MESE, GIORNO))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim KTD As New Cls_TipoDomiciliare

            KTD.Codice = campodb(myPOSTreader.Item("Tipologia"))
            KTD.Leggi(DbC.Ospiti, KTD.Codice)

            Dim KOP As New Cls_Operatore


            KOP.CodiceMedico = Val(campodb(myPOSTreader.Item("Operatore")))
            KOP.Leggi(DbC.Ospiti)


            Appoggio = Appoggio & Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm") & " - " & Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm") & "<br/>" & Replace(KOP.Nome, "OPERATORE", "OP.") & " - " & Replace(Replace(KTD.Descrizione, "Accreditato", "Accr."), "accreditato", "accr.") & "<br/>"

        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio = Appoggio & "</p>"

        If Appoggio = "<p  style=""font-size: small""></p>" Then
            Dim Kas As New Cls_Movimenti

            Kas.CENTROSERVIZIO = CSERV
            Kas.CodiceOspite = CODOSP
            Kas.UltimaMovimentoPrimaData(DbC.Ospiti, CODOSP, CSERV, DateSerial(ANNO, MESE, GIORNO))
            If Kas.TipoMov = "13" Or Kas.TipoMov = "" Then
                Appoggio = "<p  style=""font-size: small"">NON PRESENTE</p>"
            Else
                Appoggio = "&nbsp;&nbsp;&nbsp;"
            End If
        End If
        context.Response.Write(Appoggio)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function



End Class