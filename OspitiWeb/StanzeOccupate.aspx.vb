﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_StanzeOccupate
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub Estrai()
        Dim Villa As String
        Dim Reparto As String
        Dim Condizione As String

        Villa = DD_Villa.SelectedValue
        Reparto = Dd_Reparto.SelectedValue

        Condizione = ""
        If Trim(Villa) <> "" Then
            Condizione = " VILLA  = '" & Villa & "'"
        End If
        If Trim(Reparto) <> "" Then
            If Trim(Villa) <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " REPARTO = '" & Reparto & "'"
        End If
        If Trim(Txt_Piano.Text) <> "" Then
            If Trim(Reparto) <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PIANO = '" & Txt_Piano.Text & "'"
        End If
        If Trim(Txt_Stanza.Text) <> "" Then
            If Trim(Reparto) <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " Stanza = '" & Txt_Stanza.Text & "'"
        End If

        If Condizione <> "" Then
            Condizione = " Where " & Condizione
        End If

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Struttua", GetType(String))
        MyTable.Columns.Add("Reparto", GetType(String))
        MyTable.Columns.Add("Piano", GetType(String))
        MyTable.Columns.Add("Stanza", GetType(String))
        MyTable.Columns.Add("Letto", GetType(String))
        MyTable.Columns.Add("Tipologia", GetType(String))
        MyTable.Columns.Add("Tipo Arredo", GetType(String))
        MyTable.Columns.Add("Nome", GetType(String))

        Dim cmd As New OleDbCommand()


        cmd.CommandText = "Select * From Stanze " & Condizione & " Order by Villa,Reparto,Piano,Stanza,Letto"

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If LettoOccupato(campodb(myPOSTreader.Item("Villa")), campodb(myPOSTreader.Item("Reparto")), campodb(myPOSTreader.Item("Piano")), campodb(myPOSTreader.Item("Stanza")), campodb(myPOSTreader.Item("Letto"))) = True Then

                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "VIL"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("Villa"))
                XVilla.Leggi(Session("DC_OSPITIACCESSORI"))

                Dim XRep As New Cls_TabelleDescrittiveOspitiAccessori

                XRep.TipoTabella = "REP"
                XRep.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
                XRep.Leggi(Session("DC_OSPITIACCESSORI"))
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = XVilla.Descrizione
                myriga(1) = XRep.Descrizione
                myriga(2) = campodb(myPOSTreader.Item("Piano"))
                myriga(3) = campodb(myPOSTreader.Item("Stanza"))
                myriga(4) = campodb(myPOSTreader.Item("Letto"))

                Dim k As New Cls_TabelleDescrittiveOspitiAccessori

                k.TipoTabella = "TST"
                k.CodiceTabella = campodb(myPOSTreader.Item("Tipologia"))
                k.Leggi(Session("DC_OSPITIACCESSORI"))
                myriga(5) = k.Descrizione

                k.Descrizione = ""
                k.TipoTabella = "TAR"
                k.CodiceTabella = campodb(myPOSTreader.Item("TIPOARREDO"))
                k.Leggi(Session("DC_OSPITIACCESSORI"))

                myriga(6) = k.Descrizione

                myriga(7) = LettoOccupatoDa(campodb(myPOSTreader.Item("Villa")), campodb(myPOSTreader.Item("Reparto")), campodb(myPOSTreader.Item("Piano")), campodb(myPOSTreader.Item("Stanza")), campodb(myPOSTreader.Item("Letto")))
                MyTable.Rows.Add(myriga)
            End If
        Loop
        myPOSTreader.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub

    Private Function LettoOccupato(ByVal Villa As String, ByVal Reparto As String, ByVal Piano As String, ByVal Stanza As String, ByVal Letto As String) As Boolean

        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0

        LettoOccupato = False


        If Piano = "" And Stanza = "" Then
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  CodiceOspite,Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & Piano & "'" & _
                    " AND STANZA = '" & Stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY CodiceOspite,Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Dim SalvaOspite As Long = 0
        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" And SalvaOspite = 0 Then
                LettoOccupato = True
                Exit Do
            End If
            If campodb(myPOSTreader.Item("Tipologia")) = "LI" Then
                SalvaOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))                
            End If
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                SalvaOspite = 0                
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Private Function LettoOccupatoDa(ByVal Villa As String, ByVal Reparto As String, ByVal Piano As String, ByVal Stanza As String, ByVal Letto As String) As String

        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0

        LettoOccupatoDa = ""

        If Letto = "120" Then
            LettoOccupatoDa = ""

        End If
        If Piano = "" And Stanza = "" Then            
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY  CodiceOspite,Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " & _
                    " WHERE VILLA = '" & Villa & "'" & _
                    " AND REPARTO = '" & Reparto & "'" & _
                    " AND PIANO = '" & Piano & "'" & _
                    " AND STANZA = '" & Stanza & "'" & _
                    " AND LETTO = '" & Letto & "'" & _
                    " ORDER BY CodiceOspite,Data DESC,ID Desc"
        End If


        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Dim SalvaOspite As Long = 0
        Do While myPOSTreader.Read
            Dim m As New Cls_MovimentiStanze

            m.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            m.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))


            m.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
            If m.Tipologia = "OC" And m.Letto = Letto Then
                Dim Ospite As New ClsOspite
                Ospite.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

                If LettoOccupatoDa <> "" Then
                    LettoOccupatoDa = LettoOccupatoDa & ","
                End If
                LettoOccupatoDa = LettoOccupatoDa & Ospite.Nome & " " & Ospite.DataNascita
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        Call Estrai()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VerificaRette.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Call Estrai()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim KVilla As New Cls_TabelleDescrittiveOspitiAccessori
        Dim KReparto As New Cls_TabelleDescrittiveOspitiAccessori
        Dim KArredo As New Cls_TabelleDescrittiveOspitiAccessori
        Dim KTipologia As New Cls_TabelleDescrittiveOspitiAccessori

        KVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Villa)

        KReparto.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "REP", Dd_Reparto)


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class
