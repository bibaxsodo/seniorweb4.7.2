﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_ElencoNoteFatture
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim CauCont As New Cls_CausaleContabile

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From NoteFatture  Order By ID"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("CodiceServizio", GetType(String))
        Tabella.Columns.Add("CodiceProvincia", GetType(String))
        Tabella.Columns.Add("CodiceComune", GetType(String))
        Tabella.Columns.Add("CodiceRegione", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Note", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbN(myPOSTreader.Item("ID"))
            myriga(1) = campodb(myPOSTreader.Item("CodiceServizio"))
            myriga(2) = campodb(myPOSTreader.Item("CodiceProvincia"))
            myriga(3) = campodb(myPOSTreader.Item("CodiceComune"))
            myriga(4) = campodb(myPOSTreader.Item("CodiceRegione"))
            myriga(5) = campodb(myPOSTreader.Item("OspitiParentiComuniRegioni"))
            myriga(6) = campodb(myPOSTreader.Item("NoteUp")) & campodb(myPOSTreader.Item("NoteDown"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("GestioneNoteFatture.aspx?ID=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim CauCont As New Cls_CausaleContabile

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From NoteFatture wHERE NoteUp like ? or NoteDown like ?  Order By ID"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Note", Txt_Descrizione.Text)
        cmd.Parameters.AddWithValue("@Note", Txt_Descrizione.Text)



        Tabella.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("CodiceServizio", GetType(String))
        Tabella.Columns.Add("CodiceProvincia", GetType(String))
        Tabella.Columns.Add("CodiceComune", GetType(String))
        Tabella.Columns.Add("CodiceRegione", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Note", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbN(myPOSTreader.Item("ID"))
            myriga(1) = campodb(myPOSTreader.Item("CodiceServizio"))
            myriga(2) = campodb(myPOSTreader.Item("CodiceProvincia"))
            myriga(3) = campodb(myPOSTreader.Item("CodiceComune"))
            myriga(4) = campodb(myPOSTreader.Item("CodiceRegione"))
            myriga(5) = campodb(myPOSTreader.Item("OspitiParentiComuniRegioni"))
            myriga(6) = campodb(myPOSTreader.Item("NoteUp")) & campodb(myPOSTreader.Item("NoteDown"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestioneNoteFatture.aspx")
    End Sub


End Class
