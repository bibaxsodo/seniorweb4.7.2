﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading

Partial Class StampaDocumenti
    Inherits System.Web.UI.Page

    Dim myRequest As System.Net.WebRequest
    Private Delegate Sub DoWorkDelegate()
    Private _work As DoWorkDelegate
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoErrori As String

    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0

        If Txt_Ospite.Text.Trim <> "" Then Exit Function

        MySql = "SELECT MIN(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If Chk_Anticipo.Checked = True Then
            MySql = MySql & " AND (FatturaDiAnticipo <> 'S' OR FatturaDiAnticipo Is Null)"
        End If
        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()



    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue
        
        MassimoDocumento = 0

        If Txt_Ospite.Text.Trim <> "" Then Exit Function


        MySql = "SELECT MAX(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If Chk_Anticipo.Checked = True Then
            MySql = MySql & " AND (FatturaDiAnticipo <> 'S' OR FatturaDiAnticipo Is Null)"
        End If
        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If



        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function









    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        CampoTimer = "ST_" & Session("UTENTE") & "_T"
        CampoProgressBar = "ST_" & Session("UTENTE") & "_PB"
        CampoErrori = "ST_" & Session("UTENTE") & "_ER"


        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione


        If Val(Request.Item("ANNO")) > 0 Then
            Txt_AnnoRif.Text = Val(Request.Item("ANNO"))
            Txt_Anno.Text = Val(Request.Item("AnnoCompetenza"))
            DD_Mese.SelectedValue = Val(Request.Item("MeseDocumento"))
            Txt_DalDocumento.Text = Val(Request.Item("NumeroDal"))
            Txt_AlDocumento.Text = Val(Request.Item("NumeroDal"))
            DD_Registro.SelectedValue = Val(Request.Item("RegistroIVA"))            
        End If

        DD_Report.Items.Clear()
        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))



        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO1"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO2"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO3"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO4"
        End If




        Session("CampoProgressBar") = 0
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0

        EseguiJS()
    End Sub



    Private Sub DoWork(ByVal data As Object)
        Dim k As New Cls_StampaFatture


        If Txt_Ospite.Text.Trim <> "" Then
            Dim cserv As String = ""
            Dim CodiceOspite As Integer = 0
            Dim Vettore(100) As String

            Try
                Vettore = SplitWords(Txt_Ospite.Text)

                cserv = Vettore(0)
                CodiceOspite = Vettore(1)

            Catch ex As Exception

            End Try
            


            If CodiceOspite > 0 Then
                Dim CentroServizio As New Cls_CentroServizio

                CentroServizio.CENTROSERVIZIO = cserv
                CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

                Dim sottoconto As Double

                sottoconto = CodiceOspite * 100

                k.CreaRecordStampa(Val(Txt_Anno.Text), Val(DD_Mese.SelectedValue), Chk_Anticipo.Checked, Val(Txt_DalDocumento.Text), Val(Txt_AlDocumento.Text), Val(Txt_AnnoRif.Text), Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 0, data, 0, 0, CentroServizio.MASTRO, CentroServizio.CONTO, sottoconto)
            End If
        Else
            k.CreaRecordStampa(Val(Txt_Anno.Text), Val(DD_Mese.SelectedValue), Chk_Anticipo.Checked, Val(Txt_DalDocumento.Text), Val(Txt_AlDocumento.Text), Val(Txt_AnnoRif.Text), Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 0, data)
        End If


    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub WorkCompleted(ByVal result As IAsyncResult)
        _work = Nothing
    End Sub


   

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) = 999 Then
            System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
            Lbl_Waiting.Text = ""
            Lbl_Errori.Text = "<p align=left>" & System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) & "</p>"
            Timer1.Enabled = False
            Exit Sub
        End If

        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 100 Then
            Lbl_Waiting.Text = ""
            Timer1.Enabled = False

            Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)
            Session("stampa") = System.Web.HttpRuntime.Cache("stampa" + Session.SessionID)
            If Chk_NoMail.Checked = True Then
                Dim Stampa As New StampeGenerale
                Stampa = Session("stampa")
                For I = Stampa.FatturaTesta.Count - 1 To 0 Step -1
                    If Stampa.FatturaTesta.Item(I).Email <> "" Then
                        If Stampa.FatturaTesta.Item(I).Email.IndexOf("@") > 0 And Stampa.FatturaTesta.Item(I).Email.IndexOf(".") > 0 Then
                            Stampa.FatturaTesta.Item(I).Delete()
                        End If
                    End If
                Next
                Session("stampa") = Stampa
            End If

            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))

            If Session("Download") = "SI" Then
                If DD_Report.SelectedValue = "" Then
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&EXPORT=PDF','Stampe1','width=800,height=600');", True)
                    End If

                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI2") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI2&EXPORT=PDF','Stampe2','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI3") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI3&EXPORT=PDF','Stampe3','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI4") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI4&EXPORT=PDF','Stampe4','width=800,height=600');", True)
                    End If
                Else

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "&EXPORT=PDF','Stampe4','width=800,height=600');", True)
                End If
            Else
                If DD_Report.SelectedValue = "" Then
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&PRINTERKEY=ON','Stampe1','width=800,height=600');", True)
                    End If

                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI2") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI2&PRINTERKEY=ON','Stampe2','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI3") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI3&PRINTERKEY=ON','Stampe3','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI4") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI4&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
                    End If
                Else

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
                End If
            End If


            Exit Sub
        End If
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 0 Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged

        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
        EseguiJS()
    End Sub

    Protected Sub DD_Mese_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mese.SelectedIndexChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
        EseguiJS()
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
        EseguiJS()
    End Sub

    Protected Sub Txt_AnnoRif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_AnnoRif.TextChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
        EseguiJS()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Appoggio As String
        If Val(Txt_DalDocumento.Text) = 0 And Txt_Ospite.Text.Trim = "" Then
            Appoggio = "Numero Documento Dal obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Val(Txt_AlDocumento.Text) = 0 And Txt_Ospite.Text.Trim = "" Then
            Appoggio = "Numero Documento al obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If DD_Registro.SelectedValue = 0 Then
            Appoggio = "Registro Iva obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        Timer1.Enabled = True
        Session("CampoProgressBar") = 0
        Session("RagioneSocialeFattura") = ""
        Session("CampoErrori") = ""
        Session("Download") = "NO"

        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
        System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("Download" + Session.SessionID) = "NO"

        'DoWork(Session)


        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)

        EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")

    End Sub

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        Dim Appoggio As String
        If Val(Txt_DalDocumento.Text) = 0 And Txt_Ospite.Text.Trim = "" Then
            Appoggio = "Numero Documento Dal obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Val(Txt_AlDocumento.Text) = 0 And Txt_Ospite.Text.Trim = "" Then
            Appoggio = "Numero Documento al obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If DD_Registro.SelectedValue = 0 Then
            Appoggio = "Registro Iva obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        Timer1.Enabled = True
        Session("CampoProgressBar") = 0
        Session("RagioneSocialeFattura") = ""
        Session("CampoErrori") = ""
        Session("Download") = "SI"

        'DoWork(Session)


        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
        EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class


