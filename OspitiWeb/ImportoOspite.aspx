﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="ImportoOspite" EnableEventValidation="false" CodeFile="ImportoOspite.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Importo Ospite</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Importo Ospite</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Importo Ospite                                    
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <label class="LabelCampo">Tipo Operazione :</label>
                                            <asp:DropDownList ID="DD_TipoOperazione" runat="server" Width="450px">
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">IVA :</label>
                                            <asp:DropDownList ID="DD_IVA" runat="server" Width="450px"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Anticipata :</label>
                                            <asp:CheckBox ID="Chk_Anticipata" runat="server" Text="" /><br />
                                            <br />

                                            <label class="LabelCampo">Raggruppa Ospite :</label>
                                            <asp:CheckBox ID="Chk_RotturaOspite" runat="server" Text="" /><i>(Crea una sola fattura per ospite presente in più centri servizio )</i><br />
                                            <br />

                                            <label class="LabelCampo">Modalità Pagamento :</label>
                                            <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="450px">
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Periodo :</label>
                                            <asp:RadioButton ID="RB_Mensile" runat="server" GroupName="Periodo" Text="M" Checked="true" />
                                            <asp:RadioButton ID="RB_Bimestrale" runat="server" GroupName="Periodo" Text="B" />
                                            <asp:RadioButton ID="RB_Trimestrale" runat="server" GroupName="Periodo" Text="T" />
                                            <asp:RadioButton ID="RB_FinePeriodoAss" runat="server" GroupName="Periodo" Text="F" /><br />
                                            <br />

                                            <label class="LabelCampo">Compensazione :</label>
                                            <asp:RadioButton ID="RB_SI" runat="server" GroupName="compesazione" Text="SI" Checked="true" AutoPostBack="true" />
                                            <asp:RadioButton ID="RB_NO" runat="server" GroupName="compesazione" Text="NO" AutoPostBack="true" />
                                            <asp:CheckBox ID="Chk_ExtraFuoriFattura" runat="server" Text="Extra non in retta" AutoPostBack="true" />
                                            <asp:RadioButton ID="RB_Dettaglio" runat="server" GroupName="compesazione" Text="Compensa in base al tipo extra" AutoPostBack="true" />

                                            <br />

                                            <br />

                                            <label class="LabelCampo">Tipo Sconto :</label>
                                            <asp:DropDownList ID="dd_TipoSconto" class="chosen-select" runat="server" Width="450px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Sconto :</label>
                                            <asp:TextBox ID="Txt_ImportoSconto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox><br />
                                            <br />



                                            <label class="LabelCampo">Conto Esportazione:</label>
                                            <asp:TextBox ID="Txt_ContoEsportazione" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                            - 
         <asp:TextBox ID="Txt_CodiceCup" MaxLength="15" runat="server" Width="100px"></asp:TextBox><br />
                                            <br />




                                            <label class="LabelCampo">Non emettere O/P:</label>
                                            <asp:CheckBox ID="Chk_NonEmettere" runat="server" Text="" />
                                            <br />
                                            <br />


                                            <label class="LabelCampo">
                                                <asp:Label ID="lbl_GiorniSettimana" runat="server" Text="Presenza Settimana :"></asp:Label>
                                            </label>
                                            <asp:CheckBox ID="Chk_Lunedi" runat="server" Text="L" />
                                            <asp:CheckBox ID="Chk_Martedi" runat="server" Text="M" />
                                            <asp:CheckBox ID="Chk_Mercoledi" runat="server" Text="M" />
                                            <asp:CheckBox ID="Chk_Giovedi" runat="server" Text="G" />
                                            <asp:CheckBox ID="Chk_Venerdi" runat="server" Text="V" />
                                            <asp:CheckBox ID="Chk_Sabato" runat="server" Text="S" />
                                            <asp:CheckBox ID="Chk_Domenica" runat="server" Text="D" />
                                            <br />
                                            <br />
                                            <asp:GridView ID="Grd_ImportoOspite" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <div style="text-align: right">
                                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Data">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Importo">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Tipo">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Tipo" runat="server">
                                                                <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                                <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                                <asp:ListItem Value="N">Mensile Fisso</asp:ListItem>
                                                                <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Importo_2">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto_2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Importo1">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto1" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Importo2">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Importo3">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto3" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Importo4">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto4" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Tipo Operazione">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_TipoOperazione" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DD_TipoOperazioneChange"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                </Columns>

                                                <FooterStyle BackColor="White" ForeColor="#023102" />

                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <br />

                                            <div id="INVISIBILI" style="visibility: collapse; height: 1px;">
                                                <asp:Label ID="Lbl_Metodo" runat="server" Text=""></asp:Label>
                                                <br />
                                                <label class="LabelCampo">Intestatario Conto:</label>
                                                <asp:TextBox ID="Txt_IntestarioCC" Enabled="false" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Intestatario CF Conto:</label>
                                                <asp:TextBox ID="Txt_CodiceFiscaleCC" Enabled="false" runat="server" Width="320px" MaxLength="16"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">IBAN: Int</label>
                                                <asp:TextBox ID="Txt_Int" Enabled="false" MaxLength="2" runat="server" Width="24px"></asp:TextBox>
                                                Num Cont
         <asp:TextBox ID="Txt_NumCont" Enabled="false" MaxLength="2" onkeypress="return soloNumeri(event);" runat="server" Width="32px"></asp:TextBox>
                                                Cin
         <asp:TextBox ID="Txt_Cin" Enabled="false" MaxLength="1" runat="server" Width="40px"></asp:TextBox>
                                                Abi
         <asp:TextBox ID="Txt_Abi" Enabled="false" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="48px"></asp:TextBox>
                                                Cab
         <asp:TextBox ID="Txt_Cab" Enabled="false" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="40px"></asp:TextBox>
                                                C/C Bancario&nbsp;
         <asp:TextBox ID="Txt_Ccbancario" Enabled="false" MaxLength="12" runat="server" Width="328px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Banca Cliente:</label>
                                                <asp:TextBox ID="Txt_BancaCliente" Enabled="false" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />

                                                <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Altri Dati                               
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <b>I Tipi Addebito indicati in questa maschea in fase di importazione addebiti da excel, o in addebiti multipli, vengono ripartiti al comune</b><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito (1) :</label>
                                    <asp:DropDownList ID="DDTipoAddebito1" class="chosen-select" runat="server" Width="300px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito (2) :</label>
                                    <asp:DropDownList ID="DDTipoAddebito2" class="chosen-select" runat="server" Width="300px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito (3) :</label>
                                    <asp:DropDownList ID="DDTipoAddebito3" class="chosen-select" runat="server" Width="300px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito (4) :</label>
                                    <asp:DropDownList ID="DDTipoAddebito4" class="chosen-select" runat="server" Width="300px"></asp:DropDownList><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
