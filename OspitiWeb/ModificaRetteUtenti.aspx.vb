﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Public Class ModificaRetteUtenti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Regione", GetType(String))
        Tabella.Columns.Add("TipoImportoRegione", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Integer))
        Tabella.Columns.Add("Importo", GetType(Double))

        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        Tabella.Rows.Add(myriga)

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataBind()



        Call EseguiJS()
    End Sub

      Private Sub UpDateTable()
        Dim i As Integer

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Regione", GetType(String))
        Tabella.Columns.Add("TipoImportoRegione", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Integer))
        Tabella.Columns.Add("Importo", GetType(Double))
        For i = 0 To Grd_AggiornaRette.Rows.Count - 1

            Dim TxtOspite As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Ospite"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Importo"), TextBox)
            Dim Txt_Quantita As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Quantita"), TextBox)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("DD_TipoRetta"), DropDownList)
            Dim DD_Regione As DropDownList = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("DD_Regione"), DropDownList)
            Dim DD_TipoImportoRetta As DropDownList = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("DD_TipoImportoRetta"), DropDownList)



            Dim myrigaR As System.Data.DataRow = Tabella.NewRow()

            myrigaR(0) = TxtOspite.Text
            myrigaR(1) = DD_Regione.SelectedValue
            myrigaR(2) = DD_TipoImportoRetta.SelectedValue
            myrigaR(3) = DD_TipoRetta.SelectedValue
            myrigaR(4) = Txt_Quantita.Text
            myrigaR(5) = TxtImporto.Text

            Tabella.Rows.Add(myrigaR)
        Next
        ViewState("App_AddebitiMultiplo") = Tabella
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutoCompleteOspiti.ashx?Utente=" & Session("UTENTE") & "&CSERV=" & DD_CentroServizio.SelectedValue & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub
    Private Sub InserisciRiga()
        UpDateTable()
        Tabella = ViewState("App_AddebitiMultiplo")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = ""
        myriga(1) = ""
        myriga(2) = 0
        myriga(3) = 0
  

        Tabella.Rows.Add(myriga)

        ViewState("App_AddebitiMultiplo") = Tabella
        Grd_AggiornaRette.AutoGenerateColumns = False

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()


        Dim TxtOspite As TextBox = DirectCast(Grd_AggiornaRette.Rows(Grd_AggiornaRette.Rows.Count - 1).FindControl("Txt_Ospite"), TextBox)

        TxtOspite.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtOspite.ClientID + "').focus(); }); }, 500);", True)        
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Btn_Esegui_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esegui.Click
        CaricaTabella()
    End Sub


    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Regione", GetType(String))
        Tabella.Columns.Add("TipoImportoRegione", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(Integer))
        Tabella.Columns.Add("Importo", GetType(Double))

        Dim cmd1 As New OleDbCommand()


        cmd1.CommandText = "Select * From RETTEREGIONE Where CENTROSERVIZIO = ? And ANNO = ? AND MESE = ?  Order by (Select Top 1 Nome From AnagraficaComune Where AnagraficaComune.codiceospite =  RETTEREGIONE.codiceospite and codiceparente = 0)"
        cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", DD_CentroServizio.SelectedValue)
        cmd1.Parameters.AddWithValue("@ANNO", Val(Txt_Anno.Text))
        cmd1.Parameters.AddWithValue("@MESE", Val(Dd_Mese.SelectedValue))


        cmd1.Connection = cn
        Dim RettaRegione As OleDbDataReader = cmd1.ExecuteReader()
        Do While RettaRegione.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = campodbn(RettaRegione.Item("CodiceOspite"))
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            myriga(0) = Ospite.CodiceOspite & " " & Ospite.Nome

            myriga(1) = campodb(RettaRegione.Item("REGIONE"))
            myriga(2) = campodb(RettaRegione.Item("ELEMENTO"))
            myriga(3) = campodb(RettaRegione.Item("CODICEEXTRA"))
            If campodb(RettaRegione.Item("GIORNI")) = "" Then
                myriga(4) = 0
            Else
                myriga(4) = campodbn(RettaRegione.Item("GIORNI"))
            End If

            If campodb(RettaRegione.Item("IMPORTO")) = "" Then
                myriga(5) = 0
            Else
                myriga(5) = Math.Round(campodbn(RettaRegione.Item("IMPORTO")), 2)
            End If

            Tabella.Rows.Add(myriga)
        Loop
        cn.Close()


        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.Font.Size = 10
        Grd_AggiornaRette.DataBind()

        Session("CALCOLORETTE") = Tabella

    End Sub

    Protected Sub Grd_AggiornaRette_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_AggiornaRette.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim Txt_Ospite As TextBox = DirectCast(e.Row.FindControl("Txt_Ospite"), TextBox)



            Dim Txt_Importo As TextBox = DirectCast(e.Row.FindControl("Txt_Importo"), TextBox)

            Dim TxtQuantita As TextBox = DirectCast(e.Row.FindControl("Txt_Quantita"), TextBox)

            
            Dim DD_TipoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoRetta"), DropDownList)
            Dim DD_Regione As DropDownList = DirectCast(e.Row.FindControl("DD_Regione"), DropDownList)
            Dim DD_TipoImportoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoImportoRetta"), DropDownList)



            Dim Usl As New ClsUSL

            Usl.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)

            Dim Regione As New Cls_TabellaTipoImportoRegione


            Regione.UpDateDropBox(Session("DC_OSPITE"), DD_TipoImportoRetta)


            Txt_Ospite.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString

            DD_Regione.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(1).ToString
            DD_TipoImportoRetta.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString

            DD_TipoRetta.SelectedValue = campodb(Tabella.Rows(e.Row.RowIndex).Item(2).ToString)

            If campodb(Tabella.Rows(e.Row.RowIndex).Item(4).ToString) = "" Then
                TxtQuantita.Text = 0
            Else
                TxtQuantita.Text = campodbn(Tabella.Rows(e.Row.RowIndex).Item(4).ToString)
            End If
            If campodb(Tabella.Rows(e.Row.RowIndex).Item(5).ToString) = "" Then
                Txt_Importo.Text = 0
            Else
                Txt_Importo.Text = campodbn(Tabella.Rows(e.Row.RowIndex).Item(5).ToString)
            End If

            Call EseguiJS()

        End If
    End Sub

    Private Sub ModificaRetta()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmdDelete As New OleDbCommand()


        cmdDelete.CommandText = "Delete From RETTEREGIONE Where CENTROSERVIZIO = ? And ANNO = ? AND MESE = ?  "
        cmdDelete.Parameters.AddWithValue("@CENTROSERVIZIO", DD_CentroServizio.SelectedValue)
        cmdDelete.Parameters.AddWithValue("@ANNO", Val(Txt_Anno.Text))
        cmdDelete.Parameters.AddWithValue("@MESE", Val(Dd_Mese.SelectedValue))
        cmdDelete.Transaction = Transan
        cmdDelete.Connection = cn
        cmdDelete.ExecuteNonQuery()

        Dim I As Integer


        For i = 0 To Grd_AggiornaRette.Rows.Count - 1

            Dim TxtOspite As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Ospite"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Importo"), TextBox)
            Dim Txt_Quantita As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Quantita"), TextBox)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("DD_TipoRetta"), DropDownList)
            Dim DD_Regione As DropDownList = DirectCast(Grd_AggiornaRette.Rows(I).FindControl("DD_Regione"), DropDownList)
            Dim DD_TipoImportoRetta As DropDownList = DirectCast(Grd_AggiornaRette.Rows(I).FindControl("DD_TipoImportoRetta"), DropDownList)


            If TxtOspite.Text.Trim <> "" Then
                Dim Ospite As String = TxtOspite.Text
                Dim CodiceOSpite As Integer
                Dim Vettore(100) As String

                Vettore = SplitWords(Ospite)
                CodiceOSpite = Val(Vettore(0))


                If CodiceOSpite > 0 Then
                    Dim cmdInsert As New OleDbCommand()


                    cmdInsert.CommandText = "INSERT INTO RETTEREGIONE (CENTROSERVIZIO,CODICEOSPITE,MESE,ANNO,REGIONE,STATOCONTABILE,ELEMENTO,GIORNI,CODICEEXTRA,IMPORTO,DESCRIZIONE,MANUALE) VALUES " & _
                                                                    " (             ?,           ?,   ?,   ?,      ?,             ?,       ?,     ?,          ?,      ?,          ?,      ?) "
                    cmdInsert.Parameters.AddWithValue("@CENTROSERVIZIO", DD_CentroServizio.SelectedValue)
                    cmdInsert.Parameters.AddWithValue("@CODICEOSPITE", CodiceOSpite)
                    cmdInsert.Parameters.AddWithValue("@MESE", Dd_Mese.SelectedValue)
                    cmdInsert.Parameters.AddWithValue("@ANNO", Val(Txt_Anno.Text))
                    cmdInsert.Parameters.AddWithValue("@REGIONE", DD_Regione.SelectedValue)
                    cmdInsert.Parameters.AddWithValue("@STATOCONTABILE", "N")
                    cmdInsert.Parameters.AddWithValue("@ELEMENTO", DD_TipoRetta.SelectedValue)
                    cmdInsert.Parameters.AddWithValue("@GIORNI", Val(Txt_Quantita.Text))
                    cmdInsert.Parameters.AddWithValue("@CODICEEXTRA", DD_TipoImportoRetta.SelectedValue)
                    cmdInsert.Parameters.AddWithValue("@IMPORTO", CDbl(TxtImporto.Text))
                    cmdInsert.Parameters.AddWithValue("@DESCRIZIONE", "")
                    cmdInsert.Parameters.AddWithValue("@MANUALE", "M")

                    cmdInsert.Transaction = Transan
                    cmdInsert.Connection = cn
                    cmdInsert.ExecuteNonQuery()
                End If
            End If
        Next

        Transan.Commit()

        cn.Close()

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        ModificaRetta()
    End Sub

    Protected Sub Grd_AggiornaRette_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_AggiornaRette.RowDeleted

    End Sub

    Protected Sub Grd_AggiornaRette_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_AggiornaRette.RowDeleting
        UpDateTable()
        Tabella = Session("CALCOLORETTE")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        myriga(3) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    myriga(3) = 0
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        Session("CALCOLORETTE") = Tabella

        Grd_AggiornaRette.AutoGenerateColumns = False

        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()

        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class