﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class OspitiWeb_ImportMovimenti
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub OspitiWeb_ImportMovimenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If

            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim SalvaDb As New OleDbCommand

            Chk_RicaricaDati.Checked = True
            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Periodo = ? And Tipo = 'M'"
            SalvaDb.Connection = cn
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim appoggio As Date = Nothing
                Try
                    appoggio = campodb(VerificaDB.Item("UltimaModifica"))
                Catch ex As Exception

                End Try
                If Format(appoggio, "yyyyMMdd") = Format(Now, "yyyyMMdd") Then
                    Chk_RicaricaDati.Checked = False
                End If
            End If
            VerificaDB.Close()            
            cn.Close()

            Chk_RicaricaDati.Checked = True
            Chk_RicaricaDati.Visible = False
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String

        If Chk_RicaricaDati.Checked = True Then
            Token = LoginPersonam(Context)

            If Token = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                Exit Sub
            End If
        End If


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim OLDOPSITE As Integer = 0
        Dim CENTROSERVIZIO As String = ""
        Dim TIPOMOVIMENTO As String = ""
        Dim OLDDATA As String = ""

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("Cognome", GetType(String))
            Tabella.Columns.Add("Nome", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("Centroservizio", GetType(String))
            Tabella.Columns.Add("DataMovimento", GetType(String))
            Tabella.Columns.Add("Tipo", GetType(String))
            Tabella.Columns.Add("TipoInt", GetType(String))
            Tabella.Columns.Add("Seganlazione", GetType(String))
            Tabella.Columns.Add("IdEpersonam", GetType(Long))
            Tabella.Columns.Add("CodiceOspite", GetType(Long))
            Tabella.Columns.Add("Letto", GetType(String))
            Tabella.Columns.Add("Doppio", GetType(String))
            Tabella.Columns.Add("Causale", GetType(String))

        Catch ex As Exception

        End Try


        Call BusinessUnit(Token, Context)


        Tabella.Select()
        Dim DataM As String = ""
        Dim LCODICEFISCALE As String = ""
        Dim LTipo As String = ""
        Dim Entrato As Boolean = False

        For Riga = Tabella.Rows.Count - 1 To 0 Step -1

            If Tabella.Rows(Riga).Item(2).ToString = "FRRGTN36H46D142C" Then
                Riga = Riga
            End If
            Entrato = False
            If Tabella.Rows(Riga).Item(2).ToString = LCODICEFISCALE And Tabella.Rows(Riga).Item(4).ToString = DataM Then
                If LTipo = "Uscita Temporanea" Or LTipo = "Entrate" Then
                    If Tabella.Rows(Riga).Item(5).ToString = "Uscita Temporanea" Or Tabella.Rows(Riga).Item(5).ToString = "Entrate" Then
                        Tabella.Rows(Riga).Item(5) = "RIMUOVERE"
                        Tabella.Rows(Riga + 1).Item(5) = "RIMUOVERE"
                        Entrato = True
                    End If
                End If
            End If
            If Entrato = False Then
                LCODICEFISCALE = Tabella.Rows(Riga).Item(2).ToString()
                DataM = Tabella.Rows(Riga).Item(4).ToString
                LTipo = Tabella.Rows(Riga).Item(5).ToString
            End If
        Next

        For Riga = Tabella.Rows.Count - 1 To 0 Step -1
            If Tabella.Rows(Riga).Item(5) = "RIMUOVERE" Then
                Tabella.Rows.RemoveAt(Riga)
            End If
        Next
        If Cmb_CServ.SelectedValue <> "" Then
            For Riga = Tabella.Rows.Count - 1 To 0 Step -1
                If Tabella.Rows(Riga).Item(3).ToString <> Cmb_CServ.SelectedValue Then
                    Tabella.Rows.RemoveAt(Riga)
                End If
            Next
        End If

        Chk_RicaricaDati.Checked = True


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Sub


    Private Sub ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String)

        Dim OLDOPSITE As Integer = 0
        Dim CENTROSERVIZIO As String = ""
        Dim TIPOMOVIMENTO As String = ""
        Dim OLDDATA As String = ""


        Dim Data() As Byte

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim rawresp As String

        If Chk_RicaricaDati.Checked = True Then
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessID & "/guests/movements/from/" & DataFatt)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            rawresp = reader.ReadToEnd()




            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = 0 And Periodo = ? And Tipo = 'M'"
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            SalvaDb.Connection = cn

            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'M'"
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            Else
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo) values (?,?,?,?,?,'M') "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            End If
            VerificaDB.Close()
        Else
            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = 0 And Periodo = ? And Tipo ='M'"
            SalvaDb.Connection = cn
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                rawresp = campodb(VerificaDB.Item("Dati"))
            End If
            VerificaDB.Close()
        End If


        Dim LastMov As String

        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim SalvaPrecedenteTipo As String = ""
        Dim SalvaPrecedenteData As String = ""


        For Each jTok As JToken In jResults
            rawresp = rawresp & jTok.Item("guests_movements").ToString()
            For Each jTok1 As JToken In jTok.Item("guests_movements").Children
                Dim CodiceFiscale As String
                Dim ward_id As Integer



                CodiceFiscale = jTok1.Item("cf").ToString()
                ward_id = jTok1.Item("ward_id").ToString()

                Dim Ospite As New ClsOspite

                If CodiceFiscale = "PSQWND24B60E138K" Then
                    CodiceFiscale = "PSQWND24B60E138K"
                End If

                SalvaPrecedenteTipo = ""

                Ospite.CODICEFISCALE = CodiceFiscale
                Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), CodiceFiscale)
                If Ospite.CodiceOspite > 0 Then
                    LastMov = ""
                    For Each jTok2 As JToken In jTok1.Item("movements").Children

                        Dim IdEpersonam As String

                        IdEpersonam = jTok2.Item("id").ToString

                        Dim DescrizioneMovimento As String = jTok2.Item("description").ToString.ToUpper
                        'CHANGE_TUTOR_BU_SOAS
                        'And DescrizioneMovimento <> "NEW_UNIT" 

                        Dim RagWardID As New Cls_Epersonam

                        Dim MovUltAccogl As New Cls_Movimenti

                        MovUltAccogl.CodiceOspite = Ospite.CodiceOspite
                        MovUltAccogl.UltimaDataAccoglimento(Session("DC_OSPITE"))

                        Dim CservUltAccogl As New Cls_CentroServizio


                        CservUltAccogl.CENTROSERVIZIO = MovUltAccogl.CENTROSERVIZIO
                        CservUltAccogl.Leggi(Session("DC_OSPITE"), CservUltAccogl.CENTROSERVIZIO)


                        '

                        If DescrizioneMovimento <> "CHANGE_TUTOR_BU_SOAS" And DescrizioneMovimento <> "CHANGE_TUTOR_BU" And DescrizioneMovimento <> "CHANGE_DOCTOR_BU" And Not (DescrizioneMovimento = "NEW_UNIT" And (CservUltAccogl.EPersonam = RagWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")))) Then

                            If LastMov = "11" And jTok2.Item("type").ToString = "8" Then

                                Dim InAltroCSVER As String = ""
                                Try
                                    InAltroCSVER = jTok2.Item("where_sad").ToString
                                Catch ex As Exception

                                End Try

                                If InAltroCSVER = "" Then
                                    Try
                                        InAltroCSVER = jTok2.Item("where_cd").ToString
                                    Catch ex As Exception

                                    End Try
                                End If

                                If InAltroCSVER <> "" Then
                                    Dim Indice As Integer

                                    For Indice = 0 To Tabella.Rows.Count - 1

                                        If Tabella.Rows(Indice).Item(2) = CodiceFiscale And Tabella.Rows(Indice).Item(5) = "Uscita Definitiva" Then
                                            '  Tabella.Rows(Indice).Item(7) = "ERROREPRE"
                                        End If
                                    Next
                                End If

                                LastMov = jTok2.Item("type").ToString

                            Else

                                LastMov = jTok2.Item("type").ToString

                                Dim cmd As New OleDbCommand()

                                If jTok2.Item("type").ToString = "11" Then
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where (CodiceOspite = " & Ospite.CodiceOspite & " And Data = ? And [TipoMovimento] = 'A') or  IdEpersonam = " & IdEpersonam)
                                    cmd.Parameters.AddWithValue("@Data", Mid(jTok2.Item("date").ToString, 1, 10))
                                Else
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where IdEpersonam = " & IdEpersonam)
                                End If

                                cmd.Connection = cn

                                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                If Not myPOSTreader.Read Then
                                    Dim Tipo As String = ""
                                    Dim DataMov As String = ""
                                    Dim Letto As String = ""

                                    If jTok2.Item("type").ToString = "11" Then
                                        Tipo = "05"
                                    End If
                                    If jTok2.Item("type").ToString = "9" Then
                                        Tipo = "03"
                                    End If
                                    If jTok2.Item("type").ToString = "8" Then
                                        Tipo = "04"
                                    End If

                                    Dim KWardID As New Cls_Epersonam
                                    If Not IsNothing(jTok2.Item("bed")) Then
                                        Letto = jTok2.Item("bed").ToString
                                    Else
                                        Letto = ""
                                    End If

                                    If jTok2.Item("description").ToString = "NEW_UNIT" And jTok2.Item("type").ToString = "8" Then
                                        Tipo = "05"
                                    End If

                                    If jTok2.Item("type").ToString = "8" And (jTok2.Item("description").ToString.ToUpper = "CHANGE_BED" Or jTok2.Item("description").ToString.ToUpper = "NEW_BED") Then
                                        Tipo = "99"
                                        If Not IsNothing(jTok2.Item("bed")) Then
                                            Letto = jTok2.Item("bed").ToString
                                        Else
                                            Letto = ""
                                        End If
                                    End If

                                    If jTok2.Item("type").ToString = "12" Then
                                        Tipo = "13"
                                    End If

                                    DataMov = jTok2.Item("date").ToString


                                    ward_id = KWardID.RendiWardid(ward_id, Session("DC_OSPITE"))


                                    Dim Progressivo As Long
                                    Dim MySql As String
                                    Dim CS As New Cls_CentroServizio
                                    CS.DESCRIZIONE = ""
                                    CS.EPersonam = ward_id

                                    Dim AppoggioBool As Boolean
                                    AppoggioBool = True
                                    Dim Convenzionato As String

                                    Try
                                        Convenzionato = jTok2.Item("conv").ToString
                                    Catch ex As Exception

                                    End Try


                                    If Convenzionato.ToUpper = "TRUE" Then
                                        AppoggioBool = True
                                    End If
                                    If Convenzionato.ToUpper = "FALSE" Then
                                        AppoggioBool = False
                                    End If

                                    CS.LeggiEpersonam(Context.Session("DC_OSPITE"), CS.EPersonam, AppoggioBool)
                                    Dim Vercentroservizio As New Cls_Movimenti


                                    Vercentroservizio.CENTROSERVIZIO = CS.CENTROSERVIZIO
                                    Vercentroservizio.CodiceOspite = Ospite.CodiceOspite

                                    'And ((OLDOPSITE <> Ospite.CodiceOspite And CENTROSERVIZIO <> CS.CENTROSERVIZIO And OLDDATA <> Mid(DataMov, 1, 10) And TIPOMOVIMENTO <> Tipo) Or Tipo = "05") 
                                    If (Vercentroservizio.CServizioUsato(Context.Session("DC_OSPITE")) Or Tipo = "05") And (OLDOPSITE <> Ospite.CodiceOspite Or CENTROSERVIZIO <> CS.CENTROSERVIZIO Or OLDDATA <> Mid(DataMov, 1, 10) Or TIPOMOVIMENTO <> Tipo) Then

                                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                                        myriga(0) = Ospite.CognomeOspite

                                        If Ospite.NonInUso = "S" Then
                                            myriga(1) = Ospite.NomeOspite & " (OSPITE NON IN USO ATTIVARLO)"
                                        Else
                                            myriga(1) = Ospite.NomeOspite
                                        End If



                                        myriga(2) = Ospite.CODICEFISCALE

                                        If Ospite.CODICEFISCALE = "GAICLR24S52L168U" Then
                                            Ospite.CODICEFISCALE = "GAICLR24S52L168U"
                                        End If

                                        myriga(3) = CS.CENTROSERVIZIO
                                        myriga(4) = Mid(DataMov, 1, 10)

                                        If Tipo = "05" Then
                                            myriga(5) = "Accoglimento"
                                        End If
                                        If Tipo = "13" Then
                                            myriga(5) = "Uscita Definitiva"
                                        End If
                                        If Tipo = "03" Then
                                            myriga(5) = "Uscita Temporanea"
                                        End If
                                        If Tipo = "04" Then
                                            myriga(5) = "Entrate"
                                        End If
                                        If Tipo = "99" Then
                                            myriga(5) = "Cambio Letto"
                                        End If


                                        myriga(6) = Tipo
                                        myriga(7) = ""
                                        myriga(8) = IdEpersonam
                                        myriga(9) = Ospite.CodiceOspite
                                        myriga(10) = Letto
                                        myriga(11) = ""
                                        If SalvaPrecedenteTipo = Tipo And _
                                            SalvaPrecedenteData = Mid(DataMov, 1, 10) Then
                                            myriga(11) = "S"
                                        End If

                                        If Tipo = "03" Then
                                            Try
                                                If jTok2.Item("outtype").ToString.ToUpper.IndexOf("OSPED") >= 0 Then
                                                    myriga(12) = "02"
                                                Else

                                                    myriga(12) = "01"
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If
                                        If Tipo = "13" Then
                                            Try
                                                If jTok2.Item("outtype").ToString.ToUpper.IndexOf("DECEDU") >= 0 Or jTok2.Item("outtype").ToString.ToUpper.IndexOf("DECESSO") >= 0 Then
                                                    myriga(12) = "98"
                                                Else
                                                    myriga(12) = "99"
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If


                                        'Virginia Borgheri
                                        If ward_id = 1561 Then
                                            Try
                                                If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                    myriga(12) = "02"
                                                End If

                                                If jTok2.Item("outtype").ToString = "Motivi famigliari/ferie" Then
                                                    myriga(12) = "01"
                                                End If
                                            Catch ex As Exception

                                            End Try
                                            If Tipo = "13" Then
                                                Try
                                                    If jTok2.Item("outtype").ToString = "Deceduto" Then
                                                        myriga(12) = "98"
                                                    Else
                                                        myriga(12) = "99"
                                                    End If
                                                Catch ex As Exception

                                                End Try
                                            End If
                                        End If

                                        If ward_id = 1498 Then
                                            ' CONVITTO TORINO
                                            If Tipo = "03" Then
                                                Try
                                                    If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                        myriga(12) = "01"
                                                    Else
                                                        myriga(12) = "02"
                                                    End If
                                                Catch ex As Exception

                                                End Try
                                            End If
                                            If Tipo = "13" Then
                                                Try
                                                    If jTok2.Item("outtype").ToString = "Deceduto" Then
                                                        myriga(12) = "99"
                                                    Else
                                                        myriga(12) = "98"
                                                    End If
                                                Catch ex As Exception

                                                End Try
                                            End If
                                        End If

                                        If ward_id = 1155 Or ward_id = 1157 Or ward_id = 1160 Or ward_id = 1162 Or ward_id = 1166 Or _
                                           ward_id = 1167 Or ward_id = 1155 Or ward_id = 1160 Or ward_id = 1402 Or ward_id = 1403 Or _
                                           ward_id = 1404 Or ward_id = 1405 Or ward_id = 1406 Or ward_id = 1407 Or ward_id = 1409 Then
                                            Try
                                                If Tipo = "13" Then
                                                    If jTok2.Item("outtype").ToString = "Deceduto" Or jTok2.Item("outtype").ToString = "Decesso" Then
                                                        myriga(12) = "99"
                                                    Else
                                                        myriga(12) = "98"
                                                    End If
                                                    If jTok2.Item("outtype").ToString.ToUpper.IndexOf("DECESSO") >= 0 And jTok2.Item("outtype").ToString.ToUpper.IndexOf("OSPEDALE") >= 0 Then
                                                        myriga(12) = "97"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    'If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                    myriga(12) = "01"
                                                    'Else
                                                    '   myriga(12) = "02"
                                                    'End If
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If

                                        ' USCITA DI SICUREZZA
                                        If ward_id = 2502 Or ward_id = 2503 Or ward_id = 2504 Or ward_id = 2506 Or ward_id = 2507 Or ward_id = 2509 Or ward_id = 2502 Then
                                            If Tipo = "03" Then
                                                If jTok2.Item("outtype").ToString.ToUpper.IndexOf("SOCCORSO") > 0 Then
                                                    myriga(12) = "02"
                                                End If
                                                If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                    myriga(12) = "02"
                                                End If
                                                If jTok2.Item("outtype").ToString = "Motivi famigliari/ferie" Then
                                                    myriga(12) = "01"
                                                End If
                                                If jTok2.Item("outtype").ToString = "Uscita programmata" Then
                                                    myriga(12) = "01"
                                                End If
                                            End If
                                            If Tipo = "13" Then
                                                myriga(12) = "99"
                                                If jTok2.Item("outtype").ToString = "decesso" Then
                                                    myriga(12) = "98"
                                                End If
                                            End If
                                        End If

                                        ' ASP CHARITAS
                                        If ward_id = 1071 Or ward_id = 1168 Or ward_id = 1498 Then
                                            If Tipo = "03" Then
                                                If jTok2.Item("outtype").ToString = "A casa con famigliari" Then
                                                    myriga(12) = "01"
                                                End If
                                            End If
                                        End If

                                        'BINOTTO
                                        If ward_id = 2097 Or ward_id = 2104 Then
                                            Try
                                                If Tipo = "13" Then
                                                    If jTok2.Item("outtype").ToString = "Dimissione altra struttura" Then
                                                        myriga(12) = "99"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Deceduto (100%)" Then
                                                        myriga(12) = "98"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Dimissione definitiva ospedale" Then
                                                        myriga(12) = "97"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Dimissioni da centro diurno" Then
                                                        myriga(12) = "99"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    If jTok2.Item("outtype").ToString = "Ospedale (1^ giorno)" Then
                                                        myriga(12) = "02"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Uscito (100%)" Then
                                                        myriga(12) = "01"
                                                    End If
                                                End If

                                            Catch ex As Exception

                                            End Try
                                        End If

                                        If ward_id = 2030 Then
                                            Try
                                                If Tipo = "13" Then
                                                    myriga(12) = "99"
                                                    If jTok2.Item("outtype").ToString = "Dimissione altra struttura" Then
                                                        myriga(12) = "97"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Deceduto (100%)" Then
                                                        myriga(12) = "98"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    myriga(12) = "02"
                                                    If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                        myriga(12) = "02"
                                                    End If

                                                    If jTok2.Item("outtype").ToString = "Motivi famigliari/ferie" Then
                                                        myriga(12) = "01"
                                                    End If
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If

                                        'betulle
                                        If ward_id = 2099 Then

                                        End If


                                        'PREALPINA
                                        If ward_id = 2096 Then
                                            Try
                                                If Tipo = "13" Then
                                                    If jTok2.Item("outtype").ToString = "Dimissione altra struttura" Then
                                                        myriga(12) = "99"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Deceduto (100%)" Then
                                                        myriga(12) = "98"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Dimissione definitiva ospedale" Then
                                                        myriga(12) = "97"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Dimissioni da centro diurno" Then
                                                        myriga(12) = "99"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    If jTok2.Item("outtype").ToString = "Ospedale (1^ giorno)" Then
                                                        myriga(12) = "02"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Uscito (100%)" Then
                                                        myriga(12) = "01"
                                                    End If
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If

                                        'san giuseppe /san francesco
                                        If ward_id = 1442 Or ward_id = 1443 Then
                                            Try
                                                If Tipo = "13" Then
                                                    myriga(12) = "99"
                                                    If jTok2.Item("outtype").ToString = "Dimissione altra struttura" Then
                                                        myriga(12) = "97"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Deceduto (100%)" Then
                                                        myriga(12) = "98"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    myriga(12) = "02"
                                                    If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                        myriga(12) = "02"
                                                    End If

                                                    If jTok2.Item("outtype").ToString = "Motivi famigliari/ferie" Then
                                                        myriga(12) = "01"
                                                    End If
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If


                                        'COOP CREA
                                        If ward_id = 2035 Or ward_id = 2036 Or ward_id = 2037 Or ward_id = 2038 Or ward_id = 2030 Or ward_id = 2042 Then
                                            Try
                                                If Tipo = "13" Then
                                                    myriga(12) = "99"
                                                    If jTok2.Item("outtype").ToString = "Dimissione altra struttura" Then
                                                        myriga(12) = "97"
                                                    End If
                                                    If jTok2.Item("outtype").ToString = "Deceduto (100%)" Then
                                                        myriga(12) = "98"
                                                    End If
                                                End If
                                                If Tipo = "03" Then
                                                    myriga(12) = "02"
                                                    If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                        myriga(12) = "02"
                                                    End If

                                                    If jTok2.Item("outtype").ToString = "Motivi famigliari/ferie" Then
                                                        myriga(12) = "01"
                                                    End If
                                                    If ward_id = 2038 Then
                                                        myriga(12) = "AC"
                                                    End If
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If

                                        'SORESINA 
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2445 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2444 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype.IndexOf("(1)") >= 0 Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype.IndexOf("(2)") >= 0 Then
                                                myriga(12) = "01"
                                            End If

                                            If outtype = "(1) Dimissione ordinaria al domicilio del paziente" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "(4) Deceduto in Struttura" Then
                                                myriga(12) = "98"
                                            End If
                                            If outtype = "(2) Dimissione presso istituto di ricovero e cura, pubblico o privato per acuti o istituto di riabilitazione" Then
                                                myriga(12) = "99"
                                            End If
                                            '

                                            If outtype = "(5) Dimissione per trasferimento ad altra residenza sanitaria assistenziale (RSA)" Then
                                                myriga(12) = "99"
                                            End If
                                        End If
                                        'NAVIGLIO 
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2537 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2538 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype.IndexOf("(1)") >= 0 Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype.IndexOf("(2)") >= 0 Then
                                                myriga(12) = "01"
                                            End If


                                            If outtype = "(1) Dimissione ordinaria al domicilio del paziente" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "(4) Deceduto in Struttura" Then
                                                myriga(12) = "98"
                                            End If
                                            If outtype = "(2) Dimissione presso istituto di ricovero e cura, pubblico o privato per acuti o istituto di riabilitazione" Then
                                                myriga(12) = "99"
                                            End If
                                            '

                                            If outtype = "(5) Dimissione per trasferimento ad altra residenza sanitaria assistenziale (RSA)" Then
                                                myriga(12) = "99"
                                            End If
                                        End If
                                        ' CARPANEDA
                                        '2560
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2560 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2563 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2561 Or _
                                            KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2586 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2582 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2581 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2601 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype.IndexOf("(1)") >= 0 Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype.IndexOf("(2)") >= 0 Then
                                                myriga(12) = "01"
                                            End If


                                            If outtype = "(1) Dimissione ordinaria al domicilio del paziente" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "(4) Deceduto in Struttura" Then
                                                myriga(12) = "98"
                                            End If
                                            If outtype = "(2) Dimissione presso istituto di ricovero e cura, pubblico o privato per acuti o istituto di riabilitazione" Then
                                                myriga(12) = "99"
                                            End If
                                            '

                                            If outtype = "(5) Dimissione per trasferimento ad altra residenza sanitaria assistenziale (RSA)" Then
                                                myriga(12) = "99"
                                            End If
                                        End If

                                        'BAGGIO
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2552 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype.IndexOf("(2)") >= 0 Then
                                                myriga(12) = "99"
                                            End If


                                            If Tipo = "03" Then
                                                If outtype.IndexOf("(2)") >= 0 Then
                                                    myriga(12) = "01" 'USCITA VACANZA
                                                End If
                                                If outtype.IndexOf("(1)") >= 0 Then
                                                    myriga(12) = "02" 'USCITA PER OSPEDALE
                                                End If
                                                If outtype = "(2) Dimissione presso istituto di ricovero e cura, pubblico o privato per acuti o istituto di riabilitazione" Then
                                                    myriga(12) = "02" 'USCITA PER OSPEDALE
                                                End If
                                            End If


                                            If outtype = "(1) Dimissione ordinaria al domicilio del paziente" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "(4) Deceduto in Struttura" Then
                                                myriga(12) = "98"
                                            End If

                                            '

                                            If outtype = "(5) Dimissione per trasferimento ad altra residenza sanitaria assistenziale (RSA)" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "(6) Dimissione per trasferimento ad altra tipologia di assistenza (regime) o ad altra tipologia ospite all’interno dello stesso istituto" Then
                                                myriga(12) = "97"
                                            End If
                                        End If
                                        'DOMIZIANA
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2389 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype.IndexOf("Ricovero ospedaliero") >= 0 Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype.IndexOf("Motivi famigliari/ferie") >= 0 Then
                                                myriga(12) = "01"
                                            End If

                                            If outtype = "Cambio tipologia di accoglienza" Then
                                                myriga(12) = "97"
                                            End If
                                            If outtype.IndexOf("Decesso") >= 0 Then
                                                myriga(12) = "98"
                                            End If
                                            If outtype = "Dimisione ad altra struttura residenziale o semi-residenziale" Then
                                                myriga(12) = "99"
                                            End If

                                            If outtype = "Dimissione a domicilio" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "Dimissione a struttura sanitaria (senza mantenimento del posto)" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "Trasferimento/Chiusura amministrativa" Then
                                                myriga(12) = "99"
                                            End If
                                        End If


                                        'LIETO SOGGIORNO 
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2480 Or KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 2482 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try

                                            If outtype = "Ricovero ospedaliero" Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype = "Decesso" Then
                                                myriga(12) = "98"
                                            End If
                                        End If
                                        'PENSIONATO ILENIA
                                        If KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE")) = 1100 Then
                                            Dim outtype As String = ""
                                            Try
                                                outtype = jTok2.Item("outtype").ToString
                                            Catch ex As Exception

                                            End Try


                                            If outtype = "Ritorno al Domicilio" Then
                                                myriga(12) = "99"
                                            End If
                                            If outtype = "Altra Struttura" Then
                                                myriga(12) = "97"
                                            End If
                                            If outtype = "Ricovero ospedaliero" Then
                                                myriga(12) = "02"
                                            End If

                                            If outtype = "Decesso" Or outtype = "Deceduto" Then
                                                myriga(12) = "98"
                                            End If
                                        End If

                                        OLDOPSITE = Ospite.CodiceOspite
                                        CENTROSERVIZIO = CS.CENTROSERVIZIO

                                        myriga(7) = ""
                                        If CS.TIPOCENTROSERVIZIO = "D" And myriga(5) = "Entrate" Then
                                            myriga(7) = "NONIMP"
                                        End If
                                        If CS.TIPOCENTROSERVIZIO = "D" And myriga(5) = "Uscita Temporanea" Then
                                            myriga(7) = "NONIMP"
                                        End If

                                        If CS.TIPOCENTROSERVIZIO = "D" And Tipo = "13" Then
                                            'myriga(12) = ""
                                        End If

                                        OLDDATA = Mid(DataMov, 1, 10)
                                        TIPOMOVIMENTO = Tipo



                                        Tabella.Rows.Add(myriga)
                                        SalvaPrecedenteTipo = Tipo
                                        SalvaPrecedenteData = Mid(DataMov, 1, 10)

                                    Else
                                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                                        myriga(0) = Ospite.CognomeOspite
                                        myriga(1) = Ospite.NomeOspite & "(" & ward_id & "<>" & Vercentroservizio.CENTROSERVIZIO & ")"


                                        myriga(2) = Ospite.CODICEFISCALE


                                        myriga(3) = CS.CENTROSERVIZIO
                                        myriga(4) = Mid(DataMov, 1, 10)

                                        If Tipo = "05" Then
                                            myriga(5) = "Accoglimento"
                                        End If
                                        If Tipo = "13" Then
                                            myriga(5) = "Uscita Definitiva"
                                        End If
                                        If Tipo = "03" Then
                                            myriga(5) = "Uscita Temporanea"
                                        End If
                                        If Tipo = "04" Then
                                            myriga(5) = "Entrate"
                                        End If
                                        If Tipo = "99" Then
                                            myriga(5) = "Cambio Letto"
                                        End If

                                        myriga(6) = Tipo
                                        myriga(7) = "ERRORECS"
                                        myriga(8) = IdEpersonam
                                        myriga(9) = Ospite.CodiceOspite
                                        myriga(10) = Letto
                                        myriga(11) = ""
                                        If SalvaPrecedenteTipo = Tipo And _
                                            SalvaPrecedenteData = Mid(DataMov, 1, 10) Then
                                            myriga(11) = "S"
                                        End If
                                        If ward_id = 1498 Then
                                            ' CONVITTO TORINO
                                            If Tipo = "03" Then
                                                Try
                                                    If jTok2.Item("outtype").ToString = "Ricovero ospedaliero" Then
                                                        myriga(12) = "01"
                                                    Else
                                                        myriga(12) = "02"
                                                    End If
                                                Catch ex As Exception

                                                End Try
                                            End If
                                        End If

                                        Tabella.Rows.Add(myriga)
                                        SalvaPrecedenteTipo = Tipo
                                        SalvaPrecedenteData = Mid(DataMov, 1, 10)
                                    End If

                                Else
                                    Dim Letto1 As String
                                    Dim CS1 As New Cls_CentroServizio
                                    CS1.DESCRIZIONE = ""



                                    Dim KWardID As New Cls_Epersonam

                                    ward_id = KWardID.RendiWardid(ward_id, Context.Session("DC_OSPITE"))

                                    CS1.EPersonam = ward_id

                                    Dim AppoggioBool As Boolean
                                    AppoggioBool = True
                                    Dim Conv As String = ""
                                    Try
                                        Conv = jTok2.Item("conv").ToString
                                    Catch ex As Exception

                                    End Try

                                    If Conv.ToUpper = "TRUE" Then
                                        AppoggioBool = True
                                    End If
                                    If Conv.ToUpper = "FALSE" Then
                                        AppoggioBool = False
                                    End If

                                    CS1.LeggiEpersonam(Context.Session("DC_OSPITE"), CS1.EPersonam, AppoggioBool)
                                    Dim Tipo1 As String = ""
                                    Dim Type As String = ""
                                    Try
                                        Type = jTok2.Item("type").ToString
                                    Catch ex As Exception

                                    End Try
                                    Try
                                        Letto1 = jTok2.Item("bed").ToString
                                    Catch ex As Exception

                                    End Try
                                    If Type = "11" Then
                                        Tipo1 = "05"
                                    End If
                                    If Type = "9" Then
                                        Tipo1 = "03"
                                    End If
                                    If Type = "8" Then
                                        Tipo1 = "04"
                                    End If
                                    If Type = "12" Then
                                        Tipo1 = "13"
                                    End If
                                    If Type = "8" And (jTok2.Item("description").ToString.ToUpper = "CHANGE_BED" Or jTok2.Item("description").ToString.ToUpper = "NEW_BED") Then
                                        Tipo1 = "99"
                                        Try
                                            Letto1 = jTok2.Item("bed").ToString
                                        Catch ex As Exception

                                        End Try
                                    End If


                                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                                    myriga(0) = jTok1.Item("fullname").ToString()
                                    myriga(1) = ""

                                    myriga(2) = jTok1.Item("cf").ToString()

                                    myriga(3) = CS1.CENTROSERVIZIO
                                    myriga(4) = Mid(jTok2.Item("date").ToString, 1, 10)


                                    If Tipo1 = "05" Then
                                        myriga(5) = "Accoglimento"
                                    End If
                                    If Tipo1 = "13" Then
                                        myriga(5) = "Uscita Definitiva"
                                    End If
                                    If Tipo1 = "03" Then
                                        myriga(5) = "Uscita Temporanea"
                                    End If
                                    If Tipo1 = "04" Then
                                        myriga(5) = "Entrate"
                                    End If

                                    If Tipo1 = "99" Then
                                        myriga(5) = "Cambio Letto"
                                    End If
                                    myriga(6) = Tipo1
                                    myriga(7) = "ERROREPRE"
                                    myriga(8) = IdEpersonam
                                    myriga(9) = 0
                                    myriga(10) = Letto1

                                    myriga(11) = ""
                                    If SalvaPrecedenteTipo = Tipo1 And _
                                        SalvaPrecedenteData = Mid(jTok2.Item("date").ToString, 1, 10) Then
                                        myriga(11) = "S"
                                    End If

                                    Tabella.Rows.Add(myriga)
                                    SalvaPrecedenteTipo = Tipo1
                                    SalvaPrecedenteData = Mid(jTok2.Item("date").ToString, 1, 10)
                                End If
                                myPOSTreader.Close()
                            End If
                        End If
                    Next
                Else
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = jTok1.Item("fullname").ToString()
                    myriga(1) = ""

                    myriga(2) = jTok1.Item("cf").ToString()

                    myriga(3) = ""
                    myriga(4) = ""



                    myriga(5) = ""
                    myriga(6) = ""
                    myriga(7) = "ERRORE"
                    myriga(8) = 0
                    myriga(9) = 0
                    myriga(10) = ""
                    myriga(11) = ""

                    Tabella.Rows.Add(myriga)
                End If
            Next
        Next



        cn.Close()


    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim DD_Cserv As DropDownList = DirectCast(e.Row.FindControl("DD_Cserv"), DropDownList)

            Dim Cs As New Cls_CentroServizio

            Cs.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)

            DD_Cserv.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERRORE" Then
                DD_Cserv.BackColor = Drawing.Color.Red
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Red

            End If


            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERRORECS" Then
                DD_Cserv.BackColor = Drawing.Color.Orange
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                REM CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Orange

            End If

            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "NONIMP" Then
                DD_Cserv.BackColor = Drawing.Color.Green
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Green

            End If

            If Tabella.Rows(e.Row.RowIndex).Item(5).ToString = "Cambio Letto" Then
                DD_Cserv.BackColor = Drawing.Color.Green
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = True

                e.Row.BackColor = Drawing.Color.Green

            End If
            If Tabella.Rows(e.Row.RowIndex).Item(2).ToString = "TSSDNC36D54B499N" Then
                Dim i As Integer
                i = 0
            End If


            If Tabella.Rows(e.Row.RowIndex).Item(7).ToString = "ERROREPRE" Then
                DD_Cserv.BackColor = Drawing.Color.Aqua
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)


                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua

            End If



            If Tabella.Rows(e.Row.RowIndex).Item(11).ToString = "S" Then
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)
                CheckBox.Enabled = False
            End If
        End If
    End Sub

    Private Sub ListaBussineUnit(ByVal Token As String, ByVal context As Object, ByRef VettoreBU() As Integer)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()


        Dim Indice As Integer

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("id").ToString) >= 0 Then
                VettoreBU(Indice) = Val(jTok2.Item("id").ToString)
                Indice = Indice + 1
            End If
        Next
    End Sub


    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            'If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
            '    BusinessUnit = Val(jTok2.Item("id").ToString)
            '    ImportaMovimenti(BusinessUnit, Token)
            '    Exit For
            'Else
            BusinessUnit = Val(jTok2.Item("id").ToString)
            ImportaMovimenti(BusinessUnit, Token)
            'End If
        Next



    End Function

    Private Function CentriServizioEp(ByVal Token As String, ByVal context As HttpContext) As String
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        CentriServizioEp = rawresp
    End Function


    Private Function ElencoSottoStrutture(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As Integer
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Try
            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("structures").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                    ElencoSottoStrutture = Val(jTok2.Item("id").ToString)
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try

    End Function

    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try

            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE"))

            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String

            If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
                request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

                guarda = context.Session("ChiaveCr")
            Else
                request.Add("username", context.Session("EPersonamUser"))

                guarda = context.Session("EPersonamPSWCRYPT")
            End If


            request.Add("code", guarda)
            REM request.Add("password", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)
            REM Dim result As Object = client.UploadValues("http://api-v1.e-personam.com/v0/oauth/token", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()


            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer
        Dim MySql As String

        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Context.Session("DC_OSPITE"))

        cn.Open()


        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim DD_Cserv As DropDownList = DirectCast(GridView1.Rows(Riga).FindControl("DD_Cserv"), DropDownList)
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)
            If CheckBox.Checked = True Then
                If DD_Cserv.SelectedValue = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Non puoi importare i movimenti senza cserv');", True)
                    Exit Sub
                End If
            End If

            If CheckBox.Checked = True Then
                Dim Progressivo As Long


                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                cmdIns.Connection = cn
                cmdIns.Parameters.AddWithValue("@IdEpersonam", Val(Tabella.Rows(Riga).Item(8)))
                cmdIns.Parameters.AddWithValue("@CodiceFiscale", Tabella.Rows(Riga).Item(2))
                cmdIns.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                cmdIns.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(9)))
                cmdIns.Parameters.AddWithValue("@Data", Tabella.Rows(Riga).Item(4))
                cmdIns.Parameters.AddWithValue("@TipoMovimento", "M")
                cmdIns.Parameters.AddWithValue("@Descrizione", "")
                cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                cmdIns.ExecuteNonQuery()


                If Tabella.Rows(Riga).Item(6) = "99" Then

                    Dim MovDim As New Cls_MovimentiStanze
                    Dim MyData As Date = DateSerial(Val(Mid(Tabella.Rows(Riga).Item(4), 7, 4)), Val(Mid(Tabella.Rows(Riga).Item(4), 4, 2)), Val(Mid(Tabella.Rows(Riga).Item(4), 1, 2)))

                    MovDim.Villa = ""
                    MovDim.CentroServizio = DD_Cserv.SelectedValue
                    MovDim.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                    MovDim.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
                    If MovDim.Villa <> "" Then
                        MovDim.Id = 0
                        MovDim.Data = MyData.AddDays(-1)
                        MovDim.Tipologia = "LI"
                        MovDim.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                    End If

                    Dim MovLetto As New Cls_MovimentiStanze
                    Dim Letto As String = Tabella.Rows(Riga).Item(10)


                    MovLetto.CentroServizio = DD_Cserv.SelectedValue
                    MovLetto.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                    MovLetto.Data = MyData
                    MovLetto.Tipologia = "OC"
                    MovLetto.Villa = "01"
                    If Mid(Letto, 1, 1) = "1" Then
                        MovLetto.Reparto = "01"
                    End If
                    If Mid(Letto, 1, 1) = "2" Then
                        MovLetto.Reparto = "02"
                    End If
                    If Mid(Letto, 1, 1) = "3" Then
                        MovLetto.Reparto = "03"
                    End If
                    If Mid(Letto, 1, 1) = "4" Then
                        MovLetto.Reparto = "04"
                    End If
                    MovLetto.Piano = ""
                    MovLetto.Stanza = ""
                    MovLetto.Letto = Letto
                    MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))

                Else




                    Dim cmd1 As New OleDbCommand()
                    cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & DD_Cserv.SelectedValue & "' And  CodiceOspite = " & Val(Tabella.Rows(Riga).Item(9)))
                    cmd1.Connection = cn
                    Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                    If myPOSTreader1.Read Then
                        If Not IsDBNull(myPOSTreader1.Item(0)) Then
                            Progressivo = myPOSTreader1.Item(0) + 1
                        Else
                            Progressivo = 1
                        End If
                    Else
                        Progressivo = 1
                    End If



                    Dim MyData As Date = DateSerial(Val(Mid(Tabella.Rows(Riga).Item(4), 7, 4)), Val(Mid(Tabella.Rows(Riga).Item(4), 4, 2)), Val(Mid(Tabella.Rows(Riga).Item(4), 1, 2)))

                    MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)

                    cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
                    cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                    cmdw.Parameters.AddWithValue("@CentroServizio", DD_Cserv.SelectedValue)
                    cmdw.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(9)))
                    cmdw.Parameters.AddWithValue("@Data", DateSerial(Year(MyData), Month(MyData), Day(MyData)))
                    cmdw.Parameters.AddWithValue("@TIPOMOV", Tabella.Rows(Riga).Item(6))
                    cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
                    Dim CAUSALE As String = campodb(Tabella.Rows(Riga).Item(12))

                    If Tabella.Rows(Riga).Item(6) = "03" Then
                        If CAUSALE = "" Then
                            CAUSALE = "02"
                        End If
                    End If

                    If Tabella.Rows(Riga).Item(6) = "13" Then
                        If CAUSALE = "" Then
                            CAUSALE = "99"
                        End If
                    End If

                    cmdw.Parameters.AddWithValue("@CAUSALE", CAUSALE)
                    cmdw.Parameters.AddWithValue("@Descrizione", "")
                    cmdw.Parameters.AddWithValue("@EPersonam", 1)
                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()

                    Dim Letto As String = Tabella.Rows(Riga).Item(10)

                    If Letto <> "" And (Tabella.Rows(Riga).Item(6) = "13" Or Tabella.Rows(Riga).Item(6) = "05") Then
                        Dim MovLetto As New Cls_MovimentiStanze

                        MovLetto.CentroServizio = DD_Cserv.SelectedValue
                        MovLetto.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        MovLetto.Data = MyData
                        If Tabella.Rows(Riga).Item(6) = "13" Then
                            MovLetto.Tipologia = "LI"
                        Else
                            MovLetto.Tipologia = "OC"
                        End If
                        MovLetto.Villa = "01"
                        If Mid(Letto, 1, 1) = "1" Then
                            MovLetto.Reparto = "01"
                        End If
                        If Mid(Letto, 1, 1) = "2" Then
                            MovLetto.Reparto = "02"
                        End If
                        If Mid(Letto, 1, 1) = "3" Then
                            MovLetto.Reparto = "03"
                        End If
                        If Mid(Letto, 1, 1) = "4" Then
                            MovLetto.Reparto = "04"
                        End If
                        MovLetto.Piano = ""
                        MovLetto.Stanza = ""
                        MovLetto.Letto = Letto
                        MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                    End If

                    If Tabella.Rows(Riga).Item(6) = "05" Then

                        Dim Cserv As New Cls_CentroServizio

                        Cserv.CENTROSERVIZIO = DD_Cserv.SelectedValue
                        Cserv.Leggi(Session("DC_OSPITE"), DD_Cserv.SelectedValue)
                        Dim s As New Cls_Pianodeiconti

                        s.Descrizione = ""
                        s.Mastro = Cserv.MASTRO
                        s.Conto = Cserv.CONTO
                        s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100
                        s.Decodfica(Session("DC_GENERALE"))
                        If s.Descrizione = "" Then
                            Dim xOsp As New ClsOspite

                            xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                            xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)))
                            s.Descrizione = xOsp.Nome
                            s.Tipo = "A"
                            s.TipoAnagrafica = "O"
                            s.Scrivi(Session("DC_GENERALE"))
                        End If

                        Dim KPar As New Cls_Parametri

                        KPar.LeggiParametri(Session("DC_OSPITE"))

                        If KPar.MastroAnticipo > 0 Then
                            s.Descrizione = ""
                            s.Mastro = KPar.MastroAnticipo
                            s.Conto = Cserv.CONTO
                            s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100
                            s.Decodfica(Session("DC_GENERALE"))
                            If s.Descrizione = "" Then
                                Dim xOsp As New ClsOspite

                                xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)))
                                s.Descrizione = xOsp.Nome
                                s.Tipo = "P"
                                s.TipoAnagrafica = "O"
                                s.Scrivi(Session("DC_GENERALE"))
                            End If
                        End If

                        Dim Par As Integer
                        Dim OldCserv As New Cls_CentroServizio

                        OldCserv.CENTROSERVIZIO = DD_Cserv.SelectedValue
                        OldCserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

                        For Par = 1 To 10
                            Dim sPar As New Cls_Pianodeiconti
                            Dim Parente As New Cls_Parenti

                            Parente.Nome = ""
                            Parente.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                            Parente.CodiceParente = Par
                            Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                            If Parente.Nome <> "" Then
                                sPar.Descrizione = ""
                                sPar.Mastro = Cserv.MASTRO
                                sPar.Conto = Cserv.CONTO
                                sPar.Sottoconto = (Val(Tabella.Rows(Riga).Item(9)) * 100) + Par
                                sPar.Decodfica(Session("DC_GENERALE"))
                                If sPar.Descrizione = "" Then
                                    Dim xOsp As New Cls_Parenti


                                    xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                    xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)), Par)
                                    sPar.Tipo = "A"
                                    sPar.TipoAnagrafica = "P"
                                    sPar.Descrizione = xOsp.Nome
                                    sPar.Scrivi(Session("DC_GENERALE"))
                                End If
                                If KPar.MastroAnticipo > 0 Then
                                    s.Descrizione = ""
                                    s.Mastro = KPar.MastroAnticipo
                                    s.Conto = Cserv.CONTO
                                    s.Sottoconto = Val(Tabella.Rows(Riga).Item(9)) * 100 + Par
                                    s.Decodfica(Session("DC_GENERALE"))
                                    If s.Descrizione = "" Then
                                        Dim xOsp As New Cls_Parenti

                                        xOsp.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                                        xOsp.Leggi(Session("DC_OSPITE"), Val(Tabella.Rows(Riga).Item(9)), Par)
                                        s.Descrizione = xOsp.Nome
                                        s.Tipo = "A"
                                        s.TipoAnagrafica = "P"
                                        s.Scrivi(Session("DC_GENERALE"))
                                    End If
                                End If

                            End If
                        Next
                    End If

                    If Tabella.Rows(Riga).Item(6) = "13" Then


                        Dim MovDim As New Cls_MovimentiStanze

                        MovDim.CentroServizio = DD_Cserv.SelectedValue
                        MovDim.CodiceOspite = Val(Tabella.Rows(Riga).Item(9))
                        MovDim.UltimoMovimentoOspite(Session("DC_OSPITIACCESSORI"))
                        If MovDim.Id > 0 Then
                            MovDim.Id = 0
                            MovDim.Data = MyData
                            MovDim.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                        End If
                    End If
                    End If
            End If
        Next

        cn.Close()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Movimenti Importati');", True)

        Call Btn_Movimenti_Click(sender, e)
    End Sub

    Protected Sub Btn_Movimenti_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Btn_Movimenti.Command

    End Sub

    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)

    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
                If GridView1.Rows(Riga).Cells(5).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                    CheckBox.Checked = True
                End If
            End If
        Next
    End Sub
End Class

