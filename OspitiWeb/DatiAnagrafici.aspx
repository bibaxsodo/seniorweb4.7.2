﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DatiAnagrafici" EnableEventValidation="false" CodeFile="DatiAnagrafici.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Altri Dati Anagrafici</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">


    <script type="text/javascript">
        var windowObjectReference;

        function openRequestedPopup() {

            windowObjectReference = window.open("PdfDaHtml.aspx",
                "DescriptiveWindowName",
                "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");

        }
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
        }


        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 325px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Altri Dati Anagrafica</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Data Decesso :</label>
                                                <asp:TextBox ID="Txt_Decesso" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Luogo del Decesso :</label>
                                                <asp:TextBox ID="Txt_LuogoDecesso" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Nazionalità : </label>
                                                <asp:DropDownList ID="DD_Nazionalita" runat="server"></asp:DropDownList><br />
                                                <br />

                                                <label class="LabelCampo">Struttura Provenienza :</label>
                                                <asp:TextBox ID="Txt_StrutturaProvenienza" MaxLength="20" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Assistente Sociale :</label>
                                                <asp:TextBox ID="Txt_AssistenteSociale" MaxLength="20" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Distretto :</label>
                                                <asp:TextBox ID="Txt_Distretto" MaxLength="20" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Numero Documento :</label>
                                                <asp:TextBox ID="Txt_Numero" MaxLength="10" runat="server" Width="140px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Data Documento :</label>
                                                <asp:TextBox ID="Txt_Data" MaxLength="10" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Tipo Documento :</label>
                                                <asp:DropDownList ID="DD_TipoDocumento" runat="server"></asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Emettitore :</label>
                                                <asp:TextBox ID="Txt_Emettitore" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Data Scadenza :</label>
                                                <asp:TextBox ID="Txt_DataScadenza" MaxLength="10" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Stato Civile :</label>
                                                <asp:DropDownList ID="DD_StatoCivile" runat="server"></asp:DropDownList>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />

                                                <label class="LabelCampo">USL :</label>
                                                <asp:DropDownList ID="DD_CodiceUSL" class="chosen-select" runat="server" Width="50%"></asp:DropDownList><br />
                                                <br />


                                                <label class="LabelCampo">Codice CIG :</label>
                                                <asp:TextBox ID="Txt_CodiceCIG" MaxLength="20" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />


                                                <label class="LabelCampo">Conto Contabilità : </label>
                                                <asp:TextBox ID="Txt_Sottoconto" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />


                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Paternità :</label>
                                                <asp:TextBox ID="Txt_Paternita" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Maternità :</label>
                                                <asp:TextBox ID="Txt_Maternità" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Coniuge :</label>
                                                <asp:TextBox ID="Txt_Coniuge" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Professione Coniuge :</label>
                                                <asp:TextBox ID="Txt_ProfessioneConiuge" MaxLength="30" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>



                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Note" ID="TabPanel2">
                                <HeaderTemplate>
                                    Consensi
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <label class="LabelCampo">Opposizione 730:</label>
                                                <asp:CheckBox ID="chk_Opposizione730" runat="server" Text="" /><br />
                                                <br />
                                            </div>
                                        </div>
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <p style="text-align: center;">Consenso all’inserimento dei dati personali in Senior (l’informativa è stata compresa e firmata dall’interessato)</p>
                                                <p style="text-align: center;">
                                                    <asp:RadioButton ID="RB_SIConsenso1" runat="server" Text="SI" GroupName="Consenso1" />
                                                    <asp:RadioButton ID="RB_NOConsenso1" runat="server" Text="NO" GroupName="Consenso1" />
                                                </p>
                                                <br />
                                                <p style="text-align: center;">Consenso al trattamento per finalita’ di marketing</p>
                                                <p style="text-align: center;">
                                                    <asp:RadioButton ID="RB_SIConsenso2" runat="server" Text="SI" GroupName="Consenso2" />
                                                    <asp:RadioButton ID="RB_NOConsenso2" runat="server" Text="NO" GroupName="Consenso2" />
                                                </p>
                                                <br />

                                                <div class="Column">
                                                    <label class="LabelCampo">Seleziona Report:</label>
                                                    <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>
                                                </div>
                                                <div class="Column">
                                                    <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClientClick="javascript:return openRequestedPopup();" class="EffettoBottoniTondi" Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Note" ID="TabPanel1">
                                <HeaderTemplate>
                                    Note
                                </HeaderTemplate>
                                <ContentTemplate>
                                    Note<br />
                                    <asp:TextBox ID="Txt_Note" runat="server" Width="850px" Height="400px"></asp:TextBox>
                                    <xasp:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" TargetControlID="Txt_Note"></xasp:HtmlEditorExtender>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errore" runat="server" Width="784px" ForeColor="Red"></asp:Label>
                                    <br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
