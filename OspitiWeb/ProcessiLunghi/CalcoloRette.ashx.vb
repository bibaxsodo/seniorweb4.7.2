﻿Public Class CalcoloRette
    Implements System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState

    Public StDataCond As String



    Public Function DataAccoglimento(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Date
        Dim RsMovimenti As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim MySql As String

        OspitiDb.Open(ConnessioniOspiti)



        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' And " & StDataCond & " Order By DATA Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        On Error Resume Next
        If Not RsMovimenti.EOF Then
            DataAccoglimento = MoveFromDb(RsMovimenti, "Data")
            RsMovimenti.Close()
        Else
            RsMovimenti.Close()
            MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' Order By DATA Desc"

            RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not RsMovimenti.EOF Then
                DataAccoglimento = MoveFromDb(RsMovimenti, "Data")
            End If
            RsMovimenti.Close()
        End If
        OspitiDb.Close()
    End Function
    Public Function DataUscitaDefinitiva(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Date
        Dim RsMovimenti As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim MySql As String

        OspitiDb.Open(ConnessioniOspiti)

        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '13' And " & StDataCond & " Order By DATA Desc"

        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not RsMovimenti.EOF Then
            DataUscitaDefinitiva = MoveFromDb(RsMovimenti, "Data")
        End If
        RsMovimenti.Close()
        OspitiDb.Close()
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim MySql As String
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(ConnessioniOspiti)

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresente = False
        Else
            OspitePresente = False
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = "13" And Format(MoveFromDb(RsMovimenti.Fields("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        End If
        RsMovimenti.Close()
        OspitiDb.Close()
    End Function


    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Protected Sub calcolo(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal txtanno As Long, ByVal ddmese As Long, ByVal ConnessioneOspiti As String)




        Dim ConnectionString As String = ConnessioneOspiti
        Dim TipoCentro As String
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long
        Dim Mese As Integer
        Dim Anno As Integer
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim OspitiDb As New ADODB.Connection
        Dim MyRs As New ADODB.Recordset

        OspitiDb.Open(ConnessioneOspiti)

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette



        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati

        XS.ApriDB(ConnessioneOspiti, "")
        XS.STRINGACONNESSIONEDB = ConnessioneOspiti
        XS.CaricaCausali()

        Mese = ddmese
        Anno = txtanno
        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"


        VarCentroServizio = "0005"
        varcodiceospite = 371


        If CodiceOspite = 0 Then
            MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And MOVIMENTI.TIPOMOV = '05' AND (NonInUso <> 'S' or NonInUso  Is Null) GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        Else
            MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        End If

        System.Web.HttpContext.Current.Session("ProgressBar") = "Inizio Calcolo"
        XS.EliminaDatiRette(Mese, Anno, "", 0)
        Do While Not MyRs.EOF



            Dim MyCserv As New Cls_CentroServizio

            MyCserv.Leggi(ConnessioneOspiti, MoveFromDb(MyRs.Fields("CentroServizio")))

            VarCentroServizio = MoveFromDb(MyRs.Fields("CentroServizio"))
            varcodiceospite = MoveFromDb(MyRs.Fields("CodiceOspite"))
            Dim LeggiOspite As New ClsOspite

            LeggiOspite.Leggi(ConnessioneOspiti, varcodiceospite)

            System.Web.HttpContext.Current.Session("ProgressBar") = "Calcolo Ospite : " & LeggiOspite.Nome

            If Mese = Val(MyCserv.Mese1) Or Mese = Val(MyCserv.Mese2) Or Mese = Val(MyCserv.Mese3) Or Mese = Val(MyCserv.Mese4) Or Mese = Val(MyCserv.Mese5) Then


            Else
                If Not OspitePresente(VarCentroServizio, varcodiceospite, Mese, Anno, ConnessioneOspiti) Then

                    Call XS.AzzeraTabella()
                    XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)
                    xMese = Mese
                    xAnno = Anno
                    If xMese = 12 Then
                        xMese = 1
                        xAnno = Anno + 1
                    Else
                        xMese = Mese + 1
                        xAnno = Anno
                    End If
                    Dim XOsp As New ClsOspite

                    XOsp.Leggi(ConnessioneOspiti, varcodiceospite)
                    If XOsp.FattAnticipata = "S" Then
                        Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese, xAnno)
                        Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese, xAnno)


                        Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese, xAnno)

                        Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese, xAnno)

                        Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese, xAnno)
                        Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese, xAnno)



                        Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese, xAnno, False)
                        Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese, xAnno)
                    End If
                    TipoCentro = XS.CampoCentroServizio(VarCentroServizio, "TIPOCENTROSERVIZIO")
                    Call XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                Else
                    Call XS.AzzeraTabella()


                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
                    End If


                    If Val(MyCserv.VerificaImpegnativa) = 1 Then
                        Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
                    End If


                    XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

                    XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



                    XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
                    XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
                    XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



                    XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

                    XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

                    XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)


                    'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
                    XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

                    XS.MioCalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



                    XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

                    XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

                    If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
                        REM  Errori = True
                    Else
                        XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                    End If
                End If
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        OspitiDb.Close()

    End Sub


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim Anno As String = context.Request.QueryString("Anno")
        Dim Mese As String = context.Request.QueryString("Mese")

        calcolo(System.Web.HttpContext.Current.Session("CENTROSERVIZIO"), System.Web.HttpContext.Current.Session("CODICEOSPITE"), Anno, Mese, System.Web.HttpContext.Current.Session("DC_OSPITE"))

        context.Response.Write("FINE")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class