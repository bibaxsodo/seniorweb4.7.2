﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports Microsoft.VisualBasic
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq


Partial Class OspitiWeb_NucleiEpersonam
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()



    <Serializable()> Public Class nucleoEp
        Public Codice As String
        Public Descrizione As String
    End Class



    Private Sub DecodificaCodiceEpersonam()
        Dim Token As String
        Dim NuceliDropBox As List(Of nucleoEp) = New List(Of nucleoEp)



        Token = LoginPersonam(Context)
        Dim Url As String
        Dim rawresp As String

        Url = "https://api.e-personam.com/api/v1.0/business_units"

        Dim client As HttpWebRequest = WebRequest.Create(Url)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing



        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        rawresp = reader.ReadToEnd()



        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim IndiceRighe As Integer = 0
        Dim VtTipo(1000) As String
        Dim VtTipoCodice(1000) As String
        Dim VtPadre(1000) As Integer



        For Each jTok1 As JToken In jResults

            If jTok1.Item("structure").ToString.ToUpper = "true".ToUpper Then
                Dim Cerca As Integer
                Dim trovato As Boolean = False

                For Cerca = 0 To IndiceRighe - 1
                    If VtTipoCodice(Cerca) = jTok1.Item("id") Then
                        trovato = True
                    End If
                Next

                If Not trovato Then
                    IndiceRighe = IndiceRighe + 1
                    VtTipo(IndiceRighe) = jTok1.Item("name").ToString
                    VtTipoCodice(IndiceRighe) = jTok1.Item("id")
                    VtPadre(IndiceRighe) = 0

                End If
            End If


            If Not IsNothing(jTok1.Item("substructures")) Then
                For Each jTok2 As JToken In jTok1.Item("substructures")

                    Dim Cerca As Integer
                    Dim trovato As Boolean = False

                    For Cerca = 0 To IndiceRighe - 1
                        If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                            VtPadre(Cerca) = jTok1.Item("id")
                            trovato = True
                        End If
                    Next
                    If Not trovato Then
                        IndiceRighe = IndiceRighe + 1
                        VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                        VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                        VtPadre(IndiceRighe) = jTok1.Item("id")
                    End If

                    If Not IsNothing(jTok2.Item("units")) Then
                        For Each jTok3 As JToken In jTok2.Item("units")
                            '5214
                            If Val(jTok3.Item("id").ToString) = 5214 Then
                                trovato = False

                            End If

                            trovato = False
                            For Cerca = 0 To IndiceRighe - 1
                                If VtTipoCodice(Cerca) = jTok3.Item("id") Then
                                    VtPadre(Cerca) = jTok3.Item("id")
                                    trovato = True
                                End If
                            Next
                            If Not trovato Then
                                IndiceRighe = IndiceRighe + 1
                                VtTipo(IndiceRighe) = jTok3.Item("name").ToString
                                VtTipoCodice(IndiceRighe) = jTok3.Item("id")
                                VtPadre(IndiceRighe) = jTok2.Item("id")
                            End If

                        Next
                    End If
                Next
            End If
        Next

        Dim Ordina As Integer
        Dim OrdinaX As Integer

        For Ordina = 1 To IndiceRighe
            If VtPadre(Ordina) = 0 Then

                Dim Nucleo As New nucleoEp

                Nucleo.Codice = VtTipoCodice(Ordina)
                Nucleo.Descrizione = VtTipo(Ordina)

                NuceliDropBox.Add(Nucleo)

                Dim Indicex As Integer

                For Indicex = 0 To IndiceRighe
                    If VtPadre(Indicex) = VtTipoCodice(Ordina) Then
                        Dim Nucleo1 As New nucleoEp
                        Nucleo1.Codice = VtTipoCodice(Indicex)
                        Nucleo1.Descrizione = "---" & VtTipo(Indicex)

                        NuceliDropBox.Add(Nucleo1)
                        Dim Indicey As Integer

                        For Indicey = 0 To IndiceRighe
                            If VtPadre(Indicey) = VtTipoCodice(Indicex) Then
                                Dim Nucleo2 As New nucleoEp

                                Nucleo2.Codice = VtTipoCodice(Indicey)
                                Nucleo2.Descrizione = "------" & VtTipo(Indicey)

                                NuceliDropBox.Add(Nucleo2)
                                Dim Indicez As Integer

                                For Indicez = 0 To IndiceRighe
                                    If VtPadre(Indicez) = VtTipoCodice(Indicey) Then

                                        Dim Nucleo3 As New nucleoEp

                                        Nucleo3.Codice = VtTipoCodice(Indicez)
                                        Nucleo3.Descrizione = "---------" & VtTipo(Indicez)

                                        NuceliDropBox.Add(Nucleo3)
                                    End If
                                Next

                            End If
                        Next
                    End If
                Next

            End If

        Next


        For Ordina = 1 To IndiceRighe
            If VtTipoCodice(Ordina) = Val(Request.Item("CODICE")) Then
                lblDatiCella.Text = VtTipoCodice(Ordina) & " - " & VtTipo(Ordina)
            End If
        Next

        ViewState("NuceliDropBox") = NuceliDropBox
    End Sub


    Private Sub UpdateDropDown(ByRef appoggio As DropDownList)
        Dim NuceliDropBox As List(Of nucleoEp) = New List(Of nucleoEp)

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True


        NuceliDropBox = ViewState("NuceliDropBox")

        For i = 0 To NuceliDropBox.Count - 1
            appoggio.Items.Add(NuceliDropBox(i).Descrizione)
            appoggio.Items(appoggio.Items.Count - 1).Value = Val(NuceliDropBox(i).Codice)
        Next
    End Sub


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub


        DecodificaCodiceEpersonam()



        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice", GetType(String))

        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim RecordPresenti As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NucleiCentroServizi Where IdSenior = ?")
        cmd.Parameters.AddWithValue("@id", Val(Request.Item("CODICE")))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myrigaRiga As System.Data.DataRow = MyTable.NewRow()
            myrigaRiga(0) = campodb(myPOSTreader.Item("IdEpersonam"))
            MyTable.Rows.Add(myrigaRiga)

            RecordPresenti = True
        Loop
        myPOSTreader.Close()
        cn.Close()
        If Not RecordPresenti Then
            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()
            myrigaR(0) = 0
            MyTable.Rows.Add(myrigaR)
        End If


        ViewState("App_Retta") = MyTable
        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Cella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Cella.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable

        Grd_Cella.DataBind()




        Call EseguiJS()
    End Sub

    Protected Sub Grd_Cella_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Cella.RowDeleted

    End Sub

    Protected Sub Grd_Cella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Cella.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = 0
            
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = 0

                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
    Protected Sub Grd_Cella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Cella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DD_Nucleo As DropDownList = DirectCast(e.Row.FindControl("DD_Nucleo"), DropDownList)
            Dim Txt_IdNuclei As TextBox = DirectCast(e.Row.FindControl("Txt_IdNuclei"), TextBox)
            UpdateDropDown(DD_Nucleo)


            DD_Nucleo.SelectedValue = Val(MyTable.Rows(e.Row.RowIndex).Item(0).ToString)
            Txt_IdNuclei.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

        End If
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice", GetType(String))


        'intypecod


        For i = 0 To Grd_Cella.Rows.Count - 1

            Dim DD_Nucleo As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Nucleo"), DropDownList)
            Dim Txt_IdNuclei As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_IdNuclei"), TextBox)



            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()

            myrigaR(0) = Txt_IdNuclei.Text
            


            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        UpDateTable()
        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim RecordPresenti As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from NucleiCentroServizi Where IdSenior = ?")
        cmd.Parameters.AddWithValue("@id", Val(Request.Item("CODICE")))
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        For i = 0 To Grd_Cella.Rows.Count - 1

            Dim Txt_IdNuclei As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_IdNuclei"), TextBox)


            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = ("INSERT INTO NucleiCentroServizi  (IdSenior,IDePERSONAM) VALUES (?,?)")
            cmdIns.Parameters.AddWithValue("@id", Val(Request.Item("CODICE")))
            cmdIns.Parameters.AddWithValue("@id", Val(Txt_IdNuclei.Text))
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        Next
        cn.Close()



        Call EseguiJS()
    End Sub


    Protected Sub DD_Nucleo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RowInd As GridViewRow = ddl.Parent.Parent
        Dim IndiceRiga As Integer

        IndiceRiga = RowInd.RowIndex

        Dim DD_Nucleo As DropDownList = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("DD_Nucleo"), DropDownList)
        Dim Txt_IdNuclei As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_IdNuclei"), TextBox)

        Txt_IdNuclei.Text = DD_Nucleo.SelectedValue


    End Sub
End Class
