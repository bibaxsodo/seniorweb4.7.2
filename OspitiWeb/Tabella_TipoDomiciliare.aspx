﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Tabella_TipoDomiciliare" CodeFile="Tabella_TipoDomiciliare.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Tipologia Domiciliare</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Tipologia Domiciliare</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Imb_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Tipologia Domiciliare        
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <label class="LabelCampo">Codice : </label>
                                    <asp:TextBox ID="Txt_Codice" MaxLength="2" Width="50px" runat="server" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="472px" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDescrizione" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Considera Ingresso :</label>
                                    <asp:CheckBox ID="Chk_ConsideraIngressso" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Fatturazione Fine Mese :</label>
                                    <asp:CheckBox ID="Chk_FatturaFineMese" runat="server" Text="(Operatore)" />
                                    <br />
                                    <br />
                                    <br />

                                    <asp:GridView ID="Grd_DatiDomicliare" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="IB_Inserisci" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tipo Addebito">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoAddebito" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Tipo Retta">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoRetta" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Tipo Operatore">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoOperatore" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tipo">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Tipo" runat="server">
                                                        <asp:ListItem Value="I">Importo Fisso</asp:ListItem>
                                                        <asp:ListItem Value="O">Orario</asp:ListItem>
                                                        <asp:ListItem Value="M">30 min</asp:ListItem>
                                                        <asp:ListItem Value="Q">15 min</asp:ListItem>
                                                        <asp:ListItem Value="C">5 min</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Ripartizione">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Ripartizione" runat="server">
                                                        <asp:ListItem Value="O">Ospite</asp:ListItem>
                                                        <asp:ListItem Value="R">Regione</asp:ListItem>
                                                        <asp:ListItem Value="C">Comune</asp:ListItem>
                                                        <asp:ListItem Value="J">Jolly</asp:ListItem>
                                                        <asp:ListItem Value="T">Operatore</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Importo1">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto1" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Importo2">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Importo3">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto3" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Operatori">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Operatori" runat="server">
                                                        <asp:ListItem Value="0"></asp:ListItem>
                                                        <asp:ListItem Value="1">Importo 1 Operatore</asp:ListItem>
                                                        <asp:ListItem Value="2">Importo >=2 Operatori</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Ingresso">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Ingressi" runat="server">
                                                        <asp:ListItem Value="0"></asp:ListItem>
                                                        <asp:ListItem Value="1">1° Ingresso</asp:ListItem>
                                                        <asp:ListItem Value="2">2°> Ingresso</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Festivo">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Festivo" runat="server">
                                                        <asp:ListItem Value="0"></asp:ListItem>
                                                        <asp:ListItem Value="1">1 Festivo</asp:ListItem>
                                                        <asp:ListItem Value="2">2 Feriale</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="FasciaOraria">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_FasciaOraria" runat="server">
                                                        <asp:ListItem Value="0"></asp:ListItem>
                                                        <asp:ListItem Value="1">1 Diurna</asp:ListItem>
                                                        <asp:ListItem Value="2">2 Notturna</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>



                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
