﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_IncassiAnticipi
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabellaDoc As New System.Data.DataTable("tabellaDoc")

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Private Sub PienaGriglia()
        Dim Xs As New Cls_CausaleContabile
        Dim DareAvereIncPag As String
        Dim CausaleAcqVend As String
        Dim DareAvere As String
        Dim MySql As String
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Importo As Double
        Dim DocumentoPagato As Double
        Dim DataDocumento As String
        Dim NumeroDocumento As String
        Dim CausaleContabile As String
        Dim NumeroRegistrazione As Long


        Xs.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)
        DareAvereIncPag = ""
        If Not IsNothing(Xs.Righe(0)) Then
            DareAvereIncPag = Xs.Righe(0).DareAvere
        End If
        CausaleAcqVend = Xs.VenditaAcquisti
        If DareAvereIncPag = "D" Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If



        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If
        


        MySql = "SELECT (SELECT SUM(IMPORTO) FROM TABELLALEGAMI WHERE CODICEDOCUMENTO = MovimentiContabiliTesta.NumeroRegistrazione ) AS DocumentoPagato, MovimentiContabiliTesta.NumeroRegistrazione, MovimentiContabiliTesta.CausaleContabile, MovimentiContabiliTesta.DataDocumento, MovimentiContabiliTesta.NumeroDocumento, MovimentiContabiliTesta.Descrizione, MovimentiContabiliTesta.DataRegistrazione, MovimentiContabiliRiga.Importo, MovimentiContabiliRiga.DareAvere, MovimentiContabiliRiga.Tipo " & _
                         " FROM MovimentiContabiliRiga  INNER JOIN MovimentiContabiliTesta  ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero  " & _
                         " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & " AND MovimentiContabiliRiga.ContoPartita = " & Conto & " AND MovimentiContabiliRiga.SottocontoPartita = " & Sottoconto & " AND (" & Xs.EstraiCondizioniSQL(Session("DC_TABELLE")) & ")" & _
                         " ORDER BY NumeroRegistrazione "

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("Copia", GetType(Long))
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If NumeroRegistrazione = myPOSTreader.Item("NumeroRegistrazione") Then
                If DareAvereIncPag = myPOSTreader.Item("DareAvere") Then
                    Importo = Importo - myPOSTreader.Item("Importo")
                Else
                    Importo = Importo + myPOSTreader.Item("Importo")
                End If

            Else
                If Request.Item("TIPO") <> "SCADENZARIO" Then

                    If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = NumeroRegistrazione
                        myriga(1) = DataDocumento
                        myriga(2) = NumeroDocumento
                        If Importo < 0 Then
                            myriga(3) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                        Else
                            myriga(3) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                        End If
                        myriga(4) = "0,00"

                        myriga(5) = False

                        myriga(6) = False
                        myriga(7) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                Else
                    Dim kScadenze As New Cls_Scadenziario
                    Dim TabScadenze As New System.Data.DataTable
                    Dim i As Long

                    kScadenze.NumeroRegistrazioneContabile = NumeroRegistrazione
                    kScadenze.loaddati(Session("DC_GENERALE"), TabScadenze)

                    For i = 0 To TabScadenze.Rows.Count - 1
                        If TabScadenze.Rows(i).Item(3).ToString <> "Chiusa" Then
                            Dim myriga As System.Data.DataRow = Tabella.NewRow()
                            myriga(0) = NumeroRegistrazione
                            myriga(1) = DataDocumento
                            myriga(2) = NumeroDocumento
                            If TabScadenze.Rows(i).Item(1).ToString = "" Then
                                myriga(3) = "0,00"
                            Else
                                If Importo < 0 Then
                                    myriga(3) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString) * -1, 2), "#,##0.00")
                                Else
                                    myriga(3) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString), 2), "#,##0.00")
                                End If
                            End If
                            myriga(4) = "0,00"
                            myriga(5) = True


                            myriga(6) = False
                            myriga(7) = TabScadenze.Rows(i).Item(4)
                            Tabella.Rows.Add(myriga)
                        End If
                    Next

                End If

                NumeroRegistrazione = myPOSTreader.Item("NumeroRegistrazione")
                DocumentoPagato = Math.Abs(campodbN(myPOSTreader.Item("DocumentoPagato")))
                DataDocumento = campodbd(myPOSTreader.Item("DataDocumento"))
                NumeroDocumento = myPOSTreader.Item("NumeroDocumento")
                CausaleContabile = myPOSTreader.Item("CausaleContabile")
                If NumeroRegistrazione = 15483 Then
                    NumeroRegistrazione = 15483
                End If

                Dim XCau As New Cls_CausaleContabile

                XCau.Leggi(Session("DC_TABELLE"), myPOSTreader.Item("CausaleContabile"))

                If XCau.VenditaAcquisti <> CausaleAcqVend Then
                    If DareAvereIncPag <> myPOSTreader.Item("DareAvere") Then
                        Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                    Else
                        Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                    End If
                Else
                    If DareAvereIncPag = myPOSTreader.Item("DareAvere") Then
                        Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                    Else
                        Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
            If Request.Item("TIPO") <> "SCADENZARIO" Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = NumeroRegistrazione
                myriga(1) = DataDocumento
                myriga(2) = NumeroDocumento
                If Importo < 0 Then
                    myriga(3) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                Else
                    myriga(3) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                End If
                myriga(4) = "0,00"
                myriga(5) = False

                myriga(6) = False
                myriga(7) = 0
                Tabella.Rows.Add(myriga)
            Else
                Dim kScadenze As New Cls_Scadenziario
                Dim TabScadenze As New System.Data.DataTable
                Dim i As Long

                kScadenze.NumeroRegistrazioneContabile = NumeroRegistrazione
                kScadenze.loaddati(Session("DC_GENERALE"), TabScadenze)

                For i = 0 To TabScadenze.Rows.Count - 1
                    If TabScadenze.Rows(i).Item(3).ToString <> "Chiusa" Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = NumeroRegistrazione
                        myriga(1) = DataDocumento
                        myriga(2) = NumeroDocumento
                        If TabScadenze.Rows(i).Item(1).ToString = "" Then
                            myriga(3) = "0,00"
                        Else
                            If Importo < 0 Then
                                myriga(3) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString) * -1, 2), "#,##0.00")
                            Else
                                myriga(3) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString), 2), "#,##0.00")
                            End If
                        End If
                        myriga(4) = "0,00"
                        myriga(5) = True


                        myriga(6) = False
                        myriga(7) = TabScadenze.Rows(i).Item(4)
                        Tabella.Rows.Add(myriga)
                    End If
                Next
            End If

        End If

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        Session("Save_Tab_IA") = Tabella


        cn.Close()
        For I = 0 To Grid.Rows.Count - 1

            Grid.Rows(I).Cells(0).Text = "<a href=""#"" onclick=""copiaimporto('" & I & "');""><img src=""..\generaleweb\images\copiaimporto.png"" height=""16"" width=""16"" alt=""digita importo""></a>" & Grid.Rows(I).Cells(0).Text

        Next

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        Dim MyJs As String

        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataBolletta.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataBolletta.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "$('#" & Txt_Importo.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importo.ClientID & "').val(), true, true); } );  $('#" & Txt_Importo.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importo.ClientID & "').val(),2); $('#" & Txt_Importo.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatLoad", MyJs, True)


        If Page.IsPostBack = True Then Exit Sub


        Lbl_ID.Text = "0"

        Session("Save_Tab_IA") = Nothing

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBoxPag(ConnectionString, Dd_CausaleContabile)


        Dim ModOsp As New Cls_ModalitaPagamento


        ModOsp.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPagamento)


        DD_ModalitaPagamento.SelectedValue = x.MODALITAPAGAMENTO

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("Copia", GetType(Long))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        If Request.Item("TIPO") = "SCADENZARIO" Then
            myriga(5) = True
            myriga(6) = False
        Else
            myriga(5) = False
            myriga(6) = False
        End If
        myriga(7) = 0
        Tabella.Rows.Add(myriga)
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        Dim CParenti As New Cls_Parenti
        Dim COspite As New ClsOspite

        COspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        CParenti.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_OspiteParenti)
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Text = ""
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Value = 99
        DD_OspiteParenti.Items(DD_OspiteParenti.Items.Count - 1).Selected = True


        DD_OspiteParenti.SelectedValue = Val(Request.Item("CodiceParente"))
        DD_OspiteParenti.Enabled = False

        Dim ModalitaPagamento As String = ""
        If Val(Request.Item("CodiceParente")) = 99 Then
            ModalitaPagamento = COspite.MODALITAPAGAMENTO
        Else
            CParenti.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))
            ModalitaPagamento = CParenti.MODALITAPAGAMENTO

        End If
 


        Txt_Importo.Text = "0,00"

        Dim ParamTbl As New Cls_Parametri

        ParamTbl.LeggiParametri(Session("DC_OSPITE"))

        Call Calcolo(ParamTbl.MeseFatturazione, ParamTbl.AnnoFatturazione)


        Lbl_Periodo.Text = ParamTbl.MeseFatturazione & "/" & ParamTbl.AnnoFatturazione

        If ModalitaPagamento <> "" Then
            Dim CSModal As New ClsModalitaPagamento

            CSModal.Codice = ModalitaPagamento
            CSModal.Leggi(Session("DC_OSPITE"))

            Dim cn As OleDbConnection



            cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from CausaliContabiliRiga order by Codice")
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodb(myPOSTreader.Item("Mastro")) = CSModal.MASTRO And campodb(myPOSTreader.Item("Conto")) = CSModal.CONTO And campodb(myPOSTreader.Item("Sottoconto")) = CSModal.SOTTOCONTO Then
                    Dim cmdT As New OleDbCommand()
                    cmdT.CommandText = ("select * from CausaliContabiliTesta Where Codice = ?")
                    cmdT.Parameters.AddWithValue("@Codice", campodb(myPOSTreader.Item("Codice")))
                    cmdT.Connection = cn
                    Dim MyRdT As OleDbDataReader = cmdT.ExecuteReader()
                    If MyRdT.Read Then
                        If campodb(MyRdT.Item("Tipo")) = "P" And campodb(MyRdT.Item("VenditaAcquisti")) = "V" Then
                            Dd_CausaleContabile.SelectedValue = campodb(myPOSTreader.Item("Codice"))
                        End If
                        MyRdT.Close()
                    End If
                End If
            Loop
            myPOSTreader.Close()
            cn.Close()
            If Dd_CausaleContabile.SelectedValue = "" Then
                Exit Sub
            End If
            Call PienaGriglia()
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare causale contabile');", True)
            REM Lbl_errori.Text = "Specificare causale contabile"
            Exit Sub
        End If
        Call PienaGriglia()        
    End Sub

    Private Function CreaAnticipo() As Boolean
        Dim x As New ClsOspite
        Dim xPar As New Cls_Parenti

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim ImportoBollo As Double = 0
        Dim CodiceBollo As String = ""
        Dim Importo As Double = 0

        Dim TipoOperazione As New Cls_TipoOperazione


        If DD_OspiteParenti.SelectedValue = 99 Then
            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(ConnectionString, Session("CODICEOSPITE"))


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                x.TIPOOPERAZIONE = KCs.TipoOperazione
                x.CODICEIVA = KCs.AliquotaIva
                x.FattAnticipata = KCs.Anticipata
                x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                x.Compensazione = KCs.Compensazione
                x.SETTIMANA = KCs.Settimana
            End If
        Else
            xPar.CodiceOspite = Session("CODICEOSPITE")
            xPar.CodiceParente = DD_OspiteParenti.SelectedValue
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), DD_OspiteParenti.SelectedValue)


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = DD_OspiteParenti.SelectedValue
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xPar.TIPOOPERAZIONE = KCs.TipoOperazione
                xPar.CODICEIVA = KCs.AliquotaIva
                xPar.FattAnticipata = KCs.Anticipata
                xPar.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xPar.Compensazione = KCs.Compensazione
            End If

            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)
        End If




        TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)




        If TipoOperazione.CausaleDocumentoAnticipo = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Causale Contabile Anticipo non presente in tipo operazione</center>');", True)
            CreaAnticipo = False
            Exit Function
        End If




        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Registrazione As New Cls_MovimentoContabile
        CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleDocumentoAnticipo)




        Dim ImportIVA As Double
        Dim CIva As New Cls_IVA

        CIva.Codice = x.CODICEIVA
        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)
        Importo = CDbl(Txt_Importo.Text)

        If TipoOperazione.SoggettaABollo = "S" Then
            Dim ClsBollo As New Cls_bolli

            ClsBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)

            If Importo > ClsBollo.ImportoApplicazione Then
                ImportoBollo = ClsBollo.ImportoBollo
                CodiceBollo = ClsBollo.CodiceIVA
                Importo = Importo - ImportoBollo

            End If
        End If

        

        If TipoOperazione.SCORPORAIVA = 1 Then
            Importo = Math.Round(CDbl(Txt_Importo.Text) * 100 / (100 + (CIva.Aliquota * 100)), 2)
        End If

        ImportIVA = CDbl(Importo * CIva.Aliquota)

        
        Registrazione.DataRegistrazione = Txt_DataRegistrazione.Text
        Registrazione.DataDocumento = Txt_DataRegistrazione.Text
        Registrazione.NumeroDocumento = Registrazione.MaxProtocollo(ConnectionStringGenerale, Year(Txt_DataRegistrazione.Text), CausaleContabile.RegistroIVA) + 1
        Registrazione.CausaleContabile = TipoOperazione.CausaleDocumentoAnticipo
        Registrazione.MeseCompetenza = Month(Txt_DataRegistrazione.Text)
        Registrazione.AnnoCompetenza = Year(Txt_DataRegistrazione.Text)
        Registrazione.CentroServizio = Session("CODICESERVIZIO")
        Registrazione.Tipologia = "O"
        If DD_OspiteParenti.SelectedValue > 0 Then
            Registrazione.Tipologia = "P"
        End If
        Registrazione.AnnoProtocollo = Year(Txt_DataRegistrazione.Text)
        Registrazione.NumeroProtocollo = Registrazione.NumeroDocumento


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(ConnectionString, Session("CODICESERVIZIO"))


        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = CentroServizio.MASTRO
            Conto = CentroServizio.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = CentroServizio.MASTRO
            Conto = CentroServizio.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If

        Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(0).MastroPartita = Mastro
        Registrazione.Righe(0).ContoPartita = Conto
        Registrazione.Righe(0).SottocontoPartita = Sottoconto
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = CausaleContabile.Righe(0).Segno
        Registrazione.Righe(0).Importo = Importo + ImportIVA + ImportoBollo
        Registrazione.Righe(0).Tipo = "CF"
        Registrazione.Righe(0).RigaDaCausale = 1


        Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
        Registrazione.Righe(1).Segno = CausaleContabile.Righe(1).Segno
        Registrazione.Righe(1).Importo = ImportIVA
        Registrazione.Righe(1).Imponibile = Importo
        Registrazione.Righe(1).CodiceIVA = x.CODICEIVA
        Registrazione.Righe(1).Tipo = "IV"
        Registrazione.Righe(1).RigaDaCausale = 2


        Registrazione.Righe(2) = New Cls_MovimentiContabiliRiga
        Dim TabPar As New Cls_Parametri

        TabPar.LeggiParametri(Session("DC_OSPITE"))

        Registrazione.Righe(2).MastroPartita = TabPar.MastroAnticipo
        Registrazione.Righe(2).ContoPartita = Conto
        Registrazione.Righe(2).SottocontoPartita = Sottoconto

        Registrazione.Righe(2).DareAvere = CausaleContabile.Righe(2).DareAvere
        Registrazione.Righe(2).Segno = CausaleContabile.Righe(2).Segno
        Registrazione.Righe(2).Importo = Importo
        Registrazione.Righe(2).Imponibile = 0
        Registrazione.Righe(2).CodiceIVA = x.CODICEIVA
        Registrazione.Righe(2).Tipo = ""
        Registrazione.Righe(2).RigaDaCausale = 3


        If ImportoBollo > 0 Then


            Registrazione.Righe(3) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(3).MastroPartita = CausaleContabile.Righe(1).Mastro
            Registrazione.Righe(3).ContoPartita = CausaleContabile.Righe(1).Conto
            Registrazione.Righe(3).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
            Registrazione.Righe(3).DareAvere = CausaleContabile.Righe(1).DareAvere
            Registrazione.Righe(3).Segno = CausaleContabile.Righe(1).Segno
            Registrazione.Righe(3).Descrizione = "Bollo"
            Registrazione.Righe(3).Importo = 0
            Registrazione.Righe(3).Imponibile = ImportoBollo
            Registrazione.Righe(3).CodiceIVA = CodiceBollo
            Registrazione.Righe(3).Tipo = "IV"
            Registrazione.Righe(3).RigaDaCausale = 2


            Registrazione.Righe(4) = New Cls_MovimentiContabiliRiga


            Registrazione.Righe(4).MastroPartita = CausaleContabile.Righe(8).Mastro
            Registrazione.Righe(4).ContoPartita = CausaleContabile.Righe(8).Conto
            Registrazione.Righe(4).SottocontoPartita = CausaleContabile.Righe(8).Sottoconto

            Registrazione.Righe(4).DareAvere = CausaleContabile.Righe(8).DareAvere
            Registrazione.Righe(4).Segno = CausaleContabile.Righe(8).Segno
            Registrazione.Righe(4).Importo = ImportoBollo
            Registrazione.Righe(4).Imponibile = 0
            Registrazione.Righe(4).CodiceIVA = ImportoBollo
            Registrazione.Righe(4).Tipo = ""
            Registrazione.Righe(4).RigaDaCausale = 9

        End If

        Dim VerificaValidita As String = Registrazione.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE"))

        If VerificaValidita <> "OK" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>" & VerificaValidita & "</center>');", True)
            CreaAnticipo = False
            Exit Function
        End If

        If Registrazione.Scrivi(ConnectionStringGenerale, -1) = False Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            CreaAnticipo = False
            Exit Function
        End If




        Dim Legami As New Cls_Legami

        Legami.Importo(0) = Txt_Importo.Text
        Legami.NumeroDocumento(0) = Registrazione.NumeroRegistrazione
        Legami.NumeroPagamento(0) = Registrazione.NumeroRegistrazione + 1
        Legami.Scrivi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione, 0)

        CreaAnticipo = True
    End Function
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim NumeroRegistrazione As Long

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Lbl_errori.Text = ""
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Exit Sub
        End If
        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare causale contabile');", True)
            REM Lbl_errori.Text = "Digitare causale contabile"
            Exit Sub
        End If
        If Txt_Importo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare importo incassato');", True)
            REM Lbl_errori.Text = "Digitare importo pagato/incassato"
            Exit Sub
        End If
        Try
            If CDbl(Txt_Importo.Text) = 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare importo incassato');", True)
                Exit Sub
            End If
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare importo incassato');", True)
            Exit Sub
        End Try
        


        Dim xTot As Double
        Dim i As Long
        Dim DescrizioneInIncasso As String

        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = Session("CODICEOSPITE")
        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)



        DescrizioneInIncasso = Ospite.Nome


        Try
            Tabella = Session("Save_Tab_IA")
            
            xTot = 0
            If Not IsNothing(Tabella) Then
                For i = 0 To Tabella.Rows.Count - 1
                    Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)

                    If TxtImponibile.Text <> "" Then
                        If TxtImponibile.Text <> 0 Then
                            If CDbl(TxtImponibile.Text) > 0 And CDbl(Tabella.Rows(i).Item(3)) < 0 Then
                                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore importo documento negativo in <br/>riga " & i + 1 & " importo pagato positivo');", True)
                                Exit Sub
                            End If
                            If Math.Abs(CDbl(TxtImponibile.Text)) > Math.Abs(CDbl(Tabella.Rows(i).Item(3))) Then
                                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore importo pagato/incassato <br/>riga " & i + 1 & " maggiore importo documento');", True)
                                Exit Sub
                            End If
                            xTot = xTot + CDbl(TxtImponibile.Text)

                            DescrizioneInIncasso = DescrizioneInIncasso & " " & Tabella.Rows(i).Item(2) & "-" & Tabella.Rows(i).Item(1)
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
        If Math.Round(CDbl(Txt_Importo.Text), 2) < Math.Round(xTot, 2) And xTot <> 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Importo distribuito maggiore del pagato/incassato :" & xTot & " " & CDbl(Txt_Importo.Text) & "');", True)
            REM Lbl_errori.Text = "Importo distribuito maggiore del pagato/incassato"
            Exit Sub
        End If



        If VerificaSeInserito() Then
            Response.Redirect("ElencoRegistrazione.aspx")
            Exit Sub
        End If


        DescrizioneInIncasso = DescrizioneInIncasso & " " & Dd_CausaleContabile.SelectedItem.Text

        Dim xCO As New ClsOspite
        Dim xPar As New Cls_Parenti
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")


        Dim TipoOperazione As New Cls_TipoOperazione

        If DD_OspiteParenti.SelectedValue = 99 Then

            xCO.CodiceOspite = Session("CODICEOSPITE")
            xCO.Leggi(ConnectionString, Session("CODICEOSPITE"))


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xCO.TIPOOPERAZIONE = KCs.TipoOperazione
                xCO.CODICEIVA = KCs.AliquotaIva
                xCO.FattAnticipata = KCs.Anticipata
                xCO.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xCO.Compensazione = KCs.Compensazione
                xCO.SETTIMANA = KCs.Settimana
            End If

            TipoOperazione.Leggi(ConnectionString, xCO.TIPOOPERAZIONE)
        Else
            xPar.CodiceOspite = Session("CODICEOSPITE")
            xPar.CodiceParente = DD_OspiteParenti.SelectedValue
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), DD_OspiteParenti.SelectedValue)


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = DD_OspiteParenti.SelectedValue
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xPar.TIPOOPERAZIONE = KCs.TipoOperazione
                xPar.CODICEIVA = KCs.AliquotaIva
                xPar.FattAnticipata = KCs.Anticipata
                xPar.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xPar.Compensazione = KCs.Compensazione                
            End If

            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)

        End If




        If xTot = 0 Then
            If TipoOperazione.Anticipo <> "" Then
                If CreaAnticipo() = False Then
                    Exit Sub
                End If
            End If
        End If


        Dim X As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile

        X.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

        X.NumeroRegistrazione = NumeroRegistrazione
        X.DataRegistrazione = Txt_DataRegistrazione.Text
        If IsDate(Txt_DataBolletta.Text) Then
            X.DataBolletta = Txt_DataBolletta.Text
        Else
            X.DataBolletta = Nothing
        End If

        If DD_ModalitaPagamento.SelectedValue <> "" Then
            Dim MiaModalita1 As New ClsModalitaPagamento

            MiaModalita1.Codice = DD_ModalitaPagamento.SelectedValue
            MiaModalita1.Leggi(Session("DC_OSPITE"))

            If MiaModalita1.ModalitaPagamento <> "" Then
                X.ModalitaPagamento = MiaModalita1.ModalitaPagamento
            End If
        End If


        X.CentroServizio = Session("CODICESERVIZIO")


        X.NumeroBolletta = Txt_NumeroBolletta.Text
        If Txt_Descrizione.Text = "" Then
            X.Descrizione = DescrizioneInIncasso
        Else
            X.Descrizione = Txt_Descrizione.Text
        End If
        X.Utente = Session("UTENTE")


        X.CausaleContabile = Dd_CausaleContabile.SelectedValue
        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim XCs As New Cls_CentroServizio

        Dim KoSP As New Cls_Parametri

        KoSP.LeggiParametri(Session("DC_OSPITE"))

        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If
        Dim Riga As Long

        Riga = 0

        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        X.Righe(0).MastroPartita = Mastro
        X.Righe(0).ContoPartita = Conto
        X.Righe(0).SottocontoPartita = Sottoconto
        X.Righe(0).Importo = Txt_Importo.Text
        X.Righe(0).Segno = "+"
        X.Righe(0).Tipo = "CF"
        X.Righe(0).RigaDaCausale = 1
        X.Righe(0).DareAvere = MyCau.Righe(0).DareAvere

        Riga = Riga + 1


        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        Dim X1 As New Cls_CausaleContabile

        X1.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim xS As New Cls_Pianodeiconti

        Mastro = X1.Righe(1).Mastro
        Conto = X1.Righe(1).Conto
        Sottoconto = X1.Righe(1).Sottoconto

        If DD_ModalitaPagamento.SelectedValue <> "" Then
            Dim MiaModalita As New ClsModalitaPagamento

            MiaModalita.Codice = DD_ModalitaPagamento.SelectedValue
            MiaModalita.Leggi(Session("DC_OSPITE"))

            If MiaModalita.MASTRO > 0 Then
                Mastro = MiaModalita.MASTRO
                Conto = MiaModalita.CONTO
                Sottoconto = MiaModalita.SOTTOCONTO
            End If
        End If

        X.Righe(Riga).MastroPartita = Mastro
        X.Righe(Riga).ContoPartita = Conto
        X.Righe(Riga).SottocontoPartita = Sottoconto
        X.Righe(Riga).Importo = Txt_Importo.Text
        X.Righe(Riga).Segno = "+"
        X.Righe(Riga).Tipo = ""
        X.Righe(Riga).RigaDaCausale = 2
        X.Righe(Riga).DareAvere = MyCau.Righe(1).DareAvere
        Riga = Riga + 1



        If X.Scrivi(Session("DC_GENERALE"), NumeroRegistrazione) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Exit Sub
        End If

        NumeroRegistrazione = X.NumeroRegistrazione



        If xTot <> 0 Then
            Dim Xleg As Integer
            Dim Legami As New Cls_Legami
            Tabella = Session("Save_Tab_IA")
            Xleg = 0

            For i = 0 To Tabella.Rows.Count - 1
                Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
                Dim Chk_Chiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("Chk_Chiusa"), CheckBox)

                If TxtImponibile.Text.Trim <> "" Then
                    If CDbl(TxtImponibile.Text) <> 0 Then
                        Legami.Importo(Xleg) = TxtImponibile.Text
                        Legami.NumeroDocumento(Xleg) = Val(Tabella.Rows(i).Item(0))
                        Legami.NumeroPagamento(Xleg) = X.NumeroRegistrazione
                        Xleg = Xleg + 1
                    End If
                End If
            Next

            Legami.Scrivi(Session("DC_GENERALE"), 0, NumeroRegistrazione)
        End If




        Dim xo1 As New ClsOspite

        xo1.CodiceOspite = Session("CODICEOSPITE")
        xo1.Leggi(Session("DC_OSPITE"), X.CodiceOspite)

        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", NumeroRegistrazione, xo1.Nome, "I", "INCASSO", "")

        Response.Redirect("ElencoRegistrazione.aspx")
    End Sub

    Function VerificaSeInserito() As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmdTesta As New OleDbCommand()
        cmdTesta.CommandText = "SELECT * FROM MovimentiContabiliTesta  Where Utente = ? And DATAAGGIORNAMENTO >= ?"
        cmdTesta.Connection = cn
        cmdTesta.Parameters.AddWithValue("@Utente", Session("UTENTE"))
        cmdTesta.Parameters.AddWithValue("@Utente", Now.AddSeconds(-2))
        Dim ReadTesta As OleDbDataReader = cmdTesta.ExecuteReader()
        If ReadTesta.Read Then
            ReadTesta.Close()

            cn.Close()
            Return True
        End If
        ReadTesta.Close()

        cn.Close()
        Return False
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function





    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Lbl_ID.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi prima modificare');", True)
            Exit Sub
        End If


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("ElencoRegistrazione.aspx?CodiceParente=" & Val(Request.Item("CodiceParente")) & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub




    Private Sub Calcolo(ByVal Mese As Integer, ByVal Anno As Integer)
        Dim XS As New Cls_CalcoloRette
        Dim VarCentroServizio As String = Session("CODICESERVIZIO")
        Dim varcodiceospite As Long = Session("CODICEOSPITE")
        Dim MyCserv As New Cls_CentroServizio
        Dim f As New Cls_Parametri


        f.LeggiParametri(Session("DC_OSPITE"))

        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"

        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati

        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = Session("DC_OSPITE")
        XS.CaricaCausali()

        MyCserv.Leggi(Session("DC_OSPITE"), VarCentroServizio)

        Call XS.AzzeraTabella()

        XS.EliminaDatiRette(Mese, Anno, VarCentroServizio, varcodiceospite)


        If MyCserv.TIPOCENTROSERVIZIO = "D" Then
            XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If


        If Val(MyCserv.VerificaImpegnativa) = 1 Then
            Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If


        XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



        XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
        XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
        XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



        XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

        XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)


        'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
        XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.MioCalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



        XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

        If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
            REM  Errori = True
        Else
            XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        If Request.Item("CodiceParente") = 99 Or Request.Item("CodiceParente") = 0 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And (Elemento = 'RGP' or Elemento = 'RPX')")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
            cmd.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_ImportoRettaPresenza.Text = Format(campodbN(myPOSTreader.Item("Timporto")), "#,##0.00")
                Txt_GiorniPresenza.Text = campodbN(myPOSTreader.Item("TGiorni"))
            End If
            myPOSTreader.Close()

            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento = 'RGA'")
            cmd2.Connection = cn
            cmd2.Parameters.AddWithValue("@Anno", Anno)
            cmd2.Parameters.AddWithValue("@Mese", Mese)
            cmd2.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd2.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Txt_ImportoRettaAssenza.Text = Format(campodbN(myPOSTreader2.Item("Timporto")), "#,##0.00")
                Txt_GiorniAssenza.Text = campodbN(myPOSTreader2.Item("TGiorni"))
            End If
            myPOSTreader2.Close()

            Dim cmd3 As New OleDbCommand()
            cmd3.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento Like 'E%'")
            cmd3.Connection = cn
            cmd3.Parameters.AddWithValue("@Anno", Anno)
            cmd3.Parameters.AddWithValue("@Mese", Mese)
            cmd3.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd3.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
            If myPOSTreader3.Read Then

                Txt_ExtraFissi.Text = Format(campodbN(myPOSTreader3.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader3.Close()

            Dim cmd4 As New OleDbCommand()
            cmd4.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento Like 'ADD'")
            cmd4.Connection = cn
            cmd4.Parameters.AddWithValue("@Anno", Anno)
            cmd4.Parameters.AddWithValue("@Mese", Mese)
            cmd4.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd4.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
            If myPOSTreader4.Read Then
                Txt_ImportoAddibiti.Text = Format(campodbN(myPOSTreader4.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader4.Close()


            Dim cmd5 As New OleDbCommand()
            cmd5.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento Like 'ACC'")
            cmd5.Connection = cn
            cmd5.Parameters.AddWithValue("@Anno", Anno)
            cmd5.Parameters.AddWithValue("@Mese", Mese)
            cmd5.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd5.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
            If myPOSTreader5.Read Then
                Txt_ImportoAccrediti.Text = Format(campodbN(myPOSTreader5.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader5.Close()
        Else
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento = 'RGP'")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
            cmd.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_ImportoRettaPresenza.Text = Format(campodbN(myPOSTreader.Item("Timporto")), "#,##0.00")
                Txt_GiorniPresenza.Text = campodbN(myPOSTreader.Item("TGiorni"))
            End If
            myPOSTreader.Close()

            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento = 'RGA'")
            cmd2.Connection = cn
            cmd2.Parameters.AddWithValue("@Anno", Anno)
            cmd2.Parameters.AddWithValue("@Mese", Mese)
            cmd2.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd2.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd2.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Txt_ImportoRettaAssenza.Text = Format(campodbN(myPOSTreader2.Item("Timporto")), "#,##0.00")
                Txt_GiorniAssenza.Text = campodbN(myPOSTreader2.Item("TGiorni"))
            End If
            myPOSTreader2.Close()

            Dim cmd3 As New OleDbCommand()
            cmd3.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento Like 'E%'")
            cmd3.Connection = cn
            cmd3.Parameters.AddWithValue("@Anno", Anno)
            cmd3.Parameters.AddWithValue("@Mese", Mese)
            cmd3.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd3.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd3.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))

            Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
            If myPOSTreader3.Read Then
                Txt_ExtraFissi.Text = Format(campodbN(myPOSTreader3.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader3.Close()

            Dim cmd4 As New OleDbCommand()
            cmd4.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento Like 'ADD'")
            cmd4.Connection = cn
            cmd4.Parameters.AddWithValue("@Anno", Anno)
            cmd4.Parameters.AddWithValue("@Mese", Mese)
            cmd4.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd4.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd4.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
            If myPOSTreader4.Read Then
                Txt_ImportoAddibiti.Text = Format(campodbN(myPOSTreader4.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader4.Close()


            Dim cmd5 As New OleDbCommand()
            cmd5.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento Like 'ACC'")
            cmd5.Connection = cn
            cmd5.Parameters.AddWithValue("@Anno", Anno)
            cmd5.Parameters.AddWithValue("@Mese", Mese)
            cmd5.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd5.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd5.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
            If myPOSTreader5.Read Then
                Txt_ImportoAccrediti.Text = Format(campodbN(myPOSTreader5.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader5.Close()
        End If
        cn.Close()

        Call CalcolaTotale()


        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim XCs As New Cls_CentroServizio

        Dim KoSP As New Cls_Parametri

        KoSP.LeggiParametri(Session("DC_OSPITE"))

        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim Somma As Double = 0

        Dim cmd6 As New OleDbCommand()
        cmd6.CommandText = ("select sum(Importo) as Totale from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And DareAvere = 'D' ")
        cmd6.Connection = cn
        cmd6.Parameters.AddWithValue("@Mastro", Mastro)
        cmd6.Parameters.AddWithValue("@Conto", Conto)
        cmd6.Parameters.AddWithValue("@SottoConto", Sottoconto)
        Dim myPOSTreader6 As OleDbDataReader = cmd6.ExecuteReader()
        If myPOSTreader6.Read Then
            Somma = campodbN(myPOSTreader6.Item("Totale"))
        End If
        myPOSTreader6.Close()


        Dim cmd7 As New OleDbCommand()
        cmd7.CommandText = ("select sum(Importo) as Totale from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And DareAvere = 'A' ")
        cmd7.Connection = cn
        cmd7.Parameters.AddWithValue("@Mastro", Mastro)
        cmd7.Parameters.AddWithValue("@Conto", Conto)
        cmd7.Parameters.AddWithValue("@SottoConto", Sottoconto)
        Dim myPOSTreader7 As OleDbDataReader = cmd7.ExecuteReader()
        If myPOSTreader7.Read Then
            Somma = Somma - campodbN(myPOSTreader7.Item("Totale"))
        End If
        myPOSTreader7.Close()        

        Lbl_Debito.Text = Format(Somma, "#,##0.00")

        Somma = 0

        Dim cmd8 As New OleDbCommand()
        cmd8.CommandText = ("select sum(Importo) as Totale from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And DareAvere = 'D' ")
        cmd8.Connection = cn
        cmd8.Parameters.AddWithValue("@Mastro", KoSP.MastroAnticipo)
        cmd8.Parameters.AddWithValue("@Conto", Conto)
        cmd8.Parameters.AddWithValue("@SottoConto", Sottoconto)
        Dim myPOSTreader8 As OleDbDataReader = cmd8.ExecuteReader()
        If myPOSTreader8.Read Then
            Somma = campodbN(myPOSTreader8.Item("Totale"))
        End If
        myPOSTreader8.Close()


        Dim cmd9 As New OleDbCommand()
        cmd9.CommandText = ("select sum(Importo) as Totale from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And DareAvere = 'A' ")
        cmd9.Connection = cn
        cmd9.Parameters.AddWithValue("@Mastro", KoSP.MastroAnticipo)
        cmd9.Parameters.AddWithValue("@Conto", Conto)
        cmd9.Parameters.AddWithValue("@SottoConto", Sottoconto)
        Dim myPOSTreader9 As OleDbDataReader = cmd9.ExecuteReader()
        If myPOSTreader9.Read Then
            Somma = Somma - campodbN(myPOSTreader9.Item("Totale"))
        End If
        myPOSTreader9.Close()

        Dim IncassiNonLegati As Double = 0

        Dim cmd10 As New OleDbCommand()
        cmd10.CommandText = ("select Importo,(Select sum(Importo) From TabellaLegami Where CodicePagamento = Numero) as Legato from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And DareAvere = 'A' ")
        cmd10.Connection = cn
        cmd10.Parameters.AddWithValue("@Mastro", Mastro)
        cmd10.Parameters.AddWithValue("@Conto", Conto)
        cmd10.Parameters.AddWithValue("@SottoConto", Sottoconto)
        Dim myPOSTreader10 As OleDbDataReader = cmd10.ExecuteReader()
        Do While myPOSTreader10.Read
            IncassiNonLegati = IncassiNonLegati + campodbN(myPOSTreader10.Item("Importo")) - campodbN(myPOSTreader10.Item("Legato"))
        Loop
        myPOSTreader10.Close()

        cn.Close()

        Lbl_Anticipo.Text = Format(Somma, "#,##0.00")



        Lbl_IncassiNonInChiuso.text = Format(IncassiNonLegati, "#,##0.00")

    End Sub

    Private Sub CalcolaTotale()
        Dim Totale As Double
        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(ConnectionString)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            x.TIPOOPERAZIONE = KCs.TipoOperazione
            x.CODICEIVA = KCs.AliquotaIva
            x.FattAnticipata = KCs.Anticipata
            x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            x.Compensazione = KCs.Compensazione
            x.SETTIMANA = KCs.Settimana
        End If


        Dim TipoOperazione As New Cls_TipoOperazione

        TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)

        Call CalcolaIVA()
        Totale = CDbl(Txt_ImportoRettaPresenza.Text)

        Totale = Totale + CDbl(Txt_ImportoRettaAssenza.Text)
        Totale = Totale + CDbl(Txt_ImportoIVA.Text)

        Totale = Totale + CDbl(Txt_ImportoAddibiti.Text)
        Totale = Totale - CDbl(Txt_ImportoAccrediti.Text)

        If TipoOperazione.SoggettaABollo = "S" Then
            Dim ClsBollo As New Cls_bolli

            ClsBollo.Leggi(Session("DC_TABELLE"), Now)


            If (CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaAssenza.Text)) > ClsBollo.ImportoBollo Then
                Txt_Bollo.Text = Format(ClsBollo.ImportoBollo, "#,##0.00")
            End If
        End If

        If Txt_Bollo.Text = "" Then
            Txt_Bollo.Text = "0,00"
        End If

        Txt_Totale.Text = Format(Totale + CDbl(Txt_Bollo.Text), "#,##0.00")
    End Sub
    Private Sub CalcolaIVA()
        Dim x As New ClsOspite
        Dim xPar As New Cls_Parenti
        Dim MySql As String

        Dim Txt_AnnoText As Integer
        Dim Dd_mese As Integer


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")


        If Request.Item("CodiceParente") = 99 Then
            x.Leggi(ConnectionString, Session("CODICEOSPITE"))


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                x.TIPOOPERAZIONE = KCs.TipoOperazione
                x.CODICEIVA = KCs.AliquotaIva
                x.FattAnticipata = KCs.Anticipata
                x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                x.Compensazione = KCs.Compensazione
                x.SETTIMANA = KCs.Settimana
            End If
        Else
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))



            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = Val(Request.Item("CodiceParente"))
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xPar.TIPOOPERAZIONE = KCs.TipoOperazione
                xPar.CODICEIVA = KCs.AliquotaIva
                xPar.FattAnticipata = KCs.Anticipata
                xPar.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xPar.Compensazione = KCs.Compensazione

            End If
        End If

        Dim TipoOperazione As New Cls_TipoOperazione

        If Request.Item("CodiceParente") = 99 Then
            TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)
        Else
            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)
        End If

        Dim ImportIVA As Double
        Dim Importo As Double
        Dim CodiceBollo As String
        Dim ImportoBollo As Double

        Dim CIva As New Cls_IVA

        If Request.Item("CodiceParente") = 99 Then
            CIva.Codice = x.CODICEIVA
        Else
            CIva.Codice = xPar.CODICEIVA
        End If


        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

        ImportIVA = CDbl((CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaAssenza.Text)) * CIva.Aliquota)





        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        If CDbl(Txt_ImportoAddibiti.Text) > 0 Then

            MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND  CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Dd_mese & " And Year(DATA) = " & Txt_AnnoText & " And (Elaborato = 0 or Elaborato Is Null) "
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim KTipoAddebito As New Cls_Addebito

                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)


                ImportIVA = ImportIVA + Math.Round((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
            Loop
            myPOSTreader.Close()
        End If

        If CDbl(Txt_ImportoAccrediti.Text) > 0 Then

            MySql = "SelecT * FROM ADDACR WHERE TIPOMOV = 'AC' AND  CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Dd_mese & " And Year(DATA) = " & Txt_AnnoText & " And (Elaborato = 0 or Elaborato Is Null) "
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read


                Dim KTipoAddebito As New Cls_Addebito

                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)


                ImportIVA = ImportIVA - Math.Round((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
            Loop
            myPOSTreader.Close()
        End If
        cn.Close()

        Txt_ImportoIVA.Text = Format(ImportIVA, "#,##0.00")
    End Sub

    Protected Sub Dd_CausaleContabile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.TextChanged
        If Dd_CausaleContabile.SelectedValue = "" Then
            Exit Sub
        End If
        Call PienaGriglia()
    End Sub

    Protected Sub Txt_DataBolletta_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DataBolletta.TextChanged

    End Sub
End Class
