﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.IO.Compression


Partial Class OspitiWeb_Incassidarid
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0



        MySql = "SELECT MIN(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MassimoDocumento = 0



        MySql = "SELECT MAX(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text


        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub DD_Mese_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mese.SelectedIndexChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_AnnoRif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_AnnoRif.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub


    Protected Sub OspitiWeb_Export_Documenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione

        Dim Bn As New Cls_Banche

        Bn.UpDateDropBox(Session("DC_OSPITE"), DD_Banca)


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim CauInc As New Cls_CausaleContabile

        CauInc.UpDateDropBoxPag(Session("DC_TABELLE"), Dd_CausaleContabile)



        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Session("CampoProgressBar") = 0


        DD_SelezionaInvia.Items.Clear()

        DD_SelezionaInvia.Items.Add("")
        DD_SelezionaInvia.Items(DD_SelezionaInvia.Items.Count - 1).Value = ""


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim Cmd As New OleDbCommand

        Cmd.CommandText = "select DATARID,IdRid  from MOVIMENTICONTABILITESTA where not DATARID is null and not (IdRid = '' or IdRid is null) group by DATARID,IdRid  Order by DATARID desc"
        Cmd.Connection = cn
        Cmd.ExecuteNonQuery()
        Dim myPOSTreader As OleDbDataReader = Cmd.ExecuteReader()
        Do While myPOSTreader.Read
            DD_SelezionaInvia.Items.Add(Format(campodbD(myPOSTreader.Item("DATARID")), "dd/MM/yyyy") & " " & campodb(myPOSTreader.Item("IdRid")))
            DD_SelezionaInvia.Items(DD_SelezionaInvia.Items.Count - 1).Value = Format(campodbD(myPOSTreader.Item("DATARID")), "dd/MM/yyyy") & " " & campodb(myPOSTreader.Item("IdRid"))
        Loop
        cn.Close()
        

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Txt_DataIncasso.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare una data incasso');", True)
            Exit Sub
        End If


        If Not IsDate(Txt_DataIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare una data incasso');", True)
            Exit Sub
        End If


        'If Dd_CausaleContabile.SelectedValue = "" Then
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare la causale di incasso');", True)
        '    Exit Sub
        'End If



        Call CreaGriglia()

        Btn_Salva.visible = True

    End Sub




    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function





    Sub CreaGriglia()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String

        MeseContr = DD_Mese.SelectedValue

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("CognomeNome", GetType(String))
            Tabella.Columns.Add("Iban", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("DataMandato", GetType(String))
            Tabella.Columns.Add("IdMandato", GetType(String))
            Tabella.Columns.Add("ImportoTotale", GetType(String))
            Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
            Tabella.Columns.Add("DO11_INDIRIZZO", GetType(String))
            Tabella.Columns.Add("AdrLine", GetType(String))
            Tabella.Columns.Add("First", GetType(String))
            Tabella.Columns.Add("CODICEOSPITE", GetType(String))
            Tabella.Columns.Add("CODICEPARENTE", GetType(String))
            Tabella.Columns.Add("CognomeNomeIntestatario", GetType(String))
            Tabella.Columns.Add("NumeroProtocollo", GetType(String))
            Tabella.Columns.Add("AnnoProtocollo", GetType(String))
            Tabella.Columns.Add("NumeroRegistrazioneIncasso", GetType(String))
        Catch ex As Exception

        End Try

        Dim contenuto As String = ""
        If FileUpload1.FileName <> "" Then
            Dim NomeSocieta As String = "xml"
            Dim Appo As String
            Randomize()
            Dim NomeFile As String = Format(Now, "ddMMyyyyhhmmss") & Int(Rnd(1) * 10)

            Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
            End If
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xml")




            Dim objStreamReader As StreamReader


            objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xml")

            ' Leggo il contenuto del file sino alla fine (ReadToEnd)
            contenuto = objStreamReader.ReadToEnd()

            objStreamReader.Close()
        End If


        Dim Condizione As String = ""

        If DD_Struttura.SelectedValue <> "" Then
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            Else
                Dim Indice As Integer

                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " OR "
                        End If
                        Condizione = Condizione & " CentroServizio =  '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = " And (" & Condizione & ") "
            End If
        Else
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            End If
        End If
        If DD_SelezionaInvia.SelectedValue <> "" Then
            MySql = "SELECT * " & _
                        " FROM MovimentiContabiliTesta " & _
                        " WHERE MovimentiContabiliTesta.DataRid = ? And " & _
                        " MovimentiContabiliTesta.IdRid = ? "

        Else
            If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
                MySql = "SELECT * " & _
                        " FROM MovimentiContabiliTesta " & _
                        " WHERE MovimentiContabiliTesta.DataRegistrazione >= ? And " & _
                        " MovimentiContabiliTesta.DataRegistrazione <= ? And NumeroProtocollo > 0"
            Else
                MySql = "SELECT * " & _
                        " FROM MovimentiContabiliTesta " & _
                        " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                        " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                        " AND AnnoProtocollo = " & Txt_AnnoRif.Text
                MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "
            End If
        End If

        If DD_Registro.SelectedValue <> "0" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If


        MySql = MySql & " And (Select count(tabellalegami.Importo) from tabellalegami where CodiceDocumento = MovimentiContabiliTesta.NumeroRegistrazione) =0 "
        MySql = MySql & Condizione

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        MySql = MySql & " ORDER BY (SELECT TOP 1  SottocontoPartita FROM [MovimentiContabiliRiga] WHERE Numero = NumeroRegistrazione AND RigaDaCausale =1 )"
        cmd.CommandText = MySql  '& " Order by NumeroProtocollo"
        cmd.Connection = cn

        If DD_SelezionaInvia.SelectedValue <> "" Then
            cmd.Parameters.AddWithValue("@DataRid", Mid(DD_SelezionaInvia.SelectedValue, 1, 10))
            cmd.Parameters.AddWithValue("@IdRid", Mid(DD_SelezionaInvia.SelectedValue, 12, Len(DD_SelezionaInvia.SelectedValue) - 11))
        Else
            If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
                cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataDal.Text)
                cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataAl.Text)
            End If
        End If

        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0


        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue

        AppoggioNumero = 0
        AppoggioTotale = 0

        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            


            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""
            Dim ImporotTotale As Double = 0


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If


                If campodb(MyReadSC.Item("DareAvere")) = "A" Then
                    ImporotTotale = campodbN(MyReadSC.Item("Importo")) * -1
                Else
                    ImporotTotale = campodbN(MyReadSC.Item("Importo"))
                End If
            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_FIRST As String = ""
            Dim DO11_CODICEOSPITE As Integer = 0
            Dim DO11_CODICEPARENTE As Integer = 0
            Dim DO11_TIPOFLUSSO As String = ""
            Dim DO11_INTESTATARIO As String = ""
            Dim DO11_BANCA As String = ""


            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                DO11_CODICEOSPITE = CodiceOspite
                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))



                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

                DO11_BANCA = ModPag.CodiceBanca

                If Trim(ModPag.IntestatarioConto) <> "" Then
                    DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                    DO11_INDIRIZZO = ""
                    DO11_CAP = ""
                    DO11_CITTA_NOME = ""
                    DO11_PROVINCIA = ""
                End If

                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti


                DO11_CODICEOSPITE = CodiceOspite
                DO11_CODICEPARENTE = CodiceParente

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA = ModPag.CodiceBanca

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If

                If Trim(ModPag.IntestatarioConto) <> "" Then
                    DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                    DO11_INDIRIZZO = ""
                    DO11_CAP = ""
                    DO11_CITTA_NOME = ""
                    DO11_PROVINCIA = ""
                End If
            End If

            If campodb(myPOSTreader.Item("CodicePagamento")) <> "" Then
                DO11_CONDPAG_CODICE_SEN = campodb(myPOSTreader.Item("CodicePagamento"))
            End If
            If DO11_CONDPAG_CODICE_SEN = "" Then
                DO11_CONDPAG_CODICE_SEN = "XX"
            End If
            Dim M As New Cls_TipoPagamento
            M.Descrizione = ""
            M.Codice = DO11_CONDPAG_CODICE_SEN
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If

            Dim MPAgP As New ClsModalitaPagamento

            MPAgP.Codice = DO11_CONDPAG_CODICE_SEN
            MPAgP.Leggi(Session("DC_OSPITE"))
            DO11_TIPOFLUSSO = MPAgP.Tipo


            Dim Entra As Boolean = True

            If contenuto <> "" Then
                Entra = False
                If contenuto.IndexOf("<MndtId>" & DO11_BANCA_IDMANDATO & "</MndtId>") > 0 Then
                    Entra = True
                End If
                If contenuto.IndexOf("<InstrId>" & campodbN(myPOSTreader.Item("NumeroRegistrazione"))) > 0 Then
                    Entra = True
                End If
                If contenuto.IndexOf(campodbN(myPOSTreader.Item("NumeroRegistrazione")) & "</InstrId>") > 0 Then
                    Entra = True
                End If
            End If
            If Entra Then
                If DD_Banca.SelectedValue = "" Or (DO11_BANCA = DD_Banca.SelectedValue) Then
                    If M.Descrizione.IndexOf("R.I.D.") > 0 Or M.Descrizione.IndexOf("SEPA") > 0 Or DO11_TIPOFLUSSO = "R" Then

                        Dim AppoggioData As Date

                        AppoggioTotale = AppoggioTotale + ImporotTotale
                        AppoggioNumero = AppoggioNumero + 1


                        Try
                            AppoggioData = DO11_BANCA_DATAMANDATO
                        Catch ex As Exception

                        End Try

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = DO11_RAGIONESOCIALE
                        myriga(1) = DO11_BANCA_IBAN
                        myriga(2) = DO11_CF
                        myriga(3) = Format(AppoggioData, "yyyy-MM-dd")
                        myriga(4) = DO11_BANCA_IDMANDATO
                        myriga(5) = Format(ImporotTotale, "0.00").Replace(",", ".")
                        myriga(6) = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                        myriga(7) = DO11_INDIRIZZO
                        myriga(8) = DO11_CAP & " " & DO11_CITTA_NOME & " " & DO11_PROVINCIA
                        myriga(9) = DO11_FIRST
                        myriga(10) = DO11_CODICEOSPITE
                        myriga(11) = DO11_CODICEPARENTE
                        myriga(12) = DO11_INTESTATARIO
                        myriga(13) = campodbN(myPOSTreader.Item("NumeroProtocollo"))
                        myriga(14) = campodbN(myPOSTreader.Item("AnnoProtocollo"))
                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        GridView1.AutoGenerateColumns = False
        Btn_Dowload.Enabled = True


        Session("TabellaRid") = Tabella
        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
        Lbl_Totali.Text = "Numero Rid estratti " & AppoggioNumero & " per un importo di " & Format(AppoggioTotale, "#,##0.00")
        Lbl_Totali.Visible = True
        Btn_Seleziona.Visible = True

    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                If GridView1.Rows(Riga).Cells(3).Text <> "000000000000000000000000" And GridView1.Rows(Riga).Cells(4).Text <> "SI" Then
                    CheckBox.Checked = True
                    CheckBox.Checked = True
                End If
            End If
        Next
    End Sub

    Protected Sub Btn_Salva_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Salva.Click
        If Txt_DataIncasso.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare una data incasso');", True)
            Exit Sub
        End If


        If Not IsDate(Txt_DataIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare una data incasso');", True)
            Exit Sub
        End If


        'If Dd_CausaleContabile.SelectedValue = "" Then
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Devi indicare la causale di incasso');", True)
        '    Exit Sub
        'End If


        Crea_XML()
    End Sub




    Sub Crea_XML()

        Tabella = Session("TabellaRid")

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String


        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0
        Dim DataIncasso As Date = Txt_DataIncasso.Text



        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"


        Dim KTab As New Cls_TabellaSocieta

        KTab.Leggi(Session("DC_TABELLE"))

        If DD_Banca.SelectedValue <> "" Then
            Dim mpD As New Cls_Banche

            mpD.Codice = DD_Banca.SelectedValue
            mpD.Leggi(Session("DC_OSPITE"), mpD.Codice)


            KTab.OrgId = mpD.OrgId
            KTab.MmbId = mpD.MmbId
            KTab.IBANRID = mpD.IBANRID
            KTab.PrvtId = mpD.PrvtId
            KTab.EndToEndId = mpD.EndToEndId
        End If

        

        AppoggioNumero = 0
        AppoggioTotale = 0

        Dim Riga As Integer
        Dim RecurrentOrFirst As String = ""


        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            Dim VerificaLegami As New Cls_Legami

            If CheckBox.Checked = True And VerificaLegami.TotaleLegame(Session("DC_GENERALE"), Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString)) = 0 Then
                Dim RegistrazioneDocumento As New Cls_MovimentoContabile

                RegistrazioneDocumento.NumeroRegistrazione = Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString)
                RegistrazioneDocumento.Leggi(Session("DC_GENERALE"), RegistrazioneDocumento.NumeroRegistrazione)


                If Dd_CausaleContabile.SelectedValue = "" Then

                    Dim CausaleDocumento As New Cls_CausaleContabile

                    CausaleDocumento.Codice = RegistrazioneDocumento.CausaleContabile
                    CausaleDocumento.Leggi(Session("DC_TABELLE"), CausaleDocumento.Codice)


                    If CausaleDocumento.CausaleIncasso = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Documento senza causale di incasso non posso procedere');", True)
                        Exit Sub
                    End If

                End If
            End If
        Next

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            Dim VerificaLegami As New Cls_Legami

            If CheckBox.Checked = True And VerificaLegami.TotaleLegame(Session("DC_GENERALE"), Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString)) = 0 Then



                AppoggioNumero = AppoggioNumero + 1
                AppoggioTotale = AppoggioTotale + Replace(Tabella.Rows(Riga).Item("ImportoTotale"), ".", ",")

                Dim ImportoRegistrazione As Double = Replace(Tabella.Rows(Riga).Item("ImportoTotale"), ".", ",")

                Dim Registrazione As New Cls_MovimentoContabile
                Dim RegistrazioneDocumento As New Cls_MovimentoContabile

                RegistrazioneDocumento.NumeroRegistrazione = Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString)
                RegistrazioneDocumento.Leggi(Session("DC_GENERALE"), RegistrazioneDocumento.NumeroRegistrazione)


                If Dd_CausaleContabile.SelectedValue = "" Then

                    Dim CausaleDocumento As New Cls_CausaleContabile

                    CausaleDocumento.Codice = RegistrazioneDocumento.CausaleContabile
                    CausaleDocumento.Leggi(Session("DC_TABELLE"), CausaleDocumento.Codice)


                    If CausaleDocumento.CausaleIncasso = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaErrore('Documento senza causale di incasso non posso procedere');", True)
                        Exit Sub
                    End If

                    Registrazione.CausaleContabile = CausaleDocumento.CausaleIncasso
                Else
                    Registrazione.CausaleContabile = Dd_CausaleContabile.SelectedValue
                End If



                Registrazione.CentroServizio = RegistrazioneDocumento.CentroServizio
                Registrazione.DataRegistrazione = Txt_DataIncasso.Text
                Registrazione.Descrizione = "Incasso Rid - " & Tabella.Rows(Riga).Item("NumeroProtocollo").ToString & " " & Tabella.Rows(Riga).Item("AnnoProtocollo").ToString

                Dim CausaleContabileDocumento As New Cls_CausaleContabile

                CausaleContabileDocumento.Leggi(Session("DC_TABELLE"), RegistrazioneDocumento.CausaleContabile)


                Dim CausaleContabile As New Cls_CausaleContabile

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                If IsNothing(Registrazione.Righe(0)) Then
                    Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
                End If

                Registrazione.Righe(0).MastroPartita = RegistrazioneDocumento.Righe(0).MastroPartita
                Registrazione.Righe(0).ContoPartita = RegistrazioneDocumento.Righe(0).ContoPartita
                Registrazione.Righe(0).SottocontoPartita = RegistrazioneDocumento.Righe(0).SottocontoPartita
                Registrazione.Righe(0).Importo = Math.Abs(ImportoRegistrazione)
                Registrazione.Righe(0).Descrizione = Registrazione.Descrizione
                Registrazione.Righe(0).Segno = "+"
                Registrazione.Righe(0).Tipo = "CF"
                Registrazione.Righe(0).RigaDaCausale = 1
                Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere

                If CausaleContabileDocumento.TipoDocumento = "NC" Then
                    If Registrazione.Righe(0).DareAvere = "D" Then
                        Registrazione.Righe(0).DareAvere = "A"
                    Else
                        Registrazione.Righe(0).DareAvere = "D"
                    End If
                End If

                Registrazione.Righe(0).Utente = Session("UTENTE")

                If IsNothing(Registrazione.Righe(1)) Then
                    Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
                End If

                Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
                Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
                Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
                Registrazione.Righe(1).Importo = Math.Abs(ImportoRegistrazione)
                Registrazione.Righe(1).Descrizione = Registrazione.Descrizione
                Registrazione.Righe(1).Segno = "+"
                Registrazione.Righe(1).Tipo = ""
                Registrazione.Righe(1).RigaDaCausale = 2
                Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
                Registrazione.Righe(1).Utente = Session("UTENTE")

                If CausaleContabileDocumento.TipoDocumento = "NC" Then
                    If Registrazione.Righe(1).DareAvere = "D" Then
                        Registrazione.Righe(1).DareAvere = "A"
                    Else
                        Registrazione.Righe(1).DareAvere = "D"
                    End If
                End If

                Registrazione.Scrivi(Session("DC_GENERALE"), 0)

                Dim Legame As New Cls_Legami

                Legame.NumeroDocumento(0) = Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString)
                Legame.NumeroPagamento(0) = Registrazione.NumeroRegistrazione
                Legame.Importo(0) = Math.Abs(ImportoRegistrazione)
                Legame.Scrivi(Session("DC_GENERALE"), Val(Tabella.Rows(Riga).Item("NumeroRegistrazione").ToString), Registrazione.NumeroRegistrazione)
                'NumeroRegistrazioneIncasso
                Tabella.Rows(Riga).Item("NumeroRegistrazioneIncasso") = Registrazione.NumeroRegistrazione
            Else
                CheckBox.Checked = False
            End If
        Next


        Dim ErroriNonCongruita As String = ""

        For Riga = GridView1.Rows.Count - 1 To 0 Step -1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & Tabella.Rows(Riga).Item("NumeroRegistrazioneIncasso") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("NumeroRegistrazione") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("NumeroProtocollo") & "/" & Tabella.Rows(Riga).Item("AnnoProtocollo") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("CognomeNome") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("Iban") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("CodiceFiscale") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("DataMandato") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("IdMandato") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;"">" & Tabella.Rows(Riga).Item("ImportoTotale") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "</tr>"
            End If
        Next

        ErroriNonCongruita = ErroriNonCongruita & "<tr><td></td><td></td><td></td><td><b>" & DD_Banca.SelectedItem.Text & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>NUMERO INCASSI CREATI</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>" & AppoggioNumero & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>PER UN TOTALE DI: </b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;""><b>" & Format(AppoggioTotale, "#,##0.00") & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "</tr>"


        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Session("NOMEFILE") = NomeFile


        Session("CSVFILE") = Nothing


        Session("ERRORIIMPORT") = ErroriNonCongruita

        'Btn_Modifica
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {    DialogBoxx('ReportRidCreati.aspx?INCASSI=SI');  });", True)

        Btn_Dowload.Enabled = False

    End Sub






    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick

    End Sub

    Protected Sub Btn_Dowload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Dowload.Click

    End Sub
End Class
