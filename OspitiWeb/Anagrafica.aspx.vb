﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util


Partial Class _Default
    Inherits System.Web.UI.Page



    Private Sub DisabilitaCache()

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.Headers.Add("Cache-Control", "no-cache, no-store")


        Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1))
    End Sub


    Private Function ModificaOspite() As Boolean

        ModificaOspite = True
        Try

            LoginPersonam()

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim JSon As String
            Dim Genere As Long

            Genere = 1
            If RB_Maschile.Checked = True Then
                Genere = 1
            End If
            If RB_Femminile.Checked = True Then
                Genere = 2
            End If

            JSon = "{""surname"":""" & Txt_Cognome.Text & ""","            
            JSon = JSon & """firstname"":""" & Txt_Nome.Text & ""","
            JSon = JSon & """gender"":""" & Genere & ""","
            JSon = JSon & """cf"":""" & Txt_CodiceFiscale.Text & ""","
            JSon = JSon & """birthdate"":""" & Txt_DataNascita.Text & """}"
            'birthcity_id ...


            Data = System.Text.Encoding.ASCII.GetBytes(JSon)


            Dim M As New ClsOspite

            M.CodiceOspite = Session("CODICEOSPITE")
            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)



            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim Cs As New Cls_CentroServizio

            Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)

            '& M.CODICEFISCALE
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & Cs.EPersonam & "/guests/" & M.CODICEFISCALE)

            client.Method = "PUT"
            client.Headers.Add("Authorization", "Bearer " & Session("access_token"))
            client.ContentType = "Content-Type: application/json"

            client.ContentLength = Data.Length

            Dim dataStream As Stream = client.GetRequestStream()


            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()

            Dim response As HttpWebResponse = client.GetResponse()

            Dim respStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(respStream)
            Dim appoggio As String
            appoggio = reader.ReadToEnd()


        Catch ex As Exception
            ModificaOspite = False
        End Try

    End Function
    Private Sub LoginPersonam()
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")


        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")


        Dim guarda As String


        If Trim(Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler
        'curl -i -k https://api-v0.e-personam.com/v0/oauth/token -F grant_type=password  -F client_id=98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d -F client_secret=5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f -F username=paolo -F password=cristina

        'Dim responsemia As System.Net.WebResponse
        'responsemia = request.GetResponse()


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()


        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Session("access_token") = s.access_token


    End Sub
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class
    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Sub CaricaPagina()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        Imd_FotoOspite.ImageUrl = "../images/nofoto.jpg"
        Try
            If x.PathImmagine.Trim <> "" Then
                Imd_FotoOspite.ImageUrl = "../" & x.PathImmagine
            End If
        Catch ex As Exception

        End Try
        
        Txt_Cognome.Text = x.CognomeOspite
        Txt_Nome.Text = x.NomeOspite

        If x.Sesso = "F" Then
            RB_Femminile.Checked = True
        End If
        If x.Sesso = "M" Then
            RB_Maschile.Checked = True
        End If

        Lbl_DatiContatto.Text = ""
        If x.RESIDENZATELEFONO1 <> "" Then
            Lbl_DatiContatto.Text = "<i class=""fa fa-phone""></i> " & x.RESIDENZATELEFONO1
        End If


        If x.RESIDENZATELEFONO2 <> "" Then
            Lbl_Cellulare.Text = "<i class=""fa fa-mobile""></i> " & x.RESIDENZATELEFONO2
        End If

        If x.RESIDENZATELEFONO3 <> "" Then
            'Lbl_Mail.Text = " <i class=""fa fa-envelope-o""></i> <a href=""mailto:" & x.RESIDENZATELEFONO3 & """ >" & x.RESIDENZATELEFONO3 & "<a/>"
            Lbl_Mail.Text = " <i class=""fa fa-envelope-o""></i> " & x.RESIDENZATELEFONO3
        End If

        If x.TELEFONO1 <> "" Then
            Lbl_Fax.Text = "<i class=""fa fa-fax""></i> " & x.TELEFONO1
        End If

        If x.CodiceMedico <> "" Then
            Dim Med As New Cls_Medici

            Med.CodiceMedico = x.CodiceMedico
            Med.Leggi(Session("DC_OSPITE"))

            Lbl_Medico.Text = Med.Nome & "<br/><label class=""LabelCampo"">&nbsp;</label>" & Med.Telefono1 & " " & Med.Telefono2

        End If
        Dim Com As New ClsComune

        Com.Comune = x.ComuneDiNascita
        Com.Provincia = x.ProvinciaDiNascita
        Com.DecodficaComune(ConnectionString)
        Txt_ComuneNascita.Text = Com.Descrizione

        d.Comune = x.ComuneDiNascita
        d.Provincia = x.ProvinciaDiNascita
        d.DecodficaComune(ConnectionString)
        Txt_ComuneNascita.Text = Trim(d.Provincia & " " & d.Comune & " " & d.Descrizione)

        Txt_MotivoNonInUso.Text = x.Vincolo
        Txt_DataNascita.Text = x.DataNascita

        Txt_CodiceFiscale.Text = x.CODICEFISCALE

        If Format(x.DataDomanda, "dd/MM/yyyy") <> "01/01/0001" Then
            Txt_DataDomanda.Text = Format(x.DataDomanda, "dd/MM/yyyy")
        Else
            Txt_DataDomanda.Text = ""
        End If
        Dim CentroServizio As String
        Dim CM As New Cls_Movimenti

        CM.UltimaData(ConnectionString, Session("CODICEOSPITE"))
        CentroServizio = CM.CENTROSERVIZIO

        Dim xCM As New Cls_ImportoOspite
        Dim Appoggio As String = ""


        xCM.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        If xCM.Tipo = "G" Then
            Appoggio = "Giornaliera"
        End If
        If xCM.Tipo = "M" Then
            Appoggio = "Mensile"
        End If
        If xCM.Tipo = "A" Then
            Appoggio = "Annuale"
        End If

        If x.NonInUso = "S" Then
            Chk_NonInUso.Checked = True
        Else
            Chk_NonInUso.Checked = False
        End If



        If Year(xCM.Data) = 1 Then
            Lbl_ImportoOspite.Text = "&nbsp;"
        Else
            If xCM.Importo_2 > 0 Then
                Lbl_ImportoOspite.Text = Format(xCM.Data, "dd/MM/yyyy") & " " & Format(xCM.Importo, "#,##0.00") & " - " & Format(xCM.Importo_2, "#,##0.00") & " " & Appoggio
            Else
                Lbl_ImportoOspite.Text = Format(xCM.Data, "dd/MM/yyyy") & " " & Format(xCM.Importo, "#,##0.00") & " " & Appoggio
            End If
        End If

        Dim xCM1 As New Cls_StatoAuto

        xCM1.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        Dim xUsl As New ClsUSL

        xUsl.CodiceRegione = xCM1.USL
        xUsl.DecodificaRegione(ConnectionString, xCM1.USL)

        If xCM1.USL <> "" And xUsl.Nome <> "" Then
            xUsl.Leggi(ConnectionString)
        End If
        Dim Importo As String

        Importo = ""
        If xUsl.Nome <> "" Then
            Dim UltDataUsl As New Cls_ImportoRegione

            UltDataUsl.UltimaData(ConnectionString, xCM1.USL, xCM1.TipoRetta)

            Dim MTipoRetta As New Cls_TabellaTipoImportoRegione

            MTipoRetta.Codice = xCM1.TipoRetta
            MTipoRetta.Leggi(Session("DC_OSPITE"), xCM1.TipoRetta)
            If IsNothing(MTipoRetta.Descrizione) Then
                Importo = " " & Format(UltDataUsl.Importo, "#,##0.00") & " Euro "
            Else
                If MTipoRetta.Descrizione.Length > 3 Then
                    Importo = " <BR/> " & MTipoRetta.Descrizione & " " & Format(UltDataUsl.Importo, "#,##0.00") & " Euro "
                Else
                    Importo = MTipoRetta.Descrizione & " " & Format(UltDataUsl.Importo, "#,##0.00") & " Euro "
                End If
            End If


        End If

        If Year(xCM1.Data) = 1 Then
            Lbl_StatoAuto.Text = "&nbsp;"
        Else
            If xCM1.STATOAUTO = "N" Then
                Lbl_StatoAuto.Text = Format(xCM1.Data, "dd/MM/yyyy") & " - Non Autosufficiente <BR/>" & xUsl.Nome & Importo
            Else
                Lbl_StatoAuto.Text = Format(xCM1.Data, "dd/MM/yyyy") & " - Autosufficiente <BR/>" & xUsl.Nome & Importo
            End If
        End If


        Dim xLs As New Cls_rettatotale

        Appoggio = ""
        xLs.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        If xLs.Tipo = "G" Then
            Appoggio = "Giornaliera"
        End If
        If xLs.Tipo = "M" Then
            Appoggio = "Mensile"
        End If
        If xLs.Tipo = "A" Then
            Appoggio = "Annuale"
        End If

        Dim tiporetta As New Cls_TipoRetta

        tiporetta.Codice = xLs.TipoRetta
        tiporetta.Leggi(ConnectionString, tiporetta.Codice)



        If Year(xLs.Data) = 1 Then
            Lbl_RettaTotale.Text = "&nbsp;"
        Else
            Lbl_RettaTotale.Text = Format(xLs.Data, "dd/MM/yyyy") & " " & Format(xLs.Importo, "#,##0.00") & " " & Appoggio
            If tiporetta.Descrizione <> "" Then
                Lbl_RettaTotale.Text = Lbl_RettaTotale.Text & "<i style=""font-size:small;"">(" & tiporetta.Descrizione & ")</i>"
            End If
            Dim KExtra As New Cls_ExtraFisso

            KExtra.CODICEOSPITE = Session("CODICEOSPITE")
            KExtra.CENTROSERVIZIO = Session("CODICESERVIZIO")
            KExtra.Data = xLs.Data
            If KExtra.ElencaExtrafissiTesto(ConnectionString) <> "" Then
                Lbl_RettaTotale.Text = Lbl_RettaTotale.Text & "<i style=""font-size:small;"">(" & KExtra.ElencaExtrafissiTesto(ConnectionString) & ")</i>"
            End If

        End If

        Dim xMod As New Cls_Modalita

        xMod.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        Appoggio = ""
        If xMod.MODALITA = "O" Then
            Appoggio = "Differenza Ospite"
        End If
        If xMod.MODALITA = "P" Then
            Appoggio = "Differenza Parenti"
        End If
        If xMod.MODALITA = "C" Then
            Appoggio = "Differenza Comune"
        End If
        If xMod.MODALITA = "E" Then
            Appoggio = "Differenza Ente"
        End If

        If Year(xMod.Data) = 1 Then
            Lbl_Modalita.Text = "&nbsp;"
        Else
            Lbl_Modalita.Text = Format(xMod.Data, "dd/MM/yyyy") & " " & Appoggio
        End If

        Dim ko As New Cls_ImportoComune

        ko.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        Dim DeCo As New ClsComune

        DeCo.Comune = ko.COMUNE
        DeCo.Provincia = ko.PROV
        DeCo.Descrizione = ""
        DeCo.DecodficaComune(ConnectionString)

        If DeCo.Descrizione <> "" And ko.COMUNE <> "" And ko.PROV <> "" Then
            DeCo.Leggi(ConnectionString)
        End If

        If Year(ko.Data) = 1 Then
            Lbl_ImportoComune.Text = "&nbsp;"
        Else
            Lbl_ImportoComune.Text = Format(ko.Data, "dd/MM/yyyy") & " " & Format(ko.Importo, "#,##0.00") & " " & DeCo.Descrizione
        End If



        Lbl_Riferimenti.Text = "<table><tr><td></td><td></td></tr> <tr><td colspan=""2"" style=""background-color:#aad4ff;"">" & xUsl.Nome & "</td></tr><tr><td>Recapito</td><td>" & xUsl.RecapitoNome & "</td></tr><tr><td>Telefono</td><td>" & xUsl.RESIDENZATELEFONO4 & "</td></tr> <tr><td colspan=""2"" style=""background-color:#aad4ff;"">" & DeCo.Descrizione & "</td></tr> <tr><td>Recapito</td><td>" & DeCo.RecapitoNome & "</td></tr><td>Telefono</td><td>" & DeCo.RESIDENZATELEFONO4 & "</td></tr></table>"


        Dim ko1 As New Cls_ImportoJolly

        ko1.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        Dim DeCo1 As New ClsComune

        DeCo1.Comune = ko1.COMUNE
        DeCo1.Provincia = ko1.PROV
        DeCo1.DecodficaComune(ConnectionString)


        If Year(ko1.Data) = 1 Then
            Lbl_ImportoJolly.Text = "&nbsp;"
        Else
            Lbl_ImportoJolly.Text = Format(ko1.Data, "dd/MM/yyyy") & " " & Format(ko1.Importo, "#,##0.00") & " " & DeCo1.Descrizione
        End If


        Lbl_Parenti.Text = "<table style=""width: 900px"">"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Cognome Nome</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Telefono</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Cellulare</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Mail</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Grado Parentela</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td class=""HeaderGriglia"" style=""height: 24px""><strong>Quota</strong></td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "</tr>"


        Dim xsParenti As New Cls_Parenti
        Dim DecodificaTabelle As New Cls_Tabelle


        Dim XCalc As New Cls_CalcoloRette

        XCalc.STRINGACONNESSIONEDB = Session("DC_OSPITE")


        XCalc.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 1)

        Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"

        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=1"">" & xsParenti.Nome & "</td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"

        DecodificaTabelle.TipTab = "GPA"
        DecodificaTabelle.Codice = xsParenti.GradoParentela
        DecodificaTabelle.Leggi(Session("DC_OSPITE"))
        Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"

        If XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", 1, Now) > 0 Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", 1, Now), "#,##0.00") & " " & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", 1, Now), "#,##0.00") & "</td>"
        Else
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 1, Now), "#,##0.00") & "</td>"
        End If

        Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"

        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 2)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=2"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 2, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 3)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=3"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 3, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 4)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=4"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 4, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 5)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=5"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 5, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 6)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=6"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 6, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 7)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=7"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"
            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 7, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""

        xsParenti.Leggi(ConnectionString, Session("CODICEOSPITE"), 8)
        If xsParenti.Nome <> "" Then
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a style=""text-decoration: none; color: black;"" href=""Parenti.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=8"">" & xsParenti.Nome & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono1 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & xsParenti.Telefono2 & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;""><a href=""mailto:" & xsParenti.Telefono3 & """>" & xsParenti.Telefono3 & "</a></td>"

            DecodificaTabelle.TipTab = "GPA"
            DecodificaTabelle.Codice = xsParenti.GradoParentela
            DecodificaTabelle.Leggi(Session("DC_OSPITE"))
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & DecodificaTabelle.Descrizione & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<td style=""background-color:#cccccc;"">" & Format(XCalc.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 8, Now), "#,##0.00") & "</td>"
            Lbl_Parenti.Text = Lbl_Parenti.Text & "<tr>"
        End If
        xsParenti.Nome = ""
        Lbl_Parenti.Text = Lbl_Parenti.Text & "</table>"
        XCalc.ChiudiDB()

        Dim kImp As New Cls_Impegnativa

        kImp.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        If Year(kImp.DATAINIZIO) = 1 Then
            Lbl_Impegnativa.Text = "&nbsp;"
        Else
            Lbl_Impegnativa.Text = Format(kImp.DATAINIZIO, "dd/MM/yyyy") & " " & Format(kImp.DATAFINE, "dd/MM/yyyy") & " " & kImp.DESCRIZIONE
        End If


        Dim kIse As New Cls_Ise

        kIse.UltimaData(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

        If Year(kIse.DATAINIZIO) = 1 Then
            Lbl_DatiIse.Text = "&nbsp;"
        Else
            Lbl_DatiIse.Text = Format(kIse.DATAINIZIO, "dd/MM/yyyy") & " " & Format(kIse.DATAFINE, "dd/MM/yyyy") & " " & kIse.DESCRIZIONE
        End If



        Dim DAcc As New Cls_Movimenti
        DAcc.CodiceOspite = Session("CODICEOSPITE")
        DAcc.CENTROSERVIZIO = Session("CODICESERVIZIO")
        DAcc.UltimaDataAccoglimento(Session("DC_OSPITE"))
        Txt_DataAccoglimento.Text = Format(DAcc.Data, "dd/MM/yyyy")

        DAcc.Data = Nothing
        DAcc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

        If DAcc.Data = Nothing Then
            Txt_DataUscitaDefinitiva.Text = ""
        Else
            Txt_DataUscitaDefinitiva.Text = Format(DAcc.Data, "dd/MM/yyyy")
        End If



        Dim KPag As New Cls_DatiPagamento


        KPag.CodiceOspite = Session("CODICEOSPITE")
        KPag.CodiceParente = 0
        KPag.LeggiUltimaData(Session("DC_OSPITE"))

        If KPag.Banca <> "" Or KPag.Cin <> "" Then
            Dim CCAp As String = KPag.CCBancario

            If Len(CCAp) = 11 Then
                CCAp = "0" & CCAp
            End If
            If Len(CCAp) = 10 Then
                CCAp = "00" & CCAp
            End If
            If Len(CCAp) = 9 Then
                CCAp = "000" & CCAp
            End If
            If Len(CCAp) = 8 Then
                CCAp = "0000" & CCAp
            End If

            Lbl_DatiPagamento.Text = "Iban " & KPag.CodiceInt & Format(Val(KPag.NumeroControllo), "00") & KPag.Cin & Format(Val(KPag.Abi), "00000") & Format(Val(KPag.Cab), "00000") & CCAp
            If KPag.IntestatarioConto <> "" Then
                Lbl_DatiPagamento.Text = Lbl_DatiPagamento.Text & "<br/> Intestato : " & KPag.IntestatarioConto
            End If
        End If


        Dim KPagP1 As New Cls_DatiPagamento
        Dim XInd As Integer

        For XInd = 1 To 10

            KPagP1.Banca = ""
            KPagP1.Cin = ""
            KPagP1.CodiceOspite = Session("CODICEOSPITE")
            KPagP1.CodiceParente = XInd
            KPagP1.LeggiUltimaData(Session("DC_OSPITE"))

            If KPagP1.Banca <> "" Or KPagP1.Cin <> "" Then
                Lbl_DatiPagamento.Text = Lbl_DatiPagamento.Text & "(P) Iban " & KPagP1.CodiceInt & Format(Val(KPagP1.NumeroControllo), "00") & KPagP1.Cin & Format(Val(KPagP1.Abi), "00000") & Format(Val(KPagP1.Cab), "00000") & Format(Val(KPagP1.CCBancario), "000000000000")
                If KPagP1.IntestatarioConto <> "" Then
                    Lbl_DatiPagamento.Text = Lbl_DatiPagamento.Text & "<br/> Intestato : " & KPagP1.IntestatarioConto
                End If
            End If
        Next



        Dim VerCf As New Cls_CodiceFiscale

        Dim Verifica As Boolean = False


        Try
            Verifica = VerCf.Check_CodiceFiscale(Txt_CodiceFiscale.Text)
        Catch ex As Exception

        End Try


        If Verifica = False Then
            Txt_CodiceFiscale.BackColor = Drawing.Color.Red
        Else
            VerCf.Cognome = Txt_Cognome.Text
            VerCf.Nome = Txt_Nome.Text
            VerCf.DataNascita = Txt_DataNascita.Text
            VerCf.Comune = x.ComuneDiNascita
            VerCf.Provincia = x.ProvinciaDiNascita

            If RB_Femminile.Checked = True Then
                VerCf.Sesso = "F"
            Else
                VerCf.Sesso = "M"
            End If


            Dim AppoggioCF As String

            VerCf.StringaConnessione = Session("DC_OSPITE")
            AppoggioCF = VerCf.Make_CodiceFiscale()


            Dim TestComune As New ClsComune

            TestComune.Provincia = x.ProvinciaDiNascita
            TestComune.Comune = x.ComuneDiNascita
            TestComune.Leggi(Session("DC_OSPITE"))


            If TestComune.CODXCODF <> "" Then
                If Mid(AppoggioCF.ToUpper & Space(16), 12, 4) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 12, 4) Then
                    Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
                End If
            End If

            If Mid(AppoggioCF.ToUpper & Space(16), 1, 6) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 1, 6) Then
                Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
            End If

            If IsDate(Txt_DataNascita.Text) Then
                If Mid(AppoggioCF.ToUpper & Space(16), 7, 5) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 7, 5) Then
                    Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
                End If
            End If
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<STATISTICHE>") >= 0 Then
            Response.Redirect("Menu_Visualizzazioni.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
            Exit Sub
        End If





        Dim MyJS As String


        MyJS = "$('#" & Txt_DataNascita.ClientID & "').mask(""99/99/9999"");"
        '{ changeMonth: true,changeYear: true},
        MyJS = MyJS & "$('#" & Txt_DataNascita.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJS = MyJS & "$('#" & Txt_DataDomanda.ClientID & "').mask(""99/99/9999"");"
        MyJS = MyJS & "$('#" & Txt_DataDomanda.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"


        MyJS = MyJS & "$('#" & Txt_DataAccoglimento.ClientID & "').mask(""99/99/9999"");"


        MyJS = MyJS & "$(" & Chr(34) & "#" & Txt_ComuneNascita.ClientID & Chr(34) & ").autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Anagrafica", MyJS, True)

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = False Then
            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")
            'DisabilitaCache()

            Call CaricaPagina()

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception
                    K1 = Nothing
                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            If Not IsNothing(Session("ABILITAZIONI")) Then
                If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                    Btn_Elimina.Visible = False
                End If
            End If


            Dim x1 As New ClsOspite

            x1.CodiceOspite = Session("CODICEOSPITE")
            x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)


            If Not IsNothing(Session("ABILITAZIONI")) Then
                If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 And x1.IdEpersonam > 0 Then
                    Txt_ComuneNascita.Enabled = False
                    Txt_Nome.Enabled = False
                    Txt_Cognome.Enabled = False
                    Txt_DataNascita.Enabled = False
                    RB_Femminile.Enabled = False
                    RB_Maschile.Enabled = False
                    Txt_CodiceFiscale.Enabled = False
                End If
            End If

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"

        End If
        Lbl_Errori.Text = ""
    End Sub

    Protected Sub Modifica()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))
        x.CodiceOspite = Session("CODICEOSPITE")

        x.Nome = Txt_Cognome.Text & " " & Txt_Nome.Text
        x.CognomeOspite = Txt_Cognome.Text
        x.NomeOspite = Txt_Nome.Text

        x.Vincolo = Txt_MotivoNonInUso.Text
        x.UTENTE = Session("UTENTE")
        x.DATAAGGIORNAMENTO = Now
        If IsDate(Txt_DataNascita.Text) Then
            x.DataNascita = Txt_DataNascita.Text
        Else
            x.DataNascita = Nothing
        End If

        If RB_Femminile.Checked = True Then
            x.Sesso = "F"
        End If

        If RB_Maschile.Checked = True Then
            x.Sesso = "M"
        End If


        If Txt_ComuneNascita.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_ComuneNascita.Text)

            If Vettore.Length >= 3 Then

                x.ProvinciaDiNascita = Vettore(0)
                x.ComuneDiNascita = Vettore(1)
            End If
        End If


        x.CODICEFISCALE = Txt_CodiceFiscale.Text
        If IsDate(Txt_DataDomanda.Text) Then
            x.DataDomanda = Txt_DataDomanda.Text
        Else
            x.DataDomanda = Nothing
        End If

        If Chk_NonInUso.Checked = True Then
            x.NonInUso = "S"
        Else
            x.NonInUso = ""
        End If

        x.ScriviOspite(ConnectionString)

        
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Modifica", "alert('Modifica Effettuata');", True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Cognome.Text.Trim() = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare nome e cognome');", True)
            Exit Sub
        End If
        If Txt_Nome.Text.Trim() = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare nome e cognome');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataNascita.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data nascita formalmente errata');", True)
            Exit Sub
        End If
        If Txt_DataDomanda.Text <> "" Then
            If Not IsDate(Txt_DataDomanda.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data domanda formalmente errata');", True)
                Exit Sub
            End If
        End If

        If Chk_NonInUso.Checked = False And Txt_MotivoNonInUso.Text.Trim <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il flag non inuso,se indichi un motivo');", True)
            Exit Sub
        End If

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")


        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                'If Not ModificaOspite() Then
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                '    Exit Sub
                'End If
            End If
        End If

        Call Modifica()
    End Sub

  

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim cn As OleDbConnection


        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where " & _
                           "CodiceOspite = ? ")
        cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read() Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono movimenti non posso eliminare');", True)
            myPOSTreader.Close()
            cn.Close()
            Exit Sub
        End If
        cn.Close()


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "D", "OSPITE", "")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))

        x.Elimina(ConnectionString, Session("CODICEOSPITE"))

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub

    Protected Sub Txt_CodiceFiscale_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_CodiceFiscale.TextChanged

    End Sub



    Private Sub ForzaLogin()

        Dim k As New Cls_Login


        Session("TIPOAPP") = "RSA"


        k.Utente = Request.Form("Txt_Login")
        k.Chiave = Request.Form("Txt_Enc_Password")

        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.Leggi(Application("SENIOR"))



        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""



        If k.Ospiti <> "" Then
            Session("UTENTE") = k.Utente
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr
            Session("ABILITAZIONI") = k.ABILITAZIONI

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                Response.Redirect("MainPortineria.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                Exit Sub
            End If
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub


End Class

