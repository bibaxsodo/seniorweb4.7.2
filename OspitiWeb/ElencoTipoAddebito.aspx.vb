﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_ElencoTipoAddebito
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From TabTipoAddebito Order By Descrizione"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Struttura/Servizio", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Iva", GetType(String))
        Tabella.Columns.Add("Conto Contabilità", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Scadenza", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Codice")


            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            If kVilla.Descrizione <> "" Then
                myriga(1) = kVilla.Descrizione
                If Cserv.DESCRIZIONE <> "" Then
                    myriga(1) = kVilla.Descrizione & " - " & Cserv.DESCRIZIONE
                End If
            Else
                myriga(1) = Cserv.DESCRIZIONE
            End If





            myriga(2) = myPOSTreader.Item("Descrizione")

            Dim M As New Cls_IVA

            M.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
            M.Leggi(Session("DC_TABELLE"), M.Codice)

            myriga(3) = M.Descrizione


            Dim MPianoConti As New Cls_Pianodeiconti

            MPianoConti.Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            MPianoConti.Conto = Val(campodb(myPOSTreader.Item("Conto")))
            MPianoConti.Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
            MPianoConti.Decodfica(Session("DC_GENERALE"))
            If MPianoConti.Mastro > 0 Then
                myriga(4) = MPianoConti.Descrizione & "(" & MPianoConti.Mastro & "." & MPianoConti.Conto & "." & MPianoConti.Sottoconto & ")"
            End If

            myriga(5) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")
            myriga(6) = campodb(myPOSTreader.Item("Scadenza"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("TipoAddebito.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub


    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("TipoAddebito.aspx")
    End Sub

    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand



       
        cmd.CommandText = "Select Top 100 * From TabTipoAddebito Order By Codice"


        cmd.Connection = cn
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice")
        Tabella.Columns.Add("Descrizione")
        Tabella.Columns.Add("CodiceIVA")
        Tabella.Columns.Add("Mastro")
        Tabella.Columns.Add("Conto")
        Tabella.Columns.Add("Sottoconto")
        Tabella.Columns.Add("Decodifica")
        Tabella.Columns.Add("CENTROSERVIZIO")
        Tabella.Columns.Add("Struttura")
        Tabella.Columns.Add("Importo")

        Tabella.Columns.Add("DataScadenza")


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("Codice"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            Dim CSIva As New Cls_IVA


            CSIva.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
            CSIva.Leggi(Session("DC_TABELLE"), CSIva.Codice)

            myriga(2) = CSIva.Descrizione


            myriga(3) = Val(campodb(myPOSTreader.Item("Mastro")))
            myriga(4) = Val(campodb(myPOSTreader.Item("Conto")))
            myriga(5) = Val(campodb(myPOSTreader.Item("SottoConto")))
            Dim Ms As New Cls_Pianodeiconti

            Ms.Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Ms.Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Ms.Sottoconto = Val(campodb(myPOSTreader.Item("SottoConto")))
            Ms.Decodfica(Session("DC_GENERALE"))
            myriga(6) = Ms.Descrizione

            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            myriga(7) = Cserv.DESCRIZIONE

            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            myriga(8) = kVilla.Descrizione

            myriga(9) = campodbn(myPOSTreader.Item("importo"))


            myriga(10) = campodb(myPOSTreader.Item("Scadenza"))


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        If Txt_Descrizione.Text.Trim = "" Then
            cmd.CommandText = "Select * From TabTipoAddebito Order By Descrizione"
        Else
            cmd.CommandText = "Select * From TabTipoAddebito WHERE DESCRIZIONE LIKE ? Order By Descrizione"
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        End If
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Struttura/Servizio", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Iva", GetType(String))
        Tabella.Columns.Add("Conto Contabilità", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Scadenza", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Codice")


            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            If kVilla.Descrizione <> "" Then
                myriga(1) = kVilla.Descrizione
                If Cserv.DESCRIZIONE <> "" Then
                    myriga(1) = kVilla.Descrizione & " - " & Cserv.DESCRIZIONE
                End If
            Else
                myriga(1) = Cserv.DESCRIZIONE
            End If





            myriga(2) = myPOSTreader.Item("Descrizione")

            Dim M As New Cls_IVA

            M.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
            M.Leggi(Session("DC_TABELLE"), M.Codice)

            myriga(3) = M.Descrizione


            Dim MPianoConti As New Cls_Pianodeiconti

            MPianoConti.Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            MPianoConti.Conto = Val(campodb(myPOSTreader.Item("Conto")))
            MPianoConti.Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
            MPianoConti.Decodfica(Session("DC_GENERALE"))
            If MPianoConti.Mastro > 0 Then
                myriga(4) = MPianoConti.Descrizione & "(" & MPianoConti.Mastro & "." & MPianoConti.Conto & "." & MPianoConti.Sottoconto & ")"
            End If

            myriga(5) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")
            myriga(6) = campodb(myPOSTreader.Item("Scadenza"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub
End Class
