﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Parenti" EnableEventValidation="false" ValidateRequest="false" CodeFile="Parenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Anagrafica Parente</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        var windowObjectReference;

        function openRequestedPopup() {

            windowObjectReference = window.open("PdfDaHtml.aspx",
                "DescriptiveWindowName",
                "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");

        }
        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }
        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');
            $("#pippo").css('visibility', 'visible');
            $("#Txt_DataAcco").mask("99/99/9999");
            $("#Txt_DataAcco").datepicker({ changeMonth: true, changeYear: true, yearRange: "1890:2025" }, $.datepicker.regional["it"]);
        }
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }


            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });


    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 50%;
            height: 350px;
            top: 30%;
            left: 30%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica Parente</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" OnClick="Btn_Modifica_Click" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                        <br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica Parente        
                 
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <br />
                                    <br />

                                    <label class="LabelCampo">Cognome :</label>
                                    <asp:TextBox ID="Txt_CognomeNome" MaxLength="30" runat="server" Width="350px"></asp:TextBox>


                                    <br />
                                    <br />

                                    <label class="LabelCampo">Nome :</label>
                                    <asp:TextBox ID="Txt_Nome" MaxLength="30" runat="server" Width="350px"></asp:TextBox>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">Sesso :</label>
                                    <asp:RadioButton ID="RB_Maschile" runat="server" GroupName="sesso" />
                                    Maschile
 <asp:RadioButton ID="RB_Femminile" runat="server" GroupName="sesso" />
                                    Femminile<br />
                                    <br />

                                    <label class="LabelCampo">Data Nascita :</label>
                                    <asp:TextBox ID="Txt_DataNascita" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Comune nascita :</label>
                                    <asp:TextBox ID="Txt_Comune" runat="server" MaxLength="40" Width="350px"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">Codice Fiscale :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" MaxLength="16" runat="server" Width="200px"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" BackColor="#33CCCC" Text="Calcola" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" MaxLength="30" runat="server" Width="120px"></asp:TextBox>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">Cellulare :</label>
                                    <asp:TextBox ID="Txt_Telefono2" MaxLength="30" runat="server" Width="120px"></asp:TextBox>

                                    <input id="login_exchange" type="text" name="login" size="1" width="1" height="" autocomplete="off" style="z-index: -1000; height: 1px; width: 1px; border: none; position: absolute;">
                                    <input id="pswd_exchange" type="password" name="pswd" size="1" autocomplete="off" style="z-index: -1000; height: 1px; width: 1px; border: none; position: absolute;">
                                    <br />
                                    <br />
                                    <label class="LabelCampo">E-Mail :</label>
                                    <asp:TextBox ID="Txt_Telefono3" MaxLength="50" runat="server" Text="" Width="350px"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Fax :</label>
                                    <asp:TextBox ID="Txt_Telefono4_Mod" MaxLength="50" Text="" runat="server" Width="120px"></asp:TextBox>



                                    <br />
                                    <br />

                                    <label class="LabelCampo">Grado Parentela :</label>
                                    <asp:DropDownList ID="DD_Parentela" runat="server" Width="144px"></asp:DropDownList>





                                    <br />
                                    <br />


                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" MaxLength="100" runat="server" Width="350px"></asp:TextBox>





                                    <br />
                                    <br />

                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="120px"></asp:TextBox>


                                    <br />
                                    <br />


                                    <label class="LabelCampo">Comune residenza :</label>
                                    <asp:TextBox ID="Txt_ComRes" MaxLength="100" runat="server" Width="350px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Destinatario ft. ospite :</label>
                                    <asp:CheckBox ID="Chk_ParenteIndirizzo" runat="server" Text="" />

                                    <br />
                                    <br />
                                    <asp:Button ID="Btn_RecuperaEperesonam" runat="server" Text="Recupera da Epersonam" Visible="false" />
                                    <br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Dati Economici
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <p style="text-align: right;">
                                                <a href="#" onclick="VisualizzaDivRegistrazione();">
                                                    <img src="../images/iconabilitazioni.png" /></a>
                                            </p>
                                            <label class="LabelCampo">Intestatario  :</label>
                                            <asp:CheckBox ID="Chk_Intestatario" runat="server" Text="" />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Tipo Operazione :</label>
                                            <asp:DropDownList ID="DD_TipoOperazione" runat="server" Width="450px"></asp:DropDownList>


                                            <br />
                                            <br />

                                            <label class="LabelCampo">IVA :</label>
                                            <asp:DropDownList ID="DD_IVA" runat="server" Width="450px"></asp:DropDownList>





                                            <br />
                                            <br />

                                            <label class="LabelCampo">Anticipata :</label>
                                            <asp:CheckBox ID="Chk_Anticipata" runat="server" Text="" />

                                            <br />
                                            <br />
                                            <label class="LabelCampo">Raggruppa Ospite :</label>
                                            <asp:CheckBox ID="Chk_RotturaOspite" runat="server" Text="" /><i>(Crea una sola fattura per ospite presente in più centri servizio )</i>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Modalità Pagamento :</label>
                                            <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="450px"></asp:DropDownList>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Tipo Sconto :</label>
                                            <asp:DropDownList ID="dd_TipoSconto" class="chosen-select" runat="server" Width="450px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Sconto :</label>
                                            <asp:TextBox ID="Txt_ImportoSconto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Periodo :</label>
                                            <asp:RadioButton ID="RB_Mensile" runat="server" GroupName="Periodo" Text="M" Checked="true" />
                                            <asp:RadioButton ID="RB_Bimestrale" runat="server" GroupName="Periodo" Text="B" />
                                            <asp:RadioButton ID="RB_Trimestrale" runat="server" GroupName="Periodo" Text="T" />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Compensazione :</label>
                                            <asp:RadioButton ID="RB_SI" runat="server" GroupName="compesazione" Text="SI" Checked="true" AutoPostBack="true" />
                                            <asp:RadioButton ID="RB_NO" runat="server" GroupName="compesazione" Text="NO" AutoPostBack="true" />
                                            <asp:CheckBox ID="Chk_ExtraFuoriFattura" runat="server" Text="Extra non in retta" AutoPostBack="true" />
                                            <asp:RadioButton ID="RB_Dettaglio" runat="server" GroupName="compesazione" Text="Compensa in base al tipo extra" AutoPostBack="true" />


                                            <br />
                                            <br />

                                            <label class="LabelCampo">Esportazione :</label>
                                            <asp:TextBox ID="Txt_Esportazione" MaxLength="20" runat="server" Width="100px"></asp:TextBox>
                                            - 
 <asp:TextBox ID="Txt_CodiceCup" MaxLength="15" runat="server" Width="100px"></asp:TextBox>





                                            <br />
                                            <br />

                                            <label class="LabelCampo">Conto Contabilità : </label>
                                            <asp:TextBox ID="Txt_Sottoconto" runat="server" Width="424px"></asp:TextBox>


                                            <br />
                                            <br />


                                            <label class="LabelCampo">Opposizione 730:</label>
                                            <asp:CheckBox ID="chk_Opposizione730" runat="server" Text="" /><br />
                                            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="352px"></asp:Label>
                                            <br />
                                            <br />
                                            <asp:GridView ID="Grd_ImportoParenti" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <FooterTemplate>
                                                            <div style="text-align: right">
                                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" /></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Data">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tipo">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Tipo" runat="server">
                                                                <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                                                <asp:ListItem Value="M">Mensile</asp:ListItem>
                                                                <asp:ListItem Value="A">Annuale</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Percentuale">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtPercentuale" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo_2">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto_2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo1">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto1" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo2">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo3">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto3" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo4">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto4" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Tipo Operazione">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_TipoOperazione" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DD_TipoOperazioneChange"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>


                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />

                                            </asp:GridView>


                                            <div id="blur" style="visibility: hidden;">&nbsp;</div>
                                            <div id="pippo" style="visibility: hidden;" class="wait">
                                                <div style="text-align: left; display: inline-table;">
                                                    <a href="#" onclick="Chiudi();">
                                                        <img src="../images/annulla.png" title="Chiudi" /></a>
                                                </div>
                                                <div style="text-align: right; float: right;">
                                                    <asp:ImageButton ID="Img_CreaAccoglimento" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                                                </div>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Indirizzo E-Mail :</label>
                                                <asp:TextBox ID="Txt_Utente" runat="server" Width="300px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Password :</label>
                                                <asp:TextBox ID="TxT_Password" TextMode="Password" Style="height: 25px;" runat="server" Width="120px"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Conferma Password :</label>
                                                <asp:TextBox ID="TxT_ConfermaPassword" TextMode="Password" Style="height: 25px;" runat="server" Width="120px"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Abilitazioni :</label>
                                                <asp:CheckBox ID="Chk_Diurno" runat="server" Text="Diurno" />&nbsp;
             <asp:CheckBox ID="Chk_Contabilita" runat="server" Text="Contabilita" />&nbsp;
             <asp:CheckBox ID="Chk_Denaro" runat="server" Text="Denaro" />&nbsp;
             <asp:CheckBox ID="Chk_Stanze" runat="server" Text="Stanze" />&nbsp;                     
                                            </div>

                                            <div id="INVISIBILI" style="visibility: collapse; height: 1px;">
                                                <asp:Label ID="Lbl_Metodo" runat="server"></asp:Label>
                                                <br />
                                                <label class="LabelCampo">Intestatario Conto :</label>
                                                <asp:TextBox ID="Txt_IntestarioCC" Enabled="False" runat="server"
                                                    Width="320px" MaxLength="30"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Intestatario CF Conto :</label>
                                                <asp:TextBox ID="Txt_CodiceFiscaleCC" Enabled="False" runat="server"
                                                    Width="320px" MaxLength="16"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">IBAN : Int</label>
                                                <asp:TextBox ID="Txt_Int" Enabled="False" MaxLength="2"
                                                    runat="server" Width="24px"></asp:TextBox>
                                                Num Cont
                                                <asp:TextBox ID="Txt_NumCont" Enabled="False" MaxLength="2"
                                                    onkeypress="return soloNumeri(event);" runat="server" Width="32px"></asp:TextBox>
                                                Cin
                                                <asp:TextBox ID="Txt_Cin" Enabled="False" MaxLength="1" runat="server"
                                                    Width="40px"></asp:TextBox>
                                                Abi
                                                <asp:TextBox ID="Txt_Abi" Enabled="False" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" runat="server" Width="48px"></asp:TextBox>
                                                Cab
                                                <asp:TextBox ID="Txt_Cab" Enabled="False" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" runat="server" Width="40px"></asp:TextBox>
                                                C/C Bancario&nbsp;
                                                <asp:TextBox ID="Txt_Ccbancario" Enabled="False" MaxLength="12" runat="server"
                                                    Width="328px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Banca Cliente :</label>
                                                <asp:TextBox ID="Txt_BancaCliente" Enabled="False" runat="server"
                                                    Width="320px" MaxLength="30"></asp:TextBox><br />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPane3">
                                <HeaderTemplate>
                                    Destinatario Fattura        
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Nome :</label>
                                    <asp:TextBox ID="Txt_NomeRecapito" MaxLength="100" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_IndirizzoRecapito" MaxLength="100" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_ComuneRecapito" MaxLength="100" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap4" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />




                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Note" ID="TabPanel3">
                                <HeaderTemplate>
                                    Consensi
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="cornicegrigia">
                                        <p style="text-align: center;">Consenso all’inserimento dei dati personali in Senior (l’informativa è stata compresa e firmata dall’interessato)</p>
                                        <p style="text-align: center;">
                                            <asp:RadioButton ID="RB_SIConsenso1" runat="server" Text="SI" GroupName="Consenso1" />
                                            <asp:RadioButton ID="RB_NOConsenso1" runat="server" Text="NO" GroupName="Consenso1" />
                                        </p>
                                        <br />
                                        <p style="text-align: center;">Consenso al trattamento per finalita’ di marketing</p>
                                        <p style="text-align: center;">
                                            <asp:RadioButton ID="RB_SIConsenso2" runat="server" Text="SI" GroupName="Consenso2" />
                                            <asp:RadioButton ID="RB_NOConsenso2" runat="server" Text="NO" GroupName="Consenso2" />
                                        </p>
                                        <br />
                                    </div>


                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Seleziona Report:</label>
                                                <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClientClick="javascript:return openRequestedPopup();" class="EffettoBottoniTondi" Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>
                                            </td>
                                        </tr>
                                    </table>

                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Note
                                </HeaderTemplate>
                                <ContentTemplate>
                                    NOTE :<br />
                                    <cc2:Editor
                                        ID="Txt_Note"
                                        Width="850px"
                                        Height="400px"
                                        runat="server" />

                                    <br />
                                    <br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>

                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>

        </div>    
    </form>
</body>

</html>

