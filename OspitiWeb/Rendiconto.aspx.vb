﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_Rendiconto
    Inherits System.Web.UI.Page

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub ProcSociale()

        Dim Selezione As String

        Dim ProvSelezionata As String
        Dim ComuneSelezionata As String

        Dim Extra1 As String
        Dim Extra2 As String
        Dim Extra3 As String
        Dim Extra4 As String

        Dim MySql As String
        Dim TotSql As String



        Dim LastTipo As Integer
        
        Dim WSocieta As String
        Dim MImportoComune As Double
        Dim GiorniPresenza As Integer
        Dim GiorniAssenza As Integer
        Dim TotMImportoComune As Double

        Dim MeseIni As Integer
        Dim MeseA As Integer
        Dim Anno As Integer
        Dim Mese As Integer

        Dim PrimaVolta As Boolean
        Dim Cserv As String
        Dim CodiceOspite As Integer
        Dim CreaRecordEta As Boolean

        Dim CreaRecord As Boolean
        Dim CreaRecordNaz As Boolean
        Dim CreaRecordTipUtente As Boolean        
        Dim STATOCONTABILE As String

        Dim cn As OleDbConnection

        Dim MImportoOspite As Double
        Dim MImportoParenti As Double
        Dim MImportoRegione As Double
        Dim MImportoEnte As Double
        Dim MImportoJolly As Double
        Dim TotMImportoJolly As Double

        Dim ImportoAssenza As Double
        Dim TgExtra1 As Double
        Dim TgExtra2 As Double
        Dim TgExtra3 As Double
        Dim TgExtra4 As Double

        Dim dataini As Date
        Dim DataInizio As Date
        Dim dataFine As Date

        Dim Provincia As String
        Dim Comune As String
        Dim Salto As Boolean
        Dim Stampa As New StampeOspiti

        Dim Param As New Cls_Parametri
        

        Param.AddebitiRettaInRendiconto = ""
        Param.LeggiParametri(Session("DC_OSPITE"))





        Dim OldVb6 As New Cls_FunzioniVB6


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim VettoreNS(100) As String

        VettoreNS = SplitWords(DD_Comune.SelectedValue)
        ProvSelezionata = ""
        ComuneSelezionata = ""
        If VettoreNS.Length > 1 Then
            ComuneSelezionata = VettoreNS(1)
            ProvSelezionata = VettoreNS(0)
        End If




        Selezione = "Data Stampa " & Now & vbNewLine
        Selezione = Selezione & "Periodo dal " & DD_MeseDa.SelectedItem.Text & "/" & Txt_Anno.Text & " al " & DD_MeseA.SelectedItem.Text & "/" & Txt_Anno.Text & vbNewLine
        If Trim(ProvSelezionata) <> "" And Trim(ComuneSelezionata) <> "" Then
            Selezione = Selezione & "Comune di selezione : " & DD_Comune.SelectedValue & vbNewLine
        End If
        If Trim(DD_CServ.SelectedValue) <> "" Then
            Selezione = Selezione & "Centro Servizio " & DD_CServ.SelectedItem.Text & vbNewLine
        End If
        

        If Chk_ADDACR.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti]" & vbNewLine
        End If
        If Chk_ACOspitiParenti.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti Ospiti Parenti]" & vbNewLine
        End If
        If Chk_MovEU.Checked = True Then
            Selezione = Selezione & "[Movimenti Entrata Uscita]" & vbNewLine
        End If
        If Chk_MovImp.Checked = True Then
            Selezione = Selezione & "[Movimenti Impegnativa]" & vbNewLine
        End If
        If ChkDataIniFinePrestazione.Checked = True Then
            Selezione = Selezione & "[Data Inizio Fine Prestazione]" & vbNewLine
        End If
        If Chk_ImpComEnt.Checked = True Then
            Selezione = Selezione & "[Importo Comune - Ente]" & vbNewLine
        End If
        If Chk_ImpGiornalieri.Checked = True Then
            Selezione = Selezione & "[Importi Giornalieri]" & vbNewLine
        End If
        If Chk_ImportoA0.Checked = True Then
            Selezione = Selezione & "[Importi a Zero]" & vbNewLine
        End If
        If ChkPresenze.Checked = True Then
            Selezione = Selezione & "[Solo Presenze]" & vbNewLine
        End If
        If ChkAssenza.Checked = True Then
            Selezione = Selezione & "[Solo Assenze]" & vbNewLine
        End If
        If Chk_NonAuto.Checked = True Then
            Selezione = Selezione & "[Non Autosufficenti]" & vbNewLine
        End If
        If Chk_Auto.Checked = True Then
            Selezione = Selezione & "[Autosufficenti]" & vbNewLine
        End If
        If Chk_SoloRetta.Checked = True Then
            Selezione = Selezione & "[Solo Retta]" & vbNewLine
        End If
        If Chk_SoloAddebitiAccrediti.Checked = True Then
            Selezione = Selezione & "[Solo Addebiti Accrediti]" & vbNewLine
        End If


        Extra1 = ""
        Extra2 = ""
        Extra3 = ""
        Extra4 = ""
        LastTipo = 1



        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From CAUSALI"
        cmdRd.Connection = cn

        Dim RsCausale As OleDbDataReader = cmdRd.ExecuteReader()
        Do While RsCausale.Read

            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto1C") > 0 Then
                Extra1 = Extra1 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto2C") > 0 Then
                Extra2 = Extra2 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto3C") > 0 Then
                Extra3 = Extra3 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto4C") > 0 Then
                Extra4 = Extra4 & campodb(RsCausale.Item("Codice"))
            End If

        Loop
        RsCausale.Close()


        Dim k As New Cls_DecodificaSocieta


        WSocieta = k.DecodificaSocieta(Session("DC_TABELLE"))
        MImportoComune = 0
        GiorniPresenza = 0
        GiorniAssenza = 0
        TotMImportoComune = 0
        ImportoAssenza = 0
        MeseIni = Val(DD_MeseDa.SelectedValue)
        MeseA = Val(DD_MeseA.SelectedValue)
        Anno = Txt_Anno.Text

        For I = MeseIni To MeseA
            Mese = I
        
            MySql = "SELECT RETTECOMUNE.Giorni,RETTECOMUNE.Importo,RETTECOMUNE.CodiceOspite,RETTECOMUNE.CentroServizio,RETTECOMUNE.Mese,RETTECOMUNE.Anno,RETTECOMUNE.STATOCONTABILE,RETTECOMUNE.Provincia,RETTECOMUNE.Comune,RETTECOMUNE.ELEMENTO,RETTECOMUNE.CODICEEXTRA FROM AnagraficaComune INNER JOIN RETTECOMUNE ON (AnagraficaComune.CodiceComune = RETTECOMUNE.COMUNE) AND (AnagraficaComune.CodiceProvincia = RETTECOMUNE.PROVINCIA) WHERE ((RETTECOMUNE.MESE = " & Mese & " AND RETTECOMUNE.ANNO = " & Anno & " AND AnagraficaComune.PERIODO = 'M') "

            If Mese = 2 Or Mese = 4 Or Mese = 6 Or Mese = 8 Or Mese = 10 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'B' AND (RETTECOMUNE.MESE = " & Mese - 1 & " OR RETTECOMUNE.MESE = " & Mese & "  ) AND RETTECOMUNE.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'B' AND (RETTECOMUNE.MESE = " & Mese - 1 & " OR RETTECOMUNE.MESE = " & Mese & "  ) AND RETTECOMUNE.ANNO = " & Anno & ")"
            End If
            If Mese = 3 Or Mese = 6 Or Mese = 9 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'T' AND (RETTECOMUNE.MESE = " & Mese - 1 & " OR RETTECOMUNE.MESE = " & Mese - 2 & " OR RETTECOMUNE.MESE = " & Mese & ") AND RETTECOMUNE.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'T' AND (RETTECOMUNE.MESE = " & Mese - 1 & " OR RETTECOMUNE.MESE = " & Mese - 2 & " OR RETTECOMUNE.MESE = " & Mese & ") AND RETTECOMUNE.ANNO = " & Anno & ")"
            End If


            MySql = MySql & " )"
            TotSql = TotSql & " )"




            If Trim(ProvSelezionata) <> "" Then
                MySql = MySql & " And RETTECOMUNE.PROVINCIA = '" & ProvSelezionata & "' And RETTECOMUNE.COMUNE = '" & ComuneSelezionata & "' "
                TotSql = TotSql & " And RETTECOMUNE.PROVINCIA = '" & ProvSelezionata & "' And RETTECOMUNE.COMUNE = '" & ComuneSelezionata & "' "
            End If


            If ChkAssenza.Checked = True Then
                MySql = MySql & " And RETTECOMUNE.ELEMENTO = 'RGA' "
                TotSql = TotSql & " And RETTECOMUNE.ELEMENTO = 'RGA' "
            End If
            If ChkPresenze.Checked = True Then
                TotSql = TotSql & " And RETTECOMUNE.ELEMENTO = 'RGP' "
                MySql = MySql & " And RETTECOMUNE.ELEMENTO = 'RGP' "
            End If

            If Chk_SoloRetta.Checked = True Then
                MySql = MySql & " And (RETTECOMUNE.ELEMENTO = 'RGP' OR RETTECOMUNE.ELEMENTO = 'RGA' OR RETTECOMUNE.ELEMENTO = 'RPX') "
                TotSql = TotSql & " And (RETTECOMUNE.ELEMENTO = 'RGP' OR RETTECOMUNE.ELEMENTO = 'RGA' OR RETTECOMUNE.ELEMENTO = 'RPX') "
            End If

            If Chk_SoloAddebitiAccrediti.Checked = True Then
                MySql = MySql & " And (RETTECOMUNE.ELEMENTO = 'ADD' OR RETTECOMUNE.ELEMENTO = 'ACC')"
                TotSql = TotSql & " And (RETTECOMUNE.ELEMENTO = 'ADD' OR RETTECOMUNE.ELEMENTO = 'ACC')"
            End If

            If Chk_Auto.Checked = True Then
                MySql = MySql & " And STATOCONTABILE = 'A'"
                TotSql = TotSql & " And STATOCONTABILE = 'A'"
            End If
            If Chk_NonAuto.Checked = True Then
                MySql = MySql & " And STATOCONTABILE = 'N'"
                TotSql = TotSql & " And STATOCONTABILE = 'N'"
            End If


            MySql = MySql & " Order By RETTECOMUNE.PROVINCIA,RETTECOMUNE.Comune,RETTECOMUNE.CentroServizio,RetteComune.CodiceOspite,RetteComune.Mese,RetteComune.Anno,RetteComune.STATOCONTABILE "


            PrimaVolta = True
            Cserv = ""
            CodiceOspite = 0
            'Mese = 0
            'Anno = 0
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn


            Dim Rs_RettaComune As OleDbDataReader = cmd.ExecuteReader()

            Do While Rs_RettaComune.Read
                Dim MyEseguire As Boolean = True

                MyEseguire = True

                If Val(Txt_FiltroAnno.Text) > 0 Then
                    If Val(campodb(Rs_RettaComune.Item("Anno"))) <> Val(Txt_FiltroAnno.Text) Then
                        MyEseguire = False
                    End If
                End If

                If campodbn(Rs_RettaComune.Item("CodiceOspite")) = 4331 Then
                    MyEseguire = True
                End If


                If MyEseguire = True Then
                    If (Trim(DD_CServ.SelectedValue) <> "" And campodb(Rs_RettaComune.Item("CentroServizio")) = DD_CServ.SelectedValue) _
                       Or DD_CServ.SelectedValue = "" Then

                        If campodb(Rs_RettaComune.Item("CentroServizio")) <> Cserv Or _
                           campodbn(Rs_RettaComune.Item("CodiceOspite")) <> CodiceOspite Or _
                           campodbn(Rs_RettaComune.Item("Mese")) <> Mese Or _
                           campodbn(Rs_RettaComune.Item("Anno")) <> Anno Or _
                           campodb(Rs_RettaComune.Item("STATOCONTABILE")) <> STATOCONTABILE Then
                            If MImportoComune <> 0 Or (Chk_ImportoA0.Checked = True And Not PrimaVolta) Then
                                MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno, "")
                                MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno, "")
                                MImportoRegione = ImportoRegione(Cserv, CodiceOspite, Mese, Anno, STATOCONTABILE)
                                MImportoEnte = ImportoEnte(Cserv, CodiceOspite, Mese, Anno, STATOCONTABILE)

                                MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)
                                CreaRecordEta = True
                                CreaRecord = True
                                CreaRecordNaz = True
                                CreaRecordTipUtente = True


                                If CreaRecordEta = True And CreaRecord = True And CreaRecordNaz = True And CreaRecordTipUtente Then

                                    Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoComune").NewRow

                                    Dim ClCserv As New Cls_CentroServizio

                                    ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                                    If ClCserv.TIPOCENTROSERVIZIO = "D" Then
                                        Dim f As String
                                        f = StringaPresenzeOspite(Cserv, CodiceOspite, Anno, Mese)
                                        TgExtra1 = 0
                                        TgExtra2 = 0
                                        TgExtra3 = 0
                                        TgExtra4 = 0
                                        For Li = 1 To Len(f) Step 2
                                            If InStr(1, Extra1, Mid(f, Li, 2)) > 0 Then
                                                TgExtra1 = TgExtra1 + 1
                                            End If
                                            If InStr(1, Extra2, Mid(f, Li, 2)) > 0 Then
                                                TgExtra2 = TgExtra2 + 1
                                            End If
                                            If InStr(1, Extra3, Mid(f, Li, 2)) > 0 Then
                                                TgExtra3 = TgExtra3 + 1
                                            End If
                                            If InStr(1, Extra4, Mid(f, Li, 2)) > 0 Then
                                                TgExtra4 = TgExtra4 + 1
                                            End If
                                        Next Li
                                        Rs_Stampa.Item("GiorniExtra1") = TgExtra1
                                        Rs_Stampa.Item("GiorniExtra2") = TgExtra2
                                        Rs_Stampa.Item("GiorniExtra3") = TgExtra3
                                        Rs_Stampa.Item("GiorniExtra4") = TgExtra4

                                        Rs_Stampa.Item("ImportoExtra1") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 1)
                                        Rs_Stampa.Item("ImportoExtra2") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 2)
                                        Rs_Stampa.Item("ImportoExtra3") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 3)
                                        Rs_Stampa.Item("ImportoExtra4") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 4)
                                    End If

                                    Rs_Stampa.Item("CodiceServizio") = Cserv
                                    Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE
                                    Rs_Stampa.Item("CodiceOspite") = CodiceOspite
                                    Rs_Stampa.Item("Provincia") = Provincia
                                    Rs_Stampa.Item("Comune") = Comune


                                    Dim RettaTotale As New Cls_rettatotale

                                    RettaTotale.CENTROSERVIZIO = Cserv
                                    RettaTotale.CODICEOSPITE = CodiceOspite
                                    RettaTotale.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                                    Rs_Stampa.Item("TipoRetta") = ""
                                    If RettaTotale.TipoRetta <> "" Then
                                        Dim TipoRetta As New Cls_TipoRetta
                                        TipoRetta.Codice = RettaTotale.TipoRetta
                                        TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Codice)

                                        Rs_Stampa.Item("TipoRetta") = TipoRetta.Descrizione
                                    End If

                                    Rs_Stampa.Item("TipoImportoRegione") = ""
                                    Dim MRegione As New Cls_StatoAuto
                                    MRegione.CODICEOSPITE = CodiceOspite
                                    MRegione.CENTROSERVIZIO = Cserv
                                    MRegione.UltimaData(Session("DC_OSPITE"), MRegione.CODICEOSPITE, MRegione.CENTROSERVIZIO)

                                    If MRegione.TipoRetta <> "" Then
                                        Dim TabellaTipoImportoRegione As New Cls_TabellaTipoImportoRegione
                                        TabellaTipoImportoRegione.Codice = MRegione.TipoRetta
                                        TabellaTipoImportoRegione.Leggi(Session("DC_OSPITE"), TabellaTipoImportoRegione.Codice)

                                        Rs_Stampa.Item("TipoImportoRegione") = TabellaTipoImportoRegione.Descrizione
                                    End If



                                    Dim XOsp As New ClsOspite

                                    XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)


                                    Rs_Stampa.Item("DataNascita") = XOsp.DataNascita

                                    Dim XCom As New ClsComune

                                    XCom.Provincia = Provincia
                                    XCom.Comune = Comune
                                    XCom.Leggi(Session("DC_OSPITE"))
                                    Rs_Stampa.Item("DecodificaComune") = XCom.Descrizione
                                    Rs_Stampa.Item("Attenzione") = XCom.Attenzione

                                    If Chk_NonIncludereDataNascita.Checked = False Then
                                        Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        End If
                                    Else
                                        Rs_Stampa.Item("Nome") = XOsp.Nome
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                                        End If
                                    End If


                                    Dim Tab As New Cls_Tabelle

                                    Tab.TipTab = "NAZ"
                                    Tab.Codice = XOsp.Nazionalita
                                    Tab.Leggi(Session("DC_OSPITE"))
                                    Rs_Stampa.Item("Nazionalita") = Tab.Descrizione
                                    Rs_Stampa.Item("Mese") = Mese
                                    Rs_Stampa.Item("Anno") = Anno
                                    Rs_Stampa.Item("StatoAuto") = STATOCONTABILE

                                    If Chk_ImportoA0.Checked = True And GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' AND StatoContabile = '" & STATOCONTABILE & "'"
                                        GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                                        GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)


                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If


                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & "  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                    End If

                                    Dim IseeOspite As New Cls_Ise

                                    IseeOspite.CODICEOSPITE = CodiceOspite
                                    IseeOspite.CENTROSERVIZIO = Cserv
                                    IseeOspite.UltimaData(Session("DC_OSPITE"), IseeOspite.CODICEOSPITE, IseeOspite.CENTROSERVIZIO)

                                    Rs_Stampa.Item("Isee") = IseeOspite.ISEE


                                    Dim Accomp As New Cls_Accompagnamento

                                    Accomp.CENTROSERVIZIO = Cserv
                                    Accomp.CODICEOSPITE = CodiceOspite
                                    Accomp.LeggiAData(Session("DC_OSPITE"), Now)

                                    Rs_Stampa.Item("Accompagnamento") = Accomp.Accompagnamento

                                    Dim xCalc As New Cls_CalcoloRette
                                    Dim ValImportoOspite As Double = 0
                                    Dim ValImportoParenti As Double = 0
                                    Dim ValImportoComune As Double = 0
                                    Dim ValImportoRegione As Double = 0
                                    Dim ValImportoJolly As Double = 0
                                    Dim IndiceParenti As Integer = 0

                                    xCalc.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                                    xCalc.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                                    Dim KO As New ClsOspite

                                    KO.Leggi(Session("DC_OSPITE"), CodiceOspite)

                                    ValImportoOspite = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "O", 0, Now), 2)
                                    For IndiceParenti = 1 To 10
                                        ValImportoParenti = ValImportoParenti + Math.Round(CDbl(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "P", IndiceParenti, Now)), 2)
                                    Next
                                    ValImportoComune = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0, Now), 2)
                                    ValImportoRegione = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "R", 0, Now), 2)
                                    ValImportoJolly = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "J", 0, Now), 2)
                                    xCalc.ChiudiDB()

                                    Rs_Stampa.Item("QuotaEnteSociale") = ValImportoComune

                                    Rs_Stampa.Item("QuotaPrivata") = ValImportoOspite + ValImportoParenti



                                    Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                                    Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                                    Rs_Stampa.Item("ImportoAssenza") = ImportoAssenza
                                    Rs_Stampa.Item("TotaleRetta") = Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + TotMImportoComune + TotMImportoJolly, 2)
                                    Rs_Stampa.Item("Ospite") = Math.Round(MImportoOspite, 2)
                                    Rs_Stampa.Item("Parente") = Math.Round(MImportoParenti, 2)
                                    Rs_Stampa.Item("Regione") = Math.Round(MImportoRegione, 2)


                                    If Chk_ImpComEnt.Checked = True Then
                                        Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune - Math.Abs(MImportoEnte), 2)
                                    Else
                                        Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune, 2)
                                    End If
                                    Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE 'CampoOspite(CodiceOspite, "CODICEFISCALE")

                                    Dim MovimentiNelPeriodo As New Cls_Movimenti

                                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                    MovimentiNelPeriodo.UltimaDataAccoglimento(Session("DC_OSPITE"))
                                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                        Rs_Stampa.Item("DataInizio") = MovimentiNelPeriodo.Data
                                    Else
                                        Rs_Stampa.Item("DataInizio") = DateSerial(Anno, Mese, 1)
                                    End If

                                    MovimentiNelPeriodo.Data = Nothing
                                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                    MovimentiNelPeriodo.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                        Rs_Stampa.Item("DataFine") = MovimentiNelPeriodo.Data
                                    Else
                                        Rs_Stampa.Item("DataFine") = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                                    End If

                                    Rs_Stampa.Item("TemporaneoPermanente") = OspitePermanente(Cserv, CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)))
                                    If Chk_ADDACR.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                                    End If
                                    If Chk_MovEU.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_MovStato.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_TipoRetta.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_MovImp.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_ImpGiornalieri.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                                    End If


                                    Rs_Stampa.Item("IntestaSocieta") = WSocieta
                                    Rs_Stampa.Item("Selezione") = Selezione

                                    Dim XServ As New Cls_CentroServizio

                                    XServ.CENTROSERVIZIO = Cserv
                                    XServ.Leggi(Session("DC_OSPITE"), Cserv)
                                    If XServ.TIPOCENTROSERVIZIO = "D" Then


                                        Dim x As New Cls_Diurno


                                        x.CodiceOspite = CodiceOspite
                                        x.CENTROSERVIZIO = Cserv
                                        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                                        Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                                        Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                                        Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                                        Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                                        Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                                        Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                                        Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                                        Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                                        Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                                        Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                                        Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                                        Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                                        Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                                        Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                                        Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                                        Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                                        Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                                        Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                                        Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                                        Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                                        Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                                        Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                                        Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                                        Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                                        Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                                        Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                                        Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                                        Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                                        Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                                        Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                                        Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31

                                        If Trim(x.LeggiMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)) = "" Then
                                            Rs_Stampa.Item("Nome") = Rs_Stampa.Item("Nome") & "*"                                         
                                        End If


                                        If x.CodiceOspite = 85 Then
                                            x.CodiceOspite = 85
                                        End If
                                        Rs_Stampa.Item("GiorniPresTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPieno)
                                        Rs_Stampa.Item("GiorniAssTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss)
                                        If XServ.CausaleTempoPienoAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss1)
                                        End If
                                        If XServ.CausaleTempoPienoAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss2)
                                        End If


                                        Rs_Stampa.Item("GiorniPresPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTime)
                                        Rs_Stampa.Item("GiorniAssPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss)
                                        If XServ.CausalePartTimeAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss1)
                                        End If
                                        If XServ.CausalePartTimeAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss2)
                                        End If




                                        Rs_Stampa.Item("GiorniMalattia") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "M")

                                        Rs_Stampa.Item("GiorniRicovero") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "A")

                                        Dim sc2 As New MSScriptControl.ScriptControl
                                        Dim SalvaComune As Double
                                        Dim m As New Cls_CalcoloRette

                                        sc2.Language = "vbscript"

                                        m.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                                        m.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                                        SalvaComune = m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0)

                                        Dim MS As New Cls_CausaliEntrataUscita

                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPieno
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaPresTP") = sc2.Eval("SalvaComune")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("SalvaOspite = 0")
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPienoAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaComune")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausalePartTime
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")


                                        Rs_Stampa.Item("QuotaPresPT") = sc2.Eval("SalvaComune")


                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausalePartTimeAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssPT") = sc2.Eval("SalvaComune")

                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = "M"
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaMalattia") = sc2.Eval("SalvaComune")


                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = "A"
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaRicovero") = sc2.Eval("SalvaComune")


                                    Else
                                        Dim Rs_Movimenti As New ADODB.Recordset
                                        Dim Rs_MYMOV As New ADODB.Recordset
                                        Dim MyDataini As Date, MyDatafine As Date
                                        Dim WValore As String = ""
                                        Dim Uscito As Boolean
                                        Dim GGUSCITA As String, GGENTRATA As String
                                        Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                                        Dim Accolto As Boolean
                                        Dim Dimesso As Boolean
                                        Dim GiornoRientro As Integer
                                        Dim IndiceGiorni As Integer

                                        Dim Myserv As New Cls_CentroServizio


                                        Myserv.CENTROSERVIZIO = Cserv
                                        Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                                        Dim KPar As New Cls_Parametri

                                        KPar.LeggiParametri(Session("DC_OSPITE"))

                                        Accolto = False
                                        Dimesso = False
                                        GGUSCITA = KPar.GIORNOUSCITA
                                        GGENTRATA = KPar.GIORNOENTRATA
                                        If GGENTRATA = "P" Then
                                            GiornoRientro = 1
                                        Else
                                            GiornoRientro = 0
                                        End If

                                        MyDataini = DateSerial(Anno, Mese, 1)

                                        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                                        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                                        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                                        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                                        Dim cmdM As New OleDbCommand()
                                        cmdM.CommandText = MySql
                                        cmdM.Connection = cn

                                        Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                                        If ReaderMovimenti.Read() Then
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                                                Uscito = True
                                                WValore = campodb(ReaderMovimenti.Item("Causale"))
                                                'DATAINIZIO = MyDataini
                                                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                                                'If Not Rs_MYMOV.EOF Then
                                                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                                                'End If
                                                'Rs_MYMOV.Close
                                                NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                                            End If
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                    Accolto = True
                                                End If
                                                Uscito = False
                                                NGiorniAssenza = 0
                                            End If
                                        End If
                                        ReaderMovimenti.Close()


                                        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                                        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                                        Dim cmd1 As New OleDbCommand()
                                        cmd1.CommandText = MySql
                                        cmd1.Connection = cn

                                        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                                        GiornoUscita = 1
                                        Do While ReaderMovimenti1.Read
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                                    If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                                        GiornoUscita = 0
                                                    Else
                                                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                        Uscito = True
                                                        NGiorniAssenza = 1
                                                    End If
                                                Else
                                                    GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                    Uscito = True
                                                    NGiorniAssenza = 1
                                                End If
                                            End If
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                                                If Uscito = True Then
                                                    For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                        NGiorniAssenza = NGiorniAssenza + 1
                                                    Next IndiceGiorni
                                                End If
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                                                Else
                                                    GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                                                End If
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                                                End If

                                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                                                WValore = "*"
                                                Uscito = True
                                                Dimesso = True
                                                NGiorniAssenza = 2
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                    NGiorniAssenza = NGiorniAssenza + 1
                                                Next IndiceGiorni
                                                Uscito = False
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                                Next IndiceGiorni
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                                    Else
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                                    End If
                                                End If
                                                Uscito = False
                                                Accolto = True
                                            End If

                                        Loop
                                        If Uscito Then
                                            For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                NGiorniAssenza = NGiorniAssenza + 1
                                            Next IndiceGiorni
                                        End If
                                        For IndiceGiorni = 27 To 31
                                            If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                            End If
                                        Next IndiceGiorni
                                        ReaderMovimenti1.Close()

                                        For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                                        Next

                                    End If

                                    Dim Trascodifiche As String = ""
                                    Dim CodiciInseriti As String = ""

                                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                        Dim CercaCausale As New Cls_CausaliEntrataUscita
                                        Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                                        If PosizioneNewLine > 0 Then
                                            CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                                            If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                                            Else
                                                If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                                    CercaCausale.LeggiCausale(Session("DC_OSPITE"))
                                                    If Trascodifiche <> "" Then
                                                        Trascodifiche = Trascodifiche & ","
                                                    End If
                                                    CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                                    Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                                                End If
                                            End If
                                        End If
                                    Next
                                    Rs_Stampa.Item("Trascodifiche") = Trascodifiche


                                    Stampa.Tables("RendicontoComune").Rows.Add(Rs_Stampa)
                                End If

                                If Cserv = campodb(Rs_RettaComune.Item("CentroServizio")) And _
                                   CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite")) Then
                                    Salto = True
                                Else
                                    Salto = False
                                End If

                                Cserv = campodb(Rs_RettaComune.Item("CentroServizio"))
                                CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite"))
                                Mese = campodb(Rs_RettaComune.Item("Mese"))
                                Anno = campodb(Rs_RettaComune.Item("Anno"))
                                STATOCONTABILE = campodb(Rs_RettaComune.Item("STATOCONTABILE"))
                                Provincia = campodb(Rs_RettaComune.Item("Provincia"))
                                Comune = campodb(Rs_RettaComune.Item("Comune"))
                                If ChkDataIniFinePrestazione.Checked = True Then
                                    If Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                       Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                        dataini = DataAccoglimento(Cserv, CodiceOspite, Mese, Anno)
                                    Else
                                        dataini = DateSerial(Anno, Mese, 1)
                                    End If
                                    If Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                       Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                        dataFine = DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno)
                                    Else
                                        dataFine = DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno))
                                    End If

                                End If
                            End If
                            GiorniPresenza = 0
                            GiorniAssenza = 0
                            MImportoComune = 0
                            MImportoJolly = 0
                            MImportoEnte = 0
                            TotMImportoComune = 0
                            TotMImportoJolly = 0
                            ImportoAssenza = 0
                            Cserv = campodb(Rs_RettaComune.Item("CentroServizio"))
                            CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite"))
                            Mese = campodb(Rs_RettaComune.Item("Mese"))
                            Anno = campodb(Rs_RettaComune.Item("Anno"))
                            STATOCONTABILE = campodb(Rs_RettaComune.Item("STATOCONTABILE"))
                            Provincia = campodb(Rs_RettaComune.Item("Provincia"))
                            Comune = campodb(Rs_RettaComune.Item("Comune"))
                        End If
                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "RGP" Then
                            GiorniPresenza = campodb(Rs_RettaComune.Item("Giorni"))
                        End If

                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "RGA" And Param.AddebitiRettaInRendiconto.IndexOf("<" & campodb(Rs_RettaComune.Item("CODICEEXTRA")) & ">") >= 0 Then
                            GiorniAssenza = campodb(Rs_RettaComune.Item("Giorni"))
                            ImportoAssenza = ImportoAssenza + campodb(Rs_RettaComune.Item("Importo"))
                        End If
                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "ACC" Then
                            MImportoComune = MImportoComune - campodb(Rs_RettaComune.Item("Importo"))
                        Else
                            MImportoComune = MImportoComune + campodb(Rs_RettaComune.Item("Importo"))
                            If campodb(Rs_RettaComune.Item("ELEMENTO")) <> "ADD" Then
                                TotMImportoComune = TotMImportoComune + campodb(Rs_RettaComune.Item("Importo"))
                            End If
                        End If

                        PrimaVolta = False
                    End If
                End If
            Loop

            If MImportoComune <> 0 Or (Chk_ImportoA0.Checked = True And Not PrimaVolta) Then

                CreaRecordEta = True
                CreaRecord = True
                CreaRecordNaz = True
                CreaRecordTipUtente = True


                If CreaRecordEta = True And CreaRecord = True And CreaRecordNaz = True And CreaRecordTipUtente Then

                    MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno, "")
                    MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno, "")
                    MImportoRegione = ImportoRegione(Cserv, CodiceOspite, Mese, Anno, STATOCONTABILE)

                    MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)


                    Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoComune").NewRow

                    Dim ClCserv As New Cls_CentroServizio

                    ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                    Dim XOsp As New ClsOspite

                    XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)


                    If ClCserv.TIPOCENTROSERVIZIO = "D" Then
                        Dim f As String
                        f = StringaPresenzeOspite(Cserv, CodiceOspite, Anno, Mese)
                        TgExtra1 = 0
                        TgExtra2 = 0
                        TgExtra3 = 0
                        TgExtra4 = 0
                        For Li = 1 To Len(f) Step 2
                            If InStr(1, Extra1, Mid(f, Li, 2)) > 0 Then
                                TgExtra1 = TgExtra1 + 1
                            End If
                            If InStr(1, Extra2, Mid(f, Li, 2)) > 0 Then
                                TgExtra2 = TgExtra2 + 1
                            End If
                            If InStr(1, Extra3, Mid(f, Li, 2)) > 0 Then
                                TgExtra3 = TgExtra3 + 1
                            End If
                            If InStr(1, Extra4, Mid(f, Li, 2)) > 0 Then
                                TgExtra4 = TgExtra4 + 1
                            End If
                        Next Li
                        Rs_Stampa.Item("GiorniExtra1") = TgExtra1
                        Rs_Stampa.Item("GiorniExtra2") = TgExtra2
                        Rs_Stampa.Item("GiorniExtra3") = TgExtra3
                        Rs_Stampa.Item("GiorniExtra4") = TgExtra4

                        Rs_Stampa.Item("ImportoExtra1") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 1)
                        Rs_Stampa.Item("ImportoExtra2") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 2)
                        Rs_Stampa.Item("ImportoExtra3") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 3)
                        Rs_Stampa.Item("ImportoExtra4") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 4)



                        Dim x As New Cls_Diurno


                        x.CodiceOspite = CodiceOspite
                        x.CENTROSERVIZIO = Cserv
                        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                        Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                        Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                        Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                        Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                        Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                        Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                        Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                        Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                        Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                        Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                        Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                        Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                        Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                        Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                        Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                        Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                        Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                        Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                        Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                        Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                        Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                        Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                        Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                        Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                        Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                        Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                        Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                        Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                        Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                        Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                        Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31

                        If Trim(x.LeggiMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)) = "" Then
                            Rs_Stampa.Item("Nome") = Rs_Stampa.Item("Nome") & "*"
                            If Chk_SoloIniziali.Checked = True Then
                                Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & "*"
                            End If
                        End If


                        If x.CodiceOspite = 85 Then
                            x.CodiceOspite = 85
                        End If
                        Rs_Stampa.Item("GiorniPresTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPieno)
                        Rs_Stampa.Item("GiorniAssTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss)
                        If ClCserv.CausaleTempoPienoAss1 <> "" Then
                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss1)
                        End If
                        If ClCserv.CausaleTempoPienoAss2 <> "" Then
                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss2)
                        End If

                        Rs_Stampa.Item("GiorniPresPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTime)
                        Rs_Stampa.Item("GiorniAssPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss)
                        If ClCserv.CausalePartTimeAss1 <> "" Then
                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss1)
                        End If
                        If ClCserv.CausalePartTimeAss2 <> "" Then
                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss2)
                        End If

                        Rs_Stampa.Item("GiorniMalattia") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "M")

                        Rs_Stampa.Item("GiorniRicovero") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "A")

                        Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE 'CampoOspite(CodiceOspite, "CODICEFISCALE")

                        Dim sc2 As New MSScriptControl.ScriptControl
                        Dim SalvaComune As Double
                        Dim m As New Cls_CalcoloRette

                        sc2.Language = "vbscript"

                        m.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                        m.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                        SalvaComune = m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0)

                        Dim MS As New Cls_CausaliEntrataUscita

                        sc2.Reset()
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausaleTempoPieno
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaPresTP") = sc2.Eval("SalvaComune")



                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausaleTempoPienoAss
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaComune")



                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausalePartTime
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")


                        Rs_Stampa.Item("QuotaPresPT") = sc2.Eval("SalvaComune")


                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausalePartTimeAss
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaAssPT") = sc2.Eval("SalvaComune")

                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = "M"
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaMalattia") = sc2.Eval("SalvaComune")


                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = "A"
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        Try
                            If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Catch ex As Exception

                        End Try
                        
                        Rs_Stampa.Item("QuotaRicovero") = sc2.Eval("SalvaComune")

                    Else
                        Dim Rs_Movimenti As New ADODB.Recordset
                        Dim Rs_MYMOV As New ADODB.Recordset
                        Dim MyDataini As Date, MyDatafine As Date
                        Dim WValore As String = ""
                        Dim Uscito As Boolean
                        Dim GGUSCITA As String, GGENTRATA As String
                        Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                        Dim Accolto As Boolean
                        Dim Dimesso As Boolean
                        Dim GiornoRientro As Integer
                        Dim IndiceGiorni As Integer

                        Dim Myserv As New Cls_CentroServizio


                        Myserv.CENTROSERVIZIO = Cserv
                        Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                        Dim KPar As New Cls_Parametri

                        KPar.LeggiParametri(Session("DC_OSPITE"))

                        Accolto = False
                        Dimesso = False
                        GGUSCITA = KPar.GIORNOUSCITA
                        GGENTRATA = KPar.GIORNOENTRATA
                        If GGENTRATA = "P" Then
                            GiornoRientro = 1
                        Else
                            GiornoRientro = 0
                        End If

                        MyDataini = DateSerial(Anno, Mese, 1)

                        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                        Dim cmdM As New OleDbCommand()
                        cmdM.CommandText = MySql
                        cmdM.Connection = cn

                        Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                        If ReaderMovimenti.Read() Then
                            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                                Uscito = True
                                WValore = campodb(ReaderMovimenti.Item("Causale"))
                                'DATAINIZIO = MyDataini
                                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                                'If Not Rs_MYMOV.EOF Then
                                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                                'End If
                                'Rs_MYMOV.Close
                                NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                            End If
                            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                    Accolto = True
                                End If
                                Uscito = False
                                NGiorniAssenza = 0
                            End If
                        End If
                        ReaderMovimenti.Close()


                        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                        Dim cmd1 As New OleDbCommand()
                        cmd1.CommandText = MySql
                        cmd1.Connection = cn

                        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                        GiornoUscita = 1
                        Do While ReaderMovimenti1.Read
                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                                If GGUSCITA = "P" Then
                                    GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                    If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                        GiornoUscita = 0
                                    Else
                                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                        Uscito = True
                                        NGiorniAssenza = 1
                                    End If
                                Else
                                    GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                    Uscito = True
                                    NGiorniAssenza = 1
                                End If
                            End If
                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                                If Uscito = True Then
                                    For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                        NGiorniAssenza = NGiorniAssenza + 1
                                    Next IndiceGiorni
                                End If
                                If GGUSCITA = "P" Then
                                    GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                                Else
                                    GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                                End If
                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                Else
                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                                End If

                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                                WValore = "*"
                                Uscito = True
                                Dimesso = True
                                NGiorniAssenza = 2
                            End If

                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                    NGiorniAssenza = NGiorniAssenza + 1
                                Next IndiceGiorni
                                Uscito = False
                            End If

                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                Next IndiceGiorni
                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                Else
                                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                    Else
                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                    End If
                                End If
                                Uscito = False
                                Accolto = True
                            End If

                        Loop
                        If Uscito Then
                            For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                NGiorniAssenza = NGiorniAssenza + 1
                            Next IndiceGiorni
                        End If
                        For IndiceGiorni = 27 To 31
                            If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                            End If
                        Next IndiceGiorni
                        ReaderMovimenti1.Close()

                        For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                        Next

                    End If                

                    Dim Trascodifiche As String = ""
                    Dim CodiciInseriti As String = ""
                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                        Dim CercaCausale As New Cls_CausaliEntrataUscita
                        Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                        If PosizioneNewLine > 0 Then
                            CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                            If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                            Else                                
                                If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                    CercaCausale.LeggiCausale(Session("DC_OSPITE"))

                                    If Trascodifiche <> "" Then
                                        Trascodifiche = Trascodifiche & ","
                                    End If

                                    CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                    Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                                End If
                            End If
                        End If
                    Next


                    Rs_Stampa.Item("Trascodifiche") = Trascodifiche
                    Rs_Stampa.Item("CodiceServizio") = Cserv
                    Rs_Stampa.Item("CodiceOspite") = CodiceOspite
                    Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE

                    Dim RettaTotale2 As New Cls_rettatotale

                    RettaTotale2.CENTROSERVIZIO = Cserv
                    RettaTotale2.CODICEOSPITE = CodiceOspite
                    RettaTotale2.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                    Rs_Stampa.Item("TipoRetta") = ""
                    If RettaTotale2.TipoRetta <> "" Then
                        Dim TipoRetta As New Cls_TipoRetta
                        TipoRetta.Codice = RettaTotale2.TipoRetta
                        TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Codice)

                        Rs_Stampa.Item("TipoRetta") = TipoRetta.Descrizione
                    End If

                    Rs_Stampa.Item("TipoImportoRegione") = ""
                    Dim MRegione As New Cls_StatoAuto
                    MRegione.CODICEOSPITE = CodiceOspite
                    MRegione.CENTROSERVIZIO = Cserv
                    MRegione.UltimaData(Session("DC_OSPITE"), MRegione.CODICEOSPITE, MRegione.CENTROSERVIZIO)

                    If MRegione.TipoRetta <> "" Then
                        Dim TabellaTipoImportoRegione As New Cls_TabellaTipoImportoRegione
                        TabellaTipoImportoRegione.Codice = MRegione.TipoRetta
                        TabellaTipoImportoRegione.Leggi(Session("DC_OSPITE"), TabellaTipoImportoRegione.Codice)

                        Rs_Stampa.Item("TipoImportoRegione") = TabellaTipoImportoRegione.Descrizione
                    End If

                    Rs_Stampa.Item("Provincia") = Provincia
                    Rs_Stampa.Item("Comune") = Comune

                    Rs_Stampa.Item("DataNascita") = XOsp.DataNascita

                    Dim KDc As New ClsComune

                    KDc.Provincia = Provincia
                    KDc.Comune = Comune
                    KDc.Leggi(Session("DC_OSPITE"))
                    Rs_Stampa.Item("DecodificaComune") = KDc.Descrizione
                    Rs_Stampa.Item("Attenzione") = KDc.Attenzione

                    'KDc.Attenzione
                    If Chk_NonIncludereDataNascita.Checked = False Then
                        Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                        If Chk_SoloIniziali.Checked = True Then
                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                        End If
                    Else
                        Rs_Stampa.Item("Nome") = XOsp.Nome
                        If Chk_SoloIniziali.Checked = True Then
                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                        End If
                    End If



                    Dim Tab As New Cls_Tabelle

                    Tab.TipTab = "NAZ"
                    Tab.Codice = XOsp.Nazionalita
                    Tab.Leggi(Session("DC_OSPITE"))

                    Rs_Stampa.Item("Nazionalita") = Tab.Descrizione
                    Rs_Stampa.Item("Mese") = Mese
                    Rs_Stampa.Item("Anno") = Anno
                    Rs_Stampa.Item("StatoAuto") = STATOCONTABILE

                    If Chk_ImportoA0.Checked = True And GiorniPresenza = 0 And GiorniAssenza = 0 Then
                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP'  AND StatoContabile = '" & STATOCONTABILE & "'"
                        GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA'  AND StatoContabile = '" & STATOCONTABILE & "'"
                        GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If
                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 4  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 4  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEREGIONE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEREGIONE Where " & MySql)
                        End If
                    End If

                    MImportoEnte = ImportoEnte(Cserv, CodiceOspite, Mese, Anno, STATOCONTABILE)
                    Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                    Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                    Rs_Stampa.Item("ImportoAssenza") = ImportoAssenza

                    Dim IseeOspite As New Cls_Ise

                    IseeOspite.CODICEOSPITE = CodiceOspite
                    IseeOspite.CENTROSERVIZIO = Cserv
                    IseeOspite.UltimaData(Session("DC_OSPITE"), IseeOspite.CODICEOSPITE, IseeOspite.CENTROSERVIZIO)

                    Rs_Stampa.Item("Isee") = IseeOspite.ISEE


                    Dim Accomp As New Cls_Accompagnamento

                    Accomp.CENTROSERVIZIO = Cserv
                    Accomp.CODICEOSPITE = CodiceOspite
                    Accomp.LeggiAData(Session("DC_OSPITE"), Now)

                    Rs_Stampa.Item("Accompagnamento") = Accomp.Accompagnamento

                    Dim xCalc As New Cls_CalcoloRette
                    Dim ValImportoOspite As Double = 0
                    Dim ValImportoParenti As Double = 0
                    Dim ValImportoComune As Double = 0
                    Dim ValImportoRegione As Double = 0
                    Dim ValImportoJolly As Double = 0
                    Dim IndiceParenti As Integer = 0
                    
                    xCalc.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                    xCalc.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                    Dim KO As New ClsOspite

                    KO.Leggi(Session("DC_OSPITE"), CodiceOspite)

                    ValImportoOspite = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "O", 0, Now), 2)
                    For IndiceParenti = 1 To 10
                        ValImportoParenti = ValImportoParenti + Math.Round(CDbl(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "P", IndiceParenti, Now)), 2)
                    Next
                    ValImportoComune = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0, Now), 2)
                    ValImportoRegione = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "R", 0, Now), 2)
                    ValImportoJolly = Math.Round(xCalc.QuoteGiornaliere(Cserv, CodiceOspite, "J", 0, Now), 2)
                    xCalc.ChiudiDB()

                    Rs_Stampa.Item("QuotaEnteSociale") = ValImportoComune

                    Rs_Stampa.Item("QuotaPrivata") = ValImportoOspite + ValImportoParenti


                    Rs_Stampa.Item("TotaleRetta") = Format(Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + TotMImportoComune + MImportoJolly, 2), "#,##0.00")
                    Rs_Stampa.Item("Ospite") = Format(Math.Round(MImportoOspite, 2), "#,##0.00")
                    Rs_Stampa.Item("Parente") = Format(Math.Round(MImportoParenti, 2), "#,##0.00")
                    Rs_Stampa.Item("Regione") = Format(Math.Round(MImportoRegione, 2), "#,##0.00")

                    Rs_Stampa.Item("ImpJolly") = Format(Math.Round(MImportoJolly, 2), "#,##0.00")

                    If Chk_ImpComEnt.Checked = True Then
                        Rs_Stampa.Item("ImpComune") = Format(Math.Round(MImportoComune - Math.Abs(MImportoEnte), 2), "#,##0.00")
                    Else
                        Rs_Stampa.Item("ImpComune") = Format(Math.Round(MImportoComune, 2), "#,##0.00")
                    End If

                    Dim MovimentiNelPeriodo As New Cls_Movimenti

                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                    MovimentiNelPeriodo.UltimaDataAccoglimento(Session("DC_OSPITE"))
                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                        Rs_Stampa.Item("DataInizio") = MovimentiNelPeriodo.Data
                    Else
                        Rs_Stampa.Item("DataInizio") = DateSerial(Anno, Mese, 1)
                    End If

                    MovimentiNelPeriodo.Data = Nothing
                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                    MovimentiNelPeriodo.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                        Rs_Stampa.Item("DataFine") = MovimentiNelPeriodo.Data
                    Else
                        Rs_Stampa.Item("DataFine") = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    End If


                    Rs_Stampa.Item("TemporaneoPermanente") = OspitePermanente(Cserv, CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)))
                    If Chk_ADDACR.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                    End If
                    If Chk_MovEU.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_MovStato.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_TipoRetta.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_MovImp.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_ImpGiornalieri.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                    End If
                    If Chk_AccoglimentoUscitaDef.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If


                    Rs_Stampa.Item("IntestaSocieta") = WSocieta
                    Rs_Stampa.Item("Selezione") = Selezione

                    Stampa.Tables("RendicontoComune").Rows.Add(Rs_Stampa)
                End If
            End If
            Rs_RettaComune.Close()

            GiorniPresenza = 0
            GiorniAssenza = 0
            MImportoComune = 0
            TotMImportoComune = 0
            Cserv = ""
            CodiceOspite = 0
            'Mese = 0
            'Anno = 0
            STATOCONTABILE = ""
            Provincia = ""
            Comune = ""

        Next I
        cn.Close()


        Dim Numero As Integer
        Dim CodiceServizio As String
        Dim DecodificaComune As String

        'Dim foundRowsRM As System.Data.DataRow()

        Dim foundRowsT As System.Data.DataRow()
        foundRowsT = Stampa.Tables("RendicontoComune").Select("", "CodiceServizio,DecodificaComune,Nome,Mese")
        If foundRowsT.Length > 0 Then
            CodiceServizio = foundRowsT(0).Item("CodiceServizio")
            DecodificaComune = campodb(foundRowsT(0).Item("DecodificaComune"))
        End If
        

        For MInd = 0 To foundRowsT.Length - 1
            If campodb(CodiceServizio) <> campodb(foundRowsT(MInd).Item("CodiceServizio")) Then
                Numero = 0
            End If
            If campodb(DecodificaComune) <> campodb(foundRowsT(MInd).Item("DecodificaComune")) Then
                Numero = 0
            End If
            Numero = Numero + 1

            foundRowsT(MInd).Item("Numero") = Numero

            CodiceServizio = foundRowsT(MInd).Item("CodiceServizio")
            DecodificaComune = campodb(foundRowsT(MInd).Item("DecodificaComune"))

        Next MInd


        Session("stampa") = Stampa

    End Sub

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Function ImportoComune(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RETTECOMUNE Where (Elemento = 'RGP' OR Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp

        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTECOMUNE Where (Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTECOMUNE Where (Elemento = 'RGP') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaComune As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaComune.Read Then
            If Not IsDBNull(Rs_RettaComune.Item(0)) Then
                ImportoComune = Rs_RettaComune.Item(0)
            End If
        End If
        Rs_RettaComune.Close()
        cn.Close()

    End Function


    Private Function ImportoJolly(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RETTEJolly Where (Elemento = 'RGP' OR Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp

        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTEJolly Where (Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTEJolly Where (Elemento = 'RGP') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaJolly As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaJolly.Read Then
            If Not IsDBNull(Rs_RettaJolly.Item(0)) Then
                ImportoJolly = Rs_RettaJolly.Item(0)
            End If
        End If
        Rs_RettaJolly.Close()
        cn.Close()
    End Function

    Private Function ImportoParenti(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RetteParente Where (Elemento = 'RGP' OR Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteParente Where (Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteParente Where (Elemento = 'RGP') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If

        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaParenti As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaParenti.Read Then
            If Not IsDBNull(Rs_RettaParenti.Item(0)) Then
                ImportoParenti = Rs_RettaParenti.Item(0)
            End If
        End If
        Rs_RettaParenti.Close()
        cn.Close()
    End Function

    Private Function ImportoOspite(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RetteOspite Where (Elemento = 'RGP' OR Elemento = 'RGA') AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteOspite Where (Elemento = 'RGP') AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteOspite Where (Elemento = 'RGP' ) AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaOspite As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaOspite.Read Then
            If Not IsDBNull(Rs_RettaOspite.Item(0)) Then
                ImportoOspite = Rs_RettaOspite.Item(0)
            End If
        End If
        Rs_RettaOspite.Close()

        cn.Close()
    End Function


    Private Function ImportoEnte(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RetteEnte Where (Elemento = 'RGP' OR Elemento = 'RGA') AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteEnte Where (Elemento = 'RGP') AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RetteEnte Where (Elemento = 'RGP' ) AND Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaEnte As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaEnte.Read Then
            If Not IsDBNull(Rs_RettaEnte.Item(0)) Then
                ImportoEnte = Rs_RettaEnte.Item(0)
            End If
        End If
        Rs_RettaEnte.Close()
        cn.Close()
    End Function



    Private Function ImportoRegionePresenze(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGP') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaRegione As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaRegione.Read Then
            If Not IsDBNull(Rs_RettaRegione.Item(0)) Then
                ImportoRegionePresenze = Rs_RettaRegione.Item(0)
            End If
        End If
        Rs_RettaRegione.Close()
        cn.Close()
    End Function


    Private Function ImportoRegioneAssenze(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGP' OR Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp

        MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        
        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaRegione As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaRegione.Read Then
            If Not IsDBNull(Rs_RettaRegione.Item(0)) Then
                ImportoRegioneAssenze = Rs_RettaRegione.Item(0)
            End If
        End If
        Rs_RettaRegione.Close()
        cn.Close()
    End Function


    Private Function ImportoRegione(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, Optional ByVal Stato As String = "") As Double
        Dim MySql As String

        MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGP' OR Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        If ChkAssenza.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGA') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If ChkPresenze.Checked = True Then
            MySql = "Select sum(IMPORTO) From RETTEREGIONE Where (Elemento = 'RGP') AND  Mese = " & Mese & " And Anno = " & Anno & " And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        End If
        If Stato <> "" Then
            MySql = MySql & " AND StatoContabile = '" & Stato & "'"
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim Rs_RettaRegione As OleDbDataReader = cmd.ExecuteReader()
        If Rs_RettaRegione.Read Then
            If Not IsDBNull(Rs_RettaRegione.Item(0)) Then
                ImportoRegione = Rs_RettaRegione.Item(0)
            End If
        End If
        Rs_RettaRegione.Close()
        cn.Close()
    End Function

    Private Function ImportoExtraComune(ByVal CodiceOspite As Long, ByVal DataIn As Date, ByVal ExtT As Long) As Double
        Dim MySql As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From IMPORTOCOMUNE Where CodiceOspite = " & CodiceOspite & " And DATA <= {ts '" & Format(DataIn, "yyyy-MM-dd") & " 00:00:00'} Order By Data Desc"
        cmd.Connection = cn


        Dim MyRs As OleDbDataReader = cmd.ExecuteReader()
        If MyRs.Read Then
            If ExtT = 1 Then
                ImportoExtraComune = campodbn(MyRs.Item("Importo1"))
            End If
            If ExtT = 2 Then
                ImportoExtraComune = campodbn(MyRs.Item("Importo2"))
            End If
            If ExtT = 3 Then
                ImportoExtraComune = campodbn(MyRs.Item("Importo3"))
            End If
            If ExtT = 4 Then
                ImportoExtraComune = campodbn(MyRs.Item("Importo4"))
            End If
        End If
        MyRs.Close()

        cn.Close()
    End Function

    Public Function StringaPresenzeOspite(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal Anno As Integer, ByVal Mese As Integer) As String

        Dim AppR As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From AssenzeCentroDiurno Where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & Cserv & "' And Anno = " & Anno & " And Mese = " & Mese
        cmd.Connection = cn


        StringaPresenzeOspite = ""

        Dim MyRs As OleDbDataReader = cmd.ExecuteReader()
        If MyRs.Read Then
            For I = 1 To 31
                AppR = campodb(MyRs.Item("GIORNO" & Trim(I)))
                If Len(AppR) = 1 Then AppR = " " & AppR
                StringaPresenzeOspite = StringaPresenzeOspite & AppR
            Next I
        End If
        MyRs.Close()

        cn.Close()
    End Function


    Private Function TotaleDaMsql(ByVal Mysql As String) As Integer

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        TotaleDaMsql = 0
        Dim cmd As New OleDbCommand()
        cmd.CommandText = Mysql
        cmd.Connection = cn
        Dim MyRs As OleDbDataReader = cmd.ExecuteReader()
        If MyRs.Read Then
            TotaleDaMsql = Val(campodb(MyRs.Item(0)))
        End If
        MyRs.Close()
        cn.Close()
    End Function

    Private Function OspitePermanente(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal DataFine As Date) As String

        Dim Temporaneo As Boolean

        Dim TxtDataFine As Date
        Dim DataInizio As Date
        Dim MyDatafine As Date


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Temporaneo = False
        TxtDataFine = DataFine

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select top 1 * From IMPEGNATIVE where CodiceOspite = " & CodiceOspite & " And DATAINIZIO <= {ts '" & Format(TxtDataFine, "yyyy-MM-dd") & " 00:00:00'} Order by DATAINIZIO DESC"

        cmd.Connection = cn

        Dim LEGGIrs As OleDbDataReader = cmd.ExecuteReader()
        Do While LEGGIrs.Read
            DataInizio = campodbD(LEGGIrs.Item("DATAINIZIO"))
            MyDatafine = campodbD(LEGGIrs.Item("DATAFINE"))
            If IsDate(MyDatafine) Then
                Temporaneo = True
            End If
        Loop
        LEGGIrs.Close()

        cn.Close()

        If Temporaneo Then
            OspitePermanente = "Temporaneo"
        Else
            OspitePermanente = "Permanente"
        End If

        cn.Close()
    End Function

    Public Function CercaGiornaliere(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, ByVal Riferimento As String) As String
        Dim x As New Cls_CalcoloRette
        Dim ImportoOspite As Double
        Dim ImportoParenti As Double
        Dim ImportoComune As Double
        Dim ImportoRegione As Double
        Dim ImportoJolly As Double
        Dim i As Integer

        x.STRINGACONNESSIONEDB = Session("DC_OSPITE")

        x.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

        Dim KO As New ClsOspite

        KO.Leggi(Session("DC_OSPITE"), CodOsp)        

        ImportoOspite = Math.Round(x.QuoteGiornaliere(Cserv, CodOsp, "O", 0, Now), 2)
        For i = 1 To 10
            ImportoParenti = ImportoParenti + Math.Round(CDbl(x.QuoteGiornaliere(Cserv, CodOsp, "P", i, Now)), 2)
        Next
        ImportoComune = Math.Round(x.QuoteGiornaliere(Cserv, CodOsp, "C", 0, Now), 2)
        ImportoRegione = Math.Round(x.QuoteGiornaliere(Cserv, CodOsp, "R", 0, Now), 2)
        ImportoJolly = Math.Round(x.QuoteGiornaliere(Cserv, CodOsp, "J", 0, Now), 2)
        x.ChiudiDB()

        CercaGiornaliere = " Quota Ospite " & ImportoOspite & " Quota Parenti : " & ImportoParenti & " Quota Comune :" & ImportoComune & " Quota Regione " & ImportoRegione

    End Function


    Public Function CercaAddebitiAccredito(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, ByVal Riferimento As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim VerTotAD As Double
        Dim STRINGA As String
        Dim TipoMov As String
        Dim OldVb6 As New Cls_FunzioniVB6

        If Chk_ADDACR.Checked = False Then
            Return ""
            Exit Function
        End If

        STRINGA = ""


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From ADDACR Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And RIFERIMENTO = '" & Riferimento & "'"
        cmd.Connection = cn
        Dim LEGGIrs As OleDbDataReader = cmd.ExecuteReader()

        Do While LEGGIrs.Read
            If Format(campodbD(LEGGIrs.Item("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And Format(campodbD(LEGGIrs.Item("Data")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                If campodb(LEGGIrs.Item("TipoMov")) = "AD" Then
                    TipoMov = "Addebito"
                Else
                    TipoMov = "Accredito"
                End If
                STRINGA = STRINGA & Format(campodbD(LEGGIrs.Item("Data")), "dd/MM/yyyy") & "  Periodo : " & campodb(LEGGIrs.Item("MESECOMPETENZA")) & "/" & campodb(LEGGIrs.Item("ANNOCOMPETENZA")) & "  Tipo Movimento : " & TipoMov & " " & campodb(LEGGIrs.Item("Descrizione")) & " " & Format(campodbn(LEGGIrs.Item("Importo")), "#,##0.00") & vbNewLine
                VerTotAD = VerTotAD + Math.Abs(campodbn(LEGGIrs.Item("Importo")))
            End If
        Loop
        LEGGIrs.Close()

        If Chk_ACOspitiParenti.Checked = True Then
            Dim cmdACOP As New OleDbCommand()

            cmdACOP.Connection = cn
            cmdACOP.CommandText = "Select * From ADDACR Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And (RIFERIMENTO = 'O' Or RIFERIMENTO = 'P' )"
            Dim LeggiAccrs As OleDbDataReader = cmdACOP.ExecuteReader()
            Do While LeggiAccrs.Read
                If Format(campodbD(LeggiAccrs.Item("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And Format(campodbD(LeggiAccrs.Item("Data")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                    If campodb(LeggiAccrs.Item("TipoMov")) = "AD" Then
                        TipoMov = "Addebito (" & campodb(LeggiAccrs.Item("Riferimento")) & ")"
                    Else
                        TipoMov = "Accredito (" & campodb(LeggiAccrs.Item("Riferimento")) & ")"
                    End If
                    STRINGA = STRINGA & Format(campodbD(LeggiAccrs.Item("Data")), "dd/MM/yyyy") & "  Periodo : " & campodb(LeggiAccrs.Item("MESECOMPETENZA")) & "/" & campodb(LeggiAccrs.Item("ANNOCOMPETENZA")) & "  Tipo Movimento : " & TipoMov & " " & campodb(LeggiAccrs.Item("Descrizione")) & " " & Format(campodbn(LeggiAccrs.Item("Importo")), "#,##0.00") & vbNewLine
                End If
            Loop
            LEGGIrs.Close()
        End If


        Dim cmdRO As New OleDbCommand()

        cmdRO.CommandText = "Select * From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Mese = " & Mese & " And Anno = " & Anno
        cmdRO.Connection = cn
        Dim LeggiRO As OleDbDataReader = cmdRO.ExecuteReader()
        Do While LeggiRO.Read
            If campodb(LeggiRO.Item("ELEMENTO")) <> "" Then
                If Chk_NonExtra.Checked = True Then
                    If Mid(campodb(LeggiRO.Item("ELEMENTO")), 1, 1) = "E" Then
                        STRINGA = STRINGA & "Extra Fisso Ospite : " & Math.Round(campodbn(LeggiRO.Item("Importo")), 2) & " "
                    End If
                End If
            End If
        Loop
        LeggiRO.Close()

        Dim cmdRP As New OleDbCommand()

        cmdRP.CommandText = "Select * From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Mese = " & Mese & " And Anno = " & Anno
        cmdRP.Connection = cn

        Dim LeggiRP As OleDbDataReader = cmdRP.ExecuteReader()
        Do While LeggiRP.Read
            If campodb(LeggiRP.Item("ELEMENTO")) <> "" Then
                If Chk_NonExtra.Checked = True Then
                    If Mid(campodb(LeggiRP.Item("ELEMENTO")), 1, 1) = "E" Then
                        STRINGA = STRINGA & "Extra Fisso Parente : " & Math.Round(campodbn(LeggiRP.Item("Importo")), 2) & " "
                    End If
                End If
            End If
        Loop
        LeggiRP.Close()


        Dim cmdRC As New OleDbCommand()

        cmdRC.CommandText = "Select * From RetteComune Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Mese = " & Mese & " And Anno = " & Anno
        cmdRC.Connection = cn
        Dim LeggiRC As OleDbDataReader = cmdRP.ExecuteReader()
        Do While LeggiRC.Read
            If campodb(LeggiRC.Item("ELEMENTO")) <> "" Then
                If Chk_NonExtra.Checked = True Then
                    If Mid(campodb(LeggiRC.Item("ELEMENTO")), 1, 1) = "E" Then
                        STRINGA = STRINGA & "Extra Fisso Comune : " & campodbn(LeggiRC.Item("Importo")) & " "
                    End If
                End If
                If VerTotAD = 0 Then
                    If campodb(LeggiRC.Item("ELEMENTO")) = "ADD" Then
                        STRINGA = STRINGA & "Addebito Comune : " & campodbn(LeggiRC.Item("Importo")) & " "
                    End If
                    If campodb(LeggiRC.Item("ELEMENTO")) = "ACC" Then
                        STRINGA = STRINGA & "Accredito Comune : " & campodbn(LeggiRC.Item("Importo")) & " "
                    End If
                End If
            End If
        Loop
        LeggiRC.Close()




        Dim cmdRJ As New OleDbCommand()

        cmdRJ.CommandText = "Select * From RetteJolly Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Mese = " & Mese & " And Anno = " & Anno
        cmdRJ.Connection = cn
        Dim LeggiRJ As OleDbDataReader = cmdRP.ExecuteReader()
        Do While LeggiRJ.Read
            If campodb(LeggiRJ.Item("ELEMENTO")) <> "" Then
                If Chk_NonExtra.Checked = True Then
                    If Mid(campodb(LeggiRJ.Item("ELEMENTO")), 1, 1) = "E" Then
                        STRINGA = STRINGA & "Extra Fisso Jolly : " & campodbn(LeggiRJ.Item("Importo")) & " "
                    End If
                End If
                If VerTotAD = 0 Then
                    If campodb(LeggiRJ.Item("ELEMENTO")) = "ADD" Then
                        STRINGA = STRINGA & "Addebito Jolly : " & campodbn(LeggiRJ.Item("Importo")) & " "
                    End If
                    If campodb(LeggiRJ.Item("ELEMENTO")) = "ACC" Then
                        STRINGA = STRINGA & "Accredito Jolly : " & campodbn(LeggiRJ.Item("Importo")) & " "
                    End If
                End If
            End If
        Loop
        LeggiRJ.Close()

        cn.Close()
        CercaAddebitiAccredito = STRINGA
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function CercaTipoRetta(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As String
        Dim LK As New Cls_TipoRetta
        Dim MR As New Cls_rettatotale


        MR.CODICEOSPITE = CodOsp
        MR.CENTROSERVIZIO = Cserv
        MR.UltimaData(Session("DC_OSPITE"), CodOsp, Cserv)

        LK.Tipo = MR.TipoRetta
        LK.Leggi(Session("DC_OSPITE"), LK.Tipo)

        CercaTipoRetta = "Tipo Retta :" & LK.Descrizione
    End Function
    Public Function CercaMovimentiEU(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long, ByRef Uscita1Osp As Date, ByRef Uscita2Osp As Date, ByRef Entrata1Osp As Date, ByRef Entrata2Osp As Date, ByRef UscitaPermesso As Date, ByRef EntrataPermesso As Date) As String

        Dim cn As OleDbConnection


        Dim PresenteUscitaTemp As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Dim TipoUsc As String = ""


        Dim cmdIMPPrec As New OleDbCommand()

        cmdIMPPrec.Connection = cn

        cmdIMPPrec.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And DATA < ? Order by Data"
        cmdIMPPrec.Parameters.AddWithValue("@DATA", DateSerial(Anno, Mese, 1))

        Dim LeggiImpPrec As OleDbDataReader = cmdIMPPrec.ExecuteReader()
        Do While LeggiImpPrec.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImpPrec.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))

            If campodb(LeggiImpPrec.Item("TipoMov")) = "03" Then
                If Tipo.Descrizione.ToUpper.IndexOf("OSPEDA") >= 0 Then            
                    TipoUsc = "O"
                Else
                    'PresenteUscitaTemp = True
                    TipoUsc = ""
                End If

            End If
            If campodb(LeggiImpPrec.Item("TipoMov")) = "04" Then
                TipoUsc = ""
            End If
        Loop
        LeggiImpPrec.Close()


        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn

        cmdIMP.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " Order by Data"

        
        Dim LeggiImp As OleDbDataReader = cmdIMP.ExecuteReader()
        Do While LeggiImp.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImp.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))

            If campodb(LeggiImp.Item("TipoMov")) = "03" Then
                If Tipo.Descrizione.ToUpper.IndexOf("OSPEDA") >= 0 Then
                    If Year(Uscita1Osp) < 2000 Then
                        Uscita1Osp = campodbD(LeggiImp.Item("Data"))
                    Else
                        Uscita2Osp = campodbD(LeggiImp.Item("Data"))
                    End If
                    TipoUsc = "O"
                Else
                    If Not PresenteUscitaTemp Then
                        UscitaPermesso = campodbD(LeggiImp.Item("Data"))
                    Else
                        EntrataPermesso = Nothing
                    End If
                    PresenteUscitaTemp = True
                    TipoUsc = ""
                End If

            End If
            If campodb(LeggiImp.Item("TipoMov")) = "04" Then
                If TipoUsc = "O" Then
                    If Year(Entrata1Osp) < 2000 Then
                        Entrata1Osp = campodbD(LeggiImp.Item("Data"))
                    Else
                        Entrata2Osp = campodbD(LeggiImp.Item("Data"))
                    End If
                Else
                    EntrataPermesso = campodbD(LeggiImp.Item("Data"))
                End If
            End If

        Loop
        LeggiImp.Close()


        cn.Close()


    End Function


    Public Function CercaMovimenti(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As String
        Dim MyRs As New ADODB.Recordset
        Dim STRINGA As String
        Dim MovDes As String


        If Chk_MovEU.Checked = False Then Exit Function

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        STRINGA = ""

        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn


        If Chk_AccoglimentoUscitaDef.Checked = True Then
            cmdIMP.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (TipoMov= '05' Or TipoMov = '13')"
        Else
            cmdIMP.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Month(DATA) = " & Mese & " And Year(DATA) = " & Anno
        End If


        Dim LeggiImp As OleDbDataReader = cmdIMP.ExecuteReader()
        Do While LeggiImp.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImp.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))
            If campodb(LeggiImp.Item("TipoMov")) = "05" Then
                If Trim(campodb(LeggiImp.Item("Causale"))) <> "" Then
                    MovDes = "Accoglimento " & Tipo.Descrizione
                Else
                    MovDes = "Accoglimento "
                End If
            End If
            If campodb(LeggiImp.Item("TipoMov")) = "05" Then
                Dim k As New Cls_rettatotale

                k.CODICEOSPITE = CodOsp
                k.CENTROSERVIZIO = Cserv
                k.Data = campodbD(LeggiImp.Item("Data"))
                k.RettaTotaleAData(Session("DC_OSPITE"), CodOsp, Cserv, k.Data)

                Dim KTR As New Cls_TipoRetta

                KTR.Codice = k.TipoRetta
                KTR.Leggi(Session("DC_OSPITE"), KTR.Codice)

                If KTR.Descrizione = "POSTICIPO ENTRATA" Then
                    Dim kDopo As New Cls_rettatotale

                    kDopo.CODICEOSPITE = CodOsp
                    kDopo.CENTROSERVIZIO = Cserv
                    kDopo.Data = campodbD(LeggiImp.Item("Data"))
                    kDopo.PrimaDataDopoData(Session("DC_OSPITE"), CodOsp, Cserv, k.Data)

                    k.Data = kDopo.Data
                End If
            End If
            If campodb(LeggiImp.Item("TipoMov")) = "13" Then MovDes = "Uscita Definitiva " & Tipo.Descrizione
            If campodb(LeggiImp.Item("TipoMov")) = "03" Then MovDes = "Uscita " & Tipo.Descrizione
            If campodb(LeggiImp.Item("TipoMov")) = "04" Then MovDes = "Entrata"
            If Trim(campodb(LeggiImp.Item("TipoMov"))) <> "" Then STRINGA = STRINGA & MovDes & " " & Format(campodbD(LeggiImp.Item("Data")), "dd/MM/yyyy") & "..."

        Loop
        LeggiImp.Close()


        cn.Close()

        CercaMovimenti = STRINGA
    End Function

    Public Function CercaMovimentiReg(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As String
        Dim MyRs As New ADODB.Recordset
        Dim STRINGA As String
        Dim MovDes As String


        If Chk_MovStato.Checked = False Then Exit Function

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        STRINGA = ""

        Dim StatoC As New Cls_StatoAuto

        StatoC.CODICEOSPITE = CodOsp
        StatoC.CENTROSERVIZIO = Cserv
        StatoC.UltimaData(Session("DC_OSPITE"), StatoC.CODICEOSPITE, StatoC.CENTROSERVIZIO)

        If Year(StatoC.Data) = Anno And Month(StatoC.Data) = Mese Then
            STRINGA = STRINGA & "Accoglimento Usl " & Format(StatoC.Data, "dd/MM/yyyy") & " "
        End If

        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn



        cmdIMP.CommandText = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Month(DATA) = " & Mese & " And Year(DATA) = " & Anno


        Dim LeggiImp As OleDbDataReader = cmdIMP.ExecuteReader()
        Do While LeggiImp.Read
            Dim Tipo As New Cls_CausaliEntrataUscita

            Tipo.Codice = campodb(LeggiImp.Item("Causale"))
            Tipo.LeggiCausale(Session("DC_OSPITE"))
            If Year(StatoC.Data) = Anno And Month(StatoC.Data) = Mese And campodb(LeggiImp.Item("TipoMov")) = "05" Then
            Else
                If campodb(LeggiImp.Item("TipoMov")) = "05" Then
                    If Trim(campodb(LeggiImp.Item("Causale"))) <> "" Then
                        MovDes = "Accoglimento " & Tipo.Descrizione
                    Else
                        MovDes = "Accoglimento "
                    End If
                End If

                If campodb(LeggiImp.Item("TipoMov")) = "13" Then MovDes = "Uscita Definitiva " & Tipo.Descrizione
                If campodb(LeggiImp.Item("TipoMov")) = "03" Then MovDes = "Uscita " & Tipo.Descrizione
                If campodb(LeggiImp.Item("TipoMov")) = "04" Then MovDes = "Entrata"
                If Trim(campodb(LeggiImp.Item("TipoMov"))) <> "" Then STRINGA = STRINGA & MovDes & " " & Format(campodbD(LeggiImp.Item("Data")), "dd/MM/yyyy") & "..."
            End If
        Loop
        LeggiImp.Close()


        cn.Close()

        CercaMovimentiReg = STRINGA
    End Function


    Public Function CercaImpegnativa(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Byte, ByVal Anno As Long) As String
        Dim MyRs As New ADODB.Recordset
        Dim STRINGA As String

        If Chk_MovImp.Checked = False Then
            Return ""
            Exit Function
        End If


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        STRINGA = ""
        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn
        cmdIMP.CommandText = "Select * From IMPEGNATIVE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " Order By  DATAINIZIO Desc,DATAFINE Desc"

        Dim LeggiRJ As OleDbDataReader = cmdIMP.ExecuteReader()
        If LeggiRJ.Read Then
            STRINGA = "Impegnativa : " & campodb(LeggiRJ.Item("Descrizione"))
        End If
        LeggiRJ.Close()

        cn.Close()
        CercaImpegnativa = STRINGA
    End Function

    Private Function DataAccoglimento(ByVal Cserv, ByVal CodOsp, ByVal Mese, ByVal Anno) As Date
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim StDataCond As String
        Dim VbF6 As New Cls_FunzioniVB6

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, VbF6.GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"

        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' And " & StDataCond & " Order By DATA Desc"

        Dim cmdIMP As New OleDbCommand()

        cmdIMP.Connection = cn
        cmdIMP.CommandText = MySql
        Dim RsMovimenti As OleDbDataReader = cmdIMP.ExecuteReader()
        If RsMovimenti.Read Then
            DataAccoglimento = campodbD(RsMovimenti.Item("Data"))
        Else

            MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' Order By DATA Desc"
            Dim cmdMov As New OleDbCommand()

            cmdMov.Connection = cn
            cmdMov.CommandText = MySql

            Dim RsMov As OleDbDataReader = cmdMov.ExecuteReader()
            If RsMov.Read Then
                DataAccoglimento = campodbD(RsMov.Item("Data"))
            End If
            RsMov.Close()
        End If
        RsMovimenti.Close()

        cn.Close()
    End Function


    Private Function DataUscitaDefinitiva(ByVal Cserv, ByVal CodOsp, ByVal Mese, ByVal Anno) As Date
        Dim cn As OleDbConnection
        Dim StDataCond As String
        Dim MySql As String
        Dim OldVb6 As New Cls_FunzioniVB6

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"

        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '13' And " & StDataCond & " Order By DATA Desc"

        Dim cmdMov As New OleDbCommand()

        cmdMov.Connection = cn
        cmdMov.CommandText = MySql

        Dim RsMov As OleDbDataReader = cmdMov.ExecuteReader()
        If RsMov.Read Then
            DataUscitaDefinitiva = campodbD(RsMov.Item("Data"))
        End If

        RsMov.Close()

        cn.Close()
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Call ProcSociale()
        Dim XS As New Cls_Login

        

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))
        'x = Session("stampa")

        If DD_Report.SelectedValue = "" Then            
            If Chk_StampaDownloadPerGruppo.Checked = False Then
                If Chk_SoloRetta.Checked = True And XS.ReportPersonalizzato("RENDICONTOCOMUNESOLORETTA") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNESOLORETTA','StampeRendiconto','width=800,height=600');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNE','StampeRendiconto','width=800,height=600');", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('EstraiPerSociale.aspx','StampeRendiconto','width=800,height=600');", True)
            End If
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "','StampeRendiconto','width=800,height=600');", True)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then Exit Sub



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim MyJS As String


        'MyJS = "$(" & Chr(34) & "#" & Txt_Comune.ClientID & Chr(34) & ").autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"

        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NuovoOspiteLoad", MyJS, True)


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, Nothing)

        Dim Xcs As New Cls_CentroServizio

        Xcs.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)

        Dim XOs As New ClsUSL

        XOs.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)

        DD_MeseDa.Items.Add("Gennaio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 1
        DD_MeseDa.Items.Add("Febbraio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 2
        DD_MeseDa.Items.Add("Marzo")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 3
        DD_MeseDa.Items.Add("Aprile")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 4
        DD_MeseDa.Items.Add("Maggio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 5
        DD_MeseDa.Items.Add("Giugno")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 6
        DD_MeseDa.Items.Add("Luglio")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 7
        DD_MeseDa.Items.Add("Agosto")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 8
        DD_MeseDa.Items.Add("Settembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 9
        DD_MeseDa.Items.Add("Ottobre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 10
        DD_MeseDa.Items.Add("Novembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 11
        DD_MeseDa.Items.Add("Dicembre")
        DD_MeseDa.Items(DD_MeseDa.Items.Count - 1).Value = 12


        DD_MeseA.Items.Add("Gennaio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 1
        DD_MeseA.Items.Add("Febbraio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 2
        DD_MeseA.Items.Add("Marzo")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 3
        DD_MeseA.Items.Add("Aprile")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 4
        DD_MeseA.Items.Add("Maggio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 5
        DD_MeseA.Items.Add("Giugno")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 6
        DD_MeseA.Items.Add("Luglio")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 7
        DD_MeseA.Items.Add("Agosto")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 8
        DD_MeseA.Items.Add("Settembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 9
        DD_MeseA.Items.Add("Ottobre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 10
        DD_MeseA.Items.Add("Novembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 11
        DD_MeseA.Items.Add("Dicembre")
        DD_MeseA.Items(DD_MeseA.Items.Count - 1).Value = 12


        DD_Report.Items.Clear()
        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO1")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO1").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "RENDICONTOPERSONALIZZATO1"
        End If

        If Trim(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "RENDICONTOPERSONALIZZATO2"
        End If

        If Trim(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO3")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO3").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "RENDICONTOPERSONALIZZATO3"
        End If

        If Trim(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO4")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("RENDICONTOPERSONALIZZATO4").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "RENDICONTOPERSONALIZZATO4"
        End If



        Dim k1 As New ClsComune

        k1.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)


        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = k.AnnoFatturazione
        DD_MeseA.SelectedValue = k.MeseFatturazione
        DD_MeseDa.SelectedValue = k.MeseFatturazione

    End Sub



    Private Sub ProcSanitario()
        Dim Selezione As String

        Dim ProvSelezionata As String
        Dim ComuneSelezionata As String

        Dim Extra1 As String
        Dim Extra2 As String
        Dim Extra3 As String
        Dim Extra4 As String

        Dim MySql As String
        Dim TotSql As String



        Dim LastTipo As Integer

        Dim WSocieta As String
        Dim MImportoComune As Double
        Dim GiorniPresenza As Integer
        Dim GiorniAssenza As Integer
        Dim TotMImportoComune As Integer

        Dim MeseIni As Integer
        Dim MeseA As Integer
        Dim Anno As Integer
        Dim Mese As Integer

        Dim PrimaVolta As Boolean
        Dim Cserv As String
        Dim CodiceOspite As Integer
        Dim CreaRecordEta As Boolean

        Dim CreaRecord As Boolean
        Dim CreaRecordNaz As Boolean
        Dim CreaRecordTipUtente As Boolean
        Dim STATOCONTABILE As String

        Dim cn As OleDbConnection

        Dim MImportoOspite As Double
        Dim MImportoParenti As Double
        Dim MImportoRegione As Double
        Dim MImportoRegionePres As Double
        Dim MImportoRegioneAss As Double

        Dim MImportoEnte As Double
        Dim MImportoJolly As Double
        Dim TotMImportoJolly As Double
        Dim TgExtra1 As Double
        Dim TgExtra2 As Double
        Dim TgExtra3 As Double
        Dim TgExtra4 As Double

        Dim dataini As Date
        Dim DataInizio As Date
        Dim dataFine As Date

        Dim Regione As String
        Dim Salto As Boolean
        Dim Stampa As New StampeOspiti
        Dim OldTipoRetta As String = ""



        Dim OldVb6 As New Cls_FunzioniVB6


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        LastTipo = 2



        Dim k As New Cls_DecodificaSocieta


        WSocieta = k.DecodificaSocieta(Session("DC_TABELLE"))


        Selezione = "Data Stampa " & Now & vbNewLine
        Selezione = Selezione & "Periodo dal " & DD_MeseDa.SelectedItem.Text & "/" & Txt_Anno.Text & " al " & DD_MeseA.SelectedItem.Text & "/" & Txt_Anno.Text & vbNewLine
        If Trim(ProvSelezionata) <> "" And Trim(ComuneSelezionata) <> "" Then
            Selezione = Selezione & "Comune di selezione : " & DD_Comune.SelectedItem.Text & vbNewLine
        End If
        If Trim(DD_CServ.SelectedValue) <> "" Then
            Selezione = Selezione & "Centro Servizio " & DD_CServ.SelectedItem.Text & vbNewLine
        End If


        If Chk_ADDACR.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti]" & vbNewLine
        End If
        If Chk_ACOspitiParenti.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti Ospiti Parenti]" & vbNewLine
        End If
        If Chk_MovEU.Checked = True Then
            Selezione = Selezione & "[Movimenti Entrata Uscita]" & vbNewLine
        End If
        If Chk_MovImp.Checked = True Then
            Selezione = Selezione & "[Movimenti Impegnativa]" & vbNewLine
        End If
        If ChkDataIniFinePrestazione.Checked = True Then
            Selezione = Selezione & "[Data Inizio Fine Prestazione]" & vbNewLine
        End If
        If Chk_ImpComEnt.Checked = True Then
            Selezione = Selezione & "[Importo Comune - Ente]" & vbNewLine
        End If
        If Chk_ImpGiornalieri.Checked = True Then
            Selezione = Selezione & "[Importi Giornalieri]" & vbNewLine
        End If
        If Chk_ImportoA0.Checked = True Then
            Selezione = Selezione & "[Importi a Zero]" & vbNewLine
        End If
        If ChkPresenze.Checked = True Then
            Selezione = Selezione & "[Solo Presenze]" & vbNewLine
        End If
        If ChkAssenza.Checked = True Then
            Selezione = Selezione & "[Solo Assenze]" & vbNewLine
        End If
        If Chk_NonAuto.Checked = True Then
            Selezione = Selezione & "[Non Autosufficenti]" & vbNewLine
        End If
        If Chk_Auto.Checked = True Then
            Selezione = Selezione & "[Autosufficenti]" & vbNewLine
        End If
        If Chk_SoloRetta.Checked = True Then
            Selezione = Selezione & "[Solo Retta]" & vbNewLine
        End If
        If Chk_SoloAddebitiAccrediti.Checked = True Then
            Selezione = Selezione & "[Solo Addebiti Accrediti]" & vbNewLine
        End If




        MeseIni = Val(DD_MeseDa.SelectedValue)
        MeseA = Val(DD_MeseA.SelectedValue)
        Anno = Txt_Anno.Text

        For I = MeseIni To MeseA

            Cserv = ""
            CodiceOspite = 0
            STATOCONTABILE = ""
            Regione = ""
            GiorniPresenza = 0
            GiorniAssenza = 0
            MImportoRegione = 0
            MImportoRegioneAss = 0
            MImportoRegionePres = 0

            Mese = I
            Anno = Txt_Anno.Text

            MySql = "SELECT RETTEREGIONE.Giorni,RETTEREGIONE.Importo,RETTEREGIONE.CodiceOspite,RETTEREGIONE.CentroServizio,RETTEREGIONE.Mese,RETTEREGIONE.Anno,RETTEREGIONE.STATOCONTABILE,RETTEREGIONE.REGIONE,RETTEREGIONE.ELEMENTO,RETTEREGIONE.CODICEEXTRA  FROM AnagraficaComune INNER JOIN RETTEREGIONE ON (AnagraficaComune.CodiceRegione = RETTEREGIONE.REGIONE) WHERE ((AnagraficaComune.Periodo = 'M' AND RETTEREGIONE.MESE = " & Mese & " AND RETTEREGIONE.ANNO = " & Anno & ")"

            If Mese = 2 Or Mese = 4 Or Mese = 6 Or Mese = 8 Or Mese = 10 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.Periodo = 'B' AND (RETTEREGIONE.MESE = " & Mese - 1 & " OR RETTEREGIONE.MESE = " & Mese & ") AND RETTEREGIONE.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.Periodo = 'B' AND (RETTEREGIONE.MESE = " & Mese - 1 & " OR RETTEREGIONE.MESE = " & Mese & ") AND RETTEREGIONE.ANNO = " & Anno & ")"
            End If
            If Mese = 3 Or Mese = 6 Or Mese = 9 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.Periodo = 'T' AND (RETTEREGIONE.MESE = " & Mese - 1 & " OR RETTEREGIONE.MESE = " & Mese - 2 & " OR RETTEREGIONE.MESE = " & Mese & ") AND RETTEREGIONE.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.Periodo = 'T' AND (RETTEREGIONE.MESE = " & Mese - 1 & " OR RETTEREGIONE.MESE = " & Mese - 2 & " OR RETTEREGIONE.MESE = " & Mese & ") AND RETTEREGIONE.ANNO = " & Anno & ")"
            End If

            If Mese = 1 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'K' AND ( (RETTEREGIONE.MESE = " & Mese & "  AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 12  AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") or  (RETTEREGIONE.MESE = 11  AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") ))"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'K' AND ( (RETTEREGIONE.MESE = " & Mese & " AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 12 AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") or  (RETTEREGIONE.MESE = 11  AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") ))"
            End If
            If Mese = 2 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'K' AND ( (RETTEREGIONE.MESE = " & Mese & "  AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 1   AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 12  AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") ))"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'K' AND ( (RETTEREGIONE.MESE = " & Mese & " AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 1 AND RETTEREGIONE.ANNO =  " & Anno & ") or  (RETTEREGIONE.MESE = 12  AND RETTEREGIONE.ANNO =  " & Anno - 1 & ") ))"
            End If
            If Mese >= 3 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'K' AND ( RETTEREGIONE.MESE = " & Mese & "  Or RETTEREGIONE.MESE =  " & Mese - 1 & " Or RETTEREGIONE.MESE =  " & Mese - 2 & " ) AND RETTEREGIONE.ANNO =  " & Anno & " )"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'K' AND ( RETTEREGIONE.MESE = " & Mese & "  Or RETTEREGIONE.MESE =  " & Mese - 1 & " Or RETTEREGIONE.MESE =  " & Mese - 2 & " ) AND RETTEREGIONE.ANNO =  " & Anno & " )"
            End If
            
            
            MySql = MySql & ")"
            TotSql = TotSql & " ) "

            If ChkAssenza.Checked = True Then
                MySql = MySql & " And RETTEREGIONE.ELEMENTO = 'RGA' "
                TotSql = TotSql & " And RETTEREGIONE.ELEMENTO = 'RGA' "
            End If
            If ChkPresenze.Checked = True Then
                MySql = MySql & " And RETTEREGIONE.ELEMENTO = 'RGP' "
                TotSql = TotSql & " And RETTEREGIONE.ELEMENTO = 'RGP' "
            End If


            If Chk_SoloRetta.Checked = True Then
                MySql = MySql & " And (RETTEREGIONE.ELEMENTO = 'RGP' OR RETTEREGIONE.ELEMENTO = 'RGA' OR RETTEREGIONE.ELEMENTO = 'RPX') "
                TotSql = TotSql & " And (RETTEREGIONE.ELEMENTO = 'RGP' OR RETTEREGIONE.ELEMENTO = 'RGA' OR RETTEREGIONE.ELEMENTO = 'RPX') "
            End If

            If Chk_SoloAddebitiAccrediti.Checked = True Then
                MySql = MySql & " And (RETTEREGIONE.ELEMENTO = 'ADD' OR RETTEREGIONE.ELEMENTO = 'ACC')"
                TotSql = TotSql & " And (RETTEREGIONE.ELEMENTO = 'ADD' OR RETTEREGIONE.ELEMENTO = 'ACC')"
            End If

            If DD_Regione.SelectedValue <> "" Then
                Dim VrRegione As New ClsUSL

                VrRegione.CodiceRegione = DD_Regione.SelectedValue
                VrRegione.Leggi(Session("DC_OSPITE"))

                If VrRegione.Raggruppamento = "" Then

                    MySql = MySql & " And RETTEREGIONE.REGIONE = '" & DD_Regione.SelectedValue & "' "
                    TotSql = TotSql & " And RETTEREGIONE.REGIONE = '" & DD_Regione.SelectedValue & "' "
                Else
                    Dim Appoggio As String = ""
                    Dim cmdRag As New OleDbCommand()
                    cmdRag.CommandText = "Select CodiceRegione from AnagraficaComune Where Raggruppamento  = ?"
                    cmdRag.Connection = cn
                    cmdRag.Parameters.AddWithValue("@Raggruppamento", DD_Regione.SelectedValue)


                    Dim ReadRag As OleDbDataReader = cmdRag.ExecuteReader()

                    Do While ReadRag.Read
                        If Appoggio <> "" Then
                            Appoggio = Appoggio & " OR "
                        End If
                        Appoggio = Appoggio & " RETTEREGIONE.REGIONE = '" & ReadRag.Item("CodiceRegione") & "'  "
                    Loop
                    ReadRag.Close()
                    If Appoggio <> "" Then
                        MySql = MySql & " And (" & Appoggio & ")"
                        TotSql = TotSql & " And (" & Appoggio & ")"
                    End If
                End If
            End If
            'If Chk_ADDACR.Value = 1 Then
            '  MySql = MySql & " And RETTEREGIONE.ELEMENTO <> 'ADD'  And RETTEREGIONE.ELEMENTO <> 'ACC' "
            'End If

            MySql = MySql & " Order By RETTEREGIONE.REGIONE,RETTEREGIONE.CentroServizio,RETTEREGIONE.CodiceOspite,RETTEREGIONE.Anno,RETTEREGIONE.Mese,RETTEREGIONE.STATOCONTABILE "
            Cserv = ""
            CodiceOspite = 0
            Mese = 0
            Anno = 0
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn


            Dim Rs_RettaRegione As OleDbDataReader = cmd.ExecuteReader()

            Do While Rs_RettaRegione.Read

                Dim MyEseguire As Boolean

                If campodb(Rs_RettaRegione.Item("CodiceOspite")) = 510 Then
                    MyEseguire = True
                End If

                MyEseguire = True

                If Val(Txt_FiltroAnno.Text) > 0 Then
                    If Val(campodb(Rs_RettaRegione.Item("Anno"))) <> Val(Txt_FiltroAnno.Text) Then
                        MyEseguire = False
                    End If
                End If




                If MyEseguire = True Then
                    If (Trim(DD_CServ.SelectedValue) <> "" And campodb(Rs_RettaRegione.Item("CentroServizio")) = DD_CServ.SelectedValue) _
                       Or DD_CServ.SelectedValue = "" Then

                        'If campodb(Rs_RettaRegione.Item("CodiceOspite")) = 478 Then
                        'Threading.Thread.Sleep(10)
                        'End If
                        'campodb(Rs_RettaRegione.Item("STATOCONTABILE")) <> STATOCONTABILE Or _
                        If campodb(Rs_RettaRegione.Item("CentroServizio")) <> Cserv Or _
                           campodb(Rs_RettaRegione.Item("CodiceOspite")) <> CodiceOspite Or _
                           campodb(Rs_RettaRegione.Item("Mese")) <> Mese Or _
                           campodb(Rs_RettaRegione.Item("Anno")) <> Anno Or _
                           campodb(Rs_RettaRegione.Item("CODICEEXTRA")) <> OldTipoRetta Then




                            CreaRecord = True

                            If CreaRecord = True And (MImportoRegione <> 0 Or (Chk_ImportoA0.Checked = True And (GiorniAssenza > 0 Or GiorniPresenza > 0) And Trim(Regione) <> "")) Then
                                MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno)
                                MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno)
                                MImportoComune = ImportoComune(Cserv, CodiceOspite, Mese, Anno)
                                MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)


                                Dim ClCserv As New Cls_CentroServizio

                                ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                                Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoUsl").NewRow

                                Rs_Stampa.Item("CodiceServizio") = Cserv
                                Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE
                                Rs_Stampa.Item("CodiceOspite") = CodiceOspite
                                Rs_Stampa.Item("DueCaratteri") = Mid(Regione, 1, 2)

                                Dim Uscita1Ospedale As Date = Nothing
                                Dim Entrata1Ospedale As Date = Nothing
                                Dim Uscita2Ospedale As Date = Nothing
                                Dim Entrata2Ospedale As Date = Nothing
                                Dim UscitaPermesso As Date = Nothing
                                Dim EntrataPermesso As Date = Nothing


                                CercaMovimentiEU(Cserv, CodiceOspite, Mese, Anno, Uscita1Ospedale, Uscita2Ospedale, Entrata1Ospedale, Entrata2Ospedale, UscitaPermesso, EntrataPermesso)


                                If Year(Uscita1Ospedale) > 2000 Then
                                    Rs_Stampa.Item("Uscita1Osp") = Format(Uscita1Ospedale, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Uscita1Osp") = ""
                                End If


                                If Year(Entrata1Ospedale) > 2000 Then
                                    Rs_Stampa.Item("Entrata1Osp") = Format(Entrata1Ospedale, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Entrata1Osp") = ""
                                End If


                                If Year(Uscita2Ospedale) > 2000 Then
                                    Rs_Stampa.Item("Uscita2Osp") = Format(Entrata1Ospedale, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Uscita2Osp") = ""
                                End If


                                If Year(Entrata2Ospedale) > 2000 Then
                                    Rs_Stampa.Item("Entrata2Osp") = Format(Entrata2Ospedale, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Entrata2Osp") = ""
                                End If

                                If Year(UscitaPermesso) > 2000 Then
                                    Rs_Stampa.Item("Uscita1Permesso") = Format(UscitaPermesso, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Uscita1Permesso") = ""
                                End If


                                If Year(EntrataPermesso) > 2000 Then
                                    Rs_Stampa.Item("Entrata1Permesso") = Format(EntrataPermesso, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("Entrata1Permesso") = ""
                                End If


                                Dim MovimentiNelPeriodo As New Cls_Movimenti
                                Dim DataIniAcco As Date

                                MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                MovimentiNelPeriodo.UltimaDataAccoglimentoConQuotaSanitaria(Session("DC_OSPITE"))

                                DataIniAcco = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")

                                Rs_Stampa.Item("DataAccoglimento") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                                If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                    Rs_Stampa.Item("DataInizio") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("DataInizio") = Format(DateSerial(Anno, Mese, 1), "dd/MM/yyyy")
                                End If

                                MovimentiNelPeriodo.Data = Nothing
                                MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                MovimentiNelPeriodo.UltimaDataUscitaDefinitivaQuotaSanitaria(Session("DC_OSPITE"))
                                Rs_Stampa.Item("MotivoDimissione") = ""

                                Rs_Stampa.Item("DataUscita") = ""
                                If Format(DataIniAcco, "yyyyMMdd") < Format(MovimentiNelPeriodo.Data, "yyyyMMdd") Then
                                    Rs_Stampa.Item("DataUscita") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                                    Rs_Stampa.Item("MotivoDimissione") = MovimentiNelPeriodo.Descrizione
                                End If

                                If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                    Rs_Stampa.Item("DataFine") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                                Else
                                    Rs_Stampa.Item("DataFine") = Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/MM/yyyy")
                                End If



                                'Rs_Stampa.Item("UltimiCaratteri") = Mid(Regione, 3, 2)



                                Rs_Stampa.Item("Regione") = Regione

                                Dim MReg As New Cls_ImportoRegione
                                MReg.Codice = Regione
                                MReg.UltimaData(Session("DC_OSPITE"), Regione, OldTipoRetta)

                                Rs_Stampa.Item("QuotaGiornalieraUSL") = MReg.Importo



                                Dim RettaTotale As New Cls_rettatotale

                                RettaTotale.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                                If RettaTotale.TipoRetta <> "" Then
                                    Dim TipoRetta As New Cls_TipoRetta

                                    TipoRetta.Tipo = RettaTotale.TipoRetta
                                    TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Tipo)
                                    Rs_Stampa.Item("TipoRettaUtente") = TipoRetta.Descrizione
                                End If

                                Dim Listino As New Cls_Listino

                                Listino.CODICEOSPITE = CodiceOspite
                                Listino.CENTROSERVIZIO = Cserv
                                Listino.LeggiAData(Session("DC_OSPITE"), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))


                                Dim ListinoInizo As New Cls_Listino

                                ListinoInizo.CODICEOSPITE = CodiceOspite
                                ListinoInizo.CENTROSERVIZIO = Cserv
                                ListinoInizo.LeggiAData(Session("DC_OSPITE"), DateSerial(Anno, Mese,1))
                                If IsNothing(ListinoInizo.CodiceListino) Then
                                    ListinoInizo.CODICEOSPITE = CodiceOspite
                                    ListinoInizo.CENTROSERVIZIO = Cserv
                                    ListinoInizo.LeggiAData(Session("DC_OSPITE"), Rs_Stampa.Item("DataInizio"))
                                End If

                                If Listino.CodiceListino <> "" And ListinoInizo.CodiceListino<>"" Then
                                    Dim TabListinoInizio As New Cls_Tabella_Listino

                                    TabListinoInizio.Codice = ListinoInizo.CodiceListino
                                    TabListinoInizio.LeggiCausale(Session("DC_OSPITE"))


                                    If OldTipoRetta = TabListinoInizio.TIPORETTAUSL Then
                                        Listino.CodiceListino =TabListinoInizio.Codice
                                    End If
                                End If

                                If Listino.CodiceListino <> "" Then
                                    Dim TabListino As New Cls_Tabella_Listino

                                    TabListino.Codice = Listino.CodiceListino
                                    TabListino.LeggiCausale(Session("DC_OSPITE"))
                                    Rs_Stampa.Item("Listino") = TabListino.Descrizione
                                end if


                                    Dim XOsp As New ClsOspite

                                    XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)

                                    Dim XUsl As New ClsUSL

                                    XUsl.CodiceRegione = Regione
                                    XUsl.Leggi(Session("DC_OSPITE"))

                                    Rs_Stampa.Item("Decodifica") = XUsl.Nome
                                    Rs_Stampa.Item("Attenzione") = XUsl.Attenzione

                                    Rs_Stampa.Item("Raggruppamento") = XUsl.Raggruppamento

                                    If Chk_NomeInVisualizzaioneInUsl.Checked = True Then
                                        Rs_Stampa.Item("Decodifica") = XUsl.Nome & " " & XUsl.NonInVisualizzazione
                                    End If

                                    If Chk_NonIncludereDataNascita.Checked = False Then
                                        Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        End If
                                    Else
                                        Rs_Stampa.Item("Nome") = XOsp.Nome
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                                        End If
                                    End If

                                    Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE

                                    Rs_Stampa.Item("CartellaClinica") = XOsp.CartellaClinica


                                    Dim DocEs As New Cls_MovimentoContabile
                                    Dim NumeroRegistrazione As Integer = 0

                                If XUsl.Raggruppamento = "" Then
                                    NumeroRegistrazione = DocEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), XUsl.MastroCliente, XUsl.ContoCliente, XUsl.SottoContoCliente, Cserv, Anno, Mese, "R" & Regione, Anno, Mese)
                                Else
                                    Dim Ragruppamento As New ClsUSL

                                    Ragruppamento.CodiceRegione = XUsl.Raggruppamento
                                    Ragruppamento.Leggi(Session("DC_OSPITE"))

                                    Rs_Stampa.Item("RaggruppamentoDescrizione") = Ragruppamento.Nome
                                    NumeroRegistrazione = DocEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), Ragruppamento.MastroCliente, Ragruppamento.ContoCliente, Ragruppamento.SottoContoCliente, Cserv, Anno, Mese, "R" & XUsl.Raggruppamento, Anno, Mese)
                                End If
                                DocEs.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)


                                    Rs_Stampa.Item("NumeroDocumento") = DocEs.NumeroProtocollo
                                    Rs_Stampa.Item("DataDocumento") = DocEs.DataRegistrazione


                                    Dim Tab As New Cls_Tabelle

                                    Tab.TipTab = "NAZ"
                                    Tab.Codice = XOsp.Nazionalita
                                    Tab.Leggi(Session("DC_OSPITE"))

                                    Dim MyUltData As New Cls_StatoAuto

                                    MyUltData.CENTROSERVIZIO = Cserv
                                    MyUltData.CODICEOSPITE = CodiceOspite
                                    MyUltData.UltimaDataConUsl(Session("DC_OSPITE"), MyUltData.CODICEOSPITE, MyUltData.CENTROSERVIZIO)

                                    'If Trim(MyUltData.TipoRetta) <> "" Then
                                    If OldTipoRetta <> "" Then
                                        Dim LK As New Cls_TabellaTipoImportoRegione

                                        LK.Codice = OldTipoRetta
                                        Rs_Stampa.Item("UltimiCaratteri") = OldTipoRetta

                                        LK.Leggi(Session("DC_OSPITE"), LK.Codice)
                                        Rs_Stampa.Item("TipoRetta") = LK.Descrizione
                                    End If
                                    'End If




                                    Dim ComRes As New ClsComune

                                    ComRes.Comune = XOsp.RESIDENZACOMUNE1
                                    ComRes.Provincia = XOsp.RESIDENZAPROVINCIA1
                                    ComRes.Leggi(Session("DC_OSPITE"))


                                    Rs_Stampa.Item("ComuneResidenza") = ComRes.Descrizione
                                    Rs_Stampa.Item("IndirizzoOspite") = XOsp.RESIDENZAINDIRIZZO1

                                    Rs_Stampa.Item("CapOspite") = XOsp.RESIDENZACAP1
                                    Rs_Stampa.Item("ProvinciaOspite") = ComRes.CodificaProvincia
                                    Rs_Stampa.Item("MedicoOspite") = XOsp.NomeMedico


                                    ComRes.Descrizione = ""

                                    ComRes.Comune = XOsp.ComuneDiNascita
                                    ComRes.Provincia = XOsp.ProvinciaDiNascita
                                    ComRes.Leggi(Session("DC_OSPITE"))


                                    Rs_Stampa.Item("ComuneNascita") = ComRes.Descrizione


                                    Rs_Stampa.Item("Nazionalita") = Tab.Descrizione
                                    Rs_Stampa.Item("DataNascita") = XOsp.DataNascita
                                    Rs_Stampa.Item("Mese") = Mese
                                    Rs_Stampa.Item("Anno") = Anno
                                    Rs_Stampa.Item("StatoAuto") = STATOCONTABILE
                                    Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                                    Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                                    Rs_Stampa.Item("TotaleRetta") = Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + MImportoComune + MImportoJolly, 2)
                                    Rs_Stampa.Item("Ospite") = Math.Round(MImportoOspite, 2)
                                    Rs_Stampa.Item("Parente") = Math.Round(MImportoParenti, 2)
                                    Rs_Stampa.Item("ImpRegione") = Math.Round(MImportoRegione, 2)
                                    Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune, 2)

                                    Rs_Stampa.Item("ImportoAssenzeUsl") = Math.Round(MImportoRegioneAss, 2)
                                    Rs_Stampa.Item("ImportoPresenzeUsl") = Math.Round(MImportoRegionePres, 2)
                                    Rs_Stampa.Item("ImpJolly") = Math.Round(MImportoJolly, 2)

                                    MySql = "Select sum(Importo) from RetteComune where CodiceOspite = ? And CentroServizio = ? And Anno  = ? And Mese = ? And Elemento like 'RGP'"

                                    Dim cmdRettaPresenze As New OleDbCommand()
                                    cmdRettaPresenze.CommandText = MySql
                                    cmdRettaPresenze.Connection = cn
                                    cmdRettaPresenze.Parameters.AddWithValue("CodiceOspite", CodiceOspite)
                                    cmdRettaPresenze.Parameters.AddWithValue("CentroServizio", Cserv)
                                    cmdRettaPresenze.Parameters.AddWithValue("Anno", Anno)
                                    cmdRettaPresenze.Parameters.AddWithValue("Mese", Mese)
                                    Dim Rs_RR As OleDbDataReader = cmdRettaPresenze.ExecuteReader()
                                    If Rs_RR.Read Then
                                        Rs_Stampa.Item("ImportoPresenzeComune") = Math.Round(campodbn(Rs_RR.Item(0)), 2)
                                    End If
                                    Rs_RR.Close()

                                    MySql = "Select sum(Importo) from RetteComune where CodiceOspite = ? And CentroServizio = ? And Anno  = ? And Mese = ? And Elemento like 'RGA'"

                                    Dim cmdRettaPresenzeP As New OleDbCommand()
                                    cmdRettaPresenzeP.CommandText = MySql
                                    cmdRettaPresenzeP.Connection = cn
                                    cmdRettaPresenzeP.Parameters.AddWithValue("CodiceOspite", CodiceOspite)
                                    cmdRettaPresenzeP.Parameters.AddWithValue("CentroServizio", Cserv)
                                    cmdRettaPresenzeP.Parameters.AddWithValue("Anno", Anno)
                                    cmdRettaPresenzeP.Parameters.AddWithValue("Mese", Mese)
                                    Dim Rs_RA As OleDbDataReader = cmdRettaPresenzeP.ExecuteReader()
                                    If Rs_RA.Read Then
                                        Rs_Stampa.Item("ImportoAssenzeComune") = Math.Round(campodbn(Rs_RA.Item(0)), 2)
                                    End If
                                    Rs_RR.Close()


                                    MySql = "Select sum(Importo) from RetteOspite where CodiceOspite = ? And CentroServizio = ? And Anno  = ? And Mese = ? And Elemento like 'RGP'"

                                    Dim cmdRettaAssenzeO As New OleDbCommand()
                                    cmdRettaAssenzeO.CommandText = MySql
                                    cmdRettaAssenzeO.Connection = cn
                                    cmdRettaAssenzeO.Parameters.AddWithValue("CodiceOspite", CodiceOspite)
                                    cmdRettaAssenzeO.Parameters.AddWithValue("CentroServizio", Cserv)
                                    cmdRettaAssenzeO.Parameters.AddWithValue("Anno", Anno)
                                    cmdRettaAssenzeO.Parameters.AddWithValue("Mese", Mese)
                                    Dim Rs_RPO As OleDbDataReader = cmdRettaAssenzeO.ExecuteReader()
                                    If Rs_RPO.Read Then
                                        Rs_Stampa.Item("ImportoPresenzeOspite") = Math.Round(campodbn(Rs_RPO.Item(0)), 2)
                                    End If
                                    Rs_RPO.Close()

                                    MySql = "Select sum(Importo) from RetteOspite where CodiceOspite = ? And CentroServizio = ? And Anno  = ? And Mese = ? And Elemento like 'RGA'"

                                    Dim cmdRettaAssenzeP As New OleDbCommand()
                                    cmdRettaAssenzeP.CommandText = MySql
                                    cmdRettaAssenzeP.Connection = cn
                                    cmdRettaAssenzeP.Parameters.AddWithValue("CodiceOspite", CodiceOspite)
                                    cmdRettaAssenzeP.Parameters.AddWithValue("CentroServizio", Cserv)
                                    cmdRettaAssenzeP.Parameters.AddWithValue("Anno", Anno)
                                    cmdRettaAssenzeP.Parameters.AddWithValue("Mese", Mese)
                                    Dim Rs_RAO As OleDbDataReader = cmdRettaAssenzeP.ExecuteReader()
                                    If Rs_RAO.Read Then
                                        Rs_Stampa.Item("ImportoAssenzeOspite") = Math.Round(campodbn(Rs_RAO.Item(0)), 2)
                                    End If
                                    Rs_RAO.Close()


                                    If Salto Then
                                        Rs_Stampa.Item("Descrizione") = ""
                                        If Chk_ADDACR.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                                        End If
                                        If Chk_MovEU.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_MovStato.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimentiReg(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_TipoRetta.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_MovImp.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_ImpGiornalieri.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                                        End If

                                    Else
                                        Rs_Stampa.Item("Descrizione") = ""
                                        If Chk_ADDACR.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                                        End If
                                        If Chk_MovEU.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_MovStato.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimentiReg(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_TipoRetta.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_MovImp.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                        End If
                                        If Chk_ImpGiornalieri.Checked = True Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                                        End If
                                    End If

                                    If ChkDataIniFinePrestazione.Checked = True Then
                                        If Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                           Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                            dataini = DataAccoglimento(Cserv, CodiceOspite, Mese, Anno)
                                        Else
                                            dataini = DateSerial(Anno, Mese, 1)
                                        End If
                                        If Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                           Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                            dataFine = DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno)
                                        Else
                                            dataFine = DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno))
                                        End If
                                        If Trim(Rs_Stampa.Item("Descrizione")) <> "" Then
                                            Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & " Date Prestazioni " & Format(dataini, "dd/MM/yyyy") & " Al " & Format(dataFine, "dd/MM/yyyy")
                                        Else
                                            Rs_Stampa.Item("Descrizione") = " Date Prestazioni " & Format(dataini, "dd/MM/yyyy") & " Al " & Format(dataFine, "dd/MM/yyyy")
                                        End If

                                    End If
                                    If campodb(Rs_RettaRegione.Item("CentroServizio")) = Cserv And _
                                       campodb(Rs_RettaRegione.Item("CodiceOspite")) = CodiceOspite Then
                                        If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "RGP" Then
                                            GiorniPresenza = campodb(Rs_RettaRegione.Item("Giorni"))
                                        Else
                                            GiorniAssenza = campodb(Rs_RettaRegione.Item("Giorni"))
                                        End If
                                    End If

                                    Rs_Stampa.Item("IntestaSocieta") = WSocieta
                                    Rs_Stampa.Item("Selezione") = Selezione


                                    Dim XServ As New Cls_CentroServizio

                                    XServ.CENTROSERVIZIO = Cserv
                                    XServ.Leggi(Session("DC_OSPITE"), Cserv)
                                    If XServ.TIPOCENTROSERVIZIO = "D" Then


                                        Dim x As New Cls_Diurno


                                        x.CodiceOspite = CodiceOspite
                                        x.CENTROSERVIZIO = Cserv
                                        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                                        Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                                        Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                                        Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                                        Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                                        Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                                        Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                                        Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                                        Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                                        Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                                        Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                                        Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                                        Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                                        Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                                        Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                                        Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                                        Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                                        Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                                        Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                                        Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                                        Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                                        Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                                        Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                                        Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                                        Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                                        Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                                        Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                                        Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                                        Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                                        Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                                        Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                                        Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31



                                        Rs_Stampa.Item("GiorniPresTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPieno)
                                        Rs_Stampa.Item("GiorniAssTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss)
                                        If XServ.CausaleTempoPienoAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss1)
                                        End If
                                        If XServ.CausaleTempoPienoAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss2)
                                        End If


                                        Rs_Stampa.Item("GiorniPresPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTime)
                                        Rs_Stampa.Item("GiorniAssPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss)
                                        If XServ.CausalePartTimeAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss1)
                                        End If
                                        If XServ.CausalePartTimeAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss2)
                                        End If




                                        Dim sc2 As New MSScriptControl.ScriptControl
                                        Dim SalvaRegione As Double
                                        Dim m As New Cls_CalcoloRette

                                        sc2.Language = "vbscript"

                                        m.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                                        m.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                                        SalvaRegione = m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0)


                                        Rs_Stampa.Item("QuotaGiornalieraOspite") = Format(m.QuoteGiornaliere(Cserv, CodiceOspite, "O", 0, Now), "#,##0.00")
                                        Rs_Stampa.Item("QuotaGiornalieraComune") = Format(m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0, Now), "#,##0.00")


                                        Dim MS As New Cls_CausaliEntrataUscita

                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaRegione = " & Replace(SalvaRegione, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPieno
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaPresTP") = sc2.Eval("SalvaRegione")

                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaRegione, ",", "."))
                                        sc2.ExecuteStatement("SalvaOspite = 0")
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPienoAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaRegione")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaRegione = " & Replace(SalvaRegione, ",", "."))
                                        sc2.ExecuteStatement("SalvaOspite = 0")
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPienoAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaRegione")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaRegione = " & Replace(SalvaRegione, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausalePartTime
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")


                                        Rs_Stampa.Item("QuotaPresPT") = sc2.Eval("SalvaRegione")

                                    Else
                                        Dim Rs_Movimenti As New ADODB.Recordset
                                        Dim Rs_MYMOV As New ADODB.Recordset
                                        Dim MyDataini As Date, MyDatafine As Date
                                        Dim WValore As String = ""
                                        Dim Uscito As Boolean
                                        Dim GGUSCITA As String, GGENTRATA As String
                                        Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                                        Dim Accolto As Boolean
                                        Dim Dimesso As Boolean
                                        Dim GiornoRientro As Integer
                                        Dim IndiceGiorni As Integer

                                        Dim Myserv As New Cls_CentroServizio


                                        Myserv.CENTROSERVIZIO = Cserv
                                        Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                                        Dim KPar As New Cls_Parametri

                                        KPar.LeggiParametri(Session("DC_OSPITE"))

                                        Accolto = False
                                        Dimesso = False
                                        GGUSCITA = KPar.GIORNOUSCITA
                                        GGENTRATA = KPar.GIORNOENTRATA
                                        If GGENTRATA = "P" Then
                                            GiornoRientro = 1
                                        Else
                                            GiornoRientro = 0
                                        End If

                                        MyDataini = DateSerial(Anno, Mese, 1)

                                        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                                        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                                        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                                        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                                        Dim cmdM As New OleDbCommand()
                                        cmdM.CommandText = MySql
                                        cmdM.Connection = cn

                                        Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                                        If ReaderMovimenti.Read() Then
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                                                Uscito = True
                                                WValore = campodb(ReaderMovimenti.Item("Causale"))
                                                'DATAINIZIO = MyDataini
                                                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                                                'If Not Rs_MYMOV.EOF Then
                                                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                                                'End If
                                                'Rs_MYMOV.Close
                                                NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                                            End If
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                    Accolto = True
                                                End If
                                                Uscito = False
                                                NGiorniAssenza = 0
                                            End If
                                        End If
                                        ReaderMovimenti.Close()


                                        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                                        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                                        Dim cmd1 As New OleDbCommand()
                                        cmd1.CommandText = MySql
                                        cmd1.Connection = cn

                                        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                                        GiornoUscita = 1
                                        Do While ReaderMovimenti1.Read
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                                    If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                                        GiornoUscita = 0
                                                    Else
                                                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                        Uscito = True
                                                        NGiorniAssenza = 1
                                                    End If
                                                Else
                                                    GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                    Uscito = True
                                                    NGiorniAssenza = 1
                                                End If
                                            End If
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                                                If Uscito = True Then
                                                    For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                        If IndiceGiorni > 0 Then
                                                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                        End If
                                                        NGiorniAssenza = NGiorniAssenza + 1
                                                    Next IndiceGiorni
                                                End If
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                                                Else
                                                    GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                                                End If
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                                                End If

                                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                                                WValore = "*"
                                                Uscito = True
                                                Dimesso = True
                                                NGiorniAssenza = 2
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                    If IndiceGiorni > 0 Then
                                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                    End If
                                                    NGiorniAssenza = NGiorniAssenza + 1
                                                Next IndiceGiorni
                                                Uscito = False
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                                    If IndiceGiorni > 0 Then
                                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                                    End If
                                                Next IndiceGiorni
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                                    Else
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                                    End If
                                                End If
                                                Uscito = False
                                                Accolto = True
                                            End If

                                        Loop
                                        If Uscito Then
                                            For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                                                If IndiceGiorni > 0 Then
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                End If
                                                NGiorniAssenza = NGiorniAssenza + 1
                                            Next IndiceGiorni
                                        End If
                                        For IndiceGiorni = 27 To 31
                                            If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                                                If IndiceGiorni > 0 Then
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                                End If
                                            End If
                                        Next IndiceGiorni
                                        ReaderMovimenti1.Close()

                                        For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                                        Next

                                    End If

                                    Dim Trascodifiche As String = ""
                                    Dim CodiciInseriti As String = ""
                                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                        Dim CercaCausale As New Cls_CausaliEntrataUscita
                                        Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                                        If PosizioneNewLine > 0 Then
                                            CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                                            If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                                            Else
                                                If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                                    CercaCausale.LeggiCausale(Session("DC_OSPITE"))

                                                    If Trascodifiche <> "" Then
                                                        Trascodifiche = Trascodifiche & ","
                                                    End If

                                                    CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                                    Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                                                End If
                                            End If
                                        End If
                                    Next
                                    Rs_Stampa.Item("Trascodifiche") = Trascodifiche

                                    Stampa.Tables("RendicontoUsl").Rows.Add(Rs_Stampa)
                                End If

                                If Cserv = campodb(Rs_RettaRegione.Item("CentroServizio")) And _
                                  CodiceOspite = campodb(Rs_RettaRegione.Item("CodiceOspite")) Then
                                    Salto = True
                                Else
                                    Salto = False
                                End If

                                Cserv = campodb(Rs_RettaRegione.Item("CentroServizio"))
                                CodiceOspite = campodb(Rs_RettaRegione.Item("CodiceOspite"))
                                Mese = campodb(Rs_RettaRegione.Item("Mese"))
                                Anno = campodb(Rs_RettaRegione.Item("Anno"))
                                STATOCONTABILE = campodb(Rs_RettaRegione.Item("STATOCONTABILE"))

                                Regione = campodb(Rs_RettaRegione.Item("Regione"))
                                If Chk_Raggruppamento.Checked = True Then
                                    Dim MyRegione As New ClsUSL

                                    MyRegione.CodiceRegione = Regione
                                    MyRegione.Leggi(Session("DC_OSPITE"))

                                    If MyRegione.Raggruppamento <> "" Then
                                        Regione = MyRegione.Raggruppamento
                                    End If

                                End If

                                OldTipoRetta = campodb(Rs_RettaRegione.Item("CODICEEXTRA"))

                                GiorniPresenza = 0
                                GiorniAssenza = 0
                                MImportoRegione = 0
                                MImportoRegioneAss = 0
                                MImportoRegionePres = 0
                            End If
                            If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "RGP" Then
                                GiorniPresenza = campodb(Rs_RettaRegione.Item("Giorni"))
                            End If
                            If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "RGA" Then
                                GiorniAssenza = campodb(Rs_RettaRegione.Item("Giorni"))
                            End If
                            If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "RGP" Then
                                MImportoRegionePres = Math.Round(MImportoRegionePres + campodb(Rs_RettaRegione.Item("Importo")), 2)
                            End If
                            If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "RGA" Then
                                MImportoRegioneAss = Math.Round(MImportoRegioneAss + campodb(Rs_RettaRegione.Item("Importo")), 2)
                            End If

                            If campodb(Rs_RettaRegione.Item("ELEMENTO")) = "ACC" Then
                                MImportoRegione = Math.Round(MImportoRegione - campodb(Rs_RettaRegione.Item("Importo")), 2)
                            Else
                                MImportoRegione = Math.Round(MImportoRegione + campodb(Rs_RettaRegione.Item("Importo")), 2)
                            End If
                        End If
                    End If

            Loop


            If MImportoRegione <> 0 Then
                MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno)
                MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno)
                MImportoComune = ImportoComune(Cserv, CodiceOspite, Mese, Anno)
                MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)

                Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoUsl").NewRow

                Rs_Stampa.Item("CodiceServizio") = Cserv


                Dim Uscita1Ospedale As Date
                Dim Entrata1Ospedale As Date
                Dim Uscita2Ospedale As Date
                Dim Entrata2Ospedale As Date
                Dim UscitaPermesso As Date
                Dim EntrataPermesso As Date


                CercaMovimentiEU(Cserv, CodiceOspite, Mese, Anno, Uscita1Ospedale, Uscita2Ospedale, Entrata1Ospedale, Entrata2Ospedale, UscitaPermesso, EntrataPermesso)
                If Year(Uscita1Ospedale) > 2000 Then
                    Rs_Stampa.Item("Uscita1Osp") = Format(Uscita1Ospedale, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Uscita1Osp") = ""
                End If


                If Year(Entrata1Ospedale) > 2000 Then
                    Rs_Stampa.Item("Entrata1Osp") = Format(Entrata1Ospedale, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Entrata1Osp") = ""
                End If


                If Year(Uscita2Ospedale) > 2000 Then
                    Rs_Stampa.Item("Uscita2Osp") = Format(Entrata1Ospedale, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Uscita2Osp") = ""
                End If


                If Year(Entrata2Ospedale) > 2000 Then
                    Rs_Stampa.Item("Entrata2Osp") = Format(Entrata2Ospedale, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Entrata2Osp") = ""
                End If

                If Year(UscitaPermesso) > 2000 Then
                    Rs_Stampa.Item("Uscita1Permesso") = Format(UscitaPermesso, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Uscita1Permesso") = ""
                End If


                If Year(EntrataPermesso) > 2000 Then
                    Rs_Stampa.Item("Entrata1Permesso") = Format(EntrataPermesso, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("Entrata1Permesso") = ""
                End If


                Dim ClCserv As New Cls_CentroServizio

                ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE
                Rs_Stampa.Item("CodiceOspite") = CodiceOspite



                Rs_Stampa.Item("DueCaratteri") = Mid(Regione, 1, 2)
                'Rs_Stampa.Item("UltimiCaratteri") = Mid(Regione, 3, 2)

                Rs_Stampa.Item("DueCaratteri") = Mid(Regione, 1, 2)

                Dim MReg As New Cls_ImportoRegione
                MReg.Codice = Regione
                MReg.UltimaData(Session("DC_OSPITE"), Regione, OldTipoRetta)

                Rs_Stampa.Item("QuotaGiornalieraUSL") = MReg.Importo


                Rs_Stampa.Item("Regione") = Regione



                Dim RettaTotale As New Cls_rettatotale

                RettaTotale.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                If RettaTotale.TipoRetta <> "" Then
                    Dim TipoRetta As New Cls_TipoRetta

                    TipoRetta.Tipo = RettaTotale.TipoRetta
                    TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Tipo)
                    Rs_Stampa.Item("TipoRettaUtente") = TipoRetta.Descrizione
                End If

                Dim Listino As New Cls_Listino

                Listino.CODICEOSPITE = CodiceOspite
                Listino.CENTROSERVIZIO = Cserv
                Listino.LeggiAData(Session("DC_OSPITE"), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

                If Listino.CodiceListino <> "" Then
                    Dim TabListino As New Cls_Tabella_Listino

                    TabListino.Codice = Listino.CodiceListino
                    TabListino.LeggiCausale(Session("DC_OSPITE"))
                    Rs_Stampa.Item("Listino") = TabListino.Descrizione
                End If



                Dim XOsp As New ClsOspite

                XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)

                Dim XUsl As New ClsUSL

                XUsl.CodiceRegione = Regione
                XUsl.Leggi(Session("DC_OSPITE"))

                Rs_Stampa.Item("Decodifica") = XUsl.Nome
                Rs_Stampa.Item("Attenzione") = XUsl.Attenzione

                Rs_Stampa.Item("Raggruppamento") = XUsl.Raggruppamento

                Dim NomeRaggruppamento As New ClsUSL

                NomeRaggruppamento.CodiceRegione = XUsl.Raggruppamento
                NomeRaggruppamento.Leggi(Session("DC_OSPITE"))
                Rs_Stampa.Item("RaggruppamentoDescrizione") = NomeRaggruppamento.Nome
                

                If Chk_NomeInVisualizzaioneInUsl.Checked = True Then
                    Rs_Stampa.Item("Decodifica") = XUsl.Nome & " " & XUsl.NonInVisualizzazione
                End If

                If Chk_NonIncludereDataNascita.Checked = False Then
                    Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                    If Chk_SoloIniziali.Checked = True Then
                        Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                    End If
                Else
                    Rs_Stampa.Item("Nome") = XOsp.Nome
                    If Chk_SoloIniziali.Checked = True Then
                        Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                    End If
                End If

                Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE

                Rs_Stampa.Item("CartellaClinica") = XOsp.CartellaClinica



                Dim DocEs As New Cls_MovimentoContabile
                Dim NumeroRegistrazione As Integer = 0


                If XUsl.Raggruppamento = "" Then
                    NumeroRegistrazione = DocEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), XUsl.MastroCliente, XUsl.ContoCliente, XUsl.SottoContoCliente, Cserv, Anno, Mese, "R" & Regione, Anno, Mese)
                Else
                    Dim Ragruppamento As New ClsUSL

                    Ragruppamento.CodiceRegione = XUsl.Raggruppamento
                    Ragruppamento.Leggi(Session("DC_OSPITE"))

                    NumeroRegistrazione = DocEs.EsisteDocumentoRendiNumero(Session("DC_GENERALE"), Ragruppamento.MastroCliente, Ragruppamento.ContoCliente, Ragruppamento.SottoContoCliente, Cserv, Anno, Mese, "R" & XUsl.Raggruppamento, Anno, Mese)
                End If
                DocEs.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)


                Rs_Stampa.Item("NumeroDocumento") = DocEs.NumeroProtocollo
                Rs_Stampa.Item("DataDocumento") = DocEs.DataRegistrazione


                Dim Tab As New Cls_Tabelle

                Tab.TipTab = "NAZ"
                Tab.Codice = XOsp.Nazionalita
                Tab.Leggi(Session("DC_OSPITE"))

                Rs_Stampa.Item("Nazionalita") = Tab.Descrizione

                Dim MyUltData As New Cls_StatoAuto

                MyUltData.CENTROSERVIZIO = Cserv
                MyUltData.CODICEOSPITE = CodiceOspite
                MyUltData.USL = Regione
                MyUltData.UltimaDataConUslIndicata(Session("DC_OSPITE"), MyUltData.CODICEOSPITE, MyUltData.CENTROSERVIZIO, MyUltData.USL)

                If Trim(MyUltData.TipoRetta) <> "" Then
                    Dim LK As New Cls_TabellaTipoImportoRegione

                    LK.Codice = MyUltData.TipoRetta
                    LK.Leggi(Session("DC_OSPITE"), LK.Codice)
                    Rs_Stampa.Item("TipoRetta") = LK.Descrizione
                    Rs_Stampa.Item("UltimiCaratteri") = MyUltData.TipoRetta
                End If

                Dim ComRes As New ClsComune

                ComRes.Comune = XOsp.RESIDENZACOMUNE1
                ComRes.Provincia = XOsp.RESIDENZAPROVINCIA1
                ComRes.Leggi(Session("DC_OSPITE"))


                Rs_Stampa.Item("ComuneResidenza") = ComRes.Descrizione
                Rs_Stampa.Item("IndirizzoOspite") = XOsp.RESIDENZAINDIRIZZO1

                Rs_Stampa.Item("CapOspite") = XOsp.RESIDENZACAP1
                Rs_Stampa.Item("ProvinciaOspite") = ComRes.CodificaProvincia
                Rs_Stampa.Item("MedicoOspite") = XOsp.NomeMedico



                ComRes.Descrizione = ""

                ComRes.Comune = XOsp.ComuneDiNascita
                ComRes.Provincia = XOsp.ProvinciaDiNascita
                ComRes.Leggi(Session("DC_OSPITE"))


                Rs_Stampa.Item("ComuneNascita") = ComRes.Descrizione


                Dim MovimentiNelPeriodo As New Cls_Movimenti
                Dim DataIniAcco As Date

                MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                MovimentiNelPeriodo.UltimaDataAccoglimentoConQuotaSanitaria(Session("DC_OSPITE"))

                DataIniAcco = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")

                Rs_Stampa.Item("DataAccoglimento") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                    Rs_Stampa.Item("DataInizio") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("DataInizio") = Format(DateSerial(Anno, Mese, 1), "dd/MM/yyyy")
                End If

                MovimentiNelPeriodo.Data = Nothing
                MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                MovimentiNelPeriodo.UltimaDataUscitaDefinitivaQuotaSanitaria(Session("DC_OSPITE"))
                Rs_Stampa.Item("MotivoDimissione") = ""

                Rs_Stampa.Item("DataUscita") = ""
                If Format(DataIniAcco, "yyyyMMdd") < Format(MovimentiNelPeriodo.Data, "yyyyMMdd") Then
                    Rs_Stampa.Item("DataUscita") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                    Rs_Stampa.Item("MotivoDimissione") = MovimentiNelPeriodo.Descrizione
                End If

                If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                    Rs_Stampa.Item("DataFine") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")
                Else
                    Rs_Stampa.Item("DataFine") = Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/MM/yyyy")
                End If

                Rs_Stampa.Item("DataNascita") = XOsp.DataNascita
                Rs_Stampa.Item("Mese") = Mese
                Rs_Stampa.Item("Anno") = Anno
                Rs_Stampa.Item("StatoAuto") = STATOCONTABILE
                Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                Rs_Stampa.Item("TotaleRetta") = Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + MImportoComune + MImportoJolly, 2)

                Rs_Stampa.Item("Ospite") = Math.Round(MImportoOspite, 2)

                Rs_Stampa.Item("Parente") = Math.Round(MImportoParenti, 2)

                Rs_Stampa.Item("ImpRegione") = Math.Round(MImportoRegione, 2)
                Rs_Stampa.Item("ImportoAssenzeUsl") = Math.Round(MImportoRegioneAss, 2)
                Rs_Stampa.Item("ImportoPresenzeUsl") = Math.Round(MImportoRegionePres, 2)




                Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune, 2)


                Rs_Stampa.Item("ImpJolly") = Math.Round(MImportoJolly, 2)



                If Chk_ADDACR.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                End If
                If Chk_MovEU.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                End If

                If Chk_MovStato.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimentiReg(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                End If

                If Chk_TipoRetta.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                End If
                If Chk_MovImp.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                End If
                If Chk_ImpGiornalieri.Checked = True Then
                    Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                End If


                Rs_Stampa.Item("IntestaSocieta") = WSocieta
                Rs_Stampa.Item("Selezione") = Selezione

                Dim XServ As New Cls_CentroServizio

                XServ.CENTROSERVIZIO = Cserv
                XServ.Leggi(Session("DC_OSPITE"), Cserv)
                If XServ.TIPOCENTROSERVIZIO = "D" Then


                    Dim x As New Cls_Diurno


                    x.CodiceOspite = CodiceOspite
                    x.CENTROSERVIZIO = Cserv
                    x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                    Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                    Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                    Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                    Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                    Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                    Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                    Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                    Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                    Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                    Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                    Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                    Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                    Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                    Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                    Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                    Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                    Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                    Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                    Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                    Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                    Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                    Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                    Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                    Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                    Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                    Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                    Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                    Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                    Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                    Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                    Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31
                Else
                    Dim Rs_Movimenti As New ADODB.Recordset
                    Dim Rs_MYMOV As New ADODB.Recordset
                    Dim MyDataini As Date, MyDatafine As Date
                    Dim WValore As String = ""
                    Dim Uscito As Boolean
                    Dim GGUSCITA As String, GGENTRATA As String
                    Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                    Dim Accolto As Boolean
                    Dim Dimesso As Boolean
                    Dim GiornoRientro As Integer
                    Dim IndiceGiorni As Integer

                    Dim Myserv As New Cls_CentroServizio


                    Myserv.CENTROSERVIZIO = Cserv
                    Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                    Dim KPar As New Cls_Parametri

                    KPar.LeggiParametri(Session("DC_OSPITE"))

                    Accolto = False
                    Dimesso = False
                    GGUSCITA = KPar.GIORNOUSCITA
                    GGENTRATA = KPar.GIORNOENTRATA
                    If GGENTRATA = "P" Then
                        GiornoRientro = 1
                    Else
                        GiornoRientro = 0
                    End If

                    MyDataini = DateSerial(Anno, Mese, 1)

                    MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                    MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                    REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                    MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                    Dim cmdM As New OleDbCommand()
                    cmdM.CommandText = MySql
                    cmdM.Connection = cn

                    Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                    If ReaderMovimenti.Read() Then
                        If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                            Uscito = True
                            WValore = campodb(ReaderMovimenti.Item("Causale"))
                            'DATAINIZIO = MyDataini
                            'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                            'If Not Rs_MYMOV.EOF Then
                            '   DATAINIZIO = Rs_MYMOV.Fields("Data
                            'End If
                            'Rs_MYMOV.Close
                            NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                        End If
                        If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                            If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                Accolto = True
                            End If
                            Uscito = False
                            NGiorniAssenza = 0
                        End If
                    End If
                    ReaderMovimenti.Close()


                    MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                    MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                    Dim cmd1 As New OleDbCommand()
                    cmd1.CommandText = MySql
                    cmd1.Connection = cn

                    Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                    GiornoUscita = 1
                    Do While ReaderMovimenti1.Read
                        If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                            If GGUSCITA = "P" Then
                                GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                    GiornoUscita = 0
                                Else
                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                    Uscito = True
                                    NGiorniAssenza = 1
                                End If
                            Else
                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                Uscito = True
                                NGiorniAssenza = 1
                            End If
                        End If
                        If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                            If Uscito = True Then
                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                    If IndiceGiorni > 0 Then
                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                    End If
                                    NGiorniAssenza = NGiorniAssenza + 1
                                Next IndiceGiorni
                            End If
                            If GGUSCITA = "P" Then
                                GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                            Else
                                GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                            End If
                            If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                            Else
                                Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                            End If

                            GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                            WValore = "*"
                            Uscito = True
                            Dimesso = True
                            NGiorniAssenza = 2
                        End If

                        If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                            For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                If IndiceGiorni > 0 Then
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                End If
                                NGiorniAssenza = NGiorniAssenza + 1
                            Next IndiceGiorni
                            Uscito = False
                        End If

                        If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                            For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                If IndiceGiorni > 0 Then
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                End If
                            Next IndiceGiorni
                            If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                            Else
                                If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                Else
                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                End If
                            End If
                            Uscito = False
                            Accolto = True
                        End If

                    Loop
                    If Uscito Then
                        For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                            If IndiceGiorni > 0 Then
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                            End If
                            NGiorniAssenza = NGiorniAssenza + 1
                        Next IndiceGiorni
                    End If
                    For IndiceGiorni = 27 To 31
                        If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                            If IndiceGiorni > 0 Then
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                            End If
                        End If
                    Next IndiceGiorni
                    ReaderMovimenti1.Close()

                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                    Next

                End If

                Dim Trascodifiche As String = ""
                Dim CodiciInseriti As String = ""
                For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                    Dim CercaCausale As New Cls_CausaliEntrataUscita
                    Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                    If PosizioneNewLine > 0 Then
                        CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                        If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                        Else
                            If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                CercaCausale.LeggiCausale(Session("DC_OSPITE"))

                                If Trascodifiche <> "" Then
                                    Trascodifiche = Trascodifiche & ","
                                End If

                                CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                            End If
                        End If
                    End If
                Next
                Rs_Stampa.Item("Trascodifiche") = Trascodifiche

                Stampa.Tables("RendicontoUsl").Rows.Add(Rs_Stampa)
            End If
            Rs_RettaRegione.Close()
        Next

        Dim RinumeraRs As New ADODB.Recordset
        Dim Numero As Integer
        Dim ApDueCaratteri As String
        Dim ApUltimiCaratteri As String

        'Dim foundRowsRM As System.Data.DataRow()

        Dim foundRowsT As System.Data.DataRow()
        foundRowsT = Stampa.Tables("RendicontoUsl").Select("", "DueCaratteri,UltimiCaratteri,Nome")

        If foundRowsT.Length > 0 Then
            ApDueCaratteri = foundRowsT(0).Item("DueCaratteri")
            ApUltimiCaratteri = campodb(foundRowsT(0).Item("UltimiCaratteri"))
        End If

        For MInd = 0 To foundRowsT.Length - 1
            If ApDueCaratteri <> foundRowsT(MInd).Item("DueCaratteri") Then
                Numero = 0
            End If
            Numero = Numero + 1

            foundRowsT(MInd).Item("Numero") = Numero

            ApDueCaratteri = foundRowsT(MInd).Item("DueCaratteri")
            ApUltimiCaratteri = campodb(foundRowsT(MInd).Item("UltimiCaratteri"))

        Next MInd


        'Stampa.Tables("RendicontoUsl").Clear()

        'For Mind = 0 To foundRowsT.Length - 1

        '    Dim Riga As System.Data.DataRow = Stampa.Tables("RendicontoUsl").NewRow

        '    For Numero = 0 To Stampa.Tables("RendicontoUsl").Columns.Count - 1
        '        Riga(Numero) = foundRowsT(Mind).Item(Numero)
        '    Next
        '    Stampa.Tables("RendicontoUsl").Rows.Add(Riga)

        'Next



        Session("stampa") = Stampa
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Call ProcSanitario()

        If DD_Report.SelectedValue = "" Then
            If Chk_StampaDownloadPerGruppo.Checked = False Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RENDICONTOREGIONE','StampeRendiconto','width=800,height=600');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('EstraiPerUSLSanitario.aspx','StampeRendiconto','width=800,height=600');", True)
            End If
        Else
            If Chk_StampaDownloadPerGruppo.Checked = False Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "','StampeRendiconto','width=800,height=600');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('EstraiPerUSLSanitario.aspx?REPORT=" & DD_Report.SelectedValue & "','StampeRendiconto','width=800,height=600');", True)
            End If

        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Chk_ACOspitiParenti_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_ACOspitiParenti.CheckedChanged
        If Chk_ACOspitiParenti.Checked = True And Chk_ADDACR.Checked = False Then
            Chk_ACOspitiParenti.Checked = False
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi selezionare addebiti accrediti');", True)
            Exit Sub
        End If

    End Sub

    Protected Sub Chk_NonExtra_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_NonExtra.CheckedChanged
        If Chk_NonExtra.Checked = True And Chk_ADDACR.Checked = False Then
            Chk_NonExtra.Checked = False
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi selezionare addebiti accrediti');", True)
            Exit Sub
        End If
    End Sub



    Private Sub ProcJolly()

        Dim Selezione As String

        Dim ProvSelezionata As String
        Dim ComuneSelezionata As String

        Dim Extra1 As String
        Dim Extra2 As String
        Dim Extra3 As String
        Dim Extra4 As String

        Dim MySql As String
        Dim TotSql As String



        Dim LastTipo As Integer

        Dim WSocieta As String
        Dim MImportoComune As Double
        Dim GiorniPresenza As Integer
        Dim GiorniAssenza As Integer
        Dim TotMImportoComune As Double

        Dim MeseIni As Integer
        Dim MeseA As Integer
        Dim Anno As Integer
        Dim Mese As Integer

        Dim PrimaVolta As Boolean
        Dim Cserv As String
        Dim CodiceOspite As Integer
        Dim CreaRecordEta As Boolean

        Dim CreaRecord As Boolean
        Dim CreaRecordNaz As Boolean
        Dim CreaRecordTipUtente As Boolean
        Dim STATOCONTABILE As String

        Dim cn As OleDbConnection

        Dim MImportoOspite As Double
        Dim MImportoParenti As Double
        Dim MImportoRegione As Double
        Dim MImportoEnte As Double
        Dim MImportoJolly As Double
        Dim TotMImportoJolly As Double

        Dim ImportoAssenza As Double
        Dim TgExtra1 As Double
        Dim TgExtra2 As Double
        Dim TgExtra3 As Double
        Dim TgExtra4 As Double

        Dim dataini As Date
        Dim DataInizio As Date
        Dim dataFine As Date

        Dim Provincia As String
        Dim Comune As String
        Dim Salto As Boolean
        Dim Stampa As New StampeOspiti

        Dim Param As New Cls_Parametri







        Dim OldVb6 As New Cls_FunzioniVB6


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim VettoreNS(100) As String

        VettoreNS = SplitWords(DD_Comune.SelectedValue)
        ProvSelezionata = ""
        ComuneSelezionata = ""
        If VettoreNS.Length > 1 Then
            ComuneSelezionata = VettoreNS(1)
            ProvSelezionata = VettoreNS(0)
        End If




        Selezione = "Data Stampa " & Now & vbNewLine
        Selezione = Selezione & "Periodo dal " & DD_MeseDa.SelectedItem.Text & "/" & Txt_Anno.Text & " al " & DD_MeseA.SelectedItem.Text & "/" & Txt_Anno.Text & vbNewLine
        If Trim(ProvSelezionata) <> "" And Trim(ComuneSelezionata) <> "" Then
            Selezione = Selezione & "Comune di selezione : " & DD_Comune.SelectedValue & vbNewLine
        End If
        If Trim(DD_CServ.SelectedValue) <> "" Then
            Selezione = Selezione & "Centro Servizio " & DD_CServ.SelectedItem.Text & vbNewLine
        End If


        If Chk_ADDACR.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti]" & vbNewLine
        End If
        If Chk_ACOspitiParenti.Checked = True Then
            Selezione = Selezione & "[Addebiti Accrediti Ospiti Parenti]" & vbNewLine
        End If
        If Chk_MovEU.Checked = True Then
            Selezione = Selezione & "[Movimenti Entrata Uscita]" & vbNewLine
        End If
        If Chk_MovImp.Checked = True Then
            Selezione = Selezione & "[Movimenti Impegnativa]" & vbNewLine
        End If
        If ChkDataIniFinePrestazione.Checked = True Then
            Selezione = Selezione & "[Data Inizio Fine Prestazione]" & vbNewLine
        End If
        If Chk_ImpComEnt.Checked = True Then
            Selezione = Selezione & "[Importo Comune - Ente]" & vbNewLine
        End If
        If Chk_ImpGiornalieri.Checked = True Then
            Selezione = Selezione & "[Importi Giornalieri]" & vbNewLine
        End If
        If Chk_ImportoA0.Checked = True Then
            Selezione = Selezione & "[Importi a Zero]" & vbNewLine
        End If
        If ChkPresenze.Checked = True Then
            Selezione = Selezione & "[Solo Presenze]" & vbNewLine
        End If
        If ChkAssenza.Checked = True Then
            Selezione = Selezione & "[Solo Assenze]" & vbNewLine
        End If
        If Chk_NonAuto.Checked = True Then
            Selezione = Selezione & "[Non Autosufficenti]" & vbNewLine
        End If
        If Chk_Auto.Checked = True Then
            Selezione = Selezione & "[Autosufficenti]" & vbNewLine
        End If
        If Chk_SoloRetta.Checked = True Then
            Selezione = Selezione & "[Solo Retta]" & vbNewLine
        End If
        If Chk_SoloAddebitiAccrediti.Checked = True Then
            Selezione = Selezione & "[Solo Addebiti Accrediti]" & vbNewLine
        End If


        Extra1 = ""
        Extra2 = ""
        Extra3 = ""
        Extra4 = ""
        LastTipo = 1



        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From CAUSALI"
        cmdRd.Connection = cn

        Dim RsCausale As OleDbDataReader = cmdRd.ExecuteReader()
        Do While RsCausale.Read

            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto1C") > 0 Then
                Extra1 = Extra1 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto2C") > 0 Then
                Extra2 = Extra2 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto3C") > 0 Then
                Extra3 = Extra3 & campodb(RsCausale.Item("Codice"))
            End If
            If InStr(1, campodb(RsCausale.Item("Regole")), "XSalvaExtImporto4C") > 0 Then
                Extra4 = Extra4 & campodb(RsCausale.Item("Codice"))
            End If

        Loop
        RsCausale.Close()


        Dim k As New Cls_DecodificaSocieta


        WSocieta = k.DecodificaSocieta(Session("DC_TABELLE"))
        MImportoComune = 0
        GiorniPresenza = 0
        GiorniAssenza = 0
        TotMImportoComune = 0
        ImportoAssenza = 0
        MeseIni = Val(DD_MeseDa.SelectedValue)
        MeseA = Val(DD_MeseA.SelectedValue)
        Anno = Txt_Anno.Text

        For I = MeseIni To MeseA
            Mese = I

            MySql = "SELECT RETTEJOLLY.Giorni,RETTEJOLLY.Importo,RETTEJOLLY.CodiceOspite,RETTEJOLLY.CentroServizio,RETTEJOLLY.Mese,RETTEJOLLY.Anno,RETTEJOLLY.Provincia,RETTEJOLLY.Comune,RETTEJOLLY.ELEMENTO FROM AnagraficaComune INNER JOIN RETTEJOLLY ON (AnagraficaComune.CodiceComune = RETTEJOLLY.COMUNE) AND (AnagraficaComune.CodiceProvincia = RETTEJOLLY.PROVINCIA) WHERE ((RETTEJOLLY.MESE = " & Mese & " AND RETTEJOLLY.ANNO = " & Anno & " AND AnagraficaComune.PERIODO = 'M') "

            If Mese = 2 Or Mese = 4 Or Mese = 6 Or Mese = 8 Or Mese = 10 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'B' AND (RETTEJOLLY.MESE = " & Mese - 1 & " OR RETTEJOLLY.MESE = " & Mese & "  ) AND RETTEJOLLY.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'B' AND (RETTEJOLLY.MESE = " & Mese - 1 & " OR RETTEJOLLY.MESE = " & Mese & "  ) AND RETTEJOLLY.ANNO = " & Anno & ")"
            End If
            If Mese = 3 Or Mese = 6 Or Mese = 9 Or Mese = 12 Then
                MySql = MySql & " OR (AnagraficaComune.PERIODO = 'T' AND (RETTEJOLLY.MESE = " & Mese - 1 & " OR RETTEJOLLY.MESE = " & Mese - 2 & " OR RETTEJOLLY.MESE = " & Mese & ") AND RETTEJOLLY.ANNO = " & Anno & ")"
                TotSql = TotSql & " OR (AnagraficaComune.PERIODO = 'T' AND (RETTEJOLLY.MESE = " & Mese - 1 & " OR RETTEJOLLY.MESE = " & Mese - 2 & " OR RETTEJOLLY.MESE = " & Mese & ") AND RETTEJOLLY.ANNO = " & Anno & ")"
            End If


            MySql = MySql & " )"
            TotSql = TotSql & " )"




            If Trim(ProvSelezionata) <> "" Then
                MySql = MySql & " And RETTEJOLLY.PROVINCIA = '" & ProvSelezionata & "' And RETTEJOLLY.COMUNE = '" & ComuneSelezionata & "' "
                TotSql = TotSql & " And RETTEJOLLY.PROVINCIA = '" & ProvSelezionata & "' And RETTEJOLLY.COMUNE = '" & ComuneSelezionata & "' "
            End If


            If ChkAssenza.Checked = True Then
                MySql = MySql & " And RETTEJOLLY.ELEMENTO = 'RGA' "
                TotSql = TotSql & " And RETTEJOLLY.ELEMENTO = 'RGA' "
            End If
            If ChkPresenze.Checked = True Then
                TotSql = TotSql & " And RETTEJOLLY.ELEMENTO = 'RGP' "
            End If

            If Chk_SoloRetta.Checked = True Then
                MySql = MySql & " And (RETTEJOLLY.ELEMENTO = 'RGP' OR RETTEJOLLY.ELEMENTO = 'RGA' OR RETTEJOLLY.ELEMENTO = 'RPX') "
                TotSql = TotSql & " And (RETTEJOLLY.ELEMENTO = 'RGP' OR RETTEJOLLY.ELEMENTO = 'RGA' OR RETTEJOLLY.ELEMENTO = 'RPX') "
            End If

            If Chk_SoloAddebitiAccrediti.Checked = True Then
                MySql = MySql & " And (RETTEJOLLY.ELEMENTO = 'ADD' OR RETTEJOLLY.ELEMENTO = 'ACC')"
                TotSql = TotSql & " And (RETTEJOLLY.ELEMENTO = 'ADD' OR RETTEJOLLY.ELEMENTO = 'ACC')"
            End If



            MySql = MySql & " Order By RETTEJOLLY.PROVINCIA,RETTEJOLLY.Comune,RETTEJOLLY.CentroServizio,RETTEJOLLY.CodiceOspite,RETTEJOLLY.Mese,RETTEJOLLY.Anno"


            PrimaVolta = True
            Cserv = ""
            CodiceOspite = 0
            'Mese = 0
            'Anno = 0
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn


            Dim Rs_RettaComune As OleDbDataReader = cmd.ExecuteReader()

            Do While Rs_RettaComune.Read
                Dim MyEseguire As Boolean = True

                MyEseguire = True

                If Val(Txt_FiltroAnno.Text) > 0 Then
                    If Val(campodb(Rs_RettaComune.Item("Anno"))) <> Val(Txt_FiltroAnno.Text) Then
                        MyEseguire = False
                    End If
                End If

                If campodbn(Rs_RettaComune.Item("CodiceOspite")) = 4331 Then
                    MyEseguire = True
                End If


                If MyEseguire = True Then
                    If (Trim(DD_CServ.SelectedValue) <> "" And campodb(Rs_RettaComune.Item("CentroServizio")) = DD_CServ.SelectedValue) _
                       Or DD_CServ.SelectedValue = "" Then

                        If campodb(Rs_RettaComune.Item("CentroServizio")) <> Cserv Or _
                           campodbn(Rs_RettaComune.Item("CodiceOspite")) <> CodiceOspite Or _
                           campodbn(Rs_RettaComune.Item("Mese")) <> Mese Or _
                           campodbn(Rs_RettaComune.Item("Anno")) <> Anno Then
                            If MImportoComune <> 0 Or (Chk_ImportoA0.Checked = True And Not PrimaVolta) Then
                                MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno, "")
                                MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno, "")
                                MImportoRegione = ImportoRegione(Cserv, CodiceOspite, Mese, Anno, "")
                                MImportoEnte = ImportoEnte(Cserv, CodiceOspite, Mese, Anno, "")

                                MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)
                                CreaRecordEta = True
                                CreaRecord = True
                                CreaRecordNaz = True
                                CreaRecordTipUtente = True


                                If CreaRecordEta = True And CreaRecord = True And CreaRecordNaz = True And CreaRecordTipUtente Then

                                    Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoComune").NewRow

                                    Dim ClCserv As New Cls_CentroServizio

                                    ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                                    If ClCserv.TIPOCENTROSERVIZIO = "D" Then
                                        Dim f As String
                                        f = StringaPresenzeOspite(Cserv, CodiceOspite, Anno, Mese)
                                        TgExtra1 = 0
                                        TgExtra2 = 0
                                        TgExtra3 = 0
                                        TgExtra4 = 0
                                        For Li = 1 To Len(f) Step 2
                                            If InStr(1, Extra1, Mid(f, Li, 2)) > 0 Then
                                                TgExtra1 = TgExtra1 + 1
                                            End If
                                            If InStr(1, Extra2, Mid(f, Li, 2)) > 0 Then
                                                TgExtra2 = TgExtra2 + 1
                                            End If
                                            If InStr(1, Extra3, Mid(f, Li, 2)) > 0 Then
                                                TgExtra3 = TgExtra3 + 1
                                            End If
                                            If InStr(1, Extra4, Mid(f, Li, 2)) > 0 Then
                                                TgExtra4 = TgExtra4 + 1
                                            End If
                                        Next Li
                                        Rs_Stampa.Item("GiorniExtra1") = TgExtra1
                                        Rs_Stampa.Item("GiorniExtra2") = TgExtra2
                                        Rs_Stampa.Item("GiorniExtra3") = TgExtra3
                                        Rs_Stampa.Item("GiorniExtra4") = TgExtra4

                                        Rs_Stampa.Item("ImportoExtra1") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 1)
                                        Rs_Stampa.Item("ImportoExtra2") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 2)
                                        Rs_Stampa.Item("ImportoExtra3") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 3)
                                        Rs_Stampa.Item("ImportoExtra4") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 4)
                                    End If

                                    Rs_Stampa.Item("CodiceServizio") = Cserv
                                    Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE
                                    Rs_Stampa.Item("CodiceOspite") = CodiceOspite
                                    Rs_Stampa.Item("Provincia") = Provincia
                                    Rs_Stampa.Item("Comune") = Comune


                                    Dim RettaTotale As New Cls_rettatotale

                                    RettaTotale.CENTROSERVIZIO = Cserv
                                    RettaTotale.CODICEOSPITE = CodiceOspite

                                    RettaTotale.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                                    Rs_Stampa.Item("TipoRetta") = ""
                                    If RettaTotale.TipoRetta <> "" Then
                                        Dim TipoRetta As New Cls_TipoRetta
                                        TipoRetta.Codice = RettaTotale.TipoRetta
                                        TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Codice)

                                        Rs_Stampa.Item("TipoRetta") = TipoRetta.Descrizione
                                    End If

                                    Dim XOsp As New ClsOspite

                                    XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)


                                    Rs_Stampa.Item("DataNascita") = XOsp.DataNascita

                                    Dim XCom As New ClsComune

                                    XCom.Provincia = Provincia
                                    XCom.Comune = Comune
                                    XCom.Leggi(Session("DC_OSPITE"))
                                    Rs_Stampa.Item("DecodificaComune") = XCom.Descrizione
                                    Rs_Stampa.Item("Attenzione") = XCom.Attenzione

                                    If Chk_NonIncludereDataNascita.Checked = False Then
                                        Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                                        End If
                                    Else
                                        Rs_Stampa.Item("Nome") = XOsp.Nome
                                        If Chk_SoloIniziali.Checked = True Then
                                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                                        End If
                                    End If


                                    Dim Tab As New Cls_Tabelle

                                    Tab.TipTab = "NAZ"
                                    Tab.Codice = XOsp.Nazionalita
                                    Tab.Leggi(Session("DC_OSPITE"))
                                    Rs_Stampa.Item("Nazionalita") = Tab.Descrizione
                                    Rs_Stampa.Item("Mese") = Mese
                                    Rs_Stampa.Item("Anno") = Anno



                                    Rs_Stampa.Item("StatoAuto") = ""

                                    If Chk_ImportoA0.Checked = True And GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' AND StatoContabile = '" & STATOCONTABILE & "'"
                                        GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                                        GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)


                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If


                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & "  AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                                        End If
                                    End If

                                    Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                                    Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                                    Rs_Stampa.Item("ImportoAssenza") = ImportoAssenza
                                    Rs_Stampa.Item("TotaleRetta") = Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + TotMImportoComune + TotMImportoJolly, 2)
                                    Rs_Stampa.Item("Ospite") = Math.Round(MImportoOspite, 2)
                                    Rs_Stampa.Item("Parente") = Math.Round(MImportoParenti, 2)
                                    Rs_Stampa.Item("Regione") = Math.Round(MImportoRegione, 2)


                                    If Chk_ImpComEnt.Checked = True Then
                                        Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune - Math.Abs(MImportoEnte), 2)
                                    Else
                                        Rs_Stampa.Item("ImpComune") = Math.Round(MImportoComune, 2)
                                    End If
                                    Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE 'CampoOspite(CodiceOspite, "CODICEFISCALE")

                                    Dim MovimentiNelPeriodo As New Cls_Movimenti

                                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                    MovimentiNelPeriodo.UltimaDataAccoglimento(Session("DC_OSPITE"))


                                    Dim DataIniAcco As Date

                                    DataIniAcco = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")

                                    Rs_Stampa.Item("DataAccoglimento") = Format(MovimentiNelPeriodo.Data, "dd/MM/yyyy")


                                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                        Rs_Stampa.Item("DataInizio") = MovimentiNelPeriodo.Data
                                    Else
                                        Rs_Stampa.Item("DataInizio") = DateSerial(Anno, Mese, 1)
                                    End If

                                    MovimentiNelPeriodo.Data = Nothing
                                    MovimentiNelPeriodo.CodiceOspite = CodiceOspite
                                    MovimentiNelPeriodo.CENTROSERVIZIO = Cserv
                                    MovimentiNelPeriodo.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                                    If Year(MovimentiNelPeriodo.Data) = Anno And Month(MovimentiNelPeriodo.Data) = Mese Then
                                        Rs_Stampa.Item("DataFine") = MovimentiNelPeriodo.Data
                                    Else
                                        Rs_Stampa.Item("DataFine") = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                                    End If

                                    Rs_Stampa.Item("TemporaneoPermanente") = OspitePermanente(Cserv, CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)))
                                    If Chk_ADDACR.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                                    End If
                                    If Chk_MovEU.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_MovStato.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_TipoRetta.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_MovImp.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                                    End If
                                    If Chk_ImpGiornalieri.Checked = True Then
                                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                                    End If


                                    Rs_Stampa.Item("IntestaSocieta") = WSocieta
                                    Rs_Stampa.Item("Selezione") = Selezione

                                    Dim XServ As New Cls_CentroServizio

                                    XServ.CENTROSERVIZIO = Cserv
                                    XServ.Leggi(Session("DC_OSPITE"), Cserv)
                                    If XServ.TIPOCENTROSERVIZIO = "D" Then


                                        Dim x As New Cls_Diurno


                                        x.CodiceOspite = CodiceOspite
                                        x.CENTROSERVIZIO = Cserv
                                        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                                        Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                                        Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                                        Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                                        Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                                        Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                                        Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                                        Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                                        Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                                        Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                                        Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                                        Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                                        Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                                        Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                                        Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                                        Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                                        Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                                        Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                                        Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                                        Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                                        Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                                        Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                                        Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                                        Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                                        Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                                        Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                                        Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                                        Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                                        Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                                        Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                                        Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                                        Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31

                                        If Trim(x.LeggiMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)) = "" Then
                                            Rs_Stampa.Item("Nome") = Rs_Stampa.Item("Nome") & "*"
                                            If Chk_SoloIniziali.Checked = True Then
                                                Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & "*"
                                            End If
                                        End If


                                        If x.CodiceOspite = 85 Then
                                            x.CodiceOspite = 85
                                        End If
                                        Rs_Stampa.Item("GiorniPresTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPieno)
                                        Rs_Stampa.Item("GiorniAssTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss)
                                        If XServ.CausaleTempoPienoAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss1)
                                        End If
                                        If XServ.CausaleTempoPienoAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausaleTempoPienoAss2)
                                        End If


                                        Rs_Stampa.Item("GiorniPresPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTime)
                                        Rs_Stampa.Item("GiorniAssPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss)
                                        If XServ.CausalePartTimeAss1 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss1)
                                        End If
                                        If XServ.CausalePartTimeAss2 <> "" Then
                                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, XServ.CausalePartTimeAss2)
                                        End If




                                        Rs_Stampa.Item("GiorniMalattia") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "M")

                                        Rs_Stampa.Item("GiorniRicovero") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "A")

                                        Dim sc2 As New MSScriptControl.ScriptControl
                                        Dim SalvaComune As Double
                                        Dim m As New Cls_CalcoloRette

                                        sc2.Language = "vbscript"

                                        m.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                                        m.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                                        SalvaComune = m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0)

                                        Dim MS As New Cls_CausaliEntrataUscita

                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPieno
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaPresTP") = sc2.Eval("SalvaComune")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("SalvaOspite = 0")
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausaleTempoPienoAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaComune")



                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausalePartTime
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")


                                        Rs_Stampa.Item("QuotaPresPT") = sc2.Eval("SalvaComune")


                                        sc2.Reset()
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = XServ.CausalePartTimeAss
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaAssPT") = sc2.Eval("SalvaComune")

                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = "M"
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaMalattia") = sc2.Eval("SalvaComune")


                                        sc2.Reset()
                                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                                        MS.Codice = "A"
                                        MS.LeggiCausale(Session("DC_OSPITE"))

                                        sc2.AddCode(MS.Regole)

                                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                                        Rs_Stampa.Item("QuotaRicovero") = sc2.Eval("SalvaComune")


                                    Else
                                        Dim Rs_Movimenti As New ADODB.Recordset
                                        Dim Rs_MYMOV As New ADODB.Recordset
                                        Dim MyDataini As Date, MyDatafine As Date
                                        Dim WValore As String = ""
                                        Dim Uscito As Boolean
                                        Dim GGUSCITA As String, GGENTRATA As String
                                        Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                                        Dim Accolto As Boolean
                                        Dim Dimesso As Boolean
                                        Dim GiornoRientro As Integer
                                        Dim IndiceGiorni As Integer

                                        Dim Myserv As New Cls_CentroServizio


                                        Myserv.CENTROSERVIZIO = Cserv
                                        Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                                        Dim KPar As New Cls_Parametri

                                        KPar.LeggiParametri(Session("DC_OSPITE"))

                                        Accolto = False
                                        Dimesso = False
                                        GGUSCITA = KPar.GIORNOUSCITA
                                        GGENTRATA = KPar.GIORNOENTRATA
                                        If GGENTRATA = "P" Then
                                            GiornoRientro = 1
                                        Else
                                            GiornoRientro = 0
                                        End If

                                        MyDataini = DateSerial(Anno, Mese, 1)

                                        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                                        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                                        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                                        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                                        Dim cmdM As New OleDbCommand()
                                        cmdM.CommandText = MySql
                                        cmdM.Connection = cn

                                        Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                                        If ReaderMovimenti.Read() Then
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                                                Uscito = True
                                                WValore = campodb(ReaderMovimenti.Item("Causale"))
                                                'DATAINIZIO = MyDataini
                                                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                                                'If Not Rs_MYMOV.EOF Then
                                                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                                                'End If
                                                'Rs_MYMOV.Close
                                                NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                                            End If
                                            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                                    Accolto = True
                                                End If
                                                Uscito = False
                                                NGiorniAssenza = 0
                                            End If
                                        End If
                                        ReaderMovimenti.Close()


                                        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                                        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                                        Dim cmd1 As New OleDbCommand()
                                        cmd1.CommandText = MySql
                                        cmd1.Connection = cn

                                        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                                        GiornoUscita = 1
                                        Do While ReaderMovimenti1.Read
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                                    If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                                        GiornoUscita = 0
                                                    Else
                                                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                        Uscito = True
                                                        NGiorniAssenza = 1
                                                    End If
                                                Else
                                                    GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                                    Uscito = True
                                                    NGiorniAssenza = 1
                                                End If
                                            End If
                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                                                If Uscito = True Then
                                                    For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                        NGiorniAssenza = NGiorniAssenza + 1
                                                    Next IndiceGiorni
                                                End If
                                                If GGUSCITA = "P" Then
                                                    GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                                                Else
                                                    GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                                                End If
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                                                End If

                                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                                                WValore = "*"
                                                Uscito = True
                                                Dimesso = True
                                                NGiorniAssenza = 2
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                    NGiorniAssenza = NGiorniAssenza + 1
                                                Next IndiceGiorni
                                                Uscito = False
                                            End If

                                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                                Next IndiceGiorni
                                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                                Else
                                                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                                    Else
                                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                                    End If
                                                End If
                                                Uscito = False
                                                Accolto = True
                                            End If

                                        Loop
                                        If Uscito Then
                                            For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                                NGiorniAssenza = NGiorniAssenza + 1
                                            Next IndiceGiorni
                                        End If
                                        For IndiceGiorni = 27 To 31
                                            If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                            End If
                                        Next IndiceGiorni
                                        ReaderMovimenti1.Close()

                                        For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                                        Next

                                    End If

                                    Dim Trascodifiche As String = ""
                                    Dim CodiciInseriti As String = ""

                                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                                        Dim CercaCausale As New Cls_CausaliEntrataUscita
                                        Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                                        If PosizioneNewLine > 0 Then
                                            CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                                            If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                                            Else
                                                If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                                    CercaCausale.LeggiCausale(Session("DC_OSPITE"))
                                                    If Trascodifiche <> "" Then
                                                        Trascodifiche = Trascodifiche & ","
                                                    End If
                                                    CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                                    Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                                                End If
                                            End If
                                        End If
                                    Next
                                    Rs_Stampa.Item("Trascodifiche") = Trascodifiche


                                    Stampa.Tables("RendicontoComune").Rows.Add(Rs_Stampa)
                                End If

                                If Cserv = campodb(Rs_RettaComune.Item("CentroServizio")) And _
                                   CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite")) Then
                                    Salto = True
                                Else
                                    Salto = False
                                End If

                                Cserv = campodb(Rs_RettaComune.Item("CentroServizio"))
                                CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite"))
                                Mese = campodb(Rs_RettaComune.Item("Mese"))
                                Anno = campodb(Rs_RettaComune.Item("Anno"))

                                Provincia = campodb(Rs_RettaComune.Item("Provincia"))
                                Comune = campodb(Rs_RettaComune.Item("Comune"))
                                If ChkDataIniFinePrestazione.Checked = True Then
                                    If Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                       Format(DataAccoglimento(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                        dataini = DataAccoglimento(Cserv, CodiceOspite, Mese, Anno)
                                    Else
                                        dataini = DateSerial(Anno, Mese, 1)
                                    End If
                                    If Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And _
                                       Format(DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                        dataFine = DataUscitaDefinitiva(Cserv, CodiceOspite, Mese, Anno)
                                    Else
                                        dataFine = DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno))
                                    End If

                                End If
                            End If
                            GiorniPresenza = 0
                            GiorniAssenza = 0
                            MImportoComune = 0
                            MImportoJolly = 0
                            MImportoEnte = 0
                            TotMImportoComune = 0
                            TotMImportoJolly = 0
                            ImportoAssenza = 0
                            Cserv = campodb(Rs_RettaComune.Item("CentroServizio"))
                            CodiceOspite = campodb(Rs_RettaComune.Item("CodiceOspite"))
                            Mese = campodb(Rs_RettaComune.Item("Mese"))
                            Anno = campodb(Rs_RettaComune.Item("Anno"))

                            Provincia = campodb(Rs_RettaComune.Item("Provincia"))
                            Comune = campodb(Rs_RettaComune.Item("Comune"))
                        End If
                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "RGP" Then
                            GiorniPresenza = campodb(Rs_RettaComune.Item("Giorni"))
                        End If
                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "RGA" Then
                            GiorniAssenza = campodb(Rs_RettaComune.Item("Giorni"))
                            ImportoAssenza = ImportoAssenza + campodb(Rs_RettaComune.Item("Importo"))
                        End If
                        If campodb(Rs_RettaComune.Item("ELEMENTO")) = "ACC" Then
                            MImportoComune = MImportoComune - campodb(Rs_RettaComune.Item("Importo"))
                        Else
                            MImportoComune = MImportoComune + campodb(Rs_RettaComune.Item("Importo"))
                            If campodb(Rs_RettaComune.Item("ELEMENTO")) <> "ADD" Then
                                TotMImportoComune = TotMImportoComune + campodb(Rs_RettaComune.Item("Importo"))
                            End If
                        End If

                        PrimaVolta = False
                    End If
                End If
            Loop

            If MImportoComune <> 0 Or (Chk_ImportoA0.Checked = True And Not PrimaVolta) Then

                CreaRecordEta = True
                CreaRecord = True
                CreaRecordNaz = True
                CreaRecordTipUtente = True


                If CreaRecordEta = True And CreaRecord = True And CreaRecordNaz = True And CreaRecordTipUtente Then

                    MImportoOspite = ImportoOspite(Cserv, CodiceOspite, Mese, Anno, "")
                    MImportoParenti = ImportoParenti(Cserv, CodiceOspite, Mese, Anno, "")
                    MImportoRegione = ImportoRegione(Cserv, CodiceOspite, Mese, Anno, "")

                    MImportoJolly = ImportoJolly(Cserv, CodiceOspite, Mese, Anno)


                    Dim Rs_Stampa As System.Data.DataRow = Stampa.Tables("RendicontoComune").NewRow

                    Dim ClCserv As New Cls_CentroServizio

                    ClCserv.Leggi(Session("DC_OSPITE"), Cserv)

                    Dim XOsp As New ClsOspite

                    XOsp.Leggi(Session("DC_OSPITE"), CodiceOspite)


                    If ClCserv.TIPOCENTROSERVIZIO = "D" Then
                        Dim f As String
                        f = StringaPresenzeOspite(Cserv, CodiceOspite, Anno, Mese)
                        TgExtra1 = 0
                        TgExtra2 = 0
                        TgExtra3 = 0
                        TgExtra4 = 0
                        For Li = 1 To Len(f) Step 2
                            If InStr(1, Extra1, Mid(f, Li, 2)) > 0 Then
                                TgExtra1 = TgExtra1 + 1
                            End If
                            If InStr(1, Extra2, Mid(f, Li, 2)) > 0 Then
                                TgExtra2 = TgExtra2 + 1
                            End If
                            If InStr(1, Extra3, Mid(f, Li, 2)) > 0 Then
                                TgExtra3 = TgExtra3 + 1
                            End If
                            If InStr(1, Extra4, Mid(f, Li, 2)) > 0 Then
                                TgExtra4 = TgExtra4 + 1
                            End If
                        Next Li
                        Rs_Stampa.Item("GiorniExtra1") = TgExtra1
                        Rs_Stampa.Item("GiorniExtra2") = TgExtra2
                        Rs_Stampa.Item("GiorniExtra3") = TgExtra3
                        Rs_Stampa.Item("GiorniExtra4") = TgExtra4

                        Rs_Stampa.Item("ImportoExtra1") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 1)
                        Rs_Stampa.Item("ImportoExtra2") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 2)
                        Rs_Stampa.Item("ImportoExtra3") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 3)
                        Rs_Stampa.Item("ImportoExtra4") = ImportoExtraComune(CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)), 4)



                        Dim x As New Cls_Diurno


                        x.CodiceOspite = CodiceOspite
                        x.CENTROSERVIZIO = Cserv
                        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)

                        Rs_Stampa.Item("Giorno1") = "1" & vbNewLine & x.Giorno1
                        Rs_Stampa.Item("Giorno2") = "2" & vbNewLine & x.Giorno2
                        Rs_Stampa.Item("Giorno3") = "3" & vbNewLine & x.Giorno3
                        Rs_Stampa.Item("Giorno4") = "4" & vbNewLine & x.Giorno4
                        Rs_Stampa.Item("Giorno5") = "5" & vbNewLine & x.Giorno5
                        Rs_Stampa.Item("Giorno6") = "6" & vbNewLine & x.Giorno6
                        Rs_Stampa.Item("Giorno7") = "7" & vbNewLine & x.Giorno7
                        Rs_Stampa.Item("Giorno8") = "8" & vbNewLine & x.Giorno8
                        Rs_Stampa.Item("Giorno9") = "9" & vbNewLine & x.Giorno9
                        Rs_Stampa.Item("Giorno10") = "10" & vbNewLine & x.Giorno10
                        Rs_Stampa.Item("Giorno11") = "11" & vbNewLine & x.Giorno11
                        Rs_Stampa.Item("Giorno12") = "12" & vbNewLine & x.Giorno12
                        Rs_Stampa.Item("Giorno13") = "13" & vbNewLine & x.Giorno13
                        Rs_Stampa.Item("Giorno14") = "14" & vbNewLine & x.Giorno14
                        Rs_Stampa.Item("Giorno15") = "15" & vbNewLine & x.Giorno15
                        Rs_Stampa.Item("Giorno16") = "16" & vbNewLine & x.Giorno16
                        Rs_Stampa.Item("Giorno17") = "17" & vbNewLine & x.Giorno17
                        Rs_Stampa.Item("Giorno18") = "18" & vbNewLine & x.Giorno18
                        Rs_Stampa.Item("Giorno19") = "19" & vbNewLine & x.Giorno19
                        Rs_Stampa.Item("Giorno20") = "20" & vbNewLine & x.Giorno20
                        Rs_Stampa.Item("Giorno21") = "21" & vbNewLine & x.Giorno21
                        Rs_Stampa.Item("Giorno22") = "22" & vbNewLine & x.Giorno22
                        Rs_Stampa.Item("Giorno23") = "23" & vbNewLine & x.Giorno23
                        Rs_Stampa.Item("Giorno24") = "24" & vbNewLine & x.Giorno24
                        Rs_Stampa.Item("Giorno25") = "25" & vbNewLine & x.Giorno25
                        Rs_Stampa.Item("Giorno26") = "26" & vbNewLine & x.Giorno26
                        Rs_Stampa.Item("Giorno27") = "27" & vbNewLine & x.Giorno27
                        Rs_Stampa.Item("Giorno28") = "28" & vbNewLine & x.Giorno28
                        Rs_Stampa.Item("Giorno29") = "29" & vbNewLine & x.Giorno29
                        Rs_Stampa.Item("Giorno30") = "30" & vbNewLine & x.Giorno30
                        Rs_Stampa.Item("Giorno31") = "31" & vbNewLine & x.Giorno31

                        If Trim(x.LeggiMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese)) = "" Then
                            Rs_Stampa.Item("Nome") = Rs_Stampa.Item("Nome") & "*"
                            If Chk_SoloIniziali.Checked = True Then
                                Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & "*"
                            End If
                        End If


                        If x.CodiceOspite = 85 Then
                            x.CodiceOspite = 85
                        End If
                        Rs_Stampa.Item("GiorniPresTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPieno)
                        Rs_Stampa.Item("GiorniAssTP") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss)
                        If ClCserv.CausaleTempoPienoAss1 <> "" Then
                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss1)
                        End If
                        If ClCserv.CausaleTempoPienoAss2 <> "" Then
                            Rs_Stampa.Item("GiorniAssTP") = Rs_Stampa.Item("GiorniAssTP") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausaleTempoPienoAss2)
                        End If

                        Rs_Stampa.Item("GiorniPresPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTime)
                        Rs_Stampa.Item("GiorniAssPT") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss)
                        If ClCserv.CausalePartTimeAss1 <> "" Then
                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss1)
                        End If
                        If ClCserv.CausalePartTimeAss2 <> "" Then
                            Rs_Stampa.Item("GiorniAssPT") = Rs_Stampa.Item("GiorniAssPT") + x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, ClCserv.CausalePartTimeAss2)
                        End If

                        Rs_Stampa.Item("GiorniMalattia") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "M")

                        Rs_Stampa.Item("GiorniRicovero") = x.RipetizioniCausaleMese(Session("DC_OSPITE"), x.CodiceOspite, Cserv, Anno, Mese, "A")

                        Rs_Stampa.Item("CodiceFiscale") = XOsp.CODICEFISCALE 'CampoOspite(CodiceOspite, "CODICEFISCALE")

                        Dim sc2 As New MSScriptControl.ScriptControl
                        Dim SalvaComune As Double
                        Dim m As New Cls_CalcoloRette

                        sc2.Language = "vbscript"

                        m.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                        m.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                        SalvaComune = m.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0)

                        Dim MS As New Cls_CausaliEntrataUscita

                        sc2.Reset()
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausaleTempoPieno
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaPresTP") = sc2.Eval("SalvaComune")



                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausaleTempoPienoAss
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaAssTP") = sc2.Eval("SalvaComune")



                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausalePartTime
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")


                        Rs_Stampa.Item("QuotaPresPT") = sc2.Eval("SalvaComune")


                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = ClCserv.CausalePartTimeAss
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaAssPT") = sc2.Eval("SalvaComune")

                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = "M"
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaMalattia") = sc2.Eval("SalvaComune")


                        sc2.Reset()
                        sc2.ExecuteStatement("Dim SalvaParente(10)")
                        sc2.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))
                        sc2.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                        MS.Codice = "A"
                        MS.LeggiCausale(Session("DC_OSPITE"))

                        sc2.AddCode(MS.Regole)

                        If Not IsDBNull(MS.Regole) And MS.Regole <> "" Then sc2.Run("Calcolo")

                        Rs_Stampa.Item("QuotaRicovero") = sc2.Eval("SalvaComune")

                    Else
                        Dim Rs_Movimenti As New ADODB.Recordset
                        Dim Rs_MYMOV As New ADODB.Recordset
                        Dim MyDataini As Date, MyDatafine As Date
                        Dim WValore As String = ""
                        Dim Uscito As Boolean
                        Dim GGUSCITA As String, GGENTRATA As String
                        Dim NGiorniAssenza As Integer, GiornoUscita As Integer
                        Dim Accolto As Boolean
                        Dim Dimesso As Boolean
                        Dim GiornoRientro As Integer
                        Dim IndiceGiorni As Integer

                        Dim Myserv As New Cls_CentroServizio


                        Myserv.CENTROSERVIZIO = Cserv
                        Myserv.Leggi(Session("DC_OSPITE"), Myserv.CENTROSERVIZIO)

                        Dim KPar As New Cls_Parametri

                        KPar.LeggiParametri(Session("DC_OSPITE"))

                        Accolto = False
                        Dimesso = False
                        GGUSCITA = KPar.GIORNOUSCITA
                        GGENTRATA = KPar.GIORNOENTRATA
                        If GGENTRATA = "P" Then
                            GiornoRientro = 1
                        Else
                            GiornoRientro = 0
                        End If

                        MyDataini = DateSerial(Anno, Mese, 1)

                        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



                        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "


                        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

                        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


                        Dim cmdM As New OleDbCommand()
                        cmdM.CommandText = MySql
                        cmdM.Connection = cn

                        Dim ReaderMovimenti As OleDbDataReader = cmdM.ExecuteReader()

                        If ReaderMovimenti.Read() Then
                            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                                Uscito = True
                                WValore = campodb(ReaderMovimenti.Item("Causale"))
                                'DATAINIZIO = MyDataini
                                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                                'If Not Rs_MYMOV.EOF Then
                                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                                'End If
                                'Rs_MYMOV.Close
                                NGiorniAssenza = DateDiff("d", campodbD(ReaderMovimenti.Item("Data")), MyDataini) + 1

                            End If
                            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                                    Accolto = True
                                End If
                                Uscito = False
                                NGiorniAssenza = 0
                            End If
                        End If
                        ReaderMovimenti.Close()


                        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodiceOspite & " And "
                        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


                        Dim cmd1 As New OleDbCommand()
                        cmd1.CommandText = MySql
                        cmd1.Connection = cn

                        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



                        GiornoUscita = 1
                        Do While ReaderMovimenti1.Read
                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                                If GGUSCITA = "P" Then
                                    GiornoUscita = Day(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1))
                                    If Month(DateSerial(Year(campodbD(ReaderMovimenti1.Item("Data"))), Month(campodbD(ReaderMovimenti1.Item("Data"))), Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbD(ReaderMovimenti1.Item("Data"))) Then
                                        GiornoUscita = 0
                                    Else
                                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                        Uscito = True
                                        NGiorniAssenza = 1
                                    End If
                                Else
                                    GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data")))
                                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                                    Uscito = True
                                    NGiorniAssenza = 1
                                End If
                            End If
                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                                If Uscito = True Then
                                    For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                        Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                        NGiorniAssenza = NGiorniAssenza + 1
                                    Next IndiceGiorni
                                End If
                                If GGUSCITA = "P" Then
                                    GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                                Else
                                    GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                                End If
                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                Else
                                    Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                                End If

                                GiornoUscita = Day(campodbD(ReaderMovimenti1.Item("Data"))) + 1
                                WValore = "*"
                                Uscito = True
                                Dimesso = True
                                NGiorniAssenza = 2
                            End If

                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                    NGiorniAssenza = NGiorniAssenza + 1
                                Next IndiceGiorni
                                Uscito = False
                            End If

                            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                                For IndiceGiorni = GiornoUscita To Day(campodbD(ReaderMovimenti1.Item("Data"))) - 1
                                    Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                                Next IndiceGiorni
                                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                                Else
                                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = KPar.CAUSALEACCOGLIMENTO
                                    Else
                                        Rs_Stampa.Item("Giorno" & Day(campodbD(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                                    End If
                                End If
                                Uscito = False
                                Accolto = True
                            End If

                        Loop
                        If Uscito Then
                            For IndiceGiorni = GiornoUscita To Day(MyDatafine)
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = WValore
                                NGiorniAssenza = NGiorniAssenza + 1
                            Next IndiceGiorni
                        End If
                        For IndiceGiorni = 27 To 31
                            If Day(DateSerial(Anno, Mese, IndiceGiorni)) <> IndiceGiorni Then
                                Rs_Stampa.Item("Giorno" & IndiceGiorni) = "*"
                            End If
                        Next IndiceGiorni
                        ReaderMovimenti1.Close()

                        For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                            Rs_Stampa.Item("Giorno" & IndiceGiorni) = IndiceGiorni & vbNewLine & Rs_Stampa.Item("Giorno" & IndiceGiorni)
                        Next

                    End If

                    Dim Trascodifiche As String = ""
                    Dim CodiciInseriti As String = ""
                    For IndiceGiorni = 1 To GiorniMese(Mese, Anno)
                        Dim CercaCausale As New Cls_CausaliEntrataUscita
                        Dim PosizioneNewLine As Integer = campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)).IndexOf(vbNewLine)

                        If PosizioneNewLine > 0 Then
                            CercaCausale.Codice = Trim(Mid(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni)), PosizioneNewLine + 3, Len(campodb(Rs_Stampa.Item("Giorno" & IndiceGiorni))) - PosizioneNewLine))
                            If Trim(CercaCausale.Codice) = "*" Or Trim(CercaCausale.Codice) = "" Then

                            Else
                                If CodiciInseriti.IndexOf("[" & CercaCausale.Codice & "]") < 0 Then
                                    CercaCausale.LeggiCausale(Session("DC_OSPITE"))

                                    If Trascodifiche <> "" Then
                                        Trascodifiche = Trascodifiche & ","
                                    End If

                                    CodiciInseriti = CodiciInseriti & "[" & CercaCausale.Codice & "]"
                                    Trascodifiche = Trascodifiche & CercaCausale.Codice & " = " & CercaCausale.Descrizione
                                End If
                            End If
                        End If
                    Next


                    Rs_Stampa.Item("Trascodifiche") = Trascodifiche
                    Rs_Stampa.Item("CodiceServizio") = Cserv
                    Rs_Stampa.Item("CodiceOspite") = CodiceOspite
                    Rs_Stampa.Item("DecodificaCentroServizio") = ClCserv.DESCRIZIONE

                    Dim RettaTotale2 As New Cls_rettatotale

                    RettaTotale2.CENTROSERVIZIO = Cserv
                    RettaTotale2.CODICEOSPITE = CodiceOspite
                    RettaTotale2.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                    Rs_Stampa.Item("TipoRetta") = ""
                    If RettaTotale2.TipoRetta <> "" Then
                        Dim TipoRetta As New Cls_TipoRetta
                        TipoRetta.Codice = RettaTotale2.TipoRetta
                        TipoRetta.Leggi(Session("DC_OSPITE"), TipoRetta.Codice)

                        Rs_Stampa.Item("TipoRetta") = TipoRetta.Descrizione
                    End If

                    Rs_Stampa.Item("Provincia") = Provincia
                    Rs_Stampa.Item("Comune") = Comune

                    Rs_Stampa.Item("DataNascita") = XOsp.DataNascita

                    Dim KDc As New ClsComune

                    KDc.Provincia = Provincia
                    KDc.Comune = Comune
                    KDc.Leggi(Session("DC_OSPITE"))
                    Rs_Stampa.Item("DecodificaComune") = KDc.Descrizione
                    Rs_Stampa.Item("Attenzione") = KDc.Attenzione

                    'KDc.Attenzione
                    If Chk_NonIncludereDataNascita.Checked = False Then
                        Rs_Stampa.Item("Nome") = XOsp.Nome & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                        If Chk_SoloIniziali.Checked = True Then
                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1) & " " & Format(XOsp.DataNascita, "dd/MM/yyyy")
                        End If
                    Else
                        Rs_Stampa.Item("Nome") = XOsp.Nome
                        If Chk_SoloIniziali.Checked = True Then
                            Rs_Stampa.Item("Nome") = Mid(XOsp.CognomeOspite & Space(2), 1, 1) & " " & Mid(XOsp.NomeOspite & Space(2), 1, 1)
                        End If
                    End If



                    Dim Tab As New Cls_Tabelle

                    Tab.TipTab = "NAZ"
                    Tab.Codice = XOsp.Nazionalita
                    Tab.Leggi(Session("DC_OSPITE"))

                    Rs_Stampa.Item("Nazionalita") = Tab.Descrizione
                    Rs_Stampa.Item("Mese") = Mese
                    Rs_Stampa.Item("Anno") = Anno
                    Rs_Stampa.Item("StatoAuto") = ""

                    If Chk_ImportoA0.Checked = True And GiorniPresenza = 0 And GiorniAssenza = 0 Then
                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP'  AND StatoContabile = '" & STATOCONTABILE & "'"
                        GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                        MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA'  AND StatoContabile = '" & STATOCONTABILE & "'"
                        GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RetteOspite Where " & MySql)

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 1  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If
                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 2  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)


                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 3  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' And CodiceParente = 4  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' And CodiceParente = 4  AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEPARENTE Where " & MySql)
                        End If

                        If GiorniPresenza = 0 And GiorniAssenza = 0 Then
                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGP' AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniPresenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEREGIONE Where " & MySql)

                            MySql = " CODICEOSPITE = " & CodiceOspite & " and CENTROSERVIZIO = '" & Cserv & "' AND MESE = " & Mese & " AND ANNO = " & Anno & " And Elemento = 'RGA' AND StatoContabile = '" & STATOCONTABILE & "'"
                            GiorniAssenza = TotaleDaMsql("Select sum(Giorni) as TotGiorni From RETTEREGIONE Where " & MySql)
                        End If
                    End If

                    MImportoEnte = ImportoEnte(Cserv, CodiceOspite, Mese, Anno, STATOCONTABILE)
                    Rs_Stampa.Item("GiorniPresenza") = GiorniPresenza
                    Rs_Stampa.Item("GiorniAssenza") = GiorniAssenza
                    Rs_Stampa.Item("ImportoAssenza") = ImportoAssenza

                    Rs_Stampa.Item("TotaleRetta") = Format(Math.Round(MImportoOspite + MImportoParenti + MImportoRegione + TotMImportoComune + MImportoJolly, 2), "#,##0.00")
                    Rs_Stampa.Item("Ospite") = Format(Math.Round(MImportoOspite, 2), "#,##0.00")
                    Rs_Stampa.Item("Parente") = Format(Math.Round(MImportoParenti, 2), "#,##0.00")
                    Rs_Stampa.Item("Regione") = Format(Math.Round(MImportoRegione, 2), "#,##0.00")

                    Rs_Stampa.Item("ImpJolly") = Format(Math.Round(MImportoJolly, 2), "#,##0.00")

                    If Chk_ImpComEnt.Checked = True Then
                        Rs_Stampa.Item("ImpComune") = Format(Math.Round(MImportoComune - Math.Abs(MImportoEnte), 2), "#,##0.00")
                    Else
                        Rs_Stampa.Item("ImpComune") = Format(Math.Round(MImportoComune, 2), "#,##0.00")
                    End If
                    Rs_Stampa.Item("DataInizio") = DataInizio
                    Rs_Stampa.Item("DataFine") = dataFine
                    Rs_Stampa.Item("TemporaneoPermanente") = OspitePermanente(Cserv, CodiceOspite, DateSerial(Anno, Mese, OldVb6.GiorniMese(Mese, Anno)))
                    If Chk_ADDACR.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaAddebitiAccredito(Cserv, CodiceOspite, Mese, Anno, "R") & vbNewLine
                    End If
                    If Chk_MovEU.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_MovStato.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_TipoRetta.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaTipoRetta(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_MovImp.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaImpegnativa(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If
                    If Chk_ImpGiornalieri.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaGiornaliere(Cserv, CodiceOspite, Mese, Anno, "") & vbNewLine
                    End If
                    If Chk_AccoglimentoUscitaDef.Checked = True Then
                        Rs_Stampa.Item("Descrizione") = Rs_Stampa.Item("Descrizione") & CercaMovimenti(Cserv, CodiceOspite, Mese, Anno) & vbNewLine
                    End If


                    Rs_Stampa.Item("IntestaSocieta") = WSocieta
                    Rs_Stampa.Item("Selezione") = Selezione

                    Stampa.Tables("RendicontoComune").Rows.Add(Rs_Stampa)
                End If
            End If
            Rs_RettaComune.Close()

            GiorniPresenza = 0
            GiorniAssenza = 0
            MImportoComune = 0
            TotMImportoComune = 0
            Cserv = ""
            CodiceOspite = 0
            'Mese = 0
            'Anno = 0
            STATOCONTABILE = ""
            Provincia = ""
            Comune = ""

        Next I
        cn.Close()


        Dim Numero As Integer
        Dim CodiceServizio As String
        Dim DecodificaComune As String

        'Dim foundRowsRM As System.Data.DataRow()

        Dim foundRowsT As System.Data.DataRow()
        foundRowsT = Stampa.Tables("RendicontoComune").Select("", "CodiceServizio,DecodificaComune,Nome,Mese")
        If foundRowsT.Length > 0 Then
            CodiceServizio = foundRowsT(0).Item("CodiceServizio")
            DecodificaComune = campodb(foundRowsT(0).Item("DecodificaComune"))
        End If


        For MInd = 0 To foundRowsT.Length - 1
            If campodb(CodiceServizio) <> campodb(foundRowsT(MInd).Item("CodiceServizio")) Then
                Numero = 0
            End If
            If campodb(DecodificaComune) <> campodb(foundRowsT(MInd).Item("DecodificaComune")) Then
                Numero = 0
            End If
            Numero = Numero + 1

            foundRowsT(MInd).Item("Numero") = Numero

            CodiceServizio = foundRowsT(MInd).Item("CodiceServizio")
            DecodificaComune = campodb(foundRowsT(MInd).Item("DecodificaComune"))

        Next MInd


        Session("stampa") = Stampa

    End Sub

    Protected Sub Img_Jolly_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Jolly.Click
        Call ProcJolly()
        Dim XS As New Cls_Login



        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))
        'x = Session("stampa")

        If DD_Report.SelectedValue = "" Then
            If Chk_StampaDownloadPerGruppo.Checked = False Then
                If Chk_SoloRetta.Checked = True And XS.ReportPersonalizzato("RENDICONTOCOMUNESOLORETTA") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNESOLORETTA','StampeRendiconto','width=800,height=600');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RENDICONTOCOMUNE','StampeRendiconto','width=800,height=600');", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('EstraiPerSociale.aspx','StampeRendiconto','width=800,height=600');", True)
            End If
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "','StampeRendiconto','width=800,height=600');", True)
        End If

    End Sub
End Class
