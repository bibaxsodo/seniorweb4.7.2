﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Visualizzazioni" CodeFile="Menu_Visualizzazioni.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Visualizzazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Statistiche</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" valign="top">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center;">
                                    <a href="ProspettoOspitiPresenti.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Ospiti Presenti" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Prospetto.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Prospetto Presenze" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="VerificaRette.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Verifica Rette" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="RicercaAnagrafica.aspx?TIPO=DOCOSPPAR">
                                        <img src="../images/bottonestatistica.jpg" alt="Documenti Ospite/Parente" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="VisualizzaMovimenti.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Visualizza Movimenti" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">OSPITI 
        PRES.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PROSP. 
        PRESENZE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VERIF. 
        RETTE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SIT.CONTABILE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VIS. 
        MOVIMENTI</span></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="VisualizzazioneAddebitoAccredito.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Visualizza Addebiti/Accrediti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="RicercaAnagrafica.aspx?TIPO=EstrattoContoDenaro">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Estratto conto denaro" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="EstrattoContoDenaro.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Estratto Conto Denaro" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="StanzeLibere.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Letti Liberi" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="StanzeOccupate.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Letti Occupati" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <tr>
                                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">VIS. 
        ADD./ACR.</span></td>
                                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">ESTRATTO 
        CONTO OSPITE</span></td>
                                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">ESTRATTO 
        CONTO</span></td>
                                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">LETTI LIBERI</span></td>
                                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">LETTI OCCUPATI</span></td>
                                </tr>

                            <tr>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;">

                                    <a href="ProspettoPresenzeAssenzeStanzaMovimenti.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Prospetto Presenze Stanza" style="border-width: 0;"></a> </td>
                                <td style="text-align: center;">
                                    <a href="FlussoOspiti.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Flusso Ospiti" class="Effetto" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="VisualizzazioneQuote.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Visualizza Quote" style="border-width: 0;"></a> </td>

                                <td style="text-align: center;">
                                    <a href="ProspettoConguagliRettaMese.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="ProspettoConguagliRettaMese" style="border-width: 0;"></a> </td>

                                <td style="text-align: center;">
                                    <a href="Frm_EstrazionePer730.aspx">
                                        <img src="../images/bottonestatistica.jpg" alt="Estrazione 730" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOVIMENTI LETTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">FLUSSO OSPITI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CRONOLOGIA QUOTE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONGUAGLIO ANTICIPATI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">EST.730</span></td>
                            </tr>


                            <tr>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;">

                                    <a href="VisualizzaImpegnative.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Visualizza Impegnative" style="border-width: 0;"></a> </td>
                                <td style="text-align: center;">
                                    <a href="VisualizzaISE.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Visualizza ISE" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="SituazioneContabileCentroServizio.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Letti Occupati" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="VisualizzaIncassi.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" alt="Incassi" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="Menu_StatisticheDomiciliari.aspx">
                                        <img src="../images/Menu_Visualizzazioni.png" class="Effetto" alt="Statistiche Domiciliari" style="border-width: 0;"></a>
                                </td>

                                <td></td>

                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPEGNATIVE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ISE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SIT. CONT. CSERV</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SIT.INCASSI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAT.DOMICILIARI</span></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
