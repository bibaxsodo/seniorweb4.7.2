﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_VisualizzaImpegnative" CodeFile="VisualizzaImpegnative.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Visualizza Impegnativa</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Statistiche - Visualizza Impegnativa</div>
                                <div class="SottoTitoloOSPITE">
                                    <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <div class="DivTastiOspite">
                                    <span class="BenvenutoText">Benvenuto
                                        <asp:Label ID="Lbl_Utente" runat="server"></asp:Label></span>

                                    <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi"
                                        Height="38px" ToolTip="Stampa" ID="Btn_Stampa"></asp:ImageButton>
                                    <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                    <asp:ImageButton ID="ImageButton4" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />

                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                                    Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Visualizza Impegnativa

                                        </HeaderTemplate>



                                        <ContentTemplate>

                                            <label class="LabelCampo">Struttura:</label>

                                            &nbsp;
       
        <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />

                                            <br />


                                            <label class="LabelCampo">Centro Servizio :</label>

                                            &nbsp;<asp:DropDownList ID="DD_CServ" runat="server" Width="354px"></asp:DropDownList><br />

                                            <br />

                                            <label class="LabelCampo">Data Dal :</label>

                                            &nbsp;
      
        <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Data Al :</label>


                                            &nbsp;<asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox><br />

                                            <br />
                                            <br />
                                            <label class="LabelCampo">Tipo Estrazione :</label>
                                            <asp:RadioButton ID="RD_TipoEsportazioneI" Checked="False" runat="server" GroupName="Tipo" />Periodo su Data Inizio
        <asp:RadioButton ID="RD_TipoEsportazioneF" Checked="True" runat="server" GroupName="Tipo" />Periodo su Data Fine
           
        <br />
                                            <br />
                                            <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                                BorderWidth="1px" GridLines="Vertical">
                                                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                <AlternatingRowStyle BackColor="Gainsboro" />
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                                    ForeColor="White" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ImageButton4" />
                </Triggers>
            </asp:UpdatePanel>


            <br />
            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>

                    <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                        Attendere prego.....<br />
                        &nbsp;&nbsp;<img height="30px" src="images/loading.gif">
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <br />

        </div>
    </form>
</body>
</html>
