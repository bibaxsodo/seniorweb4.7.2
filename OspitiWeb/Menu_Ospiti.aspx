﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Ospiti" CodeFile="Menu_Ospiti.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Ospiti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>


    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>


    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
            posizionenotifiche();
        });


        function posizionenotifiche() {
            if ($("#news").css("visibility") == "visible") {
                var topnews = $("#news").offset();
                var heightnews = $("#news").height();
                $("#notifiche").css("top", (topnews.top + heightnews + 20) + "px");
            } else {
                $("#notifiche").css("top", "120px");
            }
        }

        function chiudinews() {
            document.cookie = "Entrata=13; expires=July 31, 2020 12:00:00 UTC";
            $("#news").css('visibility', 'hidden');
            posizionenotifiche();
        }


        function visualizzanotifiche() {

            $("#notifiche").css('visibility', 'visible');
            posizionenotifiche();
        }


        function chiudinotifiche() {

            $("#notifiche").css('visibility', 'hidden');
        }



        $(document).ready(function () {
            // JAVASCRIPT (jQuery)

            // Trigger action when the contexmenu is about to be shown
            $("#menutastodestro").bind("contextmenu", function (event) {

                // Avoid the real one
                event.preventDefault();

                // Show contextmenu
                $("#rightmenu").toggle(100).

                    // In the right position (the mouse)
                    css({
                        top: event.pageY + "px",
                        left: event.pageX + "px"
                    });
            });


            // If the document is clicked somewhere
            $(document).bind("mousedown", function (e) {

                // If the clicked element is not the menu
                if (!$(e.target).parents(".custom-menu").length > 0) {

                    // Hide it
                    $("#rightmenu").hide(100);
                }
            });


            // If the menu element is clicked
            $(document).click(function () {

                // This is the triggered action name
                switch ($(this).attr("data-action")) {

                    // A case for each action. Your actions here  
                    case "Anagrafica": break;
                    case "Addebiti Accrediti": break;
                    case "Movimenti": break;
                    case "Diurno": break;
                    case "Denaro": break;
                    case "Documenti": break;
                    case "Estratto Conto": break;
                }

                // Hide it AFTER the action was triggered
                $("#rightmenu").hide(100);
            });
        });
    </script>
    <style>
        body:hover::#TastiSenior {
            display: none;
        }


        #notifiche {
            border: solid 1px #2d0000;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 25%;
            height: 10%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(125, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(125, 0, 0, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(125, 0, 0, 0.75);
            z-index: 133;
        }

        #news {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 25%;
            height: 50%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }
        /* CSS3 */

        /* The whole thing */
        .custom-menu {
            display: none;
            z-index: 1000;
            position: absolute;
            border: 1px solid #CCC;
            white-space: nowrap;
            font-family: sans-serif;
            background: #FFF;
            color: #333;
            border-radius: 5px;
            padding: 0;
        }

        .custom-menu-list {
            display: none;
            z-index: 1001;
            position: absolute;
            border: 1px solid #CCC;
            white-space: nowrap;
            font-family: sans-serif;
            background: #FFF;
            color: #333;
            border-radius: 5px;
            padding: 0;
            left: 50px;
            top: 30px;
        }

        .custom-menu-listSmall {
            display: none;
            z-index: 1001;
            position: absolute;
            border: 1px solid #CCC;
            white-space: nowrap;
            font-family: sans-serif;
            background: #FFF;
            color: #333;
            border-radius: 5px;
            padding: 0;
            left: 50px;
            top: 180px;
        }


        /* Each of the items in the list */
        .custom-menu li {
            padding: 8px 12px;
            cursor: pointer;
            list-style-type: none;
            transition: all .3s ease;
            user-select: none;
        }

        .custom-menu a {
            text-decoration: none;
            color: #007DC4;
        }

        .custom-menu-list {
            display: none;
        }

        #IdMenuOspiti:hover + #MenuOspiti {
            display: block;
        }


        #MenuOspiti:hover {
            display: block;
        }

        #IdTabelleOspiti:hover + #MenuTabelleOspiti {
            display: block;
        }


        #IdTabelleOspiti:hover {
            display: block;
        }

        #MenuOspiti > #IdTabelleOspiti:hover {
            display: block;
        }

        #MenuOspiti > #MenuTabelleOspiti:hover {
            display: block;
        }



        #idStatisticheOspiti:hover + #MenuStatisticheOspiti {
            display: block;
        }


        #idStatisticheOspiti:hover {
            display: block;
        }

        #MenuOspiti > #idStatisticheOspiti:hover {
            display: block;
        }

        #MenuOspiti > #MenuStatisticheOspiti:hover {
            display: block;
        }



        #idStrumenti:hover + #MenuStrumenti {
            display: block;
        }

        #idStrumenti:hover {
            display: block;
        }

        #MenuOspiti > #idStrumenti:hover {
            display: block;
        }

        #MenuOspiti > #MenuStrumenti:hover {
            display: block;
        }


        #IdMenuGenerale:hover + #MenuGenerale {
            display: block;
        }


        #MenuGenerale:hover {
            display: block;
        }




        #idBudget:hover + #MenuBudget {
            display: block;
        }

        #idBudget:hover {
            display: block;
        }

        #MenuGenerale > #idBudget:hover {
            display: block;
        }

        #MenuGenerale > #MenuBudget:hover {
            display: block;
        }






        #idStampeGenerale:hover + #MenuStampeGenerale {
            display: block;
        }

        #idStampeGenerale:hover {
            display: block;
        }

        #MenuGenerale > #idStampeGenerale:hover {
            display: block;
        }

        #MenuGenerale > #MenuStampeGenerale:hover {
            display: block;
        }


        #idVisualizzazione:hover + #MenuVisualizzazione {
            display: block;
        }

        #idVisualizzazione:hover {
            display: block;
        }

        #MenuGenerale > #idVisualizzazione:hover {
            display: block;
        }

        #MenuGenerale > #MenuVisualizzazione:hover {
            display: block;
        }



        #idTabelleGenerale:hover + #MenuTabelleGenerale {
            display: block;
        }

        #idTabelleGenerale:hover {
            display: block;
        }

        #MenuGenerale > #idTabelleGenerale:hover {
            display: block;
        }

        #MenuGenerale > #MenuTabelleGenerale:hover {
            display: block;
        }





        #idStrumentiGenerale:hover + #MenuStrumentiGenerale {
            display: block;
        }

        #idStrumentiGenerale:hover {
            display: block;
        }

        #MenuGenerale > #idStrumentiGenerale:hover {
            display: block;
        }

        #MenuGenerale > #MenuStrumentiGenerale:hover {
            display: block;
        }





        #idMenuProtocollo:hover + #MenuProtocollo {
            display: block;
        }


        #MenuProtocollo:hover {
            display: block;
        }






        #idMenuMailingList:hover + #MenuMailingList {
            display: block;
        }


        #MenuMailingList:hover {
            display: block;
        }




        .custom-menu li:hover {
            background-color: #DEF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="False">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel ID="updatepanel1" runat="server">
            <ContentTemplate>
                <div>
                    <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>


                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Principale</div>
                                <div class="SottoTitolo">
                                    <br />
                                    <br />
                                </div>
                            </td>

                            <td style="text-align: right; vertical-align: top;">
                                <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                            </td>
                        </tr>
                        <tr>


                            <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                                <a href="../MainMenu.aspx">
                                    <img src="../images/Menu_Indietro.png" class="Effetto" /></a>

                            </td>
                            <td colspan="2" style="vertical-align: top;">
                                <table style="width: 60%;">
                                    <tr>

                                        <td style="text-align: center;">
                                            <div id="menutastodestro">
                                                <a href="RicercaAnagrafica.aspx" id="idospiti">
                                                    <img src="../images/Menu_Ospite.png" class="Effetto" alt="Ospite" class="Effetto" style="border-width: 0;"></a>
                                            </div>




                                            <ul class="custom-menu" id="rightmenu">
                                                <a href="RicercaAnagrafica.aspx" id="A1">
                                                    <li data-action="first">Anagrafica</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=ADDACR" id="A2">
                                                    <li data-action="first">Addebiti/Accrediti</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=MOVIMENTI" id="A3">
                                                    <li data-action="first">Movimenti</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=DIURNO" id="A4">
                                                    <li data-action="first">Diurno</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=GESTIONEDENARO" id="A5">
                                                    <li data-action="first">Denaro</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=FATTURANC" id="A6">
                                                    <li data-action="first">Fatture/NC</li>
                                                </a>
                                                <a href="RicercaAnagrafica.aspx?TIPO=INCASSIANTICIPI" id="A7">
                                                    <li data-action="first">Incassi Anticipi</li>
                                                </a>
                                            </ul>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="MovimentiDiurniCentroServizio.aspx" id="idDiurni">
                                                <img role="tooltip" alt="Modifica Diurni CentroServizio" src="../images/Menu_DiurnoMultipli.jpg" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="MovimentiDomiciliariCentroServizio.aspx" id="idDomiciliari">
                                                <img alt="Domiciliari" src="../images/Menu_Domiciliari.jpg" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="AddebitiAccreditiMultipli.aspx" id="IDAddebitiAccreditiMultipli">
                                                <img src="../images/Menu_AddebitiAccreditiMultipli.jpg" alt="Addebiti Multipli" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;"><a href="Menu_Visualizzazioni.aspx" id="IdStatistiche">
                                            <img src="../images/Menu_Visualizzazioni.png" class="Effetto" alt="Visualizzazione" style="border-width: 0;"></a></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">OSPITE</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRE.DIURNO CSERV</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">DOMICILIARI</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">ADDEBITI MULTIPLI</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">STATISTICHE</span></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center;">
                                            <a href="Calcolo.aspx" id="idcalcolo">
                                                <img alt="Calcolo" src="../images/Menu_Calcolo.png" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="Rendiconto.aspx">
                                                <img alt="Fatture Dettagliate" src="../images/Menu_Rendiconto.jpg" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="EmissioneRette.aspx" id="idemissione">
                                                <img src="../images/Menu_Emissione.png" class="Effetto" alt="Emissione" style="border-width: 0;"></a>
                                        </td>

                                        <td style="text-align: center;">
                                            <a href="StampaDocumenti.aspx">
                                                <img alt="Fatture" class="Effetto" src="../images/Menu_StampaDocumenti.png" style="border-width: 0;"></a>
                                        </td>

                                        <td style="text-align: center;">
                                            <a href="ricalcolo.aspx">
                                                <img alt="Calcolo" src="../images/Menu_Ricalcolo.jpg" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">CALCOLO</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">RENDICONTO</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">EMISSIONE</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAMPA DOCUMENTI</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">RICALCOLO</span></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center;">
                                            <a href="push/CalcoloEmissioneStampaFatture.aspx" id="IdElaborazione">
                                                <img alt="Calcolo/Emissione/Stampa Fatture" src="../images/Menu_Notifica.png" class="Effetto" style="border-width: 0;"></a>

                                        </td>


                                        <td style="text-align: center;">
                                            <a href="Menu_Epersonam.aspx" id="IdEPersonam">
                                                <img alt="Servizi" src="../images/Menu_EPersonam.PNG" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="Menu_Export.aspx" id="IdExport">
                                                <img alt="Servizi" src="../images/Menu_Export_1.png" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="Menu_Tabelle.aspx" id="idtabelle">
                                                <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Taeblle" style="border-width: 0;"></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="Menu_Strumenti.aspx" id="idstrumenti">
                                                <img alt="Servizi" src="../images/Menu_Strumenti.png" class="Effetto" style="border-width: 0;"></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">ELABOR.FATTURE</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">ePERSONAM</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXPORT</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">TABELLE</span></td>
                                        <td style="text-align: center; vertical-align: top;"><span class="MenuText">STRUMENTI</span></td>
                                    </tr>


                                </table>


                            </td>
                        </tr>
                    </table>

                </div>

                <div id="news" style="position: absolute; right: 10px; top: 120; overflow: auto;">
                    <h1>Novità:</h1>
                    <br />
                    <b>Imposta di bollo sulle fatture cartacee, elettroniche e documenti elettronici rilevanti ai fini fiscali</b><br />
                    <br />
                    Nei documenti fattura per i quali è previsto il bollo, il documento avrà il flg valorizzato in modo automatico per le fatture soggette a bollo nei registri non cartacei tale per cui in fase di generazione del documento in formato elettronico vi sia specifica annotazione.<br />
                    <br />
                    <b>Novità:</b><br />
                    <br />
                    La tipologia di registro non cartaceo è attivabile tramite parametro di configurazione. Nella Tabella Tipo Registro, disponibile nel Menù Generale - Tabelle - Tipo Registro, è possibile indicare se il registro è cartaceo con valorizzazione del parametro <i>Registro Cartaceo</i><br />
                    <br />
                    La gestione del bollo è attivabile tramite parametro di configurazione per tutti i documenti/tipo operazione dove è possibile indicare se soggetti a Bollo e se Bollo Virtuale.<br />
                    Occorre nella Tabella Tipo Operazione, disponibile nel Menù Ospiti - Tabelle - Menu' Configurazione, indicare la configurazione voluta con apposita valorizzazione dei flg.<br />
                    <br />
                    <b>Gestione Scadenziario</b><br />
                    <br />
                    Per gli utenti con la gestione dello scadenziario nei documenti, è stata data la possibilità di legare tassativamente il pagamento alla scadenza<br />
                    Questo controllo è attivabile tramite parametro disponibile nel Menù Generale - Tabelle - Dati Generali.<br />
                    Il parametro si chiama <i>Blocca Legame su documenti con Scadenzario</i><br />
                    Inoltre è attivabile il parametro <i>Scadenziario Check Chiuso</i> che rende obbligatoria la chiusura della scadenza durante il pagamento.<br />
                    <br />
                    <b>Novità:</b>
                    Per gli utenti con la gestione dello scadenziario nei documenti, è stata data la possibilità di inserire ulteriori date di scadenza tra quelle presentate dal tipo di pagamento.

       <br />
                    <br />
                    <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
                    <br />
                    <h1>Novità:</h1>
                    <br />
                    E’ stata introdotta la nuova funzione ELABORAZIONE FATTURE nel Menù Ospiti – Principale con la possibilità di eseguire in un solo comando sial il CALCOLO RETTE che l’ELABORAZIONE delle stesse.<br />
                    <br />
                    L’utente, in fase di esecuzione del comando, potrà accedere a tutte le funzioni di Senior e sarà cura del sistema informare tramite messaggio sia nel Menù Principale che nella notifica di Google Chrome l’elaborazione terminata.<br />
                    <br />
                    I messaggi di eventuali errori o segnalazioni di Warning sono invariati rispetto al funzionamento di prodotto così come la Conferma e la procedura di stampa dei file fatture.<br />
                    <br />
                    <br />
                    <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
                    <br />
                    <h1>Novità:</h1>
                    <br />
                    E’ stata data una nuova organizzazione al menù principale  <b>Ospiti – Principale</b><br />
                    <br />
                    È presente il nuovo comando <b>ePERSONAM </b>all’interno del quale sono presenti tutte le funzioni di import dei dati dall’applicativo ePersonam vs l’applicativo Senior<br />
                    <br />
                    È presente il comando <b>EXPORT</b>all’interno del quale sono disponibili tutte le funzioni di generazione dei file xml e/o export dati precedentemente disponibili all’interno del comando <b>STRUMENTI</b><br />
                    <br />
                    <br />
                    <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
                    <br />
                    <h1>Novità:</h1>
                    E’ stata introdotta la possibilità di gestire i LISTINI RETTE da abilitare mediante creazione di uno o più listini a seconda delle diverse rette al fine di associarlo a ciascun ospite.<br />
                    <br />
                    Per attivare tale nuova funzione occorre accedere alla seguente tabella LISTINO disponibile da Ospiti - Tabelle – Listino e compilare i vari attori con i dati dei differenti contributi.<br />
                    <br />
                    Accedendo al dettaglio di ciascun ospite, sarà presente nel menù laterale di sx il nuovo tab LISTINO mediante il quale si potrà fare l’associazione tra ospite ed il listino rette di riferimento. E’ stata introdotta la possibilità di gestire i LISTINI RETTE da abilitare mediante creazione di uno o più listini a seconda della propria offerta al fine di associarlo a ciascun ospite.<br />
                    <br />
                    Per attivare tale nuova funzione occorre accedere alla seguente tabella LISTINO disponibile da Ospiti - Tabelle – Listino e compilare i vai attori con i dati della propria offerta.<br />
                    <br />
                    Accedendo al dettaglio di ciascun ospite, sarà presente nel menù laterale di sx il nuovo tab LISTINO mediante il quale si potrà fare l’associazione tra ospite ed il listino di riferimento<br />
                    <br />
                    <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
                </div>

                <div id="notifiche" style="position: absolute; right: 10px; overflow: auto;">
                    <asp:Button ID="BtnClose" runat="server" Text="X" Style="position: absolute; right: 4px; top: 2px;" />

                    <asp:Label ID="lblElaborazioneCompletata" runat="server" Text=""></asp:Label>
                </div>
                <asp:Timer ID="Notifica" runat="server" Interval="20000">
                </asp:Timer>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>

</html>
