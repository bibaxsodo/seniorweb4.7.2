﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.AsymmetricAlgorithm
Imports System.Security.Cryptography.RSA
Imports System.Security.Cryptography.RSACryptoServiceProvider
Imports System.Text

Partial Class OspitiWeb_ImportExcelToXml
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Public Function CodificaCF(ByVal CF As String) As String

        Try

            Dim Certificate As String = HostingEnvironment.ApplicationPhysicalPath() & "\Certificati\SanitelCF.cer"
            Dim cert As New X509Certificate2(Certificate)
            Dim rsaEncryptor As System.Security.Cryptography.RSACryptoServiceProvider = CType(cert.PublicKey.Key, System.Security.Cryptography.RSACryptoServiceProvider)

            Dim stringa As Byte() = System.Text.Encoding.ASCII.GetBytes(CF)
            Dim cipherData() As Byte = rsaEncryptor.Encrypt(stringa, False)

            Return Convert.ToBase64String(cipherData)

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim cn As OdbcConnection
        Dim nomefileexcel As String
        Dim MyXML As String = ""
        Dim Errori As String = ""
        Dim FineFile As Integer = 0


        lbl_errori.Text = ""
        If Txt_CodiceRegione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Regione non indicato');", True)
            Exit Sub
        End If

        If Txt_CodiceAsl.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Regione non indicato');", True)
            Exit Sub
        End If


        If Txt_CodiceSSA.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice SSA non indicato');", True)
            Exit Sub
        End If

        If Txt_CFPropietario.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('CF proprietario non indicato');", True)
            Exit Sub
        End If

        If Txt_PIVA.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Partita Iva non indicata');", True)
            Exit Sub
        End If


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If


        Dim NomeSocieta As String = "xml"
        Dim Appo As String
        Randomize()
        Dim NomeFile As String = Format(Now, "ddMMyyyyhhmmss") & Int(Rnd(1) * 10)

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File excel non trovata');", True)
            Exit Sub
        End If

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("nome", GetType(String))
        MyTable.Columns.Add("codicefiscale", GetType(String))
        MyTable.Columns.Add("datadocumento", GetType(String))
        MyTable.Columns.Add("numerodocumento", GetType(String))
        MyTable.Columns.Add("datapagamento", GetType(String))
        MyTable.Columns.Add("flagoperazione", GetType(String))
        MyTable.Columns.Add("tipospesa", GetType(String))
        MyTable.Columns.Add("importo", GetType(String))
        MyTable.Columns.Add("datadocumentorimborso", GetType(String))
        MyTable.Columns.Add("numerodocumentorimborso", GetType(String))

        'codicefiscale
        'nome	codicefiscale	datadocumento	numerodocumento	datapagamento	flagoperazione	tipospesa	importo	datadocumentorimborso	numerodocumentorimborso


        Dim numriga As Integer

        'Try

        '    Dim excelApp As Microsoft.Office.Interop.Excel.Application
        '    Dim FileExcel As Microsoft.Office.Interop.Excel.Workbook
        '    Dim FoglioExcel As Microsoft.Office.Interop.Excel.Worksheet
        '    Dim RangeExcel As Microsoft.Office.Interop.Excel.Range


        '    'applicazione Excel
        '    excelApp = New Microsoft.Office.Interop.Excel.Application
        '    'cartella di lavoro Excel 
        '    FileExcel = excelApp.Workbooks.Open(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls")
        '    excelApp.Visible = True


        '    FileExcel.Activate()

        '    'seleziono il primo foglio

        '    FoglioExcel = FileExcel.Worksheets(1)
        '    RangeExcel = FoglioExcel.Range("A1", "A88")

        '    'RangeExcel = FoglioExcel.Cells(1, 4) 'prima riga quarta colonna 

        '    For numriga = 2 To FoglioExcel.Rows.Count
        '        Dim appoggio As String = ""

        '        If Not IsNothing(FoglioExcel.Cells(numriga, 1).value) Then
        '            appoggio = FoglioExcel.Cells(numriga, 1).value()
        '        End If

        '        If appoggio <> "" Then
        '            Dim myriga As System.Data.DataRow = MyTable.NewRow()
        '            myriga(0) = FoglioExcel.Cells(numriga, 1).value
        '            myriga(1) = FoglioExcel.Cells(numriga, 2).value
        '            myriga(2) = FoglioExcel.Cells(numriga, 3).value
        '            myriga(3) = FoglioExcel.Cells(numriga, 4).value
        '            myriga(4) = FoglioExcel.Cells(numriga, 5).value
        '            myriga(5) = FoglioExcel.Cells(numriga, 6).value
        '            myriga(6) = FoglioExcel.Cells(numriga, 7).value
        '            myriga(7) = FoglioExcel.Cells(numriga, 8).value
        '            myriga(8) = FoglioExcel.Cells(numriga, 9).value
        '            myriga(9) = FoglioExcel.Cells(numriga, 10).value




        '            MyTable.Rows.Add(myriga)
        '        Else
        '            Exit For
        '        End If
        '    Next
        '    FineFile = numriga - 2
        '    RangeExcel = Nothing
        '    FoglioExcel = Nothing
        '    FileExcel = Nothing
        '    excelApp.Quit()
        '    excelApp = Nothing
        'Catch ex As Exception
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('formato file non corretto " & ex.Message.Replace("'", "") & " ');", True)
        '    Exit Sub
        'End Try
        Dim conn As System.Data.OleDb.OleDbConnection
        Dim dataReader As System.Data.OleDb.OleDbDataReader
        Dim cmd As System.Data.OleDb.OleDbCommand
        Dim conStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls;Extended Properties='Excel 12.0 Xml;HDR=Yes;'"

        
        conn = New System.Data.OleDb.OleDbConnection(conStr)
        conn.Open()

        cmd = New System.Data.OleDb.OleDbCommand("select * from [FOGLIO$]", conn)


        dataReader = cmd.ExecuteReader()
        numriga = 0
        While (dataReader.Read())
            If dataReader.GetValue(0).ToString = "" Then
                Exit While
            End If
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = dataReader.GetValue(0).ToString
            myriga(1) = dataReader.GetValue(1).ToString
            myriga(2) = dataReader.GetValue(2).ToString
            myriga(3) = dataReader.GetValue(3).ToString
            myriga(4) = dataReader.GetValue(4).ToString
            myriga(5) = dataReader.GetValue(5).ToString
            myriga(6) = dataReader.GetValue(6).ToString
            myriga(7) = dataReader.GetValue(7).ToString
            myriga(8) = dataReader.GetValue(8).ToString
            myriga(9) = dataReader.GetValue(9).ToString
            MyTable.Rows.Add(myriga)
            numriga = numriga + 1
        End While
        conn.Close()

        FineFile = numriga
        'Try
        '    'cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls;ReadOnly=0")
        '    'Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & "\\lettsisa02\DHL Reports\ROSSTEST" & ";Extended Properties=""text;HDR=No;FMT=Delimite
        '    cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".csv;Extensions=asc,csv,tab,txt;Persist Security Info=False")

        '    cn.Open()

        'Catch ex As Exception
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('formato file non corretto (xls 97)');", True)
        '    Exit Sub
        'End Try


        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
        MyXML = MyXML & "<precompilata xsi:noNamespaceSchemaLocation=""730_precompilata.xsd"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbNewLine
        MyXML = MyXML & "<proprietario>" & vbNewLine
        If Txt_CodiceRegione.Text.Trim <> "" Then
            MyXML = MyXML & "<codiceRegione>" & Txt_CodiceRegione.Text.Trim & "</codiceRegione>" & vbNewLine
        End If
        If Txt_CodiceAsl.Text.Trim <> "" Then
            MyXML = MyXML & "<codiceAsl>" & Txt_CodiceAsl.Text.Trim & "</codiceAsl>" & vbNewLine
        End If
        If Txt_CodiceSSA.Text.Trim <> "" Then
            MyXML = MyXML & "<codiceSSA>" & Txt_CodiceSSA.Text.Trim & "</codiceSSA>" & vbNewLine
        End If
        MyXML = MyXML & "<cfProprietario>" & CodificaCF(Txt_CFPropietario.Text.Trim) & "</cfProprietario>" & vbNewLine
        MyXML = MyXML & "</proprietario>" & vbNewLine

        Dim codicefiscale As String = ""
        Dim datadocumento As String = ""
        Dim numerodocumento As String = ""
        Dim datapagamento As String = ""
        Dim flagoperazione As String = ""
        Dim tipospesa As String = ""
        Dim importo As String = ""
        Dim datadocumentorimborso As String = ""
        Dim numerodocumentorimborso As String = ""
        Dim nome As String = ""

        Dim Pretipospesa As String = ""
        Dim Preimporto As Double = 0
        Dim Preimporto2 As Double = 0
        Dim Preimporto3 As Double = 0
        Dim Preimporto4 As Double = 0

        Try

            'Dim cmd As New OdbcCommand()
            'cmd.CommandText = ("select * from [FOGLIO$]")

            'cmd.Connection = cn

            'Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()



            'Do While myPOSTreader.Read
            For numriga = 0 To FineFile - 1


                Dim myriga As System.Data.DataRow = MyTable.Rows(numriga)

                If campodb(myriga.Item("nome")) = "" Then Exit For


                If numerodocumento <> campodb(myriga.Item("numerodocumento")) And numerodocumento <> "" Then

                    datadocumento = Mid(datadocumento, 7, 4) & "-" & Mid(datadocumento, 4, 2) & "-" & Mid(datadocumento, 1, 2)
                    datapagamento = Mid(datapagamento, 7, 4) & "-" & Mid(datapagamento, 4, 2) & "-" & Mid(datapagamento, 1, 2)
                    MyXML = MyXML & "<documentoSpesa>" & vbNewLine
                    MyXML = MyXML & "<idSpesa>" & vbNewLine
                    MyXML = MyXML & "<pIva>" & Txt_PIVA.Text & "</pIva>" & vbNewLine
                    MyXML = MyXML & "<dataEmissione>" & datadocumento & "</dataEmissione>" & vbNewLine
                    MyXML = MyXML & "<numDocumentoFiscale>" & vbNewLine
                    MyXML = MyXML & "<dispositivo>" & Txt_Dispositivo.Text & "</dispositivo>" & vbNewLine
                    MyXML = MyXML & "<numDocumento>" & numerodocumento.Trim & "</numDocumento>" & vbNewLine
                    MyXML = MyXML & "</numDocumentoFiscale>" & vbNewLine
                    MyXML = MyXML & "</idSpesa>" & vbNewLine

                    If flagoperazione = "R" Then


                        datadocumentorimborso = Mid(datadocumentorimborso, 7, 4) & "-" & Mid(datadocumentorimborso, 4, 2) & "-" & Mid(datadocumentorimborso, 1, 2)

                        If (datadocumentorimborso = "" Or datadocumentorimborso = "--") And codicefiscale <> "" Then
                            Errori = Errori & "<tr><td>" & nome & "</td>"
                            Errori = Errori & "<td>Data documento rimborsoo non indicato</td>"
                            Errori = Errori & "<td>" & codicefiscale & "</td>"
                            Errori = Errori & "<td>" & datadocumento & "</td>"
                            Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                            Errori = Errori & "</tr>"
                        End If

                        If Not IsDate(datadocumentorimborso) And codicefiscale <> "" Then
                            Errori = Errori & "<tr><td>" & nome & "</td>"
                            Errori = Errori & "<td>Data documento rimborsoo non indicato</td>"
                            Errori = Errori & "<td>" & codicefiscale & "</td>"
                            Errori = Errori & "<td>" & datadocumento & "</td>"
                            Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                            Errori = Errori & "</tr>"
                        End If

                        MyXML = MyXML & "<idRimborso>" & vbNewLine
                        MyXML = MyXML & "<pIva>" & Txt_PIVA.Text & "</pIva>" & vbNewLine
                        MyXML = MyXML & "<dataEmissione>" & datadocumentorimborso & "</dataEmissione>" & vbNewLine
                        MyXML = MyXML & "<numDocumentoFiscale>" & vbNewLine
                        MyXML = MyXML & "<dispositivo>" & Txt_Dispositivo.Text & "</dispositivo>" & vbNewLine
                        MyXML = MyXML & "<numDocumento>" & numerodocumentorimborso & "</numDocumento>" & vbNewLine
                        MyXML = MyXML & "</numDocumentoFiscale>" & vbNewLine
                        MyXML = MyXML & "</idRimborso>" & vbNewLine
                    End If
                    MyXML = MyXML & "<dataPagamento>" & datapagamento & "</dataPagamento>" & vbNewLine
                    If datapagamento < datadocumento Then
                        MyXML = MyXML & "<flagPagamentoAnticipato>1</flagPagamentoAnticipato>" & vbNewLine
                    End If
                    MyXML = MyXML & "<flagOperazione>" & flagoperazione & "</flagOperazione>" & vbNewLine
                    MyXML = MyXML & "<cfCittadino>" & CodificaCF(codicefiscale) & "</cfCittadino>" & vbNewLine
                    MyXML = MyXML & "<voceSpesa>" & vbNewLine
                    MyXML = MyXML & "<tipoSpesa>" & tipospesa.Trim & "</tipoSpesa>" & vbNewLine
                    MyXML = MyXML & "<importo>" & Format(Math.Abs(CDbl(importo)), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                    MyXML = MyXML & "</voceSpesa>" & vbNewLine

                    If Math.Abs(CDbl(importo)) = 0 Then
                        Errori = Errori & "<tr><td>" & nome & "</td>"
                        Errori = Errori & "<td>Errore importo a zero</td>"
                        Errori = Errori & "<td>" & codicefiscale & "</td>"
                        Errori = Errori & "<td>" & datadocumento & "</td>"
                        Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                        Errori = Errori & "</tr>"
                    End If

                    If Preimporto > 0 Then
                        MyXML = MyXML & "<voceSpesa>" & vbNewLine
                        MyXML = MyXML & "<tipoSpesa>" & tipospesa.Trim & "</tipoSpesa>" & vbNewLine
                        MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                        MyXML = MyXML & "</voceSpesa>" & vbNewLine
                    End If
                    If Preimporto2 > 0 Then
                        MyXML = MyXML & "<voceSpesa>" & vbNewLine
                        MyXML = MyXML & "<tipoSpesa>" & tipospesa.Trim & "</tipoSpesa>" & vbNewLine
                        MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto2), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                        MyXML = MyXML & "</voceSpesa>" & vbNewLine
                    End If
                    If Preimporto3 > 0 Then
                        MyXML = MyXML & "<voceSpesa>" & vbNewLine
                        MyXML = MyXML & "<tipoSpesa>" & tipospesa.Trim & "</tipoSpesa>" & vbNewLine
                        MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto3), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                        MyXML = MyXML & "</voceSpesa>" & vbNewLine
                    End If
                    If Preimporto4 > 0 Then
                        MyXML = MyXML & "<voceSpesa>" & vbNewLine
                        MyXML = MyXML & "<tipoSpesa>" & tipospesa.Trim & "</tipoSpesa>" & vbNewLine
                        MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto4), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                        MyXML = MyXML & "</voceSpesa>" & vbNewLine
                    End If

                    MyXML = MyXML & "</documentoSpesa>" & vbNewLine
                End If
                If numerodocumento = campodb(myriga.Item("numerodocumento")) Then
                    If Preimporto > 0 Then
                        If Preimporto2 > 0 Then
                            If Preimporto3 > 0 Then
                                If importo = "" Then
                                    Preimporto4 = 0
                                Else
                                    Preimporto4 = importo
                                End If
                            Else
                                If importo = "" Then
                                    Preimporto3 = 0
                                Else
                                    Preimporto3 = importo
                                End If
                            End If
                        Else
                            If importo = "" Then
                                Preimporto2 = 0
                            Else
                                Preimporto2 = importo
                            End If
                        End If
                    Else
                        Pretipospesa = tipospesa
                        If importo = "" Then
                            Preimporto = 0
                        Else
                            Preimporto = importo
                        End If
                    End If
                Else
                    Pretipospesa = ""
                    Preimporto = 0
                    Preimporto2 = 0
                    Preimporto3 = 0
                    Preimporto4 = 0
                End If

                codicefiscale = campodb(myriga.Item("codicefiscale"))
                datadocumento = campodb(myriga.Item("datadocumento"))
                numerodocumento = campodb(myriga.Item("numerodocumento"))
                nome = campodb(myriga.Item("nome"))
                datadocumentorimborso = campodb(myriga.Item("datadocumentorimborso"))
                numerodocumentorimborso = campodb(myriga.Item("numerodocumentorimborso"))

                Dim M As New Cls_CodiceFiscale

                If Not M.Check_CodiceFiscale(codicefiscale) And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Codice Fiscale Formalmente Errato</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                datapagamento = campodb(myriga.Item("datapagamento"))

                If Not IsDate(datapagamento) And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data pagamento formalmente errata </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                Else
                    If Year(datapagamento) < 1900 Then
                        Errori = Errori & "<tr><td>" & nome & "</td>"
                        Errori = Errori & "<td>Data pagamento formalmente errata </td>"
                        Errori = Errori & "<td>" & codicefiscale & "</td>"
                        Errori = Errori & "<td>" & datadocumento & "</td>"
                        Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                        Errori = Errori & "</tr>"
                    End If
                End If
                If (datadocumento = "" Or datadocumento = "--") And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data Documento formalmente errata </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                If Not IsDate(datadocumento) And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data Documento formalmente errata </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                Else
                    If Year(datadocumento) < 1900 Then
                        Errori = Errori & "<tr><td>" & nome & "</td>"
                        Errori = Errori & "<td>Data documento formalmente errata </td>"
                        Errori = Errori & "<td>" & codicefiscale & "</td>"
                        Errori = Errori & "<td>" & datadocumento & "</td>"
                        Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                        Errori = Errori & "</tr>"
                    End If
                End If

                flagoperazione = campodb(myriga.Item("flagoperazione"))
                datadocumentorimborso = campodb(myriga.Item("datadocumentorimborso"))
                numerodocumentorimborso = campodb(myriga.Item("numerodocumentorimborso"))
                If flagoperazione = "IN" Then
                    flagoperazione = "I"
                End If
                If flagoperazione = "RI" Then
                    flagoperazione = "R"
                End If

                

                If numerodocumento = "" And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Numero Documento errato </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                If flagoperazione = "" And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Flag operazione formalmente errato </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If


                If flagoperazione = "RI" And codicefiscale <> "" And Not IsDate(datadocumentorimborso) Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data documento rimborso errata</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                If flagoperazione = "RI" And codicefiscale <> "" And numerodocumentorimborso = "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Documento di rimborso errato</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                tipospesa = campodb(myriga.Item("tipospesa"))
                If tipospesa = "" And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Tipo spesa formalmente errato </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                If tipospesa <> "SR" And tipospesa <> "CT" And tipospesa <> "PI" And tipospesa <> "AA" And tipospesa <> "TK" And tipospesa <> "IC" And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Tipo spesa formalmente errato </td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If


                importo = campodb(myriga.Item("importo"))
                If CDbl(importo) = 0 Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Importo uguale a zero</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If
            Next

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('formato file non corretto (xls 97)');", True)
            'cn.Close()
            Exit Sub
        End Try


        'cn.Close()
        If numerodocumento <> "" Then

            datadocumento = Mid(datadocumento, 7, 4) & "-" & Mid(datadocumento, 4, 2) & "-" & Mid(datadocumento, 1, 2)
            datapagamento = Mid(datapagamento, 7, 4) & "-" & Mid(datapagamento, 4, 2) & "-" & Mid(datapagamento, 1, 2)
            MyXML = MyXML & "<documentoSpesa>" & vbNewLine
            MyXML = MyXML & "<idSpesa>" & vbNewLine
            MyXML = MyXML & "<pIva>" & Txt_PIVA.Text & "</pIva>" & vbNewLine
            MyXML = MyXML & "<dataEmissione>" & datadocumento & "</dataEmissione>" & vbNewLine
            MyXML = MyXML & "<numDocumentoFiscale>" & vbNewLine
            MyXML = MyXML & "<dispositivo>" & Txt_Dispositivo.Text & "</dispositivo>" & vbNewLine
            MyXML = MyXML & "<numDocumento>" & numerodocumento.Trim & "</numDocumento>" & vbNewLine
            MyXML = MyXML & "</numDocumentoFiscale>" & vbNewLine
            MyXML = MyXML & "</idSpesa>" & vbNewLine

            If flagoperazione = "R" Then
                MyXML = MyXML & "<idRimborso>" & vbNewLine
                MyXML = MyXML & "<pIva>" & Txt_PIVA.Text & "</pIva>" & vbNewLine
                MyXML = MyXML & "<dataEmissione>" & datadocumentorimborso & "</dataEmissione>" & vbNewLine
                MyXML = MyXML & "<numDocumentoFiscale>" & vbNewLine
                MyXML = MyXML & "<dispositivo>" & Txt_Dispositivo.Text & "</dispositivo>" & vbNewLine
                MyXML = MyXML & "<numDocumento>" & numerodocumentorimborso & "</numDocumento>" & vbNewLine
                MyXML = MyXML & "</numDocumentoFiscale>" & vbNewLine
                MyXML = MyXML & "</idRimborso>" & vbNewLine

                If numerodocumentorimborso = "" And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Numero documento rimborsoo non indicato</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If


                If (datadocumentorimborso = "" Or datadocumentorimborso = "--") And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data documento rimborsoo non indicato</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If

                If Not IsDate(datadocumentorimborso) And codicefiscale <> "" Then
                    Errori = Errori & "<tr><td>" & nome & "</td>"
                    Errori = Errori & "<td>Data documento rimborsoo non indicato</td>"
                    Errori = Errori & "<td>" & codicefiscale & "</td>"
                    Errori = Errori & "<td>" & datadocumento & "</td>"
                    Errori = Errori & "<td style=""text-align:right;"">" & numerodocumento & "</td>"
                    Errori = Errori & "</tr>"
                End If


            End If
            MyXML = MyXML & "<dataPagamento>" & datapagamento & "</dataPagamento>" & vbNewLine
            If datapagamento < datadocumento Then
                MyXML = MyXML & "<flagPagamentoAnticipato>1</flagPagamentoAnticipato>" & vbNewLine
            End If
            MyXML = MyXML & "<flagOperazione>" & flagoperazione & "</flagOperazione>" & vbNewLine
            MyXML = MyXML & "<cfCittadino>" & CodificaCF(codicefiscale) & "</cfCittadino>" & vbNewLine
            MyXML = MyXML & "<voceSpesa>" & vbNewLine
            MyXML = MyXML & "<tipoSpesa>" & tipospesa & "</tipoSpesa>" & vbNewLine
            MyXML = MyXML & "<importo>" & Format(Math.Abs(CDbl(importo)), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
            MyXML = MyXML & "</voceSpesa>" & vbNewLine
            If Preimporto > 0 Then
                MyXML = MyXML & "<voceSpesa>" & vbNewLine
                MyXML = MyXML & "<tipoSpesa>" & tipospesa & "</tipoSpesa>" & vbNewLine
                MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                MyXML = MyXML & "</voceSpesa>" & vbNewLine
            End If
            If Preimporto2 > 0 Then
                MyXML = MyXML & "<voceSpesa>" & vbNewLine
                MyXML = MyXML & "<tipoSpesa>" & tipospesa & "</tipoSpesa>" & vbNewLine
                MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto2), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                MyXML = MyXML & "</voceSpesa>" & vbNewLine
            End If
            If Preimporto3 > 0 Then
                MyXML = MyXML & "<voceSpesa>" & vbNewLine
                MyXML = MyXML & "<tipoSpesa>" & tipospesa & "</tipoSpesa>" & vbNewLine
                MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto3), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                MyXML = MyXML & "</voceSpesa>" & vbNewLine
            End If
            If Preimporto4 > 0 Then
                MyXML = MyXML & "<voceSpesa>" & vbNewLine
                MyXML = MyXML & "<tipoSpesa>" & tipospesa & "</tipoSpesa>" & vbNewLine
                MyXML = MyXML & "<importo>" & Format(Math.Abs(Preimporto4), "0.00").Replace(",", ".") & "</importo>" & vbNewLine
                MyXML = MyXML & "</voceSpesa>" & vbNewLine
            End If


            MyXML = MyXML & "</documentoSpesa>" & vbNewLine
        End If
        MyXML = MyXML & "</precompilata>" & vbNewLine
        'Catch ex As Exception
        '' ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & ex.Message.Replace("'", "") & "');", True)
        'Exit Sub
        'End Try

        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & NomeFile & ".xls")
        Catch ex As Exception

        End Try
        Session("ERRORIIMPORT") = Errori

        Session("MyXML") = MyXML
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() { __doPostBack('Btn_Dowload', '0');  DialogBoxx('ReportXML730.aspx');  });", True)

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub OspitiWeb_ImportExcelToXml_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub

    Protected Sub btn_codifica_Click(sender As Object, e As EventArgs) Handles btn_codifica.Click

        Txt_Pin.Text = CodificaCF(Txt_Pin.Text)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Session("ABILITAZIONI").IndexOf("<SOLO730>") >= 0 Then
            Response.Redirect("../Menu730.aspx")
        Else
            Response.Redirect("Menu_Export.aspx")
        End If

    End Sub


    Protected Sub Btn_Dowload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Dowload.Click

        Dim responseText As String = Session("MyXML")
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=730telematico.xml")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8
        REM Response.ContentEncoding = Encoding.UTF16
        Response.ContentType = "text/xml; charset=utf-8"
        REM Response.ContentType = "text/xml; charset=utf-16"
        Response.Write(responseText)
        Response.End()
    End Sub
End Class
