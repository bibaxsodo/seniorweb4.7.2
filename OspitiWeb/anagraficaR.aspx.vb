﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_anagraficaR
    Inherits System.Web.UI.Page

    Private Delegate Sub DoWorkDelegate(ByRef data As Object)
    Private Sub AutentificaLogin(ByVal utente As String)
        Dim tkt As FormsAuthenticationTicket
        Dim cookiestr As String
        Dim ck As HttpCookie

        tkt = New FormsAuthenticationTicket(1, utente, DateTime.Now, DateTime.Now.AddMinutes(30), True, "")
        cookiestr = FormsAuthentication.Encrypt(tkt)
        ck = New HttpCookie(FormsAuthentication.FormsCookieName, cookiestr)
        ck.Expires = tkt.Expiration
        ck.Path = FormsAuthentication.FormsCookiePath
        Response.Cookies.Add(ck)

    End Sub
    Protected Sub OspitiWeb_anagraficaR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If
        If Request.Form("Txt_Login") <> "" And Request.Form("Txt_Enc_Password") <> "" Then
        Else
            If Trim(Session("UTENTE")) = "" Then
                Response.Redirect("..\Login.aspx")
                Exit Sub
            End If
        End If

        Dim Token As String = ""

        Dim AppoggioLOG As String = ""


        Dim M As New ValidaJwtws.WebService
        Dim TokenDAE As String = ""
        Dim UtenteDiEpersonam As String = ""

        TokenDAE = Request.Form("token")
        If TokenDAE <> "" Then

            Dim Utente As String = M.ValidaJwtws(TokenDAE)


            Dim jResults As JObject = JObject.Parse(Utente)

            Try
                UtenteDiEpersonam = jResults.Item("una").ToString
            Catch ex As Exception

            End Try
        End If


        Session("INIZIO") = Now
        Session("CampoProgressBar") = 0
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0

        Cache("CampoProgressBar" + Session.SessionID) = 0


        If UtenteDiEpersonam <> "" Then
            Call ForzaLogin(UtenteDiEpersonam)
            AutentificaLogin(UtenteDiEpersonam)


            If Trim(Session("UTENTE")) = "" Then
                Response.Redirect("..\Login.aspx")
                Exit Sub
            End If


            If Not IsNothing(Session("ABILITAZIONI")) Then
                If Session("ABILITAZIONI").IndexOf("<EPERSONAM-AUT>") >= 0 Then

                    Session("CODICEFISCALE") = Request.Form("CODICEFISCALE")
                    Session("NUCLEO") = Request.Form("NUCLEO")
                    If campodb(Request.Form("CODICEFISCALE")) = "" Or IsNothing(Request.Form("CODICEFISCALE")) Then
                        Session("CODICEFISCALE") = Request.Item("CODICEFISCALE")
                        Session("NUCLEO") = Request.Item("NUCLEO")
                    End If
                    Session("RICERCAOSPITI") = ""
                    If Request.Item("RICERCAOSPITI") = "SI" Or campodb(Session("CODICEFISCALE")) = "" Or IsNothing(Session("CODICEFISCALE")) Then
                        Session("RICERCAOSPITI") = "SI"
                    End If

                    Dim NomeFile As String

                    NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\db_" & Format(Now, "yyyyMMdd") & "_" & ".xml"
                    Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                    tw.Write(Session("CODICEFISCALE"))
                    tw.Close()

                    If Session("CODICEFISCALE") <> "" Then
                        Dim CercaPerCf As New ClsOspite

                        CercaPerCf.CODICEFISCALE = Session("CODICEFISCALE")
                        CercaPerCf.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CercaPerCf.CODICEFISCALE)

                        If CercaPerCf.CodiceOspite > 0 Then
                            Dim Mov As New Cls_Movimenti

                            Mov.CodiceOspite = CercaPerCf.CodiceOspite
                            Mov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                            Session("CODICESERVIZIO") = Mov.CENTROSERVIZIO
                            Session("CODICEOSPITE") = CercaPerCf.CodiceOspite
                        End If
                    End If
                    Response.Redirect("Anagrafica.aspx")
                    'Timer1.Enabled = True

                    'Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

                    't.Start(HttpContext.Current)


                    'Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
                    'Lbl_Waiting.Text = Lbl_Waiting.Text & "Dialogo In Corso... <br /><br /><br />"
                    'Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
                    'Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
                Else
                    If Session("RICERCAOSPITI") = "SI" Then
                        Response.Redirect("RicercaAnagrafica.aspx")
                    Else
                        Response.Redirect("Anagrafica.aspx")
                    End If
                End If
            Else
                If Session("RICERCAOSPITI") = "SI" Then
                    Response.Redirect("RicercaAnagrafica.aspx")
                Else
                    Response.Redirect("Anagrafica.aspx")
                End If
            End If
        Else

            Session("CODICEFISCALE") = Request.Form("CODICEFISCALE")
            Session("NUCLEO") = Request.Form("NUCLEO")
            If campodb(Request.Form("CODICEFISCALE")) = "" Or IsNothing(Request.Form("CODICEFISCALE")) Then
                Session("CODICEFISCALE") = Request.Item("CODICEFISCALE")
                Session("NUCLEO") = Request.Item("NUCLEO")
            End If
            Session("RICERCAOSPITI") = ""
            If Request.Item("RICERCAOSPITI") = "SI" Or campodb(Session("CODICEFISCALE")) = "" Or IsNothing(Session("CODICEFISCALE")) Then
                Session("RICERCAOSPITI") = "SI"
            End If

            Timer1.Enabled = True

            Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

            't.Start(HttpContext.Current)
            Call DoWork(HttpContext.Current)

            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Dialogo In Corso... <br /><br /><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"

        End If
    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Sub DoWork(ByVal data As Object)

        AllineaDB(data)
    End Sub

    Private Sub AllineaDB(ByRef data As Object)


        Dim Token As String = ""
        Dim SessionInt As System.Web.HttpContext = data
        Dim AppoggioLOG As String = ""

        Try
            Token = LoginPersonam(data)
        Catch ex As Exception

        End Try

        If Token = "" Then Exit Sub

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        If IsDate(Param.DataCheckEpersonam) And Year(Param.DataCheckEpersonam) > 2000 Then
            DataFatt = Format(Param.DataCheckEpersonam, "yyyy-MM-dd")
        End If



        REM DataFatt = "2015-01-01"
        Dim Indice As Integer = 0
        Dim NumeroRighe As Integer = 0
        Dim Pagina = 1

        Dim UrlConnessione As String = ""




        'Dim VettoreBu(100) As Integer
        'If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Sicurezza".ToUpper) > 0 Or Session("DC_OSPITE").ToString.ToUpper.IndexOf("ODA".ToUpper) > 0 Then
        '    ListaBussineUnit(Token, Context, VettoreBu)
        'Else
        '    VettoreBu(0) = BusinessUnit(Token, Session)
        'End If


        'Do While VettoreBu(Indice) > 0
        '    Session("APPOGGIO") = ""
        '    Session("APPOGGIOPARENTE") = ""
        '    Session("LETTOMOVIMENTI") = ""

        '    Pagina = 0
        '    AppoggioLOG = AppoggioLOG & " " & VettoreBu(Indice)
        '    Do
        '        NumeroRighe = 0
        '        If Pagina = 0 Then
        '            ' UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/from/" & DataFatt
        '            UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & VettoreBu(Indice) & "/guests/updated/" & DataFatt & "?per_page=500"
        '        Else
        '            'UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/from/" & DataFatt & "?page=" & Indice
        '            UrlConnessione = "https://api-v0.e-personam.com/v0/business_units/" & VettoreBu(Indice) & "/guests/updated/" & DataFatt & "?per_page=500&page=" & Pagina

        '        End If
        '        Pagina = Pagina + 1

        '        Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

        '        client.Method = "GET"
        '        client.Headers.Add("Authorization", "Bearer " & Token)
        '        client.ContentType = "Content-Type: application/json"

        '        Dim reader As StreamReader
        '        Dim response As HttpWebResponse = Nothing
        '        Dim rawresp As String = "[]"
        '        Try

        '            response = DirectCast(client.GetResponse(), HttpWebResponse)

        '            reader = New StreamReader(response.GetResponseStream())



        '            rawresp = reader.ReadToEnd()
        '        Catch ex As Exception

        '        End Try


        '        Dim jResults As JArray = JArray.Parse(rawresp)


        '        For Each jTok As JToken In jResults
        '            Dim M As New ClsOspite
        '            Dim TId As Integer
        '            M.CodiceOspite = 0
        '            M.CODICEFISCALE = jTok.Item("cf").ToString()

        '            Try
        '                TId = jTok.Item("id").ToString()
        '            Catch ex As Exception

        '            End Try

        '            NumeroRighe = NumeroRighe + 1

        '            M.LeggiPerCodiceFiscale(Session("DC_OSPITE"), M.CODICEFISCALE)


        '            If M.CODICEFISCALE = "FLLCSN32P10E253M" Then
        '                M.CODICEFISCALE = "FLLCSN32P10E253M"
        '            End If
        '            If M.CodiceOspite = 0 Then
        '                M.IdEpersonam = TId
        '                M.LeggiPerIdEpersonam(Session("DC_OSPITE"))
        '            End If
        '            If M.CodiceOspite > 0 Then
        '                Dim AppoCognome As String
        '                Dim AppoNome As String
        '                Dim AppoDatascinta As Date
        '                Dim AppoCf As String
        '                Dim AppoSesso As String
        '                Dim AppoProvinciaDiNascita As String
        '                Dim AppoComuneDiNascita As String
        '                Dim AppoRESIDENZATELEFONO1 As String
        '                Dim AppoRESIDENZAINDIRIZZO1 As String
        '                Dim AppoRESIDENZACAP1 As String
        '                Dim AppoRESIDENZAPROVINCIA1 As String
        '                Dim AppoRESIDENZACOMUNE1 As String

        '                Dim AppoRESIDENZATELEFONO2 As String
        '                Dim AppoRESIDENZATELEFONO3 As String
        '                Dim AppoTELEFONO1 As String
        '                Dim AppoAssistenteSociale As String
        '                Dim AppoTesseraSanitaria As String

        '                AppoCognome = M.CognomeOspite
        '                AppoNome = M.NomeOspite
        '                AppoDatascinta = M.DataNascita
        '                AppoCf = M.CODICEFISCALE
        '                AppoSesso = M.Sesso
        '                AppoProvinciaDiNascita = M.ProvinciaDiNascita
        '                AppoComuneDiNascita = M.ComuneDiNascita
        '                AppoRESIDENZATELEFONO1 = M.RESIDENZATELEFONO1
        '                AppoRESIDENZAINDIRIZZO1 = M.RESIDENZAINDIRIZZO1
        '                AppoRESIDENZACAP1 = M.RESIDENZACAP1
        '                AppoRESIDENZAPROVINCIA1 = M.RESIDENZAPROVINCIA1
        '                AppoRESIDENZACOMUNE1 = M.RESIDENZACOMUNE1

        '                AppoRESIDENZATELEFONO2 = M.RESIDENZATELEFONO2
        '                AppoRESIDENZATELEFONO3 = M.RESIDENZATELEFONO3
        '                AppoTELEFONO1 = M.TELEFONO1
        '                AppoAssistenteSociale = M.AssistenteSociale
        '                AppoTesseraSanitaria = M.TesseraSanitaria

        '                M.CognomeOspite = jTok.Item("surname").ToString()
        '                M.NomeOspite = jTok.Item("firstname").ToString()
        '                M.Nome = M.CognomeOspite & " " & M.NomeOspite
        '                Try
        '                    M.DataNascita = campodb(jTok.Item("birthdate").ToString())
        '                Catch ex As Exception

        '                End Try

        '                If Year(M.DataNascita) < 1800 Then
        '                    M.DataNascita = Now
        '                End If

        '                M.CODICEFISCALE = jTok.Item("cf").ToString()

        '                Try
        '                    If Val(jTok.Item("gender").ToString()) = 1 Then
        '                        M.Sesso = "M"

        '                    Else
        '                        M.Sesso = "F"
        '                    End If
        '                Catch ex As Exception

        '                End Try

        '                Dim appoggio As String
        '                Try
        '                    appoggio = jTok.Item("birthcity").ToString()

        '                    M.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
        '                    M.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.TELEFONO1 = jTok.Item("fax").ToString()
        '                Catch ex As Exception

        '                End Try



        '                Try
        '                    M.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.RESIDENZACAP1 = jTok.Item("rescap").ToString()
        '                Catch ex As Exception

        '                End Try



        '                Try
        '                    M.AssistenteSociale = jTok.Item("ass_soc_fullname").ToString()
        '                Catch ex As Exception

        '                End Try

        '                Try
        '                    M.TesseraSanitaria = jTok.Item("numero_identificazione_tessera_sanitaria").ToString()
        '                Catch ex As Exception

        '                End Try


        '                Try
        '                    appoggio = jTok.Item("rescity").ToString()

        '                    M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
        '                    M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

        '                Catch ex As Exception

        '                End Try


        '                Try
        '                    M.IdEpersonam = jTok.Item("id").ToString()
        '                Catch ex As Exception

        '                End Try

        '                If Session("SOLOVERIFICA") = "SI" Then

        '                    If Trim(AppoCognome) <> Trim(M.CognomeOspite) Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "CognomeOspite")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCognome)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CognomeOspite)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If Trim(AppoNome) <> Trim(M.NomeOspite) Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "NomeOspite")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoNome)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.NomeOspite)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoDatascinta <> M.DataNascita Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "DataNascita")
        '                        If AppoDatascinta.Year = 1 Then
        '                            CmdI.Parameters.AddWithValue("@ValoreSenior", Now)
        '                        Else
        '                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoDatascinta)
        '                        End If
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.DataNascita)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoCf <> M.CODICEFISCALE Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "CODICEFISCALE")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCf)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.CODICEFISCALE)
        '                        CmdI.ExecuteNonQuery()
        '                    End If

        '                    If AppoSesso <> M.Sesso Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "SESSO")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoSesso)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.Sesso)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoProvinciaDiNascita <> M.ProvinciaDiNascita Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "ProvinciaDiNascita")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoProvinciaDiNascita)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ProvinciaDiNascita)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoComuneDiNascita <> M.ComuneDiNascita Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "ComuneDiNascita")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoComuneDiNascita)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.ComuneDiNascita)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoRESIDENZATELEFONO1 <> M.RESIDENZATELEFONO1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If

        '                    If AppoRESIDENZATELEFONO2 <> M.RESIDENZATELEFONO2 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO2")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO2)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO2)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoRESIDENZATELEFONO3 <> M.RESIDENZATELEFONO3 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO3")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZATELEFONO3)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZATELEFONO3)
        '                        CmdI.ExecuteNonQuery()
        '                    End If

        '                    If AppoTELEFONO1 <> M.TELEFONO1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZATELEFONO1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTELEFONO1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.TELEFONO1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If

        '                    If AppoRESIDENZAINDIRIZZO1 <> M.RESIDENZAINDIRIZZO1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAINDIRIZZO1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAINDIRIZZO1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAINDIRIZZO1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoRESIDENZACAP1 <> M.RESIDENZACAP1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACAP1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACAP1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACAP1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoRESIDENZAPROVINCIA1 <> M.RESIDENZAPROVINCIA1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "AppoRESIDENZAPROVINCIA1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAPROVINCIA1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZAPROVINCIA1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoRESIDENZACOMUNE1 <> M.RESIDENZACOMUNE1 Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACOMUNE1")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACOMUNE1)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.RESIDENZACOMUNE1)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoAssistenteSociale <> M.AssistenteSociale Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "AssistenteSociale")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoAssistenteSociale)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.AssistenteSociale)
        '                        CmdI.ExecuteNonQuery()
        '                    End If
        '                    If AppoTesseraSanitaria <> M.TesseraSanitaria Then
        '                        Dim CmdI As New OleDbCommand
        '                        CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
        '                                    " (?, ?, ?, ?,?)"
        '                        CmdI.Connection = cn
        '                        CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                        CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                        CmdI.Parameters.AddWithValue("@Campo", "TesseraSanitaria")
        '                        CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTesseraSanitaria)
        '                        CmdI.Parameters.AddWithValue("@ValoreEpersonam", M.TesseraSanitaria)
        '                        CmdI.ExecuteNonQuery()
        '                    End If

        '                Else
        '                    M.ScriviOspite(Session("DC_OSPITE"))


        '                    Dim CmdI As New OleDbCommand
        '                    CmdI.CommandText = "INSERT INTO  Log_AllineamentoAnagrafica (CognomeOspite ,NomeOspite ,Nome , DataNascita ,  CODICEFISCALE ,Sesso , ProvinciaDiNascita , ComuneDiNascita , RESIDENZATELEFONO1 , RESIDENZAINDIRIZZO1  ,RESIDENZACAP1 ,  RESIDENZAPROVINCIA1 , RESIDENZACOMUNE1, EMail, Cellulare, Fax ,CodiceOspite ,CodiceParente, Data) VALUES " & _
        '                                " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        '                    CmdI.Connection = cn
        '                    CmdI.Parameters.AddWithValue("@CognomeOspite", AppoCognome)
        '                    CmdI.Parameters.AddWithValue("@NomeOspite", AppoNome)
        '                    CmdI.Parameters.AddWithValue("@Nome", AppoCognome & " " & AppoNome)
        '                    CmdI.Parameters.AddWithValue("@DataNascita", AppoDatascinta)
        '                    CmdI.Parameters.AddWithValue("@CODICEFISCALE", AppoCf)
        '                    CmdI.Parameters.AddWithValue("@Sesso", AppoSesso)
        '                    CmdI.Parameters.AddWithValue("@ProvinciaDiNascita", AppoProvinciaDiNascita)
        '                    CmdI.Parameters.AddWithValue("@ComuneDiNascita", AppoComuneDiNascita)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO1", AppoRESIDENZATELEFONO1)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", AppoRESIDENZAINDIRIZZO1)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZACAP1", AppoRESIDENZACAP1)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", AppoRESIDENZAPROVINCIA1)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZACOMUNE1", AppoRESIDENZACOMUNE1)

        '                    CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO2", AppoRESIDENZATELEFONO2)
        '                    CmdI.Parameters.AddWithValue("@RESIDENZATELEFONO3", AppoRESIDENZATELEFONO3)
        '                    CmdI.Parameters.AddWithValue("@TELEFONO1", AppoTELEFONO1)

        '                    CmdI.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                    CmdI.Parameters.AddWithValue("@CodiceParente", 0)
        '                    CmdI.Parameters.AddWithValue("@Data", Now)
        '                    Try
        '                        CmdI.ExecuteNonQuery()
        '                    Catch ex As Exception

        '                    End Try

        '                End If



        '                VerificaParente(M.CODICEFISCALE, M.CodiceOspite, Token, Session("SOLOVERIFICA"))
        '            Else
        '                If Session("SOLOVERIFICA") <> "SI" Then
        '                    Dim CodiceFiscale As String = M.CODICEFISCALE
        '                    Dim Letto As String

        '                    Dim CentroServizio As String = ""
        '                    Dim DataAcco As Date = Nothing
        '                    Dim DataAppoggio As String = Nothing


        '                    VisuallizzaAccoglimentoOspite(Token, Session("DC_OSPITE"), True, M.CODICEFISCALE, CentroServizio, DataAppoggio, VettoreBu(Indice))

        '                    If Len(DataAppoggio) > 10 Then
        '                        DataAcco = Mid(DataAppoggio, 1, 10)
        '                    Else
        '                        If Not IsNothing(DataAppoggio) Then
        '                            If DataAppoggio = "" Then
        '                                DataAcco = Now
        '                            Else
        '                                DataAcco = DataAppoggio
        '                            End If
        '                        End If
        '                    End If

        '                    If Format(DataAcco, "yyyyMMdd") > Format(Now, "yyyyMMdd") Or Param.NonInAtteasa = 0 Then

        '                        Dim Cs As New Cls_CentroServizio

        '                        Cs.CENTROSERVIZIO = CentroServizio

        '                        Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


        '                        If Cs.DESCRIZIONE <> "" Then

        '                            Dim Anag As New ClsOspite

        '                            M.CognomeOspite = jTok.Item("surname").ToString()
        '                            M.NomeOspite = jTok.Item("firstname").ToString()
        '                            M.Nome = M.CognomeOspite & " " & M.NomeOspite

        '                            Try
        '                                M.DataNascita = campodb(jTok.Item("birthdate").ToString())
        '                            Catch ex As Exception
        '                                M.DataNascita = Now
        '                            End Try
        '                            If Year(M.DataNascita) < 1900 Then
        '                                M.DataNascita = Now
        '                            End If


        '                            M.InserisciOspite(Session("DC_OSPITE"), M.CognomeOspite, M.NomeOspite, M.DataNascita)

        '                            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)

        '                            M.CODICEFISCALE = jTok.Item("cf").ToString()

        '                            Try
        '                                If Val(jTok.Item("gender").ToString()) = 1 Then
        '                                    M.Sesso = "M"

        '                                Else
        '                                    M.Sesso = "F"
        '                                End If
        '                            Catch ex As Exception

        '                            End Try

        '                            Dim appoggio As String
        '                            Try
        '                                appoggio = jTok.Item("birthcity").ToString()

        '                                M.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
        '                                M.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.TELEFONO1 = jTok.Item("fax").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.RESIDENZAINDIRIZZO1 = jTok.Item("resaddress").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.RESIDENZACAP1 = jTok.Item("rescap").ToString()
        '                            Catch ex As Exception

        '                            End Try


        '                            Try
        '                                M.AssistenteSociale = jTok.Item("ass_soc_fullname").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                M.TesseraSanitaria = jTok.Item("numero_identificazione_tessera_sanitaria").ToString()
        '                            Catch ex As Exception

        '                            End Try





        '                            Try
        '                                appoggio = jTok.Item("rescity").ToString()

        '                                M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
        '                                M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

        '                            Catch ex As Exception

        '                            End Try

        '                            Try
        '                                Letto = jTok.Item("bed").ToString()
        '                            Catch ex As Exception

        '                            End Try

        '                            M.ScriviOspite(Session("DC_OSPITE"))






        '                            Dim Pc As New Cls_Pianodeiconti
        '                            Pc.Mastro = Cs.MASTRO
        '                            Pc.Conto = Cs.CONTO
        '                            Pc.Sottoconto = M.CodiceOspite * 100
        '                            Pc.Decodfica(Session("DC_GENERALE"))
        '                            Pc.Mastro = Cs.MASTRO
        '                            Pc.Conto = Cs.CONTO
        '                            Pc.Sottoconto = M.CodiceOspite * 100
        '                            Pc.Descrizione = M.Nome
        '                            Pc.Tipo = "A"
        '                            Pc.TipoAnagrafica = "O"
        '                            Pc.Scrivi(Session("DC_GENERALE"))




        '                            Dim Trovato As Boolean = False



        '                            Dim TipoMovimentoSenior As String = ""
        '                            Dim Progressivo As Long
        '                            Dim MySql As String


        '                            TipoMovimentoSenior = "05"

        '                            Dim xtr As OleDbTransaction = cn.BeginTransaction()


        '                            Dim MovLetto As New Cls_MovimentiStanze


        '                            If Trim(Letto) <> "" Then
        '                                MovLetto.CentroServizio = Cs.CENTROSERVIZIO
        '                                MovLetto.CodiceOspite = M.CodiceOspite
        '                                MovLetto.Data = DataAcco
        '                                MovLetto.Tipologia = "OC"
        '                                MovLetto.Villa = "01"
        '                                If Mid(Letto, 1, 1) = "1" Then
        '                                    MovLetto.Reparto = "01"
        '                                End If
        '                                If Mid(Letto, 1, 1) = "2" Then
        '                                    MovLetto.Reparto = "02"
        '                                End If
        '                                If Mid(Letto, 1, 1) = "3" Then
        '                                    MovLetto.Reparto = "03"
        '                                End If
        '                                If Mid(Letto, 1, 1) = "4" Then
        '                                    MovLetto.Reparto = "04"
        '                                End If
        '                                MovLetto.Piano = ""
        '                                MovLetto.Stanza = ""
        '                                MovLetto.Letto = Letto
        '                                MovLetto.AggiornaDB(Session("DC_OSPITIACCESSORI"))
        '                            End If


        '                            Dim cmdIns As New OleDbCommand()
        '                            cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
        '                            cmdIns.Connection = cn
        '                            cmdIns.Transaction = xtr
        '                            cmdIns.Parameters.AddWithValue("@IdEpersonam", 1)
        '                            cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        '                            cmdIns.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
        '                            cmdIns.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                            cmdIns.Parameters.AddWithValue("@Data", Format(DataAcco, "dd/MM/yyyy"))
        '                            cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
        '                            cmdIns.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
        '                            cmdIns.Parameters.AddWithValue("@DataModifica", Now)
        '                            cmdIns.ExecuteNonQuery()

        '                            Dim cmd1 As New OleDbCommand()
        '                            cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & Cs.CENTROSERVIZIO & "' And  CodiceOspite = " & M.CodiceOspite)
        '                            cmd1.Connection = cn
        '                            cmd1.Transaction = xtr
        '                            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        '                            If myPOSTreader1.Read Then
        '                                If Not IsDBNull(myPOSTreader1.Item(0)) Then
        '                                    Progressivo = myPOSTreader1.Item(0) + 1
        '                                Else
        '                                    Progressivo = 1
        '                                End If
        '                            Else
        '                                Progressivo = 1
        '                            End If

        '                            Dim MyData As Date = DataAcco

        '                            MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
        '                            Dim cmdw As New OleDbCommand()
        '                            cmdw.CommandText = (MySql)
        '                            cmdw.Transaction = xtr
        '                            cmdw.Parameters.AddWithValue("@Utente", Session("UTENTE"))
        '                            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
        '                            cmdw.Parameters.AddWithValue("@CentroServizio", Cs.CENTROSERVIZIO)
        '                            cmdw.Parameters.AddWithValue("@CodiceOspite", M.CodiceOspite)
        '                            cmdw.Parameters.AddWithValue("@Data", DateSerial(Year(MyData), Month(MyData), Day(MyData)))
        '                            cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
        '                            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
        '                            cmdw.Parameters.AddWithValue("@CAUSALE", "")
        '                            cmdw.Parameters.AddWithValue("@Descrizione", "Accoglimento EPersonam")
        '                            cmdw.Parameters.AddWithValue("@EPersonam", 1)
        '                            cmdw.Connection = cn
        '                            cmdw.ExecuteNonQuery()

        '                            xtr.Commit()
        '                        End If
        '                    End If
        '                End If
        '            End If
        '        Next
        '    Loop While NumeroRighe > 0
        '    Indice = Indice + 1
        'Loop
        'Dim CmdI18 As New OleDbCommand
        'CmdI18.CommandText = "UPDATE TabellaParametri SET  DataCheckEpersonam = ?"
        'CmdI18.Connection = cn
        'CmdI18.Parameters.AddWithValue("@DataCheckEpersonam", Now)
        'CmdI18.ExecuteNonQuery()


        cn.Close()
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 101



    End Sub




    Private Sub VerificaParente(ByVal CodiceFiscale As String, ByVal CodiceOspite As Long, ByVal Token As String, ByVal SOLOVERIFICA As String)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        If CodiceFiscale = "" Then
            Exit Sub
        End If


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing
        Dim rawresp As String = ""


        Try
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & CodiceFiscale & "/relatives")

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()
        Catch ex As Exception

            Dim CmdI As New OleDbCommand
            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                        " (?, ?, ?, ?,?)"
            CmdI.Connection = cn
            CmdI.Parameters.AddWithValue("@CodiceOspite", 0)
            CmdI.Parameters.AddWithValue("@CodiceParente", 0)
            CmdI.Parameters.AddWithValue("@Campo", "ERRORE")
            CmdI.Parameters.AddWithValue("@ValoreSenior", "Errore " & "https://api-v0.e-personam.com/v0/guests/" & CodiceFiscale & "/relatives")
            CmdI.Parameters.AddWithValue("@ValoreEpersonam", ex.Message)
            CmdI.ExecuteNonQuery()
        End Try


        Dim jResults As JArray

        Try
            jResults = JArray.Parse(rawresp)
        Catch ex As Exception

        End Try


        Dim Appoggio As String
        Dim Modifica As Boolean

        If IsDBNull(rawresp) Or rawresp = "" Then
            Exit Sub
        End If


        If IsDBNull(jResults) Then
            Exit Sub
        End If

        For Each jTok2 As JToken In jResults

            'If jTok2.Item("signed_to_pay") = "true" Then
            Dim Prova As New Cls_Parenti
            If CodiceOspite = 1687 Then
                CodiceOspite = 1687
            End If

            Prova.CodiceOspite = CodiceOspite
            Try
                Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
            Catch ex As Exception

            End Try

            Try
                Prova.IdEpersonam = jTok2.Item("id").ToString()
            Catch ex As Exception

            End Try


            Dim kv6 As New Cls_CodiceFiscale

            If Prova.CODICEFISCALE <> "" Then
                If kv6.Check_CodiceFiscale(Prova.CODICEFISCALE) = True Then

                    Modifica = True
                    Prova.Nome = ""
                    Prova.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Prova.CODICEFISCALE)
                    If Prova.Nome = "" Then
                        Prova.LeggiPerIdEpersonam(Session("DC_OSPITE"), Prova.IdEpersonam)
                        If Prova.Nome = "" Then
                            Modifica = False
                        End If
                    End If

                    Dim AppoCognome As String
                    Dim AppoNome As String
                    Dim AppoDatascinta As Date
                    Dim AppoCf As String
                    Dim AppoSesso As String
                    Dim AppoProvinciaDiNascita As String
                    Dim AppoComuneDiNascita As String
                    Dim AppoTELEFONO1 As String
                    Dim AppoEmail As String
                    Dim AppoRESIDENZAINDIRIZZO1 As String
                    Dim AppoRESIDENZACAP1 As String
                    Dim AppoRESIDENZAPROVINCIA1 As String
                    Dim AppoRESIDENZACOMUNE1 As String
                    Dim AppoPARENTEINDIRIZZO As Integer


                    AppoCognome = Prova.CognomeParente
                    AppoNome = Prova.NomeParente
                    AppoDatascinta = Prova.DataNascita
                    AppoCf = Prova.CODICEFISCALE
                    AppoSesso = Prova.Sesso
                    AppoProvinciaDiNascita = Prova.ProvinciaDiNascita
                    AppoComuneDiNascita = Prova.ComuneDiNascita
                    AppoTELEFONO1 = Prova.Telefono1
                    AppoEmail = Prova.Telefono3
                    AppoRESIDENZAINDIRIZZO1 = Prova.RESIDENZAINDIRIZZO1
                    AppoRESIDENZACAP1 = Prova.RESIDENZACAP1
                    AppoRESIDENZAPROVINCIA1 = Prova.RESIDENZAPROVINCIA1
                    AppoRESIDENZACOMUNE1 = Prova.RESIDENZACOMUNE1
                    AppoPARENTEINDIRIZZO = Prova.ParenteIndirizzo




                    Prova.Nome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString
                    Prova.CognomeParente = jTok2.Item("surname").ToString
                    Prova.NomeParente = jTok2.Item("firstname").ToString

                    If Prova.CognomeParente = "DR SERNI" Then
                        Prova.CognomeParente = "DR SERNI"
                    End If
                    Try
                        Prova.Telefono1 = jTok2.Item("phone").ToString
                    Catch ex As Exception

                    End Try
                    Try
                        Prova.Telefono3 = jTok2.Item("email").ToString
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZACAP1 = jTok2.Item("rescap").ToString
                    Catch ex As Exception

                    End Try


                    Try
                        Appoggio = jTok2.Item("rescity_alpha6").ToString()

                        Prova.RESIDENZAPROVINCIA1 = Mid(Appoggio & Space(6), 1, 3)
                        Prova.RESIDENZACOMUNE1 = Mid(Appoggio & Space(6), 4, 3)
                    Catch ex As Exception

                    End Try


                    Try
                        If jTok2.Item("signed_to_receive_bill") = "true" Or jTok2.Item("signed_to_receive_bill") = "True" Then
                            Prova.ParenteIndirizzo = 1
                        Else
                            Prova.ParenteIndirizzo = 0
                        End If
                    Catch ex As Exception
                        Prova.ParenteIndirizzo = 0
                    End Try

                    Try
                        Prova.CODICEFISCALE = jTok2.Item("cf").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.RESIDENZAINDIRIZZO1 = jTok2.Item("resaddress").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Prova.IdEpersonam = jTok2.Item("id").ToString()
                    Catch ex As Exception

                    End Try

                    If SOLOVERIFICA = "SI" Then
                        If AppoCognome <> Prova.CognomeParente Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "CognomeParente")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCognome)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.CognomeParente)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoNome <> Prova.NomeParente Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "NomeParente")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoNome)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.NomeParente)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoCf <> Prova.CODICEFISCALE Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "CODICEFISCALE")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoCf)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.CODICEFISCALE)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZAINDIRIZZO1 <> Prova.RESIDENZAINDIRIZZO1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAINDIRIZZO1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAINDIRIZZO1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZAINDIRIZZO1)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZAPROVINCIA1 <> Prova.RESIDENZAPROVINCIA1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZAPROVINCIA1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZAPROVINCIA1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZAPROVINCIA1)
                            CmdI.ExecuteNonQuery()
                        End If
                        If AppoRESIDENZACOMUNE1 <> Prova.RESIDENZACOMUNE1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "RESIDENZACOMUNE1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoRESIDENZACOMUNE1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.RESIDENZACOMUNE1)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoTELEFONO1 <> Prova.Telefono1 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "Telefono1")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoTELEFONO1)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.Telefono1)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoEmail <> Prova.Telefono3 Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "Email")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoEmail)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.Telefono3)
                            CmdI.ExecuteNonQuery()
                        End If

                        If AppoPARENTEINDIRIZZO <> Prova.ParenteIndirizzo Then
                            Dim CmdI As New OleDbCommand
                            CmdI.CommandText = "INSERT INTO  Log_VariazioneDati (CodiceOspite ,CodiceParente, Campo, ValoreSenior,ValoreEpersonam) VALUES " & _
                                        " (?, ?, ?, ?,?)"
                            CmdI.Connection = cn
                            CmdI.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                            CmdI.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                            CmdI.Parameters.AddWithValue("@Campo", "ParenteIndirizzo")
                            CmdI.Parameters.AddWithValue("@ValoreSenior", AppoPARENTEINDIRIZZO)
                            CmdI.Parameters.AddWithValue("@ValoreEpersonam", Prova.ParenteIndirizzo)
                            CmdI.ExecuteNonQuery()
                        End If


                    Else
                        Dim CmdZ As New OleDbCommand
                        CmdZ.CommandText = "INSERT INTO  Log_AllineamentoAnagrafica (CognomeOspite ,NomeOspite ,Nome , CODICEFISCALE , RESIDENZAINDIRIZZO1  ,RESIDENZAPROVINCIA1 , RESIDENZACOMUNE1 ,CodiceOspite ,CodiceParente, Data) VALUES " & _
                                    " (? ,? ,?, ?, ? ,?, ? ,? ,?, ?)"
                        CmdZ.Connection = cn
                        CmdZ.Parameters.AddWithValue("@CognomeOspite", Prova.CognomeParente)
                        CmdZ.Parameters.AddWithValue("@NomeOspite", Prova.NomeParente)
                        CmdZ.Parameters.AddWithValue("@Nome", Prova.Nome)
                        CmdZ.Parameters.AddWithValue("@CODICEFISCALE", Prova.CODICEFISCALE)
                        CmdZ.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", Prova.RESIDENZAINDIRIZZO1)
                        CmdZ.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", Prova.RECAPITOPROVINCIA)
                        CmdZ.Parameters.AddWithValue("@RESIDENZACOMUNE1", Prova.RESIDENZACOMUNE1)
                        CmdZ.Parameters.AddWithValue("@CodiceOspite", Prova.CodiceOspite)
                        CmdZ.Parameters.AddWithValue("@CodiceParente", Prova.CodiceParente)
                        CmdZ.Parameters.AddWithValue("@Data", Now)
                        CmdZ.ExecuteNonQuery()


                        If Modifica = True Then

                            Prova.ScriviParente(Session("DC_OSPITE"))

                        Else

                            Prova.ScriviParente(Session("DC_OSPITE"))
                            Dim UltimoCs As New Cls_Movimenti

                            UltimoCs.UltimaData(Session("DC_OSPITE"), CodiceOspite)

                            Dim Cs As New Cls_CentroServizio

                            Cs.Leggi(Session("DC_OSPITE"), UltimoCs.CENTROSERVIZIO)

                            Dim Pc As New Cls_Pianodeiconti
                            Pc.Mastro = Cs.MASTRO
                            Pc.Conto = Cs.CONTO
                            Pc.Sottoconto = CodiceOspite * 100 + Prova.CodiceParente
                            Pc.Decodfica(Session("DC_GENERALE"))

                            Pc.Mastro = Cs.MASTRO
                            Pc.Conto = Cs.CONTO
                            Pc.Sottoconto = CodiceOspite * 100 + Prova.CodiceParente
                            Pc.Descrizione = Prova.Nome
                            Pc.Tipo = "A"
                            Pc.TipoAnagrafica = "P"
                            Pc.Scrivi(Session("DC_GENERALE"))

                        End If
                        REM Session("APPOGGIOPARENTE") = Session("APPOGGIOPARENTE") & CodiceFiscale & ";" & Prova.CognomeParente & ";" & Prova.NomeParente & ";" & Prova.Telefono1 & ";" & Prova.RESIDENZAPROVINCIA1 & ";" & Prova.RESIDENZACOMUNE1 & ";" & Prova.CODICEFISCALE & ";" & Prova.RESIDENZAINDIRIZZO1 & vbNewLine



                        Dim MyBill As String = ""


                        Try
                            MyBill = jTok2.Item("signed_to_receive_bill")
                        Catch ex As Exception

                        End Try
                        If UCase(MyBill) = "TRUE" Then
                            Dim Anag As New ClsOspite
                            Anag.Leggi(Session("DC_OSPITE"), CodiceOspite)
                            Anag.RecapitoNome = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString

                            If CodiceOspite = 298 Then
                                CodiceOspite = 298
                            End If
                            Try
                                Appoggio = jTok2.Item("rescity_alpha6").ToString()
                                Anag.RecapitoProvincia = Mid(Appoggio & Space(6), 1, 3)
                                Anag.RecapitoComune = Mid(Appoggio & Space(6), 4, 3)
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.RecapitoIndirizzo = jTok2.Item("resaddress").ToString()
                            Catch ex As Exception

                            End Try


                            Try
                                Anag.RESIDENZACAP4 = jTok2.Item("rescap").ToString
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.RESIDENZATELEFONO4 = jTok2.Item("phone").ToString
                            Catch ex As Exception

                            End Try

                            Try
                                Anag.TELEFONO3 = jTok2.Item("email").ToString
                            Catch ex As Exception

                            End Try

                            Dim cmdOspite As New OleDbCommand

                            cmdOspite.CommandText = "UPDATE AnagraficaComune Set RecapitoNome=?,RecapitoProvincia=?,RecapitoComune=?,RecapitoIndirizzo=?,RESIDENZATELEFONO4=?,RESIDENZACAP4=?,TELEFONO4=? Where CodiceOspite = ? And Tipologia = 'O'"
                            cmdOspite.Connection = cn

                            If Anag.RecapitoNome.Length > 50 Then
                                Anag.RecapitoNome = Mid(Anag.RecapitoNome, 1, 50)
                            End If
                            If Anag.RecapitoProvincia.Length > 3 Then
                                Anag.RecapitoProvincia = Mid(Anag.RecapitoProvincia, 1, 3)
                            End If
                            If Anag.RecapitoComune.Length > 3 Then
                                Anag.RecapitoComune = Mid(Anag.RecapitoComune, 1, 3)
                            End If
                            If Anag.RecapitoIndirizzo.Length > 50 Then
                                Anag.RecapitoIndirizzo = Mid(Anag.RecapitoIndirizzo, 1, 50)
                            End If
                            If Anag.RESIDENZATELEFONO4.Length > 100 Then
                                Anag.RESIDENZATELEFONO4 = Mid(Anag.RESIDENZATELEFONO4, 1, 100)
                            End If
                            If Anag.TELEFONO3.Length > 100 Then
                                Anag.TELEFONO3 = Mid(Anag.TELEFONO3, 1, 100)
                            End If

                            cmdOspite.Parameters.AddWithValue("@RecapitoNome", Anag.RecapitoNome)
                            cmdOspite.Parameters.AddWithValue("@RecapitoProvincia", Anag.RecapitoProvincia)
                            cmdOspite.Parameters.AddWithValue("@RecapitoComune", Anag.RecapitoComune)
                            cmdOspite.Parameters.AddWithValue("@RecapitoIndirizzo", Anag.RecapitoIndirizzo)
                            cmdOspite.Parameters.AddWithValue("@RESIDENZATELEFONO4", Anag.RESIDENZATELEFONO4)
                            cmdOspite.Parameters.AddWithValue("@RESIDENZACAP4", Anag.RESIDENZACAP4)
                            cmdOspite.Parameters.AddWithValue("@TELEFONO4", Anag.TELEFONO3)
                            cmdOspite.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
                            cmdOspite.ExecuteNonQuery()
                        Else
                            If AppoPARENTEINDIRIZZO = 1 Then
                                Dim cmdOspite As New OleDbCommand

                                cmdOspite.CommandText = "UPDATE AnagraficaComune Set RecapitoNome=?,RecapitoProvincia=?,RecapitoComune=?,RecapitoIndirizzo=?,RESIDENZATELEFONO4=?,RESIDENZACAP4=?,TELEFONO4=?  Where CodiceOspite = ? And Tipologia ='O' "
                                cmdOspite.Connection = cn
                                cmdOspite.Parameters.AddWithValue("@RecapitoNome", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoProvincia", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoComune", "")
                                cmdOspite.Parameters.AddWithValue("@RecapitoIndirizzo", "")
                                cmdOspite.Parameters.AddWithValue("@RESIDENZATELEFONO4", "")
                                cmdOspite.Parameters.AddWithValue("@RESIDENZACAP4", "")
                                cmdOspite.Parameters.AddWithValue("@TELEFONO4", "")
                                cmdOspite.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
                                cmdOspite.ExecuteNonQuery()
                            End If

                        End If
                    End If
                End If
            End If
        Next

        cn.Close()


    End Sub

    Private Function LoginPersonam(ByVal context As Object) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim SessionInt As System.Web.HttpContext = context
        'Dim BlowFish As New Blowfish("advenias2014")




        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")

        Dim guarda As String
        If Trim(Session("EPersonamPSWCRYPT")) = "" Then

            Dim AppoggioUtente As String = Session("UTENTE")

            For i = 1 To 20
                AppoggioUtente = AppoggioUtente.Replace("<" & i & ">", "")
            Next i

            request.Add("username", AppoggioUtente)

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If


        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function



    Private Function ElencoSottoStrutture(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As Integer
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()
        Try
            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("structures").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                    ElencoSottoStrutture = Val(jTok2.Item("id").ToString)
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try

    End Function

    Private Sub ListaBussineUnit(ByVal Token As String, ByVal context As Object, ByRef VettoreBU() As Integer)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()


        Dim Indice As Integer

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("id").ToString) >= 0 Then
                VettoreBU(Indice) = Val(jTok2.Item("id").ToString)
                Indice = Indice + 1
            End If
        Next
    End Sub
    Private Function BusinessUnit(ByVal Token As String, ByVal context As Object) As Long

        Try

            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            Dim appoggio As String = context("NomeEPersonam")

            BusinessUnit = 0

            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("business_units").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context("NomeEPersonam"))) >= 0 Then
                    BusinessUnit = Val(jTok2.Item("id").ToString)
                    Exit For
                Else
                    If context("NomeEPersonam") = "PROVA" Then
                        BusinessUnit = 1825
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function




    Private Sub ForzaLogin(ByVal Utente As String)

        Dim ktest As New Cls_Login
        Dim k As New Cls_Login

        Session("TIPOAPP") = "RSA"

        ktest.Utente = Utente & "<1>"
        ktest.LeggiSP(Application("SENIOR"))
        Session("Password") = Request.Form("Txt_Enc_Password")
        If ktest.Ospiti <> "" Then
            Session("UTENTE") = Utente
            Session("Password") = ktest.ChiaveCr
            Response.Redirect("../Menu_Societa.aspx?CODICEFISCALE=" & Request.Item("CODICEFISCALE") & "&NUCLEO=" & Request.Item("NUCLEO"))
            Exit Sub
        End If


        k.Utente = Utente.ToLower
        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.LeggiSP(Application("SENIOR"))





        Session("SOCIETALI") = k.CaricaSocieta(Application("SENIOR"), k.Utente)
        Session("Password") = k.Chiave
        Session("ChiaveCr") = k.ChiaveCr
        Session("CLIENTE") = k.Cliente

        Session("SENIOR") = Application("SENIOR")
        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""
        Session("NomeEPersonam") = k.NomeEPersonam
        Session("GDPRAttivo") = k.GDPRAttivo

        k.LeggiEpersonamUser(Application("SENIOR"))

        Session("EPersonamUser") = k.EPersonamUser.Replace("w", "")
        Session("EPersonamPSW") = k.EPersonamPSW
        Session("EPersonamPSWCRYPT") = k.EPersonamPSWCRYPT


        If k.Ospiti <> "" Then
            Session("UTENTE") = k.Utente
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr
            Session("ABILITAZIONI") = k.ABILITAZIONI

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                Response.Redirect("MainPortineria.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                Exit Sub
            End If
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 100 Or DateDiff("s", Session("INIZIO"), Now) > 10 Then
            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))
            Param.DataCheckEpersonam = Now
            Param.ScriviParametri(Session("DC_OSPITE"))


            If Session("RICERCAOSPITI") = "SI" Then
                Response.Redirect("RicercaAnagrafica.aspx")
            Else

                Dim KS As New ClsOspite

                KS.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Session("CODICEFISCALE"))

                Session("CODICEOSPITE") = KS.CodiceOspite


                Dim Cserv As New Cls_CentroServizio
                Dim m As New Cls_Epersonam

                Cserv.EPersonam = m.RendiWardid(Session("NUCLEO"), Session("DC_OSPITE"))
                Cserv.LeggiEpersonam(Session("DC_OSPITE"), Session("NUCLEO"), True)

                Session("CODICESERVIZIO") = Cserv.CENTROSERVIZIO

                If Cserv.CENTROSERVIZIO = "" Then
                    Dim Cserv1 As New Cls_CentroServizio

                    Cserv1.EPersonamN = Session("NUCLEO")
                    Cserv1.LeggiEpersonam(Session("DC_OSPITE"), Session("NUCLEO"), False)

                    Session("CODICESERVIZIO") = Cserv1.CENTROSERVIZIO
                    If Session("CODICESERVIZIO") = "" Then
                        Dim cn As OleDbConnection

                        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                        cn.Open()

                        Dim cmd2 As New OleDbCommand()

                        cmd2.CommandText = "SELECT CENTROSERVIZIO From  MOVIMENTI WHERE CODICEOSPITE = ? ORDER BY DATA DESC"
                        cmd2.Connection = cn
                        cmd2.Parameters.AddWithValue("@CodiceOSpite", Session("CODICEOSPITE"))
                        Dim Reader2 As OleDbDataReader = cmd2.ExecuteReader()
                        If Reader2.Read Then
                            Session("CODICESERVIZIO") = campodb(Reader2.Item("CENTROSERVIZIO"))
                        End If
                        cn.Close()
                    End If
                End If

                Response.Redirect("Anagrafica.aspx")
            End If
        Else
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Dialogo In Corso... <br /><br /><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub

    Public Sub VisuallizzaAccoglimentoOspite(ByVal Token As String, ByVal ConnesioneOspiti As String, ByVal Chk_RicaricaDati As Boolean, ByVal codicefiscale As String, ByRef CServ As String, ByRef DataAccoglimento As String, Optional ByVal ParametroBusinessUnit As Integer = 0)

        Dim ConnectionString As String = ConnesioneOspiti
        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim eP As New Cls_Epersonam

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(ConnesioneOspiti)
        If Param.MeseFatturazione = 1 Then
            Param.MeseFatturazione = 12
            Param.AnnoFatturazione = Param.AnnoFatturazione - 1
        Else
            Param.MeseFatturazione = Param.MeseFatturazione - 1
        End If

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        Dim rawresp As String = ""
        Dim NonDati As Integer = 0
        Try
            If IsNothing(Session("LETTOMOVIMENTI")) Then
                NonDati = 1
            Else
                If Session("LETTOMOVIMENTI") = "" Then
                    NonDati = 1
                End If
            End If
        Catch ex As Exception
            NonDati = 1
        End Try

        If NonDati = 1 Then
            If ParametroBusinessUnit = 0 Then
                ParametroBusinessUnit = BusinessUnit(Token, Session)
            End If
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & ParametroBusinessUnit & "/guests/movements/from/" & DataFatt)




            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())



            rawresp = reader.ReadToEnd()

            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = 0 And Periodo = ? And Tipo = 'M'"
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            SalvaDb.Connection = cn

            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'M'"
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            Else
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo) values (?,?,?,?,?,'M') "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            End If
            VerificaDB.Close()

            Session("LETTOMOVIMENTI") = rawresp
        Else
            rawresp = Session("LETTOMOVIMENTI")
        End If



        Dim jResults As JArray = JArray.Parse(rawresp)
        Dim LastMov As String

        Dim Ospite As New ClsOspite

        Ospite.CODICEFISCALE = codicefiscale
        Ospite.LeggiPerCodiceFiscale(ConnesioneOspiti, Ospite.CODICEFISCALE)



        For Each jTok As JToken In jResults
            rawresp = rawresp & jTok.Item("guests_movements").ToString()
            For Each jTok1 As JToken In jTok.Item("guests_movements").Children

                Dim ward_id As Integer


                ward_id = jTok1.Item("ward_id").ToString()

                If codicefiscale = jTok1.Item("cf").ToString() Then
                    LastMov = ""
                    For Each jTok2 As JToken In jTok1.Item("movements").Children
                        Dim IdEpersonam As String
                        Dim Descrizione As String
                        IdEpersonam = jTok2.Item("id").ToString

                        Try
                            Descrizione = jTok2.Item("description").ToString
                        Catch ex As Exception

                        End Try

                        If (jTok2.Item("type").ToString = "8" And (Descrizione = "ENTER" Or Descrizione = "")) Or jTok2.Item("type").ToString <> "8" Then

                            If LastMov = "11" And jTok2.Item("type").ToString = "8" Then

                            Else

                                LastMov = jTok2.Item("type").ToString

                                Dim cmd As New OleDbCommand()

                                If jTok2.Item("type").ToString = "11" Then
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  CodiceOspite = " & Ospite.CodiceOspite & " And Data = ?")
                                    cmd.Parameters.AddWithValue("@Data", Mid(jTok2.Item("date").ToString, 1, 10))

                                Else
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  IdEpersonam = " & IdEpersonam)
                                End If

                                cmd.Connection = cn

                                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                If Not myPOSTreader.Read Then
                                    Dim Tipo As String = ""
                                    Dim DataMov As String = ""

                                    If jTok2.Item("type").ToString = "11" Then
                                        Tipo = "05"
                                    End If
                                    If jTok2.Item("type").ToString = "9" Then
                                        Tipo = "03"
                                    End If
                                    If jTok2.Item("type").ToString = "8" Then
                                        Tipo = "04"
                                    End If
                                    If jTok2.Item("type").ToString = "12" Then
                                        Tipo = "13"
                                    End If

                                    If Tipo = "05" Then
                                        DataMov = jTok2.Item("date").ToString

                                        ward_id = eP.RendiWardid(ward_id, ConnesioneOspiti)

                                        Dim CS As New Cls_CentroServizio
                                        CS.DESCRIZIONE = ""



                                        CS.EPersonam = ward_id
                                        CS.LeggiEpersonam(ConnesioneOspiti, CS.EPersonam, jTok2.Item("conv").ToString)

                                        DataAccoglimento = jTok2.Item("date").ToString
                                        If CS.CENTROSERVIZIO = "" Then
                                            DataAccoglimento = DataAccoglimento & " " & ward_id
                                        End If
                                        CServ = CS.CENTROSERVIZIO
                                        If CServ <> "" Then
                                            myPOSTreader.Close()
                                            Trovato = True
                                        End If
                                    End If

                                    Dim DataVerifica As Date

                                    Try
                                        DataVerifica = jTok2.Item("date").ToString
                                    Catch ex As Exception

                                    End Try

                                    If Tipo = "13" And Format(DataVerifica, "yyyyMMdd") < Format(DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, 1), "yyyyMMdd") Then
                                        Trovato = False
                                        CServ = ""
                                        DataAccoglimento = ""
                                    End If

                                End If
                                myPOSTreader.Close()
                            End If
                        End If
                    Next
                End If
                If Trovato = True Then
                    Exit For
                End If
            Next
            If Trovato = True Then
                Exit For
            End If
        Next

        cn.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



End Class

