﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MedService" CodeFile="MedService.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Senior WEB - CAMBIO SERVER</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">IL SOFTWARE SENIOR HA CAMBIATO SERVER ADESSO LO TROVATE ALL'INDIRIZZO :</div>
                        <div class="SottoTitolo">
                            <a href="https://senior.medservices.cloud:3450/Seniorweb/Login.aspx">https://senior.medservices.cloud:3450/Seniorweb/Login.aspx</a>
                            <br />
                            <br />
                            <b>Nelle stampe e nelle esportazione il vostro browser bloccherà l'apertura dei popup,dovete autorizzare il sito, così come stato fatto per il server sodo.it lo dovete fare anche per il nuovo server</b>
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <a href="Login.aspx">
                            <img src="images/Menu_Indietro.png" class="Effetto" />
                        </a>
                        <br />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table>
                            <tr>

                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                                <td style="text-align: center; width: 112px;">&nbsp;</td>
                                <td style="text-align: center; width: 112px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; vertical-align: top;" colspan="5">
                                    <asp:Label ID="Lbl_Errore" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>


                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>



                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
