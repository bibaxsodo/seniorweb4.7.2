﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Login_generale" CodeFile="Login_generale.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/structure.css" />
    <script src="js/JSErrore.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" class="box login" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <fieldset class="boxBody">
            <label>Nome Utente</label>
            <asp:TextBox ID="Txt_Login" runat="server" TabIndex="1" required></asp:TextBox>
            <label><a href="mailto:info@sodo.it" class="rLink" tabindex="5">Password dimenticata</a>Password</label>
            <asp:TextBox ID="Txt_Password" TextMode="Password" runat="server" TabIndex="2" required></asp:TextBox>
        </fieldset>
        <footer>
            <center>
	  <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/avanti.jpg"></asp:ImageButton>	  	  
	</center>
        </footer>
    </form>
    <footer id="main">
        Login ....... | <a href="http://www.sodo.it">Sodo Informatica</a>
    </footer>
</body>
</html>
