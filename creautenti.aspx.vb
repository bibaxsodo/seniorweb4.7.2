﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports BCrypt.Net

Partial Class creautenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim D As Integer

            MyTable = Session("Appoggio")

            D = Val(e.CommandArgument)




            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

            cn.Open()
            Dim Command1 As New OleDbCommand("SELECT * FROM UTENTE Where UTENTE = ?", cn)



            Command1.Parameters.AddWithValue("@UTENTE", MyTable.Rows(D).Item(0).ToString)


            Dim myreader1 As OleDbDataReader = Command1.ExecuteReader()
            If myreader1.Read() Then
                Txt_Utente.Text = campodb(myreader1.Item("UTENTE"))
                Txt_Password.Text = campodb(myreader1.Item("CHIAVE"))
            End If
            myreader1.Close()
            cn.Close()


            Dim k As New Cls_Login



            Session("USER_SODO") = False


            Session("TIPOAPP") = "RSA"

            k.Utente = UCase(Txt_Utente.Text)
            k.Chiave = Txt_Password.Text

            k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
            k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
            k.Leggi(Application("SENIOR"))
            Session("Password") = Txt_Password.Text
            Session("ChiaveCr") = k.ChiaveCr


            If Session("UTENTEIMPER") = "" Then
                Session("SOCIETALI") = k.CaricaSocieta(Application("SENIOR"), k.Utente)
            End If

            Session("DC_OSPITE") = k.Ospiti
            Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
            Session("DC_TABELLE") = k.TABELLE
            Session("DC_GENERALE") = k.Generale
            Session("DC_TURNI") = k.Turni
            Session("STAMPEOSPITI") = k.STAMPEOSPITI
            Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
            Session("ProgressBar") = ""
            Session("CODICEREGISTRO") = ""
            Session("NomeEPersonam") = k.NomeEPersonam
            Session("CLIENTE") = k.Cliente
            Session("SENIOR") = Application("SENIOR")


            k.LeggiEpersonamUser(Application("SENIOR"))


            If k.EPersonamUser.IndexOf("w") > 0 Then
                k.EPersonamUser = k.EPersonamUser.Replace("w", "")
            End If

            Session("EPersonamUser") = k.EPersonamUser
            Session("EPersonamPSW") = k.EPersonamPSW
            Session("EPersonamPSWCRYPT") = k.EPersonamPSWCRYPT
            Session("GDPRAttivo") = k.GDPRAttivo

            If k.Ospiti <> "" Then
                Session("ABILITAZIONI") = k.ABILITAZIONI
                Session("UTENTE") = Txt_Utente.Text.ToLower


                If Session("ABILITAZIONI").IndexOf("<PROTOCOLLO>") >= 0 Then
                    Response.Redirect("Protocollo/Menu_Protocollo.aspx")
                    Exit Sub
                End If

                If Session("ABILITAZIONI").IndexOf("<STATISTICHE>") >= 0 Then
                    Response.Redirect("OspitiWeb\Menu_Visualizzazioni.aspx")
                    Exit Sub
                End If

                If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                    Response.Redirect("MainPortineria.aspx")
                    Exit Sub
                End If
                If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                    Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                    Exit Sub
                End If
                If Session("ABILITAZIONI").IndexOf("<RICHIESTE>") > 0 Then
                    Response.Redirect("TurniWeb\Menu_Richieste.aspx")
                    Exit Sub
                End If
                Response.Redirect("MainMenu.aspx")
                Exit Sub
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('<center><b>Nome utente o password errati</b></center>');", True)
            End If
            Exit Sub
        End If
        If e.CommandName = "Select" Then
            Dim d As Integer

            MyTable = Session("Appoggio")

            d = Val(e.CommandArgument)

            'If MyTable.Rows(d).Item(0).ToString = Session("UTENTE") Then
            '    Lbl_Errori.Text = "Non posso modificare l'utente attivo"
            '    Exit Sub
            'End If

            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

            cn.Open()
            Dim Command1 As New OleDbCommand("SELECT * FROM UTENTE Where UTENTE = ?", cn)



            Command1.Parameters.AddWithValue("@UTENTE", MyTable.Rows(d).Item(0).ToString)


            Dim myreader1 As OleDbDataReader = Command1.ExecuteReader()
            If myreader1.Read() Then
                Txt_Utente.Text = campodb(myreader1.Item("UTENTE"))
                Txt_Password.Text = campodb(myreader1.Item("CHIAVE"))
                Txt_ChiaveCR.Text = campodb(myreader1.Item("CHIAVECR"))
                Txt_ConfermaPassword.Text = campodb(myreader1.Item("CHIAVE"))
                Txt_Email.Text = campodb(myreader1.Item("EMAIL"))

                Txt_Report.Text = campodb(myreader1.Item("PERSONALIZZAZIONI")).Replace(" ", vbNewLine)
                Dim Abilitazioni As String

                Abilitazioni = campodb(myreader1.Item("ABILITAZIONI"))
                Chk_GestioneOspiti.Checked = False

                If Abilitazioni.IndexOf("<GO>") >= 0 Then
                    Chk_GestioneOspiti.Checked = True
                End If
                If Abilitazioni.IndexOf("<ANAGRAFICA>") >= 0 Then
                    Chk_Anagrafica.Checked = True
                End If
                If Abilitazioni.IndexOf("<CALCOLO>") >= 0 Then
                    Chk_Calcolo.Checked = True
                End If
                If Abilitazioni.IndexOf("<EMISSIONE>") >= 0 Then
                    Chk_Emissione.Checked = True
                End If
                If Abilitazioni.IndexOf("<ADDEBITI>") >= 0 Then
                    Chk_AddebitiAccrediti.Checked = True
                End If
                If Abilitazioni.IndexOf("<INCASSO>") >= 0 Then
                    Chk_Incasso.Checked = True
                End If
                If Abilitazioni.IndexOf("<TABELLA>") >= 0 Then
                    Chk_Tabelle.Checked = True
                End If
                If Abilitazioni.IndexOf("<SERVIZI>") >= 0 Then
                    Chk_Servizi.Checked = True
                End If
                If Abilitazioni.IndexOf("<MOVIMENTI>") >= 0 Then
                    Chk_Movimenti.Checked = True
                End If
                If Abilitazioni.IndexOf("<DENARO>") >= 0 Then
                    Chk_Denaro.Checked = True
                End If
                If Abilitazioni.IndexOf("<GG>") >= 0 Then
                    Chk_GestioneGenerale.Checked = True
                End If
                If Abilitazioni.IndexOf("<PIANOCONTI>") >= 0 Then
                    Chk_PianoConti.Checked = True
                End If
                If Abilitazioni.IndexOf("<REGISTRAZIONE>") >= 0 Then
                    Chk_Registrazioni.Checked = True
                End If
                If Abilitazioni.IndexOf("<BUDGET>") >= 0 Then
                    Chk_Budget.Checked = True
                End If
                If Abilitazioni.IndexOf("<USER>") >= 0 Then
                    Chk_GestioneUtenti.Checked = True
                End If


                If Abilitazioni.IndexOf("<CAUSALICONTABILI>") >= 0 Then
                    Chk_CausaliContabili.Checked = True
                End If
                If Abilitazioni.IndexOf("<MODALITAPAGAMENTO>") >= 0 Then
                    Chk_ModalitaPagamento.Checked = True
                End If
                If Abilitazioni.IndexOf("<TIPOREGISTRO>") >= 0 Then
                    Chk_TipoRegistro.Checked = True
                End If
                If Abilitazioni.IndexOf("<REGISTROIVA>") >= 0 Then
                    Chk_RegistroIVA.Checked = True
                End If
                If Abilitazioni.IndexOf("<DETRAIBILITA>") >= 0 Then
                    Chk_Detraibilita.Checked = True
                End If
                If Abilitazioni.IndexOf("<PERIODOCONFRONTO>") >= 0 Then
                    Chk_PeriodoaConfronto.Checked = True
                End If

                If Abilitazioni.IndexOf("<RITENUTE>") >= 0 Then
                    Chk_Ritenute.Checked = True
                End If


                If Abilitazioni.IndexOf("<SPESOMETRO>") >= 0 Then
                    Chk_Spesometro2017.Checked = True
                End If


                If Abilitazioni.IndexOf("<NONGENERALE>") >= 0 Then
                    Chk_NonGenerale.Checked = True
                End If
                If Abilitazioni.IndexOf("<SOLOGENERALE>") >= 0 Then
                    Chk_SoloGenerale.Checked = True
                End If

                If Abilitazioni.IndexOf("<NONPROTO>") >= 0 Then
                    Chk_NonProtocollo.Checked = True
                End If

                If Abilitazioni.IndexOf("<NONMAILING>") >= 0 Then
                    Chk_NonMailingList.Checked = True
                End If

                'Chk_NonProtocollo
                'Chk_NonMailingList

                If Abilitazioni.IndexOf("<NONOSPITI>") >= 0 Then
                    Chk_NonOspiti.Checked = True
                End If
                If Abilitazioni.IndexOf("<PORTENERIA>") >= 0 Then
                    Chk_Porteneria.Checked = True
                End If
                If Abilitazioni.IndexOf("<RICHIESTE>") >= 0 Then
                    Chk_Richieste.Checked = True
                End If
                If Abilitazioni.IndexOf("<EPERSONAM>") >= 0 Then
                    Chk_EPersonam.Checked = True
                End If
                If Abilitazioni.IndexOf("<EPERSONAM-AUT>") >= 0 Then
                    Chk_EPersonamAut.Checked = True
                End If
                If Abilitazioni.IndexOf("<ABILMODIFICA>") >= 0 Then
                    Chk_abilmodifica.Checked = True
                End If
                If Abilitazioni.IndexOf("<APP>") >= 0 Then
                    Chk_App.Checked = True
                End If
                If Abilitazioni.IndexOf("<SOLO730>") >= 0 Then
                    Chk_Solo730.Checked = True
                End If
                If Abilitazioni.IndexOf("<LOGPRIVACY>") >= 0 Then
                    Chk_LogPrivacy.Checked = True
                End If

                If Abilitazioni.IndexOf("<EPERSONAMUSER>") >= 0 Then
                    Chk_EPersonamUser.Checked = True
                End If

                If Abilitazioni.IndexOf("<INSERIMENTOON>") >= 0 Then
                    Chk_InserimentoOspite.Checked = True
                End If

                If Abilitazioni.IndexOf("<APPALTI>") >= 0 Then
                    Chk_Appalti.Checked = True
                End If

            End If
            Command1.Dispose()
            myreader1.Close()
            cn.Close()
            Txt_Utente.Enabled = False
            Exit Sub
        End If
    End Sub

    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_SODO") <> True Then

            Response.Redirect("login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call CaricaElenco()
    End Sub

    Private Sub CaricaElenco()
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))



        cn.Open()
        Dim Command1 As New OleDbCommand("SELECT * FROM UTENTE Where Cliente = ?", cn)

        Command1.Parameters.AddWithValue("@Id", Request.Item("IDCLIENTE"))

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("UTENTE", GetType(String))

        Dim myreader1 As OleDbDataReader = Command1.ExecuteReader()
        Do While myreader1.Read()
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = myreader1.Item("UTENTE")
            MyTable.Rows.Add(myriga)

        Loop
        Command1.Dispose()
        myreader1.Close()
        cn.Close()

        GridView1.AutoGenerateColumns = False
        GridView1.DataSource = MyTable
        GridView1.DataBind()
        Session("Appoggio") = MyTable
    End Sub


    Private Sub Pulisci()
        Txt_ConfermaPassword.Text = ""
        Txt_Email.Text = ""
        Txt_Password.Text = ""
        Txt_Utente.Text = ""
        Txt_ChiaveCR.Text = ""
        Txt_Report.Text = "MASTRINO=mastrinov.rpt BILANCIO=bilancioasezionicontrapposte.rpt REGISTROIVA=registroiva.rpt REGISTROIVATOT=registroivatotali.rpt LIQUIDAZIONEIVA=liquidazioneiva.rpt PIANOCONTI=ODV_pianoconti.rpt GIORNALECONTABILITA=giornalecontabilita.rpt STAMPAREVERSALI=reversaliunioncamere.rpt STAMPAMANDATI= STAMPADISTINTA=distintaunion.rpt BILANCIOIV= PRIMANOTA=PrimaNota.rpt PRIMANOTAGIORNO= PRIMANOTAREGISTRAZIONE= CLIENTIFORNITORI=ODV_StampaClientiFornitori.rpt PIANOCONTICONTROLLO=listapianoconti.rpt PIANOCONTIANOMALIE= FATTUREDETTAGLIATESANITARIO=stampaasliolo.rpt FATTUREDETTAGLIATESOCIALE=socialeiolo.rpt STAMPADOCUMENTIOSPITI1=fatturavillafioritaintestata.rpt STAMPADOCUMENTIOSPITI2= STAMPADOCUMENTIOSPITI3= STAMPADOCUMENTIOSPITI4= DOCUMENTOPERSONALIZZATO1= DOCUMENTOPERSONALIZZATO2= DOCUMENTOPERSONALIZZATO3= DOCUMENTOPERSONALIZZATO4= RENDICONTOCOMUNE=rendicontocomune.rpt RENDICONTOREGIONE=rendicontoregione.rpt RICEVUTA=ricevutaincasso.rpt STAMPASCHEDA=scheda.rpt".Replace(" ", vbNewLine)
        Txt_Utente.Enabled = True
    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        Lbl_Errori.Text = ""
        If Txt_Password.Text = "" And Txt_Utente.Enabled = True Then
            Lbl_Errori.Text = "Non puoi inserire un utente senza password"
            Exit Sub
        End If
        If Txt_ConfermaPassword.Text <> Txt_Password.Text Then
            Lbl_Errori.Text = "Le password non coincidono"
            Exit Sub
        End If
        If Len(Txt_Password.Text) < 5 And Txt_Utente.Enabled = True Then
            Lbl_Errori.Text = "Password troppo corta"
            Exit Sub
        End If


        If Chk_EPersonamUser.Checked = True And Txt_Utente.Text.IndexOf("senior") < 0 Then
            Lbl_Errori.Text = "Il nome epersonam può essere associato solo ad utente 'senior...' "
            Exit Sub
        End If

        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim Abilitazioni As String


        Abilitazioni = ""
        If Chk_GestioneOspiti.Checked = True Then
            Abilitazioni = "<GO>"
        End If
        If Chk_Anagrafica.Checked = True Then
            Abilitazioni = Abilitazioni & "<ANAGRAFICA>"
        End If
        If Chk_Calcolo.Checked = True Then
            Abilitazioni = Abilitazioni & "<CALCOLO>"
        End If
        If Chk_Emissione.Checked = True Then
            Abilitazioni = Abilitazioni & "<EMISSIONE>"
        End If
        If Chk_AddebitiAccrediti.Checked = True Then
            Abilitazioni = Abilitazioni & "<ADDEBITI>"
        End If
        If Chk_Incasso.Checked = True Then
            Abilitazioni = Abilitazioni & "<INCASSO>"
        End If
        If Chk_Tabelle.Checked = True Then
            Abilitazioni = Abilitazioni & "<TABELLA>"
        End If
        If Chk_Servizi.Checked = True Then
            Abilitazioni = Abilitazioni & "<SERVIZI>"
        End If
        If Chk_Movimenti.Checked = True Then
            Abilitazioni = Abilitazioni & "<MOVIMENTI>"
        End If
        If Chk_Denaro.Checked = True Then
            Abilitazioni = Abilitazioni & "<DENARO>"
        End If
        If Chk_GestioneGenerale.Checked = True Then
            Abilitazioni = Abilitazioni & "<GG>"
        End If

        If Chk_Registrazioni.Checked = True Then
            Abilitazioni = Abilitazioni & "<REGISTRAZIONE>"
        End If
        If Chk_Budget.Checked = True Then
            Abilitazioni = Abilitazioni & "<BUDGET>"
        End If
        If Chk_PianoConti.Checked = True Then
            Abilitazioni = Abilitazioni & "<PIANOCONTI>"
        End If

        If Chk_GestioneUtenti.Checked = True Then
            Abilitazioni = Abilitazioni & "<USER>"
        End If

        If Chk_CausaliContabili.Checked = True Then
            Abilitazioni = Abilitazioni & "<CAUSALICONTABILI>"
        End If
        If Chk_ModalitaPagamento.Checked = True Then
            Abilitazioni = Abilitazioni & "<MODALITAPAGAMENTO>"
        End If
        If Chk_TipoRegistro.Checked = True Then
            Abilitazioni = Abilitazioni & "<TIPOREGISTRO>"
        End If
        If Chk_RegistroIVA.Checked = True Then
            Abilitazioni = Abilitazioni & "<REGISTROIVA>"
        End If
        If Chk_Detraibilita.Checked = True Then
            Abilitazioni = Abilitazioni & "<DETRAIBILITA>"
        End If
        If Chk_PeriodoaConfronto.Checked = True Then
            Abilitazioni = Abilitazioni & "<PERIODOCONFRONTO>"
        End If

        If Chk_Ritenute.Checked = True Then
            Abilitazioni = Abilitazioni & "<RITENUTE>"
        End If

        If Chk_Spesometro2017.Checked = True Then
            Abilitazioni = Abilitazioni & "<SPESOMETRO>"
        End If

        If Chk_NonGenerale.Checked = True Then
            Abilitazioni = Abilitazioni & "<NONGENERALE>"
        End If
        If Chk_SoloGenerale.Checked = True Then
            Abilitazioni = Abilitazioni & "<SOLOGENERALE>"
        End If
        If Chk_NonOspiti.Checked = True Then
            Abilitazioni = Abilitazioni & "<NONOSPITI>"
        End If
        If Chk_Porteneria.Checked = True Then
            Abilitazioni = Abilitazioni & "<PORTENERIA>"
        End If
        If Chk_Richieste.Checked = True Then
            Abilitazioni = Abilitazioni & "<RICHIESTE>"
        End If

        If Chk_EPersonam.Checked = True Then
            Abilitazioni = Abilitazioni & "<EPERSONAM>"
        End If

        If Chk_EPersonamAut.Checked = True Then
            Abilitazioni = Abilitazioni & "<EPERSONAM-AUT>"
        End If

        If Chk_abilmodifica.Checked = True Then            
            Abilitazioni = Abilitazioni & "<ABILMODIFICA>"
        End If

        If Chk_APP.Checked = True Then
            Abilitazioni = Abilitazioni & "<APP>"
        End If


        If Chk_Protocollo.Checked = True Then
            Abilitazioni = Abilitazioni & "<PROTOCOLLO>"
        End If
        If Chk_SoloUscite.Checked = True Then
            Abilitazioni = Abilitazioni & "<SOLOUSCITE>"
        End If
        If Chk_SoloEntrate.Checked = True Then
            Abilitazioni = Abilitazioni & "<SOLOENTRATE>"
        End If

        If Chk_Solo730.Checked = True Then
            Abilitazioni = Abilitazioni & "<SOLO730>"
        End If


        If Chk_EPersonamUser.Checked = True Then
            Abilitazioni = Abilitazioni & "<EPERSONAMUSER>"
        End If

        If Chk_NonProtocollo.Checked = True Then
            'Abilitazioni.IndexOf("<NONPROTO>") >= 0 
            Abilitazioni = Abilitazioni & "<NONPROTO>"
        End If

        If Chk_NonMailingList.Checked = True Then
            'Abilitazioni.IndexOf("<NONPROTO>") >= 0 
            Abilitazioni = Abilitazioni & "<NONMAILING>"
        End If

        If Chk_LogPrivacy.Checked = True Then
            Abilitazioni = Abilitazioni & "<LOGPRIVACY>"
        End If



        If Chk_InserimentoOspite.Checked = True Then
            Abilitazioni = Abilitazioni & "<INSERIMENTOON>"
        End If

        If Chk_Appalti.Checked = True Then
            Abilitazioni = Abilitazioni & "<APPALTI>"
        End If

        Dim ConnectionString As String = Application("SENIOR")
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()





        Dim Command1 As New OleDbCommand("SELECT * FROM UTENTE Where UTENTE = ?", cn)


        Command1.Parameters.AddWithValue("@UTENTE", Txt_Utente.Text)


        Dim myreader1 As OleDbDataReader = Command1.ExecuteReader()
        If myreader1.Read() Then
            If Txt_Utente.Enabled = True Then
                Lbl_Errori.Text = "Nome utente già utilizzato non posso procedere con l'inserimento"
                myreader1.Close()
                cn.Close()
                Exit Sub
            End If
            If Txt_Password.Text = "" Then
                MySql = "update UTENTE set ChiaveCR = ?,CLIENTE = ?,EMAIL = ?,ABILITAZIONI = ?,PERSONALIZZAZIONI = ?  where UTENTE = ?"
            Else
                MySql = "update UTENTE set CHIAVE = ?,ChiaveCR = ?,CLIENTE = ?,EMAIL = ?,ABILITAZIONI = ?,PERSONALIZZAZIONI = ?  where UTENTE = ?"
            End If

            Dim Command2 As New OleDbCommand(MySql, cn)

            If Txt_Password.Text <> "" Then
                Command2.Parameters.AddWithValue("@CHIAVE", Txt_Password.Text)                
            End If
            Command2.Parameters.AddWithValue("@CHIAVECR", Txt_ChiaveCR.Text)

            Command2.Parameters.AddWithValue("@CLIENTE", Request.Item("IDCLIENTE"))
            Command2.Parameters.AddWithValue("@EMAIL", Txt_Email.Text)
            Command2.Parameters.AddWithValue("@ABILITAZIONI", Abilitazioni)
            Command2.Parameters.AddWithValue("@PERSONALIZZAZIONI", Txt_Report.Text.Replace(vbNewLine, " "))


            Command2.Parameters.AddWithValue("@UTENTE", Txt_Utente.Text)
            Command2.ExecuteNonQuery()

        Else
            MySql = "Insert Into UTENTE (CHIAVE,ChiaveCR,CLIENTE,STAMPEFINANZIARIA ,STAMPEOSPITI,STAMPETURNI ,PERSONALIZZAZIONI ,EMAIL ,ABILITAZIONI ,UTENTE ) values (?,?,?,?,?,?,?,?,?,?)"
            Dim Command2 As New OleDbCommand(MySql, cn)
            Command2.Parameters.AddWithValue("@CHIAVE", Txt_Password.Text)

            Command2.Parameters.AddWithValue("@CHIAVECR", Txt_ChiaveCR.Text)

            Command2.Parameters.AddWithValue("@CLIENTE", Request.Item("IDCLIENTE"))
            Command2.Parameters.AddWithValue("@STAMPEFINANZIARIA", "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=DATABASESTAMPE;User Id=sa;Password=MASTER;")
            Command2.Parameters.AddWithValue("@STAMPEOSPITI", "")
            Command2.Parameters.AddWithValue("@STAMPETURNI", "")
            Command2.Parameters.AddWithValue("@PERSONALIZZAZIONI", Txt_Report.Text.Replace(vbNewLine, " "))
            Command2.Parameters.AddWithValue("@EMAIL", Txt_Email.Text)
            Command2.Parameters.AddWithValue("@ABILITAZIONI", Abilitazioni)
            Command2.Parameters.AddWithValue("@UTENTE", Txt_Utente.Text)
            Command2.ExecuteNonQuery()

        End If
        Command1.Dispose()
        myreader1.Close()
        cn.Close()



        Txt_ConfermaPassword.Text = ""
        Txt_Email.Text = ""
        Txt_Password.Text = ""
        Txt_Utente.Text = ""
        Txt_ChiaveCR.Text = ""
        Txt_Utente.Enabled = True
        Call CaricaElenco()

        Chk_GestioneOspiti.Checked = False

        Chk_Anagrafica.Checked = False

        Chk_Calcolo.Checked = False

        Chk_Emissione.Checked = False


        Chk_AddebitiAccrediti.Checked = False

        Chk_Incasso.Checked = False

        Chk_Tabelle.Checked = False

        Chk_Servizi.Checked = False

        Chk_Movimenti.Checked = False

        Chk_Denaro.Checked = False

        Chk_GestioneGenerale.Checked = False


        Chk_Registrazioni.Checked = False

        Chk_Budget.Checked = False

        Chk_PianoConti.Checked = False


        Chk_GestioneUtenti.Checked = False


        Chk_CausaliContabili.Checked = False

        Chk_ModalitaPagamento.Checked = False

        Chk_TipoRegistro.Checked = False

        Chk_RegistroIVA.Checked = False

        Chk_Detraibilita.Checked = False

        Chk_PeriodoaConfronto.Checked = False

        Chk_Ritenute.Checked = False
        Chk_Spesometro2017.Checked = False
        Chk_NonGenerale.Checked = False

        Chk_SoloGenerale.Checked = False

        Chk_NonOspiti.Checked = False
        Chk_Porteneria.Checked = False

        Chk_Richieste.Checked = False

        Chk_EPersonam.Checked = False

        Chk_EPersonamAut.Checked = False
        Chk_EPersonamUser.Checked = False

        Chk_abilmodifica.Checked = False
        Chk_App.Checked = False

        Chk_InserimentoOspite.Checked = False
        Chk_Appalti.Checked = False
    End Sub

    Protected Sub Btn_Pulisci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Pulisci.Click
        Call Pulisci()
    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Login.aspx")
    End Sub


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try

            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE"))
            Dim I As Integer
            For I = 1 To 20
                request.Add("username", Txt_Utente.Text.ToString.Replace("<" & I & ">", ""))
            Next
            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String

            guarda = Txt_ChiaveCR.Text '"$2a$10$0jD7O/J51.5cJI7eO5joLOEr9YLdm715UE/YKRqE8skrMTZFL5r6u"

            request.Add("code", guarda)
            REM request.Add("password", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)
            REM Dim result As Object = client.UploadValues("http://api-v1.e-personam.com/v0/oauth/token", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()


            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Protected Sub Btn_Verifica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Verifica.Click
        Dim token As String


        token = LoginPersonam(Context)

        If token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  alert('Errore connessione');  });", True)

            Exit Sub
        End If

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  alert('" & rawresp.Replace("'", "") & "');  });", True)


    End Sub

    Protected Sub Btn_Token_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Token.Click
        Dim Token As String


        Token = LoginPersonam(HttpContext.Current)

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  alert('" & Token.Replace("'", "") & "');  });", True)

    End Sub

    Protected Sub GridView1_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GridView1.RowDeleted

    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim D As Integer

        MyTable = Session("Appoggio")

        D = Val(e.RowIndex)


        If MyTable.Rows(D).Item(0).ToString = Session("UTENTE") Then
            Lbl_Errori.Text = "Non posso eliminare l'utente attivo"
            Exit Sub
        End If

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim Command1 As New OleDbCommand("DELETE  FROM UTENTE Where UTENTE = ?", cn)


        Command1.Parameters.AddWithValue("@UTENTE", MyTable.Rows(D).Item(0).ToString)
        Command1.ExecuteNonQuery()
        cn.Close()

        Txt_ConfermaPassword.Text = ""
        Txt_Email.Text = ""
        Txt_ChiaveCR.Text = ""
        Txt_Password.Text = ""
        Txt_Utente.Text = ""
        Txt_Utente.Enabled = True
        Call CaricaElenco()
    End Sub




End Class
