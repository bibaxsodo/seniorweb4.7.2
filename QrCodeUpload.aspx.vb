﻿Imports System.Data.OleDb
Partial Class QrCodeUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim cnSenior As OleDbConnection

        cnSenior = New System.Data.OleDb.OleDbConnection(Application("SENIOR"))


        cnSenior.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * From Token where tOKEN = ? And DataScadenza >= ?"
        cmd.Parameters.AddWithValue("@Token", Request.Item("DEST"))
        cmd.Parameters.AddWithValue("@DataScadenza", Now)
        cmd.Connection = cnSenior

        Dim Read As OleDbDataReader = cmd.ExecuteReader()

        If Read.Read() Then
            lbl_nome.Text = campodb(Read.Item("Value2"))
            AutentificaLogin()
        Else
            lbl_nome.Text = "QRCode scaduto"
        End If




        cnSenior.Close()

    End Sub
    Private Sub AutentificaLogin()
        Dim tkt As FormsAuthenticationTicket
        Dim cookiestr As String
        Dim ck As HttpCookie

        tkt = New FormsAuthenticationTicket(1, "", DateTime.Now, DateTime.Now.AddMinutes(10), True, "")
        cookiestr = FormsAuthentication.Encrypt(tkt)
        ck = New HttpCookie(FormsAuthentication.FormsCookieName, cookiestr)
        ck.Expires = tkt.Expiration
        ck.Path = FormsAuthentication.FormsCookiePath
        Response.Cookies.Add(ck)

    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
