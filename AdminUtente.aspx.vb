﻿Imports System.Data.SqlTypes

Partial Class AdminUtente
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub

        If Request.Item("Logout") = "TRUE" Then
            Session.Clear()
            Session.Abandon()
        End If
        Session("UTENTE") = ""
        Session("ODV") = ""
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim k As New Cls_Login


        'Session("TIPOAPP") = "RSA"

        k.Utente = UCase(Txt_Login.Text)
        k.Chiave = Txt_Password.Text
        k.Leggi(Application("SENIOR"))

        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""


        If k.Ospiti <> "" Then
            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = Txt_Login.Text
            Response.Redirect("creautenti.aspx")
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub


End Class
