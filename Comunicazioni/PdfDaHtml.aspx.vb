﻿
Partial Class Comunicazioni_PdfDaHtml
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        Timer1.Interval = 1000


    End Sub


    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim contenuto As String

        Timer1.Enabled = False

        contenuto = Session("PdfDaHTML")
        Response.Clear()

        Dim sb As New StringBuilder()
        sb.Append("<html>")
        sb.AppendFormat("<body onload='document.forms[""form""].submit()'>")
        sb.AppendFormat("<form name='form' action='http://94.177.206.89/DaHtmlaPdf/dagetapdf.aspx?&VERIFICA=" & SHA1("SAURO" & contenuto & "GABELLINI") & "' method='post'>")
        sb.AppendFormat("<textarea name='URLHTML' style='display:none;'>" & contenuto & "</textarea>")

        sb.Append("</form>")
        sb.Append("</body>")
        sb.Append("</html>")

        Response.Write(sb.ToString())

        Response.End()
    End Sub
End Class
