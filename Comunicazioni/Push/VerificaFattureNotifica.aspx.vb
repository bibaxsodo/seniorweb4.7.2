﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util


Partial Class Comunicazioni_Push_VerificaFattureNotifica
    Inherits System.Web.UI.Page
    Dim TabRegistri As New System.Data.DataTable("TabRegistri")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\..\Login.aspx")
            Exit Sub
        End If
        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        TabRegistri.Clear()
        TabRegistri.Columns.Clear()
        TabRegistri.Columns.Add("Numero", GetType(String))
        TabRegistri.Columns.Add("Registro", GetType(String))
        TabRegistri.Columns.Add("PDF", GetType(String))

        Dim cn As OleDbConnection


        Dim XS As New Cls_Login


        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select RegistroIVA From Temp_MovimentiContabiliTesta group by  RegistroIVA "
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = TabRegistri.NewRow()
            myriga(0) = myPOSTreader.Item("RegistroIVA")
            Dim DecReg As New Cls_RegistroIVA


            DecReg.Tipo = myPOSTreader.Item("RegistroIVA")
            DecReg.Leggi(Session("DC_TABELLE"), DecReg.Tipo)

            myriga(1) = DecReg.Descrizione

            myriga(2) = "R_" & XS.Cliente & "_R_" & DecReg.Tipo & ".pdf"


            TabRegistri.Rows.Add(myriga)
        Loop


        Grid.AutoGenerateColumns = True
        Grid.DataSource = TabRegistri


        Grid.DataBind()



        Dim i As Integer

        For i = 0 To Grid.Rows.Count - 1

            Grid.Rows(i).Cells(2).Text = "<a href=""" & Page.ResolveUrl("../../temp/" & Grid.Rows(i).Cells(2).Text) & """ target=""_blank"" >" & Grid.Rows(i).Cells(2).Text & "</a>"
        Next

        Call ErroriNotifiche()

    End Sub

    Private Sub ErroriNotifiche()
        Dim cn As OleDbConnection
        Dim Appoggio As String = ""

        Label1.Text = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from notifiche ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Appoggio = campodb(myPOSTreader.Item("ErroriCalcolo")) & campodb(myPOSTreader.Item("ErroriEmissione")) & campodb(myPOSTreader.Item("ErroriStampa")) & campodb(myPOSTreader.Item("ErroriWarning"))
        End If
        myPOSTreader.Close()
        cn.Close()

        If Not IsNothing(Appoggio) Then
            Appoggio = Appoggio.Replace(Chr(13), "<br/>")
        Else
            Appoggio = ""
        End If

        Label1.Text = Appoggio
    End Sub


    Protected Sub Btn_Conferma_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Conferma.Click
        Dim Em As New Cls_EmissioneRetta

        Em.ConnessioneGenerale = Session("DC_GENERALE")
        Em.ConnessioneOspiti = Session("DC_OSPITE")


        If Not Em.VerificaEffettiva() Then


            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono registrazioni dopo emmissione di prova, non posso rendere definitive');", True)
            REM Exit Sub

        End If



        Dim cn As New OleDbConnection

        cn.ConnectionString = Session("DC_GENERALE")

        cn.Open()


        Dim CmdTesta As New OleDbCommand

        CmdTesta.CommandText = "Select AnnoProtocollo,RegistroIva,max(DataRegistrazione) AS PiuAlta,max(DataRegistrazione) AS PiuBassa  From  Temp_MovimentiContabiliTesta Group by AnnoProtocollo,RegistroIva"
        CmdTesta.Connection = cn
        Dim ReadREg As OleDbDataReader = CmdTesta.ExecuteReader()
        Do While ReadREg.Read()
            Dim CmdTestaEffettivo As New OleDbCommand

            CmdTestaEffettivo.CommandText = "Select top 1 DataRegistrazione From  MovimentiContabiliTesta Where  AnnoProtocollo = ? And RegistroIva = ? Order by DataRegistrazione Desc"
            CmdTestaEffettivo.Connection = cn
            CmdTestaEffettivo.Parameters.AddWithValue("@AnnoProtocollo", campodbN(ReadREg.Item("AnnoProtocollo")))
            CmdTestaEffettivo.Parameters.AddWithValue("@RegistroIva", campodbN(ReadREg.Item("RegistroIva")))
            Dim RedEffettivo As OleDbDataReader = CmdTestaEffettivo.ExecuteReader()

            If RedEffettivo.Read Then
                If Format(campodbd(RedEffettivo.Item("DataRegistrazione")), "yyyy-MM-dd") > Format(campodbd(ReadREg.Item("PiuBassa")), "yyyy-MM-dd") Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione/i con data precedente a quella in archivio');", True)
                    Exit Sub
                End If
            End If
        Loop
        ReadREg.Close()


        Dim CmdRiga As New OleDbCommand
        CmdRiga.CommandText = "Select count(*) as registrazioneerrata From  Temp_MovimentiContabiliRiga Where importo  > 0 and (Select COUNT(*) from PianoConti Where Mastro = Temp_MovimentiContabiliRiga.MastroPartita and Conto = Temp_MovimentiContabiliRiga.ContoPartita and Sottoconto = Temp_MovimentiContabiliRiga.SottocontoPartita ) = 0"
        CmdRiga.Connection = cn
        Dim ReadRiga As OleDbDataReader = CmdRiga.ExecuteReader()
        If ReadRiga.Read() Then
            If campodbN(ReadRiga.Item("registrazioneerrata")) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione/i con conto non esistente nel piano dei conti');", True)
                Exit Sub
            End If
        End If
        ReadRiga.Close()

        Dim AppoggioNumero As Integer = 0

        AppoggioNumero = VerificaRegistrazione()
        If AppoggioNumero > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione formalmente errata " & AppoggioNumero & "');", True)
            Exit Sub
        End If




        If Request.Item("TIPO") = "NONEMISSIONE" Then
            Em.DaProvaAdEffettiva(True)
        Else
            Em.DaProvaAdEffettiva()
        End If

        Dim CmdTestaPrivacy As New OleDbCommand

        CmdTestaPrivacy.CommandText = "Select * From  Temp_MovimentiContabiliTesta"
        CmdTestaPrivacy.Connection = cn
        Dim ReadTesta As OleDbDataReader = CmdTestaPrivacy.ExecuteReader()
        Do While ReadTesta.Read()
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", campodbN(ReadTesta.Item("NumeroRegistrazione")), "", "I", "DOCUMENTO", "")
        Loop
        ReadTesta.Close()


        Em.EliminaTemporanei()

        cn.Close()

        If Request.Item("URL") = "FatturaDaAddebitiAccrediti.aspx" Then
            Try
                Dim cnOspite As New OleDbConnection
                Dim Vettore(100) As Integer
                Dim I As Integer

                Vettore = Session("RIATTIVAADD")
                cnOspite.ConnectionString = Session("DC_OSPITE")

                cnOspite.Open()

                For I = 0 To 100
                    If Not IsNothing(Vettore(I)) Then
                        If Vettore(I) > 0 Then
                            Dim CmdUpdate As New OleDbCommand

                            CmdUpdate.Connection = cnOspite
                            CmdUpdate.CommandText = "UPDATE ADDACR SET ADDACR.NumeroRegistrazione =0,ADDACR.Elaborato = 0   WHERE DataModifica = {ts '" & Format(Now, "yyyy-MM-dd") & " 00:00:00'} AND ID = ?"
                            CmdUpdate.Parameters.AddWithValue("@ID", Vettore(I))
                            CmdUpdate.ExecuteNonQuery()
                        End If
                    End If
                Next
                cnOspite.Close()

            Catch ex As Exception

            End Try
        End If




        'Btn_Modifica.Enabled = False
        Btn_Conferma.Enabled = False


        If Request.Item("TIPO") = "NONEMISSIONE" Then
            Exit Sub
        End If

        Dim XParam As New Cls_Parametri
        Dim KAnno As Long
        Dim KMese As Long

        XParam.LeggiParametri(Session("DC_OSPITE"))
        KMese = XParam.MeseFatturazione
        KAnno = XParam.AnnoFatturazione
        If KMese = 12 Then
            KMese = 1
            KAnno = KAnno + 1
        Else
            KMese = KMese + 1
            KAnno = KAnno
        End If
        XParam.MeseFatturazione = KMese
        XParam.AnnoFatturazione = KAnno
        XParam.ScriviParametri(Session("DC_OSPITE"))

        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))

    End Sub


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function VerificaRegistrazione() As Integer

        Dim cn As OleDbConnection

        Dim TotCliFor As Double
        Dim DareAvere As String
        Dim TotImposta As Double
        Dim TotImponibile As Double
        Dim TotDare As Double
        Dim TotAvere As Double
        Dim TotAttivita As Double
        Dim TotPassivita As Double
        Dim TotCosti As Double
        Dim TotRicavi As Double
        Dim TotOrdine As Double
        Dim Differenza As Double
        Dim MySql As String = ""

        Dim StringaDegliErrori As String = ""


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        VerificaRegistrazione = 0


        MySql = "SELECT NumeroRegistrazione, CausaleContabile FROM Temp_MovimentiContabiliTesta"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120

        Dim MRead As OleDbDataReader = cmd.ExecuteReader()
        Do While MRead.Read
            TotCliFor = 0
            DareAvere = ""
            TotImposta = 0
            TotImponibile = 0
            TotDare = 0
            TotAvere = 0
            TotAttivita = 0
            TotPassivita = 0
            TotCosti = 0
            TotRicavi = 0
            TotOrdine = 0

            MySql = "SELECT * FROM Temp_MovimentiContabiliRiga WHERE Numero = " & campodbN(MRead.Item("NumeroRegistrazione")) & " ORDER BY RigaDaCausale"
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = MySql
            cmdR.Connection = cn
            cmdR.CommandTimeout = 120

            Dim MReadRoga As OleDbDataReader = cmdR.ExecuteReader()
            Do While MReadRoga.Read
                If campodb(MReadRoga.Item("Tipo")) = "CF" Then
                    TotCliFor = campodbN(MReadRoga.Item("Importo"))
                    DareAvere = campodb(MReadRoga.Item("DareAvere"))
                End If
                If campodb(MReadRoga.Item("Tipo")) = "IV" Then
                    Dim MiaIVa As New Cls_IVA
                    MiaIVa.Codice = campodb(MReadRoga.Item("CodiceIVA"))
                    MiaIVa.Leggi(Session("DC_TABELLE"), MiaIVa.Codice)

                    If Math.Round(CDbl(campodb(MReadRoga.Item("Imponibile"))) * MiaIVa.Aliquota, 2, MidpointRounding.AwayFromZero) <> Math.Round(CDbl(campodb(MReadRoga.Item("Importo"))), 2, MidpointRounding.AwayFromZero) Then
                        VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                    End If

                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - Math.Round(campodbN(MReadRoga.Item("Importo")), 2)
                        TotImponibile = TotImponibile - Math.Round(campodbN(MReadRoga.Item("Imponibile")), 2)
                    Else
                        TotImposta = TotImposta + Math.Round(campodbN(MReadRoga.Item("Importo")), 2)
                        TotImponibile = TotImponibile + Math.Round(campodbN(MReadRoga.Item("Imponibile")), 2)
                    End If
                End If
                If campodb(MReadRoga.Item("Tipo")) = "AR" Or campodb(MReadRoga.Item("Tipo")) = "AB" Then
                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile - campodb(MReadRoga.Item("Imponibile"))
                    Else
                        TotImposta = TotImposta + campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile + campodb(MReadRoga.Item("Imponibile"))
                    End If
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                    TotDare = TotDare + campodbN(MReadRoga.Item("Importo"))
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "A" Then
                    TotAvere = TotAvere + campodbN(MReadRoga.Item("Importo"))
                End If
            Loop
            If Format(TotDare, "0.00") <> Format(TotAvere, "0.00") Then
                VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                Exit Function
            End If
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), campodb(MRead.Item("CausaleContabile")))

            If CauCon.TipoDocumento <> "" Then
                If Format(TotCliFor, "0.00") <> Format(TotImponibile + TotImposta, "0.00") Then
                    VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                    Exit Function
                End If
            End If

        Loop
        MRead.Close()

        cn.Close()
    End Function



    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Ospiti.aspx")
    End Sub
End Class
