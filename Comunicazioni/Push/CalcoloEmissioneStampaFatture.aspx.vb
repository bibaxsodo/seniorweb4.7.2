﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class Comunicazioni_Push_CalcoloEmissioneStampaFatture
    Inherits System.Web.UI.Page
    Private Delegate Sub DoWorkDelegate(ByRef data As Object)

    REM CALCOLO
    Public StDataCond As String
    Private ConessioniOspiti As String
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoErrori As String
    Private CServInS As String
    Private CodOsp As Long
    REM EMISSIONE


    Private DataP As Date
    Private DataA As Date

    Private AttivaEnte As Date
    Private DataEnte As Date
    Private ScadenzaEnte As Date


    Private ConnessioneGenerale As String
    Private ConnessioneOspiti As String
    Private ConnessioneTabelle As String
    Private CampoEstrazione As String
    Private CampoProva As String
    Private CampoWr As String

    Dim Vt_List2(100) As String
    Public DiProva As Boolean
    Dim TabRegistri As New System.Data.DataTable("TabRegistri")



    Private Function LeggiRegistriIVAMETODO(ByVal Anno As Integer, ByVal Numero As Integer) As Integer

        Return 0
        Exit Function

        Dim cnMetodo As OleDbConnection
        Dim DataBaseMetodo As String = ""
        Dim Codice As String = ""

        LeggiRegistriIVAMETODO = 0

        If Session("NomeEPersonam") = "MAGIERA" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPMAGANS;User Id=sa;Password=advenias2012;"
        Else
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPCHARITAS;User Id=sa;Password=advenias2012;"
        End If



        cnMetodo = New Data.OleDb.OleDbConnection(DataBaseMetodo)

        cnMetodo.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from [Y_VistaTabRegistriIvaVenditeProgressivi]  where Tipo = ? And ESERCIZIO = ?")
        cmd.Connection = cnMetodo
        cmd.Parameters.AddWithValue("@Tipo", Numero)
        cmd.Parameters.AddWithValue("@Anno", Anno)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            LeggiRegistriIVAMETODO = campodb(myPOSTreader.Item("NRMAXPROTO"))
        End If
        myPOSTreader.Close()


        cnMetodo.Close()

    End Function

    Private Sub BindGen()
        TabRegistri = Session("TabRegistri")
        Grd_Registri.AutoGenerateColumns = False
        Grd_Registri.DataSource = TabRegistri


        Grd_Registri.Columns(2).HeaderText = "Protocollo " & Year(Txt_DataP.Text)
        If IsDate(Txt_DataA.Text) Then
            If Year(Txt_DataP.Text) <> Year(Txt_DataA.Text) Then
                Grd_Registri.Columns(3).HeaderText = "Protocollo " & Year(Txt_DataA.Text)
                Grd_Registri.Columns(3).Visible = True
            Else
                Grd_Registri.Columns(3).Visible = False
            End If
        Else
            Grd_Registri.Columns(3).Visible = False
        End If

        Grd_Registri.DataBind()
    End Sub


    Private Sub CaricaTabellaRegistri()

        TabRegistri.Clear()
        TabRegistri.Columns.Clear()
        TabRegistri.Columns.Add("Numero", GetType(String))
        TabRegistri.Columns.Add("Registro", GetType(String))
        TabRegistri.Columns.Add("Protocollo1", GetType(String))
        TabRegistri.Columns.Add("Protocollo2", GetType(String))


        Dim cn As OleDbConnection
        Dim cnGen As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnGen = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        cnGen.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TipoRegistro Where TipoIVA = 'V' OR TipoIVA = 'C'"

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = TabRegistri.NewRow()
            myriga(0) = campodbN(myPOSTreader.Item("Tipo"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            Dim cmdProg As New OleDbCommand()
            cmdProg.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_DataP.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
            cmdProg.Connection = cnGen
            Dim RdProg As OleDbDataReader = cmdProg.ExecuteReader()
            If RdProg.Read Then
                myriga(2) = campodbN(RdProg.Item(0))
            End If
            RdProg.Close()


            If IsDate(Txt_DataA.Text) Then
                If Year(Txt_DataP.Text) <> Year(Txt_DataA.Text) Then
                    Dim cmdProgA As New OleDbCommand()
                    cmdProgA.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_DataA.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
                    cmdProgA.Connection = cnGen
                    Dim RdProgA As OleDbDataReader = cmdProgA.ExecuteReader()
                    If RdProgA.Read Then
                        myriga(3) = campodbN(RdProgA.Item(0))
                    End If
                    RdProgA.Close()
                Else
                    myriga(3) = 0
                End If
            Else
                myriga(3) = 0
            End If

            If Session("NomeEPersonam") = "MAGIERA" Or Session("NomeEPersonam") = "CHARITAS" Then
                Dim NumeroX As Integer = LeggiRegistriIVAMETODO(Year(Txt_DataA.Text), campodbN(myPOSTreader.Item("Tipo")))

                If Val(myriga(3)) < NumeroX Then
                    myriga(3) = NumeroX
                End If
            End If

            TabRegistri.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
        cnGen.Close()

        Session("TabRegistri") = TabRegistri
        BindGen()
    End Sub


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Boolean
        Dim MySql As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(ConnessioniOspiti)

        cn.Open()

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim RsMovimenti As OleDbDataReader = cmd.ExecuteReader()


        If RsMovimenti.Read Then
            OspitePresente = False
            If campodb(RsMovimenti.Item("TipoMov")) = "13" And Format(campodbd(RsMovimenti.Item("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If campodb(RsMovimenti.Item("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        Else
            OspitePresente = False
        End If
        RsMovimenti.Close()
        cn.Close()
    End Function


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Public Sub DoWork(ByVal data As Object)

        calcolo("", 0, Txt_Anno.Text, Dd_Mese.SelectedValue, ConessioniOspiti, data)
    End Sub



    Private Function SendNotifica(ByVal API_KEY As String, ByVal GCM_ENDPOINT As String, ByVal subscriptionId As String) As Long
        Dim rawresp As String


        Try



            Dim client As HttpWebRequest = WebRequest.Create("https://android.googleapis.com/gcm/send")



            Dim json As String = "{""registration_ids"":[""" + subscriptionId + """]}"


            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(json)

            client.Method = "POST"
            client.KeepAlive = False
            client.Headers.Add("Authorization", "key=" & API_KEY)
            client.ContentType = "application/json" '"Content-Type: application/json"
            client.ContentLength = byteArray.Length

            Dim dataStream As Stream = client.GetRequestStream()
            REM client.ContentLength = byteArray.Length

            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()


            Dim responseReader As New StreamReader(client.GetResponse().GetResponseStream())
            Dim responseData As String = responseReader.ReadToEnd()

            Console.Write(responseData.ToString)

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('" & ex.Message & "');", True)
            Exit Function
        End Try

    End Function


    Protected Sub calcolo(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal txtanno As Long, ByVal ddmese As Long, ByVal ConnessioneOspiti As String, ByRef data As Object)




        Dim ConnectionString As String = ConnessioneOspiti
        Dim TipoCentro As String
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long
        Dim Mese As Integer
        Dim Anno As Integer
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim cn As OleDbConnection
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data


        'System.Web.SessionState.HttpSessionState = data


        Dim Mnotify As New Cls_Notifica


        Mnotify.API_KEY = Session("API_KEY")
        Mnotify.subscriptionId = Session("subscriptionId")


        cn = New Data.OleDb.OleDbConnection(ConnessioneOspiti)

        cn.Open()


        Dim UsoExtraParente As Integer = 0



        Dim cmdParenteRetta1 As New OleDbCommand()
        cmdParenteRetta1.CommandText = "SELECT count(*) as usati FROM [EXTRAOSPITE] where RIPARTIZIONE = 'P' "
        cmdParenteRetta1.Connection = cn
        Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
        If RDParenteRetta1.Read() Then
            If Val(campodb(RDParenteRetta1.Item(0))) > 0 Then
                UsoExtraParente = 1
            End If
        End If
        RDParenteRetta1.Close()


        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette
        SyncLock (SessioneTP.SyncRoot())
            SessioneTP("CampoErrori") = ""
            SessioneTP("CampoProgressBar") = 0
        End SyncLock


        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.ConguaglioMinimo = f.ConguaglioMinimo
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo
        XS.ApriDB(ConnessioneOspiti, Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = ConnessioneOspiti
        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.CaricaCausali()
        Mese = ddmese
        Anno = txtanno
        StDataCond = " Data < {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}"

        Dim ListinoPresente As Boolean = False
        Dim VerificaListino As New Cls_Tabella_Listino

        ListinoPresente = VerificaListino.EsisteListino(ConnessioneOspiti)


        VarCentroServizio = "0005"
        varcodiceospite = 371

        Dim Mysql As String
        Dim MysqlCount As String


        If CodiceOspite = 0 Then
            Mysql = " SELECT MOVIMENTI.CENTROSERVIZIO, MOVIMENTI.CODICEOSPITE FROM MOVIMENTI "
            Mysql = Mysql & " WHERE ("
            Mysql = Mysql & "(SELECT TOP 1 TIPOMOV FROM MOVIMENTI AS SUBMOV WHERE SUBMOV.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBMOV.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND DATA <= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC,PROGRESSIVO DESC ) <> '13' "
            Mysql = Mysql & " OR "
            Mysql = Mysql & " (SELECT COUNT(TIPOMOV) FROM MOVIMENTI AS SUBMOV WHERE SUBMOV.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBMOV.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND SUBMOV.DATA >={ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'}  AND SUBMOV.DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} )  > 0"
            Mysql = Mysql & " OR"

            Mysql = Mysql & " (SELECT COUNT(TIPOMOV) FROM ADDACR AS SUBADC WHERE  SUBADC.CENTROSERVIZIO =MOVIMENTI.CENTROSERVIZIO AND"
            Mysql = Mysql & " SUBADC.CODICEOSPITE =  MOVIMENTI.CODICEOSPITE AND SUBADC.DATA >={ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'}   AND SUBADC.DATA <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} )  > 0"

            Mysql = Mysql & " ) "
            Mysql = Mysql & " AND (SELECT count(CodiceOspite)  From AnagraficaComune Where AnagraficaComune.CodiceOspite = MOVIMENTI.CODICEOSPITE And CodiceParente = 0 And NonInUso = 'S') < 1"

            Mysql = Mysql & " GROUP BY  MOVIMENTI.CENTROSERVIZIO, MOVIMENTI.CODICEOSPITE "

            MysqlCount = "select count(*) as Totale From (" & Mysql & ") As TabAppo"

        Else
            Mysql = "SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE "
            MysqlCount = "select count(*) as Totale From (SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & CodiceOspite & " AND CENTROSERVIZIO = '" & Cserv & "' And MOVIMENTI.TIPOMOV = '05') As TabAppo"
        End If

        Dim xMax As Long
        Dim xNum As Long = 0

        xMax = 0

        Dim cmdConta As New OleDbCommand()
        cmdConta.CommandText = MysqlCount
        cmdConta.Connection = cn
        Dim RdOspitiConta As OleDbDataReader = cmdConta.ExecuteReader()
        If RdOspitiConta.Read() Then
            xMax = RdOspitiConta.Item("Totale")
        End If
        RdOspitiConta.Close()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = Mysql
        cmd.Connection = cn
        Dim RdOspiti As OleDbDataReader = cmd.ExecuteReader()

        XS.EliminaDatiRette(Mese, Anno, "", 0)


        Do While RdOspiti.Read


            Dim MyCserv As New Cls_CentroServizio

            MyCserv.Leggi(ConnessioneOspiti, campodb(RdOspiti.Item("CentroServizio")))


            If MyCserv.NonCalcolare = 0 Then

                VarCentroServizio = campodb(RdOspiti.Item("CentroServizio"))
                varcodiceospite = campodb(RdOspiti.Item("CodiceOspite"))

                Dim LeggiOspite As New ClsOspite

                LeggiOspite.Leggi(ConnessioneOspiti, varcodiceospite)


                If LeggiOspite.CONSENSOINSERIMENTO = 2 Then
                    XS.StringaDegliErrori = XS.StringaDegliErrori & "Consenso non dato al trattamento dei dati personali per ospite " & LeggiOspite.Nome & vbNewLine
                End If

                xNum = xNum + 1


                Try
                    Cache("CampoProgressBar" + Session.SessionID) = Math.Round(100 / xMax * xNum, 2)
                    Cache("NomeOspite" + Session.SessionID) = campodb(LeggiOspite.Nome)
                    Session("CampoProgressBar") = Math.Round(100 / xMax * xNum, 2)
                    Session("NomeOspite") = campodb(LeggiOspite.Nome)
                Catch ex As Exception

                End Try

                SyncLock (SessioneTP.SyncRoot())
                    SessioneTP("CampoProgressBar") = Math.Round(100 / xMax * xNum, 2)
                    SessioneTP("NomeOspite") = LeggiOspite.Nome
                End SyncLock


                Dim cmdAddOsp As New OleDbCommand()
                cmdAddOsp.CommandText = "Update TabellaParametri Set CodiceOspite = " & Val(campodb(RdOspiti.Item("CodiceOspite")))
                cmdAddOsp.Connection = cn
                cmdAddOsp.ExecuteNonQuery()

                If Mese = Val(MyCserv.Mese1) Or Mese = Val(MyCserv.Mese2) Or Mese = Val(MyCserv.Mese3) Or Mese = Val(MyCserv.Mese4) Or Mese = Val(MyCserv.Mese5) Then


                Else
                    If Not OspitePresente(VarCentroServizio, varcodiceospite, Mese, Anno, ConnessioneOspiti) Then

                        Call XS.AzzeraTabella()

                        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                        XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

                        XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)
                        xMese = Mese
                        xAnno = Anno
                        If xMese = 12 Then
                            xMese = 1
                            xAnno = Anno + 1
                        Else
                            xMese = Mese + 1
                            xAnno = Anno
                        End If
                        Dim XOsp As New ClsOspite

                        XOsp.Leggi(ConnessioneOspiti, varcodiceospite)
                        If XOsp.FattAnticipata = "S" Then
                            Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese, xAnno)
                            Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese, xAnno)


                            Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese, xAnno)

                            Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese, xAnno)

                            Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese, xAnno)
                            Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese, xAnno)



                            Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese, xAnno, False)
                            Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese, xAnno)
                        End If
                        TipoCentro = XS.CampoCentroServizio(VarCentroServizio, "TIPOCENTROSERVIZIO")
                        Call XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                    Else
                        Call XS.AzzeraTabella()

                        If ListinoPresente Then
                            Dim MOs As New Cls_Listino

                            MOs.CENTROSERVIZIO = VarCentroServizio
                            MOs.CODICEOSPITE = varcodiceospite
                            MOs.LeggiAData(ConnessioneOspiti, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))


                            If MOs.CodiceListino <> "" Then
                                Dim DeCListi As New Cls_Tabella_Listino

                                DeCListi.Codice = MOs.CodiceListino
                                DeCListi.LeggiCausale(ConnessioneOspiti)

                                If Year(DeCListi.DATA) > 1900 Then
                                    If Format(DeCListi.DATA, "yyyMMdd") <= Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                                        Dim DecOspite As New ClsOspite

                                        DecOspite.CodiceOspite = varcodiceospite
                                        DecOspite.Leggi(ConnessioneOspiti, DecOspite.CodiceOspite)

                                        XS.StringaDegliErrori = XS.StringaDegliErrori & "Ospite " & varcodiceospite & " " & DecOspite.Nome & " ha un listino scaduto " & DeCListi.Descrizione & vbNewLine
                                    End If
                                End If
                            End If

                        End If

                        XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)
                        XS.PrimoDataAccoglimentoOspite = XS.PrimaDataAccoglimento(VarCentroServizio, varcodiceospite, Mese, Anno)

                        If UsoExtraParente = 1 Then
                            XS.VerificaPrimaCalcolo(VarCentroServizio, varcodiceospite)
                        End If

                        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
                        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
                        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
                        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
                        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
                        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

                        If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                            XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
                        End If


                        XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

                        If MyCserv.TIPOCENTROSERVIZIO = "A" Then
                            XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
                        End If

                        If Val(MyCserv.VerificaImpegnativa) = 1 Then
                            Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
                        End If


                        XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, Mese, Anno)


                        XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



                        XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
                        XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
                        XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



                        XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

                        XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

                        XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)


                        'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
                        XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

                        XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



                        XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

                        XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

                        If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
                            REM  Errori = True
                        Else
                            XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
                        End If
                    End If
                End If
            End If
        Loop
        RdOspiti.Close()


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = "Update TabellaParametri Set CodiceOspite = 0"
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()

        SyncLock (SessioneTP.SyncRoot())
            SessioneTP("CampoProgressBar") = 101
            SessioneTP("CampoErrori") = ""
        End SyncLock

        Cache("CampoProgressBar" + Session.SessionID) = 101
        Cache("CampoErrori" + Session.SessionID) = ""


        REM Response.Redirect("EmissioneRette.aspx?EMETTI=PROVA")

        If XS.StringaDegliErrori = "" Then
            Call EseguiEm(data)
        End If

        Mnotify.ErroriCalcolo = XS.StringaDegliErrori

        Mnotify.ErroriEmissione = Cache("EmissioneCampoErrori" + Session.SessionID)
        Mnotify.ErroriWarning = Cache("CampoWr" + Session.SessionID)
        Mnotify.ErroriStampa = System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID)
        Mnotify.Scrivi(ConnessioneOspiti)


        'Mnotify.API_KEY = Session("API_KEY")
        'Mnotify.subscriptionId = Session("subscriptionId")

        SendNotifica(Mnotify.API_KEY, Session("GCM_ENDPOINT"), Mnotify.subscriptionId)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\..\Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()


        ConessioniOspiti = Session("DC_OSPITE")
        ConnessioneOspiti = Session("DC_OSPITE")
        ConnessioneTabelle = Session("DC_TABELLE")
        ConnessioneGenerale = Session("DC_GENERALE")
        Application(CampoProgressBar) = ""
        Application(CampoProva) = ""

        Session("CampoProgressBar") = ""
        Session("CODICESERVIZIO") = ""
        Session("CODICEOSPITE") = 0

        Cache("CampoProgressBar" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))


        Dim NumeroRegistrazioniIni As Integer = 0
        Dim NumeroRegistrazioniFin As Integer = 0
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()



        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select count(*) as NumeroRegistrazioni From Temp_MovimentiContabiliTesta "
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            NumeroRegistrazioniIni = campodbN(myPOSTreader.Item("NumeroRegistrazioni"))
        End If
        myPOSTreader.Close()


        Threading.Thread.Sleep(1500)


        Dim cmdV As New OleDbCommand()
        cmdV.CommandText = "Select count(*) as NumeroRegistrazioni From Temp_MovimentiContabiliTesta"
        cmdV.Connection = cn
        Dim ReadV1 As OleDbDataReader = cmdV.ExecuteReader()
        If ReadV1.Read Then
            NumeroRegistrazioniFin = campodbN(ReadV1.Item("NumeroRegistrazioni"))
        End If
        ReadV1.Close()
        cn.Close()

        If NumeroRegistrazioniFin <> NumeroRegistrazioniIni Then
            Response.Redirect("../Menu_Ospiti.aspx")
            Exit Sub
        End If

        Dim fc As New Cls_Parametri

        fc.LeggiParametri(Session("DC_OSPITE"))

        If f.CodiceOspite <> fc.CodiceOspite Then
            Response.Redirect("../Menu_Ospiti.aspx")
            Exit Sub
        End If


        Txt_DataP.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataA.Text = Format(Now, "dd/MM/yyyy")


        Call CaricaTabellaRegistri()


        Dim ConnectionString As String = Session("DC_OSPITE")


        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione


        Dim Notifia As New Cls_Notifica


        Notifia.Delete(Session("DC_OSPITE"))

    End Sub

    Protected Sub EseguiEm(ByRef data As Object)
        If Not IsDate(Txt_DataP.Text) Then

            Exit Sub
        End If
        If Not IsDate(Txt_DataA.Text) Then
            Txt_DataA.Text = Txt_DataP.Text
        End If

        DataP = Txt_DataP.Text
        DataA = Txt_DataA.Text

        If IsDate(Txt_DataEnte.Text) Then
            DataEnte = Txt_DataEnte.Text
        Else
            DataEnte = Nothing
        End If

        If IsDate(Txt_ScadenzaEnte.Text) Then
            ScadenzaEnte = Txt_ScadenzaEnte.Text
        Else
            ScadenzaEnte = Nothing
        End If



        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(Session("DC_OSPITE"))

        SyncLock (Session.SyncRoot())
            Session("CampoProva") = ""
            Session("CampoProva") = "P"

            Session("CampoProgressBar") = "Inizio elaborazione..."
            Session("EmissioneMese") = Dd_Mese.SelectedValue
            Session("EmissioneAnno") = Txt_Anno.Text
        End SyncLock

        Cache("CampoProva" + Session.SessionID) = "P"
        Cache("CampoProgressBar" + Session.SessionID) = "Inizio elaborazione..."
        Cache("EmissioneMese" + Session.SessionID) = Dd_Mese.SelectedValue
        Cache("EmissioneAnno" + Session.SessionID) = Txt_Anno.Text



        Call EmissioneR(DataP, DataA, data, DataEnte, ScadenzaEnte)


        Cache("EmissioneCampoErrori" + Session.SessionID) = Cache("CampoErrori" + Session.SessionID)
        Cache("CampoErrori" + Session.SessionID) = ""

        Dim k As New Cls_StampaFatture
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data
        TabRegistri = SessioneTP("TabRegistri")

        Dim Indice As Integer



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        For Indice = 0 To TabRegistri.Rows.Count - 1


            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From Temp_MovimentiContabiliTesta Where  RegistroIVA = ?"
            cmd.Parameters.AddWithValue("@RegistroIVA", Val(TabRegistri.Rows(Indice).Item(0)))
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

            If myPOSTreader.Read Then


                k.CreaRecordStampa(0, 0, False, 0, 0, 0, Val(TabRegistri.Rows(Indice).Item(0)), "", "", "", 1, data, 0, 99999999)

                Session("stampa") = System.Web.HttpRuntime.Cache("stampa" + Session.SessionID)

                Dim XS As New Cls_Login

                Dim rpt As New ReportDocument

                XS.Utente = Session("UTENTE")
                XS.LeggiSP(Application("SENIOR"))

                If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") = "" Then
                    Exit Sub
                End If

                Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1")



                Try
                    rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1"))
                Catch ex As Exception
                End Try

                Try
                    rpt.SetDataSource(Session("stampa"))
                Catch ex As Exception

                End Try


                rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & "temp\R_" & XS.Cliente & "_R_" & Val(TabRegistri.Rows(Indice).Item(0)) & ".pdf")

            End If
        Next

    End Sub



    Public Sub EmissioneR(ByVal DataPr As Date, ByVal DataAn As Date, ByRef data As Object, ByVal DataEnte As Date, ByVal ScadenzaEnte As Date)
        Dim MyRs As New ADODB.Recordset
        Dim DocumentoAl As Long, DocumentoDal As Long

        Dim Mese As Integer
        Dim InizioVerificaRegistrazione As Long
        Dim BeginNumeroRegistraione As Long
        Dim EmC As New Cls_EmissioneRetta

        Dim StringaErrore As String
        Dim StringaWarning As String
        Dim MeseContr As Integer
        Dim Anno As Integer

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data

        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(ConnessioneOspiti)

        Mese = LeggiParametri.MeseFatturazione
        MeseContr = LeggiParametri.MeseFatturazione
        Anno = LeggiParametri.AnnoFatturazione
        EmC.ConnessioneGenerale = ConnessioneGenerale
        EmC.ConnessioneOspiti = ConnessioneOspiti
        EmC.ConnessioneTabelle = ConnessioneTabelle

        EmC.ApriDB()

        Session("CampoProva") = "P"
        EmC.EliminaTemporanei()
        EmC.EmissioneDiProva = "P"


        TabRegistri = SessioneTP("TabRegistri")

        Dim Indice As Integer



        For Indice = 0 To TabRegistri.Rows.Count - 1


            EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = Val(TabRegistri.Rows(Indice).Item(2))

            If Val(TabRegistri.Rows(Indice).Item(3)) > 0 Then
                EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = Val(TabRegistri.Rows(Indice).Item(3))
            End If
        Next


        InizioVerificaRegistrazione = EmC.MaxRegistrazione()

        BeginNumeroRegistraione = EmC.MaxRegistrazione()


        StringaErrore = ""
        StringaWarning = ""

        SessioneTP("CampoProgressBar") = "Elaborazione Comune..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Comune..."
        If LeggiParametri.EmissioneComuni = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaComune(MeseContr, Anno, "", DataPr, DataAn, data)
                Else
                    Call EmC.CreaRettaComune(MeseContr, Anno, "", DataAn, DataAn, data)
                End If
            Else
                Call EmC.CreaRettaComune(MeseContr, Anno, "", DataEnte, DataAn, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Regione..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Regione..."
        If LeggiParametri.EmissioneRegione = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaRegione(MeseContr, Anno, "", DataPr, data)
                Else
                    Call EmC.CreaRettaRegione(MeseContr, Anno, "", DataAn, data)
                End If
            Else
                Call EmC.CreaRettaRegione(MeseContr, Anno, "", DataEnte, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Regione Raggruppata..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Regione Raggruppata..."
        If LeggiParametri.EmissioneRegione = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, "", DataPr, data)
                Else
                    Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, "", DataAn, data)
                End If
            Else
                Call EmC.CreaRettaRegioneRaggruppata(MeseContr, Anno, "", DataEnte, data)
            End If
        End If
        SessioneTP("CampoProgressBar") = "Elaborazione Jolly..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Jolly..."
        If LeggiParametri.EmissioneJolly = 0 Then
            If Year(DataEnte) < 1900 Then
                If IsDate(DataPr) Then
                    Call EmC.CreaRettaJolly(MeseContr, Anno, "", DataPr, DataPr, data)
                Else
                    Call EmC.CreaRettaJolly(MeseContr, Anno, "", DataPr, DataPr, data)
                End If
            Else
                Call EmC.CreaRettaJolly(MeseContr, Anno, "", DataPr, DataPr, data)
            End If
        End If


        SessioneTP("CampoProgressBar") = "Crea punto ordinamento..."
        SessioneTP("RagioneSocialeFattura") = ""

        Cache("CampoProgressBar" + Session.SessionID) = "Crea punto ordinamento..."
        Cache("RagioneSocialeFattura" + Session.SessionID) = ""

        For Indice = 0 To TabRegistri.Rows.Count - 1
            EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataPr), Val(TabRegistri.Rows(Indice).Item(0)))
            EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) = EmC.CalcolaProgressivo(Year(DataAn), Val(TabRegistri.Rows(Indice).Item(0)))
        Next

        SessioneTP("CampoProgressBar") = "Elaborazione Ospiti..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Ospiti..."

        If LeggiParametri.EmissioneOspite = 0 Then
            If IsDate(DataPr) Then
                Call EmC.CreaRettaOspite(MeseContr, Anno, "", DataAn, DataPr, data)
            Else
                Call EmC.CreaRettaOspite(MeseContr, Anno, "", DataAn, DataPr, data)
            End If
        End If

        SessioneTP("CampoProgressBar") = "Elaborazione Parenti..."
        Cache("CampoProgressBar" + Session.SessionID) = "Elaborazione Parenti..."
        If LeggiParametri.EmissioneParente = 0 Then
            If IsDate(DataPr) Then
                Call EmC.CreaRettaParente(MeseContr, Anno, "", DataAn, DataPr, data)
            Else
                Call EmC.CreaRettaParente(MeseContr, Anno, "", DataAn, DataPr, data)
            End If
        End If

        If EmC.ComuniIntestatiOspiti() Then
            EmC.ModificaFatturaComuniIntestatario()
        End If

        Call EmC.AssemblaFattureOspitiParenti()

        If LeggiParametri.RaggruppaRicavi = 1 Then
            Call EmC.RaggruppaContiRicavo()
        End If



        SessioneTP("CampoProgressBar") = "Ordina Protocolli..."
        SessioneTP("RagioneSocialeFattura") = ""

        Cache("CampoProgressBar" + Session.SessionID) = "Ordina Protocolli..."
        Cache("RagioneSocialeFattura" + Session.SessionID) = ""

        For Indice = 0 To TabRegistri.Rows.Count - 1
            DocumentoAl = EmC.CalcolaProgressivo(Year(DataAn), Val(TabRegistri.Rows(Indice).Item(0)))
            DocumentoDal = EmC.UltimoNumeroProtocollo(Year(DataAn) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) + 1
            If DocumentoAl > DocumentoDal Then
                Call EmC.RiordinaFatture(DocumentoDal, DocumentoAl, Val(TabRegistri.Rows(Indice).Item(0)), Anno, Mese, EmC.StringaErrore, Year(DataA), ConnessioneGenerale, ConnessioneOspiti)
            End If

            If Format(DataAn, "ddMMyyyy") <> Format(DataPr, "ddMMyyyy") Then
                DocumentoAl = EmC.CalcolaProgressivo(Year(DataPr), Val(TabRegistri.Rows(Indice).Item(0)))
                DocumentoDal = EmC.UltimoNumeroProtocollo(Year(DataPr) - 2000, Val(TabRegistri.Rows(Indice).Item(0))) + 1
                If DocumentoAl > DocumentoDal Then
                    Call EmC.RiordinaFatture(DocumentoDal, DocumentoAl, Val(TabRegistri.Rows(Indice).Item(0)), Anno, Mese, EmC.StringaErrore, Year(DataPr), ConnessioneGenerale, ConnessioneOspiti)
                End If
            End If
        Next


        If Txt_DataScadenza.Text <> "" Then
            SessioneTP("CampoProgressBar") = "Inserisci Scadenze Fatture..."
            Cache("CampoProgressBar" + Session.SessionID) = "Inserisci Scadenze Fatture..."

            Call EmC.ScadenzaFatture(Txt_DataScadenza.Text, ConnessioneGenerale, ScadenzaEnte, SessioneTP("UTENTE"))
        End If


        SessioneTP("CampoProgressBar") = "Termine operazione..."

        SessioneTP("RegistrazioneDal") = EmC.BeginNumeroRegistraione
        SessioneTP("RegistrazioneAl") = EmC.MaxRegistrazione()


        Cache("CampoProgressBar" + Session.SessionID) = "Termine operazione..."
        Cache("RegistrazioneDal" + Session.SessionID) = EmC.BeginNumeroRegistraione
        Cache("RegistrazioneAl" + Session.SessionID) = EmC.MaxRegistrazione()


        EmC.CloseDB()


        SessioneTP("CampoErrori") = EmC.StringaErrore
        SessioneTP("CampoWr") = EmC.StringaWarning
        SessioneTP("CampoProgressBar") = "FINE"

        If IsNothing(EmC.StringaErrore) Then
            EmC.StringaErrore = ""
        End If
        If IsNothing(EmC.StringaWarning) Then
            EmC.StringaWarning = ""
        End If
        Cache("CampoErrori" + Session.SessionID) = EmC.StringaErrore
        Cache("CampoWr" + Session.SessionID) = EmC.StringaWarning
        Cache("CampoProgressBar" + Session.SessionID) = "FINE"

    End Sub







    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        SyncLock (Session.SyncRoot())
            Session("CampoErrori") = ""
            Session("NomeOspite") = ""
            Session("NomeOspite1") = ""
            Session("NomeOspite2") = ""
            Session("LastOspite") = ""
            Session("CampoProgressBar") = 0
        End SyncLock

        'DoWork(Session)

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)

        Response.Redirect("CalcoloStampaFattureInfo.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Ospiti.aspx")
    End Sub

    Protected Sub Img_TestNotifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_TestNotifica.Click
        SendNotifica(Session("API_KEY"), Session("GCM_ENDPOINT"), Session("subscriptionId"))
    End Sub



    Protected Sub Grd_Registri_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grd_Registri.RowCancelingEdit
        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Registri.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblProtocollo1 As Label = DirectCast(e.Row.FindControl("LblProtocollo1"), Label)
            If Not IsNothing(LblProtocollo1) Then

            Else
                Dim TxtProtocollo1 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo1"), TextBox)
                TxtProtocollo1.Text = TabRegistri.Rows(e.Row.RowIndex).Item(2).ToString
            End If


            Dim LblProtocollo2 As Label = DirectCast(e.Row.FindControl("LblProtocollo2"), Label)
            If Not IsNothing(LblProtocollo2) Then

            Else
                Dim TxtProtocollo2 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo2"), TextBox)
                TxtProtocollo2.Text = TabRegistri.Rows(e.Row.RowIndex).Item(3).ToString
            End If
        End If
    End Sub
    Protected Sub Grd_Registri_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grd_Registri.RowEditing
        Grd_Registri.EditIndex = e.NewEditIndex
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grd_Registri.RowUpdated

    End Sub

    Protected Sub Grd_Registri_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grd_Registri.RowUpdating
        Dim TxtProtcollo1 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo1"), TextBox)
        Dim TxtProtcollo2 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo2"), TextBox)


        TabRegistri = Session("TabRegistri")
        Dim row = Grd_Registri.Rows(e.RowIndex)

        TabRegistri.Rows(row.DataItemIndex).Item(2) = TxtProtcollo1.Text
        TabRegistri.Rows(row.DataItemIndex).Item(3) = TxtProtcollo2.Text



        Session("TabRegistri") = TabRegistri

        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
