﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class Comunicazioni_DocumentoOneri
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Oneri As New Cls_Oneri

        Oneri.UpDropDownList(Session("DC_OSPITE"), DD_Oneri)

    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            Dim NomeFile As String

            NomeFile = ImportFileXLS(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Private Function ImportFileXLS(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim nomefileexcel As String
        Dim HTMLDocumenti As String = ""

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Txt_NonTrovati.Visible = True


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
                Exit Function
            End If
        Else
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File excel non trovata');", True)
                Exit Function
            End If
        End If



        'cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then

            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")
        Else
            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        End If



        Dim Oldcodicefiscale As String = ""

        Dim OldNome As String = ""
        Dim Olddatadocumento As String = ""
        Dim Oldnumerodocumento As String = ""
        Dim Olddatapagamento As String = ""
        Dim OldOperazione As String = ""
        Dim Oldimporto As String = ""
        Dim OldQuotaSanitaria As String = ""
        Dim OldNomeOspite As String = ""
        Dim OldCodiceFiscaleOspite As String = ""
        Dim OldClasse As String = ""
        Dim OldTipologia As String = ""
        Dim OldDataAccoglimento As String = ""
        Dim OldDataUscitaDefinitiva As String = ""
        Dim OldQuota As String = ""
        Dim TotaleOneri As Double = 0



        Dim GrigliaTab As String = ""
        GrigliaTab = "<table>"
        GrigliaTab = GrigliaTab & "<tr>"
        GrigliaTab = GrigliaTab & "<td>Data Documento</td>"
        GrigliaTab = GrigliaTab & "<td>Numero Documento</td>"
        GrigliaTab = GrigliaTab & "<td>Data Pagamento</td>"
        GrigliaTab = GrigliaTab & "<td>Tipo Operazione</td>"
        GrigliaTab = GrigliaTab & "<td>Importo Inc/Pag</td>"


        cn.Open()
        ''Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}\\;Extended Properties='Text;HDR=Yes;FMT=CSVDelimited(;)
        'Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=" & Txt_Rsl.text & ";"

        Dim cmd As New OdbcCommand()

        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")

            'CsvImport" & Appoggio & ".csv"
        Else
            cmd.CommandText = ("select * from [FOGLIO$]")
        End If

        cmd.Connection = cn


        Dim myPOSTreader As OdbcDataReader

        Try
            myPOSTreader = cmd.ExecuteReader()
        Catch ex As Exception
            cn.Close()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file');", True)
            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
            Catch elimino As Exception

            End Try
            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
            Catch elimino As Exception

            End Try

            Exit Function
        End Try

        Do While myPOSTreader.Read
            Dim Nome As String = ""
            Dim codicefiscale As String = ""
            Dim datadocumento As String = ""
            Dim numerodocumento As String = ""
            Dim datapagamento As String = ""
            Dim Operazione As String = ""
            Dim importo As String = ""
            Dim QuotaSanitaria As String = ""
            Dim NomeOspite As String = ""
            Dim CodiceFiscaleOspite As String = ""
            Dim Classe As String = ""
            Dim Tipologia As String = ""
            Dim DataAccoglimento As String = ""
            Dim DataUscitaDefinitiva As String = ""
            Dim Quota As String = ""

            If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                Dim Vettore(100) As String
                Dim CampoInput As String

                CampoInput = myPOSTreader.Item(0)
                Try
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                Catch ex As Exception

                End Try

                Vettore = SplitWords(CampoInput)
                Try
                    Nome = Vettore(0)
                    codicefiscale = Vettore(1)
                    datadocumento = Vettore(2)
                    numerodocumento = Vettore(3)
                    datapagamento = Vettore(4)
                    Operazione = Vettore(5)
                    importo = Vettore(6)
                    QuotaSanitaria = Vettore(7)
                    NomeOspite = Vettore(8)
                    CodiceFiscaleOspite = Vettore(9)
                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                    Exit Function
                End Try
                Try
                    Classe = Vettore(10)
                    Tipologia = Vettore(11)
                    DataAccoglimento = Vettore(12)
                    DataUscitaDefinitiva = Vettore(13)
                    Quota = Vettore(14)
                Catch ex As Exception

                End Try
            Else

                Try
                    Nome = campodb(myPOSTreader.Item("Nome")).Trim

                    codicefiscale = campodb(myPOSTreader.Item("codicefiscale"))

                    datadocumento = campodb(myPOSTreader.Item("datadocumento"))
                    numerodocumento = campodb(myPOSTreader.Item("numerodocumento"))
                    datapagamento = campodb(myPOSTreader.Item("datapagamento"))

                    Operazione = campodb(myPOSTreader.Item("Operazione"))

                    importo = campodb(myPOSTreader.Item("importo"))

                    QuotaSanitaria = campodb(myPOSTreader.Item("QuotaSanitaria"))

                    CodiceFiscaleOspite = campodb(myPOSTreader.Item("CodiceFiscaleOspite"))


                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                    Exit Function

                End Try

                Try
                    Classe = campodb(myPOSTreader.Item("Classe"))
                    Tipologia = campodb(myPOSTreader.Item("Tipologia"))
                    DataAccoglimento = campodb(myPOSTreader.Item("DataAccoglimento"))
                    DataUscitaDefinitiva = campodb(myPOSTreader.Item("DataUscitaDefinitiva"))
                    Quota = campodb(myPOSTreader.Item("Quota"))
                Catch ex As Exception

                End Try

            End If

            If Oldcodicefiscale <> codicefiscale And Oldcodicefiscale <> "" Then
                GrigliaTab = GrigliaTab & "</table>"
                ExportPDF(HTMLDocumenti, OldNome, Oldcodicefiscale, Olddatadocumento, Oldnumerodocumento, Olddatapagamento, OldOperazione, Oldimporto, OldQuotaSanitaria, OldNomeOspite, OldCodiceFiscaleOspite, OldClasse, OldTipologia, OldDataAccoglimento, OldDataUscitaDefinitiva, OldQuota, GrigliaTab, TotaleOneri)

                GrigliaTab = "<table>"
                GrigliaTab = GrigliaTab & "<tr>"
                GrigliaTab = GrigliaTab & "<td>Data Documento</td>"
                GrigliaTab = GrigliaTab & "<td>Numero Documento</td>"
                GrigliaTab = GrigliaTab & "<td>Data Pagamento</td>"
                GrigliaTab = GrigliaTab & "<td>Tipo Operazione</td>"
                GrigliaTab = GrigliaTab & "<td>Importo Inc/Pag</td>"

                GrigliaTab = GrigliaTab & "</tr>"
                GrigliaTab = GrigliaTab & "<tr>"
                GrigliaTab = GrigliaTab & "<td>" & datadocumento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & numerodocumento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & datapagamento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & Operazione & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & importo & "</td>"


                GrigliaTab = GrigliaTab & "</tr>"
                Oldcodicefiscale = codicefiscale
                OldNome = Nome
                Olddatadocumento = datadocumento
                Oldnumerodocumento = numerodocumento
                Olddatapagamento = datapagamento
                OldOperazione = Operazione
                Oldimporto = importo
                OldQuotaSanitaria = QuotaSanitaria
                OldNomeOspite = NomeOspite
                OldCodiceFiscaleOspite = CodiceFiscaleOspite
                OldClasse = Classe
                OldTipologia = Tipologia
                OldDataAccoglimento = DataAccoglimento
                OldDataUscitaDefinitiva = DataUscitaDefinitiva
                OldQuota = Quota

                If Operazione = "FT" Then
                    TotaleOneri = CDbl(importo)
                Else
                    TotaleOneri = CDbl(importo) * -1
                End If

            Else

                GrigliaTab = GrigliaTab & "<tr>"
                GrigliaTab = GrigliaTab & "<td>" & datadocumento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & numerodocumento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & datapagamento & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & Operazione & "</td>"
                GrigliaTab = GrigliaTab & "<td>" & importo & "</td>"


                GrigliaTab = GrigliaTab & "</tr>"

                Oldcodicefiscale = codicefiscale

                OldNome = Nome
                Olddatadocumento = datadocumento
                Oldnumerodocumento = numerodocumento
                Olddatapagamento = datapagamento
                OldOperazione = Operazione
                Oldimporto = importo
                OldQuotaSanitaria = QuotaSanitaria
                OldNomeOspite = NomeOspite
                OldCodiceFiscaleOspite = CodiceFiscaleOspite
                OldClasse = Classe
                OldTipologia = Tipologia
                OldDataAccoglimento = DataAccoglimento
                OldDataUscitaDefinitiva = DataUscitaDefinitiva
                OldQuota = Quota


                If Operazione = "FT" Then
                    TotaleOneri = TotaleOneri + CDbl(importo)
                Else
                    TotaleOneri = TotaleOneri + CDbl(importo)
                End If
            End If
        Loop


        '

        'Session("PdfDaHTML") = contenuto


        If Rb_WORD.Checked = True Then

            Dim strBody As New StringBuilder()

            strBody.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office' " & _
               "xmlns:w='urn:schemas-microsoft-com:office:word'" & _
               "xmlns='http://www.w3.org/TR/REC-html40'>" & _
               "<head><title>Time</title>")

            strBody.Append("<!--[if gte mso 9]>" & _
                "<xml>" & _
                "<w:WordDocument>" & _
                "<w:View>Print</w:View>" & _
                "<w:Zoom>90</w:Zoom>" & _
                "<w:DoNotOptimizeForBrowser/>" & _
                "</w:WordDocument>" & _
                "</xml>" & _
                "<![endif]-->")

            strBody.Append("<style>" & _
                "<!-- /* Style Definitions */" & _
                "@page Section1" & _
                "   {size:8.5in 11.0in; " & _
                "   margin:1.0in 1.25in 1.0in 1.25in ; " & _
                "   mso-header-margin:.5in; " & _
                "   mso-footer-margin:.5in; mso-paper-source:0;}" & _
                " div.Section1" & _
                "   {page:Section1;}" & _
                "-->" & _
                "</style></head>")

            strBody.Append("<body lang=EN-US style='tab-interval:.5in'><div class=Section1>")

            strBody.Append(HTMLDocumenti)

            strBody.Append("</div></body></html>")


            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Charset = ""
            HttpContext.Current.Response.ContentType = "application/msword"
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=Contratto.doc")

            HttpContext.Current.Response.Write(strBody)
            HttpContext.Current.Response.End()
            HttpContext.Current.Response.Flush()
        Else

            Dim Contenuto As String = HTMLDocumenti

            HTMLDocumenti = Server.UrlEncode(HTMLDocumenti.Replace(vbNewLine, ""))

            Session("PdfDaHTML") = HTMLDocumenti
            Response.Redirect("PdfDaHTml.aspx")
        End If

    End Function



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function


    Private Sub ExportPDF(ByRef HTML As String, ByVal Nome As String, ByVal codicefiscale As String, ByVal datadocumento As String, ByVal numerodocumento As String, ByVal datapagamento As String, ByVal Operazione As String, ByVal importo As String, ByVal QuotaSanitaria As String, ByVal NomeOspite As String, ByVal CodiceFiscaleOspite As String, ByVal Classe As String, ByVal Tipologia As String, ByVal DataAccoglimento As String, ByVal DataUscitaDefinitiva As String, ByVal Quota As String, ByVal GrigliaTab As String, ByVal TotaleOneri As Double)

        Dim TabParametri As New Cls_Parametri

        TabParametri.LeggiParametri(Session("DC_OSPITE"))

        Dim Appo As New Cls_Oneri


        Appo.Id = Val(DD_Oneri.SelectedValue)

        Appo.Leggi(Session("DC_OSPITE"))


        ' Leggo il contenuto del file sino alla fine (ReadToEnd)
        Dim contenuto As String = Appo.Testo


        'contenuto = contenuto.Replace("@COMUNEPARENTE2@", ApoC1.Descrizione)
        contenuto = contenuto.Replace("@COGNOMENOME@", Nome)
        contenuto = contenuto.Replace("@CODICEFISCALE@", codicefiscale)


        Dim Ospite As New ClsOspite

        Ospite.Nome = ""
        Ospite.CODICEFISCALE = codicefiscale
        Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospite.CODICEFISCALE)

        If Ospite.Nome <> "" Then

            contenuto = contenuto.Replace("@DATANASCITA@", Format(Ospite.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Ospite.ProvinciaDiNascita
            Comune.Comune = Ospite.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITA@", Comune.Descrizione)


            Dim ComuneRes As New ClsComune

            ComuneRes.Provincia = Ospite.RESIDENZAPROVINCIA1
            ComuneRes.Comune = Ospite.RESIDENZACOMUNE1
            ComuneRes.Leggi(Session("DC_OSPITE"))



            contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", Ospite.RESIDENZAINDIRIZZO1)
            contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ComuneRes.Descrizione)
            contenuto = contenuto.Replace("@INTESTATARIOCAP@", Ospite.RESIDENZACAP1)


            '@INTESTATARIOINDIRIZZO@

        Else
            Dim Parente As New Cls_Parenti

            Parente.Nome = ""
            Parente.CODICEFISCALE = codicefiscale
            Parente.LeggiPerCodiceFiscaleSenzaOspite(Session("DC_OSPITE"), Parente.CODICEFISCALE)



            contenuto = contenuto.Replace("@DATANASCITA@", Format(Parente.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Parente.ProvinciaDiNascita
            Comune.Comune = Parente.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITA@", Comune.Descrizione)



            Dim ComuneRes As New ClsComune

            ComuneRes.Provincia = Parente.RESIDENZAPROVINCIA1
            ComuneRes.Comune = Parente.RESIDENZACOMUNE1
            ComuneRes.Leggi(Session("DC_OSPITE"))



            contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", Parente.RESIDENZAINDIRIZZO1)
            contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ComuneRes.Descrizione)
            contenuto = contenuto.Replace("@INTESTATARIOCAP@", Parente.RESIDENZACAP1)


        End If


        contenuto = contenuto.Replace("@DATADOCUMENTO@", datadocumento)
        contenuto = contenuto.Replace("@NUMERODOCUMENTO@", numerodocumento)
        contenuto = contenuto.Replace("@DATAPAGAMENTO@", datapagamento)
        contenuto = contenuto.Replace("@OPERAZIONE@", Operazione)
        contenuto = contenuto.Replace("@IMPORTO@", importo)
        contenuto = contenuto.Replace("@QUOTASANITARIA@", QuotaSanitaria)
        contenuto = contenuto.Replace("@NOMEOSPITE@", NomeOspite)
        contenuto = contenuto.Replace("@CODICEFISCALEOSPITE@", CodiceFiscaleOspite)
        contenuto = contenuto.Replace("@CLASSE@", Classe)
        contenuto = contenuto.Replace("@TIPOLOGIA@", Tipologia)
        contenuto = contenuto.Replace("@DATAACCOGLIMENTO@", DataAccoglimento)
        contenuto = contenuto.Replace("@DATAUSCITADEFINITIVA@", DataUscitaDefinitiva)
        contenuto = contenuto.Replace("@QUOTA@", Quota)
        contenuto = contenuto.Replace("@TABELLADOCUMENTI@", GrigliaTab)

        contenuto = contenuto.Replace("@TOTALEONERI@", Format(TotaleOneri, "#,##0.00"))



        Ospite.Nome = ""
        Ospite.CODICEFISCALE = CodiceFiscaleOspite
        Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospite.CODICEFISCALE)

        If Ospite.Nome <> "" Then

            contenuto = contenuto.Replace("@DATANASCITAOSPITE@", Format(Ospite.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Ospite.ProvinciaDiNascita
            Comune.Comune = Ospite.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITAOSPITE@", Comune.Descrizione)
        End If


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        If Rb_WORD.Checked = True Then
            HTML = HTML & contenuto & "<br clear=all style=""mso-special-character:page-break;page-break-before:always"">"
        Else
            HTML = HTML & contenuto & "<div style=""page-break-before: always;""></div>"
        End If

    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, ";")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Comunicazioni.aspx")

    End Sub
End Class

