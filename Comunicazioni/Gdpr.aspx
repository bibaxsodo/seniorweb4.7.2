﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Comunicazioni_Gdpr" CodeFile="Gdpr.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Informativa</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Comunicazioni - Informativa sulla Privacy</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Elenco_GDPR.aspx">
                            <img src="../images/Menu_Indietro.png" alt="Menù" title="Menù" /></a>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Informativa" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Id :</label>
                                    <asp:TextBox ID="Txt_ID" onkeypress="return handleEnter(this, event)" Enabled="false" Width="70px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Descrizione :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Data Scadenza :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Data" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <table width="90%">
                                        <tr>
                                            <td width="60%">Testo</td>
                                            <td width="30%">Variabili</td>
                                        </tr>
                                        <tr>
                                            <td width="70%">
                                                <cc2:Editor
                                                    ID="Txt_Testo"
                                                    Width="850px"
                                                    Height="400px"
                                                    runat="server" />
                                            </td>
                                            <td width="30%">
                                                <div id="idRegola" style="width: 100%; height: 380px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                    @NOMEOSPITE@ <i style="font-size: x-small;">nome dell'ospite</i><br />
                                                    @NOMETITOLARE@ <i style="font-size: x-small;">Nome Titolare</i><br />
                                                    @INDIRIZZO@ <i style="font-size: x-small;">Indirizzo Parente/Ospite</i><br />
                                                    @CAP@ <i style="font-size: x-small;">CAP Parente/Ospite</i><br />
                                                    @CITTA@ <i style="font-size: x-small;">Città Parente/Ospite</i><br />
                                                    @TEL@ <i style="font-size: x-small;">Telefono Parente/Ospite</i><br />
                                                    @EMAIL@ <i style="font-size: x-small;">E-Mail Parente/Ospite</i><br />
                                                    @NOMEPARENTE@ <i style="font-size: x-small;">Nome Parente</i><br />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </AJAX:TabPanel>

                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
