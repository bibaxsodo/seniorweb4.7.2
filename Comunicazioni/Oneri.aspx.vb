﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO
Partial Class Comunicazioni_Oneri
    Inherits System.Web.UI.Page

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Comune')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/autocompletecomune.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Cap')!= null) {  "
        MyJs = MyJs & " $(els[i]).focus(function() { if ($(els[i]).val() == '') {  __doPostBack(""Btn_RefreshPostali"", ""0""); } }); "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtConto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)



        If Val(Request.Item("ID")) > 0 Then
            Txt_ID.Text = Val(Request.Item("ID"))
            LeggiContratto()
        End If

        Call EseguiJS()
    End Sub

    Private Sub LeggiContratto()

        Dim k As New Cls_Oneri

        k.Id = Val(Txt_ID.Text)
        k.Leggi(Session("DC_OSPITE"))

        Txt_Descrizione.Text = k.Descrizione

        If Year(k.DataScadenza) < 1000 Then
            Txt_Data.Text = ""
        Else
            Txt_Data.Text = Format(k.DataScadenza, "dd/MM/yyyy")
        End If
        Txt_Testo.Content = k.Testo

    End Sub


    Private Sub ScriviContratto()
        Dim k As New Cls_Oneri

        k.Id = Val(Txt_ID.Text)

        k.Descrizione = Txt_Descrizione.Text
        k.DataScadenza = IIf(Txt_Data.Text = "", Nothing, Txt_Data.Text)
        k.Testo = Txt_Testo.Content

        k.Scrivi(Session("DC_OSPITE"))



        Txt_ID.Text = 0


        Txt_Descrizione.Text = ""
        Txt_Data.Text = ""
        Txt_Testo.Content = ""

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Descrizione</center>');", True)
            Exit Sub
        End If
        'If Not IsDate(Txt_Data.Text) Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Data Scadenza</center>');", True)
        '    Exit Sub
        'End If


        ScriviContratto()
        Response.Redirect("Elenco_Oneri.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim k As New Cls_Oneri

        k.Id = Val(Txt_ID.Text)

        k.Delete(Session("DC_OSPITE"))

        Response.Redirect("Elenco_Oneri.aspx")
    End Sub
End Class
