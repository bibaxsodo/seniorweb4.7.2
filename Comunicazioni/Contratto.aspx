﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Contratto" CodeFile="Contratto.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Contratto</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Comunicazioni - Contratto</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Elenco_contratti.aspx">
                            <img src="../images/Menu_Indietro.png" alt="Menù" title="Menù" /></a>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Contratto" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Id :</label>
                                    <asp:TextBox ID="Txt_ID" onkeypress="return handleEnter(this, event)" Enabled="false" Width="70px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Descrizione :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Data Scadenza :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Data" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <table width="90%">
                                        <tr>
                                            <td width="60%">Testo</td>
                                            <td width="30%">Variabili</td>
                                        </tr>
                                        <tr>
                                            <td width="70%">
                                                <cc2:Editor
                                                    ID="Txt_Testo"
                                                    Width="850px"
                                                    Height="400px"
                                                    runat="server" />
                                            </td>
                                            <td width="30%">
                                                <div id="idRegola" style="width: 100%; height: 380px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                    @COGNOMENOME@ <i style="font-size: x-small;">nome dell'ospite</i><br />
                                                    @CODICEFISCALE@ <i style="font-size: x-small;">codice fiscale dell'ospite</i><br />
                                                    @DATANASCITA@ <i style="font-size: x-small;">data nascita dell'ospite</i><br />
                                                    @INTESTATARIO@  <i style="font-size: x-small;">nome dell'intestatario  fattura</i><br />
                                                    @INTESTATARIOINDIRIZZO@ <i style="font-size: x-small;">comune dell'intestario fattura</i><br />
                                                    @INTESTATARIOCOMUNE@ <i style="font-size: x-small;">comune dell'intestario fattura</i><br />
                                                    @INTESTATARIOCAP@ <i style="font-size: x-small;">CAP dell'intestario fattura</i><br />
                                                    @MEDICO@ <i style="font-size: x-small;">nome medico</i><br />
                                                    @PARENTE1@ <i style="font-size: x-small;">nome del primo parente</i><br />
                                                    @PARENTE2@ <i style="font-size: x-small;">nome del secondo parente</i>
                                                    <br />
                                                    @TELEFONOPARENTE1@ <i style="font-size: x-small;">telefono del primo parente</i><br />
                                                    @TELEFONOPARENTE2@ <i style="font-size: x-small;">telefono del secondo parente</i><br />
                                                    @INDIRIZZOPARENTE1@ <i style="font-size: x-small;">indirizzo del primo parente</i><br />
                                                    @INDIRIZZOPARENTE2@ <i style="font-size: x-small;">indirizzo del secondo parente</i><br />
                                                    @COMUNEPARENTE1@ <i style="font-size: x-small;">comune del primo parente</i><br />
                                                    @COMUNEPARENTE2@ <i style="font-size: x-small;">comune del secondo parente</i><br />
                                                    @CAPPARENTE1@ <i style="font-size: x-small;">CAP del primo parente</i><br />
                                                    @CAPPARENTE2@ <i style="font-size: x-small;">CAP del secondo parente</i><br />
                                                    @NUMERODOCUMENTO@ <i style="font-size: x-small;">Numero documento ospite</i><br />
                                                    @TIPODOCUMENTO@ <i style="font-size: x-small;">Tipo Documento ospite</i><br />
                                                    @DATADOCUMENTO@ <i style="font-size: x-small;">Data Documento ospite</i><br />
                                                    @NCARTELLA@ <i style="font-size: x-small;">Numero Cartella</i><br />
                                                    @DATAACCOGLIMENTO@ <i style="font-size: x-small;">Data Accoglimento</i><br />
                                                    @TELEFONOOSPITE@ <i style="font-size: x-small;">Telefono Ospite</i><br />
                                                    @SESSO@ <i style="font-size: x-small;">Sesso</i><br />
                                                    @LETTO@ <i style="font-size: x-small;">Letto</i><br />
                                                    @STATO@ <i style="font-size: x-small;">Stato osptie</i><br />
                                                    @LIBERO1@ <i style="font-size: x-small;">Campo Libero 1</i><br />
                                                    @LIBERO2@ <i style="font-size: x-small;">Campo Libero 2</i><br />
                                                    @LIBERO3@ <i style="font-size: x-small;">Campo Libero 3</i><br />
                                                    @QUOTA@ <i style="font-size: x-small;">Retta+Extra</i><br />
                                                    @EPERSONAM-ROOM@ <i style="font-size: x-small;">Stanza da E-Personam</i><br />
                                                    @USL@ <i style="font-size: x-small;">Usl compartecipazione</i><br />
                                                    @DATAACCOGLIMENTO@ <i style="font-size: x-small;">Data accoglimento</i><br />
                                                    @DATANASCITA@ <i style="font-size: x-small;">Data Nascita</i><br />
                                                    @COMUNENASCITA@ <i style="font-size: x-small;">Comune Nascita</i><br />
                                                    @CENTROSERVIZIO@ <i style="font-size: x-small;">Centro Servizio</i><br />
                                                    @OSPITEINDIRIZZO@ <i style="font-size: x-small;">Indirizzo Ospite</i><br />
                                                    @OSPITECAP@ <i style="font-size: x-small;">Cap Ospite</i><br />
                                                    @OSPITECOMUNE@ <i style="font-size: x-small;">Comune Ospite</i><br />
                                                    @CODICEOSPITE@ <i style="font-size: x-small;">Codice Ospite</i><br />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </AJAX:TabPanel>

                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
