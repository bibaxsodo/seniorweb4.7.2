﻿Namespace WebHandler

    Public Class decadificapianodeiconti
        Implements System.Web.IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            Dim mastro As String = context.Request.QueryString("mastro")
            Dim conto As String = context.Request.QueryString("conto")
            Dim sottoconto As String = context.Request.QueryString("sottoconto")
            Dim Utente As String = context.Request.QueryString("UTENTE")
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

            If mastro = "" Then Exit Sub
            If conto = "" Then Exit Sub
            If sottoconto = "" Then Exit Sub

            Dim x As New Cls_Pianodeiconti


            Dim ConnectionString As String = context.Session("DC_GENERALE")

            x.Mastro = mastro
            x.Conto = conto
            x.Sottoconto = sottoconto

            x.Decodfica(ConnectionString)
            context.Response.Write(x.Descrizione)
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class

End Namespace
