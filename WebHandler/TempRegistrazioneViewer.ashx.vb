﻿Imports System.Data.OleDb

Namespace WebHandler

    Public Class TempRegistrazioneViewer
        Implements System.Web.IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            context.Response.ContentType = "text/plain"
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim NumeroRegistrazione As String = context.Request.QueryString("NumeroRegistrazione")

            Dim Tabella As String
            Dim I As Integer

            Tabella = "<table width=""100%"">"
            Tabella = Tabella & "<tr>"
            Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
            Tabella = Tabella & "</td>"
            Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Dare"
            Tabella = Tabella & "</td>"
            Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
            Tabella = Tabella & "</td>"
            Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Avere"
            Tabella = Tabella & "</td>"
            Tabella = Tabella & "</tr>"
            Dim DbC As New Cls_Login

            DbC.Utente = context.Session("UTENTE")
            DbC.LeggiSP(context.Application("SENIOR"))

            Dim cn As OleDbConnection

            Dim NumeroRighe As Integer = 0

            cn = New Data.OleDb.OleDbConnection(DbC.Generale)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from Temp_MovimentiContabiliRiga where " &
                               "Numero = ? Order by RigaDaCausale")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmd.Connection = cn
            Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
            Do While VerReader.Read

                If campodb(VerReader.Item("DareAvere")) = "A" Then
                    Tabella = Tabella & "<tr>"
                    Tabella = Tabella & "<td width=""40%"">"
                    Tabella = Tabella & "</td>"
                    Tabella = Tabella & "<td width=""10%"">"
                    Tabella = Tabella & "</td>"
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = campodbN(VerReader.Item("MastroPartita"))
                    PianoConti.Conto = campodbN(VerReader.Item("ContoPartita"))
                    PianoConti.Sottoconto = campodbN(VerReader.Item("SottocontoPartita"))
                    PianoConti.Decodfica(DbC.Generale)

                    Tabella = Tabella & "<td width=""40%"">" & PianoConti.Descrizione & "</td>"
                    Tabella = Tabella & "<td width=""10%"" style=""text-align: right;"">" & Format(campodbN(VerReader.Item("Importo")), "#,##0.00") & "</td>"
                    Tabella = Tabella & "</tr>"
                    Tabella = Tabella & "<tr>"
                    Tabella = Tabella & "<td width=""40%"">"
                    Tabella = Tabella & "</td>"
                    Tabella = Tabella & "<td width=""10%"">"
                    Tabella = Tabella & "</td>"

                    PianoConti.Mastro = campodbN(VerReader.Item("MastroControPartita"))
                    PianoConti.Conto = campodbN(VerReader.Item("ContoControPartita"))
                    PianoConti.Sottoconto = campodbN(VerReader.Item("SottocontoControPartita"))
                    PianoConti.Decodfica(DbC.Generale)

                    Tabella = Tabella & "<td width=""40%""><font size=""2"" color=""#cccccc;""><i>" & PianoConti.Descrizione & "</i></font></td>"
                    Tabella = Tabella & "<td width=""10%"" style=""text-align: right;""></td>"
                    Tabella = Tabella & "</tr>"
                    NumeroRighe = NumeroRighe + 1
                End If
                If campodb(VerReader.Item("DareAvere")) = "D" Then
                    Tabella = Tabella & "<tr>"

                    NumeroRighe = NumeroRighe + 1
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = campodbN(VerReader.Item("MastroPartita"))
                    PianoConti.Conto = campodbN(VerReader.Item("ContoPartita"))
                    PianoConti.Sottoconto = campodbN(VerReader.Item("SottocontoPartita"))
                    PianoConti.Decodfica(DbC.Generale)

                    Tabella = Tabella & "<td width=""40%"">" & PianoConti.Descrizione & "</td>"
                    Tabella = Tabella & "<td width=""10%"" style=""text-align: right;"">" & Format(campodbN(VerReader.Item("Importo")), "#,##0.00") & "</td>"
                    Tabella = Tabella & "<td width=""40%"">"
                    Tabella = Tabella & "</td>"
                    Tabella = Tabella & "<td width=""10%"">"
                    Tabella = Tabella & "</td >"
                    Tabella = Tabella & "</tr>"
                    Tabella = Tabella & "<tr>"

                    PianoConti.Mastro = campodbN(VerReader.Item("MastroControPartita"))
                    PianoConti.Conto = campodbN(VerReader.Item("ContoControPartita"))
                    PianoConti.Sottoconto = campodbN(VerReader.Item("SottocontoControPartita"))
                    PianoConti.Decodfica(DbC.Generale)

                    Tabella = Tabella & "<td width=""40%""><font size=""2"" color=""#cccccc;""><i>" & PianoConti.Descrizione & "</i></font></td>"
                    Tabella = Tabella & "<td width=""10%"" style=""text-align: right;""></td>"
                    Tabella = Tabella & "<td width=""40%"">"
                    Tabella = Tabella & "</td>"
                    Tabella = Tabella & "<td width=""10%"">"
                    Tabella = Tabella & "</td >"
                    Tabella = Tabella & "</tr>"
                End If
            Loop
            Tabella = Tabella & "</table>"
            Dim altezza As String
            altezza = (NumeroRighe * 50) + 30

            context.Response.Write("<div id=""RegistrazioneViewer""  style=""position: absolute; -moz-box-shadow: 8px 6px 5px 5px #000000;-webkit-box-shadow: 8px 6px 5px 5px #000000;box-shadow: 8px 6px 5px 5px #000000; filter: alpha(opacity=70);-moz-opacity: 0.7;-khtml-opacity: 0.7;opacity: 0.7; width: 700px;height: " & altezza & "px; border: 2px #00117a solid;background-color: #2c2ddd; color: #ffffff;"">" & Tabella & "</div>")

        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

        Function campodb(ByVal oggetto As Object) As String


            If IsDBNull(oggetto) Then
                Return ""
            Else
                Return oggetto
            End If
        End Function


        Function campodbN(ByVal oggetto As Object) As Double
            If IsDBNull(oggetto) Then
                Return 0
            Else
                Return oggetto
            End If
        End Function

    End Class

End Namespace
