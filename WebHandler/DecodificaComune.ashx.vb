﻿Namespace WebHandler

    Public Class DecodificaComune
        Implements IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            Dim comune As String = context.Request.QueryString("comune")
            Dim provincia As String = context.Request.QueryString("provincia")
            Dim Utente As String = context.Request.QueryString("UTENTE")
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

            'Dim DbC As New Cls_Login

            'DbC.Utente = Utente
            'DbC.LeggiSP(context.Application("SENIOR"))

            If comune.Trim = "" Then Exit Sub
            If provincia.Trim = "" Then Exit Sub

            Dim x As New ClsComune

            Dim ConnectionString As String = context.Session("DC_OSPITE")

            x.Provincia = provincia
            x.Comune = comune
            x.DecodficaComune(ConnectionString)
            context.Response.Write(x.Descrizione)
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class

End Namespace