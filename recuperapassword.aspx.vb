﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System
Imports System.Net
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel

Partial Class recuperapassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub


    Public Function SendAnEmail(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal ParamArray Vetttore() As String) As String
        Try
            '  Pass in the message information to a new MailMessage
            Dim msg As New Net.Mail.MailMessage(MsgFrom, MsgTo, MsgSubject, MsgBody)

            '   Create an SmtpClient to send the e-mail
            'Dim mailClient As New SmtpClient("smtp.csv-vicenza.org")  '  = local machine IP Address

            'Dim mailClient As New SmtpClient("smtp.sendgrid.net")  '  = local machine IP Address

            'mailClient.Port = 587



            Dim mailClient As New SmtpClient("smtp.serpiolle.com")  '  = local machine IP Address


            'mailClient.Credentials = New System.Net.NetworkCredential("advenias.sendgrid", "Advenias1!")

            mailClient.Credentials = New System.Net.NetworkCredential("gabe@serpiolle.com", "silvia451917")

            '  Use the Windows credentials of the current User

            'If Txt_Mail.Text.Trim <> "" Then
            '    mailClient.Credentials = New System.Net.NetworkCredential(Txt_UserName.Text, Txt_Password.Text)
            '    MsgFrom = Txt_Mail.Text
            'Else
            '    mailClient.Credentials = New System.Net.NetworkCredential("segreteria@csv-vicenza.org", "segreteria13")
            'End If



            msg.IsBodyHtml = True
            Dim I As Integer
            For I = 1 To Vetttore.Length - 1
                If Not IsNothing(Vetttore(I)) Then
                    Dim at As New Attachment(Vetttore(I))
                    msg.Attachments.Add(at)
                End If
            Next

            REM mailClient.UseDefaultCredentials = True


            ' Pass the message to the mail server
            mailClient.Send(msg)

            '  Optional user reassurance:
            ' ScriviLog("Messaggio Inviato a " & MsgTo)

            '  Housekeeping
            msg.Dispose()

            Return "OK"
        Catch ex As FormatException
            ' ScriviLog(ex.Message & " :Format Exception")
            Return ex.Message
        Catch ex As SmtpException
            ' ScriviLog(ex.Message & " :SMTP Exception")
            Return ex.Message
        End Try
    End Function



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        '        Ciao gabellini sauro!

        'Qualcuno ha chiesto un link per resettare la tua password. Per resettare la password fai clic sul link sottostante 'Cambia la tua password'. NON RISPONDERE A QUESTA MAIL

        'Cambia la mia password

        'Se non hai effettuato la richiesta di recupero password, ti preghiamo di ignorare questa e-mail.

        'La password non cambierà fino a quando non accederai al link qui sopra e ne creerai una nuova.

        Dim M As New Cls_Login


        M.Utente = Txt_Login.Text
        M.Ospiti = ""
        M.EmailUtente = ""
        M.LeggiSP(Application("SENIOR"))

        If M.Ospiti <> "" Then
            Dim Appoggio As String

            Appoggio = "Ciao " & M.Utente & "<br />"
            Appoggio = Appoggio & "Qualcuno ha chiesto il link per resettare la password di accesso all'applicativo Senior. Per resettare la password fai clic sul link sottostante :" & "<br />"

            If Request.Url.AbsoluteUri.ToUpper.IndexOf("medservices.cloud".ToUpper) > 0 Then
                Appoggio = Appoggio & "<a  href=""https://senior.medservices.cloud:3450/Seniorweb/ModificaPassword.aspx?USER=" & Txt_Login.Text & "&TOKEN=" & M.PasswordCriptata & """>Cambia la mia password</a>" & "<br />"
            Else
                Appoggio = Appoggio & "<a  href=""" & Request.Url.AbsoluteUri.ToUpper.Replace("recuperapassword.aspx".ToUpper, "").ToLower & "ModificaPassword.aspx?USER=" & Txt_Login.Text & "&TOKEN=" & M.PasswordCriptata & """>Cambia la mia password</a>" & "<br />"
            End If
            Appoggio = Appoggio & "Se non hai effettuato la richiesta di recupero password, ti preghiamo di ignorare questa e-mail." & "<br />"

            Appoggio = Appoggio & "La password non cambierà fino a quando non accederai al link qui sopra e ne creerai una nuova." & "<br />" & "<br />"

            Appoggio = Appoggio & "NON RISPONDERE A QUESTA MAIL. MAIL GENERATA IN AUTOMATICO DAL SISTEMA."

            'AdveniasAss <assistenza@advenias.it
            If M.EmailUtente <> "" Then
                SendAnEmail("senior@advenias.it", M.EmailUtente, "Recupero Password", Appoggio, "")
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Errore mail non indicata, scrivere senior@advenias.it</center>');", True)
            End If
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Utente non trovato</center>');", True)
        End If
    End Sub
End Class
