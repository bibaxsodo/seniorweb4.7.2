﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="AddSocieta" CodeFile="AddSocieta.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Senior</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Senior</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;"></td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Home" runat="server" BackColor="Transparent" ImageUrl="images/Menu_Indietro.png" ToolTip="Home" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <label class="LabelCampo">Connessione DB :</label>
                        <asp:TextBox ID="Txt_StringaConnessione" runat="server" Width="60%"></asp:TextBox><br />
                        <br />
                        <br />
                        <label class="LabelCampo">Istanza DB :</label>
                        <asp:TextBox ID="Txt_IstanzaDB" runat="server" Width="60%"></asp:TextBox><br />
                        <br />
                        <br />
                        <label class="LabelCampo">Nome Società :</label>
                        <asp:TextBox ID="Txt_NomeSocieta" runat="server" Width="300px"></asp:TextBox><br />
                        <br />
                        <label class="LabelCampo">Nome E-Personam :</label><asp:TextBox ID="Txt_NomeEpersonam" runat="server"></asp:TextBox><br />
                        <br />
                        <asp:Button ID="Btn_CreaSocieta" runat="server" Text="Crea DB" />
                        <br />
                        <asp:Label ID="lbl_output" runat="server" Text=""></asp:Label>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
