IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Budget' AND xtype = N'U')
BEGIN
CREATE TABLE Budget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Livello1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Livello2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Livello3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colonna' AND Object_ID = Object_ID(N'Budget'))
BEGIN
ALTER TABLE Budget ADD [Colonna] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CategoriaCespite' AND xtype = N'U')
BEGIN
CREATE TABLE CategoriaCespite([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Legge' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Legge] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Minima' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Minima] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anticipata' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Anticipata] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoConto' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [SottoConto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroContropartita' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [MastroContropartita] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoContropartita' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [ContoContropartita] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoContropartita' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [SottoContoContropartita] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroImobilizazione' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [MastroImobilizazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoImobilizazione' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [ContoImobilizazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoImobilizazione' AND Object_ID = Object_ID(N'CategoriaCespite'))
BEGIN
ALTER TABLE CategoriaCespite ADD [SottocontoImobilizazione] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Cespiti' AND xtype = N'U')
BEGIN
CREATE TABLE Cespiti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ubicazione' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [Ubicazione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Categoria' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [Categoria] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroAcq' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [NumeroAcq] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoAcq' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [AnnoAcq] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroVnd' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [NumeroVnd] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoVnd' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [AnnoVnd] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoPercentuale' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [TipoPercentuale] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Dismesso' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [Dismesso] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoAcq' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [ImportoAcq] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoVnd' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [ImportoVnd] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DescrizioneAcq' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DescrizioneAcq] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DescrizioneVnd' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DescrizioneVnd] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataUltimaStampaCB' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DataUltimaStampaCB] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoFondo' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [ImportoFondo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDismissione' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DataDismissione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DescrizioneDismissione' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DescrizioneDismissione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ColonneBudget' AND xtype = N'U')
BEGIN
CREATE TABLE ColonneBudget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'ColonneBudget'))
BEGIN
ALTER TABLE ColonneBudget ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'ColonneBudget'))
BEGIN
ALTER TABLE ColonneBudget ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'ColonneBudget'))
BEGIN
ALTER TABLE ColonneBudget ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'ColonneBudget'))
BEGIN
ALTER TABLE ColonneBudget ADD [Livello1] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ColoreMastroConto' AND xtype = N'U')
BEGIN
CREATE TABLE ColoreMastroConto([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colore' AND Object_ID = Object_ID(N'ColoreMastroConto'))
BEGIN
ALTER TABLE ColoreMastroConto ADD [Colore] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'ColoreMastroConto'))
BEGIN
ALTER TABLE ColoreMastroConto ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'ColoreMastroConto'))
BEGIN
ALTER TABLE ColoreMastroConto ADD [Mastro] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'EstrazioneXBilancio' AND xtype = N'U')
BEGIN
CREATE TABLE EstrazioneXBilancio([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroA' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [MastroA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroC' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [MastroC] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroP' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [MastroP] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroR' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [MastroR] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ordine' AND Object_ID = Object_ID(N'EstrazioneXBilancio'))
BEGIN
ALTER TABLE EstrazioneXBilancio ADD [Ordine] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'IncassiOspiti' AND xtype = N'U')
BEGIN
CREATE TABLE IncassiOspiti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [NumeroDocumento] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroIncasso' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [NumeroIncasso] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDocumento' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [DataDocumento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataIncasso' AND Object_ID = Object_ID(N'IncassiOspiti'))
BEGIN
ALTER TABLE IncassiOspiti ADD [DataIncasso] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Leasing' AND xtype = N'U')
BEGIN
CREATE TABLE Leasing([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'Leasing'))
BEGIN
ALTER TABLE Leasing ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Leasing'))
BEGIN
ALTER TABLE Leasing ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Leasing'))
BEGIN
ALTER TABLE Leasing ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Leasing'))
BEGIN
ALTER TABLE Leasing ADD [Importo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'LegameBudgetRegistrazione' AND xtype = N'U')
BEGIN
CREATE TABLE LegameBudgetRegistrazione([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Livello1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Livello2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Livello3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colonna' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Colonna] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaRegistrazione' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [RigaRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoConto' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [SottoConto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'LegameContoVistaLogica' AND xtype = N'U')
BEGIN
CREATE TABLE LegameContoVistaLogica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceVista' AND Object_ID = Object_ID(N'LegameContoVistaLogica'))
BEGIN
ALTER TABLE LegameContoVistaLogica ADD [CodiceVista] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'LegameContoVistaLogica'))
BEGIN
ALTER TABLE LegameContoVistaLogica ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'LegameContoVistaLogica'))
BEGIN
ALTER TABLE LegameContoVistaLogica ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'LegameContoVistaLogica'))
BEGIN
ALTER TABLE LegameContoVistaLogica ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'LegameContoVistaLogica1' AND xtype = N'U')
BEGIN
CREATE TABLE LegameContoVistaLogica1([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'LegameContoVistaLogica1'))
BEGIN
ALTER TABLE LegameContoVistaLogica1 ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceVista' AND Object_ID = Object_ID(N'LegameContoVistaLogica1'))
BEGIN
ALTER TABLE LegameContoVistaLogica1 ADD [CodiceVista] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'LegameContoVistaLogica1'))
BEGIN
ALTER TABLE LegameContoVistaLogica1 ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'LegameContoVistaLogica1'))
BEGIN
ALTER TABLE LegameContoVistaLogica1 ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'LegameContoVistaLogica1'))
BEGIN
ALTER TABLE LegameContoVistaLogica1 ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiCespite' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiCespite([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'MovimentiCespite'))
BEGIN
ALTER TABLE MovimentiCespite ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCespite' AND Object_ID = Object_ID(N'MovimentiCespite'))
BEGIN
ALTER TABLE MovimentiCespite ADD [CodiceCespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoMov' AND Object_ID = Object_ID(N'MovimentiCespite'))
BEGIN
ALTER TABLE MovimentiCespite ADD [TipoMov] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'MovimentiCespite'))
BEGIN
ALTER TABLE MovimentiCespite ADD [Importo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiContabiliRiga_Deleted' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiContabiliRiga_Deleted([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [MastroPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [ContoPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [SottocontoPartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [MastroContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [ContoContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [SottocontoContropartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Tipo] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [CodiceIVA] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Imponibile' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Imponibile] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Detraibile' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Detraibile] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Scadenza' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Scadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prorata' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Prorata] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RibaltamentoManuale' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [RibaltamentoManuale] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaBollato' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [RigaBollato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaDaCausale' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [RigaDaCausale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceRitenuta' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [CodiceRitenuta] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Quantita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Quantita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleDescrittiva' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [CausaleDescrittiva] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseRiferimento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [MeseRiferimento] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoRiferimento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [AnnoRiferimento] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DestinatoVendita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [DestinatoVendita] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Trigger_Data' AND Object_ID = Object_ID(N'MovimentiContabiliRiga_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliRiga_Deleted ADD [Trigger_Data] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiContabiliTesta_Deleted' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiContabiliTesta_Deleted([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataDocumento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroDocumento] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Competenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Competenza] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Descrizione] nvarchar (Max);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVASospesa' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [IVASospesa] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [TipoDocumento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroIVA' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [RegistroIVA] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoProtocollo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [AnnoProtocollo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocollo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroProtocollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [CodicePagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GeneraleFinanziaria' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [GeneraleFinanziaria] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Trasferimento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Trasferimento] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Economato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Economato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoCompetenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [AnnoCompetenza] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseCompetenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [MeseCompetenza] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FatturaDiAnticipo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [FatturaDiAnticipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroBolletta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroBolletta] nvarchar (16);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataBolletta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataBolletta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModalitaPagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [ModalitaPagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stornato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Stornato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipologia' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Tipologia] nvarchar (7);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataValuta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataValuta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDistinta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataDistinta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataMandato] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataReversale' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [DataReversale] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDistinta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroDistinta] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroReversale' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroReversale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoStorno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [TipoStorno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoImpegno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [AnnoImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroImpegno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [NumeroImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cespite' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Cespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BolloMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [BolloMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Bis' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Bis] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Trigger_Data' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD [Trigger_Data] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'NoteFattura' AND xtype = N'U')
BEGIN
CREATE TABLE NoteFattura([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'NoteFattura'))
BEGIN
ALTER TABLE NoteFattura ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'NoteFattura'))
BEGIN
ALTER TABLE NoteFattura ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Parametri' AND xtype = N'U')
BEGIN
CREATE TABLE Parametri([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PathWSDL' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [PathWSDL] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleEntrata' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaleEntrata] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleUscita' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaleUscita] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleGiroconto' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaleGiroconto] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaliReversali' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaliReversali] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaliMandati' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaliMandati] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ParametriCespiti' AND xtype = N'U')
BEGIN
CREATE TABLE ParametriCespiti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'ParametriCespiti'))
BEGIN
ALTER TABLE ParametriCespiti ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabileDismissione' AND Object_ID = Object_ID(N'ParametriCespiti'))
BEGIN
ALTER TABLE ParametriCespiti ADD [CausaleContabileDismissione] nvarchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PersonaleInventario' AND xtype = N'U')
BEGIN
CREATE TABLE PersonaleInventario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cognome' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [Cognome] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Nome' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [Nome] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataNascita' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [DataNascita] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [Telefono] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EMail' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [EMail] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDipendente' AND Object_ID = Object_ID(N'PersonaleInventario'))
BEGIN
ALTER TABLE PersonaleInventario ADD [CodiceDipendente] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PersonaleInventario1' AND xtype = N'U')
BEGIN
CREATE TABLE PersonaleInventario1([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDipendente' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [CodiceDipendente] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cognome' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [Cognome] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EMail' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [EMail] nvarchar (60);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Nome' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [Nome] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [Telefono] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataNascita' AND Object_ID = Object_ID(N'PersonaleInventario1'))
BEGIN
ALTER TABLE PersonaleInventario1 ADD [DataNascita] datetime;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PostazionePersonale' AND xtype = N'U')
BEGIN
CREATE TABLE PostazionePersonale([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdPersonale' AND Object_ID = Object_ID(N'PostazionePersonale'))
BEGIN
ALTER TABLE PostazionePersonale ADD [IdPersonale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStanza' AND Object_ID = Object_ID(N'PostazionePersonale'))
BEGIN
ALTER TABLE PostazionePersonale ADD [IdStanza] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdPostazione' AND Object_ID = Object_ID(N'PostazionePersonale'))
BEGIN
ALTER TABLE PostazionePersonale ADD [IdPostazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'PostazionePersonale'))
BEGIN
ALTER TABLE PostazionePersonale ADD [Data] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PostazioniInventario' AND xtype = N'U')
BEGIN
CREATE TABLE PostazioniInventario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'StanzaId' AND Object_ID = Object_ID(N'PostazioniInventario'))
BEGIN
ALTER TABLE PostazioniInventario ADD [StanzaId] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'PostazioniInventario'))
BEGIN
ALTER TABLE PostazioniInventario ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PrenotazioniRiga' AND xtype = N'U')
BEGIN
CREATE TABLE PrenotazioniRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Livello1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Livello2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Livello3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Riga' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Riga] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colonna' AND Object_ID = Object_ID(N'PrenotazioniRiga'))
BEGIN
ALTER TABLE PrenotazioniRiga ADD [Colonna] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PrenotazioniTesta' AND xtype = N'U')
BEGIN
CREATE TABLE PrenotazioniTesta([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAtto' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [DataAtto] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroAtto' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [NumeroAtto] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoAtto' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [TipoAtto] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumentoEGO' AND Object_ID = Object_ID(N'PrenotazioniTesta'))
BEGIN
ALTER TABLE PrenotazioniTesta ADD [NumeroDocumentoEGO] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'PrenotazioniUsate' AND xtype = N'U')
BEGIN
CREATE TABLE PrenotazioniUsate([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoPrenotazione' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [AnnoPrenotazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroPrenotazione' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [NumeroPrenotazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaPrenotazione' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [RigaPrenotazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaRegistrazione' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [RigaRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoConto' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [SottoConto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'PrenotazioniUsate'))
BEGIN
ALTER TABLE PrenotazioniUsate ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'RateiRisconti' AND xtype = N'U')
BEGIN
CREATE TABLE RateiRisconti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ServizioDal' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [ServizioDal] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ServizioAl' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [ServizioAl] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroDare' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [MastroDare] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoDare' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [ContoDare] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoDare' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [SottoContoDare] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroAvere' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [MastroAvere] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoAvere' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [ContoAvere] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoAvere' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [SottoContoAvere] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoTotale' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [ImportoTotale] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RateoRisconto' AND Object_ID = Object_ID(N'RateiRisconti'))
BEGIN
ALTER TABLE RateiRisconti ADD [RateoRisconto] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'RegoleBudget' AND xtype = N'U')
BEGIN
CREATE TABLE RegoleBudget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoPianoDeiConti' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [ContoPianoDeiConti] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoFisso' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [ImportoFisso] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Livello1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Livello2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Livello3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroPianoDeiConti' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [MastroPianoDeiConti] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Percentuale' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Percentuale] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoPianoDeiConti' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [SottoContoPianoDeiConti] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colonna' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD [Colonna] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'salvapianoconti' AND xtype = N'U')
BEGIN
CREATE TABLE salvapianoconti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Mastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Conto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sezione' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Sezione] nvarchar (11);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Classe' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Classe] nvarchar (11);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoBene' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [TipoBene] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoAnagrafica' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [TipoAnagrafica] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EntrataUscita' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [EntrataUscita] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Capitolo' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Capitolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Articolo' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [Articolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NONINUSO' AND Object_ID = Object_ID(N'salvapianoconti'))
BEGIN
ALTER TABLE salvapianoconti ADD [NONINUSO] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'StanzeInventario' AND xtype = N'U')
BEGIN
CREATE TABLE StanzeInventario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'StanzeInventario'))
BEGIN
ALTER TABLE StanzeInventario ADD [Descrizione] nvarchar (200);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdReparto' AND Object_ID = Object_ID(N'StanzeInventario'))
BEGIN
ALTER TABLE StanzeInventario ADD [IdReparto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'StanzeInventario'))
BEGIN
ALTER TABLE StanzeInventario ADD [Numero] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'StoricizzaStanze' AND xtype = N'U')
BEGIN
CREATE TABLE StoricizzaStanze([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCespite' AND Object_ID = Object_ID(N'StoricizzaStanze'))
BEGIN
ALTER TABLE StoricizzaStanze ADD [CodiceCespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'StoricizzaStanze'))
BEGIN
ALTER TABLE StoricizzaStanze ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stanza' AND Object_ID = Object_ID(N'StoricizzaStanze'))
BEGIN
ALTER TABLE StoricizzaStanze ADD [Stanza] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Personale' AND Object_ID = Object_ID(N'StoricizzaStanze'))
BEGIN
ALTER TABLE StoricizzaStanze ADD [Personale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'StoricizzaStanze'))
BEGIN
ALTER TABLE StoricizzaStanze ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'StrutturaIVDirettivaCEE' AND xtype = N'U')
BEGIN
CREATE TABLE StrutturaIVDirettivaCEE([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceLivello' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [CodiceLivello] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePadre' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [CodicePadre] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [Codice] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOperazione' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE ADD [CodiceOperazione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'StrutturaIVDirettivaCEE2' AND xtype = N'U')
BEGIN
CREATE TABLE StrutturaIVDirettivaCEE2([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceLivello' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [CodiceLivello] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePadre' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [CodicePadre] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [Codice] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOperazione' AND Object_ID = Object_ID(N'StrutturaIVDirettivaCEE2'))
BEGIN
ALTER TABLE StrutturaIVDirettivaCEE2 ADD [CodiceOperazione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Tabella1' AND xtype = N'U')
BEGIN
CREATE TABLE Tabella1([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Tabella1'))
BEGIN
ALTER TABLE Tabella1 ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Tabella1'))
BEGIN
ALTER TABLE Tabella1 ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaCoFog' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaCoFog([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaCoFog'))
BEGIN
ALTER TABLE TabellaCoFog ADD [Codice] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'TabellaCoFog'))
BEGIN
ALTER TABLE TabellaCoFog ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaCoFog'))
BEGIN
ALTER TABLE TabellaCoFog ADD [Descrizione] nvarchar (150);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaLegami' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaLegami([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDocumento' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD [CodiceDocumento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePagamento' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD [CodicePagamento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD [Importo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaLivelli' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaLivelli([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO1' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO2' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO3' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO4' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO4] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO5' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO5] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO6' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO6] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO7' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO7] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO8' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO8] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO9' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO9] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LIVELLO10' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [LIVELLO10] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO1' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO1] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO2' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO2] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO3' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO3] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO4' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO4] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO5' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO5] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO6' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO6] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO7' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO7] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO8' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO8] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO9' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO9] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TESTO10' AND Object_ID = Object_ID(N'TabellaLivelli'))
BEGIN
ALTER TABLE TabellaLivelli ADD [TESTO10] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaPerGiroconti' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaPerGiroconti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroDa' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [MastroDa] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoDA' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [ContoDA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoDa' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [SottoContoDa] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MasroA' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [MasroA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoA' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [ContoA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoA' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [SottocontoA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroA' AND Object_ID = Object_ID(N'TabellaPerGiroconti'))
BEGIN
ALTER TABLE TabellaPerGiroconti ADD [MastroA] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaReparti' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaReparti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaReparti'))
BEGIN
ALTER TABLE TabellaReparti ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TabellaReparti'))
BEGIN
ALTER TABLE TabellaReparti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TabellaReparti'))
BEGIN
ALTER TABLE TabellaReparti ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Temp_TabellaLegami' AND xtype = N'U')
BEGIN
CREATE TABLE Temp_TabellaLegami([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDocumento' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD [CodiceDocumento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePagamento' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD [CodicePagamento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD [Importo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoBudget' AND xtype = N'U')
BEGIN
CREATE TABLE TipoBudget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Livello1] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Livello2] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Livello3] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'tipo' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ntipo' AND Object_ID = Object_ID(N'TipoBudget'))
BEGIN
ALTER TABLE TipoBudget ADD [Ntipo] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoClienteFornitore' AND xtype = N'U')
BEGIN
CREATE TABLE TipoClienteFornitore([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TipoClienteFornitore'))
BEGIN
ALTER TABLE TipoClienteFornitore ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoClienteFornitore'))
BEGIN
ALTER TABLE TipoClienteFornitore ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipologiaCategoria' AND xtype = N'U')
BEGIN
CREATE TABLE TipologiaCategoria([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipologiaCategoria'))
BEGIN
ALTER TABLE TipologiaCategoria ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipologiaCategoria'))
BEGIN
ALTER TABLE TipologiaCategoria ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipologiaCategoria'))
BEGIN
ALTER TABLE TipologiaCategoria ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'VariazioniBudget' AND xtype = N'U')
BEGIN
CREATE TABLE VariazioniBudget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Anno] decimal;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello1' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Livello1] decimal;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello2' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Livello2] decimal;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Livello3' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Livello3] decimal;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Colonna' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Colonna] decimal;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'importo' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'VariazioniBudget'))
BEGIN
ALTER TABLE VariazioniBudget ADD [Descrizione] nvarchar (200);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'VariazioniPrenotazioni' AND xtype = N'U')
BEGIN
CREATE TABLE VariazioniPrenotazioni([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoPrenotazione' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [AnnoPrenotazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [Descrizione] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroPrenotazione' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [NumeroPrenotazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaPrenotazione' AND Object_ID = Object_ID(N'VariazioniPrenotazioni'))
BEGIN
ALTER TABLE VariazioniPrenotazioni ADD [RigaPrenotazione] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'VistaLogica' AND xtype = N'U')
BEGIN
CREATE TABLE VistaLogica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'VistaLogica'))
BEGIN
ALTER TABLE VistaLogica ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'VistaLogica'))
BEGIN
ALTER TABLE VistaLogica ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Temp_MovimentiContabiliTesta' AND xtype = N'U')
BEGIN
CREATE TABLE Temp_MovimentiContabiliTesta([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDocumento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataDocumento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroDocumento] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Competenza' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Competenza] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Descrizione] nvarchar (Max);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVASospesa' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [IVASospesa] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoDocumento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [TipoDocumento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroIVA' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [RegistroIVA] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoProtocollo' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [AnnoProtocollo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocollo' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroProtocollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePagamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CodicePagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GeneraleFinanziaria' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [GeneraleFinanziaria] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Trasferimento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Trasferimento] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Economato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Economato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoCompetenza' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [AnnoCompetenza] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseCompetenza' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [MeseCompetenza] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FatturaDiAnticipo' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [FatturaDiAnticipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroBolletta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroBolletta] nvarchar (16);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataBolletta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataBolletta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModalitaPagamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [ModalitaPagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stornato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Stornato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipologia' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Tipologia] nvarchar (7);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataValuta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataValuta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDistinta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataDistinta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataMandato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataMandato] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataReversale' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [DataReversale] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDistinta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroDistinta] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroMandato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroReversale' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroReversale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoStorno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [TipoStorno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoImpegno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [AnnoImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroImpegno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [NumeroImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cespite' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Cespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BolloMandato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [BolloMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Bis' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Bis] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PeriodoRiferimentoRettaMese' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [PeriodoRiferimentoRettaMese] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PeriodoRiferimentoRettaAnno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [PeriodoRiferimentoRettaAnno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerLiquidazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [PerLiquidazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoPagatoLiquidazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [ImportoPagatoLiquidazione] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'xCespite' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [xCespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDego' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [IDego] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCig' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CodiceCig] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceLeasing' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CodiceLeasing] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RiferimentoContratto' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [RiferimentoContratto] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDODV' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [IDODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoODV' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [AnnoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoODV' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [TipoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdProgettoODV' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [IdProgettoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspTipoPagamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspTipoPagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspNumeroRegolarizzazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspNumeroRegolarizzazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspNumeroElenco' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspNumeroElenco] nvarchar (7);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspSpeseMandato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspSpeseMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspDataEsecuzionePagamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspDataEsecuzionePagamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceSIOPE' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspCodiceSIOPE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceCUP' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspCodiceCUP] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceDelegatoQuietanzante' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [EspCodiceDelegatoQuietanzante] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cofog' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [Cofog] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProgressivoAnno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [ProgressivoAnno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProgressivoNumero' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [ProgressivoNumero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD [CodiceOspite] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Temp_MovimentiContabiliRiga' AND xtype = N'U')
BEGIN
CREATE TABLE Temp_MovimentiContabiliRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroPartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [MastroPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoPartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [ContoPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoPartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [SottocontoPartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroContropartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [MastroContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoContropartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [ContoContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoContropartita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [SottocontoContropartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Tipo] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [CodiceIVA] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Imponibile' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Imponibile] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Detraibile' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Detraibile] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Scadenza' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Scadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prorata' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Prorata] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RibaltamentoManuale' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [RibaltamentoManuale] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaBollato' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [RigaBollato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaDaCausale' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [RigaDaCausale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceRitenuta' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [CodiceRitenuta] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Quantita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Quantita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleDescrittiva' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [CausaleDescrittiva] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseRiferimento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [MeseRiferimento] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoRiferimento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [AnnoRiferimento] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DestinatoVendita' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [DestinatoVendita] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaRegistrazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [RigaRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Invisibile' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [Invisibile] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CENTROSERVIZIO' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [CENTROSERVIZIO] varchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoExtra' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliRiga'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliRiga ADD [TipoExtra] varchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Temp_Scadenzario' AND xtype = N'U')
BEGIN
CREATE TABLE Temp_Scadenzario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazioneContabile' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [NumeroRegistrazioneContabile] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataScadenza' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [DataScadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Chiusa' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [Chiusa] tinyint;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiContabiliRiga' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiContabiliRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [MastroPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [ContoPartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoPartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [SottocontoPartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [MastroContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [ContoContropartita] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoContropartita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [SottocontoContropartita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Descrizione] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Tipo] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [CodiceIVA] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Imponibile' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Imponibile] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Detraibile' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Detraibile] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Scadenza' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Scadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prorata' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Prorata] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RibaltamentoManuale' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [RibaltamentoManuale] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaBollato' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [RigaBollato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaDaCausale' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [RigaDaCausale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceRitenuta' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [CodiceRitenuta] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Quantita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Quantita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleDescrittiva' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [CausaleDescrittiva] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseRiferimento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [MeseRiferimento] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoRiferimento' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [AnnoRiferimento] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DestinatoVendita' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [DestinatoVendita] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RigaRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [RigaRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Invisibile' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [Invisibile] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CENTROSERVIZIO' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [CENTROSERVIZIO] varchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoExtra' AND Object_ID = Object_ID(N'MovimentiContabiliRiga'))
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD [TipoExtra] varchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiContabiliTesta' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiContabiliTesta([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataDocumento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroDocumento] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Competenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Competenza] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Descrizione] nvarchar (Max);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVASospesa' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [IVASospesa] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoDocumento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [TipoDocumento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroIVA' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [RegistroIVA] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoProtocollo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [AnnoProtocollo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocollo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroProtocollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodicePagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CodicePagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GeneraleFinanziaria' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [GeneraleFinanziaria] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Trasferimento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Trasferimento] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Economato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Economato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoCompetenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [AnnoCompetenza] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseCompetenza' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [MeseCompetenza] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FatturaDiAnticipo' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [FatturaDiAnticipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroBolletta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroBolletta] nvarchar (16);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataBolletta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataBolletta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModalitaPagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [ModalitaPagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stornato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Stornato] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipologia' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Tipologia] nvarchar (7);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataValuta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataValuta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDistinta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataDistinta] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataMandato] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataReversale' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [DataReversale] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDistinta' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroDistinta] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroReversale' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroReversale] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoStorno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [TipoStorno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoImpegno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [AnnoImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroImpegno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [NumeroImpegno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cespite' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Cespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BolloMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [BolloMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Bis' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Bis] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PeriodoRiferimentoRettaMese' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [PeriodoRiferimentoRettaMese] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PeriodoRiferimentoRettaAnno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [PeriodoRiferimentoRettaAnno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerLiquidazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [PerLiquidazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoPagatoLiquidazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [ImportoPagatoLiquidazione] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'xCespite' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [xCespite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDego' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [IDego] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCig' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CodiceCig] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceLeasing' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CodiceLeasing] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RiferimentoContratto' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [RiferimentoContratto] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDODV' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [IDODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoODV' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [AnnoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoODV' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [TipoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdProgettoODV' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [IdProgettoODV] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspTipoPagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspTipoPagamento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspNumeroRegolarizzazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspNumeroRegolarizzazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspNumeroElenco' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspNumeroElenco] nvarchar (7);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspSpeseMandato' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspSpeseMandato] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspDataEsecuzionePagamento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspDataEsecuzionePagamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceSIOPE' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspCodiceSIOPE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceCUP' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspCodiceCUP] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EspCodiceDelegatoQuietanzante' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [EspCodiceDelegatoQuietanzante] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cofog' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [Cofog] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProgressivoAnno' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [ProgressivoAnno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProgressivoNumero' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [ProgressivoNumero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD [CodiceOspite] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Scadenzario' AND xtype = N'U')
BEGIN
CREATE TABLE Scadenzario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazioneContabile' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [NumeroRegistrazioneContabile] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataScadenza' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [DataScadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Chiusa' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD [Chiusa] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistrazioneIncasso' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD RegistrazioneIncasso int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'UltimeRicerca' AND xtype = N'U')
BEGIN
CREATE TABLE UltimeRicerca([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataOraRicerca' AND Object_ID = Object_ID(N'UltimeRicerca'))
BEGIN
ALTER TABLE UltimeRicerca ADD [DataOraRicerca] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'UltimeRicerca'))
BEGIN
ALTER TABLE UltimeRicerca ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'UltimeRicerca'))
BEGIN
ALTER TABLE UltimeRicerca ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'FatturaElettronica' AND xtype = N'U')
BEGIN
CREATE TABLE FatturaElettronica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD [UTENTE] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Progressivo' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD [Progressivo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'XMLModificato' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD XMLModificato text;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'XMLOriginale' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD XMLOriginale text;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataOra' AND Object_ID = Object_ID(N'FatturaElettronica'))
BEGIN
ALTER TABLE FatturaElettronica ADD DataOra datetime;
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'AddOn' AND xtype = N'U')
BEGIN
CREATE TABLE AddOn([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'AddOn'))
BEGIN
ALTER TABLE AddOn ADD [Descrizione] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Path' AND Object_ID = Object_ID(N'AddOn'))
BEGIN
ALTER TABLE AddOn ADD [Path] nvarchar (150);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'attobilancio' AND xtype = N'U')
BEGIN
CREATE TABLE attobilancio([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'anno' AND Object_ID = Object_ID(N'attobilancio'))
BEGIN
ALTER TABLE attobilancio ADD [anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'dataatto' AND Object_ID = Object_ID(N'attobilancio'))
BEGIN
ALTER TABLE attobilancio ADD [dataatto] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'numeroatto' AND Object_ID = Object_ID(N'attobilancio'))
BEGIN
ALTER TABLE attobilancio ADD [numeroatto] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'tipoatto' AND Object_ID = Object_ID(N'attobilancio'))
BEGIN
ALTER TABLE attobilancio ADD [tipoatto] nvarchar (2);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'BilancioPreventivo' AND xtype = N'U')
BEGIN
CREATE TABLE BilancioPreventivo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [UTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Conto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Saldo' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Saldo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoConto' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [SottoConto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Gennaio' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Gennaio] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Febbraio' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Febbraio] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Marzo' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Marzo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Aprile' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Aprile] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Maggio' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Maggio] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Giugno' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Giugno] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Luglio' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Luglio] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Agosto' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Agosto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Settembre' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Settembre] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ottobre' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Ottobre] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Novembre' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Novembre] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Dicembre' AND Object_ID = Object_ID(N'BilancioPreventivo'))
BEGIN
ALTER TABLE BilancioPreventivo ADD [Dicembre] float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifNumero' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD RifNumero varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifData' AND Object_ID = Object_ID(N'MovimentiContabiliTesta_Deleted'))
BEGIN
ALTER TABLE MovimentiContabiliTesta_Deleted ADD RifData date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifNumero' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD RifNumero varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifData' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD RifData date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifNumero' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD RifNumero varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifData' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD RifData date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EnteDestinatario' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD EnteDestinatario varchar(7);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EnteDestinatario' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD EnteDestinatario varchar(7);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRid' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD DataRid date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdRid' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD IdRid varchar(7);
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiRitenute' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiRitenute([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazioneDocumento' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD NumeroRegistrazioneDocumento int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceTributo' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD CodiceTributo varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImponibileRitenuta' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD ImponibileRitenuta float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoRitenuta' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD TipoRitenuta varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoRitenuta' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD ImportoRitenuta float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazionePagamento' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD NumeroRegistrazionePagamento int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoPagamento' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD ImportoPagamento float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazionePagamentoRitenuta' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD NumeroRegistrazionePagamentoRitenuta int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoPagamentoRitenuta' AND Object_ID = Object_ID(N'MovimentiRitenute'))
BEGIN
ALTER TABLE MovimentiRitenute ADD ImportoPagamentoRitenuta float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'RegoleBudget'))
BEGIN
ALTER TABLE RegoleBudget ADD CentroServizio nvarchar (4);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifNumero' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD RifNumero varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RifData' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD RifData date;
END





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRid' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD DataRid date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdRid' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD IdRid varchar(7);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Cespiti'))
BEGIN
ALTER TABLE Cespiti ADD Utente nvarchar (255);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ExportDATA' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD ExportDATA datetime;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ExportDATA' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD ExportDATA datetime;
END





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BolloVirtuale' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD BolloVirtuale int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BolloVirtuale' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD BolloVirtuale int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoCompetenza' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD AnnoCompetenza int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseCompetenza' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD MeseCompetenza int; 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataOraElaborazione' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [DataOraElaborazione] datetime;
END



ALTER TABLE Temp_MovimentiContabiliRiga  ALTER COLUMN DestinatoVendita varchar (250); 

ALTER TABLE MovimentiContabiliRiga  ALTER COLUMN DestinatoVendita varchar (250);

ALTER TABLE MovimentiContabiliRiga_Deleted  ALTER COLUMN DestinatoVendita varchar (250);	





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistrazioneIncasso' AND Object_ID = Object_ID(N'Temp_Scadenzario'))
BEGIN
ALTER TABLE Temp_Scadenzario ADD [RegistrazioneIncasso] int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Automatico' AND Object_ID = Object_ID(N'LegameBudgetRegistrazione'))
BEGIN
ALTER TABLE LegameBudgetRegistrazione ADD Automatico int;
END





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistrazioneRiferimento' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD RegistrazioneRiferimento int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistrazioneRiferimento' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD RegistrazioneRiferimento int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CigClientiFornitori' AND xtype = N'U')
BEGIN
CREATE TABLE CigClientiFornitori([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDebitoreCreditore' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD CodiceDebitoreCreditore int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCig' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD CodiceCig varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataInizio' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD DataInizio date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataFine' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD DataFine date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoBudget' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD ImportoBudget float;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD Utente varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD DataAggiornamento datetime;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CigClientiFornitori'))
BEGIN
ALTER TABLE CigClientiFornitori ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'StatoRegistrazione' AND Object_ID = Object_ID(N'MovimentiContabiliTesta'))
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD StatoRegistrazione int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'StatoRegistrazione' AND Object_ID = Object_ID(N'Temp_MovimentiContabiliTesta'))
BEGIN
ALTER TABLE Temp_MovimentiContabiliTesta ADD StatoRegistrazione int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdDistinta' AND Object_ID = Object_ID(N'TabellaLegami'))
BEGIN
ALTER TABLE TabellaLegami ADD IdDistinta int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdDistinta' AND Object_ID = Object_ID(N'Temp_TabellaLegami'))
BEGIN
ALTER TABLE Temp_TabellaLegami ADD IdDistinta int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdDistinta' AND Object_ID = Object_ID(N'Scadenzario'))
BEGIN
ALTER TABLE Scadenzario ADD IdDistinta int;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'DistintaIncassoPagamento' AND xtype = N'U')
BEGIN
CREATE TABLE DistintaIncassoPagamento([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDistinta' AND Object_ID = Object_ID(N'DistintaIncassoPagamento'))
BEGIN
ALTER TABLE DistintaIncassoPagamento ADD DataDistinta date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'DistintaIncassoPagamento'))
BEGIN
ALTER TABLE DistintaIncassoPagamento ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoAV' AND Object_ID = Object_ID(N'DistintaIncassoPagamento'))
BEGIN
ALTER TABLE DistintaIncassoPagamento ADD TipoAV varchar(1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Generato' AND Object_ID = Object_ID(N'DistintaIncassoPagamento'))
BEGIN
ALTER TABLE DistintaIncassoPagamento ADD Generato int;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'DistintaIncassoPagamentoRiga' AND xtype = N'U')
BEGIN
CREATE TABLE DistintaIncassoPagamentoRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdDistinta' AND Object_ID = Object_ID(N'DistintaIncassoPagamentoRiga'))
BEGIN
ALTER TABLE DistintaIncassoPagamentoRiga ADD IdDistinta int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazioneDocumento' AND Object_ID = Object_ID(N'DistintaIncassoPagamentoRiga'))
BEGIN
ALTER TABLE DistintaIncassoPagamentoRiga ADD NumeroRegistrazioneDocumento int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdScadenza' AND Object_ID = Object_ID(N'DistintaIncassoPagamentoRiga'))
BEGIN
ALTER TABLE DistintaIncassoPagamentoRiga ADD IdScadenza int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'DistintaIncassoPagamentoRiga'))
BEGIN
ALTER TABLE DistintaIncassoPagamentoRiga ADD Importo float;
END