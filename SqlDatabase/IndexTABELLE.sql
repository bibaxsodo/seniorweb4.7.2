﻿declare @qry nvarchar(max);
select @qry = (

    select  'IF EXISTS(SELECT * FROM sys.indexes WHERE name='''+ i.name +''' AND object_id = OBJECT_ID(''['+s.name+'].['+o.name+']''))      drop index ['+i.name+'] ON ['+s.name+'].['+o.name+'];  '
    from sys.indexes i 
        inner join sys.objects o on  i.object_id=o.object_id
        inner join sys.schemas s on o.schema_id = s.schema_id
    where o.type<>'S' and is_primary_key<>1 and index_id>0
    and s.name!='sys' and s.name!='sys' and is_unique_constraint=0
for xml path(''));

exec sp_executesql @qry

IF NOT EXISTS (SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME LIKE 'CausaliContabiliRiga'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE CausaliContabiliRiga ADD PRIMARY KEY (ID);
END 

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_CausaliContabiliRiga' AND object_id = OBJECT_ID('CausaliContabiliRiga'))
BEGIN
CREATE INDEX IX_CausaliContabiliRiga ON CausaliContabiliRiga (Codice);
END

IF NOT EXISTS (SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME LIKE 'CausaliContabiliTesta'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE CausaliContabiliTesta ADD PRIMARY KEY (ID);
END 

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'PK_CausaliContabiliTesta' AND object_id = OBJECT_ID('CausaliContabiliTesta'))
BEGIN
CREATE UNIQUE INDEX PK_CausaliContabiliTesta ON CausaliContabiliTesta (Codice);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_CausaliContabiliTestaCodice' AND object_id = OBJECT_ID('CausaliContabiliTesta'))
BEGIN
CREATE UNIQUE INDEX IX_CausaliContabiliTestaCodice ON CausaliContabiliTesta (Codice);
END
