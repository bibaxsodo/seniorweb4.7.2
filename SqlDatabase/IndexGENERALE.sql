﻿declare @qry nvarchar(max);
select @qry = (

    select  'IF EXISTS(SELECT * FROM sys.indexes WHERE name='''+ i.name +''' AND object_id = OBJECT_ID(''['+s.name+'].['+o.name+']''))      drop index ['+i.name+'] ON ['+s.name+'].['+o.name+'];  '
    from sys.indexes i 
        inner join sys.objects o on  i.object_id=o.object_id
        inner join sys.schemas s on o.schema_id = s.schema_id
    where o.type<>'S' and is_primary_key<>1 and index_id>0
    and s.name!='sys' and s.name!='sys' and is_unique_constraint=0
for xml path(''));

exec sp_executesql @qry




IF NOT EXISTS (SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME LIKE 'MovimentiContabiliRiga'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE MovimentiContabiliRiga ADD PRIMARY KEY (ID);
END 



IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_MovimentiContabiliRigaMCS' AND object_id = OBJECT_ID('MovimentiContabiliRiga'))
BEGIN
CREATE INDEX IX_MovimentiContabiliRigaMCS ON MovimentiContabiliRiga (MastroPartita,ContoPartita,SottocontoPartita);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'PK_MovimentiContabiliRigaNumero' AND object_id = OBJECT_ID('MovimentiContabiliRiga'))
BEGIN
CREATE INDEX PK_MovimentiContabiliRigaNumero ON MovimentiContabiliRiga (Numero);
END


IF NOT EXISTS (SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME LIKE 'MovimentiContabiliTesta'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE MovimentiContabiliTesta ADD PRIMARY KEY (ID);
END 

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'PK_MovimentiContabiliTesta' AND object_id = OBJECT_ID('MovimentiContabiliTesta'))
BEGIN
CREATE UNIQUE INDEX PK_MovimentiContabiliTesta ON MovimentiContabiliTesta (NumeroRegistrazione);
END


IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_MovimentiContabiliTestaProtocollo' AND object_id = OBJECT_ID('MovimentiContabiliTesta'))
BEGIN
CREATE INDEX IX_MovimentiContabiliTestaProtocollo ON MovimentiContabiliTesta (AnnoProtocollo,NumeroProtocollo);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_MovimentiContabiliTestaDataRegistrazione' AND object_id = OBJECT_ID('MovimentiContabiliTesta'))
BEGIN
CREATE INDEX IX_MovimentiContabiliTestaDataRegistrazione ON MovimentiContabiliTesta (DataRegistrazione);
END





IF NOT EXISTS (SELECT * 
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' 
AND TABLE_NAME LIKE 'TabellaLegami'
AND TABLE_SCHEMA ='dbo' )
BEGIN
ALTER TABLE TabellaLegami ADD PRIMARY KEY (ID);
END 


IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_TabellaLegamiCodPag' AND object_id = OBJECT_ID('TabellaLegami'))
BEGIN
CREATE INDEX IX_TabellaLegamiCodPag ON TabellaLegami (CodicePagamento);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_TabellaLegamiCodDoc' AND object_id = OBJECT_ID('TabellaLegami'))
BEGIN
CREATE INDEX IX_TabellaLegamiCodDoc ON TabellaLegami (CodiceDocumento);
END



IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'NonClusteredIndex-20190115-082306' AND object_id = OBJECT_ID('MovimentiContabiliRiga'))
BEGIN
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190115-082306] ON  [MovimentiContabiliRiga]
(
	[DareAvere] ASC,
	[CENTROSERVIZIO] ASC,
	[TipoExtra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END



IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'NonClusteredIndex-20190115-082439' AND object_id = OBJECT_ID('MovimentiContabiliTesta'))
BEGIN
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190115-082439] ON  [MovimentiContabiliTesta]
(
	[NumeroRegistrazione] ASC,
	[AnnoCompetenza] ASC,
	[MeseCompetenza] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END













