
/****** STORED PROCEDURE ******/

IF OBJECT_ID('Prestazione_Tipologia_FindByName') IS NOT NULL DROP PROCEDURE Prestazione_Tipologia_FindByName

exec('
CREATE  PROCEDURE [Prestazione_Tipologia_FindByName] 
    @NameToFind nvarchar(250)
AS
    SELECT   
	    ID_Prestazione_Tipologia
        ,ID_ePersonam
       ,Chiave_Importazione
       ,[Descrizione]
    FROM   
        Prestazione_Tipologia
    WHERE  Chiave_Importazione like ''%'' + @NameToFind + ''%''
')

IF OBJECT_ID('Prestazione_Tipologia_List_for_Dropdown') IS NOT NULL DROP PROCEDURE Prestazione_Tipologia_List_for_Dropdown

exec('CREATE PROCEDURE [dbo].[Prestazione_Tipologia_List_for_Dropdown] 
    
AS
    SELECT   
		ID_Prestazione_Tipologia
       ,Descrizione
    FROM   
        dbo.Prestazione_Tipologia
')


IF OBJECT_ID('Prestazioni_GetByContrattoPrestazione') IS NOT NULL DROP PROCEDURE Prestazioni_GetByContrattoPrestazione

exec('CREATE PROCEDURE [dbo].[Prestazioni_GetByContrattoPrestazione] 
    @ID_Prestazione [Int],
	@Chiave_Import Varchar(250),
	@ID_FasciaISE [Int]
AS
    SELECT  
		 ID_Listino,importo
    FROM  
        dbo.Prestazioni_Listino  inner join [dbo].[Prestazioni_ContrattiDiServizio]

		on 
		dbo.Prestazioni_ContrattiDiServizio.ID_ContrattoDiServizio = dbo.Prestazioni_Listino.ID_ContrattoDiServizio


    WHERE (dbo.Prestazioni_Listino.ID_Prestazione_Tipologia = @ID_Prestazione)
	and dbo.Prestazioni_ContrattiDiServizio.Chiave_Import = @Chiave_import
	and dbo.Prestazioni_Listino.ID_FasciaISE = @ID_FasciaISE 
')

IF OBJECT_ID('Tabella_FasciaISE_List_for_Dropdown') IS NOT NULL DROP PROCEDURE Tabella_FasciaISE_List_for_Dropdown
exec('
CREATE PROCEDURE [dbo].[Tabella_FasciaISE_List_for_Dropdown]
AS
    SELECT 
        ID_FasciaISEE
       ,Descrizione
    FROM 
        dbo.Tabella_FasciaISE

')

IF OBJECT_ID('Prestazioni_Tipologie_Add') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_Add
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_Add]
(
    @ID_Prestazione_Tipologia [Int]
   ,@Codice_Prestazione       [NVarchar](50)
   ,@Chiave_Import            [NVarchar](200)
   ,@Descrizione              [NVarchar](500)
   ,@Note                     [Text]
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.Prestazioni_Tipologie
    (
        Codice_Prestazione
       ,Chiave_Import
       ,Descrizione
       ,Note
    )
    VALUES
    (
        @Codice_Prestazione
       ,@Chiave_Import
       ,@Descrizione
       ,@Note
    )
    SELECT 
        SCOPE_IDENTITY() AS ID_Prestazione_Tipologia
    COMMIT
')

IF OBJECT_ID('Prestazioni_Tipologie_FindByChiave_Import') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_FindByChiave_Import
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_FindByChiave_Import] 
    @Chiave_Import NVarchar(200)
AS
    SELECT   
        ID_Prestazione_Tipologia
       ,Codice_Prestazione
       ,Chiave_Import
       ,Descrizione
       ,Note
    FROM   
        dbo.Prestazioni_Tipologie
    WHERE Chiave_Import = @Chiave_Import
')

IF OBJECT_ID('Prestazioni_Tipologie_Get') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_Get
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_Get] 
    @ID_Prestazione_Tipologia [Int]
AS
    SELECT  
        ID_Prestazione_Tipologia
       ,Codice_Prestazione
       ,Chiave_Import
       ,Descrizione
       ,Note
    FROM  
        dbo.Prestazioni_Tipologie
    WHERE(ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
')

IF OBJECT_ID('Prestazioni_Tipologie_List') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_List
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_List] 
    @PageIndex Int = 0
   ,@PageSize  Int = 10000
AS
    SELECT 
        ID_Prestazione_Tipologia
       ,Codice_Prestazione
       ,Chiave_Import
       ,Descrizione
       ,Note
    FROM 
        dbo.Prestazioni_Tipologie
    ORDER BY 
        Descrizione
    OFFSET @PageSize * (@PageIndex) ROWS FETCH NEXT @PageSize ROWS ONLY
')

IF OBJECT_ID('Prestazioni_Tipologie_Update') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_Update
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_Update]
(
    @ID_Prestazione_Tipologia [Int]
   ,@Codice_Prestazione       [NVarchar](50)
   ,@Chiave_Import            [NVarchar](200)
   ,@Descrizione              [NVarchar](500)
   ,@Note                     [Text]
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.Prestazioni_Tipologie
        SET             
            Codice_Prestazione = @Codice_Prestazione,
            Chiave_Import = @Chiave_Import,
            Descrizione = @Descrizione,
            Note = @Note
    WHERE    
        (ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
    COMMIT
')

IF OBJECT_ID('Branche_Add') IS NOT NULL DROP PROCEDURE Branche_Add
exec('
CREATE PROCEDURE dbo.Branche_Add
(
    @ID_Branca   [Int]
   ,@Descrizione [NVarchar](200)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.Branche
    (
        Descrizione
    )
    VALUES
    (
        @Descrizione
    )
    SELECT 
        SCOPE_IDENTITY() AS ID_Branca
    COMMIT
')

IF OBJECT_ID('Branche_Delete') IS NOT NULL DROP PROCEDURE Branche_Delete
exec('
CREATE PROCEDURE dbo.Branche_Delete 
    @ID_Branca [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.Branche
    WHERE        
        (ID_Branca = @ID_Branca)
    COMMIT
')

IF OBJECT_ID('Branche_Get') IS NOT NULL DROP PROCEDURE Branche_Get
exec('
CREATE PROCEDURE dbo.Branche_Get 
    @ID_Branca [Int]
AS
    SELECT  
        ID_Branca
       ,Descrizione
    FROM  
        dbo.Branche
    WHERE(ID_Branca = @ID_Branca)
')

IF OBJECT_ID('Branche_List') IS NOT NULL DROP PROCEDURE Branche_List
exec('
CREATE PROCEDURE [dbo].[Branche_List]
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT 
            ID_Branca
           ,Descrizione
        FROM 
            dbo.Branche
        ORDER BY 
            Descrizione
    END
')

IF OBJECT_ID('Branche_List_By_ID_Prestazione_Tipologia') IS NOT NULL DROP PROCEDURE Branche_List_By_ID_Prestazione_Tipologia
exec('
CREATE PROCEDURE [dbo].[Branche_List_By_ID_Prestazione_Tipologia] 
    @ID_Prestazione_Tipologia Int
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT  
            Branche.ID_Branca
           ,Branche.Descrizione
        FROM  
            Branche
            INNER JOIN Prestazioni_Tipologie__Branche ON Branche.ID_Branca = Prestazioni_Tipologie__Branche.ID_Branca
        WHERE(Prestazioni_Tipologie__Branche.ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
        ORDER BY 
            Branche.Descrizione
    END
')

IF OBJECT_ID('Branche_Update') IS NOT NULL DROP PROCEDURE Branche_Update
exec('
CREATE PROCEDURE dbo.Branche_Update
(
    @ID_Branca   [Int]
   ,@Descrizione [NVarchar](200)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.Branche
        SET 
            Descrizione = @Descrizione
    WHERE    
        (ID_Branca = @ID_Branca)
    COMMIT
')

IF OBJECT_ID('Prestazione_Tipologia__Branca_Delete_By_ID_Prestazione_Tipologia') IS NOT NULL DROP PROCEDURE Prestazione_Tipologia__Branca_Delete_By_ID_Prestazione_Tipologia
exec('
CREATE PROCEDURE [dbo].[Prestazione_Tipologia__Branca_Delete_By_ID_Prestazione_Tipologia] 
    @ID_Prestazione_Tipologia [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.Prestazioni_Tipologie__Branche
    WHERE        
        (ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
    COMMIT
')

IF OBJECT_ID('Prestazione_Tipologia_FindByName') IS NOT NULL DROP PROCEDURE Prestazione_Tipologia_FindByName
exec('
CREATE  PROCEDURE [Prestazione_Tipologia_FindByName] 
    @NameToFind nvarchar(250)
AS
    SELECT   
	    ID_Prestazione_Tipologia
        ,ID_ePersonam
       ,Chiave_Importazione
       ,[Descrizione]
    FROM   
        Prestazione_Tipologia
    WHERE  Chiave_Importazione like ''%'' + @NameToFind + ''%''
')

IF OBJECT_ID('Prestazione_Tipologia_List_for_Dropdown') IS NOT NULL DROP PROCEDURE Prestazione_Tipologia_List_for_Dropdown
exec('
CREATE PROCEDURE [dbo].[Prestazione_Tipologia_List_for_Dropdown] 
    
AS
    SELECT   
		ID_Prestazione_Tipologia
       ,Descrizione
    FROM   
        dbo.Prestazione_Tipologia
GO
')
IF OBJECT_ID('Prestazioni_GetByContrattoPrestazione') IS NOT NULL DROP PROCEDURE Prestazioni_GetByContrattoPrestazione
exec('
CREATE PROCEDURE [dbo].[Prestazioni_GetByContrattoPrestazione] 
    @ID_Prestazione [Int],
	@Chiave_Import Varchar(250),
	@ID_FasciaISE [Int]
AS
    SELECT  
		 ID_Listino,importo
    FROM  
        dbo.Prestazioni_Listino  inner join [dbo].[Prestazioni_ContrattiDiServizio]

		on 
		dbo.Prestazioni_ContrattiDiServizio.ID_ContrattoDiServizio = dbo.Prestazioni_Listino.ID_ContrattoDiServizio


    WHERE (dbo.Prestazioni_Listino.ID_Prestazione_Tipologia = @ID_Prestazione)
	and dbo.Prestazioni_ContrattiDiServizio.Chiave_Import = @Chiave_import
	and dbo.Prestazioni_Listino.ID_FasciaISE = @ID_FasciaISE
')

IF OBJECT_ID('Prestazioni_Listino_Add') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_Add
exec('
CREATE PROCEDURE dbo.Prestazioni_Listino_Add
(
    @ID_Listino               [Int]
   ,@ID_Prestazione_Tipologia [Int]
   ,@ID_ContrattoDiServizio   [Int]
   ,@Importo                  [Money]
   ,@ID_FasciaISE             [Int]
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.Prestazioni_Listino
    (
        ID_Prestazione_Tipologia
       ,ID_ContrattoDiServizio
       ,Importo
       ,ID_FasciaISE
    )
    VALUES
    (
        @ID_Prestazione_Tipologia
       ,@ID_ContrattoDiServizio
       ,@Importo
       ,@ID_FasciaISE
    )
    SELECT 
        SCOPE_IDENTITY() AS ID_Listino
    COMMIT
')

IF OBJECT_ID('Prestazioni_Listino_Delete') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_Delete
exec('
CREATE PROCEDURE dbo.Prestazioni_Listino_Delete 
    @ID_Listino [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.Prestazioni_Listino
    WHERE        
        (ID_Listino = @ID_Listino)
    COMMIT
')

IF OBJECT_ID('Prestazioni_Listino_Get') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_Get
exec('
CREATE PROCEDURE dbo.Prestazioni_Listino_Get 
    @ID_Listino [Int]
AS
    SELECT  
        ID_Listino
       ,ID_Prestazione_Tipologia
       ,ID_ContrattoDiServizio
       ,Importo
       ,ID_FasciaISE
    FROM  
        dbo.Prestazioni_Listino
    WHERE(ID_Listino = @ID_Listino)
')

IF OBJECT_ID('Prestazioni_Listino_List') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_List
exec('
CREATE PROCEDURE dbo.Prestazioni_Listino_List 
    @PageIndex AS Integer = 0
   ,@PageSize AS  Integer = 100000
AS
    SELECT 
        ID_Listino
       ,ID_Prestazione_Tipologia
       ,ID_ContrattoDiServizio
       ,Importo
       ,ID_FasciaISE
    FROM 
        dbo.Prestazioni_Listino
    ORDER BY 
        ID_Prestazione_Tipologia
    OFFSET @PageSize * (@PageIndex) ROWS FETCH NEXT @PageSize ROWS ONLY
')

IF OBJECT_ID('Prestazioni_Listino_List_By_ID_Prestazione_Tipologia') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_List_By_ID_Prestazione_Tipologia
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Listino_List_By_ID_Prestazione_Tipologia] 
    @ID_Prestazione_Tipologia Int
AS
    SELECT  
        Prestazioni_Listino.ID_Listino
       ,Prestazioni_Listino.ID_Prestazione_Tipologia
       ,Prestazioni_Listino.ID_ContrattoDiServizio
       ,Prestazioni_Listino.Importo
       ,Prestazioni_Listino.ID_FasciaISE
       ,Prestazioni_ContrattiDiServizio.Descrizione AS ContrattoDiServizio
       ,Tabella_FasciaISE.Descrizione AS FasciaISEE
    FROM  
        Prestazioni_Listino
        INNER JOIN Prestazioni_ContrattiDiServizio ON Prestazioni_Listino.ID_ContrattoDiServizio = Prestazioni_ContrattiDiServizio.ID_ContrattoDiServizio
        LEFT OUTER JOIN Tabella_FasciaISE ON Prestazioni_Listino.ID_FasciaISE = Tabella_FasciaISE.ID_FasciaISEE
    WHERE(Prestazioni_Listino.ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
    ORDER BY 
        Prestazioni_Listino.ID_ContrattoDiServizio
')

IF OBJECT_ID('Prestazioni_Listino_Update') IS NOT NULL DROP PROCEDURE Prestazioni_Listino_Update
exec('
CREATE PROCEDURE dbo.Prestazioni_Listino_Update
(
    @ID_Listino               [Int]
   ,@ID_Prestazione_Tipologia [Int]
   ,@ID_ContrattoDiServizio   [Int]
   ,@Importo                  [Money]
   ,@ID_FasciaISE             [Int]
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.Prestazioni_Listino
        SET 
            ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia,
            ID_ContrattoDiServizio = @ID_ContrattoDiServizio,
            Importo = @Importo,
            ID_FasciaISE = @ID_FasciaISE
    WHERE    
        (ID_Listino = @ID_Listino)
    COMMIT
')

IF OBJECT_ID('Prestazioni_Tipologie__Branche_Add') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie__Branche_Add
exec('
CREATE PROCEDURE dbo.Prestazioni_Tipologie__Branche_Add
(
    @ID_Prestazione_Tipologia [Int]
   ,@ID_Branca                [Int]
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.Prestazioni_Tipologie__Branche
    (
        ID_Prestazione_Tipologia
       ,ID_Branca
    )
    VALUES
    (
        @ID_Prestazione_Tipologia
       ,@ID_Branca
    )
    COMMIT
')

IF OBJECT_ID('Prestazioni_Tipologie__Branche_Delete') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie__Branche_Delete
exec('
CREATE PROCEDURE dbo.Prestazioni_Tipologie__Branche_Delete 
    @ID_Prestazione_Tipologia [Int]
   ,@ID_Branca                [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.Prestazioni_Tipologie__Branche
    WHERE        
        (ID_Branca = @ID_Branca
         AND ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
')

IF OBJECT_ID('Prestazioni_Tipologie_ListForElenco') IS NOT NULL DROP PROCEDURE Prestazioni_Tipologie_ListForElenco
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie_ListForElenco] 
    @ID_Branca      Int          = 0
   ,@StringaRicerca NVarchar(50) = ''''
   ,@PageIndex      Int          = 0
   ,@PageSize       Int          = 10000
AS
    SELECT  distinct(
        Prestazioni_Tipologie.ID_Prestazione_Tipologia)
       ,Prestazioni_Tipologie.Codice_Prestazione
       ,Prestazioni_Tipologie.Chiave_Import
       ,Prestazioni_Tipologie.Descrizione
       ,Prestazioni_Tipologie.Note
    FROM  
        Prestazioni_Tipologie
        LEFT OUTER JOIN Prestazioni_Tipologie__Branche ON Prestazioni_Tipologie.ID_Prestazione_Tipologia = Prestazioni_Tipologie__Branche.ID_Prestazione_Tipologia
    WHERE((Prestazioni_Tipologie__Branche.ID_Branca = @ID_Branca)
          OR (@ID_Branca = 0))
         AND ((Prestazioni_Tipologie.Descrizione LIKE ''%'' + @StringaRicerca + ''%'')
              OR (@StringaRicerca = ''''))
    ORDER BY 
        Prestazioni_Tipologie.Descrizione
    OFFSET @PageSize * (@PageIndex) ROWS FETCH NEXT @PageSize ROWS ONLY
')

IF OBJECT_ID('Tabella_FasciaISE_List_for_Dropdown') IS NOT NULL DROP PROCEDURE Tabella_FasciaISE_List_for_Dropdown
exec('
CREATE PROCEDURE [dbo].[Tabella_FasciaISE_List_for_Dropdown]
AS
    SELECT 
        ID_FasciaISEE
       ,Descrizione
    FROM 
        dbo.Tabella_FasciaISE
')



IF OBJECT_ID('Prestazioni_Tipologie__ePersonam_exams_Get') IS NOT NULL DROP PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Get]
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Get] 
    @ID_Prestazione_Tipologia__ePersonam_exams [Int]
AS
    SELECT  
        ID_Prestazione_Tipologia__ePersonam_exams
       ,ID_Prestazione_Tipologia
       ,Codice_ePersonam
       ,Descrizione_ePersonam
       ,Tipologia_ePersonam
    FROM  
        dbo.Prestazioni_Tipologie__ePersonam_exams
    WHERE(ID_Prestazione_Tipologia__ePersonam_exams = @ID_Prestazione_Tipologia__ePersonam_exams)
')

IF OBJECT_ID('Prestazioni_Tipologie__ePersonam_exams_List_by_ID_Prestazione_Tipologia') IS NOT NULL DROP PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_List_by_ID_Prestazione_Tipologia]
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_List_by_ID_Prestazione_Tipologia] 
    @ID_Prestazione_Tipologia [Int]
AS
    SELECT  
        ID_Prestazione_Tipologia__ePersonam_exams
       ,ID_Prestazione_Tipologia
       ,Codice_ePersonam
       ,Descrizione_ePersonam
       ,Tipologia_ePersonam
    FROM  
        dbo.Prestazioni_Tipologie__ePersonam_exams
    WHERE(ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia)
')


IF OBJECT_ID('Prestazioni_Tipologie__ePersonam_exams_Add') IS NOT NULL DROP PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Add]
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Add]
(
    @ID_Prestazione_Tipologia__ePersonam_exams Int
   ,@ID_Prestazione_Tipologia                  [Int]
   ,@Codice_ePersonam                          [NVarchar](200)
   ,@Descrizione_ePersonam                     [NVarchar](50)
   ,@Tipologia_ePersonam                       [NVarchar](50)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.Prestazioni_Tipologie__ePersonam_exams
    (
        ID_Prestazione_Tipologia
       ,Codice_ePersonam
       ,Descrizione_ePersonam
       ,Tipologia_ePersonam
    )
    VALUES
    (
        @ID_Prestazione_Tipologia
       ,@Codice_ePersonam
       ,@Descrizione_ePersonam
       ,@Tipologia_ePersonam
    )
    SELECT 
        SCOPE_IDENTITY() AS ID_Prestazione_Tipologia__ePersonam_exams
    COMMIT
')

IF OBJECT_ID('Prestazioni_Tipologie__ePersonam_exams_Update') IS NOT NULL DROP PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Update]
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Update]
(
    @ID_Prestazione_Tipologia__ePersonam_exams [Int]
   ,@ID_Prestazione_Tipologia                  [Int]
   ,@Codice_ePersonam                          [NVarchar](200)
   ,@Descrizione_ePersonam                     [NVarchar](50)
   ,@Tipologia_ePersonam                       [NVarchar](50)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.Prestazioni_Tipologie__ePersonam_exams
        SET             
            ID_Prestazione_Tipologia = @ID_Prestazione_Tipologia,
            Codice_ePersonam = @Codice_ePersonam,
            Descrizione_ePersonam = @Descrizione_ePersonam,
            Tipologia_ePersonam = @Tipologia_ePersonam
    WHERE    
        (ID_Prestazione_Tipologia__ePersonam_exams = @ID_Prestazione_Tipologia__ePersonam_exams)
    COMMIT
 ')

IF OBJECT_ID('Prestazioni_Tipologie__ePersonam_exams_Delete') IS NOT NULL DROP PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Delete]
exec('
CREATE PROCEDURE [dbo].[Prestazioni_Tipologie__ePersonam_exams_Delete] 
    @ID_Prestazione_Tipologia__ePersonam_exams [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.Prestazioni_Tipologie__ePersonam_exams
    WHERE        
        (ID_Prestazione_Tipologia__ePersonam_exams = @ID_Prestazione_Tipologia__ePersonam_exams)
    COMMIT
')




