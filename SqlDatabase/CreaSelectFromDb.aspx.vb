﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class SqlDatabase_CreaSelectFromDb
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        
        Esporta(Session("DC_OSPITE"), "OSPITE")
        Esporta(Session("DC_TABELLE"), "TABELLE")
        Esporta(Session("DC_GENERALE"), "GENERALE")
        Esporta(Session("DC_OSPITIACCESSORI"), "OSPITIACCESSORI")

    End Sub


    Private Sub Esporta(ByVal condizione As String, ByVal nome As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim ExpMySql As String = ""


        cn = New Data.OleDb.OleDbConnection(condizione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT *  FROM sysobjects WHERE  xtype= 'U' and (not name like 'sys_%') and (not name like 'INFORMATION%') and (not name like 'Errori%')")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ExpMySql = ExpMySql & "IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'" & campodb(myPOSTreader.Item("name")).Replace("[dbo].", "") & "' AND xtype = N'U')" & vbNewLine
            ExpMySql = ExpMySql & "BEGIN" & vbNewLine
            ExpMySql = ExpMySql & "CREATE TABLE " & campodb(myPOSTreader.Item("name")).Replace("[dbo].", "") & "([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];" & vbNewLine
            ExpMySql = ExpMySql & "END" & vbNewLine & vbNewLine

            If campodb(myPOSTreader.Item("name")).IndexOf(" ") < 1 Then
                MySql = "SELECT TABLE_NAME, COLUMN_NAME, COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID') AS COLUMN_ID, * "
                MySql = MySql & " FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & campodb(myPOSTreader.Item("name")) & "';"
                Dim cmdI As New OleDbCommand()
                cmdI.CommandText = MySql
                cmdI.Connection = cn
                Dim ReadCD As OleDbDataReader = cmdI.ExecuteReader()
                Do While ReadCD.Read

                    If campodb(ReadCD.Item("COLUMN_NAME")).ToUpper <> "ID" Then
                        If campodb(ReadCD.Item("DATA_TYPE")) = "nvarchar" Or campodb(ReadCD.Item("DATA_TYPE")) = "varchar" Then
                            ExpMySql = ExpMySql & "IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'" & campodb(ReadCD.Item("COLUMN_NAME")) & "' AND Object_ID = Object_ID(N'" & campodb(myPOSTreader.Item("name")) & "'))" & vbNewLine
                            ExpMySql = ExpMySql & "BEGIN" & vbNewLine
                            If campodb(ReadCD.Item("CHARACTER_MAXIMUM_LENGTH")) = -1 Then
                                ExpMySql = ExpMySql & "ALTER TABLE " & campodb(myPOSTreader.Item("name")) & " ADD [" & campodb(ReadCD.Item("COLUMN_NAME")) & "] " & campodb(ReadCD.Item("DATA_TYPE")) & " (Max)" & ";" & vbNewLine
                            Else
                                ExpMySql = ExpMySql & "ALTER TABLE " & campodb(myPOSTreader.Item("name")) & " ADD [" & campodb(ReadCD.Item("COLUMN_NAME")) & "] " & campodb(ReadCD.Item("DATA_TYPE")) & " (" & campodb(ReadCD.Item("CHARACTER_MAXIMUM_LENGTH")) & ")" & ";" & vbNewLine
                            End If
                            ExpMySql = ExpMySql & "END" & vbNewLine & vbNewLine
                        Else
                            ExpMySql = ExpMySql & "IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'" & campodb(ReadCD.Item("COLUMN_NAME")) & "' AND Object_ID = Object_ID(N'" & campodb(myPOSTreader.Item("name")) & "'))" & vbNewLine
                            ExpMySql = ExpMySql & "BEGIN" & vbNewLine
                            ExpMySql = ExpMySql & "ALTER TABLE " & campodb(myPOSTreader.Item("name")) & " ADD [" & campodb(ReadCD.Item("COLUMN_NAME")) & "] " & campodb(ReadCD.Item("DATA_TYPE")) & ";" & vbNewLine
                            ExpMySql = ExpMySql & "END" & vbNewLine & vbNewLine
                        End If
                    End If
                Loop
                ReadCD.Close()
            End If
        Loop
        myPOSTreader.Close()

        cn.Close()


        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\" & nome & ".sql")

        tw.WriteLine(ExpMySql)
        tw.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
