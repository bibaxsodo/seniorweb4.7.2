﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class AddSocieta
    Inherits System.Web.UI.Page

    Protected Sub Btn_CreaSocieta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CreaSocieta.Click
        Dim StringaConnessioneSenior As String
        Dim StringaIstanza As String
        If Trim(Txt_StringaConnessione.Text) = "" Then
            lbl_output.Text = "Indica stringa connessione"
            Exit Sub
        End If

        If Trim(Txt_IstanzaDB.Text) = "" Then
            lbl_output.Text = "Indica stringa connessione"
            Exit Sub
        End If

        StringaConnessioneSenior = Txt_StringaConnessione.Text
        StringaIstanza = Txt_IstanzaDB.Text


        If Txt_NomeSocieta.Text.IndexOf(" ") > 0 Then
            lbl_output.Text = "Errore non puoi inserire uno spazio in nome società"
            Exit Sub
        End If

        If Trim(Txt_NomeSocieta.Text) = "" Then
            lbl_output.Text = "Devi indicare il nome società"
            Exit Sub
        End If



        Dim StringaConnessioneTabelle As String
        Dim StringaConnessioneGenerale As String
        Dim StringaConnessioneOspiti As String
        Dim StringaConnessioneOspitiAccessori As String

        StringaConnessioneTabelle = StringaIstanza.Replace("Initial Catalog=MASTER;", "Initial Catalog=Senior_" & Txt_NomeSocieta.Text & "_Tabelle;")


        StringaConnessioneOspitiAccessori = StringaIstanza.Replace("Initial Catalog=MASTER;", "Initial Catalog=Senior_" & Txt_NomeSocieta.Text & "_OspitiAccessori;")

        StringaConnessioneOspiti = StringaIstanza.Replace("Initial Catalog=MASTER;", "Initial Catalog=Senior_" & Txt_NomeSocieta.Text & "_Ospiti;")

        StringaConnessioneGenerale = StringaIstanza.Replace("Initial Catalog=MASTER;", "Initial Catalog=Senior_" & Txt_NomeSocieta.Text & "_Generale;")


        Dim cn As OleDbConnection
        Dim MaxCliente As Integer

        cn = New Data.OleDb.OleDbConnection(StringaIstanza)

        cn.Open()


        Try
            Dim cmdCreateDB As New OleDbCommand()
            cmdCreateDB.CommandText = ("create database senior_" & Txt_NomeSocieta.Text & "_ospiti;")
            cmdCreateDB.Connection = cn
            cmdCreateDB.ExecuteNonQuery()
        Catch ex As Exception

        End Try


        Try
            Dim cmdCreateDB As New OleDbCommand()
            cmdCreateDB.CommandText = ("create database senior_" & Txt_NomeSocieta.Text & "_ospitiaccessori;")
            cmdCreateDB.Connection = cn
            cmdCreateDB.ExecuteNonQuery()
        Catch ex As Exception

        End Try


        Try
            Dim cmdCreateDB As New OleDbCommand()
            cmdCreateDB.CommandText = ("create database senior_" & Txt_NomeSocieta.Text & "_generale;")
            cmdCreateDB.Connection = cn
            cmdCreateDB.ExecuteNonQuery()
        Catch ex As Exception

        End Try


        Try
            Dim cmdCreateDB As New OleDbCommand()
            cmdCreateDB.CommandText = ("create database senior_" & Txt_NomeSocieta.Text & "_tabelle;")
            cmdCreateDB.Connection = cn
            cmdCreateDB.ExecuteNonQuery()
        Catch ex As Exception

        End Try

        cn.Close()

        Dim cn2 As OleDbConnection
        cn2 = New Data.OleDb.OleDbConnection(StringaConnessioneSenior)

        cn2.Open()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Cliente) from ArchivioClienti")
        cmd.Connection = cn2

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxCliente = Val(campodb(myPOSTreader.Item(0))) + 1
        End If
        myPOSTreader.Close()


        Dim cmdIns As New OleDbCommand()
        Dim MySql As String
        MySql = "INSERT INTO ArchivioClienti ([CLIENTE],[TABELLE],[GENERALE],[FINANZIARIA],[OSPITI],[OSPITIACCESSORI],[TURNI],[RAGIONESOCIALE],[LIVEIDUSER],[LIVEIDPASSWORD],[ODV],[NomeEPersonam]) values (?,?,?,?,?,?,?,?,?,?,?,?)"
        cmdIns.CommandText = MySql
        cmdIns.Connection = cn2

        cmdIns.Parameters.AddWithValue("@CLIENTE", MaxCliente)
        cmdIns.Parameters.AddWithValue("@TABELLE", StringaConnessioneTabelle)
        cmdIns.Parameters.AddWithValue("@GENERALE", StringaConnessioneGenerale)
        cmdIns.Parameters.AddWithValue("@FINANZIARIA", "")
        cmdIns.Parameters.AddWithValue("@OSPITI", StringaConnessioneOspiti)
        cmdIns.Parameters.AddWithValue("@OSPITIACCESSORI", StringaConnessioneOspitiAccessori)
        cmdIns.Parameters.AddWithValue("@TURNI", "")
        cmdIns.Parameters.AddWithValue("@RAGIONESOCIALE", Txt_NomeEpersonam.Text)
        cmdIns.Parameters.AddWithValue("@LIVEIDUSER", "")
        cmdIns.Parameters.AddWithValue("@LIVEIDPASSWORD", "")
        cmdIns.Parameters.AddWithValue("@ODV", "")
        cmdIns.Parameters.AddWithValue("@NOMEEPERSONAM", Txt_NomeEpersonam.Text)
        cmdIns.ExecuteNonQuery()

        cn2.Close()

        lbl_output.Text = "Operazione effettuato"

        Response.Redirect("SelezionaSocieta.aspx")

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_SODO") <> True Then

            Response.Redirect("login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), "", Page)

        Txt_StringaConnessione.Text = Application("SENIOR")

        Txt_IstanzaDB.Text = Application("SENIOR").ToString.Replace("Initial Catalog=SENIOR;", "Initial Catalog=MASTER;")
    End Sub

    Protected Sub Btn_Home_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Home.Click
        Response.Redirect("SelezionaSocieta.aspx")
    End Sub
End Class

