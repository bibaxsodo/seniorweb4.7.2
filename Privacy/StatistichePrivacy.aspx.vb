﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class Privacy_StatistichePrivacy
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Utente", GetType(String))
        MyTable.Columns.Add("Pagina", GetType(String))
        MyTable.Columns.Add("STRUTTURA", GetType(String))
        MyTable.Columns.Add("CENTRO SERVIZIO", GetType(String))
        MyTable.Columns.Add("NOME OSPITE", GetType(String))
        MyTable.Columns.Add("NOME UTENTE", GetType(String))
        MyTable.Columns.Add("NUMERO REGISTRAZIONE", GetType(String))
        MyTable.Columns.Add("INTESTATARIO REGISTRAZIONE", GetType(String))
        MyTable.Columns.Add("DESCRIZIONE", GetType(String))
        MyTable.Columns.Add("TIPO", GetType(String))
        MyTable.Columns.Add("DATAORA", GetType(String))
        
        Dim cmd As New OleDbCommand()



        cmd.CommandText = "Select * From [LogPrivacy] Where [DataOra] >= ? And [DataOra] <= ? ORDER BY DATAORA"

        If DD_UTENTE.SelectedValue <> "" Then
            cmd.CommandText = "Select * From [LogPrivacy] Where [DataOra] >= ? And [DataOra] <= ? And Utente Like '" & DD_UTENTE.SelectedValue & "' ORDER BY DATAORA"
        End If
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataDal)
        cmd.Parameters.AddWithValue("@DataAl", DataAl)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("Utente"))
            myriga(1) = campodb(myPOSTreader.Item("Pagina")).Replace("_ASPX", "")
            myriga(2) = campodb(myPOSTreader.Item("STRUTTURA"))            
            Dim CSERV As New Cls_CentroServizio
            CSERV.DESCRIZIONE = ""
            If campodb(myPOSTreader.Item("CSERV")) <> "" Then
                CSERV.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CSERV")))
                myriga(3) = CSERV.DESCRIZIONE
            Else
                myriga(3) = ""
            End If
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            If Ospite.CodiceOspite > 0 Then
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                myriga(4) = Ospite.Nome
            Else
                myriga(4) = ""
            End If


            Dim Parente As New Cls_Parenti

            Parente.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            Parente.CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            If Parente.CodiceParente > 0 Then
                Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                myriga(5) = Parente.Nome
            Else
                myriga(5) = ""
            End If

            myriga(6) = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            If Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))) > 0 Then
                Dim Mov As New Cls_MovimentoContabile
                Try

                    Mov.Leggi(Session("DC_GENERALE"), Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))

                    Dim PianConti As New Cls_Pianodeiconti

                    PianConti.Mastro = Mov.Righe(0).MastroPartita
                    PianConti.Conto = Mov.Righe(0).ContoPartita
                    PianConti.Sottoconto = Mov.Righe(0).SottocontoPartita
                    PianConti.Decodfica(Session("DC_GENERALE"))
                    myriga(7) = PianConti.Descrizione
                Catch ex As Exception
                    myriga(7) = ""
                End Try
            Else
                myriga(7) = ""
            End If


            myriga(8) = campodb(myPOSTreader.Item("CODIFICA")) & " " & campodb(myPOSTreader.Item("Nome"))
            myriga(9) = campodb(myPOSTreader.Item("TIPO"))
            myriga(10) = campodb(myPOSTreader.Item("DATAORA"))

            MyTable.Rows.Add(myriga)
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim ConnectionString As String = Application("SENIOR")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim CLIENTE As Integer
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()


        MySql = "Select * from ArchivioClienti where [OSPITI] = ? "
        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@OSPITI", Session("DC_OSPITE"))
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CLIENTE = Val(campodb(myPOSTreader.Item("CLIENTE")))
        End If
        myPOSTreader.Close()

        DD_UTENTE.Items.Clear()
        Dim cmdu As New OleDbCommand()
        cmdu.CommandText = "SELECT * FROM UTENTE WHERE CLIENTE = ?"
        cmdu.Parameters.AddWithValue("@CLIENTE", CLIENTE)
        cmdu.Connection = cn
        Dim UTENTERD As OleDbDataReader = cmdu.ExecuteReader()
        Do While UTENTERD.Read
            DD_UTENTE.Items.Add(campodb(UTENTERD.Item("UTENTE")))
            DD_UTENTE.Items(DD_UTENTE.Items.Count - 1).Value = campodb(UTENTERD.Item("UTENTE"))
        Loop
        UTENTERD.Close()

        DD_UTENTE.Items.Add("Tutti")
        DD_UTENTE.Items(DD_UTENTE.Items.Count - 1).Value = ""

        cn.Close()

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If

        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Mailing.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("/seniorweb/MainMenu.aspx")
    End Sub



End Class
