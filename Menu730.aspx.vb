﻿
Partial Class Menu730
    Inherits System.Web.UI.Page

    Protected Sub Btn_Crea730_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Crea730.Click
        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))


        Response.Redirect("https://seniorweb.e-personam.com/Invio730TS/ImportExcel730.aspx?Utente=" & Session("UTENTE") & "&Societa=" & k.RagioneSociale & "&Data=" & Format(Now, "dd/MM/yyyy") & "&Url=" & Request.Url.ToString)

    End Sub

    Protected Sub Btn_Invia730_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Invia730.Click
        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))


        Response.Redirect("http://seniorweb.e-personam.com/Invio730TS/Invio730TS.aspx?Utente=" & Session("UTENTE") & "&Societa=" & k.RagioneSociale & "&Data=" & Format(Now, "dd/MM/yyyy") & "&Url=" & Request.Url.ToString)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


    End Sub
End Class
