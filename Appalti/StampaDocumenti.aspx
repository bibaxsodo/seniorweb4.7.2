﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_StampaDocumenti" CodeFile="StampaDocumenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Stampa Documenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse - Stampa Documenti</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <!--<span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span> -->
                            <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Stampa" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Stampa Documenti                   
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Anno :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_AnnoRif" AutoPostBack="True" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    (Anno Contabile)
 <br />
                                    <br />

                                    <label class="LabelCampo">Anno :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_Anno" AutoPostBack="True" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Mese :</label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="DD_Mese" OnSelectedIndexChanged="DD_Mese_SelectedIndexChanged" AutoPostBack="True"
                                        runat="server" Width="128px">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Registro :</label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="DD_Registro" OnTextChanged="DD_Registro_TextChanged" AutoPostBack="True" runat="server"></asp:DropDownList>

                                    <asp:CheckBox ID="Chk_Anticipo" runat="server" />
                                    Fattura Anticipo<br />
                                    <br />

                                    <label class="LabelCampo">Dal Documento :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_DalDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Al Documento :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_AlDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server"></asp:Label>
                                    <br />
                                    <div id="msg"></div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                            </asp:Timer>
                                            <asp:Label ID="Lbl_Waiting" runat="server" Text="Label"></asp:Label>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>


                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
