﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading

Partial Class Appalti_StampaDocumenti
    Inherits System.Web.UI.Page


    Dim myRequest As System.Net.WebRequest
    Private Delegate Sub DoWorkDelegate()
    Private _work As DoWorkDelegate
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoErrori As String

    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0

        MySql = "SELECT MIN(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If Chk_Anticipo.Checked = True Then
            MySql = MySql & " AND (FatturaDiAnticipo <> 'S' OR FatturaDiAnticipo Is Null)"
        End If
        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()



    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MassimoDocumento = 0



        MySql = "SELECT MAX(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If Chk_Anticipo.Checked = True Then
            MySql = MySql & " AND (FatturaDiAnticipo <> 'S' OR FatturaDiAnticipo Is Null)"
        End If
        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function









    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        CampoTimer = "ST_" & Session("UTENTE") & "_T"
        CampoProgressBar = "ST_" & Session("UTENTE") & "_PB"
        CampoErrori = "ST_" & Session("UTENTE") & "_ER"


        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione


        If Val(Request.Item("ANNO")) > 0 Then
            Txt_AnnoRif.Text = Val(Request.Item("ANNO"))
            Txt_Anno.Text = Val(Request.Item("AnnoCompetenza"))
            DD_Mese.SelectedValue = Val(Request.Item("MeseDocumento"))
            Txt_DalDocumento.Text = Val(Request.Item("NumeroDal"))
            Txt_AlDocumento.Text = Val(Request.Item("NumeroDal"))
            DD_Registro.SelectedValue = Val(Request.Item("RegistroIVA"))
        End If

        
        Session("CampoProgressBar") = 0
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
    End Sub



    Private Sub DoWork(ByVal data As Object)
        Dim k As New Cls_StampaFatture

        k.CreaRecordStampa(Txt_Anno.Text, DD_Mese.SelectedValue, Chk_Anticipo.Checked, Val(Txt_DalDocumento.Text), Val(Txt_AlDocumento.Text), Txt_AnnoRif.Text, Val(DD_Registro.SelectedValue), "", "", "", 0, data)
    End Sub



    Private Sub WorkCompleted(ByVal result As IAsyncResult)
        _work = Nothing
    End Sub




    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) = 999 Then
            System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
            Lbl_Waiting.Text = ""
            Lbl_Errori.Text = "<p align=left>" & System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) & "</p>"
            Timer1.Enabled = False
            Exit Sub
        End If

        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 100 Then
            Lbl_Waiting.Text = ""
            Timer1.Enabled = False

            Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)
            Session("stampa") = System.Web.HttpRuntime.Cache("stampa" + Session.SessionID)
            
            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))

            If Session("Download") = "SI" Then
                
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&EXPORT=PDF','Stampe4','width=800,height=600');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
            End If


            Exit Sub
        End If
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 0 Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged

        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub DD_Mese_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mese.SelectedIndexChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Txt_AnnoRif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_AnnoRif.TextChanged
        Try
            Txt_DalDocumento.Text = MinimoDocumento()
            Txt_AlDocumento.Text = MassimoDocumento()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Appoggio As String
        If Val(Txt_DalDocumento.Text) = 0 Then
            Appoggio = "Numero Documento Dal obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Val(Txt_AlDocumento.Text) = 0 Then
            Appoggio = "Numero Documento al obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If DD_Registro.SelectedValue = 0 Then
            Appoggio = "Registro Iva obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        Timer1.Enabled = True
        Session("CampoProgressBar") = 0
        Session("RagioneSocialeFattura") = ""
        Session("CampoErrori") = ""
        Session("Download") = "NO"

        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
        System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("Download" + Session.SessionID) = "NO"

        'DoWork(Session)


        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")

    End Sub

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        Dim Appoggio As String
        If Val(Txt_DalDocumento.Text) = 0 Then
            Appoggio = "Numero Documento Dal obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Val(Txt_AlDocumento.Text) = 0 Then
            Appoggio = "Numero Documento al obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If DD_Registro.SelectedValue = 0 Then
            Appoggio = "Registro Iva obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        Timer1.Enabled = True
        Session("CampoProgressBar") = 0
        Session("RagioneSocialeFattura") = ""
        Session("CampoErrori") = ""
        Session("Download") = "SI"

        'DoWork(Session)


        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub
End Class

