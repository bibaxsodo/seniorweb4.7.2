﻿
Partial Class Appalti_Mansioni
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Struttura As New Cls_AppaltiTabellaStruttura


        Struttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)

        If Request.Item("CODICE") = "" Then
            Call EseguiJS()
            Exit Sub
        End If



        Dim Mansioni As New Cls_AppaltiTabellaMansione


        Mansioni.ID = Request.Item("CODICE")
        Mansioni.Leggi(Session("DC_TABELLE"), Mansioni.ID)


        Txt_Codice.Text = Mansioni.ID

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = Mansioni.Descrizione
        Txt_MansioneImport.Text = Mansioni.CodificaImport
        DD_Struttura.SelectedValue = Mansioni.Struttura

        Txt_MansioneImport2.Text = Mansioni.CodificaImport2


        If Mansioni.Festivo = 1 Then
            Chk_Festivita.Checked = True
        Else
            Chk_Festivita.Checked = False
        End If

        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        'If Txt_Codice.Text.Trim = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
        '    Exit Sub
        'End If

        Dim Mansioni As New Cls_AppaltiTabellaMansione
        Mansioni.ID = Val(Txt_Codice.Text)


        Mansioni.Descrizione = Txt_Descrizione.Text
        Mansioni.CodificaImport = Txt_MansioneImport.Text
        Mansioni.CodificaImport2 = Txt_MansioneImport2.Text
        Mansioni.Struttura = DD_Struttura.SelectedValue


        If Chk_Festivita.Checked = True Then
            Mansioni.Festivo = 1
        Else
            Mansioni.Festivo = 0
        End If

        
        Mansioni.Scrivi(Session("DC_TABELLE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If
        Dim TpAd As New Cls_AppaltiTabellaStruttura

        TpAd.ID = Txt_Codice.Text
        TpAd.Elimina(Session("DC_TABELLE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});  "
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_MasimaleDomiciliari')!= null) ) {  " & vbNewLine
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); " & vbNewLine
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });" & vbNewLine
        MyJs = MyJs & "    }" & vbNewLine


        'Txt_MasimaleDomiciliari

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Mansioni.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True

        Call EseguiJS()
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_AppaltiTabellaMansione

            x.ID = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_TABELLE"), x.ID)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_AppaltiTabellaMansione

                x.ID = 0
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione)

                If x.ID <> 0 Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub
End Class
