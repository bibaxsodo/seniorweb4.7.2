﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Appalti_Elenco_Struttura
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabUtente As New System.Data.DataTable("TabUtente")

    Private Sub DecodificaCodiceEpersonam()
        Dim Token As String

        Token = LoginPersonam(Context)
        Dim Url As String
        Dim rawresp As String

        Url = "https://api.e-personam.com/api/v1.0/business_units"

        Dim client As HttpWebRequest = WebRequest.Create(Url)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing



        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        rawresp = reader.ReadToEnd()



        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim IndiceRighe As Integer = 0
        Dim VtTipo(1000) As String
        Dim VtTipoCodice(1000) As String
        Dim VtPadre(1000) As Integer



        For Each jTok1 As JToken In jResults

            If jTok1.Item("structure").ToString.ToUpper = "true".ToUpper Then
                Dim Cerca As Integer
                Dim trovato As Boolean = False

                For Cerca = 0 To IndiceRighe - 1
                    If VtTipoCodice(Cerca) = jTok1.Item("id") Then
                        trovato = True
                    End If
                Next

                If Not trovato Then
                    IndiceRighe = IndiceRighe + 1
                    VtTipo(IndiceRighe) = jTok1.Item("name").ToString
                    VtTipoCodice(IndiceRighe) = jTok1.Item("id")
                    VtPadre(IndiceRighe) = 0
                End If
            End If


            If Not IsNothing(jTok1.Item("substructures")) Then
                For Each jTok2 As JToken In jTok1.Item("substructures")

                    Dim Cerca As Integer
                    Dim trovato As Boolean = False

                    For Cerca = 0 To IndiceRighe - 1
                        If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                            VtPadre(Cerca) = jTok1.Item("id")
                            trovato = True
                        End If
                    Next
                    If Not trovato Then
                        IndiceRighe = IndiceRighe + 1
                        VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                        VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                        VtPadre(IndiceRighe) = jTok1.Item("id")
                    End If

                    If Not IsNothing(jTok2.Item("units")) Then
                        For Each jTok3 As JToken In jTok2.Item("units")
                            '5214
                            If Val(jTok3.Item("id").ToString) = 5214 Then
                                trovato = False

                            End If

                            trovato = False
                            For Cerca = 0 To IndiceRighe - 1
                                If VtTipoCodice(Cerca) = jTok3.Item("id") Then
                                    VtPadre(Cerca) = jTok3.Item("id")
                                    trovato = True
                                End If
                            Next
                            If Not trovato Then
                                IndiceRighe = IndiceRighe + 1
                                VtTipo(IndiceRighe) = jTok3.Item("name").ToString
                                VtTipoCodice(IndiceRighe) = jTok3.Item("id")
                                VtPadre(IndiceRighe) = jTok2.Item("id")
                            End If

                        Next
                    End If
                Next
            End If

        Next


        TabUtente.Clear()
        TabUtente.Columns.Add("Codice", GetType(String))
        TabUtente.Columns.Add("Descrizione", GetType(String))
        TabUtente.Columns.Add("Livello", GetType(String))



        Dim Ordina As Integer
        Dim OrdinaX As Integer

        For Ordina = 1 To IndiceRighe
            If VtPadre(Ordina) = 0 Then

                Dim myriga As System.Data.DataRow = TabUtente.NewRow()

                myriga(0) = VtTipoCodice(Ordina)
                myriga(1) = VtTipo(Ordina)
                myriga(2) = "1"
                TabUtente.Rows.Add(myriga)

                Dim Indicex As Integer

                For Indicex = 0 To IndiceRighe
                    If VtPadre(Indicex) = VtTipoCodice(Ordina) Then


                        Dim myrigaL2 As System.Data.DataRow = TabUtente.NewRow()

                        myrigaL2(0) = VtTipoCodice(Indicex)
                        myrigaL2(1) = VtTipo(Indicex)
                        myrigaL2(2) = "2"
                        TabUtente.Rows.Add(myrigaL2)


                        Dim Indicey As Integer

                        For Indicey = 0 To IndiceRighe
                            If VtPadre(Indicey) = VtTipoCodice(Indicex) Then
                                
                                Dim myrigaL3 As System.Data.DataRow = TabUtente.NewRow()

                                myrigaL3(0) = VtTipoCodice(Indicey)
                                myrigaL3(1) = VtTipo(Indicey)
                                myrigaL3(2) = "3"
                                TabUtente.Rows.Add(myrigaL3)


                                Dim Indicez As Integer

                                For Indicez = 0 To IndiceRighe
                                    If VtPadre(Indicez) = VtTipoCodice(Indicey) Then
                                        
                                        Dim myrigaL4 As System.Data.DataRow = TabUtente.NewRow()

                                        myrigaL4(0) = VtTipoCodice(Indicez)
                                        myrigaL4(1) = VtTipo(Indicez)
                                        myrigaL4(2) = "4"
                                        TabUtente.Rows.Add(myrigaL4)

                                    End If
                                Next

                            End If
                        Next
                    End If
                Next

            End If

        Next



    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Appalti_Strutture Order By Descrizione"
        cmd.Connection = cn


        DecodificaCodiceEpersonam()

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("E-Personam", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Dim Cerca As Integer = 0
            For Cerca = 0 To TabUtente.Rows.Count - 1

                If Val(campodb(TabUtente.Rows(Cerca).Item(0).ToString)) = Val(campodb(myPOSTreader.Item("CodificaImport"))) Then
                    myriga(2) = TabUtente.Rows(Cerca).Item(1).ToString
                    If campodbN(TabUtente.Rows(Cerca).Item(2).ToString) <> 0 Then
                        Dim Livello As Integer = campodbN(TabUtente.Rows(Cerca).Item(2).ToString) - 1
                        Dim CercaPadre As Integer = 0

                        For CercaPadre = Cerca To 0 Step -1
                            If campodbN(TabUtente.Rows(CercaPadre).Item(2).ToString) = Livello Then
                                myriga(2) = myriga(2) & " - " & TabUtente.Rows(CercaPadre).Item(1).ToString
                                Exit For
                            End If
                        Next
                    End If
                End If
            Next

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("Strutture.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        If Txt_Descrizione.Text.Trim = "" Then
            cmd.CommandText = "Select * From Appalti_Strutture Order By Descrizione"
        Else
            cmd.CommandText = "Select * From Appalti_Strutture Where Descrizione Like ? Order By Descrizione"
            If Txt_Descrizione.Text.IndexOf("%") >= 0 Then
                cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
            Else
                cmd.Parameters.AddWithValue("@Descrizione", "%" & Txt_Descrizione.Text & "%")
            End If
        End If
        cmd.Connection = cn

        DecodificaCodiceEpersonam()


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("E-Personam", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")
            myriga(1) = myPOSTreader.Item("Descrizione")


            Dim Cerca As Integer = 0
            For Cerca = 0 To TabUtente.Rows.Count - 1

                If Val(campodb(TabUtente.Rows(Cerca).Item(0).ToString)) = Val(campodb(myPOSTreader.Item("CodificaImport"))) Then
                    myriga(2) = TabUtente.Rows(Cerca).Item(1).ToString
                    If campodbN(TabUtente.Rows(Cerca).Item(2).ToString) <> 0 Then
                        Dim Livello As Integer = campodbN(TabUtente.Rows(Cerca).Item(2).ToString) - 1
                        Dim CercaPadre As Integer = 0

                        For CercaPadre = Cerca To 0 Step -1
                            If campodbN(TabUtente.Rows(CercaPadre).Item(2).ToString) = Livello Then
                                myriga(2) = myriga(2) & " - " & TabUtente.Rows(CercaPadre).Item(1).ToString
                                Exit For
                            End If
                        Next
                    End If
                End If
            Next


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("Strutture.aspx")
    End Sub
End Class
