﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class Appalti_StruttureMansioni
    Inherits System.Web.UI.Page
      Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Sub loadpagina(ByVal id As Integer, ByVal idstruttura As Integer)
        Dim x As New ClsOspite
        Dim d As New Cls_Appalti_StruttureMansioni

        Dim ConnectionString As String = Session("DC_TABELLE")

        d.loaddati(ConnectionString, Session("DC_GENERALE"), id, idstruttura, MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Val(Request.Item("IdAppalto")) = 0 Then
            Dim AppST As New Cls_Appalti_AppaltiStrutture

            AppST.LeggiID(Session("DC_TABELLE"), Val(Request.Item("id")))




            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Session("IDSTRUTTURA") = AppST.IdStrutture
            Struttura.ID = AppST.IdStrutture
            Struttura.Leggi(Session("DC_TABELLE"), Struttura.ID)


            Dim Appalto As New Cls_AppaltiAnagrafica


            Appalto.Id = AppST.IdAppalto
            Appalto.Leggi(Session("DC_TABELLE"))


            lbl_StrutturaMansione.Text = Appalto.Descrizione & " " & Struttura.Descrizione
            Call loadpagina(AppST.IdAppalto, AppST.IdStrutture)
        Else

            Dim Appalto As New Cls_AppaltiAnagrafica


            Appalto.Id = Val(Request.Item("IdAppalto"))
            Appalto.Leggi(Session("DC_TABELLE"))


            lbl_StrutturaMansione.Text = Appalto.Descrizione

            Call loadpagina(Appalto.Id, 0)
        End If

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),4); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtConto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompletecomune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""380px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_StruttureMansioni.aspx")
    End Sub

    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then





            Dim DD_Mansione As DropDownList = DirectCast(e.Row.FindControl("DD_Mansione"), DropDownList)

            Dim Mansione As New Cls_AppaltiTabellaMansione

            Mansione.UpDateDropBoxSt(Session("DC_TABELLE"), DD_Mansione, Session("IDSTRUTTURA"))

            'DD_Mansione.Items.Add("KM")

            DD_Mansione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(0).ToString
            'DD_Mansione.Items(DD_Mansione.Items.Count - 1).Value = -1


            Dim DD_Regola As DropDownList = DirectCast(e.Row.FindControl("DD_Regola"), DropDownList)

            DD_Regola.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(1).ToString

            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = Format(CDbl(MyTable.Rows(e.Row.RowIndex).Item(2).ToString), "#,##0.0000")


            Dim TxtConto As TextBox = DirectCast(e.Row.FindControl("TxtConto"), TextBox)

            TxtConto.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            Dim DDIva As DropDownList = DirectCast(e.Row.FindControl("DDIva"), DropDownList)

            Dim IVA As New Cls_IVA


            IVA.UpDateDropBox(Session("DC_TABELLE"), DDIva)


            DDIva.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(4).ToString


            Dim DDTIPOUTENTE As DropDownList = DirectCast(e.Row.FindControl("DDTIPOUTENTE"), DropDownList)


            Dim TipoUtente As New Cls_Appalti_TipoUtente


            TipoUtente.UpDateDropBox(Session("DC_TABELLE"), DDTIPOUTENTE)


            DDTIPOUTENTE.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(5).ToString






            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Struttura", GetType(String))
        MyTable.Columns.Add("Mansione", GetType(String))
        MyTable.Columns.Add("Regole", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Conto", GetType(String))
        MyTable.Columns.Add("CodiceIVA", GetType(String))
        MyTable.Columns.Add("TipoUtente", GetType(String))
        'DDTIPOUTENTE

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim DD_Mansione As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Mansione"), DropDownList)
            Dim DD_Regola As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Regola"), DropDownList)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DDIVA As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DDIVA"), DropDownList)
            Dim TxtConto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtConto"), TextBox)
            Dim DDTIPOUTENTE As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DDTIPOUTENTE"), DropDownList)



            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = DD_Mansione.SelectedValue
            myrigaR(1) = DD_Regola.SelectedValue

            If TxtImporto.Text = "" Then
                TxtImporto.Text = "0,00"
            End If
            myrigaR(2) = TxtImporto.Text

            myrigaR(3) = TxtConto.Text
            myrigaR(4) = DDIVA.SelectedValue

            myrigaR(5) = DDTIPOUTENTE.SelectedValue


            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        Call UpDateTable()
        MyTable = ViewState("App_Retta")

        Dim AppST As New Cls_Appalti_AppaltiStrutture

        AppST.LeggiID(Session("DC_TABELLE"), Val(Request.Item("id")))


        If Val(Request.Item("IdAppalto")) = 0 Then
            Dim AppMans As New Cls_Appalti_StruttureMansioni

            AppMans.IdAppalto = AppST.IdAppalto
            AppMans.Struttura = AppST.IdStrutture


            AppMans.AggiornaDaTabella(Session("DC_TABELLE"), MyTable)

        Else

            Dim cn As New OleDbConnection


            Dim ConnectionString As String = Session("DC_TABELLE")

            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()

            Dim CANCELLACmd As New OleDbCommand


            CANCELLACmd.Connection = cn
            CANCELLACmd.CommandText = "Delete from Appalti_StruttureMansioni where idAppalto = ? and ModificaDaAppalto = 1"
            CANCELLACmd.Parameters.AddWithValue("@IdAppalto", Val(Request.Item("IdAppalto")))
            CANCELLACmd.ExecuteNonQuery()


            Dim cmd As New OleDbCommand

            cmd.CommandText = "Select * From Appalti_AppaltiStrutture where idAppalto = ? Order By IdAppalto,IdStrutture"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@IdAppalto", Val(Request.Item("IdAppalto")))



            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim AppMans As New Cls_Appalti_StruttureMansioni

                AppMans.IdAppalto = Val(Request.Item("IdAppalto"))
                AppMans.Struttura = Val(campodb(myPOSTreader.Item("IdStrutture")))



                AppMans.AggiornaDaTabella(Session("DC_TABELLE"), MyTable, 1)

            Loop

            myPOSTreader.Close()

            cn.Close()
        End If
        
        Response.Redirect("Elenco_StruttureMansioni.aspx")
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        myriga(1) = "O"
        myriga(2) = 0



        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()




        Call EseguiJS()
    End Sub

    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class
