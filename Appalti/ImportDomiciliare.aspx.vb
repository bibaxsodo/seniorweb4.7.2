﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq


Partial Class Appalti_ImportDomiciliare
    Inherits System.Web.UI.Page


    Dim Festivita(100) As MeseGiorno

    Dim Tabella As New System.Data.DataTable("tabella")

    
    Structure MeseGiorno
        Public Mese As Integer
        Public Giorno As Integer
    End Structure

    Private Sub CancellaPrestazioniMese(ByVal Mese As Integer, ByVal Anno As Integer)
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete From Appalti_Prestazioni Where year(data) = ? and month(data) = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Connection = cn
       
        cmd.ExecuteNonQuery()
        cn.Close()

    End Sub


    Private Sub CaricaFestivita(ByVal Anno As Integer)
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Appalti_Festivita Where  (Anno = 0 or Anno Is Null)"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Festivita(Indice).Giorno = campodbN(myPOSTreader.Item("Giorno"))
            Festivita(Indice).Mese = campodbN(myPOSTreader.Item("Mese"))

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()


        Dim cmdAnno As New OleDbCommand()
        cmdAnno.CommandText = "Select * From Appalti_Festivita Where  Anno = ? "
        cmdAnno.Connection = cn
        cmdAnno.Parameters.AddWithValue("@Anno", Anno)
        Dim ReadAnno As OleDbDataReader = cmdAnno.ExecuteReader()
        Do While ReadAnno.Read
            Festivita(Indice).Giorno = campodbN(ReadAnno.Item("Giorno"))
            Festivita(Indice).Mese = campodbN(ReadAnno.Item("Mese"))

            Indice = Indice + 1
        Loop
        ReadAnno.Close()

        cn.Close()

    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub OspitiWeb_ImportDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception

                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Lbl_NomeOspite.Text = Session("EPersonamUser")
        End If

    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_ImportExport.aspx")
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String = ""
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        Token = LoginPersonam(Context)
        If Token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam - Token non recuperato');", True)
            Exit Sub
        End If
        Session("TOKEN") = Token
        Session("PAGINA") = 0

        Dim UtenteTecnico As String = Session("EPersonamUser")
        If UtenteTecnico.ToUpper = "senioruser_1888w".ToUpper Or UtenteTecnico.ToUpper = "senioruser_1888".ToUpper Then
            UtenteTecnico = "2741" 'uscita sicurezza
            Call ImportDomiciliare(Token, UtenteTecnico)

        Else
            Call ImportDomiciliare(Token, UtenteTecnico.Replace("senioruser_", ""))

        End If


        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Operazione terminata');", True)
    End Sub



    Function RichiediJSONUtente(ByVal Token As String, ByVal BU As Long, ByVal IdUtente As Integer) As String
        Dim rawresp As String = ""
        Dim Url As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

    

        Url = "https://api.e-personam.com/api/v1.0/business_units/" & BU & "/guests/" & IdUtente

        Try

            Dim client As HttpWebRequest = WebRequest.Create(Url)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing



            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam " & IdUtente & " ');", True)
            Return ""
            Exit Function
        End Try

        cn.Close()

        Return rawresp
    End Function


    Function LeggiDataPresaInCarico(ByVal Token As String, ByVal BU As Long, ByVal IdUtente As Integer, ByVal DataLimite As Date, ByRef Indirizzo As String, ByRef  Cap As String, ByRef Provincia As String, ByRef Comune As String) As Date
        Dim MaxData As Date = Nothing
        Dim rawresp As String

        rawresp = RichiediJSONUtente(Token, BU, IdUtente)


        Dim jResults As JObject = JObject.Parse(rawresp)


        Try
            MaxData = Mid(jResults.Item("current_status").Item("data_ora_ingresso").ToString, 1, 10)

        Catch ex As Exception

        End Try


        Try
            Indirizzo = jResults.Item("residence").Item("street").ToString()
        Catch ex As Exception

        End Try


        Try
            cap = jResults.Item("residence").Item("cap").ToString()
        Catch ex As Exception

        End Try

        Dim appoggio As String =""
        Try
            appoggio = jResults.Item("residence").Item("city_alpha6").ToString()

            Provincia = Mid(appoggio & Space(6), 1, 3)
            Comune = Mid(appoggio & Space(6), 4, 3)

        Catch ex As Exception

        End Try

        Return MaxData
    End Function

    Function RichiediJSONEpersonam(ByVal Token As String, ByVal BU As Long, ByRef Pagina As Integer, ByVal Anno As Integer, ByVal Mese As Integer) As String
        Dim rawresp As String = ""
        Dim Url As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        If Pagina = 0 Then
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese '& "?per_page=10"
            Pagina = Pagina + 1
        Else
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese & "?page=" & Pagina ' & "&per_page=100"
        End If
        Pagina = Pagina + 1

        Try

            Dim client As HttpWebRequest = WebRequest.Create(Url)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.Headers.Add("view", "for_app")
            'rawresp
            client.ContentType = "Content-Type: application/json"

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing



            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam " & BU & " ');", True)
            Return ""
            Exit Function
        End Try

        cn.Close()

        Return rawresp
    End Function

    Function GiornFestivo(ByVal Data As Date) As Integer
        Dim Mese As Integer = Month(Data)
        Dim Giorno As Integer = Day(Data)
        Dim Indice As Integer = 0

        If Data.DayOfWeek = DayOfWeek.Sunday Then Return 1 : Exit Function


        For Indice = 0 To 100
            If Festivita(Indice).Mese = Mese And Festivita(Indice).Giorno = Giorno Then
                Return 1
                Exit Function
            End If
        Next

        Return 0
    End Function


    Public Sub ImportDomiciliare(ByVal Token As String, ByVal IdStruttura As Integer)
        Dim NumeroRighe As Integer = 0
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Data", GetType(String)) ' 0
        Tabella.Columns.Add("Struttura", GetType(String)) '1
        Tabella.Columns.Add("Durata", GetType(String)) '2
        Tabella.Columns.Add("Operatore", GetType(String)) '3
        Tabella.Columns.Add("Mansione", GetType(String)) '4
        Tabella.Columns.Add("Segnalazione", GetType(String)) '5
        

        Dim Data() As Byte

        Dim DataPresaInCarico As Date = Nothing

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        CaricaFestivita(Param.AnnoFatturazione)


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()



        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim Pagina As Integer = 0

        NumeroRighe = 0



        Dim rawresp As String = ""

        'IdStruttura = 2741

        rawresp = RichiediJSONEpersonam(Token, IdStruttura, Pagina, Param.AnnoFatturazione, Param.MeseFatturazione)

        'If rawresp = "" Then Exit Do
        'If rawresp = "[]" Then Exit Do
        'If rawresp = "{}" Then Exit Do



        CancellaPrestazioniMese(Param.MeseFatturazione, Param.AnnoFatturazione)

        Dim jResults As JArray = JArray.Parse(rawresp)


        'Dim NomeFile As String

        'NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Token_" & Format(Now, "yyyyMMdd") & ".xml"
        'Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'tw.Write(rawresp)
        'tw.Close()




        Dim IndiceRighe As Integer = 0

        For Each jTok1 As JToken In jResults
            Dim cf As String = ""
            Dim CodiceOspite As Integer = 0
            Dim IdUtente As Integer = 0
            Dim NomeUtente As String = ""

            Try
                cf = jTok1.Item("cf").ToString
            Catch ex As Exception

            End Try



            Try
                IdUtente = jTok1.Item("id").ToString
            Catch ex As Exception

            End Try



            Try
                NomeUtente = jTok1.Item("fullname").ToString
            Catch ex As Exception

            End Try


            If cf.Trim <> "" Then
                Dim Utente As New ClsOspite

                If IdUtente > 0 Then

                    Utente.IdEpersonam = IdUtente
                    Utente.LeggiPerIdEpersonam(Session("DC_OSPITE"))

                Else
                    Utente.CODICEFISCALE = cf
                    Utente.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Utente.CODICEFISCALE)

                End If

                If Utente.CODICEFISCALE = "CNTLCN25T25L483F" Then
                    Utente.CODICEFISCALE = "CNTLCN25T25L483F"
                End If


                If NomeUtente <> "" Then
                    If Utente.CodiceOspite > 0 And Utente.RecapitoComune = "" Then
                        If Chk_NonAggiornareUtenti.Checked = False Then
                            DataPresaInCarico = LeggiDataPresaInCarico(Token, IdStruttura, IdUtente, Now, Utente.RESIDENZAINDIRIZZO1, Utente.RESIDENZACAP1, Utente.RESIDENZAPROVINCIA1, Utente.RESIDENZACOMUNE1)
                        End If

                        If IsDate(DataPresaInCarico) Then
                            Utente.Nome = NomeUtente
                            Utente.CognomeOspite = Mid(Utente.Nome, 1, Utente.Nome.IndexOf(" "))
                            Utente.NomeOspite = Mid(Utente.Nome, Utente.Nome.IndexOf(" ") + 1, Len(Utente.Nome) - Utente.Nome.IndexOf(" "))

                            Utente.CODICEFISCALE = cf
                            Utente.IdEpersonam = IdUtente
                            Utente.TIPOLOGIA = "O"

                            Utente.DataAccreditoDeposito = DataPresaInCarico
                            Utente.ScriviOspite(Session("DC_OSPITE"))
                        End If
                    End If
                    If Utente.CodiceOspite = 0 Then
                        Utente.CODICEFISCALE = cf
                        Utente.Nome = NomeUtente
                        Utente.IdEpersonam = IdUtente

                        If Utente.Nome.IndexOf(" ") > 0 Then


                            Utente.CognomeOspite = Mid(Utente.Nome, 1, Utente.Nome.IndexOf(" "))
                            Utente.TIPOLOGIA = "O"
                            Utente.NomeOspite = Mid(Utente.Nome, Utente.Nome.IndexOf(" ") + 1, Len(Utente.Nome) - Utente.Nome.IndexOf(" "))
                            Utente.InserisciOspiteSeCFNONPresente(Session("DC_OSPITE"), Utente.CognomeOspite, Utente.NomeOspite, Nothing, Utente.CODICEFISCALE)

                            CodiceOspite = Utente.CodiceOspite
                            DataPresaInCarico = LeggiDataPresaInCarico(Token, IdStruttura, IdUtente, Now, Utente.RESIDENZAINDIRIZZO1, Utente.RESIDENZACAP1, Utente.RESIDENZAPROVINCIA1, Utente.RESIDENZACOMUNE1)

                            If IsDate(DataPresaInCarico) Then
                                Utente.Nome = NomeUtente
                                Utente.CognomeOspite = Mid(Utente.Nome, 1, Utente.Nome.IndexOf(" "))
                                Utente.NomeOspite = Mid(Utente.Nome, Utente.Nome.IndexOf(" ") + 1, Len(Utente.Nome) - Utente.Nome.IndexOf(" "))

                                Utente.CODICEFISCALE = cf
                                Utente.IdEpersonam = IdUtente
                                Utente.TIPOLOGIA = "O"

                                Utente.DataAccreditoDeposito = DataPresaInCarico
                                Utente.ScriviOspite(Session("DC_OSPITE"))
                            End If
                        End If
                    End If


                End If

                CodiceOspite = Utente.CodiceOspite
            End If

            For Each jTok2 As JToken In jTok1.Item("domestic_activities").Children
                Dim Segnalazione As String = ""

                Dim activity_id As Integer = 0
                Dim description As String = ""
                Dim date_done As Date = Nothing
                Dim duration As Double = 0
                Dim Motivation As Integer = 0
                Dim unit_id As Integer = 0
                Dim unit_description As String = ""
                Dim traveled_km As Double = 0
                Dim InTypeCod As String = ""
                Dim InTypeDescription As String = ""

                Try
                    activity_id = jTok2.Item("activity_id").ToString
                Catch ex As Exception

                End Try

                If activity_id = 38005 Then
                    activity_id = 38005
                End If


                Try
                    description = jTok2.Item("description").ToString
                Catch ex As Exception

                End Try



                Try
                    InTypeCod = jTok1.Item("intype").Item("cod").ToString
                Catch ex As Exception

                End Try

                Try
                    InTypeDescription = jTok1.Item("intype").Item("description").ToString
                Catch ex As Exception

                End Try




                Try
                    date_done = jTok2.Item("date_done").ToString
                Catch ex As Exception

                End Try


                Try
                    If IsDBNull(jTok2.Item("done").ToString) Or jTok2.Item("done").ToString = "" Then
                        Motivation = 1 ' Eseguito se Null??
                    Else
                        Motivation = Val(jTok2.Item("done").ToString) + 1
                    End If
                Catch ex As Exception
                    Motivation = 99
                End Try

                If activity_id = 55738 Then
                    Segnalazione = Segnalazione
                End If


                Dim Mansioni As New Cls_AppaltiTabellaMansione

                Mansioni.CodificaImport = activity_id

                Dim UtenteTecnico As String = Session("EPersonamUser")
                If UtenteTecnico.ToUpper = "senioruser_1888w".ToUpper Or UtenteTecnico.ToUpper = "senioruser_1888".ToUpper Then
                    Mansioni.LeggiImportFestivoFeriale(Session("DC_TABELLE"), Mansioni.CodificaImport, GiornFestivo(date_done))
                Else
                    Mansioni.LeggiImport(Session("DC_TABELLE"), Mansioni.CodificaImport)
                End If

                If Mansioni.ID = 0 Then

                    Segnalazione = Segnalazione & "Mansione codice " & activity_id & " descrizione " & description & " non trovata"
                End If


                Try
                    traveled_km = jTok2.Item("traveled_km").ToString
                Catch ex As Exception

                End Try



                Try
                    duration = jTok2.Item("duration").ToString
                Catch ex As Exception

                End Try

                Try
                    unit_id = jTok2.Item("unit_id").ToString
                Catch ex As Exception

                End Try


                Try
                    unit_description = jTok2.Item("unit_description").ToString
                Catch ex As Exception

                End Try

                Dim Struttura As New Cls_AppaltiTabellaStruttura

                Struttura.CodificaImport = unit_id
                Struttura.LeggiCentroDiCosto(Session("DC_TABELLE"), Struttura.CodificaImport)

                If Struttura.CodificaImport = "3459" Then
                    Struttura.CodificaImport = "3459"
                End If


                If Struttura.ID = 0 Then
                    Segnalazione = Segnalazione & "Struttura codice " & unit_id & " descrizione " & unit_description & " non trovata"
                End If


                Dim AppaltoDaStruttura As New Cls_Appalti_AppaltiStrutture


                AppaltoDaStruttura.IdStrutture = Struttura.ID
                AppaltoDaStruttura.LeggiAppaltoDaStruttura(Session("DC_TABELLE"))

                If AppaltoDaStruttura.IdAppalto = 0 And Struttura.ID > 0 Then
                    Segnalazione = Segnalazione & "Appalto da struttura " & Struttura.ID & " descrizione " & Struttura.Descrizione & " non trovata"
                End If


                Dim Operatore As New Cls_Operatore

                If Motivation >= 1 Then
                    For Each jTok3 As JToken In jTok2.Item("operators").Children
                        Dim ID As Integer = 0
                        Dim FullName As String = ""
                        Try
                            ID = jTok3.Item("id").ToString
                        Catch ex As Exception

                        End Try

                        Try
                            FullName = jTok3.Item("fullname").ToString
                        Catch ex As Exception

                        End Try



                        Operatore.Nome = ""
                        Operatore.CodiceMedico = ID
                        Operatore.Leggi(Session("DC_OSPITE"))

                        If Operatore.Nome = "" Then
                            Operatore.CodiceMedico = ID
                            Operatore.Nome = FullName
                            Operatore.Scrivi(Session("DC_OSPITE"))
                        Else
                            If Operatore.Nome.ToUpper.Trim <> FullName.ToUpper.Trim Then
                                Segnalazione = Segnalazione & "Operatore codice " & ID & " nome diverso in  epersonam " & FullName & " che in Senior " & Operatore.Nome
                            End If
                        End If
                        If Segnalazione = "" Then
                            Dim Prestazione As New Cls_Appalti_Prestazioni



                            Prestazione.IdAppalto = AppaltoDaStruttura.IdAppalto
                            Prestazione.IdStrutture = AppaltoDaStruttura.IdStrutture
                            Prestazione.Data = date_done
                            Prestazione.IdMansione = Mansioni.ID
                            Prestazione.Ore = Math.Round(duration / 60, 4)
                            Prestazione.CodiceOperatore = Operatore.CodiceMedico
                            Prestazione.intypecod = InTypeCod
                            Prestazione.intypedescription = InTypeDescription
                            Prestazione.CodiceOspite = CodiceOspite
                            Prestazione.Done = Motivation

                            Prestazione.Scrivi(Session("DC_TABELLE"))

                            If traveled_km > 0 And Param.MandatoSIA <> "" Then

                                Dim PrestazioneKM As New Cls_Appalti_Prestazioni



                                PrestazioneKM.IdAppalto = AppaltoDaStruttura.IdAppalto
                                PrestazioneKM.IdStrutture = AppaltoDaStruttura.IdStrutture
                                PrestazioneKM.Data = date_done
                                PrestazioneKM.IdMansione = Param.MandatoSIA 'MANSIONE KM
                                PrestazioneKM.Ore = Math.Round(traveled_km, 2)
                                PrestazioneKM.CodiceOperatore = Operatore.CodiceMedico

                                PrestazioneKM.intypecod = InTypeCod
                                PrestazioneKM.intypedescription = InTypeDescription
                                PrestazioneKM.CodiceOspite = CodiceOspite
                                PrestazioneKM.Scrivi(Session("DC_TABELLE"))

                            End If
                        End If
                    Next

                    If Segnalazione = "" Then
                        'Dim Prestazione As New Cls_Appalti_Prestazioni



                        'Prestazione.IdAppalto = AppaltoDaStruttura.IdAppalto
                        'Prestazione.IdStrutture = AppaltoDaStruttura.IdStrutture
                        'Prestazione.Data = date_done
                        'Prestazione.IdMansione = Mansioni.ID
                        'Prestazione.Ore = Math.Round(duration / 60, 2)
                        'Prestazione.CodiceOperatore = Operatore.CodiceMedico
                        'Prestazione.Scrivi(Session("DC_TABELLE"))
                    Else



                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = date_done
                        myriga(1) = Struttura.Descrizione

                        myriga(2) = duration

                        myriga(3) = Operatore.Nome
                        myriga(4) = Mansioni.Descrizione



                        myriga(5) = Segnalazione

                        Tabella.Rows.Add(myriga)
                    End If
                End If
            Next
        Next

        If Param.IdMandatoDaCF > 0 Then
            Dim Cmd As New OleDbCommand


            Cmd.CommandText = "SELECT [IdAppalto],[IdStrutture],codiceoperatore,intypecod,intypedescription,count(*) as TotPrest from [Appalti_Prestazioni] where month(data) = ? and year(data) = ?  AND  IDMANSIONE <> " & Param.MandatoSIA & " group by[IdAppalto],[IdStrutture],codiceoperatore,intypecod,intypedescription "
            Cmd.Parameters.AddWithValue("@Mese", Param.MeseFatturazione)
            Cmd.Parameters.AddWithValue("@Mese", Param.AnnoFatturazione)
            Cmd.Connection = cn
            Dim Prestazioni As OleDbDataReader = Cmd.ExecuteReader()
            Do While Prestazioni.Read

                Dim PrestazioneKM As New Cls_Appalti_Prestazioni



                PrestazioneKM.IdAppalto = campodbN(Prestazioni.Item("IdAppalto"))
                PrestazioneKM.IdStrutture = campodbN(Prestazioni.Item("IdStrutture"))
                PrestazioneKM.Data = DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, 1)
                PrestazioneKM.IdMansione = Param.IdMandatoDaCF 'ACCESSI
                PrestazioneKM.Ore = Math.Round(campodbN(Prestazioni.Item("TotPrest")), 2)
                PrestazioneKM.CodiceOperatore = campodbN(Prestazioni.Item("codiceoperatore"))

                PrestazioneKM.intypecod = campodb(Prestazioni.Item("InTypeCod"))
                PrestazioneKM.intypedescription = campodb(Prestazioni.Item("InTypeDescription"))

                PrestazioneKM.Scrivi(Session("DC_TABELLE"))

            Loop
            Prestazioni.Close()



        End If

        cn.Close()




        ViewState("App_AddebitiMultiplo") = Tabella ' TabApp

        GridView1.AutoGenerateColumns = True

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

    End Sub



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/v1/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser").ToString.Replace("W", "").Replace("w", ""))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

End Class
