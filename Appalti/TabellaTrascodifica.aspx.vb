﻿
Partial Class Appalti_TabellaTrascodifica
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_NoteFatture_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then Exit Sub

        DD_TipTab.Items.Clear()
        DD_TipTab.Items.Add("STRUTTURA")
        DD_TipTab.Items(DD_TipTab.Items.Count - 1).Value = "ST"
        DD_TipTab.Items.Add("MANSIONE")
        DD_TipTab.Items(DD_TipTab.Items.Count - 1).Value = "MN"
 
        Dim x As New Cls_TabellaTrascodificheEsportazioni


        If Request.Item("Id") <> "" Then
            x.ID = Request.Item("Id")
            x.LeggiByID(Session("DC_TABELLE"))

            Txt_ID.Enabled = False
            Txt_ID.Text = x.ID
            DD_TipTab.SelectedValue = x.TIPOTAB
            Txt_Senior.Text = x.SENIOR
            Txt_Export.Text = x.EXPORT


        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Senior.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice Senior');", True)
            Exit Sub
        End If

        If Txt_Export.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice export    ');", True)
            Exit Sub
        End If

        Dim x As New Cls_TabellaTrascodificheEsportazioni

        x.ID = 0
        If Not IsNothing(Request.Item("Id")) Then
            x.ID = Request.Item("Id")
            x.Leggi(Session("DC_TABELLE"))
        End If

        x.TIPOTAB = DD_TipTab.SelectedValue
        x.SENIOR = Txt_Senior.Text
        x.EXPORT = Txt_Export.Text

        x.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("Elenco_TabellaTrascodifica.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_ID.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice');", True)
            Exit Sub
        End If

        Dim x As New Cls_TabellaTrascodificheEsportazioni


        x.ID = Request.Item("Id")
        x.Elimina(Session("DC_TABELLE"))
        Response.Redirect("Elenco_TabellaTrascodifica.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_TabellaTrascodifica.aspx")
    End Sub
End Class
