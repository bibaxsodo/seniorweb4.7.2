﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Elenco_Mansioni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then            
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Appalti_Mansione Order By Descrizione"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("Mansioni.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        If Txt_Descrizione.Text.Trim = "" Then
            cmd.CommandText = "Select * From Appalti_Mansione Order By Descrizione"
        Else
            cmd.CommandText = "Select * From Appalti_Mansione Where Descrizione Like ? Order By Descrizione"
            If Txt_Descrizione.Text.IndexOf("%") >= 0 Then
                cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
            Else
                cmd.Parameters.AddWithValue("@Descrizione", "%" & Txt_Descrizione.Text & "%")
            End If
        End If
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("Mansioni.aspx")
    End Sub
End Class
