﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_SinotticoPrestazioni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Dim TabellaOrdinamento As New System.Data.DataTable("tabella")


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MAppalto As New Cls_AppaltiAnagrafica

        MAppalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)

        Dim MOperatore As New Cls_Operatore

        MOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore)


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Sub GeneraMese(ByVal Genera As Boolean)

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""

        If Val(DD_Appalto.SelectedValue) > 0 Then
            Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
        End If
        If Val(Txt_Anno.Text) > 0 And Val(Dd_Mese.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " month(Data)  = " & Val(Dd_Mese.SelectedValue) & " And year(Data) = " & Val(Txt_Anno.Text)
        End If
        If Val(DD_Struttura.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " IdStrutture  = " & Val(DD_Struttura.SelectedValue)
        End If
        If Condizione = "" Then
            cmd.CommandText = "Select CodiceOperatore From Appalti_Prestazioni Group By CodiceOperatore "
        Else
            cmd.CommandText = "Select CodiceOperatore From Appalti_Prestazioni where " & Condizione & " Group By CodiceOperatore Order by CodiceOperatore"
        End If
        cmd.Connection = cn
        Lbl_Sinottico.Text = "<table class=""tabella"">"

        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<tr style=""height:30px;"">"
        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:300px;"">"
        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "NOME"
        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
        For Giorni = 1 To GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text))
            Dim DateGiorno As Date

            DateGiorno = DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni)

            If DateGiorno.DayOfWeek = DayOfWeek.Sunday Or DateGiorno.DayOfWeek = DayOfWeek.Saturday Then
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:80px; color:red;"">"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & Giorni
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
            Else
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:80px;"">"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & Giorni
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
            End If
        Next

        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</tr>"
        Dim Riga As Integer = 0


        TabellaOrdinamento.Clear()
        TabellaOrdinamento.Columns.Clear()
        TabellaOrdinamento.Columns.Add("Nome", GetType(String))
        TabellaOrdinamento.Columns.Add("Contenuto", GetType(String))
  

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaR As System.Data.DataRow = TabellaOrdinamento.NewRow()


            myrigaR(1) = "<tr style=""height:40px;font-size: x-small;"">"
            myrigaR(1) = myrigaR(1) & "<td style=""width:300px;position: relative;"">"

            Dim M As New Cls_Operatore

            M.CodiceMedico = campodb(myPOSTreader.Item("CodiceOperatore"))
            M.Leggi(Session("DC_OSPITE"))
            myrigaR(1) = myrigaR(1) & M.Nome
            myrigaR(0) = myrigaR(0) & M.Nome
            Dim Codicelink As String = ""

            Codicelink = "<a href=""#"" onclick=""eliminaOperatore(" & campodb(myPOSTreader.Item("CodiceOperatore")) & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");""  >x</a>"

            myrigaR(1) = myrigaR(1) & "<div style=""position: absolute;top: 0px;right: 10px;"">" & Codicelink & "</div>"


            myrigaR(1) = myrigaR(1) & "</td>"

            For Giorni = 1 To GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text))
                myrigaR(1) = myrigaR(1) & "<td style=""width:80px;text-align: center;"" id=""Id_" & Giorni & "_" & campodb(myPOSTreader.Item("CodiceOperatore")) & """ > "

                Dim CmdP As New OleDbCommand
                CmdP.Connection = cn
                CmdP.CommandText = "Select * From Appalti_Prestazioni where " & Condizione & " And Data = ? And CodiceOperatore = ?"
                CmdP.Parameters.AddWithValue("@Data", DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni))
                CmdP.Parameters.AddWithValue("@CodiceOperatore", campodb(myPOSTreader.Item("CodiceOperatore")))
                Dim NonFlag As Boolean = False
                Dim RigheImport As String = ""
                Dim MaxRige As Integer = 0

                Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
                Do While MovimentiGiorno.Read

                    Dim Man As New Cls_AppaltiTabellaMansione


                    Man.ID = campodbn(MovimentiGiorno.Item("IdMansione"))
                    Man.Leggi(Session("DC_TABELLE"), Man.ID)


                    'Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<div class=""Cella""  onclick=""ShowIndicaOre(" & campodb(myPOSTreader.Item("CodiceOperatore")) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");"">" & campodbn(MovimentiGiorno.Item("Ore")) & "<br/>" & Man.CodificaImport & "</div>"

                    RigheImport = RigheImport & campodbn(MovimentiGiorno.Item("Ore")) & " " & Man.CodificaImport & "<br/>"
                    MaxRige = MaxRige + 1

                    If MaxRige = 3 Then Exit Do
                    NonFlag = True
                Loop
                MovimentiGiorno.Close()



                If Not NonFlag Then
                    myrigaR(1) = myrigaR(1) & "<div class=""Cella""  onclick=""ShowIndicaOre(" & campodb(myPOSTreader.Item("CodiceOperatore")) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");""></div>"
                Else
                    myrigaR(1) = myrigaR(1) & "<div class=""Cella""  onclick=""ShowIndicaOre(" & campodb(myPOSTreader.Item("CodiceOperatore")) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");"">" & RigheImport & "</div>"
                End If


                myrigaR(1) = myrigaR(1) & "</td>"
            Next
            myrigaR(1) = myrigaR(1) & "</tr>"

   

            TabellaOrdinamento.Rows.Add(myrigaR)
        Loop
        myPOSTreader.Close()

        Dim Indice As Integer

        Dim dataView As New System.Data.DataView(TabellaOrdinamento)
        dataView.Sort = " Nome"
        Dim dataTable As System.Data.DataTable = dataView.ToTable()

        Riga = 0
        For Indice = 0 To dataTable.Rows.Count - 1
            Lbl_Sinottico.Text = Lbl_Sinottico.Text & dataTable.Rows(Indice).Item(1).ToString
            Riga = Riga + 1
            If Riga > 10 Then

                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<tr style=""height:30px;"">"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:300px;"">"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "NOME"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
                For Giorni = 1 To GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text))
                    Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:80px;"">"
                    Lbl_Sinottico.Text = Lbl_Sinottico.Text & Giorni
                    Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
                Next

                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</tr>"
                Riga = 0
            End If

        Next


        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</table>"



        If Genera Then
            REM Includi Utenti Programmati ma non ancora inseriti
            Dim cmdUtentiProgrammati As New OleDbCommand


            cmdUtentiProgrammati.CommandText = "SELECT  *  FROM [Appalti_Programmazione] WHERE IdStruttura = ? AND IdAppalto = ? AND (SELECT COUNT(*) FROM Appalti_Prestazioni   WHERE Appalti_Prestazioni.IdAppalto =   [Appalti_Programmazione].IdAppalto    AND    Appalti_Prestazioni.IdStrutture = [Appalti_Programmazione].IdStruttura AND Appalti_Prestazioni.CodiceOperatore =   [Appalti_Programmazione].IdOperatore AND  YEAR(Appalti_Prestazioni.Data) = ? AND  MONTH(Appalti_Prestazioni.Data) = ?) =0"
            cmdUtentiProgrammati.Parameters.AddWithValue("@IdStruttura", Val(DD_Struttura.SelectedValue))
            cmdUtentiProgrammati.Parameters.AddWithValue("@IdAppalto", Val(DD_Appalto.SelectedValue))
            cmdUtentiProgrammati.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))
            cmdUtentiProgrammati.Parameters.AddWithValue("@Mese", Val(Dd_Mese.SelectedValue))
            cmdUtentiProgrammati.Connection = cn


            Dim ReaderUtentiProgrammati As OleDbDataReader = cmdUtentiProgrammati.ExecuteReader()
            Do While ReaderUtentiProgrammati.Read

                InserisciOperatore(campodb(ReaderUtentiProgrammati.Item("IdOperatore")))

            Loop
            ReaderUtentiProgrammati.Close()
        End If


        cn.Close()


        
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Appalto.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Struttura.SelectedValue) = 0 Then
            Exit Sub
        End If


        GeneraMese(False)

        BtnAddOperatore.Visible = True
        DD_Operatore.Visible = True

        DD_Operatore.SelectedValue = ""
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Sub InserisciOperatore(ByVal CodiceOperatore As String)
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If CodiceOperatore <> "" Then

            Dim M As New Cls_Operatore

            M.CodiceMedico = CodiceOperatore
            M.Leggi(Session("DC_OSPITE"))

            If Lbl_Sinottico.Text.IndexOf(M.Nome) < 0 Then
                Lbl_Sinottico.Text = Lbl_Sinottico.Text.Replace("</table>", "")
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<tr style=""height:40px;font-size: x-small;"">"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:300px; position: relative;"">"

                Dim Condizione As String = ""

                If Val(DD_Appalto.SelectedValue) > 0 Then
                    Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
                End If
                If Val(Txt_Anno.Text) > 0 And Val(Dd_Mese.SelectedValue) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " And "
                    End If
                    Condizione = " month(Data)  = " & Val(Dd_Mese.SelectedValue) & " And year(Data) = " & Val(Txt_Anno.Text)
                End If
                If Val(DD_Struttura.SelectedValue) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " And "
                    End If
                    Condizione = " IdStrutture  = " & Val(DD_Struttura.SelectedValue)
                End If


                Lbl_Sinottico.Text = Lbl_Sinottico.Text & M.Nome
                Dim Codicelink As String = ""

                Codicelink = "<a href=""#"" onclick=""eliminaOperatore(" & CodiceOperatore & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");""  >x</a>"

                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<div style=""position: absolute;top: 0px;right: 10px;"">" & Codicelink & "</div>"

                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"

                For Giorni = 1 To GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text))
                    Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<td style=""width:80px;text-align: center;"" id=""Id_" & Giorni & "_" & Val(CodiceOperatore) & """ > "

                    Dim CmdP As New OleDbCommand
                    CmdP.Connection = cn
                    CmdP.CommandText = "Select * From Appalti_Prestazioni where " & Condizione & " And Data = ? And CodiceOperatore = ?"
                    CmdP.Parameters.AddWithValue("@Data", DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni))
                    CmdP.Parameters.AddWithValue("@CodiceOperatore", Val(CodiceOperatore))
                    Dim NonFlag As Boolean = False


                    Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
                    Do While MovimentiGiorno.Read

                        Dim Man As New Cls_AppaltiTabellaMansione


                        Man.ID = campodbn(MovimentiGiorno.Item("IdMansione"))
                        Man.Leggi(Session("DC_TABELLE"), Man.ID)


                        Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<div class=""Cella""  onclick=""ShowIndicaOre(" & Val(CodiceOperatore) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");"">" & campodbn(MovimentiGiorno.Item("Ore")) & "/" & Man.CodificaImport & "</div>"
                        NonFlag = True
                    Loop
                    MovimentiGiorno.Close()
                    If Not NonFlag Then
                        Dim GiornoSettimana As Integer

                        GiornoSettimana = DateSerial(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text), Giorni).DayOfWeek
                        If GiornoSettimana = 0 Then GiornoSettimana = 7


                        Dim SchemaSettimana As New Cls_Appalti_Programmazione
                        Dim RigheImport As String = ""
                        SchemaSettimana.GeneraGiorno(Session("DC_TABELLE"), Session("DC_OSPITE"), Val(DD_Appalto.SelectedValue), Val(DD_Struttura.SelectedValue), CodiceOperatore, DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni))

                        Dim CmdP2 As New OleDbCommand
                        CmdP2.Connection = cn
                        CmdP2.CommandText = "Select * From Appalti_Prestazioni where " & Condizione & " And Data = ? And CodiceOperatore = ?"
                        CmdP2.Parameters.AddWithValue("@Data", DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni))
                        CmdP2.Parameters.AddWithValue("@CodiceOperatore", Val(CodiceOperatore))


                        Dim MovimentiGiorno2 As OleDbDataReader = CmdP2.ExecuteReader()
                        Do While MovimentiGiorno2.Read

                            Dim Man As New Cls_AppaltiTabellaMansione


                            Man.ID = campodbn(MovimentiGiorno2.Item("IdMansione"))
                            Man.Leggi(Session("DC_TABELLE"), Man.ID)

                            RigheImport = RigheImport & campodbn(MovimentiGiorno2.Item("Ore")) & " " & Man.CodificaImport & "<br/>"                            

                            NonFlag = True
                        Loop
                        MovimentiGiorno2.Close()

                        If Not NonFlag Then
                            Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<div class=""Cella""  onclick=""ShowIndicaOre(" & Val(CodiceOperatore) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");""></div>"
                        Else
                            Lbl_Sinottico.Text = Lbl_Sinottico.Text & "<div class=""Cella""  onclick=""ShowIndicaOre(" & Val(CodiceOperatore) & "," & Giorni & "," & Val(Dd_Mese.SelectedValue) & "," & Txt_Anno.Text & "," & DD_Appalto.SelectedValue & "," & DD_Struttura.SelectedValue & ");"">" & RigheImport & "</div>"
                        End If
                    End If


                    Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</td>"
                Next
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</tr>"
                Lbl_Sinottico.Text = Lbl_Sinottico.Text & "</table>"
            End If
        End If
        cn.Close()

    End Sub

    Protected Sub BtnAddOperatore_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddOperatore.Click
        InserisciOperatore(Val(DD_Operatore.SelectedValue))

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub DD_Appalto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Appalto.SelectedIndexChanged

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBoxAppalto(Session("DC_TABELLE"), DD_Struttura, Val(DD_Appalto.SelectedValue))
    End Sub


    Protected Sub Img_GeneraMese_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_GeneraMese.Click
        If Val(Txt_Anno.Text) = 0 Then
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Appalto.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Struttura.SelectedValue) = 0 Then
            Exit Sub
        End If


        GeneraMese(True)

        BtnAddOperatore.Visible = True
        DD_Operatore.Visible = True

        DD_Operatore.SelectedValue = ""
    End Sub
End Class
