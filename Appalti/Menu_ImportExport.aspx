﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Menu_ImportExport" CodeFile="Menu_ImportExport.aspx.vb" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Commesse</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function chiudinews() {

            $("#news").css('visibility', 'hidden');
        }
    </script>
    <style>
        #news {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 400px;
            height: 470px;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;"></td>
                    <td>
                        <div class="Titolo">Commesse - Import Export</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 900px;">

                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <a href="ImportDaTurni.aspx">
                                        <img alt="Import" class="Effetto" src="../images/Menu_Export_1.png" style="border-width: 0;"></a>

                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="../OspitiWeb/Export_Documenti.aspx?APPALTI=SI">
                                        <img alt="EXPORT" class="Effetto" src="../images/Menu_Export_1.png" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="importdomiciliare.aspx">
                                        <img alt="EXPORT" class="Effetto" src="../images/Menu_EPersonam.PNG" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="importCUS.aspx">
                                        <img alt="EXPORT" class="Effetto" src="../images/Menu_Export_1.png" style="border-width: 0;"></a></td>

                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Imb_ImportAttivita" runat="server" src="../images/Menu_Export_1.png" />
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT PR.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXPORT</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DOMICILIARI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CUS</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT ATTIVITA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>


                            </tr>


                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <a href="EliminaPrestazioni.aspx">
                                        <img alt="Import" class="Effetto" src="../images/Menu_ModificaRetta.JPG" style="border-width: 0;"></a>

                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="ControlloPrestazioni.aspx">
                                        <img alt="Import" class="Effetto" src="../images/Menu_Export_1.png" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>




                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ELIMINA PR.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTROLLO PRESTAZIONI</span></td>

                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>



                        </table>
                    </td>
                </tr>

            </table>


        </div>


    </form>
</body>
</html>
