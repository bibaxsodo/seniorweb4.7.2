﻿
Partial Class Appalti_Menu_ImportExport
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub Imb_ImportAttivita_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_ImportAttivita.Click
        Dim UserLogin As New Cls_Login

        UserLogin.Utente = Session("UTENTE")

        UserLogin.LeggiSP(Application("SENIOR"))
        Dim parametri As New NameValueCollection

        parametri.Add("PC", UserLogin.PasswordCriptata)
   

        RedirectWithData(Response, "/ImportAttivita/ImportAttivita.aspx?Utente=" & Session("UTENTE"), parametri)
    End Sub

    Public Sub RedirectWithData(ByRef aThis As HttpResponse, ByVal aDestination As String, _
                             ByVal aData As NameValueCollection)
        aThis.Clear()
        Dim sb As StringBuilder = New StringBuilder()

        sb.Append("<html>")
        sb.AppendFormat("<body onload='document.forms[""form""].submit()'>")
        sb.AppendFormat("<form name='form' action='{0}' method='post'>", aDestination)

        For Each key As String In aData
            sb.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", key, aData(key))
        Next

        sb.Append("</form>")
        sb.Append("</body>")
        sb.Append("</html>")

        aThis.Write(sb.ToString())

        aThis.End()
    End Sub
End Class
