﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Elenco_StruttureMansioni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        EseguiJS()


        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim MAppalto As New Cls_AppaltiAnagrafica

        MAppalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Appalti_AppaltiStrutture Order By IdAppalto,IdStrutture"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Commessa", GetType(String))
        Tabella.Columns.Add("Struttura", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")

            Dim M As New Cls_AppaltiAnagrafica

            M.Id = Val(myPOSTreader.Item("IdAppalto"))
            M.Leggi(Session("DC_TABELLE"))

            myriga(1) = M.Descrizione

            Dim ZM As New Cls_AppaltiTabellaStruttura

            ZM.ID = Val(myPOSTreader.Item("IdStrutture"))
            ZM.Leggi(Session("DC_TABELLE"), ZM.ID)


            myriga(2) = ZM.Descrizione
            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()


    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("StruttureMansioni.aspx?id=" & Codice)
        End If
    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        Dim Condizione As String = ""

        If Val(DD_Appalto.SelectedValue) > 0 Then
            Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
        End If
        If Condizione = "" Then
            cmd.CommandText = "Select * From Appalti_AppaltiStrutture Order By IdAppalto,IdStrutture"
        Else
            cmd.CommandText = "Select * From Appalti_AppaltiStrutture  where " & Condizione & " Order By IdAppalto,IdStrutture"
        End If
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Commessa", GetType(String))
        Tabella.Columns.Add("Struttura", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Id")

            Dim M As New Cls_AppaltiAnagrafica

            M.Id = Val(myPOSTreader.Item("IdAppalto"))
            M.Leggi(Session("DC_TABELLE"))

            myriga(1) = M.Descrizione

            Dim ZM As New Cls_AppaltiTabellaStruttura

            ZM.ID = Val(myPOSTreader.Item("IdStrutture"))
            ZM.Leggi(Session("DC_TABELLE"), ZM.ID)


            myriga(2) = ZM.Descrizione
            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        Response.Redirect("StruttureMansioni.aspx?IdAppalto=" & DD_Appalto.SelectedValue)
    End Sub
End Class
