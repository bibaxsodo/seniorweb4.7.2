﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Elabora" CodeFile="Elabora.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Elabora</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">      
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function TieniSessioneInVita() {
            $.ajax({
                cache: false,
                dataType: "text",
                timeout: 3000,
                url: "interrogaparametri.ashx",
                success: function (data) {

                },
                error: function (jqXHR, textStatus, errorThrown) {


                }
            });
        }
        $(document).ready(function () {
            setInterval(TieniSessioneInVita, 60000);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse - Elabora</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Esegui" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Elabora               
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Mese :</label>
                                    <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">Commesse :</label>
                                    <asp:DropDownList ID="DD_Appalto" runat="server" Width="350px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    </div>
            <br />
                                    <label style="display: block; float: left; width: 300px;">Data Registrazione  :</label>
                                    <asp:TextBox ID="Txt_Data" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <asp:Button ID="Btn_Aggiorna" runat="server" Text="Aggiorna" /><br />
                                    <br />
                                    <asp:GridView ID="Grd_Registri" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Height="160px" ShowFooter="True" Width="612px">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/annulla.png" EditImageUrl="~/images/modifica.png"
                                                ShowEditButton="True" UpdateImageUrl="~/images/aggiorna.png" />
                                            <asp:TemplateField HeaderText="Registro">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblRegistro" runat="server" Text='<%# Eval("Registro") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblRegistro" runat="server" Text='<%# Eval("Registro") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Protcolllo1">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtProtcollo1" runat="server" Style="text-align: right;" Width="200px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblProtocollo1" Style="text-align: right;" runat="server" Text='<%# Eval("Protocollo1") %>' Width="200px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Protcolllo2">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtProtcollo2" Style="text-align: right;" runat="server" Width="200px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblProtocollo2" Style="text-align: right;" runat="server" Text='<%# Eval("Protocollo2") %>' Width="200px"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>
                                    <br />
                                    <asp:Label ID="Label1" runat="server" Width="776px"></asp:Label>
                                    <div>

                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                                </asp:Timer>
                                                <asp:GridView ID="Grid" runat="server" BackColor="#DEBA84"
                                                    BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                    CellSpacing="2" Width="814px">
                                                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                                <asp:Label ID="Lbl_Waiting" runat="server" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
