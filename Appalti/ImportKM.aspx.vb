﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Appalti_ImportKM
    Inherits System.Web.UI.Page

    Dim Festivita(100) As MeseGiorno

    Dim Tabella As New System.Data.DataTable("tabella")


    Structure MeseGiorno
        Public Mese As Integer
        Public Giorno As Integer
    End Structure

    Private Sub CancellaPrestazioniMese(ByVal Mese As Integer, ByVal Anno As Integer)
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete From Appalti_Prestazioni Where year(data) = ? and month(data) = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Connection = cn

        cmd.ExecuteNonQuery()
        cn.Close()

    End Sub


    Private Sub CaricaFestivita()
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Appalti_Festivita"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Festivita(Indice).Giorno = campodbN(myPOSTreader.Item("Giorno"))
            Festivita(Indice).Mese = campodbN(myPOSTreader.Item("Mese"))

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()


        cn.Close()

    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub OspitiWeb_ImportDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception

                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


        End If

    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String = ""
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        Token = LoginPersonam(Context)
        If Token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
            Exit Sub
        End If
        Session("TOKEN") = Token
        Session("PAGINA") = 0
        Call ImportDomiciliare(Token)

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Operazione terminata');", True)
    End Sub


    Function RichiediJSONEpersonam(ByVal Token As String, ByVal BU As Long, ByRef Pagina As Integer, ByVal Anno As Integer, ByVal Mese As Integer) As String
        Dim rawresp As String = ""
        Dim Url As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



        '/api/v1.0/business_units/

        If Pagina = 0 Then
            Url = "https://api.e-personam.com/api/v1.0/business_units/" & BU & "/traveled_kms/" & Anno
            Pagina = Pagina + 1
        Else
            Url = "https://api.e-personam.com/api/v1.0/business_units/" & BU & "/traveled_kms/" & Anno
        End If
        Pagina = Pagina + 1

        Try

            Dim client As HttpWebRequest = WebRequest.Create(Url)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing



            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam " & BU & " ');", True)
            Return ""
            Exit Function
        End Try

        cn.Close()

        Return rawresp
    End Function

    Function GiornFestivo(ByVal Data As Date) As Integer
        Dim Mese As Integer = Month(Data)
        Dim Giorno As Integer = Day(Data)
        Dim Indice As Integer = 0

        If Data.DayOfWeek = DayOfWeek.Sunday Then Return 1 : Exit Function


        For Indice = 0 To 100
            If Festivita(Indice).Mese = Mese And Festivita(Indice).Giorno = Giorno Then
                Return 1
                Exit Function
            End If
        Next

        Return 0
    End Function


    Public Sub ImportDomiciliare(ByVal Token As String)
        Dim NumeroRighe As Integer = 0
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        CaricaFestivita()

        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Data", GetType(String)) ' 0
        Tabella.Columns.Add("Struttura", GetType(String)) '1
        Tabella.Columns.Add("Durata", GetType(String)) '2
        Tabella.Columns.Add("Operatore", GetType(String)) '3
        Tabella.Columns.Add("Mansione", GetType(String)) '4
        Tabella.Columns.Add("Segnalazione", GetType(String)) '5


        Dim Data() As Byte

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()



        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim Pagina As Integer = 0

        NumeroRighe = 0



        Dim rawresp As String = ""
        '2741
        rawresp = RichiediJSONEpersonam(Token, 2741, Pagina, Param.AnnoFatturazione, Param.MeseFatturazione)

        'If rawresp = "" Then Exit Do
        'If rawresp = "[]" Then Exit Do
        'If rawresp = "{}" Then Exit Do



        Dim jResults As JArray = JArray.Parse(rawresp)





        Dim IndiceRighe As Integer = 0

        For Each jTok1 As JToken In jResults


        Next



        cn.Close()




        ViewState("App_AddebitiMultiplo") = Tabella ' TabApp

        GridView1.AutoGenerateColumns = True

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

    End Sub



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/v1/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

End Class
