﻿Imports System.Threading
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Data.OleDb
Partial Class Appalti_StampaFattureProva
    Inherits System.Web.UI.Page




    Public Sub DoWork(ByVal data As Object)
        Dim k As New Cls_StampaFatture
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data

        If Btn_Conferma.Enabled = True Or Label1.Text.IndexOf("Emissione di Prova<br >") >= 0 Then
            If System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = "NONEMISSIONE" Then
                k.CreaRecordStampa(0, 0, False, 0, 0, 0, Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 1, data, 0, 99999999)
            Else
                k.CreaRecordStampa(0, 0, False, 0, 0, 0, Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 1, data, Val(System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID)), Val(System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID)))
            End If
        Else
            If System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = "NONEMISSIONE" Then
                k.CreaRecordStampa(0, 0, False, 0, 0, 0, Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 0, data, 0, 99999999)
            Else
                k.CreaRecordStampa(0, 0, False, 0, 0, 0, Val(DD_Registro.SelectedValue), Txt_Campo1.Text, Txt_Campo2.Text, Txt_Campo3.Text, 0, data, Val(System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID)), Val(System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID)))
            End If
        End If

    End Sub


    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick


        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) = 999 Then
            System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
            Lbl_Waiting.Text = "<p align=left>" & System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) & "</p>"
            Exit Sub
        End If

        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 100 Then
            Lbl_Waiting.Text = ""
            Timer1.Enabled = False
            'Response.Redirect("StampaReport.aspx?REPORT=STAMPADOCUMENTIOSPITI")

            Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)

            Session("stampa") = System.Web.HttpRuntime.Cache("stampa" + Session.SessionID)

            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))
            If Session("Download") = "SI" Then
                If DD_Report.SelectedValue = "" Then
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&EXPORT=PDF','Stampe1','width=800,height=600');", True)
                    End If

                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI2") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI2&EXPORT=PDF','Stampe2','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI3") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI3&EXPORT=PDF','Stampe3','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI4") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI4&EXPORT=PDF','Stampe4','width=800,height=600');", True)
                    End If
                Else

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "&EXPORT=PDF','Stampe4','width=800,height=600');", True)
                End If
            Else
                If DD_Report.SelectedValue = "" Then
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&PRINTERKEY=ON','Stampe1','status=1,width=800,height=600');", True)
                    End If

                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI2") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI2&PRINTERKEY=ON','Stampe2','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI3") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI3&PRINTERKEY=ON','Stampe3','width=800,height=600');", True)
                    End If
                    If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI4") <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI4&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
                    End If
                Else

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
                End If
            End If

            Exit Sub
        End If
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 0 Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"

            REM Lbl_Waiting.Text = "<div  style=""background-color:yellow; width:300px; height:100px; left:40%; position:absolute;""><br/><img height=30px src=""images/loading.gif""><br />" & Application(CampoProgressBar) & "%</div>"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Appoggio As String = ""
        Dim NumeroErrori As Long = 0
        Dim AppoggioCpy As String


        AppoggioCpy = Session("CampoWr")
        If Not IsNothing(AppoggioCpy) Then
            AppoggioCpy = AppoggioCpy.Replace(Chr(13), "<br/>")
        Else
            AppoggioCpy = ""
        End If


        Appoggio = Session("CampoErrori")

        If Not IsNothing(Appoggio) Then
            Appoggio = Appoggio.Replace(Chr(13), "<br/>")
        Else
            Appoggio = ""
        End If
        If Len(Appoggio) > 1 Then
            Btn_Conferma.Enabled = False
        End If

        Label1.Text = Appoggio & "<br >" & AppoggioCpy

        Dim Em As New Cls_EmissioneRetta

        Em.ConnessioneGenerale = Session("DC_GENERALE")
        Em.ConnessioneTabelle = Session("DC_TABELLE")
        Em.ElencoMovimentiDiProva(Chk_Documenti)

        Session("RagioneSocialeFattura") = ""
        Session("CampoProgressBar") = 0
        Cache("CampoProgressBar" + Session.SessionID) = 0

        Dim RegIva As New Cls_RegistroIVA

        RegIva.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Dim ImportoDare As Double
        Dim ImportoAvere As Double
        Dim StringaErrori As String = ""

        Dim cn As New OleDbConnection

        cn.ConnectionString = Session("DC_GENERALE")

        cn.Open()


        Dim CmdTesta As New OleDbCommand

        CmdTesta.CommandText = "Select * From  Temp_MovimentiContabiliTesta Order by NumeroRegistrazione"
        CmdTesta.Connection = cn
        Dim ReadREg As OleDbDataReader = CmdTesta.ExecuteReader()


        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO1").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO1"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO2"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO3").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO3"
        End If

        If Trim(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("DOCUMENTOPERSONALIZZATO4").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "DOCUMENTOPERSONALIZZATO4"
        End If


        DD_Fatture.Items.Clear()
        DD_Fatture.Items.Add("")
        DD_Fatture.Items(DD_Fatture.Items.Count - 1).Value = 0

        Do While ReadREg.Read
            Dim CmdVerificaRegistrazioneD As New OleDbCommand()
            CmdVerificaRegistrazioneD.CommandText = "Select sum(Importo) as TotImp From  Temp_MovimentiContabiliRiga Where DareAvere = 'D' And  Numero = " & campodbN(ReadREg.Item("NumeroRegistrazione"))
            CmdVerificaRegistrazioneD.Connection = cn
            Dim ReadVerificaRegistrazioneD As OleDbDataReader = CmdVerificaRegistrazioneD.ExecuteReader()
            If ReadVerificaRegistrazioneD.Read Then
                ImportoDare = campodbN(ReadVerificaRegistrazioneD.Item("TotImp"))
            End If
            ReadVerificaRegistrazioneD.Close()


            Dim CmdVerificaRegistrazioneA As New OleDbCommand()
            CmdVerificaRegistrazioneA.CommandText = "Select sum(Importo) as TotImp From  Temp_MovimentiContabiliRiga Where DareAvere = 'A' And  Numero = " & campodbN(ReadREg.Item("NumeroRegistrazione"))
            CmdVerificaRegistrazioneA.Connection = cn
            Dim ReadVerificaRegistrazioneA As OleDbDataReader = CmdVerificaRegistrazioneA.ExecuteReader()
            If ReadVerificaRegistrazioneA.Read Then
                ImportoAvere = campodbN(ReadVerificaRegistrazioneA.Item("TotImp"))
            End If
            ReadVerificaRegistrazioneA.Close()

            If Math.Round(ImportoAvere, 2) <> Math.Round(ImportoDare, 2) Then
                StringaErrori = StringaErrori & "ATTENZIONE Dare avere non coincido in registrazione " & campodbN(ReadREg.Item("NumeroRegistrazione")) & " D = " & Math.Round(ImportoDare, 2) & " A = " & Math.Round(ImportoAvere, 2) & "<br />"
            End If



            Dim CmdLeggiTesta As New OleDbCommand()
            CmdLeggiTesta.CommandText = "Select * From  Temp_MovimentiContabiliRiga Where RigaDaCausale = 1 And  Numero = " & campodbN(ReadREg.Item("NumeroRegistrazione"))
            CmdLeggiTesta.Connection = cn
            Dim ReadTesta As OleDbDataReader = CmdLeggiTesta.ExecuteReader()
            If ReadTesta.Read Then
                Dim KConto As New Cls_Pianodeiconti

                KConto.Mastro = campodbN(ReadTesta.Item("MastroPartita"))
                KConto.Conto = campodbN(ReadTesta.Item("ContoPartita"))
                KConto.Sottoconto = campodbN(ReadTesta.Item("SottocontoPartita"))
                KConto.Decodfica(Session("DC_GENERALE"))
                DD_Fatture.Items.Add(KConto.Descrizione & " " & campodbN(ReadREg.Item("NumeroProtocollo")))
                DD_Fatture.Items(DD_Fatture.Items.Count - 1).Value = campodbN(ReadREg.Item("NumeroRegistrazione"))
            End If
            ReadVerificaRegistrazioneA.Close()


        Loop

        cn.Close()



        Label1.Text = Label1.Text & StringaErrori


        If Not IsNothing(Session("CampoWr")) Then
            If Session("CampoWr") <> "" Then
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
        If Not IsNothing(Session("CampoErrori")) Then
            If Session("CampoErrori") <> "" Then
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
        If StringaErrori <> "" Then
            TabContainer1.ActiveTabIndex = 1
        End If
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Fattura"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Function VerificaRegistrazione() As Integer

        Dim cn As OleDbConnection

        Dim TotCliFor As Double
        Dim DareAvere As String
        Dim TotImposta As Double
        Dim TotImponibile As Double
        Dim TotDare As Double
        Dim TotAvere As Double
        Dim TotAttivita As Double
        Dim TotPassivita As Double
        Dim TotCosti As Double
        Dim TotRicavi As Double
        Dim TotOrdine As Double
        Dim Differenza As Double
        Dim MySql As String = ""

        Dim StringaDegliErrori As String = ""


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        VerificaRegistrazione = 0


        MySql = "SELECT NumeroRegistrazione, CausaleContabile FROM Temp_MovimentiContabiliTesta"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120

        Dim MRead As OleDbDataReader = cmd.ExecuteReader()
        Do While MRead.Read
            TotCliFor = 0
            DareAvere = ""
            TotImposta = 0
            TotImponibile = 0
            TotDare = 0
            TotAvere = 0
            TotAttivita = 0
            TotPassivita = 0
            TotCosti = 0
            TotRicavi = 0
            TotOrdine = 0

            MySql = "SELECT * FROM Temp_MovimentiContabiliRiga WHERE Numero = " & campodbN(MRead.Item("NumeroRegistrazione")) & " ORDER BY RigaDaCausale"
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = MySql
            cmdR.Connection = cn
            cmdR.CommandTimeout = 120

            Dim MReadRoga As OleDbDataReader = cmdR.ExecuteReader()
            Do While MReadRoga.Read
                If campodb(MReadRoga.Item("Tipo")) = "CF" Then
                    TotCliFor = campodbN(MReadRoga.Item("Importo"))
                    DareAvere = campodb(MReadRoga.Item("DareAvere"))
                End If
                If campodb(MReadRoga.Item("Tipo")) = "IV" Then
                    Dim MiaIVa As New Cls_IVA
                    MiaIVa.Codice = campodb(MReadRoga.Item("CodiceIVA"))
                    MiaIVa.Leggi(Session("DC_TABELLE"), MiaIVa.Codice)

                    If Modulo.MathRound(CDbl(campodb(MReadRoga.Item("Imponibile"))) * MiaIVa.Aliquota, 2) <> Modulo.MathRound(CDbl(campodb(MReadRoga.Item("Importo"))), 2) Then
                        VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                    End If

                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - Math.Round(campodbN(MReadRoga.Item("Importo")), 2)
                        TotImponibile = TotImponibile - Math.Round(campodbN(MReadRoga.Item("Imponibile")), 2)
                    Else
                        TotImposta = TotImposta + Math.Round(campodbN(MReadRoga.Item("Importo")), 2)
                        TotImponibile = TotImponibile + Math.Round(campodbN(MReadRoga.Item("Imponibile")), 2)
                    End If
                End If
                If campodb(MReadRoga.Item("Tipo")) = "AR" Or campodb(MReadRoga.Item("Tipo")) = "AB" Then
                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile - campodb(MReadRoga.Item("Imponibile"))
                    Else
                        TotImposta = TotImposta + campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile + campodb(MReadRoga.Item("Imponibile"))
                    End If
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                    TotDare = TotDare + campodbN(MReadRoga.Item("Importo"))
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "A" Then
                    TotAvere = TotAvere + campodbN(MReadRoga.Item("Importo"))
                End If
            Loop
            If Format(TotDare, "0.00") <> Format(TotAvere, "0.00") Then
                VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                Exit Function
            End If
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), campodb(MRead.Item("CausaleContabile")))

            If CauCon.TipoDocumento <> "" Then
                If Format(TotCliFor, "0.00") <> Format(TotImponibile + TotImposta, "0.00") Then
                    VerificaRegistrazione = campodbN(MRead.Item("NumeroRegistrazione"))
                    Exit Function
                End If
            End If

        Loop
        MRead.Close()

        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Btn_Conferma_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Conferma.Click
        Dim Em As New Cls_EmissioneRetta

        Em.ConnessioneGenerale = Session("DC_GENERALE")
        Em.ConnessioneOspiti = Session("DC_OSPITE")


        If Not Em.VerificaEffettiva() Then


            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono registrazioni dopo emmissione di prova, non posso rendere definitive');", True)
            REM Exit Sub

        End If




        Em.EliminaMovimentiDiProva(Chk_Documenti)


        Dim cn As New OleDbConnection

        cn.ConnectionString = Session("DC_GENERALE")

        cn.Open()


        Dim CmdTesta As New OleDbCommand

        CmdTesta.CommandText = "Select AnnoProtocollo,RegistroIva,max(DataRegistrazione) AS PiuAlta,max(DataRegistrazione) AS PiuBassa  From  Temp_MovimentiContabiliTesta Group by AnnoProtocollo,RegistroIva"
        CmdTesta.Connection = cn
        Dim ReadREg As OleDbDataReader = CmdTesta.ExecuteReader()
        Do While ReadREg.Read()
            Dim CmdTestaEffettivo As New OleDbCommand

            CmdTestaEffettivo.CommandText = "Select top 1 DataRegistrazione From  MovimentiContabiliTesta Where  AnnoProtocollo = ? And RegistroIva = ? Order by DataRegistrazione Desc"
            CmdTestaEffettivo.Connection = cn
            CmdTestaEffettivo.Parameters.AddWithValue("@AnnoProtocollo", campodbN(ReadREg.Item("AnnoProtocollo")))
            CmdTestaEffettivo.Parameters.AddWithValue("@RegistroIva", campodbN(ReadREg.Item("RegistroIva")))
            Dim RedEffettivo As OleDbDataReader = CmdTestaEffettivo.ExecuteReader()

            If RedEffettivo.Read Then
                If Format(campodbd(RedEffettivo.Item("DataRegistrazione")), "yyyy-MM-dd") > Format(campodbd(ReadREg.Item("PiuBassa")), "yyyy-MM-dd") Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione/i con data precedente a quella in archivio');", True)
                    Exit Sub
                End If
            End If
        Loop
        ReadREg.Close()


        Dim CmdRiga As New OleDbCommand
        CmdRiga.CommandText = "Select count(*) as registrazioneerrata From  Temp_MovimentiContabiliRiga Where importo  > 0 and (Select COUNT(*) from PianoConti Where Mastro = Temp_MovimentiContabiliRiga.MastroPartita and Conto = Temp_MovimentiContabiliRiga.ContoPartita and Sottoconto = Temp_MovimentiContabiliRiga.SottocontoPartita ) = 0"
        CmdRiga.Connection = cn
        Dim ReadRiga As OleDbDataReader = CmdRiga.ExecuteReader()
        If ReadRiga.Read() Then
            If campodbN(ReadRiga.Item("registrazioneerrata")) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione/i con conto non esistente nel piano dei conti');", True)
                Exit Sub
            End If
        End If
        ReadRiga.Close()

        Dim AppoggioNumero As Integer = 0

        AppoggioNumero = VerificaRegistrazione()
        If AppoggioNumero > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registrazione formalmente errata " & AppoggioNumero & "');", True)
            Exit Sub
        End If




        If Request.Item("TIPO") = "NONEMISSIONE" Then
            Em.DaProvaAdEffettiva(True)
        Else
            Em.DaProvaAdEffettiva()
        End If

        Dim CmdTestaPrivacy As New OleDbCommand

        CmdTestaPrivacy.CommandText = "Select * From  Temp_MovimentiContabiliTesta"
        CmdTestaPrivacy.Connection = cn
        Dim ReadTesta As OleDbDataReader = CmdTestaPrivacy.ExecuteReader()
        Do While ReadTesta.Read()
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", campodbN(ReadTesta.Item("NumeroRegistrazione")), "", "I", "DOCUMENTO", "")
        Loop
        ReadTesta.Close()


        Em.EliminaTemporanei()

        cn.Close()

        If Request.Item("URL") = "FatturaDaAddebitiAccrediti.aspx" Then
            Try
                Dim cnOspite As New OleDbConnection
                Dim Vettore(100) As Integer
                Dim I As Integer

                Vettore = Session("RIATTIVAADD")
                cnOspite.ConnectionString = Session("DC_OSPITE")

                cnOspite.Open()

                For I = 0 To 100
                    If Not IsNothing(Vettore(I)) Then
                        If Vettore(I) > 0 Then
                            Dim CmdUpdate As New OleDbCommand

                            CmdUpdate.Connection = cnOspite
                            CmdUpdate.CommandText = "UPDATE ADDACR SET ADDACR.NumeroRegistrazione =0,ADDACR.Elaborato = 0   WHERE DataModifica = {ts '" & Format(Now, "yyyy-MM-dd") & " 00:00:00'} AND ID = ?"
                            CmdUpdate.Parameters.AddWithValue("@ID", Vettore(I))
                            CmdUpdate.ExecuteNonQuery()
                        End If
                    End If
                Next
                cnOspite.Close()

            Catch ex As Exception

            End Try
        End If




        Btn_Modifica.Enabled = False
        Btn_Conferma.Enabled = False


        If Request.Item("TIPO") = "NONEMISSIONE" Then
            Exit Sub
        End If

        Dim XParam As New Cls_Parametri
        Dim KAnno As Long
        Dim KMese As Long

        XParam.LeggiParametri(Session("DC_OSPITE"))
        KMese = XParam.MeseFatturazione
        KAnno = XParam.AnnoFatturazione
        If KMese = 12 Then
            KMese = 1
            KAnno = KAnno + 1
        Else
            KMese = KMese + 1
            KAnno = KAnno
        End If
        XParam.MeseFatturazione = KMese
        XParam.AnnoFatturazione = KAnno
        'XParam.ScriviParametri(Session("DC_OSPITE"))



    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        Timer1.Enabled = True
        Session("CampoProgressBar") = ""
        Session("RagioneSocialeFattura") = ""
        Session("Download") = "SI"

        Session("TIPO") = Request.Item("TIPO")
        If Val(DD_Registro.SelectedValue) = 0 Then
            If DD_Fatture.SelectedValue = 0 Then
                If Btn_Conferma.Enabled = True Then
                    Dim cn As New OleDbConnection

                    cn.ConnectionString = Session("DC_GENERALE")

                    cn.Open()


                    Dim CmdTesta As New OleDbCommand

                    CmdTesta.CommandText = "Select RegistroIva From  Temp_MovimentiContabiliTesta Group by RegistroIva"
                    CmdTesta.Connection = cn
                    Dim ReadREg As OleDbDataReader = CmdTesta.ExecuteReader()
                    If ReadREg.Read() Then
                        If ReadREg.Read Then
                            ReadREg.Close()
                            cn.Close()
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il registro iva');", True)
                            Exit Sub

                        End If
                    End If
                    ReadREg.Close()
                    cn.Close()
                Else
                    If Val(DD_Registro.SelectedValue) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il registro iva');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If


        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
        System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) = ""
        If IsNothing(Request.Item("TIPO")) Then
            System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = ""
        Else
            System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = Request.Item("TIPO")
        End If



        If DD_Fatture.SelectedValue = 0 Then
            Session("RegistrazioneDal") = Val(Request.Item("RegistrazioneDal"))
            Session("RegistrazioneAl") = Val(Request.Item("RegistrazioneAl"))

            System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID) = Val(Request.Item("RegistrazioneDal"))
            System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID) = Val(Request.Item("RegistrazioneAl"))
        Else
            Session("RegistrazioneDal") = Val(DD_Fatture.SelectedValue)
            Session("RegistrazioneAl") = Val(DD_Fatture.SelectedValue)
            System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID) = Val(DD_Fatture.SelectedValue)
            System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID) = Val(DD_Fatture.SelectedValue)
        End If

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Timer1.Enabled = True
        Session("CampoProgressBar") = ""
        Session("RagioneSocialeFattura") = ""
        Session("Download") = ""

        Session("TIPO") = Request.Item("TIPO")
        If Val(DD_Registro.SelectedValue) = 0 Then
            If DD_Fatture.SelectedValue = 0 Then
                If Btn_Conferma.Enabled = True Then
                    Dim cn As New OleDbConnection

                    cn.ConnectionString = Session("DC_GENERALE")

                    cn.Open()


                    Dim CmdTesta As New OleDbCommand

                    CmdTesta.CommandText = "Select RegistroIva From  Temp_MovimentiContabiliTesta Group by RegistroIva"
                    CmdTesta.Connection = cn
                    Dim ReadREg As OleDbDataReader = CmdTesta.ExecuteReader()
                    If ReadREg.Read() Then
                        If ReadREg.Read Then
                            ReadREg.Close()
                            cn.Close()
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il registro iva');", True)
                            Exit Sub

                        End If
                    End If
                    ReadREg.Close()
                    cn.Close()
                Else
                    If Val(DD_Registro.SelectedValue) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il registro iva');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If


        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0
        System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + Session.SessionID) = ""
        If IsNothing(Request.Item("TIPO")) Then
            System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = ""
        Else
            System.Web.HttpRuntime.Cache("TIPO" + Session.SessionID) = Request.Item("TIPO")
        End If



        If DD_Fatture.SelectedValue = 0 Then
            Session("RegistrazioneDal") = Val(Request.Item("RegistrazioneDal"))
            Session("RegistrazioneAl") = Val(Request.Item("RegistrazioneAl"))

            System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID) = Val(Request.Item("RegistrazioneDal"))
            System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID) = Val(Request.Item("RegistrazioneAl"))
        Else
            Session("RegistrazioneDal") = Val(DD_Fatture.SelectedValue)
            Session("RegistrazioneAl") = Val(DD_Fatture.SelectedValue)
            System.Web.HttpRuntime.Cache("RegistrazioneDal" + Session.SessionID) = Val(DD_Fatture.SelectedValue)
            System.Web.HttpRuntime.Cache("RegistrazioneAl" + Session.SessionID) = Val(DD_Fatture.SelectedValue)
        End If

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub
End Class
