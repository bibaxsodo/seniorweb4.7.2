﻿
Partial Class Appalti_Parametri
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Write("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))


        Txt_Anno.Text = k.AnnoFatturazione


        Dd_Mese.SelectedValue = k.MeseFatturazione


        Txt_CodiceOperatore.Text = k.MastroAnticipo

        Txt_MansioneKM.Text = k.MandatoSIA

        Txt_Mastro.Text = k.Mastro
        Txt_ContoRegione.Text = k.ContoRegione

        Txt_MansioneAccesssi.Text = k.IdMandatoDaCF

    End Sub

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click

        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))


        k.AnnoFatturazione = Txt_Anno.Text


        k.MeseFatturazione = Dd_Mese.SelectedValue


        k.Mastro = Txt_Mastro.Text
        k.ContoRegione = Txt_ContoRegione.Text


        k.MastroAnticipo = Val(Txt_CodiceOperatore.Text)

        k.MandatoSIA = Txt_MansioneKM.Text

        k.IdMandatoDaCF = Txt_MansioneAccesssi.Text

        k.ScriviParametri(Session("DC_OSPITE"))

        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub
End Class
