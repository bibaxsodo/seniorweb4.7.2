﻿Imports System.Data.OleDb

Public Class GetValueCella
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        Giorno = context.Request.QueryString("Giorno")
        CodiceOperatore = context.Request.QueryString("CodiceOperatore")

        Anno = context.Request.QueryString("Anno")


        Mese = context.Request.QueryString("Mese")

        Appalto = context.Request.QueryString("Appalto")
        Struttura = context.Request.QueryString("Struttura")


        Dim cn As New OleDbConnection
        Dim Appoggio As String = ""


        Dim ConnectionString As String = context.Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "Select * From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  Data = ? And CodiceOperatore = ?"

        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
        Dim NonFlag As Boolean = False
        Dim RigheImport As String = ""


        Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
        Do While MovimentiGiorno.Read

            Dim Man As New Cls_AppaltiTabellaMansione


            Man.ID = campodbn(MovimentiGiorno.Item("IdMansione"))
            Man.Leggi(context.Session("DC_TABELLE"), Man.ID)


            RigheImport = RigheImport & campodbn(MovimentiGiorno.Item("Ore")) & " " & Man.CodificaImport & "<br/>"
            'Appoggio = Appoggio & "<div class=""Cella""  onclick=""ShowIndicaOre(" & campodbn(MovimentiGiorno.Item("CodiceOperatore")) & "," & Giorno & "," & Mese & "," & Anno & "," & Appalto & "," & Struttura & ");"">" & campodbn(MovimentiGiorno.Item("Ore")) & "<br/>" & Man.CodificaImport & "</div>"
            NonFlag = True
        Loop
        MovimentiGiorno.Close()
        If Not NonFlag Then
            Appoggio = Appoggio & "<div class=""Cella""  onclick=""ShowIndicaOre(" & CodiceOperatore & "," & Giorno & "," & Mese & "," & Anno & "," & Appalto & "," & Struttura & ");""></div>"
        Else
            Appoggio = Appoggio & "<div class=""Cella""  onclick=""ShowIndicaOre(" & CodiceOperatore & "," & Giorno & "," & Mese & "," & Anno & "," & Appalto & "," & Struttura & ");"">" & RigheImport & "</div>"
        End If

        CmdP.Clone()

        cn.Close()

        context.Response.Write(Appoggio)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
End Class