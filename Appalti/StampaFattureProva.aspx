﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_StampaFattureProva" CodeFile="StampaFattureProva.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Emissione Appalti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">


    <script type="text/javascript">      
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function VisualizzaDivRegistrazione() {
            $("#Documenti").css('visibility', 'visible');
        }

        function deletebox(cella) {
            $("div").remove("#RegistrazioneViewer");
            $("#RegistrazioneViewer").remove();
        }
        function apribox(NumeroRegistrazione, cella) {


            $.ajax({
                url: "'/WebHandler/TempRegistrazioneViewer.ashx?NumeroRegistrazione=" + NumeroRegistrazione,
                success: function (data, stato) {
                    $("body").append(data);
                    $("#RegistrazioneViewer").css('top', $("#" + cella).position().top + 30);
                    $("#RegistrazioneViewer").css('left', 300);
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }

        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>

    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>

</head>
<body onmouseover="deletebox(0);">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Stampa Emissione Appalti</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Stampa" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Conferma 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Button ID="Btn_Conferma" runat="server" Text="Confermi Emissione" OnClientClick="return window.confirm('Sei sicuro?');" /><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Errori">
                                <HeaderTemplate>
                                    Errori e Warning
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div style="width: 700px; height: 450px; padding: 2px; overflow: auto; border: 1px solid #ccc;" id="Warning">
                                        <asp:Label ID="Label1" runat="server" Width="600px"></asp:Label>
                                    </div>
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Documenti Creati
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div>
                                        <div style="width: 700px; height: 450px; padding: 2px; overflow: auto; border: 1px solid #ccc;" id="Documenti">
                                            <asp:CheckBoxList ID="Chk_Documenti" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Stampa Documenti
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div>
                                        <label class="LabelCampo">Registro :</label>
                                        <asp:DropDownList ID="DD_Registro" runat="server"></asp:DropDownList><br />
                                        <br />
                                        <label class="LabelCampo">Fattura :</label>
                                        <asp:DropDownList ID="DD_Fatture" class="chosen-select" runat="server"></asp:DropDownList><br />
                                        <br />
                                        <label class="LabelCampo">Seleziona Report:</label>
                                        <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList><br />
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Campo Personalizzati:</label><br />
                                        <asp:TextBox ID="Txt_Campo1" MaxLength="50" runat="server" Width="789px"></asp:TextBox>
                                        <br />
                                        <asp:TextBox ID="Txt_Campo2" MaxLength="50" runat="server" Width="789px"></asp:TextBox>
                                        <br />
                                        <asp:TextBox ID="Txt_Campo3" MaxLength="50" runat="server" Width="789px"></asp:TextBox>
                                        <br />

                                    </div>
                                    </div>
                                </ContentTemplate>
                            </xasp:TabPanel>



                        </xasp:TabContainer>

                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Timer ID="Timer1" runat="server" Interval="1000"></asp:Timer>
                                    <asp:Label ID="Lbl_Waiting" runat="server" Text=""></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
