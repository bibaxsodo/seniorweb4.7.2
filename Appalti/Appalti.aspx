﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Appalti" CodeFile="Appalti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Commessa</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse - Commessa</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Commessa
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Id</label>
                                    <asp:TextBox ID="Txt_Id" runat="server" MaxLength="8" Width="100px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Descrizione </label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" MaxLength="100" Width="350px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Committente </label>
                                    <asp:DropDownList ID="DD_Usl" runat="server" class="chosen-select"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Gruppo</label>
                                    <asp:TextBox ID="Txt_Gruppo" runat="server" MaxLength="50" Width="350px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Cig :</label>
                                    <asp:TextBox ID="Txt_Cig" runat="server" MaxLength="100" Width="350px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">DataInizio :</label>
                                    <asp:TextBox ID="Txt_DataInizio" MaxLength="130" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">DataFine :</label>
                                    <asp:TextBox ID="Txt_DataFine" runat="server" MaxLength="5" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Causale Contabile :</label>
                                    <asp:DropDownList ID="DD_CausaleContabile" runat="server" class="chosen-select" Width="350px"></asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">Raggruppa Ricavi:</label>
                                    <asp:CheckBox ID="Chk_RaggruppaRicavi" runat="server" Text="" /><br />
                                    <br />

                                    <label class="LabelCampo">Modalità Fat. :</label>
                                    <asp:CheckBox ID="Chk_ModFatt" runat="server" Text="Fatture separate per struttura" /><br />
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Tipo :</label>
                                    <asp:RadioButton ID="RB_Forfait" Checked runat="server" AutoPostBack="true" Text="Appalto a Forfait" GroupName="Tipo" />
                                    <asp:RadioButton ID="RB_ImportoVaribile" runat="server" AutoPostBack="true" Text="Importo Variabile" GroupName="Tipo" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Importo Forfait : </label>
                                    <asp:TextBox ID="Txt_ImportoForfait" runat="server" Style="text-align: right;" Width="220px" MaxLength="27"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto :</label>
                                    <asp:TextBox ID="Txt_Conto" runat="server" Width="350px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Codice IVA :</label>
                                    <asp:DropDownList ID="DD_CodiceIVA" runat="server"></asp:DropDownList>



                                </ContentTemplate>

                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Strutture
                 
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <hr />
                                    <br />
                                    Dati IVA e Conto Ricavo per KM
          <br />
                                    <br />
                                    <label class="LabelCampo">Conto :</label>
                                    <asp:TextBox ID="Txt_ContoKM" runat="server" Width="350px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Codice IVA :</label>
                                    <asp:DropDownList ID="DD_CodiceIVAKM" runat="server"></asp:DropDownList>
                                    <br />
                                    <hr />
                                    <br />
                                    <asp:GridView ID="Grd_Retta" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Struttura">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Struttura" runat="server" class="chosen-select"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="350px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Importo Forfait">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_ImportoForait" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="KM">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_KM" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Importo KM">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_ImportoKM" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>



                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </ContentTemplate>

                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Allegati
                 
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Modify" CommandName="Edit" runat="Server" ImageUrl="~/images/modifica.png" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="IB_Update" CommandName="Update" runat="Server" ImageUrl="~/images/aggiorna.png" />
                                                    <asp:ImageButton ID="IB_Cancel" CommandName="Cancel" runat="Server" ImageUrl="~/images/annulla.png" />
                                                </EditItemTemplate>
                                                <ItemStyle Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Printer" CommandName="DOWNLOAD" runat="Server" ToolTip="Download File" ImageUrl="~/images/download.png" CommandArgument='<%#  Eval("NomeFile") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="40px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NOME FILE">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="286px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="286px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_DELETEFILE" CommandName="CANCELLAFILE" ToolTip="Elimina File" OnClientClick="return window.confirm('Eliminare?');" runat="Server" ImageUrl="../images/elimina.jpg" CommandArgument='<%#  Container.DataItemIndex  %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="40px" />
                                            </asp:TemplateField>
                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White"
                                            BorderColor="#6FA7D1" BorderWidth="1px" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    Carica un file :<br />
                                    <div id="upload-file-container">
                                        <div style="text-align: center; width: 300px; padding-top: 15px;" id="nomefile"></div>
                                        <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" />
                                    </div>
                                    <br />
                                    <br />
                                    <asp:Button ID="UpLoadFile" runat="server" Height="28px" Width="88px" ToolTip="Up Load File" BackColor="#007DC4" ForeColor="White" Text="Carica File" />

                                </ContentTemplate>

                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel3">
                                <HeaderTemplate>
                                    Budget
                 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Teorico Mensile : </label>
                                    <asp:TextBox ID="Txt_ImportoTeoricoMensile" runat="server" Style="text-align: right;" Width="220px" MaxLength="27"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Teorico Giornaliero : </label>
                                    <asp:TextBox ID="Txt_ImportoTeoricoGiornaliero" runat="server" Style="text-align: right;" Width="220px" MaxLength="27"></asp:TextBox>
                                    Teorico Mensile In aggiunta al Giornaliero :
        <asp:TextBox ID="Txt_ImportoTeoricoMensileInaddGiornaliero" runat="server" Style="text-align: right;" Width="220px" MaxLength="27"></asp:TextBox><br />
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grid_Budget" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="TipoOperatore">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_TipoOperatore" runat="server" class="chosen-select"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="350px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Ore">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_Ore" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Importo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>




                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </ContentTemplate>

                            </xasp:TabPanel>




                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel4">
                                <HeaderTemplate>Righe Aggiuntive</HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grd_RigheAggiuntive" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Conto">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtConto" runat="server" Text="" Width="300px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="310px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Struttura">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Struttura" class="chosen-select2" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="310px" />
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="REGOLA">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Regola" runat="server" class="chosen-select2">
                                                        <asp:ListItem Value="F" Text="Fisso"></asp:ListItem>
                                                        <asp:ListItem Value="O" Text="Orario"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="310px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="IVA">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_IVA" runat="server" class="chosen-select2"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="310px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Qunatita">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_Quantita" runat="server" Text="" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Importo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_Importo" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Descrizione">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="300px"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="310px" />
                                            </asp:TemplateField>


                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
