﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_EliminaPrestazioni
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MAppalto As New Cls_AppaltiAnagrafica

        MAppalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)

        Dim MOperatore As New Cls_Operatore

        MOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore)

        Dim Cooperativa As New Cls_Appalti_Cooperative


        Cooperativa.UpDateDropBox(Session("DC_TABELLE"), DD_Cooperativa)


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_ImportExport.aspx")
    End Sub

    Protected Sub DD_Appalto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Appalto.SelectedIndexChanged

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBoxAppalto(Session("DC_TABELLE"), DD_Struttura, Val(DD_Appalto.SelectedValue))
    End Sub

    Protected Sub Img_EliminaMese_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_EliminaMese.Click
        If Val(Txt_Anno.Text) = 0 Then
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Appalto.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Struttura.SelectedValue) = 0 Then
            Exit Sub
        End If

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""

        Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
        If Condizione <> "" Then
            Condizione = Condizione & " And "
        End If
        Condizione = Condizione & " month(Data)  = " & Val(Dd_Mese.SelectedValue) & " And year(Data) = " & Val(Txt_Anno.Text)
        If Condizione <> "" Then
            Condizione = Condizione & " And "
        End If
        Condizione = Condizione & " IdStrutture  = " & Val(DD_Struttura.SelectedValue)

        If Val(DD_Cooperativa.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " codicecooperativa  = " & Val(DD_Cooperativa.SelectedValue)

        End If

        cmd.CommandText = "Delete  From Appalti_Prestazioni where " & Condizione

        cmd.Connection = cn

        cmd.ExecuteNonQuery()


        Response.Redirect("Menu_ImportExport.aspx")

    End Sub
End Class
