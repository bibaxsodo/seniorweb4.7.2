﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_AnagraficaUtenti" CodeFile="AnagraficaUtenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Utenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse -  Anagrafica Utenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica Utenti
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Codice Utente</label>
                                    <asp:TextBox ID="Txt_CodiceUtente" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Cognome :</label>
                                    <asp:TextBox ID="Txt_CogNome" runat="server" MaxLength="30" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Nome :</label>
                                    <asp:TextBox ID="Txt_Nome" runat="server" MaxLength="30" Width="440px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data Nascita :</label>
                                    <asp:TextBox ID="Txt_DataNascita" runat="server" MaxLength="30" Width="100px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale </label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" MaxLength="16"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" runat="server" MaxLength="30" Width="453px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_ComRes" MaxLength="60" runat="server" Width="350px"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" runat="server" MaxLength="16" Width="108px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Telefono Cellulare :</label>
                                    <asp:TextBox ID="Txt_TelefonoCellulare" runat="server" MaxLength="30" Width="98px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Utente :</label>
                                    <asp:DropDownList ID="DD_TipoUtente" runat="server" Width="98px"></asp:DropDownList>
                                    <br />

                                </ContentTemplate>

                            </xasp:TabPanel>



                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
