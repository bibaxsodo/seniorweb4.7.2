﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.Data.OleDb
Imports SeniorWebApplication

Partial Class Appalti_StampaAllegati
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim ConnessioneOspiti As String

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        ConnessioneOspiti = Session("DC_OSPITE")

        Dim f As New Cls_Parametri


        f.LeggiParametri(ConnessioneOspiti)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

        Session("CampoWr") = ""
        Session("CampoErrori") = ""
        Call EseguiJS()


        Dim LS As New Cls_AppaltiAnagrafica


        LS.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("APPALTIALLEGATI2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("APPALTIALLEGATI2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "APPALTIALLEGATI2"
        End If

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        ElaboraXsd()


        If DD_Report.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=APPALTIALLEGATI','Stampe4','width=800,height=600');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "','Stampe4','width=800,height=600');", True)
        End If
    End Sub


    Private Sub ElaboraXsd()
        Dim ConnessioneTabelle As String

        ConnessioneTabelle = Session("DC_TABELLE")

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnessioneTabelle)
        cn.Open()

        Dim cmd As New OleDbCommand()
        Dim UltimoID As Integer

        If Val(DD_Appalto.SelectedValue) = 0 Then
            cmd.CommandText = "Select [IdAppalto],[IdStruttura] ,[IdMansione] ,[CodiceOperatore],[CodiceIVA],sum(Imponibile) As TotImponibile,sum(Ore) as TotOre,sum(Accessi) as TotAccessi,intypecod,intypedescription   From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And CalcoloPassivo = 0 group by [IdAppalto],[IdStruttura] ,[IdMansione] ,[CodiceOperatore],[CodiceIVA],intypecod,intypedescription  "
        Else
            cmd.CommandText = "Select [IdAppalto],[IdStruttura] ,[IdMansione] ,[CodiceOperatore],[CodiceIVA],sum(Imponibile) As TotImponibile,sum(Ore) as TotOre,sum(Accessi) as TotAccessi,intypecod,intypedescription    From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And IdAppalto = " & DD_Appalto.SelectedValue & " And CalcoloPassivo = 0 group by [IdAppalto],[IdStruttura] ,[IdMansione] ,[CodiceOperatore],[CodiceIVA],intypecod,intypedescription  "
        End If

        cmd.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))

        cmd.Parameters.AddWithValue("@Mese", Val(Dd_Mese.SelectedValue))

        cmd.Connection = cn

        Dim Imposta As Double = 0
        Dim cmdVr As New OleDbCommand()

        If Val(DD_Appalto.SelectedValue) = 0 Then
            cmdVr.CommandText = "Select [CodiceIVA],sum(Imponibile) As TotImponibile,intypecod,intypedescription   From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And CalcoloPassivo = 0 group by [CodiceIVA],intypecod,intypedescription"
        Else
            cmdVr.CommandText = "Select [CodiceIVA],sum(Imponibile) As TotImponibile,intypecod,intypedescription  From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And IdAppalto = " & DD_Appalto.SelectedValue & "  And CalcoloPassivo = 0 group by [CodiceIVA],intypecod,intypedescription"
        End If

        cmdVr.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))

        cmdVr.Parameters.AddWithValue("@Mese", Val(Dd_Mese.SelectedValue))

        cmdVr.Connection = cn

        Dim PsVr As OleDbDataReader = cmdVr.ExecuteReader()
        Do While PsVr.Read
            Dim CodiceIVA As New Cls_IVA

            CodiceIVA.Codice = campodb(PsVr.Item("CodiceIVA"))
            CodiceIVA.Leggi(ConnessioneTabelle, CodiceIVA.Codice)
            Imposta = Imposta + Math.Round(campodbN(PsVr.Item("TotImponibile")) * CodiceIVA.Aliquota, 2)
        Loop
        PsVr.Close()

        Dim Stampa As New Cls_Appalti_AppaltiStrutture

        Dim Scheda As New AppaltiXSD
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Riga As System.Data.DataRow = Scheda.Tables("Allegati").NewRow

            Riga.Item("IdAppalto") = campodbN(myPOSTreader.Item("IdAppalto"))
            Dim MApalt As New Cls_AppaltiAnagrafica

            MApalt.Id = campodbN(myPOSTreader.Item("IdAppalto"))
            MApalt.Leggi(ConnessioneTabelle)

            Dim MUsl As New ClsUSL


            MUsl.CodiceRegione = MApalt.Committente
            MUsl.Leggi(Session("DC_OSPITE"))

            Riga.Item("Committente") = MUsl.Nome

            Riga.Item("Cig") = MUsl.CodiceCig
            Riga.Item("Cig") = MUsl.CodiceCig


            If MUsl.PARTITAIVA = 0 Then
                Riga.Item("PivaCf") = MUsl.CodiceFiscale
            Else
                Riga.Item("PivaCf") = Format(MUsl.PARTITAIVA, "00000000000")
            End If

            Riga.Item("CAP") = MUsl.RESIDENZACAP1

            Dim mCom As New ClsComune

            mCom.Provincia = MUsl.RESIDENZAPROVINCIA1
            mCom.Comune = MUsl.RESIDENZACOMUNE1
            mCom.Leggi(Session("DC_OSPITE"))

            Riga.Item("Citta") = mCom.Descrizione


            Riga.Item("Periodo") = Dd_Mese.SelectedItem.Text & "/" & Txt_Anno.Text



            Riga.Item("Appalto") = MApalt.Descrizione


            Riga.Item("IdStruttura") = campodbN(myPOSTreader.Item("IdStruttura"))

            Dim MStruttura As New Cls_AppaltiTabellaStruttura

            MStruttura.ID = campodbN(myPOSTreader.Item("IdStruttura"))

            MStruttura.Leggi(ConnessioneTabelle, MStruttura.ID)


            If campodbN(myPOSTreader.Item("IdMansione")) = 99999 Then
                If MStruttura.ID = 0 Then
                    MStruttura.Descrizione = "zzz"
                End If
            End If


            Riga.Item("Struttura") = MStruttura.Descrizione

            Riga.Item("IdMansione") = campodbN(myPOSTreader.Item("IdMansione"))

            Dim MMansione As New Cls_AppaltiTabellaMansione

            MMansione.ID = campodbN(myPOSTreader.Item("IdMansione"))
            MMansione.Leggi(ConnessioneTabelle, MMansione.ID)

            Riga.Item("Mansione") = MMansione.Descrizione
            If campodbN(myPOSTreader.Item("IdMansione")) = 99999 Then
                Riga.Item("Mansione") = "zzz"
            End If

            If campodbN(myPOSTreader.Item("IdMansione")) = 0 Then
                Riga.Item("Mansione") = "Km "
            End If
            Riga.Item("CodiceOperatore") = campodbN(myPOSTreader.Item("CodiceOperatore"))

            Dim MOperatore As New Cls_Operatore

            MOperatore.CodiceMedico = campodbN(myPOSTreader.Item("CodiceOperatore"))
            MOperatore.Leggi(Session("DC_OSPITE"))

            Riga.Item("NomeOperatore") = MOperatore.Nome
            If campodbN(myPOSTreader.Item("CodiceOperatore")) = 0 Then
                Riga.Item("NomeOperatore") = "Km "
            End If

            If MOperatore.TipoOperatore <> "" Then
                Dim TpOpe As New Cls_TipoOperatore

                TpOpe.Leggi(Session("DC_OSPITE"), MOperatore.TipoOperatore)

                Riga.Item("TipoOperatore") = TpOpe.Descrizione
            Else
                Riga.Item("TipoOperatore") = ""
            End If

            Dim MP As New Cls_IVA

            MP.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
            MP.Leggi(Session("DC_TABELLE"), MP.Codice)

            Riga.Item("CodiceIVA") = MP.Descrizione

            If MApalt.Tipo = "F" And campodbN(myPOSTreader.Item("IdMansione")) <> 99999 Then
                Riga.Item("Imponibile") = 0
            Else
                Riga.Item("Imponibile") = campodbN(myPOSTreader.Item("TotImponibile"))
            End If

            Riga.Item("Ore") = campodbN(myPOSTreader.Item("TotOre"))


            Riga.Item("Imposta") = Imposta



            Riga.Item("intypecod") = campodb(myPOSTreader.Item("intypecod"))

            Riga.Item("intypedescription") = campodb(myPOSTreader.Item("intypedescription"))

            Scheda.Tables("Allegati").Rows.Add(Riga)
        Loop
        cn.Close()

        Session("stampa") = Scheda
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

End Class
