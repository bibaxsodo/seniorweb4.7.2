﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting
Partial Class Appalti_ControlloPrestazioni
    Inherits System.Web.UI.Page
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Now.Month

    End Sub

    Protected Sub Btn_Elabora_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elabora.Click

        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Anno elaborazione');", True)
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Mese elaborazione');", True)
            Exit Sub
        End If


        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        Lbl_Errori.Text = ""

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim MySql As String = "SELECT IdAppalto,IdStrutture, IdMansione FROM [Appalti_Prestazioni]  where  MONTH(Data)= ? And YEAR(Data) = ?  And  (select count(*) from Appalti_StruttureMansioni where Appalti_StruttureMansioni .IdMansione = [Appalti_Prestazioni].IdMansione and Appalti_StruttureMansioni.IdAppalto = [Appalti_Prestazioni].IdAppalto and (Appalti_StruttureMansioni.IdStrutture = [Appalti_Prestazioni].IdStrutture or Appalti_StruttureMansioni.IdStrutture = 0) ) =0 group by IdAppalto,IdStrutture, IdMansione"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120

        cmd.Parameters.AddWithValue("@Mese", Dd_Mese.SelectedValue)
        cmd.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))


        Dim MRead As OleDbDataReader = cmd.ExecuteReader()

        Do While MRead.Read

            If Param.IdMandatoDaCF <> campodbN(MRead.Item("IdMansione")) Then
                Dim Mansione As New Cls_AppaltiTabellaMansione

                Mansione.ID = campodbN(MRead.Item("IdMansione"))
                Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)


                Dim Struttura As New Cls_AppaltiTabellaStruttura

                Struttura.ID = campodbN(MRead.Item("IdStrutture"))
                Struttura.Leggi(Session("DC_TABELLE"), Struttura.ID)



                Dim Appalto As New Cls_AppaltiAnagrafica

                Appalto.Id = campodbN(MRead.Item("IdAppalto"))
                Appalto.Leggi(Session("DC_TABELLE"))


                Lbl_Errori.Text = Lbl_Errori.Text & "Mansione " & Mansione.Descrizione & " non presente per appalto : " & Appalto.Descrizione & " Struttura " & Struttura.Descrizione & vbNewLine
            End If
        Loop
        cn.Close()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_ImportExport.aspx")

    End Sub
End Class
