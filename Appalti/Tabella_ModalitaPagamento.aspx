﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Tabella_ModalitaPagamento" CodeFile="Tabella_ModalitaPagamento.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Modalita Pagamento</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">   
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commessse - Modalità Pagamento</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Imb_Duplica"></asp:ImageButton>&nbsp;
        <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Inserisci/Modifica (F2)" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Modalità Pagamento" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Modalità Pagamento                     
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Codice : </label>
                                    <asp:TextBox ID="Txt_Codice" MaxLength="2" AutoPostBack="true" runat="server" Width="51px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCod" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione : </label>
                                    <asp:TextBox ID="Txt_Descrizione" MaxLength="50" AutoPostBack="true" runat="server" Width="341px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" /><br />
                                    <br />
                                    <label class="LabelCampo">Sottoconto : </label>
                                    <asp:TextBox ID="Txt_Sottoconto" MaxLength="50" runat="server" Width="341px"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Modalita Pagamento : </label>
                                    <asp:DropDownList ID="DD_ModalitaPagamento" runat="server"></asp:DropDownList><br />

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione Estesa : </label>
                                    <asp:TextBox ID="Txt_DescrizioneEstea" MaxLength="50" runat="server" TextMode="MultiLine" Height="100px" Width="341px"></asp:TextBox>
                                    <br />
                                    <br />
                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>
