﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_TabelleRegioni" CodeFile="TabelleRegioni.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Tabella Committenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function DialogBox(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }

    </script>
    <style>
        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: Cuprum, Roboto, Calibri, 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 16px;
            height: 615px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }

        .cornicegrigia2 {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: Cuprum, Roboto, Calibri, 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 16px;
            height: 325px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse - Committenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Btn_Duplica"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server"
                                OnClientClick="return window.confirm('Eliminare?');"
                                ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina"
                                ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <label class="MenuDestra"><a href="#" onclick="DialogBox('../GeneraleWeb/PianoConti.aspx');">Piano Conti</a></label>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Codice :</label>
                                                <asp:TextBox ID="Txt_Codice" MaxLength="4" runat="server" Width="50px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Descrizione :</label>
                                                <asp:TextBox ID="Txt_Descrizione" runat="server" Width="352px" MaxLength="50"></asp:TextBox>
                                                &nbsp;Nome In Visualizzazione&nbsp;
        <asp:TextBox ID="Txt_NomeConiuge" runat="server" Width="200px" MaxLength="50" AutoPostBack="true"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">All' attenzione di :</label>
                                                <asp:TextBox ID="Txt_Attenzione" runat="server" Width="320px" MaxLength="40"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Indirizzo :</label>
                                                <asp:TextBox ID="Txt_Indirizzo" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Cap :</label>
                                                <asp:TextBox ID="Txt_Cap" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="112px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Comune :</label>
                                                <asp:TextBox ID="Txt_ComRes" runat="server" Width="300px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Partita IVA :</label>
                                                <asp:TextBox ID="Txt_Piva" MaxLength="11" onkeypress="return soloNumeri(event);" runat="server" Width="120px" AutoPostBack="true"></asp:TextBox>
                                                <asp:Image ID="Img_VerPIVA" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                                Codice Fiscale :
       <asp:TextBox ID="Txt_CodiceFiscale" MaxLength="16" runat="server" Width="112px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Conto : </label>
                                                <asp:TextBox ID="Txt_Sottoconto" runat="server" Width="350px"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Modalità Pagamento:</label>
                                                <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="248px"></asp:DropDownList>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Mail :</label>
                                                <asp:TextBox ID="TxT_Mail" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />

                                                <br />
                                                <label class="LabelCampo">Referente :</label>
                                                <asp:TextBox ID="Txt_Referente" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Telefono :</label>
                                                <asp:TextBox ID="Txt_TelefonoReferente" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Codice Destinatario :</label>
                                                <asp:TextBox ID="Txt_CodiceDestinatario" runat="server" Width="384px" MaxLength="50"></asp:TextBox><br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>




                                </ContentTemplate>
                            </xasp:TabPanel>


                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
