﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class Appalti_GrigliaSoloStampa
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Txt_Parametri.Text = Session("ParametriSoloStampa")
        MyTable = Session("GrigliaSoloStampa")



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub
End Class