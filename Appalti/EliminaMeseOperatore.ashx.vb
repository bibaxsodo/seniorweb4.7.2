﻿Imports System.Data.OleDb

Public Class EliminaMeseOperatore
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim Anno As Integer
        Dim Mese As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        CodiceOperatore = context.Request.QueryString("CodiceOperatore")

        Anno = context.Request.QueryString("Anno")


        Mese = context.Request.QueryString("Mese")

        Appalto = context.Request.QueryString("Appalto")
        Struttura = context.Request.QueryString("Struttura")

        Dim cn As New OleDbConnection
        Dim Appoggio As String = ""


        Dim ConnectionString As String = context.Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "delete From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  year(Data) = ? and month(datA) = ? And CodiceOperatore = ?"

        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Anno", Anno)
        CmdP.Parameters.AddWithValue("@Mese", Mese)
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)

        CmdP.ExecuteNonQuery()

        cn.Close()

        context.Response.Write("OK")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class