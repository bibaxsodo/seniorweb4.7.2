﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class Appalti_Tabella_Operatore
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        myriga(1) = ""
        myriga(2) = 0
        myriga(3) = 0
        myriga(4) = 0
        myriga(5) = 0

        myriga(6) = 0
        myriga(7) = 0
        myriga(8) = 0
        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable

        Grd_Cella.DataBind()




        Call EseguiJS()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim TpOpe As New Cls_TipoOperatore

        TpOpe.UpDateDropBox(Session("DC_OSPITE"), DD_TipoOperatore)



        If Val(Request.Item("CODICE")) = 0 Then
            Dim CodOpe As New Cls_Operatore

            Txt_CodiceMedico.Text = CodOpe.MaxOperatore(Session("DC_OSPITE"))
            Exit Sub
        End If



        Dim Med As New Cls_Operatore

        Med.CodiceMedico = Request.Item("CODICE")
        Med.Leggi(Session("DC_OSPITE"))
        Txt_CodiceMedico.Text = Med.CodiceMedico
        Txt_Nome.Text = Med.Nome
        Txt_CodiceFiscale.Text = Med.CodiceFiscale
        Txt_Indirizzo.Text = Med.RESIDENZAINDIRIZZO1

        Lbl_NomeOspite.Text = Med.Nome


        Dim DeCom As New ClsComune
        DeCom.Comune = Med.RESIDENZACOMUNE1
        DeCom.Provincia = Med.RESIDENZAPROVINCIA1
        DeCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = DeCom.Comune & " " & DeCom.Provincia & " " & DeCom.Descrizione

        Txt_Telefono.Text = Med.Telefono1
        Txt_TelefonoCellulare.Text = Med.Telefono2
        Txt_Specializzazione.Text = Med.Specializazione
        Txt_Note.Text = Med.Note

        DD_TipoOperatore.SelectedValue = Med.TipoOperatore


        Txt_CodiceMedico.Enabled = False


        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "V", "OPERATORE", "")




        Dim Appalto As New Cls_Appalti_Programmazione


        Appalto.IdOperatore = Med.CodiceMedico
        Appalto.loaddati(Session("DC_TABELLE"), Appalto.IdOperatore, MyTable)

        If MyTable.Rows.Count <= 0 Then
            MyTable.Clear()
            MyTable.Columns.Clear()
            

            MyTable.Columns.Add("Regole", GetType(String))
            MyTable.Columns.Add("Data", GetType(String))
            MyTable.Columns.Add("Numero", GetType(String))
            MyTable.Columns.Add("Giorni", GetType(String))
            MyTable.Columns.Add("Mansione", GetType(String))
            MyTable.Columns.Add("Appalto", GetType(String))
            MyTable.Columns.Add("Struttura", GetType(String))
            MyTable.Columns.Add("Utente", GetType(String))
            MyTable.Columns.Add("Ore", GetType(String))

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = 0

            myrigaR(1) = 0

            myrigaR(2) = 0
            myrigaR(3) = 0
            myrigaR(4) = 0
            myrigaR(5) = 0

            myrigaR(6) = 0
            myrigaR(7) = 0
            myrigaR(8) = 0

            MyTable.Rows.Add(myrigaR)

        End If


        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        EseguiJS()
    End Sub




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Med As New Cls_Operatore
        If Val(Txt_CodiceMedico.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice, codice deve essere numerico');", True)
            Exit Sub
        End If
        If Txt_Nome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare cognome nome');", True)
            Exit Sub
        End If


        If Request.Item("CODICE") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "I", "OPERATORE", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_Operatore

            OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
            OldDatiPar.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "M", "OPERATORE", AppoggioJS)

        End If

        Med.CodiceMedico = Txt_CodiceMedico.Text
        Med.Nome = Txt_Nome.Text
        Med.CodiceFiscale = Txt_CodiceFiscale.Text
        Med.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text


        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                Med.RESIDENZAPROVINCIA1 = Vettore(0)
                Med.RESIDENZACOMUNE1 = Vettore(1)
            End If
        End If


        Med.Telefono1 = Txt_Telefono.Text
        Med.Telefono2 = Txt_TelefonoCellulare.Text
        Med.Specializazione = Txt_Specializzazione.Text
        Med.Note = Txt_Note.Text


        Med.TipoOperatore = DD_TipoOperatore.SelectedValue

        Med.Scrivi(Session("DC_OSPITE"))


        UpDateTable()
        MyTable = ViewState("App_Retta")

        Dim TipoASet As New Cls_Appalti_Programmazione


        TipoASet.IdOperatore = Med.CodiceMedico
        TipoASet.AggiornaDaTabella(Session("DC_TABELLE"), MyTable)



        Call PaginaPrecedente()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_CodiceMedico.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        Dim MyMed As New Cls_Operatore
        MyMed.CodiceMedico = Txt_CodiceMedico.Text

        If MyMed.VerificaElimina(Session("DC_OSPITE")) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso utilizzare codice usate');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Operatore

        OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
        OldDatiPar.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "D", "OPERATORE", AppoggioJS)

        MyMed.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub
    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Operatore.aspx")
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""250px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Grd_Cella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Cella.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = 0
                        myriga(1) = 0
                        myriga(2) = 0
                        myriga(3) = 0
                        myriga(4) = 0
                        myriga(5) = 0


                        myriga(6) = 0
                        myriga(7) = 0
                        myriga(8) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = 0
                    myriga(1) = 0
                    myriga(2) = 0
                    myriga(3) = 0
                    myriga(4) = 0
                    myriga(5) = 0

                    myriga(6) = 0
                    myriga(7) = 0
                    myriga(8) = 0

                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try




        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Grd_Cella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Cella.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub


    Protected Sub Grd_Cella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Cella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DD_Regola As DropDownList = DirectCast(e.Row.FindControl("DD_Regola"), DropDownList)
            DD_Regola.SelectedValue = campodbn(MyTable.Rows(e.Row.RowIndex).Item(0).ToString)


            Dim Txt_Data As TextBox = DirectCast(e.Row.FindControl("Txt_Data"), TextBox)

            If campodb(MyTable.Rows(e.Row.RowIndex).Item(1).ToString) = "" Then
                Txt_Data.Text = campodb(MyTable.Rows(e.Row.RowIndex).Item(1).ToString)
            Else
                Txt_Data.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString
            End If

            Dim Txt_Numero As TextBox = DirectCast(e.Row.FindControl("Txt_Numero"), TextBox)


            Dim Txt_Intervallo As TextBox = DirectCast(e.Row.FindControl("Txt_Intervallo"), TextBox)


            Txt_Numero.Text = campodbn(MyTable.Rows(e.Row.RowIndex).Item(2).ToString)


            Dim Chk_Lunedi As CheckBox = DirectCast(e.Row.FindControl("Chk_Lunedi"), CheckBox)
            Dim Chk_Martedi As CheckBox = DirectCast(e.Row.FindControl("Chk_Martedi"), CheckBox)
            Dim Chk_Mercoledi As CheckBox = DirectCast(e.Row.FindControl("Chk_Mercoledi"), CheckBox)
            Dim Chk_Giovedi As CheckBox = DirectCast(e.Row.FindControl("Chk_Giovedi"), CheckBox)
            Dim Chk_Venerdì As CheckBox = DirectCast(e.Row.FindControl("Chk_Venerdì"), CheckBox)
            Dim Chk_Sabato As CheckBox = DirectCast(e.Row.FindControl("Chk_Sabato"), CheckBox)
            Dim Chk_Domenica As CheckBox = DirectCast(e.Row.FindControl("Chk_Domenica"), CheckBox)
            Dim Dd_Mese As DropDownList = DirectCast(e.Row.FindControl("Dd_Mese"), DropDownList)

            If DD_Regola.SelectedValue = "0" Then

                Txt_Intervallo.Visible = False
            End If

            If DD_Regola.SelectedValue = "1" Then


                Txt_Data.Visible = True
                Txt_Numero.Visible = True
                Txt_Intervallo.Visible = False
            End If

            If DD_Regola.SelectedValue = "2" Then

                Txt_Data.Visible = True
                Txt_Numero.Visible = True
                Txt_Intervallo.Visible = True

                Chk_Lunedi.Visible = False
                Chk_Martedi.Visible = False
                Chk_Mercoledi.Visible = False

                Chk_Giovedi.Visible = False
                Chk_Venerdì.Visible = False
                Chk_Sabato.Visible = False
                Chk_Domenica.Visible = False

            End If
            If DD_Regola.SelectedValue = "4" Then
                Txt_Data.Visible = True
                Txt_Numero.Visible = False

                Chk_Lunedi.Visible = False
                Chk_Domenica.Visible = False
                Chk_Giovedi.Visible = False
                Chk_Martedi.Visible = False
                Chk_Mercoledi.Visible = False
                Chk_Sabato.Visible = False
                Chk_Venerdì.Visible = False
                Txt_Intervallo.Visible = False



            End If


            If DD_Regola.SelectedValue = "3" Then
                Txt_Data.Visible = True
                Txt_Numero.Visible = True

                Chk_Lunedi.Visible = False
                Chk_Domenica.Visible = False
                Chk_Giovedi.Visible = False
                Chk_Martedi.Visible = False
                Chk_Mercoledi.Visible = False
                Chk_Sabato.Visible = False
                Chk_Venerdì.Visible = False
                Txt_Intervallo.Visible = False



            End If





            Dim GiorniSettimana As String = ""

            GiorniSettimana = campodb(MyTable.Rows(e.Row.RowIndex).Item(3).ToString)

            If GiorniSettimana.IndexOf("<1>") >= 0 Then
                Chk_Lunedi.Checked = True
            End If
            If GiorniSettimana.IndexOf("<2>") >= 0 Then
                Chk_Martedi.Checked = True
            End If
            If GiorniSettimana.IndexOf("<3>") >= 0 Then
                Chk_Mercoledi.Checked = True
            End If
            If GiorniSettimana.IndexOf("<4>") >= 0 Then
                Chk_Giovedi.Checked = True
            End If
            If GiorniSettimana.IndexOf("<5>") >= 0 Then
                Chk_Venerdì.Checked = True
            End If
            If GiorniSettimana.IndexOf("<6>") >= 0 Then
                Chk_Sabato.Checked = True
            End If
            If GiorniSettimana.IndexOf("<7>") >= 0 Then
                Chk_Domenica.Checked = True
            End If

            If DD_Regola.SelectedValue = "2" Then
                If GiorniSettimana.IndexOf("<INTERVALLO") >= 0 Then
                    Dim NumeroIntervallo As String

                    NumeroIntervallo = GiorniSettimana.Replace("<INTERVALLO=", "").Replace(">", "")

                    Txt_Intervallo.Text = NumeroIntervallo
                End If

            End If


            If DD_Regola.SelectedValue = "3" Then
                If GiorniSettimana.IndexOf("<MESE") >= 0 Then
                    Dim Mese As String

                    Mese = GiorniSettimana.Replace("<MESE=", "").Replace(">", "")
                    Try
                        Txt_Intervallo.Text = Mese
                    Catch ex As Exception

                    End Try                    
                End If

            End If




            Dim DD_Mansione As DropDownList = DirectCast(e.Row.FindControl("DD_Mansione"), DropDownList)
            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione)
            DD_Mansione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(4).ToString

            Dim DD_Appalto As DropDownList = DirectCast(e.Row.FindControl("DD_Appalto"), DropDownList)
            Dim Appalto As New Cls_AppaltiAnagrafica


            Appalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)
            DD_Appalto.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(5).ToString

            Dim DD_Struttura As DropDownList = DirectCast(e.Row.FindControl("DD_Struttura"), DropDownList)
            Dim Struttura As New Cls_AppaltiTabellaStruttura


            Struttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)
            DD_Struttura.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim DD_Utente As DropDownList = DirectCast(e.Row.FindControl("DD_Utente"), DropDownList)
            Dim Utenti As New ClsOspite



            Utenti.UpDateDropBox(Session("DC_OSPITE"), DD_Utente)



            DD_Utente.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(7).ToString

            Dim Txt_Ore As TextBox = DirectCast(e.Row.FindControl("Txt_Ore"), TextBox)

            Txt_Ore.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(8).ToString), "#,##0.00")



        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Regole", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Numero", GetType(String))
        MyTable.Columns.Add("Giorni", GetType(String))
        MyTable.Columns.Add("Mansione", GetType(String))
        MyTable.Columns.Add("Appalto", GetType(String))
        MyTable.Columns.Add("Struttura", GetType(String))
        MyTable.Columns.Add("Utente", GetType(String))
        MyTable.Columns.Add("Ore", GetType(String))

        'intypecod


        For i = 0 To Grd_Cella.Rows.Count - 1

            Dim DD_Regola As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Regola"), DropDownList)
            Dim Txt_Data As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Data"), TextBox)

            Dim Txt_Numero As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Numero"), TextBox)
            Dim Txt_Intervallo As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Intervallo"), TextBox)

            Dim Chk_Lunedi As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Lunedi"), CheckBox)
            Dim Chk_Martedi As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Martedi"), CheckBox)
            Dim Chk_Mercoledi As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Mercoledi"), CheckBox)
            Dim Chk_Giovedi As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Giovedi"), CheckBox)
            Dim Chk_Venerdì As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Venerdì"), CheckBox)
            Dim Chk_Sabato As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Sabato"), CheckBox)
            Dim Chk_Domenica As CheckBox = DirectCast(Grd_Cella.Rows(i).FindControl("Chk_Domenica"), CheckBox)


            Dim DD_Mansione As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Mansione"), DropDownList)
            Dim DD_Appalto As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Appalto"), DropDownList)
            Dim DD_Struttura As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Struttura"), DropDownList)
            Dim DD_Utente As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Utente"), DropDownList)
            Dim Txt_Ore As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Ore"), TextBox)



            Dim Vettore(100) As String



            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = DD_Regola.SelectedValue

            If Txt_Data.Text = "" Then
                myrigaR(1) = ""
            Else
                myrigaR(1) = Txt_Data.Text
            End If
            myrigaR(2) = Txt_Numero.Text

            Dim Appoggio As String = ""
            If Chk_Lunedi.Checked = True Then
                Appoggio = Appoggio & "<1>"
            End If
            If Chk_Martedi.Checked = True Then
                Appoggio = Appoggio & "<2>"
            End If
            If Chk_Mercoledi.Checked = True Then
                Appoggio = Appoggio & "<3>"
            End If
            If Chk_Giovedi.Checked = True Then
                Appoggio = Appoggio & "<4>"
            End If
            If Chk_Venerdì.Checked = True Then
                Appoggio = Appoggio & "<5>"
            End If
            If Chk_Sabato.Checked = True Then
                Appoggio = Appoggio & "<6>"
            End If
            If Chk_Domenica.Checked = True Then
                Appoggio = Appoggio & "<7>"
            End If
            If DD_Regola.SelectedValue = 2 Then
                Appoggio = Appoggio & "<INTERVALLO=" & Txt_Intervallo.Text & ">"
            End If
            If DD_Regola.SelectedValue = 3 Then
                Appoggio = Appoggio & "<MESE=" & Txt_Intervallo.Text & ">"
            End If


            myrigaR(3) = Appoggio

            myrigaR(4) = DD_Mansione.SelectedValue
            myrigaR(5) = DD_Appalto.SelectedValue
            myrigaR(6) = DD_Struttura.SelectedValue

            myrigaR(7) = Val(DD_Utente.SelectedValue)

            myrigaR(8) = Txt_Ore.Text


            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub DD_Appalto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RowInd As GridViewRow = ddl.Parent.Parent
        Dim IndiceRiga As Integer

        IndiceRiga = RowInd.RowIndex

        Dim DD_Struttura As DropDownList = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("DD_Struttura"), DropDownList)


        Dim Struttura As New Cls_AppaltiTabellaStruttura


        Struttura.UpDateDropBoxAppalto(Session("DC_TABELLE"), DD_Struttura, ddl.SelectedValue)


    End Sub

    Protected Sub DD_Giorno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RowInd As GridViewRow = ddl.Parent.Parent
        Dim IndiceRiga As Integer

        IndiceRiga = RowInd.RowIndex


        Dim Txt_Data As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_Data"), TextBox)


        Dim Txt_Numero As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_Numero"), TextBox)

        Dim Txt_Intervallo As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_Intervallo"), TextBox)


        Dim Chk_Lunedi As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Lunedi"), CheckBox)
        Dim Chk_Martedi As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Martedi"), CheckBox)
        Dim Chk_Mercoledi As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Mercoledi"), CheckBox)
        Dim Chk_Giovedi As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Giovedi"), CheckBox)
        Dim Chk_Venerdì As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Venerdì"), CheckBox)
        Dim Chk_Sabato As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Sabato"), CheckBox)
        Dim Chk_Domenica As CheckBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Chk_Domenica"), CheckBox)



        If ddl.SelectedValue = "0" Then            
            Txt_Intervallo.Visible = False
            Exit Sub
        End If

        If ddl.SelectedValue = "4" Then
            Txt_Data.Visible = True
            Txt_Numero.Visible = False

            Chk_Lunedi.Visible = False
            Chk_Domenica.Visible = False
            Chk_Giovedi.Visible = False
            Chk_Martedi.Visible = False
            Chk_Mercoledi.Visible = False
            Chk_Sabato.Visible = False
            Chk_Venerdì.Visible = False
            Txt_Intervallo.Visible = False



            Exit Sub
        End If


        If ddl.SelectedValue = "3" Then
            Txt_Data.Visible = True
            Txt_Numero.Visible = True

            Chk_Lunedi.Visible = False
            Chk_Domenica.Visible = False
            Chk_Giovedi.Visible = False
            Chk_Martedi.Visible = False
            Chk_Mercoledi.Visible = False
            Chk_Sabato.Visible = False
            Chk_Venerdì.Visible = False
            Txt_Intervallo.Visible = False



            Exit Sub
        End If




        If ddl.SelectedValue = "2" Then
            Txt_Data.Visible = True
            Txt_Numero.Visible = True

            Chk_Lunedi.Visible = False
            Chk_Domenica.Visible = False
            Chk_Giovedi.Visible = False
            Chk_Martedi.Visible = False
            Chk_Mercoledi.Visible = False
            Chk_Sabato.Visible = False
            Chk_Venerdì.Visible = False
            Txt_Intervallo.Visible = True


            Exit Sub
        End If

        If ddl.SelectedValue = "1" Then

            Txt_Data.Visible = True
            Txt_Numero.Visible = True
            Txt_Intervallo.Visible = False


        Else

            Txt_Data.Visible = False
            Txt_Numero.Visible = False
            Txt_Intervallo.Visible = False


        End If
    End Sub
End Class
