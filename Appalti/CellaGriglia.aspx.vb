﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_CellaGriglia
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        myriga(1) = 0
        myriga(2) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable

        Grd_Cella.DataBind()




        Call EseguiJS()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Grd_Cella_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Cella.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grd_Cella_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Cella.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DD_Mansione As DropDownList = DirectCast(e.Row.FindControl("DD_Mansione"), DropDownList)
            Dim Mansione As New Cls_AppaltiTabellaMansione

            
            Mansione.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione)
            DD_Mansione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(0).ToString


            Dim DD_Utente As DropDownList = DirectCast(e.Row.FindControl("DD_Utente"), DropDownList)
            Dim Utenti As New ClsOspite


            Utenti.UpDateDropBox(Session("DC_OSPITE"), DD_Utente)
            DD_Utente.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(1).ToString

            Dim Txt_Ore As TextBox = DirectCast(e.Row.FindControl("Txt_Ore"), TextBox)

            Txt_Ore.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(2).ToString), "#,##0.00")


            Dim Txt_intypecod As TextBox = DirectCast(e.Row.FindControl("Txt_intypecod"), TextBox)
            Dim Txt_intypedescription As TextBox = DirectCast(e.Row.FindControl("Txt_intypedescription"), TextBox)

            Txt_intypecod.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString

            Txt_intypedescription.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString

        End If
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Mansione", GetType(String))
        MyTable.Columns.Add("Utente", GetType(String))
        MyTable.Columns.Add("Ore", GetType(String))

        MyTable.Columns.Add("intypecod", GetType(String))
        MyTable.Columns.Add("intypedescription", GetType(String))

        'intypecod
        

        For i = 0 To Grd_Cella.Rows.Count - 1

            Dim DD_Mansione As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Mansione"), DropDownList)
            Dim DD_Utente As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Utente"), DropDownList)
            Dim Txt_Ore As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Ore"), TextBox)

            Dim Txt_intypecod As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_intypecod"), TextBox)
            Dim Txt_intypedescription As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_intypedescription"), TextBox)


            Dim Vettore(100) As String



            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = DD_Mansione.SelectedValue

            myrigaR(1) = DD_Utente.SelectedValue

            myrigaR(2) = Txt_Ore.Text

            myrigaR(3) = Txt_intypecod.Text
            myrigaR(4) = Txt_intypedescription.Text



            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        Giorno = Request.Item("Giorno")
        CodiceOperatore = Request.Item("CodiceOperatore")

        Anno = Request.Item("Anno")


        Mese = Request.Item("Mese")

        Appalto = Request.Item("Appalto")
        Struttura = Request.Item("Struttura")


        Dim Operatore As New Cls_Operatore

        Operatore.CodiceMedico = CodiceOperatore
        Operatore.Leggi(Session("DC_OSPITE"))

        lblDatiCella.Text = "Operatore : " & Operatore.Nome & " Giorno : " & Giorno


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Mansione", GetType(String))
        MyTable.Columns.Add("Utente", GetType(String))
        MyTable.Columns.Add("Ore", GetType(String))
        MyTable.Columns.Add("intypecod", GetType(String))
        MyTable.Columns.Add("intypedescription", GetType(String))

        Dim FlagInseritoRiga As Boolean = False

        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "Select * From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  Data = ? And CodiceOperatore = ?"

        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)



        Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
        Do While MovimentiGiorno.Read
            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = campodb(MovimentiGiorno.Item("IdMansione"))

            myrigaR(1) = campodb(MovimentiGiorno.Item("CodiceOspite"))

            myrigaR(2) = campodbn(MovimentiGiorno.Item("Ore"))


            myrigaR(3) = campodb(MovimentiGiorno.Item("intypecod"))
            myrigaR(4) = campodb(MovimentiGiorno.Item("intypedescription"))


            MyTable.Rows.Add(myrigaR)
            FlagInseritoRiga = True
        Loop

        If Not FlagInseritoRiga Then
            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = 0

            myrigaR(1) = 0

            myrigaR(2) = 0



            MyTable.Rows.Add(myrigaR)
        End If

        MovimentiGiorno.Close()
        cn.Close()


        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        UpDateTable()

        MyTable = ViewState("App_Retta")

        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        Giorno = Request.Item("Giorno")
        CodiceOperatore = Request.Item("CodiceOperatore")

        Anno = Request.Item("Anno")


        Mese = Request.Item("Mese")

        Appalto = Request.Item("Appalto")
        Struttura = Request.Item("Struttura")



        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "DELETE From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  Data = ? And CodiceOperatore = ?"
        CmdP.Transaction = Transan
        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)

        CmdP.ExecuteNonQuery()




        For i = 0 To Grd_Cella.Rows.Count - 1

            Dim DD_Mansione As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Mansione"), DropDownList)
            Dim DD_Utente As DropDownList = DirectCast(Grd_Cella.Rows(i).FindControl("DD_Utente"), DropDownList)
            Dim Txt_Ore As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_Ore"), TextBox)

            Dim Txt_intypecod As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_intypecod"), TextBox)
            Dim Txt_intypedescription As TextBox = DirectCast(Grd_Cella.Rows(i).FindControl("Txt_intypedescription"), TextBox)


            Dim CmdIns As New OleDbCommand

            CmdIns.Connection = cn

            CmdIns.CommandText = "INSERT INTO Appalti_Prestazioni (IdAppalto,IdStrutture,Data,CodiceOperatore,IdMansione,Ore,CodiceOspite,intypecod,intypedescription) VALUES (?,?,?,?,?,?,?,?,?)"

            CmdIns.Transaction = Transan
            CmdIns.Parameters.AddWithValue("@Appalto", Appalto)
            CmdIns.Parameters.AddWithValue("@Struttura", Struttura)
            CmdIns.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
            CmdIns.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
            CmdIns.Parameters.AddWithValue("@IdMansione", Val(DD_Mansione.SelectedValue))
            CmdIns.Parameters.AddWithValue("@Ore", CDbl(Txt_Ore.Text))
            CmdIns.Parameters.AddWithValue("@CodiceOspite", Val(DD_Utente.SelectedValue))

            CmdIns.Parameters.AddWithValue("@intypecod", Txt_intypecod.Text)
            CmdIns.Parameters.AddWithValue("@intypedescription", Txt_intypedescription.Text)

            CmdIns.ExecuteNonQuery()


        Next


        Transan.Commit()
        cn.Close()

        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Dati Modificati');", True)
        Exit Sub
    End Sub

    Protected Sub Grd_Cella_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Cella.RowDeleted

    End Sub

    Protected Sub Grd_Cella_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Cella.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = 0
                        myriga(1) = 0
                        myriga(2) = 0
                        myriga(3) = ""
                        myriga(4) = ""
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = 0
                    myriga(1) = 0
                    myriga(2) = 0
                    myriga(3) = ""
                    myriga(4) = ""
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Cella.AutoGenerateColumns = False

        Grd_Cella.DataSource = MyTable
        Grd_Cella.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub


    Protected Sub DD_Utentee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RowInd As GridViewRow = ddl.Parent.Parent
        Dim IndiceRiga As Integer

        IndiceRiga = RowInd.RowIndex
        
        Dim Txt_intypecod As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_intypecod"), TextBox)
        Dim Txt_intypedescription As TextBox = DirectCast(Grd_Cella.Rows(IndiceRiga).FindControl("Txt_intypedescription"), TextBox)

        Dim TipoOspite As New ClsOspite

        TipoOspite.CodiceOspite = Val(ddl.SelectedValue)
        TipoOspite.Leggi(Session("DC_OSPITE"), TipoOspite.CodiceOspite)

        If Not IsNothing(TipoOspite.TipoOspite) Then
            Dim TipoUtente As New Cls_Appalti_TipoUtente

            TipoUtente.intypecod = TipoOspite.TipoOspite
            TipoUtente.Leggi(Session("DC_TABELLE"), TipoUtente.intypecod)

            Txt_intypecod.Text = TipoUtente.intypecod
            Txt_intypedescription.Text = TipoUtente.intypedescription

        End If

    End Sub
End Class
