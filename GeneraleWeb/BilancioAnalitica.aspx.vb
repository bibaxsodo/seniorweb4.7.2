﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting

Partial Class GeneraleWeb_BilancioAnalitica
    Inherits System.Web.UI.Page


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub CreaBilancio(ByVal Anno As Integer, ByVal DataDal As Date, ByVal DataAl As Date, ByVal MeseDal As Integer, ByVal MeseAl As Integer, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim Bilancio As New StampeGenerale


        Dim DatiSocieta As New Cls_DecodificaSocieta
        Dim WSocieta As String = ""
        Dim VettoreColonne(100) As String
        Dim VettoreNumero(100) As Long
        Dim VettoreValore(100) As Double


        WSocieta = DatiSocieta.DecodificaSocieta(ConnessioneTabelle)

        Dim cnGenerale As OleDbConnection
        

        cnGenerale = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cnGenerale.Open()

        Dim MySql As String
        Dim Colonne As Integer = 0


        VettoreColonne(Colonne) = "Non Assegnato"
        VettoreNumero(Colonne) = 0

        Colonne = Colonne + 1

        MySql = "Select * from ColonneBudget Where Anno = ? Order by Livello1"

        Dim cmdColonneBudget As New OleDbCommand()
        cmdColonneBudget.CommandText = MySql
        cmdColonneBudget.Connection = cnGenerale
        cmdColonneBudget.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        Dim RdColonneBudget As OleDbDataReader = cmdColonneBudget.ExecuteReader()
        Do While RdColonneBudget.Read
            VettoreColonne(Colonne) = campodb(RdColonneBudget.Item("Descrizione"))
            VettoreNumero(Colonne) = campodbN(RdColonneBudget.Item("Livello1"))

            Colonne = Colonne + 1
        Loop
        RdColonneBudget.Close()





        MySql = "Select * from TipoBudget Where Anno = ? Order by Livello1,Livello2,Livello3"

        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = MySql
        cmd2.Connection = cnGenerale
        cmd2.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        Dim RdMastro2 As OleDbDataReader = cmd2.ExecuteReader()
        Do While RdMastro2.Read

            Dim Somma As Double = 0
            Dim ValoreColonne(100) As Double

            Dim Importob As Double = 0


            Dim Livello1 As Integer = campodbN(RdMastro2.Item("Livello1"))
            Dim Livello2 As Integer = campodbN(RdMastro2.Item("Livello2"))
            Dim Livello3 As Integer = campodbN(RdMastro2.Item("Livello3"))
            Dim AppoSql As String = ""
            Dim x As Integer

            For x = 0 To 100
                VettoreValore(x) = 0
            Next


            For x = 0 To Colonne - 1





                If Chk_ConsideraCompetenza.Checked = True Then
                    AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?  " & _
                              " And (AnnoCompetenza = ? And ((MeseCompetenza >= ? And MeseCompetenza <= ?)  or MeseCompetenza = 0 )) "
                Else
                    AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?  "
                End If

                Dim CommandSnRegR As New OleDbCommand(AppoSql, cnGenerale)

                CommandSnRegR.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@Anno").Value = Txt_Anno.Text

                CommandSnRegR.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@LIVELLO1").Value = Livello1
                CommandSnRegR.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@LIVELLO2").Value = Livello2
                CommandSnRegR.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@LIVELLO3").Value = Livello3

                CommandSnRegR.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@COLONNA").Value = x

                If Chk_ConsideraCompetenza.Checked = True Then
                    CommandSnRegR.Parameters.AddWithValue("@AnnoCompetenza", Anno)
                    CommandSnRegR.Parameters.AddWithValue("@MeseCompetenza", MeseDal)
                    CommandSnRegR.Parameters.AddWithValue("@MeseCompetenza", MeseAl)
                End If

                Dim mySnRegR As OleDbDataReader = CommandSnRegR.ExecuteReader()
                If mySnRegR.Read Then
                    If Not IsDBNull(mySnRegR.Item(0)) Then
                        Somma = Somma + CDbl(mySnRegR.Item(0))
                    End If
                End If
                mySnRegR.Close()
                CommandSnRegR.Dispose()


                VettoreValore(x) = Format(Somma, "#,##0.00")
            Next



            Dim k As System.Data.DataRow = Bilancio.Tables("BilancioAnalitica").NewRow
            k.Item("Anno") = Anno
            k.Item("Livello1") = Livello1
            k.Item("Livello2") = Livello2
            k.Item("Livello3") = Livello3
            k.Item("Tipo") = campodb(RdMastro2.Item("Tipo"))

            k.Item("Descrizione") = campodb(RdMastro2.Item("Descrizione"))
            If Livello3 > 0 Then
                k.Item("Descrizione") = "   " & campodb(RdMastro2.Item("Descrizione"))
            End If

            k.Item("Colonna1Descrizione") = VettoreColonne(0)
            k.Item("Colonna1") = VettoreValore(0)

            k.Item("Colonna2Descrizione") = VettoreColonne(1)
            k.Item("Colonna2") = VettoreValore(1)

            k.Item("Colonna3Descrizione") = VettoreColonne(2)
            k.Item("Colonna3") = VettoreValore(2)

            k.Item("Colonna4Descrizione") = VettoreColonne(3)
            k.Item("Colonna4") = VettoreValore(3)

            k.Item("Colonna5Descrizione") = VettoreColonne(4)
            k.Item("Colonna5") = VettoreValore(4)

            k.Item("Colonna6Descrizione") = VettoreColonne(5)
            k.Item("Colonna6") = VettoreValore(5)


            k.Item("Colonna7Descrizione") = VettoreColonne(6)
            k.Item("Colonna7") = VettoreValore(6)


            k.Item("Colonna8Descrizione") = VettoreColonne(7)
            k.Item("Colonna8") = VettoreValore(7)




            Bilancio.Tables("BilancioAnalitica").Rows.Add(k)



        Loop
        RdMastro2.Close()
        cmd2.Dispose()




        cnGenerale.Close()


        Session("stampa") = Bilancio

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=BILANCIOANALITICA&PRINTERKEY=OFF&XSD=SI','Stampe','width=800,height=600');", True)


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJS As String
        MyJS = "$(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJS = MyJS & " $(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJS, True)



        MyJS = "$(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJS = MyJS & " $(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataAl", MyJS, True)




        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)

        Txt_DataDal.Text = Format(DateSerial(Year(Now), 1, 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")

        DD_MeseDal.SelectedValue = 1
        DD_MeseAl.SelectedValue = 12

    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        CreaBilancio(Txt_Anno.Text, Txt_DataDal.Text, Txt_DataAl.Text, DD_MeseDal.SelectedValue, DD_MeseAl.SelectedValue, Session("DC_TABELLE"), Session("DC_GENERALE"))
    End Sub
End Class
