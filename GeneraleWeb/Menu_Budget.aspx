﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Budget" CodeFile="Menu_Budget.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Generale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Contabilità</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Lbl_BudgetAnalitica" runat="server" Text=""></asp:Label><br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        &nbsp;</td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%; vertical-align: top;">
                            <tr>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><a href="VisualizzaBudget.aspx">
                                    <img src="../images/Menu_VisualizzaBudget.png" class="Effetto" alt="Visualizza Budget/Analitica" style="border-width: 0;"></a></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><a href="RicercaRegistrazioniBudget.aspx">
                                    <img src="../images/Menu_VisualizzaBudget.png" class="Effetto" alt="Ricerca Registazioni" style="border-width: 0;"></a></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><a href="BilancioAnalitica.aspx">
                                    <img src="../images/Menu_VisualizzaBudget.png" class="Effetto" alt="Bilancio Analitica" style="border-width: 0;"></a></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top; width: 20%;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VISUALIZZA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RICERCA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">BILANCIO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>
                            <tr>
                                <td style="text-align: center;"><a href="ElencoContiBudget.aspx">
                                    <img style="border-width: 0;" src="../images/Menu_TabellaContiBudget.png" class="Effetto" alt="Conti" /></a></td>
                                <td style="text-align: center;"><a href="ElencoColonneBudget.aspx">
                                    <img style="border-width: 0;" src="../images/Menu_ColonneBudget.png" class="Effetto" alt="Colonne" /></a></td>
                                <td style="text-align: center;"><a href="Elenco_RegoleBudget.aspx">
                                    <img style="border-width: 0;" src="../images/Menu_ColonneBudget.png" class="Effetto" alt="Regole" /></a></td>
                                <td style="text-align: center;"><a href="AllineaPDCBudget.aspx">
                                    <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Allinea PDC - Analitica" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">COLONNE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">REGOLE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ALLINEA PDC</span></td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>
                            <tr>

                                <td style="text-align: center;"><a href="ControlloRegistrazioniAnalitica.aspx">
                                    <img src="../images/Menu_Strumenti.png" class="Effetto" alt="Controllo Registrazioni Analitica" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="ApplicaRegola.aspx">
                                    <img src="../images/Menu_Strumenti.png" class="Effetto" alt="Genera Regole Analitica su Registrazioni" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTROLLO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">APPLICA REGOLE</span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>
                            <tr id="gurppobudget">

                                <td style="text-align: center;"><a href="RicercaPrenotazione.aspx">
                                    <img src="../images/Menu_RicercaPrenotazione.png" class="Effetto" alt="Ricerca Prenotazione" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="ElencoPrenotazioni.aspx">
                                    <img style="border-width: 0;" src="../images/Menu_InserimentoPrenotazione.png" class="Effetto" alt="Gestione Prenotazione" /></a></td>
                                <td style="text-align: center;">&nbsp;<a href="ElencoVariazioniBudget.aspx"><img src="../images/Menu_DiminuzioneBudget.png" class="Effetto" alt="Variazione Budget" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">&nbsp;<a href="InserimentoBudget.aspx"><img src="../images/Menu_InserimentoBudget.png" class="Effetto" alt="Inserimento Budget" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr id="gurppobudgetlabel">
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RICERCA 
        PREN.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">GEST. 
        PRENOTAZIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VARIAZIONE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">INSERIMENTO</span></td>

                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
