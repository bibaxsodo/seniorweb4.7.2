﻿
Partial Class GeneraleWeb_WizardFattura
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If




        If Page.IsPostBack = True Then Exit Sub

        Dim CodiciIva As New Cls_IVA

        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA1)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA2)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA3)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA4)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA5)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim XCausali As New Cls_CausaleContabile

        XCausali.UpDateDropBoxDoc(Session("DC_TABELLE"), Dd_CausaleContabile)

        Dim x As New Cls_TipoPagamento

        x.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)


        Dim xd As New ClsDetraibilita
        xd.UpDateDropBox(Session("DC_TABELLE"), DD_Detraibilita)

    End Sub

    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        Dim X As New Cls_CausaleContabile

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), X.RegistroIVA)

        Lbl_RegistroIVA.Text = "(" & DecReg.Descrizione & ")"

        DD_Detraibilita.SelectedValue = X.Detraibilita
        DD_IVA1.SelectedValue = X.CodiceIva
    End Sub

    Private Function CreaRighe(ByVal Imposta As Double, ByVal Imponibile As Double, ByVal SottoContoIVA As String, ByVal Segno As String, ByVal Appoggio As Cls_MovimentoContabile, ByVal Indice As Long, ByVal CodiceIVA As String) As Long

        Dim xL As New Cls_CausaleContabile

        xL.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If xL.VenditaAcquisti = "A" Then
            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            Dim Detraibile As Double
            Dim Indetraibile As Double
            Detraibile = Math.Round(dT.Aliquota * Math.Round(Imposta, 2), 2)
            Indetraibile = Imposta - Detraibile

            Dim Prorata As Long
            Dim XP As New Cls_Prorata

            Prorata = 0
            XP.Anno = Val(Txt_AnnoProtocollo.Text) - 1
            XP.Leggi(Session("DC_TABELLE"))
            Prorata = XP.DefinitivoPercentuale

            If dT.Prorata = 1 Then
                Indetraibile = Indetraibile - Math.Round(Indetraibile * Math.Round(Prorata, 2), 2)
            End If

            If dT.Automatico = "S" Then
                If dT.GiroACosto = "S" Then

                    Dim Cau As New Cls_CausaleContabile
                    Dim DareAvere As String

                    Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)


                    Dim Mastro As Long
                    Dim Conto As Long
                    Dim Sottoconto As Long
                    Dim Vettore(100) As String

                    Vettore = SplitWords(SottoContoIVA)

                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))


                    Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga

                    Appoggio.Righe(Indice).MastroPartita = Mastro
                    Appoggio.Righe(Indice).ContoPartita = Conto
                    Appoggio.Righe(Indice).SottocontoPartita = Sottoconto

                    Appoggio.Righe(Indice).Numero = 0
                    Appoggio.Righe(Indice).Segno = "+"

                    Appoggio.Righe(Indice).DareAvere = Segno
                    Appoggio.Righe(Indice).Prorata = 0

                    Appoggio.Righe(Indice).Quantita = 0

                    Appoggio.Righe(Indice).Importo = Indetraibile
                    Appoggio.Righe(Indice).CodiceIVA = CodiceIVA


                    Indice = Indice + 1
                    Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga

                    Appoggio.Righe(Indice).MastroPartita = dT.IVAGiroACostoMastro
                    Appoggio.Righe(Indice).ContoPartita = dT.IVAGiroACostoConto
                    Appoggio.Righe(Indice).SottocontoPartita = dT.IVAGiroACostoSottoconto

                    Appoggio.Righe(Indice).Numero = 0
                    Appoggio.Righe(Indice).Segno = "+"

                    Appoggio.Righe(Indice).DareAvere = Segno
                    Appoggio.Righe(Indice).Prorata = 0

                    Appoggio.Righe(Indice).Quantita = 0
                    Appoggio.Righe(Indice).Descrizione = "Giroconto per IVA Indetraibile"

                    Appoggio.Righe(Indice).Importo = 0
                    Appoggio.Righe(Indice).CodiceIVA = CodiceIVA

                    Indice = Indice + 1
                    Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga


                    Appoggio.Righe(Indice).DareAvere = Segno
                    Appoggio.Righe(Indice).MastroPartita = dT.IVAGiroACostoMastro
                    Appoggio.Righe(Indice).ContoPartita = dT.IVAGiroACostoConto
                    Appoggio.Righe(Indice).SottocontoPartita = dT.IVAGiroACostoSottoconto
                    Appoggio.Righe(Indice).Descrizione = "Giroconto a IVA Indetraibile"




                    Indice = Indice + 1
                    Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga



                    Appoggio.Righe(Indice).DareAvere = Segno
                    Appoggio.Righe(Indice).MastroPartita = dT.IVAGiroACostoMastro
                    Appoggio.Righe(Indice).ContoPartita = dT.IVAGiroACostoConto
                    Appoggio.Righe(Indice).SottocontoPartita = dT.IVAGiroACostoSottoconto

                    Appoggio.Righe(Indice).Descrizione = "Giroconto a Costi"

                    Indice = Indice + 1

                Else

                    Dim Mastro As Long
                    Dim Conto As Long
                    Dim Sottoconto As Long
                    Dim Vettore(100) As String

                    Vettore = SplitWords(SottoContoIVA)

                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))


                    Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga

                    Appoggio.Righe(Indice).Segno = "+"
                    If Segno = "D" Then
                        Appoggio.Righe(Indice).DareAvere = "A"
                    Else
                        Appoggio.Righe(Indice).DareAvere = "D"
                    End If
                    Appoggio.Righe(Indice).MastroPartita = Mastro
                    Appoggio.Righe(Indice).ContoPartita = Conto
                    Appoggio.Righe(Indice).SottocontoPartita = Sottoconto
                    Appoggio.Righe(Indice).Importo = Indetraibile


                    Appoggio.Righe(Indice).Descrizione = "Giroconto dell'IVA a costi"
                    Indice = Indice + 1
                    End If
            Else
                    If dT.GiroACosto = "S" Then

                        Dim Mastro As Long
                        Dim Conto As Long
                        Dim Sottoconto As Long
                        Dim Vettore(100) As String

                        Vettore = SplitWords(SottoContoIVA)

                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))

                        Appoggio.Righe(Indice) = New Cls_MovimentiContabiliRiga
                    Appoggio.Righe(Indice).Segno = "+"
                        Appoggio.Righe(Indice).DareAvere = Segno
                        Appoggio.Righe(Indice).MastroPartita = Mastro
                        Appoggio.Righe(Indice).ContoPartita = Conto
                        Appoggio.Righe(Indice).SottocontoPartita = Sottoconto

                        Appoggio.Righe(Indice).Descrizione = "Giroconto dell'IVA a costi"
                        Indice = Indice + 1
                    End If
            End If
        End If

        Return Indice
    End Function


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Trim(Dd_CausaleContabile.SelectedValue) = "" Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica causale contabile');", True)
            REM Lbl_errori.Text = "Specifica causale contabile"
            Exit Sub
        End If

        If Val(Txt_AnnoProtocollo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica anno protocollo');", True)
            REM Lbl_errori.Text = "Specifica anno protocollo"
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Exit Sub
        End If

        If Not IsDate(Txt_DataDocumento.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Documento formalmente errata');", True)
            REM Lbl_errori.Text = "Data Documento formalmente errata"
            Exit Sub
        End If

        If Trim(Txt_ClienteFornitore.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente / Fornitore non specificato');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Exit Sub
        End If

        If DD_Detraibilita.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica detraibilità');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Exit Sub
        End If


        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If


        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length < 3 Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If
        If Val(Vettore(2)) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If

        Dim TotaleDocumento As Double

        If Txt_Imponibile1.Text <> "" Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)

            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            If dT.Automatico <> "S" Then
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile1.Text) + Math.Round(CDbl(Txt_Imponibile1.Text) * Xs.Aliquota, 2)
            Else
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile1.Text)
            End If
        Else
            Txt_Imponibile1.Text = 0
        End If
        If Txt_Imponibile2.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)

            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            If dT.Automatico = "S" Then
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile2.Text) + Math.Round(CDbl(Txt_Imponibile2.Text) * Xs.Aliquota, 2)
            Else
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile2.Text)
            End If
        Else
            Txt_Imponibile2.Text = 0
        End If
        If Txt_Imponibile3.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)

            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            If dT.Automatico = "S" Then
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile3.Text) + Math.Round(CDbl(Txt_Imponibile3.Text) * Xs.Aliquota, 2)
            Else
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile3.Text)
            End If
        Else
            Txt_Imponibile3.Text = 0
        End If
        If Txt_Imponibile4.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)

            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            If dT.Automatico = "S" Then
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile4.Text) + Math.Round(CDbl(Txt_Imponibile4.Text) * Xs.Aliquota, 2)
            Else
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile4.Text)
            End If
        Else
            Txt_Imponibile4.Text = 0
        End If
        If Txt_Imponibile5.Text <> "" Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)

            Dim dT As New ClsDetraibilita

            dT.Codice = DD_Detraibilita.SelectedValue
            dT.Leggi(Session("DC_TABELLE"))

            If dT.Automatico = "S" Then
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile5.Text) + Math.Round(CDbl(Txt_Imponibile5.Text) * Xs.Aliquota, 2)
            Else
                TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile5.Text)
            End If
        Else
            Txt_Imponibile5.Text = 0
        End If

        If TotaleDocumento = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un importo');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Exit Sub
        End If

        Dim x As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile
        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, 0)

        x.NumeroRegistrazione = 0
        x.DataRegistrazione = Txt_DataRegistrazione.Text
        x.CausaleContabile = Dd_CausaleContabile.SelectedValue

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        x.Descrizione = Txt_Descrizione.Text
        x.DataDocumento = Txt_DataDocumento.Text
        x.NumeroDocumento = Txt_NumeroDocumento.Text
        x.RegistroIVA = MyCau.RegistroIVA

        x.CausaleContabile = Dd_CausaleContabile.SelectedValue

        x.AnnoProtocollo = Val(Txt_AnnoProtocollo.Text)
        x.NumeroProtocollo = Val(Txt_NumeroProtocollo.Text)

        x.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue


        x.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue
        x.IVASospesa = "N"

        x.Righe(0) = New Cls_MovimentiContabiliRiga
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        x.Righe(0).MastroPartita = Mastro
        x.Righe(0).ContoPartita = Conto
        x.Righe(0).SottocontoPartita = Sottoconto

        x.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        x.Righe(0).Segno = "+"
        x.Righe(0).Importo = TotaleDocumento
        x.Righe(0).Tipo = "CF"

        x.Righe(0).Descrizione = ""
        x.Righe(0).RigaDaCausale = 1

        x.Righe(0).MastroContropartita = 0
        x.Righe(0).ContoContropartita = 0
        x.Righe(0).SottocontoContropartita = 0
        x.Righe(0).Numero = 0

        Dim Indice As Integer = 1


        If CDbl(Txt_Imponibile1.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile1.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile1.Text
            x.Righe(Indice).CodiceIVA = DD_IVA1.SelectedValue
            x.Righe(Indice).Importo = Math.Round(Imponibile * Xs.Aliquota, 2)

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            Indice = CreaRighe(Math.Round(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If

        If CDbl(Txt_Imponibile2.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile2.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile2.Text
            x.Righe(Indice).CodiceIVA = DD_IVA2.SelectedValue
            x.Righe(Indice).Importo = Math.Round(Imponibile * Xs.Aliquota, 2)

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
        End If

        If CDbl(Txt_Imponibile3.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile3.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile3.Text
            x.Righe(Indice).CodiceIVA = DD_IVA3.SelectedValue
            x.Righe(Indice).Importo = Math.Round(Imponibile * Xs.Aliquota, 2)

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
        End If


        If CDbl(Txt_Imponibile4.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile4.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile4.Text
            x.Righe(Indice).CodiceIVA = DD_IVA4.SelectedValue
            x.Righe(Indice).Importo = Math.Round(Imponibile * Xs.Aliquota, 2)

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
        End If



        If CDbl(Txt_Imponibile5.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile5.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile5.Text
            x.Righe(Indice).CodiceIVA = DD_IVA5.SelectedValue
            x.Righe(Indice).Importo = Math.Round(Imponibile * Xs.Aliquota, 2)

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
        End If


        If Txt_Conto1.Text <> "" And CDbl(Txt_Imponibile1.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
                x.Righe(Indice).RigaDaCausale = 2
            Else
                DareAvere = Cau.Righe(2).DareAvere
                x.Righe(Indice).RigaDaCausale = 3
            End If



            Vettore = SplitWords(Txt_Conto1.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0


            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)


            x.Righe(Indice).Importo = Txt_Imponibile1.Text ' + Math.Round(CDbl(Txt_Imponibile1.Text) * Xs.Aliquota, 2)
            x.Righe(Indice).CodiceIVA = DD_IVA1.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3                
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto2.Text <> "" And CDbl(Txt_Imponibile2.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto2.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)


            x.Righe(Indice).Importo = Txt_Imponibile2.Text + Math.Round(CDbl(Txt_Imponibile2.Text) * Xs.Aliquota, 2)
            x.Righe(Indice).CodiceIVA = DD_IVA2.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto3.Text <> "" And CDbl(Txt_Imponibile3.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto3.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)


            x.Righe(Indice).Importo = Txt_Imponibile3.Text + Math.Round(CDbl(Txt_Imponibile3.Text) * Xs.Aliquota, 2)
            x.Righe(Indice).CodiceIVA = DD_IVA3.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If



        If Txt_Conto4.Text <> "" And CDbl(Txt_Imponibile4.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto4.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)


            x.Righe(Indice).Importo = Txt_Imponibile4.Text + Math.Round(CDbl(Txt_Imponibile4.Text) * Xs.Aliquota, 2)
            x.Righe(Indice).CodiceIVA = DD_IVA4.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto5.Text <> "" And CDbl(Txt_Imponibile5.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto5.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)


            x.Righe(Indice).Importo = Txt_Imponibile5.Text + Math.Round(CDbl(Txt_Imponibile5.Text) * Xs.Aliquota, 2)
            x.Righe(Indice).CodiceIVA = DD_IVA5.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If

        x.Scrivi(Session("DC_GENERALE"), 0)



        Dim xModalita As New Cls_TipoPagamento

        xModalita.Codice = x.ModalitaPagamento
        xModalita.Leggi(Session("DC_TABELLE"))



        Dim XS2 As New Cls_Scadenziario
        Dim DataScadenza As New Date

        XS2.NumeroRegistrazioneContabile = x.NumeroRegistrazione

        DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
        If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
            DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
        End If
        If xModalita.Tipo = "F" Then
            If Month(x.DataDocumento) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
            Else
                DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
            End If
        End If
        XS2.DataScadenza = DataScadenza
        Dim ImportoNetto As Double = TotaleDocumento

        If xModalita.Scadenze > 0 Then
            XS2.Importo = Math.Round(ImportoNetto / xModalita.Scadenze, 2)
            XS2.Chiusa = 0
            XS2.Descrizione = ""
            XS2.ScriviScadenza(Session("DC_GENERALE"))
        End If

        If xModalita.Scadenze > 1 Then
            Dim XS1 As New Cls_Scadenziario
            XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
            XS1.Importo = Math.Round(ImportoNetto / xModalita.Scadenze, 2)

            DataScadenza = DateAdd("d", xModalita.GiorniSeconda + xModalita.GiorniPrima, x.DataDocumento)
            If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
            XS1.DataScadenza = DataScadenza

            XS1.Chiusa = 0
            XS1.Descrizione = ""
            XS1.ScriviScadenza(Session("DC_GENERALE"))
        End If

        If xModalita.Scadenze > 2 Then
            Dim GIORNIDA As Long
            GIORNIDA = xModalita.GiorniSeconda + xModalita.GiorniPrima
            For i = 3 To xModalita.Scadenze
                GIORNIDA = GIORNIDA + xModalita.GiorniAltre
                Dim XS1 As New Cls_Scadenziario
                XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                DataScadenza = DateAdd("d", GIORNIDA, x.DataDocumento)
                If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                XS1.DataScadenza = DataScadenza
                XS1.Importo = Math.Round(ImportoNetto / xModalita.Scadenze, 2)
                XS1.Chiusa = 0
                XS1.Descrizione = ""
                XS1.ScriviScadenza(Session("DC_GENERALE"))
            Next i
        End If


        Dd_CausaleContabile.SelectedValue = ""
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDocumento.Text = ""
        Txt_NumeroDocumento.Text = ""
        Txt_ClienteFornitore.Text = ""
        Txt_AnnoProtocollo.Text = Year(Now)
        Txt_NumeroProtocollo.Text = ""
        Txt_Descrizione.text = ""
        Txt_Conto1.Text = ""
        Txt_Conto2.Text = ""
        Txt_Conto3.Text = ""
        Txt_Conto4.Text = ""
        Txt_Conto5.Text = ""

        Txt_Imponibile1.Text = 0
        Txt_Imponibile2.Text = 0
        Txt_Imponibile3.Text = 0
        Txt_Imponibile4.Text = 0
        Txt_Imponibile5.Text = 0

        DD_IVA1.SelectedValue = ""
        DD_IVA2.SelectedValue = ""
        DD_IVA3.SelectedValue = ""
        DD_IVA4.SelectedValue = ""
        DD_IVA5.SelectedValue = ""

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Inserito documento registrazione n. " & x.NumeroRegistrazione & "');", True)
    End Sub



    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Txt_ClienteFornitore_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.Disposed
        Call Txt_ClienteFornitore_TextChanged(sender, e)
    End Sub

    Protected Sub Txt_ClienteFornitore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.TextChanged

        Dim k As New Cls_ClienteFornitore
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Appoggio1 As String = ""
        Dim Appoggio2 As String = ""

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
            k.MastroCliente = Mastro
            k.ContoCliente = Conto
            k.SottoContoCliente = Sottoconto
            k.Leggi(Session("DC_OSPITE"))
            If k.Nome = "" Then
                k.MastroCliente = 0
                k.ContoCliente = 0
                k.SottoContoCliente = 0
                k.MastroFornitore = Mastro
                k.ContoFornitore = Conto
                k.SottoContoFornitore = Sottoconto
                k.Leggi(Session("DC_OSPITE"))

                Dim xS As New Cls_Pianodeiconti

                xS.Mastro = k.MastroCosto
                xS.Conto = k.ContoCosto
                xS.Sottoconto = k.SottocontoCosto
                If k.MastroCosto <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Txt_Conto1.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto2.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto3.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto4.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto5.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                End If
            Else
                Dim xS As New Cls_Pianodeiconti

                xS.Mastro = k.MastroCosto
                xS.Conto = k.ContoCosto
                xS.Sottoconto = k.SottocontoCosto
                If k.MastroCosto <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Txt_Conto1.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto2.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto3.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto4.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto5.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                End If
            End If
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call Txt_ClienteFornitore_TextChanged(sender, e)
    End Sub
End Class

