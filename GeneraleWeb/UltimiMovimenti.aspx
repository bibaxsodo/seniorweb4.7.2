﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_UltimiMovimenti" CodeFile="UltimiMovimenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ultimi Movimenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
            width: 1083px;
            vertical-align: top;
        }
    </style>
    <script type="text/javascript">
        function deletebox(cella) {
            $("div").remove("#RegistrazioneViewer");
            $("#RegistrazioneViewer").remove();
        }
        function apribox(NumeroRegistrazione, cella) {


            $.ajax({
                url: "RegistrazioneViewer.ashx?NumeroRegistrazione=" + NumeroRegistrazione,
                success: function (data, stato) {
                    $("body").append(data);
                    $("#RegistrazioneViewer").css('top', $("#" + cella).position().top + 30);
                    $("#RegistrazioneViewer").css('left', 300);
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }

        $(document).ready(function () {
            if (window.innerHeight > 0) {
                $("#BarraLaterale").css("height", (window.innerHeight - 400) + "px");
            }
            else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 400) + "px"); }


            if (window.innerWidth > 1600) {
                $("#tab1").css("position", "relative");
                $("#tab1").css("float", "left");
                $("#tab1").css("width", "50%");

                $("#tab2").css("position", "relative");
                $("#tab2").css("float", "right");
                $("#tab2").css("width", "50%");

                $("#tab3").css("position", "relative");
                $("#tab3").css("float", "left");
                $("#tab3").css("width", "51%");

                $("#tab4").css("position", "relative");
                $("#tab4").css("float", "right");
                $("#tab4").css("width", "49%");
            }
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() + 1 >= $(document).height()) {
                $("#Img_Espandi").trigger("click");
            }
        });
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body onmouseover="deletebox(0);">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            <asp:Label ID="Lbl_Titolo" runat="server" Text=""></asp:Label></div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;"></td>
                </tr>


                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td>

                                    <table width="100%" id="tab1">
                                        <tr>
                                            <td align="left" style="width: 120px;">Ordinamento :</td>
                                            <td style="width: 200px;">
                                                <asp:RadioButton ID="Rb_Id" runat="server" Text="ID" Checked="true" GroupName="Orinamento" AutoPostBack="true" />
                                                <asp:RadioButton ID="Rb_DataRegistrazione" runat="server" Text="Data Reg." GroupName="Orinamento" AutoPostBack="true" />
                                                <asp:RadioButton ID="Rb_Protocollo" runat="server" Text="Protoc." GroupName="Orinamento" AutoPostBack="true" />
                                            </td>
                                            <td style="color: #565151; width: 150px;">
                                                <asp:DropDownList ID="DD_Tipo" runat="server" AutoPostBack="true">
                                                    <asp:ListItem Text="Decrescente" Value="DESC"></asp:ListItem>
                                                    <asp:ListItem Text="Crescente" Value="ASC"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="color: #565151; width: 80px;">Causale :</td>
                                            <td style="color: #565151; width: 100px;">
                                                <asp:DropDownList ID="DD_CausaleContabile" Width="100px" class="chosen-select" runat="server"></asp:DropDownList></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table width="100%" id="tab2">
                                        <tr>
                                            <td style="color: #565151; width: 80px;">Numero :</td>
                                            <td style="color: #565151; width: 100px;">
                                                <asp:TextBox ID="Txt_NumeroProtocollo" onkeypress="return handleEnter(this, event)" runat="server" Width="80px"></asp:TextBox></td>
                                            <td style="color: #565151; width: 100px;">Anno Protocollo :</td>
                                            <td style="color: #565151; width: 100px;">
                                                <asp:TextBox ID="Txt_AnnoProtocollo" onkeypress="return handleEnter(this, event)" runat="server" Width="80px"></asp:TextBox></td>
                                            <td style="color: #565151; width: 120px;">Registro :</td>
                                            <td style="color: #565151; width: 150px;">
                                                <asp:DropDownList ID="DD_RegistroIva" runat="server"></asp:DropDownList></td>
                                        </tr>
                                    </table>

                                    <table width="100%" id="tab3">
                                        <tr>
                                            <td style="color: #565151; width: 90px;">Data Dal :</td>
                                            <td style="width: 110px; width: 110px;">
                                                <asp:TextBox ID="Txt_DataDal" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td style="color: #565151; width: 90px;">Data Al :</td>
                                            <td style="width: 110px; width: 110px;">
                                                <asp:TextBox ID="Txt_DataAl" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td style="color: #565151; width: 60px;">Conto :</td>
                                            <td>
                                                <asp:TextBox ID="Txt_Conto" onkeypress="return handleEnter(this, event)" runat="server" Width="300px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table width="100%" id="tab4">
                                        <tr>
                                            <td style="color: #565151; width: 110px;">N.Reg. Dal :</td>
                                            <td style="width: 110px;">
                                                <asp:TextBox ID="Txt_RegistrazioneDal" onkeypress="return handleEnterSoloNumero(this, event)" runat="server" Width="80px"></asp:TextBox></td>
                                            <td style="color: #565151; width: 110px;">N.Reg. Al :</td>
                                            <td style="width: 110px;">
                                                <asp:TextBox ID="Txt_RegistrazioneAl" onkeypress="return handleEnterSoloNumero(this, event)" runat="server" Width="80px"></asp:TextBox></td>
                                            <td style="color: #565151; width: 70px;">
                                                <asp:Label ID="lblCentroServizio" Visible="false" runat="server" Text="Centro Servizio :"></asp:Label></td>
                                            <td style="color: #565151; width: 150px;">
                                                <asp:DropDownList ID="DD_CServ" Visible="false" runat="server" Width="150px"></asp:DropDownList></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                </td>
                                <td style="text-align: right;">
                                    <asp:ImageButton ID="Btn_Ricerca" runat="server" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" BackColor="Transparent" alt="Ricerca" ToolTip="Ricerca" Style="margin-right: 1px;" />
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>





                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <asp:ImageButton ID="ImgRicerca" ImageUrl="images/ricerca.jpg" Width="112px" Height="100px" alt="Ricerca Registrazioni" class="Effetto" ToolTip="Ricerca Registrazioni/Saldo" runat="server" /><br />
                    </td>

                    <td class="style1" colspan="2" style="vertical-align: top;">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>

                                <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                                    <asp:ImageButton ID="ImgTipo" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" runat="server" />
                                </div>
                                <asp:GridView ID="Grid" runat="server" CellPadding="3"
                                    Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                    BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                                    PageSize="1220">
                                    <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="26px" ItemStyle-Width="26px" FooterStyle-Width="26px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                                    ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                                    CommandArgument="<%#   Container.DataItemIndex  %>" ToolTip="Richiama" />
                                            </ItemTemplate>

                                            <FooterStyle Width="26px"></FooterStyle>

                                            <HeaderStyle Width="26px"></HeaderStyle>

                                            <ItemStyle Width="26px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                </asp:GridView>

                                <asp:ImageButton ID="Img_Espandi" runat="server" ImageUrl="../images/Blanco.png" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; text-align: center;">
                        <br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>

            </table>

        </div>
    </form>
</body>
</html>
