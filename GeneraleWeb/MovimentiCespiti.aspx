﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_MovimentiCespiti" CodeFile="MovimentiCespiti.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:GridView ID="Grid" runat="server" CellPadding="3"
                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                PageSize="20">
                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                <Columns>
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                <AlternatingRowStyle BackColor="#DCDCDC" />
            </asp:GridView>
            <asp:Label ID="lbl_Totale" runat="server" Text="" Width="100%"></asp:Label>
        </div>
    </form>
</body>
</html>
