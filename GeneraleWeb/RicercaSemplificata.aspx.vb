﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_RicercaSemplificata
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Esegui_Ricerca(ByVal ToExcel As Boolean)
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim MySql As String
        Dim Condizione As String
        Dim Tipo As Integer = 0
        Dim MySqlrs As String
        Dim MySqlrs2 As String
        Dim Saldo As Double


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Trim(Txt_Sottoconto.Text) <> "" Then
            Tipo = 2
            MySql = "Select '0' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And "
            MySqlrs = "Select '1' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And "
            MySqlrs2 = "Select '2' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And "
        Else
            Tipo = 1
            MySql = "Select '0' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione  And "
            MySqlrs = "Select '1' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione  And "
            MySqlrs2 = "Select '2' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliTesta.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione  And "
        End If

        Condizione = ""

        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)


            If Val(Vettore(0)) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND "
                End If
                Condizione = Condizione & " MastroPartita = " & Val(Vettore(0))
            End If

            If Val(Vettore(1)) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND "
                End If
                Condizione = Condizione & " ContoPartita = " & Val(Vettore(1))
            End If

            If Val(Vettore(2)) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND "
                End If
                Condizione = Condizione & " SottoContoPartita = " & Val(Vettore(2))
            End If
            Saldo = 0

            If IsDate(Txt_DataDal.Text) Then
                Dim DataDal As Date

                DataDal = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))
                Dim cmdSaldoD As New OleDbCommand()
                cmdSaldoD.CommandText = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Val(Vettore(0)) & _
                    " AND ContoPartita = " & Val(Vettore(1)) & _
                    " AND SottocontoPartita = " & Val(Vettore(2)) & _
                    " AND DareAvere = 'D' " & _
                    " AND DataRegistrazione < {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
                cmdSaldoD.Connection = cn

                Dim ReadSaldoD As OleDbDataReader = cmdSaldoD.ExecuteReader()
                If ReadSaldoD.Read Then
                    Saldo = Saldo + campodbdbl(ReadSaldoD.Item("Totale"))
                End If
                ReadSaldoD.Close()

                Dim cmdSaldo As New OleDbCommand()
                cmdSaldo.CommandText = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Val(Vettore(0)) & _
                    " AND ContoPartita = " & Val(Vettore(1)) & _
                    " AND SottocontoPartita = " & Val(Vettore(2)) & _
                    " AND DareAvere = 'A' " & _
                    " AND DataRegistrazione < {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
                cmdSaldo.Connection = cn

                Dim ReadSaldo As OleDbDataReader = cmdSaldo.ExecuteReader()
                If ReadSaldo.Read Then
                    Saldo = Saldo - campodbdbl(ReadSaldo.Item("Totale"))
                End If
                ReadSaldo.Close()


            End If

        End If

        If Txt_Descrizione.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MovimentiContabiliTesta.Descrizione Like '" & Txt_Descrizione.Text & "'"
        End If


        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If

        If RB_Entrate.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If

            Dim Appo As New Cls_ParametriGenerale

            Appo.LeggiParametri(Session("DC_GENERALE"))

            Condizione = Condizione & " CausaleContabile = '" & Appo.CausaleEntrata & "'"
        End If

        If Txt_ImportoDal.Text <> "" Then
            If CDbl(Txt_ImportoDal.Text) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " Importo >=  ? "
            End If
        End If

        If Txt_ImportoAl.Text <> "" Then
            If CDbl(Txt_ImportoAl.Text) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " Importo <=  ?"
            End If
        End If

        If RB_Uscite.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If

            Dim Appo As New Cls_ParametriGenerale


            Appo.LeggiParametri(Session("DC_GENERALE"))

            Condizione = Condizione & " CausaleContabile = '" & Appo.CausaleUscita & "'"
        End If

        If RB_Giroconto.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Dim Appo As New Cls_ParametriGenerale

            Appo.LeggiParametri(Session("DC_GENERALE"))

            Condizione = Condizione & " CausaleContabile = '" & Appo.CausaleGiroconto & "'"
        End If


        Lbl_Errori.Text = ""
        If Condizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nessuna condizione indicata');", True)
            REM Lbl_Errori.Text = "Errore nessuna condizione indicata"
            Exit Sub
        End If
        If Tipo = 2 Then
            MySql = MySql & Condizione & " Order By DataRegistrazione,NumeroRegistrazione,CausaleContabile, RigaDaCausale "
        Else
            MySql = MySql & Condizione & " Order By DataRegistrazione,NumeroRegistrazione,CausaleContabile, RigaDaCausale,MastroPartita,ContoPartita,SottocontoPartita "
        End If



        cmd.CommandText = (MySql)
        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAl As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
        End If

        If Txt_ImportoDal.Text <> "" Then
            If CDbl(Txt_ImportoDal.Text) > 0 Then
                cmd.Parameters.AddWithValue("@ImportoDal", CDbl(Txt_ImportoDal.Text))
            End If
        End If


        If Txt_ImportoAl.Text <> "" Then
            If CDbl(Txt_ImportoAl.Text) > 0 Then
                cmd.Parameters.AddWithValue("@ImportoAl", CDbl(Txt_ImportoAl.Text))
            End If
        End If



        cmd.Connection = cn




        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("CausaleContabile", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))
        Tabella.Columns.Add("Saldo", GetType(String))
        If Trim(Txt_Sottoconto.Text) = "" Then
            Tabella.Columns.Add("Conto", GetType(String))
        End If
     

        If Trim(Txt_Sottoconto.Text) <> "" Then
            If Math.Abs(Saldo) > 0 Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(6) = Format(Saldo, "#,##0.00")
                Tabella.Rows.Add(myriga)
            End If
        End If

        Dim oldCodiceServizio As String
        Dim OldCodiceOspite As Long
        oldCodiceServizio = ""
        OldCodiceOspite = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("NumeroRegistrazione")
            If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                myriga(1) = ""
            Else
                myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            End If


            Dim dC As New Cls_CausaleContabile

            dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
            myriga(2) = dC.Descrizione

            myriga(3) = campodb(myPOSTreader.Item("Descrizione"))


            If campodb(myPOSTreader.Item("DareAvere")) = "D" Then
                myriga(4) = Format(campodbdbl(myPOSTreader.Item("Importo")), "#,##0.00")
                myriga(5) = 0
                Saldo = Saldo + campodbdbl(myPOSTreader.Item("Importo"))
            Else
                myriga(4) = 0
                myriga(5) = Format(campodbdbl(myPOSTreader.Item("Importo")), "#,##0.00")
                Saldo = Saldo - campodbdbl(myPOSTreader.Item("Importo"))
            End If
            'myriga(6) = myPOSTreader.Item("RigaDaCausale")
            myriga(6) = Format(Saldo, "#,##0.00")

            If Trim(Txt_Sottoconto.Text) = "" Then
                Dim xs As New Cls_Pianodeiconti
                xs.Mastro = Val(campodb(myPOSTreader.Item("Mastropartita")))
                xs.Conto = Val(campodb(myPOSTreader.Item("Contopartita")))
                xs.Sottoconto = Val(campodb(myPOSTreader.Item("Sottocontopartita")))
                xs.Decodfica(Session("DC_GENERALE"))
                myriga(7) = campodb(xs.Descrizione)
            End If

            Tabella.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()
        cn.Close()

        If ToExcel = True Then
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.DataBind()
        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If
        Session("RicercaGenerale") = Tabella
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbdbl(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Dim MyJs As String

        MyJs = "$(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataAl", MyJs, True)

        'MyJs = "$(" & Chr(34) & Txt_Sottoconto.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "',{delay:10,minChars:3});"
        MyJs = "$(" & Chr(34) & "#" & Txt_Sottoconto.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatSottoconto", MyJs, True)


        MyJs = "$('#" & Txt_ImportoDal.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_ImportoDal.ClientID & "').val(), true, true); } );  $('#" & Txt_ImportoDal.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoDal.ClientID & "').val(),2); $('#" & Txt_ImportoDal.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ImportoDal", MyJs, True)

        MyJs = "$('#" & Txt_ImportoAl.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_ImportoAl.ClientID & "').val(), true, true); } );  $('#" & Txt_ImportoAl.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoAl.ClientID & "').val(),2); $('#" & Txt_ImportoAl.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ImportoAl", MyJs, True)

        Lbl_Utente.text = Session("UTENTE")

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_DataDal.Text = Format(DateSerial(Year(Now), 1, 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")


        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand

        If e.CommandName = "Seleziona" Then
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = Session("RicercaGenerale")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(0).ToString



            ViewState("XLINEA") = d
            ViewState("XBACKUP") = Grid.Rows(d).BackColor
            Grid.Rows(d).BackColor = Drawing.Color.Violet


            Response.Redirect("RegistrazioneSemplificata.aspx")

        End If

        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)

            Session("NumeroRegistrazione") = Registrazione
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/RegistrazioneSemplificata.aspx');  });", True)
            REM Response.Redirect("../GeneraleWeb/primanota.aspx")
            Exit Sub
        End If
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data al formalmente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Errore data al formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                Exit Sub
            End If
        End If
 

        Call Esegui_Ricerca(False)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data al formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                Exit Sub
            End If
        End If
   
        Call Esegui_Ricerca(True)
        If Tabella.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("UltimiMovimenti.aspx?TIPO=SMP")
    End Sub

End Class
