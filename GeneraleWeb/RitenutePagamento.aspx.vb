﻿
Partial Class GeneraleWeb_RitenutePagamento
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load
        Dim Ritenut As New Cls_RitenuteAcconto


        If Page.IsPostBack = True Then Exit Sub



        Ritenut.LeggiMovimentiDaRegistrazione(Session("DC_GENERALE"), Session("DC_TABELLE"), 0, Request.Item("NumeroRegistrazione"), MyTable)

        Grid.AutoGenerateColumns = True
        Grid.DataSource = MyTable
        Grid.DataBind()

        Session("GestioneLegami") = MyTable
    End Sub


    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Select" Then
            Dim d As Long

            MyTable = Session("GestioneLegami")
            d = Val(e.CommandArgument)

            If CDbl(MyTable.Rows(d).Item(13).ToString) = 0 Then
                Response.Redirect("RitenuteModificaPagamenti.aspx?ID=" & MyTable.Rows(d).Item(14).ToString & "&NUMERO=" & Request.Item("NumeroRegistrazione"))
            End If


        End If
    End Sub
End Class
