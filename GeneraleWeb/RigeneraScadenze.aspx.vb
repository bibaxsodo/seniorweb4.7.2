﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class GeneraleWeb_RigeneraScadenze
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim cn As OleDbConnection
        Dim Segnalazioni As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = "Select *, (Select sum(Importo) From TabellaLegami Where CodiceDocumento = NumeroRegistrazione) as TotaleIncassato From MovimentiContabiliTesta  where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA = ?"

        cmd.Parameters.AddWithValue("@DataRegistrazioneDal", Txt_DataInizio.Text)
        cmd.Parameters.AddWithValue("@DataRegistrazioneAl", Txt_DataFine.Text)
        cmd.Parameters.AddWithValue("@RegistroIVA", Val(DD_RegistroIVA.SelectedValue))

        cmd.Connection = cn
        Dim RsOspiteParente As OleDbDataReader = cmd.ExecuteReader()
        Do While RsOspiteParente.Read
            Dim ImportoPagato As Double = 0
            Dim Documento As New Cls_MovimentoContabile
            Dim Procedo As Boolean = True
            Dim ChiudiScadenze As Boolean = False


            Documento.NumeroRegistrazione = campodbN(RsOspiteParente.Item("NumeroRegistrazione"))
            Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)

            Dim XScd As New Cls_Scadenziario
            Dim TbEScd As New System.Data.DataTable("TbEScd")
            XScd.NumeroRegistrazioneContabile = Documento.NumeroRegistrazione
            XScd.loaddati(Session("DC_GENERALE"), TbEScd)

            If TbEScd.Rows.Count > 1 Then
                Segnalazioni = "Scadenze già presenti per documento " & Documento.NumeroRegistrazione
                Procedo = False
            End If

            If TbEScd.Rows.Count = 1 Then
                If Not IsDBNull(TbEScd.Rows(0).Item(1)) Then
                    If Val(TbEScd.Rows(0).Item(1)) > 0 Then
                        Segnalazioni = "Scadenze già presenti per documento " & Documento.NumeroRegistrazione
                        Procedo = False
                    End If
                End If
            End If


            ImportoPagato = campodbN(RsOspiteParente.Item("TotaleIncassato"))


            If Math.Round(Documento.ImportoDocumento(Session("DC_TABELLE")), 2) = Math.Round(ImportoPagato, 2) Then
                ChiudiScadenze = True
            Else
                If ImportoPagato > 0 Then
                    Segnalazioni = "Scadenze non create per documento " & Documento.NumeroRegistrazione & " pagato per " & ImportoPagato & " Importo Documento " & Math.Round(Documento.ImportoDocumento(Session("DC_TABELLE")), 2)
                    Procedo = False
                End If
            End If

            If Procedo Then
                Dim xModalita As New Cls_TipoPagamento

                xModalita.Codice = Documento.ModalitaPagamento
                xModalita.Leggi(Session("DC_TABELLE"))


                Dim XS As New Cls_Scadenziario
                Dim DataScadenza As New Date

                XS.NumeroRegistrazioneContabile = Documento.NumeroRegistrazione

                If Year(Documento.DataDocumento) < 1900 Then
                    DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, Documento.DataDocumento)
                    If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                        DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, Documento.DataDocumento)
                    End If
                    If xModalita.Tipo = "F" Then
                        If Month(Documento.DataRegistrazione) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                            DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                        Else
                            DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                        End If
                    End If
                Else
                    DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, Documento.DataDocumento)
                    If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                        DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, Documento.DataDocumento)
                    End If
                    If xModalita.Tipo = "F" Then
                        If Month(Documento.DataDocumento) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                            DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                        Else
                            DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                        End If
                    End If
                End If

                XS.DataScadenza = DataScadenza
                Dim ImportoNetto As Double = Documento.ImportoDocumento(Session("DC_TABELLE")) - Documento.ImportoRitenuta(Session("DC_TABELLE"))
                Dim ImportoScadenza As Double = 0

                If xModalita.Scadenze > 0 Then
                    XS.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                    ImportoScadenza = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                    XS.Chiusa = 0
                    If ChiudiScadenze = True Then
                        XS.Chiusa = 1
                    End If
                    XS.Descrizione = ""
                    XS.ScriviScadenza(Session("DC_GENERALE"))
                End If

                If xModalita.Scadenze > 1 Then
                    Dim XS1 As New Cls_Scadenziario
                    XS1.NumeroRegistrazioneContabile = Documento.NumeroRegistrazione
                    'XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                    If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                        XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                        ImportoScadenza = ImportoScadenza + XS1.Importo
                    Else
                        XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                        ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    End If

                    DataScadenza = DateAdd("d", xModalita.GiorniSeconda + xModalita.GiorniPrima, Documento.DataDocumento)
                    If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                    XS1.DataScadenza = DataScadenza

                    XS1.Chiusa = 0
                    If ChiudiScadenze = True Then
                        XS1.Chiusa = 1
                    End If
                    XS1.Descrizione = ""
                    XS1.ScriviScadenza(Session("DC_GENERALE"))
                End If

                If xModalita.Scadenze > 2 Then
                    Dim GIORNIDA As Long
                    GIORNIDA = xModalita.GiorniSeconda + xModalita.GiorniPrima
                    For i = 3 To xModalita.Scadenze
                        GIORNIDA = GIORNIDA + xModalita.GiorniAltre
                        Dim XS1 As New Cls_Scadenziario
                        XS1.NumeroRegistrazioneContabile = Documento.NumeroRegistrazione
                        DataScadenza = DateAdd("d", GIORNIDA, Documento.DataDocumento)
                        If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                        XS1.DataScadenza = DataScadenza
                        'XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                        If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                            XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                            ImportoScadenza = ImportoScadenza + XS1.Importo
                        Else
                            XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                            ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                        End If

                        XS1.Chiusa = 0
                        If ChiudiScadenze = True Then
                            XS1.Chiusa = 1
                        End If
                        XS1.Descrizione = ""
                        XS1.ScriviScadenza(Session("DC_GENERALE"))
                    Next i
                End If
            End If

        Loop

        cn.Close()

        Lbl_Errori.Text = Segnalazioni

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim RegIVA As New Cls_RegistroIVA


        RegIVA.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
