﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class GeneraleWeb_UltimiMovimenti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Private Sub CaricaGriglia()
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim ConnectionString As String = Session("DC_TABELLE")

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("N.Reg.", GetType(String))        
        Tabella.Columns.Add("Data Reg.", GetType(String))
        If Request.Item("TIPO") = "DOC" Then
            Tabella.Columns.Add("Num. Doc.", GetType(String))
            Tabella.Columns.Add("Data Doc.", GetType(String))
            Tabella.Columns.Add("Num. Prot.", GetType(Long))
            Tabella.Columns.Add("Anno Prot.", GetType(Long))
        End If
        If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then
            Tabella.Columns.Add("Documenti Pagati", GetType(String))
        End If

        Tabella.Columns.Add("Causale", GetType(String))

        If Request.Item("TIPO") = "DOC" Or Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then
            Tabella.Columns.Add("Clienti", GetType(String))
        End If

        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))


        Dim MySql As String
        Dim EstraiCondizioniSQL As String


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()

        Dim cnTB As OleDbConnection

        cnTB = New Data.OleDb.OleDbConnection(ConnectionString)

        cnTB.Open()


        MySql = ""
        EstraiCondizioniSQL = ""

        If Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "MANREV" Then
            MySql = "Select * From CausaliContabiliTesta  "
        End If

        If Request.Item("TIPO") = "INC" Then
            MySql = "Select * From CausaliContabiliTesta Where TIPO = 'P'"
        End If
        If Request.Item("TIPO") = "INCS" Then
            MySql = "Select * From CausaliContabiliTesta Where TIPO = 'P'"
        End If
        If Request.Item("TIPO") = "DOC" Then
            MySql = "Select  * From CausaliContabiliTesta Where (TIPO = 'I'  OR TIPO = 'R')  "
        End If

        If MySql <> "" Then

            Dim cmdX As New OleDbCommand()

            cmdX.CommandText = MySql
            cmdX.Connection = cnTB
            Dim myPOSTreaderX As OleDbDataReader = cmdX.ExecuteReader()
            Do While myPOSTreaderX.Read
                If EstraiCondizioniSQL <> "" Then
                    EstraiCondizioniSQL = EstraiCondizioniSQL & " or "
                End If
                EstraiCondizioniSQL = EstraiCondizioniSQL & " CausaleContabile = '" & myPOSTreaderX.Item("CODICE") & "'"
            Loop
            myPOSTreaderX.Close()
        End If
        cnTB.Close()


        If Request.Item("TIPO") = "SMP" Then
            Dim ParametriGenerale As New Cls_ParametriGenerale

            ParametriGenerale.LeggiParametri(ConnectionStringGenerale)
            EstraiCondizioniSQL = ""
        End If


        Dim k1 As New Cls_SqlString

        k1.Add("Txt_DataAl", Txt_DataAl.Text)
        k1.Add("Txt_DataDal", Txt_DataDal.Text)
        k1.Add("Txt_RegistrazioneDal", Txt_RegistrazioneDal.Text)
        k1.Add("Txt_RegistrazioneAl", Txt_RegistrazioneAl.Text)
        k1.Add("Txt_Conto", Txt_Conto.Text)
        k1.Add("Rb_DataRegistrazione", Rb_DataRegistrazione.Checked)
        k1.Add("Rb_Id", Rb_Id.Checked)
        k1.Add("DD_Tipo", DD_Tipo.SelectedValue)
        k1.Add("TIPO", Request.Item("TIPO"))
        k1.Add("DD_CausaleContabile", DD_CausaleContabile.SelectedValue)

        k1.Add("DD_RegistroIva", DD_RegistroIva.SelectedValue)

        k1.Add("Txt_AnnoProtocollo", Txt_AnnoProtocollo.Text)
        k1.Add("Txt_NumeroProtocollo", Txt_NumeroProtocollo.Text)
        k1.Add("DD_CServ", DD_CServ.SelectedValue)


        Session("RicercaGeneraleSQLString") = k1

        Dim NumeroRiga As Integer = 0
        Dim cmd As New OleDbCommand()
        Dim Record As String
        If Txt_DataAl.Text = "" And Txt_DataDal.Text = "" And Txt_RegistrazioneDal.Text = "" And Txt_RegistrazioneAl.Text = "" And Txt_Conto.Text = "" Then
            Record = "Top 20 *"
        Else
            Record = " *"
        End If

        If Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "MANREV" Then
            MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
        End If
        If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
            MySql = "Select " & Record & " From MovimentiContabiliTesta"
        End If

        If Request.Item("TIPO") = "DOC" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If
        If Request.Item("TIPO") = "INC" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If
        If Request.Item("TIPO") = "INCS" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAL As Date
            DataAL = Txt_DataAl.Text
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                MySql = MySql & " Where "
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataRegistrazione <= {ts '" & Format(DataAL, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date
            DataDal = Txt_DataDal.Text

            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If Val(Txt_AnnoProtocollo.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " AnnoProtocollo = " & Val(Txt_AnnoProtocollo.Text)
        End If
        If Val(Txt_NumeroProtocollo.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroProtocollo = " & Val(Txt_NumeroProtocollo.Text)
        End If

        If Val(Txt_RegistrazioneDal.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroRegistrazione >= " & Val(Txt_RegistrazioneDal.Text)
        End If

        If Val(Txt_RegistrazioneAl.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroRegistrazione <= " & Val(Txt_RegistrazioneAl.Text)
        End If


        If DD_CausaleContabile.SelectedValue <> "" Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
        End If

        If DD_CServ.SelectedValue <> "" Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " CentroServizio = '" & DD_CServ.SelectedValue & "'"
        End If

        If Val(DD_RegistroIva.SelectedValue) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " RegistroIva = " & Val(DD_RegistroIva.SelectedValue)
        End If


        If Txt_Conto.Text <> "" Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If

            Dim SMastro As Long = 0
            Dim SConto As Long = 0
            Dim SSottoconto As Long = 0


            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Conto.Text
            Vettore = SplitWords(APPOGGIO)

            Try
                SMastro = Vettore(0)
                SConto = Vettore(1)
                SSottoconto = Vettore(2)
            Catch ex As Exception

            End Try
            
            MySql = MySql & " (Select count(*) From MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & SMastro & " And ContoPartita = " & SConto & " And SottocontoPartita = " & SSottoconto & ") > 0"
        End If


        If Rb_Id.Checked = True Then
            MySql = MySql & "  Order by NumeroRegistrazione " & DD_Tipo.SelectedValue
        End If
        If Rb_DataRegistrazione.Checked = True Then
            MySql = MySql & "  Order by DataRegistrazione " & DD_Tipo.SelectedValue
        End If
        If Rb_Protocollo.Checked = True Then
            MySql = MySql & "  Order by [AnnoProtocollo],[NumeroProtocollo] " & DD_Tipo.SelectedValue
        End If



        cmd.CommandText = (MySql)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Not IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                NumeroRiga = NumeroRiga + 1
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = myPOSTreader.Item("NumeroRegistrazione")
                'If Val(campodb(myPOSTreader.Item("IdProgettoODV"))) > 0 Or Val(campodb(myPOSTreader.Item("IDODV"))) > 0 Or Val(campodb(myPOSTreader.Item("TipoODV"))) > 0 Then
                '    myriga(0) = myPOSTreader.Item("NumeroRegistrazione") & "L"
                'End If


                If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                    myriga(1) = ""
                Else
                    myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
                End If

                If Request.Item("TIPO") = "DOC" Then
                    myriga(2) = myPOSTreader.Item("NumeroDocumento")
                    If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                        myriga(3) = ""
                    Else
                        myriga(3) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
                    End If
                    myriga(4) = myPOSTreader.Item("NumeroProtocollo")
                    myriga(5) = myPOSTreader.Item("AnnoProtocollo")

                    Dim dC As New Cls_CausaleContabile

                    dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
                    myriga(6) = dC.Descrizione

                    Dim XM As New Cls_MovimentoContabile

                    XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))



                    Dim PConti As New Cls_Pianodeiconti

                    PConti.Mastro = XM.Righe(0).MastroPartita
                    PConti.Conto = XM.Righe(0).ContoPartita
                    PConti.Sottoconto = XM.Righe(0).SottocontoPartita
                    PConti.Decodfica(Session("DC_GENERALE"))

                    myriga(7) = PConti.Descrizione

                    Dim AppoggioDescrizione As String = XM.Descrizione
                    If AppoggioDescrizione.Length > 100 Then
                        AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                    End If
                    myriga(8) = AppoggioDescrizione

                    myriga(9) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")
                Else

                    If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then

                        Dim RdLegami As New Cls_Legami

                        myriga(2) = ""
                        RdLegami.Leggi(ConnectionStringGenerale, 0, Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))

                        Dim InDleg As Integer

                        For InDleg = 0 To 3
                            If RdLegami.NumeroDocumento(InDleg) > 0 Then
                                Dim LeggiDoc As New Cls_MovimentoContabile

                                LeggiDoc.Leggi(ConnectionStringGenerale, RdLegami.NumeroDocumento(InDleg))
                                If LeggiDoc.NumeroDocumento = "" Then
                                    myriga(2) = myriga(3) & " Doc. n. " & LeggiDoc.NumeroProtocollo & " data " & LeggiDoc.DataDocumento
                                Else
                                    myriga(2) = myriga(3) & " Doc. n. " & LeggiDoc.NumeroDocumento & " data " & LeggiDoc.DataDocumento
                                End If
                            End If
                        Next
                        If Len(myriga(2)) > 50 Then
                            myriga(2) = Mid(myriga(2), 1, 49) & "..."
                        End If


                        Dim dC As New Cls_CausaleContabile

                        dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

                        myriga(3) = dC.Descrizione

                        Dim XM As New Cls_MovimentoContabile

                        XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))

                        Dim PConti As New Cls_Pianodeiconti

                        PConti.Mastro = XM.Righe(0).MastroPartita
                        PConti.Conto = XM.Righe(0).ContoPartita
                        PConti.Sottoconto = XM.Righe(0).SottocontoPartita
                        PConti.Decodfica(Session("DC_GENERALE"))

                        myriga(4) = PConti.Descrizione

                        Dim AppoggioDescrizione As String = XM.Descrizione
                        If AppoggioDescrizione.Length > 100 Then
                            AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                        End If
                        myriga(5) = AppoggioDescrizione
                        myriga(6) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")

                    Else
                        Dim dC As New Cls_CausaleContabile

                        dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

                        Dim AppoggioDescrizione As String = dC.Descrizione
                        If AppoggioDescrizione.Length > 100 Then
                            AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                        End If
                        myriga(2) = AppoggioDescrizione

                        Dim XM As New Cls_MovimentoContabile

                        XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))

                        Dim AppoggioDescrizioneInt As String = XM.Descrizione
                        If AppoggioDescrizioneInt.Length > 100 Then
                            AppoggioDescrizioneInt = Mid(AppoggioDescrizioneInt, 1, 100)
                        End If

                        myriga(3) = AppoggioDescrizioneInt
                        myriga(4) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")

                    End If
                End If

                Tabella.Rows.Add(myriga)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        Session("UltimoNumero") = NumeroRiga + 1
        ViewState("RicercaGenerale") = Tabella
        Grid.AutoGenerateColumns = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        Dim i As Long
        For i = 0 To Grid.Rows.Count - 1

            Grid.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Right
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "SMPPRN" Then
                Grid.Rows(i).Cells(5).HorizontalAlign = HorizontalAlign.Right
            End If
            If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then
                Grid.Rows(i).Cells(6).HorizontalAlign = HorizontalAlign.Right
            End If
            If Request.Item("TIPO") = "DOC" Then
                Grid.Rows(i).Cells(5).HorizontalAlign = HorizontalAlign.Right
                Grid.Rows(i).Cells(6).HorizontalAlign = HorizontalAlign.Right
                Grid.Rows(i).Cells(10).HorizontalAlign = HorizontalAlign.Right
            End If
        Next
        Grid.DataBind()

        For i = 0 To Grid.Rows.Count - 1
            If Grid.Rows(i).Cells(1).Text.IndexOf("L") > 0 Then
                Dim k As Image = Grid.Rows(i).Cells(0).FindControl("Seleziona")
                k.ImageUrl = "../images/Lock.png"
            End If
            Grid.Rows(i).Cells(1).Text = Grid.Rows(i).Cells(1).Text.Replace("L", "")
        Next

        Call EseguiJS()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("ULTIMOPROGRAMMA") = "ULTIMO"

        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If



        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Dim LoadCau As New Cls_CausaleContabile
        If Request.Item("TIPO") = "DOC" Then
            LoadCau.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleContabile)
        Else
            If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then
                LoadCau.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleContabile)
            Else
                LoadCau.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleContabile)
            End If
        End If

        Dim RegIva As New Cls_RegistroIVA

        RegIva.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIva)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                Session("RicercaGeneraleSQLString") = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Request.Item("SOLOVISUALIZZAZIONE") = "SI" Then
            Call MettiInvisibileJS()
        End If

        Lbl_Titolo.Text = "Contabilità - Principale - Ultimo movimento"


        Dim Cserv As New Cls_CentroServizio

        Cserv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)

        Dim Into As New Cls_DatiGenerali


        Into.LeggiDati(Session("DC_TABELLE"))


        '?TIPO=DOC

        If Into.AttivaCServPrimanoIncassi = 1 Or Request.Item("TIPO") = "DOC" Then

            DD_CServ.Visible = True
            lblCentroServizio.Visible = True
        End If



        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Session("RicercaGeneraleSQLString") = Nothing
            Call CaricaGriglia()
        Else

            If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
                Dim k As New Cls_SqlString
                Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("RicercaGeneraleSQLString")

                Txt_DataAl.Text = k.GetValue("Txt_DataAl")
                Txt_DataDal.Text = k.GetValue("Txt_DataDal")
                Txt_RegistrazioneDal.Text = k.GetValue("Txt_RegistrazioneDal")
                Txt_RegistrazioneAl.Text = k.GetValue("Txt_RegistrazioneAl")
                Txt_Conto.Text = k.GetValue("Txt_Conto")
                Rb_DataRegistrazione.Checked = k.GetValue("Rb_DataRegistrazione")
                Rb_Id.Checked = k.GetValue("Rb_Id")
                DD_Tipo.SelectedValue = k.GetValue("DD_Tipo")
                DD_CausaleContabile.SelectedValue = k.GetValue("DD_CausaleContabile")
                Txt_AnnoProtocollo.Text = k.GetValue("Txt_AnnoProtocollo")
                Txt_NumeroProtocollo.Text = k.GetValue("Txt_NumeroProtocollo")
                DD_RegistroIva.SelectedValue = k.GetValue("DD_RegistroIva")
                DD_CServ.SelectedValue = k.GetValue("DD_CServ")

                Call Btn_Ricerca_Click(sender, Appoggio)



            Else
                Call CaricaGriglia()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grid.PageCount Then
                    Grid.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tabella = ViewState("RicercaGenerale")

            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If

        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
    End Sub

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("RicercaGenerale")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()

        Dim i As Integer

        For i = 0 To Grid.Rows.Count - 1
            If Grid.Rows(i).Cells(1).Text.IndexOf("L") > 0 Then
                Dim k As Image = Grid.Rows(i).Cells(0).FindControl("Seleziona")
                k.ImageUrl = "../images/Lock.png"
            End If
            Grid.Rows(i).Cells(1).Text = Grid.Rows(i).Cells(1).Text.Replace("L", "")
        Next



    End Sub

    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit

    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then

            If Request.Item("SOLOVISUALIZZAZIONE") = "SI" Then
                Exit Sub
            End If

            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = ViewState("RicercaGenerale")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(0).ToString

            

            If Request.Item("TIPO") = "SMP" Then
                Response.Redirect("RegistrazioneSemplificata.aspx?PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "SMPPRN" Then
                Response.Redirect("primanota.aspx?PROVENIENTE=SEMPLIFICATA&PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "PRNT" Then
                Response.Redirect("primanota.aspx?PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "DOC" Then
                Response.Redirect("documenti.aspx?PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "INC" Then
                Response.Redirect("incassipagamenti.aspx?PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "INCS" Then
                Response.Redirect("incassipagamenti.aspx?TIPO=SCADENZARIO&PAGINA=" & Grid.PageIndex)
            End If
            If Request.Item("TIPO") = "MANREV" Then
                Response.Redirect("primanota.aspx?PROVENIENTE=REVMAN&PAGINA=" & Grid.PageIndex)
            End If

        End If
    End Sub

    Protected Sub ImgTipo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgTipo.Click        
        Session("NumeroRegistrazione") = 0
        If Request.Item("TIPO") = "PRNT" Then
            Response.Redirect("primanota.aspx")
        End If
        If Request.Item("TIPO") = "SMPPRN" Then
            Response.Redirect("primanota.aspx?PROVENIENTE=SEMPLIFICATA")
        End If
        If Request.Item("TIPO") = "DOC" Then
            Response.Redirect("documenti.aspx")
        End If
        If Request.Item("TIPO") = "INC" Then
            Response.Redirect("incassipagamenti.aspx")
        End If
        If Request.Item("TIPO") = "INCS" Then
            Response.Redirect("incassipagamenti.aspx?TIPO=SCADENZARIO")
        End If
        If Request.Item("TIPO") = "SMP" Then
            Response.Redirect("RegistrazioneSemplificata.aspx")
        End If
        If Request.Item("TIPO") = "MANREV" Then
            Response.Redirect("CreaMandatiReversali.aspx")
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
            Response.Redirect("RicercaSemplificata.aspx?TIPO=" & Request.Item("TIPO"))
        Else
            Response.Redirect("Ricerca.aspx?TIPO=" & Request.Item("TIPO"))
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("TIPO") = "MANREV" Then
            Response.Redirect("Menu_Budget.aspx")
            Exit Sub
        End If
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click
        Call CaricaGriglia()
        Call EseguiJS()
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "SMPPRN" Then
                e.Row.Cells(1).HorizontalAlign = HorizontalAlign.Right
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            End If
            If Request.Item("TIPO") = "INCS" Or Request.Item("TIPO") = "INC" Then
                e.Row.Cells(1).HorizontalAlign = HorizontalAlign.Right
                e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
                e.Row.Cells(3).Width = 380
            End If

            If Request.Item("TIPO") = "DOC" Then
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
                e.Row.Cells(10).HorizontalAlign = HorizontalAlign.Right
            End If

            Dim NumeroRegistrazione As Long

            NumeroRegistrazione = Val(e.Row.Cells(1).Text)

            e.Row.Cells(1).Text = "<label id=""lbl" & e.Row.Cells(1).Text & """ onmouseout=""deletebox('lbl" & e.Row.Cells(1).Text & "');"" onmouseover=""apribox(" & NumeroRegistrazione & ",'lbl" & e.Row.Cells(1).Text & "');"">" & e.Row.Cells(1).Text & "</label>"
        End If

  

    End Sub



    


    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Private Sub MettiInvisibileJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "



        MyJs = MyJs & " if ((appoggio.match('ImgHome')!= null) || (appoggio.match('ImgRicerca')!= null) || (appoggio.match('ImgTipo')!= null) || (appoggio.match('Btn_Esci')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
    End Sub


    Protected Sub Rb_DataRegistrazione_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_DataRegistrazione.CheckedChanged
        If Rb_DataRegistrazione.Checked = True Then
            Dim Appoggio As System.Web.UI.ImageClickEventArgs
            Call Btn_Ricerca_Click(sender, Appoggio)
        End If
    End Sub

    Protected Sub Rb_Id_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_Id.CheckedChanged
        If Rb_Id.Checked = True Then
            Dim Appoggio As System.Web.UI.ImageClickEventArgs
            Call Btn_Ricerca_Click(sender, Appoggio)
        End If
    End Sub

    Protected Sub DD_Tipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Tipo.SelectedIndexChanged
        Dim Appoggio As System.Web.UI.ImageClickEventArgs
        Call Btn_Ricerca_Click(sender, Appoggio)
    End Sub


    Protected Sub Img_Espandi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Espandi.Click
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim ConnectionString As String = Session("DC_TABELLE")


        Tabella = ViewState("RicercaGenerale")

        Dim MySql As String
        Dim EstraiCondizioniSQL As String


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()

        Dim cnTB As OleDbConnection

        cnTB = New Data.OleDb.OleDbConnection(ConnectionString)

        cnTB.Open()


        MySql = ""
        EstraiCondizioniSQL = ""

        If Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "MANREV" Then
            MySql = "Select * From CausaliContabiliTesta  "
        End If

        If Request.Item("TIPO") = "INC" Then
            MySql = "Select * From CausaliContabiliTesta Where TIPO = 'P'"
        End If
        If Request.Item("TIPO") = "INCS" Then
            MySql = "Select * From CausaliContabiliTesta Where TIPO = 'P'"
        End If
        If Request.Item("TIPO") = "DOC" Then
            MySql = "Select  * From CausaliContabiliTesta Where (TIPO = 'I'  OR TIPO = 'R')  "
        End If

        If MySql <> "" Then

            Dim cmdX As New OleDbCommand()

            cmdX.CommandText = MySql
            cmdX.Connection = cnTB
            Dim myPOSTreaderX As OleDbDataReader = cmdX.ExecuteReader()
            Do While myPOSTreaderX.Read
                If EstraiCondizioniSQL <> "" Then
                    EstraiCondizioniSQL = EstraiCondizioniSQL & " or "
                End If
                EstraiCondizioniSQL = EstraiCondizioniSQL & " CausaleContabile = '" & myPOSTreaderX.Item("CODICE") & "'"
            Loop
            myPOSTreaderX.Close()
        End If
        cnTB.Close()


        If Request.Item("TIPO") = "SMP" Then
            Dim ParametriGenerale As New Cls_ParametriGenerale

            ParametriGenerale.LeggiParametri(ConnectionStringGenerale)
            EstraiCondizioniSQL = ""
        End If


        Dim k1 As New Cls_SqlString

        k1.Add("Txt_DataAl", Txt_DataAl.Text)
        k1.Add("Txt_DataDal", Txt_DataDal.Text)
        k1.Add("Txt_RegistrazioneDal", Txt_RegistrazioneDal.Text)
        k1.Add("Txt_RegistrazioneAl", Txt_RegistrazioneAl.Text)
        k1.Add("Txt_Conto", Txt_Conto.Text)
        k1.Add("Rb_DataRegistrazione", Rb_DataRegistrazione.Checked)
        k1.Add("Rb_Id", Rb_Id.Checked)
        k1.Add("DD_Tipo", DD_Tipo.SelectedValue)
        k1.Add("TIPO", Request.Item("TIPO"))
        k1.Add("DD_CausaleContabile", DD_CausaleContabile.SelectedValue)

        k1.Add("DD_RegistroIva", DD_RegistroIva.SelectedValue)

        k1.Add("Txt_AnnoProtocollo", Txt_AnnoProtocollo.Text)
        k1.Add("Txt_NumeroProtocollo", Txt_NumeroProtocollo.Text)


        Session("RicercaGeneraleSQLString") = k1


        Dim cmd As New OleDbCommand()
        Dim Record As String
        If Txt_DataAl.Text = "" And Txt_DataDal.Text = "" And Txt_RegistrazioneDal.Text = "" And Txt_RegistrazioneAl.Text = "" And Txt_Conto.Text = "" Then
            Record = " *"
        Else
            Record = " *"
        End If

        If Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "MANREV" Then
            MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
        End If
        If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
            MySql = "Select " & Record & " From MovimentiContabiliTesta"
        End If

        If Request.Item("TIPO") = "DOC" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If
        If Request.Item("TIPO") = "INC" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If
        If Request.Item("TIPO") = "INCS" Then
            If EstraiCondizioniSQL = "" Then
                MySql = "Select " & Record & " From MovimentiContabiliTesta  "
            Else
                MySql = "Select " & Record & " From MovimentiContabiliTesta WHERE (" & EstraiCondizioniSQL & ") "
            End If
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAL As Date
            DataAL = Txt_DataAl.Text
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                MySql = MySql & " Where "
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataRegistrazione <= {ts '" & Format(DataAL, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date
            DataDal = Txt_DataDal.Text

            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If Val(Txt_AnnoProtocollo.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " AnnoProtocollo = " & Val(Txt_AnnoProtocollo.Text)
        End If
        If Val(Txt_NumeroProtocollo.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroProtocollo = " & Val(Txt_NumeroProtocollo.Text)
        End If

        If Val(Txt_RegistrazioneDal.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroRegistrazione >= " & Val(Txt_RegistrazioneDal.Text)
        End If

        If Val(Txt_RegistrazioneAl.Text) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " NumeroRegistrazione <= " & Val(Txt_RegistrazioneAl.Text)
        End If


        If DD_CausaleContabile.SelectedValue <> "" Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
        End If

        If Val(DD_RegistroIva.SelectedValue) > 0 Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If
            MySql = MySql & " RegistroIva = " & Val(DD_RegistroIva.SelectedValue)
        End If


        If Txt_Conto.Text <> "" Then
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "SMPPRN" Then
                If InStr(MySql, "Where") > 0 Then
                    MySql = MySql & " And "
                Else
                    MySql = MySql & " Where "
                End If
            Else
                MySql = MySql & " And "
            End If

            Dim SMastro As Long
            Dim SConto As Long
            Dim SSottoconto As Long


            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Conto.Text
            Vettore = SplitWords(APPOGGIO)

            Try
                SMastro = Vettore(0)
                SConto = Vettore(1)
                SSottoconto = Vettore(2)
            Catch ex As Exception

            End Try
            
            MySql = MySql & " (Select count(*) From MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & SMastro & " And ContoPartita = " & SConto & " And SottocontoPartita = " & SSottoconto & ") > 0"
        End If


        If Rb_Id.Checked = True Then
            MySql = MySql & "  Order by NumeroRegistrazione " & DD_Tipo.SelectedValue
        End If
        If Rb_DataRegistrazione.Checked = True Then
            MySql = MySql & "  Order by DataRegistrazione " & DD_Tipo.SelectedValue
        End If


        Dim IndiceRiga As Integer = 0

        cmd.CommandText = (MySql)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Not IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then

                IndiceRiga = IndiceRiga + 1

                If IndiceRiga > 5 + Session("UltimoNumero") Then
                    Exit Do
                End If
                If IndiceRiga >= Session("UltimoNumero") Then

                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = myPOSTreader.Item("NumeroRegistrazione")
                    If Val(campodb(myPOSTreader.Item("IdProgettoODV"))) > 0 Or Val(campodb(myPOSTreader.Item("IDODV"))) > 0 Or Val(campodb(myPOSTreader.Item("TipoODV"))) > 0 Then
                        myriga(0) = myPOSTreader.Item("NumeroRegistrazione") & "L"
                    End If


                    If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                        myriga(1) = ""
                    Else
                        myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
                    End If

                    If Request.Item("TIPO") = "DOC" Then
                        myriga(2) = myPOSTreader.Item("NumeroDocumento")
                        If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                            myriga(3) = ""
                        Else
                            myriga(3) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
                        End If
                        myriga(4) = myPOSTreader.Item("NumeroProtocollo")
                        myriga(5) = myPOSTreader.Item("AnnoProtocollo")

                        Dim dC As New Cls_CausaleContabile

                        dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
                        myriga(6) = dC.Descrizione

                        Dim XM As New Cls_MovimentoContabile

                        XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))



                        Dim PConti As New Cls_Pianodeiconti

                        PConti.Mastro = XM.Righe(0).MastroPartita
                        PConti.Conto = XM.Righe(0).ContoPartita
                        PConti.Sottoconto = XM.Righe(0).SottocontoPartita
                        PConti.Decodfica(Session("DC_GENERALE"))

                        myriga(7) = PConti.Descrizione

                        Dim AppoggioDescrizione As String = XM.Descrizione
                        If AppoggioDescrizione.Length > 100 Then
                            AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                        End If
                        myriga(8) = AppoggioDescrizione

                        myriga(9) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")
                    Else

                        If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then

                            Dim RdLegami As New Cls_Legami

                            myriga(2) = ""
                            RdLegami.Leggi(ConnectionStringGenerale, 0, Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))

                            Dim InDleg As Integer

                            For InDleg = 0 To 3
                                If RdLegami.NumeroDocumento(InDleg) > 0 Then
                                    Dim LeggiDoc As New Cls_MovimentoContabile

                                    LeggiDoc.Leggi(ConnectionStringGenerale, RdLegami.NumeroDocumento(InDleg))
                                    If LeggiDoc.NumeroDocumento = "" Then
                                        myriga(2) = myriga(3) & " Doc. n. " & LeggiDoc.NumeroProtocollo & " data " & LeggiDoc.DataDocumento
                                    Else
                                        myriga(2) = myriga(3) & " Doc. n. " & LeggiDoc.NumeroDocumento & " data " & LeggiDoc.DataDocumento
                                    End If
                                End If
                            Next
                            If Len(myriga(2)) > 50 Then
                                myriga(2) = Mid(myriga(2), 1, 49) & "..."
                            End If


                            Dim dC As New Cls_CausaleContabile

                            dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

                            myriga(3) = dC.Descrizione

                            Dim XM As New Cls_MovimentoContabile

                            XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))

                            Dim PConti As New Cls_Pianodeiconti

                            PConti.Mastro = XM.Righe(0).MastroPartita
                            PConti.Conto = XM.Righe(0).ContoPartita
                            PConti.Sottoconto = XM.Righe(0).SottocontoPartita
                            PConti.Decodfica(Session("DC_GENERALE"))

                            myriga(4) = PConti.Descrizione

                            Dim AppoggioDescrizione As String = XM.Descrizione
                            If AppoggioDescrizione.Length > 100 Then
                                AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                            End If
                            myriga(5) = AppoggioDescrizione
                            myriga(6) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")

                        Else
                            Dim dC As New Cls_CausaleContabile

                            dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

                            Dim AppoggioDescrizione As String = dC.Descrizione
                            If AppoggioDescrizione.Length > 100 Then
                                AppoggioDescrizione = Mid(AppoggioDescrizione, 1, 100)
                            End If
                            myriga(2) = AppoggioDescrizione

                            Dim XM As New Cls_MovimentoContabile

                            XM.Leggi(ConnectionStringGenerale, myPOSTreader.Item("NumeroRegistrazione"))

                            myriga(3) = XM.Descrizione
                            myriga(4) = Format(XM.ImportoRegistrazioneDocumento(ConnectionString), "#,##0.00")

                        End If
                    End If

                    Tabella.Rows.Add(myriga)
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        Session("UltimoNumero") = IndiceRiga
        ViewState("RicercaGenerale") = Tabella
        Grid.AutoGenerateColumns = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        Dim i As Long
        For i = 0 To Grid.Rows.Count - 1

            Grid.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Right
            If Request.Item("TIPO") = "SMP" Or Request.Item("TIPO") = "PRNT" Or Request.Item("TIPO") = "SMPPRN" Then
                Grid.Rows(i).Cells(5).HorizontalAlign = HorizontalAlign.Right
            End If
            If Request.Item("TIPO") = "INC" Or Request.Item("TIPO") = "INCS" Then
                Grid.Rows(i).Cells(6).HorizontalAlign = HorizontalAlign.Right
            End If
            If Request.Item("TIPO") = "DOC" Then
                Grid.Rows(i).Cells(5).HorizontalAlign = HorizontalAlign.Right
                Grid.Rows(i).Cells(6).HorizontalAlign = HorizontalAlign.Right
                Grid.Rows(i).Cells(10).HorizontalAlign = HorizontalAlign.Right
            End If
        Next
        Grid.DataBind()

        For i = 0 To Grid.Rows.Count - 1
            If Grid.Rows(i).Cells(1).Text.IndexOf("L") > 0 Then
                Dim k As Image = Grid.Rows(i).Cells(0).FindControl("Seleziona")
                k.ImageUrl = "../images/Lock.png"
            End If
            Grid.Rows(i).Cells(1).Text = Grid.Rows(i).Cells(1).Text.Replace("L", "")
        Next

        Call EseguiJS()
    End Sub
End Class


