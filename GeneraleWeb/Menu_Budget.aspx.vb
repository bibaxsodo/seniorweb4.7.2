﻿
Partial Class GeneraleWeb_Menu_Budget
    Inherits System.Web.UI.Page

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim DatiGenerale As New Cls_DatiGenerali

        DatiGenerale.LeggiDati(Session("DC_TABELLE"))


        Lbl_BudgetAnalitica.Text = "Budget"
        If DatiGenerale.BudgetAnalitica = 1 Then
            Lbl_BudgetAnalitica.Text = "Analitica"

            'gurppobudget
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "$(document).ready(function() {  $('#gurppobudgetlabel').css('visibility', 'hidden');  $('#gurppobudget').css('visibility', 'hidden'); });", True)
        End If
    End Sub
End Class
