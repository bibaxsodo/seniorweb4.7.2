﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_stamparegistriiva" CodeFile="stamparegistriiva.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Stampa Registri IVA</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'apertura del popup da parte del browser");
            }
        }
    </script>

    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }

        .style1 {
            width: 1114px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Stampe - Registro IVA</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Width="38px" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" ToolTip="Stampa dettagli" />
                            <asp:ImageButton ID="ImageButton2" runat="server" Width="38px" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" ToolTip="Stampa totali" />
                            <asp:ImageButton ID="ImageButton3" runat="server" Width="38px" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Conferma Registro" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Stampa Registro IVA" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Stampa Registro IVA
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" runat="server" AutoPostBack="true" Width="100px" onkeypress="return soloNumeri(event);" MaxLength="4"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Registro IVA :</label>
                                    <asp:DropDownList ID="DD_RegistroIVA" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                                    </br>
          
          
          <label class="LabelCampo">Periodo : </label>
                                    <asp:RadioButton ID="RB_Mensile" runat="server" GroupName="PERIODO" Checked="True" />Mensile
          <asp:RadioButton ID="RB_Trimestrale" runat="server" GroupName="PERIODO" />Trimestrale<br />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Ultima Stampa :</label><br />
                                    </br>
         <label class="LabelCampo">Pagina :</label>
                                    <asp:TextBox ID="Txt_PaginaU" Width="100px" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_AnnoU" Width="100px" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Mese :</label>
                                    <asp:DropDownList ID="DD_MeseU" runat="server">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Stampa Attuale:</label><br />
                                    <br />
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_AnnoA" Width="100px" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Mese :</label>
                                    <asp:DropDownList ID="DD_MeseA" Width="100px" runat="server">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Ult.Pagina Stampata :</label>
                                    <asp:TextBox ID="Txt_UltPaginaStampata" Width="100px" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Contabilità Semplificata:</label>
                                    <asp:CheckBox ID="Chk_ContabilitaSemplificata" runat="server" Width="19px" /><br />
                                    <br />
                                    <br />

                                    <asp:CheckBox ID="Chk_AggiungiPIVACF" runat="server" Width="19px" Checked Visible="false" /><br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server" Width="408px" ForeColor="Red"></asp:Label><br />
                                    <br />
                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                        <ProgressTemplate>
                                            <div id="blur">&nbsp;</div>
                                            <div id="Div1">&nbsp;</div>
                                            <div id="pippo" class="wait">
                                                <br />
                                                <img height="30px" src="images/loading.gif"><br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
