﻿
Partial Class GeneraleWeb_CreaMandatiReversali
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_CreaMandatiReversali_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBox(ConnectionString, Dd_CausaleContabile)
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        If Val(Txt_Numero.Text) = 0 Then
            Dim Xt As New Cls_CausaleContabile

            Xt.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Xt.Righe(0).DareAvere = "D" Then
                Dim SotCD As New Cls_Pianodeiconti

                SotCD.Mastro = Xt.Righe(0).Mastro
                SotCD.Conto = Xt.Righe(0).Conto
                SotCD.Sottoconto = Xt.Righe(0).Sottoconto
                SotCD.Decodfica(Session("DC_GENERALE"))
                Txt_SottocontoDare.Text = SotCD.Mastro & " " & SotCD.Conto & " " & SotCD.Sottoconto & " " & SotCD.Descrizione

                Dim SotCA As New Cls_Pianodeiconti

                SotCA.Mastro = Xt.Righe(1).Mastro
                SotCA.Conto = Xt.Righe(1).Conto
                SotCA.Sottoconto = Xt.Righe(1).Sottoconto
                SotCA.Decodfica(Session("DC_GENERALE"))
                Txt_SottocontoAvere.Text = SotCA.Mastro & " " & SotCA.Conto & " " & SotCA.Sottoconto & " " & SotCA.Descrizione
            End If
            If Xt.Righe(0).DareAvere = "A" Then
                Dim SotCD As New Cls_Pianodeiconti

                SotCD.Mastro = Xt.Righe(1).Mastro
                SotCD.Conto = Xt.Righe(1).Conto
                SotCD.Sottoconto = Xt.Righe(1).Sottoconto
                SotCD.Decodfica(Session("DC_GENERALE"))
                Txt_SottocontoDare.Text = SotCD.Mastro & " " & SotCD.Conto & " " & SotCD.Sottoconto & " " & SotCD.Descrizione

                Dim SotCA As New Cls_Pianodeiconti

                SotCA.Mastro = Xt.Righe(0).Mastro
                SotCA.Conto = Xt.Righe(0).Conto
                SotCA.Sottoconto = Xt.Righe(0).Sottoconto
                SotCA.Decodfica(Session("DC_GENERALE"))
                Txt_SottocontoAvere.Text = SotCA.Mastro & " " & SotCA.Conto & " " & SotCA.Sottoconto & " " & SotCA.Descrizione
            End If
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare data di registrazione</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Importo.Text.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Importo</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_SottocontoDare.Text.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Dare</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_SottocontoAvere.Text.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Avere</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If




        If CDbl(Txt_Importo.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Importo</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim XPar As New Cls_ParametriGenerale


        XPar.LeggiParametri(Session("DC_GENERALE"))

        Dim Appoggio(1000) As String
        Dim Indice As Integer = 0
        Dim Mandato As Boolean = False
        Dim Reversale As Boolean = False


        If Not IsNothing(XPar.CausaliMandati) Then
            Appoggio = SplitWords(XPar.CausaliMandati)

            For Indice = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(Indice)) Then

                    If Appoggio(Indice) = Dd_CausaleContabile.SelectedValue Then
                        Mandato = True
                    End If
                End If
            Next
        End If
        For Indice = 0 To Appoggio.Length - 1 : Appoggio(Indice) = "" : Next

        If Not IsNothing(XPar.CausaliReversali) Then
            Appoggio = SplitWords(XPar.CausaliReversali)

            For Indice = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(Indice)) Then

                    If Appoggio(Indice) = Dd_CausaleContabile.SelectedValue Then
                        Reversale = True
                    End If
                End If
            Next
        End If
        If Mandato = False And Reversale = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Causale Non Mandato O Reversale</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Già creato il giornale bollato</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        Dim xMastro As Long
        Dim xConto As Long
        Dim xSottoconto As Long
        Dim xVettore(100) As String
        xVettore = SplitWords(Txt_SottocontoDare.Text)


        If xVettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Dare</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        xMastro = Val(xVettore(0))
        xConto = Val(xVettore(1))
        xSottoconto = Val(xVettore(2))

        If xMastro = 0 Or xConto = 0 Or xSottoconto = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Dare</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim k As New Cls_Pianodeiconti

        k.Mastro = xMastro
        k.Conto = xConto
        k.Sottoconto = xSottoconto
        k.Descrizione = ""
        k.Decodfica(Session("DC_GENERALE"))
        If k.Descrizione = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Dare</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        xVettore = SplitWords(Txt_SottocontoAvere.Text)

        If xVettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Avere</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        xMastro = Val(xVettore(0))
        xConto = Val(xVettore(1))
        xSottoconto = Val(xVettore(2))

        If xMastro = 0 Or xConto = 0 Or xSottoconto = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Avere</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        k.Mastro = xMastro
        k.Conto = xConto
        k.Sottoconto = xSottoconto
        k.Descrizione = ""
        k.Decodfica(Session("DC_GENERALE"))
        If k.Descrizione = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto Avere</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Response.Redirect("CreaMandatiReversaliStep_1.aspx?CausaleContabile=" & Dd_CausaleContabile.SelectedValue & "&NumeroRegistrazione=" & Val(Txt_Numero.Text) & "&DataRegistrazione=" & Txt_DataRegistrazione.Text & "&Descrizione=" & Txt_Descrizione.Text & "&SottocontoDare=" & Txt_SottocontoDare.Text & "&SottocontoAvere=" & Txt_SottocontoAvere.Text & "&Importo=" & Txt_Importo.Text)

    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("UltimiMovimenti.aspx?TIPO=MANREV")
    End Sub
End Class
