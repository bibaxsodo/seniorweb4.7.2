﻿Imports System.Data.OleDb

Partial Class GeneraleWeb_StampaDistinte
    Inherits System.Web.UI.Page

    Private Function CalcolaDistintePrecedenti(ByVal tipo As String, ByVal numdist As Long, ByVal AnnoDist As Long) As Double
        Dim cn As OleDbConnection


        Dim NumeroRegistrazione As Double
        Dim Importo As Double
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        If tipo = "MAN" Then
            CalcolaDistintePrecedenti = 0


            MySql = "Select NumeroRegistrazione  From MovimentiContabiliTesta,MovimentiContabiliRiga  Where  (TipoStorno Is Null OR TipoStorno = '')  And  DareAvere = 'D' And NumeroRegistrazione = Numero And NumeroMandato > 0 And NumeroDistinta <> 0 And NumeroDistinta < " & numdist & "  And year(DataMandato) = " & AnnoDist & " GROUP BY NumeroRegistrazione"
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn
            Dim MyRd As OleDbDataReader = cmd.ExecuteReader()

            Do While MyRd.Read
                NumeroRegistrazione = campodbN(MyRd.Item("NumeroRegistrazione"))
                Dim KDoc As New Cls_MovimentoContabile

                KDoc.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)
                Importo = KDoc.ImportoDocumento(NumeroRegistrazione)
                If Importo = 0 Then
                    For i = 0 To KDoc.Righe.Length - 1
                        If Not IsNothing(KDoc.Righe(i)) Then
                            If KDoc.Righe(i).RigaDaCausale = 1 Then
                                Importo = KDoc.Righe(i).Importo
                            End If
                        End If
                    Next
                End If


                If Importo = 0 Then
                    Importo = KDoc.ImportoRegistrazioneDocumento(Session("DC_GENERALE"))
                End If
                CalcolaDistintePrecedenti = CalcolaDistintePrecedenti + Importo                
            Loop
            MyRd.Close()

            MySql = "Select NumeroRegistrazione  From MovimentiContabiliTesta,MovimentiContabiliRiga  Where  (TipoStorno = 'S')  And  DareAvere = 'D' And NumeroRegistrazione = Numero And NumeroMandato > 0 And NumeroDistinta <> 0 And NumeroDistinta < " & numdist & "  And year(DataMandato) = " & AnnoDist & " GROUP BY NumeroRegistrazione"
            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = MySql
            cmd1.Connection = cn
            Dim MyRd1 As OleDbDataReader = cmd1.ExecuteReader()
            Do While MyRd1.Read
                NumeroRegistrazione = campodb(MyRd1.Item("NumeroRegistrazione"))
                Dim KDoc As New Cls_MovimentoContabile

                KDoc.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

                Importo = KDoc.ImportoDocumento(NumeroRegistrazione)
                If Importo = 0 Then
                    For i = 0 To KDoc.Righe.Length - 1
                        If Not IsNothing(KDoc.Righe(i)) Then
                            If KDoc.Righe(i).RigaDaCausale = 1 Then
                                Importo = KDoc.Righe(i).Importo
                            End If
                        End If
                    Next
                End If
                If Importo = 0 Then
                    Importo = KDoc.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))
                End If
                CalcolaDistintePrecedenti = CalcolaDistintePrecedenti - Importo                
            Loop
            MyRd1.Close()

        End If

        If tipo = "REV" Then
            CalcolaDistintePrecedenti = 0
            MySql = "Select NumeroRegistrazione  From MovimentiContabiliTesta,MovimentiContabiliRiga  Where (TipoStorno Is Null OR TipoStorno = '')  And   DareAvere = 'D' And NumeroRegistrazione = Numero And NumeroDistinta <> 0 And NumeroReversale > 0 And NumeroDistinta < " & numdist & " And year(DataReversale) = " & AnnoDist & "  GROUP BY NumeroRegistrazione"
            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = MySql
            cmdRd.Connection = cn
            Dim MyRdR As OleDbDataReader = cmdRd.ExecuteReader()

            Do While MyRdR.Read
                NumeroRegistrazione = campodbN(MyRdR.Item("NumeroRegistrazione"))

                Dim KDoc As New Cls_MovimentoContabile

                KDoc.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

                Importo = KDoc.ImportoDocumento(Session("DC_TABELLE"))
                If Importo = 0 Then
                    For i = 0 To KDoc.Righe.Length - 1
                        If Not IsNothing(KDoc.Righe(i)) Then
                            If KDoc.Righe(i).RigaDaCausale = 1 Then
                                Importo = KDoc.Righe(i).Importo
                            End If
                        End If
                    Next                    
                End If
                If Importo = 0 Then
                    Importo = KDoc.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))
                End If
                CalcolaDistintePrecedenti = CalcolaDistintePrecedenti + Importo                
            Loop
            MyRdR.Close()

            MySql = "Select NumeroRegistrazione  From MovimentiContabiliTesta,MovimentiContabiliRiga  Where TipoStorno = 'S' And DareAvere = 'D' And NumeroRegistrazione = Numero And NumeroDistinta <> 0 And NumeroReversale > 0 And NumeroDistinta < " & numdist & " And year(DataReversale) = " & AnnoDist & "  GROUP BY NumeroRegistrazione"
            Dim cmdRd1 As New OleDbCommand()
            cmdRd1.CommandText = MySql
            cmdRd1.Connection = cn
            Dim MyRdR1 As OleDbDataReader = cmdRd1.ExecuteReader()
            Do While MyRdR1.Read
                NumeroRegistrazione = campodb(MyRdR1.Item("NumeroRegistrazione"))
                Dim KDoc As New Cls_MovimentoContabile

                KDoc.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

                Importo = KDoc.ImportoDocumento(Session("DC_TABELLE"))
                If Importo = 0 Then
                    For i = 0 To KDoc.Righe.Length - 1
                        If Not IsNothing(KDoc.Righe(i)) Then
                            If KDoc.Righe(i).RigaDaCausale = 1 Then
                                Importo = KDoc.Righe(i).Importo
                            End If
                        End If
                    Next
                End If
                If Importo = 0 Then
                    Importo = KDoc.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))
                End If
                CalcolaDistintePrecedenti = CalcolaDistintePrecedenti - Importo                
            Loop
            MyRdR1.Close()
        End If

        cn.Close()
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Aggiorna_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Aggiorna.Click
        Dim cn As OleDbConnection

        Dim MySql As String
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        Chk_Reversali.Items.Clear()
        If Val(Txt_Numero.Text) = 0 Then
            MySql = "Select * From MovimentiContabiliTesta Where NumeroReversale > 0 And (TipoStorno Is Null or TipoStorno = '') And (NumeroDistinta = 0  or NumeroDistinta Is Null) And year(DataReversale) = " & Txt_Anno.Text & " Order by NumeroReversale"
        Else
            MySql = "Select * From MovimentiContabiliTesta Where NumeroReversale > 0 And (TipoStorno Is Null or TipoStorno = '') And NumeroDistinta = " & Val(Txt_Numero.Text) & " And year(DataReversale) = " & Txt_Anno.Text & " Order by NumeroReversale"
        End If
        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim ReadRs As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadRs.Read
            Chk_Reversali.Items.Add(campodb(ReadRs.Item("NumeroReversale")) & " " & campodb(ReadRs.Item("DataReversale")))
        Loop
        ReadRs.Close()

        Chk_Mandati.Items.Clear()
        If Val(Txt_Numero.Text) = 0 Then
            MySql = "Select * From MovimentiContabiliTesta Where NumeroMandato > 0 And (TipoStorno Is Null or TipoStorno = '') And (NumeroDistinta = 0  or NumeroDistinta Is Null) And year(DataMandato) = " & Txt_Anno.Text & " Order by NumeroMandato"
        Else
            MySql = "Select * From MovimentiContabiliTesta Where NumeroMandato > 0 And (TipoStorno Is Null or TipoStorno = '') And NumeroDistinta = " & Val(Txt_Numero.Text) & " And year(DataMandato) = " & Txt_Anno.Text & " Order by NumeroMandato"
        End If

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Connection = cn
        Dim ReadRs1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While ReadRs1.Read
            Chk_Mandati.Items.Add(campodb(ReadRs1.Item("NumeroMandato")) & " " & campodb(ReadRs1.Item("DataMandato")))
        Loop
        ReadRs.Close()


        Chk_StornoReversali.Items.Clear()
        If Val(Txt_Numero.Text) = 0 Then
            MySql = "Select * From MovimentiContabiliTesta Where NumeroReversale > 0 And TipoStorno = 'S' And (NumeroDistinta = 0  or NumeroDistinta Is Null) And year(DataReversale) = " & Txt_Anno.Text
        Else
            MySql = "Select * From MovimentiContabiliTesta Where NumeroReversale > 0 And TipoStorno = 'S' And NumeroDistinta = " & Val(Txt_Numero.Text) & " And year(DataReversale) = " & Txt_Anno.Text
        End If
        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = MySql
        cmd2.Connection = cn
        Dim ReadRs2 As OleDbDataReader = cmd2.ExecuteReader()
        Do While ReadRs2.Read
            Chk_StornoReversali.Items.Add(campodb(ReadRs2.Item("NumeroReversale")) & " " & campodb(ReadRs2.Item("DataReversale")))
        Loop
        ReadRs.Close()


        Chk_StornoMandati.Items.Clear()
        If Val(Txt_Numero.Text) = 0 Then
            MySql = "Select * From MovimentiContabiliTesta Where NumeroMandato > 0 And TipoStorno = 'S' And (NumeroDistinta = 0  or NumeroDistinta Is Null) And year(DataMandato) = " & Txt_Anno.Text
        Else
            MySql = "Select * From MovimentiContabiliTesta Where NumeroMandato > 0 And TipoStorno = 'S' And NumeroDistinta = " & Val(Txt_Numero.Text) & " And year(DataMandato) = " & Txt_Anno.Text
        End If
        Dim cmd3 As New OleDbCommand()
        cmd3.CommandText = MySql
        cmd3.Connection = cn
        Dim ReadRs3 As OleDbDataReader = cmd3.ExecuteReader()
        Do While ReadRs3.Read
            Chk_StornoReversali.Items.Add(campodb(ReadRs3.Item("NumeroMandato")) & " " & campodb(ReadRs3.Item("DataMandato")))
        Loop
        ReadRs.Close()


        If Val(Txt_Numero.Text) > 0 Then
            Dim I As Integer

            For I = 0 To Chk_Mandati.Items.Count - 1
                Chk_Mandati.Items(I).Selected = True
                Chk_Mandati.Items(I).Selected = True
            Next
            For I = 0 To Chk_Reversali.Items.Count - 1
                Chk_Reversali.Items(I).Selected = True
                Chk_Reversali.Items(I).Selected = True
            Next
            For I = 0 To Chk_StornoReversali.Items.Count - 1
                Chk_StornoReversali.Items(I).Selected = True
                Chk_StornoReversali.Items(I).Selected = True
            Next
            For I = 0 To Chk_StornoMandati.Items.Count - 1
                Chk_StornoMandati.Items(I).Selected = True
                Chk_StornoMandati.Items(I).Selected = True
            Next            
        End If

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_DataDistinta')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call EseguiJS()
    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        Dim cnStampa As OleDbConnection
        Dim cnGenerale As OleDbConnection
        Dim ImpDPrima As Double
        Dim PosizioneSpazio As Integer
        Dim NumeroReversale As Integer
        Dim DataReversale As Date

        Dim NumeroRegistrazione As Integer
        Dim Debitore As String
        Dim Importo As Double
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim NumeroMandato As Long
        Dim DataMandato As Date


        If Not IsDate(Txt_DataDistinta.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare causale contabile</center>');", True)
            EseguiJS()
            Exit Sub
        End If


        cnStampa = New Data.OleDb.OleDbConnection(Session("STAMPEFINANZIARIA"))
        cnGenerale = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        cnStampa.Open()
        cnGenerale.Open()

        Dim cmdDelStampa As New OleDbCommand()
        cmdDelStampa.CommandText = "Delete From Distinta"
        cmdDelStampa.Connection = cnStampa
        cmdDelStampa.ExecuteNonQuery()


        REM cnGenerale.BeginTransaction()

        If Val(Txt_Numero.Text) > 0 Then
            Dim cmdUPGenerale As New OleDbCommand()
            cmdUPGenerale.CommandText = "UPDATE MovimentiContabiliTesta SET MovimentiContabiliTesta.DataDistinta = Null, MovimentiContabiliTesta.NumeroDistinta = 0 WHERE year(DataDistinta) = " & Year(Txt_DataDistinta.Text) & "  AND NumeroDistinta = " & Val(Txt_Numero.Text)
            cmdUPGenerale.Connection = cnGenerale
            cmdUPGenerale.ExecuteNonQuery()
        End If


        Txt_Anno.Enabled = False
        Txt_Numero.Enabled = False
        If Val(Txt_Numero.Text) = 0 Then
            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = "Select max(NumeroDistinta ) as num From MovimentiContabiliTesta Where year(DataDistinta) = " & Txt_Anno.Text
            cmd2.Connection = cnGenerale            
            Dim ReadRs2 As OleDbDataReader = cmd2.ExecuteReader()
            If ReadRs2.Read Then
                Txt_Numero.Text = campodbN(ReadRs2.Item("Num")) + 1
            End If
            ReadRs2.Close()
        End If

        ImpDPrima = 0
        For I = 0 To Chk_Reversali.Items.Count - 1
            If Chk_Reversali.Items(I).Selected = True Then
                PosizioneSpazio = InStr(1, Chk_Reversali.Items(I).Text, " ")
                If ImpDPrima = 0 Then
                    ImpDPrima = CalcolaDistintePrecedenti("REV", Txt_Numero.Text, Txt_Anno.Text)
                End If
                NumeroReversale = 0
                DataReversale = Nothing
                If PosizioneSpazio > 0 Then
                    NumeroReversale = Mid(Chk_Reversali.Items(I).Text, 1, PosizioneSpazio)
                    DataReversale = Mid(Chk_Reversali.Items(I).Text, PosizioneSpazio, Len(Chk_Reversali.Items(I).Text) - PosizioneSpazio + 1)
                End If

                NumeroRegistrazione = 0

                Dim cmdNum As New OleDbCommand()
                cmdNum.CommandText = "Select * FRom  MovimentiContabiliTesta Where (TipoStorno Is Null Or TipoStorno = '') And  year(DataReversale) = " & Year(DataReversale) & " And NumeroReversale = " & NumeroReversale
                cmdNum.Connection = cnGenerale

                Dim ReadNum As OleDbDataReader = cmdNum.ExecuteReader()
                If ReadNum.Read() Then
                    NumeroRegistrazione = campodbN(ReadNum.Item("NumeroRegistrazione"))
                End If
                ReadNum.Close()

                Dim cmd2 As New OleDbCommand()
                cmd2.CommandText = "UPDATE MovimentiContabiliTesta set NumeroDistinta =?,DataDistinta = ?  Where (TipoStorno Is Null Or TipoStorno = '') And  year(DataReversale) = " & Year(DataReversale) & " And NumeroReversale = " & NumeroReversale
                cmd2.Connection = cnGenerale

                cmd2.Parameters.AddWithValue("@NumeroDistinta", Txt_Numero.Text)
                cmd2.Parameters.AddWithValue("@DataDistinta", Txt_DataDistinta.Text)
                cmd2.ExecuteNonQuery()



                Mastro = 0
                Conto = 0
                Sottoconto = 0                
                Dim cmd3 As New OleDbCommand()
                cmd3.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.Tipo = 'CF' AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                cmd3.Connection = cnGenerale
                Dim ReadRs3 As OleDbDataReader = cmd3.ExecuteReader()
                If ReadRs3.Read Then
                    Mastro = campodbN(ReadRs3.Item("MastroPartita"))
                    Conto = campodbN(ReadRs3.Item("ContoPartita"))
                    Sottoconto = campodbN(ReadRs3.Item("SottocontoPartita"))
                End If
                ReadRs3.Close()


                If Mastro = 0 And Conto = 0 And Sottoconto = 0 Then                    
                    Dim cmd5 As New OleDbCommand()
                    cmd5.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.RigaDaCausale =1  AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                    cmd5.Connection = cnGenerale
                    Dim ReadRs5 As OleDbDataReader = cmd5.ExecuteReader()
                    If ReadRs5.Read Then
                        Mastro = campodbN(ReadRs5.Item("MastroPartita"))
                        Conto = campodbN(ReadRs5.Item("ContoPartita"))
                        Sottoconto = campodbN(ReadRs5.Item("SottocontoPartita"))
                    End If
                    ReadRs5.Close()
                End If


                Dim XPC As New Cls_Pianodeiconti
                XPC.Mastro = Mastro
                XPC.Conto = Conto
                XPC.Sottoconto = Sottoconto
                XPC.Decodfica(Session("DC_GENERALE"))
                Debitore = XPC.Descrizione
                'MyRs.Open("Select * From AnagraficaComune where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottoContoFornitore = " & Sottoconto, OspitiDb, adOpenKeyset, adLockOptimistic)
                'If Not MyRs.EOF Then
                'Debitore = MoveFromDb(MyRs!Nome)
                'End  If
                'MyRs.Close()

                'Importo = ImportoDocumento(NumeroRegistrazione)

                Dim cmd4 As New OleDbCommand()
                cmd4.CommandText = "Select Importo From MovimenticontabiliRiga where  MastroPartita = 6 And ContoPartita = 30 And Numero = " & NumeroRegistrazione
                cmd4.Connection = cnGenerale
                Dim ReadRs4 As OleDbDataReader = cmd4.ExecuteReader()
                If ReadRs4.Read Then
                    Importo = campodbN(ReadRs4.Item("Importo"))
                End If
                ReadRs4.Close()

                
                If Importo = 0 Then
                    Dim cmd5 As New OleDbCommand()
                    cmd5.CommandText = "Select Importo From MovimentiContabiliRiga Where RigaDaCausale = 1 And Numero = " & NumeroRegistrazione
                    cmd5.Connection = cnGenerale
                    Dim ReadRs5 As OleDbDataReader = cmd5.ExecuteReader()
                    If ReadRs5.Read Then
                        Importo = campodbN(ReadRs5.Item("Importo"))
                    End If
                    ReadRs5.Close()
                End If
                If Importo = 0 Then

                    Dim cmd6 As New OleDbCommand()
                    cmd6.CommandText = "Select SUM(Importo) AS IMPORTO From MovimentiContabiliRiga Where DareAvere= 'D' And Numero = " & NumeroRegistrazione
                    cmd6.Connection = cnGenerale
                    Dim ReadRs6 As OleDbDataReader = cmd6.ExecuteReader()
                    If ReadRs6.Read Then
                        Importo = campodbN(ReadRs6.Item("Importo"))
                    End If
                    ReadRs6.Close()
                End If

                Dim cmdIns As New OleDbCommand()

                Dim DatiSocieta As New Cls_DecodificaSocieta



                cmdIns.CommandText = "INSERT INTO DISTINTA (tipo,Anno,numero,DATAEMISSIONE,Debitore,Importo,TDistintaCompetenza,AnnoDistinta,NumeroDistinta,DataDistinta,IntestaSocieta,TPrecedentiCompetenza,TMandatiPrecedenti,TReversaliPrecedenti, PRINTERKEY) VALUES " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  "

                cmdIns.Connection = cnStampa
                cmdIns.Parameters.AddWithValue("@Tipo", "REV")
                cmdIns.Parameters.AddWithValue("@Anno", Year(DataReversale))

                cmdIns.Parameters.AddWithValue("@numero", NumeroReversale)
                cmdIns.Parameters.AddWithValue("@DATAEMISSIONE", DataReversale)
                cmdIns.Parameters.AddWithValue("@Debitore", Debitore)
                cmdIns.Parameters.AddWithValue("@Importo", Importo)
                cmdIns.Parameters.AddWithValue("@TDistintaCompetenza", Importo)
                cmdIns.Parameters.AddWithValue("@AnnoDistinta", Year(Txt_DataDistinta.Text))
                cmdIns.Parameters.AddWithValue("@NumeroDistinta", Txt_Numero.Text)
                cmdIns.Parameters.AddWithValue("@DataDistinta", Txt_DataDistinta.Text)
                cmdIns.Parameters.AddWithValue("@IntestaSocieta", DatiSocieta.DecodificaSocieta(Session("DC_TABELLE")))
                cmdIns.Parameters.AddWithValue("@TPrecedentiCompetenza", ImpDPrima)
                cmdIns.Parameters.AddWithValue("@TMandatiPrecedenti", CalcolaDistintePrecedenti("MAN", Txt_Numero.Text, Txt_Anno.Text))
                cmdIns.Parameters.AddWithValue("@TReversaliPrecedenti", CalcolaDistintePrecedenti("REV", Txt_Numero.Text, Txt_Anno.Text))
                cmdIns.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))
                cmdIns.ExecuteNonQuery()                
            End If
        Next

        ImpDPrima = 0

        For I = 0 To Chk_Mandati.Items.Count - 1
            If Chk_Mandati.Items(I).Selected = True Then
                PosizioneSpazio = InStr(1, Chk_Mandati.Items(I).Text, " ")
                If ImpDPrima = 0 Then
                    ImpDPrima = CalcolaDistintePrecedenti("MAN", Txt_Numero.Text, Txt_Anno.Text)
                End If
                NumeroMandato = 0
                DataMandato = Nothing
                If PosizioneSpazio > 0 Then
                    NumeroMandato = Mid(Chk_Mandati.Items(I).Text, 1, PosizioneSpazio)
                    DataMandato = Mid(Chk_Mandati.Items(I).Text, PosizioneSpazio, Len(Chk_Mandati.Items(I).Text) - PosizioneSpazio + 1)
                End If

                NumeroRegistrazione = 0
                Dim cmdNum As New OleDbCommand()
                cmdNum.CommandText = "Select * FRom  MovimentiContabiliTesta Where (TipoStorno Is Null Or TipoStorno = '') And year(DataMandato) = " & Year(DataMandato) & " And NumeroMandato = " & NumeroMandato
                cmdNum.Connection = cnGenerale
                
                Dim ReadNum As OleDbDataReader = cmdNum.ExecuteReader()
                If ReadNum.Read() Then
                    NumeroRegistrazione = campodbN(ReadNum.Item("NumeroRegistrazione"))
                End If
                ReadNum.Close()


                Dim cmd2 As New OleDbCommand()
                cmd2.CommandText = "UPDATE MovimentiContabiliTesta set NumeroDistinta =?,DataDistinta = ?  Where (TipoStorno Is Null Or TipoStorno = '') And year(DataMandato) = " & Year(DataMandato) & " And NumeroMandato = " & NumeroMandato
                cmd2.Connection = cnGenerale

                cmd2.Parameters.AddWithValue("@NumeroDistinta", Txt_Numero.Text)
                cmd2.Parameters.AddWithValue("@DataDistinta", Txt_DataDistinta.Text)
                cmd2.ExecuteNonQuery()



                Mastro = 0
                Conto = 0
                Sottoconto = 0
                Dim cmd3 As New OleDbCommand()
                cmd3.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.Tipo = 'CF' AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                cmd3.Connection = cnGenerale
                Dim ReadRs3 As OleDbDataReader = cmd3.ExecuteReader()
                If ReadRs3.Read Then
                    Mastro = campodbN(ReadRs3.Item("MastroPartita"))
                    Conto = campodbN(ReadRs3.Item("ContoPartita"))
                    Sottoconto = campodbN(ReadRs3.Item("SottocontoPartita"))
                End If
                ReadRs3.Close()

                If Mastro = 0 And Conto = 0 And Sottoconto = 0 Then                    
                    Dim cmd6 As New OleDbCommand()
                    cmd6.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.RigaDaCausale =1  AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                    cmd6.Connection = cnGenerale
                    Dim ReadRs6 As OleDbDataReader = cmd6.ExecuteReader()
                    If ReadRs6.Read Then
                        Mastro = campodbN(ReadRs6.Item("MastroPartita"))
                        Conto = campodbN(ReadRs6.Item("ContoPartita"))
                        Sottoconto = campodbN(ReadRs6.Item("SottocontoPartita"))
                    End If
                    ReadRs6.Close()
                End If

                Dim DecConto As New Cls_Pianodeiconti

                DecConto.Mastro = Mastro
                DecConto.Conto = Conto
                DecConto.Sottoconto = Sottoconto
                DecConto.Decodfica(Session("DC_GENERALE"))

                Debitore = DecConto.Descrizione
                'MyRs.Open("Select * From AnagraficaComune where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottoContoFornitore = " & Sottoconto, OspitiDb, adOpenKeyset, adLockOptimistic)
                'If Not MyRs.EOF Then
                'Debitore = MoveFromDb(MyRs!Nome)
                'End If
                'MyRs.Close()

                Dim K As New Cls_MovimentoContabile

                K.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

                Importo = K.ImportoDocumento(Session("DC_TABELLE"))
                If Importo = 0 Then
                    Dim cmd5 As New OleDbCommand()
                    cmd5.CommandText = "Select Importo From MovimentiContabiliRiga Where RigaDaCausale = 1 And Numero = " & NumeroRegistrazione
                    cmd5.Connection = cnGenerale
                    Dim ReadRs5 As OleDbDataReader = cmd5.ExecuteReader()
                    If ReadRs5.Read Then
                        Importo = campodbN(ReadRs5.Item("Importo"))
                    End If
                    ReadRs5.Close()
                End If
                If Importo = 0 Then
                    Dim cmd5 As New OleDbCommand()
                    cmd5.CommandText = "Select Importo From MovimentiContabiliRiga Where DareAvere = 'D' And Numero = " & NumeroRegistrazione
                    cmd5.Connection = cnGenerale
                    Dim ReadRs5 As OleDbDataReader = cmd5.ExecuteReader()
                    If ReadRs5.Read Then
                        Importo = campodbN(ReadRs5.Item("Importo"))
                    End If
                    ReadRs5.Close()                    
                End If

                Dim cmdIns As New OleDbCommand()

                Dim DatiSocieta As New Cls_DecodificaSocieta


                cmdIns.CommandText = "INSERT INTO DISTINTA (tipo,Anno,numero,DATAEMISSIONE,Debitore,Importo,TDistintaCompetenza,AnnoDistinta,NumeroDistinta,DataDistinta,IntestaSocieta,TPrecedentiCompetenza,TMandatiPrecedenti,TReversaliPrecedenti, PRINTERKEY) VALUES " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  "

                cmdIns.Connection = cnStampa
                cmdIns.Parameters.AddWithValue("@Tipo", "MAN")
                cmdIns.Parameters.AddWithValue("@Anno", Year(DataMandato))

                cmdIns.Parameters.AddWithValue("@numero", NumeroMandato)
                cmdIns.Parameters.AddWithValue("@DATAEMISSIONE", DataMandato)
                cmdIns.Parameters.AddWithValue("@Debitore", Debitore)
                cmdIns.Parameters.AddWithValue("@Importo", Importo)
                cmdIns.Parameters.AddWithValue("@TDistintaCompetenza", Importo)
                cmdIns.Parameters.AddWithValue("@AnnoDistinta", Year(Txt_DataDistinta.Text))
                cmdIns.Parameters.AddWithValue("@NumeroDistinta", Txt_Numero.Text)
                cmdIns.Parameters.AddWithValue("@DataDistinta", Txt_DataDistinta.Text)
                cmdIns.Parameters.AddWithValue("@IntestaSocieta", DatiSocieta.DecodificaSocieta(Session("DC_TABELLE")))
                cmdIns.Parameters.AddWithValue("@TPrecedentiCompetenza", ImpDPrima)
                cmdIns.Parameters.AddWithValue("@TMandatiPrecedenti", CalcolaDistintePrecedenti("MAN", Txt_Numero.Text, Txt_Anno.Text))
                cmdIns.Parameters.AddWithValue("@TReversaliPrecedenti", CalcolaDistintePrecedenti("REV", Txt_Numero.Text, Txt_Anno.Text))
                cmdIns.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))
                cmdIns.ExecuteNonQuery()

            End If
        Next

        'ImpDPrima = 0
        'For I = 0 To Lst_StornoReversali.ListCount - 1
        '    If Lst_StornoReversali.Selected(I) = True Then
        '        PosizioneSpazio = InStr(1, Lst_StornoReversali.List(I), " ")
        '        If ImpDPrima = 0 Then
        '            ImpDPrima = CalcolaDistintePrecedenti("REV", Txt_Numero.Text, Txt_Anno.Text)
        '        End If

        '        NumeroReversale = 0
        '        DataReversale = 0
        '        If PosizioneSpazio > 0 Then
        '            NumeroReversale = Mid(Lst_StornoReversali.List(I), 1, PosizioneSpazio)
        '            DataReversale = Mid(Lst_StornoReversali.List(I), PosizioneSpazio, Len(Lst_StornoReversali.List(I)) - PosizioneSpazio + 1)
        '        End If

        '        NumeroRegistrazione = 0
        '        MyRs.Open("Select * From MovimentiContabiliTesta Where TipoStorno = 'S' And  year(DataReversale) = " & Year(DataReversale) & " And NumeroReversale = " & NumeroReversale, GeneraleDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            NumeroRegistrazione = MoveFromDb(MyRs!NumeroRegistrazione)
        '            MoveToDb(MyRs!NumeroDistinta, Txt_Numero.Text)
        '            MoveToDb(MyRs!DataDistinta, Txt_DataDistinta.Text)
        '            MyRs.Update()
        '        End If
        '        MyRs.Close()

        '        Mastro = 0
        '        Conto = 0
        '        Sottoconto = 0
        '        MyRs.Open("SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.Tipo = 'CF' AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " ", GeneraleDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            Mastro = MoveFromDb(MyRs!MastroPartita)
        '            Conto = MoveFromDb(MyRs!ContoPartita)
        '            Sottoconto = MoveFromDb(MyRs!SottocontoPartita)
        '        End If
        '        MyRs.Close()

        '        If Mastro = 0 And Conto = 0 And Sottoconto = 0 Then
        '            MyRs.Open("SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.RigaDaCausale =1  AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " ", GeneraleDb, adOpenKeyset, adLockOptimistic)
        '            If Not MyRs.EOF Then
        '                Mastro = MoveFromDb(MyRs!MastroPartita)
        '                Conto = MoveFromDb(MyRs!ContoPartita)
        '                Sottoconto = MoveFromDb(MyRs!SottocontoPartita)
        '            End If
        '            MyRs.Close()
        '        End If

        '        Debitore = DecodificaConto(Mastro, Conto, Sottoconto)
        '        MyRs.Open("Select * From AnagraficaComune where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottoContoFornitore = " & Sottoconto, OspitiDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            Debitore = MoveFromDb(MyRs!Nome)
        '        End If
        '        MyRs.Close()


        '        Importo = ImportoDocumento(NumeroRegistrazione)
        '        If Importo = 0 Then
        '            MyRs.Open("Select Importo From MovimentiContabiliRiga Where RigaDaCausale = 1 And Numero = " & NumeroRegistrazione, GeneraleDb, adOpenKeyset, adLockOptimistic)
        '            If Not MyRs.EOF Then
        '                Importo = MoveFromDb(MyRs!Importo)
        '            End If
        '            MyRs.Close()
        '        End If

        '        RsStampa.Open("Select * From Distinta", StampeDb, adOpenKeyset, adLockOptimistic)
        '        RsStampa.AddNew()
        '        MoveToDb(RsStampa!tipo, "ST.REV")
        '        MoveToDb(RsStampa!Anno, Year(DataReversale))
        '        MoveToDb(RsStampa!numero, NumeroReversale)
        '        MoveToDb(RsStampa!DATAEMISSIONE, DataReversale)
        '        MoveToDb(RsStampa!Debitore, Debitore)
        '        MoveToDb(RsStampa!Importo, Importo * -1)
        '        MoveToDb(RsStampa!TDistintaCompetenza, Importo * -1)
        '        MoveToDb(RsStampa!AnnoDistinta, Year(Txt_DataDistinta.Text))
        '        MoveToDb(RsStampa!NumeroDistinta, Txt_Numero.Text)
        '        MoveToDb(RsStampa!DataDistinta, Txt_DataDistinta.Text)
        '        MoveToDb(RsStampa!IntestaSocieta, DecodificaSocieta())
        '        MoveToDb(RsStampa!TPrecedentiCompetenza, ImpDPrima)
        '        RsStampa.Update()
        '        RsStampa.Close()
        '    End If
        'Next

        'ImpDPrima = 0
        'For I = 0 To Lst_StornoMandati.ListCount - 1
        '    If Lst_StornoMandati.Selected(I) = True Then
        '        PosizioneSpazio = InStr(1, Lst_StornoMandati.List(I), " ")
        '        If ImpDPrima = 0 Then
        '            ImpDPrima = CalcolaDistintePrecedenti("MAN", Txt_Numero.Text, Txt_Anno.Text)
        '        End If

        '        NumeroMandato = 0
        '        DataMandato = 0
        '        If PosizioneSpazio > 0 Then
        '            NumeroMandato = Mid(Lst_StornoMandati.List(I), 1, PosizioneSpazio)
        '            DataMandato = Mid(Lst_StornoMandati.List(I), PosizioneSpazio, Len(Lst_StornoMandati.List(I)) - PosizioneSpazio + 1)
        '        End If

        '        NumeroRegistrazione = 0
        '        MyRs.Open("Select * From MovimentiContabiliTesta Where TipoStorno = 'S' And  year(DataMandato) = " & Year(DataMandato) & " And NumeroMandato = " & NumeroMandato, GeneraleDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            NumeroRegistrazione = MoveFromDb(MyRs!NumeroRegistrazione)
        '            MoveToDb(MyRs!NumeroDistinta, Txt_Numero.Text)
        '            MoveToDb(MyRs!DataDistinta, Txt_DataDistinta.Text)
        '            MyRs.Update()
        '        End If
        '        MyRs.Close()

        '        Mastro = 0
        '        Conto = 0
        '        Sottoconto = 0
        '        MyRs.Open("SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.Tipo = 'CF' AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " ", GeneraleDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            Mastro = MoveFromDb(MyRs!MastroPartita)
        '            Conto = MoveFromDb(MyRs!ContoPartita)
        '            Sottoconto = MoveFromDb(MyRs!SottocontoPartita)
        '        End If
        '        MyRs.Close()

        '        If Mastro = 0 And Conto = 0 And Sottoconto = 0 Then
        '            MyRs.Open("SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.RigaDaCausale =1  AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " ", GeneraleDb, adOpenKeyset, adLockOptimistic)
        '            If Not MyRs.EOF Then
        '                Mastro = MoveFromDb(MyRs!MastroPartita)
        '                Conto = MoveFromDb(MyRs!ContoPartita)
        '                Sottoconto = MoveFromDb(MyRs!SottocontoPartita)
        '            End If
        '            MyRs.Close()
        '        End If

        '        Debitore = DecodificaConto(Mastro, Conto, Sottoconto)
        '        MyRs.Open("Select * From AnagraficaComune where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottoContoFornitore = " & Sottoconto, OspitiDb, adOpenKeyset, adLockOptimistic)
        '        If Not MyRs.EOF Then
        '            Debitore = MoveFromDb(MyRs!Nome)
        '        End If
        '        MyRs.Close()


        '        Importo = ImportoDocumento(NumeroRegistrazione)
        '        If Importo = 0 Then
        '            MyRs.Open("Select Importo From MovimentiContabiliRiga Where RigaDaCausale = 1  And Numero = " & NumeroRegistrazione, GeneraleDb, adOpenKeyset, adLockOptimistic)
        '            If Not MyRs.EOF Then
        '                Importo = MoveFromDb(MyRs!Importo)
        '            End If
        '            MyRs.Close()
        '        End If

        '        RsStampa.Open("Select * From Distinta", StampeDb, adOpenKeyset, adLockOptimistic)
        '        RsStampa.AddNew()
        '        MoveToDb(RsStampa!tipo, "ST.MAN")
        '        MoveToDb(RsStampa!Anno, Year(DataMandato))
        '        MoveToDb(RsStampa!numero, NumeroMandato)
        '        MoveToDb(RsStampa!DATAEMISSIONE, DataMandato)
        '        MoveToDb(RsStampa!Debitore, Debitore)
        '        MoveToDb(RsStampa!Importo, Importo * -1)
        '        MoveToDb(RsStampa!TDistintaCompetenza, Importo * -1)
        '        MoveToDb(RsStampa!AnnoDistinta, Year(Txt_DataDistinta.Text))
        '        MoveToDb(RsStampa!NumeroDistinta, Txt_Numero.Text)
        '        MoveToDb(RsStampa!DataDistinta, Txt_DataDistinta.Text)
        '        MoveToDb(RsStampa!IntestaSocieta, DecodificaSocieta())
        '        MoveToDb(RsStampa!TPrecedentiCompetenza, ImpDPrima)
        '        RsStampa.Update()
        '        RsStampa.Close()
        '    End If
        'Next

        cnGenerale.Close()
        cnStampa.Close()

        Txt_Anno.Enabled = True
        Txt_Numero.Enabled = True
        

        Session("SelectionFormula") = "{Distinta.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=STAMPADISTINTA&PRINTERKEY=ON','Stampe','width=800,height=600');", True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub


    
End Class
