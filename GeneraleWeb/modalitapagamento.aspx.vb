﻿
Partial Class GeneraleWeb_modalitapagamento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJs As String

        MyJs = "$('#" & Txt_Spese.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Spese.ClientID & "').val(), true, true); } );  $('#" & Txt_Spese.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Spese.ClientID & "').val(),2); $('#" & Txt_Spese.ClientID & "').val(ap); }); "        
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", MyJs, True)

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        DD_CampoFE.Items.Clear()
        DD_CampoFE.Items.Add("contanti")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP01"
        DD_CampoFE.Items.Add("assegno")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP02"
        DD_CampoFE.Items.Add("assegno circolare")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP03"
        DD_CampoFE.Items.Add("contanti presso Tesoreria")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP04"
        DD_CampoFE.Items.Add("bonifico")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP05"
        DD_CampoFE.Items.Add("vaglia cambiario")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP06"
        DD_CampoFE.Items.Add("bollettino bancario")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP07"
        DD_CampoFE.Items.Add("carta di credito")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP08"
        DD_CampoFE.Items.Add("RID")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP09"
        DD_CampoFE.Items.Add("RID utenze")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP10"
        DD_CampoFE.Items.Add("RID veloce")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP11"
        DD_CampoFE.Items.Add("RIBA")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP12"
        DD_CampoFE.Items.Add("MAV")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP13"
        DD_CampoFE.Items.Add("quietanza erario")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP14"
        DD_CampoFE.Items.Add("giroconto su conti di contabilità speciale")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP15"
        DD_CampoFE.Items.Add("domiciliazione bancaria")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP16"
        DD_CampoFE.Items.Add("domiciliazione postale")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = "MP17"
        DD_CampoFE.Items.Add("")
        DD_CampoFE.Items(DD_CampoFE.Items.Count - 1).Value = ""

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x As New Cls_TipoPagamento



        If Request.Item("Codcie") <> "" Then
            x.Codice = Request.Item("Codcie")
            x.Leggi(Session("DC_TABELLE"))

            Txt_Codice.Text = x.Codice
            Txt_Codice.Enabled = False
            Txt_Descrizione.Text = x.Descrizione

            Txt_GiorniPrima.Text = x.GiorniPrima
            Txt_GiorniSecondo.Text = x.GiorniSeconda
            Txt_GiorniAltre.Text = x.GiorniAltre

            Txt_Scadenze.Text = x.Scadenze

            Txt_Spese.Text = x.Spese

            DD_CampoFE.SelectedValue = x.CampoFE


            If x.Tipo = "F" Then
                RB_FineMese.Checked = True
                RB_Normale.Checked = False
            Else
                RB_Normale.Checked = True
                RB_FineMese.Checked = False
            End If

            If x.AperturaGestioneScadenze = 1 Then
                Chk_AperturaGestioneScadenze.Checked = True
            Else
                Chk_AperturaGestioneScadenze.Checked = False
            End If

            If x.IvaImponibile = "S" Then
                Rb_Si.Checked = True
            Else
                Rb_No.Checked = False
            End If
        Else
            Txt_Codice.Text = x.MaxTipoPagamento(Session("DC_TABELLE"))
        End If
    End Sub

    Private Sub Pulisci()



        Txt_Codice.Text = ""
        Txt_Descrizione.Text = ""

        Txt_GiorniPrima.Text = 0
        Txt_GiorniSecondo.Text = 0
        Txt_GiorniAltre.Text = 0

        Txt_Scadenze.Text = 0

        Txt_Spese.Text = 0
        DD_CampoFE.SelectedValue = ""


        RB_Normale.Checked = True
        RB_FineMese.Checked = False
        Chk_AperturaGestioneScadenze.Checked = False
    End Sub



    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Val(Txt_Codice.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then

            Dim xVer As New Cls_TipoPagamento

            xVer.Codice = Txt_Codice.Text
            xVer.Leggi(Session("DC_TABELLE"))

            If xVer.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                Exit Sub
            End If
        End If

        If Rb_Si.Checked = True And Val(Txt_Scadenze.Text) > 0 Then

        End If
        Dim x As New Cls_TipoPagamento


        If Not IsNothing(Request.Item("Codcie")) Then
            x.Codice = Request.Item("Codcie")
            x.Leggi(Session("DC_TABELLE"))
        End If

        x.Codice = Txt_Codice.Text
        x.Descrizione = Txt_Descrizione.Text

        x.GiorniPrima = Val(Txt_GiorniPrima.Text)
        x.GiorniSeconda = Val(Txt_GiorniSecondo.Text)
        x.GiorniAltre = Val(Txt_GiorniAltre.Text)

        x.Scadenze = Val(Txt_Scadenze.Text)

        If Trim(Txt_Spese.Text) = "" Then
            Txt_Spese.Text = 0
        End If
        x.Spese = CDbl(Txt_Spese.Text)
        x.CampoFE = DD_CampoFE.SelectedValue

        If RB_FineMese.Checked = True Then
            x.Tipo = "F"
        Else
            x.Tipo = ""
        End If
        If Chk_AperturaGestioneScadenze.Checked = True Then
            x.AperturaGestioneScadenze = 1
        Else
            x.AperturaGestioneScadenze = 0
        End If

        If Rb_Si.Checked = True Then
            x.IvaImponibile = "S"
        Else
            x.IvaImponibile = ""
        End If


        x.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("ElencoModalitapagamento.aspx")

    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Codice.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice');", True)
            Exit Sub
        End If

        Dim x As New Cls_TipoPagamento


        x.Codice = Request.Item("Codcie")

        If x.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If


        x.Elimina(Session("DC_TABELLE"))
        Response.Redirect("ElencoModalitapagamento.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoModalitapagamento.aspx")
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoPagamento

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoPagamento

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"))

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
