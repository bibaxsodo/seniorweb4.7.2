﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Partial Class GeneraleWeb_Stampa_ReportXSD
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        With CrystalReportViewer1
            .HasDrillUpButton = True
            .HasExportButton = True
            .HasGotoPageButton = True
            .HasPageNavigationButtons = True
            .HasPrintButton = True
            .HasRefreshButton = True
            .HasSearchButton = True
            .HasToggleGroupTreeButton = True
            .HasViewList = True
            .HasZoomFactorList = True
        End With
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim XS As New Cls_Login

        Dim rpt As New ReportDocument


        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))
        'x = Session("stampa")

        If XS.ReportPersonalizzato(Request.Item("REPORT")) = "" Then
            'window.close()

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Report non indicato'); window.close();", True)

            Exit Sub
        End If


        Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT"))


        If Not System.IO.File.Exists(NomeSt) Then
            Response.Redirect("Reportnonpresente.aspx?NOME=" & NomeSt)
            Exit Sub
        End If

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT"))) = "" Then
            Exit Sub
        End If
        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT")))



        rpt.SetDataSource(Session("stampa"))


        
        CrystalReportViewer1.DisplayGroupTree = False

        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ReuseParameterValuesOnRefresh = True

        CrystalReportViewer1.Visible = True
                   
  

    End Sub

    Private Sub GeneraleWeb_Stampa_ReportXSD_Unload(sender As Object, e As EventArgs) Handles Me.Unload

    End Sub
End Class
