﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class GeneraleWeb_LegameRegistrazioneBudget
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabellaRisultati As New System.Data.DataTable("tabella")
    Dim TRPreGrid As New System.Data.DataTable("tabella")

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub CARICAGRIGLE()
        ViewState("PRXLINEA") = -1
        ViewState("rgXLINEA") = -1

        Dim MyRs As New ADODB.Recordset
        Dim RSLegame As New ADODB.Recordset
        Dim Totale As Double
        Dim tipo As String


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Id", GetType(Long))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & Val(Request.Item("Numero")) & " Order by  DareAvere Desc"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim kConto As New Cls_Pianodeiconti

            kConto.Mastro = campodbN(myPOSTreader.Item("MastroPartita"))
            kConto.Conto = campodbN(myPOSTreader.Item("ContoPartita"))
            kConto.Sottoconto = campodbN(myPOSTreader.Item("SottoContoPartita"))

            kConto.Decodfica(Session("DC_GENERALE"))
            tipo = kConto.Tipo

            Totale = 0

            Dim cmdLegame As New OleDbCommand()
            cmdLegame.CommandText = "Select Sum(Importo) as Totale From PrenotazioniUsate Where NumeroRegistrazione = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & campodbN(myPOSTreader.Item("RigaRegistrazione"))
            cmdLegame.Connection = cn
            Dim myRDLegame As OleDbDataReader = cmdLegame.ExecuteReader()
            If myRDLegame.Read Then
                Totale = campodbN(myRDLegame.Item("Totale"))
            End If
            myRDLegame.Close()

            Dim cmdLegame1 As New OleDbCommand()
            cmdLegame1.CommandText = "Select Sum(Importo) as Totale From LegameBudgetRegistrazione  Where NumeroRegistrazione = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & Val(campodb(myPOSTreader.Item("RigaRegistrazione")))
            cmdLegame1.Connection = cn
            Dim myRDLegame1 As OleDbDataReader = cmdLegame1.ExecuteReader()
            If myRDLegame1.Read Then
                Totale = Totale + campodbN(myRDLegame1.Item("Totale"))
            End If
            myRDLegame1.Close()

            Dim ImportoRiga As Double = 0

            If tipo = "R" Then
                If campodb(myPOSTreader.Item("DareAvere")) = "D" And Totale < 0 Then
                    ImportoRiga = campodb(myPOSTreader.Item("Importo")) - (Totale * -1)
                Else
                    ImportoRiga = campodb(myPOSTreader.Item("Importo")) - Totale
                End If
            End If
            If tipo = "C" Then
                If campodb(myPOSTreader.Item("DareAvere")) = "A" And Totale < 0 Then
                    ImportoRiga = campodb(myPOSTreader.Item("Importo")) - (Totale * -1)
                Else
                    ImportoRiga = campodb(myPOSTreader.Item("Importo")) - Totale
                End If
            End If


            If (tipo = "C" Or tipo = "R") And ImportoRiga > 0 Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Val(campodb(myPOSTreader.Item("RigaRegistrazione")))

                myriga(1) =kConto.Mastro & " " & kConto.Conto & " " & kConto.Sottoconto & " " & kConto.Descrizione
                If campodb(myPOSTreader.Item("DareAvere")) = "D" Then
                    myriga(2) = Format(ImportoRiga, "#,##0.00")
                Else
                    myriga(3) = Format(ImportoRiga, "#,##0.00")
                End If
                Tabella.Rows.Add(myriga)
            End If
        Loop

        ViewState("RigheRegistrazione") = Tabella

        GrdRighe.AutoGenerateColumns = False
        GrdRighe.DataSource = Tabella
        GrdRighe.Font.Size = 10
        GrdRighe.DataBind()

        TabellaRisultati.Clear()
        TabellaRisultati.Columns.Clear()
        TabellaRisultati.Columns.Add("Anno", GetType(Long))
        TabellaRisultati.Columns.Add("Livello", GetType(String))
        TabellaRisultati.Columns.Add("Colonna", GetType(Long))
        TabellaRisultati.Columns.Add("Decodifica", GetType(String))        
        TabellaRisultati.Columns.Add("Importo", GetType(String))
        TabellaRisultati.Columns.Add("Riga", GetType(Long))
        TabellaRisultati.Columns.Add("Conto", GetType(String))
        TabellaRisultati.Columns.Add("Competenza", GetType(String))

        Dim cmdUs As New OleDbCommand()
        cmdUs.CommandText = "Select * From LegameBudgetRegistrazione Where NumeroRegistrazione = " & Val(Request.Item("Numero")) & " Order by AnnoCompetenza,MeseCompetenza,Anno,Livello1,Livello2,Livello3"
        cmdUs.Connection = cn
        Dim myRD As OleDbDataReader = cmdUs.ExecuteReader()
        Do While myRD.Read
            Dim Mastro As Long
            Dim Conto As Long
            Dim SottoConto As Long

            Mastro = 0
            Conto = 0
            SottoConto = 0

            Dim cmdLegame As New OleDbCommand()
            cmdLegame.CommandText = "Select  MastroPartita,ContoPartita,SottoContoPartita From   MovimentiContabiliRiga Where Numero = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & campodbN(myRD.Item("RigaRegistrazione"))
            cmdLegame.Connection = cn
            Dim myRDLegame As OleDbDataReader = cmdLegame.ExecuteReader()
            If myRDLegame.Read Then
                Mastro = campodbN(myRDLegame.Item("MastroPartita"))
                Conto = campodbN(myRDLegame.Item("ContoPartita"))
                SottoConto = campodbN(myRDLegame.Item("SottocontoPartita"))
            End If
            myRDLegame.Close()

            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            myriga(0) = campodb(myRD.Item("Anno"))

            Dim x As New Cls_TipoBudget

            x.Anno = campodbN(myRD.Item("Anno"))
            x.Livello1 = campodbN(myRD.Item("Livello1"))
            x.Livello2 = campodbN(myRD.Item("Livello2"))
            x.Livello3 = campodbN(myRD.Item("Livello3"))

            x.Decodfica(Session("DC_GENERALE"))


            myriga(1) = x.Livello1 & " " & x.Livello2 & " " & x.Livello3 & " " & x.Descrizione
            myriga(2) = campodbN(myRD.Item("Colonna"))

            Dim L As New Cls_colonnebudget

            L.Anno = campodbN(myRD.Item("Anno"))
            L.Livello1 = campodbN(myRD.Item("Colonna"))
            L.Leggi(Session("DC_GENERALE"))

            myriga(3) = L.Descrizione
            myriga(4) = Format(campodbN(myRD.Item("Importo")), "#,##0.00")
            myriga(5) = campodbN(myRD.Item("RigaRegistrazione"))

            Dim xConto As New Cls_Pianodeiconti

            xConto.Mastro = Mastro
            xConto.Conto = Conto
            xConto.Sottoconto = SottoConto

            xConto.Decodfica(Session("DC_GENERALE"))
            myriga(6) = xConto.Descrizione


            myriga(7) = campodbN(myRD.Item("MeseCompetenza")) & "/" & campodbN(myRD.Item("AnnoCompetenza"))

            TabellaRisultati.Rows.Add(myriga)
        Loop
        myRD.Close()
        cn.Close()

        If TabellaRisultati.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            myriga(0) = 0
            myriga(1) = 0
            myriga(2) = 0
            myriga(3) = 0
            TabellaRisultati.Rows.Add(myriga)
        End If
        Grid.AutoGenerateColumns = False
        Grid.DataSource = TabellaRisultati
        Grid.Font.Size = 10
        Grid.DataBind()
        ViewState("TabellaRisultati") = TabellaRisultati
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Val(Txt_Anno.Text) > 0 Then
            Dim MyJs1 As String

            MyJs1 = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "autocompleteCONTO", MyJs1, True)
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.NumeroRegistrazione = Val(Request.Item("Numero"))
        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        Dim RegoleBudget As New Cls_RegoleBudget

        RegoleBudget.ApplicaRegola(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)


        Dim x As New Cls_colonnebudget

        Txt_Anno.Text = Year(Registrazione.DataRegistrazione)

        Txt_AnnoCompetenza.Text = Year(Registrazione.DataRegistrazione)
        DD_Mese.SelectedValue = Month(Registrazione.DataRegistrazione)

        x.Anno = Year(Registrazione.DataRegistrazione)
        x.UpDateDropBox(Session("DC_GENERALE"), DD_Colonna)
        DD_Colonna.SelectedValue = ""

        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "autocompleteCONTO", MyJs, True)

        Call CARICAGRIGLE()




    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ||  (appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub GrdRighe_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdRighe.RowCommand
        If (e.CommandName = "Seleziona") Then
            Try

            
                Dim d As Long = e.CommandArgument

                If ViewState("rgXLINEA") <> -1 Then
                    GrdRighe.Rows(ViewState("rgXLINEA")).BackColor = ViewState("rgXBACKUP")
                End If

                Tabella = ViewState("RigheRegistrazione")
                ViewState("SelezionatoRiga") = Tabella.Rows(d).Item(0).ToString

                If Tabella.Rows(d).Item(2).ToString = "" Then
                    Tabella.Rows(d).Item(2) = "0"
                End If
                If Tabella.Rows(d).Item(3).ToString = "" Then
                    Tabella.Rows(d).Item(3) = "0"
                End If
                If CDbl(Tabella.Rows(d).Item(2).ToString) > 0 Then
                    Txt_Importo.Text = Tabella.Rows(d).Item(2).ToString
                End If
                If CDbl(Tabella.Rows(d).Item(3).ToString) > 0 Then
                    Txt_Importo.Text = Tabella.Rows(d).Item(3).ToString
                End If
                Try
                    Dim Vettore(100) as String

                    Vettore =SplitWords(Tabella.Rows(d).Item(1))

                    Dim PDc As New Cls_TipoBudget

                    PDc.Anno= Txt_Anno.Text
                    PDc.Livello1 = Vettore(0)
                    PDc.Livello2= Vettore(1)
                    PDc.Livello3 = Vettore(2)
                    PDc.Decodfica(Session("DC_GENERALE"))

                    If PDc.Descrizione<>"" Then
                            Txt_Livello.Text = PDc.Livello1 & " " & PDc.Livello2 & " " & PDc.Livello3 & " " & PDc.Descrizione
                    Else
                        Txt_Livello.Text =""
                    End If
                Catch ex As Exception

                End Try
                

                ViewState("rgXLINEA") = d
                ViewState("rgXBACKUP") = GrdRighe.Rows(d).BackColor
                GrdRighe.Rows(d).BackColor = Drawing.Color.Violet

            Catch ex As Exception

            End Try
        End If
    End Sub



    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Delete") Then
            TabellaRisultati = ViewState("TabellaRisultati")

            Dim cn As OleDbConnection
            Dim MySql As String

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
            cn.Open()

            MySql = "DELETE FROM LegameBudgetRegistrazione WHERE Anno = ? AND Livello1  = ? AND Livello2  = ? AND Livello3  = ? AND Colonna  = ? AND  Importo = ? AND NumeroRegistrazione = ? AND RigaRegistrazione = ?"

            Dim cmdUs As New OleDbCommand()
            cmdUs.CommandText = MySql
            cmdUs.Connection = cn

            Dim Anno As Long = 0
            Dim Numero As Long = 0
            Dim Riga As Long = 0
            Dim NumeroRegistrazione As Long = 0
            Dim Colonnna As Long = 0
            Dim Importo As Double = 0

            Dim Livello As String = ""

            Anno = Val(TabellaRisultati.Rows(e.CommandArgument).Item(0))
            Livello = TabellaRisultati.Rows(e.CommandArgument).Item(1)


            Dim Livello1 As Long
            Dim Livello2 As Long
            Dim Livello3 As Long
            Dim Vettore(100) As String
            Vettore = SplitWords(Livello)

            If Vettore.Length >= 3 Then
                Livello1 = Val(Vettore(0))
                Livello2 = Val(Vettore(1))
                Livello3 = Val(Vettore(2))
            End If

            Colonnna = Val(TabellaRisultati.Rows(e.CommandArgument).Item(2))
            NumeroRegistrazione = Val(Request.Item("Numero"))
            Try
                Importo = CDbl(TabellaRisultati.Rows(e.CommandArgument).Item(4))
            Catch ex As Exception

            End Try

            Try
                Riga = Val(TabellaRisultati.Rows(e.CommandArgument).Item(5))
            Catch ex As Exception

            End Try

            If Riga = 0 Then Exit Sub


            cmdUs.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdUs.Parameters.AddWithValue("@Livello1", Livello1)
            cmdUs.Parameters.AddWithValue("@Livello2", Livello2)
            cmdUs.Parameters.AddWithValue("@Livello3", Livello3)
            cmdUs.Parameters.AddWithValue("@Colona", Colonnna)
            cmdUs.Parameters.AddWithValue("@Importo", Importo)
            cmdUs.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmdUs.Parameters.AddWithValue("@RigaRegistrazione", Riga)
            cmdUs.ExecuteNonQuery()

            cn.Close()

            Call CARICAGRIGLE()
        End If
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function







    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted
        REM O
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        REM O
    End Sub

    Protected Sub BTN_Inserisci_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_Inserisci.Click

        If Txt_Importo.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Selezionare Importo</center>');", True)
            Exit Sub
        End If
        If Val(Txt_Anno.Text) < 1990 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare anno</center>');", True)
            Exit Sub
        End If

        If Val(ViewState("rgXLINEA")) = -1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Selezionare riga registrazione</center>');", True)
            Exit Sub
        End If

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))


        If DatiGenerali.CompetenzaObbligatiroAnalitica = 1 And Val(Txt_AnnoCompetenza.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi indicare il periodo di competenza</center>');", True)
            Exit Sub
        End If

        If DatiGenerali.CompetenzaObbligatiroAnalitica = 1 And Val(DD_Mese.SelectedValue) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi indicare il periodo di competenza</center>');", True)
            Exit Sub
        End If


        Dim MySql As String
        
        Dim NumeroRegistrazione As Long
        Dim RigaRegistrazione As Long


        Dim Livello1 As Long
        Dim Livello2 As Long
        Dim Livello3 As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_Livello.Text)

        If Vettore.Length >= 3 Then
            Livello1 = Val(Vettore(0))
            Livello2 = Val(Vettore(1))
            Livello3 = Val(Vettore(2))
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare il livello</center>');", True)
            Exit Sub
        End If

        Dim Param As New Cls_DatiGenerali

        Param.LeggiDati(Session("DC_TABELLE"))

        If Param.BudgetAnalitica = 0 Then
            Dim MyClBudget As New Cls_Budget

            If MyClBudget.DisponibilitaBudget(Session("DC_GENERALE"), Txt_Anno.Text, Livello1, Livello2, Livello3, DD_Colonna.SelectedValue, Val(Request.Item("Numero"))) < CDbl(Txt_Importo.Text) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non  vi è disponibilità nel budget</center>');", True)
                Exit Sub
            End If
        End If

        Tabella = ViewState("RigheRegistrazione")

        NumeroRegistrazione = Val(Request.Item("Numero"))
        RigaRegistrazione = Val(ViewState("SelezionatoRiga"))

        Dim TotImP As Double = 0

        TotImP = CDbl(campodbN(Tabella.Rows(ViewState("rgXLINEA")).Item(2))) + CDbl(campodbN(Tabella.Rows(ViewState("rgXLINEA")).Item(3)))
        If CDbl(Txt_Importo.Text) > TotImP Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Importo maggiore della riga contabile</center>');", True)
            Exit Sub
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        MySql = "INSERT INTO LegameBudgetRegistrazione (Anno,Livello1,Livello2,Livello3,Colonna,Importo,NumeroRegistrazione,RigaRegistrazione,AnnoCompetenza,MeseCompetenza) VALUES (?,?,?,?,?,?,?,?,?,?)"

        Dim cmdUs As New OleDbCommand()
        cmdUs.CommandText = MySql
        cmdUs.Connection = cn

        cmdUs.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        cmdUs.Parameters.AddWithValue("@Livello1", Livello1)
        cmdUs.Parameters.AddWithValue("@Livello2", Livello2)
        cmdUs.Parameters.AddWithValue("@Livello3", Livello3)
        cmdUs.Parameters.AddWithValue("@Colona", DD_Colonna.SelectedValue)
        cmdUs.Parameters.AddWithValue("@Importo", CDbl(Txt_Importo.Text))
        cmdUs.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmdUs.Parameters.AddWithValue("@RigaRegistrazione", RigaRegistrazione)
        cmdUs.Parameters.AddWithValue("@AnnoCompetenza", Txt_AnnoCompetenza.Text)
        cmdUs.Parameters.AddWithValue("@MeseCompetenza", DD_Mese.SelectedValue)
        cmdUs.ExecuteNonQuery()

        cn.Close()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Rita collegata');", True)
        Call CARICAGRIGLE()

        Txt_Anno.Text = Year(Now)
        Txt_Livello.Text = ""
        Txt_Importo.Text = "0,00"
        DD_Colonna.SelectedValue = 0

    End Sub
End Class
