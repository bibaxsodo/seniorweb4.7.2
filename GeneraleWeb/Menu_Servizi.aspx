﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Servizi" CodeFile="Menu_Servizi.aspx.vb" %>

<head id="Head1" runat="server">
    <title>Menu Generale</title>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Generale</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strumenti<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        &nbsp;</td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center; width: 20%;">
                                    <a href="ChiusureContabili.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Chiusure Contabili" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="Parametri.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Parametri" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="ControlloRegistrazioni.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Contro Registrazioni" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="XMLLiquidazioneIVATrimestrale.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Spesometro" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="ImportCorrispettivi.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Spesometro" style="border-width: 0;"></a>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CHIUSURE CONT.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PARAMETRI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTROL.REG.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">LIQUIDAZIONE XML</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT CORRISPETTIVI</span></td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <a href="DatiFatture.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Dati Fatture (nuovo spesometro)" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ImportRegistrazioni.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Import Registrazioni" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="PagamentoF24.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Pagamento F24" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="EliminaRegistrazioneF24.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" title="Visualizza Elimina 24" alt="Visualizza Elimina 24" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="Importxml.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Import XML" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DATI FATTURE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT REG. STIPENDI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PAGAMENTO F24</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VIS/ELIM. F24</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT XML</span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;">
                                    <a href="CreazioneMovimentiCespiti.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Creazione Movimenti Cespiti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="RegistrazioneRatei.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Creazione Risconti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="RigeneraScadenze.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Creazione Risconti" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="GeneraRegistri.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Genera Registri" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="ImportIncassi.aspx">
                                        <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Importo Incassi" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOV.CESPITI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CREA.RISCONTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">GENERA SCADENZE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">GENERA REGISTRI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT. INCASSI</span></td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>
                            <tr>
                    </td>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>

                <tr>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                </tr>
                 <tr>
                    <td style="text-align: center;">
                        <a href="GeneraDistintaIncassoPagamento.aspx">
                            <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Genera Distinta Incasso/Pagamento" style="border-width: 0;">
                        </a>
                    </td>
                    <td style="text-align: center;">
                        <a href="GeneraRegistrazioniDaDistinta.aspx">
                            <img src="../images/Menu_ChiusureContabili.png" class="Effetto" alt="Genera Registrazioni Da Distinta" style="border-width: 0;">
                        </a>
                    </td>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;">&nbsp;</td>
                 </tr>
                 <tr>
                     <td style="text-align: center; vertical-align: top;"><span class="MenuText">DISTINTA</span></td>
                     <td style="text-align: center; vertical-align: top;"><span class="MenuText">GEN.REGISTRAZIONI</span></td>
                     <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                     <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                     <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                 </tr>
            </table>
            </td>
    </tr>
    
    <tr>
        <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
        </td>
        <td></td>
        <td></td>
    </tr>
            </table> 
    
       
        </div>
    </form>
</body>
</html>