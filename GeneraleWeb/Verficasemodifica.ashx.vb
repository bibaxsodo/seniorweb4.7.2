﻿Imports System.Data.OleDb

Public Class Verficasemodifica
    Implements System.Web.IHttpHandler, IRequiresSessionState


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim Anno As String = context.Request.QueryString("Anno")
        Dim Livello1 As String = context.Request.QueryString("Liveollo1")
        Dim Livello2 As String = context.Request.QueryString("Livello2")
        Dim Livello3 As String = context.Request.QueryString("Livello3")
        Dim Colonna As String = context.Request.QueryString("Colonna")
        Dim Utente As String = context.Request.QueryString("UTENTE")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim DbC As New Cls_Login
        Dim cn As OleDbConnection


        Utente = context.Session("UTENTE")

        DbC.Utente = Utente
        DbC.LeggiSP(context.Application("SENIOR"))



        cn = New Data.OleDb.OleDbConnection(DbC.Generale)


        cn.Open()
        If Livello3 = 0 And Livello2 > 0 Then
            Dim CommandBudget As New OleDbCommand("Select *,(select sum(Importo) From VariazioniBudget Where Budget.Anno = VariazioniBudget.Anno And Budget.Livello1 = VariazioniBudget.Livello1 And Budget.Livello2 = VariazioniBudget.Livello2 And Budget.Livello3 = VariazioniBudget.Livello3 And Budget.Colonna = VariazioniBudget.Colonna) as totimp  From Budget Where Anno = ? And Livello1 = ? And Livello2 = 0 And Colonna = ? ", cn)
            Dim TotImpo As Double = 0

            CommandBudget.Parameters.AddWithValue("@Anno", Anno)
            CommandBudget.Parameters.AddWithValue("@Livello1", Livello1)
            CommandBudget.Parameters.AddWithValue("@Colonna", Colonna)


            Dim myBudget As OleDbDataReader = CommandBudget.ExecuteReader()
            Do While myBudget.Read()
                If campodbN(myBudget.Item("Livello1")) = Livello1 And campodbN(myBudget.Item("Livello2")) = Livello2 And campodbN(myBudget.Item("Livello3")) = Livello3 Then
                Else
                    TotImpo = TotImpo + campodbN(myBudget.Item("importo")) + campodbN(myBudget.Item("totimp"))
                End If
            Loop
            myBudget.Close()
            If TotImpo > 0 Then
                cn.Close()
                context.Response.Write("ERR")
                Exit Sub
            End If
        End If

        If Livello3 = 0 Then
            Dim MySql As String
            If Val(Livello2) > 0 Then
                MySql = "Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 > ? And Colonna = ? "
            Else
                MySql = "Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 >= ? And Livello3 >= ? And Colonna = ? "
            End If
            Dim CommandBudget As New OleDbCommand(MySql, cn)
            Dim TotImpo As Double = 0

            CommandBudget.Parameters.AddWithValue("@Anno", Anno)
            CommandBudget.Parameters.AddWithValue("@Livello1", Livello1)
            CommandBudget.Parameters.AddWithValue("@Livello2", Livello2)
            CommandBudget.Parameters.AddWithValue("@Livello3", Livello3)
            CommandBudget.Parameters.AddWithValue("@Colonna", Colonna)


            Dim myBudget As OleDbDataReader = CommandBudget.ExecuteReader()
            Do While myBudget.Read()
                If campodbN(myBudget.Item("Livello1")) = Livello1 And campodbN(myBudget.Item("Livello2")) = Livello2 And campodbN(myBudget.Item("Livello3")) = Livello3 Then
                Else
                    TotImpo = TotImpo + campodbN(myBudget.Item("importo"))
                End If
                '+ campodbN(myBudget.Item("totimp"))
            Loop
            myBudget.Close()
            If TotImpo = 0 Then
                cn.Close()
                context.Response.Write("OK")
                Exit Sub
            End If


        Else
            Dim CommandBudget1 As New OleDbCommand("Select *,(select sum(Importo) From VariazioniBudget Where Budget.Anno = VariazioniBudget.Anno And Budget.Livello1 = VariazioniBudget.Livello1 And Budget.Livello2 = VariazioniBudget.Livello2 And Budget.Livello3 = VariazioniBudget.Livello3 And Budget.Colonna = VariazioniBudget.Colonna) as totimp  From Budget Where Anno = ? And Livello1 = ? And Livello2 = 0 And Livello3 = 0 And Colonna = ? ", cn)
            Dim TotImpo1 As Double = 0

            CommandBudget1.Parameters.AddWithValue("@Anno", Anno)
            CommandBudget1.Parameters.AddWithValue("@Livello1", Livello1)
            CommandBudget1.Parameters.AddWithValue("@Colonna", Colonna)

            Dim myBudget1 As OleDbDataReader = CommandBudget1.ExecuteReader()
            Do While myBudget1.Read()

                TotImpo1 = TotImpo1 + campodbN(myBudget1.Item("importo")) + campodbN(myBudget1.Item("totimp"))
            Loop
            myBudget1.Close()
            If TotImpo1 > 0 Then
                cn.Close()
                context.Response.Write("FALSE")
                Exit Sub
            End If

            Dim CommandBudget As New OleDbCommand("Select *,(select sum(Importo) From VariazioniBudget Where Budget.Anno = VariazioniBudget.Anno And Budget.Livello1 = VariazioniBudget.Livello1 And Budget.Livello2 = VariazioniBudget.Livello2 And Budget.Livello3 = VariazioniBudget.Livello3 And Budget.Colonna = VariazioniBudget.Colonna) as totimp  From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = 0 And Colonna = ? ", cn)
            Dim TotImpo As Double = 0

            CommandBudget.Parameters.AddWithValue("@Anno", Anno)
            CommandBudget.Parameters.AddWithValue("@Livello1", Livello1)
            CommandBudget.Parameters.AddWithValue("@Livello2", Livello2)
            CommandBudget.Parameters.AddWithValue("@Colonna", Colonna)


            Dim myBudget As OleDbDataReader = CommandBudget.ExecuteReader()
            Do While myBudget.Read()

                TotImpo = TotImpo + campodbN(myBudget.Item("importo")) + campodbN(myBudget.Item("totimp"))
            Loop
            myBudget.Close()
            If TotImpo = 0 Then
                cn.Close()
                context.Response.Write("OK")
                Exit Sub
            End If
        End If

        cn.Close()
        context.Response.Write("FALSE")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class