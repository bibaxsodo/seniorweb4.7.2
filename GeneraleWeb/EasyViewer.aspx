﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_EasyViewer" CodeFile="EasyViewer.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Pagina senza titolo</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <script type="text/javascript">
        function piugrande() {
            if (document.getElementById("caratteri").style.fontSize == 'medium') {
                document.getElementById("caratteri").style.fontSize = 'x-small';
            } else {
                document.getElementById("caratteri").style.fontSize = 'medium';
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label" Width="240px"></asp:Label>
        </div>
    </form>
</body>
</html>
