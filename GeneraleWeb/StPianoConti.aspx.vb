﻿
Partial Class GeneraleWeb_StPianoConti
    Inherits System.Web.UI.Page


    Private Sub CreaStampa()
        Dim StampeDb As New ADODB.Connection
        Dim MyRs As New ADODB.Recordset
        Dim WrRs As New ADODB.Recordset
        Dim RsReg As New ADODB.Recordset
        Dim Condizione As String
        Dim MySql As String
        Dim WSocieta As String
        Dim DVB As New Cls_FunzioniVB6
        Dim GeneraleDb As New ADODB.Connection

        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        StampeDb.Open(Session("StampeFinanziaria"))
        StampeDb.Execute("Delete From PianoConti Where  PRINTERKEY = '" & ViewState("PRINTERKEY") & "'")

        GeneraleDb.Open(Session("DC_GENERALE"))

        Dim XSoc As New Cls_DecodificaSocieta

        WSocieta = XSoc.DecodificaSocieta(Session("DC_TABELLE"))


        Condizione = ""

        WrRs.Open("PianoConti", StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        
        If RB_CliNO.Checked = True Then
            Condizione = " WHERE (TipoAnagrafica Is Null Or (TipoAnagrafica <> 'D' and TipoAnagrafica <> 'O' and TipoAnagrafica <> 'P' and TipoAnagrafica <> 'C' and TipoAnagrafica <> 'R') ) "
        End If

        If RB_SOCINO.Checked = True Then
            If Condizione = "" Then
                Condizione = Condizione & " WHERE"
            Else
                Condizione = Condizione & " AND"
            End If
            Condizione = Condizione & " (TipoAnagrafica Is Null OR TipoAnagrafica <> 'S')  "
        End If



   

        If Rb_UsatiSI.Checked = True Then
            If Condizione = "" Then
                Condizione = Condizione & " WHERE"
            Else
                Condizione = Condizione & " AND"
            End If
            Condizione = Condizione & " (Select Count(*) From MovimentiContabiliRiga Where MovimentiContabiliRiga.MastroPartita = PianoConti.Mastro And MovimentiContabiliRiga.ContoPartita = PianoConti.Conto And MovimentiContabiliRiga.SottoContoPartita = PianoConti.SottoConto ) > 0"
        End If

        If Rb_UsatiNO.Checked = True Then
            If Condizione = "" Then
                Condizione = Condizione & " WHERE"
            Else
                Condizione = Condizione & " AND"
            End If
            Condizione = Condizione & " (Select Count(*) From MovimentiContabiliRiga Where MovimentiContabiliRiga.MastroPartita = PianoConti.Mastro And MovimentiContabiliRiga.ContoPartita = PianoConti.Conto And MovimentiContabiliRiga.SottoContoPartita = PianoConti.SottoConto ) = 0"
        End If


        If RB_Comprsi.Checked = False Then
            If Condizione = "" Then
                Condizione = Condizione & " WHERE"
            Else
                Condizione = Condizione & " AND"
            End If
            If RB_Esclusi.Checked = True Then Condizione = Condizione & " (NonInUso Is Null OR NonInUso <> 'S')"
            If RB_Soloro.Checked = True Then Condizione = Condizione & " NonInUso = 'S'"
        End If

        
        MySql = "SELECT * FROM PianoConti " & _
                Condizione & _
                " ORDER BY Mastro, Conto, Sottoconto"


        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            WrRs.AddNew()
            DVB.MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
            DVB.MoveToDb(WrRs.Fields("Mastro"), DVB.MoveFromDbWC(MyRs, "Mastro"))
            DVB.MoveToDb(WrRs.Fields("Conto"), DVB.MoveFromDbWC(MyRs, "Conto"))
            DVB.MoveToDb(WrRs.Fields("Sottoconto"), DVB.MoveFromDbWC(MyRs, "SottoConto"))
            If DVB.MoveFromDbWC(MyRs, "Conto") = 0 And DVB.MoveFromDbWC(MyRs, "SottoConto") = 0 Then
                DVB.MoveToDb(WrRs.Fields("Descrizione"), DVB.MoveFromDbWC(MyRs, "Descrizione"))
            End If

            If DVB.MoveFromDbWC(MyRs, "Conto") > 0 And DVB.MoveFromDbWC(MyRs, "SottoConto") = 0 Then
                DVB.MoveToDb(WrRs.Fields("Descrizione"), Space(10) & DVB.MoveFromDbWC(MyRs, "Descrizione"))
            End If
            If DVB.MoveFromDbWC(MyRs, "Conto") > 0 And DVB.MoveFromDbWC(MyRs, "SottoConto") > 0 Then
                DVB.MoveToDb(WrRs.Fields("Descrizione"), Space(20) & DVB.MoveFromDbWC(MyRs, "Descrizione"))
            End If


            DVB.MoveToDb(WrRs.Fields("tipo"), DVB.MoveFromDbWC(MyRs, "Tipo"))
            DVB.MoveToDb(WrRs.Fields("Sezione"), DVB.MoveFromDbWC(MyRs, "Sezione"))
            DVB.MoveToDb(WrRs.Fields("Classe"), DVB.MoveFromDbWC(MyRs, "Classe"))
            DVB.MoveToDb(WrRs.Fields("TipoBene"), DVB.MoveFromDbWC(MyRs, "TipoBene"))
            DVB.MoveToDb(WrRs.Fields("TipoAnagrafica"), DVB.MoveFromDbWC(MyRs, "TipoAnagrafica"))
            DVB.MoveToDb(WrRs.Fields("EntrataUscita"), DVB.MoveFromDbWC(MyRs, "EntrataUscita"))
            DVB.MoveToDb(WrRs.Fields("Capitolo"), DVB.MoveFromDbWC(MyRs, "Capitolo"))
            DVB.MoveToDb(WrRs.Fields("Articolo"), DVB.MoveFromDbWC(MyRs, "Articolo"))
            DVB.MoveToDb(WrRs.Fields("NonInUso"), DVB.MoveFromDbWC(MyRs, "NonInUso"))
            DVB.MoveToDb(WrRs.Fields("DescrizioneXSort"), DVB.MoveFromDbWC(MyRs, "Descrizione"))

            DVB.MoveToDb(WrRs.Fields("PRINTERKEY"), ViewState("PRINTERKEY"))
            WrRs.Update()
            MyRs.MoveNext()
        Loop
        MyRs.Close()

        StampeDb.Close()
        GeneraleDb.Close()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click



        Call CreaStampa()

        Session("SelectionFormula") = "{PianoConti.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

        If RB_TipoStruttura.Checked = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=PIANOCONTI&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        End If

        If RB_TipoControllo.Checked = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=PIANOCONTICONTROLLO&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        End If
        If RB_TipoAnomalie.Checked = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=PIANOCONTIANOMALIE&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        End If

        'Response.Redirect("StampaReport.aspx?REPORT=PIANOCONTI")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub


End Class
