﻿Imports System.Xml
Imports System.Net
Imports System.IO
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text

Partial Class GeneraleWeb_Tabella_DatiGenerali
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_Tabella_DatiGenerali_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim MyCau As New Cls_CausaleContabile

        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CasualeRettifica)
        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleApertura)
        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleChiusura)


        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CasualeDefualt)

        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CasualeDefualtNC)


        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CasualeDefualtAttivita2)

        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CasualeDefualtNCAttiva2)


        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CespitiAmmort)

        MyCau.UpDateDropBox(Session("DC_TABELLE"), DD_CespitiDimissione)



        Dim X As New Cls_DatiGenerali

        X.LeggiDati(Session("DC_TABELLE"))

        Txt_ContoClienti.Text = X.ClientiConto
        Txt_MastroCliente.Text = X.ClientiMastro

        Txt_MatroFornitore.Text = X.FornitoriMastro
        Txt_ContoFornitore.Text = X.FornitoriConto




        Dim Des As New Cls_Pianodeiconti

        Des.Mastro = X.BilancioAperturaMastro
        Des.Conto = X.BilancioAperturaConto
        Des.Sottoconto = X.BilancioAperturaSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoBilancioApertura.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione


        Des.Mastro = X.MastroBollo
        Des.Conto = X.ContoBollo
        Des.Sottoconto = X.SottoContoBollo
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoBollo.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione




        Des.Descrizione = ""
        Des.Mastro = X.BilancioChiusuraMastro
        Des.Conto = X.BilancioChiusuraConto
        Des.Sottoconto = X.BilancioChiusuraSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoBilancioChiusura.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione


        Des.Descrizione = ""
        Des.Mastro = X.ProfittiPerditeMastro
        Des.Conto = X.ProfittiPerditeConto
        Des.Sottoconto = X.ProfittiPerditeSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoProfittiPerdite.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione


        Des.Descrizione = ""
        Des.Mastro = X.UtilePatrimonialeMastro
        Des.Conto = X.UtilePatrimonialeConto
        Des.Sottoconto = X.ProfittiPerditeSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoUtilePatrimoniale.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione

        Des.Descrizione = ""
        Des.Mastro = X.PerditaPatrimonialeMastro
        Des.Conto = X.PerditaPatrimonialeConto
        Des.Sottoconto = X.PerditaPatrimonialeSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoPerdiPatrimoniale.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione

        Des.Descrizione = ""
        Des.Mastro = X.UtileEconomicoMastro
        Des.Conto = X.UtileEconomicoConto
        Des.Sottoconto = X.UtileEconomicoSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoUtileEconomico.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione

        Des.Descrizione = ""
        Des.Mastro = X.PerditaEconomicoMastro
        Des.Conto = X.PerditaEconomicoConto
        Des.Sottoconto = X.PerditaEconomicoSottoconto
        Des.Decodfica(Session("DC_GENERALE"))
        Txt_SottocontoPerditaEconomico.Text = Des.Mastro & " " & Des.Conto & " " & Des.Sottoconto & " " & Des.Descrizione


        Txt_DataChiusura.Text = Format(X.DataChiusura, "dd/MM/yyyy")

        DD_CasualeRettifica.SelectedValue = X.CausaleRettifica

        DD_CausaleApertura.SelectedValue = X.CausaleApertura
        DD_CausaleChiusura.SelectedValue = X.CausaleChiusura


        If X.StampaDaGestioneDocumenti = 1 Then
            Chk_StampaDaGestioneDocumenti.Checked = True
        Else
            Chk_StampaDaGestioneDocumenti.Checked = False
        End If

        If X.MovimentiRitenute = 1 Then
            Chk_MovimentiRitenuta.Checked = True
        Else
            Chk_MovimentiRitenuta.Checked = False
        End If

        If X.GirocontoRitenuta = 1 Then
            Chk_GirocontoRitentute.Checked = True
        Else
            Chk_GirocontoRitentute.Checked = False
        End If

        If X.BudgetAnalitica = 0 Then
            Rd_Budget.Checked = True
            Rd_Analitica.Checked = False
        Else
            Rd_Budget.Checked = False
            Rd_Analitica.Checked = True
        End If

        If X.AttivaCServPrimanoIncassi = 1 Then
            Chk_AttivaCServPrimanoIncassi.Checked = True
        Else
            Chk_AttivaCServPrimanoIncassi.Checked = False
        End If

        DD_CasualeDefualt.SelectedValue = X.CausaleContabileDefault

        DD_CasualeDefualtNC.SelectedValue = X.CausaleContabileNCDefault


        If Val(X.TestAgyo) = 1 Then
            Chk_EffettivoAgyo.Checked = True
        Else
            Chk_EffettivoAgyo.Checked = False
        End If

        If X.ModalitaPagamentoOblDoc = 1 Then
            Chk_ModalitaPagamentoOblDoc.Checked = True
        Else
            Chk_ModalitaPagamentoOblDoc.Checked = False
        End If

        If X.ScadenziarioCheckChiuso = 1 Then
            Chk_ScadenziarioCheckChiuso.Checked = True
        Else
            Chk_ScadenziarioCheckChiuso.Checked = False
        End If

        If X.BloccaLegameScadenzario = 1 Then
            Chk_BloccaLegameScadenzario.Checked = True
        Else
            Chk_BloccaLegameScadenzario.Checked = False
        End If

        If X.TipoVisualizzazione = 1 Then
            Chk_TipoVisualizzazione.Checked = True
        Else
            Chk_TipoVisualizzazione.Checked = False
        End If


        If X.CompetenzaObbligatiroAnalitica = 1 Then
            Chk_CompetenzaObbligatiroAnalitica.Checked = True
        Else
            Chk_CompetenzaObbligatiroAnalitica.Checked = False
        End If

        If X.GestioneDocumentiNew = 1 Then
            Chk_GestioneDocumenti.Checked = True
        Else
            Chk_GestioneDocumenti.Checked = False
        End If

        Txt_IdAgyo.Text = X.IdAgyo
        Txt_PasswordAgyo.Text = X.PasswordAgyo

        Txt_TokenAgyo.Text = X.TokenAgyo


        DD_CasualeDefualtAttivita2.SelectedValue = X.CausaleContabileDefaultAttivita2

        DD_CasualeDefualtNCAttiva2.SelectedValue = X.CausaleContabileNCDefaultAttivita2




        Dim ParamCesp As New Cls_ParametriCespiti


        ParamCesp.LeggiParametri(Session("DC_GENERALE"))


        DD_CespitiAmmort.SelectedValue = ParamCesp.CausaleContabile

        DD_CespitiDimissione.SelectedValue = ParamCesp.CausaleContabileDismissione


        Call EseguiJS()


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_DataChiusura')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('Txt_DataReversale') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim X As New Cls_DatiGenerali

        X.LeggiDati(Session("DC_TABELLE"))

        X.ClientiConto = Txt_ContoClienti.Text
        X.ClientiMastro = Txt_MastroCliente.Text
        X.FornitoriMastro = Txt_MatroFornitore.Text
        X.FornitoriConto = Txt_ContoFornitore.Text

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String


        Vettore = SplitWords(Txt_SottocontoBilancioApertura.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.BilancioAperturaMastro = Mastro
        X.BilancioAperturaConto = Conto
        X.BilancioAperturaSottoconto = Sottoconto




        Vettore = SplitWords(Txt_SottocontoBollo.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.MastroBollo = Mastro
        X.ContoBollo = Conto
        X.SottoContoBollo = Sottoconto



        Vettore = SplitWords(Txt_SottocontoBilancioChiusura.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.BilancioChiusuraMastro = Mastro
        X.BilancioChiusuraConto = Conto
        X.BilancioChiusuraSottoconto = Sottoconto

        Vettore = SplitWords(Txt_SottocontoProfittiPerdite.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.ProfittiPerditeMastro = Mastro
        X.ProfittiPerditeConto = Conto
        X.ProfittiPerditeSottoconto = Sottoconto


        Vettore = SplitWords(Txt_SottocontoUtilePatrimoniale.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.UtilePatrimonialeMastro = Mastro
        X.UtilePatrimonialeConto = Conto
        X.ProfittiPerditeSottoconto = Sottoconto



        Vettore = SplitWords(Txt_SottocontoUtilePatrimoniale.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.UtilePatrimonialeMastro = Mastro
        X.UtilePatrimonialeConto = Conto
        X.ProfittiPerditeSottoconto = Sottoconto


        Vettore = SplitWords(Txt_SottocontoPerdiPatrimoniale.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.PerditaPatrimonialeMastro = Mastro
        X.PerditaPatrimonialeConto = Conto
        X.PerditaPatrimonialeSottoconto = Sottoconto


        Vettore = SplitWords(Txt_SottocontoUtileEconomico.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.UtileEconomicoMastro = Mastro
        X.UtileEconomicoConto = Conto
        X.UtileEconomicoSottoconto = Sottoconto



        Vettore = SplitWords(Txt_SottocontoPerditaEconomico.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        X.PerditaEconomicoMastro = Mastro
        X.PerditaEconomicoConto = Conto
        X.PerditaEconomicoSottoconto = Sottoconto



        X.DataChiusura = Txt_DataChiusura.Text

        X.CausaleRettifica = DD_CasualeRettifica.SelectedValue

        X.CausaleApertura = DD_CausaleApertura.SelectedValue
        X.CausaleChiusura = DD_CausaleChiusura.SelectedValue

        If Chk_StampaDaGestioneDocumenti.Checked = True Then
            X.StampaDaGestioneDocumenti = 1
        Else
            X.StampaDaGestioneDocumenti = 0
        End If



        If Chk_MovimentiRitenuta.Checked = True Then
            X.MovimentiRitenute = 1
        Else
            X.MovimentiRitenute = 0
        End If


        If Chk_GirocontoRitentute.Checked = True Then
            X.GirocontoRitenuta = 1
        Else
            X.GirocontoRitenuta = 0
        End If

        If Rd_Budget.Checked = True Then
            X.BudgetAnalitica = 0
        Else
            X.BudgetAnalitica = 1
        End If

        X.CausaleContabileDefault = DD_CasualeDefualt.SelectedValue

        X.CausaleContabileNCDefault = DD_CasualeDefualtNC.SelectedValue


        If Chk_ScadenziarioCheckChiuso.Checked = True Then

            X.ScadenziarioCheckChiuso = 1
        Else
            X.ScadenziarioCheckChiuso = 0

        End If

        If Chk_BloccaLegameScadenzario.Checked = True Then
            X.BloccaLegameScadenzario = 1
        Else
            X.BloccaLegameScadenzario = 0
        End If



        If Chk_AttivaCServPrimanoIncassi.Checked = True Then
            X.AttivaCServPrimanoIncassi = 1
        Else
            X.AttivaCServPrimanoIncassi = 0
        End If



        If Chk_ModalitaPagamentoOblDoc.Checked = True Then
            X.ModalitaPagamentoOblDoc = 1
        Else
            X.ModalitaPagamentoOblDoc = 0
        End If



        If Chk_CompetenzaObbligatiroAnalitica.Checked = True Then
            X.CompetenzaObbligatiroAnalitica = 1
        Else
            X.CompetenzaObbligatiroAnalitica = 0
        End If


        X.CausaleContabileDefaultAttivita2 = DD_CasualeDefualtAttivita2.SelectedValue

        X.CausaleContabileNCDefaultAttivita2 = DD_CasualeDefualtNCAttiva2.SelectedValue


        X.IdAgyo = Txt_IdAgyo.Text
        X.PasswordAgyo = Txt_PasswordAgyo.Text

        X.TokenAgyo = Txt_TokenAgyo.Text


        If Chk_EffettivoAgyo.Checked = True Then
            X.TestAgyo = 1
        Else
            X.TestAgyo = 0
        End If

        If Chk_TipoVisualizzazione.Checked = True Then
            X.TipoVisualizzazione = 1
        Else
            X.TipoVisualizzazione = 0
        End If


        If Chk_GestioneDocumenti.Checked = True Then
            X.GestioneDocumentiNew = 1
        Else
            X.GestioneDocumentiNew = 0
        End If

        X.ScriviDati(Session("DC_TABELLE"))



        Dim ParamCesp As New Cls_ParametriCespiti


        ParamCesp.LeggiParametri(Session("DC_GENERALE"))


        ParamCesp.CausaleContabile = DD_CespitiAmmort.SelectedValue

        ParamCesp.CausaleContabileDismissione = DD_CespitiDimissione.SelectedValue



        ParamCesp.ScriviParametri(Session("DC_GENERALE"))
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Private Function GetNonce(ByVal ID As String) As String
        Dim _WebRequest As HttpWebRequest = CreateWebRequest()

        Dim XmlDocument As New XmlDocument()

        Dim MyXML As String = ""

        GetNonce = ""

        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?> " & _
               " <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://schema.auth.hub.teamsystem.com/v1"">" & _
                "  <SOAP-ENV:Body>" & _
                 "     <ns1:getNonce_request>" & _
                 "         <ns1:id>" & ID & "</ns1:id>" & _
                 "     </ns1:getNonce_request>" & _
                 " </SOAP-ENV:Body>" & _
                "</SOAP-ENV:Envelope>"


        XmlDocument.LoadXml(MyXML)


        Using MyStream As Stream = _WebRequest.GetRequestStream()

            XmlDocument.Save(MyStream)
        End Using



        Using MyResponse As WebResponse = _WebRequest.GetResponse()

            Using MyReader As StreamReader = New StreamReader(MyResponse.GetResponseStream())

                Dim Appoggio As String
                Appoggio = MyReader.ReadToEnd()

                Dim Inizio As Integer
                Dim Fine As Integer

                Inizio = Appoggio.IndexOf("<nonce>") + 8
                Fine = Appoggio.IndexOf("</nonce>")

                GetNonce = Mid(Appoggio, Inizio, Fine - Inizio + 1)
            End Using
        End Using


    End Function


    Private Function CreateWebRequest() As HttpWebRequest
        Dim DatiGen As New Cls_DatiGenerali

        DatiGen.LeggiDati(Session("DC_TABELLE"))



        Dim webRequest As HttpWebRequest


        If Val(DatiGen.TestAgyo) = 1 Then
            webRequest = HttpWebRequest.Create("https://soap-b2b-auth-service.agyo.io/AuthApi_v1/AuthApi.ws")
        Else
            webRequest = HttpWebRequest.Create("https://soap-b2b-auth-service-test.agyo.io/AuthApi_v1/AuthApi.ws")
        End If

        webRequest.Headers.Add("SOAP:Action")
        '   //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.ContentType = "application/soap+xml;charset=UTF-8;action=getNonce"
        webRequest.Accept = "text/xml"
        webRequest.Method = "POST"
        Return webRequest
    End Function





    Private Function CreateWebRequestverifyDigest() As HttpWebRequest
        Dim DatiGen As New Cls_DatiGenerali

        DatiGen.LeggiDati(Session("DC_TABELLE"))



        Dim webRequest As HttpWebRequest

        If Val(DatiGen.TestAgyo) = 1 Then
            webRequest = HttpWebRequest.Create("https://soap-b2b-auth-service.agyo.io/AuthApi_v1/AuthApi.ws")
        Else
            webRequest = HttpWebRequest.Create("https://soap-b2b-auth-service-test.agyo.io/AuthApi_v1/AuthApi.ws")
        End If


        webRequest.Headers.Add("SOAP:Action")
        '   //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.ContentType = "application/soap+xml;charset=UTF-8;action=verifyDigest"
        webRequest.Accept = "text/xml"
        webRequest.Method = "POST"
        Return webRequest
    End Function


    Public Shared Function GenerateSHA256String(ByVal inputString As String) As String
        Dim sha256 As System.Security.Cryptography.SHA256 = System.Security.Cryptography.SHA256Managed.Create()
        Dim hash As New System.Text.StringBuilder
        Dim crypt As System.Security.Cryptography.SHA256Managed

        crypt = New System.Security.Cryptography.SHA256Managed()


        Dim crypto As Byte() = crypt.ComputeHash(Encoding.UTF8.GetBytes(inputString))

        Dim stringBuilder As New StringBuilder()

        For i As Integer = 0 To crypto.Length - 1
            stringBuilder.Append(crypto(i).ToString("x2"))
        Next

        Return stringBuilder.ToString()
    End Function

    Private Function CreateDigest(ByVal id As String, ByVal Password As String, ByVal nonce As String) As String
        Dim Idlow As String = id.ToLower

        Return GenerateSHA256String(GenerateSHA256String(Idlow + Password) + nonce)
    End Function

    Private Function GetToken(ByVal ID As String, ByVal Password As String, ByVal nonce As String) As String
        Dim _WebRequest As HttpWebRequest = CreateWebRequestverifyDigest()

        Dim XmlDocument As New XmlDocument()

        Dim MyXML As String = ""

        
        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & _
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://schema.auth.hub.teamsystem.com/v1"">" & _
                "  <SOAP-ENV:Body>" & _
                "    <ns1:verifyDigest_request>" & _
                "      <ns1:id>" & ID & "</ns1:id>" & _
                "      <ns1:digest>" + CreateDigest(ID, Password, nonce) + "</ns1:digest>" & _
                "    </ns1:verifyDigest_request>" & _
                "  </SOAP-ENV:Body>" & _
                "</SOAP-ENV:Envelope>"


        XmlDocument.LoadXml(MyXML)


        Using MyStream As Stream = _WebRequest.GetRequestStream()

            XmlDocument.Save(MyStream)
        End Using



        Using MyResponse As WebResponse = _WebRequest.GetResponse()

            Using MyReader As StreamReader = New StreamReader(MyResponse.GetResponseStream())

                Dim Appoggio As String
                Appoggio = MyReader.ReadToEnd()

                Dim Inizio As Integer
                Dim Fine As Integer

                Inizio = Appoggio.IndexOf("<token>") + 8
                Fine = Appoggio.IndexOf("</token>")

                GetToken = Mid(Appoggio, Inizio, Fine - Inizio + 1)
            End Using
        End Using


    End Function


    Protected Sub BTN_AGYO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_AGYO.Click

        'GetNonce(Txt_IdAgyo.Text)
        Txt_TokenAgyo.Text = GetToken(Txt_IdAgyo.Text, Txt_PasswordAgyo.Text, GetNonce(Txt_IdAgyo.Text))
    End Sub
End Class
