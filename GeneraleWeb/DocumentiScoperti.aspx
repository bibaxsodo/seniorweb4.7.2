﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_DocumentiScoperti" CodeFile="DocumentiScoperti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Documenti Scoperti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript"> 
        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


        function DialogBox(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Visualizzazioni - Documenti Scoperti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">

                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px"
                                ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="38px"
                                ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" />
                        </div>
                    </td>
                </tr>


                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Documenti Scoperti
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Conto Contabilità :</label>
                                    <asp:TextBox ID="Txt_Sottoconto" CssClass="MyAutoComplete" runat="server" Width="488px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data Dal :</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Data Al :</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label style="display: block; float: left; width: 200px;">Modalità di Pagamento:</label>
                                    <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="350px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 200px;">Ordinamento:</label>
                                    <asp:RadioButton ID="DD_OrdinamentoRagioneSociale" runat="server" Text="Ragione Sociale" GroupName="Ordinamento" Checked />
                                    <asp:RadioButton ID="DD_OrdinamentoSottoconto" runat="server" Text="Sottoconto" GroupName="Ordinamento" />

                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="384px"></asp:Label><br />
                                    &nbsp;      
         <asp:GridView ID="Grid" runat="server" CellPadding="3"
             Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
             BorderWidth="1px" GridLines="Vertical">
             <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
             <Columns>
                 <asp:TemplateField HeaderText="">
                     <ItemTemplate>
                         <asp:ImageButton ID="Richiama" CommandName="Richiama" runat="Server"
                             ImageUrl="~/images/select.png" class="EffettoBottoniTondi" CommandArgument='<%#  Eval("NumeroRegistrazione") %>' />
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
             <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
             <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
             <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
             <AlternatingRowStyle BackColor="#DCDCDC" />
         </asp:GridView>

                                    <br />

                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                        <asp:GridView ID="GridView1" runat="server" Height="70px" Width="760px">
                        </asp:GridView>

                        <br />
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
