﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting
Imports System.Web.Script.Serialization
Imports System.Security.Cryptography.Pkcs

Partial Class Documenti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabIVA As New System.Data.DataTable("TabIVA")
    Dim TabCR As New System.Data.DataTable("TabCostiRicavi")
    Dim TabRT As New System.Data.DataTable("TabRitenute")

    Private Sub BindGridIva()
        TabIVA = ViewState("TABELLAIVA")
        GridIVA.AutoGenerateColumns = False
        GridIVA.DataSource = TabIVA
        GridIVA.DataBind()
    End Sub

    Private Sub BindGridCR()
        TabCR = ViewState("TABELLACR")
        GridCostiRicavi.AutoGenerateColumns = False
        GridCostiRicavi.DataSource = TabCR
        GridCostiRicavi.DataBind()
    End Sub

    Private Sub BindGridRT()
        TabRT = ViewState("TABELLART")
        GridRitenuta.AutoGenerateColumns = False
        GridRitenuta.DataSource = TabRT
        GridRitenuta.DataBind()
    End Sub






    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        Dim Param As New Cls_DatiGenerali

        Param.LeggiDati(Session("DC_TABELLE"))


        If Param.GestioneDocumentiNew = 1 Then
            Response.Redirect("DocumentiJS.aspx?" & Request.QueryString.ToString)
            Exit Sub
        End If

        'If Session("TIPOAPP") <> "RSA" Then
        '    If Request.Item("PAGINA") = "" Then
        '        If Val(Request.Item("CHIAMATAESTERNA")) > 0 Then
        '            Response.Redirect("DocumentiEasy.aspx?CHIAMATAESTERNA=" & Request.Item("CHIAMATAESTERNA") & "&ID=" & Val(Session("NumeroRegistrazione")))
        '        Else
        '            Response.Redirect("DocumentiEasy.aspx?ID=" & Val(Session("NumeroRegistrazione")))
        '        End If
        '    Else
        '        If Val(Request.Item("CHIAMATAESTERNA")) > 0 Then
        '            Response.Redirect("DocumentiEasy.aspx?CHIAMATAESTERNA=" & Request.Item("CHIAMATAESTERNA") & "&ID=" & Val(Session("NumeroRegistrazione")) & "&PAGINA=" & Request.Item("PAGINA"))
        '        Else
        '            Response.Redirect("DocumentiEasy.aspx?ID=" & Val(Session("NumeroRegistrazione")) & "&PAGINA=" & Request.Item("PAGINA"))
        '        End If
        '    End If

        '    Exit Sub
        'End If
        LblBox.Text = ""

        Timer1.Interval = 900 * 1000


        Call EseguiJS(True)

        If Request.Item("CHIAMATAESTERNA") > 0 Then
            Btn_Esci.Visible = False
            ImgRic.Visible = False
            Call MettiInvisibileJS()
        End If




        If Param.TipoVisualizzazione = 1 Then
            GridIVA.CellPadding = "0"
            GridIVA.CellSpacing = "0"

            GridCostiRicavi.CellPadding = "0"
            GridCostiRicavi.CellSpacing = "0"


            GridRitenuta.CellPadding = "0"
            GridRitenuta.CellSpacing = "0"

        End If


        If Page.IsPostBack = True Then Exit Sub


        If Param.TipoVisualizzazione = 1 Then
            ChkAspetto.Checked = True
        Else
            ChkAspetto.Checked = False
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior


        If Val(Request.Item("NumeroRegistrazione")) > 0 Then
            Session("NumeroRegistrazione") = Val(Request.Item("NumeroRegistrazione"))
        End If


        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        If Request.Item("NONMENU") <> "OK" Then
            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Else
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "V", "", "")

        End If

        Lbl_Importo.Text = "<table style=""float: right;""><td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totaledocumento.jpg')""><font color=white><div id=""my_totdoc"">0,00</div></font></td>" & _
                     "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalenetto.jpg')""><font color=white><div id=""my_totrit"">0,00</div></font></td>" & _
                      "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalepagato.jpg')""><font color=white><div id=""my_totnetto"">0,00</div></font></td></table>"

        REM Btn_Cancella.Attributes.Add("onclick", "return Conferma();")


        Dim ListComuni As New ClsComune

        ListComuni.UpDateDropBox(Session("DC_OSPITE"), DD_Comune)

        Dim ListaRegioni As New ClsUSL

        ListaRegioni.UpDateDropBox(Session("DC_OSPITE"), DD_Regione)



        TabIVA.Clear()
        TabIVA.Columns.Clear()
        TabIVA.Columns.Add("Imponibile", GetType(String))
        TabIVA.Columns.Add("Iva", GetType(String))
        TabIVA.Columns.Add("Imposta", GetType(String))
        TabIVA.Columns.Add("Detraibile", GetType(String))
        TabIVA.Columns.Add("Sottoconto", GetType(String))
        TabIVA.Columns.Add("DareAvere", GetType(String))
        TabIVA.Columns.Add("Descrizione", GetType(String))

        Dim myriga1 As System.Data.DataRow = TabIVA.NewRow()
        myriga1(0) = 0
        myriga1(1) = 0
        TabIVA.Rows.Add(myriga1)
        ViewState("TABELLAIVA") = TabIVA

        Call BindGridIva()

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))

        Dim myriga As System.Data.DataRow = TabCR.NewRow()
        myriga(3) = 0
        myriga(4) = 0
        myriga(8) = 0
        myriga(9) = 0
        myriga(10) = 0
        myriga(11) = 0
        TabCR.Rows.Add(myriga)
        ViewState("TABELLACR") = TabCR
        Call BindGridCR()



        TabRT.Clear()
        TabRT.Columns.Clear()
        TabRT.Columns.Add("Imponibile", GetType(String))
        TabRT.Columns.Add("TRitenuta", GetType(String))
        TabRT.Columns.Add("Ritenuta", GetType(String))
        TabRT.Columns.Add("SottocontoDare", GetType(String))
        TabRT.Columns.Add("SottocontoAvere", GetType(String))
        TabRT.Columns.Add("Descrizione", GetType(String))
        Dim myrigaR As System.Data.DataRow = TabRT.NewRow()
        myrigaR(2) = 0
        TabRT.Rows.Add(myrigaR)
        ViewState("TABELLART") = TabRT
        Call BindGridRT()



        Dim x As New Cls_TipoPagamento

        x.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)


        Dim x1 As New Cls_CausaleContabile

        x1.UpDateDropBoxDoc(Session("DC_TABELLE"), Dd_CausaleContabile)


        Dim x2 As New Cls_CentroServizio

        x2.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Dim x3 As New Cls_Leasing

        x3.UpDateDropBox(Session("DC_GENERALE"), DD_Leasing)


        If Request.Item("NUMEROREGISTRAZIONETEMP") > 0 Then
            Dim DatiGenerali As New Cls_DatiGenerali

            DatiGenerali.LeggiDati(Session("DC_TABELLE"))

            If DatiGenerali.StampaDaGestioneDocumenti = 1 Then
                Btn_Stampa.Visible = True
            End If
        End If




        Call PulisciRegistrazione(True)
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        If Request.Item("Data") <> "" Then
            Dim DataCopia As String
            DataCopia = Request.Item("Data") & "00000000"

            Try
                Txt_DataRegistrazione.Text = Format(DateSerial(Val(Mid(DataCopia, 1, 4)), Val(Mid(DataCopia, 5, 2)), Val(Mid(DataCopia, 7, 2))), "dd/MM/yyyy")

                Dd_CausaleContabile.SelectedValue = Request.Item("CausaleContabile")
            Catch ex As Exception

            End Try
        End If



        If Val(Session("NumeroRegistrazione")) > 0 Then
            Dim xDoc As New Cls_MovimentoContabile
            xDoc.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))

            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), xDoc.CausaleContabile)


            If xDoc.NumeroRegistrazione > 0 Then
                Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
                Call leggiregistrazione()
            End If
        End If
        If Request.Item("NUMEROREGISTRAZIONETEMP") > 0 Then
            Dim xDoc As New Cls_MovimentoContabile
            xDoc.Leggi(Session("DC_GENERALE"), Val(Request.Item("NUMEROREGISTRAZIONETEMP")), True)

            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), xDoc.CausaleContabile)


            If xDoc.NumeroRegistrazione > 0 Then
                Txt_Numero.Text = Request.Item("NUMEROREGISTRAZIONETEMP")
                Call leggiregistrazione()

                Call UpTableCR()
                Call UpTableIVA()
                Call UpTableRT()
                Call RicalcolaImportiDocumento()


                Dim MyJs As String
                Dim XTI As New Cls_Legami

                MyJs = "$(document).ready(function() { setTimeout( function() { "
                MyJs = MyJs & "$('#my_totdoc').html('" & Format(TotaleDocumento(), "#,##0.00") & "');  "
                MyJs = MyJs & "$('#my_totrit').html('" & Format(TotaleDocumento() - TotaleRitenuta(), "#,##0.00") & "');  "
                MyJs = MyJs & "$('#my_totnetto').html('" & Format(XTI.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text)), "#,##0.00") & "');  "
                MyJs = MyJs & " ), 500);"
                MyJs = MyJs & "});"

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Vs_ITD", MyJs, True)


                Txt_Numero.Text = 0
                Exit Sub
            End If
        End If

        Call RicalcolaImportiDocumento()



    End Sub

    Protected Sub GridIVA_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridIVA.RowCancelingEdit
        Lbl_ErroriIVA.Text = ""
        GridIVA.EditIndex = -1
        Call BindGridIva()
    End Sub

    Private Sub InserisciRigaIVA()

        If Dd_CausaleContabile.SelectedValue.Trim = "" Then
            Exit Sub
        End If
        Lbl_ErroriIVA.Text = ""

        UpTableIVA()

        Tabella = ViewState("TABELLAIVA")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        Dim Indice As Long
        Dim Ks As New Cls_CausaleContabile
        Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If Ks.Tipo = "R" Then
            Indice = 1
        Else
            Indice = 2
        End If

        Dim xS As New Cls_Pianodeiconti

        xS.Mastro = Ks.Righe(Indice).Mastro
        xS.Conto = Ks.Righe(Indice).Conto
        xS.Sottoconto = Ks.Righe(Indice).Sottoconto

        xS.Decodfica(Session("DC_GENERALE"))
        myriga(0) = 0
        myriga(2) = 0

        If Ks.Detraibilita <> "" Then
            myriga(3) = Ks.Detraibilita
        End If

        myriga(4) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

        myriga(5) = Ks.Righe(Indice).DareAvere

        Tabella.Rows.Add(myriga)

        ViewState("TABELLAIVA") = Tabella
        Call BindGridIva()
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub GridIVA_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridIVA.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaIVA()
        End If
        EseguiJS()
    End Sub

    Protected Sub GridIVA_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridIVA.RowCreated

    End Sub





    Protected Sub GridIVA_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridIVA.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TxtImponibileIVA As TextBox = DirectCast(e.Row.FindControl("TxtImponibileIVA"), TextBox)
            Dim DDIva As DropDownList = DirectCast(e.Row.FindControl("DDIva"), DropDownList)
            Dim TxtImpostaIVA As TextBox = DirectCast(e.Row.FindControl("TxtImpostaIVA"), TextBox)
            Dim DDDetraibileIVA As DropDownList = DirectCast(e.Row.FindControl("DDDetraibileIVA"), DropDownList)
            Dim TxtSottocontoIVA As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoIVA"), TextBox)

            Dim RBDareIVA As RadioButton = DirectCast(e.Row.FindControl("RBDareIVA"), RadioButton)
            Dim RBAvereIVA As RadioButton = DirectCast(e.Row.FindControl("RBAvereIVA"), RadioButton)
            Dim TxtDescrizioneIVA As TextBox = DirectCast(e.Row.FindControl("TxtDescrizioneIVA"), TextBox)

            If Not IsNothing(DDIva) Then
                Dim x As New Cls_IVA

                If Val(Txt_Numero.Text) = 0 Then
                    x.UpDateDropBoxSoloInUso(Session("DC_TABELLE"), DDIva)
                Else
                    x.UpDateDropBox(Session("DC_TABELLE"), DDIva)
                End If

            End If

            If Not IsNothing(DDDetraibileIVA) Then
                Dim x As New ClsDetraibilita
                x.UpDateDropBox(Session("DC_TABELLE"), DDDetraibileIVA)
            End If

            Tabella = ViewState("TABELLAIVA")

            If Tabella.Rows.Count > 0 Then
                If Not IsNothing(TxtImponibileIVA) Then
                    TxtImponibileIVA.Text = Tabella.Rows(e.Row.DataItemIndex).Item(0).ToString
                End If
                If Not IsNothing(TxtImpostaIVA) Then

                    TxtImpostaIVA.Text = Tabella.Rows(e.Row.DataItemIndex).Item("Imposta").ToString
                End If
                If Not IsNothing(RBDareIVA) Then
                    If Tabella.Rows(e.Row.DataItemIndex).Item("DareAvere").ToString = "D" Then
                        RBDareIVA.Checked = True
                        RBAvereIVA.Checked = False
                    Else
                        RBDareIVA.Checked = False
                        RBAvereIVA.Checked = True
                    End If
                End If
                If Not IsNothing(DDIva) Then
                    DDIva.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item("Iva").ToString
                End If
                If Not IsNothing(DDDetraibileIVA) Then
                    DDDetraibileIVA.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item("Detraibile").ToString
                End If
                If Not IsNothing(TxtSottocontoIVA) Then
                    TxtSottocontoIVA.Text = Tabella.Rows(e.Row.DataItemIndex).Item("Sottoconto").ToString
                End If
                If Not IsNothing(TxtDescrizioneIVA) Then
                    TxtDescrizioneIVA.Text = Tabella.Rows(e.Row.DataItemIndex).Item("Descrizione").ToString
                End If
            End If
        End If
    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Protected Sub GridIVA_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GridIVA.RowDeleted

    End Sub

    Protected Sub GridIVA_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridIVA.RowDeleting
        Lbl_ErroriIVA.Text = ""
        Tabella = ViewState("TABELLAIVA")

        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try

        ViewState("TABELLAIVA") = Tabella
        Lbl_ErroriIVA.Text = ""
        Call BindGridIva()
    End Sub

    Protected Sub GridIVA_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridIVA.RowEditing
        GridIVA.EditIndex = e.NewEditIndex
        Call BindGridIva()
        Lbl_ErroriIVA.Text = ""
    End Sub

    Protected Sub GridIVA_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GridIVA.RowUpdated
        Dim k As Integer
        k = 1
        Lbl_ErroriIVA.Text = ""
    End Sub

    Protected Sub GridIVA_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridIVA.RowUpdating
        'Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("TxtImponibileIVA"), TextBox)
        'Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("DDIva"), DropDownList)
        'Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("TxtImpostaIVA"), TextBox)
        'Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("DDDetraibileIVA"), DropDownList)
        'Dim TxtSottocontoIVA As TextBox = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("TxtSottocontoIVA"), TextBox)

        'Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("RBDareIVA"), RadioButton)
        'Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("RBAvereIVA"), RadioButton)
        'Dim TxtDescrizioneIVA As TextBox = DirectCast(GridIVA.Rows(e.RowIndex).FindControl("TxtDescrizioneIVA"), TextBox)

        'Dim x As New Cls_ControllaSottoconto
        'Lbl_ErroriIVA.Text = ""
        'If Not x.ControllaSottoconto(TxtSottocontoIVA.Text) Then

        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreIVA1", "VisualizzaErrore('Sottoconto errato');", True)
        '    Dim MyJs As String

        '    MyJs = "$(document).ready(function() { $('#" & TxtImponibileIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileIVA.ClientID & "').val(), true, true); } );   $('#" & TxtImponibileIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileIVA.ClientID & "').val(),2); $('#" & TxtImponibileIVA.ClientID & "').val(ap); }); });"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaIV", MyJs, True)

        '    MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImpostaIVA.ClientID & "').val(), true, true); } );    $('#" & TxtImpostaIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImpostaIVA.ClientID & "').val(),2); $('#" & TxtImpostaIVA.ClientID & "').val(ap); }); });"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaImIVA", MyJs, True)

        '    MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoIVA.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizScIVA", MyJs, True)        

        '    Exit Sub
        'End If

        'If TxtImpostaIVA.Text = "" Then
        '    TxtImpostaIVA.Text = "0"
        'End If
        'If TxtImponibileIVA.Text = "" Then
        '    TxtImponibileIVA.Text = "0"
        'End If

        'Tabella = ViewState("TABELLAIVA")
        'Dim row = GridIVA.Rows(e.RowIndex)

        'Tabella.Rows(row.DataItemIndex).Item(0) = TxtImponibileIVA.Text
        'Tabella.Rows(row.DataItemIndex).Item(1) = DDIva.Text
        'Tabella.Rows(row.DataItemIndex).Item(2) = TxtImpostaIVA.Text
        'Tabella.Rows(row.DataItemIndex).Item(3) = DDDetraibileIVA.SelectedValue
        'Tabella.Rows(row.DataItemIndex).Item(4) = TxtSottocontoIVA.Text
        'If RBDareIVA.Checked = True Then
        '    Tabella.Rows(row.DataItemIndex).Item(5) = "D"
        'Else
        '    Tabella.Rows(row.DataItemIndex).Item(5) = "A"
        'End If
        'Tabella.Rows(row.DataItemIndex).Item(6) = TxtDescrizioneIVA.Text

        'ViewState("TABELLAIVA") = Tabella

        'GridIVA.EditIndex = -1
        'Call BindGridIva()

        'Dim xL As New Cls_CausaleContabile

        'xL.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        'If xL.VenditaAcquisti = "A" Then
        '    Dim dT As New ClsDetraibilita

        '    dT.Codice = DDDetraibileIVA.SelectedValue
        '    dT.Leggi(Session("DC_TABELLE"))

        '    Dim Detraibile As Double
        '    Dim Indetraibile As Double
        '    Detraibile = Modulo.MathRound(dT.Aliquota * Modulo.MathRound(CDbl(TxtImpostaIVA.Text), 2), 2)
        '    Indetraibile = CDbl(TxtImpostaIVA.Text) - Detraibile

        '    Dim Prorata As Long
        '    Dim XP As New Cls_Prorata

        '    Prorata = 0        
        '    XP.Anno = Val(Txt_AnnoProtocollo.Text) - 1
        '    XP.Leggi(Session("DC_TABELLE"))
        '    Prorata = XP.DefinitivoPercentuale

        '    If dT.Prorata = 1 Then
        '        Indetraibile = Indetraibile - Modulo.MathRound(Indetraibile * Modulo.MathRound(Prorata, 2), 2)
        '    End If

        '    If dT.Automatico = "S" Then
        '        If dT.GiroACosto = "S" Then
        '            Tabella = ViewState("TABELLACR")


        '            Dim myriga As System.Data.DataRow = Tabella.NewRow()

        '            myriga(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = True Then
        '                myriga(1) = "D"
        '            Else
        '                myriga(1) = "A"
        '            End If
        '            myriga(2) = "N"
        '            myriga(3) = 0
        '            myriga(4) = Indetraibile
        '            myriga(7) = "Giroconto per IVA Indetraibile"
        '            Tabella.Rows.Add(myriga)


        '            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

        '            myriga1(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = True Then
        '                myriga1(1) = "D"
        '            Else
        '                myriga1(1) = "A"
        '            End If
        '            myriga1(2) = "N"
        '            myriga1(3) = 0

        '            Dim xS As New Cls_Pianodeiconti

        '            xS.Mastro = dT.IVAGiroACostoMastro
        '            xS.Conto = dT.IVAGiroACostoConto
        '            xS.Sottoconto = dT.IVAGiroACostoSottoconto

        '            xS.Decodfica(Session("DC_GENERALE"))

        '            myriga1(4) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
        '            myriga1(7) = "Giroconto a IVA Indetraibile"
        '            Tabella.Rows.Add(myriga1)

        '            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()

        '            myriga2(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = True Then
        '                myriga2(1) = "D"
        '            Else
        '                myriga2(1) = "A"
        '            End If
        '            myriga2(2) = "N"
        '            myriga2(3) = 0

        '            Dim xS2 As New Cls_Pianodeiconti

        '            xS2.Mastro = dT.IVAGiroACostoMastro
        '            xS2.Conto = dT.IVAGiroACostoConto
        '            xS2.Sottoconto = dT.IVAGiroACostoSottoconto

        '            xS2.Decodfica(Session("DC_GENERALE"))

        '            myriga2(4) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS2.Descrizione
        '            myriga2(7) = "Giroconto a Costi"
        '            Tabella.Rows.Add(myriga2)

        '            ViewState("TABELLACR") = Tabella

        '            Call BindGridCR()
        '        Else
        '            Tabella = ViewState("TABELLACR")


        '            Dim myriga As System.Data.DataRow = Tabella.NewRow()

        '            myriga(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = False Then
        '                myriga(1) = "D"
        '            Else
        '                myriga(1) = "A"
        '            End If
        '            myriga(2) = "N"
        '            myriga(3) = 0
        '            myriga(4) = Indetraibile
        '            myriga(7) = "Giroconto dell'IVA a costi"
        '            Tabella.Rows.Add(myriga)

        '            ViewState("TABELLACR") = Tabella
        '            Call BindGridCR()
        '        End If
        '    Else
        '        If dT.GiroACosto = "S" Then
        '            Tabella = ViewState("TABELLACR")


        '            Dim myriga As System.Data.DataRow = Tabella.NewRow()

        '            myriga(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = True Then
        '                myriga(1) = "D"
        '            Else
        '                myriga(1) = "A"
        '            End If
        '            myriga(2) = "N"
        '            myriga(3) = 0
        '            myriga(4) = Indetraibile
        '            myriga(7) = "Giroconto per IVA Indetraibile"
        '            Tabella.Rows.Add(myriga)


        '            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

        '            myriga1(0) = TxtSottocontoIVA.Text
        '            If RBDareIVA.Checked = True Then
        '                myriga1(1) = "D"
        '            Else
        '                myriga1(1) = "A"
        '            End If
        '            myriga1(2) = "N"
        '            myriga1(3) = 0

        '            Dim xS As New Cls_Pianodeiconti

        '            xS.Mastro = dT.IVAGiroACostoMastro
        '            xS.Conto = dT.IVAGiroACostoConto
        '            xS.Sottoconto = dT.IVAGiroACostoSottoconto

        '            xS.Decodfica(Session("DC_GENERALE"))

        '            myriga1(4) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
        '            myriga1(7) = "Giroconto a IVA Indetraibile"
        '            Tabella.Rows.Add(myriga1)



        '            ViewState("TABELLACR") = Tabella

        '            Call BindGridCR()
        '        End If
        '    End If
        'End If


        'Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub GridCostiRicavi_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridCostiRicavi.RowCancelingEdit
        GridCostiRicavi.EditIndex = -1
        Call BindGridCR()
        Lbl_ErroriCR.Text = ""
    End Sub

    Private Sub InserisciCostiRicavi(Optional ByVal Riga As Integer = 0)
        Lbl_ErroriCR.Text = ""

        If Dd_CausaleContabile.SelectedValue = "" Then
            Exit Sub
        End If


        UpTableCR()

        Tabella = ViewState("TABELLACR")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        Dim Indice As Long
        Dim Ks As New Cls_CausaleContabile
        Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)


        If Ks.Tipo = "R" Then
            Indice = 2
        Else
            Indice = 1
        End If
        If Riga > 0 Then
            If Not IsNothing(Ks.Righe(Riga - 1)) Then
                Indice = Riga - 1
            End If
        End If

        Dim xS As New Cls_Pianodeiconti

        xS.Mastro = Ks.Righe(Indice).Mastro
        xS.Conto = Ks.Righe(Indice).Conto
        xS.Sottoconto = Ks.Righe(Indice).Sottoconto

        xS.Decodfica(Session("DC_GENERALE"))

        myriga(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

        myriga(1) = Ks.Righe(Indice).DareAvere




        myriga(3) = 0
        myriga(4) = 0

        myriga(11) = Indice + 1

        Dim k As New Cls_ClienteFornitore

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 And Riga = 0 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
            k.MastroCliente = Mastro
            k.ContoCliente = Conto
            k.SottoContoCliente = Sottoconto
            k.Leggi(Session("DC_OSPITE"))
            If k.Nome = "" Then
                k.MastroCliente = 0
                k.ContoCliente = 0
                k.SottoContoCliente = 0
                k.MastroFornitore = Mastro
                k.ContoFornitore = Conto
                k.SottoContoFornitore = Sottoconto
                k.Leggi(Session("DC_OSPITE"))

                If k.MastroCosto > 0 Then
                    Dim xS1 As New Cls_Pianodeiconti

                    xS1.Mastro = k.MastroCosto
                    xS1.Conto = k.ContoCosto
                    xS1.Sottoconto = k.SottocontoCosto
                    xS1.Decodfica(Session("DC_GENERALE"))
                    myriga(0) = xS1.Mastro & " " & xS1.Conto & " " & xS1.Sottoconto & " " & xS1.Descrizione
                End If
            Else
                If k.MastroRicavo > 0 Then
                    Dim xS2 As New Cls_Pianodeiconti
                    xS2.Mastro = k.MastroRicavo
                    xS2.Conto = k.ContoRicavo
                    xS2.Sottoconto = k.SottocontoRicavo
                    xS2.Decodfica(Session("DC_GENERALE"))
                    myriga(0) = xS2.Mastro & " " & xS2.Conto & " " & xS2.Sottoconto & " " & xS2.Descrizione
                End If
            End If
        End If
        myriga(6) = Txt_ClienteFornitore.Text
        Tabella.Rows.Add(myriga)
        ViewState("TABELLACR") = Tabella
        Call BindGridCR()
    End Sub
    Protected Sub GridCostiRicavi_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridCostiRicavi.RowCommand
        If (e.CommandName = "Inserisci") Then
            InserisciCostiRicavi()
        End If
        EseguiJS()
    End Sub

    Protected Sub GridCostiRicavi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridCostiRicavi.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim TxtSottocontoCR As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoCR"), TextBox)

            Dim RBDareCR As RadioButton = DirectCast(e.Row.FindControl("RBDareCR"), RadioButton)
            Dim RBAvereCR As RadioButton = DirectCast(e.Row.FindControl("RBAvereCR"), RadioButton)
            Dim RBPorataSiCR As RadioButton = DirectCast(e.Row.FindControl("RBPorataSiCR"), RadioButton)
            Dim RBPorataNoCR As RadioButton = DirectCast(e.Row.FindControl("RBPorataNoCR"), RadioButton)

            Dim TxtQuantitaCR As TextBox = DirectCast(e.Row.FindControl("TxtQuantitaCR"), TextBox)
            Dim TxtImportoCR As TextBox = DirectCast(e.Row.FindControl("TxtImportoCR"), TextBox)
            Dim DDIvaCr As DropDownList = DirectCast(e.Row.FindControl("DDIvaCr"), DropDownList)
            Dim TxtSottocontoControPartitaCR As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoControPartitaCR"), TextBox)
            Dim TxtDescrizioneCr As TextBox = DirectCast(e.Row.FindControl("TxtDescrizioneCr"), TextBox)
            Dim TxtAnnoCr As TextBox = DirectCast(e.Row.FindControl("TxtAnnoCr"), TextBox)
            Dim DDMeseCr As DropDownList = DirectCast(e.Row.FindControl("DDMeseCr"), DropDownList)
            Dim Txt_RigaDaCausale As TextBox = DirectCast(e.Row.FindControl("Txt_RigaDaCausale"), TextBox)
            Dim Txt_RigaRegistrazione As TextBox = DirectCast(e.Row.FindControl("Txt_RigaRegistrazione"), TextBox)

            Dim DDCentroServizio As DropDownList = DirectCast(e.Row.FindControl("DDCentroServizio"), DropDownList)
            Dim Txt_TipoExtra As TextBox = DirectCast(e.Row.FindControl("Txt_TipoExtra"), TextBox)

            Lbl_ErroriCR.Text = ""

            If Not IsNothing(DDIvaCr) Then
                Dim x As New Cls_IVA


                If Val(Txt_Numero.Text) = 0 Then
                    x.UpDateDropBoxSoloInUso(Session("DC_TABELLE"), DDIvaCr)
                Else
                    x.UpDateDropBox(Session("DC_TABELLE"), DDIvaCr)
                End If
            End If

            Dim Param As New Cls_DatiGenerali

            Param.LeggiDati(Session("DC_TABELLE"))



            If Param.TipoVisualizzazione = 1 Then
                RBAvereCR.Text = "A"
                RBDareCR.Text = "D"
                DDIvaCr.Width = 120
            End If



            If Not IsNothing(DDCentroServizio) Then
                Dim x As New Cls_CentroServizio

                x.UpDateDropBox(Session("DC_OSPITE"), DDCentroServizio)
            End If

            If Not IsNothing(TxtQuantitaCR) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtQuantitaCR.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtQuantitaCR.ClientID & "').val(), true, true); } );    $('#" & TxtQuantitaCR.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtQuantitaCR.ClientID & "').val(),2); $('#" & TxtQuantitaCR.ClientID & "').val(ap); }); });"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaCR1", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaCR1", MyJs, True)
            End If

            If Not IsNothing(TxtImportoCR) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImportoCR.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImportoCR.ClientID & "').val(), true, true); } );     $('#" & TxtImportoCR.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImportoCR.ClientID & "').val(),2); $('#" & TxtImportoCR.ClientID & "').val(ap); }); });"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaCR2", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaCR2", MyJs, True)
            End If

            If Not IsNothing(TxtSottocontoCR) Then

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoCR.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizSCCR", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizSCCR", MyJs, True)
            End If
            If Not IsNothing(TxtSottocontoControPartitaCR) Then

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoControPartitaCR.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizSCCR1", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizSCCR1", MyJs, True)
            End If
            Tabella = ViewState("TABELLACR")

            If Tabella.Rows.Count > 0 Then
                If Not IsNothing(TxtSottocontoCR) Then
                    TxtSottocontoCR.Text = Tabella.Rows(e.Row.DataItemIndex).Item(0).ToString
                End If
                If Not IsNothing(RBDareCR) Then
                    If Tabella.Rows(e.Row.DataItemIndex).Item(1).ToString = "D" Then
                        RBDareCR.Checked = True
                    End If
                    If Tabella.Rows(e.Row.DataItemIndex).Item(1).ToString = "A" Then
                        RBAvereCR.Checked = True
                    End If
                End If
                If Not IsNothing(RBPorataSiCR) Then
                    If Tabella.Rows(e.Row.DataItemIndex).Item(2).ToString = "S" Then
                        RBPorataSiCR.Checked = True
                        RBPorataNoCR.Checked = False
                    Else
                        RBPorataSiCR.Checked = False
                        RBPorataNoCR.Checked = True
                    End If
                End If
                If Not IsNothing(TxtQuantitaCR) Then
                    TxtQuantitaCR.Text = Tabella.Rows(e.Row.DataItemIndex).Item(3).ToString
                End If
                If Not IsNothing(TxtImportoCR) Then
                    TxtImportoCR.Text = Tabella.Rows(e.Row.DataItemIndex).Item(4).ToString
                End If
                If Not IsNothing(DDIvaCr) Then
                    DDIvaCr.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item(5).ToString
                End If
                If Not IsNothing(TxtSottocontoControPartitaCR) Then
                    TxtSottocontoControPartitaCR.Text = Tabella.Rows(e.Row.DataItemIndex).Item(6).ToString
                End If
                If Not IsNothing(TxtDescrizioneCr) Then
                    TxtDescrizioneCr.Text = Tabella.Rows(e.Row.DataItemIndex).Item(7).ToString
                End If
                If Not IsNothing(TxtAnnoCr) Then
                    TxtAnnoCr.Text = Tabella.Rows(e.Row.DataItemIndex).Item(8).ToString
                End If
                If Not IsNothing(DDMeseCr) Then
                    DDMeseCr.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item(9).ToString
                End If
                If Not IsNothing(Txt_RigaRegistrazione) Then
                    Txt_RigaRegistrazione.Text = Tabella.Rows(e.Row.DataItemIndex).Item(10).ToString
                End If
                If Not IsNothing(Txt_RigaDaCausale) Then
                    Txt_RigaDaCausale.Text = Tabella.Rows(e.Row.DataItemIndex).Item(11).ToString
                End If

                If Not IsNothing(DDCentroServizio) Then
                    DDCentroServizio.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item(12).ToString
                End If

                If Not IsNothing(Txt_TipoExtra) Then
                    Txt_TipoExtra.Text = Tabella.Rows(e.Row.DataItemIndex).Item(13).ToString
                End If


            End If
        End If
    End Sub

    Protected Sub GridCostiRicavi_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GridCostiRicavi.RowDeleted

    End Sub

    Protected Sub GridCostiRicavi_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridCostiRicavi.RowDeleting

        Lbl_ErroriCR.Text = ""
        Tabella = ViewState("TABELLACR")

        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)

                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)

                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try
        ViewState("TABELLACR") = Tabella

        Call BindGridCR()

    End Sub

    Protected Sub GridCostiRicavi_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridCostiRicavi.RowEditing
        GridCostiRicavi.EditIndex = e.NewEditIndex
        Call BindGridCR()
        Lbl_ErroriCR.Text = ""
    End Sub

    Protected Sub GridCostiRicavi_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GridCostiRicavi.RowUpdated
        Lbl_ErroriCR.Text = ""
    End Sub


    Protected Sub GridCostiRicavi_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridCostiRicavi.RowUpdating
        Dim TxtSottocontoCR As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtSottocontoCR"), TextBox)

        Dim RBDareCR As RadioButton = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("RBDareCR"), RadioButton)
        Dim RBAvereCR As RadioButton = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("RBAvereCR"), RadioButton)
        Dim RBPorataSiCR As RadioButton = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("RBPorataSiCR"), RadioButton)
        Dim RBPorataNoCR As RadioButton = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("RBPorataNoCR"), RadioButton)

        Dim TxtQuantitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtQuantitaCR"), TextBox)
        Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtImportoCR"), TextBox)
        Dim DDIvaCr As DropDownList = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("DDIvaCr"), DropDownList)
        Dim TxtSottocontoControPartitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtSottocontoControPartitaCR"), TextBox)
        Dim TxtDescrizioneCr As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtDescrizioneCr"), TextBox)
        Dim TxtAnnoCr As TextBox = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("TxtAnnoCr"), TextBox)

        Dim DDMeseCr As DropDownList = DirectCast(GridCostiRicavi.Rows(e.RowIndex).FindControl("DDMeseCr"), DropDownList)


        Dim x As New Cls_ControllaSottoconto
        Lbl_ErroriCR.Text = ""
        If Not x.ControllaSottoconto(TxtSottocontoCR.Text) Then

            Dim MyJs As String

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoCR.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizSCCR", MyJs, True)

            MyJs = "$(document).ready(function() { $('#" & TxtQuantitaCR.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtQuantitaCR.ClientID & "').val(), true, true); } );    $('#" & TxtQuantitaCR.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtQuantitaCR.ClientID & "').val(),2); $('#" & TxtQuantitaCR.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaCR1", MyJs, True)

            MyJs = "$(document).ready(function() { $('#" & TxtImportoCR.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImportoCR.ClientID & "').val(), true, true); } );     $('#" & TxtImportoCR.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImportoCR.ClientID & "').val(),2); $('#" & TxtImportoCR.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaCR2", MyJs, True)

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoControPartitaCR.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizSCCR1", MyJs, True)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Sottoconto errato');", True)
            Exit Sub
        End If

        Tabella = ViewState("TABELLACR")
        Dim row = GridCostiRicavi.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = TxtSottocontoCR.Text
        If RBDareCR.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(1) = "D"
        End If
        If RBAvereCR.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(1) = "A"
        End If
        If RBPorataSiCR.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(2) = "S"
        End If
        If RBPorataNoCR.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(2) = "N"
        End If

        Tabella.Rows(row.DataItemIndex).Item(3) = TxtQuantitaCR.Text
        Tabella.Rows(row.DataItemIndex).Item(4) = TxtImportoCR.Text
        Tabella.Rows(row.DataItemIndex).Item(5) = DDIvaCr.SelectedValue
        Tabella.Rows(row.DataItemIndex).Item(6) = TxtSottocontoControPartitaCR.Text
        Tabella.Rows(row.DataItemIndex).Item(7) = TxtDescrizioneCr.Text
        Tabella.Rows(row.DataItemIndex).Item(8) = Val(TxtAnnoCr.Text)
        Tabella.Rows(row.DataItemIndex).Item(9) = Val(DDMeseCr.SelectedValue)

        ViewState("TABELLACR") = Tabella

        GridCostiRicavi.EditIndex = -1
        Call BindGridCR()
    End Sub




    Protected Sub GridRitenuta_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridRitenuta.RowCancelingEdit
        GridRitenuta.EditIndex = -1
        Call BindGridRT()
        Lbl_RitenutaEr.Text = ""
    End Sub
    Private Sub InserisciRigaRitenuta()
        Lbl_RitenutaEr.Text = ""
        Tabella = ViewState("TABELLART")

        Dim Ks As New Cls_CausaleContabile
        Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = 0
        myriga(2) = 0

        If Not IsNothing(Ks.Righe(8)) Then
            If Ks.Righe(8).DareAvere = "D" Then
                Dim XDec As New Cls_Pianodeiconti
                XDec.Mastro = Ks.Righe(8).Mastro
                XDec.Conto = Ks.Righe(8).Conto
                XDec.Sottoconto = Ks.Righe(8).Sottoconto
                XDec.Decodfica(Session("DC_GENERALE"))
                myriga(3) = Ks.Righe(8).Mastro & " " & Ks.Righe(8).Conto & " " & Ks.Righe(8).Sottoconto & " " & XDec.Descrizione

                myriga(4) = Txt_ClienteFornitore.Text
            Else
                Dim XDec As New Cls_Pianodeiconti
                XDec.Mastro = Ks.Righe(8).Mastro
                XDec.Conto = Ks.Righe(8).Conto
                XDec.Sottoconto = Ks.Righe(8).Sottoconto
                XDec.Decodfica(Session("DC_GENERALE"))
                myriga(4) = Ks.Righe(8).Mastro & " " & Ks.Righe(8).Conto & " " & Ks.Righe(8).Sottoconto & " " & XDec.Descrizione

                myriga(3) = Txt_ClienteFornitore.Text
            End If
        End If

        Tabella.Rows.Add(myriga)

        ViewState("TABELLART") = Tabella
        Call BindGridRT()

    End Sub

    Protected Sub GridRitenuta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridRitenuta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaRitenuta()
        End If
        Call EseguiJS()
    End Sub

    Protected Sub GridRitenuta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridRitenuta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TxtImponibileRT As TextBox = DirectCast(e.Row.FindControl("TxtImponibileRT"), TextBox)
            Dim DDTRitenutaRt As DropDownList = DirectCast(e.Row.FindControl("DDTRitenutaRt"), DropDownList)
            Dim TxtRitenutaRT As TextBox = DirectCast(e.Row.FindControl("TxtRitenutaRT"), TextBox)
            Dim TxtSottocontoDareRT As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoDareRT"), TextBox)
            Dim TxtSottocontoAvereRT As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoAvereRT"), TextBox)
            Dim TxtDescrizioneRT As TextBox = DirectCast(e.Row.FindControl("TxtDescrizioneRT"), TextBox)
            Lbl_RitenutaEr.Text = ""

            If Not IsNothing(TxtImponibileRT) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImponibileRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileRT.ClientID & "').val(), true, true); } );     $('#" & TxtImponibileRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileRT.ClientID & "').val(),2); $('#" & TxtImponibileRT.ClientID & "').val(ap); }); });"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRitIm", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
            End If

            If Not IsNothing(DDTRitenutaRt) Then
                Dim x As New ClsRitenuta
                x.UpDateDropBox(Session("DC_TABELLE"), DDTRitenutaRt)
            End If

            If Not IsNothing(TxtRitenutaRT) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtRitenutaRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtRitenutaRT.ClientID & "').val(), true, true); } );      $('#" & TxtRitenutaRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtRitenutaRT.ClientID & "').val(),2); $('#" & TxtRitenutaRT.ClientID & "').val(ap); }); });"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRit", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRit", MyJs, True)
            End If

            If Not IsNothing(TxtSottocontoDareRT) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoDareRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizRTD", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visRTD", MyJs, True)
            End If

            If Not IsNothing(TxtSottocontoAvereRT) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoAvereRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizRTA", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visRTA", MyJs, True)
            End If

            Tabella = ViewState("TABELLART")

            If Tabella.Rows.Count > 0 Then
                If Not IsNothing(TxtImponibileRT) Then
                    TxtImponibileRT.Text = Tabella.Rows(e.Row.DataItemIndex).Item(0).ToString
                End If
                If Not IsNothing(DDTRitenutaRt) Then
                    DDTRitenutaRt.SelectedValue = Tabella.Rows(e.Row.DataItemIndex).Item("TRitenuta").ToString
                End If
                If Not IsNothing(TxtRitenutaRT) Then
                    TxtRitenutaRT.Text = Tabella.Rows(e.Row.DataItemIndex).Item("Ritenuta").ToString
                End If

                If Not IsNothing(TxtSottocontoDareRT) Then
                    TxtSottocontoDareRT.Text = Tabella.Rows(e.Row.DataItemIndex).Item("SottocontoDare").ToString
                End If
                If Not IsNothing(TxtSottocontoAvereRT) Then
                    TxtSottocontoAvereRT.Text = Tabella.Rows(e.Row.DataItemIndex).Item("SottocontoAvere").ToString
                End If
                If Not IsNothing(TxtDescrizioneRT) Then
                    TxtDescrizioneRT.Text = Tabella.Rows(e.Row.DataItemIndex).Item("Descrizione").ToString
                End If
            End If
        End If
    End Sub

    Protected Sub GridRitenuta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridRitenuta.RowDeleting
        Lbl_RitenutaEr.Text = ""
        Tabella = ViewState("TABELLART")

        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)

                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)

                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try

        ViewState("TABELLART") = Tabella
        Lbl_RitenutaEr.Text = ""
        Call BindGridRT()
    End Sub

    Protected Sub GridRitenuta_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridRitenuta.RowEditing
        GridRitenuta.EditIndex = e.NewEditIndex
        Call BindGridRT()
        Lbl_RitenutaEr.Text = ""
    End Sub

    Protected Sub GridRitenuta_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GridRitenuta.RowUpdated
        Dim k As Integer
        k = 1
        Lbl_RitenutaEr.Text = ""
    End Sub

    Protected Sub GridRitenuta_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridRitenuta.RowUpdating
        Dim TxtImponibileRT As TextBox = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("TxtImponibileRT"), TextBox)
        Dim DDTRitenutaRt As DropDownList = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("DDTRitenutaRt"), DropDownList)
        Dim TxtRitenutaRT As TextBox = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("TxtRitenutaRT"), TextBox)
        Dim TxtSottocontoDareRT As TextBox = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("TxtSottocontoDareRT"), TextBox)
        Dim TxtSottocontoAvereRT As TextBox = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("TxtSottocontoAvereRT"), TextBox)
        Dim TxtDescrizioneRT As TextBox = DirectCast(GridRitenuta.Rows(e.RowIndex).FindControl("TxtDescrizioneRT"), TextBox)
        Dim x As New Cls_ControllaSottoconto

        If Not x.ControllaSottoconto(TxtSottocontoDareRT.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreRT1", "VisualizzaErrore('Sottoconto dare errato');", True)

            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileRT.ClientID & "').val(), true, true); } );     $('#" & TxtImponibileRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileRT.ClientID & "').val(),2); $('#" & TxtImponibileRT.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)


            MyJs = "$(document).ready(function() { $('#" & TxtRitenutaRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtRitenutaRT.ClientID & "').val(), true, true); } );      $('#" & TxtRitenutaRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtRitenutaRT.ClientID & "').val(),2); $('#" & TxtRitenutaRT.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRit", MyJs, True)

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoDareRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizRTD", MyJs, True)

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoAvereRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizRTA", MyJs, True)
            Exit Sub
        End If
        If Not x.ControllaSottoconto(TxtSottocontoAvereRT.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreRT2", "VisualizzaErrore('Sottoconto avere errato');", True)
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileRT.ClientID & "').val(), true, true); } );     $('#" & TxtImponibileRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileRT.ClientID & "').val(),2); $('#" & TxtImponibileRT.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)


            MyJs = "$(document).ready(function() { $('#" & TxtRitenutaRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtRitenutaRT.ClientID & "').val(), true, true); } );      $('#" & TxtRitenutaRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtRitenutaRT.ClientID & "').val(),2); $('#" & TxtRitenutaRT.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRit", MyJs, True)

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoDareRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizRTD", MyJs, True)

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottocontoAvereRT.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizRTA", MyJs, True)
            Exit Sub
        End If

        Tabella = ViewState("TABELLART")
        Dim row = GridRitenuta.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = TxtImponibileRT.Text
        Tabella.Rows(row.DataItemIndex).Item(1) = DDTRitenutaRt.Text
        Tabella.Rows(row.DataItemIndex).Item(2) = TxtRitenutaRT.Text
        Tabella.Rows(row.DataItemIndex).Item(3) = TxtSottocontoDareRT.Text
        Tabella.Rows(row.DataItemIndex).Item(4) = TxtSottocontoAvereRT.Text
        Tabella.Rows(row.DataItemIndex).Item(5) = TxtDescrizioneRT.Text


        ViewState("TABELLART") = Tabella

        GridRitenuta.EditIndex = -1
        Call BindGridRT()

        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Numero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Numero.TextChanged
        If Val(Txt_Numero.Text) > 0 Then
            Dim x As New Cls_MovimentoContabile
            x.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), x.CausaleContabile)
            If Ks.RegistroIVA = 0 Then
                Exit Sub
            End If
            If x.NumeroRegistrazione > 0 Then
                Txt_Numero.Enabled = False
                Call leggiregistrazione()
            End If
        End If
    End Sub
    Private Function TotaleImponibile() As Double
        Dim X As New Cls_CausaleContabile
        Dim i As Long
        Dim TotInterno As Double = 0

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        TabIVA = ViewState("TABELLAIVA")
        For i = 0 To TabIVA.Rows.Count - 1
            If CDbl(TabIVA.Rows(i).Item("Imponibile")) > 0 Then
                If TabIVA.Rows(i).Item("DareAvere") <> X.Righe(0).DareAvere Then
                    TotaleImponibile = TotaleImponibile + CDbl(TabIVA.Rows(i).Item("Imponibile"))
                Else
                    TotaleImponibile = TotaleImponibile - CDbl(TabIVA.Rows(i).Item("Imponibile"))
                End If
            End If
        Next

        Return TotaleImponibile
    End Function
    Private Function TotaleDocumento() As Double
        Dim X As New Cls_CausaleContabile
        Dim i As Long
        Dim TotInterno As Double = 0

        If Dd_CausaleContabile.SelectedValue = "" Then
            Return 0
            Exit Function
        End If
        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        TabIVA = ViewState("TABELLAIVA")
        For i = 0 To TabIVA.Rows.Count - 1
            If Not IsDBNull(TabIVA.Rows(i).Item("Imponibile")) Then
                If CDbl(TabIVA.Rows(i).Item("Imponibile")) <> 0 Then
                    If TabIVA.Rows(i).Item("DareAvere") <> X.Righe(0).DareAvere Then
                        TotInterno = TotInterno + CDbl(TabIVA.Rows(i).Item("Imponibile"))
                        TotInterno = TotInterno + CDbl(TabIVA.Rows(i).Item("Imposta"))
                    Else
                        TotInterno = TotInterno - CDbl(TabIVA.Rows(i).Item("Imponibile"))
                        TotInterno = TotInterno - CDbl(TabIVA.Rows(i).Item("Imposta"))
                    End If
                End If
            End If
        Next

        Return TotInterno

    End Function

    Private Function TotaleRitenuta() As Double
        Dim X As New Cls_CausaleContabile
        Dim i As Long
        Dim TotInterno As Double = 0

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        TabRT = ViewState("TABELLART")
        If X.TipoDocumento = "FA" Then
            For i = 0 To TabRT.Rows.Count - 1
                If Not IsDBNull(TabRT.Rows(i).Item("Ritenuta")) Then
                    If TabRT.Rows(i).Item("Ritenuta") <> "" Then
                        If CDbl(TabRT.Rows(i).Item("Ritenuta")) > 0 Then
                            If X.Righe(8).DareAvere = X.Righe(0).DareAvere Then
                                TotInterno = TotInterno + CDbl(TabRT.Rows(i).Item("Ritenuta"))
                            Else
                                TotInterno = TotInterno - CDbl(TabRT.Rows(i).Item("Ritenuta"))
                            End If
                        End If
                    End If
                End If
            Next
        Else
            For i = 0 To TabRT.Rows.Count - 1
                If Not IsDBNull(TabRT.Rows(i).Item("Ritenuta")) Then
                    If TabRT.Rows(i).Item("Ritenuta") <> "" Then
                        If CDbl(TabRT.Rows(i).Item("Ritenuta")) > 0 Then
                            If IsDBNull(X.Righe(8).DareAvere) And IsDBNull(X.Righe(0).DareAvere) Then
                            Else
                                If X.Righe(8).DareAvere <> X.Righe(0).DareAvere Then
                                    TotInterno = TotInterno + CDbl(TabRT.Rows(i).Item("Ritenuta"))
                                Else
                                    TotInterno = TotInterno - CDbl(TabRT.Rows(i).Item("Ritenuta"))
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        End If

        Return TotInterno

    End Function
    Private Function TotaleContabile() As Double
        Dim X As New Cls_CausaleContabile
        Dim i As Long
        Dim TotInterno As Double = 0

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        TabCR = ViewState("TABELLACR")
        For i = 0 To TabCR.Rows.Count - 1
            If Not IsDBNull(TabCR.Rows(i).Item("Importo")) Then
                Try
                    If CDbl(TabCR.Rows(i).Item("Importo")) > 0 Then
                        If campodb(TabCR.Rows(i).Item("DareAvere")) <> X.Righe(0).DareAvere Then
                            TotInterno = TotInterno + CDbl(TabCR.Rows(i).Item("Importo"))
                        Else
                            TotInterno = TotInterno - CDbl(TabCR.Rows(i).Item("Importo"))
                        End If
                    End If
                Catch ex As Exception

                End Try
            End If
        Next

        Return TotInterno

    End Function
    Private Sub RicalcolaImportiDocumento()
        Dim XTI As New Cls_Legami



        Lbl_Importo.Text = "<table style=""float: right;""><td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totaledocumento.jpg')""><font color=white><div id=""my_totdoc"">" & Format(TotaleDocumento(), "#,##0.00") & "</div></font></td>" & _
                           "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalenetto.jpg')""><font color=white><div id=""my_totrit"">" & Format(TotaleDocumento() - TotaleRitenuta(), "#,##0.00") & "</div></font></td>" & _
                            "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalepagato.jpg')""><font color=white><div id=""my_totnetto"">" & Format(Math.Abs(XTI.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text))), "#,##0.00") & "</div></font></td></table>"

        Dim MyJs As String


        MyJs = "$(document).ready(function() { "
        MyJs = MyJs & "$('#my_totdoc').html('" & Format(TotaleDocumento(), "#,##0.00") & "');  "
        MyJs = MyJs & "$('#my_totrit').html('" & Format(TotaleDocumento() - TotaleRitenuta(), "#,##0.00") & "');  "
        MyJs = MyJs & "$('#my_totnetto').html('" & Format(XTI.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text)), "#,##0.00") & "');  "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Vs_ITD", MyJs, True)

    End Sub
    Private Sub SriviRegistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile
        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Val(Txt_Numero.Text))

        x.NumeroRegistrazione = Val(Txt_Numero.Text)
        x.DataRegistrazione = Txt_DataRegistrazione.Text
        x.CausaleContabile = Dd_CausaleContabile.SelectedValue

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        x.Descrizione = Txt_DescrizioneTesta.Text

        If IsDate(Txt_DataDocumento.Text) Then
            x.DataDocumento = Txt_DataDocumento.Text
        Else
            x.DataDocumento = Nothing
        End If
        x.NumeroDocumento = Txt_NumeroDocumento.Text
        x.RegistroIVA = MyCau.RegistroIVA

        x.CausaleContabile = Dd_CausaleContabile.SelectedValue

        x.AnnoProtocollo = Val(Txt_AnnoProtocollo.Text)
        x.NumeroProtocollo = Val(Txt_NumeroProtocollo.Text)
        x.Bis = Txt_BisProtocollo.Text

        x.CodicePagamento = DD_ModalitaPagamento.SelectedValue

        x.AnnoCompetenza = Val(Txt_Anno.Text)
        x.MeseCompetenza = Dd_Mese.SelectedValue
        x.PeriodoRiferimentoRettaAnno = Val(Txt_AnnoRif.Text)
        x.PeriodoRiferimentoRettaMese = DD_MeseRif.SelectedValue
        x.Tipologia = DD_Tipologia.SelectedValue

        If DD_Tipologia.SelectedValue = "C" Then
            x.Tipologia = DD_Tipologia.SelectedValue & DD_Comune.SelectedValue.Replace(" ", "")
        End If

        If DD_Tipologia.SelectedValue = "J" Then
            x.Tipologia = DD_Tipologia.SelectedValue & DD_Comune.SelectedValue.Replace(" ", "")
        End If
        If DD_Tipologia.SelectedValue = "R" Then
            x.Tipologia = DD_Tipologia.SelectedValue & DD_Regione.SelectedValue
        End If

        x.CentroServizio = DD_CentroServizio.SelectedValue

        If IsDate(Txt_DataRif.Text) Then
            x.RifData = Txt_DataRif.Text
        Else
            x.RifData = Nothing
        End If
        x.RifNumero = Txt_RifNumero.Text




        If IsDate(Txt_DataSepa.Text) Then
            x.DataRid = Txt_DataSepa.Text
        Else
            x.DataRid = Nothing
        End If
        x.idRid = Txt_IDSEPA.Text



        If Chk_FatturazioneAnticipata.Checked = True Then
            x.FatturaDiAnticipo = "S"
        Else
            x.FatturaDiAnticipo = ""
        End If

        x.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue
        If RB_IvaSospesaSI.Checked = True Then
            x.IVASospesa = "S"
        Else
            x.IVASospesa = "N"
        End If



        If IsDate(Txt_ExportData.Text) Then
            x.ExportDATA = Txt_ExportData.Text
        Else
            x.ExportDATA = Nothing
        End If

        If Chk_BolloVirtuale.Checked = True Then
            x.BolloVirtuale = 1
        Else
            x.BolloVirtuale = 0
        End If

        x.CodiceCig = DD_Cig.SelectedValue

        x.RegistrazioneRiferimento = val(Txt_RegistrazioneRiferimento.Text)

        For i = 0 To x.Righe.Length - 1
            If Not IsNothing(x.Righe(i)) Then
                x.Righe(i) = Nothing
            End If
        Next


        x.Righe(0) = New Cls_MovimentiContabiliRiga
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        x.Righe(0).MastroPartita = Mastro
        x.Righe(0).ContoPartita = Conto
        x.Righe(0).SottocontoPartita = Sottoconto

        x.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        x.Righe(0).Segno = "+"
        x.Righe(0).Importo = TotaleDocumento()
        x.Righe(0).Tipo = "CF"

        x.Righe(0).Descrizione = ""
        x.Righe(0).RigaDaCausale = 1

        x.Righe(0).MastroContropartita = 0
        x.Righe(0).ContoContropartita = 0
        x.Righe(0).SottocontoContropartita = 0
        x.Righe(0).Numero = Val(Txt_Numero.Text)

        Dim Indice As Long = 1

        TabIVA = ViewState("TABELLAIVA")

        For i = 0 To TabIVA.Rows.Count - 1
            If Not IsDBNull(TabIVA.Rows(i).Item(0)) Then
                x.Righe(Indice) = New Cls_MovimentiContabiliRiga
                x.Righe(Indice).Imponibile = TabIVA.Rows(i).Item(0)
                x.Righe(Indice).CodiceIVA = TabIVA.Rows(i).Item(1)
                x.Righe(Indice).Importo = TabIVA.Rows(i).Item(2)
                x.Righe(Indice).Detraibile = TabIVA.Rows(i).Item(3)
                x.Righe(Indice).Numero = Txt_Numero.Text
                x.Righe(Indice).Tipo = "IV"
                x.Righe(Indice).Segno = "+"

                Vettore = SplitWords(TabIVA.Rows(i).Item(4))

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroPartita = Mastro
                x.Righe(Indice).ContoPartita = Conto
                x.Righe(Indice).SottocontoPartita = Sottoconto
                x.Righe(Indice).DareAvere = TabIVA.Rows(i).Item(5)
                x.Righe(Indice).Descrizione = TabIVA.Rows(i).Item(6)

                If MyCau.Tipo = "R" Then
                    x.Righe(Indice).RigaDaCausale = 2
                Else
                    x.Righe(Indice).RigaDaCausale = 3
                End If

                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto

                Indice = Indice + 1
            End If
        Next

        TabCR = ViewState("TABELLACR")

        For i = 0 To TabCR.Rows.Count - 1
            If Not IsDBNull(TabCR.Rows(i).Item(0)) Then
                x.Righe(Indice) = New Cls_MovimentiContabiliRiga
                Vettore = SplitWords(TabCR.Rows(i).Item(0))

                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))

                x.Righe(Indice).MastroPartita = Mastro
                x.Righe(Indice).ContoPartita = Conto
                x.Righe(Indice).SottocontoPartita = Sottoconto

                x.Righe(Indice).Numero = Txt_Numero.Text
                x.Righe(Indice).Segno = "+"

                x.Righe(Indice).DareAvere = campodb(TabCR.Rows(i).Item(1))
                x.Righe(Indice).Prorata = campodb(TabCR.Rows(i).Item(2))

                x.Righe(Indice).Quantita = campodbN(TabCR.Rows(i).Item(3))

                x.Righe(Indice).Importo = campodbN(TabCR.Rows(i).Item(4))

                If IsDBNull(TabCR.Rows(i).Item(5)) Then
                    x.Righe(Indice).CodiceIVA = ""
                Else
                    x.Righe(Indice).CodiceIVA = TabCR.Rows(i).Item(5)
                End If

                If Not IsDBNull(TabCR.Rows(i).Item(6)) Then
                    Vettore = SplitWords(TabCR.Rows(i).Item(6))

                    If Vettore.Length > 2 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If

                    x.Righe(Indice).MastroContropartita = Mastro
                    x.Righe(Indice).ContoContropartita = Conto
                    x.Righe(Indice).SottocontoContropartita = Sottoconto
                Else
                    x.Righe(Indice).MastroContropartita = 0
                    x.Righe(Indice).ContoContropartita = 0
                    x.Righe(Indice).SottocontoContropartita = 0
                End If

                x.Righe(Indice).Descrizione = TabCR.Rows(i).Item(7)
                If Not IsDBNull(TabCR.Rows(i).Item(8)) Then
                    x.Righe(Indice).AnnoRiferimento = TabCR.Rows(i).Item(8)
                Else
                    x.Righe(Indice).AnnoRiferimento = 0
                End If
                If Not IsDBNull(TabCR.Rows(i).Item(9)) Then
                    x.Righe(Indice).MeseRiferimento = TabCR.Rows(i).Item(9)
                Else
                    x.Righe(Indice).MeseRiferimento = 0
                End If
                x.Righe(Indice).Tipo = ""

                If campodbN(TabCR.Rows(i).Item(10)) = 0 Then
                    x.Righe(Indice).RigaRegistrazione = Indice
                Else
                    x.Righe(Indice).RigaRegistrazione = campodbN(TabCR.Rows(i).Item(10))
                End If
                x.Righe(Indice).RigaDaCausale = campodbN(TabCR.Rows(i).Item(11))
                If x.Righe(Indice).RigaDaCausale = 0 Or x.Righe(Indice).RigaDaCausale = 1 Then
                    If MyCau.Tipo = "R" Then
                        x.Righe(Indice).RigaDaCausale = 3
                    Else
                        x.Righe(Indice).RigaDaCausale = 2
                    End If
                Else
                    x.Righe(Indice).RigaDaCausale = campodbN(TabCR.Rows(i).Item(11))
                End If
                x.Righe(Indice).CentroServizio = campodb(TabCR.Rows(i).Item(12))
                x.Righe(Indice).TipoExtra = campodb(TabCR.Rows(i).Item(13))

                Indice = Indice + 1
            End If
        Next

        TabRT = ViewState("TABELLART")

        For i = 0 To TabRT.Rows.Count - 1

            If Not IsDBNull(TabRT.Rows(i).Item(0)) Then
                If Val(TabRT.Rows(i).Item(0)) > 0 Then
                    x.Righe(Indice) = New Cls_MovimentiContabiliRiga

                    Vettore = SplitWords(TabRT.Rows(i).Item(3))

                    If Vettore.Length > 2 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If

                    x.Righe(Indice).DareAvere = "D"
                    x.Righe(Indice).MastroPartita = Mastro
                    x.Righe(Indice).ContoPartita = Conto
                    x.Righe(Indice).SottocontoPartita = Sottoconto
                    x.Righe(Indice).Imponibile = TabRT.Rows(i).Item(0)
                    x.Righe(Indice).CodiceRitenuta = TabRT.Rows(i).Item(1)
                    x.Righe(Indice).Importo = TabRT.Rows(i).Item(2)
                    x.Righe(Indice).Descrizione = TabRT.Rows(i).Item(5)
                    x.Righe(Indice).Tipo = "RI"
                    x.Righe(Indice).Segno = "+"
                    x.Righe(Indice).Numero = Txt_Numero.Text
                    Indice = Indice + 1

                    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
                    Vettore = SplitWords(TabRT.Rows(i).Item(4))

                    If Vettore.Length > 2 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If

                    x.Righe(Indice).Numero = Txt_Numero.Text
                    x.Righe(Indice).MastroPartita = Mastro
                    x.Righe(Indice).ContoPartita = Conto
                    x.Righe(Indice).SottocontoPartita = Sottoconto
                    x.Righe(Indice).Imponibile = TabRT.Rows(i).Item(0)
                    x.Righe(Indice).CodiceRitenuta = TabRT.Rows(i).Item(1)
                    x.Righe(Indice).Importo = TabRT.Rows(i).Item(2)
                    x.Righe(Indice).Descrizione = TabRT.Rows(i).Item(5)
                    x.Righe(Indice).DareAvere = "A"
                    x.Righe(Indice).Tipo = "RI"
                    x.Righe(Indice).Segno = "+"
                    x.Righe(Indice).RigaDaCausale = 9
                    Indice = Indice + 1
                End If
            End If
        Next
        x.Utente = Session("UTENTE")

        If x.Scrivi(Session("DC_GENERALE"), Txt_Numero.Text) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore in registrazione documento!');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Dim xModalita As New Cls_TipoPagamento

        xModalita.Codice = x.ModalitaPagamento
        xModalita.Leggi(Session("DC_TABELLE"))

        Dim NonScadenze As Boolean = False
        If Val(Txt_Numero.Text) > 0 Then
            Dim Scadenze As New Cls_Scadenziario

            If Scadenze.VerificaScadenzaChiuso(Session("DC_GENERALE"), Val(Txt_Numero.Text)) Then
                Scadenze.EliminaScadenze(Session("DC_GENERALE"), Val(Txt_Numero.Text))
                NonScadenze = True
            End If
        End If


        If Val(Txt_Numero.Text) = 0 Or NonScadenze Then
            Dim XS As New Cls_Scadenziario
            Dim DataScadenza As New Date

            XS.NumeroRegistrazioneContabile = x.NumeroRegistrazione

            If Year(x.DataDocumento) < 1900 Then
                DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
                If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                    DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
                End If
                If xModalita.Tipo = "F" Then
                    If Month(x.DataRegistrazione) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                        DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                    Else
                        DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                    End If
                End If
            Else
                DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
                If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                    DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
                End If
                If xModalita.Tipo = "F" Then
                    If Month(x.DataDocumento) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                        DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                    Else
                        DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                    End If
                End If
            End If

            XS.DataScadenza = DataScadenza
            Dim ImportoNetto As Double = TotaleDocumento() - TotaleRitenuta()
            Dim ImportoScadenza As Double = 0

            If xModalita.Scadenze > 0 Then

                ImportoScadenza = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                XS.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                If MyCau.TipoDocumento = "NC" Then
                    XS.Importo = XS.Importo * -1
                End If

                XS.Chiusa = 0
                XS.Descrizione = ""
                XS.ScriviScadenza(Session("DC_GENERALE"))
            End If

            If xModalita.Scadenze > 1 Then
                Dim XS1 As New Cls_Scadenziario
                XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                    XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                    ImportoScadenza = ImportoScadenza + XS1.Importo
                Else
                    XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                End If
                If MyCau.TipoDocumento = "NC" Then
                    XS1.Importo = XS1.Importo * -1
                End If



                DataScadenza = DateAdd("d", xModalita.GiorniSeconda + xModalita.GiorniPrima, x.DataDocumento)
                If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                XS1.DataScadenza = DataScadenza

                XS1.Chiusa = 0
                XS1.Descrizione = ""
                XS1.ScriviScadenza(Session("DC_GENERALE"))
            End If

            If xModalita.Scadenze > 2 Then
                Dim GIORNIDA As Long
                GIORNIDA = xModalita.GiorniSeconda + xModalita.GiorniPrima
                For i = 3 To xModalita.Scadenze
                    GIORNIDA = GIORNIDA + xModalita.GiorniAltre
                    Dim XS1 As New Cls_Scadenziario
                    XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                    DataScadenza = DateAdd("d", GIORNIDA, x.DataDocumento)
                    If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                    XS1.DataScadenza = DataScadenza

                    If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                        XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                        ImportoScadenza = ImportoScadenza + XS1.Importo
                    Else
                        XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                        ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    End If
                    'XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    If MyCau.TipoDocumento = "NC" Then
                        XS1.Importo = XS1.Importo * -1
                    End If
                    XS1.Chiusa = 0
                    XS1.Descrizione = ""
                    XS1.ScriviScadenza(Session("DC_GENERALE"))
                Next i
            End If
        End If


        If Val(Txt_Numero.Text) = 0 And x.NumeroRegistrazione > 0 Then
            Dim RigheRitenuta As New Cls_RitenuteAcconto

            For i = 0 To TabRT.Rows.Count - 1

                If Not IsDBNull(TabRT.Rows(i).Item(0)) Then
                    If Val(TabRT.Rows(i).Item(0)) > 0 Then

                        RigheRitenuta.ID = 0
                        RigheRitenuta.NumeroRegistrazioneDocumento = x.NumeroRegistrazione
                        RigheRitenuta.NumeroRegistrazionePagamento = 0
                        RigheRitenuta.CodiceTributo = ""
                        RigheRitenuta.ImponibileRitenuta = TabRT.Rows(i).Item(0)
                        RigheRitenuta.TipoRitenuta = TabRT.Rows(i).Item(1)

                        Dim CSRit As New ClsRitenuta

                        CSRit.Codice = RigheRitenuta.TipoRitenuta
                        CSRit.Leggi(Session("DC_TABELLE"))

                        RigheRitenuta.CodiceTributo = CSRit.Tributo

                        RigheRitenuta.ImportoRitenuta = TabRT.Rows(i).Item(2)
                        RigheRitenuta.NumeroRegistrazionePagamentoRitenuta = 0
                        RigheRitenuta.ImportoPagamentoRitenuta = 0
                        RigheRitenuta.ImportoPagamento = 0
                        RigheRitenuta.Scrivi(Session("DC_GENERALE"))
                    End If
                End If
            Next
        Else
           If Val(Txt_Numero.Text) > 0 Then
                Dim RigheRitenuta As New Cls_RitenuteAcconto
                Dim Presente As Double

                Presente = RigheRitenuta.RitenuteSuDocumentoImponibile(Session("DC_GENERALE"), Val(Txt_Numero.Text), 1)

                If Presente = 0 Then
                    For i = 0 To TabRT.Rows.Count - 1

                        If Not IsDBNull(TabRT.Rows(i).Item(0)) Then
                            If Val(TabRT.Rows(i).Item(0)) > 0 Then

                                RigheRitenuta.ID = 0
                                RigheRitenuta.NumeroRegistrazioneDocumento = x.NumeroRegistrazione
                                RigheRitenuta.NumeroRegistrazionePagamento = 0
                                RigheRitenuta.CodiceTributo = ""
                                RigheRitenuta.ImponibileRitenuta = TabRT.Rows(i).Item(0)
                                RigheRitenuta.TipoRitenuta = TabRT.Rows(i).Item(1)

                                Dim CSRit As New ClsRitenuta

                                CSRit.Codice = RigheRitenuta.TipoRitenuta
                                CSRit.Leggi(Session("DC_TABELLE"))

                                RigheRitenuta.CodiceTributo = CSRit.Tributo

                                RigheRitenuta.ImportoRitenuta = TabRT.Rows(i).Item(2)
                                RigheRitenuta.NumeroRegistrazionePagamentoRitenuta = 0
                                RigheRitenuta.ImportoPagamentoRitenuta = 0
                                RigheRitenuta.ImportoPagamento = 0
                                RigheRitenuta.Scrivi(Session("DC_GENERALE"))
                            End If
                        End If
                    Next
                End If
            End If
        End If



        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_NumeroProtocollo.Text = x.NumeroProtocollo

    End Sub

    Private Sub GrigliaProcedureAbilitate()

        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnTabelle.Open()

        Dim cmdProcedureAbilitate As New OleDbCommand()
        Dim ProcedureAbilitate As String = ""

        cmdProcedureAbilitate.CommandText = ("select * from Societa ")
        cmdProcedureAbilitate.Connection = cnTabelle
        Dim VerProcedureAbilitate As OleDbDataReader = cmdProcedureAbilitate.ExecuteReader()
        If VerProcedureAbilitate.Read Then
            ProcedureAbilitate = campodb(VerProcedureAbilitate.Item("ProcedureAbilitate"))
        End If
        VerProcedureAbilitate.Close()
        cnTabelle.Close()

        If ProcedureAbilitate.IndexOf("<ALLEGATIDOCUMENTI>") >= 0 Then
            Lbl_BtnScadenzario.Text = Lbl_BtnScadenzario.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxSlim('AllegatoRegistrazione.aspx?NumeroRegistrazione=" & Txt_Numero.Text & "');"">ALLEGATO</a></label>"

        End If

        cnTabelle.Close()
    End Sub


    Private Sub leggiregistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        If Val(Request.Item("NUMEROREGISTRAZIONETEMP")) > 0 Then
            x.Leggi(ConnectionStringGenerale, Txt_Numero.Text, True)
        Else
            x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)
        End If

        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_DataRegistrazione.Text = x.DataRegistrazione
        Try
            Dd_CausaleContabile.SelectedValue = x.CausaleContabile
        Catch ex As Exception
            Dd_CausaleContabile.Items(Dd_CausaleContabile.Items.Count - 1).Selected = False
            Dd_CausaleContabile.Items.Add("ERRORE")
            Dd_CausaleContabile.Items(Dd_CausaleContabile.Items.Count - 1).Value = "ERRORE"
            Dd_CausaleContabile.Items(Dd_CausaleContabile.Items.Count - 1).Selected = True
        End Try

        Txt_DescrizioneTesta.Text = x.Descrizione
        Txt_Numero.Enabled = False
        Txt_DataDocumento.Text = x.DataDocumento
        Txt_NumeroDocumento.Text = x.NumeroDocumento

        Txt_AnnoProtocollo.Text = x.AnnoProtocollo
        Txt_NumeroProtocollo.Text = x.NumeroProtocollo
        Txt_BisProtocollo.Text = x.Bis
        Txt_DescrizioneTesta.Text = x.Descrizione




        LblUltimaModifica.Text = ""

        Dim cnUltimaModifica As OleDbConnection

        cnUltimaModifica = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnUltimaModifica.Open()

        Dim cmdUltimaModifica As New OleDbCommand()


        cmdUltimaModifica.CommandText = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & x.NumeroRegistrazione


        cmdUltimaModifica.Connection = cnUltimaModifica


        Dim DataUltimaModifica As OleDbDataReader = cmdUltimaModifica.ExecuteReader()
        If DataUltimaModifica.Read Then
            LblUltimaModifica.Text = "Ultima modifica " & Format(campodbd(DataUltimaModifica.Item("DataAggiornamento")), "dd/MM/yyyy HH:mm:ss")
        End If


        Dim cmdFatturaElettronica As New OleDbCommand

        cmdFatturaElettronica.CommandText = "Select * From FatturaElettronica Where NumeroRegistrazione = " & x.NumeroRegistrazione & " Order by DataOra desc"


        cmdFatturaElettronica.Connection = cnUltimaModifica

        Dim RdFatturaElettronica As OleDbDataReader = cmdFatturaElettronica.ExecuteReader()
        If RdFatturaElettronica.Read Then
            LblUltimaModifica.Text = LblUltimaModifica.Text & "<br><font color=""red"">Fatture Elettronica Progressivo :" & campodbN(RdFatturaElettronica.Item("Progressivo")) & "</font>"
        End If
        RdFatturaElettronica.Close()

        cnUltimaModifica.Close()




        If Year(x.ExportDATA) > 2000 Then
            Txt_ExportData.Text = Format(x.ExportDATA, "dd/MM/yyyy HH:mm:ss")
        Else
            Txt_ExportData.Text = ""
        End If

        If x.BolloVirtuale = 1 Then
            Chk_BolloVirtuale.Checked = True
        Else
            Chk_BolloVirtuale.Checked = False
        End If

        Txt_RegistrazioneRiferimento.Text = x.RegistrazioneRiferimento


        If x.FatturaDiAnticipo = "S" Then
            Chk_FatturazioneAnticipata.Checked = True
        Else
            Chk_FatturazioneAnticipata.Checked = False
        End If
        Try
            DD_ModalitaPagamento.SelectedValue = x.CodicePagamento
        Catch ex As Exception
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Selected = False
            DD_ModalitaPagamento.Items.Add("ERRORE")
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Value = "ERRORE"
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Selected = True
        End Try


        Txt_Anno.Text = x.AnnoCompetenza
        Dd_Mese.SelectedValue = x.MeseCompetenza

        Txt_AnnoRif.Text = x.PeriodoRiferimentoRettaAnno
        DD_MeseRif.SelectedValue = x.PeriodoRiferimentoRettaMese

        Txt_DataRif.Text = x.RifData
        Txt_RifNumero.Text = x.RifNumero

        If Year(x.DataRid) > 1900 Then
            Txt_DataSepa.Text = x.DataRid
        End If
        Txt_IDSEPA.Text = x.idRid

        DD_CentroServizio.SelectedValue = x.CentroServizio
        If Mid(x.Tipologia & Space(20), 1, 1) = "O" Then
            DD_Tipologia.SelectedValue = "O"
        End If
        If Mid(x.Tipologia & Space(20), 1, 1) = "P" Then
            DD_Tipologia.SelectedValue = "P"
        End If
        If Mid(x.Tipologia & Space(20), 1, 1) = "C" Then
            DD_Tipologia.SelectedValue = "C"

            DD_Comune.SelectedValue = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim

            If Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim <> "" And DD_Comune.SelectedValue = "" Then
                Dim TrovaCodice As Boolean = False
                Dim Indice As Integer


                For Indice = 0 To DD_Comune.Items.Count - 1
                    If DD_Comune.Items(Indice).Value = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim Then
                        TrovaCodice = True
                    End If
                Next

                If TrovaCodice = False Then

                    Dim Comune As New ClsComune

                    Comune.Descrizione = ""
                    Comune.Provincia = Mid(x.Tipologia & Space(20), 2, 3).Trim
                    Comune.Comune = Mid(x.Tipologia & Space(20), 5, 3).Trim
                    Comune.Leggi(Session("DC_OSPITE"))

                    If Comune.Descrizione <> "" Then
                        DD_Comune.Items.Add(Comune.Descrizione & " (" & Comune.NomeConiuge & ")")
                        DD_Comune.Items(DD_Comune.Items.Count - 1).Value = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim

                        DD_Comune.SelectedValue = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim
                    End If
                End If
            End If
        End If

        If Mid(x.Tipologia & Space(20), 1, 1) = "J" Then
            DD_Tipologia.SelectedValue = "J"

            DD_Comune.SelectedValue = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim

            If Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim <> "" And DD_Comune.SelectedValue = "" Then
                Dim TrovaCodice As Boolean = False
                Dim Indice As Integer


                For Indice = 0 To DD_Comune.Items.Count - 1
                    If DD_Comune.Items(Indice).Value = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim Then
                        TrovaCodice = True
                    End If
                Next

                If TrovaCodice = False Then

                    Dim Comune As New ClsComune

                    Comune.Descrizione = ""
                    Comune.Provincia = Mid(x.Tipologia & Space(20), 2, 3).Trim
                    Comune.Comune = Mid(x.Tipologia & Space(20), 5, 3).Trim
                    Comune.Leggi(Session("DC_OSPITE"))

                    If Comune.Descrizione <> "" Then
                        DD_Comune.Items.Add(Comune.Descrizione & " (" & Comune.NomeConiuge & ")")
                        DD_Comune.Items(DD_Comune.Items.Count - 1).Value = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim

                        DD_Comune.SelectedValue = Mid(x.Tipologia & Space(20), 2, 3).Trim & " " & Mid(x.Tipologia & Space(20), 5, 3).Trim
                    End If
                End If
            End If

        End If
        If Mid(x.Tipologia & Space(20), 1, 1) = "R" Then
            DD_Tipologia.SelectedValue = "R"

            DD_Regione.SelectedValue = Trim(Mid(x.Tipologia & Space(20), 2, 4))

            If Trim(Mid(x.Tipologia & Space(20), 2, 4)) <> "" And DD_Regione.SelectedValue = "" Then
                Dim TrovaCodice As Boolean = False
                Dim Indice As Integer


                For Indice = 0 To DD_Regione.Items.Count - 1
                    If DD_Regione.Items(Indice).Value = Trim(Mid(x.Tipologia & Space(20), 2, 4)) Then
                        TrovaCodice = True
                    End If
                Next

                If TrovaCodice = False Then

                    Dim USL As New ClsUSL

                    USL.Nome = ""
                    USL.CodiceRegione = Trim(Mid(x.Tipologia & Space(20), 2, 4))
                    USL.Leggi(Session("DC_OSPITE"))

                    If USL.Nome <> "" Then
                        DD_Regione.Items.Add(USL.Nome & " (" & USL.NonInVisualizzazione & ")")
                        DD_Regione.Items(DD_Regione.Items.Count - 1).Value = Trim(Mid(x.Tipologia & Space(20), 2, 4))

                        DD_Regione.SelectedValue = Trim(Mid(x.Tipologia & Space(20), 2, 4))
                    End If
                End If
            End If
        End If

        'DD_ModalitaPagamento.SelectedValue = x.ModalitaPagamento
        If x.IVASospesa = "S" Then
            RB_IvaSospesaSI.Checked = True
            RB_IvaSospesaNO.Checked = False
        Else
            RB_IvaSospesaNO.Checked = True
            RB_IvaSospesaSI.Checked = False
        End If
        For i = 0 To x.Righe.Length - 1
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).RigaDaCausale = 1 Then
                    Dim xc As New Cls_Pianodeiconti
                    Dim PartitaIVA As String = ""

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)


                    If xc.TipoAnagrafica = "D" Then
                        Dim MCs As New Cls_ClienteFornitore

                        MCs.Nome = ""
                        MCs.MastroFornitore = xc.Mastro
                        MCs.ContoFornitore = xc.Conto
                        MCs.SottoContoFornitore = xc.Sottoconto
                        MCs.Leggi(Session("DC_OSPITE"))
                        If MCs.Nome <> "" Then
                            If MCs.PARTITAIVA > 0 Then
                                PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                            Else
                                PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                            End If

                            
                            Dim Cig As New Cls_CigClientiFornitori

                            Try
                                Cig.UpDateDropBox(Session("DC_GENERALE"), DD_Cig, MCs.CODICEDEBITORECREDITORE)
                            Catch ex As Exception

                            End Try

                            DD_Cig.SelectedValue = x.CodiceCig
                        Else

                            MCs.MastroFornitore = 0
                            MCs.ContoFornitore = 0
                            MCs.SottoContoFornitore = 0
                            MCs.MastroCliente = xc.Mastro
                            MCs.ContoCliente = xc.Conto
                            MCs.SottoContoCliente = xc.Sottoconto
                            MCs.Leggi(Session("DC_OSPITE"))

                            Dim Cig As New Cls_CigClientiFornitori

                            Try
                                Cig.UpDateDropBox(Session("DC_GENERALE"), DD_Cig, MCs.CODICEDEBITORECREDITORE)
                            Catch ex As Exception

                            End Try

                            DD_Cig.SelectedValue = x.CodiceCig

                            If MCs.PARTITAIVA > 0 Then
                                PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                            Else
                                PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                            End If
                        End If
                    End If
                    Txt_ClienteFornitore.Text = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione & PartitaIVA
                End If
            End If
        Next

        Dim Xl As New Cls_CausaleContabile

        Xl.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), Xl.RegistroIVA)

        Lbl_RegistroIVA.Text = "(" & DecReg.Descrizione & ")"


        TabIVA.Clear()
        TabIVA.Columns.Clear()
        TabIVA.Columns.Add("Imponibile", GetType(String))
        TabIVA.Columns.Add("Iva", GetType(String))
        TabIVA.Columns.Add("Imposta", GetType(String))
        TabIVA.Columns.Add("Detraibile", GetType(String))
        TabIVA.Columns.Add("Sottoconto", GetType(String))
        TabIVA.Columns.Add("DareAvere", GetType(String))
        TabIVA.Columns.Add("Descrizione", GetType(String))

        For i = 0 To x.Righe.Length
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).Tipo = "IV" Then
                    Dim myriga As System.Data.DataRow = TabIVA.NewRow()
                    Dim xc As New Cls_Pianodeiconti

                    myriga(0) = Format(x.Righe(i).Imponibile, "#,##0.00")
                    myriga(1) = x.Righe(i).CodiceIVA
                    myriga(2) = Format(x.Righe(i).Importo, "#,##0.00")
                    myriga(3) = x.Righe(i).Detraibile
                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)
                    myriga(4) = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione
                    myriga(5) = x.Righe(i).DareAvere
                    myriga(6) = x.Righe(i).Descrizione
                    TabIVA.Rows.Add(myriga)
                End If
            Else
                Exit For
            End If
        Next
        ViewState("TABELLAIVA") = TabIVA
        Call BindGridIva()

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))



        For i = 0 To x.Righe.Length
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).Tipo <> "IV" And x.Righe(i).Tipo <> "RI" And x.Righe(i).RigaDaCausale <> 1 Then
                    Dim myriga As System.Data.DataRow = TabCR.NewRow()
                    Dim xc As New Cls_Pianodeiconti

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)
                    myriga(0) = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione
                    myriga(1) = x.Righe(i).DareAvere
                    myriga(2) = x.Righe(i).Prorata
                    myriga(3) = x.Righe(i).Quantita
                    myriga(4) = Format(x.Righe(i).Importo, "#,##0.00")
                    myriga(5) = x.Righe(i).CodiceIVA

                    Dim xc1 As New Cls_Pianodeiconti

                    xc1.Mastro = x.Righe(i).MastroContropartita
                    xc1.Conto = x.Righe(i).ContoContropartita
                    xc1.Sottoconto = x.Righe(i).SottocontoContropartita

                    xc1.Decodfica(ConnectionStringGenerale)
                    myriga(6) = xc1.Mastro & " " & xc1.Conto & " " & xc1.Sottoconto & " " & xc1.Descrizione

                    myriga(7) = x.Righe(i).Descrizione
                    myriga(8) = x.Righe(i).AnnoRiferimento
                    myriga(9) = x.Righe(i).MeseRiferimento

                    myriga(10) = x.Righe(i).RigaRegistrazione
                    myriga(11) = x.Righe(i).RigaDaCausale
                    myriga(12) = x.Righe(i).CentroServizio
                    myriga(13) = x.Righe(i).TipoExtra


                    TabCR.Rows.Add(myriga)
                End If
            Else
                Exit For
            End If
        Next
        ViewState("TABELLACR") = TabCR
        Call BindGridCR()

        TabRT.Clear()
        TabRT.Columns.Clear()
        TabRT.Columns.Add("Imponibile", GetType(String))
        TabRT.Columns.Add("TRitenuta", GetType(String))
        TabRT.Columns.Add("Ritenuta", GetType(String))
        TabRT.Columns.Add("SottocontoDare", GetType(String))
        TabRT.Columns.Add("SottocontoAvere", GetType(String))
        TabRT.Columns.Add("Descrizione", GetType(String))

        Dim InseritoRigaRt As Boolean = False
        Dim ContoDare As String = ""
        Dim ContoAvere As String = ""


        For i = 0 To x.Righe.Length
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).Tipo = "RI" Then
                    InseritoRigaRt = True
                    If x.Righe(i).DareAvere = "D" Then
                        Dim xc1 As New Cls_Pianodeiconti

                        xc1.Mastro = x.Righe(i).MastroPartita
                        xc1.Conto = x.Righe(i).ContoPartita
                        xc1.Sottoconto = x.Righe(i).SottocontoPartita

                        xc1.Decodfica(ConnectionStringGenerale)
                        ContoDare = xc1.Mastro & " " & xc1.Conto & " " & xc1.Sottoconto & " " & xc1.Descrizione
                    Else
                        Dim xc1 As New Cls_Pianodeiconti

                        xc1.Mastro = x.Righe(i).MastroPartita
                        xc1.Conto = x.Righe(i).ContoPartita
                        xc1.Sottoconto = x.Righe(i).SottocontoPartita

                        xc1.Decodfica(ConnectionStringGenerale)
                        ContoAvere = xc1.Mastro & " " & xc1.Conto & " " & xc1.Sottoconto & " " & xc1.Descrizione
                    End If
                    If ContoAvere <> "" And ContoDare <> "" Then
                        Dim myriga As System.Data.DataRow = TabRT.NewRow()
                        myriga(0) = Format(x.Righe(i).Imponibile, "#,##0.00")
                        myriga(1) = x.Righe(i).CodiceRitenuta
                        myriga(2) = Format(x.Righe(i).Importo, "#,##0.00")
                        myriga(3) = ContoDare
                        myriga(4) = ContoAvere
                        myriga(5) = x.Righe(i).Descrizione

                        TabRT.Rows.Add(myriga)
                    End If
                End If
            Else
                Exit For
            End If
        Next

        If InseritoRigaRt = False Then
            Dim myriga As System.Data.DataRow = TabRT.NewRow()
            TabRT.Rows.Add(myriga)
        End If

        ViewState("TABELLART") = TabRT
        Call BindGridRT()

        'Lbl_BtnLegami.Text = "<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/Legami.jpg"" title=""Legami"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazionePrenotazione.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/prenotazione.png"" title=""Legame Prenotazioni"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/budget.png"" title=""Legame Budget"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBoxW('RateiRisconti.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/rateirisconti.png"" title=""Ratei Risconti"" /></a>"


        'Lbl_BtnScadenzario.Text = "<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/scadenziario.png""  title=""Scadenzario""  /></a>"
        'Lbl_BtnScadenzario.Text = Lbl_BtnScadenzario.Text & "<img src=""images/wizardocumenti.png""  title=""Wizard Documenti""  />"



        Lbl_BtnLegami.Text = ""

        Dim MyCau As New Cls_CausaleContabile

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date = Txt_DataRegistrazione.Text
        RegIva.Tipo = MyCau.RegistroIVA
        RegIva.Anno = Year(DataAppo)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)
        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then
            Txt_DataRegistrazione.Enabled = False
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');"">Legami</a></label>"
        End If

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))


        If DatiGenerali.BudgetAnalitica = 0 Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazionePrenotazione.aspx?Numero=" & Txt_Numero.Text & "');"">Legame Prenot.</a></label>"
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxAnaliticaScadenziario('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Budget</a></label>"
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxAnalitica('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Analitica</a></label>"
        End If
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('RateiRisconti.aspx?Numero=" & Txt_Numero.Text & "');"">Ratei Risconti</a></label>"
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('ReverseCharge.aspx?Numero=" & Txt_Numero.Text & "');"">Rev.Charge/Split</a></label>"

        If MyCau.VenditaAcquisti = "V" Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Maillinglist.aspx?Numero=" & Txt_Numero.Text & "');"">Mailing List</a></label>"
        End If

        Lbl_BtnScadenzario.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');"">Scadenzario</a></label>"

        GrigliaProcedureAbilitate()



        If Val(Txt_Numero.Text) > 0 Then

            Try

                Dim cn As OleDbConnection


                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cn.Open()

                Dim cmdLeggi As New OleDbCommand()


                cmdLeggi.CommandText = ("Select * From Log_MailInviate Where NumeroRegistrazione = ? ")
                cmdLeggi.Parameters.AddWithValue("@NumeroRegistrazione", Val(Txt_Numero.Text))
                cmdLeggi.Connection = cn
                Dim K As OleDbDataReader = cmdLeggi.ExecuteReader

                If K.Read Then
                    Lbl_BtnLegami.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Maillinglist.aspx?Numero=" & Txt_Numero.Text & "');"">Mail</a></label>" & Lbl_BtnLegami.Text
                End If


                cn.Close()
            Catch ex As Exception

            End Try

        End If


        Call RicalcolaImportiDocumento()

    End Sub


    Private Sub PulisciRegistrazione(Optional ByVal STARTUP As Boolean = False)

        'Lbl_BtnLegami.Text = "<img src=""images/Legami.jpg"" title=""Legami"" />"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<img src=""images/prenotazione.png"" title=""Legame Prenotazioni"" />"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<img src=""images/budget.png"" title=""Legame Budget"" />"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<img src=""images/rateirisconti.png"" title=""Ratei Risconti"" />"

        '
        'Lbl_BtnScadenzario.Text = "<img src=""images/scadenziario.png"" title=""Scadenzario""   />"
        'Lbl_BtnScadenzario.Text = Lbl_BtnScadenzario.Text & "<a href=""#"" class="""" onclick=""DialogBox('wizardfattura.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/wizardocumenti.png""  title=""Wizard Documenti""  /></a>"

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        Lbl_BtnLegami.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;Legame</label>"

        If DatiGenerali.BudgetAnalitica = 0 Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Prenotazioni</label>"
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Budget</label>"
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Analitica</label>"
        End If
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Ratei Risconti</label>"
        Lbl_BtnScadenzario.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;Scadenzario</label>"



        Txt_Numero.Text = 0
        'Txt_DataRegistrazione.Text = "__/__/____"
        Dd_CausaleContabile.SelectedValue = ""
        Txt_DescrizioneTesta.Text = ""
        Txt_Numero.Enabled = True
        Txt_DataDocumento.Text = "__/__/____"
        Txt_NumeroDocumento.Text = ""

        DD_ModalitaPagamento.SelectedValue = ""

        Txt_Anno.Text = 0 ' Year(Now)
        Dd_Mese.SelectedValue = 0 ' Month(Now)

        Txt_AnnoRif.Text = 0
        DD_MeseRif.SelectedValue = ""

        DD_Tipologia.SelectedValue = ""

        DD_Comune.SelectedValue = ""
        DD_Regione.SelectedValue = ""

        DD_ModalitaPagamento.SelectedValue = ""
        RB_IvaSospesaNO.Checked = True
        RB_IvaSospesaSI.Checked = False


        Txt_ClienteFornitore.Text = ""

        Txt_Numero.Text = 0
        'Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Dd_CausaleContabile.SelectedValue = ""
        Txt_DescrizioneTesta.Text = ""
        Txt_Numero.Enabled = True
        Txt_DataDocumento.Text = "__/__/____"
        Txt_NumeroDocumento.Text = ""

        DD_ModalitaPagamento.SelectedValue = ""


        Lbl_RegistroIVA.Text = ""

        'Txt_AnnoProtocollo.Text = Year(Now)
        Txt_NumeroProtocollo.Text = 0
        Txt_BisProtocollo.Text = ""

        Txt_AnnoRif.Text = 0
        DD_MeseRif.SelectedValue = ""



        DD_Tipologia.SelectedValue = ""
        DD_Comune.SelectedValue = ""

        DD_ModalitaPagamento.SelectedValue = ""

        RB_IvaSospesaNO.Checked = True
        RB_IvaSospesaSI.Checked = False
        Txt_ClienteFornitore.Text = ""

        Txt_DataRif.Text = ""
        Txt_RifNumero.Text = ""


        Txt_DataSepa.Text = ""
        Txt_IDSEPA.Text = ""

        Txt_RegistrazioneRiferimento.Text ="0"

        DD_Cig.Items.Clear
      

        Chk_FatturazioneAnticipata.Checked = False


        TabIVA.Clear()
        TabIVA.Columns.Clear()
        TabIVA.Columns.Add("Imponibile", GetType(String))
        TabIVA.Columns.Add("Iva", GetType(String))
        TabIVA.Columns.Add("Imposta", GetType(String))
        TabIVA.Columns.Add("Detraibile", GetType(String))
        TabIVA.Columns.Add("Sottoconto", GetType(String))
        TabIVA.Columns.Add("DareAvere", GetType(String))
        TabIVA.Columns.Add("Descrizione", GetType(String))

        Dim myriga1 As System.Data.DataRow = TabIVA.NewRow()
        myriga1(0) = 0
        myriga1(1) = 0
        TabIVA.Rows.Add(myriga1)
        ViewState("TABELLAIVA") = TabIVA

        Call BindGridIva()

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))
        Dim myriga As System.Data.DataRow = TabCR.NewRow()
        myriga(3) = 0
        myriga(4) = 0
        myriga(8) = 0
        myriga(9) = 0
        myriga(10) = 0
        myriga(11) = 0
        TabCR.Rows.Add(myriga)
        ViewState("TABELLACR") = TabCR
        Call BindGridCR()



        TabRT.Clear()
        TabRT.Columns.Clear()
        TabRT.Columns.Add("Imponibile", GetType(String))
        TabRT.Columns.Add("TRitenuta", GetType(String))
        TabRT.Columns.Add("Ritenuta", GetType(String))
        TabRT.Columns.Add("SottocontoDare", GetType(String))
        TabRT.Columns.Add("SottocontoAvere", GetType(String))
        TabRT.Columns.Add("Descrizione", GetType(String))
        Dim myrigaR As System.Data.DataRow = TabRT.NewRow()
        myrigaR(2) = 0
        TabRT.Rows.Add(myrigaR)
        ViewState("TABELLART") = TabRT
        Call BindGridRT()



        If STARTUP = False Then
            Call RicalcolaImportiDocumento()
        End If
    End Sub

    Protected Sub Dd_Ritenuta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To GridIVA.Rows.Count - 1
            If sender.clientid = GridIVA.Rows(I).Cells(2).Controls.Item(1).ClientID Then
                riga = I
            End If
        Next

        Dim Xs As New ClsRitenuta

        Xs.Codice = sender.SelectedValue
        Xs.Leggi(Session("DC_TABELLE"))


        Dim TxtRitenutaRT As TextBox = DirectCast(GridRitenuta.Rows(riga).FindControl("TxtRitenutaRT"), TextBox)
        Dim TxtImponibileRT As TextBox = DirectCast(GridRitenuta.Rows(riga).FindControl("TxtImponibileRT"), TextBox)
        If Not IsNothing(TxtImponibileRT) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileRT.ClientID & "').val(), true, true); } );     $('#" & TxtImponibileRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileRT.ClientID & "').val(),2); $('#" & TxtImponibileRT.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRitIm", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
        End If


        If Not IsNothing(TxtRitenutaRT) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtRitenutaRT.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtRitenutaRT.ClientID & "').val(), true, true); } );      $('#" & TxtRitenutaRT.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtRitenutaRT.ClientID & "').val(),2); $('#" & TxtRitenutaRT.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRit", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRit", MyJs, True)
        End If

        If Val(TxtImponibileRT.Text) = 0 Then
            Exit Sub
        End If
        Dim Imponibile As Double
        Imponibile = TxtImponibileRT.Text


        TxtRitenutaRT.Text = Modulo.MathRound(Imponibile * Xs.Percentuale, 2)

        Call UpTableRT()
        Call EseguiJS()
        Call RicalcolaImportiDocumento()
    End Sub

    'TxtImponibileIVA
    Protected Sub TxtImponibileIVAChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To GridIVA.Rows.Count - 1
            If sender.clientid = GridIVA.Rows(I).Cells(1).Controls.Item(1).ClientID Then
                riga = I


            End If
        Next

        Dim Xs As New Cls_IVA




        Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImpostaIVA"), TextBox)
        Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImponibileIVA"), TextBox)
        Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(riga).FindControl("DDIva"), DropDownList)


        Xs.Leggi(Session("DC_TABELLE"), DDIva.SelectedValue)

        If Not IsNothing(TxtImpostaIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImpostaIVA.ClientID & "').val(), true, true); } );    $('#" & TxtImpostaIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImpostaIVA.ClientID & "').val(),2); $('#" & TxtImpostaIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaImIVA", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaImIVA", MyJs, True)
        End If

        If Not IsNothing(TxtImponibileIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileIVA.ClientID & "').val(), true, true); } );   $('#" & TxtImponibileIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileIVA.ClientID & "').val(),2); $('#" & TxtImponibileIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaIV", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaIV", MyJs, True)
        End If

        If TxtImponibileIVA.Text = "" Then
            Exit Sub
        End If

        If CDbl(TxtImponibileIVA.Text) = 0 Then
            Exit Sub
        End If

        If TxtImpostaIVA.Text = "" Then
            TxtImpostaIVA.Text = "0"
        End If

        Dim Imponibile As Double
        Imponibile = TxtImponibileIVA.Text


        If TxtImpostaIVA.Text <> Format(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), "#,##0.00") And CDbl(TxtImpostaIVA.Text) <> 0 Then
            Dim MyJs As String
            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').css(""background-color"",""red"");   });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CambiaSfondo", MyJs, True)
        Else
            Dim MyJs As String
            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').css(""background-color"",""white"");   });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CambiaSfondo", MyJs, True)
        End If


        Call UpTableIVA()
        Call EseguiJS()
        Call RicalcolaImportiDocumento()

    End Sub


    Protected Sub TxtImpostaChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To GridIVA.Rows.Count - 1
            If sender.clientid = GridIVA.Rows(I).Cells(3).Controls.Item(1).ClientID Then
                riga = I


            End If
        Next

        Dim Xs As New Cls_IVA




        Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImpostaIVA"), TextBox)
        Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImponibileIVA"), TextBox)
        Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(riga).FindControl("DDIva"), DropDownList)


        Xs.Leggi(Session("DC_TABELLE"), DDIva.SelectedValue)

        If Not IsNothing(TxtImpostaIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImpostaIVA.ClientID & "').val(), true, true); } );    $('#" & TxtImpostaIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImpostaIVA.ClientID & "').val(),2); $('#" & TxtImpostaIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaImIVA", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaImIVA", MyJs, True)
        End If

        If Not IsNothing(TxtImponibileIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileIVA.ClientID & "').val(), true, true); } );   $('#" & TxtImponibileIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileIVA.ClientID & "').val(),2); $('#" & TxtImponibileIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaIV", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaIV", MyJs, True)
        End If

        If TxtImponibileIVA.Text = "" Then
            Exit Sub
        End If

        If CDbl(TxtImponibileIVA.Text) = 0 Then
            Exit Sub
        End If
        Dim Imponibile As Double
        Imponibile = TxtImponibileIVA.Text


        If TxtImpostaIVA.Text <> Format(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), "#,##0.00") Then
            Dim MyJs As String
            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').css(""background-color"",""red"");   });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CambiaSfondo", MyJs, True)
        Else
            Dim MyJs As String
            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').css(""background-color"",""white"");   });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CambiaSfondo", MyJs, True)
        End If


        Call UpTableIVA()
        Call EseguiJS()
        Call RicalcolaImportiDocumento()

    End Sub

    Protected Sub Dd_IVA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To GridIVA.Rows.Count - 1
            If sender.clientid = GridIVA.Rows(I).Cells(2).Controls.Item(1).ClientID Then
                riga = I


            End If
        Next

        Dim Xs As New Cls_IVA

        Xs.Leggi(Session("DC_TABELLE"), sender.SelectedValue)


        Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImpostaIVA"), TextBox)
        Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(riga).FindControl("TxtImponibileIVA"), TextBox)
        If Not IsNothing(TxtImpostaIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImpostaIVA.ClientID & "').val(), true, true); } );    $('#" & TxtImpostaIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImpostaIVA.ClientID & "').val(),2); $('#" & TxtImpostaIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaImIVA", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaImIVA", MyJs, True)
        End If

        If Not IsNothing(TxtImponibileIVA) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtImponibileIVA.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImponibileIVA.ClientID & "').val(), true, true); } );   $('#" & TxtImponibileIVA.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImponibileIVA.ClientID & "').val(),2); $('#" & TxtImponibileIVA.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaIV", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaIV", MyJs, True)
        End If

        If TxtImponibileIVA.Text = "" Then
            Exit Sub
        End If

        If CDbl(TxtImponibileIVA.Text) = 0 Then
            Exit Sub
        End If
        Dim Imponibile As Double
        Imponibile = TxtImponibileIVA.Text


        TxtImpostaIVA.Text = Format(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), "#,##0.00")


        Dim MyJsSf As String
        MyJsSf = "$(document).ready(function() { $('#" & TxtImpostaIVA.ClientID & "').css(""background-color"",""white"");   });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CambiaSfondo", MyJsSf, True)

        Call UpTableIVA()
        Call EseguiJS()
        Call RicalcolaImportiDocumento()
    End Sub



    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        Dim X As New Cls_CausaleContabile

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)



        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), X.RegistroIVA)

        Lbl_RegistroIVA.Text = "(" & DecReg.Descrizione & ")"


        Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(0).FindControl("TxtImponibileIVA"), TextBox)
        Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(0).FindControl("DDIva"), DropDownList)
        Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(0).FindControl("TxtImpostaIVA"), TextBox)
        Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(0).FindControl("DDDetraibileIVA"), DropDownList)
        Dim TxtSottocontoIVA As TextBox = DirectCast(GridIVA.Rows(0).FindControl("TxtSottocontoIVA"), TextBox)

        Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(0).FindControl("RBDareIVA"), RadioButton)
        Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(0).FindControl("RBAvereIVA"), RadioButton)
        Dim TxtDescrizioneIVA As TextBox = DirectCast(GridIVA.Rows(0).FindControl("TxtDescrizioneIVA"), TextBox)


        If IsDBNull(TxtSottocontoIVA.Text) Or TxtSottocontoIVA.Text = "" Then
            Dim Indice As Long
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Ks.Tipo = "R" Then
                Indice = 1
            Else
                Indice = 2
            End If

            Dim xS As New Cls_Pianodeiconti
            Try
                xS.Mastro = Ks.Righe(Indice).Mastro
                xS.Conto = Ks.Righe(Indice).Conto
                xS.Sottoconto = Ks.Righe(Indice).Sottoconto

                xS.Decodfica(Session("DC_GENERALE"))
            Catch ex As Exception

            End Try

            TxtSottocontoIVA.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            If Ks.Righe(Indice).DareAvere = "D" Then
                RBDareIVA.Checked = True
                RBAvereIVA.Checked = False
            Else
                RBDareIVA.Checked = False
                RBAvereIVA.Checked = True
            End If

            If Ks.Detraibilita <> "" Then
                DDDetraibileIVA.SelectedValue = Ks.Detraibilita
            End If
        End If

        Tabella = ViewState("TABELLACR")

        If IsDBNull(Tabella.Rows(0).Item(0)) Then
            Dim Indice As Long
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Ks.Tipo = "R" Then
                Indice = 2
            Else
                Indice = 1
            End If

            Dim xS As New Cls_Pianodeiconti

            xS.Mastro = Ks.Righe(Indice).Mastro
            xS.Conto = Ks.Righe(Indice).Conto
            xS.Sottoconto = Ks.Righe(Indice).Sottoconto

            xS.Decodfica(Session("DC_GENERALE"))

            Tabella.Rows(0).Item(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            Tabella.Rows(0).Item(1) = Ks.Righe(Indice).DareAvere

            Tabella.Rows(0).Item(11) = Indice + 1

            ViewState("TABELLACR") = Tabella

            Call BindGridCR()
        End If


        Tabella = ViewState("TABELLART")

        If IsDBNull(Tabella.Rows(0).Item(0)) Then
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)
            Tabella.Rows(0).Item(0) = 0
            Tabella.Rows(0).Item(2) = 0

            If Not IsNothing(Ks.Righe(8)) Then
                If Ks.Righe(8).DareAvere = "D" Then
                    Dim XDec As New Cls_Pianodeiconti
                    XDec.Mastro = Ks.Righe(8).Mastro
                    XDec.Conto = Ks.Righe(8).Conto
                    XDec.Sottoconto = Ks.Righe(8).Sottoconto
                    XDec.Decodfica(Session("DC_GENERALE"))
                    Tabella.Rows(0).Item(3) = Ks.Righe(8).Mastro & " " & Ks.Righe(8).Conto & " " & Ks.Righe(8).Sottoconto & " " & XDec.Descrizione

                    Tabella.Rows(0).Item(4) = Txt_ClienteFornitore.Text
                Else
                    Dim XDec As New Cls_Pianodeiconti
                    XDec.Mastro = Ks.Righe(8).Mastro
                    XDec.Conto = Ks.Righe(8).Conto
                    XDec.Sottoconto = Ks.Righe(8).Sottoconto
                    XDec.Decodfica(Session("DC_GENERALE"))
                    Tabella.Rows(0).Item(4) = Ks.Righe(8).Mastro & " " & Ks.Righe(8).Conto & " " & Ks.Righe(8).Sottoconto & " " & XDec.Descrizione

                    Tabella.Rows(0).Item(3) = Txt_ClienteFornitore.Text
                End If
            End If
            ViewState("TABELLART") = Tabella

            Call BindGridRT()
        End If




        Call EseguiJS()
    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim i As Integer
        'Lbl_errori.Text = ""
        Call UpTableIVA()
        Call UpTableCR()
        Call UpTableRT()


        If Trim(Dd_CausaleContabile.SelectedValue) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica causale contabile');", True)
            REM Lbl_errori.Text = "Specifica causale contabile"
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_AnnoProtocollo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica anno protocollo');", True)
            REM Lbl_errori.Text = "Specifica anno protocollo"
            Call EseguiJS()
            Exit Sub
        End If

        If Modulo.MathRound(TotaleContabile(), 2) <> Modulo.MathRound(TotaleImponibile(), 2) Then
            Dim TestoAppo As String = "La sommatoria degl imponibili deve essere uguale alla sommatoria degl importi delle contro partite, differenzea : " & Format(Modulo.MathRound(TotaleImponibile(), 2) - Modulo.MathRound(TotaleContabile(), 2), "#,##0.00")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & TestoAppo & "');", True)
            REM Lbl_errori.Text = "
            Call EseguiJS()
            Exit Sub
        End If


        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Call EseguiJS()
            Exit Sub
        End If


        If VerificaGirocontoAcosti() Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Giroconto iva a costi non indicato');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Call EseguiJS()
            Exit Sub
        End If

        Dim MS As New Cls_CausaleContabile

        MS.Codice = Dd_CausaleContabile.SelectedValue
        MS.Leggi(Session("DC_TABELLE"), MS.Codice)

        If Val(Txt_Numero.Text) = 0 Then
            If Val(Txt_NumeroProtocollo.Text) > 0 Then
                Dim CercaProtocollo As New Cls_MovimentoContabile

                CercaProtocollo.NumeroRegistrazione = 0
                CercaProtocollo.LeggiRegistrazionePerNumeroProtocollo(Session("DC_GENERALE"), MS.RegistroIVA, Val(Txt_NumeroProtocollo.Text), Val(Txt_AnnoProtocollo.Text))
                If CercaProtocollo.NumeroRegistrazione > 0 And Txt_BisProtocollo.Text.ToUpper = CercaProtocollo.Bis.ToUpper Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Protocollo già usato');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
        End If

        If MS.DataObbligatoria = "S" Then
            If Not IsDate(Txt_DataDocumento.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Documento formalmente errata');", True)
                REM Lbl_errori.Text = "Data Documento formalmente errata"
                Call EseguiJS()
                Exit Sub
            End If

            Dim DataRegistrazione As Date

            Try
                DataRegistrazione = Txt_DataRegistrazione.Text
            Catch ex As Exception
                DataRegistrazione = Now
            End Try


            If Year(Txt_DataDocumento.Text) < DataRegistrazione.Year - 6 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare una corretta data documento');", True)
                REM Lbl_errori.Text = "Data Documento formalmente errata"
                Call EseguiJS()
                Exit Sub
            End If
        End If

        If MS.NumeroObbligatorio = "S" Then
            If Txt_NumeroDocumento.Text.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Numero Documento non indicato');", True)
                REM Lbl_errori.Text = "Data Documento formalmente errata"
                Call EseguiJS()
                Exit Sub
            End If
        End If

        If MS.VenditaAcquisti = "V" Then
            If Txt_DataDocumento.Text = "" Then
                Txt_DataDocumento.Text = Txt_DataRegistrazione.Text
            End If
        End If

        If Trim(Txt_ClienteFornitore.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente / Fornitore non specificato');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        For i = 0 To GridIVA.Rows.Count - 1
            Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)

            If DDIva.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica codice iva');", True)
                Call EseguiJS()
                Exit Sub
            End If
        Next

        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If XDatiGenerali.ModalitaPagamentoOblDoc = 1 Then
            If DD_ModalitaPagamento.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica modalità pagamento');", True)
                Call EseguiJS()
                Exit Sub
            End If

        End If

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim DatiContabile As New Cls_MovimentoContabile

        If Val(Txt_Numero.Text) = 0 Then
            If Format(DatiContabile.MaxDataRegistroIVA(Session("DC_GENERALE"), Year(AppoggioData), MS.RegistroIVA), "yyyyMMdd") > Format(AppoggioData, "yyyyMMdd") Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso inserire già presente fattura con data successiva');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        Dim ImportoDareAvereRicavi As Double = 0
        Dim ImportoIva As Double = 0

        Dim VettoreAliquota(100) As String
        Dim VettoreImponibile(100) As String
        Dim MaxIVA As Integer
        MaxIVA = 0

        For i = 0 To GridIVA.Rows.Count - 1
            Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)

            Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBDareIVA"), RadioButton)
            Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBAvereIVA"), RadioButton)

            Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImponibileIVA"), TextBox)
            Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImpostaIVA"), TextBox)

            Dim AliquotaIVA As String
            Dim DareAvereIVA As String

            AliquotaIVA = DDIva.SelectedValue

            If TxtImponibileIVA.Text = "" Then
                TxtImponibileIVA.Text = "0"
            End If
            If RBAvereIVA.Checked = True Then
                DareAvereIVA = "A"
                ImportoIva = CDbl(TxtImponibileIVA.Text) * -1
            Else
                DareAvereIVA = "D"
                ImportoIva = CDbl(TxtImponibileIVA.Text)
            End If

            Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDDetraibileIVA"), DropDownList)
            Dim Elabora As Boolean = False
            If DDDetraibileIVA.SelectedValue = "" Then
                Elabora = True
            Else
                Dim Mtipo As New ClsDetraibilita

                Mtipo.Codice = DDDetraibileIVA.SelectedValue
                Mtipo.Leggi(Session("DC_TABELLE"))
                If Mtipo.Automatico <> "S" Then
                    Elabora = True
                End If
            End If

            If Elabora = True Then
                ' Controlla se vi è la stessa aliquota con segno opposto
                Dim IndiceIVA As Integer

                For IndiceIVA = 0 To GridIVA.Rows.Count - 1
                    Dim DDIvaControllo As DropDownList = DirectCast(GridIVA.Rows(IndiceIVA).FindControl("DDIva"), DropDownList)
                    Dim RBDareIVAControllo As RadioButton = DirectCast(GridIVA.Rows(IndiceIVA).FindControl("RBDareIVA"), RadioButton)
                    Dim RBAvereIVAControllo As RadioButton = DirectCast(GridIVA.Rows(IndiceIVA).FindControl("RBAvereIVA"), RadioButton)

                    If DDIva.SelectedValue = DDIvaControllo.SelectedValue Then
                        Dim DareAvereIVAControllo As String
                        DareAvereIVAControllo = ""
                        If RBDareIVAControllo.Checked = True Then
                            DareAvereIVAControllo = "D"
                        End If
                        If RBAvereIVAControllo.Checked = True Then
                            DareAvereIVAControllo = "A"
                        End If

                        If DareAvereIVA <> DareAvereIVAControllo Then
                            Elabora = False
                        End If
                    End If
                Next
            End If

            ImportoDareAvereRicavi = 0
            If Elabora Then
                For x = 0 To GridCostiRicavi.Rows.Count - 1
                    Dim DDIvaRica As DropDownList = DirectCast(GridCostiRicavi.Rows(x).FindControl("DDIvaCr"), DropDownList)
                    Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(x).FindControl("TxtImportoCR"), TextBox)
                    Dim RBDareCR As RadioButton = DirectCast(GridCostiRicavi.Rows(x).FindControl("RBDareCR"), RadioButton)
                    Dim RBAvereCR As RadioButton = DirectCast(GridCostiRicavi.Rows(x).FindControl("RBAvereCR"), RadioButton)
                    If AliquotaIVA = DDIvaRica.SelectedValue Then
                        If RBAvereCR.Checked = True Then
                            DareAvereIVA = "A"
                            ImportoDareAvereRicavi = ImportoDareAvereRicavi - CDbl(TxtImportoCR.Text)
                        Else
                            DareAvereIVA = "D"
                            ImportoDareAvereRicavi = ImportoDareAvereRicavi + CDbl(TxtImportoCR.Text)
                        End If
                    End If
                Next
                If Math.Round(ImportoDareAvereRicavi, 2) <> Math.Round(ImportoIva, 2) And MS.VenditaAcquisti ="V"  Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Aliquota iva, costi ricavi non coincide con righe iva - " & ImportoDareAvereRicavi & " -" & ImportoIva & "');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
        Next





        'If TotaleImponibile() = 0 Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Imponibile a zero non posso registrare');", True)
        '    Call EseguiJS()
        '    REM Lbl_errori.Text = "Imponibile a zero non posso registrare"
        '    Exit Sub
        'End If

        Dim MyLegami As New Cls_Legami
        Try

            If Math.Abs(Modulo.MathRound(TotaleDocumento() - TotaleRitenuta(), 2)) < Modulo.MathRound(MyLegami.TotaleLegame(Session("DC_GENERALE"), Txt_Numero.Text), 2) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('L importo netto dev essere uguale o maggiore dell importo legame');", True)
                Call EseguiJS()
                REM Lbl_errori.Text = "L'importo nette dev'essere uguale o maggiore dell'importo legame"
                Exit Sub
            End If
        Catch ex As Exception

        End Try


        Dim MyCau As New Cls_CausaleContabile

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date = Txt_DataRegistrazione.Text
        RegIva.Tipo = MyCau.RegistroIVA
        RegIva.Anno = Year(DataAppo)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)

        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registro iva chiuso prima della data registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If MyCau.VenditaAcquisti = "A" Then
            Dim DataDocumento As Date = Txt_DataDocumento.Text
            If Format(DataDocumento, "yyyyMMdd") > Format(DataAppo, "yyyyMMdd") Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Documento > Data Registrazione');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If



        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length < 3 Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If
        If Val(Vettore(2)) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If


        
        



        TabIVA = ViewState("TABELLAIVA")

        For i = 0 To TabIVA.Rows.Count - 1
            If Not IsDBNull(TabIVA.Rows(i).Item(0)) Then
                Vettore = SplitWords(TabIVA.Rows(i).Item(4))
                If Vettore.Length < 3 Then
                    Dim StAppoggio As String = "Conto non specificato in tabella iva, riga " & i
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                    REM Lbl_errori.Text = "Conto non specificato in tabella iva, riga " & i
                    Exit Sub
                End If
                If Val(Vettore(2)) = 0 Then
                    Dim StAppoggio As String = "Conto non specificato in tabella iva, riga " & i
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                    Exit Sub
                End If
            End If
        Next

        TabCR = ViewState("TABELLACR")

        For i = 0 To TabCR.Rows.Count - 1
            If Not IsDBNull(TabCR.Rows(i).Item(0)) Then
                Vettore = SplitWords(TabCR.Rows(i).Item(0))
                If Vettore.Length < 3 Then
                    Dim StAppoggio As String = "Conto non specificato in tabella costi e ricavi, riga " & i
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                    Exit Sub
                End If
                If Val(Vettore(2)) = 0 Then
                    Dim StAppoggio As String = "Conto non specificato in tabella costi e ricavi, riga " & i
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                    Exit Sub
                End If
            End If
        Next


        If DD_Tipologia.SelectedValue = "R" Then
            If DD_Regione.SelectedValue = "" Then
                Dim StAppoggio As String = "Indica il codice regione"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                Exit Sub
            End If


        End If


        If DD_Tipologia.SelectedValue = "C" Or DD_Tipologia.SelectedValue = "J" Then
            If DD_Comune.SelectedValue = "" Then
                Dim StAppoggio As String = "Indica il codice provincia comune"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                Exit Sub
            End If

        End If


        TabRT = ViewState("TABELLART")

        'For i = 0 To TabRT.Rows.Count - 1
        '    If Not IsDBNull(TabRT.Rows(i).Item(0)) Then
        '        Vettore = SplitWords(TabRT.Rows(i).Item(3))
        '        If Vettore.Length < 3 Then
        '            Lbl_errori.Text = "Conto non specificato in tabella ritenuta, riga " & i
        '            Exit Sub
        '        End If
        '        Vettore = SplitWords(TabRT.Rows(i).Item(4))
        '        If Vettore.Length < 3 Then
        '            Lbl_errori.Text = "Conto non specificato in tabella ritenuta, riga " & i
        '            Exit Sub
        '        End If
        '    End If
        'Next

        If Val(Txt_Numero.Text) = 0 Then
            If Txt_NumeroDocumento.Text <> "" And Txt_DataDocumento.Text <> "" Then
                Dim cn As OleDbConnection
                Dim VettoreCli(100) As String
                VettoreCli = SplitWords(Txt_ClienteFornitore.Text)



                cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                                   "DataDocumento = ? And NumeroDocumento = ? And RegistroIva = ? And (Select count(*) from MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & VettoreCli(0) & " And ContoPartita = " & VettoreCli(1) & " And SottocontoPartita = " & VettoreCli(2) & ") > 0")
                cmd.Parameters.AddWithValue("@DataDocumento", Txt_DataDocumento.Text)
                cmd.Parameters.AddWithValue("@NumeroDocumento", Txt_NumeroDocumento.Text)
                cmd.Parameters.AddWithValue("@RegistroIva", MyCau.RegistroIVA)
                cmd.Connection = cn
                Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
                If VerReader.Read Then
                    Dim StAppoggio As String = "Documento già presente,non posso procedere"
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & StAppoggio & "');", True)
                    Exit Sub
                End If
                VerReader.Close()
                cn.Close()
            End If
        End If


        Dim Inserimento As Boolean = False

        If Val(Txt_Numero.Text) > 0 Then
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "M", "DOCUMENTO", "")

        Else
            Inserimento = True
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "I", "DOCUMENTO", "")
        End If


        Call SriviRegistrazione()

        Dim RitenutaPresente As Boolean = False
        For i = 0 To TabRT.Rows.Count - 1

            If Not IsDBNull(TabRT.Rows(i).Item(0)) Then
                If Val(TabRT.Rows(i).Item(0)) > 0 Then
                    RitenutaPresente = True
                End If
            End If
        Next


        Txt_Numero.Enabled = False
        Dim MyJs As String


        'MyJs = "alert('Salvato Registrazione " & Txt_Numero.Text & " - Protocollo " & Txt_NumeroProtocollo.Text & " " & Txt_BisProtocollo.Text & "/" & Txt_AnnoProtocollo.Text & " del " & Txt_DataRegistrazione.Text & "');  "
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ModReg", MyJs, True)


        Dim kInd As Integer
        Dim TrovaBollo As Boolean = False

        Dim DG As New Cls_DatiGenerali

        DG.LeggiDati(Session("DC_TABELLE"))

        Dim RegistroIVA As New Cls_RegistroIVA


        Dim Mov As New Cls_MovimentoContabile

        Mov.NumeroRegistrazione = Txt_Numero.Text
        Mov.Leggi(Session("DC_GENERALE"), Mov.NumeroRegistrazione)

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Mov.CausaleContabile
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

        If RegistroIVA.RegistroCartaceo = 0 And Inserimento = True Then

            If CausaleContabile.Tipo = "R" Then
                For kInd = 0 To 100
                    If Not IsNothing(Mov.Righe(kInd)) Then
                        If Mov.Righe(kInd).RigaDaCausale = 9 Then
                            TrovaBollo = True
                        End If
                    End If
                Next
            End If
            If TrovaBollo = False Then
                Dim DatiBollo As New Cls_bolli

                If IsDate(Mov.DataRegistrazione) Then
                    If Year(Mov.DataRegistrazione) > 1900 Then
                        DatiBollo.Leggi(Session("DC_TABELLE"), Mov.DataRegistrazione)
                    Else
                        DatiBollo.Leggi(Session("DC_TABELLE"), Now)
                    End If
                Else
                    DatiBollo.Leggi(Session("DC_TABELLE"), Now)
                End If


                For kInd = 0 To 100
                    If Not IsNothing(Mov.Righe(kInd)) Then
                        If Mov.Righe(kInd).CodiceIVA = DatiBollo.CodiceIVA And Mov.Righe(kInd).Importo = DatiBollo.ImportoBollo And _
                         Mov.Righe(kInd).MastroPartita = DG.MastroBollo And Mov.Righe(kInd).ContoPartita = DG.ContoBollo And Mov.Righe(kInd).SottocontoPartita = DG.SottoContoBollo Then
                            TrovaBollo = True
                        End If
                    End If
                Next
            End If
        End If
        Dim WarningRegistrazione As String = ""

        If TrovaBollo = True And Mov.BolloVirtuale = 0 Then
            WarningRegistrazione = WarningRegistrazione & "<br/><hr/><font color=""ORANGE"">Bollo Virtuale non Flaggato</font>"
        End If

        For kInd = 0 To 100
            If Not IsNothing(Mov.Righe(kInd)) Then
                If Mov.Righe(kInd).Tipo = "IV" Then
                    Dim IVA As New Cls_IVA

                    IVA.Codice = Mov.Righe(kInd).CodiceIVA
                    IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                    If Modulo.MathRound(Mov.Righe(kInd).Imponibile * IVA.Aliquota, 2) <> Modulo.MathRound(Mov.Righe(kInd).Importo, 2) Then
                        WarningRegistrazione = WarningRegistrazione & "<br/><hr/><font color=""ORANGE"">Calcolo iva non coerente su imponibile " & Format(Mov.Righe(kInd).Imponibile, "#,##0.00") & " </font>"
                    End If
                End If
            End If
        Next

        If Val(Txt_RegistrazioneRiferimento.Text)> 0 Then
            WarningRegistrazione = WarningRegistrazione & "<br/><hr/><font color=""ORANGE"">Attenzione Registrazione Split/Reverse : modificare registrazioni collegate</font>"
        End If


        If DD_Cig.SelectedValue.Trim <> "" Then
            Dim VerificaCig As New Cls_CigClientiFornitori
            Dim Clifor As New Cls_ClienteFornitore

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            Clifor.MastroFornitore = Vettore(0)
            Clifor.ContoFornitore = Vettore(1)
            Clifor.SottoContoFornitore = Vettore(2)
            Clifor.Leggi(Session("DC_OSPITE"))
            If Clifor.Nome <> "" Then
                VerificaCig.CodiceDebitoreCreditore = Clifor.CODICEDEBITORECREDITORE
                VerificaCig.LeggiCig(Session("DC_GENERALE"), Clifor.CODICEDEBITORECREDITORE, DD_Cig.SelectedValue)

                If VerificaCig.ImportoCigUsato(Session("DC_GENERALE"), Session("DC_TABELLE"), Clifor.CODICEDEBITORECREDITORE, DD_Cig.SelectedValue, Clifor.MastroFornitore, Clifor.ContoFornitore, Clifor.SottoContoFornitore) > VerificaCig.ImportoBudget Then
                    WarningRegistrazione = WarningRegistrazione & "<br/><hr/><font color=""ORANGE"">Budget cig sforato</font>"
                End If
            End If
        End If

        Dim Segnalazione As String = ""


        If CausaleContabile.AbilitaGirocontoReverse = 1 And Inserimento = True Then
            Dim Reverse As New Cls_ReverseCharge



            Dim Log As New Cls_LogPrivacy

            Dim serializer As JavaScriptSerializer
            serializer = New JavaScriptSerializer()



            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "R", "REVERSE", serializer.Serialize(Mov))



            Segnalazione = Reverse.Reverse(Val(Txt_Numero.Text), CausaleContabile.DocumentoReverse, CausaleContabile.GirocontoReverse, Session("DC_GENERALE"), Session("DC_TABELLE"))

            Segnalazione = "<br>" & Segnalazione

            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", "alert('" & Segnalazione & "');", True)

        End If


        Dim BottoneChiudi As String = "<br/><a href=""#"" onclick=""ChiudiConfermaModifica();""  style=""position: inherit;bottom: 20px;left: 30%;width: 40%;""><input type=""button"" class=""SeniorButton"" value=""CHIUDI"" style=""width: 100%;""></a>"
        MyJs = "<div id=""ConfermaModifica"" class=""confermamodifca""  style=""position:absolute; top:20%; left:42%; width:24%;  height:23%;""><br />Salvato Registrazione " & Txt_Numero.Text & "<br />Protocollo " & Txt_NumeroProtocollo.Text & " " & Txt_BisProtocollo.Text & "/" & Txt_AnnoProtocollo.Text & " del " & Txt_DataRegistrazione.Text & WarningRegistrazione & Segnalazione & BottoneChiudi & "</div>"
        LblBox.Text = MyJs





        Dim ApriRitenute As Boolean = False
        Dim ApriScadenziario As Boolean = False
        Dim ApriBudget As Boolean = False

        If DG.MovimentiRitenute = 1 Then
            If RitenutaPresente Then
                MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBox('RitenuteDocumneti.aspx?NumeroRegistrazione=" & Txt_Numero.Text & "'); }, 1000);}); "
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RitenutaDialogBox", MyJs, True)
            End If
        End If


        If DD_ModalitaPagamento.SelectedValue <> "" And Inserimento = True Then
            Dim ModPagamento As New Cls_TipoPagamento


            ModPagamento.Codice = DD_ModalitaPagamento.SelectedValue
            ModPagamento.Leggi(Session("DC_TABELLE"))

            If ModPagamento.AperturaGestioneScadenze = 1 Then
                ApriScadenziario = True
            End If
        End If

        If CausaleContabile.AnaliticaObbligatoria = 1 And Inserimento = True Then
            ApriBudget = True
        End If

        If ApriScadenziario = True And ApriBudget = False Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBoxSlim('gestionescadenzario.aspx?NUMERO=" & Txt_Numero.Text & "&INSERIMENTO=TRUE'); }, 900);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScadenzeDialogBox", MyJs, True)
        End If
        If ApriBudget = True And ApriScadenziario = False Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBoxAnalitica('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "'); }, 1000);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BudgetDialogBox", MyJs, True)
        End If

        If ApriBudget = True And ApriScadenziario = True Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBoxAnaliticaScadenziario('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "', 'gestionescadenzario.aspx?NUMERO=" & Txt_Numero.Text & "&INSERIMENTO=TRUE'); }, 1000);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BudgetDialogBox", MyJs, True)
        End If

        If CausaleContabile.GestioneCespiti = 1 And Inserimento = True Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ window.open('GestioneCespiti.aspx?NUMEROREGISTRAZIONE=" & Txt_Numero.Text & "','_blank'); }, 1000);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "GestioneCespite", MyJs, True)
        End If



        Call PulisciRegistrazione()
        Call EseguiJS()
        TabContainer1.ActiveTabIndex = 0
        Session("NumeroRegistrazione") = 0


    End Sub




    Protected Sub Btn_Cancella_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Cancella.Click
        Dim xs As New Cls_Legami
        'Lbl_errori.Text = ""

        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.Leggi(Session("DC_GENERALE"), Txt_Numero.Text)


        If Registrazione.RegistrazioneRiferimento > 0 Then
            Dim TotaleLegame As Double
            TotaleLegame = xs.TotaleLegame(Session("DC_GENERALE"), Txt_Numero.Text)


            Dim Indice As Integer = 0
            Dim TotaleImposta As Double = 0

            For Indice = 0 To 100
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        TotaleImposta = TotaleImposta + Registrazione.Righe(Indice).Importo
                    End If
                End If
            Next

            If TotaleImposta <> TotaleLegame Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)
                REM Lbl_errori.Text = "Vi sono legami non posso cancellare la registrazione"
                Exit Sub
            End If
        Else
            If xs.TotaleLegame(Session("DC_GENERALE"), Txt_Numero.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)
                REM Lbl_errori.Text = "Vi sono legami non posso cancellare la registrazione"
                Exit Sub
            End If
        End If


        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Exit Sub
            End If
        End If


        If Val(Txt_Numero.Text) > 0 Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
            cn.Open()

            Dim cmdUs As New OleDbCommand()
            cmdUs.CommandText = "Select * From LegameBudgetRegistrazione Where NumeroRegistrazione = " & Val(Txt_Numero.Text) & " Order by AnnoCompetenza,MeseCompetenza,Anno,Livello1,Livello2,Livello3"
            cmdUs.Connection = cn
            Dim myRD As OleDbDataReader = cmdUs.ExecuteReader()
            Do While myRD.Read
                myRD.Close()
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Legame analitica presente');", True)
                Exit Sub
            Loop
            myRD.Close()

            cn.Close()
        End If


        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
        End If
        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare la registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If

        Dim XScd As New Cls_Scadenziario
        Dim TbEScd As New System.Data.DataTable("TbEScd")
        XScd.NumeroRegistrazioneContabile = Val(Txt_Numero.Text)
        XScd.loaddati(Session("DC_GENERALE"), TbEScd)

        If TbEScd.Rows.Count > 1 Then
            Exit Sub
        End If

        If TbEScd.Rows.Count = 1 Then
            If Not IsDBNull(TbEScd.Rows(0).Item(1)) Then
                If Val(TbEScd.Rows(0).Item(1)) > 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)
                    REM Lbl_errori.Text = "Vi sono scadenze legate alla registrazione non posso cancellare"
                    Exit Sub
                End If
            End If
        End If


        Dim MyCau As New Cls_CausaleContabile

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date = Txt_DataRegistrazione.Text
        RegIva.Tipo = MyCau.RegistroIVA
        RegIva.Anno = Year(DataAppo)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)

        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registro iva chiuso prima della data registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()


        Dim XReg As New Cls_MovimentoContabile

        XReg.NumeroRegistrazione = Txt_Numero.Text
        XReg.Leggi(Session("DC_GENERALE"), XReg.NumeroRegistrazione)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Val(Txt_Numero.Text), "", "D", "DOCUMENTO", serializer.Serialize(XReg))




        XReg.NumeroRegistrazione = Txt_Numero.Text

        If Registrazione.RegistrazioneRiferimento > 0 Then
            Dim EliminaRegRif As New Cls_MovimentoContabile

            EliminaRegRif.NumeroRegistrazione = Registrazione.RegistrazioneRiferimento
            EliminaRegRif.Leggi(Session("DC_GENERALE"), EliminaRegRif.NumeroRegistrazione)

            If EliminaRegRif.CausaleContabile = MyCau.DocumentoReverse Then

                EliminaRegRif.EliminaRegistrazione(Session("DC_GENERALE"))

                xs.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione, 0)

                For EliminaGiroconto = 0 To 100
                    If xs.NumeroPagamento(EliminaGiroconto) > 0 Then
                        Dim Giroconto As New Cls_MovimentoContabile

                        Giroconto.Leggi(Session("DC_GENERALE"), Giroconto.NumeroRegistrazione)

                        If Giroconto.CausaleContabile = MyCau.GirocontoReverse Then
                            Giroconto.EliminaRegistrazione(Session("DC_GENERALE"))
                        End If
                    End If
                Next

            End If

        End If

        XReg.EliminaRegistrazione(Session("DC_GENERALE"))




        Dim MovRit As New Cls_RitenuteAcconto

        MovRit.EliminaMovimentoRitenutaDocumento(Session("DC_GENERAlE"), XReg.NumeroRegistrazione)


        Call PulisciRegistrazione()
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Txt_ClienteFornitore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.TextChanged
        If Txt_Numero.Enabled = False Then
            Exit Sub
        End If

        Dim k As New Cls_ClienteFornitore
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Appoggio1 As String = ""
        Dim Appoggio2 As String = ""

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
            k.MastroCliente = Mastro
            k.ContoCliente = Conto
            k.SottoContoCliente = Sottoconto
            k.Leggi(Session("DC_OSPITE"))
            If k.Nome = "" Then
                k.MastroCliente = 0
                k.ContoCliente = 0
                k.SottoContoCliente = 0
                k.MastroFornitore = Mastro
                k.ContoFornitore = Conto
                k.SottoContoFornitore = Sottoconto


                k.Leggi(Session("DC_OSPITE"))

                Dim Cig As New Cls_CigClientiFornitori

                Try
                    Cig.UpDateDropBox(Session("DC_GENERALE"), DD_Cig, k.CODICEDEBITORECREDITORE)
                Catch ex As Exception

                End Try
                

                Tabella = ViewState("TABELLACR")

                If DD_ModalitaPagamento.SelectedValue = "" Then
                    DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamentoFornitore
                End If
                Dim xS As New Cls_Pianodeiconti

                xS.Mastro = k.MastroCosto
                xS.Conto = k.ContoCosto
                xS.Sottoconto = k.SottocontoCosto

                If k.MastroCosto <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Tabella.Rows(0).Item(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Tabella.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                    ViewState("TABELLACR") = Tabella
                    Call BindGridCR()
                Else
                    Tabella.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                    ViewState("TABELLACR") = Tabella
                    Call BindGridCR()
                End If

                Tabella = ViewState("TABELLART")

                Appoggio1 = Tabella.Rows(0).Item(3).ToString.Replace("0", "")
                Appoggio2 = Tabella.Rows(0).Item(4).ToString.Replace("0", "")

                If Appoggio1.Trim <> "" Or Appoggio2.Trim <> "" Then
                    If Tabella.Rows(0).Item(3) = "" Then
                        Tabella.Rows(0).Item(3) = Txt_ClienteFornitore.Text
                    Else
                        Tabella.Rows(0).Item(4) = Txt_ClienteFornitore.Text
                    End If
                    ViewState("TABELLART") = Tabella
                    Call BindGridRT()
                End If
            Else
                
                Dim Cig As New Cls_CigClientiFornitori

                Try
                    Cig.UpDateDropBox(Session("DC_GENERALE"), DD_Cig, k.CODICEDEBITORECREDITORE)
                Catch ex As Exception

                End Try

                If DD_Tipologia.SelectedValue = "" Then
                    Dim Pdc As New Cls_Pianodeiconti


                    Pdc.Mastro = Mastro
                    Pdc.Conto = Conto
                    Pdc.Sottoconto = Sottoconto
                    Pdc.Decodfica(Session("DC_GENERALE"))
                    If Pdc.TipoAnagrafica = "C" Then
                        DD_Tipologia.SelectedValue = "C"
                        DD_Comune.SelectedValue = k.CodiceProvincia & " " & k.CodiceComune
                    End If
                    If Pdc.TipoAnagrafica = "R" Then
                        DD_Tipologia.SelectedValue = "R"

                        DD_Regione.SelectedValue = k.CodiceRegione
                    End If
                End If



                Dim kas As New Cls_CausaleContabile

                kas.Codice = Dd_CausaleContabile.SelectedValue
                kas.Leggi(Session("DC_TABELLE"), kas.Codice)

                If DD_ModalitaPagamento.SelectedValue = "" Then
                    Try
                        DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento
                    Catch ex As Exception

                    End Try
                End If

                If k.DetrabilitaFornitore.Trim <> "" Then
                    Try
                        Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(1).FindControl("DDDetraibileIVA"), DropDownList)


                        DDDetraibileIVA.SelectedValue = k.DetrabilitaFornitore

                    Catch ex As Exception

                    End Try
                End If

                If Not IsNothing(kas.Righe(8)) Then
                    Dim TxtSottocontoDareRT As TextBox = DirectCast(GridRitenuta.Rows(0).FindControl("TxtSottocontoDareRT"), TextBox)
                    Dim TxtSottocontoAvereRT As TextBox = DirectCast(GridRitenuta.Rows(0).FindControl("TxtSottocontoAvereRT"), TextBox)

                    If kas.Righe(8).DareAvere = "D" Then
                        Dim XDec As New Cls_Pianodeiconti
                        XDec.Mastro = kas.Righe(8).Mastro
                        XDec.Conto = kas.Righe(8).Conto
                        XDec.Sottoconto = kas.Righe(8).Sottoconto
                        XDec.Decodfica(Session("DC_GENERALE"))
                        TxtSottocontoDareRT.Text = kas.Righe(8).Mastro & " " & kas.Righe(8).Conto & " " & kas.Righe(8).Sottoconto & " " & XDec.Descrizione
                        TxtSottocontoAvereRT.Text = Txt_ClienteFornitore.Text
                    Else
                        Dim XDec As New Cls_Pianodeiconti
                        XDec.Mastro = kas.Righe(8).Mastro
                        XDec.Conto = kas.Righe(8).Conto
                        XDec.Sottoconto = kas.Righe(8).Sottoconto
                        XDec.Decodfica(Session("DC_GENERALE"))
                        TxtSottocontoAvereRT.Text = kas.Righe(8).Mastro & " " & kas.Righe(8).Conto & " " & kas.Righe(8).Sottoconto & " " & XDec.Descrizione
                        TxtSottocontoDareRT.Text = Txt_ClienteFornitore.Text
                    End If
                End If

                Tabella = ViewState("TABELLACR")
                Dim xS As New Cls_Pianodeiconti
                xS.Mastro = k.MastroRicavo
                xS.Conto = k.ContoRicavo
                xS.Sottoconto = k.SottocontoRicavo
                If k.MastroRicavo <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Tabella.Rows(0).Item(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Tabella.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                    ViewState("TABELLACR") = Tabella
                    Call BindGridCR()
                Else
                    Tabella.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                    ViewState("TABELLACR") = Tabella
                    Call BindGridCR()
                End If
                Tabella = ViewState("TABELLART")
                Appoggio1 = Tabella.Rows(0).Item(3).ToString.Replace("0", "")
                Appoggio2 = Tabella.Rows(0).Item(4).ToString.Replace("0", "")

                If Appoggio1.Trim <> "" Or Appoggio2.Trim <> "" Then
                    If Tabella.Rows(0).Item(3) = "" Then
                        Tabella.Rows(0).Item(3) = Txt_ClienteFornitore.Text
                    Else
                        Tabella.Rows(0).Item(4) = Txt_ClienteFornitore.Text
                    End If
                    ViewState("TABELLART") = Tabella
                    Call BindGridRT()
                End If
            End If
        End If
    End Sub

    Protected Sub Txt_ClienteFornitore_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.Unload

    End Sub

    Protected Sub GridIVA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridIVA.SelectedIndexChanged

    End Sub

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub

    Protected Sub ImgRic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRic.Click

        Dim MyJs As String

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Appoggio1 As String = ""
        Dim Appoggio2 As String = ""

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        If Mastro > 0 Then
            MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=DOC&MASTRO=" & Mastro & "&CONTO=" & Conto & "&SOTTOCONTO=" & Sottoconto & "');}, 1000);});"
        Else
            MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=DOC');}, 1000);});"
        End If


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RicercaDOC", MyJs, True)



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("Chiamante") = "PrimaNota" Then

            Dim Data As Date = Now


            Try
                Data = Txt_DataRegistrazione.Text
            Catch ex As Exception

            End Try

            Response.Redirect("primanota.aspx?Chiamante=PrimaNote&Data=" & Format(Data, "yyyyMMdd"))
        End If

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("UltimiMovimenti.aspx?TIPO=DOC")
        Else
            Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=DOC")
        End If

        'Response.Redirect("UltimiMovimenti.aspx?TIPO=DOC")
    End Sub


    Private Sub EseguiJS(Optional ByVal MClient As Boolean = False)
        Dim MyJs As String
        'MyJs = "alert('r');"
        MyJs = ""
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        If Request.Item("NONMENU") = "OK" Then
            MyJs = MyJs & "$('.destraclasse').css('display','none');"
            MyJs = MyJs & "$('.Barra').css('display','none');"
            MyJs = MyJs & "$('.DivTastiOspite').css( ""top"", ""114px"" );"

        End If


        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('TxtSottocontoIVA')!= null) || (appoggio.match('TxtSottocontoCR')!= null) || (appoggio.match('TxtSottocontoControPartitaCR')!= null) || (appoggio.match('TxtSottocontoDareRT')!= null) || (appoggio.match('TxtSottocontoAvereRT')!= null)) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ( (appoggio.match('TxtImpostaIVA')!= null) || (appoggio.match('TxtImportoCR')!= null) || (appoggio.match('TxtQuantitaCR')!= null) || (appoggio.match('TxtImponibileRT')!= null) || (appoggio.match('TxtRitenutaRT')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap);  });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('TxtImponibileIVA')!= null)  ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); VerifcaIVA($(this)); });"
        MyJs = MyJs & "    }"

        'VerifcaIVA($(this));

        MyJs = MyJs & "} "




        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"

        'MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSGriglie", MyJs, True)

        'Dim int_MilliSecondsTimeReminder As Integer
        'Dim int_MilliSecondsTimeOut As Integer
        'Dim msgSession As String
        'msgSession = "Warning: Se nei prossimi 3 minuti, non fai nessuna operazione, il sistema tornerà alla pagina di login. Salva i tuoi dati."

        'int_MilliSecondsTimeReminder = ((Page.Session.Timeout - 3) * 50 * 1000)
        'int_MilliSecondsTimeOut = ((Page.Session.Timeout) * 50 * 1000)

        'MyJs = MyJs & " clearTimeout(myTimeReminder); " & vbNewLine & _
        '        " clearTimeout(myTimeOut); " & vbNewLine & _
        '        "var sessionTimeReminder = " & int_MilliSecondsTimeReminder.ToString() & ";" & vbNewLine & _
        '        "var sessionTimeout = " & int_MilliSecondsTimeOut.ToString() & ";" & vbNewLine & _
        '        "function doReminder(){ alert('" + msgSession & "'); }" & vbNewLine & _
        '        "function doRedirect(){ window.location.href='/Seniorweb/Login.aspx'; }" & vbNewLine & _
        '        " myTimeReminder=setTimeout('document.getElementById(""IdTimeOut"").style.visibility= ""visible"";',sessionTimeReminder); myTimeOut=setTimeout('window.location.href=""/Seniorweb/Login.aspx"";',sessionTimeout); " & vbNewLine



        MyJs = " $(" & Chr(34) & "#" & Txt_ClienteFornitore.ClientID & Chr(34) & ").autocomplete(""ClientiFornitori.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDocumento.ClientID & Chr(34) & ").mask(""99/99/9999""); " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRif.ClientID & Chr(34) & ").mask(""99/99/9999""); " & vbNewLine

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDocumento.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRif.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_AnnoProtocollo.ClientID & Chr(34) & ").mask(""9999""); " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_Anno.ClientID & Chr(34) & ").mask(""9999""); " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_AnnoRif.ClientID & Chr(34) & ").mask(""9999""); " & vbNewLine

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").blur(function() {" & vbNewLine
        MyJs = MyJs & " var apponumero = $(" & Chr(34) & "#" & Txt_Numero.ClientID & Chr(34) & ").val();" & vbNewLine
        MyJs = MyJs & " var prova  = parseInt(apponumero);" & vbNewLine
        MyJs = MyJs & " if (prova  != 0) {" & vbNewLine
        MyJs = MyJs & " return true; }" & vbNewLine
        MyJs = MyJs & " var today = new Date(); " & vbNewLine
        MyJs = MyJs & " var appoggio = '' + $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").val();"
        MyJs = MyJs & " var dateArray = appoggio.split('/');" & vbNewLine
        MyJs = MyJs & " if (today.getFullYear() != dateArray[2]) { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""3px solid #FFAA00"");"
        MyJs = MyJs & " } else { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""1px solid #8894A0"");"
        MyJs = MyJs & " } " & vbNewLine
        MyJs = MyJs & " });" & vbNewLine


        '' 
        MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
        MyJs = MyJs & "                $(""#menutastodestro"").bind(""contextmenu"", function(event) {" & vbNewLine
        MyJs = MyJs & "                    event.preventDefault();" & vbNewLine
        MyJs = MyJs & "                    $(""#rightmenu"").toggle(100)." & vbNewLine
        MyJs = MyJs & "            css({" & vbNewLine
        MyJs = MyJs & "                top: event.pageY + ""px""," & vbNewLine
        MyJs = MyJs & "                left: event.pageX + ""px""" & vbNewLine
        MyJs = MyJs & "            });" & vbNewLine
        MyJs = MyJs & "        });" & vbNewLine

        MyJs = MyJs & "                $(""#menutastodestroIVA"").bind(""contextmenu"", function(event) {" & vbNewLine
        MyJs = MyJs & "                    event.preventDefault();" & vbNewLine
        MyJs = MyJs & "                    $(""#rightmenuIVA"").toggle(100)." & vbNewLine
        MyJs = MyJs & "            css({" & vbNewLine
        MyJs = MyJs & "                top: event.pageY + ""px""," & vbNewLine
        MyJs = MyJs & "                left: event.pageX + ""px""" & vbNewLine
        MyJs = MyJs & "            });" & vbNewLine
        MyJs = MyJs & "        });" & vbNewLine
        MyJs = MyJs & "        });" & vbNewLine

        MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
        MyJs = MyJs & "        $(""#rightmenuIVA"").html('<a href=""javascript:inserisciriga(100);"" id=""A1"" ><li data-action=""first"">Ricalcola IVA da Costi/Ricavi</li></a>');"
        MyJs = MyJs & "        });" & vbNewLine

        If Dd_CausaleContabile.Text <> "" Then
            Dim Cau As New Cls_CausaleContabile

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "I" Then
                MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
                MyJs = MyJs & "        $(""#rightmenu"").html('<a href=""javascript:inserisciriga(5);"" id=""A1"" ><li data-action=""first"">Scadenza Collaboratori</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserisciriga(7);"" id=""A2"" ><li data-action=""first"">Abbuoni/Sconti</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserisciriga(8);"" id=""A4"" ><li data-action=""first"">Arrotondamenti</li></a>');" & vbNewLine
                MyJs = MyJs & "        });" & vbNewLine
            End If
            If Cau.Tipo = "R" Then
                MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
                MyJs = MyJs & "        $(""#rightmenu"").html('<a href=""javascript:inserisciriga(4);"" id=""A1"" ><li data-action=""first"">Extra fissi</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserisciriga(5);"" id=""A2"" ><li data-action=""first"">Extra variabili</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserisciriga(6);"" id=""A3"" ><li data-action=""first"">Accrediti</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserisciriga(9);"" id=""A4"" ><li data-action=""first"">Bollo</li></a>');" & vbNewLine
                MyJs = MyJs & "        });" & vbNewLine
            End If
        End If

        MyJs = MyJs & "        $(document).bind(""mousedown"", function(e) {"
        MyJs = MyJs & "        if (!$(e.target).parents("".custom-menu"").length > 0) {"
        MyJs = MyJs & "                 $(""#rightmenu"").hide(100);"
        MyJs = MyJs & "        }"
        MyJs = MyJs & "        });"

        'MyJs = MyJs & "        // If the menu element is clicked"
        'MyJs = MyJs & "        $(document).click(function() {"
        'MyJs = MyJs & ""
        'MyJs = MyJs & "        // This is the triggered action name"
        'MyJs = MyJs & "        switch ($(this).attr(""data-action"")) {"
        'MyJs = MyJs & ""
        'MyJs = MyJs & "        // A case for each action. Your actions here     "
        'MyJs = MyJs & "        case ""Anagrafica"": break;"
        'MyJs = MyJs & "        case ""Addebiti Accrediti"": break;"
        'MyJs = MyJs & "        case ""Movimenti"": break;"
        'MyJs = MyJs & "        case ""Diurno"": break;"
        'MyJs = MyJs & "        case ""Denaro"": break;"
        'MyJs = MyJs & "        case ""Documenti"": break;"
        'MyJs = MyJs & "        case ""Estratto Conto"": break;"
        'MyJs = MyJs & "        }"
        'MyJs = MyJs & ""
        'MyJs = MyJs & "        // Hide it AFTER the action was triggered"
        'MyJs = MyJs & "        $(""#rightmenu"").hide(100);"
        'MyJs = MyJs & "        });"
        'MyJs = MyJs & "        });"
        ''

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InizializzaCampijs", MyJs, True)

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_ClienteFornitore.ClientID & Chr(34) & ").autocomplete('ClientiFornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizCLFO", MyJs, True)


    End Sub


    Private Sub UpTableCR()
        Dim i As Integer

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))


        For i = 0 To GridCostiRicavi.Rows.Count - 1

            Dim TxtSottocontoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtSottocontoCR"), TextBox)

            Dim RBDareCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBDareCR"), RadioButton)
            Dim RBAvereCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBAvereCR"), RadioButton)
            Dim RBPorataSiCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBPorataSiCR"), RadioButton)
            Dim RBPorataNoCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBPorataNoCR"), RadioButton)

            Dim TxtQuantitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtQuantitaCR"), TextBox)
            Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtImportoCR"), TextBox)
            Dim DDIvaCr As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDIvaCr"), DropDownList)
            Dim TxtSottocontoControPartitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtSottocontoControPartitaCR"), TextBox)
            Dim TxtDescrizioneCr As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtDescrizioneCr"), TextBox)
            Dim TxtAnnoCr As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtAnnoCr"), TextBox)
            Dim DDMeseCr As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDMeseCr"), DropDownList)
            Dim Txt_RigaRegistrazione As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_RigaRegistrazione"), TextBox)
            Dim Txt_RigaDaCausale As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_RigaDaCausale"), TextBox)
            Dim DDCentroServizio As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDCentroServizio"), DropDownList)
            Dim Txt_TipoExtra As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_TipoExtra"), TextBox)

            Dim myriga1 As System.Data.DataRow = TabCR.NewRow()

            myriga1(0) = TxtSottocontoCR.Text
            If RBDareCR.Checked = True Then
                myriga1(1) = "D"
            End If
            If RBAvereCR.Checked = True Then
                myriga1(1) = "A"
            End If
            If RBPorataSiCR.Checked = True Then
                myriga1(2) = "S"
            End If
            If RBPorataNoCR.Checked = True Then
                myriga1(2) = "N"
            End If

            myriga1(3) = TxtQuantitaCR.Text
            myriga1(4) = TxtImportoCR.Text
            myriga1(5) = DDIvaCr.SelectedValue
            myriga1(6) = TxtSottocontoControPartitaCR.Text
            myriga1(7) = TxtDescrizioneCr.Text
            myriga1(8) = Val(TxtAnnoCr.Text)
            myriga1(9) = Val(DDMeseCr.SelectedValue)
            myriga1(10) = Val(Txt_RigaRegistrazione.Text)
            myriga1(11) = Val(Txt_RigaDaCausale.Text)
            myriga1(12) = DDCentroServizio.SelectedValue
            myriga1(13) = Txt_TipoExtra.Text
            TabCR.Rows.Add(myriga1)

        Next
        ViewState("TABELLACR") = TabCR
        Call BindGridCR()

    End Sub
    Private Sub UpTableIVA()
        Dim i As Integer

        TabIVA.Clear()
        TabIVA.Columns.Clear()
        TabIVA.Columns.Add("Imponibile", GetType(String))
        TabIVA.Columns.Add("Iva", GetType(String))
        TabIVA.Columns.Add("Imposta", GetType(String))
        TabIVA.Columns.Add("Detraibile", GetType(String))
        TabIVA.Columns.Add("Sottoconto", GetType(String))
        TabIVA.Columns.Add("DareAvere", GetType(String))
        TabIVA.Columns.Add("Descrizione", GetType(String))


        For i = 0 To GridIVA.Rows.Count - 1
            Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImponibileIVA"), TextBox)
            Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)
            Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImpostaIVA"), TextBox)
            Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDDetraibileIVA"), DropDownList)
            Dim TxtSottocontoIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtSottocontoIVA"), TextBox)

            Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBDareIVA"), RadioButton)
            Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBAvereIVA"), RadioButton)
            Dim TxtDescrizioneIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtDescrizioneIVA"), TextBox)



            If TxtImpostaIVA.Text = "" Then
                TxtImpostaIVA.Text = "0"
            End If
            If TxtImponibileIVA.Text = "" Then
                TxtImponibileIVA.Text = "0"
            End If

            Dim myriga1 As System.Data.DataRow = TabIVA.NewRow()


            myriga1(0) = TxtImponibileIVA.Text
            myriga1(1) = DDIva.Text
            myriga1(2) = TxtImpostaIVA.Text
            myriga1(3) = DDDetraibileIVA.SelectedValue
            myriga1(4) = TxtSottocontoIVA.Text
            If RBDareIVA.Checked = True Then
                myriga1(5) = "D"
            Else
                myriga1(5) = "A"
            End If
            myriga1(6) = TxtDescrizioneIVA.Text

            TabIVA.Rows.Add(myriga1)


        Next
        ViewState("TABELLAIVA") = TabIVA
        Call BindGridIva()
    End Sub

    Private Sub UpTableRT()
        Dim i As Integer

        TabRT.Clear()
        TabRT.Columns.Clear()
        TabRT.Columns.Add("Imponibile", GetType(String))
        TabRT.Columns.Add("TRitenuta", GetType(String))
        TabRT.Columns.Add("Ritenuta", GetType(String))
        TabRT.Columns.Add("SottocontoDare", GetType(String))
        TabRT.Columns.Add("SottocontoAvere", GetType(String))
        TabRT.Columns.Add("Descrizione", GetType(String))


        For i = 0 To GridRitenuta.Rows.Count - 1
            Dim TxtImponibileRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtImponibileRT"), TextBox)
            Dim DDTRitenutaRt As DropDownList = DirectCast(GridRitenuta.Rows(i).FindControl("DDTRitenutaRt"), DropDownList)
            Dim TxtRitenutaRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtRitenutaRT"), TextBox)
            Dim TxtSottocontoDareRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtSottocontoDareRT"), TextBox)
            Dim TxtSottocontoAvereRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtSottocontoAvereRT"), TextBox)
            Dim TxtDescrizioneRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtDescrizioneRT"), TextBox)


            Dim myrigaR As System.Data.DataRow = TabRT.NewRow()

            myrigaR(0) = TxtImponibileRT.Text
            myrigaR(1) = DDTRitenutaRt.Text
            myrigaR(2) = TxtRitenutaRT.Text
            myrigaR(3) = TxtSottocontoDareRT.Text
            myrigaR(4) = TxtSottocontoAvereRT.Text
            myrigaR(5) = TxtDescrizioneRT.Text

            TabRT.Rows.Add(myrigaR)
        Next

        ViewState("TABELLART") = TabRT
        Call BindGridRT()
    End Sub


    Private Function VerificaGirocontoAcosti() As Boolean
        Dim GiroContoRichiesta As Boolean = False
        Dim Travato As Boolean = False
        Dim i As Integer

        Call UpTableIVA()
        Call UpTableCR()

        VerificaGirocontoAcosti = False

        Dim xL As New Cls_CausaleContabile

        xL.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If xL.VenditaAcquisti = "A" Then
            Dim dT As New ClsDetraibilita


            For i = 0 To GridIVA.Rows.Count - 1

                Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)
                Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImpostaIVA"), TextBox)
                Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDDetraibileIVA"), DropDownList)

                dT.Codice = DDDetraibileIVA.SelectedValue
                dT.Leggi(Session("DC_TABELLE"))

                If TxtImpostaIVA.Text.Trim = "" Then
                    TxtImpostaIVA.Text = "0"
                End If

                If dT.Automatico = "S" And CDbl(TxtImpostaIVA.Text) > 0 Then

                    GiroContoRichiesta = True
                End If
            Next
        End If

        If GiroContoRichiesta = True Then
            For i = 0 To GridCostiRicavi.Rows.Count - 1
                Dim TxtDescrizioneCr As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtDescrizioneCr"), TextBox)

                If TxtDescrizioneCr.Text.IndexOf("Girocont") >= 0 Then
                    Travato = True
                End If
            Next
            If Travato = False Then
                VerificaGirocontoAcosti = True
            End If
        End If
    End Function

    Public Sub GirocontoEIva()
        RicalcolaIVA()
        Dim xL As New Cls_CausaleContabile

        xL.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)
        Dim Pdc As New Cls_Pianodeiconti

        Pdc.Mastro = xL.Righe(2).Mastro
        Pdc.Conto = xL.Righe(2).Conto
        Pdc.Sottoconto = xL.Righe(2).Sottoconto
        Pdc.Decodfica(Session("DC_GENERALE"))

        Dim ContoIVA As String

        ContoIVA = Pdc.Mastro & " " & Pdc.Conto & " " & Pdc.Sottoconto & " " & Pdc.Descrizione


        If xL.VenditaAcquisti = "A" Then
            Dim i As Integer
            Dim xTotaleImponibile As Double = 0
            Dim xTotaleImposta As Double = 0
            Dim xTotaleIndetraibilita As Double = 0

            For i = 0 To GridCostiRicavi.Rows.Count - 1
                Dim TxtSottocontoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtSottocontoCR"), TextBox)

                Dim RBDareCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBDareCR"), RadioButton)
                Dim RBAvereCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBAvereCR"), RadioButton)

                Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtImportoCR"), TextBox)
                Dim DDIvaCr As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDIvaCr"), DropDownList)


                Dim Iva As New Cls_IVA


                Iva.Codice = DDIvaCr.SelectedValue
                Iva.Leggi(Session("DC_TABELLE"), Iva.Codice)

                If Iva.DetraibilitaDefault <> "" Then
                    Dim dT As New ClsDetraibilita

                    dT.Codice = Iva.DetraibilitaDefault
                    dT.Leggi(Session("DC_TABELLE"))


                    If TxtImportoCR.Text <> "" Then
                        If xL.TipoDocumento = "NC" Then
                            If RBDareCR.Checked = True Then
                                xTotaleImponibile = xTotaleImponibile - CDbl(TxtImportoCR.Text)
                            Else
                                xTotaleImponibile = xTotaleImponibile + CDbl(TxtImportoCR.Text)
                            End If
                        Else
                            If RBAvereCR.Checked = True Then
                                xTotaleImponibile = xTotaleImponibile + CDbl(TxtImportoCR.Text)
                            Else
                                xTotaleImponibile = xTotaleImponibile - CDbl(TxtImportoCR.Text)
                            End If
                        End If
                    End If

                    Dim TotaleImposta As Double = 0

                    Dim Detraibile As Double
                    Dim Indetraibile As Double
                    Detraibile = Modulo.MathRound(dT.Aliquota * Modulo.MathRound(CDbl(Modulo.MathRound(CDbl(TxtImportoCR.Text) * Iva.Aliquota, 2)), 2), 2)
                    Indetraibile = Modulo.MathRound(CDbl(TxtImportoCR.Text) * Iva.Aliquota, 2) - Detraibile

                    Dim Prorata As Double
                    Dim XP As New Cls_Prorata

                    Prorata = 0
                    XP.Anno = Val(Txt_AnnoProtocollo.Text) - 1
                    XP.RegistroIVA = xL.RegistroIVA
                    XP.Leggi(Session("DC_TABELLE"))
                    Prorata = Modulo.MathRound(XP.DefinitivoPercentuale, 2)

                    If dT.Prorata = 1 Then
                        If Modulo.MathRound(Indetraibile - Int(Indetraibile), 2) = 0.5 Then
                            Indetraibile = Indetraibile - Modulo.MathRound((Indetraibile - 0.01) * Modulo.MathRound(Prorata, 2), 2)
                        Else
                            Indetraibile = Indetraibile - Modulo.MathRound(Indetraibile * Modulo.MathRound(Prorata, 2), 2)
                        End If
                    End If

                    If dT.Automatico = "S" Then
                        If dT.GiroACosto = "S" Then
                            Tabella = ViewState("TABELLACR")


                            Dim myriga As System.Data.DataRow = Tabella.NewRow()

                            myriga(0) = ContoIVA
                            If RBAvereCR.Checked = True Then
                                myriga(1) = "D"
                            Else
                                myriga(1) = "A"
                            End If
                            myriga(2) = "N"
                            myriga(3) = 0
                            myriga("IVA") = DDIvaCr.SelectedValue
                            myriga(4) = Indetraibile
                            myriga(7) = "Giroconto per IVA Indetraibile"
                            Tabella.Rows.Add(myriga)


                            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

                            myriga1(0) = ContoIVA
                            If RBDareCR.Checked = True Then
                                myriga1(1) = "D"
                            Else
                                myriga1(1) = "A"
                            End If
                            myriga1(2) = "N"
                            myriga1(3) = 0
                            myriga1("IVA") = DDIvaCr.SelectedValue

                            Dim xS As New Cls_Pianodeiconti

                            xS.Mastro = dT.IVAGiroACostoMastro
                            xS.Conto = dT.IVAGiroACostoConto
                            xS.Sottoconto = dT.IVAGiroACostoSottoconto

                            xS.Decodfica(Session("DC_GENERALE"))

                            myriga1(0) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
                            myriga1(4) = Indetraibile


                            myriga1(7) = "Giroconto a IVA Indetraibile"
                            Tabella.Rows.Add(myriga1)

                            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()

                            myriga2(0) = ContoIVA
                            If RBAvereCR.Checked = True Then
                                myriga2(1) = "D"
                            Else
                                myriga2(1) = "A"
                            End If
                            myriga2(2) = "N"
                            myriga2(3) = 0
                            myriga2("IVA") = DDIvaCr.SelectedValue

                            Dim xS2 As New Cls_Pianodeiconti

                            xS2.Mastro = dT.IVAGiroACostoMastro
                            xS2.Conto = dT.IVAGiroACostoConto
                            xS2.Sottoconto = dT.IVAGiroACostoSottoconto

                            xS2.Decodfica(Session("DC_GENERALE"))
                            myriga2(4) = Indetraibile
                            myriga2(6) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS2.Descrizione
                            myriga2(7) = "Giroconto a Costi"
                            Tabella.Rows.Add(myriga2)

                            Tabella.Rows(i).Item(4) = Format(CDbl(Tabella.Rows(i).Item(4)) + Indetraibile, "#,##0.00")

                            ViewState("TABELLACR") = Tabella

                            Call BindGridCR()
                        Else
                            Tabella = ViewState("TABELLACR")


                            Dim myriga As System.Data.DataRow = Tabella.NewRow()

                            myriga(0) = ContoIVA
                            If RBDareCR.Checked = False Then
                                myriga(1) = "D"
                            Else
                                myriga(1) = "A"
                            End If
                            myriga(2) = "N"
                            myriga(3) = 0
                            myriga("IVA") = DDIvaCr.SelectedValue
                            myriga(4) = Format(Indetraibile, "#,##0.00")
                            myriga(7) = "Giroconto dell'IVA a costi"
                            Tabella.Rows.Add(myriga)

                            Tabella.Rows(i).Item(4) = Format(CDbl(Tabella.Rows(i).Item(4)) + Indetraibile, "#,##0.00")

                            ViewState("TABELLACR") = Tabella
                            Call BindGridCR()
                        End If
                    Else
                        If dT.GiroACosto = "S" Then
                            Tabella = ViewState("TABELLACR")


                            Dim myriga As System.Data.DataRow = Tabella.NewRow()

                            myriga(0) = ContoIVA
                            ' Inverto segno IVA
                            If RBAvereCR.Checked = True Then
                                myriga(1) = "D"
                            Else
                                myriga(1) = "A"
                            End If
                            myriga(2) = "N"
                            myriga(3) = 0
                            myriga("IVA") = DDIvaCr.SelectedValue
                            myriga(4) = Format(Indetraibile, "#,##0.00")
                            myriga(7) = "Giroconto per IVA Indetraibile"
                            Tabella.Rows.Add(myriga)


                            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

                            myriga1(0) = ContoIVA
                            If RBDareCR.Checked = True Then
                                myriga1(1) = "D"
                            Else
                                myriga1(1) = "A"
                            End If
                            myriga1(2) = "N"
                            myriga1(3) = 0
                            myriga1("IVA") = DDIvaCr.SelectedValue

                            Dim xS As New Cls_Pianodeiconti

                            xS.Mastro = dT.IVAGiroACostoMastro
                            xS.Conto = dT.IVAGiroACostoConto
                            xS.Sottoconto = dT.IVAGiroACostoSottoconto

                            xS.Decodfica(Session("DC_GENERALE"))

                            myriga1(0) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
                            myriga1(4) = Format(Indetraibile, "#,##0.00")
                            myriga1(7) = "Giroconto a IVA Indetraibile"
                            Tabella.Rows.Add(myriga1)



                            ViewState("TABELLACR") = Tabella

                            Call BindGridCR()
                        End If
                    End If


                End If

            Next


            REM ViewState("TABELLACR") = Tabella

            Call BindGridCR()
        End If

    End Sub
    Protected Sub Btn_GirocontoaCosti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_GirocontoaCosti.Click




        Dim xTotaleImponibile As Double = 0
        Dim xTotaleImposta As Double = 0
        Dim xTotaleIndetraibilita As Double = 0


        Call UpTableIVA()
        Call UpTableCR()


        TabIVA = ViewState("TABELLAIVA")
        Dim ContX As Integer
        Dim Trovato As Boolean = False

        For ContX = 0 To TabIVA.Rows.Count - 1
            If campodbN(TabIVA.Rows(ContX).Item(0)) > 0 Then
                Trovato = True
            End If
        Next

        If Trovato = False Then
            Call GirocontoEIva()
            Exit Sub
        End If




        Dim xL As New Cls_CausaleContabile

        xL.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If xL.VenditaAcquisti = "A" Then
            Dim i As Integer
            For i = 0 To GridIVA.Rows.Count - 1
                Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImponibileIVA"), TextBox)
                Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)
                Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImpostaIVA"), TextBox)
                Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDDetraibileIVA"), DropDownList)
                Dim TxtSottocontoIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtSottocontoIVA"), TextBox)

                Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBDareIVA"), RadioButton)
                Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBAvereIVA"), RadioButton)
                Dim TxtDescrizioneIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtDescrizioneIVA"), TextBox)


                Dim dT As New ClsDetraibilita

                dT.Codice = DDDetraibileIVA.SelectedValue
                dT.Leggi(Session("DC_TABELLE"))

                If TxtImponibileIVA.Text <> "" Then
                    If xL.TipoDocumento = "NC" Then
                        If RBDareIVA.Checked = True Then
                            xTotaleImponibile = xTotaleImponibile - CDbl(TxtImponibileIVA.Text)
                        Else
                            xTotaleImponibile = xTotaleImponibile + CDbl(TxtImponibileIVA.Text)
                        End If
                    Else
                        If RBDareIVA.Checked = True Then
                            xTotaleImponibile = xTotaleImponibile + CDbl(TxtImponibileIVA.Text)
                        Else
                            xTotaleImponibile = xTotaleImponibile - CDbl(TxtImponibileIVA.Text)
                        End If
                    End If
                End If


                Dim TotaleImposta As Double = 0

                Dim Detraibile As Double
                Dim Indetraibile As Double
                Detraibile = Modulo.MathRound(dT.Aliquota * Modulo.MathRound(CDbl(TxtImpostaIVA.Text), 2), 2)
                Indetraibile = CDbl(TxtImpostaIVA.Text) - Detraibile

                Dim Prorata As Double
                Dim XP As New Cls_Prorata

                Prorata = 0
                XP.Anno = Val(Txt_AnnoProtocollo.Text) - 1
                XP.RegistroIVA = xL.RegistroIVA
                XP.Leggi(Session("DC_TABELLE"))
                Prorata = Modulo.MathRound(XP.DefinitivoPercentuale, 2)

                If dT.Prorata = 1 Then
                    If Modulo.MathRound(Indetraibile - Int(Indetraibile), 2) = 0.5 Then
                        Indetraibile = Indetraibile - Modulo.MathRound((Indetraibile - 0.01) * Modulo.MathRound(Prorata, 2), 2)
                    Else
                        Indetraibile = Indetraibile - Modulo.MathRound(Indetraibile * Modulo.MathRound(Prorata, 2), 2)
                    End If
                End If

                If TxtImpostaIVA.Text <> "" Then
                    xTotaleImposta = xTotaleImposta + CDbl(TxtImpostaIVA.Text)
                    If dT.Automatico = "S" Then
                        If xL.TipoDocumento = "NC" Then
                            If RBDareIVA.Checked = True Then
                                xTotaleIndetraibilita = xTotaleIndetraibilita - Indetraibile
                            Else
                                xTotaleIndetraibilita = xTotaleIndetraibilita + Indetraibile '-
                            End If
                        Else
                            If RBDareIVA.Checked = True Then
                                xTotaleIndetraibilita = xTotaleIndetraibilita + Indetraibile
                            Else
                                xTotaleIndetraibilita = xTotaleIndetraibilita - Indetraibile
                            End If
                        End If
                    End If
                End If




                If dT.Automatico = "S" Then
                    If dT.GiroACosto = "S" Then
                        Tabella = ViewState("TABELLACR")


                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = TxtSottocontoIVA.Text
                        If RBAvereIVA.Checked = True Then
                            myriga(1) = "D"
                        Else
                            myriga(1) = "A"
                        End If
                        myriga(2) = "N"
                        myriga(3) = 0
                        myriga(4) = Indetraibile
                        myriga(7) = "Giroconto per IVA Indetraibile"
                        Tabella.Rows.Add(myriga)


                        Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

                        myriga1(0) = TxtSottocontoIVA.Text
                        If RBDareIVA.Checked = True Then
                            myriga1(1) = "D"
                        Else
                            myriga1(1) = "A"
                        End If
                        myriga1(2) = "N"
                        myriga1(3) = 0

                        Dim xS As New Cls_Pianodeiconti

                        xS.Mastro = dT.IVAGiroACostoMastro
                        xS.Conto = dT.IVAGiroACostoConto
                        xS.Sottoconto = dT.IVAGiroACostoSottoconto

                        xS.Decodfica(Session("DC_GENERALE"))

                        myriga1(0) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
                        myriga1(4) = Indetraibile
                        myriga1(7) = "Giroconto a IVA Indetraibile"
                        Tabella.Rows.Add(myriga1)

                        Dim myriga2 As System.Data.DataRow = Tabella.NewRow()

                        myriga2(0) = TxtSottocontoIVA.Text
                        If RBAvereIVA.Checked = True Then
                            myriga2(1) = "D"
                        Else
                            myriga2(1) = "A"
                        End If
                        myriga2(2) = "N"
                        myriga2(3) = 0

                        Dim xS2 As New Cls_Pianodeiconti

                        xS2.Mastro = dT.IVAGiroACostoMastro
                        xS2.Conto = dT.IVAGiroACostoConto
                        xS2.Sottoconto = dT.IVAGiroACostoSottoconto

                        xS2.Decodfica(Session("DC_GENERALE"))
                        myriga2(4) = Indetraibile
                        myriga2(6) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS2.Descrizione
                        myriga2(7) = "Giroconto a Costi"
                        Tabella.Rows.Add(myriga2)

                        ViewState("TABELLACR") = Tabella

                        Call BindGridCR()
                    Else
                        Tabella = ViewState("TABELLACR")


                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = TxtSottocontoIVA.Text
                        If RBDareIVA.Checked = False Then
                            myriga(1) = "D"
                        Else
                            myriga(1) = "A"
                        End If
                        myriga(2) = "N"
                        myriga(3) = 0
                        myriga(4) = Format(Indetraibile, "#,##0.00")
                        myriga(7) = "Giroconto dell'IVA a costi"
                        Tabella.Rows.Add(myriga)


                        ViewState("TABELLACR") = Tabella
                        Call BindGridCR()
                    End If
                Else
                    If dT.GiroACosto = "S" Then
                        Tabella = ViewState("TABELLACR")


                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = TxtSottocontoIVA.Text
                        If RBAvereIVA.Checked = True Then
                            myriga(1) = "D"
                        Else
                            myriga(1) = "A"
                        End If
                        myriga(2) = "N"
                        myriga(3) = 0
                        myriga(4) = Format(Indetraibile, "#,##0.00")
                        myriga(7) = "Giroconto per IVA Indetraibile"
                        Tabella.Rows.Add(myriga)


                        Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

                        myriga1(0) = TxtSottocontoIVA.Text
                        If RBDareIVA.Checked = True Then
                            myriga1(1) = "D"
                        Else
                            myriga1(1) = "A"
                        End If
                        myriga1(2) = "N"
                        myriga1(3) = 0

                        Dim xS As New Cls_Pianodeiconti

                        xS.Mastro = dT.IVAGiroACostoMastro
                        xS.Conto = dT.IVAGiroACostoConto
                        xS.Sottoconto = dT.IVAGiroACostoSottoconto

                        xS.Decodfica(Session("DC_GENERALE"))

                        myriga1(0) = dT.IVAGiroACostoMastro & " " & dT.IVAGiroACostoConto & " " & dT.IVAGiroACostoSottoconto & " " & xS.Descrizione
                        myriga1(4) = Format(Indetraibile, "#,##0.00")
                        myriga1(7) = "Giroconto a IVA Indetraibile"
                        Tabella.Rows.Add(myriga1)



                        ViewState("TABELLACR") = Tabella

                        Call BindGridCR()
                    End If
                End If
            Next

            Tabella = ViewState("TABELLACR")
            'If campodb(Tabella.Rows(0).Item(4)) = "" Then
            Tabella.Rows(0).Item(4) = Format(xTotaleImponibile + xTotaleIndetraibilita, "#,##0.00")
            'Else
            'If CDbl(Tabella.Rows(0).Item(4)) = 0 Then
            Tabella.Rows(0).Item(4) = Format(xTotaleImponibile + xTotaleIndetraibilita, "#,##0.00")
            'End If
            'End If
            ViewState("TABELLACR") = Tabella

            Call BindGridCR()
        End If



    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        If Val(Txt_Numero.Text) > 0 Then
            Txt_Numero.Text = 0
            Txt_Numero.Enabled = True
            Txt_NumeroProtocollo.Text = 0
            Txt_DataRegistrazione.Enabled = True
            Call EseguiJS()
        End If
    End Sub

    Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
        Dim TotImp As Double

        If Val(Txt_Numero.Text) = 0 Then

            If Dd_CausaleContabile.SelectedValue = "" Then
                Exit Sub
            End If


            If GridIVA.Rows.Count = 1 Then
                Dim k As New Cls_ClienteFornitore
                Dim Mastro As Long
                Dim Conto As Long
                Dim Sottoconto As Long
                Dim Vettore(100) As String

                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                    k.MastroFornitore = Mastro
                    k.ContoFornitore = Conto
                    k.SottoContoFornitore = Sottoconto
                    k.Leggi(Session("DC_OSPITE"))
                    Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(0).FindControl("TxtImponibileIVA"), TextBox)
                    Dim Imponibile As Double
                    Try
                        Imponibile = TxtImponibileIVA.Text
                    Catch ex As Exception
                        Imponibile = 0
                    End Try

                    If k.DetrabilitaFornitore <> "" And Imponibile = 0 Then
                        Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(0).FindControl("DDDetraibileIVA"), DropDownList)

                        DDDetraibileIVA.SelectedValue = k.DetrabilitaFornitore
                    End If
                End If
            End If

            If GridCostiRicavi.Rows.Count = 1 Then
                Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(0).FindControl("TxtImportoCR"), TextBox)

                Dim X As New Cls_CausaleContabile
                Dim i As Long
                Dim TotInterno As Double = 0

                X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

                TabIVA = ViewState("TABELLAIVA")
                For i = 0 To TabIVA.Rows.Count - 1
                    If campodbN(TabIVA.Rows(i).Item("Imponibile")) > 0 Then
                        If campodb(TabIVA.Rows(i).Item("DareAvere")) <> campodb(X.Righe(0).DareAvere) Then
                            TotImp = TotImp + (CDbl(TabIVA.Rows(i).Item("Imponibile")))
                        Else
                            TotImp = TotImp - (CDbl(TabIVA.Rows(i).Item("Imponibile")))
                        End If
                    End If
                Next

                If TxtImportoCR.Text = "" Then
                    TxtImportoCR.Text = Format(TotImp, "#,##0.00")
                End If
                If CDbl(TxtImportoCR.Text) = 0 Then
                    TxtImportoCR.Text = Format(TotImp, "#,##0.00")
                End If
            End If

        End If
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
    End Sub

    Private Sub MettiInvisibileJS()
        Dim MyJs As String


        MyJs = "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('BOTTONEHOME')!= null) || (appoggio.match('BOTTONERICERCA')!= null) || (appoggio.match('BOTTONEADDSOCIO')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "



        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MettiInsibileJSCS", MyJs, True)
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        If TabContainer1.ActiveTabIndex = 1 Then
            Call InserisciRigaIVA()
            EseguiJS()
        End If
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi()
            EseguiJS()
        End If
        If TabContainer1.ActiveTabIndex = 3 Then
            Call InserisciRigaRitenuta()
        End If

    End Sub

    Protected Sub Btn_GirocontoaCosti_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Btn_GirocontoaCosti.Command
        Call UpTableCR()
        Call UpTableIVA()
        Call UpTableRT()
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub BtnRicalcola_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRicalcola.Click
        Call UpTableCR()
        Call UpTableIVA()
        Call UpTableRT()
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Updatecontainer_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Updatecontainer.DataBinding

    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Call UpTableCR()
        Call UpTableIVA()
        Call UpTableRT()
        Call RicalcolaImportiDocumento()

        Dim i As Integer
        Dim Stampa As New StampeGenerale


        Dim MyRecordStampaT As System.Data.DataRow

        Dim MyRecordStampaR As System.Data.DataRow


        MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow


        Dim CausaleContabiel As New Cls_CausaleContabile

        CausaleContabiel.Codice = Dd_CausaleContabile.SelectedValue
        CausaleContabiel.Leggi(Session("DC_TABELLE"), CausaleContabiel.Codice)



        Dim CliFor As New Cls_ClienteFornitore


        CliFor.CODICEDEBITORECREDITORE = 0
        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length < 3 Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If

        'If CausaleContabiel.VenditaAcquisti = "A" Then
        CliFor.MastroFornitore = Vettore(0)
        CliFor.ContoFornitore = Vettore(1)
        CliFor.SottoContoFornitore = Vettore(2)
        'Else
        '    CliFor.MastroCliente = Vettore(0)
        '    CliFor.ContoCliente = Vettore(1)
        '    CliFor.SottoContoCliente = Vettore(2)
        'End If

        CliFor.Leggi(Session("DC_OSPITE"))


        MyRecordStampaT.Item("RagioneSociale") = CliFor.Nome
        MyRecordStampaT.Item("Indirizzo") = CliFor.RESIDENZAINDIRIZZO1
        MyRecordStampaT.Item("CAP") = CliFor.RESIDENZACAP1

        Dim Comune As New ClsComune

        Comune.Provincia = CliFor.RESIDENZAPROVINCIA1
        Comune.Comune = CliFor.RESIDENZACOMUNE1
        Comune.Leggi(Session("DC_OSPITE"))

        MyRecordStampaT.Item("Localita") = Comune.Descrizione


        Dim Provincia As New ClsComune

        Provincia.Provincia = CliFor.RESIDENZAPROVINCIA1
        Provincia.Comune = ""
        Provincia.Leggi(Session("DC_OSPITE"))

        MyRecordStampaT.Item("Provincia") = Provincia.CodificaProvincia

        MyRecordStampaT.Item("NumeroFattura") = Txt_NumeroDocumento.Text

        MyRecordStampaT.Item("DataFattura") = Txt_DataDocumento.Text

        If CliFor.PARTITAIVA > 0 Then
            MyRecordStampaT.Item("PartitaIVACodiceFiscale") = CliFor.PARTITAIVA
        Else
            MyRecordStampaT.Item("PartitaIVACodiceFiscale") = CliFor.CodiceFiscale
        End If




        For i = 0 To GridIVA.Rows.Count - 1
            Dim TxtImponibileIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImponibileIVA"), TextBox)
            Dim DDIva As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDIva"), DropDownList)
            Dim TxtImpostaIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtImpostaIVA"), TextBox)
            Dim DDDetraibileIVA As DropDownList = DirectCast(GridIVA.Rows(i).FindControl("DDDetraibileIVA"), DropDownList)
            Dim TxtSottocontoIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtSottocontoIVA"), TextBox)

            Dim RBDareIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBDareIVA"), RadioButton)
            Dim RBAvereIVA As RadioButton = DirectCast(GridIVA.Rows(i).FindControl("RBAvereIVA"), RadioButton)
            Dim TxtDescrizioneIVA As TextBox = DirectCast(GridIVA.Rows(i).FindControl("TxtDescrizioneIVA"), TextBox)


            MyRecordStampaT.Item("Imposta1") = 0
            MyRecordStampaT.Item("AliquotaIVA1") = ""
            MyRecordStampaT.Item("Imponibile1") = 0


            MyRecordStampaT.Item("Imposta2") = 0
            MyRecordStampaT.Item("AliquotaIVA2") = ""
            MyRecordStampaT.Item("Imponibile2") = 0

            MyRecordStampaT.Item("Imposta3") = 0
            MyRecordStampaT.Item("AliquotaIVA3") = ""
            MyRecordStampaT.Item("Imponibile3") = 0


            MyRecordStampaT.Item("Imposta4") = 0
            MyRecordStampaT.Item("AliquotaIVA4") = ""
            MyRecordStampaT.Item("Imponibile4") = 0


            MyRecordStampaT.Item("Imposta5") = 0
            MyRecordStampaT.Item("AliquotaIVA5") = ""
            MyRecordStampaT.Item("Imponibile5") = 0


            If i = 0 Then
                MyRecordStampaT.Item("Imposta1") = TxtImpostaIVA.Text
                MyRecordStampaT.Item("AliquotaIVA1") = DDIva.SelectedValue & " " & DDIva.SelectedItem.Text
                MyRecordStampaT.Item("Imponibile1") = TxtImponibileIVA.Text
            End If

            If i = 1 Then
                MyRecordStampaT.Item("Imposta2") = TxtImpostaIVA.Text
                MyRecordStampaT.Item("AliquotaIVA2") = DDIva.SelectedValue & " " & DDIva.SelectedItem.Text
                MyRecordStampaT.Item("Imponibile2") = TxtImponibileIVA.Text
            End If

            If i = 2 Then
                MyRecordStampaT.Item("Imposta3") = TxtImpostaIVA.Text
                MyRecordStampaT.Item("AliquotaIVA3") = DDIva.SelectedValue & " " & DDIva.SelectedItem.Text
                MyRecordStampaT.Item("Imponibile3") = TxtImponibileIVA.Text
            End If



            If i = 3 Then
                MyRecordStampaT.Item("Imposta4") = TxtImpostaIVA.Text
                MyRecordStampaT.Item("AliquotaIVA4") = DDIva.SelectedValue & " " & DDIva.SelectedItem.Text
                MyRecordStampaT.Item("Imponibile4") = TxtImponibileIVA.Text
            End If

            If i = 4 Then
                MyRecordStampaT.Item("Imposta5") = TxtImpostaIVA.Text
                MyRecordStampaT.Item("AliquotaIVA5") = DDIva.SelectedValue & " " & DDIva.SelectedItem.Text
                MyRecordStampaT.Item("Imponibile5") = TxtImponibileIVA.Text
            End If

        Next


        For i = 0 To GridRitenuta.Rows.Count - 1
            Dim TxtImponibileRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtImponibileRT"), TextBox)
            Dim DDTRitenutaRt As DropDownList = DirectCast(GridRitenuta.Rows(i).FindControl("DDTRitenutaRt"), DropDownList)
            Dim TxtRitenutaRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtRitenutaRT"), TextBox)
            Dim TxtSottocontoDareRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtSottocontoDareRT"), TextBox)
            Dim TxtSottocontoAvereRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtSottocontoAvereRT"), TextBox)
            Dim TxtDescrizioneRT As TextBox = DirectCast(GridRitenuta.Rows(i).FindControl("TxtDescrizioneRT"), TextBox)

            MyRecordStampaT.Item("Ritenuta1") = DDTRitenutaRt.SelectedItem.Text

            Dim UltimaRitenuta = 0
            Try
                UltimaRitenuta = MyRecordStampaT.Item("ImportoRitenuta1")
            Catch ex As Exception

            End Try

            MyRecordStampaT.Item("ImportoRitenuta1") = UltimaRitenuta + TxtRitenutaRT.Text

        Next

        For i = 0 To GridCostiRicavi.Rows.Count - 1

            Dim TxtSottocontoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtSottocontoCR"), TextBox)

            Dim RBDareCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBDareCR"), RadioButton)
            Dim RBAvereCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBAvereCR"), RadioButton)
            Dim RBPorataSiCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBPorataSiCR"), RadioButton)
            Dim RBPorataNoCR As RadioButton = DirectCast(GridCostiRicavi.Rows(i).FindControl("RBPorataNoCR"), RadioButton)

            Dim TxtQuantitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtQuantitaCR"), TextBox)
            Dim TxtImportoCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtImportoCR"), TextBox)
            Dim DDIvaCr As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDIvaCr"), DropDownList)
            Dim TxtSottocontoControPartitaCR As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtSottocontoControPartitaCR"), TextBox)
            Dim TxtDescrizioneCr As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtDescrizioneCr"), TextBox)
            Dim TxtAnnoCr As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("TxtAnnoCr"), TextBox)
            Dim DDMeseCr As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDMeseCr"), DropDownList)
            Dim Txt_RigaRegistrazione As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_RigaRegistrazione"), TextBox)
            Dim Txt_RigaDaCausale As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_RigaDaCausale"), TextBox)
            Dim DDCentroServizio As DropDownList = DirectCast(GridCostiRicavi.Rows(i).FindControl("DDCentroServizio"), DropDownList)
            Dim Txt_TipoExtra As TextBox = DirectCast(GridCostiRicavi.Rows(i).FindControl("Txt_TipoExtra"), TextBox)

            MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow


            MyRecordStampaR.Item("NumeroFattura") = Txt_NumeroDocumento.Text
            MyRecordStampaR.Item("DataFattura") = Txt_DataDocumento.Text
            MyRecordStampaR.Item("DescrizioneRiga") = TxtDescrizioneCr.Text
            MyRecordStampaR.Item("Quantita") = TxtQuantitaCR.Text
            MyRecordStampaR.Item("Importo") = TxtImportoCR.Text
            MyRecordStampaR.Item("CodiceIVA") = DDIvaCr.SelectedValue

            Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
        Next

        Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)

        Session("stampa") = Stampa
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "window.open('Stampa_ReportXSD.aspx?REPORT=PROFORMAOPERATORI&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        EseguiJS()
        Exit Sub
    End Sub

    Protected Sub BtnInsertRiga3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga3.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(3)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga4.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(4)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga5.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(5)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga6.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(6)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga9.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(9)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga7.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(7)
            EseguiJS()
        End If
    End Sub

    Protected Sub BtnInsertRiga8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertRiga8.Click
        If TabContainer1.ActiveTabIndex = 2 Then
            Call InserisciCostiRicavi(8)
            EseguiJS()
        End If
    End Sub

    Private Sub RicalcolaIVA()
        UpTableCR()
        TabCR = ViewState("TABELLACR")
        Dim Vt_CodiceIva(100) As String
        Dim Vt_ImportoIva(100) As Double
        Dim Vt_ImponibileIva(100) As Double

        Dim Vt_SottoContoIva(100) As String
        Dim Vt_DareAvere(100) As String
        Dim Trovato As Boolean = False
        Dim MaxIT As Long
        MaxIT = 0

        For Riga = 0 To TabCR.Rows.Count - 1
            Trovato = False
            For i = 0 To MaxIT
                If Vt_CodiceIva(i) = TabCR.Rows(Riga).Item(5) Then
                    Vt_ImportoIva(i) = 0
                    If TabCR.Rows(Riga).Item(1) = "D" Then
                        Vt_ImponibileIva(i) = Modulo.MathRound(Vt_ImponibileIva(i) + TabCR.Rows(Riga).Item(4), 2)
                    Else
                        Vt_ImponibileIva(i) = Modulo.MathRound(Vt_ImponibileIva(i) - TabCR.Rows(Riga).Item(4), 2)
                    End If
                    Vt_SottoContoIva(i) = TabCR.Rows(Riga).Item(5)

                    Vt_DareAvere(i) = TabCR.Rows(Riga).Item(1)
                    Trovato = True
                    Exit For
                End If
            Next
            If Trovato = False Then
                MaxIT = MaxIT + 1
                Vt_CodiceIva(MaxIT) = TabCR.Rows(Riga).Item(5)
                Vt_ImportoIva(MaxIT) = 0
                If TabCR.Rows(Riga).Item(1) = "D" Then
                    Vt_ImponibileIva(MaxIT) = Modulo.MathRound(Vt_ImponibileIva(MaxIT) + TabCR.Rows(Riga).Item(4), 2)
                Else
                    Vt_ImponibileIva(MaxIT) = Modulo.MathRound(Vt_ImponibileIva(MaxIT) - TabCR.Rows(Riga).Item(4), 2)
                End If

                Vt_SottoContoIva(MaxIT) = TabCR.Rows(Riga).Item(1)
                Vt_DareAvere(MaxIT) = TabCR.Rows(Riga).Item(1)
            End If
        Next

        TabIVA.Clear()
        TabIVA.Columns.Clear()
        TabIVA.Columns.Add("Imponibile", GetType(String))
        TabIVA.Columns.Add("Iva", GetType(String))
        TabIVA.Columns.Add("Imposta", GetType(String))
        TabIVA.Columns.Add("Detraibile", GetType(String))
        TabIVA.Columns.Add("Sottoconto", GetType(String))
        TabIVA.Columns.Add("DareAvere", GetType(String))
        TabIVA.Columns.Add("Descrizione", GetType(String))

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Dd_CausaleContabile.SelectedValue
        CausaleContabile.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        For i = 0 To MaxIT
            If Vt_CodiceIva(i) <> "" Then
                Dim myriga1 As System.Data.DataRow = TabIVA.NewRow()
                myriga1(0) = Format(Math.Abs(Vt_ImponibileIva(i)), "#,##0.00")
                myriga1(1) = Vt_CodiceIva(i)
                Dim mIVA As New Cls_IVA

                mIVA.Leggi(Session("DC_TABELLE"), Vt_CodiceIva(i))


                myriga1(2) = Format(Math.Abs(Modulo.MathRound(Vt_ImponibileIva(i) * mIVA.Aliquota, 2)), "#,##0.00")
                myriga1(3) = mIVA.DetraibilitaDefault

                If CausaleContabile.Tipo = "I" Then
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CausaleContabile.Righe(2).Mastro
                    DecConto.Conto = CausaleContabile.Righe(2).Conto
                    DecConto.Sottoconto = CausaleContabile.Righe(2).Sottoconto
                    DecConto.Decodfica(Session("DC_GENERALE"))

                    myriga1(4) = CausaleContabile.Righe(2).Mastro & " " & CausaleContabile.Righe(2).Conto & " " & CausaleContabile.Righe(2).Sottoconto & " " & DecConto.Descrizione

                Else
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CausaleContabile.Righe(1).Mastro
                    DecConto.Conto = CausaleContabile.Righe(1).Conto
                    DecConto.Sottoconto = CausaleContabile.Righe(1).Sottoconto
                    DecConto.Decodfica(Session("DC_GENERALE"))

                    myriga1(4) = CausaleContabile.Righe(1).Mastro & " " & CausaleContabile.Righe(1).Conto & " " & CausaleContabile.Righe(1).Sottoconto & " " & DecConto.Descrizione
                End If
                If Vt_ImponibileIva(i) > 0 Then
                    myriga1(5) = "D"
                Else
                    myriga1(5) = "A"
                End If



                TabIVA.Rows.Add(myriga1)
            End If
        Next
        ViewState("TABELLAIVA") = TabIVA
        Call BindGridIva()

        EseguiJS()

    End Sub
    Protected Sub BtnRicalcolaIVA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRicalcolaIVA.Click
        If TabContainer1.ActiveTabIndex = 1 Then
            RicalcolaIVA()
        End If
    End Sub

    Protected Sub ChkAspetto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkAspetto.CheckedChanged
        Dim Parametri As New Cls_DatiGenerali

        Parametri.LeggiDati(Session("DC_TABELLE"))

        If ChkAspetto.Checked = True Then
            Parametri.TipoVisualizzazione = 1
            GridIVA.CellPadding = "0"
            GridIVA.CellSpacing = "0"

            GridCostiRicavi.CellPadding = "0"
            GridCostiRicavi.CellSpacing = "0"


            GridRitenuta.CellPadding = "0"
            GridRitenuta.CellSpacing = "0"
        Else
            Parametri.TipoVisualizzazione = 0
            GridIVA.CellPadding = "4"
            GridIVA.CellSpacing = "4"

            GridCostiRicavi.CellPadding = "4"
            GridCostiRicavi.CellSpacing = "4"


            GridRitenuta.CellPadding = "4"
            GridRitenuta.CellSpacing = "4"
        End If
        Parametri.ScriviDati(Session("DC_TABELLE"))
        BindGridCR()
    End Sub

    Protected Sub BntFlagRosso_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BntFlagRosso.Click
        REM qui codice
    End Sub
End Class




