﻿
Partial Class GeneraleWeb_ElencoPrenotazioni
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            Exit Sub
        End If
        Dim kX As New Cls_PrenotazioniTesta


        kX.loaddati(Session("DC_GENERALE"), Txt_Anno.Text, MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Prenotazioni.AutoGenerateColumns = True
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kX As New Cls_PrenotazioniTesta


        Txt_Anno.Text = Year(Now)

        kX.loaddati(Session("DC_GENERALE"), Year(Now), MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Prenotazioni.AutoGenerateColumns = True
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Prenotazioni.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_Prenotazioni.PageIndex = e.NewPageIndex
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Prenotazioni.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = MyTable.Rows(d).Item(2).ToString


            Response.Redirect("GestionePrenotazione.aspx?NUMERO=" & Codice & "&Anno=" & Txt_Anno.Text)
        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Prenotazioni.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestionePrenotazione.aspx?NUMERO=0&Anno=" & Txt_Anno.Text)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub
End Class

