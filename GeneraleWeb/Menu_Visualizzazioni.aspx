﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Visualizzazioni" CodeFile="Menu_Visualizzazioni.aspx.vb" %>

<head id="Head1" runat="server">
    <title>Menu Generale - Visualizzazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Generale</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizzazioni<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center; width: 20%;">
                                    <a href="DocumentiScoperti.aspx">
                                        <img src="../images/Menu_DocumentiScoperti.png" class="Effetto" alt="Documenti Scoperti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="VisualizzaBilanciPeriodoConfronto.aspx">
                                        <img style="border-width: 0;" src="../images/Menu_PeriodiAConfronto.png" class="Effetto" alt="Periodo a Confronto" /></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="VisualizzaScadenzario.aspx">
                                        <img src="../images/Menu_Scadenzario.png" class="Effetto" alt="Scadenzario" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="VisualizzazioneRitenute.aspx">
                                        <img src="../images/Bottone_Ritenute.jpg" class="Effetto" alt="RITENUTE" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="VisualizzaCig.aspx">
                                        <img src="../images/Menu_DocumentiScoperti.png" class="Effetto" alt="Visualizza Cig" style="border-width: 0;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">DOCUMENTI SCOPERTI</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">PERIODO A CONFRONTO</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">SCADENZARIO</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">RITENUTE</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>

                            <tr>
                    </td>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>

                <tr>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                </tr>

            </table>
            </td>
    </tr>
    
    <tr>
        <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
        </td>
        <td></td>
        <td></td>
    </tr>
            </table> 
    
       
        </div>
    </form>
</body>
</html>
