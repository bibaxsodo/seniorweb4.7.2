﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Collections.Generic

Partial Class GeneraleWeb_ImportCorrispettivi
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub


    Private Sub ImportFile()

        Dim NomeSocieta As String

        Lbl_Errori.Text = ""
        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")

        Dim Line As String = ""
        Dim Registrazione As Cls_MovimentoContabile = Nothing

        Dim MyTestataCorrispettivi As New TestataCorrispettivi
        Dim MyMovimentiContabili As MovimentiContabili
        Dim MyPagamenti As Pagamenti
        Dim Trascodifica As New Cls_TabellaTrascodificheEsportazioni
        Dim CausaleContabileCorrispettivo As New Cls_CausaleContabile
        Dim CausaleContabileIncasso As Cls_CausaleContabile

        Dim DatiGenerali As New Cls_DatiGenerali
        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        Dim Indice As Integer = 0
        Using objStreamReader As StreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")

            While Not objStreamReader.EndOfStream
                Line = objStreamReader.ReadLine
                Select Case Line.Substring(0, 4)
                    Case "T001"
                        MyTestataCorrispettivi = ParseTestataCorrispettivi(Line)

                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = MyTestataCorrispettivi.CausaleContabile
                        Trascodifica.TIPOTAB = "CD"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))

                        CausaleContabileCorrispettivo = New Cls_CausaleContabile
                        CausaleContabileCorrispettivo.Codice = Trascodifica.SENIOR
                        CausaleContabileCorrispettivo.Leggi(Session("DC_TABELLE"), CausaleContabileCorrispettivo.Codice)

                        Registrazione = New Cls_MovimentoContabile
                        Registrazione.DataRegistrazione = DateTime.ParseExact(MyTestataCorrispettivi.DataRegistrazione, "yyyyMMdd", Nothing)
                        Registrazione.AnnoProtocollo = Year(Registrazione.DataRegistrazione)
                        Registrazione.NumeroProtocollo = 0
                        Registrazione.RegistroIVA = CausaleContabileCorrispettivo.RegistroIVA
                        Registrazione.CausaleContabile = CausaleContabileCorrispettivo.Codice
                        Registrazione.DataDocumento = DateTime.ParseExact(MyTestataCorrispettivi.DataRegistrazione, "yyyyMMdd", Nothing)
                        Registrazione.CodicePagamento = ""
                        Registrazione.Descrizione = MyTestataCorrispettivi.Descrizione

                        Indice = 0
                        Registrazione.Righe(Indice) = New Cls_MovimentiContabiliRiga

                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = "CLIENTE"
                        Trascodifica.TIPOTAB = "CG"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))

                        Registrazione.Righe(Indice).MastroPartita = DatiGenerali.ClientiMastro
                        Registrazione.Righe(Indice).ContoPartita = DatiGenerali.ClientiConto
                        Registrazione.Righe(Indice).SottocontoPartita = Trascodifica.SENIOR
                        Registrazione.Righe(Indice).DareAvere = CausaleContabileCorrispettivo.Righe(0).DareAvere
                        Registrazione.Righe(Indice).Segno = CausaleContabileCorrispettivo.Righe(0).Segno
                        Registrazione.Righe(Indice).Importo = MyTestataCorrispettivi.TotaleCorrispettivo
                        Registrazione.Righe(Indice).Tipo = "CF"
                        Registrazione.Righe(Indice).RigaDaCausale = 1

                        For Each MyIVA In MyTestataCorrispettivi.ElencoIve
                            If MyIVA.ImponibileIva > 0 Then
                                Indice += 1
                                Registrazione.Righe(Indice) = New Cls_MovimentiContabiliRiga
                                Registrazione.Righe(Indice).MastroPartita = CausaleContabileCorrispettivo.Righe(2).Mastro
                                Registrazione.Righe(Indice).ContoPartita = CausaleContabileCorrispettivo.Righe(2).Conto
                                Registrazione.Righe(Indice).SottocontoPartita = CausaleContabileCorrispettivo.Righe(2).Sottoconto

                                Registrazione.Righe(Indice).Imponibile = MyIVA.ImponibileIva

                                Trascodifica.SENIOR = ""
                                Trascodifica.EXPORT = MyIVA.CodiceIva
                                Trascodifica.TIPOTAB = "IV"
                                Trascodifica.LeggiDaExport(Session("DC_TABELLE"))
                                Registrazione.Righe(Indice).CodiceIVA = Trascodifica.SENIOR
                                Registrazione.Righe(Indice).DareAvere = CausaleContabileCorrispettivo.Righe(2).DareAvere
                                Registrazione.Righe(Indice).Segno = CausaleContabileCorrispettivo.Righe(2).Segno
                                Registrazione.Righe(Indice).Importo = MyIVA.ImportoIva
                                Registrazione.Righe(Indice).Tipo = "IV"
                                Registrazione.Righe(Indice).RigaDaCausale = 3
                            End If
                        Next
                    Case "D001"
                        MyMovimentiContabili = ParseMovimentiContabili(Line)

                        Indice += 1

                        Registrazione.Righe(Indice) = New Cls_MovimentiContabiliRiga

                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = MyMovimentiContabili.ContoContabile
                        Trascodifica.TIPOTAB = "CC"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))

                        Dim Vettore() As String = SplitWords(Trascodifica.SENIOR)

                        Registrazione.Righe(Indice).MastroPartita = Vettore(0)
                        Registrazione.Righe(Indice).ContoPartita = Vettore(1)
                        Registrazione.Righe(Indice).SottocontoPartita = Vettore(2)
                        Registrazione.Righe(Indice).DareAvere = CausaleContabileCorrispettivo.Righe(1).DareAvere
                        Registrazione.Righe(Indice).Segno = CausaleContabileCorrispettivo.Righe(1).Segno
                        Registrazione.Righe(Indice).Importo = MyMovimentiContabili.Importo
                        Registrazione.Righe(Indice).Descrizione = MyMovimentiContabili.Stringa
                        Registrazione.Righe(Indice).MastroContropartita = Registrazione.Righe(0).MastroPartita
                        Registrazione.Righe(Indice).ContoContropartita = Registrazione.Righe(0).ContoPartita
                        Registrazione.Righe(Indice).SottocontoContropartita = Registrazione.Righe(0).SottocontoPartita

                    Case "M001"
                        If Registrazione.NumeroRegistrazione = 0 Then
                            If VerificaRegistrazionePresente(Session("DC_GENERALE"), Registrazione.CausaleContabile, Registrazione.DataRegistrazione) Then
                                Lbl_Errori.Text = Lbl_Errori.Text & "Registrazione presente non posso procedere" & "<br/>"
                                Exit While
                            End If
                            If Not Registrazione.Scrivi(Session("DC_GENERALE"), 0) Then
                                Lbl_Errori.Text = Lbl_Errori.Text & "Errore in registrazione documento" & "<br/>"
                            Else
                                Lbl_Errori.Text = Lbl_Errori.Text & "Registrato Documento numero " & Registrazione.NumeroRegistrazione & "<br/>"
                            End If
                        End If
                        Dim RegistrazioneIncasso As New Cls_MovimentoContabile
                        MyPagamenti = ParsePagamenti(Line)
                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = MyPagamenti.CausaleContabile
                        Trascodifica.TIPOTAB = "CD"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))


                        CausaleContabileIncasso = New Cls_CausaleContabile
                        CausaleContabileIncasso.Codice = Trascodifica.SENIOR
                        CausaleContabileIncasso.Leggi(Session("DC_TABELLE"), CausaleContabileIncasso.Codice)

                        Registrazione.AnnoProtocollo = Year(Registrazione.DataRegistrazione)
                        RegistrazioneIncasso.DataRegistrazione = DateTime.ParseExact(MyTestataCorrispettivi.DataRegistrazione, "yyyyMMdd", Nothing)
                        RegistrazioneIncasso.CausaleContabile = CausaleContabileIncasso.Codice
                        RegistrazioneIncasso.Descrizione = "descrizione"

                        Indice = 0
                        RegistrazioneIncasso.Righe(Indice) = New Cls_MovimentiContabiliRiga


                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = "CLIENTE"
                        Trascodifica.TIPOTAB = "CG"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))


                        RegistrazioneIncasso.Righe(Indice).MastroPartita = DatiGenerali.ClientiMastro
                        RegistrazioneIncasso.Righe(Indice).ContoPartita = DatiGenerali.ClientiConto
                        RegistrazioneIncasso.Righe(Indice).SottocontoPartita = Trascodifica.SENIOR
                        RegistrazioneIncasso.Righe(Indice).Tipo = "CF"

                        RegistrazioneIncasso.Righe(Indice).DareAvere = CausaleContabileIncasso.Righe(0).DareAvere
                        RegistrazioneIncasso.Righe(Indice).Segno = CausaleContabileIncasso.Righe(0).Segno
                        RegistrazioneIncasso.Righe(Indice).Importo = MyPagamenti.Importo


                        Indice = Indice + 1
                        RegistrazioneIncasso.Righe(Indice) = New Cls_MovimentiContabiliRiga

                        Trascodifica.SENIOR = ""
                        Trascodifica.EXPORT = MyPagamenti.ContoPagamento
                        Trascodifica.TIPOTAB = "CC"
                        Trascodifica.LeggiDaExport(Session("DC_TABELLE"))


                        Dim Vettore() As String = SplitWords(Trascodifica.SENIOR)

                        RegistrazioneIncasso.Righe(Indice).MastroPartita = Vettore(0)
                        RegistrazioneIncasso.Righe(Indice).ContoPartita = Vettore(1)
                        RegistrazioneIncasso.Righe(Indice).SottocontoPartita = Vettore(2)
                        RegistrazioneIncasso.Righe(Indice).Tipo = ""

                        RegistrazioneIncasso.Righe(Indice).DareAvere = CausaleContabileIncasso.Righe(1).DareAvere
                        RegistrazioneIncasso.Righe(Indice).Segno = CausaleContabileIncasso.Righe(1).Segno
                        RegistrazioneIncasso.Righe(Indice).Importo = MyPagamenti.Importo
                        RegistraIncassi(Registrazione, RegistrazioneIncasso, MyTestataCorrispettivi)

                        Lbl_Errori.Text = Lbl_Errori.Text & "Registrato Incasso numero " & RegistrazioneIncasso.NumeroRegistrazione & "<br/>"
                End Select
            End While

        End Using
        If Not IsNothing(Registrazione) Then
            If Registrazione.NumeroRegistrazione = 0 Then
                If VerificaRegistrazionePresente(Session("DC_GENERALE"), Registrazione.CausaleContabile, Registrazione.DataRegistrazione) Then
                    Lbl_Errori.Text = Lbl_Errori.Text & "Registrazione presente non posso procedere" & "<br/>"
                    Exit Sub
                End If
                If Not Registrazione.Scrivi(Session("DC_GENERALE"), 0) Then
                    Lbl_Errori.Text = Lbl_Errori.Text & "Errore in registrazione documento" & "<br/>"
                Else
                    Lbl_Errori.Text = Lbl_Errori.Text & "Registrato Documento numero " & Registrazione.NumeroRegistrazione & "<br/>"
                End If
            End If
        End If
    End Sub

    Private Sub RegistraIncassi(Registrazione As Cls_MovimentoContabile, RegistrazioneIncasso As Cls_MovimentoContabile, MyTestataCorrispettivi As TestataCorrispettivi)

        RegistrazioneIncasso.Scrivi(Session("DC_GENERALE"), RegistrazioneIncasso.NumeroRegistrazione)

        'Creazione Legame fra documento e incasso nella tabella  TabellaLegami
        Dim Legami As New Cls_Legami

        Legami.NumeroDocumento(0) = Registrazione.NumeroRegistrazione
        Legami.NumeroPagamento(0) = RegistrazioneIncasso.NumeroRegistrazione
        Legami.Importo(0) = RegistrazioneIncasso.Righe(0).Importo
        Legami.Scrivi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione, 0)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Public Function VerificaRegistrazionePresente(ByVal StringaConnessione As String, ByVal CausaleContabile As String, ByVal DataRegistrazione As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        VerificaRegistrazionePresente = False
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from MovimentiContabiliTesta where CausaleContabile= ? And DataRegistrazione = ?")
        cmd.Parameters.AddWithValue("@CausaleContabile", CausaleContabile)
        cmd.Parameters.AddWithValue("@DataRegistrazione", DataRegistrazione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            VerificaRegistrazionePresente = True
        End If
        myPOSTreader.Close()
        cn.Close()



    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Function ParseTestataCorrispettivi(ByVal Linea As String) As TestataCorrispettivi
        Dim MyTestataCorrispetivi As New TestataCorrispettivi
        MyTestataCorrispetivi.Identificativo = Linea.Substring(0, 24)
        MyTestataCorrispetivi.TipoCliente = Linea.Substring(24, 12)
        MyTestataCorrispetivi.DataRegistrazione = Linea.Substring(41, 8)
        MyTestataCorrispetivi.CausaleContabile = Linea.Substring(49, 5)
        MyTestataCorrispetivi.TotaleCorrispettivo = 0
        MyTestataCorrispetivi.Data = Linea.Substring(95, 8)

        Dim i As Integer = 124
        Dim j As Integer = 129
        Dim k As Integer = 137
        For n = 0 To 5
            Dim MyIva As New ElencoIva
            MyIva.CodiceIva = Linea.Substring(i, 5)
            Double.TryParse(Linea.Substring(j, 8), MyIva.ImponibileIva)
            MyIva.ImponibileIva /= 100
            Double.TryParse(Linea.Substring(k, 8), MyIva.ImportoIva)
            MyIva.ImportoIva /= 100
            If MyIva.ImponibileIva <> 0 Then
                MyTestataCorrispetivi.ElencoIve.Add(MyIva)
            End If
            MyTestataCorrispetivi.TotaleCorrispettivo += (MyIva.ImponibileIva + MyIva.ImportoIva)
            i += 21
            j += 21
            k += 21
        Next

        Return MyTestataCorrispetivi
    End Function

    Private Function ParseMovimentiContabili(ByVal Linea As String) As MovimentiContabili
        Dim MyMovimentoContabile As New MovimentiContabili
        MyMovimentoContabile.Identificativo = Linea.Substring(0, 24)
        MyMovimentoContabile.ContoContabile = Linea.Substring(24, 15)
        MyMovimentoContabile.ContoContabile2 = Linea.Substring(39, 15)
        MyMovimentoContabile.Stringa = Linea.Substring(54, 15)
        MyMovimentoContabile.Segno = Linea.Substring(69, 1)
        MyMovimentoContabile.CodiceIVA = Linea.Substring(78, 5)
        MyMovimentoContabile.Data = Linea.Substring(134, 8)
        MyMovimentoContabile.Data2 = Linea.Substring(142, 8)

        Double.TryParse(Linea.Substring(70, 8), MyMovimentoContabile.Importo)
        MyMovimentoContabile.Importo = MyMovimentoContabile.Importo / 100
        Return MyMovimentoContabile
    End Function

    Private Function ParsePagamenti(ByVal Linea As String) As Pagamenti
        Dim MyPagamento As New Pagamenti
        MyPagamento.Identificativo = Linea.Substring(0, 24)
        MyPagamento.TipoCliente = Linea.Substring(24, 12)
        MyPagamento.Data = Linea.Substring(41, 8)
        MyPagamento.CausaleContabile = Linea.Substring(49, 5)
        MyPagamento.NumeroRegistrazione = Linea.Substring(54, 6)
        MyPagamento.Riga = Linea.Substring(60, 2)
        MyPagamento.ContoPagamento = Linea.Substring(82, 12)
        MyPagamento.Segno = Linea.Substring(94, 1)
        MyPagamento.Descrizione = Linea.Substring(103, 50)

        Double.TryParse(Linea.Substring(95, 8), MyPagamento.Importo)
        MyPagamento.Importo /= 100
        Return MyPagamento
    End Function

    Private Sub btn_Import_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btn_Import.Click
        ImportFile()
    End Sub

    Private Sub ImgMenu_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Private Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class

Class TestataCorrispettivi
    Public Identificativo As String ' “T001”+Anno+”_”+Nr registrazione Stringa

    Public ReadOnly Property AnnoProtocollo() As String
        Get
            Return Identificativo.Split("_")(0).Substring(4)
        End Get
    End Property

    Public TipoCliente As String = "C1" ' generico C1
    Public DataRegistrazione As String ' yyyymmdd (anno mese giorno) stringa
    Public CausaleContabile As String
    Public NumeroRegistrazione As String '000000 Numero
    Public TotaleCorrispettivo As Double '00000000 (importo *100) Numero
    Public Data As String 'yyyymmdd (anno mese giorno) stringa
    Public Descrizione As String  'Numero Documenti...."
    Public ElencoIve As New List(Of ElencoIva)
End Class

Class ElencoIva
    Public CodiceIva As String
    Public ImponibileIva As Double
    Public ImportoIva As Double
End Class

Class MovimentiContabili
    Public Identificativo As String
    Public ContoContabile As String
    Public ContoContabile2 As String
    Public Stringa As String = "CAMPEGGIO"
    Public Segno As String
    Public Importo As Double
    Public CodiceIVA As String
    Public Data As String
    Public Data2 As String
End Class

Class Pagamenti
    Public Identificativo As String
    Public TipoCliente As String = "C1"
    Public Data As String
    Public CausaleContabile As String
    Public NumeroRegistrazione As String
    Public Riga As String
    Public ContoPagamento As String
    Public Segno As String
    Public Importo As Double
    Public Descrizione As String
End Class
