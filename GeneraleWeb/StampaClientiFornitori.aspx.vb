﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO

Partial Class GeneraleWeb_StampaClientiFornitori
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then

            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Stampa As New StampaClientiFornitori
        Dim cn As OleDbConnection
        Dim cnGenerale As OleDbConnection
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cnGenerale = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnGenerale.Open()
        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim Condizione As String = ""

        If RB_Clienti.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MastroCliente > 0 "
        End If
        If RB_Fornitori.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MastroFornitore > 0 "
        End If


        MySql = "SELECT * From AnagraficaComune Where " & Condizione & " And TIPOLOGIA = 'D'"

        cmd.CommandText = MySql
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Usato As Boolean = False
            If Rb_UsatiTutti.Checked = False Then

                If campodbN(myPOSTreader.Item("MastroCliente")) > 0 Then
                    Dim cmd1 As New OleDbCommand()
                    cmd1.CommandText = "Select count(*) from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ?"
                    cmd1.Connection = cnGenerale
                    cmd1.Parameters.AddWithValue("@MastroPartita", campodbN(myPOSTreader.Item("MastroCliente")))
                    cmd1.Parameters.AddWithValue("@ContoPartita", campodbN(myPOSTreader.Item("ContoCliente")))
                    cmd1.Parameters.AddWithValue("@SottoContoPartita", campodbN(myPOSTreader.Item("SottocontoCliente")))

                    Dim RdMastro As OleDbDataReader = cmd1.ExecuteReader()
                    If RdMastro.Read Then
                        If campodbN(RdMastro.Item(0)) > 0 Then
                            Usato = True
                        End If
                    End If
                    RdMastro.Close()
                    cmd1.Dispose()
                End If

                If campodbN(myPOSTreader.Item("MastroFornitore")) > 0 Then
                    Dim cmd2 As New OleDbCommand()
                    cmd2.CommandText = "Select count(*) from MovimentiContabiliRiga Where MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ?"
                    cmd2.Connection = cnGenerale
                    cmd2.Parameters.AddWithValue("@MastroPartita", campodbN(myPOSTreader.Item("MastroFornitore")))
                    cmd2.Parameters.AddWithValue("@ContoPartita", campodbN(myPOSTreader.Item("ContoFornitore")))
                    cmd2.Parameters.AddWithValue("@SottoContoPartita", campodbN(myPOSTreader.Item("SottocontoFornitore")))

                    Dim RdMastro2 As OleDbDataReader = cmd2.ExecuteReader()
                    If RdMastro2.Read Then
                        If campodbN(RdMastro2.Item(0)) > 0 Then
                            Usato = True
                        End If
                    End If
                    RdMastro2.Close()
                    cmd2.Dispose()
                End If

            End If

            If Rb_UsatiTutti.Checked = True Or (Rb_UsatiNO.Checked = True And Usato = False) Or (Rb_UsatiSI.Checked = True And Usato = True) Then
                Dim k As System.Data.DataRow = Stampa.Tables("ClientiFornitori").NewRow

                Dim CliFor As New Cls_ClienteFornitore

                CliFor.CODICEDEBITORECREDITORE = campodbN(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
                CliFor.Leggi(Session("DC_OSPITE"))

                k.Item("Codice") = campodbN(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
                k.Item("RagioneSociale") = CliFor.Nome
                k.Item("Indirizzo") = CliFor.RESIDENZAINDIRIZZO1

                Dim DecCom As New ClsComune

                DecCom.Provincia = CliFor.RESIDENZAPROVINCIA1
                DecCom.Comune = CliFor.RESIDENZACOMUNE1
                DecCom.DecodficaComune(Session("DC_OSPITE"))
                k.Item("Comune") = DecCom.Descrizione
                k.Item("Cap") = CliFor.RESIDENZACAP1
                k.Item("Telefono") = CliFor.Telefono1
                k.Item("Fax") = CliFor.Telefono2
                k.Item("CodiceFiscale") = CliFor.CodiceFiscale
                k.Item("PIVA") = CliFor.PARTITAIVA

                Dim DatiSoc As New Cls_TabellaSocieta

                DatiSoc.Leggi(Session("DC_TABELLE"))

                k.Item("Intestazione") = DatiSoc.RagioneSociale

                If CliFor.MastroCliente > 0 Then
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CliFor.MastroCliente
                    DecConto.Conto = CliFor.ContoCliente
                    DecConto.Sottoconto = CliFor.SottoContoCliente
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    k.Item("ContoCliente") = DecConto.Descrizione
                End If

                If CliFor.MastroFornitore > 0 Then
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CliFor.MastroFornitore
                    DecConto.Conto = CliFor.ContoFornitore
                    DecConto.Sottoconto = CliFor.SottoContoFornitore
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    k.Item("ContoFornitore") = DecConto.Descrizione
                End If

                If CliFor.MastroRicavo > 0 Then
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CliFor.MastroRicavo
                    DecConto.Conto = CliFor.ContoRicavo
                    DecConto.Sottoconto = CliFor.SottocontoRicavo
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    k.Item("ContoRicavo") = DecConto.Descrizione
                End If
                If CliFor.MastroCosto > 0 Then
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = CliFor.MastroCosto
                    DecConto.Conto = CliFor.ContoCosto
                    DecConto.Sottoconto = CliFor.SottocontoCosto
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    k.Item("ContoCosto") = DecConto.Descrizione
                End If

                If CliFor.ABICliente > 0 Then

                    k.Item("Iban") = Format(CliFor.IntCliente, "##") & Format(CliFor.NumeroControlloCliente, "00") & Format(CliFor.CinCliente, "#") & Format(CliFor.ABICliente, "00000") & Format(CliFor.CABCliente, "00000") & CliFor.CCBancarioCliente
                End If

                If CliFor.ABIFornitore > 0 Then

                    k.Item("Iban") = Format(CliFor.IntFornitore, "##") & Format(CliFor.NumeroControlloFornitore, "00") & Format(CliFor.CinFornitore, "#") & Format(CliFor.ABIFornitore, "00000") & Format(CliFor.CABFornitore, "00000") & CliFor.CCBancarioFornitore
                End If


                Stampa.Tables("ClientiFornitori").Rows.Add(k)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        cnGenerale.Close()

        Session("stampa") = Stampa

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=CLIENTIFORNITORI&PRINTERKEY=OFF&XSD=SI','Stampe','width=800,height=600');", True)

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub
End Class
