﻿
Partial Class GeneraleWeb_Menu_Tabelle
    Inherits System.Web.UI.Page

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If



        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))
        If IsNothing(k.ABILITAZIONI) Then
            k.ABILITAZIONI = ""
        End If

        If k.ABILITAZIONI.IndexOf("<CAUSALICONTABILI>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_CausaliContabili')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS1", MyJs, True)
        End If
        If k.ABILITAZIONI.IndexOf("<MODALITAPAGAMENTO>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_ModalitaPagamento')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS2", MyJs, True)
        End If
        If k.ABILITAZIONI.IndexOf("<TIPOREGISTRO>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_TipoRegistro')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS3", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTROIVA>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_RegistroIVA')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS4", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<DETRAIBILITA>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Detraibilita')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS5", MyJs, True)
        End If
        If k.ABILITAZIONI.IndexOf("<PERIODOCONFRONTO>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_PeriodoaConfronto')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS6", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<RITENUTE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Ritenute')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS7", MyJs, True)
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            Try
                K1 = Session("RicercaGeneraleSQLString")
            Catch ex As Exception

            End Try
            Session("RicercaGeneraleSQLString")= Nothing
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub
End Class
