﻿
Partial Class GeneraleWeb_Tributi
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Ks As New Cls_TabellaTributo


        If Request.Item("Codice") = "" Then

            Txt_Codice.Text = Ks.MaxTabellaTributo(Session("DC_TABELLE"))
            Exit Sub
        End If

        
        Ks.Leggi(Session("DC_TABELLE"), Request.Item("Codice"))


        Txt_Codice.Enabled = False

        Txt_Codice.Text = Ks.Codice
        Txt_Descrizione.Text = Ks.Descrizione
 
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ks As New Cls_TabellaTributo
        Lbl_Errori.Text = ""
        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            REM Lbl_Errori.Text = "Il codice deve essere maggiore di zero"
            Exit Sub
        End If

        If Trim(Txt_Descrizione.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            REM Lbl_Errori.Text = "Descrizione obbligatoria"
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim Verifica As New Cls_TabellaTributo

            Verifica.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

            If Verifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                REM Lbl_Errori.Text = "Descrizione obbligatoria"
                Exit Sub
            End If
        End If

        'Dim I As Long

        Ks.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)


        Ks.Codice = Txt_Codice.Text
        Ks.Descrizione = Txt_Descrizione.Text

        Ks.Scrivi(Session("DC_TABELLE"))



        Response.Redirect("Elenco_Tributi.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Tributi.aspx")
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TabellaTributo

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TabellaTributo

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione)

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim Ks As New Cls_TabellaTributo

        Ks.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)


        Ks.Codice = Txt_Codice.Text
        If Ks.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If



        Ks.Elimina(Session("DC_TABELLE"))

        Response.Redirect("Elenco_Tributi.aspx")
    End Sub

End Class
