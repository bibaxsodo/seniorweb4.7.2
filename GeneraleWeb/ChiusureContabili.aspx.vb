﻿
Partial Class GeneraleWeb_ChiusureContabili
    Inherits System.Web.UI.Page





    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataApertura.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataApertura.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataApertura", MyJs, True)



        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)





        Txt_Anno.Text = Year(Now)
        Txt_DataApertura.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataApertura.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data apertura formalemente errata');", True)
            Exit Sub
        End If
        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        Dim XR As New Cls_ChiusureContabili

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        XR.StringaConnessione = Session("DC_GENERALE")
        XR.StringaConnessioneTabelle = Session("DC_TABELLE")

        XR.Apridb(Session("DC_GENERALE"))
        XR.DataInizio = DateSerial(Txt_Anno.Text, DatiGenerali.EsercizioMese, DatiGenerali.EsercizioGiorno)
        XR.DataFine = DateAdd("yyyy", 1, XR.DataInizio)
        XR.DataFine = DateAdd("d", -1, XR.DataFine)
        XR.DataFineNext = DateAdd("yyyy", 1, XR.DataFine)
        XR.DataAperture = Txt_DataApertura.Text

        If Chk_Prova.Checked = True Then
            XR.Chk_Effettiva = 0
        Else
            XR.Chk_Effettiva = 1
        End If

        '
        ' Costi e Ricavi
        '
        XR.CreaChiusura("C")
        XR.CreaChiusura("R")
        '
        ' Costi e Ricavi
        '
        XR.RilevaUtilePerdita()
        '
        ' Attività, Passività e Conti d'Ordine
        '
        XR.CreaApertura("A")
        XR.CreaApertura("P")
        XR.CreaApertura("O")
        XR.CreaChiusura("A")
        XR.CreaChiusura("P")
        XR.CreaChiusura("O")

        XR.Chiudidb()
        If Chk_Prova.Checked = False Then
            DatiGenerali.DataChiusura = XR.DataFine

            DatiGenerali.ScriviDati(Session("DC_TABELLE"))
        End If

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = XR.Tabella
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
