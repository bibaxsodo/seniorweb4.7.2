﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class GeneraleWeb_StampaRateiRisconti
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_AnnoInizio.Text = Year(Now) - 1

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Import')!= null) || appoggio.match('Base')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Stampa As New StampeGenerale
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        cmd.CommandText = ("select * from RateiRisconti Where year(ServizioDal) = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@AnnoAcq", Txt_AnnoInizio.Text)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim WrRs As System.Data.DataRow = Stampa.Tables("RateiRisconti").NewRow

            If campodb(myPOSTreader.Item("RateoRisconto")) = "A" Then
                WrRs.Item("RateoRisconto") = "Rateo"
            Else
                WrRs.Item("RateoRisconto") = "Risconto"
            End If

            WrRs.Item("NumeroRegistrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))

            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.Leggi(Session("DC_GENERALE"), campodbN(myPOSTreader.Item("NumeroRegistrazione")))

            Dim DecConto As New Cls_Pianodeiconti

            DecConto.Mastro = Registrazione.Righe(0).MastroPartita
            DecConto.Conto = Registrazione.Righe(0).ContoPartita
            DecConto.Sottoconto = Registrazione.Righe(0).SottocontoPartita
            DecConto.Descrizione = ""
            DecConto.Decodfica(Session("DC_GENERALE"))


            WrRs.Item("Fornitore") = DecConto.Descrizione


            WrRs.Item("ImportoTotale") = campodb(myPOSTreader.Item("ImportoTotale"))
            Dim MioImpGG As Integer
            Dim GGFineAnno As Integer
            Dim GGDaInizioAnno As Integer

            MioImpGG = DateDiff("d", campodbd(myPOSTreader.Item("ServizioDal")), campodbd(myPOSTreader.Item("Servizioal"))) + 1
            GGFineAnno = DateDiff("d", campodbd(myPOSTreader.Item("ServizioDal")), DateSerial(Txt_AnnoInizio.Text, 12, 31)) + 1
            GGDaInizioAnno = DateDiff("d", DateSerial(Txt_AnnoInizio.Text + 1, 1, 1), campodbd(myPOSTreader.Item("Servizioal"))) + 1
            If campodbN(myPOSTreader.Item("ImportoTotale")) = 0 Then
                WrRs.Item("ImportoAnnoPrecedente") = 0
                WrRs.Item("ImportoAnnoSuccessivo") = 0
            Else
                WrRs.Item("ImportoAnnoPrecedente") = Math.Round(campodbN(myPOSTreader.Item("ImportoTotale")) / MioImpGG * GGFineAnno, 2)
                WrRs.Item("ImportoAnnoSuccessivo") = Math.Round(campodbN(myPOSTreader.Item("ImportoTotale")) / MioImpGG * GGDaInizioAnno, 2)
            End If


            WrRs.Item("Dal") = Format(campodbd(myPOSTreader.Item("ServizioDal")), "dd/MM/yyyy")
            WrRs.Item("Al") = Format(campodbd(myPOSTreader.Item("Servizioal")), "dd/MM/yyyy")
            WrRs.Item("ImportoTotale") = campodbN(myPOSTreader.Item("ImportoTotale"))
            WrRs.Item("Descrizione") = campodb(myPOSTreader.Item("Descrizione"))
            WrRs.Item("Mastro") = campodbN(myPOSTreader.Item("MastroAvere"))
            WrRs.Item("Conto") = campodbN(myPOSTreader.Item("ContoAvere"))
            WrRs.Item("Sottoconto") = campodbN(myPOSTreader.Item("SottoContoAvere"))
            DecConto.Mastro = campodbN(myPOSTreader.Item("MastroAvere"))
            DecConto.Conto = campodbN(myPOSTreader.Item("ContoAvere"))
            DecConto.Sottoconto = campodbN(myPOSTreader.Item("SottoContoAvere"))
            DecConto.Descrizione = ""
            DecConto.Decodfica(Session("DC_GENERALE"))

            WrRs.Item("DecodificaConto") = DecConto.Descrizione


            Stampa.Tables("RateiRisconti").Rows.Add(WrRs)

        Loop

        myPOSTreader.Close()
        cn.Close()



        Session("stampa") = Stampa

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=STAMPARATEIRISCONTI&XSD=SI','Stampe','width=800,height=600');", True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")

    End Sub
End Class
