﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class GeneraleWeb_TabellaCespiti
    Inherits System.Web.UI.Page




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Immobilizazione')!= null) || (appoggio.match('Txt_FondoAmmortamento')!= null) || (appoggio.match('Txt_Ammortamento')!= null)) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Anticipata')!= null) || (appoggio.match('Txt_Minima')!= null) || (appoggio.match('Txt_Dilegge')!= null))  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Call EseguiJS()


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then            
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Val(Request.Item("Codice")) > 0 Then
            Dim CatCesp As New Cls_CategoriaCespite


            CatCesp.ID = Val(Request.Item("Codice"))
            CatCesp.Leggi(Session("DC_GENERALE"), CatCesp.ID)


            Txt_ID.Text = CatCesp.ID

            Txt_Descrizione.Text = CatCesp.Descrizione
            Txt_Minima.Text = Format(CatCesp.Minima, "#,##0.00")
            Txt_Dilegge.Text = Format(CatCesp.Legge, "#,##0.00")
            Txt_Anticipata.Text = Format(CatCesp.Anticipata, "#,##0.00")


            Dim Pconto As New Cls_Pianodeiconti

            Pconto.Mastro = CatCesp.Mastro
            Pconto.Conto = CatCesp.Conto
            Pconto.Sottoconto = CatCesp.SottoConto
            Pconto.Decodfica(Session("DC_GENERALE"))
            Txt_FondoAmmortamento.Text = Pconto.Mastro & " " & Pconto.Conto & " " & Pconto.Sottoconto & " " & Pconto.Descrizione



            Pconto.Descrizione = ""
            Pconto.Mastro = CatCesp.MastroContropartita
            Pconto.Conto = CatCesp.ContoContropartita
            Pconto.Sottoconto = CatCesp.SottoContoContropartita
            Pconto.Decodfica(Session("DC_GENERALE"))
            Txt_Ammortamento.Text = Pconto.Mastro & " " & Pconto.Conto & " " & Pconto.Sottoconto & " " & Pconto.Descrizione



            Pconto.Descrizione = ""
            Pconto.Mastro = CatCesp.MastroImobilizazione
            Pconto.Conto = CatCesp.ContoImobilizazione
            Pconto.Sottoconto = CatCesp.SottocontoImobilizazione
            Pconto.Decodfica(Session("DC_GENERALE"))
            Txt_Immobilizazione.Text = Pconto.Mastro & " " & Pconto.Conto & " " & Pconto.Sottoconto & " " & Pconto.Descrizione


            
        End If

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Categorie.aspx")
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim CatCesp As New Cls_CategoriaCespite

        CatCesp.ID = Val(Txt_ID.Text)
        CatCesp.Descrizione = Txt_Descrizione.Text
        CatCesp.Minima = Txt_Minima.Text
        CatCesp.Legge = Txt_Dilegge.Text
        CatCesp.Anticipata = Txt_Anticipata.Text

        Dim Vettore(100) As String
        Dim APPOGGIO As String

        APPOGGIO = Txt_FondoAmmortamento.Text
        Vettore = SplitWords(APPOGGIO)

        If Vettore.Length >= 2 Then
            CatCesp.Mastro = Vettore(0)
            CatCesp.Conto = Vettore(1)
            CatCesp.SottoConto = Vettore(2)
        End If

        APPOGGIO = Txt_Ammortamento.Text
        Vettore = SplitWords(APPOGGIO)

        If Vettore.Length >= 2 Then
            CatCesp.MastroContropartita = Vettore(0)
            CatCesp.ContoContropartita = Vettore(1)
            CatCesp.SottoContoContropartita = Vettore(2)
        End If

        APPOGGIO = Txt_Immobilizazione.Text
        Vettore = SplitWords(APPOGGIO)

        If Vettore.Length >= 2 Then
            CatCesp.MastroImobilizazione = Vettore(0)
            CatCesp.ContoImobilizazione = Vettore(1)
            CatCesp.SottocontoImobilizazione = Vettore(2)
        End If


        CatCesp.Scrivi(Session("DC_GENERALE"))
        Response.Redirect("Elenco_Categorie.aspx")


    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim Ks As New Cls_CategoriaCespite

        Ks.Leggi(Session("DC_TABELLE"), Val(Txt_ID.Text))


        Ks.ID = Val(Txt_ID.Text)
        If Ks.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If



        Ks.Delete(Session("DC_TABELLE"))

        Response.Redirect("Elenco_Tributi.aspx")
    End Sub
End Class
