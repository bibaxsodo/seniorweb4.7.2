﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Semplificata" CodeFile="Menu_Semplificata.aspx.vb" %>

<head id="Head1" runat="server">
    <title>Menu Contabilità Semplificata</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript"> 
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style type="text/css">
        .style1 {
            width: 286px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Contabilità Semplificata</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gestione e Stampe<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        &nbsp;</td>
                    <td colspan="2" valign="top">
                        <table style="width: 100%;">
                            <tr>

                                <td style="text-align: center;">
                                    <a href="UltimiMovimenti.aspx?TIPO=SMP">
                                        <img src="../images/Menu_PrimaNota.png" class="Effetto" alt="Movimenti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="RicercaSemplificata.aspx">
                                        <img src="../images/Menu_PrimaNota.png" class="Effetto" alt="Ricerca Semplificata" title="Ricerca Semplificata/Saldo" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="UltimiMovimenti.aspx?TIPO=SMPPRN">
                                        <img src="../images/Menu_PrimaNota.png" class="Effetto" alt="Partita Doppia" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="ElencoPianoConti.aspx">
                                        <img src="../images/Menu_PianoDeiConti.png" class="Effetto" alt="Piano Dei Conti" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOVIMENTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RICERCA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PARTITA DOPPIA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PIANO CONTI</span></td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>

                            <tr>
                                <br />
                                <td class="style1">&nbsp;</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;" class="style1">
                                    <a href="StampaGiornaleContabilita.aspx">
                                        <img style="border-width: 0;" src="../images/Menu_GiornaleContabilita.png" class="Effetto" alt="Periodo a Confronto" /></a></td>
                                <td style="text-align: center;">
                                    <a href="Bilancio.aspx">
                                        <img src="../images/Menu_StampaBilancio.png" alt="Stampa Bilancio" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="StampaPrimaNota.aspx">
                                        <img style="border-width: 0;" src="../images/Menu_StampaRegistriIVA.png" class="Effetto" alt="Prima Nota" /></a></td>
                                <td style="text-align: center;">
                                    <a href="mastrino.aspx">
                                        <img src="../images/Menu_Mastrino.png" alt="Stampa Mastrino" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center;">
                                    <a href="StPianoconti.aspx">
                                        <img src="../images/Menu_PianoDeiConti.png" alt="Piano Dei Conti" class="Effetto" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;" class="style1"><span class="MenuText">STP GIORNALE CONTABILITA'</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STP BILANCIO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STP PRIMA NOTA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STP. MASTRINO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STP PIANO CONTI</span></td>
                            </tr>

                            <tr>

                                <td style="text-align: center;" class="style1">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>

                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;" class="style1">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>

                            <tr>
                                <br />
                                <td class="style1">&nbsp;</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;" class="style1">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;" class="style1">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>
                            <tr>
                    </td>
                    <br />
                    <td class="style1">&nbsp;</td>
                    <td></td>
                </tr>

                <tr>
                    <td style="text-align: center;" class="style1">&nbsp;</td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;" class="style1">&nbsp;</td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                </tr>

            </table>
            </td>
    </tr>
    
    <tr>
        <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
        </td>
        <td></td>
        <td></td>
    </tr>
            </table> 
    
       
        </div>
    </form>
</body>
</html>

