﻿Public Class DecodificaIVA
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CODICE As String = context.Request.QueryString("CODICE")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim IVA As New Cls_IVA

        IVA.Codice = CODICE
        IVA.Leggi(context.Session("DC_TABELLE"), IVA.Codice)



        context.Response.Write(IVA.Aliquota)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class