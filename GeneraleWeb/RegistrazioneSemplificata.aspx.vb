﻿Imports System.Data.OleDb

Partial Class GeneraleWeb_RegistrazioneSemplificata
    Inherits System.Web.UI.Page


    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub AttivaIjs()
        Dim MyJs As String

        If RB_Giroconto.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)        

            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        Else
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)
        End If
        If RB_Uscite.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=C&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If
        If RB_Entrate.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If




        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)


        MyJs = "$('#" & Txt_Importo.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importo.ClientID & "').val(), true, true); } );  $('#" & Txt_Importo.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importo.ClientID & "').val(),2); $('#" & Txt_Importo.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ImportoEntrate", MyJs, True)

    End Sub



    Private Sub CaricaPagina()
        Dim k As New Cls_CausaleContabile

        If Val(Session("NumeroRegistrazione")) > 0 Then
            Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
            Call leggiregistrazione()
        Else


            Call PulisciRegistrazione()
        End If
    End Sub
    Private Sub PulisciRegistrazione()
        Txt_Numero.Text = 0
        Txt_Numero.Enabled = False
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_Descrizione.Text = ""
        RB_Giroconto.Checked = True
        RB_Entrate.Checked = False
        RB_Uscite.Checked = False

        RB_Giroconto.Enabled = True
        RB_Entrate.Enabled = True
        RB_Uscite.Enabled = True

        Txt_ContoPrelievoVersamento.Text = ""
        Txt_CostoRicavo.Text = ""
        Txt_Importo.Text = "0,00"

        Dim k As New Cls_MovimentoContabile


        Lbl_Progressivo.Text = k.MaxProgressivoAnno(Session("DC_GENERALE"), Year(Txt_DataRegistrazione.Text)) + 1 & "/" & Year(Txt_DataRegistrazione.Text)


        Dim MyJs As String

        If RB_Giroconto.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)



            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        Else
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)
        End If
        If RB_Uscite.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=C&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If
        If RB_Entrate.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If


        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)


        MyJs = "$('#" & Txt_Importo.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importo.ClientID & "').val(), true, true); } );  $('#" & Txt_Importo.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importo.ClientID & "').val(),2); $('#" & Txt_Importo.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ImportoEntrate", MyJs, True)


        Lbl_TipoRegistrazione.Text = ""

        'lbl_tasti.Text = "<a href=""#"" <img height=""38px"" src=""../images/progetti.png"" title=""Progetto"" onclick=""DialogBoxBig('/ODV/RicercaProgetto.aspx')"" />"
        lbl_tasti.Text = "<a href=""/ODV/RicercaProgetto.aspx"" target=""_blank""> <img height=""38px"" src=""../images/progetti.png"" title=""Progetto"" ></a>"
    End Sub
    Private Sub leggiregistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim DecConto As New Cls_Pianodeiconti

        Dim Appo As New Cls_ParametriGenerale


        Appo.LeggiParametri(Session("DC_GENERALE"))
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)

        RB_Giroconto.Checked = False
        RB_Entrate.Checked = False
        RB_Uscite.Checked = False
        If x.CausaleContabile = Appo.CausaleEntrata Then
            RB_Entrate.Checked = True
        End If

        If x.CausaleContabile = Appo.CausaleUscita Then
            RB_Uscite.Checked = True
        End If

        If x.CausaleContabile = Appo.CausaleGiroconto Then
            RB_Giroconto.Checked = True
        End If

        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_DataRegistrazione.Text = x.DataRegistrazione        
        Txt_Descrizione.Text = x.Descrizione
        Txt_Numero.Enabled = False

        If Not IsNothing(x.Righe(0)) Then
            Txt_Importo.Text = Format(x.Righe(0).Importo, "#,##0.00")

            DecConto.Mastro = x.Righe(0).MastroPartita
            DecConto.Conto = x.Righe(0).ContoPartita
            DecConto.Sottoconto = x.Righe(0).SottocontoPartita
            DecConto.Decodfica(ConnectionStringGenerale)
            Txt_ContoPrelievoVersamento.Text = x.Righe(0).MastroPartita & " " & x.Righe(0).ContoPartita & " " & x.Righe(0).SottocontoPartita & " " & DecConto.Descrizione
        End If

        If Not IsNothing(x.Righe(1)) Then

            DecConto.Mastro = x.Righe(1).MastroPartita
            DecConto.Conto = x.Righe(1).ContoPartita
            DecConto.Sottoconto = x.Righe(1).SottocontoPartita
            DecConto.Decodfica(ConnectionStringGenerale)
            Txt_CostoRicavo.Text = x.Righe(1).MastroPartita & " " & x.Righe(1).ContoPartita & " " & x.Righe(1).SottocontoPartita & " " & DecConto.Descrizione
        End If


        If RB_Giroconto.Checked = False Then RB_Giroconto.Enabled = False
        If RB_Entrate.Checked = False Then RB_Entrate.Enabled = False
        If RB_Uscite.Checked = False Then RB_Uscite.Enabled = False

        If RB_Entrate.Checked = True Then
            Lbl_Dare.Text = "Versamento :"
            Lbl_Avere.Text = "Ricavo :"
        End If

        If RB_Uscite.Checked = True Then
            Lbl_Dare.Text = "Prelievo :"
            Lbl_Avere.Text = "Costo :"
        End If


        If RB_Giroconto.Checked = True Then
            Lbl_Dare.Text = "Entrate - Dare :"
            Lbl_Avere.Text = "Uscite - Avere :"
        End If
        If RB_Entrate.Checked = False And RB_Uscite.Checked = False And RB_Giroconto.Checked = False Then
            If x.Righe(0).DareAvere = "D" Then
                Lbl_Dare.Text = "Dare :"
            Else
                Lbl_Dare.Text = "Avere :"
            End If
            If x.Righe(1).DareAvere = "D" Then
                Lbl_Avere.Text = "Dare :"
            Else
                Lbl_Avere.Text = "Avere :"
            End If
        End If


        lbl_tasti.Text = ""
        If x.IdProgettoODV > 0 Then
            'lbl_tasti.Text = "<img height=""38px"" src=""../images/progetti.png"" title=""Progetto"" onclick=""DialogBoxBig('/ODV/GestioneProgetto.aspx?IDProgetto=" & x.IdProgettoODV & "')"" />"
            lbl_tasti.Text = "<a href=""/ODV/GestioneProgetto.aspx?IDProgetto=" & x.IdProgettoODV & """ target=""_blank""><img height=""38px"" src=""../images/progetti.png"" title=""Progetto"" ></a>"

            Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE PROGETTO</font>"
        End If
        If x.IDODV > 0 Then
            Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE TESSERAMENTO</font>"
        End If

        Dim K As New Cls_Login


        'If x.CausaleContabile = DecodificaCauzione(Session("ODV_DB")) Then
        ' Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE CAUZIONE</font>"
        'End If

        Lbl_Progressivo.Text = x.ProgressivoNumero & "/" & x.ProgressivoAnno

    End Sub


    Function DecodificaCauzione(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        DecodificaCauzione = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Parametri")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            
            DecodificaCauzione = campodb(myPOSTreader.Item("CausaleDotazioni"))


        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Protected Sub RB_Entrate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Entrate.CheckedChanged
        If RB_Entrate.Checked = True Then
            Dim MyJs As String


            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)


            Dim MyCau As New Cls_CausaleContabile
            Dim P As New Cls_ParametriGenerale

            P.LeggiParametri(Session("DC_GENERALE"))
            MyCau.Leggi(Session("DC_TABELLE"), P.CausaleEntrata)

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = MyCau.Righe(1).Mastro
            PianoConti.Conto = MyCau.Righe(1).Conto
            PianoConti.Sottoconto = MyCau.Righe(1).Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))


            Txt_CostoRicavo.Text = MyCau.Righe(1).Mastro & " " & MyCau.Righe(1).Conto & " " & MyCau.Righe(1).Sottoconto & " " & PianoConti.Descrizione


            PianoConti.Mastro = MyCau.Righe(0).Mastro
            PianoConti.Conto = MyCau.Righe(0).Conto
            PianoConti.Sottoconto = MyCau.Righe(0).Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))


            Txt_ContoPrelievoVersamento.Text = MyCau.Righe(0).Mastro & " " & MyCau.Righe(0).Conto & " " & MyCau.Righe(0).Sottoconto & " " & PianoConti.Descrizione

            Lbl_Dare.Text = "Versamento :"
            Lbl_Avere.Text = "Ricavo :"
        End If
    End Sub

    Protected Sub RB_Uscite_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Uscite.CheckedChanged
        If RB_Uscite.Checked = True Then
            Dim MyJs As String


            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=C&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)



            Dim MyCau As New Cls_CausaleContabile
            Dim P As New Cls_ParametriGenerale

            P.LeggiParametri(Session("DC_GENERALE"))
            MyCau.Leggi(Session("DC_TABELLE"), P.CausaleUscita)

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = MyCau.Righe(1).Mastro
            PianoConti.Conto = MyCau.Righe(1).Conto
            PianoConti.Sottoconto = MyCau.Righe(1).Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))


            Txt_CostoRicavo.Text = MyCau.Righe(1).Mastro & " " & MyCau.Righe(1).Conto & " " & MyCau.Righe(1).Sottoconto & " " & PianoConti.Descrizione


            PianoConti.Mastro = MyCau.Righe(0).Mastro
            PianoConti.Conto = MyCau.Righe(0).Conto
            PianoConti.Sottoconto = MyCau.Righe(0).Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))


            Txt_ContoPrelievoVersamento.Text = MyCau.Righe(0).Mastro & " " & MyCau.Righe(0).Conto & " " & MyCau.Righe(0).Sottoconto & " " & PianoConti.Descrizione

            Lbl_Dare.Text = "Prelievo :"
            Lbl_Avere.Text = "Costo :"
        End If
    End Sub

    Protected Sub RB_Giroconto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Giroconto.CheckedChanged
        If RB_Giroconto.Checked = True Then
            Dim MyJs As String


            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)



            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)

            Txt_ContoPrelievoVersamento.Text = ""
            Txt_CostoRicavo.Text = ""


            Lbl_Dare.Text = "Entrate - Dare :"
            Lbl_Avere.Text = "Uscite - Avere :"
        End If
    End Sub

    Protected Sub Btn_Pulisci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Pulisci.Click
        Call PulisciRegistrazione()
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        Dim xLog As New Cls_Login

        xLog.Utente = Session("UTENTE")
        xLog.LeggiSP(Application("SENIOR"))
        If xLog.ABILITAZIONI.IndexOf("<NONMODIFICASEMPLIFICATA>") > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non sei abilitato modifica/inserimento o cancellazione');", True)
            Exit Sub
        End If

        Dim xs As New Cls_Legami
        If xs.TotaleLegame(Session("DC_GENERALE"), Txt_Numero.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)            
            Exit Sub
        End If
        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Exit Sub
            End If
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
        End If

        If Session("TIPOAPP") <> "RSA" Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand() '
            cmd.CommandText = ("Select * from MovimentiContabiliTESTA where NumeroRegistrazione = ?")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_Numero.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If campodbn(myPOSTreader.Item("IDODV")) > 0 Or campodbn(myPOSTreader.Item("IdProgettoODV")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione creata automaticamente</center>');", True)
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
            cn.Close()
        End If



        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If

        Dim XScd As New Cls_Scadenziario
        Dim TbEScd As New System.Data.DataTable("TbEScd")

        XScd.NumeroRegistrazioneContabile = Txt_Numero.Text
        XScd.loaddati(Session("DC_GENERALE"), TbEScd)

        If TbEScd.Rows.Count > 1 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)            
            Exit Sub
        End If

        If TbEScd.Rows.Count = 1 Then
            If Not IsDBNull(TbEScd.Rows(0).Item(1)) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)                
                Exit Sub
            End If
        End If

        Dim XReg As New Cls_MovimentoContabile

        XReg.NumeroRegistrazione = Txt_Numero.Text
        XReg.EliminaRegistrazione(Session("DC_GENERALE"))

        Call PulisciRegistrazione()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Trim(Session("UTENTE")) = "" Then
            Response.Write("<script>")
            Response.Write("window.open('..\Login_generale.aspx','_blank')")
            Response.Write("</script>")
            Exit Sub
        End If


        Dim xLog As New Cls_Login

        xLog.Utente = Session("UTENTE")        
        xLog.LeggiSP(Application("SENIOR"))
        If xLog.ABILITAZIONI.IndexOf("<NONMODIFICASEMPLIFICATA>") > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non sei abilitato modifica/inserimento o cancellazione');", True)
            Exit Sub
        End If

        If Session("TIPOAPP") <> "RSA" Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand() '
            cmd.CommandText = ("Select * from MovimentiContabiliTESTA where NumeroRegistrazione = ?")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_Numero.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If campodbn(myPOSTreader.Item("IDODV")) > 0 Or campodbn(myPOSTreader.Item("IdProgettoODV")) > 0 Or campodbn(myPOSTreader.Item("TipoODV")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione creata automaticamente</center>');", True)
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
            cn.Close()
        End If

   

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare data di registrazione</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        If Txt_Importo.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Importo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        If Txt_ContoPrelievoVersamento.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto prelievo versamento</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        If Txt_CostoRicavo.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare costo ricavo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If



        If CDbl(Txt_Importo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Importo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If





        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Già creato il giornale bollato</center>');", True)
                Call AttivaIjs()
                Exit Sub
            End If
        End If


        Dim xMastro As Long
        Dim xConto As Long
        Dim xSottoconto As Long
        Dim xVettore(100) As String
        xVettore = SplitWords(Txt_ContoPrelievoVersamento.Text)


        If xVettore.Length < 2 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto prelievo versamento</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        xMastro = Val(xVettore(0))
        xConto = Val(xVettore(1))
        xSottoconto = Val(xVettore(2))

        If xMastro = 0 Or xConto = 0 Or xSottoconto = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto prelievo versamento</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        Dim k As New Cls_Pianodeiconti

        k.Mastro = xMastro
        k.Conto = xConto
        k.Sottoconto = xSottoconto
        k.Descrizione = ""
        k.Decodfica(Session("DC_GENERALE"))
        If k.Descrizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto prelievo versamento</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        xVettore = SplitWords(Txt_CostoRicavo.Text)

        If xVettore.Length < 2 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare costo ricavo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If


        xMastro = Val(xVettore(0))
        xConto = Val(xVettore(1))
        xSottoconto = Val(xVettore(2))

        If xMastro = 0 Or xConto = 0 Or xSottoconto = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare costo ricavo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If


        k.Mastro = xMastro
        k.Conto = xConto
        k.Sottoconto = xSottoconto
        k.Descrizione = ""
        k.Decodfica(Session("DC_GENERALE"))
        If k.Descrizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare costo ricavo</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If


        Dim x As New Cls_MovimentoContabile
        Dim DecConto As New Cls_Pianodeiconti

        Dim Appo As New Cls_ParametriGenerale


        Appo.LeggiParametri(Session("DC_GENERALE"))
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)

        
        If RB_Entrate.Checked = True Then
            x.CausaleContabile = Appo.CausaleEntrata
        End If

        If RB_Uscite.Checked = True Then
            x.CausaleContabile = Appo.CausaleUscita
        End If

        If RB_Giroconto.Checked = True Then
            x.CausaleContabile = Appo.CausaleGiroconto
        End If

        x.NumeroRegistrazione = Txt_Numero.Text
        x.DataRegistrazione = Txt_DataRegistrazione.Text
        x.Descrizione = Txt_Descrizione.Text


        Dim Causale As New Cls_CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), x.CausaleContabile)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Dim YMastro As Long
        Dim YConto As Long
        Dim YSottoconto As Long

        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ContoPrelievoVersamento.Text)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        Vettore = SplitWords(Txt_CostoRicavo.Text)

        YMastro = Val(Vettore(0))
        YConto = Val(Vettore(1))
        YSottoconto = Val(Vettore(2))

        If IsNothing(x.Righe(0)) Then
            x.Righe(0) = New Cls_MovimentiContabiliRiga
        End If
        x.Righe(0).Importo = Txt_Importo.Text
        x.Righe(0).MastroPartita = Mastro
        x.Righe(0).ContoPartita = Conto
        x.Righe(0).SottocontoPartita = Sottoconto

        x.Righe(0).MastroContropartita = YMastro
        x.Righe(0).ContoContropartita = YConto
        x.Righe(0).SottocontoContropartita = YSottoconto

        x.Righe(0).DareAvere = Causale.Righe(0).DareAvere
        x.Righe(0).Segno = "+"
        x.Righe(0).RigaDaCausale = 1



        


        If IsNothing(x.Righe(1)) Then
            x.Righe(1) = New Cls_MovimentiContabiliRiga
        End If
        x.Righe(1).Importo = Txt_Importo.Text
        x.Righe(1).MastroPartita = YMastro
        x.Righe(1).ContoPartita = YConto
        x.Righe(1).SottocontoPartita = YSottoconto

        x.Righe(1).MastroContropartita = YMastro
        x.Righe(1).ContoContropartita = YConto
        x.Righe(1).SottocontoContropartita = YSottoconto

        x.Righe(1).DareAvere = Causale.Righe(1).DareAvere
        x.Righe(1).Segno = "+"
        x.Righe(1).RigaDaCausale = 2


        If x.Scrivi(Session("DC_GENERALE"), Val(Txt_Numero.Text)) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione non eseguita</center>');", True)
            Call AttivaIjs()
            Exit Sub
        End If

        Txt_Numero.Text = x.NumeroRegistrazione
        Session("NumeroRegistrazione") = x.NumeroRegistrazione
        Txt_Numero.Enabled = False
        Dim MyJs As String

        x.Leggi(ConnectionStringGenerale, x.NumeroRegistrazione)
        MyJs = "alert('Salvato Registrazione " & x.NumeroRegistrazione & " Progressivo " & x.ProgressivoNumero & "/" & x.ProgressivoAnno & "');  "

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModReg", MyJs, True)
        Call AttivaIjs()

        Txt_Numero.Text = 0
        Txt_Numero.Enabled = False
        'Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_Descrizione.Text = ""
        RB_Giroconto.Checked = True
        RB_Entrate.Checked = False
        RB_Uscite.Checked = False

        RB_Giroconto.Enabled = True
        RB_Entrate.Enabled = True
        RB_Uscite.Enabled = True

        Txt_ContoPrelievoVersamento.Text = ""
        Txt_CostoRicavo.Text = ""
        Txt_Importo.Text = "0,00"




        If RB_Giroconto.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)



            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        Else
            MyJs = "$(" & Chr(34) & "#" & Txt_ContoPrelievoVersamento.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoPrelievoVersamento", MyJs, True)
        End If
        If RB_Uscite.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=C&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If
        If RB_Entrate.Checked = True Then
            MyJs = "$(" & Chr(34) & "#" & Txt_CostoRicavo.ClientID & Chr(34) & ").autocomplete('AutocompleteContoTipo.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)
        End If


        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)


        MyJs = "$('#" & Txt_Importo.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importo.ClientID & "').val(), true, true); } );  $('#" & Txt_Importo.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importo.ClientID & "').val(),2); $('#" & Txt_Importo.ClientID & "').val(ap); }); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ImportoEntrate", MyJs, True)

    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("UltimiMovimenti.aspx?TIPO=SMP")
        Else
            Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=SMP")
        End If

    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("RicercaSemplificata.aspx")
    End Sub

    Protected Sub Page_InitComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Timer1.Interval = 900 * 1000

        Call AttivaIjs()

        Lbl_Utente.Text = Session("UTENTE")

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Lbl_Dare.Text = "Entrate - Dare :"
        Lbl_Avere.Text = "Uscite - Avere :"


        Call CaricaPagina()

    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        If Session("TIPOAPP") <> "RSA" Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand() '
            cmd.CommandText = ("Select * from MovimentiContabiliTESTA where NumeroRegistrazione = ?")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_Numero.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If campodbn(myPOSTreader.Item("IDODV")) > 0 Or campodbn(myPOSTreader.Item("IdProgettoODV")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione creata automaticamente</center>');", True)
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
            cn.Close()
        End If

        Txt_Numero.Text = "0"
    End Sub

    Protected Sub Btn_ADD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_ADD.Click
        Session("NumeroRegistrazione") = 0

        Response.Redirect("RegistrazioneSemplificata.aspx")
    End Sub
End Class
