﻿
Partial Class GeneraleWeb_Detraibilita
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim xTp As New ClsDetraibilita

        If Request.Item("Codice") = "" Then
            Txt_Codice.Text = xTp.MaxDetraibilita(Session("DC_TABELLE"))
            Exit Sub
        End If

        Txt_Codice.Enabled = False
        xTp.Codice = Request.Item("Codice")
        xTp.Leggi(Session("DC_TABELLE"))
        Txt_Codice.Text = Request.Item("Codice")
        Txt_Descrizione.Text = xTp.Descrizione
        Txt_Aliquota.Text = xTp.Aliquota
        Txt_AliquotaCosti.Text = xTp.IVAGiroACostoAliquota

        If xTp.Automatico = "S" Then
            RB_Si.Checked = True
            RB_No.Checked = False
        Else
            RB_No.Checked = True
            RB_Si.Checked = False
        End If


        If xTp.GiroACosto = "S" Then
            RB_SiIva.Checked = True
            RB_NoIva.Checked = False
        Else
            RB_NoIva.Checked = True
            RB_SiIva.Checked = False
        End If

        Dim DCconto As New Cls_Pianodeiconti

        DCconto.Mastro = xTp.IVAGiroACostoMastro
        DCconto.Conto = xTp.IVAGiroACostoConto
        DCconto.Sottoconto = xTp.IVAGiroACostoSottoconto
        DCconto.Decodfica(Session("DC_GENERALE"))
        Txt_Sottoconto.Text = DCconto.Mastro & " " & DCconto.Conto & " " & DCconto.Sottoconto & " " & DCconto.Descrizione

        If xTp.Prorata = 1 Then
            Chk_Prorata.Checked = True
        Else
            Chk_Prorata.Checked = False
        End If
    End Sub



    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim MyJs As String
        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)

            call EseguiJS()
            REM Lbl_Errori.Text = "Specificare codice"
            Exit Sub
        End If
        If Txt_Descrizione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare la descrizione');", True)

            call EseguiJS()
            REM Lbl_Errori.Text = "Specificare descrizione"
            Exit Sub
        End If
        If Not IsNumeric(Txt_Aliquota.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non numerico in Aliquota');", True)

            call EseguiJS()
            REM Lbl_Errori.Text = "Non numerico in Aliquota "
            Exit Sub
        End If
        If Not IsNumeric(Txt_AliquotaCosti.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non numerico in Aliquota Costi');", True)

            Call EseguiJS()
            REM Lbl_Errori.Text = "Non numerico in Aliquota Costi"
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim xVerifica As New ClsDetraibilita

            xVerifica.Codice = Txt_Codice.Text
            xVerifica.Leggi(Session("DC_TABELLE"))

            If xVerifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                Call EseguiJS()                
                Exit Sub
            End If

        End If

        Dim i As Integer
        'If DD_TipoDetraibilita.SelectedValue = "" Then
        '    For i = 0 To DD_TipoDetraibilita.Items.Count - 1
        '        If DD_TipoDetraibilita.Items(i).Text.IndexOf(Txt_Descrizione.Text) > 0 Then
        '            Lbl_Errori.Text = "Descrizione già inserita"
        '            Exit Sub
        '        End If
        '    Next
        'End If


        Dim xTp As New ClsDetraibilita

        xTp.Codice = Txt_Codice.Text
        xTp.Leggi(Session("DC_TABELLE"))
        xTp.Codice = Txt_Codice.Text
        xTp.Descrizione = Txt_Descrizione.Text

        If Txt_Aliquota.Text = "" Then
            Txt_Aliquota.Text = "0"
        End If

        xTp.Aliquota = Txt_Aliquota.Text

        If Txt_AliquotaCosti.Text = "" Then
            Txt_AliquotaCosti.Text = "0"
        End If
        xTp.IVAGiroACostoAliquota = Txt_AliquotaCosti.Text


        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_Sottoconto.Text)

        If Vettore.Length >= 3 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        xTp.IVAGiroACostoMastro = Mastro
        xTp.IVAGiroACostoConto = Conto
        xTp.IVAGiroACostoSottoconto = Sottoconto

        If RB_Si.Checked = True Then
            xTp.Automatico = "S"            
        Else
            xTp.Automatico = "N"            
        End If


        If RB_SiIva.Checked = True Then
            xTp.GiroACosto = "S"
        Else
            xTp.GiroACosto = "N"
        End If
        
        If Chk_Prorata.Checked = True Then
            xTp.Prorata = 1
        Else
            xTp.Prorata = 0
        End If
        xTp.Scrivi(Session("Dc_Tabelle"))

        Response.Redirect("ElencoDetraibilita.aspx")
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim xTp As New ClsDetraibilita

        xTp.Codice = Txt_Codice.Text




        If xTp.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If



        xTp.Delete(Session("DC_TABELLE"))

        Response.Redirect("ElencoDetraibilita.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoDetraibilita.aspx")
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Aliquota')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsDetraibilita

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsDetraibilita

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"))

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
