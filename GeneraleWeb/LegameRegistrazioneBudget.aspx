﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_LegameRegistrazioneBudget" CodeFile="LegameRegistrazioneBudget.aspx.vb" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Legame Prenotazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <style>
        .Registrazione {
            width: 98%;
            height: 24px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=96)";
            filter: alpha(opacity=96);
            -moz-opacity: 0.96;
            -khtml-opacity: 0.96;
            opacity: 0.96;
            border: 1px #888888 solid;
            background-color: #86d5f9;
            padding: 2px;
            -moz-box-shadow: 4px 3px 2px 1px #110e0e;
            -webkit-box-shadow: 4px 3px 2px 1px #110e0e;
            box-shadow: 4px 3px 2px 1px #110e0e;
        }
    </style>

    <script type="text/javascript">          
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500px"></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="text-align: left;">
                    <div class="registrazione">Righe Registrazione</div>
                    <br />
                    <asp:GridView ID="GrdRighe" runat="server" Height="12px"
                        Width="100%" ShowFooter="false" BackColor="White" BorderColor="#CCCCCC"
                        BorderStyle="None" BorderWidth="1px">
                        <RowStyle ForeColor="#000066" />
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                        ImageUrl="~/images/select.png" class="EffettoBottoniTondi"
                                        CommandArgument="<%#   Container.DataItemIndex  %>" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ID">
                                <EditItemTemplate>
                                    <asp:Label ID="LblID" runat="server" Text='<%# Eval("ID") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblID" runat="server" Text='<%# Eval("ID") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Conto">
                                <EditItemTemplate>
                                    <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dare">
                                <EditItemTemplate>
                                    <asp:Label ID="LblDare" runat="server" Style="text-align: right;" Text='<%# Eval("Dare") %>' Width="100%"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblDare" runat="server" Style="text-align: right;" Text='<%# Eval("Dare") %>' Width="100%"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Avere">
                                <EditItemTemplate>
                                    <asp:Label ID="LblAvere" runat="server" Style="text-align: right;" Text='<%# Eval("Avere") %>' Width="100%"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblAvere" runat="server" Style="text-align: right;" Text='<%# Eval("Avere") %>' Width="100%"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    <br />
                    <div class="registrazione">Righe Legate</div>
                    <br />
                    <asp:GridView ID="Grid" runat="server" Height="12px"
                        Width="100%" ShowFooter="false" BackColor="White" BorderColor="#CCCCCC"
                        BorderStyle="None" BorderWidth="1px">
                        <RowStyle ForeColor="#000066" />
                        <Columns>
                            <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                            <asp:TemplateField HeaderText="Anno">
                                <EditItemTemplate>
                                    <asp:Label ID="LblAnno" runat="server" Text='<%# Eval("Anno") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblAnno" runat="server" Text='<%# Eval("Anno") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Livello">
                                <EditItemTemplate>
                                    <asp:Label ID="LblLivello" runat="server" Text='<%# Eval("Livello") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblLivello" runat="server" Text='<%# Eval("Livello") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Colonna">
                                <EditItemTemplate>
                                    <asp:Label ID="LblColonna" runat="server" Text='<%# Eval("Colonna") %>' Style="text-align: right;" Width="100%"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblColonna" runat="server" Text='<%# Eval("Colonna") %>' Style="text-align: right;" Width="100%"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Decodifica">
                                <EditItemTemplate>
                                    <asp:Label ID="LblDecodifica" runat="server" Text='<%# Eval("Decodifica") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblDecodifica" runat="server" Text='<%# Eval("Decodifica") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Importo">
                                <EditItemTemplate>
                                    <asp:Label ID="LblImporto" runat="server" Style="text-align: right;" Text='<%# Eval("Importo") %>' Width="100%"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblImporto" runat="server" Style="text-align: right;" Text='<%# Eval("Importo") %>' Width="100%"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Riga">
                                <EditItemTemplate>
                                    <asp:Label ID="LblRiga" runat="server" Text='<%# Eval("Riga") %>' Style="text-align: right;" Width="100%"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblRiga" runat="server" Text='<%# Eval("Riga") %>' Style="text-align: right;" Width="100%"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Conto">
                                <EditItemTemplate>
                                    <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Competenza">
                                <EditItemTemplate>
                                    <asp:Label ID="LblCompetenza" runat="server" Text='<%# Eval("Competenza")%>' Style="text-align: right;" Width="136px"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblCompetenza" runat="server" Text='<%# Eval("Competenza") %>' Style="text-align: right;" Width="136px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 70%">
                                <br />
                                <br />


                                <label style="display: block; float: left; width: 200px;">Anno</label>
                                <asp:TextBox ID="Txt_Anno" autocomplete="off" onkeypress="return soloNumeri(event);" AutoPostBack="true" runat="server" Width="52px"></asp:TextBox>
                                &nbsp;Livello
        <asp:TextBox ID="Txt_Livello" runat="server" Width="387px"></asp:TextBox>

                                <br />
                                <br />
                                <label style="display: block; float: left; width: 200px;">Colonna :</label>
                                <asp:DropDownList ID="DD_Colonna" runat="server" Width="261px">
                                </asp:DropDownList>&nbsp;Competenza - Anno :
                                <asp:TextBox ID="Txt_AnnoCompetenza" autocomplete="off" onkeypress="return soloNumeri(event);" AutoPostBack="true" runat="server" Width="52px"></asp:TextBox>
                                &nbsp;Mese :                    
                                <asp:DropDownList ID="DD_Mese" runat="server">
                                    <asp:ListItem Value="0">Non Indicato</asp:ListItem>
                                    <asp:ListItem Text="Gennaio" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Febbraio" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Marzo" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Aprile" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Maggio" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="Giugno" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="Luglio" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="Agosto" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="Settembre" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="Ottobre" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="Novembre" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="Dicembre" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<br />
                                <br />
                                <label style="display: block; float: left; width: 200px;">Importo :</label>
                                <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server"></asp:TextBox>
                                <br />
                                <br />
                            </td>
                            <td style="width: 30%; text-align: right; vertical-align: top;">
                                <asp:ImageButton ID="BTN_Inserisci" runat="server" Height="48px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="48px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
