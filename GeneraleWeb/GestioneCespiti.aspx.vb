﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_GestioneCespiti
    Inherits System.Web.UI.Page


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_DataDimissione.Enabled = False
        Txt_DescrizioneDimissione.Enabled = False
        DD_Percentuale.Items.Clear()
        DD_Percentuale.Items.Add("Di legge")
        DD_Percentuale.Items(DD_Percentuale.Items.Count - 1).Value = "L"
        DD_Percentuale.Items.Add("Minima")
        DD_Percentuale.Items(DD_Percentuale.Items.Count - 1).Value = "M"
        DD_Percentuale.Items.Add("Anticipata")
        DD_Percentuale.Items(DD_Percentuale.Items.Count - 1).Value = "A"


        Dim MP As New Cls_CategoriaCespite


        MP.UpDateDropBox(Session("DC_GENERALE"), DD_Categoria)
        Session("CodiceCespite") = 0
        If Val(Request.Item("Codcie")) > 0 Then
            Session("CodiceCespite") = Val(Request.Item("Codcie"))
            Dim Cespite As New Cls_Cespiti

            Cespite.Codice = Val(Request.Item("Codcie"))
            Cespite.Leggi(Session("DC_GENERALE"), Cespite.Codice)

            Txt_ID.Text = Cespite.Codice

            Txt_ID.Enabled = False

            Txt_Descrizione.Text = Cespite.Descrizione
            Txt_Ubicazione.Text = Cespite.Ubicazione
            DD_Categoria.SelectedValue = Cespite.Categoria
            Txt_NumeroAcq.Text = Cespite.NumeroAcq
            Txt_AnnoAcq.Text = Cespite.AnnoAcq
            Txt_DescrizioneAcq.Text = Cespite.DescrizioneAcq


            Txt_NumeroVnd.Text = Cespite.NumeroVnd
            Txt_AnnoVnd.Text = Cespite.AnnoVnd
            Txt_DescrizioneVnd.Text = Cespite.DescrizioneVnd

            Txt_ImportoAcq.Text = Format(Cespite.ImportoAcq, "#,##0.00")
            Txt_ImportoVnd.Text = Format(Cespite.ImportoVnd, "#,##0.00")

            If Cespite.Dismesso = 1 Then
                ChkDimesso.Checked = True
                Txt_DataDimissione.Text = Format(Cespite.DataDismissione, "dd/MM/yyyy")
                Txt_DescrizioneDimissione.Text = Cespite.DescrizioneDismissione

            Else
                ChkDimesso.Checked = False
            End If

            DD_Percentuale.SelectedValue = Cespite.TipoPercentuale
            Txt_ImportoBase.Text = Format(Cespite.ImportoFondo, "#,##0.00")


            Call Txt_NumeroAcq_TextChanged(sender, e)
            Call Txt_NumeroVnd_TextChanged(sender, e)

        Else
            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select max(Codice) from Cespiti ")
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_ID.Text = campodbN(myPOSTreader.Item(0)) + 1
            Else
                Txt_ID.Text = 1
            End If
            cn.Close()

            If Val(Request.Item("NUMEROREGISTRAZIONE")) > 0 Then
                Txt_NumeroAcq.Text = Val(Request.Item("NUMEROREGISTRAZIONE"))
                Call Txt_NumeroAcq_TextChanged(sender, e)
            End If
        End If

        Call ChkDimesso_CheckedChanged(sender, e)
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Import')!= null) || appoggio.match('Base')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub

    Protected Sub ChkDimesso_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkDimesso.CheckedChanged
        If ChkDimesso.Checked = False Then
            Txt_DataDimissione.Enabled = False
            Txt_DescrizioneDimissione.Enabled = False
            Txt_DataDimissione.BackColor = System.Drawing.Color.Gray
            Txt_DescrizioneDimissione.BackColor = System.Drawing.Color.Gray

        Else
            Txt_DataDimissione.Enabled = True
            Txt_DescrizioneDimissione.Enabled = True
            Txt_DataDimissione.BackColor = System.Drawing.Color.White
            Txt_DescrizioneDimissione.BackColor = System.Drawing.Color.White
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Txt_Descrizione.Text.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Non inserito importo di acquisto non posso procedere a creare la registrazione');", True)
            Exit Sub
        End If

        'If Val(Txt_ID.Text) = 0 Then
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi indicare un codice');", True)
        '    Exit Sub
        'End If

        If Val(Request.Item("Codcie")) = 0 Then
            Dim VerCesp As New Cls_Cespiti

            VerCesp.Descrizione = ""
            VerCesp.Codice = Val(Txt_ID.Text)
            VerCesp.Leggi(Session("DC_GENERALE"), VerCesp.Codice)


            If VerCesp.Descrizione.Trim <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Codice cespite già usato');", True)
                Exit Sub
            End If
        End If



        If Val(DD_Categoria.SelectedValue) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi selezionare una categoria');", True)
            Exit Sub
        End If


        Dim Cespite As New Cls_Cespiti

        Cespite.Codice = Val(Txt_ID.Text)
        Cespite.Leggi(Session("DC_GENERALE"), Cespite.Codice)

        Cespite.Codice = Val(Txt_ID.Text)
        Cespite.Descrizione = Txt_Descrizione.Text
        Cespite.Ubicazione = Txt_Ubicazione.Text
        Cespite.Categoria = Val(DD_Categoria.SelectedValue)
        Cespite.NumeroAcq = Val(Txt_NumeroAcq.Text)
        Cespite.AnnoAcq = Val(Txt_AnnoAcq.Text)
        Cespite.DescrizioneAcq = Txt_DescrizioneAcq.Text


        Cespite.NumeroVnd = Val(Txt_NumeroVnd.Text)
        Cespite.AnnoVnd = Val(Txt_AnnoVnd.Text)
        Cespite.DescrizioneVnd = Txt_DescrizioneVnd.Text

        If Txt_ImportoAcq.Text.Trim = "" Then
            Txt_ImportoAcq.Text = "0"
        End If
        If Txt_ImportoVnd.Text.Trim = "" Then
            Txt_ImportoVnd.Text = "0"
        End If
        Cespite.ImportoAcq = Txt_ImportoAcq.Text
        Cespite.ImportoVnd = Txt_ImportoVnd.Text

        If ChkDimesso.Checked = True Then
            Cespite.Dismesso = 0
            Cespite.DataDismissione = Txt_DataDimissione.Text
            Cespite.DescrizioneDismissione = Txt_DescrizioneDimissione.Text

        Else
            Cespite.Dismesso = 0
            Txt_DataDimissione.Text = Nothing
            Txt_DescrizioneDimissione.Text = ""
        End If

        Cespite.TipoPercentuale = DD_Percentuale.SelectedValue

        Try
            Cespite.ImportoFondo = CDbl(Txt_ImportoBase.Text)
        Catch ex As Exception
            Cespite.ImportoFondo = 0
        End Try


        Cespite.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("Elenco_Cespiti.aspx")
    End Sub

    Protected Sub Txt_NumeroAcq_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_NumeroAcq.TextChanged
        LblDocumentoAcq.Text = ""
        If Val(Txt_NumeroAcq.Text) > 0 Then
            Dim Registrazione As New Cls_MovimentoContabile


            Registrazione.NumeroRegistrazione = Val(Txt_NumeroAcq.Text)
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)


            If CausaleContabile.TipoDocumento = "FA" Then
                Dim PianoConti As New Cls_Pianodeiconti

                PianoConti.Mastro = Registrazione.Righe(0).MastroPartita
                PianoConti.Conto = Registrazione.Righe(0).ContoPartita
                PianoConti.Sottoconto = Registrazione.Righe(0).SottocontoPartita
                PianoConti.Decodfica(Session("DC_GENERALE"))


                LblDocumentoAcq.Text = "Fornitore : " & PianoConti.Descrizione & " Numero Documento " & Registrazione.NumeroDocumento & " del " & Format(Registrazione.DataDocumento, "dd/MM/yyyy")

                Txt_AnnoAcq.Text = Year(Registrazione.DataRegistrazione)

                Txt_ImportoAcq.Text = Format(Registrazione.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")
            End If


        End If
    End Sub

    Protected Sub Txt_NumeroVnd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_NumeroVnd.TextChanged
        LblDocumentoVnd.Text = ""
        If Val(Txt_NumeroVnd.Text) > 0 Then
            Dim Registrazione As New Cls_MovimentoContabile


            Registrazione.NumeroRegistrazione = Val(Txt_NumeroVnd.Text)
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)


            If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Then
                Dim PianoConti As New Cls_Pianodeiconti

                PianoConti.Mastro = Registrazione.Righe(0).MastroPartita
                PianoConti.Conto = Registrazione.Righe(0).ContoPartita
                PianoConti.Sottoconto = Registrazione.Righe(0).SottocontoPartita
                PianoConti.Decodfica(Session("DC_GENERALE"))


                LblDocumentoVnd.Text = "Cliente : " & PianoConti.Descrizione & " Numero Documento " & Registrazione.NumeroDocumento & " del " & Format(Registrazione.DataDocumento, "dd/MM/yyyy")

                Txt_AnnoVnd.Text = Year(Registrazione.DataRegistrazione)

                Txt_ImportoVnd.Text = Format(Registrazione.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")
            End If


        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Cespiti.aspx")
    End Sub

    Protected Sub Btn_CreaDismissione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CreaDismissione.Click


        If Val(Txt_ID.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi prima inserire il cespite e poi dismetterlo');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataDimissione.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Sono inseriti i dati di dimissione non posso procedere a creare la registrazione');", True)
            Exit Sub
        End If

        If Val(Txt_ImportoAcq.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Non inserito importo di acquisto non posso procedere a creare la registrazione');", True)
            Exit Sub
        End If




        If Val(DD_Categoria.SelectedValue) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi selezionare una categoria');", True)
            Exit Sub
        End If


        Dim ParamCespiti As New Cls_ParametriCespiti

        ParamCespiti.LeggiParametri(Session("DC_GENERALE"))

        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * From MovimentiContabilitesta Where CausaleContabile = '" & ParamCespiti.CausaleContabileDismissione & "' And Cespite = " & Val(Txt_ID.Text)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Già creata la registrazione di dismissione per questo cespite, numero registrazione " & campodb(myPOSTreader.Item("NumeroRegistrazione")) & "');", True)
            Exit Sub
        End If


        Dim NumeroReg As Integer = 0


        NumeroReg = CreaRegistrazioneDismissione(ParamCespiti.CausaleContabileDismissione, Val(Txt_ID.Text))

        Dim MovCespite As New Cls_MovimentiCespite

        MovCespite.CodiceCespite = Txt_ID.Text
        MovCespite.DataRegistrazione = Txt_DataDimissione.Text
        MovCespite.TipoMov = "DI"
        MovCespite.Importo = 0
        MovCespite.Scrivi(Session("DC_GENERALE"))

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "alert('Creato Registrazione " & NumeroReg & "');", True)
    End Sub






    Private Function CreaRegistrazioneDismissione(ByVal tipo As String, ByVal cespite As Long) As Long

        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Ammortamento As Double
        Dim ValoreAcq As Double
        Dim Categoria As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Ammortamento = 0
        ValoreAcq = 0

        MySql = "SELECT * FROM Cespiti WHERE Codice = " & cespite
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdRD As New OleDbCommand()
            cmdRD.CommandText = "SELECT SUM(Importo) FROM MovimentiCespite WHERE TipoMov = 'AM' And CodiceCespite = " & cespite
            cmdRD.Connection = cn
            Dim MovimentiCespite As OleDbDataReader = cmdRD.ExecuteReader()
            Do While MovimentiCespite.Read
                Ammortamento = Ammortamento + campodbN(MovimentiCespite.Item(0))
            Loop
            MovimentiCespite.Close()
            Ammortamento = Ammortamento + campodbN(myPOSTreader.Item("ImportoFondo"))
            ValoreAcq = campodbN(myPOSTreader.Item("ImportoAcq"))
            Categoria = campodb(myPOSTreader.Item("Categoria"))
        End If
        myPOSTreader.Close()

        Dim ParamCE As New Cls_ParametriCespiti

        ParamCE.LeggiParametri(Session("DC_GENERALE"))



        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.NumeroRegistrazione = 0
        Registrazione.DataRegistrazione = Txt_DataDimissione.Text
        Registrazione.CausaleContabile = ParamCE.CausaleContabileDismissione


        Dim CSCategoria As New Cls_CategoriaCespite

        CSCategoria.ID = Categoria
        CSCategoria.Leggi(Session("DC_GENERALE"), CSCategoria.ID)
        
        Dim CauCont As New Cls_CausaleContabile
        CauCont.Codice = ParamCE.CausaleContabileDismissione
        CauCont.Leggi(Session("DC_TABELLE"), CauCont.Codice)

        If IsNothing(Registrazione.Righe(0)) Then
            Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        End If

        Registrazione.Righe(0).DareAvere = CauCont.Righe(0).DareAvere
        Registrazione.Righe(0).Numero = 0
        Registrazione.Righe(0).MastroPartita = CSCategoria.Mastro
        Registrazione.Righe(0).ContoPartita = CSCategoria.Conto
        Registrazione.Righe(0).SottocontoPartita = CSCategoria.SottoConto

        Registrazione.Righe(0).MastroContropartita = 0
        Registrazione.Righe(0).ContoContropartita = 0
        Registrazione.Righe(0).SottocontoContropartita = 0

        Registrazione.Righe(0).Descrizione = ""

        Registrazione.Righe(0).Importo = Math.Round(Ammortamento, 2)
        Registrazione.Righe(0).Segno = "+"

        Registrazione.Righe(0).RigaDaCausale = 1
        Registrazione.Righe(0).RigaRegistrazione = 1
        Registrazione.Righe(0).Utente = Session("UTENTE")


        If IsNothing(Registrazione.Righe(1)) Then
            Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        End If

        Registrazione.Righe(1).DareAvere = CauCont.Righe(1).DareAvere
        Registrazione.Righe(1).Numero = 0
        Registrazione.Righe(1).MastroPartita = CSCategoria.MastroImobilizazione
        Registrazione.Righe(1).ContoPartita = CSCategoria.ContoImobilizazione
        Registrazione.Righe(1).SottocontoPartita = CSCategoria.SottocontoImobilizazione
        Registrazione.Righe(1).MastroContropartita = 0
        Registrazione.Righe(1).ContoContropartita = 0
        Registrazione.Righe(1).SottocontoContropartita = 0
        Registrazione.Righe(1).Descrizione = ""
        Registrazione.Righe(1).Importo = Math.Round(ValoreAcq, 2)
        Registrazione.Righe(1).Segno = "+"
        Registrazione.Righe(1).RigaDaCausale = 2
        Registrazione.Righe(1).RigaRegistrazione = 2
        Registrazione.Righe(1).Utente = Session("UTENTE")

        If IsNothing(Registrazione.Righe(2)) Then
            Registrazione.Righe(2) = New Cls_MovimentiContabiliRiga
        End If



        Registrazione.Righe(2).DareAvere = CauCont.Righe(2).DareAvere
        Registrazione.Righe(2).Numero = 0
        Registrazione.Righe(2).MastroPartita = CauCont.Righe(2).Mastro
        Registrazione.Righe(2).ContoPartita = CauCont.Righe(2).Conto
        Registrazione.Righe(2).SottocontoPartita = CauCont.Righe(2).Sottoconto
        Registrazione.Righe(2).MastroContropartita = 0
        Registrazione.Righe(2).ContoContropartita = 0
        Registrazione.Righe(2).SottocontoContropartita = 0
        Registrazione.Righe(2).Descrizione = ""
        Registrazione.Righe(2).Importo = Math.Round(ValoreAcq - Ammortamento, 2)
        Registrazione.Righe(2).Segno = "+"
        Registrazione.Righe(2).RigaDaCausale = 3
        Registrazione.Righe(2).RigaRegistrazione = 3
        Registrazione.Righe(2).Utente = Session("UTENTE")



        Registrazione.Scrivi(Session("DC_GENERALE"), 0)
        CreaRegistrazioneDismissione = Registrazione.NumeroRegistrazione
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Cespite As New Cls_Cespiti

        Cespite.Codice = Val(Request.Item("Codcie"))
        Cespite.Leggi(Session("DC_GENERALE"), Cespite.Codice)

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiCespite Where  CodiceCespite = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CODICE", Txt_ID.Text)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Vi sono movimenti per questo Cespite non posso eliminare');", True)
            Exit Sub
        End If

        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = ("delete from Cespiti Where  Codice = ?")
        cmdDelete.Connection = cn
        cmdDelete.Parameters.AddWithValue("@CODICE", Txt_ID.Text)
        cmdDelete.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("Elenco_Cespiti.aspx")

    End Sub
End Class
