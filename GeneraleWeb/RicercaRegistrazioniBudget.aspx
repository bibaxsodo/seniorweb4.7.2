﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_RicercaRegistrazioniBudget" CodeFile="RicercaRegistrazioniBudget.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ricerca</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
    <style>
        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">               

        function DialogBox(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>

</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div align="left">
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Ricerca</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" OnClientClick="return SalvaInStorage();" ToolTip="Visualizza Dati" Width="38px" />
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" OnClientClick="return SalvaInStorage();" ToolTip="Esporta in excel" Style="width: 38px" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border-width: 0px;" cellspacing="0" cellpadding="0">
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>

                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" Width="100%" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Ricerca Registrazioni
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Causale Contabile:</label>
                                    <asp:DropDownList ID="DD_CausaleContabile" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Conto Contabilità:</label>
                                    <asp:TextBox ID="Txt_Sottoconto" onkeypress="return handleEnter(this, event)" CssClass="MyAutoComplete" runat="server" Width="402px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" AutoPostBack="true" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4" Width="52px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Livello:</label>
                                    <asp:TextBox ID="Txt_Livello" onkeypress="return handleEnter(this, event)" CssClass="MyAutoComplete" runat="server" Width="402px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data dal:</label>
                                    <asp:TextBox ID="Txt_DataDal" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data al:</label>
                                    <asp:TextBox ID="Txt_DataAl" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Dettaglio Colonne:</label>
                                    <asp:CheckBox ID="Chk_Colonne" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="384px"></asp:Label>
                                    &nbsp;<asp:GridView ID="Grid" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <Columns>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                    </asp:GridView>

                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
        <asp:GridView ID="GridView1" runat="server" Height="70px" Width="760px">
        </asp:GridView>

    </form>
</body>
</html>
