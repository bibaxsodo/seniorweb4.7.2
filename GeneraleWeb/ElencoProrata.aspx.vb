﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes


Partial Class GeneraleWeb_ElencoProrata
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 50  * from Prorata  " & _
                               "  Order By Anno,RegistroIVA,Attivita")
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("RegistroIVA", GetType(String))
        Tabella.Columns.Add("Decodifica RegistroIVA", GetType(String))
        Tabella.Columns.Add("Attivita", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbn(myPOSTreader.Item("Anno"))


            myriga(1) = campodbn(myPOSTreader.Item("RegistroIVA"))

            Dim DecodificaRegistro As New Cls_RegistroIVA

            DecodificaRegistro.Tipo = campodbn(myPOSTreader.Item("RegistroIVA"))
            DecodificaRegistro.Leggi(Session("DC_TABELLE"), DecodificaRegistro.Tipo)


            myriga(2) = DecodificaRegistro.Descrizione
            myriga(3) = campodb(myPOSTreader.Item("Attivita"))


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)



            Dim Anno As String
            Dim Registro As String


            Anno = Tabella.Rows(d).Item(0).ToString

            Registro = Tabella.Rows(d).Item(1).ToString

            Response.Redirect("Prorata.aspx?Anno=" & Anno & "&Registro=" & Registro)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("Prorata.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")

    End Sub

    Protected Sub ImgHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgHome.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub


End Class
