﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_Elenco_Categorie
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 50  * from CategoriaCespite  " & _
                               "  Order By Descrizione")
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("Id"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)



            Dim Codice As String


            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("TabellaCategoria.aspx?Codice=" & Codice)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("TabellaCategoria.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")

    End Sub

    Protected Sub ImgHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgHome.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub



    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection



        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        cmd.CommandText = ("select  * from [CategoriaCespite]  " & _
                                  "  Order By Descrizione")
        cmd.Connection = cn
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Legge", GetType(String))
        Tabella.Columns.Add("Minima", GetType(String))
        Tabella.Columns.Add("Anticipata", GetType(String))
        Tabella.Columns.Add("Ammortamento", GetType(String))
        Tabella.Columns.Add("Fondo Ammortamento", GetType(String))
        Tabella.Columns.Add("Imobilizazione", GetType(String))




        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("Id"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(2) = campodb(myPOSTreader.Item("Legge"))

            myriga(3) = campodb(myPOSTreader.Item("Minima"))
            myriga(4) = campodb(myPOSTreader.Item("Anticipata"))

            Dim MSC As New Cls_Pianodeiconti



            MSC.Descrizione = ""
            MSC.Mastro = campodb(myPOSTreader.Item("Mastro"))
            MSC.Conto = campodb(myPOSTreader.Item("Conto"))
            MSC.Sottoconto = campodb(myPOSTreader.Item("SottoConto"))
            MSC.Decodfica(Session("DC_GENERALE"))

            myriga(5) = MSC.Descrizione


            MSC.Descrizione = ""
            MSC.Mastro = campodb(myPOSTreader.Item("MastroContropartita"))
            MSC.Conto = campodb(myPOSTreader.Item("ContoContropartita"))
            MSC.Sottoconto = campodb(myPOSTreader.Item("SottoContoContropartita"))
            MSC.Decodfica(Session("DC_GENERALE"))

            myriga(6) = MSC.Descrizione


            MSC.Descrizione = ""
            MSC.Mastro = campodb(myPOSTreader.Item("MastroImobilizazione"))
            MSC.Conto = campodb(myPOSTreader.Item("ContoImobilizazione"))
            MSC.Sottoconto = campodb(myPOSTreader.Item("SottocontoImobilizazione"))
            MSC.Decodfica(Session("DC_GENERALE"))

            myriga(7) = MSC.Descrizione




            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub


End Class
