﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_FatturaElettronica_2
    Inherits System.Web.UI.Page

    Protected Sub Txt_XMLCODE_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_XMLCODE.TextChanged

    End Sub

    Protected Sub Chk_AbilitaModifica_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_AbilitaModifica.CheckedChanged
        If Chk_AbilitaModifica.Checked = True Then
            Txt_XMLCODE.Enabled = True
        Else
            Txt_XMLCODE.Enabled = False
        End If
    End Sub

    Protected Sub GeneraleWeb_FatturaElettronica_2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        If IsNothing(Session("TestoXML")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        ' CODICE PER PULIZIA STRINGHE IN MODO DA CONTENERE VALORI SOLO ASCII
        Txt_XMLCODE.Text = Regex.Replace(Session("TestoXML"), "[^\u0000-\u007F]+", String.Empty)

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        Dim Appoggio As String

        If Trim(Session("Allegato")) <> "" Then

            Appoggio = "<Allegati>" & vbNewLine
            Appoggio = Appoggio & "<FormatoAttachment>PDF</FormatoAttachment>" & vbNewLine
            Appoggio = Appoggio & "<DescrizioneAttachment>" & Session("NOMEFILE") & "</DescrizioneAttachment>" & vbNewLine
            Appoggio = Appoggio & "<Attachment>"
            Appoggio = Appoggio & Session("Allegato")
            Appoggio = Appoggio & "</Attachment>" & vbNewLine
            Appoggio = Appoggio & "</Allegati>"

            Txt_XMLCODE.Text = Replace(Txt_XMLCODE.Text, "<Allegati></Allegati>", Appoggio)
        End If

        Dim Soc As New Cls_DecodificaSocieta

        Soc.Leggi(Session("DC_TABELLE"))
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmdw As New OleDbCommand

        cmdw.CommandText = "UPDATE FatturaElettronica Set XMLOriginale  =? Where Progressivo= ? "
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@XMLModificato", Txt_XMLCODE.Text)
        cmdw.Parameters.AddWithValue("@Progressivo", Val(Session("Progressivo")))
        cmdw.ExecuteNonQuery()



        cn.Close()

        Dim responseText As String = Txt_XMLCODE.Text.Replace("\r\n", "<br/>")
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=IT" & Soc.CodiceFiscale & "_" & Format(Val(Session("Progressivo")), "00000") & ".xml")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8
        REM Response.ContentEncoding = Encoding.UTF16
        Response.ContentType = "text/xml; charset=utf-8"
        REM Response.ContentType = "text/xml; charset=utf-16"
        Response.Write(responseText)
        Response.End()
    End Sub
End Class
