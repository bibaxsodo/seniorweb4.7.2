﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Security.Cryptography
Imports System.Net.Mail


Partial Class GeneraleWeb_ImportRegistrazioni
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim nomefileexcel As String

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")


        Dim MyRs As New ADODB.Recordset
        Dim RsTesta As New ADODB.Recordset
        Dim RsRiga As New ADODB.Recordset
        Dim RsCercaDoc As New ADODB.Recordset
        Dim Campi(100) As String
        Dim ViSonoErrori As String
        Dim Totale As Double
        Dim RegistrazioniNonInseriti As String
        Dim Riga As Integer
        Dim Testo As String

        Dim Rest As Integer
        Dim Codiceditta As String
        Dim RagioneSociale As String
        Dim Matricola As String
        Dim Nmatr As String
        Dim Cognome As String
        Dim Nome As String
        Dim Dataelab As String
        Dim CodiceRC As String
        Dim DescrizioneRC As String
        Dim Codsip As String
        Dim CodCG As String
        Dim Grigliaqualifica As String
        Dim Tipocosto As String
        Dim XCodiceRegione As String
        Dim Codcentrocosto As String
        Dim Codcentrocontabile As String
        Dim Desccentrocosto As String
        Dim Codicesede As String
        Dim Segnoimporto As String
        Dim Importo As String
        Dim ContoDare As String
        Dim DescrContodare As String
        Dim ContoAvere As String
        Dim DescContoavere As String
        Dim Contocna As String

        Dim Giorniretribuiti As String
        Dim Giornilavorati As String
        Dim Oreordinarie As String
        Dim Orestraordinarie As String
        Dim tipoRegistrazione As String

        Dim Mastro As String
        Dim Conto As String
        Dim Sottoconto As String
        Dim OldMatricola As String
        Dim Registrazione As Integer

        Dim VettoreRegistrazioni(3000) As Long
        Dim IndiceRegistrazioni As Integer = 0


        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")






        ViSonoErrori = ""
        RegistrazioniNonInseriti = ""
        Riga = 0        
        Do While Not objStreamReader.EndOfStream
            Riga = Riga + 1
            Testo = objStreamReader.ReadLine

            Rest = 1
            Codiceditta = Mid(Testo, 1, 10) : Rest = Rest + 10
            RagioneSociale = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Matricola = Mid(Testo, Rest, 4) : Rest = Rest + 4
            Nmatr = Mid(Testo, Rest, 4) : Rest = Rest + 4
            Cognome = Mid(Testo, Rest, 20) : Rest = Rest + 20
            Nome = Mid(Testo, Rest, 20) : Rest = Rest + 20
            Dataelab = Mid(Testo, Rest, 6) : Rest = Rest + 6
            CodiceRC = Mid(Testo, Rest, 3) : Rest = Rest + 3
            DescrizioneRC = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Codsip = Mid(Testo, Rest, 3) : Rest = Rest + 3
            CodCG = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Grigliaqualifica = Mid(Testo, Rest, 50) : Rest = Rest + 50
            Tipocosto = Mid(Testo, Rest, 1) : Rest = Rest + 1
            XCodiceRegione = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Codcentrocosto = Mid(Testo, Rest, 6) : Rest = Rest + 6
            Codcentrocontabile = Mid(Testo, Rest, 6) : Rest = Rest + 6
            Desccentrocosto = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Codicesede = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Segnoimporto = Mid(Testo, Rest, 1) : Rest = Rest + 1
            Importo = Mid(Testo, Rest, 10) : Rest = Rest + 10                   '
            ContoDare = Mid(Testo, Rest, 16) : Rest = Rest + 16
            DescrContodare = Mid(Testo, Rest, 65) : Rest = Rest + 65
            ContoAvere = Mid(Testo, Rest, 16) : Rest = Rest + 16
            DescContoavere = Mid(Testo, Rest, 65) : Rest = Rest + 65
            Contocna = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Giorniretribuiti = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Giornilavorati = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Oreordinarie = Mid(Testo, Rest, 5) : Rest = Rest + 5
            Orestraordinarie = Mid(Testo, Rest, 5) : Rest = Rest + 5
            tipoRegistrazione = Mid(Testo, Rest, 1) : Rest = Rest + 1


            Mastro = Mid(ContoDare & Space(10), 1, 2)
            Conto = Mid(ContoDare & Space(10), 3, 2)
            Sottoconto = Mid(ContoDare & Space(10), 5, 2)


            Dim Pcdc As New Cls_Pianodeiconti

            Pcdc.Mastro = Mastro
            Pcdc.Conto = Conto
            Pcdc.Sottoconto = Sottoconto
            Pcdc.Decodfica(Session("DC_GENERALE"))
            If Pcdc.Descrizione = "" Then
                RegistrazioniNonInseriti = RegistrazioniNonInseriti & "<tr class=""miotr""><td class=""miacella"">Errore conto dare inesistente in riga</td><td class=""miacella"">" & Riga & "</td></tr>"
                ViSonoErrori = "SI"
            End If

            Mastro = Mid(ContoAvere & Space(10), 1, 2)
            Conto = Mid(ContoAvere & Space(10), 3, 2)
            Sottoconto = Mid(ContoAvere & Space(10), 5, 2)


            If Mastro = 12 And Conto = 5 And Sottoconto = 1 Then
                Mastro = 12
            End If

            Pcdc.Descrizione = ""
            Pcdc.Mastro = Mastro
            Pcdc.Conto = Conto
            Pcdc.Sottoconto = Sottoconto
            Pcdc.Decodfica(Session("DC_GENERALE"))
            If Pcdc.Descrizione = "" Then
                RegistrazioniNonInseriti = RegistrazioniNonInseriti & "<tr class=""miotr""><td class=""miacella"">Errore conto avere inesistente in riga </td><td class=""miacella"">" & Riga & "</td></tr>"
                ViSonoErrori = "SI"
            End If


            OldMatricola = Matricola
        Loop
        
        objStreamReader.Close()


        If ViSonoErrori <> "" Then
            Lbl_Errori.Text = "<table><tr class=""miotr""><th class=""miaintestazione"">Messaggio</th><th class=""miaintestazione"">Riga</th></tr>"
            Lbl_Errori.Text = RegistrazioniNonInseriti & "</table>"
            Exit Sub
        End If

        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")


        
        OldMatricola = 0
        Registrazione = 0
        Dim TCamp As Integer
        Dim UltimoCampo As Integer
        Dim Errore As Boolean = False
        Dim MySql As String
        Dim TXTNUMERO As Integer
        Dim TestoOutput As String = ""
        Dim GeneraleDb As New ADODB.Connection


        TXTNUMERO = MaxRegistrazione()


        GeneraleDb.Open(Session("DC_GENERALE"))

        GeneraleDb.BeginTrans()

        
        
        Do While Not objStreamReader.EndOfStream

            Testo = objStreamReader.ReadLine
            TCamp = 1
            UltimoCampo = 1

            Rest = 1
            Codiceditta = Mid(Testo, 1, 10) : Rest = Rest + 10
            RagioneSociale = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Matricola = Mid(Testo, Rest, 4) : Rest = Rest + 4
            Nmatr = Mid(Testo, Rest, 4) : Rest = Rest + 4
            Cognome = Mid(Testo, Rest, 20) : Rest = Rest + 20
            Nome = Mid(Testo, Rest, 20) : Rest = Rest + 20
            Dataelab = Mid(Testo, Rest, 6) : Rest = Rest + 6
            CodiceRC = Mid(Testo, Rest, 3) : Rest = Rest + 3
            DescrizioneRC = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Codsip = Mid(Testo, Rest, 3) : Rest = Rest + 3
            CodCG = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Grigliaqualifica = Mid(Testo, Rest, 50) : Rest = Rest + 50
            Tipocosto = Mid(Testo, Rest, 1) : Rest = Rest + 1
            XCodiceRegione = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Codcentrocosto = Mid(Testo, Rest, 6) : Rest = Rest + 6
            Codcentrocontabile = Mid(Testo, Rest, 6) : Rest = Rest + 6
            Desccentrocosto = Mid(Testo, Rest, 60) : Rest = Rest + 60
            Codicesede = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Segnoimporto = Mid(Testo, Rest, 1) : Rest = Rest + 1
            Importo = Mid(Testo, Rest, 10) : Rest = Rest + 10                   '
            ContoDare = Mid(Testo, Rest, 16) : Rest = Rest + 16
            DescrContodare = Mid(Testo, Rest, 65) : Rest = Rest + 65
            ContoAvere = Mid(Testo, Rest, 16) : Rest = Rest + 16
            DescContoavere = Mid(Testo, Rest, 65) : Rest = Rest + 65
            Contocna = Mid(Testo, Rest, 3) : Rest = Rest + 3
            Giorniretribuiti = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Giornilavorati = Mid(Testo, Rest, 2) : Rest = Rest + 2
            Oreordinarie = Mid(Testo, Rest, 5) : Rest = Rest + 5
            Orestraordinarie = Mid(Testo, Rest, 5) : Rest = Rest + 5
            tipoRegistrazione = Mid(Testo, Rest, 1) : Rest = Rest + 1


            If Val(Importo) > 1 Then
                If Errore = False Then

                    If Matricola <> OldMatricola Then


                        TXTNUMERO = TXTNUMERO + 1
                        MySql = "SELECT * FROM MovimentiContabiliTesta where NumeroRegistrazione =0 "
                        RsTesta.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        RsTesta.AddNew()

                        RsTesta.Fields("NumeroRegistrazione").Value = TXTNUMERO

                        RsTesta.Fields("CausaleContabile").Value = Dd_CausaleContabile.SelectedValue
                        RsTesta.Fields("DataRegistrazione").Value = Txt_Data.Text
                        RsTesta.Fields("DataValuta").Value = Txt_Data.Text

                        RsTesta.Fields("Descrizione").Value = Cognome.Trim & " " & Nome.Trim
                        RsTesta.Fields("Competenza").Value = ""
                        RsTesta.Fields("Utente").Value = "IMPREG"
                        RsTesta.Fields("DataAggiornamento").Value = Now


                        TXTNUMERO = RsTesta.Fields("NumeroRegistrazione").Value
                        RsTesta.Update()
                        RsTesta.Close()
                        OldMatricola = Matricola

                        IndiceRegistrazioni = IndiceRegistrazioni + 1
                        VettoreRegistrazioni(IndiceRegistrazioni) = TXTNUMERO
                        TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">Inserito registrazione </td><td class=""miacella"">" & TXTNUMERO & "</td></tr>"
                    End If



                    MySql = "SELECT * FROM MovimentiContabiliRiga where numero =0  "
                    RsRiga.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    RsRiga.AddNew()
                    RsRiga.Fields("Utente").Value = "IMPREG"
                    RsRiga.Fields("DataAggiornamento").Value = Now
                    RsRiga.Fields("Numero").Value = TXTNUMERO


                    RsRiga.Fields("MastroPartita").Value = Mid(ContoDare, 1, 2)
                    RsRiga.Fields("ContoPartita").Value = Mid(ContoDare, 3, 2)
                    RsRiga.Fields("SottocontoPartita").Value = Mid(ContoDare, 5, 2)

                    RsRiga.Fields("MastroContropartita").Value = 0
                    RsRiga.Fields("ContoContropartita").Value = 0
                    RsRiga.Fields("SottocontoContropartita").Value = 0

                    RsRiga.Fields("Descrizione").Value = Mid(DescrizioneRC & Space(50), 1, 50)

                    RsRiga.Fields("Segno").Value = "+"
                    If Segnoimporto = "+" Then
                        RsRiga.Fields("DareAvere").Value = "D"
                    Else
                        RsRiga.Fields("DareAvere").Value = "A"
                    End If
                    RsRiga.Fields("Importo").Value = Math.Round(Mid(Importo, 1, 8) + (Mid(Importo, 9, 2) / 100), 2)
                    RsRiga.Update()
                    RsRiga.Close()


                    MySql = "SELECT * FROM MovimentiContabiliRiga where numero = 0"
                    RsRiga.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    RsRiga.AddNew()
                    RsRiga.Fields("Utente").Value = "IMPREG"
                    RsRiga.Fields("DataAggiornamento").Value = Now
                    RsRiga.Fields("Numero").Value = TXTNUMERO


                    RsRiga.Fields("MastroPartita").Value = Mid(ContoAvere, 1, 2)
                    RsRiga.Fields("ContoPartita").Value = Mid(ContoAvere, 3, 2)
                    RsRiga.Fields("SottocontoPartita").Value = Mid(ContoAvere, 5, 2)



                    RsRiga.Fields("MastroContropartita").Value = 0
                    RsRiga.Fields("ContoContropartita").Value = 0
                    RsRiga.Fields("SottocontoContropartita").Value = 0


                    RsRiga.Fields("Descrizione").Value = Mid(DescrizioneRC & Space(50), 1, 50)

                    RsRiga.Fields("Segno").Value = "+"

                    If Segnoimporto = "+" Then
                        RsRiga.Fields("DareAvere").Value = "A"
                    Else
                        RsRiga.Fields("DareAvere").Value = "D"
                    End If
                    RsRiga.Fields("Importo").Value = Math.Round(Mid(Importo, 1, 8) + (Mid(Importo, 9, 2) / 100), 2)
                    RsRiga.Update()
                    RsRiga.Close()

                End If

            End If
        Loop
        objStreamReader.Close()

        GeneraleDb.CommitTrans()


        If Chk_RagConti.Checked = True Then
            For i = 0 To IndiceRegistrazioni
                If VettoreRegistrazioni(i) > 0 Then
                    RaggruppaRegistrazioni(VettoreRegistrazioni(i))
                End If
            Next
        End If


        Lbl_Errori.Text = "<table><tr class=""miotr""><th class=""miaintestazione"">Messaggio</th><th class=""miaintestazione"">Registrazione</th></tr>"
        Lbl_Errori.Text = Lbl_Errori.Text & TestoOutput & "</table>"
        Lbl_Errori.Visible = True

    End Sub

    Sub RaggruppaRegistrazioni(ByVal NumeroRegistrazione As Long)
        Dim cn As OleDbConnection

        Dim ImportoTotale As Double = 0
        Dim DareAvere As String = ""

        Dim VettoreMastro(100)
        Dim VettoreConto(100)
        Dim VettoreSottoconto(100)
        Dim VettoreImporto(100)
        Dim VettoreDareAVere(100)
        Dim Indice As Integer = 0

        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = " SELECT MastroPartita,ContoPartita,SottocontoPartita,SUM(CASE WHEN DareAvere = 'D' THEN importo  ELSE 0 END)  AS TOTDARE,SUM(CASE WHEN DareAvere = 'A' THEN importo ELSE 0 END)  AS TOTAVERE FROM MovimentiContabiliRiga Where Numero = ? Group by MastroPartita,ContoPartita,SottocontoPartita"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            VettoreMastro(Indice) = campodb(myPOSTreader.Item("MastroPartita"))
            VettoreConto(Indice) = campodb(myPOSTreader.Item("ContoPartita"))
            VettoreSottoconto(Indice) = campodb(myPOSTreader.Item("SottocontoPartita"))
            If CDbl(campodb(myPOSTreader.Item("TOTAVERE"))) > CDbl(campodb(myPOSTreader.Item("TOTDARE"))) Then
                VettoreImporto(Indice) = campodb(myPOSTreader.Item("TOTAVERE")) - campodb(myPOSTreader.Item("TOTDARE"))
                VettoreDareAVere(Indice) = "A"
            Else
                VettoreImporto(Indice) = campodb(myPOSTreader.Item("TOTDARE")) - campodb(myPOSTreader.Item("TOTAVERE"))
                VettoreDareAVere(Indice) = "D"
            End If

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()

        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = " Delete FROM MovimentiContabiliRiga Where Numero = ?  "
        cmdDelete.Connection = cn
        cmdDelete.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
        cmdDelete.ExecuteNonQuery()



        Dim I As Integer

        For I = 0 To Indice

            If Math.Abs(VettoreImporto(I)) > 0 Then
                Dim cmdInsert As New OleDbCommand()
                Dim MySql As String = ""

                MySql = " INSERT INTO MovimentiContabiliRiga (Utente,DataAggiornamento,Numero,MastroPartita,ContoPartita,SottocontoPartita,MastroContropartita,ContoContropartita,SottocontoContropartita,Descrizione,DareAvere,Segno,Importo) values   "
                MySql = MySql & " (?,?,?,?,?,?,?,?,?,?,?,?,?) "

                cmdInsert.CommandText = MySql

                cmdInsert.Connection = cn
                cmdInsert.Parameters.AddWithValue("Utente", "IMPREG") '1
                cmdInsert.Parameters.AddWithValue("DataAggiornamento", Now) '2
                cmdInsert.Parameters.AddWithValue("Numero", NumeroRegistrazione) '3
                cmdInsert.Parameters.AddWithValue("MastroPartita", VettoreMastro(I)) '4
                cmdInsert.Parameters.AddWithValue("ContoPratita", VettoreConto(I)) '5
                cmdInsert.Parameters.AddWithValue("SottocontoPartita", VettoreSottoconto(I)) '6
                cmdInsert.Parameters.AddWithValue("MastroContropartita", 0) '7
                cmdInsert.Parameters.AddWithValue("ContoContropartita", 0) '8
                cmdInsert.Parameters.AddWithValue("SottocontoContropartita", 0) '9
                cmdInsert.Parameters.AddWithValue("Descrizione", "") '10            
                cmdInsert.Parameters.AddWithValue("DareAvere", VettoreDareAVere(I)) '12
                cmdInsert.Parameters.AddWithValue("Segno", "+") '11
                cmdInsert.Parameters.AddWithValue("Importo", Math.Abs(VettoreImporto(I))) '13
                cmdInsert.ExecuteNonQuery()
            End If
        Next


        cn.Close()
    End Sub


    Function MaxRegistrazione() As Long
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroRegistrazione) from MovimentiContabiliTesta ")        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxRegistrazione = Val(campodb(myPOSTreader.Item(0))) + 1
        End If
        myPOSTreader.Close()

        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MCausaliContabili As New Cls_CausaleContabile

        MCausaliContabili.UpDateDropBox(Session("DC_TABELLE"), Dd_CausaleContabile)
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
