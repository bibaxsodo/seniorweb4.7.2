﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting

Partial Class GeneraleWeb_ControlloRegistrazioni
    Inherits System.Web.UI.Page



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Function ControlloMovimenti() As String
        Dim MoCoTe As New ADODB.Recordset
        Dim MoCoRi As New ADODB.Recordset
        Dim PdC As New ADODB.Recordset
        Dim MySql As String

        Dim cn As OleDbConnection

        Dim TotCliFor As Double
        Dim DareAvere As String
        Dim TotImposta As Double
        Dim TotImponibile As Double
        Dim TotDare As Double
        Dim TotAvere As Double
        Dim TotAttivita As Double
        Dim TotPassivita As Double
        Dim TotCosti As Double
        Dim TotRicavi As Double
        Dim TotOrdine As Double
        Dim Differenza As Double

        Dim StringaDegliErrori As String = ""


        lbl_errori.Text = ""
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()





        If IsDate(Txt_DataInizio.Text) And IsDate(Txt_DataFine.Text) Then
            Dim DataX As Date = Txt_DataInizio.Text
            Dim DataF As Date = Txt_DataFine.Text

            MySql = "SELECT NumeroRegistrazione, CausaleContabile FROM MovimentiContabiliTesta WHERE DataRegistrazione >= {ts'" & Format(DataX, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts'" & Format(DataF, "yyyy-MM-dd") & " 00:00:00'}"
        Else
            MySql = "SELECT NumeroRegistrazione, CausaleContabile FROM MovimentiContabiliTesta"
        End If

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120

        Dim MRead As OleDbDataReader = cmd.ExecuteReader()

        Do While MRead.Read

            TotCliFor = 0
            DareAvere = ""
            TotImposta = 0
            TotImponibile = 0
            TotDare = 0
            TotAvere = 0
            TotAttivita = 0
            TotPassivita = 0
            TotCosti = 0
            TotRicavi = 0
            TotOrdine = 0

            If campodbN(MRead.Item("NumeroRegistrazione")) = 49356 Then
                TotOrdine = 0
            End If

            MySql = "SELECT * FROM MovimentiContabiliRiga WHERE Numero = " & campodbN(MRead.Item("NumeroRegistrazione")) & " ORDER BY RigaDaCausale"
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = MySql
            cmdR.Connection = cn
            cmdR.CommandTimeout = 120

            Dim MReadRoga As OleDbDataReader = cmdR.ExecuteReader()
            Do While MReadRoga.Read


                'MoveToDb(MoCoRi!Importo, Round(MoveFromDbWC(MoCoRi, "Importo"), DecEuro))

                If campodb(MReadRoga.Item("Tipo")) = "CF" Then
                    TotCliFor = campodbN(MReadRoga.Item("Importo"))
                    DareAvere = campodb(MReadRoga.Item("DareAvere"))
                End If
                If campodb(MReadRoga.Item("Tipo")) = "IV" Then
                    Dim MiaIVa As New Cls_IVA
                    MiaIVa.Codice = campodb(MReadRoga.Item("CodiceIVA"))
                    MiaIVa.Leggi(Session("DC_TABELLE"), MiaIVa.Codice)

                    If Modulo.MathRound(CDbl(campodb(MReadRoga.Item("Imponibile"))) * MiaIVa.Aliquota, 2) <> Math.Round(CDbl(campodb(MReadRoga.Item("Importo"))), 2) Then
                        StringaDegliErrori = StringaDegliErrori & " Iva non calcolata correttamente su registrazione " & campodb(MReadRoga.Item("Numero")) & " Improti " & Modulo.MathRound(CDbl(campodb(MReadRoga.Item("Imponibile"))) * MiaIVa.Aliquota, 2) & " <> " & Math.Round(CDbl(campodb(MReadRoga.Item("Importo"))), 2) & vbNewLine
                    End If

                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - campodb(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile - campodb(MReadRoga.Item("Imponibile"))
                    Else
                        TotImposta = TotImposta + campodb(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile + campodb(MReadRoga.Item("Imponibile"))
                    End If
                End If
                If campodb(MReadRoga.Item("Tipo")) = "AR" Or campodb(MReadRoga.Item("Tipo")) = "AB" Then
                    If campodb(MReadRoga.Item("DareAvere")) = DareAvere Then
                        TotImposta = TotImposta - campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile - campodb(MReadRoga.Item("Imponibile"))
                    Else
                        TotImposta = TotImposta + campodbN(MReadRoga.Item("Importo"))
                        TotImponibile = TotImponibile + campodb(MReadRoga.Item("Imponibile"))
                    End If
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                    TotDare = TotDare + campodbN(MReadRoga.Item("Importo"))
                End If
                If campodb(MReadRoga.Item("DareAvere")) = "A" Then
                    TotAvere = TotAvere + campodbN(MReadRoga.Item("Importo"))
                End If
                If campodbN(MReadRoga.Item("MastroPartita")) = 0 Or campodbN(MReadRoga.Item("ContoPartita")) = 0 Or campodbN(MReadRoga.Item("SottocontoPartita")) = 0 Then
                    StringaDegliErrori = StringaDegliErrori & "Mastro/Conto/Sottoconto errato nella registrazione: " & Str(campodbN(MRead.Item("NumeroRegistrazione"))) & vbNewLine
                Else
                    MySql = "SELECT * FROM PianoConti " & _
                            " WHERE Mastro = " & campodbN(MReadRoga.Item("MastroPartita")) & _
                            " AND Conto = " & campodbN(MReadRoga.Item("ContoPartita")) & _
                            " AND Sottoconto = " & campodbN(MReadRoga.Item("SottocontoPartita"))

                    Dim cmdP As New OleDbCommand()
                    cmdP.CommandText = MySql
                    cmdP.Connection = cn

                    Dim MReadPc As OleDbDataReader = cmdP.ExecuteReader()
                    If Not MReadPc.Read Then
                        StringaDegliErrori = StringaDegliErrori & "Sottoconto inesistente: " & campodbN(MReadRoga.Item("MastroPartita")) & "/" & campodbN(MReadRoga.Item("ContoPartita")) & "/" & campodbN(MReadRoga.Item("SottocontoPartita")) & "  nella registrazione: " & Str(campodbN(MRead.Item("NumeroRegistrazione"))) & vbNewLine
                    Else
                        If campodb(MReadPc.Item("Tipo")) = "A" Then
                            If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                                TotAttivita = TotAttivita + campodbN(MReadRoga.Item("Importo"))
                            Else
                                TotAttivita = TotAttivita - campodbN(MReadRoga.Item("Importo"))
                            End If
                        End If
                        If campodb(MReadPc.Item("Tipo")) = "P" Then
                            If campodb(MReadRoga.Item("DareAvere")) = "A" Then
                                TotPassivita = TotPassivita + campodbN(MReadRoga.Item("Importo"))
                            Else
                                TotPassivita = TotPassivita - campodbN(MReadRoga.Item("Importo"))
                            End If
                        End If
                        If campodb(MReadPc.Item("Tipo")) = "C" Then
                            If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                                TotCosti = TotCosti + campodbN(MReadRoga.Item("Importo"))
                            Else
                                TotCosti = TotCosti - campodbN(MReadRoga.Item("Importo"))
                            End If
                        End If
                        If campodb(MReadPc.Item("Tipo")) = "R" Then
                            If campodb(MReadRoga.Item("DareAvere")) = "A" Then
                                TotRicavi = TotRicavi + campodbN(MReadRoga.Item("Importo"))
                            Else
                                TotRicavi = TotRicavi - campodbN(MReadRoga.Item("Importo"))
                            End If
                        End If
                        If campodb(MReadPc.Item("Tipo")) = "O" Then
                            If campodb(MReadRoga.Item("DareAvere")) = "D" Then
                                TotOrdine = TotOrdine + campodbN(MReadRoga.Item("Importo"))
                            Else
                                TotOrdine = TotOrdine - campodbN(MReadRoga.Item("Importo"))
                            End If
                        End If
                        '          If MoveFromDbWC(PdC,"Tipo") = "O" Then TotOrdine = TotOrdine + MoveFromDbWC(MoCoRi,"Importo")
                    End If
                    MReadPc.Close()
                End If


            Loop
            If Format(TotDare, "0.00") <> Format(TotAvere, "0.00") Then
                Dim CauCon1 As New Cls_CausaleContabile

                CauCon1.Leggi(Session("DC_TABELLE"), campodb(MRead.Item("CausaleContabile")))

                StringaDegliErrori = StringaDegliErrori & "Dare diverso da Avere sulla registrazione: " & campodbN(MRead.Item("NumeroRegistrazione")) & " " & CauCon1.Descrizione & vbNewLine
            End If

            Differenza = (TotAttivita - TotPassivita) - (TotRicavi - TotCosti)
            If Differenza <> 0 Then
                If Format(Math.Abs(Differenza), "0.00") <> Format(Math.Abs(TotOrdine), "0.00") Then
                    Dim CauCon1 As New Cls_CausaleContabile

                    CauCon1.Leggi(Session("DC_TABELLE"), campodb(MRead.Item("CausaleContabile")))

                    StringaDegliErrori = StringaDegliErrori & "Attività/Passività diversa da Ricavi/Costi sulla registrazione: " & campodbN(MRead.Item("NumeroRegistrazione")) & " " & CauCon1.Descrizione & vbNewLine
                End If
            End If
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), campodb(MRead.Item("CausaleContabile")))

            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                If Format(TotCliFor, "0.00") <> Format(TotImponibile + TotImposta, "0.00") Then
                    Differenza = TotCliFor - TotImponibile - TotImposta


                    StringaDegliErrori = StringaDegliErrori & "Totale Documento diverso da Imponibile + Imposta sulla registrazione: " & campodbN(MRead.Item("NumeroRegistrazione")) & " " & CauCon.Descrizione & vbNewLine

                End If
            End If
            MReadRoga.Close()



        Loop
        MRead.Close()

        Return StringaDegliErrori
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Lbl_Errori.Text = ControlloMovimenti()
    End Sub

    Protected Sub GeneraleWeb_ControlloRegistrazioni_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataInizio.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataFine.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataInizio.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataFine.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataApertura", MyJs, True)

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


  
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
