﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_ReverseCharge" CodeFile="ReverseCharge.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <label style="display: block; float: left; width: 200px;">Causale Fattura Vendita:</label>
            <asp:DropDownList ID="Dd_CausaleContabileFV" runat="server" Width="350px"></asp:DropDownList>
            <br />
            <br />
            <label style="display: block; float: left; width: 200px;">Causale Giroconto :</label>
            <asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="350px"></asp:DropDownList>
            <br />
            <br />
            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="48px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi"
                Width="48px" />
        </div>
    </form>
</body>
</html>

