﻿
Partial Class GeneraleWeb_ElencoVariazioniBudget
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            Exit Sub
        End If
        Dim kX As New Cls_VariazioniBudget


        kX.loaddati(Session("DC_GENERALE"), Txt_Anno.Text, MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Prenotazioni.AutoGenerateColumns = True
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim kX As New Cls_VariazioniBudget


        Txt_Anno.Text = Year(Now)

        kX.loaddati(Session("DC_GENERALE"), Year(Now), MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Prenotazioni.AutoGenerateColumns = True
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Prenotazioni.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_Prenotazioni.PageIndex = e.NewPageIndex
        Grd_Prenotazioni.DataSource = MyTable
        Grd_Prenotazioni.DataBind()
    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Prenotazioni.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)



            Dim Livello1 As String
            Dim Livello2 As String
            Dim Livello3 As String
            Dim Colonna As String

            Livello1 = MyTable.Rows(d).Item(1).ToString
            Livello2 = MyTable.Rows(d).Item(2).ToString
            Livello3 = MyTable.Rows(d).Item(3).ToString

            Colonna = MyTable.Rows(d).Item(4).ToString


            Response.Redirect("VarazioneBudget.aspx?Anno=" & Txt_Anno.Text & "&Livello1=" & Livello1 & "&Livello2=" & Livello2 & "&Livello3=" & Livello3 & "&Colonna=" & Colonna & "&Data=" & MyTable.Rows(d).Item(5).ToString)

        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Prenotazioni.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("VarazioneBudget.aspx?Anno=" & Txt_Anno.Text & "&Livello1=0")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

End Class
