﻿
Partial Class GeneraleWeb_Ritenute
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim xTp As New ClsRitenuta

        Dim MT As New Cls_TabellaTributo


        MT.UpDateDropBox(Session("DC_TABELLE"), DDTribuo)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Request.Item("Codice") = "" Then
            Dim M As New ClsRitenuta
            Txt_Codice.Text = M.MaxTipoRitenute(Session("DC_TABELLE"))

            Exit Sub
        End If

        xTp.Codice = Request.Item("Codice")
        xTp.Leggi(Session("DC_TABELLE"))
        Txt_Codice.Text = Request.Item("Codice")
        Txt_Descrizione.Text = xTp.Descrizione
        TxT_Percentuale.Text = Math.Round(xTp.Percentuale, 2)

        Dim MC As New Cls_Pianodeiconti

        MC.Mastro = xTp.Mastro
        MC.Conto = xTp.Conto
        MC.Sottoconto = xTp.Sottoconto
        MC.Decodfica(Session("DC_GENERALE"))

        Txt_Conto.Text = MC.Mastro & " " & MC.Conto & " " & MC.Sottoconto & " " & MC.Descrizione


        Txt_Codice.Enabled = False
        DDTribuo.SelectedValue = xTp.Tributo

    End Sub



    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim MyJs As String = ""
        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)

            Call EseguiJS()
            REM Lbl_Errori.Text = "Specificare codice"
            Exit Sub
        End If
        If Txt_Descrizione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare la descrizione');", True)

            Call EseguiJS()
            REM Lbl_Errori.Text = "Specificare descrizione"
            Exit Sub
        End If
        If Not IsNumeric(TxT_Percentuale.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica percentuale');", True)
            Call EseguiJS()            
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim xVerifica As New ClsRitenuta

            xVerifica.Codice = Txt_Codice.Text
            xVerifica.Leggi(Session("DC_TABELLE"))

            If xVerifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già presente');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        'Dim i As Integer = 0
        'If DD_TipoDetraibilita.SelectedValue = "" Then
        '    For i = 0 To DD_TipoDetraibilita.Items.Count - 1
        '        If DD_TipoDetraibilita.Items(i).Text.IndexOf(Txt_Descrizione.Text) > 0 Then
        '            Lbl_Errori.Text = "Descrizione già inserita"
        '            Exit Sub
        '        End If
        '    Next
        'End If


        Dim xTp As New ClsRitenuta

        xTp.Codice = Txt_Codice.Text
        xTp.Leggi(Session("DC_TABELLE"))
        xTp.Codice = Txt_Codice.Text
        xTp.Descrizione = Txt_Descrizione.Text

        xTp.Percentuale = TxT_Percentuale.Text

        xTp.Tributo = DDTribuo.SelectedValue
        If Trim(Txt_Conto.Text) <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Conto.Text)
            If Val(Vettore(0)) > 0 Then
                xTp.Mastro = Val(Vettore(0))
            End If
            If Val(Vettore(1)) > 0 Then
                xTp.Conto = Val(Vettore(1))
            End If
            If Val(Vettore(2)) > 0 Then
                xTp.Sottoconto = Val(Vettore(2))
            End If
        End If

        xTp.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("ElencoRitenute.aspx")

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim xTp As New ClsRitenuta

        xTp.Codice = Txt_Codice.Text



        If xTp.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If


        xTp.Delete(Session("DC_TABELLE"))


        Response.Redirect("ElencoRitenute.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoRitenute.aspx")
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Aliquota')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsRitenuta

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsRitenuta

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"))

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub
End Class
