﻿
Partial Class GeneraleWeb_Parametri
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim LoadCau As New Cls_CausaleContabile

        LoadCau.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleEntrata)

        LoadCau.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleUscita)

        LoadCau.UpDateDropBox(Session("DC_TABELLE"), DD_Giroconto)

        LoadCau.UpDateCheckBox(Session("DC_TABELLE"), Chk_CausaliReversali)

        LoadCau.UpDateCheckBox(Session("DC_TABELLE"), Chk_CausaliMandati)


        Dim XPar As New Cls_ParametriGenerale


        XPar.LeggiParametri(Session("DC_GENERALE"))


        DD_CausaleEntrata.SelectedValue = XPar.CausaleEntrata
        DD_CausaleUscita.SelectedValue = XPar.CausaleUscita
        DD_Giroconto.SelectedValue = XPar.CausaleGiroconto


        Dim Appoggio(1000) As String
        Dim I As Integer = 0
        Dim x As Integer = 0


        If Not IsNothing(XPar.CausaliReversali) Then
            Appoggio = SplitWords(XPar.CausaliReversali)

            For I = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(I)) Then
                    For x = 0 To Chk_CausaliReversali.Items.Count - 1
                        If Chk_CausaliReversali.Items(x).Value = Appoggio(I) Then
                            Chk_CausaliReversali.Items(x).Selected = True
                        End If
                    Next
                End If
            Next
        End If

        If Not IsNothing(XPar.CausaliMandati) Then
            Appoggio = SplitWords(XPar.CausaliMandati)

            For I = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(I)) Then
                    For x = 0 To Chk_CausaliMandati.Items.Count - 1
                        If Chk_CausaliMandati.Items(x).Value = Appoggio(I) Then
                            Chk_CausaliMandati.Items(x).Selected = True
                        End If
                    Next
                End If
            Next
        End If

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim K As New Cls_ParametriGenerale



        K.LeggiParametri(Session("DC_GENERALE"))


        K.CausaleEntrata = DD_CausaleEntrata.SelectedValue
        K.CausaleUscita = DD_CausaleUscita.SelectedValue
        K.CausaleGiroconto = DD_Giroconto.SelectedValue


        Dim I As Integer
        Dim Appoggio As String = ""

        For I = 0 To Chk_CausaliMandati.Items.Count - 1
            If Chk_CausaliMandati.Items(I).Selected = True Then
                Appoggio = Appoggio & Chk_CausaliMandati.Items(I).Value & ","
            End If
        Next

        K.CausaliMandati = Appoggio
        Appoggio = ""

        For I = 0 To Chk_CausaliReversali.Items.Count - 1
            If Chk_CausaliReversali.Items(I).Selected = True Then
                Appoggio = Appoggio & Chk_CausaliReversali.Items(I).Value & ","
            End If
        Next

        K.CausaliReversali = Appoggio

        K.ModificaParametri(Session("DC_GENERALE"))


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
