﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO.Compression
Imports System.IO
Imports System.Xml
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports Ionic
Imports System.Net
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Security
Imports System.Security.Cryptography.Pkcs

Partial Class GeneraleWeb_ImportXML
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Function EstraiFileDaP7M(ByVal nomefile As String) As String
        Dim data As Byte()

        Using reader As BinaryReader = New BinaryReader(File.Open(nomefile, FileMode.Open))
            data = reader.ReadBytes(CInt(reader.BaseStream.Length))
        End Using


        Dim signedCms As New SignedCms()

        ' encodedMessage is the encoded message received from 
        ' the sender.
        signedCms.Decode(data)


        Dim vOut As String = System.Text.Encoding.UTF8.GetString(signedCms.ContentInfo.Content)


        Return vOut

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        EseguiJS()



        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If


        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Param.LeggiDati(Session("DC_TABELLE"))

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), Param.CausaleContabileDefault)

        lbl_Causali.Text = "<b>Fattura :</b> " & CausaleContabile.Descrizione & "<br/>"


        CausaleContabile.Leggi(Session("DC_TABELLE"), Param.CausaleContabileNCDefault)


        lbl_Causali.Text = lbl_Causali.Text & "<b>NC :</b> " & CausaleContabile.Descrizione & "<br/>"

        If Param.CausaleContabileDefaultAttivita2 = "" Then
            RB_Attivita2.Visible = False
        End If


        If Param.FatturazionePrivati = 0 Then
            Response.Redirect("Menu_Servizi.aspx")
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_RateoRisconto')!= null || appoggio.match('Txt_Costo')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataDal')!= null || appoggio.match('Txt_DataAl')!= null)  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Sub CreaFattura(ByVal CODICEFISCALE As String, ByVal PARTITAIVA As String, ByVal Fattura As String, ByVal DataRegistrazione As Date)
        Dim Cn As New OleDbConnection

        Cn.ConnectionString = Session("DC_GENERALE")
        Cn.Open()

        Dim LunghezzaTesta As Integer = 50
        Dim LunghezzaRiga As Integer = 50

        Try
            Dim CmdRead As New OleDbCommand

            CmdRead.CommandText = "Select top 1 * From Temp_MovimentiContabiliTesta"
            CmdRead.Connection = Cn
            Dim SizeReader As OleDbDataReader = CmdRead.ExecuteReader()
            If SizeReader.Read Then
                Dim dt As New System.Data.DataTable

                dt = SizeReader.GetSchemaTable()

                For Each riga In dt.Rows
                    If riga("ColumnName").ToString.ToUpper = "DESCRIZIONE" Then
                        LunghezzaTesta = riga("ColumnSize").ToString
                    End If
                Next
            End If
            SizeReader.Close()
        Catch ex As Exception

        End Try

        Try
            Dim CmdRead As New OleDbCommand

            CmdRead.CommandText = "Select top 1 * From Temp_MovimentiContabiliRiga"
            CmdRead.Connection = Cn
            Dim SizeReader As OleDbDataReader = CmdRead.ExecuteReader()
            If SizeReader.Read Then
                Dim dt As New System.Data.DataTable

                dt = SizeReader.GetSchemaTable()

                For Each riga In dt.Rows
                    If riga("ColumnName").ToString.ToUpper = "DESCRIZIONE" Then
                        LunghezzaRiga = riga("ColumnSize").ToString
                    End If
                Next
            End If
            SizeReader.Close()
        Catch ex As Exception

        End Try



        Dim DatiGenerali As String
        Dim DatiGeneraliDocumento As String


        DatiGenerali = ContenutoTAG(Fattura, "<DatiGenerali>")

        DatiGeneraliDocumento = ContenutoTAG(DatiGenerali, "<DatiGeneraliDocumento>")

        Dim TipoDocumento As String
        Dim Data As String
        Dim Numero As String
        Dim ImportoTotaleDocumento As String


        TipoDocumento = ContenutoTAG(DatiGeneraliDocumento, "<TipoDocumento>")
        Data = ContenutoTAG(DatiGeneraliDocumento, "<Data>")
        Numero = ContenutoTAG(DatiGeneraliDocumento, "<Numero>")

        ImportoTotaleDocumento = ""
        Try
            ImportoTotaleDocumento = ContenutoTAG(DatiGeneraliDocumento, "<ImportoTotaleDocumento>")
        Catch ex As Exception

        End Try

        Dim DatiPagamento As String = ""


        DatiPagamento = ContenutoTAG(Fattura, "<DatiPagamento>")

        Dim DettaglioPagamento As String = ""

        DettaglioPagamento = ContenutoTAG(Fattura, "<DatiPagamento>")

        Dim ImportoPagamento As String

        ImportoPagamento = ContenutoTAG(Fattura, "<ImportoPagamento>")


        If ImportoTotaleDocumento = "" Then
            ImportoTotaleDocumento = ImportoPagamento
        End If


        Dim Param As New Cls_DatiGenerali

        Param.LeggiDati(Session("DC_TABELLE"))


        Dim CliFor As New Cls_ClienteFornitore


        CliFor.CODICEDEBITORECREDITORE = 0
        CliFor.CodiceFiscale = CODICEFISCALE
        CliFor.PARTITAIVA = Val(PARTITAIVA)
        CliFor.CercaCFContiAttivi(Session("DC_OSPITE"), Session("DC_GENERALE"))




        Dim CausaleRetta As New Cls_CausaleContabile


        If CliFor.CausaleContabileFornitore <> "" Then
            CausaleRetta.Codice = CliFor.CausaleContabileFornitore
        Else

            If RB_Attivita1.Checked = True Then
                CausaleRetta.Codice = Param.CausaleContabileDefault
            Else
                CausaleRetta.Codice = Param.CausaleContabileDefaultAttivita2
            End If

        End If
        If TipoDocumento = "TD04" Then
            If RB_Attivita1.Checked = True Then
                CausaleRetta.Codice = Param.CausaleContabileNCDefault
            Else
                CausaleRetta.Codice = Param.CausaleContabileNCDefaultAttivita2
            End If
        End If

        CausaleRetta.Leggi(Session("DC_TABELLE"), CausaleRetta.Codice)

        If CausaleRetta.Descrizione = "" Then
            Exit Sub
        End If


        Dim TempNumeroRegistrazione As Long
        Dim TempDataRegistrazione As Date
        Dim TempDataDocumento As Date
        Dim TempANNOCOMPETENZA As Integer
        Dim TempMESECOMPETENZA As Integer
        Dim TempTipologia As String
        Dim TempModalitaPagamento As String
        Dim TempCausaleContabile As String
        Dim TempCentroServizio As String
        Dim TempCodicePagamento As String
        Dim TempNumeroProtocollo As Integer
        Dim TempNumeroDocumento As String
        Dim TempAnnoProtocollo As Integer
        Dim TempRegistroIva As Integer
        Dim Utente As String
        Dim TempDataAggiornamento As Date
        Dim MovCont As New Cls_MovimentoContabile




        TempNumeroRegistrazione = 1
        TempDataRegistrazione = DataRegistrazione
        TempDataDocumento = Data
        TempANNOCOMPETENZA = 0
        TempMESECOMPETENZA = 0
        TempTipologia = ""
        TempCausaleContabile = CausaleRetta.Codice
        TempCentroServizio = ""
        TempModalitaPagamento = ""

        TempNumeroProtocollo = 0
        TempNumeroDocumento = Numero
        TempAnnoProtocollo = Year(TempDataRegistrazione)
        TempRegistroIva = CausaleRetta.RegistroIVA
        TempDataAggiornamento = Now
        TempModalitaPagamento = CliFor.ModalitaPagamentoFornitore



        Dim MDel As New OleDbCommand
        MDel.Connection = Cn
        MDel.CommandText = "delete from Temp_MovimentiContabiliTesta "
        MDel.ExecuteNonQuery()

        Dim MDelRig As New OleDbCommand
        MDelRig.Connection = Cn
        MDelRig.CommandText = "delete from Temp_MovimentiContabiliRiga "
        MDelRig.ExecuteNonQuery()



        Dim M As New OleDbCommand
        M.Connection = Cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliTesta (NumeroRegistrazione,DataRegistrazione,DataDocumento,ANNOCOMPETENZA,MESECOMPETENZA,Tipologia,CausaleContabile,CentroServizio,CodicePagamento,NumeroProtocollo,NumeroDocumento,AnnoProtocollo,RegistroIva,ModalitaPagamento,Utente,DataAggiornamento,Descrizione) values " &
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'')"
        M.Parameters.AddWithValue("NumeroRegistrazione", TempNumeroRegistrazione)
        M.Parameters.AddWithValue("DataRegistrazione", TempDataRegistrazione)
        M.Parameters.AddWithValue("DataDocumento", TempDataDocumento)
        M.Parameters.AddWithValue("ANNOCOMPETENZA", TempANNOCOMPETENZA)
        M.Parameters.AddWithValue("MESECOMPETENZA", TempMESECOMPETENZA)
        M.Parameters.AddWithValue("Tipologia", TempTipologia)
        M.Parameters.AddWithValue("CausaleContabile", TempCausaleContabile)
        M.Parameters.AddWithValue("CentroServizio", TempCentroServizio)
        M.Parameters.AddWithValue("CodicePagamento", TempModalitaPagamento)
        M.Parameters.AddWithValue("NumeroProtocollo", TempNumeroProtocollo)
        M.Parameters.AddWithValue("NumeroDocumento", TempNumeroDocumento)
        M.Parameters.AddWithValue("AnnoProtocollo", TempAnnoProtocollo)
        M.Parameters.AddWithValue("RegistroIva", TempRegistroIva)
        M.Parameters.AddWithValue("TempModalitaPagamento", TempModalitaPagamento)
        M.Parameters.AddWithValue("Utente", "")
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()


        Dim TempRigaNumero As Long
        Dim TempRigaMastroPartita As Long
        Dim TempRigaContoPartita As Long
        Dim TempRigaSottoContoPartita As Double
        Dim TempRigaImporto As Double
        Dim TempRigaDescrizione As String
        Dim TempRigaTipo As String
        Dim TempRigaDareAvere As String
        Dim TempRigaSegno As String
        Dim TempRigaDaCausale As String
        Dim TempRigaUtente As String
        Dim TempRigaDataAggiornamento As Date
        Dim TempRigaCodIVA As String
        Dim TempRigaImponibile As Double
        Dim TempRigaQuantita As Double

        If ImportoTotaleDocumento = "" Then
            ImportoTotaleDocumento = 0
        End If


        TempRigaNumero = 1
        TempRigaMastroPartita = CliFor.MastroFornitore
        TempRigaContoPartita = CliFor.ContoFornitore
        TempRigaSottoContoPartita = CliFor.SottoContoFornitore
        TempRigaImporto = Modulo.MathRound(CDbl(ImportoTotaleDocumento), 2)
        TempRigaTipo = "CF"
        TempRigaDareAvere = CausaleRetta.Righe(0).DareAvere
        TempRigaSegno = "+"
        TempRigaDaCausale = 1
        TempRigaUtente = ""
        TempRigaDataAggiornamento = Now



        M.Parameters.Clear()
        M.Connection = Cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento) values " &
                               " (?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("Numero", TempRigaNumero)
        M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
        M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
        M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
        M.Parameters.AddWithValue("Importo", Modulo.MathRound(CDbl(TempRigaImporto), 2))
        M.Parameters.AddWithValue("Tipo", TempRigaTipo)
        M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
        M.Parameters.AddWithValue("Segno", TempRigaSegno)
        M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
        M.Parameters.AddWithValue("Utente", Utente)
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()



        Dim DatiBeniServizi As String = ""


        DatiBeniServizi = ContenutoTAG(Fattura, "<DatiBeniServizi>")

        If Chk_RaggruppaCosti.Checked = False Then
            Do While DatiBeniServizi.IndexOf("<DettaglioLinee>") >= 0
                Dim CodiceIVA As New Cls_IVA

                Dim DettaglioLinee As String = ""

                DettaglioLinee = ContenutoTAG(DatiBeniServizi, "<DettaglioLinee>")

                Dim Descrizione As String
                Dim Quantita As String
                Dim PrezzoTotale As String
                Dim AliquotaIVA As String
                Dim Natura As String
                Dim CodiceArticolo As String
                Dim CodiceTipo As String
                Dim CodiceValore As String
                Dim DescrizioneEstesa As String = ""

                AliquotaIVA = ContenutoTAG(DettaglioLinee, "<AliquotaIVA>")
                Descrizione = ContenutoTAG(DettaglioLinee, "<Descrizione>")
                DescrizioneEstesa = ContenutoTAG(DettaglioLinee, "<Descrizione>") & vbNewLine

                PrezzoTotale = ContenutoTAG(DettaglioLinee, "<PrezzoTotale>").Replace(".", ",")
                Natura = ContenutoTAG(DettaglioLinee, "<Natura>")
                Quantita = ContenutoTAG(DettaglioLinee, "<Quantita>").Replace(".", ",")
                Descrizione = ContenutoTAG(DettaglioLinee, "<Descrizione>")



                CodiceArticolo = ContenutoTAG(DettaglioLinee, "<CodiceArticolo>")
                CodiceTipo = ContenutoTAG(CodiceArticolo, "<CodiceArticolo>")
                CodiceValore = ContenutoTAG(CodiceArticolo, "<CodiceValore>")



                Dim MIVA As New Cls_FeAliquotaIVA

                MIVA.ALIQUOTA = CDbl(AliquotaIVA.Replace(".", ","))
                MIVA.NATURA = Natura
                MIVA.Leggi(Session("DC_TABELLE"))
                If IsNothing(MIVA.CODICE) Then
                    MIVA.CODICE = ""
                End If

                CodiceIVA.Codice = MIVA.CODICE
                CodiceIVA.Leggi(Session("DC_TABELLE"), CodiceIVA.Codice)

                TempRigaNumero = 1
                TempRigaMastroPartita = CausaleRetta.Righe(1).Mastro
                TempRigaContoPartita = CausaleRetta.Righe(1).Conto
                TempRigaSottoContoPartita = CausaleRetta.Righe(1).Sottoconto
                If CliFor.MastroCosto > 0 Then
                    TempRigaMastroPartita = CliFor.MastroCosto
                    TempRigaContoPartita = CliFor.ContoCosto
                    TempRigaSottoContoPartita = CliFor.SottocontoCosto
                End If

                Dim Tr As New Cls_FEARTICOLO


                Tr.CODICETIPO = CodiceTipo
                Tr.CODICEVALORE = CodiceValore
                Tr.Leggi(Session("DC_TABELLE"))
                If Tr.MASTRO > 0 Then
                    TempRigaMastroPartita = Tr.MASTRO
                    TempRigaContoPartita = Tr.CONTO
                    TempRigaSottoContoPartita = Tr.SOTTOCONTO
                End If

                TempRigaTipo = ""

                If PrezzoTotale.IndexOf("-") >= 0 Then
                    PrezzoTotale = Replace(PrezzoTotale, "-", "")
                    If CausaleRetta.Righe(1).DareAvere = "D" Then
                        TempRigaDareAvere = "A"
                    Else
                        TempRigaDareAvere = "D"
                    End If
                Else
                    TempRigaDareAvere = CausaleRetta.Righe(1).DareAvere
                End If


                TempRigaSegno = "+"
                TempRigaDaCausale = 2
                TempRigaUtente = ""
                TempRigaDataAggiornamento = Now
                TempRigaCodIVA = CodiceIVA.Codice
                If Quantita = "" Then
                    Quantita = 1
                End If
                TempRigaQuantita = Modulo.MathRound(CDbl(Quantita), 2)
                TempRigaImporto = Modulo.MathRound(CDbl(PrezzoTotale), 2)
                If Len(Descrizione) > LunghezzaRiga Then
                    Descrizione = Mid(Descrizione, 1, LunghezzaRiga)
                End If
                TempRigaDescrizione = Descrizione


                M.Parameters.Clear()
                M.Connection = Cn
                M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Quantita,Descrizione) values " &
                                       " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                M.Parameters.AddWithValue("Numero", TempRigaNumero)
                M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
                M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
                M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
                M.Parameters.AddWithValue("Importo", TempRigaImporto)
                M.Parameters.AddWithValue("Tipo", TempRigaTipo)
                M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
                M.Parameters.AddWithValue("Segno", TempRigaSegno)
                M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
                M.Parameters.AddWithValue("Utente", Utente)
                M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
                M.Parameters.AddWithValue("CodiceIVA", TempRigaCodIVA)
                M.Parameters.AddWithValue("Quantita", TempRigaQuantita)
                M.Parameters.AddWithValue("Descrizione", TempRigaDescrizione)
                M.ExecuteNonQuery()

                Try
                    M.Parameters.Clear()
                    M.Connection = Cn
                    M.CommandText = "UpDate Temp_MovimentiContabiliTesta Set Descrizione = Descrizione + ? Where NumeroRegistrazione= ?"
                    M.Parameters.AddWithValue("Descrizione", DescrizioneEstesa)
                    M.Parameters.AddWithValue("Numero", TempRigaNumero)
                    M.ExecuteNonQuery()
                Catch ex As Exception

                End Try



                DatiBeniServizi = DatiBeniServizi.Replace("<DettaglioLinee>" & DettaglioLinee & "</DettaglioLinee>", "")
            Loop
        Else
            Dim Descrizione(100) As String
            Dim Quantita(100) As Long
            Dim PrezzoTotale(100) As Double
            Dim AliquotaIVA(100) As String
            Dim Natura(100) As String
            Dim CodiceArticolo(100) As String
            Dim CodiceTipo(100) As String
            Dim CodiceValore(100) As String
            Dim TotaleCosto(100) As Double
            Dim DescrizioneEstesa(100) As String
            Dim Indice As Integer = 0

            Dim DescrizioneTesta As String = ""
            Do While DatiBeniServizi.IndexOf("<DettaglioLinee>") >= 0
                Dim DettaglioLinee As String = ""

                DettaglioLinee = ContenutoTAG(DatiBeniServizi, "<DettaglioLinee>")

                Dim I As Integer
                Dim VerificaIVA As String
                Dim VerificaNatura As String

                VerificaNatura = ContenutoTAG(DettaglioLinee, "<Natura>")
                VerificaIVA = ContenutoTAG(DettaglioLinee, "<AliquotaIVA>")

                If Chk_RaggruppaPerAliquota.Checked = True Then
                    For I = 0 To 100
                        If AliquotaIVA(I) = VerificaIVA And Natura(I) = VerificaNatura Then
                            Exit For
                        End If
                    Next
                Else
                    If Indice = 0 Then
                        I = 101
                    Else
                        I = 0
                    End If
                End If



                If I >= 100 Then
                    AliquotaIVA(Indice) = ContenutoTAG(DettaglioLinee, "<AliquotaIVA>")

                    DatiBeniServizi = DatiBeniServizi.Replace("<DettaglioLinee>" & DettaglioLinee & "</DettaglioLinee>", "")


                    If Descrizione(Indice) <> "" Then
                        Descrizione(Indice) = Descrizione(Indice) & vbNewLine
                    End If
                    Descrizione(Indice) = Descrizione(Indice) & ContenutoTAG(DettaglioLinee, "<Descrizione>")
                    DescrizioneEstesa(Indice) = DescrizioneEstesa(Indice) & ContenutoTAG(DettaglioLinee, "<Descrizione>") & vbNewLine
                    PrezzoTotale(Indice) = CDbl(ContenutoTAG(DettaglioLinee, "<PrezzoTotale>").Replace(".", ","))
                    Natura(Indice) = ContenutoTAG(DettaglioLinee, "<Natura>")
                    If ContenutoTAG(DettaglioLinee, "<Quantita>") <> "" Then
                        Quantita(Indice) = ContenutoTAG(DettaglioLinee, "<Quantita>").Replace(".", ",")
                    Else
                        Quantita(Indice) = Val(ContenutoTAG(DettaglioLinee, "<Quantita>").Replace(".", ","))
                    End If
                    'Descrizione = Descrizione & ContenutoTAG(DettaglioLinee, "<Descrizione>")
                    CodiceArticolo(Indice) = ContenutoTAG(DettaglioLinee, "<CodiceArticolo>")
                    CodiceTipo(Indice) = ContenutoTAG(CodiceArticolo(Indice), "<CodiceArticolo>")
                    CodiceValore(Indice) = ContenutoTAG(CodiceArticolo(Indice), "<CodiceValore>")
                    TotaleCosto(Indice) = TotaleCosto(Indice) + CDbl(PrezzoTotale(Indice))

                    Indice = Indice + 1
                Else
                    AliquotaIVA(I) = ContenutoTAG(DettaglioLinee, "<AliquotaIVA>")

                    DatiBeniServizi = DatiBeniServizi.Replace("<DettaglioLinee>" & DettaglioLinee & "</DettaglioLinee>", "")


                    If Descrizione(I) <> "" Then
                        Descrizione(I) = Descrizione(I) & vbNewLine
                    End If
                    Descrizione(I) = Descrizione(I) & ContenutoTAG(DettaglioLinee, "<Descrizione>")
                    DescrizioneEstesa(I) = DescrizioneEstesa(Indice) & ContenutoTAG(DettaglioLinee, "<Descrizione>") & vbNewLine
                    PrezzoTotale(I) = PrezzoTotale(I) + CDbl(ContenutoTAG(DettaglioLinee, "<PrezzoTotale>").Replace(".", ","))

                    Natura(I) = ContenutoTAG(DettaglioLinee, "<Natura>")

                    If ContenutoTAG(DettaglioLinee, "<Quantita>").Replace(".", ",") <> "" Then
                        Quantita(I) = Quantita(I) + ContenutoTAG(DettaglioLinee, "<Quantita>").Replace(".", ",")
                    Else
                        Quantita(I) = 0
                    End If
                    'Descrizione = Descrizione & ContenutoTAG(DettaglioLinee, "<Descrizione>")
                    CodiceArticolo(I) = ContenutoTAG(DettaglioLinee, "<CodiceArticolo>")
                    CodiceTipo(I) = ContenutoTAG(CodiceArticolo(Indice), "<CodiceArticolo>")
                    CodiceValore(I) = ContenutoTAG(CodiceArticolo(Indice), "<CodiceValore>")
                    TotaleCosto(I) = TotaleCosto(I) + CDbl(PrezzoTotale(Indice))

                End If

            Loop



            TempRigaNumero = 0
            For I = 0 To Indice - 1

                TempRigaNumero = 1
                TempRigaMastroPartita = CausaleRetta.Righe(1).Mastro
                TempRigaContoPartita = CausaleRetta.Righe(1).Conto
                TempRigaSottoContoPartita = CausaleRetta.Righe(1).Sottoconto
                If CliFor.MastroCosto > 0 Then
                    TempRigaMastroPartita = CliFor.MastroCosto
                    TempRigaContoPartita = CliFor.ContoCosto
                    TempRigaSottoContoPartita = CliFor.SottocontoCosto
                End If


                Dim Tr As New Cls_FEARTICOLO


                Tr.CODICETIPO = CodiceTipo(I)
                Tr.CODICEVALORE = CodiceValore(I)

                If Not IsNothing(CodiceTipo(I)) And Not IsNothing(CodiceValore(I)) Then
                    Tr.Leggi(Session("DC_TABELLE"))
                    If Tr.MASTRO > 0 Then
                        TempRigaMastroPartita = Tr.MASTRO
                        TempRigaContoPartita = Tr.CONTO
                        TempRigaSottoContoPartita = Tr.SOTTOCONTO
                    End If
                End If


                TempRigaTipo = ""


                TempRigaDareAvere = CausaleRetta.Righe(1).DareAvere

                TempRigaSegno = "+"
                TempRigaDaCausale = 2
                TempRigaUtente = ""
                TempRigaDataAggiornamento = Now



                Dim MIVA As New Cls_FeAliquotaIVA

                MIVA.ALIQUOTA = CDbl(AliquotaIVA(I).Replace(".", ","))
                MIVA.NATURA = Natura(I)
                MIVA.Leggi(Session("DC_TABELLE"))
                If IsNothing(MIVA.CODICE) Then
                    MIVA.CODICE = ""
                End If
                If Chk_RaggruppaPerAliquota.Checked = False Then
                    MIVA.CODICE = ""
                End If


                TempRigaCodIVA = MIVA.CODICE

                TempRigaQuantita = Modulo.MathRound(1, 2)
                TempRigaImporto = Modulo.MathRound(PrezzoTotale(I), 2)

                DescrizioneTesta = DescrizioneTesta & Descrizione(I)

                If Len(Descrizione(I)) > LunghezzaRiga Then
                    Descrizione(I) = Mid(Descrizione(I), 1, LunghezzaRiga)
                End If
                TempRigaDescrizione = Descrizione(I)


                M.Parameters.Clear()
                M.Connection = Cn
                M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Quantita,Descrizione) values " &
                                       " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                M.Parameters.AddWithValue("Numero", TempRigaNumero)
                M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
                M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
                M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
                M.Parameters.AddWithValue("Importo", TempRigaImporto)
                M.Parameters.AddWithValue("Tipo", TempRigaTipo)
                M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
                M.Parameters.AddWithValue("Segno", TempRigaSegno)
                M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
                M.Parameters.AddWithValue("Utente", Utente)
                M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
                M.Parameters.AddWithValue("CodiceIVA", TempRigaCodIVA)
                M.Parameters.AddWithValue("Quantita", TempRigaQuantita)
                M.Parameters.AddWithValue("Descrizione", TempRigaDescrizione)
                M.ExecuteNonQuery()


            Next


            If Len(DescrizioneTesta) > LunghezzaTesta Then
                DescrizioneTesta = Mid(DescrizioneTesta, 1, LunghezzaTesta - 1)
            End If

            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "UpDate Temp_MovimentiContabiliTesta Set Descrizione = ?  Where NumeroRegistrazione= ?"
            M.Parameters.AddWithValue("Descrizione", DescrizioneTesta)
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.ExecuteNonQuery()
        End If



        DatiBeniServizi = ContenutoTAG(Fattura, "<DatiBeniServizi>")


        Do While DatiBeniServizi.IndexOf("<DatiRiepilogo>") >= 0

            Dim AliquotaIVA As String
            Dim ImponibileImporto As String
            Dim EsigibilitaIVA As String
            Dim Imposta As String
            Dim Natura As String
            Dim TempRigaDetraibile As String
            Dim CodiceIVA As New Cls_IVA

            Dim DatiRiepilogo As String = ""

            DatiRiepilogo = ContenutoTAG(DatiBeniServizi, "<DatiRiepilogo>")

            Imposta = ContenutoTAG(DatiRiepilogo, "<Imposta>").Replace(".", ",")
            AliquotaIVA = ContenutoTAG(DatiRiepilogo, "<AliquotaIVA>")
            ImponibileImporto = ContenutoTAG(DatiRiepilogo, "<ImponibileImporto>").Replace(".", ",")
            EsigibilitaIVA = ContenutoTAG(DatiRiepilogo, "<EsigibilitaIVA>")
            Natura = ContenutoTAG(DatiRiepilogo, "<Natura>")


            Dim MIVA As New Cls_FeAliquotaIVA

            MIVA.ALIQUOTA = CDbl(AliquotaIVA.Replace(".", ","))
            MIVA.NATURA = Natura
            MIVA.Leggi(Session("DC_TABELLE"))
            If IsNothing(MIVA.CODICE) Then
                MIVA.CODICE = ""
            End If
            CodiceIVA.Codice = MIVA.CODICE
            CodiceIVA.Leggi(Session("DC_TABELLE"), CodiceIVA.Codice)


            TempRigaNumero = 1
            TempRigaMastroPartita = CausaleRetta.Righe(2).Mastro
            TempRigaContoPartita = CausaleRetta.Righe(2).Conto
            TempRigaSottoContoPartita = CausaleRetta.Righe(2).Sottoconto
            TempRigaDetraibile = CausaleRetta.Detraibilita
            If CliFor.DetrabilitaFornitore <> "" Then
                TempRigaDetraibile = CliFor.DetrabilitaFornitore
            End If
            TempRigaTipo = "IV"

            If ImponibileImporto.IndexOf("-") >= 0 Then
                ImponibileImporto = Replace(ImponibileImporto, "-", "")
                Imposta = Replace(Imposta, "-", "")
                If CausaleRetta.Righe(2).DareAvere = "D" Then
                    TempRigaDareAvere = "A"
                Else
                    TempRigaDareAvere = "D"
                End If
            Else
                TempRigaDareAvere = CausaleRetta.Righe(2).DareAvere
            End If


            TempRigaSegno = "+"
            TempRigaDaCausale = 3
            TempRigaUtente = ""
            TempRigaDataAggiornamento = Now
            TempRigaCodIVA = CodiceIVA.Codice
            TempRigaImponibile = Modulo.MathRound(CDbl(ImponibileImporto), 2)
            TempRigaImporto = Modulo.MathRound(CDbl(Imposta), 2)


            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Imponibile,Detraibile) values " &
                                   " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
            M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
            M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
            M.Parameters.AddWithValue("Importo", TempRigaImporto)
            M.Parameters.AddWithValue("Tipo", TempRigaTipo)
            M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
            M.Parameters.AddWithValue("Segno", TempRigaSegno)
            M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
            M.Parameters.AddWithValue("Utente", Utente)
            M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
            M.Parameters.AddWithValue("CodiceIVA", TempRigaCodIVA)
            M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
            M.Parameters.AddWithValue("Detraibile", TempRigaDetraibile)
            M.ExecuteNonQuery()



            DatiBeniServizi = DatiBeniServizi.Replace("<DatiRiepilogo>" & DatiRiepilogo & "</DatiRiepilogo>", "")
        Loop



        Cn.Close()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click



        Dim nomefileexcel As String

        Dim NomeSocieta As String

        ' Dim NomeFileXML As String = Session("CLIENTE") & "_" & Format(Now, "yyyyMMddHHmmss") & "_" & IExp & "_" & Int(Rnd(1) * 10000)

        'Using writer As StreamWriter = New StreamWriter(Server.MapPath("..\Public\XML\" & NomeFileXML & ".xml"))
        Try
            Kill(Server.MapPath("..\Public\XML\") & Session("CLIENTE") & "_*.xml")
        Catch ex As Exception

        End Try


        If Txt_DataDal.Text = "" Then
            If FileUpload1.FileName = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
                Exit Sub
            End If

        Else
            ImportDaAgyo()
        End If


        Try

            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            Dim DatiGenerale As New Cls_DatiGenerali

            DatiGenerale.LeggiDati(Session("DC_TABELLE"))

            If DatiGenerale.CausaleContabileDefault = "" Then

            End If


            NomeSocieta = k.RagioneSociale

            Dim Appo As String

            Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")

            Dim Zippor As Boolean = False
            Dim NomeFile(1000) As String
            Dim Indice As Integer = 0


            If Txt_DataDal.Text = "" Then
                Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
                If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
                    MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
                End If
                If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip", FileAttribute.Directory) = "" Then
                    MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip")
                End If
                Try
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\*.*")
                Catch ex As Exception

                End Try


                If FileUpload1.FileName.ToUpper.IndexOf(".ZIP") >= 0 Then
                    FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".ZIP")
                    Dim Zip As New Ionic.Zip.ZipFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".ZIP")

                    Zip.ExtractAll(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\", Ionic.Zip.ExtractExistingFileAction.OverwriteSilently)

                    Zip.Dispose()
                    Zippor = True
                Else

                    If FileUpload1.FileName.ToUpper.IndexOf(".p7m".ToUpper) > 0 Then
                        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".XML.p7m")
                    Else
                        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".XML")
                    End If
                End If


                If Zippor = True Then

                    For Each foundFile As String In My.Computer.FileSystem.GetFiles(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\")

                        If foundFile.ToString.ToUpper.IndexOf(".ZIP") < 0 Then
                            NomeFile(Indice) = foundFile
                            Indice = Indice + 1
                        End If
                    Next
                Else
                    If FileUpload1.FileName.ToUpper.IndexOf(".p7m".ToUpper) > 0 Then
                        NomeFile(Indice) = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".XML.p7m"
                    Else
                        NomeFile(Indice) = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\ExcelImport" & Appoggio & ".XML"
                    End If
                    Indice = Indice + 1
                End If
            Else


                For Each foundFile As String In My.Computer.FileSystem.GetFiles(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\")

                    NomeFile(Indice) = foundFile
                    Indice = Indice + 1
                Next

                Dim x As Integer
                Dim y As Integer

                For x = 0 To Indice - 2

                    For y = x + 1 To Indice - 1
                        If NomeFile(x) > NomeFile(y) Then
                            Dim Swap As String

                            Swap = NomeFile(x)
                            NomeFile(x) = NomeFile(y)
                            NomeFile(y) = Swap
                        End If
                    Next
                Next


            End If




            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("Numero Documento", GetType(String)) ' 0
            Tabella.Columns.Add("Data Documento", GetType(String)) '1
            Tabella.Columns.Add("Tipo Documento", GetType(String)) ' 2
            Tabella.Columns.Add("Ragione Sociale", GetType(String)) '3
            Tabella.Columns.Add("Totale Documento", GetType(String)) '4
            Tabella.Columns.Add("XML", GetType(String)) '5
            Tabella.Columns.Add("CLIENTE", GetType(String)) '6
            Tabella.Columns.Add("DOCUMENTO", GetType(String)) '7

            Tabella.Columns.Add("PIVA", GetType(String)) '8
            Tabella.Columns.Add("CODICEFISCALE", GetType(String)) '9

            Tabella.Columns.Add("Denominazione", GetType(String)) '10
            Tabella.Columns.Add("Indirizzo", GetType(String)) '11
            Tabella.Columns.Add("CAP", GetType(String)) '12
            Tabella.Columns.Add("Comune", GetType(String)) '13
            Tabella.Columns.Add("receiptDate", GetType(String)) '14


            Dim IExp As Integer

            For IExp = 0 To Indice - 1
                Dim objStreamReader As StreamReader
                objStreamReader = File.OpenText(NomeFile(IExp))

                Dim Testo As String = ""
                Do While Not objStreamReader.EndOfStream
                    Testo = Testo & objStreamReader.ReadLine
                Loop
                objStreamReader.Close()

                If NomeFile(IExp).IndexOf(".p7m") > 0 Then

                    Testo = EstraiFileDaP7M(NomeFile(IExp))


                End If

                Dim CedentePrestatore As String
                Dim DatiAnagrafici As String
                Dim IdFiscaleIVA As String
                Dim PartitaIVA_CP As String
                Dim CodiceFiscale_CP As String
                Dim Denominazione As String = ""

                CedentePrestatore = ContenutoTAG(Testo, "<CedentePrestatore>")

                DatiAnagrafici = ContenutoTAG(CedentePrestatore, "<DatiAnagrafici>")


                If DatiAnagrafici.IndexOf("<Anagrafica>") >= 0 Then
                    Dim Anagrafica As String
                    Anagrafica = ContenutoTAG(DatiAnagrafici, "<Anagrafica>")
                    If Anagrafica.IndexOf("<Denominazione>") >= 0 Then
                        Denominazione = ContenutoTAG(Anagrafica, "<Denominazione>")
                    End If

                    If Anagrafica.IndexOf("<Cognome>") >= 0 Then
                        Denominazione = ContenutoTAG(Anagrafica, "<Cognome>") & " " & ContenutoTAG(Anagrafica, "<Nome>")
                    End If
                End If

                CodiceFiscale_CP = ""
                PartitaIVA_CP = ""

                If DatiAnagrafici.IndexOf("<IdFiscaleIVA>") >= 0 Then
                    IdFiscaleIVA = ContenutoTAG(DatiAnagrafici, "<IdFiscaleIVA>")

                    PartitaIVA_CP = ContenutoTAG(IdFiscaleIVA, "<IdCodice>")
                End If

                If DatiAnagrafici.IndexOf("<CodiceFiscale>") >= 0 Then
                    CodiceFiscale_CP = ContenutoTAG(DatiAnagrafici, "<CodiceFiscale>")
                End If


                'CessionarioCommittente
                Dim CessionarioCommittente As String = ""
                Dim CodiceFiscale As String = ""
                Dim PartitaIVA As String = ""


                CessionarioCommittente = ContenutoTAG(Testo, "<CessionarioCommittente>")

                DatiAnagrafici = ContenutoTAG(CessionarioCommittente, "<DatiAnagrafici>")

                PartitaIVA = ""
                CodiceFiscale = ""

                If DatiAnagrafici.IndexOf("<IdFiscaleIVA>") >= 0 Then
                    IdFiscaleIVA = ContenutoTAG(DatiAnagrafici, "<IdFiscaleIVA>")

                    PartitaIVA = ContenutoTAG(IdFiscaleIVA, "<IdCodice>")
                End If

                If DatiAnagrafici.IndexOf("<CodiceFiscale>") >= 0 Then
                    CodiceFiscale = ContenutoTAG(DatiAnagrafici, "<CodiceFiscale>")
                End If



                Dim Indirizzo As String = ""
                Dim CAP As String = ""
                Dim Comune As String = ""
                Dim Provincia As String = ""
                Dim Nazione As String = ""



                If CedentePrestatore.IndexOf("<Sede>") >= 0 Then
                    Dim Sede As String
                    Sede = ContenutoTAG(CedentePrestatore, "<Sede>")
                    If Sede.IndexOf("<Indirizzo>") >= 0 Then
                        Indirizzo = ContenutoTAG(Sede, "<Indirizzo>")
                    End If
                    If Sede.IndexOf("<CAP>") >= 0 Then
                        CAP = ContenutoTAG(Sede, "<CAP>")
                    End If
                    If Sede.IndexOf("<Comune>") >= 0 Then
                        Comune = ContenutoTAG(Sede, "<Comune>")
                    End If
                    If Sede.IndexOf("<Provincia>") >= 0 Then
                        Provincia = ContenutoTAG(Sede, "<Provincia>")
                    End If
                    If Sede.IndexOf("<Nazione>") >= 0 Then
                        Nazione = ContenutoTAG(Sede, "<Nazione>")
                    End If
                    If Sede.IndexOf("<PEC>") >= 0 Then
                        Nazione = ContenutoTAG(Sede, "<PEC>")
                    End If
                End If


                Testo = Testo.Replace("<FatturaElettronicaBody xmlns="""">", "<FatturaElettronicaBody>")

                If Testo.IndexOf("</FatturaElettronicaBody>") < 0 Then
                    Testo = ""
                End If
                Do While Testo.IndexOf("<FatturaElettronicaBody>") >= 0
                    Dim Fattura As String
                    Dim NumeroDocumento As String
                    Dim DataDocumento As String

                    Fattura = ContenutoTAG(Testo, "<FatturaElettronicaBody>")

                    Dim DatiGenerali As String
                    Dim DatiGeneraliDocumento As String


                    DatiGenerali = ContenutoTAG(Fattura, "<DatiGenerali>")


                    DatiGeneraliDocumento = ContenutoTAG(DatiGenerali, "<DatiGeneraliDocumento>")

                    Dim TipoDocumento As String
                    Dim Data As String
                    Dim Numero As String
                    Dim ImportoTotaleDocumento As String


                    TipoDocumento = ContenutoTAG(DatiGeneraliDocumento, "<TipoDocumento>")
                    Data = ContenutoTAG(DatiGeneraliDocumento, "<Data>")
                    Numero = ContenutoTAG(DatiGeneraliDocumento, "<Numero>")

                    ImportoTotaleDocumento = ""
                    Try
                        ImportoTotaleDocumento = ContenutoTAG(DatiGeneraliDocumento, "<ImportoTotaleDocumento>")
                    Catch ex As Exception

                    End Try

                    Dim DatiPagamento As String = ""


                    DatiPagamento = ContenutoTAG(Fattura, "<DatiPagamento>")

                    Dim DettaglioPagamento As String = ""

                    DettaglioPagamento = ContenutoTAG(Fattura, "<DatiPagamento>")

                    Dim ImportoPagamento As String

                    ImportoPagamento = ContenutoTAG(Fattura, "<ImportoPagamento>")




                    If ImportoTotaleDocumento = "" Then
                        ImportoTotaleDocumento = ImportoPagamento
                    End If

                    If ImportoTotaleDocumento = "" Then
                        ImportoTotaleDocumento = 0
                    End If

                    Try
                        ImportoTotaleDocumento = CDbl(ImportoTotaleDocumento)
                    Catch ex As Exception
                        ImportoTotaleDocumento = ImportoPagamento
                    End Try



                    Dim myriga As System.Data.DataRow = Tabella.NewRow()


                    Dim msC As New Cls_ClienteFornitore



                    myriga(0) = Numero
                    myriga(1) = Data
                    myriga(2) = TipoDocumento
                    myriga(3) = Denominazione.Replace(".", "").Replace("\", "").Replace("/", "").Replace("&", "").Replace("'", "")
                    myriga(4) = Modulo.MathRound(CDbl(ImportoTotaleDocumento) / 100, 2)


                    Dim NomeFileXML As String = Session("CLIENTE") & "_" & Format(Now, "yyyyMMddHHmmss") & "_" & IExp & "_" & Int(Rnd(1) * 10000)

                    Using writer As StreamWriter = New StreamWriter(Server.MapPath("..\Public\XML\" & NomeFileXML & ".xml"))
                        writer.Write(Testo)
                    End Using

                    myriga(5) = Server.MapPath("..\Public\XML\" & NomeFileXML & ".xml")




                    Dim CliFor As New Cls_ClienteFornitore

                    CliFor.CodiceFiscale = CodiceFiscale_CP
                    CliFor.PARTITAIVA = Val(PartitaIVA_CP)
                    CliFor.CercaCFContiAttivi(Session("DC_OSPITE"), Session("DC_GENERALE"))
                    If CliFor.Nome <> "" Then
                        myriga(6) = ""
                    Else
                        myriga(6) = "NO"
                    End If

                    If Data = "" Or Numero = "" Then
                        myriga(7) = "ERR"
                    Else
                        If DocumentoPresente(Session("DC_GENERALE"), Numero, Data, CliFor.MastroFornitore, CliFor.ContoFornitore, CliFor.SottoContoFornitore) Then
                            myriga(7) = "SI"
                        Else
                            myriga(7) = ""
                        End If
                    End If



                    myriga(8) = PartitaIVA_CP
                    myriga(9) = CodiceFiscale_CP

                    myriga(10) = Denominazione
                    myriga(11) = Indirizzo
                    myriga(12) = CAP
                    myriga(13) = Comune

                    If IsDate(Txt_DataDal.Text) Then
                        Dim Recupero As String = NomeFile(IExp).ToString.Replace(HostingEnvironment.ApplicationPhysicalPath() & "Public\" & NomeSocieta & "\Zip\", "")

                        Recupero = Recupero.Replace(Session("CLIENTE") & "_", "")

                        If Recupero.IndexOf("C:\") >= 0 Then
                            Recupero = Recupero.Replace("C:\i", "")
                            'controllo per errore che si presenta in EventViewer
                        End If

                        Dim AnnoData As String
                        Dim MeseData As String
                        Dim GiornoData As String
                        Dim OraData As String
                        Dim MinutiData As String

                        AnnoData = Mid(Recupero, 1, 4)
                        MeseData = Mid(Recupero, 5, 2)
                        GiornoData = Mid(Recupero, 7, 2)
                        OraData = Mid(Recupero, 10, 2)
                        MinutiData = Mid(Recupero, 12, 2)

                        Dim CabioZona As TimeZoneInfo
                        Dim DataO As Date


                        Try
                            DataO = DateSerial(Val(AnnoData), Val(MeseData), Val(GiornoData)).AddHours(Val(OraData)).AddMinutes(Val(MinutiData))
                        Catch ex As Exception

                        End Try



                        Dim DataAppoggio As String = ""

                        'DataAppoggio = Format(DateSerial(Mid(Recupero, 1, 4), Mid(Recupero, 5, 2), Mid(Recupero, 7, 2)), "dd/MM/yyyy")

                        DataAppoggio = Format(CabioZona.ConvertTimeFromUtc(DataO, TimeZoneInfo.Local), "dd/MM/yyyy")

                        myriga(14) = DataAppoggio
                    Else

                        myriga(14) = Format(Now, "dd/MM/yyyy")
                    End If


                    If Chk_EscludiImportati.Checked = True And myriga(7) = "SI" Then
                    Else
                        Tabella.Rows.Add(myriga)
                    End If


                    Testo = Testo.Replace("<FatturaElettronicaBody>" & Fattura & "</FatturaElettronicaBody>", "")
                Loop


            Next


            Grid.AutoGenerateColumns = False
            Grid.DataSource = Tabella
            Grid.DataBind()

            ViewState("TABELLA") = Tabella

            Dim i As Integer

            For i = 0 To Grid.Rows.Count - 1


                Dim Csk As String
                Dim Docum As String
                Dim TipoDoc As String

                Csk = Tabella.Rows(i).Item(6)
                Docum = Tabella.Rows(i).Item(7)
                TipoDoc = Tabella.Rows(i).Item(2)

                If Csk = "NO" Then
                    Dim StringaURL As String


                    'myriga(8) = PartitaIVA
                    'myriga(9) = CodiceFiscale

                    'myriga(10) = Denominazione
                    'myriga(11) = Indirizzo
                    'myriga(12) = CAP
                    'myriga(13) = Comune


                    StringaURL = "CHIAMANTE=IMPXML&RAGIONESOCIALE=" & Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(10).ToString.Replace("'", "").Replace(".", "")))
                    StringaURL = StringaURL & "&INDIRIZZO=" & Uri.EscapeUriString(Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(11))))
                    StringaURL = StringaURL & "&CAP=" & Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(12)))
                    StringaURL = StringaURL & "&COMUNE=" & Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(13)).ToString.Replace("'", "").Replace("&apos;", ""))
                    StringaURL = StringaURL & "&CODICEFISCALE=" & Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(9)))
                    StringaURL = StringaURL & "&PARTITAIVA=" & Uri.EscapeUriString(campodb(Tabella.Rows(i).Item(8)))
                    StringaURL = StringaURL.Replace("'", "")


                    Grid.Rows(i).Cells(5).Text = "<a href=""#"" onclick=""DialogBox('AnagraficaClientiFornitori.aspx?" & StringaURL & "');"" >" & Grid.Rows(i).Cells(5).Text & "</a>"
                End If

                If Docum = "SI" Then
                    Grid.Rows(i).BackColor = Drawing.Color.Aqua
                End If
                If Docum = "ERR" Then
                    Grid.Rows(i).BackColor = Drawing.Color.Red
                End If

                If TipoDoc <> "TD01" And TipoDoc <> "TD02" And TipoDoc <> "TD04" Then
                    Grid.Rows(i).BackColor = Drawing.Color.LightCoral
                End If

            Next
        Finally
            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\*.*")
            Catch ex As Exception

            End Try

        End Try

        TabContainer1.ActiveTabIndex = 0
    End Sub


    Private Function ContenutoTAG(ByVal XML As String, ByRef TAG As String) As String
        Dim PosTag As Integer
        Dim FineTag As Integer

        Try

            PosTag = XML.IndexOf(TAG) + TAG.Length + 1

            FineTag = XML.IndexOf(TAG.Replace("<", "</"))


            ContenutoTAG = Mid(XML, PosTag, FineTag - PosTag + 1)
        Catch ex As Exception
            ContenutoTAG = ""
        End Try

    End Function


    Public Function DocumentoPresente(ByVal StringaConnessione As String, ByVal NumeroDocumento As String, ByVal DataDoc As String, ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Double) As Boolean
        Dim cn As OleDbConnection
        Dim Data As Date

        Data = DataDoc
        DocumentoPresente = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand() '
        cmd.CommandText = ("Select * from MovimentiContabiliTesta  where NumeroDocumento = ? And DataDocumento = ? And  (Select count(*) from MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & " And SottocontoPartita = " & Sottoconto & ") > 0")
        cmd.Parameters.AddWithValue("@NumeroDocumento", NumeroDocumento)
        cmd.Parameters.AddWithValue("@DataDocumento", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DocumentoPresente = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("MENU") = "GENERALE" Then
            Response.Redirect("Menu_Generale.aspx")
        Else
            Response.Redirect("Menu_Servizi.aspx")
        End If
    End Sub


    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim ParametriGenerale As New Cls_ParametriGenerale

            If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
                Exit Sub
            End If

            Session("NumeroRegistrazione") = 0

            ParametriGenerale.Elaborazione(Session("DC_GENERALE"))


            Dim d As Integer


            Tabella = ViewState("TABELLA")

            d = Val(e.CommandArgument)


            If campodb(Tabella.Rows(d).Item(6)) <> "" Then
                Exit Sub
            End If

            Dim objStreamReader As StreamReader
            objStreamReader = File.OpenText(campodb(Tabella.Rows(d).Item(5)))
            Dim contenuto As String = objStreamReader.ReadToEnd()
            objStreamReader.Close()


            Dim DataRegistrazione As Date = Nothing

            Try
                DataRegistrazione = Tabella.Rows(d).Item(14)

            Catch ex As Exception

            End Try

            CreaFattura(campodb(Tabella.Rows(d).Item(9)), campodbN(Tabella.Rows(d).Item(8)), contenuto, DataRegistrazione)


            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenNewTab", "openInNewTab('documenti.aspx?NUMEROREGISTRAZIONETEMP=1');", True)

            ParametriGenerale.FineElaborazione(Session("DC_GENERALE"))
            'openInNewTab();
        End If
        If e.CommandName = "Richiama" Then
            Dim d As Integer


            Tabella = ViewState("TABELLA")

            d = Val(e.CommandArgument)


            If campodb(Tabella.Rows(d).Item(6)) <> "" Then
                Exit Sub
            End If

            Dim objStreamReader As StreamReader
            objStreamReader = File.OpenText(campodb(Tabella.Rows(d).Item(5)))
            Dim contenuto As String = objStreamReader.ReadToEnd()
            objStreamReader.Close()

            Session("FATTURAXML") = contenuto
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>DialogBoxx('FatturaElettronicaViewer.aspx');</script>", False)


            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/FatturaElettronicaViewer.aspx');  });", True)

        End If
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        If Request.Item("MENU") = "GENERALE" Then
            Response.Redirect("Menu_Generale.aspx")
        Else
            Response.Redirect("Menu_Servizi.aspx")
        End If
    End Sub



    Private Sub ImportDaAgyo()

        Dim k As New Cls_Login
        Dim NomeSocieta As String

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))



        NomeSocieta = k.RagioneSociale


        Dim Appo As String
        Dim Zippor As Boolean = False


        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip", FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip")
        End If
        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Zip\*.*")
        Catch ex As Exception

        End Try

        ImportChangeFrom()
    End Sub



    REM Invio Con Agyo 
    Private Function CreateWebRequestDownloadFiles() As HttpWebRequest
        Dim webRequest As HttpWebRequest

        Dim M As New Cls_DatiGenerali
        Dim EndPoint As String
        M.LeggiDati(Session("DC_TABELLE"))

        If Val(M.TestAgyo) = 0 Then
            EndPoint = "https://soap-b2bapi-b2bhub-test.agyo.io/B2BReadApi_v7/B2BReadApi.ws"
        Else
            EndPoint = "https://soap-b2bapi-b2bhub.agyo.io/B2BReadApi_v7/B2BReadApi.ws"
        End If

        webRequest = HttpWebRequest.Create(EndPoint)

        webRequest.Headers.Add("SOAP:Action")
        '   //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.ContentType = "application/soap+xml;charset=UTF-8;action=downloadFiles"
        webRequest.Accept = "text/xml"
        webRequest.Method = "POST"
        Return webRequest
    End Function


    Private Function CreateWebRequestlistChangesFrom() As HttpWebRequest
        Dim webRequest As HttpWebRequest

        Dim M As New Cls_DatiGenerali
        Dim EndPoint As String
        M.LeggiDati(Session("DC_TABELLE"))

        If Val(M.TestAgyo) = 0 Then
            EndPoint = "https://soap-b2bapi-b2bhub-test.agyo.io/B2BReadApi_v7/B2BReadApi.ws"
        Else
            EndPoint = "https://soap-b2bapi-b2bhub.agyo.io/B2BReadApi_v7/B2BReadApi.ws"
        End If
        webRequest = HttpWebRequest.Create(EndPoint)

        webRequest.Headers.Add("SOAP:Action")
        '   //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.ContentType = "application/soap+xml;charset=UTF-8;action=listChangesFrom"
        webRequest.Accept = "text/xml"
        webRequest.Method = "POST"
        Return webRequest
    End Function


    Public Function TimeToUnix(ByVal dteDate As Date) As String
        If dteDate.IsDaylightSavingTime = True Then
            dteDate = DateAdd(DateInterval.Hour, -1, dteDate)
        End If
        TimeToUnix = DateDiff(DateInterval.Second, #1/1/1970#, dteDate)
    End Function




    Private Sub ImportChangeFrom()
        Dim _WebRequest As HttpWebRequest = CreateWebRequestlistChangesFrom()


        Dim XmlDocument As New XmlDocument()
        Dim MyDatiGen As New Cls_DatiGenerali
        Dim MyDatiSoc As New Cls_DecodificaSocieta
        Dim DataFrom As Long = 0
        Dim DataDa As Date


        DataDa = Txt_DataDal.Text


        DataFrom = (DataDa - DateSerial(1970, 1, 1)).TotalMilliseconds



        MyDatiGen.LeggiDati(Session("DC_TABELLE"))
        MyDatiSoc.Leggi(Session("DC_TABELLE"))

        Dim MyXML As String = ""



        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & _
                "  <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://schema.read.b2b.hub.teamsystem.com/v7"">" & _
                "  <SOAP-ENV:Body>" & _
                "    <ns1:listChangesFrom_request>" & _
                "     <ns1:auth>" & _
                "      <ns1:id>" & MyDatiGen.IdAgyo & "</ns1:id>" & _
                "      <ns1:securityToken>" & MyDatiGen.TokenAgyo & "</ns1:securityToken>" & _
                "      <ns1:appName>VR140</ns1:appName>" & _
                "   </ns1:auth>" & _
                "<ns1:timestampFrom>" & DataFrom & "</ns1:timestampFrom>" & _
                "   <ns1:pagination>" & _
                "     <ns1:page>0</ns1:page>" & _
                "     <ns1:size>600</ns1:size>" & _
                "   </ns1:pagination>" & _
                "  </ns1:listChangesFrom_request>" & _
                " </SOAP-ENV:Body> " & _
                "</SOAP-ENV:Envelope>"

        XmlDocument.LoadXml(MyXML)


        Using MyStream As Stream = _WebRequest.GetRequestStream()

            XmlDocument.Save(MyStream)
        End Using

        Dim Progressivo As Integer = 0

        Using MyResponse As WebResponse = _WebRequest.GetResponse()

            Using MyReader As StreamReader = New StreamReader(MyResponse.GetResponseStream())

                Dim Appoggio As String
                Appoggio = MyReader.ReadToEnd()

                Do While Appoggio.ToUpper.IndexOf("<document>".ToUpper) > 0
                    Dim Documento As String
                    Dim InizioD As Integer
                    Dim FineD As Integer

                    InizioD = Appoggio.ToUpper.IndexOf("<document>".ToUpper) + 11


                    FineD = Appoggio.ToUpper.IndexOf("</document>".ToUpper)

                    Documento = Mid(Appoggio, InizioD, FineD - InizioD + 1)

                    If Documento.ToUpper.IndexOf("<active>false</active>".ToUpper) >= 0 Then
                        If Documento.IndexOf("<hubId") > 0 Then
                            Dim Inizio As Integer
                            Dim Fine As Integer
                            Dim hubId As String

                            Inizio = Documento.ToUpper.IndexOf("<hubId>".ToUpper) + 8

                            Fine = Documento.ToUpper.IndexOf("</hubId>".ToUpper)

                            hubId = Mid(Documento, Inizio, Fine - Inizio + 1)


                            Dim receiptDate As String

                            Inizio = Documento.ToUpper.IndexOf("<receiptDate>".ToUpper) + Len("<receiptDate>") + 1

                            Fine = Documento.ToUpper.IndexOf("</receiptDate>".ToUpper)

                            receiptDate = Mid(Documento, Inizio, Fine - Inizio + 1)


                            receiptDate = receiptDate.Replace("-", "").Replace(":", "").Replace(".", "")


                            Progressivo = Progressivo + 1
                            DownloadDocument(hubId, receiptDate)

                        End If

                    End If

                    Appoggio = Appoggio.Replace("<document>" & Documento & "</document>", "")

                Loop
            End Using
        End Using


    End Sub


    Private Sub DownloadDocument(ByVal HubID As String, ByVal Progressivo As String)
        Dim _WebRequest As HttpWebRequest = CreateWebRequestDownloadFiles()


        Dim XmlDocument As New XmlDocument()
        Dim MyDatiGen As New Cls_DatiGenerali
        Dim MyDatiSoc As New Cls_DecodificaSocieta

        MyDatiGen.LeggiDati(Session("DC_TABELLE"))
        MyDatiSoc.Leggi(Session("DC_TABELLE"))

        Dim MyXML As String = ""



        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & _
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://schema.read.b2b.hub.teamsystem.com/v7"">" & _
                 "<SOAP-ENV:Body>" & _
                 "<ns1:downloadFiles_request>" & _
                 "<ns1:auth>" & _
                 "<ns1:id>" & MyDatiGen.IdAgyo & "</ns1:id>" & _
                 "<ns1:securityToken>" & MyDatiGen.TokenAgyo & "</ns1:securityToken>" & _
                 "<ns1:appName>VR140</ns1:appName>" & _
                 "</ns1:auth>" & _
                 "<ns1:filter>" & _
                 "<ns1:identifiers>" & _
                 "<ns1:hubId>" & HubID & "</ns1:hubId>" & _
                 "</ns1:identifiers>" & _
                 "</ns1:filter>" & _
                 "<ns1:pagination>" & _
                 "<ns1:page>0</ns1:page>" & _
                 "<ns1:size>10</ns1:size>" & _
                 "</ns1:pagination>" & _
                 "</ns1:downloadFiles_request>" & _
                 "</SOAP-ENV:Body>" & _
                 "</SOAP-ENV:Envelope>"

        XmlDocument.LoadXml(MyXML)


        Using MyStream As Stream = _WebRequest.GetRequestStream()

            XmlDocument.Save(MyStream)
        End Using

        Dim DatiGenerale As New Cls_DatiGenerali

        DatiGenerale.LeggiDati(Session("DC_TABELLE"))

        If DatiGenerale.CausaleContabileDefault = "" Then

        End If


        Dim k As New Cls_Login
        Dim NomeSocieta As String

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))



        NomeSocieta = k.RagioneSociale



        Using MyResponse As WebResponse = _WebRequest.GetResponse()

            Using MyReader As StreamReader = New StreamReader(MyResponse.GetResponseStream())

                Dim Appoggio As String
                Appoggio = MyReader.ReadToEnd()

                Dim Inizio As Integer
                Dim Fine As Integer
                Dim XML As String

                Inizio = Appoggio.IndexOf("<?xml version=""1.0"" encoding=""UTF-8""?>")

                Fine = Appoggio.IndexOf("</ns3:FatturaElettronica>") + Len("</ns3:FatturaElettronica>")

                XML = Mid(Appoggio, Inizio, Fine - Inizio + 1)



                Dim FileClienti As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "Public\" & NomeSocieta & "\Zip\" & Progressivo & HubID & ".xml")
                FileClienti.Write(XML)
                FileClienti.Close()

            End Using
        End Using
    End Sub

    Protected Sub RB_Attivita1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Attivita1.CheckedChanged
        Dim DatiGenerali As New Cls_DatiGenerali


        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), DatiGenerali.CausaleContabileDefault)

        lbl_Causali.Text = "<b>Fattura :</b> " & CausaleContabile.Descrizione & "<br/>"


        CausaleContabile.Leggi(Session("DC_TABELLE"), DatiGenerali.CausaleContabileNCDefault)


        lbl_Causali.Text = lbl_Causali.Text & "<b>NC :</b> " & CausaleContabile.Descrizione & "<br/>"
    End Sub

    Protected Sub RB_Attivita2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Attivita2.CheckedChanged
        Dim DatiGenerali As New Cls_DatiGenerali


        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), DatiGenerali.CausaleContabileDefaultAttivita2)

        lbl_Causali.Text = "<b>Fattura :</b> " & CausaleContabile.Descrizione & "<br/>"


        CausaleContabile.Leggi(Session("DC_TABELLE"), DatiGenerali.CausaleContabileNCDefaultAttivita2)


        lbl_Causali.Text = lbl_Causali.Text & "<b>NC :</b> " & CausaleContabile.Descrizione & "<br/>"
    End Sub
End Class
