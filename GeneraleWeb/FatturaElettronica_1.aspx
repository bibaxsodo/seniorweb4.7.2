﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_FatturaElettronica_1" CodeFile="FatturaElettronica_1.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Fatturazione Elettronica</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script type="text/javascript">                 

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export - Fatturazione Elettronica</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Visualizza" runat="server" Height="38px" ImageUrl="~/images/arrow_right.png" Width="38px" ToolTip="Avanti" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="../Ospitiweb/Menu_Strumenti.aspx" style="border-width: 0px;">
                            <img src="../images/Home.jpg" alt="Menù" /></a><br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Fatturazione Elettronica" ID="Tab_Anagrafica">
                                <HeaderTemplate>Fatturazione Elettronica</HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Usa PIVA e CF  :</label>
                                    <asp:CheckBox ID="Chk_UsaPIVACF" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Conto Partita :</label>
                                    <asp:CheckBox ID="Chk_IndicaMastroPartita" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Centro Servizio :</label>
                                    <asp:CheckBox ID="ChkCentroServizio" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Periodo In Desc. :</label>
                                    <asp:CheckBox ID="Chk_Competenza" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Usa PIVA Cliente :</label>
                                    <asp:CheckBox ID="Chk_UsaAnchePIVA" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Indica Bollo Virtuale :</label>
                                    <asp:CheckBox ID="Chk_BolloVirtuale" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Solo Iniziali :</label>
                                    <asp:CheckBox ID="Chk_SoloIniziali" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">N.Doc XX000 :</label>
                                    <asp:CheckBox ID="Chk_TipoNumero" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Giorni in Descrizione :</label>
                                    <asp:CheckBox ID="Chk_GiorniInDescrizione" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Ragruppa Ricavi :</label>
                                    <asp:CheckBox ID="Chk_RaggruppaRicavi" runat="server" Text="" /><br />
                                    <br />
                                    <label class="LabelCampo">Note Fattura :</label>
                                    <asp:CheckBox ID="Chk_NoteFattura" runat="server" Text="" /><br />

                                    <br />
                                    <label class="LabelCampo">Tipo Retta :</label>
                                    <asp:CheckBox ID="Chk_TipoRetta" runat="server" Text="" /><br />

                                    <br />

                                    <label class="LabelCampo">Tipo :</label>
                                    &nbsp;&nbsp;<asp:RadioButton ID="RB_Convezione" BorderWidth="0px"
                                        BorderColor="White" AutoPostBack="True" runat="server" GroupName="NumOB"
                                        Text="Convezione" Font-Names="Arial" />
                                    <asp:RadioButton ID="RB_Contratto" BorderWidth="0px" BorderColor="White"
                                        AutoPostBack="True" runat="server" GroupName="NumOB" Text="Contratto"
                                        Font-Names="Arial" />
                                    <asp:RadioButton ID="RB_Ordine" BorderWidth="0px" BorderColor="White"
                                        AutoPostBack="True" runat="server" GroupName="NumOB" Text="Ordine"
                                        Font-Names="Arial" />
                                    <asp:RadioButton ID="RB_NonIndicato" BorderWidth="0px" BorderColor="White"
                                        AutoPostBack="True" runat="server" GroupName="NumOB" Text="NonIndicato"
                                        Font-Names="Arial" /><br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="lbl_label" runat="server"></asp:Label></label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" Visible="true" /><br />
                                    <br />
                                    <label class="LabelCampo">Progressivo :</label>
                                    <asp:TextBox ID="Txt_Progressivo" runat="server" Width="100px" MaxLength="5"></asp:TextBox><br />
                                    <br />
                                    <hr />
                                    <br />
                                    <label class="LabelCampo">Riferimento Numero Linee :</label>
                                    <asp:TextBox ID="Txt_RiferimentoNumeroLinea" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">ID Documento:</label>
                                    <asp:TextBox ID="Txt_IDDocumento" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Data  :</label>
                                    <asp:TextBox ID="Txt_Data" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Numero Item :</label>
                                    <asp:TextBox ID="Txt_NumeroItem" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Comessa Convenzione:</label>
                                    <asp:TextBox ID="Txt_CodiceComeessa" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Cup :</label>
                                    <asp:TextBox ID="TxT_Cup" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Cin :</label>
                                    <asp:TextBox ID="Txt_CodiceCIn" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale :</label>
                                    <asp:TextBox ID="Txt_Causale" runat="server" Width="400px" MaxLength="50"></asp:TextBox><br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

