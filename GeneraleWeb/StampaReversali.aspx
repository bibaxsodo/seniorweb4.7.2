﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_StampaReversali" CodeFile="StampaReversali.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Stampa Reversali</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }


        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Stampa - Reversali</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="Img_Stampa" runat="server" Width="38px" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </td>
                </tr>


                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="ImgMenu" ImageUrl="images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Stampa Bilancio" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Stampa Reversali
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />

                                    <label class="LabelCampo">Dalla Data : </label>
                                    <asp:TextBox ID="Txt_DallaData" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Alla Data : </label>
                                    <asp:TextBox ID="Txt_AllaData" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Reversale : </label>
                                    <asp:TextBox ID="Txt_DataReversale" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Solo non generate :</label>
                                    <asp:CheckBox ID="Chk_SoloNonGenerate" runat="server" />
                                    <br />
                                    <br />

                                    <asp:Button ID="Btn_Aggiorna" runat="server" Text="Aggiorna" />
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Check">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Seleziona" runat="server" Checked='<%# Eval("Seleziona") %>' /></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NumeroRegistrazione"
                                                HeaderText="Numero Registrazione">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DataRegistrazione" HeaderText="Data Registrazione" />
                                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
                                            <asp:BoundField DataField="NumeroReversale" HeaderText="Numero Reversale" />
                                            <asp:BoundField DataField="DataReversale" HeaderText="Data Reversale" />
                                            <asp:BoundField DataField="CausaleContabile" HeaderText="Causale Contabile" />
                                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">

                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>

                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <HeaderStyle BackColor="#58A4D5" Font-Bold="True" ForeColor="Black" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>

                                    <br />
                                    <br />

                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                                        AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <div id="blur">&nbsp;</div>
                                            <div id="Div1">&nbsp;</div>
                                            <div id="pippo" class="wait">
                                                <br />
                                                <img height="30px" src="images/loading.gif"><br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>

                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
