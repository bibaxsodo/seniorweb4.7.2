﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="mastrino" CodeFile="mastrino.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Stampa Mastrino</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });



        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

</script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Stampa - Mastrino</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">

                        <asp:ImageButton ID="ImgMenu" ImageUrl="images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Stampa Bilancio" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Stampa Mastrino
         
                  

         
                  
                  
                                </HeaderTemplate>

                                <ContentTemplate>




                                    <label class="LabelCampo">Dal Conto:</label>
                                    <asp:TextBox ID="Txt_SottocontoDal" class="Sottoconto" CssClass="MyAutoComplete" runat="server"
                                        Width="488px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Al Conto:</label>
                                    <asp:TextBox ID="Txt_SottocontoAl" CssClass="MyAutoComplete" runat="server" Width="489px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Dal:</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Al:</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">Tipo Estrazione:</label>
                                    <asp:CheckBox ID="Chk_ImportoZERO" runat="server" />
                                    Importo a Zero
             <asp:CheckBox ID="Chk_ImponibileZERO" runat="server" />
                                    Imponibile a Zero
             <asp:CheckBox ID="Chk_saldoDivZero" runat="server" />
                                    Saldo diverso da Zero
             <asp:CheckBox ID="Chk_SaldiAnnoPrecedenti" runat="server" />
                                    Saldi anno precedenti<br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Stampa Saldo :</label>
                                    <asp:CheckBox ID="Chk_StampaSaldi" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione Riga :</label>
                                    <asp:CheckBox ID="Chk_DescrizioneRiga" runat="server" Text=""></asp:CheckBox>(Usa solo Descrizione Riga se presente)
        <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="lblStruttura" runat="server" Visible="false" Text="Struttura :"></asp:Label></label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true" Visible="false"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="lblCentroservizio" runat="server" Visible="false" Text="Centro Servizio :"></asp:Label></label>
                                    <asp:DropDownList ID="DD_CServ" runat="server" Width="354px" Visible="false"></asp:DropDownList><br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="296px"></asp:Label>
                                    <br />
                                    <br />



                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                            </asp:Timer>
                                            <asp:Label ID="Lbl_Waiting" runat="server"></asp:Label>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>




                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
