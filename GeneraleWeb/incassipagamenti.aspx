﻿<%@ Page Language="VB" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" Inherits="incassipagamenti" CodeFile="incassipagamenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Incassi e Pagamenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">                 
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

                if (event.keyCode == 119) {
                    __doPostBack("Btn_Pulisci", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 115) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 115) + "px"); }
        });

        function deletebox(cella) {
            $("div").remove("#RegistrazioneViewer");
            $("#RegistrazioneViewer").remove();
        }
        function apribox(NumeroRegistrazione, cella) {


            $.ajax({
                url: "RegistrazioneViewer.ashx?NumeroRegistrazione=" + NumeroRegistrazione,
                success: function (data, stato) {
                    $("body").append(data);
                    $("#RegistrazioneViewer").css('top', $("#" + cella).position().top + 30);
                    $("#RegistrazioneViewer").css('left', 300);
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });
        }

        $(document).ready(function () {
            $("#TabContainer1_Tab_Anagrafica_Txt_ClienteFornitore").blur(function () { setTimeout(function () { __doPostBack("Button1_Click", "0"); }, 200); });
        });

        function DialogBoxBigDocumenti(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');


            return false;

        }


        function DialogBoxBig(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');

            $("#IdCloseModal").click(function () {
                __doPostBack("BtnGiroconto", "0");
            });
            return false;

        }

        function DialogBox(Path) {

            var tot = 0;

            tot = document.body.offsetWidth - 300;

            REDIPS.dialog.show(tot, 520, '<iframe id="output" src="' + Path + '" height="510px" width="' + tot + 'px"></iframe>');

            $("#IdCloseModal").click(function () {
                __doPostBack("BtnGiroconto", "OnClick");
            });

            return false;

        }
        function CurrencyFormatted(number) {
            var meno = number.search("-");

            if (meno >= 0) {
                number = number.replace("-", "");
            }


            var numberStr = parseFloat(number).toFixed(2).toString();
            var numFormatDec = numberStr.slice(-2); /*decimal 00*/
            numberStr = numberStr.substring(0, numberStr.length - 3); /*cut last 3 strings*/
            var numFormat = new Array;
            while (numberStr.length > 3) {
                numFormat.unshift(numberStr.slice(-3));
                numberStr = numberStr.substring(0, numberStr.length - 3);
            }
            numFormat.unshift(numberStr);


            if (meno >= 0) {
                return '-' + numFormat.join('.') + ',' + numFormatDec; /*format 000.000.000,00 */
            }
            else {
                return numFormat.join('.') + ',' + numFormatDec; /*format 000.000.000,00 */
            }

        }

        function copiaimporto(Riga) {
            var table = document.getElementById('TabContainer1_Tab_Anagrafica_Grid');


            var els = document.getElementsByTagName("*");

            Riga++;
            Riga++;

            var StrRiga = '' + Riga;
            if (Riga < 10) StrRiga = '0' + Riga;

            var totale = 0;
            for (var i = 0; i < els.length; i++) {
                if (els[i].id) {
                    var appoggio = els[i].id;
                    if (appoggio.match("ctl" + StrRiga + "_Txt_ImportoPagato") != null) {

                        if ($(els[i]).val() == 0) {
                            $(els[i]).val(table.rows[Riga - 1].cells[4].innerHTML);
                        } else {
                            $(els[i]).val('0');
                        }

                    }



                    if (appoggio.match("_Txt_ImportoPagato") != null) {
                        if ($(els[i]).val() != null) {
                            var testo = $(els[i]).val();

                            testo = testo.replace('.', '');
                            testo = testo.replace(',', '.');
                            if (testo != '') {
                                totale = totale + parseFloat(testo);
                            }
                        }
                    }

                }

            }

            var sttole = '' + (Math.round(totale * 100) / 100);

            //sttole = sttole.replace('.',',');               

            $("#TabContainer1_Tab_Anagrafica_Txt_Importo").val(CurrencyFormatted(sttole));
            $("#TabContainer1_Tab_Anagrafica_Txt_ImportoCassa").val(CurrencyFormatted(sttole));

        }


        function copiaimportoScad(Riga) {
            var table = document.getElementById('TabContainer1_Tab_Anagrafica_Grid');


            var els = document.getElementsByTagName("*");

            Riga++;
            Riga++;

            var StrRiga = '' + Riga;
            if (Riga < 10) StrRiga = '0' + Riga;

            var totale = 0;
            for (var i = 0; i < els.length; i++) {
                if (els[i].id) {
                    var appoggio = els[i].id;
                    if (appoggio.match("ctl" + StrRiga + "_Txt_ImportoPagato") != null) {

                        if ($(els[i]).val() == 0) {
                            $(els[i]).val(table.rows[Riga - 1].cells[5].innerHTML);

                            var ChiusaCheck = "";
                            ChiusaCheck = appoggio.replace("_Txt_ImportoPagato", "_Chk_Chiusa");

                            if ($("#" + ChiusaCheck).val() != null) {
                                $("#" + ChiusaCheck).attr('checked', true);
                            }

                        } else {
                            $(els[i]).val('0');

                            var ChiusaCheck = "";
                            ChiusaCheck = appoggio.replace("_Txt_ImportoPagato", "_Chk_Chiusa");

                            if ($("#" + ChiusaCheck).val() != null) {
                                $("#" + ChiusaCheck).attr('checked', false);
                            }
                        }

                    }



                    if (appoggio.match("_Txt_ImportoPagato") != null) {
                        if ($(els[i]).val() != null) {
                            var testo = $(els[i]).val();

                            testo = testo.replace('.', '');
                            testo = testo.replace(',', '.');
                            if (testo != '') {
                                totale = totale + parseFloat(testo);
                            }

                        }
                    }
                }
            }

            var sttole = '' + (Math.round(totale * 100) / 100);

            //sttole = sttole.replace('.',',');               

            $("#TabContainer1_Tab_Anagrafica_Txt_Importo").val(CurrencyFormatted(sttole));
            $("#TabContainer1_Tab_Anagrafica_Txt_ImportoCassa").val(CurrencyFormatted(sttole));

        }

        function ChiudiConfermaModifica() {
            $("#ConfermaModifica").remove();
            $("#LblBox").empty();


        }
    </script>

    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .confermamodifca {
            -webkit-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            -moz-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            background-color: #82807d;
            text-align: center;
            color: White;
            z-index: 100;
        }

        .SeniorButton:hover {
            background-color: Silver;
            color: gray;
        }
    </style>
</head>
<body onclick="deletebox();">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="LblBox" runat="server" Text=""></asp:Label>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;" class="destraclasse"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Incasso Pagamenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                            <asp:ImageButton ID="Btn_Pulisci" runat="server" BackColor="Transparent" Height="38px" ImageUrl="~/images/PULISCI.JPG" Width="38px" ToolTip="Pulisci (F8)" Visible="false" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale" class="destraclasse">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" id="BOTTONEHOME" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <asp:ImageButton ID="BtnRicerca" ImageUrl="images/ricerca.jpg" Width="112px" class="Effetto" runat="server" /><br />
                        <br />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');">Vis. Piano Conti</a></label>
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('Pianoconti.aspx');">Gest. Piano Conti</a></label>
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('AnagraficaClientiFornitori.aspx');">Clienti Fornitori</a></label><br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Gestione Incassi e Pagamenti
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Numero :</label>
                                    <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="104px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Bolletta :</label>
                                    <asp:TextBox ID="Txt_DataBolletta" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Numero Bolletta :</label>
                                    <asp:TextBox ID="Txt_NumeroBolletta" autocomplete="off" runat="server" Width="104px" MaxLength="16"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Causale Contabile :</label>
                                    <asp:DropDownList ID="Dd_CausaleContabile" runat="server" class="chosen-select" Width="450px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_CentroSerivizio" runat="server" Visible="false" Text="Centro Servizio :"></asp:Label></label>
                                    <asp:DropDownList ID="DD_CSERV" class="chosen-selectcs" runat="server" Visible="false" Width="400px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione:</label>
                                    <asp:TextBox ID="Txt_Descrizione" MaxLength="250" autocomplete="off" runat="server" Height="72px" TextMode="MultiLine" Width="680px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Cliente/Fornitore :</label>
                                    <asp:TextBox ID="Txt_ClienteFornitore" CssClass="MyAutoComplete" runat="server" Width="400px"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Aggiorna" Font-Italic="true" Font-Size="X-Small" Font-Names="Arial" /><br />
                                    <br />
                                    <label class="LabelCampo">Importo :</label>
                                    <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" AutoPostBack="true" Width="104px"></asp:TextBox>
                                    <asp:Button ID="Btn_DistribuisciImporto" runat="server" Text="Distribuisci Importo" />
                                    <br />
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="Copia" HeaderText="" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Registrazione" HeaderText="Numero Registrazione" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="DataDocumento" HeaderText="Data Documento" />
                                            <asp:BoundField DataField="DataScadenza" HeaderText="Data Scadenza" />
                                            <asp:BoundField DataField="NumeroDocumento" HeaderText="Numero Documento" />
                                            <asp:BoundField DataField="ImportoDocumento" HeaderText="Importo Documento" ItemStyle-HorizontalAlign="Right" />
                                            <asp:TemplateField HeaderText="Importo Pagato" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Txt_ImportoPagato" Style="text-align: right;" autocomplete="off" onkeypress="ForceNumericInput(this, true, true)" onblur="this.value = formatNumber (this.value,2);  return true;" runat="server" Text='<%# Eval("ImportoPagato") %>'></asp:TextBox>
                                                    <asp:CheckBox ID="Chk_Chiusa" runat="server" Text="Chiusa" Visible='<%# Eval("Scadenzario") %>' Checked='<%# Eval("Chiusa") %>' /></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                    </asp:GridView>
                                    <br />
                                    <label class="LabelCampo">Cassa/Banca :</label>
                                    <asp:TextBox ID="Txt_SottocontoCassa" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
                                    &nbsp;Importo : 
        <asp:TextBox ID="Txt_ImportoCassa" Style="text-align: right;" autocomplete="off" runat="server" Width="104px" Enabled="False"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Abbuono :</label>
                                    <asp:TextBox ID="Txt_SottocontoAbbuono" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
                                    &nbsp;Importo :
                                    <asp:TextBox ID="Txt_ImportoAbbuono" Style="text-align: right;" autocomplete="off" onkeypress="ForceNumericInput(this, true, true)" runat="server" AutoPostBack="true" Width="104px"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Spese : </label>
                                    <asp:TextBox ID="Txt_Sottocontospese" CssClass="MyAutoComplete" runat="server"
                                        Width="420px"></asp:TextBox>
                                    &nbsp;Importo :
                                    <asp:TextBox ID="Txt_Importospese" autocomplete="off" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" AutoPostBack="true" Width="104px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Libero : </label>
                                    <asp:TextBox ID="Txt_SottocontoLibero" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>&nbsp;
            <asp:RadioButton ID="Rb_Dare" runat="server" GroupName="xDare" />Dare
            <asp:RadioButton ID="Rb_Avere" runat="server" GroupName="xDare" />Avere
        &nbsp;Importo :
                                    <asp:TextBox ID="Txt_ImportoLibero" Style="text-align: right;" autocomplete="off" onkeypress="ForceNumericInput(this, true, true)" runat="server" AutoPostBack="true" Width="104px"></asp:TextBox>

                                    <br />
                                    <br />


                                    <asp:Label ID="Lbl_errori" runat="server" ForeColor="Red" Width="424px"></asp:Label><br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Button ID="BtnGiroconto" runat="server" Text="" Visible="true"></asp:Button>

        <asp:Timer ID="Timer1" runat="server">
        </asp:Timer>
    </form>

</body>
</html>

