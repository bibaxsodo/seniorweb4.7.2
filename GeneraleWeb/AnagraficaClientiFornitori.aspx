﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="AnagraficaClientiFornitori" CodeFile="AnagraficaClientiFornitori.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Anagrafica Clienti Fornitori</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <style type="text/css">
        body {
            background: url(background.gif);
            margin: 0;
            padding: 0;
            font: normal 12px/14px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

        #header {
            background: url(title.png) center center no-repeat;
            height: 150px;
            width: 100%;
            margin: 45px 0 0 0;
            padding: 0;
        }

        #mainContent {
            background: #FFF;
            padding: 50px;
            width: 1255px;
            margin: 0 auto;
            overflow: hidden;
            position: relative;
            -moz-box-shadow: 0px 0px 8px #CCC; /* FF3.5+ */
            -webkit-box-shadow: 0px 0px 8px #CCC; /* Saf3.0+, Chrome */
            box-shadow: 0px 0px 8px #CCC; /* Opera 10.5, IE 9.0 */
            filter: /* IE6,IE7 e IE8 */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=0,strength=5) /* top */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=90,strength=5) /* left */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=180,strength=5) /* bottom */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=270,strength=5); /* right */
        }

        h1 {
        }

        h2 {
            padding-top: 50px;
        }

        p {
            margin-bottom: 500px;
        }

        ul.appo {
            background: #21F8F0;
            padding: 10px 35px;
            list-style-type: none;
            margin: 0 0 0 -310px;
            position: fixed;
            left: 50%;
            top: 0;
            font-size: 14px;
            font-weight: bold;
        }

        li.appo {
            float: left;
            display: inline;
            margin: 0 75px 0 0;
        }

        li a {
            float: left;
            color: #1A063D;
            text-decoration: none;
            text-shadow: 1px 1px 0px #FFFFFF;
        }

            li a.current {
                color: #1EDFD8;
                text-shadow: none;
            }

        .articleLink {
            position: absolute;
            text-decoration: none;
            color: #0099CC;
            right: 15px;
            top: 15px;
            font-weight: bold;
        }

        div.comandi {
            position: fixed;
            margin-right: 15px;
            margin-top: 500px;
            margin-left: 20px
        }

        .confermamodifca {
            -webkit-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            -moz-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            background-color: #82807d;
            text-align: center;
            color: White;
            z-index: 100;
        }

        .SeniorButton:hover {
            background-color: Silver;
            color: gray;
        }
    </style>

    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script type="text/javascript">        
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 76) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 76) + "px"); }
        });


        $(document).ready(function () {
            $('ul li:last').css('margin-right', '0');
            $("ul").corner('bottom', '18px');
            $('ul li a').click(function () {
                $('.current').removeClass('current');
                $(this).addClass('current');
            });


            $('a[href*=#]').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    && location.hostname == this.hostname) {
                    var $target = $(this.hash);
                    $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
                    if ($target.length) {
                        var targetOffset = $target.offset().top;
                        $('html,body').animate({ scrollTop: targetOffset }, 1000);
                        return false;
                    }
                }
            });

        });

    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>

        <asp:Label ID="LblBox" runat="server" Text=""></asp:Label>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <div class="comandi">
                <br />

            </div>


            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Clienti/Fornitori</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" Width="38px" ToolTip="Modifica / Inserisci (F2)" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">

                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None">
                            <xasp:TabPanel runat="server" HeaderText="Anagrafica" ID="Tab_Testata" Width="100%">
                                <HeaderTemplate>
                                    Anagrafica                        
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Codice :</label>
                                    <asp:TextBox ID="Txt_Codice" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Regione Sociale :</label>
                                    <asp:TextBox ID="Txt_RagioneSociale" runat="server" AutoPostBack="true" Width="488px" MaxLength="50"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipologia:</label>
                                    <asp:CheckBox ID="Chk_Cliente" Text="Cliente" AutoPostBack="true" runat="server" />
                                    <asp:CheckBox ID="Chk_Fornitore" Text="Fornitore" AutoPostBack="true" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" runat="server" Width="488px" MaxLength="50"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune" CssClass="MyAutoComplete" runat="server" Width="490px" MaxLength="30"></asp:TextBox>
                                    <asp:Button ID="Btn_DecodificaCap" runat="server" Text="Decodfica Cap" Font-Size="X-Small" Font-Names="Arial" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Cap :</label>
                                    <asp:TextBox ID="Txt_Cap" runat="server" Width="64px" MaxLength="5"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" runat="server" Width="488px" MaxLength="20"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Fax :</label>
                                    <asp:TextBox ID="Txt_Fax" runat="server" Width="488px" MaxLength="20"></asp:TextBox>
                                    <br />
                                    <br>
                                    <label class="LabelCampo">Cellulare :</label>
                                    <asp:TextBox ID="Txt_Cellulare" runat="server" Width="488px" MaxLength="100"></asp:TextBox>
                                    <br />
                                    <br>
                                    <label class="LabelCampo">Pec :</label>
                                    <asp:TextBox ID="Txt_Pec" runat="server" Width="488px" MaxLength="100"></asp:TextBox>
                                    <br />
                                    <br>
                                    <label class="LabelCampo">Mail :</label>
                                    <asp:TextBox ID="Txt_Mail" runat="server" Width="488px" MaxLength="100"></asp:TextBox>
                                    <br />
                                    <br>

                                    <label class="LabelCampo">
                                        Codice Fiscale :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" MaxLength="16" AutoPostBack="true" Width="248px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCF" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />


                                    <br></br>
                                    <label class="LabelCampo">
                                        Partita IVA :</label>
                                    <asp:TextBox ID="Txt_PartitaIVA" runat="server" MaxLength="11" AutoPostBack="true"
                                        onkeypress="return soloNumeri(event);" Width="248px"></asp:TextBox>

                                    <asp:Image ID="Img_VerificaPIVA" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />


                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 250px;">Tipologia<br />
                                                <asp:RadioButton ID="RB_PersonaFisica" runat="server" GroupName="Tipologia"></asp:RadioButton>


                                                Persona Fisica<br />
                                                <asp:RadioButton ID="RB_PersonaGiuridica" runat="server" GroupName="Tipologia"></asp:RadioButton>


                                                Persona Giuridica<br />
                                                <asp:RadioButton ID="RB_Altro" runat="server" GroupName="Tipologia"></asp:RadioButton>
                                                Altro<br />

                                                <asp:RadioButton ID="RB_PubblicaAmministrazione" runat="server" GroupName="Tipologia"></asp:RadioButton>
                                                Pubblica Amministrazione<br />
                                                <br />
                                            </td>
                                            <td style="width: 250px;">Professionista
            <br />
                                                <asp:RadioButton ID="RB_SiP" runat="server" GroupName="Profession"></asp:RadioButton>


                                                Si<br />
                                                <asp:RadioButton ID="Rb_NoP" runat="server" GroupName="Profession"></asp:RadioButton>


                                                No<br />
                                                <br />
                                                <br />
                                            </td>
                                            <td style="width: 250px;">Soggetto a Bollo<br />
                                                <asp:RadioButton ID="RB_SiSB" runat="server" GroupName="Soggetto"></asp:RadioButton>


                                                Si<br />
                                                <asp:RadioButton ID="RB_NoSB" runat="server" GroupName="Soggetto"></asp:RadioButton>


                                                No<br />
                                                <br />
                                                <br />
                                            </td>
                                            <td style="width: 250px;">
                                                <asp:Label ID="LBL_Economo" runat="server" Text="Economo"></asp:Label><br />
                                                <asp:RadioButton ID="RB_SiEN" runat="server" GroupName="Economo" Text="Si"></asp:RadioButton>


                                                <br />
                                                <asp:RadioButton ID="RB_NoEN" runat="server" GroupName="Economo" Text="No"></asp:RadioButton>


                                                <br />
                                                <br />
                                                <br />
                                            </td>
                                            <td style="width: 450px;">Dati Fatture<br />
                                                <asp:RadioButton ID="RB_NoEsterometro" runat="server" GroupName="DatiFatture"></asp:RadioButton>
                                                <asp:Label ID="Lbl_EsterometroNo" runat="server" Visible="true" Text="Dati Fatture"></asp:Label><br />

                                                <asp:RadioButton ID="RB_SiEsterometro" runat="server" GroupName="DatiFatture"></asp:RadioButton>
                                                <asp:Label ID="Lbl_EsterometroSi" runat="server" Visible="true" Text="Esterometro"></asp:Label><br />

                                                <asp:RadioButton ID="RB_NODATIFATTURE" runat="server" GroupName="DatiFatture"></asp:RadioButton>
                                                <asp:Label ID="Label1" runat="server" Visible="true" Text="No Dati Fatture/esterometro"></asp:Label><br />
                                            </td>
                                        </tr>
                                    </table>

                                </ContentTemplate>


                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Cliente" ID="Tab_Cliente" Width="1500px">
                                <ContentTemplate>





                                    <br />
                                    <label class="LabelCampo">
                                        Conto Cliente :</label>
                                    <asp:TextBox ID="Txt_ContoCliente" CssClass="MyAutoComplete" runat="server" Width="488px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        Conto Ricavo :</label>
                                    <asp:TextBox ID="Txt_ContoRicavo" CssClass="MyAutoComplete" runat="server" Width="488px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <br />
                                    <label class="LabelCampo">Descrizione:</label>
                                    <asp:TextBox ID="Txt_DescrizioneCli" runat="server" Width="488px" MaxLength="50"></asp:TextBox>
                                    <br />

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Incasso:</label>
                                    <asp:DropDownList ID="DD_IncassoCli" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Causale Contabile:</label>
                                    <asp:DropDownList ID="DD_CausaleContabileCli" runat="server">
                                    </asp:DropDownList>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">Dichiarazione :</label>
                                    <asp:TextBox ID="Txt_Dichiarazione" runat="server" Width="120px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Codice Univoco :</label>
                                    <asp:TextBox ID="Txt_CodiceUnivoco" runat="server" Width="120px"></asp:TextBox>
                                    <br />
                                    <br />

                                    </br>             
            
  <label class="LabelCampo">Iban :</label>
                                    <table>
                                        <tr>
                                            <td>Int</td>
                                            <td>Num.Con</td>
                                            <td>Cin</td>
                                            <td>ABI</td>
                                            <td>CAB</td>
                                            <td>CC Bancario</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Txt_IntCli" runat="server" MaxLength="2" Width="28px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_NumeroControlloCli" runat="server" MaxLength="2"
                                                    onkeypress="return soloNumeri(event);" Width="40px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CodiceCinCli" runat="server" MaxLength="1" Width="72px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CodiceABICli" runat="server" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" Width="88px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxT_CodiceCABCli" runat="server" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" Width="72px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CCBancarioCli" runat="server" MaxLength="12" Width="137px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <label class="LabelCampo">
                                        Iva Sospesa :</label>
                                    <asp:RadioButton ID="Rb_SISospCli" runat="server" GroupName="SospCli"></asp:RadioButton>
                                    SI&#160;
                                    <asp:RadioButton ID="Rb_NOSospCli" runat="server" GroupName="SospCli"></asp:RadioButton>
                                    NO&#160;
                                    <asp:RadioButton ID="Rb_NSSospCli" runat="server" GroupName="SospCli"></asp:RadioButton>
                                    Non specificato<br />
                                    <br />


                                    <label class="LabelCampo">IdDocumento :</label>
                                    <asp:TextBox ID="Txt_IdDocumento" runat="server" Width="104px" MaxLength="20"></asp:TextBox>
                                    Id Documento Data :
         <asp:TextBox ID="Txt_IdDocumentoData" runat="server" Width="104px" MaxLength="11"></asp:TextBox>
                                    Riferimento Amministrazione :
         <asp:TextBox ID="Txt_RiferimentoAmministrazione" runat="server" Width="104px" MaxLength="20"></asp:TextBox>
                                    Numero Fattura DDT :
         <asp:CheckBox ID="Chk_NFDtt" runat="server" Text=""></asp:CheckBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Note FE:</label>
                                    <asp:TextBox ID="Txt_NoteFe" runat="server" Width="304px" MaxLength="100"></asp:TextBox>
                                    Causale
         <asp:TextBox ID="Txt_Causale" runat="server" Width="204px" MaxLength="100"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipo XML. :</label>
                                    <asp:DropDownList ID="DD_EGO" runat="server">
                                        <asp:ListItem Value="C" Text="Contratto"></asp:ListItem>
                                        <asp:ListItem Value="Z" Text="Convenzione"></asp:ListItem>
                                        <asp:ListItem Value="O" Text="Ordine"></asp:ListItem>
                                        <asp:ListItem Value="N" Text="Non Indicato"></asp:ListItem>
                                    </asp:DropDownList>
                                    Numero Item :
       <asp:TextBox ID="Txt_NumItem" runat="server" Width="204px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Com. Convezione :</label>
                                    <asp:TextBox ID="Txt_CodiceCommessaConvezione" runat="server" Width="104px" MaxLength="50"></asp:TextBox>
                                    <br />
                                    <br />




                                    <asp:RadioButton ID="RB_SI" Visible="false" GroupName="AlCl" runat="server" />
                                    <asp:RadioButton ID="RB_No" Visible="false" GroupName="AlCl" runat="server" />
                                    <br />


                                    <label class="LabelCampo">&nbsp;</label>
                                    <asp:TextBox ID="Txt_ContoAllegato" runat="server" Width="488px" Visible="false"></asp:TextBox>
                                    <br />

                                    <br />
                                    <asp:TextBox ID="Txt_PostaleCli" runat="server" Visible="false" Width="188px" MaxLength="20"></asp:TextBox>
                                </ContentTemplate>




                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Fornitore" ID="Tab_Fornitore" Width="1500px">
                                <ContentTemplate>





                                    <br />
                                    <label class="LabelCampo">
                                        Conto Fornitore :</label>
                                    <asp:TextBox ID="Txt_ContoFornitore" CssClass="MyAutoComplete" runat="server" Width="488px"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        Conto Costo:</label>
                                    <asp:TextBox ID="Txt_ContoCosto" CssClass="MyAutoComplete" runat="server" Width="488px"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label class="LabelCampo">
                                        Descrizione :</label>
                                    <asp:TextBox ID="Txt_DescrizioneFornitore" runat="server" Width="488px" MaxLength="50"></asp:TextBox>
                                    <br />


                                    <asp:TextBox ID="Txt_PostaleFor" Visible="false" runat="server" Width="188px" MaxLength="20"></asp:TextBox>
                                    <br />

                                    <br />
                                    <label class="LabelCampo">Modalita Pag.:</label>
                                    <asp:DropDownList ID="DD_IncassoFor" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Causale Contabile:</label>
                                    <asp:DropDownList ID="DD_CausaleContabileFor" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Detraibilità :</label>
                                    <asp:DropDownList ID="DD_DetraiblitaFor" runat="server"></asp:DropDownList>


                                    <br />
                                    <br>Iban<table>
                                        <tr>
                                            <td>Int</td>
                                            <td>Num.Con</td>
                                            <td>Cin</td>
                                            <td>ABI</td>
                                            <td>CAB</td>
                                            <td>CC Bancario</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Txt_IntFor" runat="server" MaxLength="2" Width="28px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_NumeroControlloFor" runat="server" MaxLength="2"
                                                    onkeypress="return soloNumeri(event);" Width="40px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CodiceCinFor" runat="server" MaxLength="1" Width="72px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CodiceABIFor" runat="server" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" Width="88px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxT_CodiceCABFor" runat="server" MaxLength="5"
                                                    onkeypress="return soloNumeri(event);" Width="72px"></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_CCBancarioFor" runat="server" MaxLength="12" Width="125px"></asp:TextBox>

                                            </td>

                                        </tr>
                                    </table>


                                        <asp:RadioButton ID="RB_AllegoFornitoreSI" Visible="false" GroupName="AlClf" runat="server" />
                                        <asp:RadioButton ID="RB_AllegoFornitoreNo" Visible="false" GroupName="AlClf" runat="server" /><br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:TextBox ID="Txt_ContoAllegatoFornitore" runat="server" Visible="false" Width="488px"></asp:TextBox>
                                        <br />
                                        <br />


                                        <br>
                                            <label class="LabelCampo"></label>
                                            <asp:RadioButton ID="Rb_SISospFor" runat="server" Visible="false" GroupName="SospFor"></asp:RadioButton>
                                            <asp:RadioButton ID="Rb_NoSospFor" runat="server" Visible="false" GroupName="SospFor"></asp:RadioButton>
                                            <asp:RadioButton ID="Rb_NSSospFor" runat="server" Visible="false" GroupName="SospFor"></asp:RadioButton>
                                            <br />
                                            <br />

                                        </br>
                                    </br>



                                </ContentTemplate>
                            </xasp:TabPanel>


                            <xasp:TabPanel runat="server" HeaderText="Codici Cig" ID="TabPanel1" Width="1500px">
                                <ContentTemplate>

                                    <asp:GridView ID="Grd_Cig" runat="server" CellPadding="4" Height="60px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Data Inizio">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtDataInizio" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Data Fine">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtDataFine" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="Cig">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtCig" Width="200px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Importo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Descrizione">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtDescrizione" Width="350px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>



                                        </Columns>

                                        <FooterStyle BackColor="White" ForeColor="#023102" />

                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>


                                </ContentTemplate>
                            </xasp:TabPanel>




                        </xasp:TabContainer>

                        &nbsp;</td>
                </tr>
            </table>


        </div>

    </form>
</body>
</html>


