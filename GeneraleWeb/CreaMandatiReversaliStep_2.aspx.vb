﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class GeneraleWeb_CreaMandatiReversaliStep_2
    Inherits System.Web.UI.Page
    Dim TabellaRisultati As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub CaricaPagina()
        TabellaRisultati.Clear()
        TabellaRisultati.Columns.Clear()
        TabellaRisultati.Columns.Add("Copia", GetType(String))
        TabellaRisultati.Columns.Add("NumeroRegistrazione", GetType(Long))
        TabellaRisultati.Columns.Add("DataRegistrazione", GetType(String))
        TabellaRisultati.Columns.Add("ImportoDocumento", GetType(String))
        TabellaRisultati.Columns.Add("ImportoLegare", GetType(String))
        TabellaRisultati.Columns.Add("ImportoLegame", GetType(String))
        TabellaRisultati.Columns.Add("CausaleContabile", GetType(String))


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.Connection = cn

        Dim Condizione As String
        Condizione = ""

        If Txt_DataDal.Text.Trim <> "" And Txt_DataAl.Text.Trim <> "" Then
            If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = " Dataregistrazione >= ? And Dataregistrazione <= ?"
                Dim Txt_DataDalText As Date = Txt_DataDal.Text
                Dim Txt_DataAlText As Date = Txt_DataAl.Text

                cmd.Parameters.AddWithValue("@DATADAL", Txt_DataDalText)
                cmd.Parameters.AddWithValue("@DATAAL", Txt_DataAlText)
            End If
        End If

        If Val(Txt_NumeroRegistrazioneDal.Text) > 0 And Val(Txt_NumeroRegistrazioneAl.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = " NumeroRegistrazione >= ? And NumeroRegistrazione <= ?"
            cmd.Parameters.AddWithValue("@NUMERODAL", Txt_NumeroRegistrazioneDal.Text)
            cmd.Parameters.AddWithValue("@NUMEROAL", Txt_NumeroRegistrazioneAl.Text)

        End If
        If Txt_Sottoconto.Text.Trim <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Sottoconto.Text)
            If xVettore.Length > 1 Then
                Condizione = Condizione & " (Select count(*) From MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & Val(xVettore(0)) & _
                            " And ContoPartita = " & Val(xVettore(1)) & " And SottoContoPartita = " & Val(xVettore(2)) & " ) > 0"
            End If
        End If

        If Condizione = "" Then
            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            TabellaRisultati.Rows.Add(myriga)

            GridEstrazione.AutoGenerateColumns = False
            GridEstrazione.DataSource = TabellaRisultati
            GridEstrazione.DataBind()
            Exit Sub
        End If

        cmd.CommandText = ("select * from MOVIMENTICONTABILITESTA where " & Condizione & " And NumeroRegistrazione <> " & Val(Session("NumeroRegistrazione")))

        Dim numerorecord As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim LegDoc As New Cls_MovimentoContabile
            LegDoc.Leggi(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione"))


            Dim TotaleDocumento As Double
            TotaleDocumento = LegDoc.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

            Dim xLegame As New Cls_Legami

            If Math.Round(TotaleDocumento - xLegame.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), 2) > 0 Then
                Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
                myriga(1) = myPOSTreader.Item("NumeroRegistrazione")
                myriga(2) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
                Dim K As New Cls_CausaleContabile

                K.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CAUSALECONTABILE")))
                If K.TipoDocumento = "NC" Then
                    TotaleDocumento = TotaleDocumento * -1
                End If
                myriga(3) = Format(TotaleDocumento, "#,##0.00")

                myriga(4) = Format(TotaleDocumento - xLegame.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), "#,##0.00")
                myriga(5) = "0,00"
                Dim MyCausale As New Cls_CausaleContabile
                MyCausale.Leggi(Session("DC_TABELLE"), LegDoc.CausaleContabile)
                myriga(6) = MyCausale.Descrizione
                TabellaRisultati.Rows.Add(myriga)
                numerorecord = numerorecord + 1
                If numerorecord > 100 Then
                    Exit Do
                End If
            End If
        Loop
        myPOSTreader.Close()

        Session("GestioneLegamiEstrazioni") = TabellaRisultati
        GridEstrazione.AutoGenerateColumns = False
        GridEstrazione.DataSource = TabellaRisultati
        GridEstrazione.DataBind()

        For I = 0 To GridEstrazione.Rows.Count - 1

            GridEstrazione.Rows(I).Cells(0).Text = "<a href=""#"" onclick=""copiaimporto('" & I & "');""><img src=""images/copiaimporto.png"" height=""16"" width=""16"" alt=""digita importo""></a>" & GridEstrazione.Rows(I).Cells(0).Text

        Next

    End Sub
    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Ricerca.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal errata');", True)
                Exit Sub
            End If
        End If

        Call CaricaPagina()
        Call EseguiJS()
    End Sub

    Protected Sub GridEstrazione_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridEstrazione.RowCancelingEdit
        GridEstrazione.EditIndex = -1
        Call BindRisultati()
    End Sub

    Protected Sub GridEstrazione_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridEstrazione.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            TabellaRisultati = Session("GestioneLegamiEstrazioni")

            Dim LblImportoLegame As Label = DirectCast(e.Row.FindControl("LblImportoLegame"), Label)
            If Not IsNothing(LblImportoLegame) Then

            Else
                Dim TxtImportoLegame As TextBox = DirectCast(e.Row.FindControl("TxtImportoLegame"), TextBox)
                TxtImportoLegame.Text = TabellaRisultati.Rows(e.Row.RowIndex).Item(5).ToString
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImportoLegame.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImportoLegame.ClientID & "').val(),2); $('#" & TxtImportoLegame.ClientID & "').val(ap); }); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "JS_IMPORTOLEGAME", MyJs, True)
            End If
        End If
    End Sub

    Protected Sub GridEstrazione_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridEstrazione.RowEditing
        GridEstrazione.EditIndex = e.NewEditIndex
        Call BindRisultati()
    End Sub


    Protected Sub GridEstrazione_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridEstrazione.RowUpdating

    End Sub

    Public Sub BindRisultati()
        Dim i As Integer
        TabellaRisultati = Session("GestioneLegamiEstrazioni")
        GridEstrazione.AutoGenerateColumns = False
        GridEstrazione.DataSource = TabellaRisultati
        GridEstrazione.DataBind()

        For I = 0 To GridEstrazione.Rows.Count - 1

            GridEstrazione.Rows(i).Cells(0).Text = "<a href=""#"" onclick=""copiaimporto('" & i & "');""><img src=""images\copiaimporto.png"" height=""16"" width=""16"" alt=""digita importo""></a>" & GridEstrazione.Rows(i).Cells(0).Text

        Next

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub GeneraleWeb_CreaMandatiReversaliStep_2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim K As New Cls_MovimentoContabile

        K.Leggi(Session("DC_GENERALE"), Session("NumeroRegistrazione"))

        Lbl_DatiRegistrazione.Text = "<div  id=""Registrazione"" class=""Registrazione"">Numero Registrazione : " & K.NumeroRegistrazione & "  Importo : " & Format(K.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00") & "</div>"

        Dim PianoConti As New Cls_Pianodeiconti

        PianoConti.Mastro = K.Righe(0).MastroPartita
        PianoConti.Conto = K.Righe(0).ContoPartita
        PianoConti.Sottoconto = K.Righe(0).SottocontoPartita
        PianoConti.Decodfica(Session("DC_GENERALE"))

        Txt_Sottoconto.Text = PianoConti.Mastro & " " & PianoConti.Conto & " " & PianoConti.Sottoconto & " " & PianoConti.Descrizione
        Call CaricaPagina()
        EseguiJS()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim I As Integer
        Dim Ind As Integer
        Dim SommaLegato As Double
        Dim legami As New Cls_Legami

        legami.Leggi(Session("DC_GENERALE"), 0, Session("NumeroRegistrazione"))

        SommaLegato = 0
        For I = 0 To GridEstrazione.Rows.Count - 1
            Dim TxtImportoLegame As TextBox = DirectCast(GridEstrazione.Rows(I).FindControl("TxtImportoLegame"), TextBox)

            If TxtImportoLegame.Text <> "" Then
                If Math.Abs(CDbl(TxtImportoLegame.Text)) > 0 Then
                    SommaLegato = SommaLegato + CDbl(TxtImportoLegame.Text)
                End If
            End If
        Next

        Dim K As New Cls_MovimentoContabile

        K.Leggi(Session("DC_GENERALE"), Session("NumeroRegistrazione"))
        If SommaLegato > K.ImportoRegistrazioneDocumento(Session("DC_TABELLE")) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Importo Legato Maggiore Importo Registrazione</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If



        For I = 0 To GridEstrazione.Rows.Count - 1
            Dim TxtImportoLegame As TextBox = DirectCast(GridEstrazione.Rows(I).FindControl("TxtImportoLegame"), TextBox)

            If TxtImportoLegame.Text <> "" Then
                If Math.Abs(CDbl(TxtImportoLegame.Text)) > 0 Then
                    For Ind = 0 To 300
                        If legami.NumeroDocumento(Ind) = 0 And legami.NumeroPagamento(I) = 0 Then
                            legami.NumeroDocumento(Ind) = Val(GridEstrazione.Rows(I).Cells(1).Text)
                            legami.NumeroPagamento(Ind) = Session("NumeroRegistrazione")

                            legami.Importo(Ind) = CDbl(TxtImportoLegame.Text)
                            Exit For
                        End If
                    Next
                End If
            End If
        Next

        legami.Scrivi(Session("DC_GENERALE"), 0, Session("NumeroRegistrazione"))

        Response.Redirect("CreaMandatiReversali.aspx")
    End Sub
End Class
