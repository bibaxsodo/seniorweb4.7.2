﻿Public Class RegistrazioneViewer
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim NumeroRegistrazione As String = context.Request.QueryString("NumeroRegistrazione")
        '-ms-filter: ""progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"";
        '
        '

        Dim Tabella As String
        Dim I As Integer



        Tabella = "<table width=""100%"">"
        Tabella = Tabella & "<tr>"

        Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Dare"
        Tabella = Tabella & "</td>"

        Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Avere"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "</tr>"



        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.NumeroRegistrazione = NumeroRegistrazione
        Registrazione.Leggi(context.Session("DC_GENERALE"), NumeroRegistrazione)

        Dim NumeroRighe As Integer = 0

        If IsDate(Registrazione.DataRegistrazione) Then

            For I = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(I)) Then
                    If Registrazione.Righe(I).DareAvere = "A" Then
                        Tabella = Tabella & "<tr>"

                        Tabella = Tabella & "<td width=""40%"">"
                        Tabella = Tabella & "</td>"
                        Tabella = Tabella & "<td width=""10%"">"
                        Tabella = Tabella & "</td>"
                        Dim PianoConti As New Cls_Pianodeiconti

                        PianoConti.Mastro = Registrazione.Righe(I).MastroPartita
                        PianoConti.Conto = Registrazione.Righe(I).ContoPartita
                        PianoConti.Sottoconto = Registrazione.Righe(I).SottocontoPartita
                        PianoConti.Decodfica(context.Session("DC_GENERALE"))



                        Tabella = Tabella & "<td width=""40%"">" & PianoConti.Descrizione & "</td>"
                        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;"">" & Format(Registrazione.Righe(I).Importo, "#,##0.00") & "</td>"
                        Tabella = Tabella & "</tr>"
                        NumeroRighe = NumeroRighe + 1
                    End If
                    If Registrazione.Righe(I).DareAvere = "D" Then
                        Tabella = Tabella & "<tr>"

                        NumeroRighe = NumeroRighe + 1
                        Dim PianoConti As New Cls_Pianodeiconti

                        PianoConti.Mastro = Registrazione.Righe(I).MastroPartita
                        PianoConti.Conto = Registrazione.Righe(I).ContoPartita
                        PianoConti.Sottoconto = Registrazione.Righe(I).SottocontoPartita
                        PianoConti.Decodfica(context.Session("DC_GENERALE"))



                        Tabella = Tabella & "<td width=""40%"">" & PianoConti.Descrizione & "</td>"
                        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;"">" & Format(Registrazione.Righe(I).Importo, "#,##0.00") & "</td>"

                        Tabella = Tabella & "<td width=""40%"">"
                        Tabella = Tabella & "</td>"
                        Tabella = Tabella & "<td width=""10%"">"
                        Tabella = Tabella & "</td >"
                        Tabella = Tabella & "</tr>"
                    End If
                End If
            Next

        End If

        Tabella = Tabella & "</table>"
        Dim altezza As String
        altezza = (NumeroRighe * 30) + 30

        context.Response.Write("<div id=""RegistrazioneViewer""  style=""position: absolute; -moz-box-shadow: 8px 6px 5px 5px #000000;-webkit-box-shadow: 8px 6px 5px 5px #000000;box-shadow: 8px 6px 5px 5px #000000; filter: alpha(opacity=70);-moz-opacity: 0.7;-khtml-opacity: 0.7;opacity: 0.7; width: 600px;height: " & altezza & "px; border: 2px #00117a solid;background-color: #2c2ddd; color: #ffffff;"">" & Tabella & "</div>")

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class