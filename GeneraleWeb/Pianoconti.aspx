﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="Pianoconti" CodeFile="Pianoconti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Piano dei Conti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">   
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Piano Conti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />&nbsp;      
        <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <asp:ImageButton ID="ImgRicerca" ImageUrl="../images/PianoDeiContiInserisci.jpg" Width="112px" Height="100px" class="Effetto" alt="Ricerca Registrazioni" runat="server" /><br />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');">Piano Conti</a></label>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Piano dei conti         
              
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <br />
                                    <label class="LabelCampo">Mastro:<font color="red">*</font></label>
                                    <asp:TextBox ID="TxT_Mastro" AutoPostBack="True" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="150px" MaxLength="5"></asp:TextBox>



                                    &#160;
         <asp:Label ID="LblMastro" runat="server"></asp:Label>




                                    <br />
                                    <br />
                                    <label class="LabelCampo">Conto:</label>
                                    <asp:TextBox ID="Txt_Conto" AutoPostBack="True" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="150px" MaxLength="5"></asp:TextBox>



                                    &#160;
         <asp:Label ID="LblConto" runat="server"></asp:Label>




                                    <br />
                                    <br />
                                    <label class="LabelCampo">Sottoconto:</label>
                                    <asp:TextBox ID="Txt_Sottoconto" AutoPostBack="True" autocomplete="off"
                                        onkeypress="return soloNumeri(event);" runat="server" Width="150px"
                                        MaxLength="10"></asp:TextBox>



                                    &#160;
         <asp:Label ID="LblSottoconto" runat="server"></asp:Label>




                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Descrizione" autocomplete="off" runat="server" Width="432px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipo :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Tipo" Width="290px" runat="server">
                                        <asp:ListItem Value="A">Attività</asp:ListItem>
                                        <asp:ListItem Value="P">Passività</asp:ListItem>
                                        <asp:ListItem Value="C">Costi</asp:ListItem>
                                        <asp:ListItem Value="R">Ricavi</asp:ListItem>
                                        <asp:ListItem Value="O">Conti d'Ordine</asp:ListItem>

                                    </asp:DropDownList>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">Commento :</label>
                                    <asp:TextBox ID="Txt_commento" autocomplete="off" runat="server" Width="432px" MaxLength="50"></asp:TextBox>



                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 250px;">
                                        <asp:Label ID="lbl_Ivdirettiva" runat="server" Text="IV° Direttiva CEE"></asp:Label></label>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_SezClasse" Text="Sezione Classe :" runat="server"></asp:Label>
                                    </label>
                                    <asp:DropDownList ID="DD_SezioneClasse" runat="server" Width="290px"></asp:DropDownList>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_Segnoopposto" runat="server" Text="Segno Opposto"></asp:Label></label>
                                    <asp:DropDownList ID="DD_SegnoOpposto" runat="server" Width="290px"></asp:DropDownList>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipologia :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Tipologia" Width="290px" runat="server"></asp:DropDownList>




                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipo Conto :</label>
                                    <asp:CheckBox ID="Chk_ContoAmmort" runat="server" Font-Names="Arial" Text="Conto ammortamento (PERIODO A CONFRONTO)" />




                                    <asp:CheckBox ID="Chk_NonInUso" runat="server" Font-Names="Arial" Text="Non in uso" />




                                    <br />








                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="304px"></asp:Label><br />
        </div>
    </form>
</body>
</html>
