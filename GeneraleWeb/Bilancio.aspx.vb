﻿Imports System.Threading
Imports System.Data.OleDb

Partial Class Bilancio
    Inherits System.Web.UI.Page
    Dim myRequest As System.Net.WebRequest

    Private Delegate Sub DoWorkDelegate(ByRef data As Object)
    Private _work As DoWorkDelegate

    Public StDataCond As String
    Private ConnessioniGenerale As String
    Private ConnessioniTabelle As String
    Private ConnessioniOspiti As String
    Private ConnessioneFinanziaria As String


    Private CampoDatiGeneraliClientiMastro As Long
    Private CampoDatiGeneraliClientiConto As Long    
    Private CampoDatiGeneraliFornitoriMastro As Long
    Private CampoDatiGeneraliFornitoriConto As Long


    Dim MyRs As New ADODB.Recordset
    Dim WrDRs As New ADODB.Recordset
    Dim WrARs As New ADODB.Recordset
    Dim WrRs As New ADODB.Recordset
    Dim StampeDb As New ADODB.Connection
    Dim DataInizio As Date
    Dim W_CausaleChiusura As String
    Dim OspitiDb As New ADODB.Connection
    Dim GeneraleDb As New ADODB.Connection
    Dim TabelleDb As New ADODB.Connection
    Dim Txt_DataLimiteText As Date


    Protected Sub Stampa(ByVal data As Object)
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data


        OspitiDb.Open(ConnessioniOspiti)
        GeneraleDb.Open(ConnessioniGenerale)
        TabelleDb.Open(ConnessioniTabelle)


        SessioneTP("PRINTERKEYBILANCIO") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID
        Cache("PRINTERKEYBILANCIO" + Session.SessionID) = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        Txt_DataLimiteText = DateSerial(Mid(Txt_DataLimite.Text, 7, 4), Mid(Txt_DataLimite.Text, 4, 2), Mid(Txt_DataLimite.Text, 1, 2))


        CampoDatiGeneraliClientiMastro = CampoDatiGenerali("ClientiMastro")
        CampoDatiGeneraliClientiConto = CampoDatiGenerali("ClientiConto")
        CampoDatiGeneraliFornitoriMastro = CampoDatiGenerali("FornitoriMastro")
        CampoDatiGeneraliFornitoriConto = CampoDatiGenerali("FornitoriConto")


        Call PreparaBilancio(data)
        Call TestoSbilancio(data)


        OspitiDb.Close()
        GeneraleDb.Close()
        TabelleDb.Close()


        Threading.Thread.Sleep(500)

        SessioneTP("CampoProgressBar") = "FINE"
        Cache("CampoProgressBar" + Session.SessionID) = "FINE"

    End Sub




    Private Sub TestoSbilancio(ByVal data As Object)
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data
        Dim TotDare As Double
        Dim MySql As String

        SessioneTP("CampoProgressBar") = "Passo 11/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 11/11" & Space(10)

        StampeDb.Open(ConnessioneFinanziaria)


        TotDare = 0
        MySql = "SELECT SUM(ImportoDare) as totDare FROM BilancioSezioniContrapposte Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' And Tipo = 'CR'"
        MyRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        TotDare = MoveFromDbWC(MyRs, "totDare")
        MyRs.Close()

        MySql = "SELECT SUM(ImportoAvere) as totAvere FROM BilancioSezioniContrapposte Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' And Tipo = 'CR'"
        MyRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        TotDare = TotDare - MoveFromDbWC(MyRs, "totAvere")
        MyRs.Close()

        If TotDare > 0 Then
            StampeDb.Execute("Update BilancioSezioniContrapposte SET TSbilancio = 'Avanzo d''esercizio' WHERE PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        Else
            StampeDb.Execute("Update BilancioSezioniContrapposte SET TSbilancio = 'Disvanzo d''esercizio' WHERE PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        End If

        StampeDb.Close()


    End Sub

    Private Sub AddMastro()
        Dim MySql As String
        Dim Query As String

        StampeDb.Open(ConnessioneFinanziaria)

        MySql = "SELECT * FROM PianoConti WHERE Conto = 0  And SottoConto = 0  ORDER BY Mastro, Conto, Sottoconto"
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        Do Until MyRs.EOF

            Query = " (MastroDare = " & MoveFromDbWC(MyRs, "Mastro") & " Or MastroAvere = " & MoveFromDbWC(MyRs, "Mastro") & ") "
            WrDRs.Open("Select MastroDare,MastroAvere,Tipo From BilancioSezioniContrapposte Where " & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' Group by MastroDare,MastroAvere,Tipo ", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not WrDRs.EOF
                If MoveFromDbWC(WrDRs, "MastroDare") = MoveFromDbWC(MyRs, "Mastro") Then
                    Query = " Where  Tipo = '" & MoveFromDbWC(WrDRs, "tipo") & "' And  MastroDare = " & MoveFromDbWC(MyRs, "Mastro") & " And ContoAvere = 0 And SottoContoAvere = 0"
                    WrRs.Open("Select * From BilancioSezioniContrapposte" & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If WrRs.EOF Then
                        WrRs.AddNew()
                        MoveToDb(WrRs.Fields("tipo"), MoveFromDbWC(WrDRs, "tipo"))
                        MoveToDb(WrRs.Fields("MastroDare"), MoveFromDbWC(WrDRs, "Mastro"))
                        MoveToDb(WrRs.Fields("ContoDare"), MoveFromDbWC(WrDRs, "Conto"))
                        MoveToDb(WrRs.Fields("SottoContoDare"), MoveFromDbWC(WrDRs, "Sottoconto"))
                        MoveToDb(WrRs.Fields("DescrizioneDare"), MoveFromDbWC(WrDRs, "Descrizione"))
                        MoveToDb(WrRs.Fields("ImportoDare"), 0)
                        'WrRs!IntestaSocieta = MoveFromDb(WrDRs!IntestaSocieta)
                        'WrRs!DataLimite = MoveFromDb(WrDRs!DataLimite)
                        MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrRs.Update()
                    End If
                    WrRs.Close()
                End If
                If MoveFromDbWC(WrDRs, "MastroDare") = MoveFromDbWC(MyRs, "Mastro") Then
                    Query = " Where  Tipo = '" & MoveFromDbWC(WrDRs, "tipo") & "' And  MastroAvere = " & MoveFromDbWC(MyRs, "Mastro") & " And ContoAvere = 0 And SottoContoAvere = 0"
                    WrRs.Open("Select * From BilancioSezioniContrapposte" & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If WrRs.EOF Then
                        WrRs.AddNew()
                        MoveToDb(WrRs.Fields("tipo"), MoveFromDbWC(WrDRs, "tipo"))
                        MoveToDb(WrRs.Fields("MastroAvere"), MoveFromDbWC(MyRs, "Mastro"))
                        MoveToDb(WrRs.Fields("ContoAvere"), MoveFromDbWC(MyRs, "Conto"))
                        MoveToDb(WrRs.Fields("SottoContoAvere"), MoveFromDbWC(MyRs, "Sottoconto"))
                        MoveToDb(WrRs.Fields("DescrizioneAvere"), MoveFromDbWC(MyRs, "Descrizione"))
                        MoveToDb(WrRs.Fields("ImportoAvere"), 0)
                        MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        'WrRs!IntestaSocieta = MoveFromDb(WrDRs!IntestaSocieta)
                        'WrRs!DataLimite = MoveFromDb(WrDRs!DataLimite)
                        WrRs.Update()
                    End If
                    WrRs.Close()
                End If
                WrDRs.MoveNext()
            Loop
            WrDRs.Close()

            Query = " Mastro = " & MoveFromDbWC(MyRs, "Mastro")
            WrDRs.Open("Select * From BilancioSezioniContrapposteDare Where " & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not WrDRs.EOF Then
                WrDRs.AddNew()
                MoveToDb(WrRs.Fields("Mastro"), MoveFromDbWC(MyRs, "Mastro"))
                MoveToDb(WrRs.Fields("Conto"), MoveFromDbWC(MyRs, "Conto"))
                MoveToDb(WrRs.Fields("Sottoconto"), MoveFromDbWC(MyRs, "SottoConto"))
                MoveToDb(WrRs.Fields("Descrizione"), MoveFromDbWC(MyRs, "Descrizione"))
                MoveToDb(WrRs.Fields("Importo"), 0)
                MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                WrDRs.Update()
            End If
            WrDRs.Close()


            WrDRs.Open("Select * From BilancioSezioniContrapposteDare Where " & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not WrDRs.EOF Then
                WrDRs.AddNew()
                MoveToDb(WrRs.Fields("Mastro"), MoveFromDbWC(MyRs, "Mastro"))
                MoveToDb(WrRs.Fields("Conto"), MoveFromDbWC(MyRs, "Conto"))
                MoveToDb(WrRs.Fields("Sottoconto"), MoveFromDbWC(MyRs, "SottoConto"))
                MoveToDb(WrRs.Fields("Descrizione"), MoveFromDbWC(MyRs, "Descrizione"))
                MoveToDb(WrRs.Fields("Importo"), 0)
                MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                WrDRs.Update()
            End If
            WrDRs.Close()

            WrARs.Open("Select * From BilancioSezioniContrapposteAvere Where " & Query & " AND PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not WrARs.EOF Then
                WrARs.AddNew()
                MoveToDb(WrRs.Fields("Mastro"), MoveFromDbWC(MyRs, "Mastro"))
                MoveToDb(WrRs.Fields("Conto"), MoveFromDbWC(MyRs, "Conto"))
                MoveToDb(WrRs.Fields("Sottoconto"), MoveFromDbWC(MyRs, "SottoConto"))
                MoveToDb(WrRs.Fields("Descrizione"), MoveFromDbWC(MyRs, "Descrizione"))
                MoveToDb(WrRs.Fields("Importo"), 0)
                MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                WrARs.Update()
            End If
            WrARs.Close()

            MyRs.MoveNext()
        Loop
        MyRs.Close()
        StampeDb.Close()

    End Sub

    Private Sub PreparaBilancio(ByVal data As Object)
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data
        Dim MySql As String

        DataInizio = DateSerial(Year(Txt_DataLimiteText), 1, 1)
        MySql = "SELECT * From DatiGenerali"

        MyRs.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            If MoveFromDbWC(MyRs, "EsercizioMese") > 0 And MoveFromDbWC(MyRs, "EsercizioMese") < 13 Then
                DataInizio = DateSerial(Year(Txt_DataLimiteText), MoveFromDbWC(MyRs, "EsercizioMese"), MoveFromDbWC(MyRs, "EsercizioGiorno"))
            End If
            W_CausaleChiusura = MoveFromDbWC(MyRs, "CausaleChiusura")
        End If
        MyRs.Close()
        If Txt_DataLimiteText < DataInizio Then
            DataInizio = DateAdd("yyyy", -1, DataInizio)
        End If
        If Chk_AnnoPrecedente.Checked = True Then
            DataInizio = DateAdd("yyyy", -1, DataInizio)
        End If

        StampeDb.Open(ConnessioneFinanziaria)
        StampeDb.Execute("Delete From BilancioSezioniContrapposte where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")

        '
        ' Attività e Passività
        '
        StampeDb.Execute("Delete From BilancioSezioniContrapposteDare where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        StampeDb.Execute("Delete From BilancioSezioniContrapposteAvere where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        WrDRs.Open("Select * From BilancioSezioniContrapposteDare where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        WrARs.Open("Select * From BilancioSezioniContrapposteAvere where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        SessioneTP("CampoProgressBar") = "Passo 1/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 1/11" & Space(10)
        Call SommaDareAvere("A", data)
        SessioneTP("CampoProgressBar") = "Passo 2/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 2/11" & Space(10)
        Call SommaDareAvere("P", data)
        SessioneTP("CampoProgressBar") = "Passo 3/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 3/11" & Space(10)
        Call SommaDareAvere("O", data)
        SessioneTP("CampoProgressBar") = "Passo 4/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 4/11" & Space(10)
        Call SommaDareAvere("Q", data)
        SessioneTP("CampoProgressBar") = "Passo 5/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 5/11" & Space(10)
        Call SommaDareAvere("W", data)

        WrARs.Close()
        WrDRs.Close()

        WrRs.Open("Select * From BilancioSezioniContrapposte where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        SessioneTP("CampoProgressBar") = "Passo 6/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 6/11" & Space(10)
        Call CreaBilancio("AP")

        SessioneTP("CampoProgressBar") = "Passo 7/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 7/11" & Space(10)
        '
        ' Costi e Ricavi
        '
        StampeDb.Execute("Delete From BilancioSezioniContrapposteDare where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        StampeDb.Execute("Delete From BilancioSezioniContrapposteAvere where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'")
        WrDRs.Open("Select * From BilancioSezioniContrapposteDare where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        WrARs.Open("Select * From BilancioSezioniContrapposteAvere where PRINTERKEY = '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        Call SommaDareAvere("C", data)
        SessioneTP("CampoProgressBar") = "Passo 8/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 8/11" & Space(10)
        Call SommaDareAvere("R", data)
        WrARs.Close()
        WrDRs.Close()
        SessioneTP("CampoProgressBar") = "Passo 9/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 9/11" & Space(10)
        Call CreaBilancio("CR")
        WrRs.Close()
        SessioneTP("CampoProgressBar") = "Passo 10/11" & Space(10)
        Cache("CampoProgressBar" + Session.SessionID) = "Passo 10/11" & Space(10)

        StampeDb.Close()
    End Sub

    Function CondizioneCserv() As String
        Dim Condizione As String = ""
        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                Dim Indice As Integer
                Call AggiornaCServ()
                Condizione = Condizione & " ("
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Indice >= 1 Then
                            If Condizione <> "" Then
                                Condizione = Condizione & " Or "
                            End If
                        End If

                        Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = Condizione & ") "
            End If
        Else
            Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.SelectedValue & "'"
        End If

        Return Condizione
    End Function

    Private Sub SommaDareAvere(ByVal tipo As String, ByVal data As Object)
        Dim OldMastro As Long
        Dim OldConto As Long
        Dim MySql As String
        Dim Totale As Double
        Dim NumRec As Double
        Dim Nx As Double

        Dim Condizione As String = ""

        Dim cn As OleDbConnection

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data


        cn = New Data.OleDb.OleDbConnection(SessioneTP("DC_GENERALE"))

        cn.Open()

        Condizione = "( SELECT count(*) FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione "

        Condizione = Condizione & " WHERE "

        Condizione = Condizione & "MovimentiContabiliRiga.MastroPartita = Mastro AND "
        Condizione = Condizione & "MovimentiContabiliRiga.ContoPartita = Conto  AND "
        Condizione = Condizione & "MovimentiContabiliRiga.SottocontoPartita = Sottoconto AND "
        Condizione = Condizione & "DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} AND "
        Condizione = Condizione & "DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'})  > 0"
        If CondizioneCserv() <> "" Then
            Condizione = Condizione & " And " & CondizioneCserv()
        End If

        OldMastro = 0
        OldConto = 0
        MySql = "SELECT count(*) FROM PianoConti WHERE Sottoconto <> 0 AND Tipo = '" & tipo & "' "
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            NumRec = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()

        Nx = 0        
        MySql = "SELECT Mastro,Conto,Sottoconto,Descrizione FROM PianoConti WHERE Sottoconto <> 0 AND Tipo = '" & tipo & "'  ORDER BY Mastro, Conto, Sottoconto"

        Dim cmdPN As New OleDbCommand()
        cmdPN.CommandText = MySql
        cmdPN.Connection = cn
        cmdPN.CommandTimeout = 120
        Dim MyRs As OleDbDataReader = cmdPN.ExecuteReader()
        Do While MyRs.Read

            Nx = Nx + 1
            SessioneTP("CampoProgressBar") = Mid(SessioneTP("CampoProgressBar"), 1, 14) & Modulo.MathRound(Nx / NumRec * 100, 2)
            Cache("CampoProgressBar" + Session.SessionID) = Mid(Cache("CampoProgressBar" + Session.SessionID), 1, 14) & Modulo.MathRound(Nx / NumRec * 100, 2)
            ' (OldMastro = CampoDatiGeneraliClientiMastro Or OldMastro = CampoDatiGeneraliFornitoriMastro) And

            If ((OldMastro = CampoDatiGeneraliClientiMastro) Or (OldMastro = CampoDatiGeneraliFornitoriMastro And OldConto = CampoDatiGeneraliFornitoriConto)) And ((OldMastro <> campodbN(MyRs.Item("Mastro")) Or OldConto <> campodbN(MyRs.Item("Conto")))) And Chk_RaggruppaClientiFornitori.Checked = True Then

                'If Modulo.MathRound(Totale, 2) <> 0 Then
                If Chk_UsaNegativo.Checked = False Then
                    If Modulo.MathRound(Totale, 2) < 0 Then
                        Totale = Modulo.MathRound(Totale * -1, 2)
                        WrARs.AddNew()
                        MoveToDb(WrARs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrARs.Fields("Conto"), OldConto)
                        MoveToDb(WrARs.Fields("Sottoconto"), 0)
                        Dim DConto As New Cls_Pianodeiconti
                        DConto.Mastro = OldMastro
                        DConto.Conto = OldConto
                        DConto.Sottoconto = 0
                        DConto.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrARs.Fields("Descrizione"), DConto.Descrizione)
                        MoveToDb(WrARs.Fields("Importo"), Totale)

                        MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrARs.Update()
                    Else
                        WrDRs.AddNew()
                        MoveToDb(WrDRs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrDRs.Fields("Conto"), OldConto)
                        MoveToDb(WrDRs.Fields("Sottoconto"), 0)
                        Dim DConto As New Cls_Pianodeiconti
                        DConto.Mastro = OldMastro
                        DConto.Conto = OldConto
                        DConto.Sottoconto = 0
                        DConto.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrDRs.Fields("Descrizione"), DConto.Descrizione)
                        MoveToDb(WrDRs.Fields("Importo"), Totale)

                        MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrDRs.Update()
                    End If
                Else
                    If tipo = "P" Or tipo = "R" Then
                        Totale = Modulo.MathRound(Totale * -1, 2)
                        WrARs.AddNew()
                        MoveToDb(WrARs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrARs.Fields("Conto"), OldConto)
                        MoveToDb(WrARs.Fields("Sottoconto"), 0)
                        Dim DConto As New Cls_Pianodeiconti
                        DConto.Mastro = OldMastro
                        DConto.Conto = OldConto
                        DConto.Sottoconto = 0
                        DConto.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrARs.Fields("Descrizione"), DConto.Descrizione)
                        MoveToDb(WrARs.Fields("Importo"), Totale)

                        MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrARs.Update()
                    End If
                    If tipo = "A" Or tipo = "C" Then

                        WrDRs.AddNew()
                        MoveToDb(WrDRs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrDRs.Fields("Conto"), OldConto)
                        MoveToDb(WrDRs.Fields("Sottoconto"), 0)
                        Dim DConto As New Cls_Pianodeiconti
                        DConto.Mastro = OldMastro
                        DConto.Conto = OldConto
                        DConto.Sottoconto = 0
                        DConto.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrDRs.Fields("Descrizione"), DConto.Descrizione)
                        MoveToDb(WrDRs.Fields("Importo"), Totale)

                        MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrDRs.Update()
                    End If

                End If

            End If


            If OldMastro <> campodbN(MyRs.Item("Mastro")) Then
                Totale = 0
                OldMastro = campodbN(MyRs.Item("Mastro"))
            End If
            If OldConto <> campodbN(MyRs.Item("Conto")) Then
                Totale = 0
                OldConto = campodbN(MyRs.Item("Conto"))
            End If



            If ((CampoDatiGeneraliClientiMastro = campodbN(MyRs.Item("Mastro"))) Or (CampoDatiGeneraliFornitoriMastro = campodbN(MyRs.Item("Mastro")) And campodbN(MyRs.Item("Conto")) = CampoDatiGeneraliFornitoriConto)) And Chk_RaggruppaClientiFornitori.Checked = True Then
                Totale = Totale + totaleSottoConto(campodbN(MyRs.Item("Mastro")), campodbN(MyRs.Item("Conto")), campodbN(MyRs.Item("SottoConto")), data)
            Else
                Totale = totaleSottoConto(campodbN(MyRs.Item("Mastro")), campodbN(MyRs.Item("Conto")), campodbN(MyRs.Item("SottoConto")), data)

                If Modulo.MathRound(Totale, 2) <> 0 Then
                    If Chk_UsaNegativo.Checked = False Then
                        If Modulo.MathRound(Totale, 2) < 0 Then
                            Totale = Modulo.MathRound(Totale * -1, 2)
                            WrARs.AddNew()
                            MoveToDb(WrARs.Fields("Mastro"), campodbN(MyRs.Item("Mastro")))
                            MoveToDb(WrARs.Fields("Conto"), campodbN(MyRs.Item("Conto")))
                            MoveToDb(WrARs.Fields("Sottoconto"), campodbN(MyRs.Item("SottoConto")))
                            MoveToDb(WrARs.Fields("Descrizione"), campodb(MyRs.Item("Descrizione")))
                            MoveToDb(WrARs.Fields("Importo"), Totale)
                            MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                            WrARs.Update()
                        Else
                            WrDRs.AddNew()
                            MoveToDb(WrDRs.Fields("Mastro"), campodbN(MyRs.Item("Mastro")))
                            MoveToDb(WrDRs.Fields("Conto"), campodbN(MyRs.Item("Conto")))
                            MoveToDb(WrDRs.Fields("Sottoconto"), campodbN(MyRs.Item("SottoConto")))
                            MoveToDb(WrDRs.Fields("Descrizione"), campodb(MyRs.Item("Descrizione")))
                            MoveToDb(WrDRs.Fields("Importo"), Totale)
                            MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                            WrDRs.Update()
                        End If
                    Else
                        If tipo = "P" Or tipo = "R" Then
                            'Totale = Modulo.MathRound(Totale * -1, 2)
                            Totale = Modulo.MathRound(Totale * -1, 2)
                            WrARs.AddNew()
                            MoveToDb(WrARs.Fields("Mastro"), campodbN(MyRs.Item("Mastro")))
                            MoveToDb(WrARs.Fields("Conto"), campodbN(MyRs.Item("Conto")))
                            MoveToDb(WrARs.Fields("Sottoconto"), campodbN(MyRs.Item("SottoConto")))
                            MoveToDb(WrARs.Fields("Descrizione"), campodb(MyRs.Item("Descrizione")))
                            MoveToDb(WrARs.Fields("Importo"), Totale)
                            MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                            WrARs.Update()
                        End If
                        If tipo = "A" Or tipo = "C" Then

                            WrDRs.AddNew()
                            MoveToDb(WrDRs.Fields("Mastro"), campodbN(MyRs.Item("Mastro")))
                            MoveToDb(WrDRs.Fields("Conto"), campodbN(MyRs.Item("Conto")))
                            MoveToDb(WrDRs.Fields("Sottoconto"), campodbN(MyRs.Item("SottoConto")))
                            MoveToDb(WrDRs.Fields("Descrizione"), campodb(MyRs.Item("Descrizione")))
                            MoveToDb(WrDRs.Fields("Importo"), Totale)
                            MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                            WrDRs.Update()
                        End If
                    End If
                End If
            End If
        Loop
        MyRs.Close()

        If ((OldMastro = CampoDatiGeneraliClientiMastro) Or (OldMastro = CampoDatiGeneraliFornitoriMastro And OldConto = CampoDatiGeneraliFornitoriConto)) And (OldMastro <> 0 Or OldConto <> 0) And Chk_RaggruppaClientiFornitori.Checked = True Then

            If Modulo.MathRound(Totale, 2) <> 0 Then
                If Chk_UsaNegativo.Checked = False Then
                    If Modulo.MathRound(Totale, 2) < 0 Then
                        Totale = Modulo.MathRound(Totale * -1, 2)
                        WrARs.AddNew()
                        MoveToDb(WrARs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrARs.Fields("Conto"), 0)
                        MoveToDb(WrARs.Fields("Sottoconto"), 0)
                        Dim Dc1 As New Cls_Pianodeiconti
                        Dc1.Mastro = OldMastro
                        Dc1.Conto = 0
                        Dc1.Sottoconto = 0
                        Dc1.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrARs.Fields("Descrizione"), Dc1.Descrizione)
                        MoveToDb(WrARs.Fields("Importo"), Totale)
                        MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrARs.Update()
                    Else
                        WrDRs.AddNew()
                        MoveToDb(WrDRs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrDRs.Fields("Conto"), 0)
                        MoveToDb(WrDRs.Fields("Sottoconto"), 0)
                        Dim Dc1 As New Cls_Pianodeiconti
                        Dc1.Mastro = OldMastro
                        Dc1.Conto = 0
                        Dc1.Sottoconto = 0
                        Dc1.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrDRs.Fields("Descrizione"), Dc1.Descrizione)
                        MoveToDb(WrDRs.Fields("Importo"), Totale)
                        MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrDRs.Update()
                    End If
                Else
                    If tipo = "P" Or tipo = "R" Then
                        Totale = Modulo.MathRound(Totale * -1, 2)
                        'Totale = Modulo.MathRound(Totale * -1, 2)
                        WrARs.AddNew()
                        MoveToDb(WrARs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrARs.Fields("Conto"), 0)
                        MoveToDb(WrARs.Fields("Sottoconto"), 0)
                        Dim Dc1 As New Cls_Pianodeiconti
                        Dc1.Mastro = OldMastro
                        Dc1.Conto = 0
                        Dc1.Sottoconto = 0
                        Dc1.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrARs.Fields("Descrizione"), Dc1.Descrizione)
                        MoveToDb(WrARs.Fields("Importo"), Totale)
                        MoveToDb(WrARs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrARs.Update()
                    End If
                    If tipo = "A" Or tipo = "C" Then

                        WrDRs.AddNew()
                        MoveToDb(WrDRs.Fields("Mastro"), OldMastro)
                        MoveToDb(WrDRs.Fields("Conto"), 0)
                        MoveToDb(WrDRs.Fields("Sottoconto"), 0)
                        Dim Dc1 As New Cls_Pianodeiconti
                        Dc1.Mastro = OldMastro
                        Dc1.Conto = 0
                        Dc1.Sottoconto = 0
                        Dc1.Decodfica(ConnessioniGenerale)
                        MoveToDb(WrDRs.Fields("Descrizione"), Dc1.Descrizione)
                        MoveToDb(WrDRs.Fields("Importo"), Totale)
                        MoveToDb(WrDRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                        WrDRs.Update()
                    End If
                End If
            End If
        End If

        cn.Close()

    End Sub

    Private Sub CreaBilancioSeparato(ByVal tipo As String)
        Dim WSocieta As String
        Dim OldMastroD As Long
        Dim OldContoD As Long
        Dim TotImportoD As Long
        Dim OldMastroA As Long
        Dim OldContoA As Long
        Dim TotImportoA As Long
        Dim SwUpdate As Boolean

        Dim Ks As New Cls_DecodificaSocieta
        Dim MySql As String


        WSocieta = Ks.DecodificaSocieta("")
        OldMastroD = 0
        OldContoD = 0
        TotImportoD = 0
        OldMastroA = 0
        OldContoA = 0
        TotImportoA = 0
        SwUpdate = True

        '  WrRs.Open "Select * From BilancioSezioniContrapposte" & Query, StampeDb, adOpenKeyset, adLockOptimistic


        MySql = "SELECT * FROM BilancioSezioniContrapposteDare Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ORDER BY Mastro, Conto, Sottoconto"
        WrDRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        Do Until WrDRs.EOF

            If OldMastroD <> 0 And (OldMastroD <> MoveFromDbWC(WrDRs, "Mastro") Or OldContoD <> MoveFromDbWC(WrDRs, "Conto")) Then
                WrRs.AddNew()
                MoveToDb(WrRs.Fields("tipo"), tipo)
                MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
                MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
                MoveToDb(WrRs.Fields("SottoContoDare"), 0)

                Dim Dc1 As New Cls_Pianodeiconti
                Dc1.Mastro = OldMastroD
                Dc1.Conto = OldContoD
                Dc1.Sottoconto = 0
                Dc1.Decodfica(ConnessioniGenerale)
                MoveToDb(WrRs.Fields("DescrizioneDare"), Dc1.Descrizione)
                MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
                MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
                MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)
                MoveToDb(WrRs.Fields("Ragruppamento"), 0)
                Dim CP1 As New Cls_Pianodeiconti

                CP1.Mastro = OldMastroD
                CP1.Conto = OldContoD
                CP1.Sottoconto = 0
                CP1.Decodfica(ConnessioniGenerale)

                Dim CP2 As New Cls_Pianodeiconti

                CP2.Mastro = OldMastroD
                CP2.Conto = OldContoD
                CP2.Sottoconto = 0
                CP2.Decodfica(ConnessioniGenerale)

                If CP1.Tipo = "W" Or CP2.Tipo = "Q" Then
                    MoveToDb(WrRs.Fields("Ragruppamento"), 1)
                End If
                MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                WrRs.Update()
                TotImportoD = 0
            End If


            WrRs.AddNew()
            MoveToDb(WrRs.Fields("tipo"), tipo)
            MoveToDb(WrRs.Fields("MastroDare"), MoveFromDbWC(WrDRs, "Mastro"))
            MoveToDb(WrRs.Fields("ContoDare"), MoveFromDbWC(WrDRs, "Conto"))
            MoveToDb(WrRs.Fields("SottoContoDare"), MoveFromDbWC(WrDRs, "Sottoconto"))
            Dim CP3 As New Cls_Pianodeiconti

            CP3.Mastro = MoveFromDbWC(WrDRs, "Mastro")
            CP3.Conto = MoveFromDbWC(WrDRs, "Conto")
            CP3.Sottoconto = MoveFromDbWC(WrDRs, "Sottoconto")
            CP3.Decodfica(ConnessioniGenerale)

            MoveToDb(WrRs.Fields("DescrizioneDare"), CP3.Descrizione)
            If tipo = "A" Or tipo = "C" Then
                MoveToDb(WrRs.Fields("ImportoDare"), MoveFromDbWC(WrDRs, "Importo"))
            Else
                MoveToDb(WrRs.Fields("ImportoDare"), MoveFromDbWC(WrDRs, "Importo") * -1)
            End If
            MoveToDb(WrRs.Fields("AsteriscoDare"), "")
            MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)

            MoveToDb(WrRs.Fields("Ragruppamento"), 0)
            Dim CP4 As New Cls_Pianodeiconti

            CP4.Mastro = MoveFromDbWC(WrDRs, "Mastro")
            CP4.Conto = MoveFromDbWC(WrDRs, "Conto")
            CP4.Sottoconto = MoveFromDbWC(WrDRs, "Sottoconto")
            CP4.Decodfica(ConnessioniGenerale)

            If CP4.Tipo = "W" Or CP4.Tipo = "Q" Then
                MoveToDb(WrRs.Fields("Ragruppamento"), 1)
            End If
            MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
            WrRs.Update()

            OldMastroD = MoveFromDbWC(WrDRs, "Mastro")
            OldContoD = MoveFromDbWC(WrDRs, "Conto")
            If tipo = "A" Or tipo = "C" Then
                TotImportoD = TotImportoD + MoveFromDbWC(WrDRs, "Importo")
            Else
                TotImportoD = TotImportoD - MoveFromDbWC(WrDRs, "Importo")
            End If

            WrDRs.MoveNext()
        Loop


        WrRs.AddNew()
        MoveToDb(WrRs.Fields("tipo"), tipo)
        MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
        MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
        MoveToDb(WrRs.Fields("SottoContoDare"), 0)

        Dim CP5 As New Cls_Pianodeiconti

        CP5.Mastro = OldMastroD
        CP5.Conto = OldContoD
        CP5.Sottoconto = 0
        CP5.Decodfica(ConnessioniGenerale)

        MoveToDb(WrRs.Fields("DescrizioneDare"), CP5.Descrizione)
        MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
        MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
        MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
        MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)

        MoveToDb(WrRs.Fields("Ragruppamento"), 0)
        If CP5.Tipo = "W" Or CP5.Tipo = "Q" Then
            MoveToDb(WrRs.Fields("Ragruppamento"), 1)
        End If

        MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
        WrRs.Update()
        TotImportoD = 0

        WrDRs.Close()


        OldMastroD = 0
        OldContoD = 0
        TotImportoD = 0
        OldMastroA = 0
        OldContoA = 0
        TotImportoA = 0

        MySql = "SELECT * FROM BilancioSezioniContrapposteAvere Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ORDER BY Mastro, Conto, Sottoconto"
        WrDRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        Do Until WrDRs.EOF

            If OldMastroD <> 0 And (OldMastroD <> MoveFromDbWC(WrDRs, "Mastro") Or OldContoD <> MoveFromDbWC(WrDRs, "Conto")) Then
                WrRs.AddNew()
                MoveToDb(WrRs.Fields("tipo"), tipo)
                MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
                MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
                MoveToDb(WrRs.Fields("SottoContoDare"), 0)

                Dim CP6 As New Cls_Pianodeiconti

                CP6.Mastro = OldMastroD
                CP6.Conto = OldContoD
                CP6.Sottoconto = 0
                CP6.Decodfica(ConnessioniGenerale)

                MoveToDb(WrRs.Fields("DescrizioneDare"), CP6.Descrizione)
                MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
                MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
                MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)

                MoveToDb(WrRs.Fields("Ragruppamento"), 0)
                If CP6.Tipo = "W" Or CP6.Tipo = "Q" Then
                    MoveToDb(WrRs.Fields("Ragruppamento"), 1)
                End If

                MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                WrRs.Update()
                TotImportoD = 0
            End If


            WrRs.AddNew()
            MoveToDb(WrRs.Fields("tipo"), tipo)
            MoveToDb(WrRs.Fields("MastroDare"), MoveFromDbWC(WrDRs, "Mastro"))
            MoveToDb(WrRs.Fields("ContoDare"), MoveFromDbWC(WrDRs, "Conto"))
            MoveToDb(WrRs.Fields("SottoContoDare"), MoveFromDbWC(WrDRs, "Sottoconto"))


            Dim CP7 As New Cls_Pianodeiconti

            CP7.Mastro = MoveFromDbWC(WrDRs, "Mastro")
            CP7.Conto = MoveFromDbWC(WrDRs, "Conto")
            CP7.Sottoconto = MoveFromDbWC(WrDRs, "Sottoconto")
            CP7.Decodfica(ConnessioniGenerale)

            MoveToDb(WrRs.Fields("DescrizioneDare"), CP7.Descrizione)
            If tipo = "A" Or tipo = "C" Then
                MoveToDb(WrRs.Fields("ImportoDare"), MoveFromDbWC(WrDRs, "Importo") * -1)
            Else
                MoveToDb(WrRs.Fields("ImportoDare"), MoveFromDbWC(WrDRs, "Importo"))
            End If
            MoveToDb(WrRs.Fields("AsteriscoDare"), "")
            MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)

            MoveToDb(WrRs.Fields("Ragruppamento"), 0)

            Dim CP8 As New Cls_Pianodeiconti

            CP8.Mastro = MoveFromDbWC(WrDRs, "Mastro")
            CP8.Conto = MoveFromDbWC(WrDRs, "Conto")
            CP8.Sottoconto = MoveFromDbWC(WrDRs, "Sottoconto")
            CP8.Decodfica(ConnessioniGenerale)


            If CP8.Tipo = "W" Or CP8.Tipo = "Q" Then
                MoveToDb(WrRs.Fields("Ragruppamento"), 1)
            End If
            MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
            WrRs.Update()

            OldMastroD = MoveFromDbWC(WrDRs, "Mastro")
            OldContoD = MoveFromDbWC(WrDRs, "Conto")
            If tipo = "A" Or tipo = "C" Then
                TotImportoD = TotImportoD - MoveFromDbWC(WrDRs, "Importo")
            Else
                TotImportoD = TotImportoD + MoveFromDbWC(WrDRs, "Importo")
            End If

            WrDRs.MoveNext()
        Loop


        WrRs.AddNew()
        MoveToDb(WrRs.Fields("tipo"), tipo)
        MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
        MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
        MoveToDb(WrRs.Fields("SottoContoDare"), 0)

        Dim CP9 As New Cls_Pianodeiconti

        CP9.Mastro = OldMastroD
        CP9.Conto = OldContoD
        CP9.Sottoconto = 0
        CP9.Decodfica(ConnessioniGenerale)


        MoveToDb(WrRs.Fields("DescrizioneDare"), CP9.Descrizione)
        MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
        MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
        MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
        MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)

        MoveToDb(WrRs.Fields("Ragruppamento"), 0)
        If CP9.Tipo = "W" Or CP9.Tipo = "Q" Then
            MoveToDb(WrRs.Fields("Ragruppamento"), 1)
        End If
        MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))

        WrRs.Update()
        TotImportoD = 0



        WrDRs.Close()
    End Sub




    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long, ByVal data As Object) As Double
        Dim MCRs As New ADODB.Recordset
        Dim Condizione As String
        Dim MySql As String
        Dim Dare, Avere As Double

        Dim cn As OleDbConnection

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = Data


        cn = New Data.OleDb.OleDbConnection(SessioneTP("DC_GENERALE"))

        cn.Open()


        Condizione = ""
        If Chk_AnnoCompetenza.Checked = True Then
            Condizione = " And (Competenza IS NULL OR Competenza = '')"
        End If

        If Chk_SenzaMovimentiChiusura.Checked = True Then
            Condizione = Condizione & " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        End If


        MySql = "SELECT  SUM(case when DAREAVERE ='D' THEN IMPORTO else 0 end) as TD,SUM(case when DAREAVERE ='A' THEN IMPORTO else 0 end) as TA FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & Sottoconto & _
                    " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " AND DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'}" & Condizione

        If CondizioneCserv() <> "" Then
            MySql = MySql & " And " & CondizioneCserv()
        End If
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dare = campodbN(myPOSTreader.Item("TD"))
            Avere = campodbN(myPOSTreader.Item("TA"))
        End If
        myPOSTreader.Close()


        cn.Close()


        'MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
        '            " INNER JOIN MovimentiContabiliRiga " & _
        '            " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '            " WHERE MastroPartita = " & Mastro & _
        '            " AND ContoPartita = " & Conto & _
        '            " AND SottocontoPartita = " & Sottoconto & _
        '            " AND DareAvere = 'D' " & _
        '            " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
        '            " AND DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'}" & Condizione

        'MCRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MCRs.EOF Then
        '    Dare = Modulo.MathRound(MoveFromDbWC(MCRs, "Totale"), 2)
        '    End If
        'MCRs.Close()


        'MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
        '        " INNER JOIN MovimentiContabiliRiga " & _
        '        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '        " WHERE MastroPartita = " & Mastro & _
        '        " AND ContoPartita = " & Conto & _
        '        " AND SottocontoPartita = " & Sottoconto & _
        '        " AND DareAvere = 'A' " & _
        '        " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
        '        " AND DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'}" & Condizione

        'MCRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MCRs.EOF Then
        '    Avere = Modulo.MathRound(MoveFromDbWC(MCRs, "Totale"), 2)
        '    End If
        'MCRs.Close()


        totaleSottoConto = Modulo.MathRound(Dare - Avere, 2)

    End Function






    Private Sub CreaBilancio(ByVal tipo As String)
        Dim WSocieta As String
        Dim OldMastroD As Long
        Dim OldContoD As Long
        Dim TotImportoD As Double
        Dim OldMastroA As Long
        Dim OldContoA As Long
        Dim TotImportoA As Double
        Dim SwUpdate As Boolean
        Dim MySql As String


        Dim X As New Cls_DecodificaSocieta


        WSocieta = X.DecodificaSocieta(ConnessioniTabelle)
        OldMastroD = 0
        OldContoD = 0
        TotImportoD = 0
        OldMastroA = 0
        OldContoA = 0
        TotImportoA = 0
        SwUpdate = True
        MySql = "SELECT * FROM BilancioSezioniContrapposteDare Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ORDER BY Mastro, Conto, Sottoconto"
        WrDRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        MySql = "SELECT * FROM BilancioSezioniContrapposteAvere Where PRINTERKEY= '" & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & "' ORDER BY Mastro, Conto, Sottoconto"
        WrARs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        Do Until WrDRs.EOF And WrARs.EOF
            If SwUpdate = True Then
                WrRs.AddNew()
                SwUpdate = False
                MoveToDb(WrRs.Fields("tipo"), tipo)
                MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)
            End If
            If Not WrDRs.EOF And Val(MoveFromDbWC(WrRs, "MastroDare")) = 0 Then
                If Not MoveFromDbWC(WrDRs, "Mastro") = OldMastroD Or Not MoveFromDbWC(WrDRs, "Conto") = OldContoD Then
                    If Not Modulo.MathRound(TotImportoD, 2) = 0 Then
                        MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
                        MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
                        MoveToDb(WrRs.Fields("SottoContoDare"), 0)

                        Dim CP9 As New Cls_Pianodeiconti

                        CP9.Mastro = OldMastroD
                        CP9.Conto = OldContoD
                        CP9.Sottoconto = 0
                        CP9.Decodfica(ConnessioniGenerale)

                        MoveToDb(WrRs.Fields("DescrizioneDare"), CP9.Descrizione)

                        MoveToDb(WrRs.Fields("ImportoDare"), 0)
                        MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
                        MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
                    End If
                    OldMastroD = MoveFromDbWC(WrDRs, "Mastro")
                    OldContoD = MoveFromDbWC(WrDRs, "Conto")
                    TotImportoD = 0
                End If
                If Val(MoveFromDbWC(WrRs, "MastroDare")) = 0 Then
                    TotImportoD = Modulo.MathRound(TotImportoD + MoveFromDbWC(WrDRs, "Importo"), 2)

                    MoveToDb(WrRs.Fields("MastroDare"), MoveFromDbWC(WrDRs, "Mastro"))
                    MoveToDb(WrRs.Fields("ContoDare"), MoveFromDbWC(WrDRs, "Conto"))
                    MoveToDb(WrRs.Fields("SottoContoDare"), MoveFromDbWC(WrDRs, "SottoConto"))
                    MoveToDb(WrRs.Fields("DescrizioneDare"), MoveFromDbWC(WrDRs, "Descrizione"))
                    MoveToDb(WrRs.Fields("ImportoDare"), MoveFromDbWC(WrDRs, "Importo"))
                    MoveToDb(WrRs.Fields("AsteriscoDare"), "")

                    WrDRs.MoveNext()
                End If
            Else
                If Not Modulo.MathRound(TotImportoD, 2) = 0 Then
                    MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
                    MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
                    MoveToDb(WrRs.Fields("SottoContoDare"), 0)

                    Dim CP10 As New Cls_Pianodeiconti

                    CP10.Mastro = OldMastroD
                    CP10.Conto = OldContoD
                    CP10.Sottoconto = 0
                    CP10.Decodfica(ConnessioniGenerale)

                    MoveToDb(WrRs.Fields("DescrizioneDare"), CP10.Descrizione)

                    MoveToDb(WrRs.Fields("ImportoDare"), 0)
                    MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
                    MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
                End If
                OldMastroD = 0
                OldContoD = 0
                TotImportoD = 0
            End If
            If Not WrARs.EOF And Val(MoveFromDbWC(WrRs, "MastroAvere")) = 0 Then
                If Not MoveFromDbWC(WrARs, "Mastro") = OldMastroA Or Not MoveFromDbWC(WrARs, "Conto") = OldContoA Then
                    If Not Modulo.MathRound(TotImportoA, 2) = 0 Then
                        MoveToDb(WrRs.Fields("MastroAvere"), OldMastroA)
                        MoveToDb(WrRs.Fields("ContoAvere"), OldContoA)
                        MoveToDb(WrRs.Fields("SottoContoAvere"), 0)

                        Dim CP11 As New Cls_Pianodeiconti

                        CP11.Mastro = OldMastroA
                        CP11.Conto = OldContoA
                        CP11.Sottoconto = 0
                        CP11.Decodfica(ConnessioniGenerale)

                        MoveToDb(WrRs.Fields("DescrizioneAvere"), CP11.Descrizione)

                        MoveToDb(WrRs.Fields("ImportoAvere"), 0)
                        MoveToDb(WrRs.Fields("TotaleAvere"), TotImportoA)
                        MoveToDb(WrRs.Fields("AsteriscoAvere"), "***")
                    End If
                    OldMastroA = MoveFromDbWC(WrARs, "Mastro")
                    OldContoA = MoveFromDbWC(WrARs, "Conto")
                    TotImportoA = 0
                End If
                If MoveFromDbWC(WrRs, "MastroAvere") = 0 Then
                    TotImportoA = Modulo.MathRound(TotImportoA + MoveFromDbWC(WrARs, "Importo"), 2)

                    MoveToDb(WrRs.Fields("MastroAvere"), MoveFromDbWC(WrARs, "Mastro"))
                    MoveToDb(WrRs.Fields("ContoAvere"), MoveFromDbWC(WrARs, "Conto"))
                    MoveToDb(WrRs.Fields("SottoContoAvere"), MoveFromDbWC(WrARs, "SottoConto"))
                    MoveToDb(WrRs.Fields("DescrizioneAvere"), MoveFromDbWC(WrARs, "Descrizione"))
                    MoveToDb(WrRs.Fields("ImportoAvere"), MoveFromDbWC(WrARs, "Importo"))
                    MoveToDb(WrRs.Fields("TotaleAvere"), 0)
                    MoveToDb(WrRs.Fields("AsteriscoAvere"), "")

                    WrARs.MoveNext()
                End If
            Else
                If Not Modulo.MathRound(TotImportoA, 2) = 0 Then
                    MoveToDb(WrRs.Fields("MastroAvere"), OldMastroA)
                    MoveToDb(WrRs.Fields("ContoAvere"), OldContoA)
                    MoveToDb(WrRs.Fields("SottoContoAvere"), 0)

                    Dim CP12 As New Cls_Pianodeiconti

                    CP12.Mastro = OldMastroA
                    CP12.Conto = OldContoA
                    CP12.Sottoconto = 0
                    CP12.Decodfica(ConnessioniGenerale)


                    MoveToDb(WrRs.Fields("DescrizioneAvere"), CP12.Descrizione)

                    MoveToDb(WrRs.Fields("ImportoAvere"), 0)
                    MoveToDb(WrRs.Fields("TotaleAvere"), TotImportoA)
                    MoveToDb(WrRs.Fields("AsteriscoAvere"), "***")

                End If
                OldMastroA = 0
                OldContoA = 0
                TotImportoA = 0
            End If
            If MoveFromDbWC(WrRs, "MastroDare") <> 0 Then
                If MoveFromDbWC(WrRs, "MastroAvere") <> 0 Or WrARs.EOF Then
                    MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                    WrRs.Update()
                    SwUpdate = True
                End If
            Else
                If MoveFromDbWC(WrRs, "MastroAvere") <> 0 And WrDRs.EOF Then
                    MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
                    WrRs.Update()
                    SwUpdate = True
                End If
            End If
        Loop
        If SwUpdate = False Then
            MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
            WrRs.Update()
        End If

        If Not Modulo.MathRound(TotImportoD, 2) = 0 Or Not Modulo.MathRound(TotImportoA, 2) = 0 Then
            WrRs.AddNew()
            MoveToDb(WrRs.Fields("tipo"), tipo)
            MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)
            If Not TotImportoD = 0 Then
                MoveToDb(WrRs.Fields("MastroDare"), OldMastroD)
                MoveToDb(WrRs.Fields("ContoDare"), OldContoD)
                MoveToDb(WrRs.Fields("SottoContoDare"), 0)
                Dim CP13 As New Cls_Pianodeiconti

                CP13.Mastro = OldMastroD
                CP13.Conto = OldContoD
                CP13.Sottoconto = 0
                CP13.Decodfica(ConnessioniGenerale)


                MoveToDb(WrRs.Fields("DescrizioneDare"), CP13.Descrizione)

                MoveToDb(WrRs.Fields("ImportoDare"), 0)
                MoveToDb(WrRs.Fields("TotaleDare"), TotImportoD)
                MoveToDb(WrRs.Fields("AsteriscoDare"), "***")
            End If
            If Not Modulo.MathRound(TotImportoA, 2) = 0 Then
                MoveToDb(WrRs.Fields("MastroAvere"), OldMastroA)
                MoveToDb(WrRs.Fields("ContoAvere"), OldContoA)
                MoveToDb(WrRs.Fields("SottoContoAvere"), 0)

                Dim CP14 As New Cls_Pianodeiconti

                CP14.Mastro = OldMastroD
                CP14.Conto = OldContoD
                CP14.Sottoconto = 0
                CP14.Decodfica(ConnessioniGenerale)

                MoveToDb(WrRs.Fields("DescrizioneAvere"), CP14.Descrizione)

                MoveToDb(WrRs.Fields("ImportoAvere"), 0)
                MoveToDb(WrRs.Fields("TotaleAvere"), TotImportoA)
                MoveToDb(WrRs.Fields("AsteriscoAvere"), "***")

            End If
            MoveToDb(WrRs.Fields("PRINTERKEY"), Cache("PRINTERKEYBILANCIO" + Session.SessionID))
            WrRs.Update()
        End If
        WrDRs.Close()
        WrARs.Close()
    End Sub
    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select
    End Sub

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Public Function CampoDatiGenerali(ByVal Campo As String) As Long
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String
        MySql = "Select " & Campo & " From DatiGenerali "
        MyRs.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            CampoDatiGenerali = MoveFromDbWC(MyRs, Campo)
        End If
        MyRs.Close()
    End Function


    Private Sub DoWork(ByVal data As Object)
        Call Stampa(data)
    End Sub



    Private Sub WorkCompleted(ByVal result As IAsyncResult)
        _work = Nothing
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Lbl_Errori.Text = ""
        If Not IsDate(Txt_DataLimite.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data limite formalemente errata');", True)
            REM Lbl_Errori.Text = "Data limite formalemente errata"
            Exit Sub
        End If
        SyncLock (Session.SyncRoot())
            Session("CampoProgressBar") = ""
        End SyncLock
        Cache("CampoProgressBar" + Session.SessionID) = ""

        'Call Stampa()
        Timer1.Enabled = True

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim Appoggio As String

        Appoggio = Cache("CampoProgressBar" + Session.SessionID)
        Appoggio = Cache("CampoProgressBar" + Session.SessionID)
        If IsNothing(Appoggio) Then
            Appoggio = "FINE"
        End If
        If Appoggio.IndexOf("FINE") >= 0 Then
            Cache("CampoProgressBar" + Session.SessionID) = ""
            Cache("CampoProgressBar" + Session.SessionID) = ""
            '"



            Session("SelectionFormula") = "{BilancioSezioniContrapposte.PRINTERKEY} = " & Chr(34) & Cache("PRINTERKEYBILANCIO" + Session.SessionID) & Chr(34)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=BILANCIO&PRINTERKEY=ON','Stampe','width=800,height=600');", True)

            'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Stampa", "alert('StampaReport.aspx');", True)
            Timer1.Enabled = False
            Lbl_Waiting.Text = ""
            Exit Sub
            'Response.Redirect("StampaReport.aspx?REPORT=BILANCIO")
        End If
        If Appoggio <> "" Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br/>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & Cache("NomeOspite" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataLimite.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataLimite.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)

        ConnessioniGenerale = Session("DC_GENERALE")
        ConnessioniTabelle = Session("DC_TABELLE")
        ConnessioniOspiti = Session("DC_OSPITE")
        ConnessioneFinanziaria = Session("STAMPEFINANZIARIA")
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

 
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Txt_DataLimite.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")
        Timer1.Enabled = False


        Dim Into As New Cls_DatiGenerali

        Into.LeggiDati(Session("DC_TABELLE"))

        If Into.AttivaCServPrimanoIncassi = 1 Then

            DD_CServ.Visible = True
            lblCentroservizio.Visible = True

            DD_Struttura.Visible = True
            lblStruttura.Visible = True
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub
End Class
