﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_GestionePrenotazione" CodeFile="GestionePrenotazione.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Prenotazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript"> 

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function DialogBox(Path) {

            REDIPS.dialog.show(700, 500, '<iframe id="output" src="' + Path + '" height="490px" width="690"></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager2" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <div id="dialog-modal" title="Legami">
                <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>
            </div>

            <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Budegt -Gestione Prenotazioni</div>
                        <div class="SottoTitolo"></div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci" />&nbsp;
             <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" />&nbsp;             
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Budget.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>
                    <td
                        style="vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prenotazioni" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Prenotazioni
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" AutoPostBack="true" runat="server" Width="104px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Numero :</label>
                                    <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="104px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="679px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Tipo Atto :</label>
                                    <asp:DropDownList ID="DD_Tipo" runat="server" Width="390px"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Numero Atto :</label>
                                    <asp:TextBox ID="Txt_NumeroAtto" Width="104px" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Data Atto :</label>
                                    <asp:TextBox ID="Txt_Data" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <div style="text-align: right">
                                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" ToolTip="Inserisci (F9)"
                                                                    CommandName="Inserisci" runat="server" />
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Diminuzione" CommandName="Diminuzione" runat="Server"
                                                                ImageUrl="~/images/diminuzioni.png" Width="22px" Height="22px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Livello">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtLivello" Width="250px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Colonna">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DDColonna" runat="server">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Importo">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Descrizione">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtDescrizione" Width="300px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Riga">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblRiga" runat="server" Width="136px"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                            </asp:GridView>
                                            &nbsp;<asp:Label ID="Lbl_Errore" runat="server" ForeColor="Red" Width="888px"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
