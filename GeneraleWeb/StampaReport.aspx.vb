﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class StampaReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim rpt As New ReportDocument
        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))



        Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT"))


        If Not System.IO.File.Exists(NomeSt) Then
            Response.Redirect("Reportnonpresente.aspx?NOME=" & NomeSt)
            Exit Sub
        End If

        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT")))


        If Request.Item("XSD") = "SI" Then
            rpt.SetDataSource(Session("stampa"))
        Else
            Dim VetServer As String
            VetServer = Session("STAMPEFINANZIARIA")

            Dim Xl As Integer = VetServer.IndexOf("Data Source=")
            Xl = Xl + Len("Data Source=") + 1

            Dim Xc As Integer = VetServer.IndexOf("Initial Catalog=")
            Xc = Xc + Len("Initial Catalog=") + 1

            Dim ServerIstanza As String = Mid(VetServer, Xl, VetServer.IndexOf(";", Xl) - Xl + 1)
            Dim NomeDb As String = Mid(VetServer, Xc, VetServer.IndexOf(";", Xc) - Xc + 1)

            If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
                rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(0).SetLogon("sa", "Advenias1820!")
            Else
                rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(0).SetLogon("sa", "MASTER")
            End If


            If rpt.DataSourceConnections.Count > 1 Then
                If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
                    rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                    rpt.DataSourceConnections(1).SetLogon("sa", "Advenias1820!")
                Else
                    rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                    rpt.DataSourceConnections(1).SetLogon("sa", "MASTER")
                End If
            End If

            CrystalReportViewer1.SelectionFormula = Session("SelectionFormula")
            End If


            'rpt.VerifyDatabase()
            rpt.Refresh()

            CrystalReportViewer1.ShowAllPageIds = True



            CrystalReportViewer1.ReuseParameterValuesOnRefresh = True



            CrystalReportViewer1.DisplayGroupTree = False

            CrystalReportViewer1.ReportSource = rpt
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = True

            CrystalReportViewer1.Visible = True


            With CrystalReportViewer1
                .HasDrillUpButton = True
                .HasExportButton = True
                .HasGotoPageButton = True
                .HasPageNavigationButtons = True
                .HasPrintButton = True
                .HasRefreshButton = True
                .HasSearchButton = True
                .HasToggleGroupTreeButton = True
                .HasViewList = True
                .HasZoomFactorList = True
        End With
        Dim k As Integer = rpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext())
        Session("NumeroPagineStampate") = k
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

    End Sub
End Class
