﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_RateiRisconti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_RateoRisconto')!= null || appoggio.match('Txt_Costo')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataDal')!= null || appoggio.match('Txt_DataAl')!= null)  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DataDal", GetType(String))
        Tabella.Columns.Add("DataAl", GetType(String))
        Tabella.Columns.Add("RateiRisconti", GetType(String))
        Tabella.Columns.Add("Costo", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("RateoRisconto", GetType(String))



        Dim cn As OleDbConnection
        Dim Entrato As Double = False


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From RateiRisconti Where NumeroRegistrazione = " & Val(Request.Item("Numero"))
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("ServizioDal"))
            myriga(1) = campodb(myPOSTreader.Item("ServizioAl"))

            Dim x As New Cls_Pianodeiconti

            x.Mastro = campodbN(myPOSTreader.Item("MastroDare"))
            x.Conto = campodbN(myPOSTreader.Item("ContoDare"))
            x.Sottoconto = campodbN(myPOSTreader.Item("SottoContoDare"))
            x.Decodfica(Session("DC_GENERALE"))
            myriga(2) = x.Mastro & " " & x.Conto & " " & x.Sottoconto & " " & x.Descrizione

            x.Mastro = campodbN(myPOSTreader.Item("MastroAvere"))
            x.Conto = campodbN(myPOSTreader.Item("ContoAvere"))
            x.Sottoconto = campodbN(myPOSTreader.Item("SottoContoAvere"))
            x.Decodfica(Session("DC_GENERALE"))
            myriga(3) = x.Mastro & " " & x.Conto & " " & x.Sottoconto & " " & x.Descrizione

            myriga(4) = Format(campodbN(myPOSTreader.Item("ImportoTotale")), "#,##0.00")
            myriga(5) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(6) = campodb(myPOSTreader.Item("RateoRisconto"))

            Tabella.Rows.Add(myriga)
            Entrato = True
        Loop        
        cn.Close()
        If Entrato = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If


        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()


        EseguiJS()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then


            UpDateTable()
            Tabella = ViewState("RighePrimaNota")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(1) = 0
            myriga(2) = 0
            Tabella.Rows.Add(myriga)

            ViewState("RighePrimaNota") = Tabella
            Grid.AutoGenerateColumns = False
            Grid.DataSource = Tabella
            Grid.DataBind()



            Call EseguiJS()
        End If
    End Sub

    Private Sub faibind()
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

    End Sub



    Private Sub UpDateTable()
        Dim i As Integer

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DataDal", GetType(String))
        Tabella.Columns.Add("DataAl", GetType(String))
        Tabella.Columns.Add("RateiRisconti", GetType(String))
        Tabella.Columns.Add("Costo", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("RateoRisconto", GetType(String))


        For i = 0 To Grid.Rows.Count - 1

            Dim Txt_DataDal As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_DataDal"), TextBox)
            Dim Txt_DataAl As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_DataAl"), TextBox)
            Dim Txt_RateoRisconto As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_RateoRisconto"), TextBox)
            Dim Txt_Costo As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_Costo"), TextBox)
            Dim Txt_Importo As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_Importo"), TextBox)
            Dim Txt_Descrizione As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_Descrizione"), TextBox)

            Dim RB_Rateo As RadioButton = DirectCast(Grid.Rows(i).FindControl("RB_Rateo"), RadioButton)
            Dim RB_Risconto As RadioButton = DirectCast(Grid.Rows(i).FindControl("RB_Risconto"), RadioButton)


            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = Txt_DataDal.Text
            myriga(1) = Txt_DataAl.Text
            myriga(2) = Txt_RateoRisconto.Text
            myriga(3) = Txt_Costo.Text
            myriga(4) = Txt_Importo.Text
            myriga(5) = Txt_Descrizione.Text

            If RB_Rateo.Checked = True Then
                myriga(6) = "A"
            End If
            If RB_Risconto.Checked = True Then
                myriga(6) = "R"
            End If


            Tabella.Rows.Add(myriga)
        Next

        ViewState("RighePrimaNota") = Tabella

        Call faibind()
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim Txt_DataDal As TextBox = DirectCast(e.Row.FindControl("Txt_DataDal"), TextBox)
            Dim Txt_DataAl As TextBox = DirectCast(e.Row.FindControl("Txt_DataAl"), TextBox)
            Dim Txt_RateoRisconto As TextBox = DirectCast(e.Row.FindControl("Txt_RateoRisconto"), TextBox)
            Dim Txt_Costo As TextBox = DirectCast(e.Row.FindControl("Txt_Costo"), TextBox)
            Dim Txt_Importo As TextBox = DirectCast(e.Row.FindControl("Txt_Importo"), TextBox)
            Dim Txt_Descrizione As TextBox = DirectCast(e.Row.FindControl("Txt_Descrizione"), TextBox)
            Dim RB_Rateo As RadioButton = DirectCast(e.Row.FindControl("RB_Rateo"), RadioButton)
            Dim RB_Risconto As RadioButton = DirectCast(e.Row.FindControl("RB_Risconto"), RadioButton)



            If Not IsNothing(Txt_DataDal) Then
                Txt_DataDal.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString
            End If

            If Not IsNothing(Txt_DataAl) Then
                Txt_DataAl.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString
            End If

            If Not IsNothing(Txt_RateoRisconto) Then
                Txt_RateoRisconto.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString
            End If

            If Not IsNothing(Txt_Costo) Then
                Txt_Costo.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            End If

            If Not IsNothing(Txt_Importo) Then
                Txt_Importo.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString
            End If

            If Not IsNothing(Txt_Descrizione) Then
                Txt_Descrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString
            End If

            If Not IsNothing(RB_Rateo) Then
                If Tabella.Rows(e.Row.RowIndex).Item(6).ToString = "A" Then
                    RB_Rateo.Checked = True
                End If
                If Tabella.Rows(e.Row.RowIndex).Item(6).ToString = "R" Then
                    RB_Risconto.Checked = True
                End If
            End If


        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        Dim cn As OleDbConnection
        Dim Entrato As Double = False
        Dim MySql As String = ""


        Lbl_Erore.Text = ""
        Call UpDateTable()

        Tabella = ViewState("RighePrimaNota")

        For Max = 0 To Tabella.Rows.Count - 1
            If Not IsDate(Tabella.Rows(Max).Item(0)) Then
                Lbl_Erore.Text = "Data Dal Formalmente errata"
                Exit Sub
            End If
            If Not IsDate(Tabella.Rows(Max).Item(1)) Then
                Lbl_Erore.Text = "Data Dal Formalmente errata"
                Exit Sub
            End If
            Dim DataDal As Date
            Dim DataAl As Date
            DataDal = Tabella.Rows(Max).Item(0)
            DataAl = Tabella.Rows(Max).Item(1)
            If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then
                Lbl_Erore.Text = "Data Dal maggiore di data al"
                Exit Sub
            End If
            Dim Vettore(100) As String

            Vettore = SplitWords(Tabella.Rows(Max).Item(2))

            If Vettore.Length > 1 Then
            Else
                Lbl_Erore.Text = "Specifica conto dare"
                Exit Sub
            End If

            Vettore = SplitWords(Tabella.Rows(Max).Item(3))

            If Vettore.Length > 1 Then
            Else
                Lbl_Erore.Text = "Specifica conto avere"
                Exit Sub
            End If


        Next



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Delete from RateiRisconti where " & _
                     "NumeroRegistrazione = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Val(Request.Item("Numero")))
        cmd.Connection = cn

        cmd.ExecuteNonQuery()

        Tabella = ViewState("RighePrimaNota")

        For Max = 0 To Tabella.Rows.Count - 1
            If Not IsDBNull(Tabella.Rows(Max).Item(1)) Then
                MySql = "INSERT INTO RateiRisconti (NumeroRegistrazione,MastroDare,ContoDare,SottoContoDare,MastroAvere,ContoAvere,SottocontoAvere,ServizioDal,ServizioAl,ImportoTotale,Descrizione,RateoRisconto) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)

                cmdw.Parameters.AddWithValue("@NumeroRegistrazione", Val(Request.Item("Numero")))


                Dim Vettore(100) As String

                Vettore = SplitWords(Tabella.Rows(Max).Item(2))

                cmdw.Parameters.AddWithValue("@MastroDare", Vettore(0))
                cmdw.Parameters.AddWithValue("@ContoDare", Vettore(1))
                cmdw.Parameters.AddWithValue("@SottoContoDare", Vettore(2))

                Vettore = SplitWords(Tabella.Rows(Max).Item(3))

                cmdw.Parameters.AddWithValue("@MastroAvere", Vettore(0))
                cmdw.Parameters.AddWithValue("@ContoAvere", Vettore(1))
                cmdw.Parameters.AddWithValue("@SottoContoAvere", Vettore(2))

                cmdw.Parameters.AddWithValue("@ServizioDal", Tabella.Rows(Max).Item(0))
                cmdw.Parameters.AddWithValue("@ServizioAl", Tabella.Rows(Max).Item(1))

                cmdw.Parameters.AddWithValue("@ImportoTotale", CDbl(Tabella.Rows(Max).Item(4)))
                cmdw.Parameters.AddWithValue("@Descrizione", Tabella.Rows(Max).Item(5))


                cmdw.Parameters.AddWithValue("@RateoRisconto", Tabella.Rows(Max).Item(6))


                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next

        cn.Close()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        Tabella = ViewState("RighePrimaNota")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        Session("RighePrimaNota") = Tabella

        Call faibind()
    End Sub
End Class
