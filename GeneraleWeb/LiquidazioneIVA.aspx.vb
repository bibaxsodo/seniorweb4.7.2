﻿
Partial Class GeneraleWeb_LiquidazioneIVA
    Inherits System.Web.UI.Page
    Dim VtMese(12) As Byte
    Dim VtCodiceIva(100) As String
    Dim VtImponibile(100) As Double
    Dim VtImporto(100) As Double
    Dim VtIndetraibile(100) As Double
    Dim VtCodiceIvaAcqVnt(100) As String
    Dim VtTotaleAcqVnt(100) As Double
    Dim VtImponibileAcqVnt(100) As Double
    Dim VtImportoAcqVnt(100) As Double

    Dim Vt_Gruppi(100) As String
    Dim StampeDb As New ADODB.Connection
    Dim Trimestrale As String
    Dim Vt_TipoRegistro(100) As Long


    Private Function TrovaCodice(ByVal CodiceIva As String) As Long
        Dim Ini As Long
        Ini = 0
        Do
            Ini = Ini + 1            
        Loop While VtCodiceIva(Ini) <> CodiceIva And VtCodiceIva(Ini) <> ""
        TrovaCodice = Ini
    End Function

    Private Function TrovaCodiceAcqVnt(ByVal CodiceIva As String) As Long
        Dim Ini As Long
        Ini = 0
        Do
            Ini = Ini + 1            
        Loop While VtCodiceIvaAcqVnt(Ini) <> CodiceIva And VtCodiceIvaAcqVnt(Ini) <> ""
        TrovaCodiceAcqVnt = Ini
    End Function
    Public Function CampoDatiGenerali(ByVal Campo As String) As Object
        Dim MyRs As New ADODB.Recordset
        Dim tabelledb As New ADODB.Connection
        Dim MySql As String

        CampoDatiGenerali = ""

        tabelledb.Open(Session("DC_TABELLE"))
        MySql = "Select " & Campo & " From DatiGenerali "
        MyRs.Open(MySql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            CampoDatiGenerali = MoveFromDb(MyRs.Fields(0))
        End If
        MyRs.Close()
        tabelledb.Close()
    End Function
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub StampaVisualizzaLiquidazioneIVA(ByVal Anno As Long, ByVal Mese As Integer, ByRef Errori As String)
        Dim MyRs As New ADODB.Recordset
        Dim MyRs2 As New ADODB.Recordset
        Dim RS_MovimentiContabiliRiga As New ADODB.Recordset
        Dim Ini As Integer
        Dim RS_Prorata As New ADODB.Recordset


        Dim WSocieta As String
        Dim ImportoMaxIVA As Double
        Dim Interesse As Double
        Dim DataDal As String
        Dim DataAl As String
        Dim tabelledb As New ADODB.Connection
        Dim generaledb As New ADODB.Connection
        Dim MySql As String
        Dim Sql As String
        Dim XAnno, XMese As Integer
        Dim ImportoProgr As Double
        Dim Grid As New Cls_Grid
        Dim Indetraibile As Double
        Dim registro(100) As Integer
        Dim attivita(100) As Integer
        Dim PercentualeProrata(100) As Double
        

        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID


        Errori = ""
        tabelledb.Open(Session("DC_TABELLE"))
        generaledb.Open(Session("DC_GENERALE"))

        For I = 1 To 100
            VtCodiceIva(I) = ""
            VtImponibile(I) = 0
            VtImporto(I) = 0
            VtIndetraibile(I) = 0
        Next I

        Dim X As New Cls_DecodificaSocieta

        WSocieta = X.DecodificaSocieta(Session("DC_TABELLE"))

        ImportoMaxIVA = CampoDatiGenerali("ImportoMassimo")
        Trimestrale = CampoDatiGenerali("LiquidazioneTrimestrale")


        Interesse = 0.01

        If Mese = 13 Then
            DataDal = Anno & "-" & "01" & "-" & "01"
            DataAl = Anno & "-" & "12" & "-" & "31"
        Else
            If Trimestrale = "S" Then
                DataDal = Anno & "-" & Format(Mese - 2, "00") & "-" & "01"
            Else
                DataDal = Anno & "-" & Format(Mese, "00") & "-" & "01"
            End If
            DataAl = Anno & "-" & Format(Mese, "00") & "-" & GiorniMese(Mese, Anno)
        End If



        Dim IndiceProrata As Integer = 0
        Dim MaxIndiceProrata As Integer = 0

        Sql = "SELECT * FROM Prorata Where Anno = " & Anno - 1 & " Order by RegistroIva"
        RS_Prorata.Open(Sql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not RS_Prorata.EOF

            registro(IndiceProrata) = MoveFromDb(RS_Prorata.Fields("RegistroIva"))
            PercentualeProrata(IndiceProrata) = MoveFromDb(RS_Prorata.Fields("DefinitivoPercentuale"))
            attivita(IndiceProrata) = MoveFromDb(RS_Prorata.Fields("attivita"))

            IndiceProrata = IndiceProrata + 1
            RS_Prorata.MoveNext()
        Loop
        RS_Prorata.Close()
        MaxIndiceProrata = IndiceProrata

        If Mese = 13 Then
            XAnno = Anno - 1
            XMese = 12
        Else
            If Trimestrale = "S" Then
                XAnno = Anno
                XMese = Mese
                If XMese = 3 Then
                    XAnno = Anno - 1
                    XMese = 12
                Else
                    XMese = Mese - 3
                End If
            Else
                XAnno = Anno
                XMese = Mese
                If XMese = 1 Then
                    XAnno = Anno - 1
                    XMese = 12
                Else
                    XMese = Mese - 1
                End If
            End If
        End If

        ImportoProgr = 0
        MySql = "Select * From ProgressiviIVA Where Anno = " & XAnno & " And Mese = " & XMese & " And Gruppo = '" & DD_Gruppo.SelectedValue & "'"
        MyRs.Open(MySql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ImportoProgr = MoveFromDbWC(MyRs, "Importo")
        End If
        MyRs.Close()





        Dim Condizione As String
        Dim VenditeAcquisti As String
        Dim Ri As String
        Dim Passato As Boolean
        Dim DataPagamento As Date
        Dim Ripartizione As Double
        Dim ci As String
        Dim dt As String
        Dim ind As Long
        Dim CampoCausaleCon As Long
        Dim PercDetr As Double



        ' Fatture sospese pagate

        Condizione = ""


        MySql = "Select * From GruppoRegistriRiga Where Codice = '" & DD_Gruppo.SelectedValue & "'"
        MyRs.Open(MySql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If Condizione <> "" Then
                Condizione = Condizione & " Or "
            End If
            Condizione = Condizione & " RegistroIVA =  " & MoveFromDb(MyRs.Fields("Registro"))
            MyRs.MoveNext()
        Loop
        MyRs.Close()



        If Condizione <> "" Then
            Condizione = " And (" & Condizione & ") "
        End If

        MySql = "SELECT * FROM MovimentiContabiliTesta Where IVASospesa = 'S'  " & Condizione
        MyRs.Open(MySql, generaledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF

            Dim TipReg As New Cls_RegistroIVA
            TipReg.Tipo = Val(MoveFromDb(MyRs.Fields("RegistroIVA")))
            TipReg.Leggi(Session("DC_TABELLE"), Val(MoveFromDb(MyRs.Fields("RegistroIVA"))))

            VenditeAcquisti = TipReg.TipoAcqVnt
            Ri = TipReg.Tipo
            If Len(Trim(Ri)) = 1 Then Ri = "  " & Ri
            If Len(Trim(Ri)) = 2 Then Ri = " " & Ri
            MySql = "Select * From TabellaLegami Where  Importo > 0 And CodiceDocumento = " & MoveFromDbWC(MyRs, "NumeroRegistrazione")
            MyRs2.Open(MySql, generaledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Passato = False
            Do While Not MyRs2.EOF
                Dim LegPag As New Cls_MovimentoContabile

                LegPag.Leggi(Session("DC_GENERALE"), MoveFromDbWC(MyRs2, "CodicePagamento"))
                DataPagamento = LegPag.DataRegistrazione
                If Not IsDate(DataPagamento) Then
                    Errori = "Registrazione Errata " & MoveFromDbWC(MyRs2, "CodicePagamento") & " - " & MoveFromDbWC(MyRs, "NumeroRegistrazione")
                    Exit Sub
                End If
                If Anno = Year(DataPagamento) Then
                    If Mese = Month(DataPagamento) Or Mese = 13 Then
                        Dim LegDoc As New Cls_MovimentoContabile
                        LegPag.Leggi(Session("DC_GENERALE"), MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                        Passato = True
                        If Modulo.MathRound(LegPag.ImportoDocumento(Session("DC_TABELLE")), 2) = Modulo.MathRound(MoveFromDb(MyRs2.Fields("Importo")), 2) Then
                            Ripartizione = 1
                        Else
                            Ripartizione = LegPag.ImportoDocumento(Session("DC_TABELLE")) / MoveFromDb(MyRs2.Fields("Importo"))
                        End If

                        MySql = "Select * From MovimentiContabiliRiga Where Numero = " & MoveFromDbWC(MyRs, "NumeroRegistrazione") & " And Tipo = 'IV'"
                        RS_MovimentiContabiliRiga.Open(MySql, generaledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Do While Not RS_MovimentiContabiliRiga.EOF
                            'Grid.AddItem MoveFromDbWC(MyRs, "NumeroProtocollo") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "Detraibile") & vbTab & MoveFromDbWC(MyRs, "DataRegistrazione") & vbTab & DebitoreCreditoreDocumento(MoveFromDbWC(MyRs, "NumeroRegistrazione")) & vbTab & CampoRigaMovimentoContabile(MoveFromDbWC(MyRs, "NumeroRegistrazione"), "MastroPartita", 1) & vbTab & CampoRigaMovimentoContabile(MoveFromDbWC(MyRs, "NumeroRegistrazione"), "ContoPartita", 1) & vbTab & CampoRigaMovimentoContabile(MoveFromDbWC(MyRs, "NumeroRegistrazione"), "SottocontoPartita", 1) & vbTab & MoveFromDbWC(MyRs, "DataDocumento") & vbTab & _
                            'MoveFromDbWC(MyRs, "NumeroDocumento") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita") & vbTab & MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita") & vbTab & DecodificaConto(MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita")) & vbTab & RegistroIva
                            ci = MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva")
                            dt = MoveFromDbWC(RS_MovimentiContabiliRiga, "Detraibile")
                            If Len(Trim(ci)) = 1 Then ci = ci & " "
                            If Trim(dt) = "" Then dt = " "


                            ind = TrovaCodice(Ri & ci & dt & VenditeAcquisti)
                            VtCodiceIva(ind) = Ri & ci & dt & VenditeAcquisti 'MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva")

                            CampoCausaleCon = 0
                            If VenditeAcquisti = "V" Then
                                If (MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "A" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+") Then
                                    VtImporto(ind) = VtImporto(ind) + (MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione)
                                    VtImponibile(ind) = VtImponibile(ind) + (MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile") / Ripartizione)
                                Else
                                    VtImporto(ind) = VtImporto(ind) - (MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione)
                                    VtImponibile(ind) = VtImponibile(ind) - (MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile") / Ripartizione)
                                End If
                            Else
                                If (MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "D" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+") Then
                                    VtImporto(ind) = VtImporto(ind) + (MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione)
                                    VtImponibile(ind) = VtImponibile(ind) + (MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile") / Ripartizione)
                                Else
                                    VtImporto(ind) = VtImporto(ind) - (MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione)
                                    VtImponibile(ind) = VtImponibile(ind) - (MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile") / Ripartizione)
                                End If
                            End If
                            If Mid(VtCodiceIva(ind), 7, 1) = "V" Or Mid(VtCodiceIva(ind), 7, 1) = "C" Then PercDetr = 1

                            If Mid(VtCodiceIva(ind), 7, 1) = "A" Then
                                PercDetr = 0
                                If Mid(VtCodiceIva(ind), 6, 1) <> " " Then
                                    Dim MyDetr As New ClsDetraibilita
                                    MyDetr.Codice = Mid(VtCodiceIva(ind), 6, 1)
                                    MyDetr.Leggi(Session("DC_TABELLE"))
                                    PercDetr = MyDetr.Aliquota ' CampoDetraibilita(Mid(VtCodiceIva(ind), 6, 1), "Aliquota")
                                    If MyDetr.Prorata = 1 Then
                                        PercDetr = 1
                                    End If
                                End If
                            End If
                            If MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "D" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+" Then
                                VtIndetraibile(ind) = VtIndetraibile(ind) + Modulo.MathRound((MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione) - ((MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione) * PercDetr), 15)
                            Else
                                VtIndetraibile(ind) = VtIndetraibile(ind) - Modulo.MathRound((MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione) - ((MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo") / Ripartizione) * PercDetr), 15)
                            End If
                            RS_MovimentiContabiliRiga.MoveNext()
                        Loop
                        RS_MovimentiContabiliRiga.Close()
                    End If
                End If
                MyRs2.MoveNext()
            Loop
            MyRs2.Close()
            MyRs.MoveNext()
        Loop
        MyRs.Close()




        If Condizione = "" Then
            MySql = "SELECT MovimentiContabiliRiga.*,MovimentiContabiliTesta.RegistroIva, MovimentiContabiliTesta.TipoDocumento, MovimentiContabiliTesta.CausaleContabile " & _
                     " FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                     " WHERE (MovimentiContabiliTesta.IVASospesa Is Null " & _
                     " OR MovimentiContabiliTesta.IVASospesa <> 'S') " & _
                     " AND MovimentiContabiliRiga.Tipo = 'IV' " & _
                     " AND MovimentiContabiliTesta.DataRegistrazione >= {ts '" & DataDal & " 00:00:00'} " & _
                     " AND MovimentiContabiliTesta.DataRegistrazione <= {ts '" & DataAl & " 00:00:00'} Order By MovimentiContabiliTesta.RegistroIva,CodiceIva,Detraibile"
        Else
            MySql = "SELECT MovimentiContabiliRiga.*,MovimentiContabiliTesta.RegistroIva, MovimentiContabiliTesta.TipoDocumento, MovimentiContabiliTesta.CausaleContabile " & _
                 " FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                 " WHERE (MovimentiContabiliTesta.IVASospesa Is Null " & _
                 " OR MovimentiContabiliTesta.IVASospesa <> 'S') " & _
                 " AND MovimentiContabiliRiga.Tipo = 'IV' " & _
                 Condizione & _
                 " AND MovimentiContabiliTesta.DataRegistrazione >= {ts '" & DataDal & " 00:00:00'} " & _
                 " AND MovimentiContabiliTesta.DataRegistrazione <= {ts '" & DataAl & " 00:00:00'} Order By MovimentiContabiliTesta.RegistroIva,CodiceIva,Detraibile"
        End If


        Dim miavar, TotAc, TotAcPro, TotAcPro1, TotAcProV, TotVd, TotCr, TotAcProV1 As Double

        Dim AcquistiVendite As String
        Dim tipo As Long
        Dim TAlt As String

        miavar = 0
        TotAc = 0
        TotAcPro = 0
        TotAcProV = 0
        TotVd = 0
        TotCr = 0
        RS_MovimentiContabiliRiga.Open(MySql, generaledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not RS_MovimentiContabiliRiga.EOF
            Ri = MoveFromDb(RS_MovimentiContabiliRiga.Fields("RegistroIva"))
            If Len(Trim(Ri)) = 1 Then Ri = "  " & Ri
            If Len(Trim(Ri)) = 2 Then Ri = " " & Ri
            Dim MyRegIva As New Cls_RegistroIVA

            MyRegIva.Leggi(Session("DC_TABELLE"), Val(MoveFromDb(RS_MovimentiContabiliRiga.Fields("RegistroIva"))))

            Dim CauCont As New Cls_CausaleContabile

            CauCont.Codice = MoveFromDb(RS_MovimentiContabiliRiga.Fields("CausaleContabile"))
            CauCont.Leggi(Session("DC_TABELLE"), CauCont.Codice)


            VenditeAcquisti = MyRegIva.TipoIVA
            'AcquistiVendite = MyRegIva.TipoAcqVnt

            Dim MyCIVA As New Cls_IVA

            MyCIVA.Leggi(Session("DC_TABELLE"), MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva"))
            tipo = MyCIVA.InLiquidazione

            

            If MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva") <> "" Then
                ci = MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva")
                dt = MoveFromDbWC(RS_MovimentiContabiliRiga, "Detraibile")
                If Len(Trim(ci)) = 1 Then ci = ci & " "
                If Trim(dt) = "" Then dt = " "
                TAlt = TrovaCodice(Ri & ci & dt & VenditeAcquisti)

                VtCodiceIva(TAlt) = Ri & ci & dt & VenditeAcquisti

                If Mid(VtCodiceIva(TAlt), 7, 1) = "V" Or Mid(VtCodiceIva(TAlt), 7, 1) = "C" Then PercDetr = 1
                If Mid(VtCodiceIva(TAlt), 7, 1) = "A" Then
                    PercDetr = 0
                    If Mid(VtCodiceIva(TAlt), 6, 1) <> " " Then
                        Dim XCodD As New ClsDetraibilita

                        XCodD.Codice = Mid(VtCodiceIva(TAlt), 6, 1)
                        XCodD.Leggi(Session("DC_TABELLE"))
                        PercDetr = XCodD.Aliquota
                        If XCodD.Prorata = 1 Then
                            PercDetr = 1
                        End If
                    End If
                End If


                Dim Importo, Imponibile As Double

                Importo = MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo")
                Imponibile = MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile")
                Indetraibile = Modulo.MathRound(Importo - ((Importo * PercDetr)), 2)

        

                If CauCont.VenditaAcquisti = "A" Then
                    If MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "A" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+" Then
                        Importo = Importo * -1
                        Imponibile = Imponibile * -1
                        Indetraibile = Indetraibile * -1
                    End If
                End If
                If CauCont.VenditaAcquisti = "V" Or CauCont.VenditaAcquisti = "C" Then
                    If MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "D" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+" Then
                        Importo = Importo * -1
                        Imponibile = Imponibile * -1
                        Indetraibile = Indetraibile * -1
                    End If
                End If


                VtImporto(TAlt) = VtImporto(TAlt) + Importo
                VtImponibile(TAlt) = VtImponibile(TAlt) + Imponibile
                VtIndetraibile(TAlt) = VtIndetraibile(TAlt) + Indetraibile

                Dim XDet As New ClsDetraibilita

                XDet.Codice = MoveFromDbWC(RS_MovimentiContabiliRiga, "Detraibile")
                XDet.Leggi(Session("DC_TABELLE"))

                If XDet.Prorata = 1 Then
                    If Modulo.MathRound(Importo - Int(Importo), 2) = 0.5 Then
                        If Importo < 0 Then
                            For IndiceProrata = 0 To MaxIndiceProrata                        
                                If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 1 Then
                                    TotAcProV = TotAcProV + Modulo.MathRound((Importo + 0.01) * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                                End If
                                If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 2 Then
                                    TotAcProV1 = TotAcProV1 + Modulo.MathRound((Importo + 0.01) * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                                End If
                            Next
                        Else
                            For IndiceProrata = 0 To MaxIndiceProrata
                                If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 1 Then
                                    TotAcProV = TotAcProV + Modulo.MathRound((Importo - 0.01) * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                                End If
                                If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 2 Then
                                    TotAcProV1 = TotAcProV1 + Modulo.MathRound((Importo - 0.01) * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                                End If
                            Next
                        End If
                    Else
                        For IndiceProrata = 0 To MaxIndiceProrata
                            If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 1 Then
                                TotAcProV = TotAcProV + Modulo.MathRound(Importo * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                            End If
                            If registro(IndiceProrata) = MoveFromDbWC(RS_MovimentiContabiliRiga, "RegistroIva") And attivita(IndiceProrata) = 2 Then
                                TotAcProV1 = TotAcProV1 + Modulo.MathRound(Importo * Modulo.MathRound(PercentualeProrata(IndiceProrata), 2), 2)
                            End If
                        Next
                    End If
                End If

                End If
            RS_MovimentiContabiliRiga.MoveNext()
        Loop
        RS_MovimentiContabiliRiga.Close()

        Dim Imposta As Double

        Ini = 0
        Do
            Ini = Ini + 1

            PercDetr = 0
            If Mid(VtCodiceIva(Ini), 7, 1) = "V" Or Mid(VtCodiceIva(Ini), 7, 1) = "C" Then PercDetr = 1
            If Mid(VtCodiceIva(Ini), 7, 1) = "A" Then
                If Mid(VtCodiceIva(Ini), 6, 1) <> " " Then
                    Dim Xd As New ClsDetraibilita
                    Xd.Codice = Mid(VtCodiceIva(Ini), 6, 1)
                    Xd.Leggi(Session("DC_TABELLE"))
                    PercDetr = Xd.Aliquota
                    If Xd.Prorata = 1 Then
                        PercDetr = 1
                    End If
                End If
            End If
            Indetraibile = VtIndetraibile(Ini) ' Modulo.MathRound(VtImporto(Ini) - (VtImporto(Ini) * PercDetr), 15)

            Imposta = Modulo.MathRound(VtImporto(Ini) - VtIndetraibile(Ini), 2)   'Round(VtImporto(Ini) - Indetraibile, 15)
            'Imposta = Modulo.MathRound(VtImporto(Ini) - Indetraibile, 15)

            If Mid(VtCodiceIva(Ini), 7, 1) = "V" Then TotVd = TotVd + Imposta
            If Mid(VtCodiceIva(Ini), 7, 1) = "A" Then
                TotAc = TotAc + Imposta
                Dim Xd As New ClsDetraibilita
                Xd.Codice = Mid(VtCodiceIva(Ini), 6, 1)
                Xd.Leggi(Session("DC_TABELLE"))
                If Xd.Prorata = 0 Then
                    TotAcPro = TotAcPro + Imposta
                End If
            End If

            If Mid(VtCodiceIva(Ini), 7, 1) = "C" Then
                Dim DcIVA As New Cls_IVA

                DcIVA.Leggi(Session("DC_TABELLE"), Mid(VtCodiceIva(Ini), 4, 2))
                If Trim(DcIVA.Tipo) = "" And VtImponibile(Ini) <> 0 And Imposta = 0 Then
                    Dim MyReg As New Cls_RegistroIVA
                    MyReg.Leggi(Session("DC_TABELLE"), Mid(VtCodiceIva(Ini), 1, 3))
                    TotCr = TotCr + VentilazioneIVA(Anno, Mese, Mid(VtCodiceIva(Ini), 1, 3), MyReg.TipoAcqVnt, Ini, Grid)
                Else
                    TotCr = TotCr + Imposta
                End If
            End If
            Imposta = Modulo.MathRound(Imposta, 2)
            Indetraibile = Modulo.MathRound(Indetraibile, 2)

            Dim DecIVA As New Cls_IVA

            DecIVA.Leggi(Session("DC_Tabelle"), Mid(VtCodiceIva(Ini), 4, 2))
            If Trim(DecIVA.Tipo) = "" Then
                Grid.AddItem(Mid(VtCodiceIva(Ini), 4, 2) & vbTab & DecIVA.Descrizione & vbTab & VtImponibile(Ini) & vbTab & DecIVA.Aliquota * 100 & vbTab & Imposta & vbTab & Indetraibile & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & Mid(VtCodiceIva(Ini), 7, 1) & vbTab & Mid(VtCodiceIva(Ini), 6, 1) & vbTab & Mid(VtCodiceIva(Ini), 1, 3))
            End If
            If DecIVA.Tipo = "ES" Then
                Grid.AddItem(Mid(VtCodiceIva(Ini), 4, 2) & vbTab & DecIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibile(Ini) & vbTab & 0 & vbTab & 0 & vbTab & Mid(VtCodiceIva(Ini), 7, 1) & vbTab & Mid(VtCodiceIva(Ini), 6, 1) & vbTab & Mid(VtCodiceIva(Ini), 1, 3))
            End If
            If DecIVA.Tipo = "NS" Then
                Grid.AddItem(Mid(VtCodiceIva(Ini), 4, 2) & vbTab & DecIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibile(Ini) & vbTab & 0 & vbTab & Mid(VtCodiceIva(Ini), 7, 1) & vbTab & Mid(VtCodiceIva(Ini), 6, 1) & vbTab & Mid(VtCodiceIva(Ini), 1, 3))
            End If
            If DecIVA.Tipo = "NI" Then
                Grid.AddItem(Mid(VtCodiceIva(Ini), 4, 2) & vbTab & DecIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibile(Ini) & vbTab & Mid(VtCodiceIva(Ini), 7, 1) & vbTab & Mid(VtCodiceIva(Ini), 6, 1) & vbTab & Mid(VtCodiceIva(Ini), 1, 3))
            End If

        Loop While VtCodiceIva(Ini) <> ""

        Dim IvaEsigibile As Double
        Dim Ivacorrispettivi As Double
        Dim IvaDetraibile As Double

        If Mese = 13 Then

            If 2 = 0 Then
                IvaEsigibile = Int(TotVd / 1000) * 1000
                If TotVd - IvaEsigibile > 499 Then IvaEsigibile = IvaEsigibile + 1000
                Grid.AddItem(vbTab & "IVA esigibile per il periodo : " & vbTab & Modulo.MathRound(IvaEsigibile, 2))
                Ivacorrispettivi = Int(TotCr / 1000) * 1000
                If TotCr - Ivacorrispettivi > 499 Then Ivacorrispettivi = Ivacorrispettivi + 1000
                Grid.AddItem(vbTab & "IVA corrispettivi per il periodo : " & vbTab & Modulo.MathRound(Ivacorrispettivi, 2))
                IvaDetraibile = Int(TotAc / 1000) * 1000
                If TotAc - IvaDetraibile > 499 Then IvaDetraibile = IvaDetraibile + 1000
            Else
                If Chk_ArrotondaImporti.Checked = True Then
                    IvaEsigibile = Int(TotVd)
                    If TotVd - IvaEsigibile > 0.499 Then IvaEsigibile = IvaEsigibile + 1
                    Grid.AddItem(vbTab & "IVA esigibile per il periodo : " & vbTab & Modulo.MathRound(IvaEsigibile, 2))
                    Ivacorrispettivi = Int(TotCr)
                    If TotCr - Ivacorrispettivi > 0.499 Then Ivacorrispettivi = Ivacorrispettivi + 1
                    Grid.AddItem(vbTab & "IVA corrispettivi per il periodo : " & vbTab & Modulo.MathRound(Ivacorrispettivi, 2))
                    IvaDetraibile = Int(TotAc)
                    If TotAc - IvaDetraibile > 0.499 Then IvaDetraibile = IvaDetraibile + 1
                Else
                    IvaEsigibile = Modulo.MathRound(TotVd, 2)
                    Grid.AddItem(vbTab & "IVA esigibile per il periodo : " & vbTab & Modulo.MathRound(IvaEsigibile, 2))
                    Ivacorrispettivi = Modulo.MathRound(TotCr, 2)
                    Grid.AddItem(vbTab & "IVA corrispettivi per il periodo : " & vbTab & Modulo.MathRound(Ivacorrispettivi, 2))
                    IvaDetraibile = Modulo.MathRound(TotAc, 2)
                End If
            End If
        Else
            IvaEsigibile = TotVd
            Grid.AddItem(vbTab & "IVA esigibile per il periodo : " & vbTab & Modulo.MathRound(IvaEsigibile, 2))
            Ivacorrispettivi = TotCr
            Grid.AddItem(vbTab & "IVA corrispettivi per il periodo : " & vbTab & Modulo.MathRound(Ivacorrispettivi, 2))
            IvaDetraibile = TotAcPro
        End If

        Dim AppoggioProrata As Double
        AppoggioProrata = TotAcProV
        Grid.AddItem(vbTab & "Detrazione Prorata : " & vbTab & Modulo.MathRound(AppoggioProrata, 2))
        AppoggioProrata = AppoggioProrata + TotAcProV1

        If Modulo.MathRound((TotAcPro + TotAcPro1 + AppoggioProrata) - Modulo.MathRound(TotAcPro + TotAcPro1 + AppoggioProrata, 2), 5) < 0 Then
            IvaDetraibile = Modulo.MathRound(TotAcPro + TotAcPro1 + AppoggioProrata, 2) - 0.01
        Else
            IvaDetraibile = Modulo.MathRound(TotAcPro + TotAcPro1 + AppoggioProrata, 2)
        End If

        Grid.AddItem(vbTab & "IVA da detrarre per il periodo : " & vbTab & Modulo.MathRound(IvaDetraibile, 2))


        REM ****************************************************************************************************
        REM * INIZIO Collegamento con registrazioni Contabili e liquidazione IVA
        REM ****************************************************************************************************


        Dim ImportoCC As Double
        Dim TipoGruppo As Long

        ImportoCC = 0


        TipoGruppo = 0
        Dim RsGruppo As New ADODB.Recordset

        MySql = "Select * From GruppoRegistriTesta Where Codice = '" & DD_Gruppo.SelectedValue & "'"
        RsGruppo.Open(MySql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not RsGruppo.EOF Then
            TipoGruppo = MoveFromDb(RsGruppo.Fields("ConsideraInLiquidazioni"))
        End If
        RsGruppo.Close()


        'If Val(CampoParametriFinanziaria("MastroPerLiquidazione")) <> 0 And TipoGruppo = 1 Then

        '    Dim XRs As New ADODB.Recordset

        '    MySql = "SELECT sum(Importo)  " & _
        '            " FROM " & NomeDataGenerale & "MovimentiContabiliTesta ," & NomeDataGenerale & "MovimentiContabiliRiga WHERE MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '            " AND (IVASospesa Is Null " & _
        '            " OR IVASospesa <> 'S') " & _
        '            " AND PerLiquidazione = 1" & _
        '            " AND DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-mm-dd") & " 00:00:00'}" & _
        '            " AND DataRegistrazione <= {ts '" & Format(DataAl, "yyyy-mm-dd") & " 00:00:00'} And DareAvere = 'D' " & _
        '            " And MastroPartita = " & CampoParametriFinanziaria("MastroPerLiquidazione") & _
        '            " And ContoPartita = " & CampoParametriFinanziaria("ContoPerLiquidazione") & _
        '            " And SottoContoPartita = " & CampoParametriFinanziaria("SottoContoPerLiquidazione")
        '    XRs.Open(MySql, generaledb, adOpenKeyset, adLockOptimistic)
        '    ImportoCC = MoveFromDb(XRs.Fields(0))
        '    XRs.Close()


        '    MySql = "SELECT sum(Importo)  " & _
        '            " FROM " & NomeDataGenerale & "MovimentiContabiliTesta ," & NomeDataGenerale & "MovimentiContabiliRiga WHERE MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '            " AND (IVASospesa Is Null " & _
        '            " OR IVASospesa <> 'S') " & _
        '            " AND PerLiquidazione = 1" & _
        '            " AND DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-mm-dd") & " 00:00:00'}" & _
        '            " AND DataRegistrazione <= {ts '" & Format(DataAl, "yyyy-mm-dd") & " 00:00:00'} And DareAvere = 'A' " & _
        '            " And MastroPartita = " & CampoParametriFinanziaria("MastroPerLiquidazione") & _
        '            " And ContoPartita = " & CampoParametriFinanziaria("ContoPerLiquidazione") & _
        '            " And SottoContoPartita = " & CampoParametriFinanziaria("SottoContoPerLiquidazione")
        '    XRs.Open(MySql, generaledb, adOpenKeyset, adLockOptimistic)
        '    ImportoCC = ImportoCC - MoveFromDb(XRs.Fields(0))
        '    XRs.Close()

        'End If

        'ImportoCC = 0

        REM ****************************************************************************************************
        REM * FINE
        REM ****************************************************************************************************
        Dim IvaDebitoCredito As Double
        Dim IvaDovutaDritta As Double
        Dim InteressiDovuti As Double
        Dim ImportoDaVersare As Double

        IvaDebitoCredito = (IvaEsigibile + Ivacorrispettivi - IvaDetraibile) - ImportoCC
        If IvaDebitoCredito < 0 Then
            Grid.AddItem(vbTab & "Credito d'imposta periodo in corso : " & vbTab & Modulo.MathRound(Math.Abs(IvaDebitoCredito), 2))
        Else
            Grid.AddItem(vbTab & "Debito d'imposta periodo in corso : " & vbTab & Modulo.MathRound(IvaDebitoCredito, 2))
        End If
        If ImportoProgr < 0 Then
            Grid.AddItem(vbTab & "Credito d'imposta periodo precedente : " & vbTab & Modulo.MathRound(Math.Abs(ImportoProgr), 2))
        Else
            Grid.AddItem(vbTab & "Debito d'imposta periodo precedente : " & vbTab & Modulo.MathRound(ImportoProgr, 2))
        End If
        IvaDovutaDritta = IvaDebitoCredito + ImportoProgr
        If IvaDovutaDritta < 0 Then
            Grid.AddItem(vbTab & "IVA a credito per il periodo : " & vbTab & Modulo.MathRound(Math.Abs(IvaDovutaDritta), 2))
        Else
            Grid.AddItem(vbTab & "IVA dovuta per il periodo : " & vbTab & Modulo.MathRound(IvaDovutaDritta, 2))
        End If
        InteressiDovuti = 0
        ImportoDaVersare = 0
        If IvaDovutaDritta > 0 Then
            If Trimestrale = "S" Then
                InteressiDovuti = Modulo.MathRound(IvaDovutaDritta * Interesse, 2)
                ImportoDaVersare = IvaDovutaDritta + InteressiDovuti
                Grid.AddItem(vbTab & "Interessi dovuti per liquidazioni trimestrali : " & vbTab & InteressiDovuti)
            Else
                ImportoDaVersare = IvaDovutaDritta
            End If
        End If
        If ImportoMaxIVA > ImportoDaVersare Then
            ImportoDaVersare = 0
        End If
        Grid.AddItem(vbTab & "Importo da versare : " & vbTab & ImportoDaVersare)

        StampeDb.Open(Session("STAMPEFINANZIARIA"))
        StampeDb.Execute("Delete From StampaLiquidazione where PRINTERKEY = '" & ViewState("PRINTERKEY") & "'")
        Dim MaxRiga As Long
        Dim RegistraUnaRiga As Boolean

        MyRs.Open("Select * From StampaLiquidazione where PRINTERKEY = '" & ViewState("PRINTERKEY") & "'", StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Trimestrale = "S" Then
            MaxRiga = 9
        Else
            MaxRiga = 8
        End If
        If Math.Abs(TotAcProV) > 0 Then
            MaxRiga = MaxRiga + 1
        End If
        If CDbl(Txt_IvaPagata.Text) > 0 Then
            REM MaxRiga = MaxRiga + 1
        End If
        'For i = 4 To Grid.Rows - MaxRiga
        RegistraUnaRiga = False
        For I = 1 To Grid.Rows - MaxRiga
            Grid.Row = I

            If Grid.TextMatrix(I, 4) <> "" Or Grid.TextMatrix(I, 5) <> "" Or Grid.TextMatrix(I, 6) <> "" Or Grid.TextMatrix(I, 7) <> "" Or Grid.TextMatrix(I, 8) <> "" Then
                If CDbl(Grid.TextMatrix(I, 4)) <> 0 Or CDbl(Grid.TextMatrix(I, 5)) <> 0 Or CDbl(Grid.TextMatrix(I, 6)) <> 0 Or CDbl(Grid.TextMatrix(I, 7)) <> 0 Or CDbl(Grid.TextMatrix(I, 8)) <> 0 Then
                    MyRs.AddNew()
                    Grid.Col = 0 : MoveToDb(MyRs.Fields("codice"), Grid.Text)
                    Grid.Col = 1 : MoveToDb(MyRs.Fields("Descrizione"), Grid.Text)
                    Grid.Col = 2 : MoveToDb(MyRs.Fields("Imponibile"), Grid.Text)
                    Grid.Col = 3 : MoveToDb(MyRs.Fields("Aliquota"), Grid.Text)
                    Grid.Col = 4 : MoveToDb(MyRs.Fields("Imposta"), Grid.Text)
                    Grid.Col = 5 : MoveToDb(MyRs.Fields("Indetraibile"), Grid.Text)
                    Grid.Col = 6 : MoveToDb(MyRs.Fields("Esenti"), Grid.Text)
                    Grid.Col = 7 : MoveToDb(MyRs.Fields("Nonsoggetti"), Grid.Text)
                    Grid.Col = 8 : MoveToDb(MyRs.Fields("NonImpobile"), Grid.Text)
                    Grid.Col = 9 : MoveToDb(MyRs.Fields("VA"), Grid.Text)
                    Grid.Col = 10 : MoveToDb(MyRs.Fields("CodiceDetrazione"), Grid.Text)
                    Grid.Col = 11 : MoveToDb(MyRs.Fields("RegistroIva"), Grid.Text)


                    Dim RegistroIVA As New Cls_RegistroIVA

                    RegistroIVA.Tipo = Val(Grid.Text)
                    RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)



                    MoveToDb(MyRs.Fields("DescrizioneRegistro"), RegistroIVA.Descrizione)
                    MoveToDb(MyRs.Fields("Indicatore"), RegistroIVA.IndicatoreRegistro)
                    MoveToDb(MyRs.Fields("DescrizioneGruppo"), DD_Gruppo.SelectedItem.Text)

                    MoveToDb(MyRs.Fields("Anno"), Anno)
                    MoveToDb(MyRs.Fields("IvaEsigibile"), IvaEsigibile)
                    MoveToDb(MyRs.Fields("IvaCorrispettivi"), Ivacorrispettivi)
                    MoveToDb(MyRs.Fields("IvaDetraibile"), IvaDetraibile)
                    MoveToDb(MyRs.Fields("IvaDebitoCredito"), IvaDebitoCredito)
                    MoveToDb(MyRs.Fields("ImportoProgr"), ImportoProgr)
                    MoveToDb(MyRs.Fields("IvaDovutaDritta"), IvaDovutaDritta)
                    MoveToDb(MyRs.Fields("InteressiDovuti"), InteressiDovuti)
                    MoveToDb(MyRs.Fields("ImportoDaVersare"), ImportoDaVersare)
                    MoveToDb(MyRs.Fields("Periodo"), DD_Mese.SelectedItem.Text & " " & Anno)
                    MoveToDb(MyRs.Fields("IntestaSocieta"), WSocieta)
                    MoveToDb(MyRs.Fields("Gruppo"), DD_Gruppo.Text)
                    MoveToDb(MyRs.Fields("IvaCompensazione"), txt_compensazioneimporto.Text)
                    MoveToDb(MyRs.Fields("ImportoPagato"), Txt_IvaPagata.Text)
                    MoveToDb(MyRs.Fields("DetrazioneProrata"), AppoggioProrata)
                    MoveToDb(MyRs.Fields("IvaAcconto"), Txt_IvaAcconto.Text)
                    MoveToDb(MyRs.Fields("Prorata1"), TotAcProV)
                    MoveToDb(MyRs.Fields("Prorata2"), TotAcProV1)
                    MoveToDb(MyRs.Fields("NUMEROPAGINA"), Val(Txt_NumeroPagina.text))

                    MoveToDb(MyRs.Fields("PRINTERKEY"), ViewState("PRINTERKEY"))
                    'If Chk_StampaNumeroPagina.Value = 1 Then
                    '    MoveToDb(MyRs!NUMEROPAGINA, Txt_NumeroPagina.Text)
                    'End If

                    MyRs.Update()
                    RegistraUnaRiga = True
                End If
            End If
        Next I

        If RegistraUnaRiga = False Then
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Anno"), Anno)
            MoveToDb(MyRs.Fields("Periodo"), DD_Mese.SelectedItem.Text & " " & Anno)
            MoveToDb(MyRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(MyRs.Fields("Gruppo"), DD_Gruppo.SelectedItem.Text)
            MoveToDb(MyRs.Fields("Anno"), Anno)
            MoveToDb(MyRs.Fields("IvaEsigibile"), IvaEsigibile)
            MoveToDb(MyRs.Fields("IvaCorrispettivi"), Ivacorrispettivi)
            MoveToDb(MyRs.Fields("IvaDetraibile"), IvaDetraibile)
            MoveToDb(MyRs.Fields("IvaDebitoCredito"), IvaDebitoCredito)
            MoveToDb(MyRs.Fields("ImportoProgr"), ImportoProgr)
            MoveToDb(MyRs.Fields("IvaDovutaDritta"), IvaDovutaDritta)
            MoveToDb(MyRs.Fields("InteressiDovuti"), InteressiDovuti)
            MoveToDb(MyRs.Fields("ImportoDaVersare"), ImportoDaVersare)
            MoveToDb(MyRs.Fields("Periodo"), DD_Mese.SelectedItem.Text & " " & Anno)
            MoveToDb(MyRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(MyRs.Fields("Gruppo"), DD_Gruppo.SelectedItem.Text)
            MoveToDb(MyRs.Fields("IvaCompensazione"), txt_compensazioneimporto.Text)
            MoveToDb(MyRs.Fields("ImportoPagato"), Txt_IvaPagata.Text)
            MoveToDb(MyRs.Fields("DetrazioneProrata"), AppoggioProrata)
            MoveToDb(MyRs.Fields("IvaAcconto"), Txt_IvaAcconto.Text)
            MoveToDb(MyRs.Fields("Prorata1"), TotAcProV)
            MoveToDb(MyRs.Fields("Prorata2"), TotAcProV1)
            MoveToDb(MyRs.Fields("PRINTERKEY"), ViewState("PRINTERKEY"))
            MoveToDb(MyRs.Fields("NUMEROPAGINA"), Val(Txt_NumeroPagina.text))

            'If Chk_StampaNumeroPagina.Value = 1 Then
            '    MoveToDb(MyRs!NUMEROPAGINA, Txt_NumeroPagina.Text)
            'End If
            MyRs.Update()
        End If
        MyRs.Close()
        StampeDb.Close()

        If Mese = 13 Then Mese = 12

        'MySql = "Select * From ProgressiviIVA Where Anno = " & Anno & " And Mese = " & Mese
        MySql = "Select * From ProgressiviIVA Where Anno = " & Anno & " And Mese = " & Mese & " And Gruppo = '" & DD_Gruppo.SelectedValue & "'"
        MyRs.Open(MySql, tabelledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            MyRs.AddNew()
        End If

        If Val(Txt_IvaPagata.Text) > 0 Then
            ImportoDaVersare = ImportoDaVersare - Txt_IvaPagata.Text
            IvaDovutaDritta = IvaDovutaDritta - Txt_IvaPagata.Text
        End If

        MoveToDb(MyRs.Fields("Anno"), Anno)
        MoveToDb(MyRs.Fields("Mese"), Mese)
        MoveToDb(MyRs.Fields("Gruppo"), DD_Gruppo.SelectedValue)

        If ImportoDaVersare > 0 Then
            MoveToDb(MyRs.Fields("Importo"), 0)
            'MoveToDb MyRs!importo, IvaDovutaDritta - Modulo.MathRound(txt_compensazioneimporto.Text, 2)
        Else
            MoveToDb(MyRs.Fields("Importo"), IvaDovutaDritta - Modulo.MathRound(CDbl(txt_compensazioneimporto.Text), 2))
            If DD_Mese.Text = "13° Liquidazione" Then
                MoveToDb(MyRs.Fields("Importo"), IvaDovutaDritta - Modulo.MathRound(CDbl(txt_compensazioneimporto.Text), 2) - Txt_IvaPagata.Text - Txt_IvaAcconto.Text)
            End If
        End If

        MyRs.Update()
        MyRs.Close()
    End Sub

    Private Function VentilazioneIVA(ByVal Anno As Long, ByVal Mese As Integer, ByVal RegIVA As Byte, ByVal RegIVAAcqVnt As Byte, ByRef Ini As Integer, ByRef Grid As Cls_Grid) As Double
        Dim RS_MovimentiContabiliRiga As New ADODB.Recordset
        Dim DataDal As String
        Dim DataAl As String
        Dim MySql As String
        Dim Generaledb As New ADODB.Connection
        Dim Ri, ci, dt As String
        Dim tipo As String
        Dim VenditeAcquisti As String
        Dim AcquistiVendite As String
        Dim TAlt As String
        Dim Importo As Double
        Dim Imponibile As Double
        Dim TotaleAcquisti As Double
        Dim CoefficienteDiRiparto As Double
        Dim PercDetr As Double
        Dim Indetraibile As Double
        Dim Imposta As Double
        Dim Diff As Double
        Dim i As Integer



        For i = 1 To 100
            VtCodiceIvaAcqVnt(i) = ""
            VtTotaleAcqVnt(i) = 0
            VtImponibileAcqVnt(i) = 0
            VtImportoAcqVnt(i) = 0
        Next i

        DataDal = Anno & "-" & "01" & "-" & "01"
        If Mese = 13 Then
            DataAl = Anno & "-" & "12" & "-" & "31"
        Else
            DataAl = Anno & "-" & Format(Mese, "00") & "-" & GiorniMese(Mese, Anno)
        End If

        MySql = "SELECT MovimentiContabiliRiga.*,MovimentiContabiliTesta.RegistroIva, MovimentiContabiliTesta.TipoDocumento, MovimentiContabiliTesta.CausaleContabile " & _
             " FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where " & _
             " MovimentiContabiliRiga.Tipo = 'IV' " & _
             " AND RegistroIVA = " & RegIVAAcqVnt & _
             " AND MovimentiContabiliTesta.DataRegistrazione >= {ts '" & DataDal & " 00:00:00'} " & _
             " AND MovimentiContabiliTesta.DataRegistrazione <= {ts '" & DataAl & " 00:00:00'} Order By MovimentiContabiliTesta.RegistroIva,CodiceIva,Detraibile"




        RS_MovimentiContabiliRiga.Open(MySql, Generaledb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not RS_MovimentiContabiliRiga.EOF

            Ri = RegIVA
            '    ri = RS_MovimentiContabiliRiga!RegistroIva
            If Len(Trim(Ri)) = 1 Then Ri = "  " & Ri
            If Len(Trim(Ri)) = 2 Then Ri = " " & Ri
            VenditeAcquisti = "C"
            Dim MyCAus As New Cls_CausaleContabile

            MyCAus.Leggi(Session("DC_TABELLE"), MoveFromDb(RS_MovimentiContabiliRiga.Fields("CausaleContabile")))
            AcquistiVendite = MyCAus.VenditaAcquisti

            Dim MyIVA As New Cls_IVA

            MyIVA.Leggi(Session("DC_TABELLE"), MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva"))
            tipo = MyIVA.InLiquidazione

            If MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva") <> "" And tipo <> 1 Then
                ci = MoveFromDbWC(RS_MovimentiContabiliRiga, "CodiceIva")
                dt = MoveFromDbWC(RS_MovimentiContabiliRiga, "Detraibile")
                If Len(Trim(ci)) = 1 Then ci = ci & " "
                If Trim(dt) = "" Then dt = " "
                TAlt = TrovaCodiceAcqVnt(Ri & ci & dt & VenditeAcquisti)


                VtCodiceIvaAcqVnt(TAlt) = Ri & ci & dt & VenditeAcquisti

                Importo = MoveFromDbWC(RS_MovimentiContabiliRiga, "Importo")
                Imponibile = MoveFromDbWC(RS_MovimentiContabiliRiga, "Imponibile")
                If AcquistiVendite = "A" Then
                    If MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "A" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+" Then
                        Importo = Importo * -1
                        Imponibile = Imponibile * -1

                    End If
                End If
                If AcquistiVendite = "V" Then
                    If MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere") = "D" And MoveFromDbWC(RS_MovimentiContabiliRiga, "Segno") = "+" Then
                        Importo = Importo * -1
                        Imponibile = Imponibile * -1
                    End If
                End If

                VtTotaleAcqVnt(TAlt) = VtTotaleAcqVnt(TAlt) + Importo
                VtTotaleAcqVnt(TAlt) = VtTotaleAcqVnt(TAlt) + Imponibile
            End If
            RS_MovimentiContabiliRiga.MoveNext()
        Loop
        RS_MovimentiContabiliRiga.Close()

        TotaleAcquisti = 0
        For i = 1 To 100
            If VtCodiceIvaAcqVnt(i) = "" Then Exit For
            TotaleAcquisti = TotaleAcquisti + VtTotaleAcqVnt(i)
        Next i

        If TotaleAcquisti = 0 Then
            CoefficienteDiRiparto = 0
        Else
            CoefficienteDiRiparto = VtImponibile(Ini) / TotaleAcquisti
        End If
        TotaleAcquisti = 0
        For i = 1 To 100
            If VtCodiceIvaAcqVnt(i) = "" Then Exit For
            VtTotaleAcqVnt(i) = Modulo.MathRound(CoefficienteDiRiparto * VtTotaleAcqVnt(i), 2)
            TotaleAcquisti = TotaleAcquisti + VtTotaleAcqVnt(i)
        Next i

        If VtImponibile(Ini) <> TotaleAcquisti Then
            Diff = Modulo.MathRound(VtImponibile(Ini) - TotaleAcquisti, 2)
            VtTotaleAcqVnt(i - 1) = Modulo.MathRound(VtTotaleAcqVnt(i - 1) + Diff, 2)
        End If

        For i = 1 To 100
            If VtCodiceIvaAcqVnt(i) = "" Then Exit For
            Dim CampoIVA As New Cls_IVA
            CampoIVA.Leggi(Session("DC_TABELEE"), Mid(VtCodiceIvaAcqVnt(i), 4, 2))
            VtImponibileAcqVnt(i) = Modulo.MathRound(VtTotaleAcqVnt(i) / (1 + CampoIVA.Aliquota), 2)
            VtImportoAcqVnt(i) = Modulo.MathRound(VtTotaleAcqVnt(i) - VtImponibileAcqVnt(i), 2)

            PercDetr = 1
            '    If Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "V" Or Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "C" Then PercDetr = 1
            '    If Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "A" Then
            '      If Mid(VtCodiceIvaAcqVnt(I), 6, 1) <> " " Then
            '        PercDetr = CampoDetraibilita(Mid(VtCodiceIvaAcqVnt(I), 6, 1), "Aliquota")
            '      End If
            '    End If
            Indetraibile = Modulo.MathRound(VtImportoAcqVnt(i) - (VtImportoAcqVnt(i) * PercDetr), 2)
            Imposta = Modulo.MathRound(VtImportoAcqVnt(i) - Indetraibile, 2)
            '    If Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "V" Then TotVd = TotVd + Imposta
            '    If Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "A" Then TotAc = TotAc + Imposta
            '    If Mid(VtCodiceIvaAcqVnt(I), 7, 1) = "C" Then TotCr = TotCr + Imposta
            VentilazioneIVA = VentilazioneIVA + Imposta

            Dim xCampoIVA As New Cls_IVA

            xCampoIVA.Leggi(Session("DC_TABELLE"), Mid(VtCodiceIvaAcqVnt(i), 4, 2))
            If Trim(xCampoIVA.Tipo) = "" Then
                Grid.AddItem(Mid(VtCodiceIvaAcqVnt(i), 4, 2) & vbTab & xCampoIVA.Descrizione & vbTab & VtImponibileAcqVnt(i) & vbTab & xCampoIVA.Aliquota * 100 & vbTab & Imposta & vbTab & Indetraibile & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & Mid(VtCodiceIvaAcqVnt(i), 7, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 6, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 1, 3))
            End If
            If xCampoIVA.Tipo = "ES" Then
                Grid.AddItem(Mid(VtCodiceIvaAcqVnt(i), 4, 2) & vbTab & xCampoIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibileAcqVnt(i) & vbTab & 0 & vbTab & 0 & vbTab & Mid(VtCodiceIvaAcqVnt(i), 7, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 6, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 1, 3))
            End If
            If xCampoIVA.Tipo = "NS" Then
                Grid.AddItem(Mid(VtCodiceIvaAcqVnt(i), 4, 2) & vbTab & xCampoIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibileAcqVnt(i) & vbTab & 0 & vbTab & Mid(VtCodiceIvaAcqVnt(i), 7, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 6, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 1, 3))
            End If
            If xCampoIVA.Tipo = "NI" Then
                Grid.AddItem(Mid(VtCodiceIvaAcqVnt(i), 4, 2) & vbTab & xCampoIVA.Descrizione & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & 0 & vbTab & VtImponibileAcqVnt(i) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 7, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 6, 1) & vbTab & Mid(VtCodiceIvaAcqVnt(i), 1, 3))
            End If
        Next i
    End Function


    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function
    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        txt_compensazioneimporto.Text = 0
        Txt_IvaAcconto.Text = 0
        Txt_IvaPagata.Text = 0

        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("LIQUIDAZIONEIVA")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("LIQUIDAZIONEIVA").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "LIQUIDAZIONEIVA"
        End If

        If Trim(XS.ReportPersonalizzato("LIQUIDAZIONEIVA1")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("LIQUIDAZIONEIVA1").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "LIQUIDAZIONEIVA1"
        End If


        If Trim(XS.ReportPersonalizzato("LIQUIDAZIONEIVA2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("LIQUIDAZIONEIVA2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "LIQUIDAZIONEIVA2"
        End If


        Dim Trimestrale As String

        Dim MyGruppi As New Cls_Gruppi

        MyGruppi.UpDateDropBox(Session("DC_TABELLE"), DD_Gruppo)

        Trimestrale = CampoDatiGenerali("LiquidazioneTrimestrale")


        If Trimestrale = "S" Then
            DD_Mese.Items.Add("Marzo")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 3
            DD_Mese.Items.Add("Giugno")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 6
            DD_Mese.Items.Add("Settembre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 9
            DD_Mese.Items.Add("Dicembre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 12
            DD_Mese.Items.Add("13° Liquidazione")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 13
        Else
            DD_Mese.Items.Add("Gennaio")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 1
            DD_Mese.Items.Add("Febbraio")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 2
            DD_Mese.Items.Add("Marzo")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 3
            DD_Mese.Items.Add("Aprile")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 4
            DD_Mese.Items.Add("Maggio")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 5
            DD_Mese.Items.Add("Giugno")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 6
            DD_Mese.Items.Add("Luglio")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 7
            DD_Mese.Items.Add("Agosto")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 8
            DD_Mese.Items.Add("Settembre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 9
            DD_Mese.Items.Add("Ottobre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 10
            DD_Mese.Items.Add("Novembre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 11
            DD_Mese.Items.Add("Dicembre")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 12
            DD_Mese.Items.Add("13° Liquidazione")
            DD_Mese.Items(DD_Mese.Items.Count - 1).Value = 13
        End If
        Txt_Anno.Text = Year(Now)



        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        'MyJs = MyJs & " if (appoggio.match('TxtSottoconto')!= null) {  "
        'MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'MyJs = MyJs & "    }"

        'MyJs = MyJs & " if ((appoggio.match('Txt_DallaData')!= null) || (appoggio.match('Txt_AllaData')!= null)) {  "
        'MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        'MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_IvaPagata')!= null) || (appoggio.match('Txt_IvaAcconto')!= null) || (appoggio.match('txt_compensazioneimporto')!= null))  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) < 2000 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare Anno');", True)
            Exit Sub
        End If
        Dim Trimestrale As String

        Trimestrale = CampoDatiGenerali("LiquidazioneTrimestrale")

        Dim Errori As String
        Errori = ""
        Call StampaVisualizzaLiquidazioneIVA(Txt_Anno.Text, DD_Mese.SelectedValue, Errori)
        'Response.Redirect("StampaReport.aspx?REPORT=LIQUIDAZIONEIVA")

        Session("SelectionFormula") = "{StampaLiquidazione.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)
        If DD_Report.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=LIQUIDAZIONEIVA&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=" & DD_Report.SelectedValue & "&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        End If

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub
End Class
