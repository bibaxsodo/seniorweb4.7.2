﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System
Imports System.Web
Imports System.Configuration
Imports System.Text
Imports System.Web.Script.Serialization


Partial Class GeneraleWeb_Prorata
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Registro As New Cls_RegistroIVA


        Registro.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)


        Dim Prorata As New Cls_Prorata


        If Request.Item("Anno") > 0 Then
            Prorata.Anno = Request.Item("Anno")
            Prorata.RegistroIVA = Request.Item("Registro")
            Prorata.Leggi(Session("DC_TABELLE"))

            DD_RegistroIVA.SelectedValue = Prorata.RegistroIVA
            Txt_Anno.Text = Prorata.Anno
            Txt_Attivita.Text = Prorata.Attivita
            Txt_DefinitivoPercentuale.Text = Modulo.MathRound(Prorata.DefinitivoPercentuale, 2)

            Txt_Anno.Enabled = False
            DD_RegistroIVA.Enabled = False
        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Prorata As New Cls_Prorata
        

        Prorata.Anno = Request.Item("Anno")
        Prorata.RegistroIVA = Request.Item("Registro")
        Prorata.Leggi(Session("DC_TABELLE"))

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "PRORATA", serializer.Serialize(Prorata))


        Prorata.RegistroIVA = DD_RegistroIVA.SelectedValue
        Prorata.Anno = Txt_Anno.Text
        Prorata.Attivita = Txt_Attivita.Text
        If Txt_DefinitivoPercentuale.Text.Trim = "" Then
            Txt_DefinitivoPercentuale.Text = "0"
        End If
        Prorata.DefinitivoPercentuale = Modulo.MathRound(CDbl(Txt_DefinitivoPercentuale.Text), 2)

        Prorata.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("ElencoProrata.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Prorata As New Cls_Prorata


        Prorata.Anno = Request.Item("Anno")
        Prorata.RegistroIVA = Request.Item("Registro")
        Prorata.Leggi(Session("DC_TABELLE"))

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "PRORATA", serializer.Serialize(Prorata))

        Prorata.Delete(Session("DC_TABELLE"))

        Response.Redirect("ElencoProrata.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("ElencoProrata.aspx")

    End Sub
End Class
