﻿
Partial Class GeneraleWeb_TipoRegistro
    Inherits System.Web.UI.Page


    Private Sub Pulisci()

        Txt_Codice.Text = 0
        Txt_Descrizione.Text = ""
        Txt_RegistroVentilazione.Text = ""

        RB_Vendite.Checked = True
        RB_Acquisti.Checked = False
        RB_Corrispettivi.Checked = False
        RB_Riepilogativi.Checked = False
        RB_Giornale.Checked = False
        RB_Numeratore.Checked = False

        TxT_IndicatoreRegistro.Text = 0
        Lbl_Errori.Text = ""

        Chk_NonControllareProtocollo.Checked = False

        Chk_RegistroCartaceo.Checked = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Ks As New Cls_RegistroIVA


        If Request.Item("Codice") = "" Then

            Txt_Codice.Text = Ks.MaxRegistro(Session("DC_TABELLE"))
            Exit Sub
        End If

        

        Ks.Leggi(Session("DC_TABELLE"), Request.Item("Codice"))


        Txt_Codice.Enabled = False

        Txt_Codice.Text = Ks.Tipo
        Txt_Descrizione.Text = Ks.Descrizione
        Txt_RegistroVentilazione.Text = Ks.TipoAcqVnt

        TxT_IndicatoreRegistro.Text = Ks.IndicatoreRegistro


        Chk_NonControllareProtocollo.Checked = False
        If Ks.Protocollo = 1 Then
            Chk_NonControllareProtocollo.Checked = True
        End If
        RB_Vendite.Checked = False
        RB_Acquisti.Checked = False
        RB_Corrispettivi.Checked = False
        RB_Riepilogativi.Checked = False
        RB_Giornale.Checked = False
        RB_Numeratore.Checked = False
        If Ks.TipoIVA = "V" Then
            RB_Vendite.Checked = True
        End If
        If Ks.TipoIVA = "A" Then
            RB_Acquisti.Checked = True
        End If
        If Ks.TipoIVA = "C" Then
            RB_Corrispettivi.Checked = True
        End If
        If Ks.TipoIVA = "R" Then
            RB_Riepilogativi.Checked = True
        End If
        If Ks.TipoIVA = "G" Then
            RB_Giornale.Checked = True
        End If
        If Ks.TipoIVA = "N" Then
            RB_Numeratore.Checked = True
        End If

        If Ks.RegistroCartaceo = 1 Then
            Chk_RegistroCartaceo.Checked = True

        Else
            Chk_RegistroCartaceo.Checked = False
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ks As New Cls_RegistroIVA
        Lbl_Errori.Text = ""
        If Val(Txt_Codice.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Il codice deve essere maggiore di zero');", True)
            REM Lbl_Errori.Text = "Il codice deve essere maggiore di zero"
            Exit Sub
        End If

        If Val(Txt_Codice.Text) > 255 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice maggiore di 255 non posso procedere');", True)
            REM Lbl_Errori.Text = "Codice maggiore di 255 non posso procedere"
            Exit Sub
        End If
        If Trim(Txt_Descrizione.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            REM Lbl_Errori.Text = "Descrizione obbligatoria"
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim Verifica As New Cls_RegistroIVA

            Verifica.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

            If Verifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                REM Lbl_Errori.Text = "Descrizione obbligatoria"
                Exit Sub
            End If
        End If

        'Dim I As Long

        Ks.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

        'If DD_RegistroIVA.SelectedValue = 0 Then
        '    For I = 0 To DD_RegistroIVA.Items.Count - 1
        '        If Val(DD_RegistroIVA.Items(I).Value) = Val(Txt_Codice.Text) Then
        '            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già inserito');", True)
        '            REM Lbl_Errori.Text = "Codice già inserito"
        '            Exit Sub
        '        End If
        '    Next
        'End If

        'For I = 0 To DD_RegistroIVA.Items.Count - 1
        '    If DD_RegistroIVA.Items(I).Text = Txt_Descrizione.Text And Val(DD_RegistroIVA.Items(I).Value) <> Val(Txt_Codice.Text) Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione già inserita');", True)
        '        REM Lbl_Errori.Text = "Descrizione già inserita"
        '        Exit Sub
        '    End If
        'Next


        Ks.Tipo = Val(Txt_Codice.Text)
        Ks.Descrizione = Txt_Descrizione.Text
        Ks.TipoAcqVnt = Val(Txt_RegistroVentilazione.Text)


        If RB_Vendite.Checked = True Then
            Ks.TipoIVA = "V"
        End If
        If RB_Acquisti.Checked = True Then
            Ks.TipoIVA = "A"
        End If
        If RB_Corrispettivi.Checked = True Then
            Ks.TipoIVA = "C"
        End If
        If RB_Riepilogativi.Checked = True Then
            Ks.TipoIVA = "R"
        End If
        If RB_Giornale.Checked = True Then
            Ks.TipoIVA = "G"
        End If
        If RB_Numeratore.Checked = True Then
            Ks.TipoIVA = "N"
        End If


        Ks.IndicatoreRegistro = TxT_IndicatoreRegistro.Text
        If Chk_NonControllareProtocollo.Checked = False Then
            Ks.Protocollo = 0
        End If
        If Chk_NonControllareProtocollo.Checked = True Then
            Ks.Protocollo = 1
        End If

        If Chk_RegistroCartaceo.Checked = True Then
            Ks.RegistroCartaceo = 1
        Else
            Ks.RegistroCartaceo = 0
        End If

        Ks.Scrivi(Session("DC_TABELLE"))



        Response.Redirect("ElencoTipoRegistro.aspx")
    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoTipoRegistro.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Ks As New Cls_RegistroIVA


        Ks.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)



        If Ks.InUso(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If

        Ks.Delete(Session("DC_TABELLE"))

        Response.Redirect("ElencoTipoRegistro.aspx")
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_RegistroIVA

            x.Tipo = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"), x.Tipo)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_RegistroIVA

            x.Tipo = 0
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione.Trim)

            If x.Tipo <> 0 Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
