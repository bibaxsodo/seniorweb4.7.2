﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_EliminaRegistrazioneF24
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim StringaConnessione As String = Session("DC_GENERALE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")


        If Val(Txt_NumeroRegistrazione.Text) = 0 Then Exit Sub


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Tipo Doc.", GetType(String))
        Tabella.Columns.Add("N. Regis.", GetType(String))
        Tabella.Columns.Add("Num.Doc.", GetType(String))
        Tabella.Columns.Add("Data Doc.", GetType(String))
        Tabella.Columns.Add("Cod. Tributo", GetType(String))
        Tabella.Columns.Add("Tributo", GetType(String))
        Tabella.Columns.Add("Imponibile Rit.", GetType(String))
        Tabella.Columns.Add("Tipo Rit.", GetType(String))
        Tabella.Columns.Add("Imp. Ritenuta", GetType(String))
        Tabella.Columns.Add("Pagamento", GetType(String))
        Tabella.Columns.Add("Data Pagamento", GetType(String))
        Tabella.Columns.Add("Imp. Pagamento", GetType(String))


        MySql = "Select * From MovimentiRitenute where  NumeroRegistrazionePagamentoRitenuta = ?"
        MySql = MySql & " Order by CodiceTributo"
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_NumeroRegistrazione.Text)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            Dim RegistrazioneContabile As New Cls_MovimentoContabile

            RegistrazioneContabile.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            RegistrazioneContabile.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = RegistrazioneContabile.CausaleContabile
            CausaleContabile.Leggi(StringaConnessioneTabelle, CausaleContabile.Codice)

            myriga(0) = campodbN(myPOSTreader.Item("ID"))

            myriga(1) = CausaleContabile.TipoDocumento
            myriga(2) = RegistrazioneContabile.NumeroRegistrazione

            myriga(3) = RegistrazioneContabile.NumeroDocumento

            myriga(4) = Format(RegistrazioneContabile.DataDocumento, "dd/MM/yyyy")

            myriga(5) = campodb(myPOSTreader.Item("CodiceTributo"))
            myriga(6) = campodb(myPOSTreader.Item("CodiceTributo")) 'inserire decodifica tributi

            myriga(7) = Format(campodbN(myPOSTreader.Item("ImponibileRitenuta")), "#,##0.00")

            Dim TipoRitenuta As New ClsRitenuta

            TipoRitenuta.Codice = campodb(myPOSTreader.Item("TipoRitenuta"))
            TipoRitenuta.Leggi(StringaConnessioneTabelle)

            myriga(8) = TipoRitenuta.Descrizione

            myriga(9) = Format(campodbN(myPOSTreader.Item("ImportoRitenuta")), "#,##0.00")


            Dim RegistrazioneContabilePagamento As New Cls_MovimentoContabile

            RegistrazioneContabilePagamento.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            RegistrazioneContabilePagamento.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            myriga(10) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))


            If campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento")) = 0 Then
                myriga(11) = ""
            Else

                myriga(11) = Format(RegistrazioneContabilePagamento.DataRegistrazione, "dd/MM/yyyy")
            End If
            myriga(12) = Format(campodbN(myPOSTreader.Item("ImportoPagamento")), "#,##0.00")
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("Appoggio") = Tabella

        GridView1.AutoGenerateColumns = True

        GridView1.DataSource = Tabella

        GridView1.DataBind()

        GridView1.Visible = True

        Btn_Modifica.Visible = True

    End Sub



    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim xs As New Cls_Legami
        If xs.TotaleLegame(Session("DC_GENERALE"), Txt_NumeroRegistrazione.Text) > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)
            REM Lbl_Errore.Text = "Vi sono legami non posso cancellare la registrazione"
            Exit Sub
        End If
        If Val(Txt_NumeroRegistrazione.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_NumeroRegistrazione.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Exit Sub
            End If
        End If

        If ViewState("NonModificabile") = 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è possibile eliminare questa registrazione</center>');", True)
            Exit Sub
        End If

        If Val(Txt_NumeroRegistrazione.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
        End If

        Dim cn As OleDbConnection
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        MySql = "UPDATE MovimentiRitenute  SET NumeroRegistrazionePagamentoRitenuta  = 0, ImportoPagamentoRitenuta = 0 where  NumeroRegistrazionePagamentoRitenuta = ?"
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_NumeroRegistrazione.Text)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.NumeroRegistrazione = Txt_NumeroRegistrazione.Text
        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        Registrazione.EliminaRegistrazione(Session("DC_GENERALE"))
        cn.Close()

        Call Btn_Movimenti_Click(sender, e)
        Txt_NumeroRegistrazione.Text = ""
        Btn_Modifica.Visible = False
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
