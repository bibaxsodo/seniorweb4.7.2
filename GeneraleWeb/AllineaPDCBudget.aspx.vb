﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting
Partial Class GeneraleWeb_AllineaPDCBudget
    Inherits System.Web.UI.Page



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Function ControlloMovimenti() As String
        Dim Elenco As String = ""
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        cn.Open()




        Dim CommandPDC As New OleDbCommand("Select * From PianoConti Where  Tipo ='C' OR  Tipo = 'R' Order by Mastro,Conto,Sottoconto", cn)
        Dim ReadPDC As OleDbDataReader = CommandPDC.ExecuteReader()
        Do While ReadPDC.Read()

            Dim CommandBudget As New OleDbCommand("Select * From TipoBudget Where   Anno = ? And Livello1 = ? And  Livello2 = ? And  Livello3 = ?", cn)
            CommandBudget.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))
            CommandBudget.Parameters.AddWithValue("@Livello1", campodbN(ReadPDC.Item("Mastro")))
            CommandBudget.Parameters.AddWithValue("@Livello2", campodbN(ReadPDC.Item("Conto")))
            CommandBudget.Parameters.AddWithValue("@Livello3", campodbN(ReadPDC.Item("Sottoconto")))

            Dim ReadBudget As OleDbDataReader = CommandBudget.ExecuteReader()

            If Not ReadBudget.Read() Then
                Dim CreateBudget As New Cls_TipoBudget

                CreateBudget.Anno = Txt_Anno.Text
                CreateBudget.Livello1 = campodbN(ReadPDC.Item("Mastro"))
                CreateBudget.Livello2 = campodbN(ReadPDC.Item("Conto"))
                CreateBudget.Livello3 = campodbN(ReadPDC.Item("Sottoconto"))
                CreateBudget.Descrizione = campodb(ReadPDC.Item("Descrizione"))
                If campodb(ReadPDC.Item("Tipo")) = "R" Then
                    CreateBudget.Tipo = "E"
                End If
                If campodb(ReadPDC.Item("Tipo")) = "C" Then
                    CreateBudget.Tipo = "U"
                End If
                CreateBudget.Scrivi(Session("DC_GENERALE"))

                Elenco = Elenco & "Aggiunto conto " & CreateBudget.Anno & " " & CreateBudget.Livello1 & "." & CreateBudget.Livello2 & "." & CreateBudget.Livello3 & " " & CreateBudget.Descrizione & vbNewLine
            End If
            ReadBudget.Close()

        Loop
        ReadPDC.Close()


        cn.Close()

        Return Elenco
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Lbl_Errori.Text = ControlloMovimenti()
    End Sub

    Protected Sub GeneraleWeb_ControlloRegistrazioni_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub
End Class
