﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_InserimentoBudget" CodeFile="InserimentoBudget.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Inserimento Budget</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">     

        // Decodifica Comune
        function decodificacallback(r, IDName, Importo, Liveollo1, Livello2, Livello3, Anno, Colonna) {

            if (r == 'OK') {

                xModificaCella(IDName, Importo, Liveollo1, Livello2, Livello3, Anno, Colonna);
            }
            else {
                if (Livello3 == 0) {
                    if (r == 'ERR') {
                        VisualizzaErrore('Non puoi modificare questo livello,<br/> livello 1 impostato');
                    } else {
                        VisualizzaErrore('Non puoi modificare il livello 0,<br/> livelli successivi movimentati');
                    }
                } else {
                    VisualizzaErrore('Non puoi modificare questo livello,<br/> livello precedente impostato');
                }
            }
        }


        function ModificaCella(IDName, Importo, Liveollo1, Livello2, Livello3, Anno, Colonna) {
            var url = "Verficasemodifica.ashx?";
            url += "Anno=" + escape(Anno);
            url += "&Liveollo1=" + escape(Liveollo1);
            url += "&Livello2=" + escape(Livello2);
            url += "&Livello3=" + escape(Livello3);
            url += "&Colonna=" + escape(Colonna);
            url += "&UTENTE=";
            url += "&casuale=" + Math.floor(Math.random() * 1000);


            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) { decodificacallback(xmlhttp.responseText, IDName, Importo, Liveollo1, Livello2, Livello3, Anno, Colonna); }
            }

            xmlhttp.open("GET", url, true);
            xmlhttp.send(null);
        }

        function getOffset(el) {
            var _x = 0;
            var _y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += (el.offsetTop + el.clientTop);
                el = el.offsetParent;
            }
            return { top: _y, left: _x };
        }




        function xModificaCella(IDName, Importo, Liveollo1, Livello2, Livello3, Anno, Colonna) {
            if (document.getElementById("InputBox") != null) {
                return false;
            }

            var doc = document.body;
            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var appoggio = '';

            var myop = '';



            myop = "'" + IDName + "',$('#Importo').val()," + Liveollo1 + ',' + Livello2 + ',' + Livello3 + ',' + Anno + ',' + Colonna;



            appoggio = '<p style="text-align:left;">Digita Importo : <input id="Importo" name="Importo" onkeypress="ForceNumericInput(this, true, true)" style="text-align:right;" type="text" value="' + Importo + '" /><a href="#" onclick="ModificaEffettiva(' + myop + '); return false;" ><img src="../images/salva.jpg" style="position: absolute;right: 0px;top: 0px;"></a></p>';

            var y = getOffset(document.getElementById(IDName)).top + 20;

            creatediv("InputBox", appoggio, 400, 100, (winW - 400) / 2, y);

            $('#Importo').blur(function () {
                var ap = formatNumber($('#Importo').val(), 2);
                $('#Importo').val(ap);
            }
            );
            return false;
        }

        function ModificaEffettiva(IDName, importo, Liveollo1, Livello2, Livello3, Anno, Colonna) {
            var appoggio = '';

            appoggio = "'" + IDName + "','" + importo + "'," + Liveollo1 + ',' + Livello2 + ',' + Livello3 + ',' + Anno + ',' + Colonna;
            document.getElementById(IDName).innerHTML = '<a href="#" onclick="ModificaCella(' + appoggio + '); return false;">' + importo + '</a>';
            removeElement("InputBox");


            InserisciModifica(importo, Liveollo1, Livello2, Livello3, Anno, Colonna);

        }



        function InserisciModifica(Importo, Liveollo1, Livello2, Livello3, Anno, Colonna) {
            var url = "/WebHandler/InserisciRigaBudget.ashx?";
            url += "Anno=" + escape(Anno);
            url += "&Liveollo1=" + escape(Liveollo1);
            url += "&Livello2=" + escape(Livello2);
            url += "&Livello3=" + escape(Livello3);
            url += "&Colonna=" + escape(Colonna);
            url += "&Importo=" + escape(Importo);
            url += "&UTENTE=";
            url += "&casuale=" + Math.floor(Math.random() * 1000);


            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) { inserito(); }
            }

            xmlhttp.open("GET", url, true);
            xmlhttp.send(null);
        }

        function inserito() {
        }


        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });


    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">



            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Budget - Inserimento Budget</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Budget.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Inserimento Analitica/Budget
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <p>
                                        Anno :
    <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="59px"></asp:TextBox><br />
                                    </p>

                                    <br />
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px" Style="min-width: 80%;">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                    </asp:GridView>


                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
