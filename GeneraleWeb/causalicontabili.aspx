﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" Inherits="causalicontabili" CodeFile="causalicontabili.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Causali Contabili</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }



                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Tabelle - Causali Contabili</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />&nbsp;
        <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Causali Contabili
         
                
                                </HeaderTemplate>

                                <ContentTemplate>
                                    <br />

                                    <label class="LabelCampo">Codice :</label>
                                    <asp:TextBox ID="Txt_Codice" runat="server" AutoPostBack="true" Width="100px" MaxLength="3"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipo :</label>
                                    <asp:DropDownList ID="DD_Tipo" runat="server" AutoPostBack="True" Width="200px">
                                    </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" AutoPostBack="true" Width="400px" MaxLength="50"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDescrizione" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Registro IVA:</label>
                                    <asp:DropDownList ID="DD_RegistroIVA" runat="server" Width="200px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Documento:</label>
                                    <asp:DropDownList ID="DD_TipoDocumento" runat="server" Width="200px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Causale Incasso :</label>
                                    <asp:DropDownList ID="DD_CausaleIncasso" runat="server" Width="200px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">IVA Default :</label>
                                    <asp:DropDownList ID="DD_IVA" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        Detraibilità :</label>
                                    <asp:DropDownList ID="DD_Detraibilita" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Ritenuta :</label>
                                    <asp:DropDownList ID="DD_Ritenuta" runat="server" Width="200px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Documento Reverse/Split :</label>
                                    <asp:DropDownList ID="DD_CausaleDocumento" runat="server" Width="200px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Giroconto Reverse/Split :</label>
                                    <asp:DropDownList ID="DD_CausaleGiroconto" runat="server" Width="200px"></asp:DropDownList>
                                    <br />
                                    <table border="0">
                                        <tr>

                                            <td style="width: 100px">Data Obb.
                                                <br />
                                                <asp:RadioButton ID="RB_DataObbSI" BorderWidth="0px" BorderColor="White" runat="server" GroupName="DataOB" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_DataObbNO" BorderWidth="0px" BorderColor="White" runat="server" GroupName="DataOB" Text="NO" Font-Names="Arial" />
                                            </td>

                                            <td style="width: 100px">Num Obb.
                                                <br />
                                                <asp:RadioButton ID="RB_NumObbSI" BorderWidth="0px" BorderColor="White" runat="server" GroupName="NumOB" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_NumObbNO" BorderWidth="0px" BorderColor="White" runat="server" GroupName="NumOB" Text="NO" Font-Names="Arial" />
                                            </td>


                                            <td style="width: 140px">Causale
                                                <br />
                                                <asp:RadioButton ID="RB_Vendita" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Causale" Text="Vend." Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_Acquisti" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Causale" Text="Acq." Font-Names="Arial" />
                                            </td>



                                            <td style="width: 100px">Apri Analitica
                                                <br />
                                                <asp:RadioButton ID="RB_AnaliticaSI" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Analitica" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_AnaliticaNO" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Analitica" Text="NO" Font-Names="Arial" />
                                            </td>

                                            <td style="width: 100px">Apri Cespiti
                                                <br />
                                                <asp:RadioButton ID="RB_CespitiSI" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Cespiti" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_CespitiNO" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Cespiti" Text="NO" Font-Names="Arial" />
                                            </td>


                                            <td style="width: 100px">Giroconto Reverse Automatico<br />
                                                <asp:RadioButton ID="RB_ReverseAutomaticoSI" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Reverse" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_ReverseAutomaticoNO" BorderWidth="0px" BorderColor="White" runat="server" GroupName="Reverse" Text="NO" Font-Names="Arial" />
                                            </td>


                                            <td style="width: 100px">
                                                <asp:Label ID="Lbl_Prorata" runat="server" Text="Label" Visible="false"></asp:Label><br />
                                                <asp:RadioButton ID="RB_ProrataSI" BorderWidth="0px" Visible="false" BorderColor="White" runat="server" GroupName="Prorata" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_ProrataNO" BorderWidth="0px" Visible="false" BorderColor="White" runat="server" GroupName="Prorata" Text="NO" Font-Names="Arial" />
                                            </td>

                                            <td style="width: 100px">

                                                <asp:RadioButton ID="RB_AllegatoSI" BorderWidth="0px" Visible="false" BorderColor="White" runat="server" GroupName="alsi" Text="SI" Font-Names="Arial" /><br />
                                                <asp:RadioButton ID="RB_AllegatoNO" BorderWidth="0px" Visible="false" BorderColor="White" runat="server" GroupName="alsi" Text="NO" Font-Names="Arial" />
                                            </td>

                                        </tr>
                                    </table>

                                    <label class="LabelCampo">Codice Sede :</label>
                                    <asp:TextBox ID="Txt_CodiceSede" runat="server" Width="400px" MaxLength="50"></asp:TextBox>
                                    (esportazione Alyante)<br />
                                    <br />

                                    <br />
                                    &nbsp; 
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/cancella.png" ShowDeleteButton="True" />
                                            <asp:TemplateField HeaderText="Descrizione">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="250px"></asp:Label>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: left">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" ToolTip="Inserisci (F9)" />
                                                    </div>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="250px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sottoconto">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtSottoconto" runat="server" Width="350px"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Dare/Avere">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="RB_Dare" GroupName="DrAr" runat="server" />Dare 
                        <asp:RadioButton ID="RB_Avere" GroupName="DrAr" runat="server" />Avere
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Riga">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblRiga" runat="server" Width="100px" Text='<%# Eval("Riga") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="304px"></asp:Label><br />
                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>
