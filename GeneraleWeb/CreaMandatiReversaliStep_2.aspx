﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_CreaMandatiReversaliStep_2" CodeFile="CreaMandatiReversaliStep_2.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crea Mandati/Reversali</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <style>
        .Registrazione {
            width: 400px;
            height: 24px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=96)";
            filter: alpha(opacity=96);
            -moz-opacity: 0.96;
            -khtml-opacity: 0.96;
            opacity: 0.96;
            border: 1px #888888 solid;
            background-color: #86d5f9;
            padding: 2px;
            -moz-box-shadow: 4px 3px 2px 1px #110e0e;
            -webkit-box-shadow: 4px 3px 2px 1px #110e0e;
            box-shadow: 4px 3px 2px 1px #110e0e;
        }
    </style>
    <script type="text/javascript">                     
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function CurrencyFormatted(number) {
            var numberStr = parseFloat(number).toFixed(2).toString();
            var numFormatDec = numberStr.slice(-2); /*decimal 00*/
            numberStr = numberStr.substring(0, numberStr.length - 3); /*cut last 3 strings*/
            var numFormat = new Array;
            while (numberStr.length > 3) {
                numFormat.unshift(numberStr.slice(-3));
                numberStr = numberStr.substring(0, numberStr.length - 3);
            }
            numFormat.unshift(numberStr);
            return numFormat.join('.') + ',' + numFormatDec; /*format 000.000.000,00 */
        }

        function copiaimporto(Riga) {
            var table = document.getElementById('TabContainer1_Tab_Anagrafica_GridEstrazione');


            var els = document.getElementsByTagName("*");

            Riga++;
            Riga++;

            var StrRiga = '' + Riga;
            if (Riga < 10) StrRiga = '0' + Riga;

            var totale = 0;
            for (var i = 0; i < els.length; i++) {
                if (els[i].id) {
                    var appoggio = els[i].id;
                    if (appoggio.match("ctl" + StrRiga + "_TxtImportoLegame") != null) {

                        if (($(els[i]).val() == 0) || $(els[i]).val() == '0,00') {
                            $(els[i]).val(table.rows[Riga - 1].cells[4].innerHTML);
                        } else {
                            $(els[i]).val('0,00');
                        }
                    }

                    if (appoggio.match("_TxtImportoLegame") != null) {
                        if ($(els[i]).val() != null) {
                            var testo = $(els[i]).val();

                            testo = testo.replace('.', '');
                            testo = testo.replace(',', '.');
                            if (testo != '') {
                                totale = totale + parseFloat(testo);
                            }
                        }
                    }

                }

            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <div id="dialog-modal" title="Legami">
                <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>

                    <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Contabilità - Principale - Crea Mandati/Reversali</div>
                                <div class="SottoTitolo">
                                    <br />
                                    <br />
                                </div>
                            </td>

                            <td style="text-align: right; vertical-align: top;">
                                <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci (F2)" />&nbsp;    
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Generale.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" alt="Menù" id="BOTTONEHOME" /></a><br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Mandati/Reversali Step 3/3
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <br />

                                            <asp:Label ID="Lbl_DatiRegistrazione" runat="server" Text=""></asp:Label>
                                            <br />
                                            <label class="LabelCampo">Data Dal :</label>
                                            <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Data Al :</label>
                                            <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Numero Registrazione Dal :</label>
                                            <asp:TextBox ID="Txt_NumeroRegistrazioneDal" onkeypress="return soloNumeri(event);" runat="server" Width="80px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Numero Registrazione Al :</label>
                                            <asp:TextBox ID="Txt_NumeroRegistrazioneAl" onkeypress="return soloNumeri(event);" runat="server" Width="88px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Conto</label>
                                            <asp:TextBox ID="Txt_Sottoconto" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
                                            <br />
                                            <asp:Button ID="Btn_Ricerca" runat="server" Text="Ricerca" />
                                            <br />
                                            <br />
                                            <asp:GridView ID="GridEstrazione" runat="server" CellPadding="3" Height="60px"
                                                Width="90%" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px">
                                                <RowStyle ForeColor="#000066" />
                                                <Columns>
                                                    <asp:BoundField DataField="Copia" HeaderText="" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="NumeroRegistrazione" HeaderText="Numero Registrazione" ItemStyle-HorizontalAlign="Right" />

                                                    <asp:BoundField DataField="DataRegistrazione" HeaderText="Data Registrazione" ItemStyle-HorizontalAlign="Right" />

                                                    <asp:BoundField DataField="ImportoDocumento" HeaderText="Importo Documento" ItemStyle-HorizontalAlign="Right" />

                                                    <asp:BoundField DataField="ImportoLegare" HeaderText="Importo da Legare" ItemStyle-HorizontalAlign="Right" />


                                                    <asp:TemplateField HeaderText="Importo Legame">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtImportoLegame" Width="150px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TxtImportoLegame" Width="150px" runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CausaleContabile" HeaderText="Causale Contabile" ItemStyle-HorizontalAlign="Right" />

                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>

                                        </ContentTemplate>
                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
