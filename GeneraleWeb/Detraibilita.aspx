﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Detraibilita" EnableEventValidation="false" CodeFile="Detraibilita.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Detraibilità</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">                

        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Contabilità - Tabella - Detraibilità<br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Style="width: 38px" ToolTip="Inserisci/Modifica (F2)" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" Style="width: 38px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Detraibilità
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Codice : </label>
                                    <asp:TextBox ID="Txt_Codice" runat="server" AutoPostBack="true" Width="50px" MaxLength="1"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" AutoPostBack="true" Width="400px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDescrizione" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Aliquota :</label><asp:TextBox
                                        ID="Txt_Aliquota" onkeypress="ForceNumericInput(this, true, true)"
                                        runat="server" Width="50px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Imputazione a Costi : </label>
                                    Si
                                    <asp:RadioButton ID="RB_Si" GroupName="ImpCs" runat="server" />
                                    No
                                    <asp:RadioButton ID="RB_No" GroupName="ImpCs" runat="server" />
                                    Aliquota : 
            <asp:TextBox ID="Txt_AliquotaCosti" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="67px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Imputazione a IVA indetraibile : </label>
                                    Si
                                    <asp:RadioButton ID="RB_SiIva" GroupName="ImpIVA" runat="server" />
                                    No
                                    <asp:RadioButton ID="RB_NoIva" GroupName="ImpIVA" runat="server" />
                                    SottoConto : 
             <asp:TextBox ID="Txt_Sottoconto" CssClass="MyAutoComplete" runat="server"
                 Width="400px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Per Prorata :</label>
                                    <asp:CheckBox ID="Chk_Prorata" runat="server" /><br />
                                    <br />
                                    <p style="text-align: left;">
                                        <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Text=""></asp:Label>
                                    </p>
                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
