﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO
Imports System.Security.Cryptography
Partial Class GeneraleWeb_AllegatoRegistrazione
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub

        LoadDati()
    End Sub


    Private Sub LoadDati()
        Dim Documentazione As New Cls_MovimentoContabile


        Documentazione.NumeroRegistrazione = Val(Request.Item("NumeroRegistrazione"))
        Documentazione.Leggi(Session("DC_GENERALE"), Documentazione.NumeroRegistrazione)


        lbl_Registrazione.Text = "Numero Registrazione  <b>" & Documentazione.NumeroRegistrazione & "</b>  "
        lbl_Registrazione.Text = lbl_Registrazione.Text & "Numero Documento  <b>" & Documentazione.NumeroDocumento & "</b>"
        lbl_Registrazione.Text = lbl_Registrazione.Text & "  Data Registrazione  <b>" & Format(Documentazione.DataRegistrazione, "dd/MM/yyyy") & "</b><br/ >"

        lbl_Allegato.Text = "&nbsp;"
        lbl_Allegato1.Text = ""
        lbl_Allegato2.Text = ""
        lbl_Allegato3.Text = ""
        lbl_Allegato4.Text = ""
        lbl_Allegato5.Text = ""
        lbl_Allegato6.Text = ""
        lbl_Allegato7.Text = ""


        lbl_Allegato1.Visible = False
        lbl_Allegato2.Visible = False
        lbl_Allegato3.Visible = False
        lbl_Allegato4.Visible = False
        lbl_Allegato5.Visible = False
        lbl_Allegato6.Visible = False
        lbl_Allegato7.Visible = False


        Lnk_Delete1.Visible = False
        Lnk_Delete2.Visible = False
        Lnk_Delete3.Visible = False
        Lnk_Delete4.Visible = False
        Lnk_Delete5.Visible = False
        Lnk_Delete6.Visible = False
        Lnk_Delete7.Visible = False

        Leggi(Documentazione.NumeroRegistrazione)
        If lbl_Allegato.Text = "" Then
            lbl_Allegato.Enabled = False
        End If
    End Sub

    Private Sub Leggi(ByVal NumeroRegistrazione As Integer)
        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If



        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione)
        End If



        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Dim Entrato As Boolean = False

            If lbl_Allegato6.Text <> "" And Not Entrato Then
                lbl_Allegato7.Text = objItem.Name
                lbl_Allegato7.Visible = True
                Lnk_Delete7.Visible = True
                Entrato = True
            End If


            If lbl_Allegato5.Text <> "" And Not Entrato Then
                lbl_Allegato6.Text = objItem.Name
                lbl_Allegato6.Visible = True
                Lnk_Delete6.Visible = True
                Entrato = True
            End If

            If lbl_Allegato4.Text <> "" And Not Entrato Then
                lbl_Allegato5.Text = objItem.Name
                lbl_Allegato5.Visible = True
                Lnk_Delete5.Visible = True
                Entrato = True
            End If

            If lbl_Allegato3.Text <> "" And Not Entrato Then
                lbl_Allegato4.Text = objItem.Name
                lbl_Allegato4.Visible = True
                Lnk_Delete4.Visible = True
                Entrato = True
            End If
            If lbl_Allegato2.Text <> "" And Not Entrato Then
                lbl_Allegato3.Text = objItem.Name
                lbl_Allegato3.Visible = True
                Lnk_Delete3.Visible = True
                Entrato = True
            End If
            If lbl_Allegato1.Text <> "" And Not Entrato Then
                lbl_Allegato2.Text = objItem.Name
                lbl_Allegato2.Visible = True
                Lnk_Delete2.Visible = True
                Entrato = True
            End If
            If lbl_Allegato.Text <> "&nbsp;" And Not Entrato Then
                lbl_Allegato1.Text = objItem.Name
                lbl_Allegato1.Visible = True
                Lnk_Delete1.Visible = True
                Entrato = True
            End If
            If Entrato = False Then
                lbl_Allegato.Text = objItem.Name
                lbl_Allegato.Visible = True
                Lnk_Delete.Visible = True
            End If
        Next

    End Sub


    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpLoadFile.Click
        Dim iLen As Double
        Dim Allegato As String = ""
        Dim NomeFile As String = ""

        Dim NomeSocieta As String
        Dim NumeroRegistrazione As Integer = Request.Item("NumeroRegistrazione")


        If FileUpload1.FileName.ToString.ToUpper.IndexOf(".PDF") < 0 Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "alert('Puoi allegare solo PDF');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        If FileUpload1.HasFile Then
            iLen = FileUpload1.PostedFile.ContentLength


            If iLen > 1024 * 1024 Then

                ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "alert('File troppo grande per essere allegato');", True)
                Exit Sub
            End If

            Dim btArr(iLen) As Byte

            FileUpload1.PostedFile.InputStream.Read(btArr, 0, iLen)
            Allegato = Convert.ToBase64String(btArr)

            NomeFile = FileUpload1.FileName
        End If

        If NomeFile = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "alert('devi selezionare un file');", True)
            Exit Sub        
        End If

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If



        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione)
        Else

            'Try
            '    Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & lbl_Allegato.Text)
            'Catch ex As Exception

            'End Try
        End If



        Dim twFatture As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & NomeFile)
        twFatture.Write(Allegato)
        twFatture.Close()

        LoadDati()
    End Sub

    Function Allegato(ByVal NumeroRegistrazione As Long, ByVal NomeFile As String, ByRef Datix64 As String) As String
        Dim NomeSocieta As String

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If



        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione)
        End If


        Datix64 = ""

        

        Dim readFile As System.IO.TextReader = New StreamReader(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & NomeFile)

        Datix64 = readFile.ReadToEnd()



        


        Return NomeFile
    End Function


    Private Sub DownloadFile(ByVal NomeFileRichiesta As String)
        Dim NomeFile As String = ""
        Dim CopiaNomeFile As String = ""
        Dim Dati As String = ""


        Dim NomeSocieta As String
        Dim NumeroRegistrazione As Integer = Request.Item("NumeroRegistrazione")

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        NomeFile = Allegato(Val(Request.Item("NumeroRegistrazione")), NomeFileRichiesta, Dati)


        CopiaNomeFile = "Copia" & NomeFile

        Dim binaryData() As Byte = Convert.FromBase64String(Dati)


        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & CopiaNomeFile)
        Catch ex As Exception

        End Try

        Dim fs As New FileStream(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & CopiaNomeFile, FileMode.CreateNew)


        fs.Write(binaryData, 0, binaryData.Length)
        fs.Close()


        Try
            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=File.pdf")

            Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & CopiaNomeFile)

            Response.Flush()
            Response.End()
        Finally
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & CopiaNomeFile)
        End Try

    End Sub

    Protected Sub lbl_Allegato_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato.Click
        DownloadFile(lbl_Allegato.Text)
    End Sub

    Protected Sub Lnk_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete.Click
        DeleteItem(lbl_Allegato.Text)
    End Sub


    Private Sub DeleteItem(ByVal Nome As String)

        Dim NomeSocieta As String
        Dim NumeroRegistrazione As Integer = Request.Item("NumeroRegistrazione")

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & Nome)
        Catch ex As Exception

        End Try


        LoadDati()
    End Sub

    Protected Sub Lnk_Delete1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete1.Click
        DeleteItem(lbl_Allegato1.Text)
    End Sub

    Protected Sub Lnk_Delete2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete2.Click
        DeleteItem(lbl_Allegato2.Text)
    End Sub

    Protected Sub Lnk_Delete3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete3.Click
        DeleteItem(lbl_Allegato3.Text)
    End Sub

    Protected Sub Lnk_Delete4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete4.Click
        DeleteItem(lbl_Allegato4.Text)
    End Sub

    Protected Sub lbl_Allegato1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato1.Click
        DownloadFile(lbl_Allegato1.Text)
    End Sub

    Protected Sub lbl_Allegato2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato2.Click
        DownloadFile(lbl_Allegato2.Text)
    End Sub

    Protected Sub lbl_Allegato3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato3.Click
        DownloadFile(lbl_Allegato3.Text)
    End Sub

    Protected Sub lbl_Allegato4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato4.Click
        DownloadFile(lbl_Allegato4.Text)
    End Sub

    Protected Sub lbl_Allegato5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato5.Click
        DownloadFile(lbl_Allegato5.Text)
    End Sub

    Protected Sub lbl_Allegato6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato6.Click
        DownloadFile(lbl_Allegato6.Text)
    End Sub

    Protected Sub lbl_Allegato7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_Allegato7.Click
        DownloadFile(lbl_Allegato7.Text)
    End Sub

    Protected Sub Lnk_Delete5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete5.Click
        DeleteItem(lbl_Allegato5.Text)
    End Sub

    Protected Sub Lnk_Delete6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete6.Click
        DeleteItem(lbl_Allegato6.Text)
    End Sub

    Protected Sub Lnk_Delete7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_Delete7.Click
        DeleteItem(lbl_Allegato7.Text)
    End Sub
End Class
