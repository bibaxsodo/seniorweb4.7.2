﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class GeneraleWeb_ApplicaRegola
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Protected Sub Esegui_Ricerca(ByVal ToExcel As Boolean)
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim MySql As String
        Dim Condizione As String
        Dim Tipo As Integer = 0
        Dim MySqlrs As String
        Dim MySqlrs2 As String
        Dim Saldo As Double


        Condizione = ""
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()



        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If



        Dim cmd As New OleDbCommand()

        Dim SubSelectLegameBudgetNull As String = ""

        Dim ContoDiCosto2 As String = ""
        Dim SelectPDC As String = ""

        cmd.CommandText = "Select * From MovimentiContabiliTesta Where " & Condizione
        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAl As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
        End If

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Dt.Reg.", GetType(String))
        Tabella.Columns.Add("N.Doc.", GetType(String))
        Tabella.Columns.Add("Dt.Doc.", GetType(String))
        Tabella.Columns.Add("Importo Registrazione", GetType(String))
        Tabella.Columns.Add("Segnalazione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim LegatoEliminato As Boolean = False
            If campodbn(myPOSTreader.Item("NumeroRegistrazione")) = 187 Then
                LegatoEliminato = False
            End If
            If Chk_EliminaAutomatiche.Checked = True Then

                Dim CommandVerificaLegato As New OleDbCommand("Select *  From LegameBudgetRegistrazione Where NumeroRegistrazione = ? And Automatico =1", cn)

                CommandVerificaLegato.Parameters.Add("@NumeroRegistrazione", OleDbType.Integer, 8)
                CommandVerificaLegato.Parameters("@NumeroRegistrazione").Value = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
                Dim ReadVerificaLegato As OleDbDataReader = CommandVerificaLegato.ExecuteReader()
                If ReadVerificaLegato.Read Then

                    LegatoEliminato = True
                    Dim CommandDeleteLegato As New OleDbCommand("Delete  From LegameBudgetRegistrazione Where NumeroRegistrazione = ? And Automatico =1", cn)

                    CommandDeleteLegato.Parameters.Add("@NumeroRegistrazione", OleDbType.Integer, 8)
                    CommandDeleteLegato.Parameters("@NumeroRegistrazione").Value = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
                    CommandDeleteLegato.ExecuteNonQuery()
                End If

            End If

            Dim ApplicaRegola As New Cls_RegoleBudget
            Dim RegolaApplicata As Boolean = False

            If ApplicaRegola.ApplicaRegola(Session("DC_GENERALE"), campodbn(myPOSTreader.Item("NumeroRegistrazione"))) = True Then

                RegolaApplicata = True
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = campodbn(myPOSTreader.Item("NumeroRegistrazione"))

                Dim RegN As New Cls_MovimentoContabile

                RegN.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
                RegN.Leggi(Session("DC_GENERALE"), RegN.NumeroRegistrazione)


                myriga(1) = Format(RegN.DataRegistrazione, "dd/MM/yyyy")
                myriga(2) = RegN.NumeroDocumento

                myriga(3) = Format(RegN.DataDocumento, "dd/MM/yyyy")


                myriga(4) = Format(RegN.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")
                If LegatoEliminato = True Then
                    myriga(5) = "Rigenerato collegamento analitica"
                Else
                    myriga(5) = "Generato collegamento analitica"
                End If
                Tabella.Rows.Add(myriga)
            End If

            If RegolaApplicata = False And LegatoEliminato = True Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = campodbn(myPOSTreader.Item("NumeroRegistrazione"))

                Dim RegN As New Cls_MovimentoContabile

                RegN.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
                RegN.Leggi(Session("DC_GENERALE"), RegN.NumeroRegistrazione)


                myriga(1) = Format(RegN.DataRegistrazione, "dd/MM/yyyy")
                myriga(2) = RegN.NumeroDocumento

                myriga(3) = Format(RegN.DataDocumento, "dd/MM/yyyy")


                myriga(4) = Format(RegN.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")
                myriga(5) = "Collegamento analitica eliminato"

                Tabella.Rows.Add(myriga)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        If ToExcel = True Then
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.DataBind()


            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()

        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()



        End If



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")


        Txt_DataDal.Text = Format(DateSerial(Year(Now), Month(Now), 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")

        EseguiJS()

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call Esegui_Ricerca(False)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click


        Response.Redirect("Menu_Budget.aspx")

    End Sub


End Class
