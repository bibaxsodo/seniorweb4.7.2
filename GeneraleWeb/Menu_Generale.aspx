﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Generale" CodeFile="Menu_Generale.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Generale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });

        $(document).ready(function () {
            // JAVASCRIPT (jQuery)

            // Trigger action when the contexmenu is about to be shown
            $("#menutastodestro").bind("contextmenu", function (event) {

                // Avoid the real one
                event.preventDefault();

                // Show contextmenu
                $("#rightmenu").toggle(100).

                    // In the right position (the mouse)
                    css({
                        top: event.pageY + "px",
                        left: event.pageX + "px"
                    });
            });

            $("#menutastodestro2").bind("contextmenu", function (event) {

                // Avoid the real one
                event.preventDefault();

                // Show contextmenu
                $("#rightmenu").toggle(100).

                    // In the right position (the mouse)
                    css({
                        top: event.pageY + "px",
                        left: event.pageX + "px"
                    });
            });

            $("#menutastodestro3").bind("contextmenu", function (event) {

                // Avoid the real one
                event.preventDefault();

                // Show contextmenu
                $("#rightmenu").toggle(100).

                    // In the right position (the mouse)
                    css({
                        top: event.pageY + "px",
                        left: event.pageX + "px"
                    });
            });

            $("#menutastodestro4").bind("contextmenu", function (event) {

                // Avoid the real one
                event.preventDefault();

                // Show contextmenu
                $("#rightmenu").toggle(100).

                    // In the right position (the mouse)
                    css({
                        top: event.pageY + "px",
                        left: event.pageX + "px"
                    });
            });


            // If the document is clicked somewhere
            $(document).bind("mousedown", function (e) {

                // If the clicked element is not the menu
                if (!$(e.target).parents(".custom-menu").length > 0) {

                    // Hide it
                    $("#rightmenu").hide(100);
                }
            });


            // If the menu element is clicked
            $(document).click(function () {

                // This is the triggered action name
                switch ($(this).attr("data-action")) {

                    // A case for each action. Your actions here    
                    case "Anagrafica": break;
                    case "Addebiti Accrediti": break;
                    case "Movimenti": break;
                    case "Diurno": break;
                    case "Denaro": break;
                    case "Documenti": break;
                    case "Estratto Conto": break;
                }

                // Hide it AFTER the action was triggered
                $("#rightmenu").hide(100);
            });
        });

        function chiudinews() {
            document.cookie = "Entrata=14; expires=July 31, 2020 12:00:00 UTC";
            $("#news").css('visibility', 'hidden');
            posizionenotifiche();
        }

    </script>
    <style>
        #news {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 25%;
            height: 50%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center; width: 20%;">
                                    <div id="menutastodestro">
                                        <a href="UltimiMovimenti.aspx?TIPO=PRNT" id="Href_Registrazioni">
                                            <img src="../images/Menu_PrimaNota.png" class="Effetto" alt="Prima Note" style="border-width: 0;"></a>
                                    </div>
                                    <ul class="custom-menu" id="rightmenu">
                                        <a href="Ricerca.aspx?TIPO=MENU" id="A1">
                                            <li data-action="first">RICERCA</li>
                                        </a>
                                    </ul>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <div id="menutastodestro2">
                                        <a href="UltimiMovimenti.aspx?TIPO=INC" id="Href_Registrazioni">
                                            <img src="../images/Menu_GestioneIncassiPagamenti.png" class="Effetto" alt="Incassi Pagamenti" style="border-width: 0;"></a>
                                    </div>
                                    <ul class="custom-menu" id="Ul1">
                                        <a href="Ricerca.aspx?TIPO=MENU" id="A2">
                                            <li data-action="first">RICERCA</li>
                                        </a>
                                    </ul>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <div id="menutastodestro3">
                                        <a href="UltimiMovimenti.aspx?TIPO=DOC" id="Href_Registrazioni">
                                            <img alt="Documenti" src="../images/Menu_GestioneDocumenti.png" class="Effetto" style="border-width: 0;"></a>
                                    </div>
                                    <ul class="custom-menu" id="Ul2">
                                        <a href="Ricerca.aspx?TIPO=MENU" id="A3">
                                            <li data-action="first">RICERCA</li>
                                        </a>
                                    </ul>
                                </td>

                                <td style="text-align: center; width: 20%;">
                                    <div id="menutastodestro4">
                                        <a href="UltimiMovimenti.aspx?TIPO=INCS" id="Href_Registrazioni">
                                            <img alt="Incassi Pagamenti Scadenziario" src="../images/Menu_IncassoPagamentoScadenziario.png" class="Effetto" style="border-width: 0;"></a>
                                    </div>
                                    <ul class="custom-menu" id="Ul3">
                                        <a href="Ricerca.aspx?TIPO=MENU" id="A4">
                                            <li data-action="first">RICERCA</li>
                                        </a>
                                    </ul>
                                </td>
                                <td style="text-align: center; width: 20%;">

                                    <a href="ElencoClientiFornitori.aspx" id="Href_ClientiFornitori">
                                        <img alt="Anagrafica Clienti Fornitori" src="../images/Menu_ClientiFornitori.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">PRIMA NOTA</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">INCASSI E PAGAMENTI</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">DOCUMENTI</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">INC. PAG. SCAD.</span></td>
                                <td style="text-align: center; vertical-align: top; width: 20%;"><span class="MenuText">CLI. FOR.</span></td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="ElencoPianoconti.aspx" id="Href_PianoConti">
                                        <img src="../images/Menu_PianoDeiConti.png" class="Effetto" alt="Piano Conti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Menu_Budget.aspx" id="Href_Budget">
                                        <img alt="Budget" src="../images/Menu_Budget.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Menu_Stampe.aspx">
                                        <img alt="Stampe" src="../images/Menu_Stampe.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Menu_Visualizzazioni.aspx" id="Href_Visualizzazioni">
                                        <img src="../images/Menu_Visualizzazioni.png" class="Effetto" alt="Visualizzazione" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Menu_Tabelle.aspx" id="Href_Tabelle">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Taeblle" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PIANO DEI CONTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="lbl_BudgeAnalitica" runat="server" Text="BUDGET"></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAMPE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">VISUALIZZAZIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TABELLE</span></td>
                            </tr>

                            <tr>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td style="text-align: center;">
                        <a href="Elenco_Cespiti.aspx" id="Href_Cespiti">
                            <img alt="Servizi" src="../images/CESPITI.png" class="Effetto" style="border-width: 0;"></a>
                    </td>


                    <td style="text-align: center;">
                        <a href="Menu_Servizi.aspx" id="Href_Strumenti">
                            <img alt="Servizi" src="../images/Menu_Strumenti.png" class="Effetto" style="border-width: 0;"></a>
                    </td>

                    <td style="text-align: center;">
                        <asp:ImageButton ID="Btn_ImpXML" runat="server" src="../images/Menu_Config.png" class="Effetto" Style="border-width: 0;" />
                    </td>
                    <td style="text-align: center;">
                        <asp:ImageButton ID="Btn_ExpXML" runat="server" src="../images/Menu_ModificaCserv.jpg" class="Effetto" Style="border-width: 0;" />
                    </td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">CESPITI</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">STRUMENTI</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                        <asp:Label ID="lblImpXML" runat="server" Text="Imp XML"></asp:Label></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                        <asp:Label ID="lblExpXML" runat="server" Text="Exp XML"></asp:Label></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                </tr>

            </table>
            </td>
    </tr>
    
    </table> 
    
       
        </div>

        <div id="news" style="position: absolute; right: 10px; top: 120; overflow: auto;">
            <h1>Novità:</h1>
            <br />
            E' stata introdotta la funzionalità per la creazione dell'autofattura e il girocnto per Reverse Charge e split payment, questa funzionalità deve essere attivtà sulla causale contabile del documento inserito manualmente il flag da attivare è :
            <br />
            <br />
            Giroconto Reverse Automatico (impostare a SI)<br />
            <br />
            è necessario perche la funzionalità si comporti in modo corretta che sia indicato la Causale Documento Reverse/Split e Causale Giroconto Reverse/Split.       
      
       <br />
            <br />

            <br />
            E' stato introdotto la possibilità di gestire 2 tipologie di causali in fase di import documenti dall'XML, configurabili in Tabelle-Dati Generali e quindi selezionabili in fase di import dei documenti.
       <br />
            <br />


            <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
            <br />

            <hr />
            <br />
            <h1>Novità:</h1>
            <br />
            <b>Imposta di bollo sulle fatture cartacee, elettroniche e documenti elettronici rilevanti ai fini fiscali</b><br />
            <br />
            Nei documenti fattura per i quali è previsto il bollo, il documento avrà il flg valorizzato in modo automatico per le fatture soggette a bollo nei registri non cartacei tale per cui in fase di generazione del documento in formato elettronico vi sia specifica annotazione.<br />
            <br />
            <b>Novità:</b><br />
            <br />
            La tipologia di registro non cartaceo è attivabile tramite parametro di configurazione. Nella Tabella Tipo Registro, disponibile nel Menù Generale - Tabelle - Tipo Registro, è possibile indicare se il registro è cartaceo con valorizzazione del parametro <i>Registro Cartaceo</i><br />
            <br />
            La gestione del bollo è attivabile tramite parametro di configurazione per tutti i documenti/tipo operazione dove è possibile indicare se soggetti a Bollo e se Bollo Virtuale.<br />
            Occorre nella Tabella Tipo Operazione, disponibile nel Menù Ospiti - Tabelle - Menu' Configurazione, indicare la configurazione voluta con apposita valorizzazione dei flg.<br />
            <br />
            <b>Gestione Scadenziario</b><br />
            <br />
            Per gli utenti con la gestione dello scadenziario nei documenti, è stata data la possibilità di legare tassativamente il pagamento alla scadenza<br />
            Questo controllo è attivabile tramite parametro disponibile nel Menù Generale - Tabelle - Dati Generali.<br />
            Il parametro si chiama <i>Blocca Legame su documenti con Scadenzario</i><br />
            Inoltre è attivabile il parametro <i>Scadenziario Check Chiuso</i> che rende obbligatoria la chiusura della scadenza durante il pagamento.<br />
            <br />
            <b>Novità:</b>
            Per gli utenti con la gestione dello scadenziario nei documenti, è stata data la possibilità di inserire ulteriori date di scadenza tra quelle presentate dal tipo di pagamento.

       <br />
            <br />
            <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
            <br />
            <h1>Novità:</h1>
            <br />
            E’ stata introdotta la nuova funzione ELABORAZIONE FATTURE nel Menù Ospiti – Principale con la possibilità di eseguire in un solo comando sial il CALCOLO RETTE che l’ELABORAZIONE delle stesse.<br />
            <br />
            L’utente, in fase di esecuzione del comando, potrà accedere a tutte le funzioni di Senior e sarà cura del sistema informare tramite messaggio sia nel Menù Principale che nella notifica di Google Chrome l’elaborazione terminata.<br />
            <br />
            I messaggi di eventuali errori o segnalazioni di Warning sono invariati rispetto al funzionamento di prodotto così come la Conferma e la procedura di stampa dei file fatture.<br />
            <br />
            <br />
            <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
            <br />
            <h1>Novità:</h1>
            <br />
            E’ stata data una nuova organizzazione al menù principale  <b>Ospiti – Principale</b><br />
            <br />
            È presente il nuovo comando <b>ePERSONAM </b>all’interno del quale sono presenti tutte le funzioni di import dei dati dall’applicativo ePersonam vs l’applicativo Senior<br />
            <br />
            È presente il comando <b>EXPORT</b>all’interno del quale sono disponibili tutte le funzioni di generazione dei file xml e/o export dati precedentemente disponibili all’interno del comando <b>STRUMENTI</b><br />
            <br />
            <br />
            <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
            <br />
            <h1>Novità:</h1>
            E’ stata introdotta la possibilità di gestire i LISTINI RETTE da abilitare mediante creazione di uno o più listini a seconda delle diverse rette al fine di associarlo a ciascun ospite.<br />
            <br />
            Per attivare tale nuova funzione occorre accedere alla seguente tabella LISTINO disponibile da Ospiti - Tabelle – Listino e compilare i vari attori con i dati dei differenti contributi.<br />
            <br />
            Accedendo al dettaglio di ciascun ospite, sarà presente nel menù laterale di sx il nuovo tab LISTINO mediante il quale si potrà fare l’associazione tra ospite ed il listino rette di riferimento. E’ stata introdotta la possibilità di gestire i LISTINI RETTE da abilitare mediante creazione di uno o più listini a seconda della propria offerta al fine di associarlo a ciascun ospite.<br />
            <br />
            Per attivare tale nuova funzione occorre accedere alla seguente tabella LISTINO disponibile da Ospiti - Tabelle – Listino e compilare i vai attori con i dati della propria offerta.<br />
            <br />
            Accedendo al dettaglio di ciascun ospite, sarà presente nel menù laterale di sx il nuovo tab LISTINO mediante il quale si potrà fare l’associazione tra ospite ed il listino di riferimento<br />
            <br />
            <input type="button" style="position: inherit; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
        </div>

    </form>
</body>
</html>
