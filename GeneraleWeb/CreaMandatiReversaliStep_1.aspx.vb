﻿
Partial Class GeneraleWeb_CreaMandatiReversaliStep_1
    Inherits System.Web.UI.Page



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub GeneraleWeb_CreaMandatiReversaliStep_1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        DD_TipoPagametno.Items.Clear()

        DD_TipoPagametno.Items.Add("")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = ""

        DD_TipoPagametno.Items.Add("Cassa")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "01"

        DD_TipoPagametno.Items.Add("Bonifico")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "53"

        DD_TipoPagametno.Items.Add("C/C Postale")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "09"

        DD_TipoPagametno.Items.Add("Come da allegato")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "85"

        DD_TipoPagametno.Items.Add("Con disposizione documento esterno")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "84"

        Dim Cofog As New Cls_TabellaCofog

        Cofog.UpDateDropBox(Session("DC_GENERALE"), DD_Cofog)

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If DD_TipoPagametno.SelectedItem.Text = "Come da allegato" And Txt_NumeroElenco.Text = "" Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Manca il Numero Elenco</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_NumeroElenco.Text <> "" And DD_TipoPagametno.SelectedItem.Text <> "Come da allegato" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Il Tipo Pagamento deve essere Come da allegato</center>');", True)
            Call EseguiJS()
            Exit Sub

        End If


        If Txt_Delegato.Text.Length > 3 And Txt_Delegato.Text.IndexOf(" ") > 0 Then
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Delegato.Text)

            Dim AnCliFor As New Cls_ClienteFornitore

            AnCliFor.CODICEDEBITORECREDITORE = xVettore(0)
            AnCliFor.Leggi(Session("DC_OSPITE"))
            If AnCliFor.CodiceFiscale.Trim = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Al Delegato / Quietanziante manca il Codice Fiscale</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        If (Txt_CodiceCig.Text.Trim = "") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Codice Cig</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If (DD_Cofog.SelectedValue.Trim = "") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Cofog</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim XPar As New Cls_ParametriGenerale


        XPar.LeggiParametri(Session("DC_GENERALE"))

        Dim Appoggio(1000) As String
        Dim Indice As Integer = 0        
        Dim Mandato As Boolean = False
        Dim Reversale As Boolean = False


        If Not IsNothing(XPar.CausaliMandati) Then
            Appoggio = SplitWords(XPar.CausaliMandati)

            For Indice = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(Indice)) Then

                    If Appoggio(Indice) = Request.Item("CausaleContabile") Then
                        Mandato = True
                    End If
                End If
            Next
        End If
        For Indice = 0 To Appoggio.Length - 1 : Appoggio(Indice) = "" : Next

        If Not IsNothing(XPar.CausaliReversali) Then
            Appoggio = SplitWords(XPar.CausaliReversali)

            For Indice = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(Indice)) Then

                    If Appoggio(Indice) = Request.Item("CausaleContabile") Then
                        Reversale = True
                    End If
                End If
            Next
        End If
        If Mandato = False And Reversale = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Causale Non Mandato O Reversale</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Reversale = True And Txt_Delegato.Text.Trim <> "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Il Delegato / Quietanziante può essere solo con il Mandato</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim x As New Cls_MovimentoContabile
        Dim DecConto As New Cls_Pianodeiconti

        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Val(Session("NumeroRegistrazione")))




        x.CausaleContabile = Request.Item("CausaleContabile")
        x.NumeroRegistrazione = Val(Request.Item("NumeroRegistrazione"))
        x.DataRegistrazione = Request.Item("DataRegistrazione")
        x.Descrizione = Request.Item("Descrizione")




        If Rb_Esente_Bollo.Checked = True Then x.BolloMandato = 0
        If RB_CaricoEnte_Bollo.Checked = True Then x.BolloMandato = 1
        If Rb_CaricoCliente_Bollo.Checked = True Then x.BolloMandato = 2

        If Rb_Esente_Spese.Checked = True Then x.EspSpeseMandato = 0
        If RB_CaricoEnte_Spese.Checked = True Then x.EspSpeseMandato = 1
        If Rb_CaricoCliente_Spese.Checked = True Then x.EspSpeseMandato = 2

        x.EspTipoPagamento = DD_TipoPagametno.SelectedValue
        x.EspNumeroRegolarizzazione = Val(Txt_NumeroRegolazione.Text)

        x.AnnoImpegno = Val(Txt_AnnoImpegno.Text)
        x.NumeroImpegno = Val(Txt_NumeroImpegno.Text)
        x.CodiceCig = Txt_CodiceCig.Text
        x.EspCodiceSIOPE = Txt_Siope.Text
        x.EspCodiceCUP = Txt_Cup.Text
        x.EspDataEsecuzionePagamento = IIf(IsDate(Txt_DataEsecuzionePagamento.Text), Txt_DataEsecuzionePagamento.Text, Nothing)
        x.EspNumeroElenco = Txt_NumeroElenco.Text
        x.EspTipoPagamento = DD_TipoPagametno.SelectedValue

        If Txt_Delegato.Text.Length > 3 And Txt_Delegato.Text.IndexOf(" ") > 0 Then
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Delegato.Text)

            x.EspCodiceDelegatoQuietanzante = xVettore(0)
        End If
        x.Cofog = DD_Cofog.SelectedValue


        Dim Causale As New Cls_CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), x.CausaleContabile)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Dim YMastro As Long
        Dim YConto As Long
        Dim YSottoconto As Long

        Dim Vettore(100) As String
        Vettore = SplitWords(Request.Item("SottocontoDare"))

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))

        Vettore = SplitWords(Request.Item("SottocontoAvere"))

        YMastro = Val(Vettore(0))
        YConto = Val(Vettore(1))
        YSottoconto = Val(Vettore(2))

        If IsNothing(x.Righe(0)) Then
            x.Righe(0) = New Cls_MovimentiContabiliRiga
        End If
        x.Righe(0).Importo = CDbl(Request.Item("Importo"))
        x.Righe(0).MastroPartita = Mastro
        x.Righe(0).ContoPartita = Conto
        x.Righe(0).SottocontoPartita = Sottoconto

        x.Righe(0).MastroContropartita = YMastro
        x.Righe(0).ContoContropartita = YConto
        x.Righe(0).SottocontoContropartita = YSottoconto

        x.Righe(0).DareAvere = "D"
        x.Righe(0).Segno = "+"
        If Causale.Righe(0).DareAvere = "D" Then
            x.Righe(0).RigaDaCausale = 1
        Else
            x.Righe(0).RigaDaCausale = 2
        End If



        If IsNothing(x.Righe(1)) Then
            x.Righe(1) = New Cls_MovimentiContabiliRiga
        End If
        x.Righe(1).Importo = CDbl(Request.Item("Importo"))
        x.Righe(1).MastroPartita = YMastro
        x.Righe(1).ContoPartita = YConto
        x.Righe(1).SottocontoPartita = YSottoconto

        x.Righe(1).MastroContropartita = Mastro
        x.Righe(1).ContoContropartita = Conto
        x.Righe(1).SottocontoContropartita = Sottoconto

        x.Righe(1).DareAvere = "A"
        x.Righe(1).Segno = "+"
        If Causale.Righe(1).DareAvere = "D" Then
            x.Righe(1).RigaDaCausale = 1
        Else
            x.Righe(1).RigaDaCausale = 2
        End If



        If x.Scrivi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione"))) = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione non eseguita</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Session("NumeroRegistrazione") = x.NumeroRegistrazione
        Response.Redirect("CreaMandatiReversaliStep_2.aspx")
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class
