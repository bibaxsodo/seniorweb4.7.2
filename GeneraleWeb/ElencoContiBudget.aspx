﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_ElencoContiBudget" CodeFile="ElencoContiBudget.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elenco Conti Budget</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">      
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Generale - Budget - Conti Budget</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>


                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">Anno : 
     <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" MaxLength="4" Width="66px"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>



                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImgHome" ImageUrl="images/Home.jpg" Width="112px" Height="100px" alt="Ricerca Registrazioni" class="Effetto" runat="server" /><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>

                    <td class="style1" colspan="2" valign="top">

                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="ImgRicerca" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" alt="Ricerca Registrazioni" runat="server" /><br />
                        </div>


                        <asp:GridView ID="Grid" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                            PageSize="20">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="26px" ItemStyle-Width="26px" FooterStyle-Width="26px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                            ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                            CommandArgument="<%#   Container.DataItemIndex  %>" ToolTip="Richiama" />
                                    </ItemTemplate>

                                    <FooterStyle Width="26px"></FooterStyle>

                                    <HeaderStyle Width="26px"></HeaderStyle>

                                    <ItemStyle Width="26px"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; text-align: center;">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>

            </table>

        </div>
    </form>
</body>
</html>

