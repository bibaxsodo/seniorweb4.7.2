﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System
Imports System.Web
Imports System.Configuration
Imports System.Text
Imports System.Web.Script.Serialization


Partial Class GeneraleWeb_ProgressivaIVA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        

        Dim Gruppo As New Cls_Gruppi


        Gruppo.UpDateDropBox(Session("DC_TABELLE"), DD_GRUPPO)


        If Val(Request.Item("ID")) > 0 Then
            Dim cn As New OleDbConnection



            Dim ConnectionString As String = Session("DC_TABELLE")

            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()

            Dim cmd As New OleDbCommand

            cmd.CommandText = ("select * from ProgressiviIVA WHERE ID = ? ")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Id", Request.Item("ID"))


            Dim ProgIVA As OleDbDataReader = cmd.ExecuteReader()
            If ProgIVA.Read Then
                Txt_Anno.Text = campodbn(ProgIVA.Item("Anno"))
                Dd_Mese.SelectedValue = campodbn(ProgIVA.Item("Mese"))
                DD_GRUPPO.SelectedValue = campodbn(ProgIVA.Item("Gruppo"))

                Txt_Importo.Text = Format(campodbn(ProgIVA.Item("Importo")), "#,##0.00")
            End If
            ProgIVA.Close()

            cn.Close()

        End If

    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Digitare anno');", True)
            Exit Sub
        End If


        If Val(Dd_Mese.SelectedValue) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Selezionare mese');", True)
            Exit Sub
        End If

        If Val(DD_GRUPPO.SelectedValue) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Selezionare Gruppo');", True)
            Exit Sub
        End If


        Dim cn As New OleDbConnection


        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "PROGRESSIVIIVA", Txt_Anno.Text & "." & Dd_Mese.SelectedValue & "." & DD_GRUPPO.SelectedValue & "." & Val(Txt_Importo.Text))


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If Val(Request.Item("id")) = 0 Then
            Dim CmdVer As New OleDbCommand

            CmdVer.CommandText = "Select * from ProgressiviIVA where ANNO = ? and  MESE = ? and GRUPPO = ?"
            CmdVer.Connection = cn
            CmdVer.Parameters.AddWithValue("@ANNO", Txt_Anno.Text)
            CmdVer.Parameters.AddWithValue("@MESE", Dd_Mese.SelectedValue)
            CmdVer.Parameters.AddWithValue("@GRUPPO", DD_GRUPPO.SelectedValue)



            Dim ProgIVA As OleDbDataReader = CmdVer.ExecuteReader()
            If ProgIVA.Read Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Periodo già presente');", True)
                cn.Close()
                Exit Sub
            End If
            CmdVer.Clone()

        End If

        Dim cmd As New OleDbCommand

        If Val(Request.Item("id")) > 0 Then
            cmd.CommandText = ("UPDATE ProgressiviIVA SET ANNO = ?,MESE = ?,GRUPPO = ?,IMPORTO  = ? WHERE ID = " & Val(Request.Item("id")))
        Else
            cmd.CommandText = ("INSERT INTO ProgressiviIVA  (ANNO,MESE,GRUPPO,IMPORTO) VALUES (?,?,?,?) ")
        End If
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ANNO", Txt_Anno.Text)
        cmd.Parameters.AddWithValue("@MESE", Dd_Mese.SelectedValue)
        cmd.Parameters.AddWithValue("@GRUPPO", DD_GRUPPO.SelectedValue)
        cmd.Parameters.AddWithValue("@IMPORTO", CDbl(Txt_Importo.Text))
        cmd.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("Elenco_ProgressivaIVA.ASPX")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_ProgressivaIVA.ASPX")

    End Sub
End Class


