﻿Imports System.Threading
Imports System.Data.OleDb

Partial Class GeneraleWeb_StampaSaldi
    Inherits System.Web.UI.Page
    Private Delegate Sub DoWorkDelegate(ByRef data As Object)
    Private _work As DoWorkDelegate


    Private Sub DoWork(ByVal data As Object)
        Call Stampa(data)
    End Sub


    Protected Sub Stampa(ByVal data As Object)

        Dim StampeDb As New ADODB.Connection
        Dim MyRs As New ADODB.Recordset
        Dim RsReg As New ADODB.Recordset
        Dim CausaleVista(100)
        Dim Condizione As String
        Dim WSocieta As String
        Dim TrovatoLstVL As Boolean
        Dim TConta As Integer

        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection

        Dim Txt_MastroDalText As Long
        Dim Txt_MastroAlText As Long
        Dim Txt_ContoDalText As Long
        Dim Txt_ContoAlText As Long
        Dim Txt_SottocontoDalText As Long
        Dim Txt_SottocontoAlText As Long
        Dim Vettore(100) As String

        Dim Stampa As New StampeGenerale


        Vettore = SplitWords(Txt_SottocontoDal.Text)

        If Vettore.Length < 2 Then
            Exit Sub
        End If

        Txt_MastroDalText = Val(Vettore(0))
        Txt_ContoDalText = Val(Vettore(1))
        Txt_SottocontoDalText = Val(Vettore(2))


        Vettore = SplitWords(Txt_SottocontoAl.Text)

        If Vettore.Length < 2 Then
            Exit Sub
        End If

        Txt_MastroAlText = Val(Vettore(0))
        Txt_ContoAlText = Val(Vettore(1))
        Txt_SottocontoAlText = Val(Vettore(2))

        Dim k As New Cls_TabellaSocieta

        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        Dim DatiGenerale As New Cls_DatiGenerali

        DatiGenerale.LeggiDati(Session("DC_TABELLE"))


        WSocieta = k.DecodificaSocieta(Session("DC_TABELLE"))


        Condizione = ""
        'If Txt_MastroDal.Text <> 0 Then
        '  Condizione = " AND Mastro >= " & Txt_MastroDal.Text
        '  If Txt_ContoDal.Text <> 0 Then
        '    Condizione = Condizione & " AND Conto >= " & Txt_ContoDal.Text
        '    If Txt_SottocontoDal.Text <> 0 Then
        '      Condizione = Condizione & " AND SottoConto >= " & Txt_SottocontoDal.Text
        '    End If
        '  End If
        'End If
        'If Txt_MastroAl.Text <> 0 Then
        '  Condizione = Condizione & " AND Mastro <= " & Txt_MastroAl.Text
        '  If Txt_ContoAl.Text <> 0 Then
        '    Condizione = Condizione & " AND Conto <= " & Txt_ContoAl.Text
        '    If Txt_SottocontoAl.Text <> 0 Then
        '      Condizione = Condizione & " AND SottoConto <= " & Txt_SottocontoAl.Text
        '    End If
        '  End If
        'End If


        If Chk_Ospiti.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And ("
            Else
                Condizione = Condizione & " ("
            End If
            Condizione = Condizione & " TipoAnagrafica = 'O'"
        End If

        If Chk_parenti.Checked = True Then
            If Condizione <> "" Then
                If Chk_Ospiti.Checked = False Then
                    Condizione = Condizione & " AND ( "
                Else
                    Condizione = Condizione & " OR "
                End If
            Else
                Condizione = Condizione & " ("
            End If
            Condizione = Condizione & " TipoAnagrafica = 'P'"
        End If

        If Chk_Comuni.Checked = True Then
            If Condizione <> "" Then
                If Chk_Ospiti.Checked = False And Chk_parenti.Checked = False Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " OR "
                End If
            Else
                Condizione = Condizione & " ("
            End If
            Condizione = Condizione & " TipoAnagrafica = 'C'"
        End If

        If Chk_Regioni.Checked = True Then
            If Condizione <> "" Then
                If Chk_Ospiti.Checked = False And Chk_parenti.Checked = False And Chk_Comuni.Checked = False Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " OR "
                End If
            Else
                Condizione = Condizione & " ("
            End If
            Condizione = Condizione & " TipoAnagrafica = 'R'"
        End If

        If Chk_Ospiti.Checked = True Or Chk_parenti.Checked = True Or Chk_Comuni.Checked = True Or Chk_Regioni.Checked = True Then
            Condizione = Condizione & ") "
        End If



        Dim CreaContoDA As String
        Dim CreaContoA As String
        Dim MySql As String
        Dim SaldoIniziale As Double
        Dim SaldoIniziale2 As Double
        Dim SaldoIniziale3 As Double
        Dim OkPassa As Boolean
        Dim StrVista As String
        Dim RtCod As Integer
        Dim Query As String

        CreaContoDA = Format(Txt_MastroDalText, "000000") & Format(Txt_ContoDalText, "000000") & Format(Txt_SottocontoDalText, "000000")
        CreaContoA = Format(Txt_MastroAlText, "000000") & Format(Txt_ContoAlText, "000000") & Format(Txt_SottocontoAlText, "000000")

        MySql = "SELECT * FROM PianoConti WHERE Sottoconto <> 0 "
        If Condizione <> "" Then MySql = MySql & " AND " & Condizione
        MySql = MySql & " ORDER BY Mastro, Conto, SottoConto"

        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF

            Dim CreaConto As String

            CreaConto = Format(MoveFromDbWC(MyRs, "Mastro"), "000000") & Format(MoveFromDbWC(MyRs, "Conto"), "000000") & Format(MoveFromDbWC(MyRs, "SottoConto"), "000000")

            If MoveFromDbWC(MyRs, "SottoConto") = 112 Then
                CreaConto = CreaConto
            End If

            If CreaContoDA <= CreaConto And CreaConto <= CreaContoA Then
                SaldoIniziale = totaleSottoConto(MoveFromDbWC(MyRs, "Mastro"), MoveFromDbWC(MyRs, "Conto"), MoveFromDbWC(MyRs, "SottoConto"), DatiGenerale.CausaleChiusura)
             
                If (Math.Round(SaldoIniziale, 2) <> 0) Or (Chk_ImportiDiversiDaZero.Checked = True And Math.Round(SaldoIniziale, 2) = 0) Then

                    Dim WrRs As System.Data.DataRow = Stampa.Tables("Mastrino").NewRow

                    WrRs.Item("SaldoIniziale") = SaldoIniziale
                    WrRs.Item("SaldoIniziale2") = SaldoIniziale2
                    WrRs.Item("SaldoIniziale3") = SaldoIniziale3

                    WrRs.Item("MastroPartita") = MoveFromDbWC(MyRs, "Mastro")
                    WrRs.Item("ContoPartita") = MoveFromDbWC(MyRs, "Conto")
                    WrRs.Item("SottocontoPartita") = MoveFromDbWC(MyRs, "SottoConto")

                    If Chk_parenti.Checked = True Then
                        Dim DecConto As New Cls_Pianodeiconti

                        DecConto.Mastro = MoveFromDbWC(MyRs, "Mastro")
                        DecConto.Conto = MoveFromDbWC(MyRs, "Conto")
                        DecConto.Sottoconto = MoveFromDbWC(MyRs, "SottoConto")

                        Dim Osp As New ClsOspite

                        Osp.CodiceOspite = Int(DecConto.Sottoconto / 100)
                        Osp.Leggi(Session("DC_GENERALE"), Osp.CodiceOspite)

                        WrRs.Item("DescrizioneSottocontoPartita") = DecConto.Descrizione & "        Osp: " & Osp.Nome
                    Else
                        Dim DecConto As New Cls_Pianodeiconti

                        DecConto.Mastro = MoveFromDbWC(MyRs, "Mastro")
                        DecConto.Conto = MoveFromDbWC(MyRs, "Conto")
                        DecConto.Sottoconto = MoveFromDbWC(MyRs, "SottoConto")
                        DecConto.Decodfica(Session("DC_GENERALE"))

                        WrRs.Item("DescrizioneSottocontoPartita") = DecConto.Descrizione
                    End If

                    WrRs.Item("IntestaSocieta") = WSocieta
                    WrRs.Item("DataElaborazione") = Now
                    WrRs.Item("DataRegistrazione") = Txt_DataDal.Text
                    WrRs.Item("DataDocumento") = Txt_DataAl.Text
             
                    WrRs.Item("VistaLogica") = ""

                    Stampa.Tables("Mastrino").Rows.Add(WrRs)
                End If

            End If            
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        Session("stampa") = Stampa
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=SALDI','Stampe','width=800,height=600');", True)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Vettore(100) As String

        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        Dim DataDal As Date
        Dim DataAl As Date
        DataDal = Txt_DataDal.Text
        DataAl = Txt_DataAl.Text

        If Format(DataDal, "yyyyMMdd") >= Format(DataAl, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dal Maggiore data Al');", True)

            Exit Sub
        End If


        If Trim(Txt_SottocontoDal.Text) = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto dal ');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If

        If Trim(Txt_SottocontoAl.Text) = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto al');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If

        Vettore = SplitWords(Txt_SottocontoDal.Text)

        If Vettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto dal');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If


        Vettore = SplitWords(Txt_SottocontoAl.Text)
        If Vettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto al');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto al "
            Exit Sub
        End If


        DoWork(Session)

        REM 
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Dim MyJs As String

        'MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"

        'MyJs = "$(" & Chr(34) & ".Sottoconto" & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS1", MyJs, True)


        'MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoAl.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS2", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS3", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS4", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").blur(function() { $('#" & Txt_SottocontoAl.ClientID & "').val($(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").val());}); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS5", MyJs, True)



        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id;"
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRitIm", MyJs, True)




        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)




    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("Menu_Stampe.aspx")

    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub



    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long, ByVal W_CausaleChiusura As String) As Double
        Dim Condizione As String
        Dim MySql As String
        Dim Dare, Avere As Double

        Dim cn As OleDbConnection




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim DataDal As New Date
        Dim DataAl As New Date

        DataDal = Txt_DataDal.Text
        DataAl = Txt_DataAl.Text


        Condizione = ""


        If Chk_SenzaMovimentiChiusura.Checked = True Then
            Condizione = Condizione & " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        End If


        MySql = "SELECT  SUM(case when DAREAVERE ='D' THEN IMPORTO else 0 end) as TD,SUM(case when DAREAVERE ='A' THEN IMPORTO else 0 end) as TA FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & Sottoconto & _
                    " AND DataRegistrazione >= {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " AND DataRegistrazione <=  {ts'" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} " & Condizione


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dare = campodbN(myPOSTreader.Item("TD"))
            Avere = campodbN(myPOSTreader.Item("TA"))
        End If
        myPOSTreader.Close()


        cn.Close()

        totaleSottoConto = Dare - Avere

        'Dim MyRs As New ADODB.Recordset
        'Dim Condizione As String
        'Dim MySql As String
        'Dim GeneraleDb As New ADODB.Connection
        'Dim TabelleDb As New ADODB.Connection
        'Dim Dare As Double
        'Dim Avere As Double

        'Dim W_CausaleChiusura As String


        'Condizione = ""



        'Dim DataDal As New Date
        'Dim DataAl As New Date

        'DataDal = Txt_DataDal.Text
        'DataAl = Txt_DataAl.Text

        'W_CausaleChiusura = ""
        'TabelleDb.Open(Session("DC_TABELLE"))
        'GeneraleDb.Open(Session("DC_GENERALE"))

        'MySql = "SELECT * From DatiGenerali"

        'MyRs.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MyRs.EOF Then            
        '    W_CausaleChiusura = MoveFromDbWC(MyRs, "CausaleChiusura")
        'End If
        'MyRs.Close()
        'TabelleDb.Close()

        'If Chk_SenzaMovimentiChiusura.Checked = True Then
        '    Condizione = " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        'End If


        'MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
        '            " INNER JOIN MovimentiContabiliRiga " & _
        '            " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '            " WHERE MastroPartita = " & Mastro & _
        '            " AND ContoPartita = " & Conto & _
        '            " AND SottocontoPartita = " & Sottoconto & _
        '            " AND DareAvere = 'D' " & _
        '            " AND DataRegistrazione >= {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}" & _
        '            " AND DataRegistrazione <= {ts'" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}" & _
        '            Condizione

        'MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MyRs.EOF Then
        '    Dare = MoveFromDbWC(MyRs, "Totale")
        'End If
        'MyRs.Close()


        'MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
        '        " INNER JOIN MovimentiContabiliRiga " & _
        '        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
        '        " WHERE MastroPartita = " & Mastro & _
        '        " AND ContoPartita = " & Conto & _
        '        " AND SottocontoPartita = " & Sottoconto & _
        '        " AND DareAvere = 'A' " & _
        '        " AND DataRegistrazione >= {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}" & _
        '        " AND DataRegistrazione <= {ts'" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}" & _
        '           Condizione



        'MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MyRs.EOF Then
        '    Avere = MoveFromDbWC(MyRs, "Totale")
        'End If
        'MyRs.Close()

        'totaleSottoConto = Dare - Avere

    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function

End Class
