﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_Bollo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        Call EseguiJS()


        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim IVA As New Cls_IVA

        IVA.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)


        Dim m As New Cls_bolli


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select * from Bolli  " & _
                               "  Where ID = " & Val(Request.Item("ID")))
        cmd.Connection = cn




        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Txt_ID.Text = Val(Request.Item("ID"))

            Txt_DataValidita.Text = Format(campodbd(myPOSTreader.Item("DataValidita")), "dd/MM/yyyy")
            Txt_ImportoApplicazione.Text = Format(campodbn(myPOSTreader.Item("ImportoApplicazione")), "#,##0.00")
            Txt_ImportoBollo.Text = Format(campodbn(myPOSTreader.Item("ImportoBollo")), "#,##0.00")

            DD_IVA.SelectedValue = campodb(myPOSTreader.Item("CodiceIVA"))
        Loop
        myPOSTreader.Close()
        cn.Close()


        

    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Bollo.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Not IsDate(Txt_DataValidita.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Indicare data validità');", True)
            Exit Sub
        End If




        If Val(Txt_ImportoBollo.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi indicare un importo bollo');", True)
            Exit Sub
        End If


        If DD_IVA.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Devi indicare codice iva');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "BOLLO", "")


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand


        If Val(Request.Item("ID")) = 0 Then
            cmd.CommandText = ("INSERT INTO  Bolli (DataValidita,ImportoApplicazione,ImportoBollo,CodiceIVA) values (?,?,?,?) ")
        Else

            cmd.CommandText = ("UPDATE Bolli  SET DataValidita = ?,ImportoApplicazione  = ?,ImportoBollo  = ?,CodiceIVA   = ? " & _
                                   "  Where ID = " & Val(Request.Item("ID")))
        End If
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DataValidita", Txt_DataValidita.Text)
        cmd.Parameters.AddWithValue("@ImportoApplicazione", CDbl(Txt_ImportoApplicazione.Text))
        cmd.Parameters.AddWithValue("@ImportoBollo", CDbl(Txt_ImportoBollo.Text))
        cmd.Parameters.AddWithValue("@CodiceIVA", DD_IVA.SelectedValue)
        cmd.ExecuteNonQuery()



        cn.Close()

        Response.Redirect("Elenco_Bollo.aspx")

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim Log As New Cls_LogPrivacy

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "BOLLO", "")

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand



        cmd.CommandText = ("DELETE FROM Bolli  Where ID = " & Val(Request.Item("ID")))
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cn.Close()

        Response.Redirect("Elenco_Bollo.aspx")
    End Sub
End Class


