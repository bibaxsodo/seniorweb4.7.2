﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Tabelle" CodeFile="Menu_Tabelle.aspx.vb" %>

<head id="Head1" runat="server">
    <title>Menu Generale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Generale</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tabelle<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        &nbsp;</td>
                    <td colspan="2" valign="top">
                        <table style="width: 60%;">
                            <tr>

                                <td style="text-align: center;">
                                    <a href="ElencoCausaliContabili.aspx" id="Href_CausaliContabili">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Causali Contabili" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoModalitapagamento.aspx" id="Href_ModalitaPagamento">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Modalità Pagamento" /></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoTipoRegistro.aspx" id="Href_TipoRegistro">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Tipo Registro" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center;">
                                    <a href="ElencoRegistriIVA.aspx" id="Href_RegistroIVA">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Registro IVA" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoDetraibilita.aspx" id="Href_Detraibilita">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" alt="Detraibilità" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CAUSALI CONT.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MOD. PAGAMENTO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO REGISTRO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">REGISTRI IVA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DETRAIBILITA'</span></td>
                            </tr>

                            <tr>

                                <td style="text-align: center;">
                                    <a href="ElencoAliquotaIVA.aspx" id="Href_PeriodoaConfronto">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Elenco Aliquote IVA" /></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoRitenute.aspx" id="Href_Ritenute">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Elenco Ritenute" /></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_NoteFatture.aspx" id="A1">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Note Fatture" /></a>
                                <td style="text-align: center;">
                                    <a href="Tabella_DatiGenerali.aspx" id="A2">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Dati Generali" /></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_TabellaTrascodifica.aspx" id="A3">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Tabella Trascodifica" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ALIQUOTE IVA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RITENUTE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">NOTE FATTURE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DATI GENERALI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TAB. TRASCODIFICA</span></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="Elenco_Tributi.aspx" id="A4">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Elenco Tributi" /></a></td>
                                <td style="text-align: center;">
                                    <a href="Elenco_FEALIQUOTA.aspx" id="A5">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="FE Alqiuote IVA" /></a></td>
                                <td style="text-align: center;">
                                    <a href="Elenco_Categorie.aspx" id="A6">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Categorie Cespite" /></a></td>

                                <td style="text-align: center;">
                                    <a href="ElencoGruppiLiquidazione.aspx" id="A7">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Gruppi Liquidazione" /></a></td>
                                <td style="text-align: center;">
                                    <a href="ElencoProrata.aspx" id="A8">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Prorata" /></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TRIBUTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">FE ALIQUOTE IVA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CATEGORIE CESPITI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">GRUPPI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRORATA</span></td>
                            </tr>
                            <tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="Elenco_Bollo.aspx" id="A9">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Bollo" /></a></td>
                                <td style="text-align: center;">
                                    <a href="Elenco_ProgressivaIVA.aspx" id="A10">
                                        <img style="border-width: 0;" src="../images/Menu_Tabelle.png" class="Effetto" alt="Progressivo IVA" /></a></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">BOLLO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PROGRESSIVI IVA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
