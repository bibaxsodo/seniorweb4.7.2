﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_XMLLiquidazioneIVATrimestrale" CodeFile="XMLLiquidazioneIVATrimestrale.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Liquidazione IVA Trimestrale XML</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">
        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

        <div align="left">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Strumenti - Parametri</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_CaricaValori" runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi" ToolTip="Carica da Liquidazione" />
                            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/download.png" class="EffettoBottoniTondi" ToolTip="Download XML" />
                        </div>
                    </td>
                </tr>


                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Frontespizio
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <br />
                                    <label class="LabelCampo">Codice Fornitura :</label>
                                    <asp:TextBox ID="Txt_CodiceFornitura" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale Dichiarante :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscaleDichiarante" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Carica :</label>
                                    <asp:TextBox ID="Txt_CodiceCarica" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Anno Imposta :</label>
                                    <asp:TextBox ID="Txt_AnnoImposta" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Partita IVA :</label>
                                    <asp:TextBox ID="Txt_PartitaIVA" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale Dichiarante :</label>
                                    <asp:TextBox ID="Txt_CFDichiarante" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Carica Dichiarante :</label>
                                    <asp:TextBox ID="Txt_CodiceCaricaDichiarante" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Firma Dichiarazione :</label>
                                    <asp:TextBox ID="Txt_FirmaDichiarazione" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CF Intermediario:</label>
                                    <asp:TextBox ID="Txt_CFIntermediario" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Impegno Presentazione:</label>
                                    <asp:TextBox ID="Txt_ImpegnoPresentazione" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Data Impegno:</label>
                                    <asp:TextBox ID="Txt_DataImpegno" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Firma Intermediario :</label>
                                    <asp:TextBox ID="Txt_FirmaIntermediario" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <hr />
                                    <label class="LabelCampo">Gruppo : </label>
                                    <asp:DropDownList ID="DD_Gruppo" Width="200px" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />



                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Mese 1
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Mese 1 :</label>
                                    <asp:TextBox ID="Txt_Mese1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Attive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniAttive1" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Passive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniPassive1" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Iva Esigibile :</label>
                                    <asp:TextBox ID="Txt_IvaEsigibile1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Detratta :</label>
                                    <asp:TextBox ID="Txt_IvaDetratta1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Credito :</label>
                                    <asp:TextBox ID="Txt_IvaCredito1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Dovuta :</label>
                                    <asp:TextBox ID="Txt_IvaDovuta1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Credito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_CreditoPeriodoPrecedente1" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Debito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_DebitoPeriodoPrecedente1" Text="" runat="server"></asp:TextBox><br />


                                    <br />
                                    <label class="LabelCampo">Acconto dovuto  :</label>
                                    <asp:TextBox ID="Txt_Accontodovuto1" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Importo A Credito :</label>
                                    <asp:TextBox ID="Txt_ImportoACredito1" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Importo Da Versare :</label>
                                    <asp:TextBox ID="Txt_ImportoDaVersare1" Text="" runat="server"></asp:TextBox><br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Mese 2
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Mese 2 :</label>
                                    <asp:TextBox ID="Txt_Mese2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Attive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniAttive2" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Passive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniPassive2" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Iva Esigibile :</label>
                                    <asp:TextBox ID="Txt_IvaEsigibile2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Detratta :</label>
                                    <asp:TextBox ID="Txt_IvaDetratta2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Credito :</label>
                                    <asp:TextBox ID="Txt_IvaCredito2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Dovuta :</label>
                                    <asp:TextBox ID="Txt_IvaDovuta2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Credito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_CreditoPeriodoPrecedente2" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Debito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_DebitoPeriodoPrecedente2" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Acconto dovuto  :</label>
                                    <asp:TextBox ID="Txt_Accontodovuto2" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Importo A Credito :</label>
                                    <asp:TextBox ID="Txt_ImportoACredito2" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Importo Da Versare :</label>
                                    <asp:TextBox ID="Txt_ImportoDaVersare2" Text="" runat="server"></asp:TextBox><br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel3">
                                <HeaderTemplate>
                                    Mese 3
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Mese 3 :</label>
                                    <asp:TextBox ID="Txt_Mese3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Attive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniAttive3" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Totale Operazioni Passive :</label>
                                    <asp:TextBox ID="Txt_TotaleOperazioniPassive3" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Iva Esigibile :</label>
                                    <asp:TextBox ID="Txt_IvaEsigibile3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Detratta :</label>
                                    <asp:TextBox ID="Txt_IvaDetratta3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Credito :</label>
                                    <asp:TextBox ID="Txt_IvaCredito3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iva Dovuta :</label>
                                    <asp:TextBox ID="TXt_IvaDovuta3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Credito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_CreditoPeriodoPrecedente3" Text="" runat="server"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Debito Periodo Precedente :</label>
                                    <asp:TextBox ID="Txt_DebitoPeriodoPrecedente3" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Acconto dovuto  :</label>
                                    <asp:TextBox ID="Txt_Accontodovuto3" Text="" runat="server"></asp:TextBox><br />


                                    <br />
                                    <label class="LabelCampo">Importo A Credito :</label>
                                    <asp:TextBox ID="Txt_ImportoACredito3" Text="" runat="server"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Importo Da Versare :</label>
                                    <asp:TextBox ID="Txt_ImportoDaVersare3" Text="" runat="server"></asp:TextBox><br />


                                </ContentTemplate>
                            </xasp:TabPanel>


                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
