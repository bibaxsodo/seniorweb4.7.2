﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System
Imports System.Net
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel


Partial Class GeneraleWeb_Maillinglist
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LblMailing.Text = "<table class=""tabella"">"

        LblMailing.Text = LblMailing.Text & "<tr class=""miotr""><th class=""miaintestazione"">Data Ora</th><th class=""miaintestazione"">Oggetto</th><th class=""miaintestazione"">Esito</th><th class=""miaintestazione"">E-Mail</th></tr>"
        Try

            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()

            Dim cmdLeggi As New OleDbCommand()


            cmdLeggi.CommandText = ("Select * From Log_MailInviate Where NumeroRegistrazione = ? ")
            cmdLeggi.Parameters.AddWithValue("@NumeroRegistrazione", Val(Request.Item("Numero")))
            cmdLeggi.Connection = cn
            Dim K As OleDbDataReader = cmdLeggi.ExecuteReader
            Do While K.Read
                LblMailing.Text = LblMailing.Text & "<tr class=""miotr""><td class=""miacella"">" & campodb(K.Item("DataOra")) & "</td><td class=""miacella"">" & campodb(K.Item("Oggetto")) & "</td><td class=""miacella"">" & campodb(K.Item("Esito")) & "</td><td class=""miacella"">" & campodb(K.Item("Mail")) & "</td></tr>"
            Loop

            cn.Close()
        Catch ex As Exception

        End Try

        LblMailing.Text = LblMailing.Text & "</table>"
    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
