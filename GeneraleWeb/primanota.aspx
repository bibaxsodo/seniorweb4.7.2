﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" Inherits="GeneraleWeb_primanota" Culture="IT-it" CodeFile="primanota.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Prima Nota</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

                if (event.keyCode == 119) {
                    __doPostBack("Btn_Pulisci", "0");
                }

                if (event.keyCode == 120) {

                    $("#BTN_InserisciRiga").trigger("click");
                }


                //  __doPostBack("BTN_InserisciRiga", "0");                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function DialogBoxW(Path) {
            var tot = 0;

            tot = document.body.offsetWidth - 100;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }

        function DialogBox(Path) {

            var tot = 0;

            tot = document.body.offsetWidth - 300;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }
        function DialogBoxSlim(Path) {

            REDIPS.dialog.show(700, 400, '<iframe id="output" src="' + Path + '" height="390px" width="690"></iframe>');
            return false;

        }
        function DialogBoxBig(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
        function ChiudiConfermaModifica() {
            $("#ConfermaModifica").remove();
            $("#LblBox").empty();


        }

        function DialogBoxAnalitica(Path) {

            var tot = 0;

            tot = document.body.offsetWidth - 50;

            REDIPS.dialog.show(tot, 520, '<iframe id="outputAnaliticaScadenziario" src="' + Path + '" height="600px" width="' + tot + 'px"></iframe>');

            return false;
        }
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .confermamodifca {
            -webkit-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            -moz-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            background-color: #82807d;
            text-align: center;
            color: White;
            z-index: 100;
        }

        .SeniorButton:hover {
            background-color: Silver;
            color: gray;
        }
    </style>
</head>
<body>    
    <form id="form1" runat="server" >                   
    <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label> 
    
    <div  style="text-align:left;">
       <div id="dialog-modal"   title="Legami">   	
       <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>           
    </div>
    
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional"  runat="server">
     <ContentTemplate>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
    
    <asp:Label ID="LblBox" runat="server" Text=""></asp:Label> 
    <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
    <table  style="width:100%;" cellpadding="0" cellspacing="0">
    <tr>
    <td  style="background-color:#F0F0F0; width:185px;" class="destraclasse"></td>
    <td>
    <div class="Titolo">Contabilità - Principale - Prima Nota</div>
    <div class="SottoTitolo">
        <br />
        <br />
    </div>
    </td>
    
    <td style="text-align:right; vertical-align:top;" >    
      <div class="DivTasti">              
      <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png"  class="EffettoBottoniTondi"  ToolTip="Duplica" />&nbsp;      
      <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg"  class="EffettoBottoniTondi"  ToolTip="Modifica / Inserisci (F2)" />&nbsp;
      <asp:ImageButton ID="Btn_Elimina" Height="38px" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg"  class="EffettoBottoniTondi"  ToolTip="Elimina" />&nbsp;
      <asp:ImageButton ID="Btn_Pulisci" Height="38px" runat="server" BackColor="Transparent" ImageUrl="~/images/PULISCI.JPG" ToolTip="Pulisci (F8)" Visible="false" />&nbsp;           
      </div>
     </td>
    </tr>
    </table>
    <table cellpadding="0" cellspacing="0">
    <tr > 
    <td style="width:160px; background-color:#F0F0F0; vertical-align:top; text-align:center;" id="BarraLaterale" class="destraclasse">  
     <a href="Menu_Generale.aspx" style="border-width:0px;"><img src="images/Home.jpg"  class="Effetto"   alt="Menù" id="BOTTONEHOME" /></a><br />
     <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto"  ToolTip="Chiudi"  />     
     <asp:ImageButton ID="ImgRicerca" src="images/ricerca.jpg"  class="Effetto"   width="112px" height="100px" alt="Ricerca Registrazioni/Saldo" ToolTip="Ricerca Registrazioni/Saldo"  runat="server" /><br />  
     <br />          
     <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');">Vis. Piano Conti</a></label>
     <br />
     <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('Pianoconti.aspx');">Gest. Piano Conti</a></label>    
     <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('AnagraficaClientiFornitori.aspx');">Clienti Fornitori</a></label><br />
     
     <asp:Label ID="Lbl_BtnLegami" runat="server" Text=""></asp:Label>&nbsp; 
     <asp:Label ID="Lbl_BtnScadenzario" runat="server" Text=""></asp:Label>&nbsp;       
    </td>         
    <td colspan="2" style="background-color: #FFFFFF; vertical-align:top; width:100%;"  > 
     <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"  
             Width="100%" BorderStyle="None" style="margin-right: 39px">               
     <xasp:TabPanel runat="server" HeaderText="Prima Nota"   Width="100%" ID="Tab_Anagrafica">
         <HeaderTemplate>
             Prima Nota
         </HeaderTemplate>
     <ContentTemplate>             
     <center><asp:Label ID="Lbl_TipoRegistrazione" runat="server"></asp:Label></center>  

     <label class="LabelCampo">Numero :</label>    
     <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" ToolTip="prova" runat="server" AutoPostBack="True" Width="104px" ></asp:TextBox>     
     <br />   
     <br />
     <label class="LabelCampo">Data :</label><asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
    <br />
     <br />
     <table>
     <tr>
     <td>
    <label class="LabelCampo">Causale Contabile :</label>
    <asp:DropDownList ID="Dd_CausaleContabile" AutoPostBack="True" class="chosen-select" runat="server" Width="450px"></asp:DropDownList>    
    </td>
     <td>
    <asp:ImageButton runat="server" id="BtnOpen"  Width="40px" style="margin-top:0px;"/>
    <asp:ImageButton runat="server" id="BtnOpen2"  Width="40px" style="margin-top:0px;"/>
    </td>
    </tr>
    
    </table>
    <br />
    <br />
    <label class="LabelCampo">Descrizione :</label>
    <asp:TextBox ID="Txt_Descrizione" autocomplete="off"  MaxLength="250" runat="server" Height="72px" TextMode="MultiLine" Width="562px"></asp:TextBox>    
    <br />
    <br />
    <label class="LabelCampo"><asp:Label ID="Lbl_CentroSerivizio" runat="server" Visible="false" Text="Centro Servizio :"></asp:Label></label>
    <asp:DropDownList ID="DD_CSERV" class="chosen-selectcs" runat="server" Visible="false" Width="400px"></asp:DropDownList>
     <br />
    <br />
    <asp:Label ID="Lbl_Errore" runat="server" ForeColor="Red" Width="888px"></asp:Label>
              <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"  
                  ShowFooter="True" BackColor="White"  BorderColor="#6FA7D1"
               BorderStyle="Dotted" BorderWidth="1px"  >
                    <RowStyle ForeColor="#333333" BackColor="White" />
                    <Columns>
                                             
                                             
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="IB_Delete" CommandName="Delete" Runat="Server"
                      ImageUrl="~/images/cancella.png" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <div style="text-align:right"   >
                                    <asp:ImageButton ID="Ib_InserisciRiga" ImageUrl="~/images/inserisci.png"  ToolTip="Inserisci (F9)"
                                CommandName="Inserisci"  runat="server" />
                                </div>
                            </FooterTemplate>                                                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SOTTOCONTO">                            
                            <ItemTemplate>
                                <asp:TextBox ID="TxtRiga" onkeypress="return handleEnter(this, event)" Visible="false" runat="server"></asp:TextBox>
                                <asp:TextBox ID="TxtSottoconto" onkeypress="return handleEnter(this, event)"  CssClass="MyAutoComplete" Width="300px" runat="server"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" Font-Italic="False" />
                            <ItemStyle Width="30px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DARE">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtDare"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  AutoPostBack="true" OnTextChanged="Txt_Dare_TextChanged" autocomplete="off" Width="100px" runat="server"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" Font-Italic="False" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" />                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AVERE">                            
                            <ItemTemplate>
                                <asp:TextBox ID="TxtAvere"  style="text-align:right;"  onkeypress="return handleEnter(this, event)"  AutoPostBack="true" OnTextChanged="Txt_Avere_TextChanged" autocomplete="off" Width="100px" runat="server"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" Font-Italic="False" />
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DESCRIZIONE">                            
                            <ItemTemplate>
                                <asp:TextBox ID="TxtDescrizione" onkeypress="return handleEnter(this, event)" MaxLength="50"  autocomplete="off" Width="250px" runat="server"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" Font-Italic="False" />
                            <ItemStyle Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SOTTOCONTO CONTRO PARTITA">                            
                            <ItemTemplate>
                                <asp:TextBox ID="TxtSottocontoControPartita" onkeypress="return handleEnter(this, event)"  CssClass="MyAutoComplete" Width="300px" runat="server"></asp:TextBox>
                            </ItemTemplate>     
                            <HeaderStyle Font-Bold="False" Font-Italic="False" />
                            <ItemStyle Width="300px" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="White" ForeColor="#023102" />
                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#A6C9E2" Font-Bold="False"  ForeColor="White" 
                        BorderColor="#6FA7D1"  BorderWidth="1px"  />
                </asp:GridView>
                                 
    <br />
        <br />
         </ContentTemplate>
    </xasp:TabPanel>       
   <xasp:TabPanel runat="server" HeaderText="Altri Campi" ID="Tab_AltriCampi" Width="100%">
    <ContentTemplate>            
      <br />
       
     <label class="LabelCampo">Cig:</label>                              
     <asp:DropDownList ID="DD_Cig" runat="server" class="chosen-select"  ></asp:DropDownList>   
     <br />     
     <br />
        
     <label class="LabelCampo">Export :</label>             
     <asp:TextBox ID="Txt_ExportData" autocomplete="off" runat="server"  Width="150px" ></asp:TextBox>
     Registrazione di Riferimento
     <asp:TextBox ID="Txt_RegistrazioneRiferimento" autocomplete="off" runat="server"  Width="150px" ></asp:TextBox>
        
     <br />
     <br />
     
     <label class="LabelCampo">Periodo Competenza</label><br />
     <br />
     <label class="LabelCampo">Anno:</label>             
     &nbsp;<asp:TextBox ID="Txt_AnnoRif" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="150px" MaxLength="4" ></asp:TextBox><br />
     <br />
     <label class="LabelCampo">Mese:</label>                              
     &nbsp;<asp:DropDownList ID="DD_MeseRif" runat="server" Width="150px">
             <asp:ListItem Value="0">Seleziona Mese</asp:ListItem>
             <asp:ListItem Value="1">Gennaio</asp:ListItem>
             <asp:ListItem Value="2">Febbraio</asp:ListItem>
             <asp:ListItem Value="3">Marzo</asp:ListItem>
             <asp:ListItem Value="4">Aprile</asp:ListItem>
             <asp:ListItem Value="5">Maggio</asp:ListItem>
             <asp:ListItem Value="6">Giugno</asp:ListItem>
             <asp:ListItem Value="7">Luglio</asp:ListItem>
             <asp:ListItem Value="8">Agosto</asp:ListItem>
             <asp:ListItem Value="9">Settembre</asp:ListItem>
             <asp:ListItem Value="10">Ottobre</asp:ListItem>
             <asp:ListItem Value="11">Novembre</asp:ListItem>
             <asp:ListItem Value="12">Dicembre</asp:ListItem>
     </asp:DropDownList><br />
     <br />   
     <br />
     <label class="LabelCampo">Data Mandato:</label>
     <asp:TextBox ID="Txt_DataMandato" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     Numero Mandato :
     <asp:TextBox ID="Txt_NumeroMandato" onkeypress="return soloNumeri(event);" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     <br />
     <br />    
     <label class="LabelCampo">Data Reversale:</label>
     <asp:TextBox ID="Txt_DataReversale" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     Numero Reversale :
     <asp:TextBox ID="Txt_NumeroReversale" onkeypress="return soloNumeri(event);" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     <br />      
     <br />
     <label class="LabelCampo">Data Distinta:</label>
     <asp:TextBox ID="Txt_DataDistinta" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     Numero Reversale :
     <asp:TextBox ID="Txt_NumeroDistinta" onkeypress="return soloNumeri(event);" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
     <br />          
     </div>
</ContentTemplate>    
</xasp:TabPanel>    

    </xasp:TabContainer>        
    </td>            
    </tr>
    </table>        
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <asp:Timer ID="Timer1" runat="server">
    </asp:Timer> 
    </form>
</body>
</html>
