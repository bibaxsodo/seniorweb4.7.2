﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" Inherits="GeneraleWeb_DocumentiEasy" CodeFile="DocumentiEasy.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Documenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <style type="text/css">
        body {
            background: url(background.gif);
            margin: 0;
            padding: 0;
            font: normal 12px/14px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

        #header {
            background: url(title.png) center center no-repeat;
            height: 150px;
            width: 100%;
            margin: 45px 0 0 0;
            padding: 0;
        }

        #mainContent {
            background: #FFF;
            padding: 50px;
            width: 1255px;
            margin: 0 auto;
            overflow: hidden;
            position: relative;
            -moz-box-shadow: 0px 0px 8px #CCC; /* FF3.5+ */
            -webkit-box-shadow: 0px 0px 8px #CCC; /* Saf3.0+, Chrome */
            box-shadow: 0px 0px 8px #CCC; /* Opera 10.5, IE 9.0 */
            filter: /* IE6,IE7 e IE8 */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=0,strength=5) /* top */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=90,strength=5) /* left */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=180,strength=5) /* bottom */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=270,strength=5); /* right */
        }

        h1 {
        }

        h2 {
            padding-top: 50px;
        }

        p {
            margin-bottom: 500px;
        }

        ul.appo {
            background: #21F8F0;
            padding: 10px 35px;
            list-style-type: none;
            margin: 0 0 0 -310px;
            position: fixed;
            left: 50%;
            top: 0;
            font-size: 14px;
            font-weight: bold;
        }

        li.appo {
            float: left;
            display: inline;
            margin: 0 75px 0 0;
        }

        li a {
            float: left;
            color: #1A063D;
            text-decoration: none;
            text-shadow: 1px 1px 0px #FFFFFF;
        }

            li a.current {
                color: #1EDFD8;
                text-shadow: none;
            }

        .articleLink {
            position: absolute;
            text-decoration: none;
            color: #0099CC;
            right: 15px;
            top: 15px;
            font-weight: bold;
        }

        div.comandi {
            position: fixed;
            margin-right: 15px;
            margin-top: 500px;
            margin-left: 20px
        }
    </style>

    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>



    <script type="text/javascript">     
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
                if (event.keyCode == 119) {
                    __doPostBack("Btn_Pulisci", "0");
                }
            });
        });

        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function DialogBoxW(Path) {
            var tot = 0;

            tot = document.body.offsetWidth - 100;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }

        function DialogBox(Path) {

            REDIPS.dialog.show(700, 500, '<iframe id="output" src="' + Path + '" height="490px" width="690"></iframe>');
            return false;

        }
        function DialogBoxSlim(Path) {
            REDIPS.dialog.show(600, 200, '<iframe id="output" src="' + Path + '" height="190px" width="590"></iframe>');
            return false;

        }
        function DialogBoxBig(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <div id="dialog-modal" title="Legami">
                <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>
            </div>
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="False" />
            <asp:Button ID="BtnInsertRiga3" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga4" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga5" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga6" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga7" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga8" runat="server" Text="" Visible="false" />
            <asp:Button ID="BtnInsertRiga9" runat="server" Text="" Visible="false" />

            <asp:Button ID="BntFlagRosso" runat="server" Text="" Visible="false" />

            <asp:Button ID="BtnRicalcolaIVA" runat="server" Text="" Visible="false" />


            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Documenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <img height="38px" src="../images/Btn_ClientiFornitori.jpg" title="Clienti Fornitori" onclick="DialogBoxBig('AnagraficaClientiFornitori.aspx');" />&nbsp;      
      <img height="38px" src="../images/Btn_PianoConti.jpg" title="Piano Conti" onclick="DialogBoxBig('Pianoconti.aspx');" />&nbsp;&nbsp;
      <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />&nbsp;                     
      <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci (F2)" />&nbsp;
      <asp:ImageButton ID="Btn_Elimina" Height="38px" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" />&nbsp;
      <asp:ImageButton ID="Btn_Pulisci" Height="38px" runat="server" BackColor="Transparent" ImageUrl="~/images/PULISCI.JPG" ToolTip="Pulisci (F8)" Visible="false" />&nbsp;     
      <asp:Label ID="Lbl_BtnLegami" runat="server" Text=""></asp:Label>&nbsp; 
      <asp:Label ID="Lbl_BtnScadenzario" runat="server" Text=""></asp:Label>&nbsp; 
      <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" Height="38px" ImageUrl="images/esci.jpg" ToolTip="Chiudi" /><br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <asp:Label ID="Lbl_Importo" runat="server"></asp:Label></td>
                    <td style="text-align: right; vertical-align: top;"></td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" id="BOTTONEHOME" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_AddDoc" src="../images/Menu_GestioneDocumenti.png" class="Effetto" Width="112px" Height="100px" alt="Nuovo Documento" runat="server" /><br />
                        <asp:ImageButton ID="ImgRicerca" src="images/ricerca.png" Width="112px" Height="100px" alt="Ricerca Registrazioni" runat="server" /><br />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');"><img src="images/arrow.gif" />Piano Conti</a></label>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Documenti         
                                </HeaderTemplate>


                                <ContentTemplate>

                                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <br />
                                            <label class="LabelCampo">Numero :</label>
                                            <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" ToolTip="prova" runat="server" AutoPostBack="True" Width="104px"></asp:TextBox>
                                            <asp:Label ID="Lbl_Progressivo" runat="server" Text=""></asp:Label>
                                            <br />

                                            <br />
                                            <label class="LabelCampo">Causale :</label>
                                            <asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="286px" AutoPostBack="true" OnSelectedIndexChanged="Dd_CausaleContabile_SelectedIndexChanged"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Pagamento :</label>
                                            <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="200px"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Data Registrazione :</label>
                                            <asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" AutoPostBack="true" runat="server" Width="80px"></asp:TextBox>
                                            <asp:ImageButton runat="server" ID="ImageButton3" ImageUrl="~/Images/calendario.png" Width="16px" Height="16px" AlternateText="Visualizza Calendario" />
                                            <xasp:CalendarExtender ID="CalendarioExt1" runat="server" TargetControlID="Txt_DataRegistrazione" PopupButtonID="ImageButton3" Format="dd/MM/yyyy" Enabled="true"></xasp:CalendarExtender>
                                            <br />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Data Documento :</label>
                                            <asp:TextBox ID="Txt_DataDocumento" autocomplete="off" runat="server" Width="80px"></asp:TextBox>
                                            <asp:ImageButton runat="server" ID="ImageButton1" ImageUrl="~/Images/calendario.png" Width="16px" Height="16px" AlternateText="Visualizza Calendario" />
                                            <xasp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="Txt_DataDocumento" PopupButtonID="ImageButton1" Format="dd/MM/yyyy" Enabled="true"></xasp:CalendarExtender>
                                            <br />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Numero Documento :</label>
                                            <asp:TextBox ID="Txt_NumeroDocumento" autocomplete="off" runat="server" Width="80px" MaxLength="20"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Cliente/Fornitore :</label>
                                            <asp:TextBox ID="Txt_ClienteFornitore" OnDisposed="Txt_ClienteFornitore_Disposed" runat="server" Width="476px"></asp:TextBox>
                                            <asp:Button ID="Button1" runat="server" Text="Aggiorna" Width="61px" /><br />
                                            <br />
                                            <asp:Label ID="Lbl_RegistroIVA" runat="server" Text=""></asp:Label>
                                            <br />
                                            <label class="LabelCampo">Anno :</label>
                                            <asp:TextBox ID="Txt_AnnoProtocollo" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="60px" MaxLength="4"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Numero Protocollo :</label>
                                            <asp:TextBox ID="Txt_NumeroProtocollo" BackColor="Gray" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="80px" MaxLength="10"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Descrizione :</label>
                                            <asp:TextBox ID="Txt_Descrizione" runat="server" Width="547px" Height="65px"
                                                TextMode="MultiLine"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Ritenuta :</label>
                                            <asp:TextBox ID="Txt_Ritenuta1" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>

                                            <br />
                                            <table width="1000px">
                                                <tr>
                                                    <td style="width: 300px;">Conto</td>
                                                    <td style="width: 150px;">Imponibile</td>
                                                    <td style="width: 150px;">IVA</td>
                                                    <td style="width: 150px;">Imposta</td>
                                                    <td style="width: 100px;">D/A</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="Txt_Conto1" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imponibile1" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:DropDownList ID="DD_IVA1" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imposta1" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <asp:RadioButton ID="Rb_Dare1" runat="server" Text="D" GroupName="RIGA1" />
                                                        <asp:RadioButton ID="Rb_Avere1" runat="server" Text="A" GroupName="RIGA1" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="Txt_Conto2" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imponibile2" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:DropDownList ID="DD_IVA2" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imposta2" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <asp:RadioButton ID="Rb_Dare2" runat="server" Text="D" GroupName="RIGA2" />
                                                        <asp:RadioButton ID="Rb_Avere2" runat="server" Text="A" GroupName="RIGA2" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="Txt_Conto3" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imponibile3" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:DropDownList ID="DD_IVA3" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imposta3" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <asp:RadioButton ID="Rb_Dare3" runat="server" Text="D" GroupName="RIGA3" />
                                                        <asp:RadioButton ID="Rb_Avere3" runat="server" Text="A" GroupName="RIGA3" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="Txt_Conto4" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imponibile4" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:DropDownList ID="DD_IVA4" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imposta4" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <asp:RadioButton ID="Rb_Dare4" runat="server" Text="D" GroupName="RIGA4" />
                                                        <asp:RadioButton ID="Rb_Avere4" runat="server" Text="A" GroupName="RIGA4" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="Txt_Conto5" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imponibile5" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:DropDownList ID="DD_IVA5" runat="server" AutoPostBack="true" Width="150px"></asp:DropDownList>
                                                    </td>
                                                    <td style="width: 150px;">
                                                        <asp:TextBox ID="Txt_Imposta5" runat="server" Style="text-align: right;" AutoPostBack="true" onkeypress="ForceNumericInput(this, true, true)" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <asp:RadioButton ID="Rb_Dare5" runat="server" Text="D" GroupName="RIGA5" />
                                                        <asp:RadioButton ID="Rb_Avere5" runat="server" Text="A" GroupName="RIGA5" />
                                                    </td>
                                                </tr>
                                            </table>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <br />
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Timer ID="Timer1" runat="server">
        </asp:Timer>
    </form>
</body>
</html>
