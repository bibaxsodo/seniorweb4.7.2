﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_ElencoCausaliContabili
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior



        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Call Ricerca()

            Session("RicercaCausaliSQLString") = Nothing
        Else

            If Not IsNothing(Session("RicercaCausaliSQLString")) Then
                Dim k As New Cls_SqlString


                k = Session("RicercaCausaliSQLString")

                Txt_Descrizione.Text = k.GetValue("Txt_Descrizione")


                Call Ricerca()


            Else
                Call Ricerca()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grid.PageCount Then
                    Grid.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tabella = ViewState("Appoggio")

            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()

            Dim i As Integer

            For i = 0 To Grid.Rows.Count - 1


                If Grid.PageIndex > 0 Then
                    Grid.Rows(i).Cells(3).Text = Tabella.Rows(i + (Grid.PageIndex * 20)).Item(2)
                Else
                    Grid.Rows(i).Cells(3).Text = Tabella.Rows(i).Item(2)
                End If
            Next

        End If
        'Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()


        Dim i As Integer

        For i = 0 To Grid.Rows.Count - 1


            If Grid.PageIndex > 0 Then
                Grid.Rows(i).Cells(3).Text = Tabella.Rows(i + (Grid.PageIndex * 20)).Item(2)
            Else
                Grid.Rows(i).Cells(3).Text = Tabella.Rows(i).Item(2)
            End If
        Next

    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("causalicontabili.aspx?Codice=" & Codice & "&PAGINA=" & Grid.PageIndex)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("causalicontabili.aspx")
    End Sub


    Private Sub Ricerca()
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand



        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Descrizione", Txt_Descrizione.Text)

        Session("RicercaCausaliSQLString") = k1


        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.CommandText = "Select  * from CausaliContabiliTesta where  Descrizione Like ?  Order By Descrizione"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        Else
            cmd.CommandText = "Select  * from CausaliContabiliTesta Order By Codice,Descrizione"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        End If



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Conti Causali", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")

            If Chk_Dettaglio.Checked = True Then
                Dim cmdRighe As New OleDbCommand
                Dim Appoggio As String = "<div class=""RIGACAUSALE"">"

                cmdRighe.CommandText = "Select  * from CausaliContabiliRiga where  Codice = ?  Order By Riga"
                cmdRighe.Connection = cn
                cmdRighe.Parameters.AddWithValue("@Codice", campodb(myPOSTreader.Item("Codice")))
                Dim RdRighe As OleDbDataReader = cmdRighe.ExecuteReader()
                Do While RdRighe.Read

                    Dim Conto As New Cls_Pianodeiconti

                    Conto.Descrizione = ""
                    Conto.Mastro = campodb(RdRighe.Item("Mastro"))
                    Conto.Conto = campodb(RdRighe.Item("Conto"))
                    Conto.Sottoconto = campodb(RdRighe.Item("Sottoconto"))
                    Conto.Decodfica(Session("DC_GENERALE"))

                    If Conto.Mastro <> 0 And Conto.Conto <> 0 And Conto.Descrizione <> "" Then
                        Appoggio = Appoggio & "(" & campodb(RdRighe.Item("Riga")) & ") " & "<b>" & Conto.Mastro & " " & Conto.Conto & " " & Conto.Sottoconto & "</b> " & Conto.Descrizione & "<br/>"
                    End If
                Loop
                RdRighe.Close()

                myriga(2) = Appoggio & "</div>"
            Else
                myriga(2) = ""
            End If


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()




        Dim i As Integer

        For i = 0 To Grid.Rows.Count - 1


            If Grid.PageIndex > 0 Then
                Grid.Rows(i).Cells(3).Text = Tabella.Rows(i + (Grid.PageIndex * 20)).Item(2)
            Else
                Grid.Rows(i).Cells(3).Text = Tabella.Rows(i).Item(2)
            End If
        Next

    End Sub
    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Call Ricerca()

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub ImgHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgHome.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection



        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand



        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.CommandText = "Select  * from CausaliContabiliTesta where  Descrizione Like ?  Order By Descrizione"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        Else
            cmd.CommandText = "Select  * from CausaliContabiliTesta Order By Codice,Descrizione"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        End If


        cmd.Connection = cn
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("RegistroIVA", GetType(String))
        Tabella.Columns.Add("TipoDocumento", GetType(String))
        Tabella.Columns.Add("DataObbligatoria", GetType(String))
        Tabella.Columns.Add("NumeroObbligatorio", GetType(String))
        Tabella.Columns.Add("CausaleIncasso", GetType(String))
        Tabella.Columns.Add("AllegatoFineAnno", GetType(String))
        Tabella.Columns.Add("VenditaAcquisti", GetType(String))
        Tabella.Columns.Add("CodiceIva", GetType(String))
        Tabella.Columns.Add("Detraibilita", GetType(String))
        Tabella.Columns.Add("CentroServizio", GetType(String))
        Tabella.Columns.Add("Prorata", GetType(String))
        Tabella.Columns.Add("Report", GetType(String))
        Tabella.Columns.Add("Ritenuta", GetType(String))
        Tabella.Columns.Add("DocumentoReverse", GetType(String))
        Tabella.Columns.Add("GirocontoReverse", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("Codice"))
            myriga(1) = campodb(myPOSTreader.Item("Tipo"))
            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))
            Dim RegistroIVA As New Cls_RegistroIVA

            RegistroIVA.Descrizione = ""
            RegistroIVA.Tipo = Val(campodb(myPOSTreader.Item("RegistroIVA")))
            RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

            myriga(3) = RegistroIVA.Descrizione
            myriga(4) = campodb(myPOSTreader.Item("TipoDocumento"))
            myriga(5) = campodb(myPOSTreader.Item("DataObbligatoria"))
            myriga(6) = campodb(myPOSTreader.Item("NumeroObbligatorio"))
            myriga(7) = campodb(myPOSTreader.Item("CausaleIncasso"))
            myriga(8) = campodb(myPOSTreader.Item("AllegatoFineAnno"))
            myriga(9) = campodb(myPOSTreader.Item("VenditaAcquisti"))
            myriga(10) = campodb(myPOSTreader.Item("CodiceIva"))
            myriga(11) = campodb(myPOSTreader.Item("Detraibilita"))
            myriga(12) = campodb(myPOSTreader.Item("CentroServizio"))
            myriga(13) = campodb(myPOSTreader.Item("Prorata"))
            myriga(14) = campodb(myPOSTreader.Item("Report"))
            myriga(15) = campodb(myPOSTreader.Item("Ritenuta"))
            myriga(16) = campodb(myPOSTreader.Item("DocumentoReverse"))
            myriga(17) = campodb(myPOSTreader.Item("GirocontoReverse"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
