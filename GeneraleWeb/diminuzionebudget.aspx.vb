﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_diminuzionebudget
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Private Sub faibind()

        Tabella = ViewState("diminunzioneprenotazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub


    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            Tabella = ViewState("diminunzioneprenotazione")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)

            ViewState("diminunzioneprenotazione") = Tabella
            Call faibind()
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblAnno As Label = DirectCast(e.Row.FindControl("LblAnno"), Label)
            If Not IsNothing(LblAnno) Then

            Else
                Dim TxtAnno As TextBox = DirectCast(e.Row.FindControl("TxtAnno"), TextBox)
                TxtAnno.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString
            End If


            Dim LblData As Label = DirectCast(e.Row.FindControl("LblData"), Label)
            If Not IsNothing(LblData) Then

            Else
                Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)
                TxtData.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloDataRegistrazione", MyJs, True)
            End If

            Dim LblImporto As Label = DirectCast(e.Row.FindControl("LblImporto"), Label)
            If Not IsNothing(LblImporto) Then

            Else
                Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
                TxtImporto.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString


                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImporto.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImporto.ClientID & "').val(),2); $('#" & TxtImporto.ClientID & "').val(ap); }); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", MyJs, True)
            End If

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            End If
        End If
    End Sub


    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtAnno As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtAnno"), TextBox)
        Dim TxtData As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtData"), TextBox)
        Dim TxtImporto As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtImporto"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)


        If Val(TxtAnno.Text) = 0 Then
            Exit Sub
        End If

        If Not IsDate(TxtData.Text) Then
            Exit Sub
        End If

        If Not IsNumeric(TxtImporto.Text) Then
            Exit Sub
        End If

        Tabella = ViewState("diminunzioneprenotazione")
        Dim row = Grid.Rows(e.RowIndex)


        Tabella.Rows(row.DataItemIndex).Item(0) = TxtAnno.Text

        Tabella.Rows(row.DataItemIndex).Item(1) = TxtData.Text
        Tabella.Rows(row.DataItemIndex).Item(2) = TxtImporto.Text
        Tabella.Rows(row.DataItemIndex).Item(3) = TxtDescrizione.Text

        ViewState("diminunzioneprenotazione") = Tabella

        Grid.EditIndex = -1
        Call faibind()
    End Sub


    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        Tabella = ViewState("diminunzioneprenotazione")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        ViewState("diminunzioneprenotazione") = Tabella

        Call faibind()
    End Sub




    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call faibind()
    End Sub





    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Val(Session("RIGAPRENOTAZIONE")) = 0 Then            
            Exit Sub
        End If


        If Page.IsPostBack = True Then Exit Sub


        Dim xs As New Cls_DiminuzionePrenotazione

        xs.Anno = Session("PRENOTAZIONEANNO")
        xs.Numero = Session("PRENOTAZIONENUMERO")
        xs.Riga = Session("RIGAPRENOTAZIONE")
        xs.loaddati(Session("DC_GENERALE"), Tabella)

        ViewState("diminunzioneprenotazione") = Tabella

        Call faibind()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Tabella = ViewState("diminunzioneprenotazione")
        Dim xs As New Cls_DiminuzionePrenotazione

        xs.Anno = Session("PRENOTAZIONEANNO")
        xs.Numero = Session("PRENOTAZIONENUMERO")
        xs.Riga = Session("RIGAPRENOTAZIONE")
        xs.Scrivi(Session("DC_GENERALE"), Tabella)

        Dim MyJs As String

        MyJs = "$(document).ready(function() { alert('Modificato Diminuzione Prenotazione'); } );"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModScM", MyJs, True)
    End Sub
End Class
