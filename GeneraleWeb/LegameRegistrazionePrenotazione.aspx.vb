﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_LegameRegistrazionePrenotazione
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabellaRisultati As New System.Data.DataTable("tabella")
    Dim TRPreGrid As New System.Data.DataTable("tabella")

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub CARICAGRIGLE()
        ViewState("PRXLINEA") = -1
        ViewState("rgXLINEA") = -1

        Dim MyRs As New ADODB.Recordset
        Dim RSLegame As New ADODB.Recordset
        Dim Totale As Double
        Dim tipo As String


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Id", GetType(Long))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & Val(Request.Item("Numero")) & " Order by  DareAvere Desc"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim kConto As New Cls_Pianodeiconti

            kConto.Mastro = campodbN(myPOSTreader.Item("MastroPartita"))
            kConto.Conto = campodbN(myPOSTreader.Item("ContoPartita"))
            kConto.Sottoconto = campodbN(myPOSTreader.Item("SottoContoPartita"))

            kConto.Decodfica(Session("DC_GENERALE"))
            tipo = kConto.Tipo

            Totale = 0

            Dim cmdLegame As New OleDbCommand()
            cmdLegame.CommandText = "Select Sum(Importo) as Totale From PrenotazioniUsate Where NumeroRegistrazione = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & campodbN(myPOSTreader.Item("RigaRegistrazione"))
            cmdLegame.Connection = cn
            Dim myRDLegame As OleDbDataReader = cmdLegame.ExecuteReader()
            If myRDLegame.Read Then
                Totale = campodbN(myRDLegame.Item("Totale"))
            End If
            myRDLegame.Close()

            Dim cmdLegame1 As New OleDbCommand()
            cmdLegame1.CommandText = "Select Sum(Importo) as Totale From LegameBudgetRegistrazione  Where NumeroRegistrazione = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & Val(campodb(myPOSTreader.Item("RigaRegistrazione")))
            cmdLegame1.Connection = cn
            Dim myRDLegame1 As OleDbDataReader = cmdLegame1.ExecuteReader()
            If myRDLegame1.Read Then
                Totale = Totale + campodbN(myRDLegame1.Item("Totale"))
            End If
            myRDLegame1.Close()



            If (tipo = "C" Or tipo = "R") And (campodbN(myPOSTreader.Item("Importo")) - Totale) > 0 Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Val(campodb(myPOSTreader.Item("RigaRegistrazione")))

                myriga(1) = kConto.Descrizione
                If campodb(myPOSTreader.Item("DareAvere")) = "D" Then
                    myriga(2) = Format(campodb(myPOSTreader.Item("Importo")) - Totale, "#,##0.00")
                Else
                    myriga(3) = Format(campodb(myPOSTreader.Item("Importo")) - Totale, "#,##0.00")
                End If
                Tabella.Rows.Add(myriga)
            End If
        Loop

        ViewState("RigheRegistrazione") = Tabella

        GrdRighe.AutoGenerateColumns = False
        GrdRighe.DataSource = Tabella
        GrdRighe.Font.Size = 10
        GrdRighe.DataBind()

        TabellaRisultati.Clear()
        TabellaRisultati.Columns.Clear()
        TabellaRisultati.Columns.Add("Anno", GetType(Long))
        TabellaRisultati.Columns.Add("Numero", GetType(Long))
        TabellaRisultati.Columns.Add("Riga", GetType(Long))
        TabellaRisultati.Columns.Add("Importo", GetType(Double))
        TabellaRisultati.Columns.Add("Descrizione", GetType(String))
        TabellaRisultati.Columns.Add("SottoContoRiga", GetType(String))

        Dim cmdUs As New OleDbCommand()
        cmdUs.CommandText = "Select * From PrenotazioniUsate Where NumeroRegistrazione = " & Val(Request.Item("Numero"))
        cmdUs.Connection = cn
        Dim myRD As OleDbDataReader = cmdUs.ExecuteReader()
        Do While myRD.Read
            Dim Mastro As Long
            Dim Conto As Long
            Dim SottoConto As Long

            Mastro = 0
            Conto = 0
            SottoConto = 0

            Dim cmdLegame As New OleDbCommand()
            cmdLegame.CommandText = "Select  MastroPartita,ContoPartita,SottoContoPartita From   MovimentiContabiliRiga Where Numero = " & Val(Request.Item("Numero")) & " And RigaRegistrazione = " & campodbN(myRD.Item("RigaRegistrazione"))
            cmdLegame.Connection = cn
            Dim myRDLegame As OleDbDataReader = cmdLegame.ExecuteReader()
            If myRDLegame.Read Then
                Mastro = campodbN(myRDLegame.Item("MastroPartita"))
                Conto = campodbN(myRDLegame.Item("ContoPartita"))
                SottoConto = campodbN(myRDLegame.Item("SottocontoPartita"))
            End If
            myRDLegame.Close()

            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            myriga(0) = campodb(myRD.Item("AnnoPrenotazione"))
            myriga(1) = campodb(myRD.Item("NumeroPrenotazione"))
            myriga(2) = campodb(myRD.Item("RigaPrenotazione"))
            myriga(3) = Format(Math.Round(CDbl(campodbN(myRD.Item("Importo"))), 2), "#,##0.00")

            Dim MyPrenotazione As New Cls_PrenotazioniTesta

            MyPrenotazione.Anno = campodb(myRD.Item("AnnoPrenotazione"))
            MyPrenotazione.Numero = campodb(myRD.Item("NumeroPrenotazione"))

            MyPrenotazione.Leggi(Session("DC_GENERALE"))

            Dim xI As Long

            For xI = 0 To 100
                If Not IsNothing(MyPrenotazione.Righe(xI)) Then
                    If MyPrenotazione.Righe(xI).Riga = Val(campodb(myRD.Item("RigaPrenotazione"))) Then
                        myriga(4) = MyPrenotazione.Righe(xI).Descrizione
                    End If
                End If
            Next

            Dim XS As New Cls_Pianodeiconti

            XS.Mastro = Mastro
            XS.Conto = Conto
            XS.Sottoconto = SottoConto
            XS.Decodfica(Session("DC_GENERALE"))
            myriga(5) = XS.Descrizione

            TabellaRisultati.Rows.Add(myriga)
        Loop
        myRD.Close()
        cn.Close()

        If TabellaRisultati.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            myriga(0) = 0
            myriga(1) = 0
            myriga(2) = 0
            myriga(3) = 0
            TabellaRisultati.Rows.Add(myriga)
        End If
        Grid.AutoGenerateColumns = False
        Grid.DataSource = TabellaRisultati
        Grid.Font.Size = 10
        Grid.DataBind()
        ViewState("TabellaRisultati") = TabellaRisultati
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Call CARICAGRIGLE()

        Txt_Anno.Text = Year(Now)
        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)
    End Sub

    Protected Sub GrdRighe_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdRighe.RowCommand
        If (e.CommandName = "Seleziona") Then
            Dim d As Long = e.CommandArgument

            If ViewState("rgXLINEA") <> -1 Then
                GrdRighe.Rows(ViewState("rgXLINEA")).BackColor = ViewState("rgXBACKUP")
            End If

            Tabella = ViewState("RigheRegistrazione")
            ViewState("SelezionatoRiga") = Tabella.Rows(d).Item(0).ToString

            ViewState("rgXLINEA") = d
            ViewState("rgXBACKUP") = Grid.Rows(d).BackColor
            GrdRighe.Rows(d).BackColor = Drawing.Color.Violet

        End If
    End Sub



    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Delete") Then
            TabellaRisultati = ViewState("TabellaRisultati")

            Dim cn As OleDbConnection
            Dim MySql As String

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
            cn.Open()

            MySql = "DELETE FROM PrenotazioniUsate WHERE AnnoPrenotazione = ? AND NumeroPrenotazione = ? AND RigaPrenotazione = ? AND NumeroRegistrazione = ? AND Importo =?"

            Dim cmdUs As New OleDbCommand()
            cmdUs.CommandText = MySql
            cmdUs.Connection = cn

            Dim Anno As Long
            Dim Numero As Long
            Dim Riga As Long
            Dim NumeroRegistrazione As Long
            Dim Importo As Double

            Anno = Val(TabellaRisultati.Rows(e.CommandArgument).Item(0))
            Numero = Val(TabellaRisultati.Rows(e.CommandArgument).Item(1))
            Riga = Val(TabellaRisultati.Rows(e.CommandArgument).Item(2))
            NumeroRegistrazione = Val(Request.Item("Numero"))
            Importo = TabellaRisultati.Rows(e.CommandArgument).Item(3)



            cmdUs.Parameters.AddWithValue("@AnnoPrenotazione", Anno)
            cmdUs.Parameters.AddWithValue("@NumeroPrenotazione", Numero)
            cmdUs.Parameters.AddWithValue("@RigaPrenotazione", Riga)
            cmdUs.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)            
            cmdUs.Parameters.AddWithValue("@Importo", Importo)
            cmdUs.ExecuteNonQuery()

            cn.Close()

            Call CARICAGRIGLE()
        End If
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)
    End Sub

    Protected Sub BTN_Ricerca_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_Ricerca.Click
        Dim Conzione As String
        Dim MySql As String

        TRPreGrid.Clear()
        TRPreGrid.Columns.Clear()
        TRPreGrid.Columns.Add("Anno", GetType(Long))
        TRPreGrid.Columns.Add("Numero", GetType(Long))
        TRPreGrid.Columns.Add("Riga", GetType(Long))
        TRPreGrid.Columns.Add("Importo", GetType(Double))
        TRPreGrid.Columns.Add("Disponibile", GetType(Double))
        TRPreGrid.Columns.Add("Livello1", GetType(Long))
        TRPreGrid.Columns.Add("Livello2", GetType(Long))
        TRPreGrid.Columns.Add("Livello3", GetType(Long))
        TRPreGrid.Columns.Add("Descrizione", GetType(String))


        Conzione = ""

        If Txt_Anno.Text > 0 Then
            If Conzione <> "" Then
                Conzione = Conzione & " And  "
            End If
            Conzione = Conzione & " Anno = " & Txt_Anno.Text
        End If

        If Txt_Livello.Text <> "" Then

            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Livello.Text)

            If Vettore.Length >= 3 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))

                If Conzione <> "" Then
                    Conzione = Conzione & " And  "
                End If
                Conzione = Conzione & " Livello1 = " & Mastro

                If Conzione <> "" Then
                    Conzione = Conzione & " And  "
                End If
                Conzione = Conzione & " Livello2 = " & Conto

                If Conzione <> "" Then
                    Conzione = Conzione & " And  "
                End If
                Conzione = Conzione & " Livello3 = " & Sottoconto
            End If

        End If


        If Txt_Numero.Text <> "" Then
            If Conzione <> "" Then
                Conzione = Conzione & " And  "
            End If
            Conzione = Conzione & " Numero = " & Txt_Numero.Text
        End If
        If Conzione <> "" Then
            Conzione = "Where " & Conzione
        End If


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        MySql = "SELECT (Select sum(Importo) From  PrenotazioniUsate Where AnnoPrenotazione = PrenotazioniRiga.Anno And NumeroPrenotazione =PrenotazioniRiga.Numero And RigaPrenotazione = PrenotazioniRiga.Riga) as Usate,(Select sum(Importo) From VariazioniPrenotazioni Where AnnoPrenotazione = PrenotazioniRiga.Anno And NumeroPrenotazione = PrenotazioniRiga.Numero And RigaPrenotazione = PrenotazioniRiga.Riga) AS Diminuite, * From PrenotazioniRiga " & Conzione

        Dim cmdUs As New OleDbCommand()
        cmdUs.CommandText = MySql
        cmdUs.Connection = cn
        Dim myRD As OleDbDataReader = cmdUs.ExecuteReader()
        Do While myRD.Read
            Dim Importo As Double = campodbN(myRD.Item("Importo"))
            Dim Usate As Double = campodbN(myRD.Item("Usate"))
            Dim Diminuite As Double = campodbN(myRD.Item("Diminuite"))

            If Math.Abs(Math.Round(Importo, 2)) > Math.Abs(Math.Round(Usate, 2)) + Math.Abs(Math.Round(Diminuite, 2)) Then
                Dim myriga As System.Data.DataRow = TRPreGrid.NewRow()
                myriga(0) = campodbN(myRD.Item("Anno"))
                myriga(1) = campodbN(myRD.Item("Numero"))
                myriga(2) = campodbN(myRD.Item("Riga"))

                myriga(3) = Format(Importo, "#,##0.00")

                myriga(4) = Format(Importo - Usate - Diminuite, "#,##0.00")

                myriga(5) = Val(campodb(myRD.Item("Livello1")))
                myriga(6) = Val(campodb(myRD.Item("Livello2")))
                myriga(7) = Val(campodb(myRD.Item("Livello3")))

                Dim XC As New Cls_TipoBudget

                XC.Livello1 = Val(campodb(myRD.Item("Livello1")))
                XC.Livello2 = Val(campodb(myRD.Item("Livello1")))
                XC.Livello3 = Val(campodb(myRD.Item("Livello1")))
                XC.Decodfica(Session("DC_GENERALE"))
                myriga(8) = XC.Descrizione
                
                TRPreGrid.Rows.Add(myriga)
            End If
        Loop

        cmdUs.Clone()
        cn.Close()

        GridPrenotazioni.AutoGenerateColumns = True
        GridPrenotazioni.DataSource = TRPreGrid
        GridPrenotazioni.Font.Size = 10
        GridPrenotazioni.DataBind()

        ViewState("Prenotazioni") = TRPreGrid
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub GridPrenotazioni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridPrenotazioni.RowCommand
        If (e.CommandName = "Seleziona") Then

            If ViewState("rgXLINEA") = -1 Then
                Exit Sub
            End If

            Dim d As Long = e.CommandArgument

            If ViewState("PRXLINEA") <> -1 Then
                GridPrenotazioni.Rows(ViewState("PRXLINEA")).BackColor = ViewState("PRXBACKUP")
            End If
            
            TRPreGrid = ViewState("Prenotazioni")
            Tabella = ViewState("RigheRegistrazione")

            Dim ImportoPrenotazione As Double
            ImportoPrenotazione = TRPreGrid.Rows(d).Item(4)
            Dim ImportoRiga As Double
            If campodbN(Tabella.Rows(ViewState("rgXLINEA")).Item(3)) > campodbN(Tabella.Rows(ViewState("rgXLINEA")).Item(2)) Then
                ImportoRiga = Tabella.Rows(ViewState("rgXLINEA")).Item(3)
            Else
                ImportoRiga = Tabella.Rows(ViewState("rgXLINEA")).Item(2)
            End If

            If ImportoRiga < ImportoPrenotazione Then
                Txt_Importo.Text = ImportoRiga
            Else
                Txt_Importo.Text = ImportoPrenotazione
            End If

            ViewState("PRXLINEA") = d
            ViewState("PRXBACKUP") = GridPrenotazioni.Rows(d).BackColor
            GridPrenotazioni.Rows(d).BackColor = Drawing.Color.Violet

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MTyBox", "$(window).load(function() {  setTimeout('apridh()', 100);  });", True)
        End If
    End Sub




    Protected Sub Btn_Importo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Importo.Click

        If Txt_Importo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Selezionare Importo');", True)
            Exit Sub
        End If
        If Val(ViewState("PRXLINEA")) = -1 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Selezionare prenotazione');", True)
            Exit Sub
        End If
        If Val(ViewState("rgXLINEA")) = -1 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Selezionare riga registrazione');", True)
            Exit Sub
        End If
        Dim MySql As String
        Dim Anno As Long
        Dim Numero As Long
        Dim Riga As Long
        Dim NumeroRegistrazione As Long
        Dim RigaRegistrazione As Long


        TRPreGrid = ViewState("Prenotazioni")
        Tabella = ViewState("RigheRegistrazione")

        Anno = Val(TRPreGrid.Rows(Val(ViewState("PRXLINEA"))).Item(0))
        Numero = Val(TRPreGrid.Rows(Val(ViewState("PRXLINEA"))).Item(1))
        Riga = Val(TRPreGrid.Rows(Val(ViewState("PRXLINEA"))).Item(2))
        NumeroRegistrazione = Val(Request.Item("Numero"))
        RigaRegistrazione = Val(ViewState("SelezionatoRiga"))
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        MySql = "INSERT INTO PrenotazioniUsate (AnnoPrenotazione,NumeroPrenotazione ,RigaPrenotazione,NumeroRegistrazione,RigaRegistrazione,Importo) VALUES (?,?,?,?,?,?)"

        Dim cmdUs As New OleDbCommand()
        cmdUs.CommandText = MySql
        cmdUs.Connection = cn

        cmdUs.Parameters.AddWithValue("@AnnoPrenotazione", Anno)
        cmdUs.Parameters.AddWithValue("@NumeroPrenotazione", Numero)
        cmdUs.Parameters.AddWithValue("@RigaPrenotazione", Riga)
        cmdUs.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmdUs.Parameters.AddWithValue("@RigaRegistrazione", RigaRegistrazione)
        cmdUs.Parameters.AddWithValue("@Importo", CDbl(Txt_Importo.Text))
        cmdUs.ExecuteNonQuery()

        cn.Close()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Prenotazione collegata');", True)
        Call CARICAGRIGLE()
    End Sub


    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted
        REM O
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        REM O
    End Sub
End Class
