﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_DatiMandatiReversali
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_DatiMandatiReversali_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim x As New Cls_MovimentoContabile

        x.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))


        DD_TipoPagametno.Items.Clear()
        DD_TipoPagametno.Items.Add("Cassa")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "01"

        DD_TipoPagametno.Items.Add("Bonifico")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "53"

        DD_TipoPagametno.Items.Add("C/C Postale")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "09"

        DD_TipoPagametno.Items.Add("Come da allegato")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "85"

        DD_TipoPagametno.Items.Add("Con disposizione documento esterno")
        DD_TipoPagametno.Items(DD_TipoPagametno.Items.Count - 1).Value = "84"

        Dim Cofog As New Cls_TabellaCofog

        Cofog.UpDateDropBox(Session("DC_GENERALE"), DD_Cofog)

        Rb_Esente_Bollo.Checked = False
        RB_CaricoEnte_Bollo.Checked = False
        Rb_CaricoCliente_Bollo.Checked = False

        Rb_Esente_Spese.Checked = False
        RB_CaricoEnte_Spese.Checked = False
        Rb_CaricoCliente_Spese.Checked = False


        If x.BolloMandato = 0 Then Rb_Esente_Bollo.Checked = True
        If x.BolloMandato = 1 Then RB_CaricoEnte_Bollo.Checked = True
        If x.BolloMandato = 2 Then Rb_CaricoCliente_Bollo.Checked = True

        If x.EspSpeseMandato = 0 Then Rb_Esente_Spese.Checked = True
        If x.EspSpeseMandato = 1 Then RB_CaricoEnte_Spese.Checked = True
        If x.EspSpeseMandato = 2 Then Rb_CaricoCliente_Spese.Checked = True

        DD_TipoPagametno.SelectedValue = x.EspTipoPagamento
        Txt_NumeroRegolazione.Text = x.EspNumeroRegolarizzazione

        Txt_AnnoImpegno.Text = x.AnnoImpegno
        Txt_NumeroImpegno.Text = x.NumeroImpegno
        Txt_CodiceCig.Text = x.CodiceCig
        Txt_Siope.Text = x.EspCodiceSIOPE
        Txt_Cup.Text = x.EspCodiceCUP
        If Year(x.EspDataEsecuzionePagamento) > 1900 Then
            Txt_DataEsecuzionePagamento.Text = Format(x.EspDataEsecuzionePagamento, "dd/MM/yyyy")
        End If

        Txt_NumeroElenco.Text = x.EspNumeroElenco
        DD_TipoPagametno.SelectedValue = x.EspTipoPagamento

        Dim Anag As New Cls_ClienteFornitore
        Anag.CODICEDEBITORECREDITORE = x.EspCodiceDelegatoQuietanzante
        Anag.Nome = ""
        Anag.Leggi(Session("DC_OSPITE"))

        If Anag.Nome.Trim <> "" Then
            Txt_Delegato.Text = Anag.CODICEDEBITORECREDITORE & " " & Anag.Nome
        End If
        DD_Cofog.SelectedValue = x.Cofog

        If Year(x.DataReversale) > 1900 Then
            Txt_DataReversale.Text = Format(x.DataReversale, "dd/MM/yyyy")
        Else
            Txt_DataReversale.Text = ""
        End If
        Txt_NumeroReversale.Text = x.NumeroReversale


        Txt_NumeroMandato.Text = x.NumeroMandato

        If Year(x.DataMandato) > 1900 Then
            Txt_DataMandato.Text = Format(x.DataMandato, "dd/MM/yyyy")
        Else
            Txt_DataMandato.Text = ""
        End If



        If Year(x.DataDistinta) > 1900 Then
            Txt_DataDistinta.Text = Format(x.DataDistinta, "dd/MM/yyyy")
        Else
            Txt_DataDistinta.Text = ""
        End If


        Txt_NumeroDistinta.Text = x.NumeroDistinta

        If x.TipoStorno = "S" Then
            Chk_Storno.Checked = True
        Else
            Chk_Storno.Checked = False
        End If
        Call EseguiJS()

    End Sub

    Private Sub modifica()

        Dim x As New Cls_MovimentoContabile

        x.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))

        If Rb_Esente_Bollo.Checked = True Then x.BolloMandato = 0
        If RB_CaricoEnte_Bollo.Checked = True Then x.BolloMandato = 1
        If Rb_CaricoCliente_Bollo.Checked = True Then x.BolloMandato = 2

        If Rb_Esente_Spese.Checked = True Then x.EspSpeseMandato = 0
        If RB_CaricoEnte_Spese.Checked = True Then x.EspSpeseMandato = 1
        If Rb_CaricoCliente_Spese.Checked = True Then x.EspSpeseMandato = 2

        x.EspTipoPagamento = DD_TipoPagametno.SelectedValue
        x.EspNumeroRegolarizzazione = Val(Txt_NumeroRegolazione.Text)

        x.AnnoImpegno = Val(Txt_AnnoImpegno.Text)
        x.NumeroImpegno = Val(Txt_NumeroImpegno.Text)
        x.CodiceCig = Txt_CodiceCig.Text
        x.EspCodiceSIOPE = Txt_Siope.Text
        x.EspCodiceCUP = Txt_Cup.Text
        x.EspDataEsecuzionePagamento = IIf(IsDate(Txt_DataEsecuzionePagamento.Text), Txt_DataEsecuzionePagamento.Text, Nothing)
        x.EspNumeroElenco = Txt_NumeroElenco.Text
        x.EspTipoPagamento = DD_TipoPagametno.SelectedValue

        If Txt_Delegato.Text.Length > 3 And Txt_Delegato.Text.IndexOf(" ") > 0 Then
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Delegato.Text)

            x.EspCodiceDelegatoQuietanzante = xVettore(0)
        End If
        x.Cofog = DD_Cofog.SelectedValue

        x.DataReversale = IIf(IsDate(Txt_DataReversale.Text), Txt_DataReversale.Text, Nothing)
        x.NumeroReversale = Txt_NumeroReversale.Text


        x.NumeroMandato = Txt_NumeroMandato.Text
        x.DataMandato = IIf(IsDate(Txt_DataMandato.Text), Txt_DataMandato.Text, Nothing)

        x.DataDistinta = IIf(IsDate(Txt_DataDistinta.Text), Txt_DataDistinta.Text, Nothing)
        x.NumeroDistinta = Txt_NumeroDistinta.Text

        If Chk_Storno.Checked = True Then
            x.TipoStorno = "S"
        Else
            x.TipoStorno = ""
        End If

        If x.Scrivi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione"))) = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione non eseguita</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", "alert('Operazione Effettuata');", True)
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Val(Txt_NumeroMandato.Text) > 0 And Val(Txt_NumeroReversale.Text) > 0 Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi inserire sia il numero reversale che il numero mandato</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If IsDate(Txt_DataMandato.Text) And IsDate(Txt_DataReversale.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non puoi inserire sia la data reversale che la data mandato</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If



        If Val(Txt_NumeroMandato.Text) > 0 And Not IsDate(Txt_DataMandato.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare la data mandato se specificato numero mandato</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_NumeroReversale.Text) > 0 And Not IsDate(Txt_DataReversale.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare la data reversale se specificato numero reversale</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_NumeroMandato.Text) = 0 And IsDate(Txt_DataMandato.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare il numero mandato se specificato la data mandato</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_NumeroReversale.Text) = 0 And IsDate(Txt_DataReversale.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare il numero reversale se specificato la data reversale</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_NumeroDistinta.Text) > 0 And Not IsDate(Txt_DataDistinta.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare la data distinta se specificato numero distinta</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_NumeroDistinta.Text) = 0 And IsDate(Txt_DataDistinta.Text) Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare numero distinta se specificato la data distinta</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        If Val(Txt_NumeroDistinta.Text) > 0 And IsDate(Txt_DataDistinta.Text) Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione <> " & Val(Session("NumeroRegistrazione")) & " And NumeroDistinta = " & Txt_NumeroDistinta.Text & " And Year(DataDistinta) = " & Year(Txt_DataDistinta.Text)            
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If Format(campodbD(myPOSTreader.Item("DataDistinta")), "dd/MM/yyyy") <> Format(Txt_DataDistinta.Text, "dd/MM/yyyy") Then                    
                    myPOSTreader.Close()
                    cn.Close()
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>La data distinza inserita è diversa da quella usata</center>');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
        End If


        If Val(Txt_NumeroMandato.Text) > 0 And IsDate(Txt_DataMandato.Text) Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione <> " & Val(Session("NumeroRegistrazione")) & " And NumeroMandato = " & Txt_NumeroMandato.Text & " And Year(DataMandato) = " & Year(Txt_DataMandato.Text)
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then                
                myPOSTreader.Close()
                cn.Close()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Numero Mandato già usato da un altra registrazione, non posso proseguire</center>');", True)
                Call EseguiJS()
                Exit Sub            
            End If
            myPOSTreader.Close()
        End If

        If Val(Txt_NumeroReversale.Text) > 0 And IsDate(Txt_DataReversale.Text) Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione <> " & Val(Session("NumeroRegistrazione")) & " And Numeroreversale = " & Txt_NumeroReversale.Text & " And Year(DataReversale) = " & Year(Txt_DataReversale.Text)
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                myPOSTreader.Close()
                cn.Close()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Numero Reversale già usato da un altra registrazione, non posso proseguire</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If
            myPOSTreader.Close()
        End If
        cn.Close()


        If DD_TipoPagametno.SelectedItem.Text = "Come da allegato" And Txt_NumeroElenco.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Manca il Numero Elenco</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_NumeroElenco.Text <> "" And DD_TipoPagametno.SelectedItem.Text <> "Come da allegato" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Il Tipo Pagamento deve essere Come da allegato</center>');", True)
            Call EseguiJS()
            Exit Sub

        End If


        If Txt_Delegato.Text.Length > 3 And Txt_Delegato.Text.IndexOf(" ") > 0 Then
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Delegato.Text)

            Dim AnCliFor As New Cls_ClienteFornitore

            AnCliFor.CODICEDEBITORECREDITORE = xVettore(0)
            AnCliFor.Leggi(Session("DC_OSPITE"))
            If AnCliFor.CodiceFiscale.Trim = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Al Delegato / Quietanziante manca il Codice Fiscale</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        Call modifica()
    End Sub

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
End Class
