﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class GeneraleWeb_MovimentiCespiti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select * from MovimentiCespite  " & _
                               " Where CodiceCespite = ? Order by DataRegistrazione Desc ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Cespite", Session("CodiceCespite"))

        Dim Ammortamento As Double = 0


        Tabella.Clear()
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("Tipo Movimento", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("DataRegistrazione"))

            myriga(1) = campodb(myPOSTreader.Item("TipoMov"))

            myriga(2) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")

            Ammortamento = Ammortamento + campodbn(myPOSTreader.Item("Importo"))
            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()
        Dim Cespite As New Cls_Cespiti

        Cespite.Codice = Session("CodiceCespite")
        Cespite.Leggi(Session("DC_GENERALE"), Cespite.Codice)


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        lbl_Totale.Text = "<table Width=""100%"" style=""text-align: right;color:White;background-color:#565151;font-size:Small;font-weight:normal;""><tr><td>Prezzo Acq: " & Format(Cespite.ImportoAcq, "#,##0.00") & "</td><td>Fondo Iniziale: " & Format(Cespite.ImportoFondo, "#,##0.00") & "</td><td>Ammortizzato: " & Format(Ammortamento, "#,##0.00") & "</td><td>Tot.Amm.: " & Format(Ammortamento + Cespite.ImportoFondo, "#,##0.00") & "</td></tr></table>"
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
End Class
