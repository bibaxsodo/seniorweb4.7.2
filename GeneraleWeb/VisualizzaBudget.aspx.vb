﻿Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_VisualizzaBudget
    Inherits System.Web.UI.Page


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Private Sub PienaTabella()
        Dim Griglia(30, 1500) As String
        Dim GrRigheL1(1500) As Integer
        Dim GrRigheL2(1500) As Integer
        Dim GrRigheL3(1500) As Integer

        Dim GrTipo(1500) As String
        Dim StTipo(1500) As String

        Dim GrColonne(100) As Integer
        Dim StColonne(100) As String
        Dim Spaziatura As String
        Dim AppoSql As String
        Dim L1, L2, L3, I, y, xI, x As Integer
        Dim cn As OleDbConnection
        REM Dim cnTurni As OleDbConnection
        Dim Importob, Somma As Double
        Dim MyTable As New System.Data.DataTable("tabella")
        Dim MyDataSet As New System.Data.DataSet()

        Dim xMyTable As New System.Data.DataTable("tabella")
        Dim xMyDataSet As New System.Data.DataSet



        REM Dim MyConnectionString As String
        Dim ConnectionString As String = Session("DC_GENERALE")

        Dim DatiGenerali As New Cls_DatiGenerali


        DatiGenerali.LeggiDati(Session("DC_TABELLE"))



        cn = New Data.OleDb.OleDbConnection(ConnectionString)


        cn.Open()





        Dim CommandColonne As New OleDbCommand("Select * From ColonneBudget Where Anno = ? Order By Livello1", cn)

        CommandColonne.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Anno").Value = Txt_Anno.Text
        Dim myColonne As OleDbDataReader = CommandColonne.ExecuteReader()

        xI = 1
        Do While myColonne.Read()

            StColonne(xI) = myColonne.Item("Descrizione").ToString()
            GrColonne(xI) = myColonne.Item("Livello1").ToString()
            xI = xI + 1
        Loop
        myColonne.Close()



        Dim CommandRighe As New OleDbCommand("Select * From TipoBudget Where Anno = ? Order by Livello1,Livello2,Livello3 ", cn)

        CommandRighe.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandRighe.Parameters("@Anno").Value = Txt_Anno.Text
        Dim myRighe As OleDbDataReader = CommandRighe.ExecuteReader()
        I = 0
        Do While myRighe.Read()

            Spaziatura = ""
            If myRighe.Item("Livello1") > 0 And myRighe.Item("Livello2") = 0 And myRighe.Item("Livello3") = 0 Then
                Spaziatura = ""
            End If
            If myRighe.Item("Livello1") > 0 And myRighe.Item("Livello2") > 0 And myRighe.Item("Livello3") = 0 Then
                Spaziatura = "      "
            End If
            If myRighe.Item("Livello1") > 0 And myRighe.Item("Livello2") > 0 And myRighe.Item("Livello3") > 0 Then
                Spaziatura = "            "
            End If

            GrRigheL1(I) = myRighe.Item("Livello1")
            GrRigheL2(I) = myRighe.Item("Livello2")
            GrRigheL3(I) = myRighe.Item("Livello3")

            If myRighe.Item("Tipo") = "U" Then
                GrTipo(I) = "U"
            Else
                GrTipo(I) = "E"
            End If

            Griglia(1, I) = GrRigheL1(I) & "-" & GrRigheL2(I) & "-" & GrRigheL3(I)
            Griglia(2, I) = Spaziatura & myRighe.Item("Descrizione")
            StTipo(I) = myRighe.Item("Descrizione")
            I = I + 1
        Loop
        myRighe.Close()


        Dim CommandBudget As New OleDbCommand("Select *,(select sum(Importo) From VariazioniBudget Where Budget.Anno = VariazioniBudget.Anno And Budget.Livello1 = VariazioniBudget.Livello1 And Budget.Livello2 = VariazioniBudget.Livello2 And Budget.Livello3 = VariazioniBudget.Livello3 And Budget.Colonna = VariazioniBudget.Colonna) as totimp  From Budget Where Anno = ? ", cn)

        CommandBudget.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandBudget.Parameters("@Anno").Value = Txt_Anno.Text
        Dim myBudget As OleDbDataReader = CommandBudget.ExecuteReader()
        Do While myBudget.Read()
            For y = 0 To I
                If GrRigheL2(y) = 3 Then
                    y = y
                End If
                If myBudget.Item("Livello1") = GrRigheL1(y) And _
                   myBudget.Item("Livello2") = GrRigheL2(y) And _
                   myBudget.Item("Livello3") = GrRigheL3(y) Then
                    Exit For
                End If
            Next
            If I > y Then
                For x = 0 To xI - 1
                    If myBudget.Item("Colonna") = GrColonne(x) Then
                        Exit For
                    End If
                Next


                If IsDBNull(myBudget.Item("totimp")) Then
                    Griglia(x + 3, y) = Format(myBudget.Item("importo"), "#,##0.00")
                Else
                    Griglia(x + 3, y) = Format(myBudget.Item("importo") + myBudget.Item("totimp"), "#,##0.00")
                End If


            End If

        Loop
        myBudget.Close()

        MyTable.Columns.Add("Codifica", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Non Assegnato", GetType(String))


        xMyTable.Columns.Add("Livello1", GetType(Integer))
        xMyTable.Columns.Add("Livello2", GetType(Integer))
        xMyTable.Columns.Add("Livello3", GetType(Long))
        xMyTable.Columns.Add("Colonna", GetType(Integer))
        xMyTable.Columns.Add("EntrataUscita", GetType(String))
        xMyTable.Columns.Add("Descrizione", GetType(String))
        xMyTable.Columns.Add("Disponibilità", GetType(Double))
        xMyDataSet.Tables.Add(xMyTable)

        For x = 1 To xI - 1
            MyTable.Columns.Add(StColonne(x), GetType(String))
        Next
        MyTable.Columns.Add("Totale", GetType(String))
        MyDataSet.Tables.Add(MyTable)

        For y = 0 To I - 1
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = Griglia(1, y)
            myriga(1) = Griglia(2, y)
            If CDbl(Griglia(3, y)) > 0 Then
                myriga(2) = Format(CDbl(Griglia(3, y)), "#,##0.00")
                Somma = CDbl(Griglia(3, y))
            Else
                myriga(2) = Format(0, "#,##0.00")
                Somma = 0
            End If
            For x = 1 To xI - 1
                If CDbl(Griglia(x + 3, y)) > 0 Then
                    myriga(x + 2) = Format(CDbl(Griglia(x + 3, y)), "#,##0.00")
                    Somma = Somma + CDbl(Griglia(x + 3, y))
                Else
                    myriga(x + 2) = Format(0, "#,##0.00")
                    Somma = Somma + 0
                End If
            Next
            myriga(x + 2) = Format(Somma, "#,##0.00")


            MyTable.Rows.Add(myriga)



            REM MyRs.Open("SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where PrenotazioniRiga.Anno = " & Txt_Anno.Text & " And Livello1 = " & GrRigheL1(y) & " And Livello2 = " & GrRigheL2(y) & " And Livello3= " & GrRigheL3(y) & " And Colonna = " & GrColonne(x), GeneraleDb, adOpenForwardOnly, adLockOptimistic)
            REM If Not MyRs.EOF Then
            REM Importob = Importob - MoveFromDb(MyRs.Fields(0))

            Dim RigaPrenotato As System.Data.DataRow = MyTable.NewRow()
            RigaPrenotato(0) = ""
            RigaPrenotato(1) = "Prenotato"




            Dim CommandImpPrenotazioni As New OleDbCommand("Select Sum(Importo) From PrenotazioniRiga Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

            CommandImpPrenotazioni.Parameters.Add("@Anno", OleDbType.Integer, 8)
            CommandImpPrenotazioni.Parameters("@Anno").Value = Txt_Anno.Text

            CommandImpPrenotazioni.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
            CommandImpPrenotazioni.Parameters("@LIVELLO1").Value = GrRigheL1(y)
            CommandImpPrenotazioni.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
            CommandImpPrenotazioni.Parameters("@LIVELLO2").Value = GrRigheL2(y)
            CommandImpPrenotazioni.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
            CommandImpPrenotazioni.Parameters("@LIVELLO3").Value = GrRigheL3(y)

            CommandImpPrenotazioni.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
            CommandImpPrenotazioni.Parameters("@COLONNA").Value = 0
            Dim myPrenotazioni As OleDbDataReader = CommandImpPrenotazioni.ExecuteReader()


            If myPrenotazioni.Read() Then
                If Not IsDBNull(myPrenotazioni.Item(0)) Then
                    Importob = myPrenotazioni.Item(0)
                Else
                    Importob = 0
                End If
            End If

            myPrenotazioni.Close()
            CommandImpPrenotazioni.Dispose()

            Dim CommandImpPrenotazioniSott As New OleDbCommand("SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

            CommandImpPrenotazioniSott.Parameters.Add("@Anno", OleDbType.Integer, 8)
            CommandImpPrenotazioniSott.Parameters("@Anno").Value = Txt_Anno.Text

            CommandImpPrenotazioniSott.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
            CommandImpPrenotazioniSott.Parameters("@LIVELLO1").Value = GrRigheL1(y)
            CommandImpPrenotazioniSott.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
            CommandImpPrenotazioniSott.Parameters("@LIVELLO2").Value = GrRigheL2(y)
            CommandImpPrenotazioniSott.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
            CommandImpPrenotazioniSott.Parameters("@LIVELLO3").Value = GrRigheL3(y)

            CommandImpPrenotazioniSott.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
            CommandImpPrenotazioniSott.Parameters("@COLONNA").Value = 0
            Dim myPrenotazioniSott As OleDbDataReader = CommandImpPrenotazioniSott.ExecuteReader()


            If myPrenotazioniSott.Read() Then
                If Not IsDBNull(myPrenotazioniSott.Item(0)) Then
                    Importob = Importob - myPrenotazioniSott.Item(0)
                End If
            End If

            myPrenotazioniSott.Close()
            CommandImpPrenotazioniSott.Dispose()

            RigaPrenotato(2) = Format(Importob, "#,##0.00")

            Somma = Importob

            For x = 1 To xI - 1

                Dim CommandImpPrenotazioniCol As New OleDbCommand("Select Sum(Importo) From PrenotazioniRiga Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

                CommandImpPrenotazioniCol.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandImpPrenotazioniCol.Parameters("@Anno").Value = Txt_Anno.Text

                CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
                CommandImpPrenotazioniCol.Parameters("@LIVELLO1").Value = GrRigheL1(y)
                CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                CommandImpPrenotazioniCol.Parameters("@LIVELLO2").Value = GrRigheL2(y)
                CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                CommandImpPrenotazioniCol.Parameters("@LIVELLO3").Value = GrRigheL3(y)

                CommandImpPrenotazioniCol.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
                CommandImpPrenotazioniCol.Parameters("@COLONNA").Value = x
                Dim myPrenotazioniCol As OleDbDataReader = CommandImpPrenotazioniCol.ExecuteReader()


                If myPrenotazioniCol.Read() Then
                    If Not IsDBNull(myPrenotazioniCol.Item(0)) Then
                        Importob = myPrenotazioniCol.Item(0)
                    Else
                        Importob = 0
                    End If
                End If

                myPrenotazioniCol.Close()
                CommandImpPrenotazioniCol.Dispose()

                Dim CommandImpPrenotazioniColSot As New OleDbCommand("SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

                CommandImpPrenotazioniColSot.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandImpPrenotazioniColSot.Parameters("@Anno").Value = Txt_Anno.Text

                CommandImpPrenotazioniColSot.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
                CommandImpPrenotazioniColSot.Parameters("@LIVELLO1").Value = GrRigheL1(y)
                CommandImpPrenotazioniColSot.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                CommandImpPrenotazioniColSot.Parameters("@LIVELLO2").Value = GrRigheL2(y)
                CommandImpPrenotazioniColSot.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                CommandImpPrenotazioniColSot.Parameters("@LIVELLO3").Value = GrRigheL3(y)

                CommandImpPrenotazioniColSot.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
                CommandImpPrenotazioniColSot.Parameters("@COLONNA").Value = x
                Dim myPrenotazioniColSot As OleDbDataReader = CommandImpPrenotazioniColSot.ExecuteReader()


                If myPrenotazioniColSot.Read() Then
                    If Not IsDBNull(myPrenotazioniColSot.Item(0)) Then
                        Importob = Importob - myPrenotazioniColSot.Item(0)
                    End If
                End If
                myPrenotazioniColSot.Close()
                CommandImpPrenotazioniColSot.Dispose()

                RigaPrenotato(x + 2) = Format(Importob, "#,##0.00")
                Somma = Somma + Importob
            Next
            RigaPrenotato(x + 2) = Format(Somma, "#,##0.00")


            MyTable.Rows.Add(RigaPrenotato)

            Dim RigaLiquidato As System.Data.DataRow = MyTable.NewRow()
            RigaLiquidato(0) = ""
            RigaLiquidato(1) = "Liquidato"

            Dim CommandLiqPrenotazioniSott As New OleDbCommand("SELECT sum (PrenotazioniUsate.Importo) FROM PrenotazioniUsate INNER JOIN PrenotazioniRiga ON (PrenotazioniUsate.RigaPrenotazione = PrenotazioniRiga.Riga) AND (PrenotazioniUsate.AnnoPrenotazione = PrenotazioniRiga.Anno) AND (PrenotazioniUsate.NumeroPrenotazione = PrenotazioniRiga.Numero) Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

            CommandLiqPrenotazioniSott.Parameters.Add("@Anno", OleDbType.Integer, 8)
            CommandLiqPrenotazioniSott.Parameters("@Anno").Value = Txt_Anno.Text

            CommandLiqPrenotazioniSott.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
            CommandLiqPrenotazioniSott.Parameters("@LIVELLO1").Value = GrRigheL1(y)
            CommandLiqPrenotazioniSott.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
            CommandLiqPrenotazioniSott.Parameters("@LIVELLO2").Value = GrRigheL2(y)
            CommandLiqPrenotazioniSott.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
            CommandLiqPrenotazioniSott.Parameters("@LIVELLO3").Value = GrRigheL3(y)

            CommandLiqPrenotazioniSott.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
            CommandLiqPrenotazioniSott.Parameters("@COLONNA").Value = 0
            Dim myLiqPrenotazioniSott As OleDbDataReader = CommandLiqPrenotazioniSott.ExecuteReader()


            If myLiqPrenotazioniSott.Read() Then
                If Not IsDBNull(myLiqPrenotazioniSott.Item(0)) Then
                    Importob = myLiqPrenotazioniSott.Item(0)
                Else
                    Importob = 0
                End If
            End If

            myLiqPrenotazioniSott.Close()
            CommandLiqPrenotazioniSott.Dispose()

            RigaLiquidato(2) = Format(Importob, "#,##0.00")
            Somma = Importob
            For x = 1 To xI - 1
                Dim CommandLiqPrenotazioniSottRig As New OleDbCommand("SELECT sum (PrenotazioniUsate.Importo) FROM PrenotazioniUsate INNER JOIN PrenotazioniRiga ON (PrenotazioniUsate.RigaPrenotazione = PrenotazioniRiga.Riga) AND (PrenotazioniUsate.AnnoPrenotazione = PrenotazioniRiga.Anno) AND (PrenotazioniUsate.NumeroPrenotazione = PrenotazioniRiga.Numero) Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

                CommandLiqPrenotazioniSottRig.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandLiqPrenotazioniSottRig.Parameters("@Anno").Value = Txt_Anno.Text

                CommandLiqPrenotazioniSottRig.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
                CommandLiqPrenotazioniSottRig.Parameters("@LIVELLO1").Value = GrRigheL1(y)
                CommandLiqPrenotazioniSottRig.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                CommandLiqPrenotazioniSottRig.Parameters("@LIVELLO2").Value = GrRigheL2(y)
                CommandLiqPrenotazioniSottRig.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                CommandLiqPrenotazioniSottRig.Parameters("@LIVELLO3").Value = GrRigheL3(y)

                CommandLiqPrenotazioniSottRig.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
                CommandLiqPrenotazioniSottRig.Parameters("@COLONNA").Value = x
                Dim myLiqPrenotazioniSottRig As OleDbDataReader = CommandLiqPrenotazioniSottRig.ExecuteReader()


                If myLiqPrenotazioniSottRig.Read() Then
                    If Not IsDBNull(myLiqPrenotazioniSottRig.Item(0)) Then
                        Importob = myLiqPrenotazioniSottRig.Item(0)
                    Else
                        Importob = 0
                    End If
                Else
                    Importob = 0
                End If
                'RigaPrenotato(2) = Importob

                myLiqPrenotazioniSottRig.Close()
                CommandLiqPrenotazioniSottRig.Cancel()
                CommandLiqPrenotazioniSottRig.Dispose()

                Somma = Somma + Importob
                RigaLiquidato(x + 2) = Format(Importob, "#,##0.00")
            Next

            RigaLiquidato(x + 2) = Format(Somma, "#,##0.00")

            MyTable.Rows.Add(RigaLiquidato)

            Dim RigaLegato As System.Data.DataRow = MyTable.NewRow()
            RigaLegato(0) = ""
            RigaLegato(1) = "Legato (Non Prenot)"





            AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?"
            If GrRigheL3(y) = 0 Then
                AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ?  And Colonna = ?"
            End If
            If GrRigheL2(y) = 0 Then
                AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ?  And Colonna = ?"
            End If

            Dim CommandSnReg As New OleDbCommand(AppoSql, cn)

            CommandSnReg.Parameters.Add("@Anno", OleDbType.Integer, 8)
            CommandSnReg.Parameters("@Anno").Value = Txt_Anno.Text

            CommandSnReg.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
            CommandSnReg.Parameters("@LIVELLO1").Value = GrRigheL1(y)
            If GrRigheL2(y) > 0 Then
                CommandSnReg.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                CommandSnReg.Parameters("@LIVELLO2").Value = GrRigheL2(y)
                If GrRigheL3(y) > 0 Then
                    CommandSnReg.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                    CommandSnReg.Parameters("@LIVELLO3").Value = GrRigheL3(y)
                End If

            End If
            CommandSnReg.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
            CommandSnReg.Parameters("@COLONNA").Value = 0

            Dim mySnReg As OleDbDataReader = CommandSnReg.ExecuteReader()
            If mySnReg.Read Then
                If Not IsDBNull(mySnReg.Item(0)) Then
                    Somma = Somma + CDbl(mySnReg.Item(0))
                End If
            End If
            mySnReg.Close()
            CommandSnReg.Dispose()

            'RigaPrenotato(2) = Importob



            RigaLegato(2) = Format(Somma, "#,##0.00")
            For x = 1 To xI - 1

                Somma=0

                AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?"
                If GrRigheL3(y) = 0 Then
                    AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ? And Livello2 = ?  And Colonna = ?"
                End If
                If GrRigheL2(y) = 0 Then
                    AppoSql = "Select sum(importo) From LegameBudgetRegistrazione Where Anno = ? And Livello1 = ?  And Colonna = ?"
                End If


                Dim CommandSnRegR As New OleDbCommand(AppoSql, cn)

                CommandSnRegR.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@Anno").Value = Txt_Anno.Text

                CommandSnRegR.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@LIVELLO1").Value = GrRigheL1(y)
                If GrRigheL2(y) > 0 Then
                    CommandSnRegR.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
                    CommandSnRegR.Parameters("@LIVELLO2").Value = GrRigheL2(y)
                    If GrRigheL3(y) > 0 Then
                        CommandSnRegR.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
                        CommandSnRegR.Parameters("@LIVELLO3").Value = GrRigheL3(y)
                    End If

                End If

                CommandSnRegR.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
                CommandSnRegR.Parameters("@COLONNA").Value = x

                Dim mySnRegR As OleDbDataReader = CommandSnRegR.ExecuteReader()
                If mySnRegR.Read Then
                    If Not IsDBNull(mySnRegR.Item(0)) Then
                        Somma = Somma + CDbl(mySnRegR.Item(0))
                    End If
                End If
                mySnRegR.Close()
                CommandSnRegR.Dispose()


                RigaLegato(x + 2) = Format(Somma, "#,##0.00")
            Next
            RigaLegato(x + 2) = Format(0, "#,##0.00")

            MyTable.Rows.Add(RigaLegato)

            If DatiGenerali.BudgetAnalitica = 0 Then
                Dim RigaDisponibilita As System.Data.DataRow = MyTable.NewRow()
                RigaDisponibilita(0) = ""
                RigaDisponibilita(1) = "Disponibilità"

                If myriga(2) = "" Then
                    RigaDisponibilita(2) = Format(0 - CDbl(RigaPrenotato(2)) - CDbl(RigaLegato(2)), "#,##0.00")
                Else
                    RigaDisponibilita(2) = Format(CDbl(myriga(2)) - CDbl(RigaPrenotato(2)) - CDbl(RigaLegato(2)), "#,##0.00")
                End If
                For x = 1 To xI - 1
                    If myriga(x + 2) = "" Then
                        RigaDisponibilita(x + 2) = Format(0 - CDbl(RigaPrenotato(x + 2)) - CDbl(RigaLegato(x + 2)), "#,##0.00")
                    Else
                        RigaDisponibilita(x + 2) = Format(CDbl(myriga(x + 2)) - CDbl(RigaPrenotato(x + 2)) - CDbl(RigaLegato(x + 2)), "#,##0.00")
                    End If
                Next
                RigaDisponibilita(x + 2) = Format(myriga(x + 2) - RigaPrenotato(x + 2) - RigaLegato(x + 2), "#,##0.00")

                MyTable.Rows.Add(RigaDisponibilita)


                Dim xmyriga As System.Data.DataRow = xMyTable.NewRow()

                xmyriga(0) = GrRigheL1(y)
                xmyriga(1) = GrRigheL2(y)
                xmyriga(2) = GrRigheL3(y)
                xmyriga(3) = x
                xmyriga(4) = GrTipo(y)
                xmyriga(5) = StTipo(y)
                xmyriga(6) = RigaDisponibilita(x + 2)

                xMyTable.Rows.Add(xmyriga)
            End If
        Next


        DaGrid.AutoGenerateColumns = True
        DaGrid.DataSource = MyTable
        DaGrid.DataMember = "tabella"
        DaGrid.PageSize = 20
        DaGrid.DataBind()



        For y = 0 To DaGrid.Rows.Count - 1
            If DaGrid.Rows.Item(y).Cells(1).Text = "Prenotato" Then
                L1 = GrRigheL1((y + 3) / 5)
                L2 = GrRigheL2((y + 3) / 5)
                L3 = GrRigheL3((y + 3) / 5)
                DaGrid.Rows.Item(y).Cells(1).ForeColor = Drawing.Color.Red
                DaGrid.Rows.Item(y).Cells(2).ForeColor = Drawing.Color.Red
                Dim AppoggioLink As String = "VisualizzaPrenotati.aspx?Anno=" & Txt_Anno.Text & "&Colonna=0&Livello1=" & L1 & "&Livello2=" & L2 & "&Livello3=" & L3 & ""
                DaGrid.Rows.Item(y).Cells(2).Text = "<a  href=""#"" onclick=""DialogBox('" & AppoggioLink & "');"">" & DaGrid.Rows.Item(y).Cells(2).Text & "</a>"
                For x = 1 To xI
                    DaGrid.Rows.Item(y).Cells(x + 2).ForeColor = Drawing.Color.Red
                    If x < xI Then
                        AppoggioLink = "VisualizzaPrenotati.aspx?Anno=" & Txt_Anno.Text & "&Colonna=" & x & "&Livello1=" & L1 & "&Livello2=" & L2 & "&Livello3=" & L3 & ""
                        DaGrid.Rows.Item(y).Cells(x + 2).Text = "<a href=""#"" onclick=""DialogBox('" & AppoggioLink & "');"">" & DaGrid.Rows.Item(y).Cells(x + 2).Text & "</a>"
                    End If
                Next
            End If
            If DaGrid.Rows.Item(y).Cells(1).Text = "Liquidato" Then
                DaGrid.Rows.Item(y).Cells(1).ForeColor = Drawing.Color.PaleVioletRed
                DaGrid.Rows.Item(y).Cells(2).ForeColor = Drawing.Color.PaleVioletRed
                For x = 1 To xI
                    DaGrid.Rows.Item(y).Cells(x + 2).ForeColor = Drawing.Color.PaleVioletRed
                Next

            End If
            If DaGrid.Rows.Item(y).Cells(1).Text = "Legato (Non Prenot)" Then
                DaGrid.Rows.Item(y).Cells(1).ForeColor = Drawing.Color.Orange
                DaGrid.Rows.Item(y).Cells(2).ForeColor = Drawing.Color.Orange
                For x = 1 To xI
                    DaGrid.Rows.Item(y).Cells(x + 2).ForeColor = Drawing.Color.Orange
                Next
            End If
            If DaGrid.Rows.Item(y).Cells(1).Text = "Disponibilit&#224;" Then
                DaGrid.Rows.Item(y).Cells(1).ForeColor = Drawing.Color.Blue
                DaGrid.Rows.Item(y).Cells(1).Font.Bold = True
                DaGrid.Rows.Item(y).Cells(2).ForeColor = Drawing.Color.Blue
                DaGrid.Rows.Item(y).Cells(2).Font.Bold = True
                For x = 1 To xI
                    DaGrid.Rows.Item(y).Cells(x + 2).ForeColor = Drawing.Color.Blue
                    DaGrid.Rows.Item(y).Cells(x + 2).Font.Bold = True
                Next
            End If
        Next





        Dim AppoggioImporto As Double
        For y = 0 To DaGrid.Rows.Count - 1 Step 5

            If DaGrid.Rows.Item(y).Cells(1).BackColor = Drawing.Color.Black Then
                AppoggioImporto = 0
            End If

            AppoggioImporto = 0
            For x = 2 To DaGrid.Rows.Item(y).Cells.Count - 1
                AppoggioImporto = AppoggioImporto + Val(DaGrid.Rows.Item(y).Cells(x).Text)
            Next
            If AppoggioImporto = 0 Then

                Dim CommandCol1 As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?", cn)

                CommandCol1.Parameters.Add("@Anno", OleDbType.Integer, 8)
                CommandCol1.Parameters("@Anno").Value = Txt_Anno.Text

                CommandCol1.Parameters.Add("@Livello1", OleDbType.Integer, 8)
                CommandCol1.Parameters("@Livello1").Value = GrRigheL1(Int(y / 5))

                CommandCol1.Parameters.Add("@Livello2", OleDbType.Integer, 8)
                CommandCol1.Parameters("@Livello2").Value = GrRigheL2(Int(y / 5))

                CommandCol1.Parameters.Add("@Livello3", OleDbType.Integer, 8)
                CommandCol1.Parameters("@Livello3").Value = 0 ' GrRigheL3(Int(y / 5))
                Dim RdColonne1 As OleDbDataReader = CommandCol1.ExecuteReader()
                If RdColonne1.Read() Then
                    AppoggioImporto = Val(RdColonne1.Item("Importo").ToString)
                End If
                RdColonne1.Close()
                If AppoggioImporto = 0 And GrRigheL2(Int(y / 5)) > 0 Then

                    Dim CommandLivelUp As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?", cn)

                    CommandLivelUp.Parameters.Add("@Anno", OleDbType.Integer, 8)
                    CommandLivelUp.Parameters("@Anno").Value = Txt_Anno.Text

                    CommandLivelUp.Parameters.Add("@Livello1", OleDbType.Integer, 8)
                    CommandLivelUp.Parameters("@Livello1").Value = GrRigheL1(Int(y / 5))

                    CommandLivelUp.Parameters.Add("@Livello2", OleDbType.Integer, 8)
                    CommandLivelUp.Parameters("@Livello2").Value = 0

                    CommandLivelUp.Parameters.Add("@Livello3", OleDbType.Integer, 8)
                    CommandLivelUp.Parameters("@Livello3").Value = 0
                    Dim ReadLivelUp As OleDbDataReader = CommandLivelUp.ExecuteReader()
                    If ReadLivelUp.Read() Then
                        AppoggioImporto = Val(ReadLivelUp.Item("Importo").ToString)
                    End If
                    ReadLivelUp.Close()
                End If
            End If
            If AppoggioImporto = 0 Then
                If DatiGenerali.BudgetAnalitica = 0 Then
                    DaGrid.Rows.Item(y).Visible = False
                    DaGrid.Rows.Item(y + 1).Visible = False
                    DaGrid.Rows.Item(y + 2).Visible = False
                    DaGrid.Rows.Item(y + 3).Visible = False
                    DaGrid.Rows.Item(y + 4).Visible = False
                End If
            End If
        Next

        For y = 0 To DaGrid.Rows.Count - 1 

            AppoggioImporto = 0
            For x = 2 To DaGrid.Rows.Item(y).Cells.Count - 2
                AppoggioImporto = AppoggioImporto + campodbN(MyTable.Rows(y).Item(x))
            Next

            DaGrid.Rows.Item(y).Cells(x).Text = Format(AppoggioImporto, "#,##0.00")

        Next


        If DatiGenerali.BudgetAnalitica = 1 Then
            Dim D As Integer = 0
            For y = DaGrid.Rows.Count - 1 To 0 Step -1
                D = D + 1

                If D = 2 Then
                    DaGrid.Rows(y).Visible = False
                End If
                If D = 3 Then
                    DaGrid.Rows(y).Visible = False
                End If
                If D = 4 Then
                    DaGrid.Rows(y).Visible = False
                    DaGrid.Rows(y + 3).Cells(0).Text = DaGrid.Rows(y).Cells(0).Text
                    'DaGrid.Rows(y + 3).Cells(1).Text & " " & 
                    DaGrid.Rows(y + 3).Cells(1).Text = DaGrid.Rows(y).Cells(1).Text
                    DaGrid.Rows(y + 3).Cells(1).ForeColor = System.Drawing.Color.Black
                    Dim Colonna As Integer

                    For Colonna = 1 To DaGrid.Rows.Item(y).Cells.Count - 1
                        DaGrid.Rows(y + 3).Cells(Colonna).ForeColor = System.Drawing.Color.Black
                    Next
                    D = 0
                End If

            Next

        End If
        cn.Close()

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call PienaTabella()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Anno.Text = Year(Now)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        form1.Controls.Clear()
        form1.Controls.Add(DaGrid)

        form1.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class
