﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Regolebudget" CodeFile="Regolebudget.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Regole</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">    
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Budget/Analitica - Regole</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci" />
                            <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Budget.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Note Fatture" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Regole     
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" autocomplete="off" onkeypress="return handleEnterSoloNumero(this, event);" runat="server"
                                        Width="100px" MaxLength="4"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto Contabile:</label>
                                    <asp:TextBox ID="Txt_Conto" autocomplete="off" runat="server" Width="400px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Conto:</label>
                                    <asp:TextBox ID="TxT_Livello1" autocomplete="off" runat="server" Width="400px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Colonna:</label>
                                    <asp:DropDownList ID="DD_Colonna" runat="server" Width="261px"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Centro Servizio:</label>
                                    <asp:DropDownList ID="DD_CentroSerivizio" runat="server" Width="261px"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Importo Fisso:</label>
                                    <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Percentuale:</label>
                                    <asp:TextBox ID="Txt_Percentuale" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
