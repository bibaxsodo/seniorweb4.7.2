﻿Imports System.Data.OleDb

Public Class ClientiFornitori
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")

        Dim sb As StringBuilder = New StringBuilder

        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_GENERALE"))

        cnOspiti = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()

        cnOspiti.Open()

        Dim cmd As New OleDbCommand()
        'Dim I As Integer
        'Dim Numero As Integer
        Dim appoggio As String = ""
        Dim Corretto As Boolean = True

        Dim Valore1 As Long = 0
        Dim Valore2 As Long = 0
        Dim Valore3 As Double = 0


        appoggio = Replace(Replace(Replace(RICERCA, ",", ""), ".", ""), " ", "")
        If IsNumeric(appoggio) And Len(appoggio) >= 2 Then
            Dim Vettore(100) As String

            Vettore = SplitWords(RICERCA)

            If Vettore.Length >= 3 Then
                Valore1 = Val(Vettore(0))
                Valore2 = Val(Vettore(1))
                Valore3 = Val(Vettore(2))

                cmd.CommandText = ("select * from PianoConti where " &
                                        "Mastro = ? And Conto = ? And SottoConto = ? And (NonInUso = '' Or NonInUso is Null) ")

                cmd.Parameters.AddWithValue("@Mastro", Valore1)
                cmd.Parameters.AddWithValue("@Conto", Valore2)
                cmd.Parameters.AddWithValue("@Sottoconto", Valore3)
            End If
            If Vettore.Length = 2 Then
                Valore1 = Val(Vettore(0))
                Valore2 = Val(Vettore(1))
                Valore3 = 0

                cmd.CommandText = ("select * from PianoConti where " &
                                        "Mastro = ? And Conto = ? And (NonInUso = '' Or NonInUso is Null)  Order by Mastro,Conto,Sottoconto")

                cmd.Parameters.AddWithValue("@Mastro", Valore1)
                cmd.Parameters.AddWithValue("@Conto", Valore2)
            End If
            If Vettore.Length = 1 Then
                Valore1 = Val(Vettore(0))
                Valore2 = 0
                Valore3 = 0

                cmd.CommandText = ("select * from PianoConti where " &
                        "Mastro = ? And (NonInUso = '' Or NonInUso is Null)  Order by Mastro,Conto,Sottoconto")

                cmd.Parameters.AddWithValue("@Mastro", Valore1)
            End If

        Else

            cmd.CommandText = ("select * from PianoConti where " &
                                           "Descrizione Like ? And (NonInUso = '' Or NonInUso is Null)  Order by Descrizione")

            cmd.Parameters.AddWithValue("@Descrizione", "%" & RICERCA & "%")
        End If

        cmd.Connection = cn

        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim PartitaIVA As String = ""

            If campodb(myPOSTreader.Item("TipoAnagrafica")) = "D" Then
                Dim MCs As New Cls_ClienteFornitore

                MCs.Nome = ""
                MCs.MastroFornitore = Val(campodb(myPOSTreader.Item("MASTRO")))
                MCs.ContoFornitore = Val(campodb(myPOSTreader.Item("CONTO")))
                MCs.SottoContoFornitore = Val(campodb(myPOSTreader.Item("SOTTOCONTO")))
                MCs.Leggi(context.Session("DC_OSPITE"))

                Dim AppoggioPIVA As String = MCs.PARTITAIVA


                If MCs.Nome <> "" Then
                    If Val(AppoggioPIVA) > 0 Then
                        PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                    Else
                        PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                    End If
                Else
                    MCs.MastroFornitore = 0
                    MCs.ContoFornitore = 0
                    MCs.SottoContoFornitore = 0

                    MCs.MastroCliente = Val(campodb(myPOSTreader.Item("MASTRO")))
                    MCs.ContoCliente = Val(campodb(myPOSTreader.Item("CONTO")))
                    MCs.SottoContoCliente = Val(campodb(myPOSTreader.Item("SOTTOCONTO")))
                    MCs.Leggi(context.Session("DC_OSPITE"))

                    If Val(AppoggioPIVA) > 0 Then
                        PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                    Else
                        PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                    End If
                End If
            End If

            sb.Append(campodb(myPOSTreader.Item("MASTRO")) & " " & campodb(myPOSTreader.Item("CONTO")) & " " & campodb(myPOSTreader.Item("SOTTOCONTO")) & " " & campodb(myPOSTreader.Item("DESCRIZIONE")) & PartitaIVA).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 50 Then
                Exit Do
            End If
        Loop
        If Counter = 0 Then
            Dim cmdAnag As New OleDbCommand()
            If Val(RICERCA) > 0 Then
                cmdAnag.CommandText = ("select * from AnagraficaComune where " &
                                            " PARTITAIVA = ?  AND TIPOLOGIA IN ('C','R','D')")

                cmdAnag.Parameters.AddWithValue("@PARTITAIVA", Val(RICERCA))
            Else
                cmdAnag.CommandText = ("select * from AnagraficaComune where " &
                                            "CODICEFISCALE Like ? AND TIPOLOGIA IN ('C','R','D')")

                cmdAnag.Parameters.AddWithValue("@CODICEFISCALE", "%" & RICERCA & "%")
            End If
            cmdAnag.Connection = cnOspiti
            Dim RdAnag As OleDbDataReader = cmdAnag.ExecuteReader()
            If RdAnag.Read Then
                Dim PartitaIVAStr As String = ""

                If Val(campodb(RdAnag.Item("PARTITAIVA"))) > 0 Then
                    PartitaIVAStr = "(" & campodb(RdAnag.Item("PARTITAIVA")) & ")"
                Else
                    If campodb(RdAnag.Item("PARTITAIVA")) <> "" Then
                        PartitaIVAStr = "(" & campodb(RdAnag.Item("CODICEFISCALE")) & ")"
                    End If
                End If


                If Val(campodb(RdAnag.Item("MASTROCLIENTE"))) > 0 Then
                    sb.Append(campodb(RdAnag.Item("MASTROCLIENTE")) & " " & campodb(RdAnag.Item("ContoCliente")) & " " & campodb(RdAnag.Item("SottoContoCliente")) & " " & campodb(RdAnag.Item("NOME")) & " " & PartitaIVAStr).Append(Environment.NewLine)
                Else
                    sb.Append(campodb(RdAnag.Item("MastroFornitore")) & " " & campodb(RdAnag.Item("ContoFornitore")) & " " & campodb(RdAnag.Item("SottoContoFornitore")) & " " & campodb(RdAnag.Item("NOME")) & " " & PartitaIVAStr).Append(Environment.NewLine)
                End If
            End If
            RdAnag.Close()
        End If

        cnOspiti.Close()
        cn.Close()

        context.Response.Write(sb.ToString)
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class