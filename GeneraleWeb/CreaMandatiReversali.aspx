﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_CreaMandatiReversali" CodeFile="CreaMandatiReversali.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crea Mandati/Reversali</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">                     
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <div id="dialog-modal" title="Legami">
                <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>

                    <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Contabilità - Principale - Crea Mandati/Reversali</div>
                                <div class="SottoTitolo">
                                    <br />
                                    <br />
                                </div>
                            </td>

                            <td style="text-align: right; vertical-align: top;">
                                <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/goright.png" ToolTip="Modifica / Inserisci (F2)" />&nbsp;    
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Budget.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" alt="Menù" id="BOTTONEHOME" /></a><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />
                                <br />
                                <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');"><img src="images/arrow.gif" />Vis. Piano Conti</a></label>
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Mandati/Reversali Step 1/3
                                        </HeaderTemplate>
                                        <ContentTemplate>

                                            <label class="LabelCampo">Numero :</label>
                                            <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" ToolTip="prova" runat="server" Enabled="false" AutoPostBack="True" Width="104px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Data :</label>
                                            <asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" runat="server" Width="90px"></asp:TextBox>&nbsp;         
         <br />
                                            <br />
                                            <label class="LabelCampo">Causale Contabile :</label>
                                            <asp:DropDownList ID="Dd_CausaleContabile" AutoPostBack="True" runat="server" Width="408px"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Descrizione:</label>
                                            <asp:TextBox ID="Txt_Descrizione" autocomplete="off" MaxLength="250" runat="server" Height="72px" TextMode="MultiLine" Width="562px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_Dare" runat="server" Text="Dare"></asp:Label></label>
                                            <asp:TextBox ID="Txt_SottocontoDare" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_Avere" runat="server" Text="Avere"></asp:Label></label>
                                            <asp:TextBox ID="Txt_SottocontoAvere" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Importo : </label>
                                            <asp:TextBox ID="Txt_Importo" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
                                            <br />

                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </ContentTemplate>

                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
