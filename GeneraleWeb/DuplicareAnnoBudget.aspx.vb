﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_DuplicareAnnoBudget
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        Dim cn As New OleDbConnection
        Dim ImportoVariazioni As Double


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()




        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Budget Where Anno = " & Txt_AnnoDest.Text

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno destinazione già inserito');", True)
            Exit Sub
        End If
        myPOSTreader.Close()


      
        Dim cmdRd As New OleDbCommand

        cmdRd.CommandText = "Select * From Budget Where Anno = " & Txt_Anno.Text

        cmdRd.Connection = cn
        Dim MyRd As OleDbDataReader = cmdRd.ExecuteReader()

        Do While MyRd.Read
            Dim k As New Cls_Budget

            k.Anno = Val(Txt_AnnoDest.Text)


            ImportoVariazioni = 0
            Dim cmdRd1 As New OleDbCommand

            cmdRd1.CommandText = "Select sum(Importo) from VariazioniBudget where Anno = " & campodbn(MyRd.Item("Anno")) & " And Livello1 = " & campodbn(MyRd.Item("Livello1")) & " AND Livello2 = " & campodbn(MyRd.Item("Livello2")) & " AND Livello3 = " & campodbn(MyRd.Item("Livello3")) & " AND COLONNA = " & campodbn(MyRd.Item("Colonna"))

            cmdRd1.Connection = cn
            Dim MyRd1 As OleDbDataReader = cmdRd1.ExecuteReader()

            If MyRd1.Read Then
                ImportoVariazioni = campodbn(MyRd1.Item(0))
            End If
            MyRd1.Close()

            k.Importo = campodbn(MyRd.Item("Importo")) + ImportoVariazioni
            k.Livello1 = campodbn(MyRd.Item("Livello1"))
            k.Livello2 = campodbn(MyRd.Item("Livello2"))
            k.Livello3 = campodbn(MyRd.Item("Livello3"))
            k.Colonna = campodbn(MyRd.Item("Colonna"))
            k.Utente = "AUTOMATICO"
            k.Scrivi(Session("DC_GENERALE"))

        Loop
        MyRd.Close()



        Dim cmdRdS1 As New OleDbCommand

        cmdRdS1.CommandText = "Select * From TipoBudget Where Anno = " & Txt_Anno.Text

        cmdRdS1.Connection = cn
        Dim MyRds1 As OleDbDataReader = cmdRdS1.ExecuteReader()
        Do While MyRds1.Read
            Dim x As New Cls_TipoBudget


            x.Anno = Txt_AnnoDest.Text
            x.Livello1 = MyRds1.Item("Livello1")
            x.Livello2 = MyRds1.Item("Livello2")
            x.Livello3 = MyRds1.Item("Livello3")
            x.Tipo = MyRds1.Item("Tipo")
            x.Descrizione = MyRds1.Item("Descrizione")
            x.Scrivi(Session("DC_GENERALE"))
        Loop
        MyRds1.Close()


        Dim cmdRdS2 As New OleDbCommand

        cmdRdS2.CommandText = "Select * From ColonneBudget Where Anno = " & Txt_Anno.Text

        cmdRdS2.Connection = cn
        Dim MyRds2 As OleDbDataReader = cmdRdS1.ExecuteReader()
        Do While MyRds2.Read
            Dim x As New Cls_colonnebudget


            x.Anno = Txt_AnnoDest.Text
            x.Livello1 = MyRds2.Item("Livello1")
            x.Descrizione = MyRds2.Item("Descrizione")
            x.Scrivi(Session("DC_GENERALE"))
        Loop
        MyRds2.Close()


        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Operazione Completata');", True)


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub
End Class
