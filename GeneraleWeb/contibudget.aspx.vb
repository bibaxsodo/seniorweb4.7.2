﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_contibudget
    Inherits System.Web.UI.Page




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim x As New Cls_TipoBudget


        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno obbligatorio');", True)
            Exit Sub
        End If

        If Val(TxT_Livello1.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Primo livello obbligatorio');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If DD_Tipo.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il tipo del conto');", True)
            Exit Sub
        End If

     

        x.Anno = Txt_Anno.Text
        x.Livello1 = TxT_Livello1.Text
        x.Livello2 = TxT_Livello2.Text
        x.Livello3 = TxT_Livello3.Text
        x.Decodfica(Session("DC_GENERALE"))
        If x.Descrizione <> "" And Val(Request.Item("Livello1")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto già presente');", True)
            Exit Sub
        End If


        x.Livello1 = TxT_Livello1.Text
        x.Livello2 = TxT_Livello2.Text
        x.Livello3 = TxT_Livello3.Text
        x.Anno = Txt_Anno.Text
        x.Descrizione = Txt_Descrizione.Text
        x.Tipo = DD_Tipo.SelectedValue
        x.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("ElencoContiBudget.aspx")
    End Sub

    Private Sub Pulisci()
        Txt_Anno.Text = Year(Now)

        txt_livello1.Text = 0
        TxT_Livello2.Text = 0
        TxT_Livello3.Text = 0
        Txt_Descrizione.Text = ""
        DD_Tipo.SelectedValue = ""


    End Sub


    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        If Val(TxT_Livello1.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Primo livello obbligatorio');", True)
            Exit Sub
        End If
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select * from Budget Where Anno  = " & Val(Txt_Anno.Text) & _
                               "  And Livello1 = " & Val(TxT_Livello1.Text) & " And Livello2 = " & Val(TxT_Livello2.Text) & " And Livello3 = " & Val(TxT_Livello3.Text))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            myPOSTreader.Close()
            cn.Close()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare conto usato in bilancio');", True)
            Exit Sub
        End If
        myPOSTreader.Close()
        cn.close()


        Dim x As New Cls_TipoBudget

        x.Anno = Txt_Anno.Text
        x.Livello1 = TxT_Livello1.Text
        x.Livello2 = TxT_Livello2.Text
        x.Livello3 = TxT_Livello3.Text

        x.Elimina(Session("DC_GENERALE"))
        Response.Redirect("ElencoContiBudget.aspx")
    End Sub

    

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoContiBudget.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Anno.Text = Year(Now)
        TxT_Livello1.Text = 0
        TxT_Livello2.Text = 0
        TxT_Livello3.Text = 0

        If Val(Request.Item("Anno")) > 0 Then
            Txt_Anno.Text = Val(Request.Item("Anno"))
            TxT_Livello1.Text = Val(Request.Item("Livello1"))
            TxT_Livello2.Text = Val(Request.Item("Livello2"))
            TxT_Livello3.Text = Val(Request.Item("Livello3"))
            Dim ka As New Cls_TipoBudget

            ka.Anno = Txt_Anno.Text
            ka.Livello1 = TxT_Livello1.Text
            ka.Livello2 = TxT_Livello2.Text
            ka.Livello3 = TxT_Livello3.Text
            ka.Decodfica(Session("DC_GENERALE"))
            Txt_Descrizione.Text = ka.Descrizione
            DD_Tipo.SelectedValue = ka.Tipo


            Txt_Anno.Enabled = False
            TxT_Livello1.Enabled = False
            TxT_Livello2.Enabled = False
            TxT_Livello3.Enabled = False
        End If

    End Sub
End Class

