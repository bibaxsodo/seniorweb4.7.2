﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Menu_Stampe" CodeFile="Menu_Stampe.aspx.vb" %>

<head id="Head1" runat="server">
    <title>Menu Stampe Generale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Generale</div>
                        <div class="SottoTitolo">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Stampe<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        &nbsp;</td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">

                            <tr>
                                <td style="width: 20%; text-align: center;"><a href="StPianoconti.aspx">
                                    <img src="../images/Menu_PianoDeiConti.png" class="Effetto" alt="Piano Dei Conti" /></a></td>
                                <td style="width: 20%; text-align: center;"><a href="StampaPrimaNota.aspx">
                                    <img src="../images/Menu_StampaRegistriIVA.png" class="Effetto" alt="Prima Nota" /></a></td>
                                <td style="width: 20%; text-align: center;"><a href="mastrino.aspx">
                                    <img src="../images/Menu_Mastrino.png" class="Effetto" alt="Mastrino" /></a></td>
                                <td style="width: 20%; text-align: center;"><a href="Bilancio.aspx">
                                    <img src="../images/Menu_StampaBilancio.png" class="Effetto" alt="Stampa Bilancio" /></a></td>
                                <td style="width: 20%; text-align: center;"><a href="StampaGiornaleContabilita.aspx">
                                    <img src="../images/Menu_GiornaleContabilita.png" class="Effetto" alt="Periodo a Confronto" /></a></td>
                            </tr>

                            <tr>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">PIANO DEI CONTI</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">PRIMA NOTA</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">MASTRINO</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">BILANCIO</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">GIORNALE CONTABILITA</span></td>
                            </tr>


                            <tr>

                                <td style="width: 20%; text-align: center;"><a href="stamparegistriiva.aspx">
                                    <img src="../images/Menu_StampaRegistriIVA.png" class="Effetto" alt="Periodo Registro IVA" /></a></td>
                                <td style="width: 20%; text-align: center;"><a href="LiquidazioneIVA.aspx">
                                    <img src="../images/Menu_StampaLiquidazione.png" class="Effetto" alt="Liquidazione IVA" style="border-width: 0;"></a></td>
                                <td style="width: 20%; text-align: center;"><a href="BilancioRiclassificato.aspx">
                                    <img src="../images/Menu_Riclassificato.png" class="Effetto" alt="Riclassificato" style="border-width: 0;"></a></td>
                                <td style="width: 20%; text-align: center;"><a href="StampaClientiFornitori.aspx">
                                    <img src="../images/ClientiFornitoriStampa.jpg" class="Effetto" alt="Clienti/Fornitori" style="border-width: 0;"></a></td>
                                <td style="width: 20%; text-align: center;"><a href="StampaSaldi.aspx">
                                    <img src="../images/Menu_StampaRegistriIVA.png" class="Effetto" alt="Stampa Reversali" id="StampaReversaliID"></a></td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">REGISTRI IVA</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">LIQUIDAZIONE IVA</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">RICLASSIFICATO</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">STAMPA CLIENTI/FORNITORI</span></td>
                                <td style="width: 20%; text-align: center; vertical-align: top;"><span class="MenuText">SALDI</span></td>
                            </tr>



                            <tr>
                                <td style="width: 20%; text-align: center;"><a href="StampaReversali.aspx">
                                    <img src="../images/Menu_StampaReversali.png" class="Effetto" alt="Stampa Reversali" id="Img1"></a></td>
                                <td style="text-align: center;"><a href="StampaMandati.aspx">
                                    <img src="../images/Menu_StampaMandati.png" class="Effetto" alt="Stampa Mandati" id="StampaMandatiID"></a></td>
                                <td style="text-align: center;"><a href="StampaDistinte.aspx">
                                    <img src="../images/Menu_StampaDistinta.png" class="Effetto" alt="Stampa Distinta" id="StampaDistintaID"></a></td>
                                <td style="text-align: center;"><a href="StampaFatture.aspx">
                                    <img alt="Fatture" src="../images/Menu_StampaDocumenti.png" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center;"><a href="StampaCespiti.aspx">
                                    <img alt="Fatture" src="../images/CESPITI.png" class="Effetto" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">REVERSALI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MANDATI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DISTINTE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">FATTURE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CESPITI</span></td>
                            </tr>


                            <tr>
                                <td style="width: 20%; text-align: center;"><a href="StampaRateiRisconti.aspx">
                                    <img src="../images/Menu_StampaRateiRisconi.png" class="Effetto" alt="Stampa Ratei e Risconti"></a></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RATEI E RISCONTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
