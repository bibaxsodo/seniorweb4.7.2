﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_RitenuteModificaDocumenti" CodeFile="RitenuteModificaDocumenti.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%; text-align: right; vertical-align: top;">
                        <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />&nbsp;      
      <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci (F2)" />&nbsp;
      <asp:ImageButton ID="Btn_Elimina" Height="38px" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" />&nbsp;
      <asp:ImageButton ID="Btn_Pulisci" Height="38px" runat="server" BackColor="Transparent" ImageUrl="~/images/PULISCI.JPG" ToolTip="Pulisci (F8)" Visible="false" />&nbsp;           
                    </td>
                    <td style="width: 50%;"></td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <br />
                        <br />

                        <label class="LabelCampo">Documento :</label>
                        <asp:Label ID="Lbl_DatiDoc" runat="server" Text=""></asp:Label>
                        <br />
                        <br />

                        <label class="LabelCampo">Imponibile :</label>
                        <asp:TextBox ID="Txt_Imponibile" runat="server" Style="text-align: right;" AutoPostBack="True" Width="104px"></asp:TextBox>
                        <br />
                        <br />
                        <label class="LabelCampo">Ritenuta :</label>
                        <asp:DropDownList ID="DDRitenuta" AutoPostBack="true" OnSelectedIndexChanged="Dd_Ritenuta_SelectedIndexChanged" runat="server" Width="150px"></asp:DropDownList>
                        <br />
                        <br />

                        <label class="LabelCampo">Imposta :</label>
                        <asp:TextBox ID="Txt_Imposta" runat="server" Style="text-align: right;" AutoPostBack="True" Width="104px"></asp:TextBox>
                        <br />
                        <br />


                    </td>
                    <td style="width: 50%;">
                        <asp:Label ID="LblDati" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
