﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_gestionescadenzario" CodeFile="gestionescadenzario.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Prima Nota</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left; vertical-align: top;">

            <asp:Label ID="Lbl_DatiRegistrazione" runat="server" Text="Label"></asp:Label><br />
            <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                BorderStyle="Dotted" BorderWidth="1px">
                <RowStyle ForeColor="#333333" BackColor="White" />
                <Columns>
                    <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                    <asp:CommandField ButtonType="Image" EditImageUrl="~/images/modifica.png" ShowEditButton="True" CancelImageUrl="~/images/annulla.png" UpdateImageUrl="~/images/aggiorna.png" />



                    <asp:TemplateField HeaderText="Chiusa">
                        <ItemTemplate>
                            <asp:CheckBox ID="ChkChiusa" runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Data Scadenza">
                        <ItemTemplate>
                            <asp:TextBox ID="TxtDataScadenza" runat="server" Width="90px"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo">
                        <ItemTemplate>
                            <asp:TextBox ID="TxtImporto" Style="text-align: right;" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Descrizione">
                        <ItemTemplate>
                            <asp:TextBox ID="TxtDescrizione" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="RegistrazioneIncasso">
                        <ItemTemplate>
                            <asp:TextBox ID="TxtRegistrazioneIncasso" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Inserisci">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserimentoinriga.png" CommandName="Inserisci" CommandArgument="<%#   Container.DataItemIndex  %>" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <FooterStyle BackColor="White" ForeColor="#023102" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
            </asp:GridView>
            <br />
            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="48px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi"
                Width="48px" />
        </div>
    </form>
</body>
</html>
