﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_DocumentiJS
    Inherits System.Web.UI.Page
    Dim TabCR As New System.Data.DataTable("TabCostiRicavi")
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabIVA As New System.Data.DataTable("TabIVA")
    Dim TabRT As New System.Data.DataTable("TabRitenute")



    Function CampoConto(ByVal Valore As String, ByVal Numero As String) As String
        CampoConto = "<input type=""text"" name=""TxtConto" & Numero & """ tabindex=" & (Numero * 100) + 1 & " id=""TxtConto" & Numero & """  class=""form-control"" style=""text-align:left;"" placeholder=""Conto Costo/Ricavo"" title=""Conto Costo/Ricavo"" value=""" & Valore & """>"
    End Function


    Function CampoQuantita(ByVal Valore As String, ByVal Numero As String) As String
        CampoQuantita = "<input type=""text"" name=""TxtQuantita" & Numero & """  tabindex=" & (Numero * 100) + 2 & "  id=""TxtQuantita" & Numero & """  Class=""form-control""  style=""text-align:right;""  placeholder=""Quantita"" title=""Quantita""  autocomplete=""off"" value=""" & Valore & """>"
    End Function



    Function CampoDescrizione(ByVal Valore As String, ByVal Numero As String) As String
        CampoDescrizione = " <textarea class=""form-control""  tabindex=" & (Numero * 100) + 5 & "  style=""margin-top: 5px"" name=""Descrizione" & Numero & """ id=""Descrizione" & Numero & """  placeholder=""Descrizione"">" & Valore & "</textarea>"
    End Function


    Function CampoDareAvere(ByVal Valore As String, ByVal Numero As String) As String
        CampoDareAvere = "<div class=""input-group"">                            "
        CampoDareAvere = CampoDareAvere & "<div class=""bootstrap-select-wrapper"">  "
        CampoDareAvere = CampoDareAvere & "<select title=""Dare/Avere""  class=""form-control""  name=""TxtDareAvere" & Session("NumeroRiga") & """ tabindex=" & (Numero * 100) + 6 & " id=""TxtDareAvere" & Session("NumeroRiga") & """ >"

        If Valore = "D" Then
            CampoDareAvere = CampoDareAvere & "<option value=""D"" Selected>D</option>"
        Else
            CampoDareAvere = CampoDareAvere & "<option value=""D"">D</option>"
        End If
        If Valore = "A" Then
            CampoDareAvere = CampoDareAvere & "<option value=""A"" Selected>A</option>"
        Else
            CampoDareAvere = CampoDareAvere & "<option value=""A"">A</option>"
        End If
        CampoDareAvere = CampoDareAvere & "</select>"
        CampoDareAvere = CampoDareAvere & "</div>"
        CampoDareAvere = CampoDareAvere & "</div>"

    End Function

    Function CampoImporto(ByVal Valore As String, ByVal Numero As String) As String
        CampoImporto = "<input type=""text"" name=""TxtImporto" & Numero & """  tabindex=" & (Numero * 100) + 3 & "  id=""TxtImporto" & Numero & """   Class=""form-control"" placeholder=""Importo""  title=""Importo""  style=""text-align:right;"" autocomplete=""off""  value=""" & Valore & """>"
    End Function

    Function CampoIVA(ByVal Valore As String, ByVal Numero As String) As String

        CampoIVA = "<div class=""input-group"">                            "
        CampoIVA = CampoIVA & "<div class=""bootstrap-select-wrapper"">  "
        CampoIVA = CampoIVA & "<select title=""IVA"" class=""form-control""  tabindex=" & (Numero * 100) + 4 & "  name=""TxtIVA" & Numero & """ id=""TxtIVA" & Numero & """ >"


        CampoIVA = CampoIVA & ComboIVA(Session("DC_TABELLE"), Valore)


        CampoIVA = CampoIVA & "</select>"
        CampoIVA = CampoIVA & "</div>"
        CampoIVA = CampoIVA & "</div>"
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function ComboIVA(ByVal StringaConnessione As String, ByVal Valore As String) As String
        Dim cn As OleDbConnection


        ComboIVA = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA Where NonInUso Is Null Or NonInUso = 0 Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Seleziona As String = ""
            If campodb(myPOSTreader.Item("Codice")) = Valore Then
                Seleziona = "Selected"
            End If
            ComboIVA = ComboIVA & "<option value=""" & campodb(myPOSTreader.Item("Codice")) & """  " & Seleziona & ">" & campodb(myPOSTreader.Item("Descrizione")) & "</option>"
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Valore = "" Then
            ComboIVA = ComboIVA & "<option value="""" Selected></option>"
        End If

        Return ComboIVA
    End Function


    Private Sub CaricaTableCR()
        Lbl_Righe.Text = ""
        Session("NumeroRiga") = 0
        TabCR = ViewState("TabCR")
        For i = 0 To TabCR.Rows.Count - 1

            If i = TabCR.Rows.Count - 1 And TabCR.Rows.Count - 1 >= 0 Then
                AddRow(1, campodb(TabCR.Rows(i).Item("Sottoconto")), campodb(TabCR.Rows(i).Item("Descrizione")), campodb(TabCR.Rows(i).Item("Quantita")), campodb(TabCR.Rows(i).Item("DareAvere")), campodb(TabCR.Rows(i).Item("Importo")), campodb(TabCR.Rows(i).Item("IVA")), campodb(TabCR.Rows(i).Item("SottocontoControPartita")), campodb(TabCR.Rows(i).Item("CentroServizio")), campodb(TabCR.Rows(i).Item("RigaDaCausale")), campodb(TabCR.Rows(i).Item("Anno")), campodb(TabCR.Rows(i).Item("Mese")), campodb(TabCR.Rows(i).Item("TipoExtra")))
            Else
                AddRow(0, campodb(TabCR.Rows(i).Item("Sottoconto")), campodb(TabCR.Rows(i).Item("Descrizione")), campodb(TabCR.Rows(i).Item("Quantita")), campodb(TabCR.Rows(i).Item("DareAvere")), campodb(TabCR.Rows(i).Item("Importo")), campodb(TabCR.Rows(i).Item("IVA")), campodb(TabCR.Rows(i).Item("SottocontoControPartita")), campodb(TabCR.Rows(i).Item("CentroServizio")), campodb(TabCR.Rows(i).Item("RigaDaCausale")), campodb(TabCR.Rows(i).Item("Anno")), campodb(TabCR.Rows(i).Item("Mese")), campodb(TabCR.Rows(i).Item("TipoExtra")))
            End If
        Next
    End Sub


    Private Sub leggiregistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        If Val(Request.Item("NUMEROREGISTRAZIONETEMP")) > 0 Then
            x.Leggi(ConnectionStringGenerale, Txt_Numero.Text, True)
        Else
            x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)
        End If

        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_DataRegistrazione.Text = x.DataRegistrazione

        If x.BolloVirtuale = 1 Then
            Chk_BolloVirtuale.Checked = True
        Else
            Chk_BolloVirtuale.Checked = False
        End If

        Try
            DDCausaliContabili.SelectedValue = x.CausaleContabile
        Catch ex As Exception
            DDCausaliContabili.Items(DDCausaliContabili.Items.Count - 1).Selected = False
            DDCausaliContabili.Items.Add("ERRORE")
            DDCausaliContabili.Items(DDCausaliContabili.Items.Count - 1).Value = "ERRORE"
            DDCausaliContabili.Items(DDCausaliContabili.Items.Count - 1).Selected = True
        End Try


        Txt_Numero.Enabled = False
        Txt_DataDocumento.Text = x.DataDocumento
        Txt_NumeroDocumento.Text = x.NumeroDocumento

        Txt_AnnoProtcollo.Text = x.AnnoProtocollo
        Txt_NumeroProtocollo.Text = x.NumeroProtocollo
        Txt_EstensionePRotocollo.Text = x.Bis

        'Txt_DescrizioneTesta.Text = x.Descrizione


        ''LblUltimaModifica.Text = ""

        'Dim cnUltimaModifica As OleDbConnection

        'cnUltimaModifica = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        'cnUltimaModifica.Open()

        'Dim cmdUltimaModifica As New OleDbCommand()


        'cmdUltimaModifica.CommandText = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & x.NumeroRegistrazione


        'cmdUltimaModifica.Connection = cnUltimaModifica


        'Dim DataUltimaModifica As OleDbDataReader = cmdUltimaModifica.ExecuteReader()
        'If DataUltimaModifica.Read Then
        '    LblUltimaModifica.Text = "Ultima modifica " & Format(campodbd(DataUltimaModifica.Item("DataAggiornamento")), "dd/MM/yyyy HH:mm:ss")
        'End If


        'Dim cmdFatturaElettronica As New OleDbCommand

        'cmdFatturaElettronica.CommandText = "Select * From FatturaElettronica Where NumeroRegistrazione = " & x.NumeroRegistrazione & " Order by DataOra desc"


        'cmdFatturaElettronica.Connection = cnUltimaModifica

        'Dim RdFatturaElettronica As OleDbDataReader = cmdFatturaElettronica.ExecuteReader()
        'If RdFatturaElettronica.Read Then
        '    LblUltimaModifica.Text = LblUltimaModifica.Text & "<br><font color=""red"">Fatture Elettronica Progressivo :" & campodbN(RdFatturaElettronica.Item("Progressivo")) & "</font>"
        'End If
        'RdFatturaElettronica.Close()

        'cnUltimaModifica.Close()




        Try
            DD_ModalitaPagamento.SelectedValue = x.CodicePagamento
        Catch ex As Exception
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Selected = False
            DD_ModalitaPagamento.Items.Add("ERRORE")
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Value = "ERRORE"
            DD_ModalitaPagamento.Items(DD_ModalitaPagamento.Items.Count - 1).Selected = True
        End Try


        'DD_ModalitaPagamento.SelectedValue = x.ModalitaPagamento
        If x.IVASospesa = "S" Then
            RB_IvaSospesaSI.Checked = True
            RB_IvaSospesaNO.Checked = False
        Else
            RB_IvaSospesaNO.Checked = True
            RB_IvaSospesaSI.Checked = False
        End If
        For i = 0 To x.Righe.Length - 1
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).RigaDaCausale = 1 Then
                    Dim xc As New Cls_Pianodeiconti
                    Dim PartitaIVA As String = ""

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)


                    If xc.TipoAnagrafica = "D" Then
                        Dim MCs As New Cls_ClienteFornitore

                        MCs.Nome = ""
                        MCs.MastroFornitore = xc.Mastro
                        MCs.ContoFornitore = xc.Conto
                        MCs.SottoContoFornitore = xc.Sottoconto
                        MCs.Leggi(Session("DC_OSPITE"))
                        If MCs.Nome <> "" Then
                            If MCs.PARTITAIVA > 0 Then
                                PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                            Else
                                PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                            End If
                        Else

                            MCs.MastroFornitore = 0
                            MCs.ContoFornitore = 0
                            MCs.SottoContoFornitore = 0
                            MCs.MastroCliente = xc.Mastro
                            MCs.ContoCliente = xc.Conto
                            MCs.SottoContoCliente = xc.Sottoconto
                            MCs.Leggi(Session("DC_OSPITE"))

                            If MCs.PARTITAIVA > 0 Then
                                PartitaIVA = " (" & MCs.PARTITAIVA & ")"
                            Else
                                PartitaIVA = " (" & MCs.CodiceFiscale & ")"
                            End If
                        End If
                    End If
                    Txt_ClienteFornitore.Text = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione & PartitaIVA
                End If
            End If
        Next

        Dim Xl As New Cls_CausaleContabile

        Xl.Leggi(Session("DC_TABELLE"), DDCausaliContabili.SelectedValue)

        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), Xl.RegistroIVA)

        TxtRegistroIVA.Text = "(" & DecReg.Descrizione & ")"

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))



        For i = 0 To x.Righe.Length
            If Not IsNothing(x.Righe(i)) Then
                If x.Righe(i).Tipo <> "IV" And x.Righe(i).Tipo <> "RI" And x.Righe(i).RigaDaCausale <> 1 Then
                    Dim myriga As System.Data.DataRow = TabCR.NewRow()
                    Dim xc As New Cls_Pianodeiconti

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)
                    myriga(0) = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione
                    myriga(1) = x.Righe(i).DareAvere
                    myriga(2) = x.Righe(i).Prorata
                    myriga(3) = x.Righe(i).Quantita
                    myriga(4) = Format(x.Righe(i).Importo, "#,##0.00")
                    myriga(5) = x.Righe(i).CodiceIVA

                    Dim xc1 As New Cls_Pianodeiconti

                    xc1.Mastro = x.Righe(i).MastroContropartita
                    xc1.Conto = x.Righe(i).ContoContropartita
                    xc1.Sottoconto = x.Righe(i).SottocontoContropartita

                    xc1.Decodfica(ConnectionStringGenerale)
                    myriga(6) = xc1.Mastro & " " & xc1.Conto & " " & xc1.Sottoconto & " " & xc1.Descrizione

                    myriga(7) = x.Righe(i).Descrizione
                    myriga(8) = x.Righe(i).AnnoRiferimento
                    myriga(9) = x.Righe(i).MeseRiferimento

                    myriga(10) = x.Righe(i).RigaRegistrazione
                    myriga(11) = x.Righe(i).RigaDaCausale
                    myriga(12) = x.Righe(i).CentroServizio
                    myriga(13) = x.Righe(i).TipoExtra


                    TabCR.Rows.Add(myriga)
                End If
            Else
                Exit For
            End If
        Next
        Session("TABELLACR") = TabCR

  
        Dim MyCau As New Cls_CausaleContabile

        MyCau.Leggi(Session("DC_TABELLE"), DDCausaliContabili.SelectedValue)

        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date = Txt_DataRegistrazione.Text
        RegIva.Tipo = MyCau.RegistroIVA
        RegIva.Anno = Year(DataAppo)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)
        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then
            Txt_DataRegistrazione.Enabled = False
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');"">Legami</a></label>"
        End If

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))


        If DatiGenerali.BudgetAnalitica = 0 Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazionePrenotazione.aspx?Numero=" & Txt_Numero.Text & "');"">Legame Prenot.</a></label>"
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Budget</a></label>"
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Analitica</a></label>"
        End If
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('RateiRisconti.aspx?Numero=" & Txt_Numero.Text & "');"">Ratei Risconti</a></label>"
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('ReverseCharge.aspx?Numero=" & Txt_Numero.Text & "');"">Rev.Charge/Split</a></label>"

        If MyCau.VenditaAcquisti = "V" Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Maillinglist.aspx?Numero=" & Txt_Numero.Text & "');"">Mailing List</a></label>"
        End If

        Lbl_BtnScadenzario.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');"">Scadenzario</a></label>"

        'GrigliaProcedureAbilitate()

        If Request.Item("DUPLICA") = "SI" Then
            Txt_Numero.Text = 0
            Session("NumeroRegistrazione") = 0
            x.NumeroRegistrazione = 0
        End If


        Session("Registrazione") = x

        If Val(Txt_Numero.Text) > 0 Then

            Try

                Dim cn As OleDbConnection


                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cn.Open()

                Dim cmdLeggi As New OleDbCommand()


                cmdLeggi.CommandText = ("Select * From Log_MailInviate Where NumeroRegistrazione = ? ")
                cmdLeggi.Parameters.AddWithValue("@NumeroRegistrazione", Val(Txt_Numero.Text))
                cmdLeggi.Connection = cn
                Dim K As OleDbDataReader = cmdLeggi.ExecuteReader

                If K.Read Then
                    Lbl_BtnLegami.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Maillinglist.aspx?Numero=" & Txt_Numero.Text & "');"">Mail</a></label>" & Lbl_BtnLegami.Text
                End If


                cn.Close()
            Catch ex As Exception

            End Try

        End If



        CaricaTableCR()
    End Sub




    Private Sub UpTableCR()
        Dim i As Integer

        TabCR.Clear()
        TabCR.Columns.Clear()
        TabCR.Columns.Add("Sottoconto", GetType(String))
        TabCR.Columns.Add("DareAvere", GetType(String))
        TabCR.Columns.Add("Prorata", GetType(String))
        TabCR.Columns.Add("Quantita", GetType(String))
        TabCR.Columns.Add("Importo", GetType(String))
        TabCR.Columns.Add("IVA", GetType(String))
        TabCR.Columns.Add("SottocontoControPartita", GetType(String))
        TabCR.Columns.Add("Descrizione", GetType(String))
        TabCR.Columns.Add("Anno", GetType(Long))
        TabCR.Columns.Add("Mese", GetType(Long))
        TabCR.Columns.Add("RigaRegistrazione", GetType(Long))
        TabCR.Columns.Add("RigaDaCausale", GetType(Long))
        TabCR.Columns.Add("CentroServizio", GetType(String))
        TabCR.Columns.Add("TipoExtra", GetType(String))


        For i = 1 To Session("NumeroRiga")

            Dim myriga1 As System.Data.DataRow = TabCR.NewRow()

            myriga1(0) = Request.Item("TxtConto" & i)

            myriga1(1) = Request.Item("TxtDareAvere" & i)
            

            myriga1(2) = "N" 'prorata?


            myriga1(3) = Request.Item("TxtQuantita" & i)
            myriga1(4) = Request.Item("TxtImporto" & i)
            myriga1(5) = Request.Item("TxtIVA" & i)
            myriga1(6) = Request.Item("TxtControPartita" & i)
            myriga1(7) = Request.Item("Descrizione" & i)
            myriga1(8) = Val(Request.Item("TxtAnno" & i))
            myriga1(9) = Val(Request.Item("TxtMese" & i))
            myriga1(10) = 0 'riga registrazione ?
            myriga1(11) = Val(Request.Item("TxtRigaDaCausale" & i))
            myriga1(12) = Request.Item("TxtCserv" & i)
            myriga1(13) = Request.Item("TxtTipoExtra" & i)
            TabCR.Rows.Add(myriga1)

        Next

        ViewState("TabCR") = TabCR

    End Sub


    Function CampoControPartita(ByVal Valore As String, ByVal Numero As String) As String
        CampoControPartita = "<input type=""text"" name=""TxtControPartita" & Numero & """ id=""TxtControPartita" & Numero & """  class=""form-control"" style=""text-align:left;"" placeholder=""ControPartita"" title=""ControPartita""   value=""" & Valore & """>"
    End Function

    Public Function ComboCserv(ByVal StringaConnessione As String, ByVal Valore As String) As String
        Dim cn As OleDbConnection


        ComboCserv = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from TABELLACENTROSERVIZIO Order by Descrizione"
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Seleziona As String = ""
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = Valore Then
                Seleziona = "Selected"
            End If
            ComboCserv = ComboCserv & "<option value=""" & campodb(myPOSTreader.Item("CENTROSERVIZIO")) & """  " & Seleziona & ">" & campodb(myPOSTreader.Item("Descrizione")) & "</option>"
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Valore = "" Then
            ComboCserv = ComboCserv & "<option value="""" Selected></option>"
        End If

        Return ComboCserv
    End Function

    Function CampoCserv(ByVal Valore As String, ByVal Numero As String) As String
        CampoCserv = "                            <div class=""bootstrap-select-wrapper"">  "
        CampoCserv = CampoCserv & "							  <select title=""Centro Servizio"" class=""form-control"" name=""TxtCserv" & Numero & """ id=""TxtCserv" & Numero & """ >"
        CampoCserv = CampoCserv & ComboCserv(Session("DC_OSPITE"), Valore)
        CampoCserv = CampoCserv & "							  </select>"
        CampoCserv = CampoCserv & "							</div>"
        CampoCserv = CampoCserv & "                        </div>"

    End Function



    Function CampoRigaDaCausale(ByVal Valore As String, ByVal Numero As String) As String
        CampoRigaDaCausale = "<input type=""text"" name=""TxtRigaDaCausale" & Numero & """ id=""TxtRigaDaCausale" & Numero & """  Class=""form-control""  style=""text-align:right;""  placeholder=""Riga Causale"" title=""Riga Causale"" autocomplete=""off"" value=""" & Valore & """ >"
    End Function

    Function CampoAnnoCompetenza(ByVal Valore As String, ByVal Numero As String) As String
        CampoAnnoCompetenza = "<input type=""text"" name=""TxtAnno" & Numero & """ id=""TxtAnno" & Numero & """  Class=""form-control""  style=""text-align:right;width:112px;  ""  placeholder=""Anno Competenza Riga"" title=""Anno Competenza Riga"" autocomplete=""off"" value=""" & Valore & """ >"
    End Function


    Function CampoMeseCompetenza(ByVal Valore As String, ByVal Numero As String) As String

        Dim Selezionato(12) As String

        Selezionato(Val(Valore)) = "Selected"

        CampoMeseCompetenza = "<div class=""input-group"">                            "
        CampoMeseCompetenza = CampoMeseCompetenza & "<div class=""bootstrap-select-wrapper"">  "
        CampoMeseCompetenza = CampoMeseCompetenza & "<select title=""Mese Competenza Riga"" class=""form-control"" name=""TxtMese" & Numero & """ id=""TxtMese" & Numero & """ >"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""1"" " & Selezionato(1) & ">Gennaio</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""2"" " & Selezionato(2) & ">Febbraio</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""3"" " & Selezionato(3) & ">Marzo</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""4"" " & Selezionato(4) & ">Aprile</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""5"" " & Selezionato(5) & ">Maggio</option>"

        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""6"" " & Selezionato(6) & ">Giugno</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""7"" " & Selezionato(7) & ">Luglio</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""8"" " & Selezionato(8) & ">Agosto</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""9"" " & Selezionato(9) & ">Settembre</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""10"" " & Selezionato(10) & ">Ottobre</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""11"" " & Selezionato(11) & ">Novembre</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "<option value=""12"" " & Selezionato(12) & ">Dicembre</option>"
        CampoMeseCompetenza = CampoMeseCompetenza & "</select>"
        CampoMeseCompetenza = CampoMeseCompetenza & "</div>"
        CampoMeseCompetenza = CampoMeseCompetenza & "</div>"

    End Function

    '<input type=""text"" name=""TxtAnno" & Session("NumeroRiga") & """ id=""TxtAnno" & Session("NumeroRiga") & """  Class=""form-control""  style=""text-align:right;""  placeholder=""Anno Competenza Riga"" autocomplete=""off"" >"

    Function CampoTipoExtra(ByVal Valore As String, ByVal Numero As String) As String
        CampoTipoExtra = "<input type=""text"" name=""TxtTipoExtra" & Numero & """ id=""TxtTipoExtra" & Numero & """ Class=""form-control""  placeholder=""Tipo Riga"" title=""Tipo Riga""  autocomplete=""off"" value=""" & Valore & """>"
    End Function


    Private Sub AddRow(ByVal AddButton As Integer, ByVal Conto As String, ByVal Descrizione As String, ByVal Quantita As String, ByVal DareAvere As String, ByVal Importo As String, ByVal IVA As String, ByVal ControPartita As String, ByVal Cserv As String, ByVal RigaDaCausale As String, ByVal AnnoCompetenza As String, ByVal MeseCompetenza As String, ByVal TipoExtra As String)

        Dim Numero As Integer
        Session("NumeroRiga") = Val(Session("NumeroRiga")) + 1

        Numero = Session("NumeroRiga")



        Dim Riga As String

        Riga = "                <div class=""row items-pnl-body"" id=""item-row"">"
        Riga = Riga & "                    <div class=""col-sm-1 col"" >"
        Riga = Riga & "                        <p>"
        REM If Numero > 1 Then
        Riga = Riga & "                            <a href=""#"" onclick=""cancellariga(" & Numero & ");""><img src=""../images/cancella.png"" /></a>"
        REM End If
        Riga = Riga & "                            <br />"
        Riga = Riga & "                            <br />"
        If AddButton = 1 Then
            Riga = Riga & "<div  style=""text-align:left; width:100%; height:25px;text-align: center;""  id=""menutastodestro""  >"
            Riga = Riga & "                            <a href=""#"" tabindex=" & (Numero * 100) + 7 & "  onclick=""inserisciriga();""><img src=""../images/inserisci.png"" /></a>"
            Riga = Riga & "</div>"
        End If
        Riga = Riga & "                        </p>"
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-6 col extendable "">                        "
        Riga = Riga & CampoConto(Conto, Numero)
        Riga = Riga & CampoDescrizione(Descrizione, Numero)
        Riga = Riga & "                         <a href=""#"" onclick=""openriga2(" & Session("NumeroRiga") & ");"" >....</a>"
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-1 col"" >"

        Riga = Riga & CampoQuantita(Quantita, Numero)

        Riga = Riga & CampoDareAvere(DareAvere, Numero)




        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-2 col"">"
        Riga = Riga & "                        <div class=""input-group"">"
        Riga = Riga & "                            <div class=""input-group-addon currenty"">€</div>"
        Riga = Riga & CampoImporto(Importo, Numero)
        Riga = Riga & "                        </div>"
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-2 col taxCol"" >"

        Riga = Riga & CampoIVA(IVA, Numero)
        Riga = Riga & "                    </div>"
        Riga = Riga & "                </div>"

        Riga = Riga & "                <div class=""row items-pnl-body"" id=""item-row_aggiuntiva" & Session("NumeroRiga") & """ style=""display:none;"">"
        Riga = Riga & "                    <div class=""col-sm-1 col"" >"
        Riga = Riga & "                    </div>"

        Riga = Riga & "                    <div class=""col-sm-6 col extendable "">         "
        Riga = Riga & CampoControPartita(ControPartita, Numero)
        Riga = Riga & "                        <div class=""input-group"">                            "
        Riga = Riga & CampoCserv(Cserv, Numero)
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-1 col"" >"
        Riga = Riga & CampoRigaDaCausale(RigaDaCausale, Numero)
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-2 col"">"
        Riga = Riga & CampoAnnoCompetenza(AnnoCompetenza, Numero)
        Riga = Riga & CampoMeseCompetenza(MeseCompetenza, Numero)
        Riga = Riga & "                    </div>"
        Riga = Riga & "                    <div class=""col-sm-2 col taxCol"" >"
        Riga = Riga & CampoTipoExtra(TipoExtra, Numero)
        Riga = Riga & "                    </div>"


        Riga = Riga & "                </div>"


        Lbl_Righe.Text = Lbl_Righe.Text & Riga

    End Sub

    Private Sub EseguiJS(Optional ByVal MClient As Boolean = False)
        Dim MyJs As String

        MyJs = ""

        MyJs = MyJs & "$(document).ready(function() {" & vbNewLine

        If Request.Item("NONMENU") = "OK" Then
            MyJs = MyJs & "$('.destraclasse').css('display','none');"
            MyJs = MyJs & "$('.Barra').css('display','none');"
            MyJs = MyJs & "$('.DivTasti').css( ""top"", ""50px"" );"

        End If

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ClienteFornitore.ClientID & Chr(34) & ").autocomplete(""ClientiFornitori.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "


        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"


        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('TxtConto')!= null) || (appoggio.match('TxtControPartita')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null)  ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap);  });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); " & vbNewLine


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDocumento.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDocumento.ClientID & Chr(34) & ").mask(""99/99/9999""); " & vbNewLine


        MyJs = MyJs & " });"

        MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
        MyJs = MyJs & "                $(""#menutastodestro"").bind(""contextmenu"", function(event) {" & vbNewLine
        MyJs = MyJs & "                   event.preventDefault();" & vbNewLine
        MyJs = MyJs & "                    $(""#rightmenu"").toggle(100)." & vbNewLine
        MyJs = MyJs & "            css({" & vbNewLine
        MyJs = MyJs & "                top: event.pageY + ""px""," & vbNewLine
        MyJs = MyJs & "                left: event.pageX + ""px""" & vbNewLine
        MyJs = MyJs & "            });" & vbNewLine
        MyJs = MyJs & "        });" & vbNewLine
        MyJs = MyJs & "        });" & vbNewLine


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSGriglie", MyJs, True)

        If DDCausaliContabili.Text <> "" Then
            Dim Cau As New Cls_CausaleContabile

            Cau.Leggi(Session("DC_TABELLE"), DDCausaliContabili.SelectedValue)
            MyJs = ""
            If Cau.Tipo = "I" Then
                MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
                MyJs = MyJs & "        $(""#rightmenu"").html('<a href=""javascript:inserimentorighesupplementari(5);"" id=""A1"" ><li data-action=""first"">Scadenza Collaboratori</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserimentorighesupplementari(7);"" id=""A2"" ><li data-action=""first"">Abbuoni/Sconti</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserimentorighesupplementari(8);"" id=""A4"" ><li data-action=""first"">Arrotondamenti</li></a>');" & vbNewLine
                MyJs = MyJs & "        });" & vbNewLine
            End If
            If Cau.Tipo = "R" Then
                MyJs = MyJs & "       $(document).ready(function() {" & vbNewLine
                MyJs = MyJs & "        $(""#rightmenu"").html('<a href=""javascript:inserimentorighesupplementari(4);"" id=""A1"" ><li data-action=""first"">Extra fissi</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserimentorighesupplementari(5);"" id=""A2"" ><li data-action=""first"">Extra variabili</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserimentorighesupplementari(6);"" id=""A3"" ><li data-action=""first"">Accrediti</li></a>"
                MyJs = MyJs & "        <a href=""javascript:inserimentorighesupplementari(9);"" id=""A4"" ><li data-action=""first"">Bollo</li></a>');" & vbNewLine
                MyJs = MyJs & "        });" & vbNewLine
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSTastoDestro", MyJs, True)
        End If



        'MyJs = MyJs & "$("".chosen-select"").chosen({"
        'MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        'MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        'MyJs = MyJs & "width: ""350px"","
        'MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        'MyJs = MyJs & "});"





    End Sub


    Private Sub CaricaDatiDaStep2()
        Dim I As Integer

        TabCR = Session("TABELLACR")

        For i = TabCR.Rows.Count - 1 To 0 Step -1

            If TabCR.Rows(i).Item("RigaDaCausale") = 99 Then
                TabCR.Rows.RemoveAt(i)
            End If
        Next


        ViewState("TabCR") = TabCR

        CaricaTableCR()


        Dim Registrazione As New Cls_MovimentoContabile


        If Not IsNothing(Session("Registrazione")) Then

            Registrazione = Session("Registrazione")
        End If

        DDCausaliContabili.SelectedValue = Registrazione.CausaleContabile
        DD_ModalitaPagamento.SelectedValue = Registrazione.ModalitaPagamento
        Txt_NumeroDocumento.Text = Registrazione.NumeroDocumento
        Txt_DataDocumento.Text = Registrazione.DataDocumento
        Txt_DataRegistrazione.Text = Registrazione.DataRegistrazione

        If Registrazione.IVASospesa = "S" Then
            RB_IvaSospesaSI.Checked = True
            RB_IvaSospesaNO.Checked = False
        Else
            RB_IvaSospesaSI.Checked = False
            RB_IvaSospesaNO.Checked = True
        End If


        If Registrazione.BolloVirtuale = 1 Then
            Chk_BolloVirtuale.Checked = True
        Else
            Chk_BolloVirtuale.Checked = False
        End If

        Txt_AnnoProtcollo.Text = Registrazione.AnnoProtocollo
        Txt_NumeroPRotocollo.Text = Registrazione.NumeroProtocollo
        Txt_EstensionePRotocollo.Text=Registrazione.Bis

        Txt_ClienteFornitore.Text = Session("ClienteFornitore")

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Public Sub ModificaFornitore()

        UpTableCR()
        If Val(Txt_Numero.Text) = 0 Then
            Dim ImportoRiga1 As Double
            Try
                ImportoRiga1 = CDbl(TabCR.Rows(0).Item("Importo"))
            Catch ex As Exception

            End Try

            If ImportoRiga1 = 0 Then
                Dim k As New Cls_ClienteFornitore
                Dim Mastro As Long
                Dim Conto As Long
                Dim Sottoconto As Long
                Dim Vettore(100) As String
                Dim Appoggio1 As String = ""
                Dim Appoggio2 As String = ""

                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                    k.MastroCliente = Mastro
                    k.ContoCliente = Conto
                    k.SottoContoCliente = Sottoconto
                    k.Leggi(Session("DC_OSPITE"))
                    If k.Nome = "" Then
                        k.MastroCliente = 0
                        k.ContoCliente = 0
                        k.SottoContoCliente = 0
                        k.MastroFornitore = Mastro
                        k.ContoFornitore = Conto
                        k.SottoContoFornitore = Sottoconto


                        k.Leggi(Session("DC_OSPITE"))
                        Tabella = ViewState("TABELLACR")

                        If DD_ModalitaPagamento.SelectedValue = "" Then
                            DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamentoFornitore
                        End If
                        Dim xS As New Cls_Pianodeiconti

                        xS.Mastro = k.MastroCosto
                        xS.Conto = k.ContoCosto
                        xS.Sottoconto = k.SottocontoCosto

                        If k.MastroCosto <> 0 Then
                            xS.Decodfica(Session("DC_GENERALE"))
                            TabCR.Rows(0).Item(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                            TabCR.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                        Else
                            TabCR.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                        End If

                    Else
                        Dim kas As New Cls_CausaleContabile

                        kas.Codice = DDCausaliContabili.SelectedValue
                        kas.Leggi(Session("DC_TABELLE"), kas.Codice)

                        If DD_ModalitaPagamento.SelectedValue = "" Then
                            Try
                                DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento
                            Catch ex As Exception

                            End Try
                        End If
                        Dim xS As New Cls_Pianodeiconti
                        xS.Mastro = k.MastroRicavo
                        xS.Conto = k.ContoRicavo
                        xS.Sottoconto = k.SottocontoRicavo
                        If k.MastroRicavo <> 0 Then
                            xS.Decodfica(Session("DC_GENERALE"))
                            TabCR.Rows(0).Item(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                            TabCR.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                        Else
                            TabCR.Rows(0).Item(6) = Txt_ClienteFornitore.Text
                        End If
                    End If
                End If

            End If

        End If



        Call EseguiJS()


    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub        
        End If


        If Request("__EVENTTARGET") = "BtnCancellaRiga" Then
            Dim parameter As String = Request("__EVENTARGUMENT")

            CancellaRiga(Val(parameter))

        End If

        If Request("__EVENTTARGET") = "ProseguiRegistrazione" Then

            ProseguiRegistrazione()

        End If


        If Request("__EVENTTARGET") = "ModificaFornitore" Then

            ModificaFornitore()

        End If

        If Request("__EVENTTARGET") = "InserisciRigheExtra" Then
            Dim parameter As String = Request("__EVENTARGUMENT")


            InserisciRiga(Val(parameter))
        End If

        'ModificaFornitore

        UpTableCR()

        Call EseguiJS()
        If Page.IsPostBack = True Then Exit Sub



        If Val(Request.Item("NumeroRegistrazione")) > 0 Then
            Session("NumeroRegistrazione") = Val(Request.Item("NumeroRegistrazione"))
        End If

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, Nothing)


        Lbl_Righe.Text = ""

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxDoc(Session("DC_TABELLE"), DDCausaliContabili)

        Dim ModalitaPagamento As New Cls_TipoPagamento


        ModalitaPagamento.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)


        Session("NumeroRiga") = 0


        AddRow(1, "", "", "", "D", "", "", "", "", "", "", "", "")
        UpTableCR()


        If Request.Item("CHIAMATA") = "RITORNO" Then
            CaricaDatiDaStep2()
        End If


        Call EseguiJS()



        RB_IvaSospesaNO.Checked = False
        RB_IvaSospesaNO.Checked = True


        If Val(Session("NumeroRegistrazione")) > 0 Then
            Dim xDoc As New Cls_MovimentoContabile
            xDoc.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))

            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), xDoc.CausaleContabile)


            If xDoc.NumeroRegistrazione > 0 Then
                Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
                Call leggiregistrazione()
            End If
        End If

        If Request.Item("NUMEROREGISTRAZIONETEMP") > 0 Then
            Dim xDoc As New Cls_MovimentoContabile
            xDoc.Leggi(Session("DC_GENERALE"), Val(Request.Item("NUMEROREGISTRAZIONETEMP")), True)

            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), xDoc.CausaleContabile)


            If xDoc.NumeroRegistrazione > 0 Then
                Txt_Numero.Text = Request.Item("NUMEROREGISTRAZIONETEMP")
                Call leggiregistrazione()

                Txt_Numero.Text = 0
                Exit Sub
            End If
        End If

        If Request.Item("Data") <> "" Then
            Dim DataCopia As String
            DataCopia = Request.Item("Data") & "00000000"

            Try
                Txt_DataRegistrazione.Text = Format(DateSerial(Val(Mid(DataCopia, 1, 4)), Val(Mid(DataCopia, 5, 2)), Val(Mid(DataCopia, 7, 2))), "dd/MM/yyyy")

                DDCausaliContabili.SelectedValue = Request.Item("CausaleContabile")
            Catch ex As Exception

            End Try
        End If

        Call EseguiJS()

    End Sub

    Protected Sub BtnInserisci_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInserisci.Click
        InserisciRiga()
    End Sub





    Private Sub CancellaRiga(ByVal Numero As Integer)

        UpTableCR()
        ViewState("TabCR") = TabCR

        If Not (Numero = TabCR.Rows.Count And Numero = 1) Then
            Try
                TabCR.Rows.RemoveAt(Numero - 1)
            Catch ex As Exception

            End Try


        End If

        CaricaTableCR()

        EseguiJS()
    End Sub


    Private Sub InserisciRiga(Optional ByVal ValRigaDaCausale As Integer = 0)
        Dim Indice As Integer

        UpTableCR()
        ViewState("TabCR") = TabCR

        Dim myriga As System.Data.DataRow = TabCR.NewRow()

        Dim Ks As New Cls_CausaleContabile
        Ks.Leggi(Session("DC_TABELLE"), DDCausaliContabili.SelectedValue)

        If Ks.Tipo = "R" Then
            Indice = 2
        Else
            Indice = 1
        End If
        If ValRigaDaCausale > 0 Then
            If Not IsNothing(Ks.Righe(ValRigaDaCausale - 1)) Then
                Indice = ValRigaDaCausale - 1
            End If
        End If

        Dim xS As New Cls_Pianodeiconti

        xS.Mastro = Ks.Righe(Indice).Mastro
        xS.Conto = Ks.Righe(Indice).Conto
        xS.Sottoconto = Ks.Righe(Indice).Sottoconto

        xS.Decodfica(Session("DC_GENERALE"))

        myriga(0) = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

        myriga(1) = Ks.Righe(Indice).DareAvere

        myriga(3) = 0
        myriga(4) = 0

        myriga("RigaDaCausale") = ValRigaDaCausale
        If ValRigaDaCausale = 0 Then
            Dim myriga1 As System.Data.DataRow = TabCR.NewRow()

            Dim k As New Cls_ClienteFornitore
            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Dim Vettore(100) As String
            Dim Appoggio1 As String = ""
            Dim Appoggio2 As String = ""

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
                k.MastroCliente = Mastro
                k.ContoCliente = Conto
                k.SottoContoCliente = Sottoconto
                k.Leggi(Session("DC_OSPITE"))
                If k.Nome = "" Then
                    k.MastroCliente = 0
                    k.ContoCliente = 0
                    k.SottoContoCliente = 0
                    k.MastroFornitore = Mastro
                    k.ContoFornitore = Conto
                    k.SottoContoFornitore = Sottoconto


                    k.Leggi(Session("DC_OSPITE"))
                    Tabella = ViewState("TABELLACR")

                    If DD_ModalitaPagamento.SelectedValue = "" Then
                        DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamentoFornitore
                    End If
                    Dim xPdc As New Cls_Pianodeiconti

                    xPdc.Mastro = k.MastroCosto
                    xPdc.Conto = k.ContoCosto
                    xPdc.Sottoconto = k.SottocontoCosto

                    If k.MastroCosto <> 0 Then
                        xPdc.Decodfica(Session("DC_GENERALE"))
                        myriga1(0) = xPdc.Mastro & " " & xPdc.Conto & " " & xPdc.Sottoconto & " " & xPdc.Descrizione
                        myriga1(6) = Txt_ClienteFornitore.Text
                    Else
                        myriga1(6) = Txt_ClienteFornitore.Text
                    End If

                Else
                    Dim kas As New Cls_CausaleContabile

                    kas.Codice = DDCausaliContabili.SelectedValue
                    kas.Leggi(Session("DC_TABELLE"), kas.Codice)

                    If DD_ModalitaPagamento.SelectedValue = "" Then
                        Try
                            DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento
                        Catch ex As Exception

                        End Try
                    End If
                    Dim xPdc As New Cls_Pianodeiconti
                    xPdc.Mastro = k.MastroRicavo
                    xPdc.Conto = k.ContoRicavo
                    xPdc.Sottoconto = k.SottocontoRicavo
                    If k.MastroRicavo <> 0 Then
                        xPdc.Decodfica(Session("DC_GENERALE"))
                        myriga1(0) = xPdc.Mastro & " " & xPdc.Conto & " " & xS.Sottoconto & " " & xPdc.Descrizione
                        myriga1(6) = Txt_ClienteFornitore.Text
                    Else
                        myriga1(6) = Txt_ClienteFornitore.Text
                    End If
                End If
            End If

        End If

        myriga(6) = Txt_ClienteFornitore.Text
        TabCR.Rows.Add(myriga)
        CaricaTableCR()

        EseguiJS()
    End Sub





    Protected Sub ProseguiRegistrazione()
        UpTableCR()

        If Trim(DDCausaliContabili.SelectedValue) = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Specifica causale contabile');", True)
            Call EseguiJS()
            Call CaricaTableCR()
            Exit Sub
        End If

        If Val(Txt_AnnoProtcollo.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Specifica anno protocollo');", True)
            Call CaricaTableCR()
            Call EseguiJS()
            Exit Sub
        End If


        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Call CaricaTableCR()
            Call EseguiJS()
            Exit Sub
        End If

        Dim MS As New Cls_CausaleContabile

        MS.Codice = DDCausaliContabili.SelectedValue
        MS.Leggi(Session("DC_TABELLE"), MS.Codice)

        If MS.DataObbligatoria = "S" Then
            If Not IsDate(Txt_DataDocumento.Text) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Data Documento formalmente errata');", True)
                REM Lbl_errori.Text = "Data Documento formalmente errata"
                Call EseguiJS()
                Exit Sub
            End If

            Dim DataRegistrazione As Date

            Try
                DataRegistrazione = Txt_DataRegistrazione.Text
            Catch ex As Exception
                DataRegistrazione = Now
            End Try


            If Year(Txt_DataDocumento.Text) < DataRegistrazione.Year - 2 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Indicare una corretta data documento');", True)
                REM Lbl_errori.Text = "Data Documento formalmente errata"
                Call EseguiJS()
                Call CaricaTableCR()
                Exit Sub
            End If
        Else
            Txt_DataDocumento.Text = Txt_DataRegistrazione.Text
        End If



        If MS.VenditaAcquisti = "A" Then
            Dim DataDocumentoTs As Date = Txt_DataDocumento.Text
            Dim DataRegistrazioneTs As Date = Txt_DataRegistrazione.Text

            If Format(DataDocumentoTs, "yyyyMMdd") > Format(DataRegistrazioneTs, "yyyyMMdd") Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Documento > Data Registrazione');", True)
                Call EseguiJS()
                Call CaricaTableCR()
                Exit Sub
            End If
        End If


        TabCR = ViewState("TabCR")

        If Val(Txt_Numero.Text) = 0 Then
            For i = 0 To TabCR.Rows.Count - 1

                If TabCR.Rows(i).Item("IVA") = "" Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Specifica codice iva');", True)
                    Call EseguiJS()

                    Call CaricaTableCR()
                    Exit Sub
                End If
            Next
        End If

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If DatiGenerali.ModalitaPagamentoOblDoc = 1 Then
            If DD_ModalitaPagamento.SelectedValue = "" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<br/>Modalità pagamento obbligatoria');", True)
                Call EseguiJS()

                Call CaricaTableCR()
                Exit Sub
            End If

        End If




        Session("TABELLACR") = ViewState("TabCR")

        Dim Registrazione As New Cls_MovimentoContabile

        If Not IsNothing(Session("Registrazione")) Then

            Registrazione = Session("Registrazione")
        End If


        Registrazione.NumeroRegistrazione = Val(Txt_Numero.Text)
        
        Registrazione.CausaleContabile = DDCausaliContabili.SelectedValue
        Registrazione.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue
        Registrazione.NumeroDocumento = Txt_NumeroDocumento.Text

        If Txt_DataDocumento.Text <> "" Then
            Registrazione.DataDocumento = Txt_DataDocumento.Text
        End If

        Registrazione.DataRegistrazione = Txt_DataRegistrazione.Text

        If RB_IvaSospesaNO.Checked = True Then
            Registrazione.IVASospesa = "N"
        End If
        If RB_IvaSospesaSI.Checked = True Then
            Registrazione.IVASospesa = "S"
        End If


        If Chk_BolloVirtuale.Checked = True Then
            Registrazione.BolloVirtuale = 1
        Else
            Registrazione.BolloVirtuale = 0
        End If

        Registrazione.AnnoProtocollo = Val(Txt_AnnoProtcollo.Text)
        Registrazione.NumeroProtocollo = Val(Txt_NumeroPRotocollo.Text)
        Registrazione.Bis = Txt_EstensionePRotocollo.Text



        Session("Registrazione") = Registrazione

        Session("ClienteFornitore") = Txt_ClienteFornitore.Text

        If Request.Item("NONMENU") = "OK" Then
            Response.Redirect("DocumentiJS_Step2.aspx?NONMENU=OK")
        Else
            Response.Redirect("DocumentiJS_Step2.aspx")
        End If
    End Sub


    Protected Sub DDCausaliContabili_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCausaliContabili.SelectedIndexChanged
        Dim CausaleContabile As New Cls_CausaleContabile


        CausaleContabile.Leggi(Session("DC_TABELLE"), DDCausaliContabili.SelectedValue)

        Dim RegistroIVA As New Cls_RegistroIVA

        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)


        TxtRegistroIVA.text = RegistroIVA.Descrizione

        Call EseguiJS()


        Dim Indice As Long
        
        If CausaleContabile.Tipo = "R" Then
            Indice = 3
        Else
            Indice = 1
        End If
        UpTableCR()

        Dim Importo As Double
        Try
            Importo = TabCR.Rows(0).Item("Importo")
        Catch ex As Exception

        End Try

        If Importo = 0 Then
            Dim xS As New Cls_Pianodeiconti
            Try
                xS.Mastro = CausaleContabile.Righe(Indice).Mastro
                xS.Conto = CausaleContabile.Righe(Indice).Conto
                xS.Sottoconto = CausaleContabile.Righe(Indice).Sottoconto

                xS.Decodfica(Session("DC_GENERALE"))
            Catch ex As Exception

            End Try
            TabCR.Rows(0).Item("Sottoconto") = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            TabCR.Rows(0).Item("DareAvere") = CausaleContabile.Righe(Indice).DareAvere

       

            TabCR.Rows(0).Item("RigaRegistrazione") = 1
            'RigaRegistrazione
        End If

        ViewState("TabCR") = TabCR
        CaricaTableCR()

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("Chiamante") = "PrimaNota" Then

            Dim Data As Date = Now


            Try
                Data = Txt_DataRegistrazione.Text
            Catch ex As Exception

            End Try

            Response.Redirect("primanota.aspx?Chiamante=PrimaNote&Data=" & Format(Data, "yyyyMMdd"))
        End If

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("UltimiMovimenti.aspx?TIPO=DOC")
        Else
            Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=DOC")
        End If


    End Sub


End Class
