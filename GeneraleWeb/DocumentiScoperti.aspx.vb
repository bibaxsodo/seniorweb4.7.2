﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_DocumentiScoperti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    'CommandArgument= <%#  Eval("NumeroRegistrazione") %>


    Private Sub CaricaGriglia(ByVal ToExcel As Boolean)
        Dim Condizione As String

        Condizione = ""
        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)

            If Vettore.Length > 0 Then
                If Val(Vettore(0)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MastroPartita = " & Val(Vettore(0))
                End If
            End If
            If Vettore.Length > 1 Then
                If Val(Vettore(1)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " ContoPartita = " & Val(Vettore(1))
                End If
            End If            

            If Vettore.Length > 2 Then
                If Val(Vettore(2)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " SottoContoPartita = " & Val(Vettore(2))
                End If
            End If
        End If

        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Sottoconto", GetType(Long))
        Tabella.Columns.Add("RagioneSociale", GetType(String))
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))

        Tabella.Columns.Add("NumeroProtocollo", GetType(Long))
        Tabella.Columns.Add("AnnoProtocollo", GetType(Long))
        Tabella.Columns.Add("CausaleContabile", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo Documento", GetType(Double))
        Tabella.Columns.Add("Importo Pagato", GetType(Double))
        Tabella.Columns.Add("Disponibile", GetType(Double))
        Tabella.Columns.Add("CentroServiizo", GetType(String))

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim OldRegistrazione As Long
        Dim MySql As String

        MySql = "SELECT MovimentiContabiliTesta_1.*, MovimentiContabiliRiga.* FROM MovimentiContabiliTesta AS MovimentiContabiliTesta_1 INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta_1.NumeroRegistrazione = MovimentiContabiliRiga.Numero WHERE (((MovimentiContabiliTesta_1.RegistroIVA)>=1)) AND TIPO = 'CF' AND " & Condizione
        cmd.CommandText = (MySql)
        cmd.Connection = cn

        If IsDate(Txt_DataDal.Text) Then
            Dim Txt_DataDalText As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim Txt_DataAlText As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        End If

        OldRegistrazione = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim K As New Cls_MovimentoContabile
            Dim x As New Cls_Legami

            K.Leggi(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione"))

            If DD_ModalitaPagamento.SelectedValue = "" Or (DD_ModalitaPagamento.SelectedValue = K.ModalitaPagamento) Then

                If Math.Abs(Math.Round(K.ImportoDocumento(Session("DC_TABELLE")), 2)) > Math.Abs(Math.Round(x.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), 2)) Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(3) = myPOSTreader.Item("NumeroRegistrazione")
                    If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                        myriga(4) = ""
                    Else
                        myriga(4) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
                    End If
                    myriga(5) = myPOSTreader.Item("NumeroDocumento")
                    If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                        myriga(6) = ""
                    Else
                        myriga(6) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
                    End If
                    myriga(7) = myPOSTreader.Item("NumeroProtocollo")
                    myriga(8) = myPOSTreader.Item("AnnoProtocollo")
                    Dim dC As New Cls_CausaleContabile

                    dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
                    myriga(9) = dC.Descrizione


                    Dim xp As New Cls_MovimentoContabile


                    xp.Leggi(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione"))

                    Dim PnConti As New Cls_Pianodeiconti

                    PnConti.Mastro = xp.Righe(0).MastroPartita
                    PnConti.Conto = xp.Righe(0).ContoPartita
                    PnConti.Sottoconto = xp.Righe(0).SottocontoPartita
                    PnConti.Decodfica(Session("DC_GENERALE"))
                    myriga(2) = PnConti.Descrizione


                    PnConti.Mastro = xp.Righe(0).MastroPartita
                    PnConti.Conto = xp.Righe(0).ContoPartita
                    PnConti.Sottoconto = 0
                    PnConti.Decodfica(Session("DC_GENERALE"))
                    myriga(0) = PnConti.Descrizione

                    myriga(1) = xp.Righe(0).SottocontoPartita


                    myriga(10) = xp.Descrizione

                    myriga(11) = Format(xp.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")

                    myriga(14) = xp.CentroServizio

                    Dim lM As New Cls_Legami

                    myriga(12) = Format(lM.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), "#,##0.00")


                    myriga(13) = Format(xp.ImportoDocumento(Session("DC_TABELLE")) - lM.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), "#,##0.00")
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        If DD_OrdinamentoRagioneSociale.Checked = True Then
            Tabella.DefaultView.Sort = "RagioneSociale"
        Else
            Tabella.DefaultView.Sort = "Sottoconto"
        End If


        Session("DocumentScoperti") = Tabella

        If ToExcel = False Then
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.Font.Size = 10
            Grid.DataBind()
        Else
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.Font.Size = 10
            GridView1.DataBind()
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        CaricaGriglia(False)

        Call EseguiJS()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand


        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/Documenti.aspx');  });", True)
                REM Response.Redirect("../GeneraleWeb/Documenti.aspx")
                Exit Sub
            End If
            If CauCon.Tipo = "P" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/incassipagamenti.aspx');  });", True)
                REM Response.Redirect("../GeneraleWeb/incassipagamenti.aspx")
                Exit Sub
            End If
            Session("NumeroRegistrazione") = Registrazione
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/primanota.aspx');  });", True)
            REM Response.Redirect("../GeneraleWeb/primanota.aspx")
            Exit Sub
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True THEN Exit Sub 

        Dim x As New Cls_TipoPagamento

        x.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call EseguiJS()
        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        CaricaGriglia(False)

        Session("GrigliaSoloStampa") = Session("DocumentScoperti")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)

        Call EseguiJS()
    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub EseguiJS(Optional ByVal MClient As Boolean = False)
        Dim MyJs As String
        'MyJs = "alert('r');"
        MyJs = ""
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Sottoconto')!= null)) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        'MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSGriglie", MyJs, True)

    End Sub
End Class
