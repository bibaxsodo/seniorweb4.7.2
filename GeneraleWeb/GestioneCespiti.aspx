﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_GestioneCespiti" CodeFile="GestioneCespiti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Cespiti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {

            var tot = 0;
            if (document.body.offsetWidth > 1280) {
                tot = document.body.offsetWidth / 3;
            } else {
                tot = document.body.offsetWidth / 2;
            }

            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Cespiti - Nuovo Cespite</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Inserisci/Modifica (F2)" />
                            <asp:ImageButton ID="ImageButton1" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBox('MovimentiCespiti.aspx');">Movimenti Cespiti</a></label>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Note Fatture" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Cespite     
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">Codice : </label>
                                    <asp:TextBox ID="Txt_ID" runat="server" Width="51px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Descrizione : </label>
                                    <asp:TextBox ID="Txt_Descrizione" MaxLength="50" runat="server" Width="541px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Ubicazione : </label>
                                    <asp:TextBox ID="Txt_Ubicazione" MaxLength="50" runat="server" Width="341px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Categoria : </label>
                                    <asp:DropDownList ID="DD_Categoria" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Percentuale : </label>
                                    <asp:DropDownList ID="DD_Percentuale" runat="server"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Importo Base : </label>
                                    <asp:TextBox ID="Txt_ImportoBase" MaxLength="50" runat="server" Width="100px" Style="text-align: right;"></asp:TextBox>
                                    <hr />
                                    <center>Documento Acquisto</center>
                                    <br />
                                    <label class="LabelCampo">Numero Acq : </label>
                                    <asp:TextBox ID="Txt_NumeroAcq" AutoPostBack="true" MaxLength="50" runat="server" Width="80px"></asp:TextBox>
                                    <asp:Label ID="LblDocumentoAcq" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Anno Acq :</label>
                                    <asp:TextBox ID="Txt_AnnoAcq" MaxLength="4" runat="server" Width="80px"></asp:TextBox>
                                    Descrizione Acquisto/Fornitore :
           <asp:TextBox ID="Txt_DescrizioneAcq" MaxLength="50" runat="server" Width="341px"></asp:TextBox>
                                    Importo :
           <asp:TextBox ID="Txt_ImportoAcq" MaxLength="50" runat="server" Width="100px" Style="text-align: right;"></asp:TextBox>
                                    <br />
                                    <hr />
                                    <center>Documento Dismissione/Vendita</center>
                                    <br />
                                    <label class="LabelCampo">Numero Vnd : </label>
                                    <asp:TextBox ID="Txt_NumeroVnd" AutoPostBack="true" MaxLength="50" runat="server" Width="80px"></asp:TextBox>
                                    <asp:Label ID="LblDocumentoVnd" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Anno Vnd :</label>
                                    <asp:TextBox ID="Txt_AnnoVnd" MaxLength="4" runat="server" Width="80px"></asp:TextBox>
                                    Descrizione Vendita/Cliente :
           <asp:TextBox ID="Txt_DescrizioneVnd" MaxLength="50" runat="server" Width="341px"></asp:TextBox>
                                    Importo :
           <asp:TextBox ID="Txt_ImportoVnd" MaxLength="50" runat="server" Width="100px" Style="text-align: right;"></asp:TextBox>
                                    <br />
                                    <br />
                                    <hr />
                                    <label class="LabelCampo">Dimesso : </label>
                                    <asp:CheckBox ID="ChkDimesso" runat="server" Text="" AutoPostBack="true" />
                                    Data Dimissione :
           <asp:TextBox ID="Txt_DataDimissione" MaxLength="10" runat="server" Width="100px"></asp:TextBox>
                                    Descrizione :
           <asp:TextBox ID="Txt_DescrizioneDimissione" MaxLength="50" runat="server" Width="300px"></asp:TextBox>
                                    <asp:Button ID="Btn_CreaDismissione" runat="server" Text="Crea Dismissione" Height="30px" />
                                    <br />

                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>
