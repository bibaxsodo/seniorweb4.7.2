﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="GeneraRegistrazioniDaDistinta.aspx.vb" Inherits="GeneraRegistrazioniDaDistinta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Genera Registrazione da Distinta</title>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready( function() {
            if (window.innerHeight>0) { $("#BarraLaterale").css("height",(window.innerHeight - 105) + "px"); } else
                    { $("#BarraLaterale").css("height",(document.documentElement.offsetHeight - 105) + "px");  }
                });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }

        .confermamodifca {
            -webkit-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            -moz-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            background-color: #82807d;
            text-align: center;
            color: White;
            z-index: 100;
        }
    </style>
    <script type="text/javascript"> 
     function soloNumeri(evt) {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
       }
       return true;
     }
            
    
     function DialogBox(Path)
        {         
        
        var winW = 630, winH = 460;
        if (document.body && document.body.offsetWidth) {
         winW = document.body.offsetWidth;
         winH = document.body.offsetHeight;
        }
        if (document.compatMode=='CSS1Compat' &&
            document.documentElement &&
            document.documentElement.offsetWidth ) {
         winW = document.documentElement.offsetWidth;
         winH = document.documentElement.offsetHeight;
        }
        if (window.innerWidth && window.innerHeight) {
         winW = window.innerWidth;
         winH = window.innerHeight;
        }
        
          var APPO = winH - 100;
          
          REDIPS.dialog.show(winW - 200, winH  -100, '<iframe id="output" src="' + Path + '" height="' +  APPO + '" width="100%" ></iframe>');
          return false;        
            
        }        

        function ChiudiConfermaModifica() {
            $("#ConfermaModifica").remove();
            $("#LblBox").empty();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
        <asp:Label ID="LblBox" runat="server" Text=""></asp:Label>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Contabilità - Strumenti - Genera Registrazione da Distinta<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Img_Modifica" runat="server" Height="38px"
                                ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Img_Etrai" runat="server" Height="38px"
                                ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Genera Registrazione da Distinta
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Data Dal:</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data Al:</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data:</label>
                                    <asp:TextBox ID="Txt_Data" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data:</label>
                                    <asp:DropDownList ID="DD_CausaleContabile" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />


                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="384px"></asp:Label><br />
                                    <br />
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grid" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Seleziona">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NumeroDistinta" HeaderText="Numero Distinta" />
                                            <asp:BoundField DataField="DataDistinta" HeaderText="Data Distinta" />
                                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                                            <asp:BoundField DataField="Importo" HeaderText="Importo" />
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                    </asp:GridView>
                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                        <br />
                    </td>
                </tr>
            </table>

            <asp:GridView ID="GridView1" runat="server" Height="70px" Width="760px">
            </asp:GridView>
        </div>
    </form>
</body>
</html>
