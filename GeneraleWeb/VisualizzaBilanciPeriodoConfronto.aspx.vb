﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_VisualizzaBilanciPeriodoConfronto
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function TotaleDare(ByVal Mastro As Long, ByVal Conto As Long, ByVal SottoConto As Long, ByVal DataElabDa As Date, ByVal DataElabAl As Date) As Double
        Dim cn As OleDbConnection
        Dim Condizione As String
        Dim MySql As String
        Dim W_CausaleChiusura As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Condizione = ""
        If Chk_SenzaMovimentiChiusura.Checked = True Then
            Dim cnT As OleDbConnection
            cnT = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

            cnT.Open()

            MySql = "SELECT * From DatiGenerali"

            Dim cmdX As New OleDbCommand()
            cmdX.CommandText = (MySql)
            cmdX.Connection = cnT
            Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
            If ReadDX.Read Then
                W_CausaleChiusura = campodb(ReadDX.Item("CausaleChiusura"))
            End If
            ReadDX.Close()
            cnT.Close()


            Condizione = Condizione & " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        End If
        TotaleDare = 0

        Dim cmdD As New OleDbCommand()

        If Chk_UsaDataDocumento.Checked = False Then
            MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                  " INNER JOIN MovimentiContabiliRiga " & _
                  " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                  " WHERE MastroPartita = " & Mastro & _
                  " AND ContoPartita = " & Conto & _
                  " AND SottocontoPartita = " & SottoConto & _
                  " AND DareAvere = 'D' " & _
                  " AND DataRegistrazione >= ? " & _
                  " AND DataRegistrazione <= ? " & Condizione

            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
        Else
            MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                  " INNER JOIN MovimentiContabiliRiga " & _
                  " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                  " WHERE MastroPartita = " & Mastro & _
                  " AND ContoPartita = " & Conto & _
                  " AND SottocontoPartita = " & SottoConto & _
                  " AND DareAvere = 'D' " & _
                  " AND ((DataRegistrazione >= ? AND DataRegistrazione <= ? And  DataDocumento IS NULL) OR " & _
                  " (DataDocumento >= ? AND DataDocumento <= ?)) " & _
                  Condizione


            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
        End If


        cmdD.CommandText = (MySql)
        cmdD.Connection = cn
        

        Dim ReadD As OleDbDataReader = cmdD.ExecuteReader()
        If ReadD.Read Then
            TotaleDare = Math.Round(campodbN(ReadD.Item("Totale")), 2)
        End If
        ReadD.Close()

        cn.Close()
    End Function

    Private Function TotaleAvere(ByVal Mastro As Long, ByVal Conto As Long, ByVal SottoConto As Long, ByVal DataElabDa As Date, ByVal DataElabAl As Date) As Double
        Dim cn As OleDbConnection
        Dim Condizione As String
        Dim MySql As String
        Dim W_CausaleChiusura As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Condizione = ""
        If Chk_SenzaMovimentiChiusura.Checked = True Then

            Dim cnT As OleDbConnection
            cnT = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

            cnT.Open()

            MySql = "SELECT * From DatiGenerali"

            Dim cmdX As New OleDbCommand()   
            cmdX.CommandText = (MySql)
            cmdX.Connection = cnT
            Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
            If ReadDX.Read Then                
                W_CausaleChiusura = campodb(ReadDX.Item("CausaleChiusura"))
            End If
            ReadDX.Close()
            cnT.Close()

            Condizione = Condizione & " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        End If
        TotaleAvere = 0

        Dim cmdD As New OleDbCommand()
        If Chk_UsaDataDocumento.Checked = False Then
            MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & SottoConto & _
                    " AND DareAvere = 'A' " & _
                    " AND DataRegistrazione >= ? " & _
                    " AND DataRegistrazione <= ? " & Condizione

            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
        Else
            MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & SottoConto & _
                    " AND DareAvere = 'A' " & _
                    " AND ((DataRegistrazione >= ? AND DataRegistrazione <= ? And  DataDocumento IS NULL) OR " & _
                    " (DataDocumento >= ? AND DataDocumento <= ?)) " & _
                    Condizione
            ' (DataRegistrazione >= '02/04/2007') AND (DataRegistrazione <= '02/04/2007') AND (DataDocumento IS NULL) OR
            '(DataDocumento >= '02/04/2007') AND (DataDocumento <= '02/04/2007')



            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
            cmdD.Parameters.AddWithValue("@DataDal", DataElabDa)
            cmdD.Parameters.AddWithValue("@DataAl", DataElabAl)
        End If

        cmdD.CommandText = (MySql)
        cmdD.Connection = cn
        Dim ReadD As OleDbDataReader = cmdD.ExecuteReader()
        If ReadD.Read Then
            TotaleAvere = Math.Round(campodbN(ReadD.Item("Totale")), 2)
        End If
        ReadD.Close()

        cn.Close()
    End Function
    Private Sub EstraiBilancio(ByVal ToExcel As Boolean)
        Dim MySql As String

        Dim Totalizzatore1 As Double = 0
        Dim Totalizzatore2 As Double = 0
        Dim Totalizzatore3 As Double = 0
        Dim Totalizzatore4 As Double = 0
        Dim Totalizzatore5 As Double = 0

        Dim mTotalizzatore1 As Double = 0
        Dim mTotalizzatore2 As Double = 0
        Dim mTotalizzatore3 As Double = 0
        Dim mTotalizzatore4 As Double = 0
        Dim mTotalizzatore5 As Double = 0

        Dim OldMastro As Long = -1
        Dim OldConto As Long = -1
        Dim OldSottoConto As Long = -1

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Mastro", GetType(String))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("SottoConto", GetType(String))
        Tabella.Columns.Add("Decodifica Conto", GetType(String))
        Tabella.Columns.Add("Saldo1", GetType(String))
        Tabella.Columns.Add("Saldo2", GetType(String))
        Tabella.Columns.Add("Saldo3", GetType(String))
        Tabella.Columns.Add("Saldo4", GetType(String))
        Tabella.Columns.Add("Saldo5", GetType(String))






        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        MySql = "SELECT * FROM PianoConti WHERE SottoConto <> 0 And Tipo = 'C' ORDER BY Mastro, Conto, Sottoconto"

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim ReadMastro As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadMastro.Read

            If OldConto <> campodbN(ReadMastro.Item("Conto")) And OldConto <> -1 Then

                Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
                myriga1(0) = OldMastro
                myriga1(1) = OldConto
                myriga1(2) = 0

                Dim k1 As New Cls_Pianodeiconti

                k1.Mastro = OldMastro
                k1.Conto = OldConto
                k1.Sottoconto = 0
                k1.Decodfica(Session("DC_GENERALE"))
                myriga1(3) = k1.Descrizione

                myriga1(4) = Format(Totalizzatore1, "#,##0.00")
                myriga1(5) = Format(Totalizzatore2, "#,##0.00")
                myriga1(6) = Format(Totalizzatore3, "#,##0.00")
                myriga1(7) = Format(Totalizzatore4, "#,##0.00")
                myriga1(8) = Format(Totalizzatore5, "#,##0.00")

                Tabella.Rows.Add(myriga1)

                Totalizzatore1 = 0
                Totalizzatore2 = 0
                Totalizzatore3 = 0
                Totalizzatore4 = 0
                Totalizzatore5 = 0
            End If

            If OldMastro <> campodbN(ReadMastro.Item("Mastro")) And OldSottoConto <> -1 Then

                Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
                myriga1(0) = OldMastro
                myriga1(1) = 0
                myriga1(2) = 0

                Dim k1 As New Cls_Pianodeiconti

                k1.Mastro = OldMastro
                k1.Conto = 0
                k1.Sottoconto = 0
                k1.Decodfica(Session("DC_GENERALE"))
                myriga1(3) = k1.Descrizione

                myriga1(4) = Format(mTotalizzatore1, "#,##0.00")
                myriga1(5) = Format(mTotalizzatore2, "#,##0.00")
                myriga1(6) = Format(mTotalizzatore3, "#,##0.00")
                myriga1(7) = Format(mTotalizzatore4, "#,##0.00")
                myriga1(8) = Format(mTotalizzatore5, "#,##0.00")

                Tabella.Rows.Add(myriga1)

                mTotalizzatore1 = 0
                mTotalizzatore2 = 0
                mTotalizzatore3 = 0
                mTotalizzatore4 = 0
                mTotalizzatore5 = 0
            End If

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbN(ReadMastro.Item("Mastro"))
            myriga(1) = campodbN(ReadMastro.Item("Conto"))
            myriga(2) = campodbN(ReadMastro.Item("SottoConto"))

            Dim k As New Cls_Pianodeiconti

            k.Mastro = campodbN(ReadMastro.Item("Mastro"))
            k.Conto = campodbN(ReadMastro.Item("Conto"))
            k.Sottoconto = campodbN(ReadMastro.Item("SottoConto"))
            k.Decodfica(Session("DC_GENERALE"))
            myriga(3) = k.Descrizione

            OldMastro = k.Mastro
            OldConto = k.Conto
            OldSottoConto = k.Sottoconto

            myriga(4) = Format(TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal1.Text, Txt_DataAl1.Text) - TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal1.Text, Txt_DataAl1.Text), "#,##0.00")

            If IsDate(Txt_DataDal2.Text) Then
                myriga(5) = Format(TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal2.Text, Txt_DataAl2.Text) - TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal2.Text, Txt_DataAl2.Text), "#,##0.00")
            Else
                myriga(5) = 0
            End If

            If IsDate(Txt_DataDal3.Text) Then
                myriga(6) = Format(TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal3.Text, Txt_DataAl3.Text) - TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal3.Text, Txt_DataAl3.Text), "#,##0.00")
            Else
                myriga(6) = 0
            End If

            If IsDate(Txt_DataDal4.Text) Then
                myriga(7) = Format(TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal4.Text, Txt_DataAl4.Text) - TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal4.Text, Txt_DataAl4.Text), "#,##0.00")
            Else
                myriga(7) = 0
            End If

            If IsDate(Txt_DataDal5.Text) Then
                myriga(8) = Format(TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal5.Text, Txt_DataAl5.Text) - TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal5.Text, Txt_DataAl5.Text), "#,##0.00")
            Else
                myriga(8) = 0
            End If

            Tabella.Rows.Add(myriga)

            Totalizzatore1 = Totalizzatore1 + myriga(4)
            Totalizzatore2 = Totalizzatore2 + myriga(5)
            Totalizzatore3 = Totalizzatore3 + myriga(6)
            Totalizzatore4 = Totalizzatore4 + myriga(7)
            Totalizzatore5 = Totalizzatore5 + myriga(8)

            mTotalizzatore1 = mTotalizzatore1 + myriga(4)
            mTotalizzatore2 = mTotalizzatore2 + myriga(5)
            mTotalizzatore3 = mTotalizzatore3 + myriga(6)
            mTotalizzatore4 = mTotalizzatore4 + myriga(7)
            mTotalizzatore5 = mTotalizzatore5 + myriga(8)

        Loop
        ReadMastro.Close()


        Totalizzatore1 = 0
        Totalizzatore2 = 0
        Totalizzatore3 = 0
        Totalizzatore4 = 0
        Totalizzatore5 = 0
        mTotalizzatore1 = 0
        mTotalizzatore2 = 0
        mTotalizzatore3 = 0
        mTotalizzatore4 = 0
        mTotalizzatore5 = 0

        OldMastro = -1
        OldConto = -1
        OldSottoConto = -1
        Dim cmd1 As New OleDbCommand()
        MySql = "SELECT * FROM PianoConti WHERE SottoConto <> 0 And Tipo = 'R' ORDER BY Mastro, Conto, Sottoconto"

        cmd1.CommandText = (MySql)
        cmd1.Connection = cn
        Dim ReadMastro1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While ReadMastro1.Read

            If OldConto <> campodbN(ReadMastro1.Item("Conto")) And OldConto <> -1 Then

                Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
                myriga1(0) = OldMastro
                myriga1(1) = OldConto
                myriga1(2) = 0

                Dim k1 As New Cls_Pianodeiconti

                k1.Mastro = OldMastro
                k1.Conto = OldConto
                k1.Sottoconto = 0
                k1.Decodfica(Session("DC_GENERALE"))
                myriga1(3) = k1.Descrizione

                myriga1(4) = Format(Totalizzatore1, "#,##0.00")
                myriga1(5) = Format(Totalizzatore2, "#,##0.00")
                myriga1(6) = Format(Totalizzatore3, "#,##0.00")
                myriga1(7) = Format(Totalizzatore4, "#,##0.00")
                myriga1(8) = Format(Totalizzatore5, "#,##0.00")

                Tabella.Rows.Add(myriga1)

                Totalizzatore1 = 0
                Totalizzatore2 = 0
                Totalizzatore3 = 0
                Totalizzatore4 = 0
                Totalizzatore5 = 0
            End If

            If OldMastro <> campodbN(ReadMastro1.Item("Mastro")) And OldSottoConto <> -1 Then

                Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
                myriga1(0) = OldMastro
                myriga1(1) = 0
                myriga1(2) = 0

                Dim k1 As New Cls_Pianodeiconti

                k1.Mastro = OldMastro
                k1.Conto = 0
                k1.Sottoconto = 0
                k1.Decodfica(Session("DC_GENERALE"))
                myriga1(3) = k1.Descrizione

                myriga1(4) = Format(mTotalizzatore1, "#,##0.00")
                myriga1(5) = Format(mTotalizzatore2, "#,##0.00")
                myriga1(6) = Format(mTotalizzatore3, "#,##0.00")
                myriga1(7) = Format(mTotalizzatore4, "#,##0.00")
                myriga1(8) = Format(mTotalizzatore5, "#,##0.00")

                Tabella.Rows.Add(myriga1)

                mTotalizzatore1 = 0
                mTotalizzatore2 = 0
                mTotalizzatore3 = 0
                mTotalizzatore4 = 0
                mTotalizzatore5 = 0
            End If

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbN(ReadMastro1.Item("Mastro"))
            myriga(1) = campodbN(ReadMastro1.Item("Conto"))
            myriga(2) = campodbN(ReadMastro1.Item("SottoConto"))

            Dim k As New Cls_Pianodeiconti

            If campodbN(ReadMastro1.Item("Mastro")) = 52 And campodbN(ReadMastro1.Item("Conto")) = 1 And campodbN(ReadMastro1.Item("SottoConto")) = 1 Then
                k.Mastro = campodbN(ReadMastro1.Item("Mastro"))

            End If

            k.Mastro = campodbN(ReadMastro1.Item("Mastro"))
            k.Conto = campodbN(ReadMastro1.Item("Conto"))
            k.Sottoconto = campodbN(ReadMastro1.Item("SottoConto"))
            k.Decodfica(Session("DC_GENERALE"))
            myriga(3) = k.Descrizione

            OldMastro = k.Mastro
            OldConto = k.Conto
            OldSottoConto = k.Sottoconto

            myriga(4) = Format(TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal1.Text, Txt_DataAl1.Text) - TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal1.Text, Txt_DataAl1.Text), "#,##0.00")

            If IsDate(Txt_DataDal2.Text) Then
                myriga(5) = Format(TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal2.Text, Txt_DataAl2.Text) - TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal2.Text, Txt_DataAl2.Text), "#,##0.00")
            Else
                myriga(5) = 0
            End If

            If IsDate(Txt_DataDal3.Text) Then
                myriga(6) = Format(TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal3.Text, Txt_DataAl3.Text) - TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal3.Text, Txt_DataAl3.Text), "#,##0.00")
            Else
                myriga(6) = 0
            End If

            If IsDate(Txt_DataDal4.Text) Then
                myriga(7) = Format(TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal4.Text, Txt_DataAl4.Text) - TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal4.Text, Txt_DataAl4.Text), "#,##0.00")
            Else
                myriga(7) = 0
            End If

            If IsDate(Txt_DataDal5.Text) Then
                myriga(8) = Format(TotaleAvere(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal5.Text, Txt_DataAl5.Text) - TotaleDare(k.Mastro, k.Conto, k.Sottoconto, Txt_DataDal5.Text, Txt_DataAl5.Text), "#,##0.00")
            Else
                myriga(8) = 0
            End If

            Tabella.Rows.Add(myriga)

            Totalizzatore1 = Totalizzatore1 + myriga(4)
            Totalizzatore2 = Totalizzatore2 + myriga(5)
            Totalizzatore3 = Totalizzatore3 + myriga(6)
            Totalizzatore4 = Totalizzatore4 + myriga(7)
            Totalizzatore5 = Totalizzatore5 + myriga(8)

            mTotalizzatore1 = mTotalizzatore1 + myriga(4)
            mTotalizzatore2 = mTotalizzatore2 + myriga(5)
            mTotalizzatore3 = mTotalizzatore3 + myriga(6)
            mTotalizzatore4 = mTotalizzatore4 + myriga(7)
            mTotalizzatore5 = mTotalizzatore5 + myriga(8)

        Loop
        ReadMastro1.Close()

        Dim j As Integer = 0

        If ToExcel = False Then
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.Font.Size = 10
            Grid.DataBind()


            For j = 0 To Grid.Rows.Count - 1
                If Val(Grid.Rows(j).Cells(2).Text) = 0 Then
                    Grid.Rows(j).Cells(0).Font.Bold = True
                    Grid.Rows(j).Cells(1).Font.Bold = True
                    Grid.Rows(j).Cells(2).Font.Bold = True
                    Grid.Rows(j).Cells(3).Font.Bold = True
                    Grid.Rows(j).Cells(4).Font.Bold = True
                    Grid.Rows(j).Cells(5).Font.Bold = True
                    Grid.Rows(j).Cells(6).Font.Bold = True
                    Grid.Rows(j).Cells(7).Font.Bold = True
                    Grid.Rows(j).Cells(8).Font.Bold = True

                    Grid.Rows(j).Cells(0).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(1).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(2).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(3).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(4).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(5).ForeColor = Drawing.Color.Black

                    Grid.Rows(j).Cells(6).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(7).ForeColor = Drawing.Color.Black
                    Grid.Rows(j).Cells(8).ForeColor = Drawing.Color.Black
                End If
            Next
        Else
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.Font.Size = 10
            GridView1.DataBind()


            For j = 0 To GridView1.Rows.Count - 1
                If Val(GridView1.Rows(j).Cells(2).Text) = 0 Then
                    GridView1.Rows(j).Cells(0).Font.Bold = True
                    GridView1.Rows(j).Cells(1).Font.Bold = True
                    GridView1.Rows(j).Cells(2).Font.Bold = True
                    GridView1.Rows(j).Cells(3).Font.Bold = True
                    GridView1.Rows(j).Cells(4).Font.Bold = True
                    GridView1.Rows(j).Cells(5).Font.Bold = True
                    GridView1.Rows(j).Cells(6).Font.Bold = True
                    GridView1.Rows(j).Cells(7).Font.Bold = True
                    GridView1.Rows(j).Cells(8).Font.Bold = True

                    GridView1.Rows(j).Cells(0).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(1).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(2).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(3).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(4).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(5).ForeColor = Drawing.Color.Black

                    GridView1.Rows(j).Cells(6).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(7).ForeColor = Drawing.Color.Black
                    GridView1.Rows(j).Cells(8).ForeColor = Drawing.Color.Black
                End If
            Next
        End If



    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataDal1.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal  1 formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal  1 formalemente errata');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl1.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 1 formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al 1 formalemente errata');", True)
            Exit Sub
        End If
        If Txt_DataDal2.Text <> "" Then
            If Not IsDate(Txt_DataDal2.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 2 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal 2 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl2.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 2 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al 2 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal3.Text <> "" Then
            If Not IsDate(Txt_DataDal3.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 3 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal 3 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl3.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 3 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al 3 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal4.Text <> "" Then
            If Not IsDate(Txt_DataDal4.Text) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal 4 formalemente errata');", True)
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 4 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl4.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 4 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al 4 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal5.Text <> "" Then
            If Not IsDate(Txt_DataDal5.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 5 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal 5 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl5.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 5 formalemente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al 5 formalemente errata');", True)
                Exit Sub
            End If
        End If

        Call EstraiBilancio(False)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Not IsDate(Txt_DataDal1.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal  1 formalemente errata');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl1.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 1 formalemente errata');", True)
            Exit Sub
        End If
        If Txt_DataDal2.Text <> "" Then
            If Not IsDate(Txt_DataDal2.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 2 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl2.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 2 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal3.Text <> "" Then
            If Not IsDate(Txt_DataDal3.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 3 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl3.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 3 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal4.Text <> "" Then
            If Not IsDate(Txt_DataDal4.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 4 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl4.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 4 formalemente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal5.Text <> "" Then
            If Not IsDate(Txt_DataDal5.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal 5 formalemente errata');", True)
                Exit Sub
            End If
            If Not IsDate(Txt_DataAl5.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al 5 formalemente errata');", True)
                Exit Sub
            End If
        End If

        Call EstraiBilancio(True)
        If Tabella.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=PeriodiAConfronto.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Private Sub EseguiJS(Optional ByVal MClient As Boolean = False)
        Dim MyJs As String
        'MyJs = "alert('r');"
        MyJs = ""
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        'MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSGriglie", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Call EseguiJS()


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub
End Class
