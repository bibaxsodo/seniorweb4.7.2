﻿Imports System.Data.OleDb

Partial Class GeneraleWeb_BilancioRiclassificato
    Inherits System.Web.UI.Page

    Dim MyRs As New ADODB.Recordset
    Dim WrDRs As New ADODB.Recordset
    Dim WrARs As New ADODB.Recordset
    Dim WrRs As New ADODB.Recordset
    Dim StampeDb As New ADODB.Connection
    Dim DataInizio As Date
    Dim W_CausaleChiusura As String

    Private Sub PreparaBilancio()
        Dim WSocieta As String
        Dim MySql As String
        Dim MF As New Cls_FunzioniVB6
        Dim Descr As String
        Dim TabelleDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection


        Dim DSoc As New Cls_DecodificaSocieta

        Dim Txt_DataLimiteText As Date

        Dim Chiave As String
        Dim Totale As Double



        Txt_DataLimiteText = Txt_DataLimite.Text

        TabelleDb.Open(Session("DC_TABELLE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        WSocieta = DSoc.DecodificaSocieta(Session("DC_TABELLE"))

        DataInizio = DateSerial(Year(Txt_DataLimiteText), 1, 1)

        MySql = "SELECT * From DatiGenerali"
        MyRs.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            If MF.MoveFromDbWC(MyRs, "EsercizioMese") > 0 And MF.MoveFromDbWC(MyRs, "EsercizioMese") < 13 Then
                DataInizio = DateSerial(Year(Txt_DataLimiteText), MF.MoveFromDbWC(MyRs, "EsercizioMese"), MF.MoveFromDbWC(MyRs, "EsercizioGiorno"))
            End If
            W_CausaleChiusura = MF.MoveFromDbWC(MyRs, "CausaleChiusura")
        End If
        MyRs.Close()
        If Txt_DataLimiteText < DataInizio Then
            DataInizio = DateAdd("yyyy", -1, DataInizio)
        End If
        StampeDb.Open(Session("STAMPEFINANZIARIA"))
        StampeDb.Execute("Delete From BilancioIVDirettivaCEE wHERE PRINTERKEY = '" & Session("PRINTERKEYBILANCIOIV") & "'")
        StampeDb.Execute("Delete From BilancioIVDirettivaCEEDettaglio wHERE PRINTERKEY = '" & Session("PRINTERKEYBILANCIOIV") & "'")

        MySql = "SELECT * FROM PianoConti ORDER BY Mastro, Conto, Sottoconto"
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do Until MyRs.EOF
            Chiave = ""
            Totale = totaleSottoConto(MF.MoveFromDbWC(MyRs, "Mastro"), MF.MoveFromDbWC(MyRs, "Conto"), MF.MoveFromDbWC(MyRs, "SottoConto"))
            If Math.Round(Totale, 2) <> 0 Then
                If MF.MoveFromDbWC(MyRs, "Tipo") = "A" Or MF.MoveFromDbWC(MyRs, "Tipo") = "C" Then
                    Chiave = MF.MoveFromDbWC(MyRs, "Sezione")
                    If Math.Round(Totale, 2) < 0 Then
                        If MF.MoveFromDbWC(MyRs, "Classe") <> "" Then
                            Totale = Math.Round(Totale * -1, 2)
                            Chiave = MF.MoveFromDbWC(MyRs, "Classe")
                        End If
                    End If
                End If
                If MF.MoveFromDbWC(MyRs, "Tipo") = "P" Or MF.MoveFromDbWC(MyRs, "Tipo") = "R" Then
                    Chiave = MF.MoveFromDbWC(MyRs, "Sezione")
                    If Math.Round(Totale, 2) < 0 Then
                        Totale = Math.Round(Totale * -1, 2)
                    Else
                        If MF.MoveFromDbWC(MyRs, "Classe") <> "" Then
                            Chiave = MF.MoveFromDbWC(MyRs, "Classe")
                        Else
                            Totale = Math.Round(Totale * -1, 2)
                        End If
                    End If
                End If
                If MF.MoveFromDbWC(MyRs, "Tipo") = "O" Then
                    Chiave = MF.MoveFromDbWC(MyRs, "Sezione")
                    If Math.Round(Totale, 2) < 0 Then
                        Totale = Math.Round(Totale * -1, 2)
                    End If
                End If


                If MF.MoveFromDbWC(MyRs, "OperazioneinIV") = "S" Then
                    Totale = Math.Round(Totale * -1, 2)
                End If

                MySql = "SELECT * FROM BilancioIVDirettivaCEE WHERE Codice = '" & Chiave & "'"
                WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If WrRs.EOF Then
                    WrRs.AddNew()
                    MF.MoveToDb(WrRs.Fields("codice"), Chiave)
                    MF.MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                    MF.MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimiteText)
                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = MF.MoveFromDbWC(MyRs, "Mastro")
                    DecConto.Conto = MF.MoveFromDbWC(MyRs, "Conto")
                    DecConto.Sottoconto = MF.MoveFromDbWC(MyRs, "SottoConto")
                    DecConto.Decodfica(Session("DC_GENERALE"))


                    MF.MoveToDb(WrRs.Fields("Descrizione1"), MF.MoveFromDbWC(MyRs, "Mastro") & "  " & MF.MoveFromDbWC(MyRs, "Conto") & "  " & MF.MoveFromDbWC(MyRs, "SottoConto") & " - " & DecConto.Descrizione)
                End If
                MF.MoveToDb(WrRs.Fields("Importo"), Math.Round(MF.MoveFromDbWC(WrRs, "Importo") + Totale, 2))
                MF.MoveToDb(WrRs.Fields("PRINTERKEY"), Session("PRINTERKEYBILANCIOIV"))
                WrRs.Update()
                WrRs.Close()

                If Chk_ConDettagli.Checked = True Then
                    MySql = "SELECT * FROM BilancioIVDirettivaCEEDettaglio " & _
                            " WHERE Codice = '" & Chiave & "'" & _
                            " AND Mastro = " & MF.MoveFromDbWC(MyRs, "Mastro") & _
                            " AND Conto = " & MF.MoveFromDbWC(MyRs, "Conto") & _
                            " AND Sottoconto = " & MF.MoveFromDbWC(MyRs, "Sottoconto")
                    WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If WrRs.EOF Then
                        WrRs.AddNew()
                        MF.MoveToDb(WrRs.Fields("codice"), Chiave)
                        MF.MoveToDb(WrRs.Fields("Mastro"), MF.MoveFromDbWC(MyRs, "Mastro"))
                        MF.MoveToDb(WrRs.Fields("Conto"), MF.MoveFromDbWC(MyRs, "Conto"))
                        MF.MoveToDb(WrRs.Fields("Sottoconto"), MF.MoveFromDbWC(MyRs, "Sottoconto"))

                        Dim DecConto As New Cls_Pianodeiconti
                        DecConto.Mastro = MF.MoveFromDbWC(MyRs, "Mastro")
                        DecConto.Conto = MF.MoveFromDbWC(MyRs, "Conto")
                        DecConto.Sottoconto = MF.MoveFromDbWC(MyRs, "SottoConto")
                        DecConto.Decodfica(Session("DC_GENERALE"))

                        MF.MoveToDb(WrRs.Fields("Descrizione"), DecConto.Descrizione)
                    End If
                    MF.MoveToDb(WrRs.Fields("Importo"), Math.Round(MF.MoveFromDbWC(WrRs, "Importo") + Totale, 2))
                    MF.MoveToDb(WrRs.Fields("PRINTERKEY"), Session("PRINTERKEYBILANCIOIV"))
                    WrRs.Update()
                    WrRs.Close()
                End If
            End If

            MyRs.MoveNext()
        Loop
        MyRs.Close()

        MySql = "SELECT * FROM StrutturaIVDirettivaCEE ORDER BY Codice"
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do Until MyRs.EOF
            If Mid(MF.MoveFromDbWC(MyRs, "Codice"), 1, 1) = "/" Then
                MySql = "SELECT * FROM BilancioIVDirettivaCEE WHERE Codice = '" & MF.MoveFromDbWC(MyRs, "Codice") & "'"
                WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If WrRs.EOF Then
                    WrRs.AddNew()
                    MF.MoveToDb(WrRs.Fields("codice"), MF.MoveFromDbWC(MyRs, "Codice"))
                    MF.MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                    MF.MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimite.Text)
                    Totale = 0
                Else
                    Totale = MF.MoveFromDbWC(WrRs, "Importo")
                End If
                Descr = MF.MoveFromDbWC(MyRs, "Descrizione")
                If MF.MoveFromDbWC(MyRs, "CodiceLivello") = 1 Then
                    MF.MoveToDb(WrRs.Fields("Descrizione1"), Descr)
                Else
                    MF.MoveToDb(WrRs.Fields("Descrizione1"), "")
                End If
                If MF.MoveFromDbWC(MyRs, "CodiceLivello") = 3 Then MF.MoveToDb(WrRs.Fields("Descrizione3"), Descr)
                If MF.MoveFromDbWC(MyRs, "CodiceLivello") = 5 Then MF.MoveToDb(WrRs.Fields("Descrizione5"), Descr)
                If MF.MoveFromDbWC(MyRs, "CodiceLivello") = 7 Then MF.MoveToDb(WrRs.Fields("Descrizione7"), Descr)
                If MF.MoveFromDbWC(MyRs, "CodiceLivello") = 9 Then MF.MoveToDb(WrRs.Fields("Descrizione9"), Descr)
                If Descr = "" Then
                    Dim DecConto As New Cls_Pianodeiconti
                    DecConto.Mastro = MF.MoveFromDbWC(MyRs, "Mastro")
                    DecConto.Conto = MF.MoveFromDbWC(MyRs, "Conto")
                    DecConto.Sottoconto = MF.MoveFromDbWC(MyRs, "SottoConto")
                    DecConto.Decodfica(Session("DC_GENERALE"))

                    MF.MoveToDb(WrRs.Fields("Descrizione1"), MF.MoveFromDbWC(MyRs, "Mastro") & "  " & MF.MoveFromDbWC(MyRs, "Conto") & "  " & MF.MoveFromDbWC(MyRs, "SottoConto") & " - " & DecConto.Descrizione)
                End If
                MF.MoveToDb(WrRs.Fields("PRINTERKEY"), Session("PRINTERKEYBILANCIOIV"))
                WrRs.Update()
                WrRs.Close()
                If Chk_ConDettagli.Checked = True Then
                    MySql = "SELECT * FROM BilancioIVDirettivaCEEDettaglio WHERE Codice = '" & MF.MoveFromDbWC(MyRs, "Codice") & "'"
                    WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If WrRs.EOF Then
                        WrRs.AddNew()
                        MF.MoveToDb(WrRs.Fields("codice"), MF.MoveFromDbWC(MyRs, "Codice"))
                        MF.MoveToDb(WrRs.Fields("Mastro"), 0)
                        MF.MoveToDb(WrRs.Fields("Conto"), 0)
                        MF.MoveToDb(WrRs.Fields("Sottoconto"), 0)
                        MF.MoveToDb(WrRs.Fields("Descrizione"), "")
                        MF.MoveToDb(WrRs.Fields("Importo"), 0)
                        MF.MoveToDb(WrRs.Fields("PRINTERKEY"), Session("PRINTERKEYBILANCIOIV"))
                        WrRs.Update()
                    End If
                    WrRs.Close()
                End If
            End If
            If MF.MoveFromDbWC(MyRs, "CodiceOperazione") <> "" Then
                MySql = "SELECT * FROM BilancioIVDirettivaCEE WHERE Codice = '" & MF.MoveFromDbWC(MyRs, "CodiceOperazione") & "'"
                WrRs.Open(MySql, StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If WrRs.EOF Then
                    WrRs.AddNew()
                    MF.MoveToDb(WrRs.Fields("codice"), MF.MoveFromDbWC(MyRs, "CodiceOperazione"))
                    MF.MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                    MF.MoveToDb(WrRs.Fields("DataLimite"), Txt_DataLimite.Text)
                End If
                If MF.MoveFromDbWC(MyRs, "Segno") = "+" Then
                    MF.MoveToDb(WrRs.Fields("Importo"), Math.Round(MF.MoveFromDbWC(WrRs, "Importo") + Totale, 2))
                Else
                    MF.MoveToDb(WrRs.Fields("Importo"), Math.Round(MF.MoveFromDbWC(WrRs, "Importo") - Totale, 2))
                End If
                MF.MoveToDb(WrRs.Fields("PRINTERKEY"), Session("PRINTERKEYBILANCIOIV"))
                WrRs.Update()
                WrRs.Close()
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        StampeDb.Close()

        TabelleDb.Close()
        GeneraleDb.Close()
    End Sub


    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long) As Double
        Dim Condizione As String
        Dim MySql As String
        Dim GeneraleDb As New ADODB.Connection
        Dim mf As New Cls_FunzioniVB6
        Dim Dare As Double
        Dim Avere As Double
        Dim Txt_DataLimiteText As Date


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        

        Txt_DataLimiteText = Txt_DataLimite.Text


        Condizione = ""
        If Chk_AnnoCompetenza.Checked = True Then
            Condizione = " And (Competenza IS NULL OR Competenza = '')"
        End If

        If Chk_SenzaMovimentiChiusura.Checked = True Then
            Condizione = Condizione & " And CausaleContabile <> '" & W_CausaleChiusura & "'"
        End If

        MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & Sottoconto & _
                    " AND DareAvere = 'D' " & _
                    " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " AND DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'}" & Condizione


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn

        Dim MCRs As OleDbDataReader = cmd.ExecuteReader()
        If MCRs.READ Then
            Dare = campodbn(MCRs.Item("Totale"))
        End If
        MCRs.Close()


        MySql = "SELECT SUM (Importo) as Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'A' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione <= {ts'" & Format(Txt_DataLimiteText, "yyyy-MM-dd") & " 00:00:00'}" & Condizione

        Dim cmdA As New OleDbCommand()
        cmdA.CommandText = MySql
        cmdA.Connection = cn

        Dim MCRsA As OleDbDataReader = cmdA.ExecuteReader()
        If MCRsA.Read Then
            Avere = campodbn(MCRsA.Item("Totale"))
        End If
        MCRsA.Close()



        totaleSottoConto = Dare - Avere
        cn.Close()
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataLimite.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data limite formalemente errata');", True)
            REM Lbl_Errori.Text = "Data limite formalemente errata"
            Exit Sub
        End If


        Session("PRINTERKEYBILANCIOIV") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        Call PreparaBilancio()

        Session("SelectionFormula") = "{BilancioIVDirettivaCEE.PRINTERKEY} = " & Chr(34) & Session("PRINTERKEYBILANCIOIV") & Chr(34)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=BILANCIOIV&PRINTERKEY=ON','Stampe','width=800,height=600');", True)

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataLimite.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataLimite.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataLimite.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)


        Txt_DataLimite.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Long
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
End Class
