﻿
Partial Class GeneraleWeb_Regolebudget
    Inherits System.Web.UI.Page



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim Colonna As New Cls_colonnebudget

        Txt_Anno.Text = Year(Now)
        Colonna.Anno = Year(Now)
        Colonna.UpDateDropBox(Session("DC_GENERALE"), DD_Colonna)
        Txt_Importo.Text = "0,00"
        Txt_Percentuale.Text = "0,00"
        Dim Cserv As New Cls_CentroServizio

        Cserv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroSerivizio)

        DD_CentroSerivizio.Items(DD_CentroSerivizio.Items.Count - 1).Selected = False
        DD_CentroSerivizio.Items.Add("Non filtrare")
        DD_CentroSerivizio.Items(DD_CentroSerivizio.Items.Count - 1).Value = "****"
        DD_CentroSerivizio.Items(DD_CentroSerivizio.Items.Count - 1).Selected = True

        If Val(Request.Item("ID")) > 0 Then
            Dim Regole As New Cls_RegoleBudget

            Regole.id = Val(Request.Item("ID"))
            Regole.Decodfica(Session("DC_GENERALE"))


            Colonna.Anno = Regole.Anno
            Colonna.UpDateDropBox(Session("DC_GENERALE"), DD_Colonna)


            Txt_Anno.Text = Regole.Anno
            DD_Colonna.SelectedValue = Regole.Colonna

            Dim ContoB As New Cls_TipoBudget

            ContoB.Anno = Regole.Anno
            ContoB.Livello1 = Regole.Livello1
            ContoB.Livello2 = Regole.Livello2
            ContoB.Livello3 = Regole.Livello3
            ContoB.Decodfica(Session("DC_GENERALE"))
            TxT_Livello1.Text = ContoB.Livello1 & " " & ContoB.Livello2 & " " & ContoB.Livello3 & " " & ContoB.Descrizione

            Dim ContoP As New Cls_Pianodeiconti

            ContoP.Mastro = Regole.MastroPianoDeiConti
            ContoP.Conto = Regole.ContoPianoDeiConti
            ContoP.Sottoconto = Regole.SottoContoPianoDeiConti
            ContoP.Decodfica(Session("DC_GENERALE"))
            Txt_Conto.Text = ContoP.Mastro & " " & ContoP.Conto & " " & ContoP.Sottoconto & " " & ContoP.Descrizione


            Txt_Importo.Text = Format(Regole.ImportoFisso, "#,##0.00")
            Txt_Percentuale.Text = Format(Regole.Percentuale, "#,##0.00")

            DD_CentroSerivizio.SelectedValue = Regole.CentroServizio

        End If
        Call EseguiJS()
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ||  (appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & " $(" & Chr(34) & "#" & TxT_Livello1.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_Conto.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        Dim Livello1 As Long
        Dim Livello2 As Long
        Dim Livello3 As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(TxT_Livello1.Text)

        If Vettore.Length >= 3 Then
            Livello1 = Val(Vettore(0))
            Livello2 = Val(Vettore(1))
            Livello3 = Val(Vettore(2))
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare il livello</center>');", True)
            Exit Sub
        End If

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Vettore = SplitWords(Txt_Conto.Text)

        If Vettore.Length >= 3 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto contabiel</center>');", True)
            Exit Sub
        End If


        Dim Regole As New Cls_RegoleBudget

        Regole.id = Val(Request.Item("ID"))
        Regole.Decodfica(Session("DC_GENERALE"))


        Regole.Anno = Txt_Anno.Text
        Regole.ImportoFisso = Txt_Importo.Text
        Regole.Percentuale = Txt_Percentuale.Text
        Regole.Livello1 = Livello1
        Regole.Livello2 = Livello2
        Regole.Livello3 = Livello3
        Regole.MastroPianoDeiConti = Mastro
        Regole.ContoPianoDeiConti = Conto
        Regole.SottoContoPianoDeiConti = Sottoconto
        Regole.Colonna = DD_Colonna.SelectedValue

        Regole.CentroServizio = DD_CentroSerivizio.SelectedValue
        Regole.Scrivi(Session("DC_GENERALE"))
        Response.Redirect("Elenco_regolebudget.aspx")

    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim Regole As New Cls_RegoleBudget

        Regole.id = Val(Request.Item("ID"))
        Regole.Delete(Session("DC_GENERALE"))
        Response.Redirect("Elenco_regolebudget.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("Elenco_regolebudget.aspx")
    End Sub
End Class
