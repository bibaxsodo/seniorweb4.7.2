﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_CreazioneMovimentiCespiti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataApertura.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataApertura.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataApertura", MyJs, True)



        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)





        Txt_Anno.Text = Year(Now)
        Txt_DataApertura.Text = Format(Now, "dd/MM/yyyy")
    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Function CreaMovimento(ByVal Totale As Double, ByVal cespite As Long) As Long

        Dim Cespiti As New Cls_MovimentiCespite

        Cespiti.CodiceCespite = cespite
        Cespiti.DataRegistrazione = Txt_DataApertura.Text
        Cespiti.TipoMov = "AM"
        Cespiti.Importo = Totale
        Cespiti.Scrivi(Session("DC_GENERALE"))


        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(id) from Cespiti")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CreaMovimento = campodbn(myPOSTreader.Item(0))
        End If
        cn.Close()

    End Function


    Private Function CreaRegistrazione(ByVal tipo As String, ByVal Totale As Double, ByVal Categoria As Long) As Long
        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.DataRegistrazione = Txt_DataApertura.Text
        Registrazione.CausaleContabile = tipo
        Registrazione.Utente = Session("UTENTE")

        
        'XMastro = 0
        'XConto = 0
        'XSottoconto = 0

        'RsCategoria.Open("Select * From CategoriaCespite Where ID = " & Categoria, GeneraleDb, adOpenKeyset, adLockOptimistic)
        'If Not RsCategoria.EOF Then
        '    XMastro = MoveFromDbWC(RsCategoria, "Mastro")
        '    XConto = MoveFromDbWC(RsCategoria, "Conto")
        '    XSottoconto = MoveFromDbWC(RsCategoria, "SottoConto")
        '    rXMastro = MoveFromDbWC(RsCategoria, "MastroContropartita")
        '    rXConto = MoveFromDbWC(RsCategoria, "ContoContropartita")
        '    rXSottoconto = MoveFromDbWC(RsCategoria, "SottoContoContropartita")
        'End If
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = tipo
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


        Dim CSe As New Cls_CategoriaCespite


        CSe.ID = Categoria
        CSe.Leggi(Session("DC_GENERALE"), CSe.ID)
        If IsNothing(Registrazione.Righe(0)) Then
            Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        End If
        Registrazione.Righe(0).MastroPartita = CSe.Mastro
        Registrazione.Righe(0).ContoPartita = CSe.Conto
        Registrazione.Righe(0).SottocontoPartita = CSe.SottoConto
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = "+"
        Registrazione.Righe(0).Importo = Totale
        Registrazione.Righe(0).RigaRegistrazione = 1
        Registrazione.Righe(0).RigaDaCausale = 1

        If IsNothing(Registrazione.Righe(1)) Then
            Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        End If
        Registrazione.Righe(1).MastroPartita = CSe.MastroContropartita
        Registrazione.Righe(1).ContoPartita = CSe.ContoContropartita
        Registrazione.Righe(1).SottocontoPartita = CSe.SottoContoContropartita
        Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
        Registrazione.Righe(1).Segno = "+"
        Registrazione.Righe(1).Importo = Totale
        Registrazione.Righe(1).RigaRegistrazione = 2
        Registrazione.Righe(1).RigaDaCausale = 2


        Registrazione.Scrivi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        CreaRegistrazione = Registrazione.NumeroRegistrazione
    End Function



    Private Sub CreateRegistrazione()
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM   MovimentiCespite Where TipoMov = 'AM' And DataRegistrazione = ?"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataApertura.Text)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Movimenti già creati in questa data, non posso procedere');", True)
            Exit Sub
        End If
        myPOSTreader.Close()


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Categoria", GetType(String))



        Dim Cespiti As New Cls_ParametriCespiti


        Cespiti.LeggiParametri(Session("DC_GENERALE"))

        Dim AppoggioCategoria As Integer
        Dim OldCategoria As Integer
        Dim Importo As Double = 0
        Dim ammorto As Double = 0
        Dim MyReg As Integer = 0
        Dim PercentualeLegge As Double
        Dim PercentualeMinima As Double
        Dim PercentualeAnticipata As Double
        Dim PercentualApplicata As Double
        Dim DescrizioneNon As String
        Dim MyImporto As Double

        AppoggioCategoria = 0
        OldCategoria = 0


        Dim cmdCsp As New OleDbCommand()
        cmdCsp.CommandText = "Select * From Cespiti Where Dismesso = 0 and AnnoAcq <= " & Txt_Anno.Text & " Order By Categoria"
        cmdCsp.Connection = cn
        Dim RsCespiti As OleDbDataReader = cmdCsp.ExecuteReader()
        Do While RsCespiti.Read
            If OldCategoria = 0 Then
                OldCategoria = campodbn(RsCespiti!Categoria)
            End If

            Importo = campodbn(RsCespiti!Importoacq)

            ammorto = 0


            Dim cmdRsAmm As New OleDbCommand()
            cmdRsAmm.CommandText = "Select SUM(Importo) FROM   MovimentiCespite Where CodiceCespite = " & campodbn(RsCespiti.Item("codice")) & " And TipoMov = 'AM' And DataRegistrazione < ? "
            cmdRsAmm.Connection = cn
            cmdRsAmm.Parameters.AddWithValue("@DataApertura", Txt_DataApertura.Text)

            Dim RsAmm As OleDbDataReader = cmdRsAmm.ExecuteReader()

            If RsAmm.Read Then
                ammorto = campodbn(RsAmm.Item(0))
            End If
            RsAmm.Close()


            ammorto = ammorto + campodbn(RsCespiti.Item("ImportoFondo"))

            If Math.Round(Importo, 2) > Math.Round(ammorto, 2) Then
                If campodbn(RsCespiti.Item("Categoria")) <> OldCategoria Then
                    If AppoggioCategoria > 0 Then
                        If Chk_Prova.Checked = False Then
                            MyReg = CreaRegistrazione(Cespiti.CausaleContabile, AppoggioCategoria, OldCategoria)
                        End If
                    End If
                    AppoggioCategoria = 0
                End If
                OldCategoria = campodbn(RsCespiti.Item("Categoria"))

                PercentualeLegge = 100
                PercentualeMinima = 100
                PercentualeAnticipata = 100

                Dim Categoria As New Cls_CategoriaCespite

                Categoria.ID = campodbn(RsCespiti.Item("Categoria"))
                Categoria.Leggi(Session("DC_GENERALE"), campodbn(RsCespiti.Item("Categoria")))


                PercentualeLegge = Categoria.Legge
                PercentualeMinima = Categoria.Minima
                PercentualeAnticipata = Categoria.Anticipata

                PercentualApplicata = 100
                If campodb(RsCespiti.Item("TipoPercentuale")) = "M" Then
                    PercentualApplicata = PercentualeMinima
                End If
                If campodb(RsCespiti.Item("TipoPercentuale")) = "L" Then
                    PercentualApplicata = PercentualeLegge
                End If
                If campodb(RsCespiti.Item("TipoPercentuale")) = "A" Then
                    PercentualApplicata = PercentualeAnticipata
                End If

                DescrizioneNon = ""
                If campodb(RsCespiti.Item("TipoPercentuale")) = "" Then
                    DescrizioneNon = "(Percentuale non indicata)"
                End If

                If campodbn(RsCespiti.Item("AnnoAcq")) = Txt_Anno.Text Then
                    PercentualApplicata = Math.Round(PercentualApplicata / 2, 2)
                End If



                MyImporto = Math.Round((Importo * PercentualApplicata / 100), 2)
                If Math.Round(Importo, 2) < Math.Round(ammorto + MyImporto, 2) Then
                    MyImporto = Math.Round(Importo, 2) - Math.Round(ammorto, 2)
                End If
                'MyReg = CreaRegistrazione(ParametriCespiti, MyImporto, MoveFromDb(RsCespiti!codice))
                If Chk_Prova.Checked = False Then
                    MyReg = CreaMovimento(MyImporto, campodbn(RsCespiti.Item("codice")))
                End If

                AppoggioCategoria = AppoggioCategoria + MyImporto
                'Grid.AddItem(vbTab & MoveFromDb(RsCespiti!codice) & vbTab & MoveFromDb(RsCespiti!Descrizione) & DescrizioneNon & vbTab & Format(MyImporto, "#,##0.00") & vbTab & MyReg & vbTab & DecodificaCategoriaCespite(MoveFromDb(RsCespiti!Categoria)))

                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = campodbn(RsCespiti.Item("codice"))
                myriga(1) = campodb(RsCespiti.Item("Descrizione")) & DescrizioneNon
                myriga(2) = Format(MyImporto, "#,##0.00")
                myriga(3) = Categoria.Descrizione

                Tabella.Rows.Add(myriga)
            End If


        Loop

        If AppoggioCategoria > 0 Then
            If Chk_Prova.Checked = False Then
                MyReg = CreaRegistrazione(Cespiti.CausaleContabile, AppoggioCategoria, OldCategoria)

            End If
            AppoggioCategoria = 0
        End If

        RsCespiti.Close()


        cn.Close()



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.DataBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Cespiti As New Cls_ParametriCespiti


        Cespiti.LeggiParametri(Session("DC_GENERALE"))


        If Cespiti.CausaleContabile = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale contabile cespiti non indicata');", True)
            Exit Sub
        End If

        CreateRegistrazione()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
