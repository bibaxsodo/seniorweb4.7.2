﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class GeneraleWeb_StampaCespiti
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim CatR As New Cls_CategoriaCespite


        CatR.UpDateDropBox(Session("DC_GENERALE"), DD_Categoria)

        Txt_AnnoInizio.Text = Year(Now) - 1

        Txt_SituazioneAData.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Import')!= null) || appoggio.match('Base')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_SituazioneAData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Stampa As New StampeGenerale
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        If Val(DD_Categoria.SelectedValue) = 0 Then
            cmd.CommandText = ("select * from Cespiti  " & _
                                   " Where AnnoAcq >= ? Order by Codice ")
        Else
            cmd.CommandText = ("select * from Cespiti  " & _
                       " Where AnnoAcq >= ? And Categoria = ? Order by Codice ")
        End If
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@AnnoAcq", Txt_AnnoInizio.Text)
        If Val(DD_Categoria.SelectedValue) > 0 Then
            cmd.Parameters.AddWithValue("@Categoria", Val(DD_Categoria.SelectedValue))
        End If

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim WrRs As System.Data.DataRow = Stampa.Tables("Cespiti").NewRow

            'Cespiti()
            WrRs.Item("Codice") = campodb(myPOSTreader.Item("Codice"))
            WrRs.Item("Descrizione") = campodb(myPOSTreader.Item("Descrizione"))
            WrRs.Item("Ubicazione") = campodb(myPOSTreader.Item("Ubicazione"))
            WrRs.Item("AnnoAcq") = campodb(myPOSTreader.Item("AnnoAcq"))
            WrRs.Item("ImportoAcq") = Format(campodbN(myPOSTreader.Item("ImportoAcq")), "#,##0.00")
            WrRs.Item("AnnoVnd") = campodb(myPOSTreader.Item("AnnoVnd"))
            WrRs.Item("ImportoVnd") = campodb(myPOSTreader.Item("ImportoVnd"))
            WrRs.Item("DataDimissione") = campodb(myPOSTreader.Item("DataDismissione"))
            WrRs.Item("DescrizioneDimissione") = campodb(myPOSTreader.Item("DescrizioneDismissione"))
            WrRs.Item("Fondo") = Format(campodbN(myPOSTreader.Item("ImportoFondo")), "#,##0.00")

            Dim cmdRd As New OleDbCommand
            Dim Importo As Double = 0

            If Txt_SituazioneAData.Text = "" Then
                cmdRd.CommandText = ("select * from MovimentiCespite Where CodiceCespite = ?  ")
            Else
                cmdRd.CommandText = ("select * from MovimentiCespite Where CodiceCespite = ?  And " & _
                       "  DataRegistrazione <= ? ")
            End If

            cmdRd.Connection = cn

            cmdRd.Parameters.AddWithValue("@Codice", campodbN(myPOSTreader.Item("Codice")))

            If Txt_SituazioneAData.Text <> "" Then
                cmdRd.Parameters.AddWithValue("@SituazioneAData", Txt_SituazioneAData.Text)
            End If


            Dim RdTest As OleDbDataReader = cmdRd.ExecuteReader()
            Do While RdTest.Read
                Importo = Importo + campodbN(RdTest.Item("Importo"))
            Loop
            RdTest.Close()

            WrRs.Item("Ammortamento") = Format(Importo, "#,##0.00")


            Stampa.Tables("Cespiti").Rows.Add(WrRs)

        Loop

        myPOSTreader.Close()
        cn.Close()



        Session("stampa") = Stampa

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=STAMPACESPITI&XSD=SI','Stampe','width=800,height=600');", True)

    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.ASPX")

    End Sub
End Class
