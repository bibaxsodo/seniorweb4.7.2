﻿Imports System.Data.OleDb

Partial Class GeneraleWeb_ExportExcel
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")

    Protected Sub GeneraleWeb_ExportExcel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyTable = Session("GrigliaSoloStampa")

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Senior.xls")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        'GridView1.RenderControl(htmlWrite)


        form1.Controls.Clear()
        form1.Controls.Add(GridView1)

        form1.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class
