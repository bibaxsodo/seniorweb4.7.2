﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class Pianoconti
    Inherits System.Web.UI.Page

    Protected Sub CaricaConto()
        Dim x As New Cls_Pianodeiconti
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        
        Mastro = Val(Request.Item("MASTRO"))
        Conto = Val(Request.Item("CONTO"))
        Sottoconto = Val(Request.Item("SOTTOCONTO"))


        x.Mastro = Mastro
        x.Conto = Conto
        x.Sottoconto = Sottoconto

        x.Decodfica(Session("DC_GENERALE"))

        TxT_Mastro.Text = x.Mastro
        Txt_Conto.Text = x.Conto
        Txt_Sottoconto.Text = x.Sottoconto
        Txt_Descrizione.Text = x.Descrizione
        DD_Tipo.SelectedValue = x.Tipo
        Txt_commento.Text = x.Commento
        DD_SezioneClasse.SelectedValue = x.Sezione
        DD_SegnoOpposto.SelectedValue = x.Classe


        Try
            DD_Tipologia.SelectedValue = x.TipoAnagrafica
        Catch ex As Exception

        End Try

        'If x.TipoAnagrafica = "O" Then
        '    RB_Ospite.Checked = True
        'End If
        'If x.TipoAnagrafica = "P" Then
        '    RB_Parente.Checked = True
        'End If
        'If x.TipoAnagrafica = "C" Then
        '    RB_Comune.Checked = True
        'End If
        'If x.TipoAnagrafica = "R" Then
        '    RB_Regione.Checked = True
        'End If
        'If x.TipoAnagrafica = "D" Then
        '    RB_CliFor.Checked = True
        'End If
        'If Trim(x.TipoAnagrafica) = "" Then
        '    RB_Generico.Checked = True
        'End If
        If x.ContoDiAmmortamento = 1 Then
            Chk_ContoAmmort.Checked = True
        Else
            Chk_ContoAmmort.Checked = False
        End If
        If x.NonInUso = "S" Then
            Chk_NonInUso.Checked = True
        Else
            Chk_NonInUso.Checked = False
        End If

        Dim k As New Cls_Pianodeiconti
        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = 0
        k.Sottoconto = 0
        k.Decodfica(Session("DC_GENERALE"))
        LblMastro.Text = k.Descrizione

        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = Val(Txt_Conto.Text)
        k.Sottoconto = 0
        k.Decodfica(Session("DC_GENERALE"))
        LblConto.Text = k.Descrizione


        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = Val(Txt_Conto.Text)
        k.Sottoconto = Val(Txt_Sottoconto.Text)
        k.Decodfica(Session("DC_GENERALE"))
        LblSottoconto.Text = k.Descrizione

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

   
        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Xs As New Cls_StruttuIVDirettivaCEE

        Xs.UpDateDropBox(Session("DC_GENERALE"), DD_SezioneClasse)
        Xs.UpDateDropBox(Session("DC_GENERALE"), DD_SegnoOpposto)


        DD_Tipologia.Items.Clear()
        DD_Tipologia.Items.Add("Generico")
        DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = ""
        If Session("TIPOAPP") = "RSA" Then
            DD_Tipologia.Items.Add("Ospite")
            DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "O"
            DD_Tipologia.Items.Add("Parente")
            DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "P"
            DD_Tipologia.Items.Add("Comune")
            DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "C"
            DD_Tipologia.Items.Add("Regione")
            DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "R"
        Else
            DD_Tipologia.Items.Add("Socio")
            DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "S"
            Lbl_SezClasse.Text = "Riclassificazione"
            lbl_Ivdirettiva.Text = "Riclassificazione Regione Veneto"

            Lbl_Segnoopposto.Visible = False
            DD_SegnoOpposto.Visible = False

        End If
        DD_Tipologia.Items.Add("Cliente Fornitore")
        DD_Tipologia.Items(DD_Tipologia.Items.Count - 1).Value = "D"

        If Val(Request.Item("Mastro")) > 0 Or Val(Request.Item("Conto")) > 0 Then
            Call CaricaConto()
            TxT_Mastro.Enabled = False
            Txt_Conto.Enabled = False
            Txt_Sottoconto.Enabled = False
        End If

    End Sub

    Private Sub Pulisci()        
        TxT_Mastro.Text = 0
        Txt_Conto.Text = 0
        Txt_Sottoconto.Text = 0
        Txt_Descrizione.Text = ""
        DD_Tipo.SelectedValue = 0
        Txt_commento.Text = ""
        DD_SezioneClasse.SelectedValue = ""
        DD_SegnoOpposto.SelectedValue = ""

        LblMastro.Text = ""
        LblConto.Text = ""
        LblSottoconto.Text = ""

        Lbl_Errori.Text = ""
        DD_Tipologia.Text = ""
        Chk_ContoAmmort.Checked = False
        Chk_NonInUso.Checked = False
    End Sub
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        Dim xLog As New Cls_Login

        xLog.Utente = Session("UTENTE")
        xLog.LeggiSP(Application("SENIOR"))
        If xLog.ABILITAZIONI.IndexOf("<NONMODIFICASEMPLIFICATA>") > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non sei abilitato modifica/inserimento o cancellazione');", True)
            Exit Sub
        End If

        Dim x As New Cls_Pianodeiconti


        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If DD_Tipo.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il tipo del conto');", True)
            Exit Sub
        End If



        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Mastro = Val(TxT_Mastro.Text)
        Conto = Val(Txt_Conto.Text)
        Sottoconto = Val(Txt_Sottoconto.Text)
        
        If Mastro = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno il mastro');", True)
            Exit Sub
        End If


        If Sottoconto > 0 Then
            Dim XL As New Cls_Pianodeiconti

            XL.Mastro = Mastro
            XL.Conto = Conto
            XL.Sottoconto = 0
            XL.Decodfica(Session("DC_GENERALE"))

            If XL.Descrizione = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<CENTER>Conto non esistente</CENTER>');", True)
                Exit Sub
            End If
        End If


        If Conto > 0 Then
            Dim XL As New Cls_Pianodeiconti

            XL.Mastro = Mastro
            XL.Conto = 0
            XL.Sottoconto = 0
            XL.Decodfica(Session("DC_GENERALE"))

            If XL.Descrizione = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<CENTER>Mastro non esistente</CENTER>');", True)
                Exit Sub
            End If
        End If


        x.Mastro = Mastro
        x.Conto = Conto
        x.Sottoconto = Sottoconto

        x.Decodfica(Session("DC_GENERALE"))

        x.Mastro = Val(TxT_Mastro.Text)
        x.Conto = Val(Txt_Conto.Text)
        x.Sottoconto = Val(Txt_Sottoconto.Text)
        x.Descrizione = Txt_Descrizione.Text
        x.Tipo = DD_Tipo.SelectedValue
        x.Commento = Txt_commento.Text
        x.Sezione = DD_SezioneClasse.SelectedValue
        x.Classe = DD_SegnoOpposto.SelectedValue

        x.TipoAnagrafica = DD_Tipologia.SelectedValue
        'If RB_Ospite.Checked = True Then
        '    x.TipoAnagrafica = "O"
        'End If
        'If RB_Parente.Checked = True Then
        '    x.TipoAnagrafica = "P"
        'End If
        'If RB_Comune.Checked = True Then
        '    x.TipoAnagrafica = "C"
        'End If
        'If RB_Regione.Checked = True Then
        '    x.TipoAnagrafica = "R"
        'End If
        'If RB_CliFor.Checked = True Then
        '    x.TipoAnagrafica = "D"
        'End If
        'If RB_Generico.Checked = True Then
        '    x.TipoAnagrafica = ""
        'End If

        If Chk_ContoAmmort.Checked = True Then
            x.ContoDiAmmortamento = 1
        Else
            x.ContoDiAmmortamento = 0
        End If
        If Chk_NonInUso.Checked = True Then
            x.NonInUso = "S"
        Else
            x.NonInUso = ""
        End If

        x.Scrivi(Session("DC_GENERALE"))

        TxT_Mastro.Enabled = False
        Txt_Conto.Enabled = False
        Txt_Sottoconto.Enabled = False
        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)



        'TxT_Mastro.Text = 0
        'Txt_Conto.Text = 0
        'Txt_Sottoconto.Text = 0
        'Txt_Descrizione.Text = ""
        'DD_Tipo.SelectedValue = 0
        'Txt_commento.Text = ""
        'DD_SezioneClasse.SelectedValue = ""
        'DD_SegnoOpposto.SelectedValue = ""

        'LblMastro.Text = ""
        'LblConto.Text = ""
        'LblSottoconto.Text = ""

        'Lbl_Errori.Text = ""
        'DD_Tipologia.Text = ""
        'Chk_ContoAmmort.Checked = False
        'Chk_NonInUso.Checked = False
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        Dim xLog As New Cls_Login

        xLog.Utente = Session("UTENTE")
        xLog.LeggiSP(Application("SENIOR"))
        If xLog.ABILITAZIONI.IndexOf("<NONMODIFICASEMPLIFICATA>") > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ErroreLogin", "VisualizzaErrore('Non sei abilitato modifica/inserimento o cancellazione');", True)
            Exit Sub
        End If

        Dim x As New Cls_Pianodeiconti
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long        

        Mastro = Val(TxT_Mastro.Text)
        Conto = Val(Txt_Conto.Text)
        Sottoconto = Val(Txt_Sottoconto.Text)

        If Mastro = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno il mastro');", True)
            Exit Sub
        End If

        If Conto = 0 Then

            x.Mastro = Mastro
            x.Conto = 1
            x.Sottoconto = 0

            x.Decodfica(Session("DC_GENERALE"))

            If x.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare vi sono conti');", True)

                REM Lbl_Errori.Text = "Non posso eliminare vi sono conti"
                Exit Sub
            End If
        End If

        If Sottoconto = 0 Then

            x.Mastro = Mastro
            x.Conto = Conto
            x.Sottoconto = 1

            x.Decodfica(Session("DC_GENERALE"))

            If x.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare vi sono sottoconti');", True)

                REM Lbl_Errori.Text = "Non posso eliminare vi sono sottoconti"
                Exit Sub
            End If
        End If

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiContabiliRiga where " & _
                           "MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? ")
        cmd.Parameters.AddWithValue("@Mastro", Mastro)
        cmd.Parameters.AddWithValue("@Conto", Conto)
        cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare vi sono registrazioni con questo conto');", True)
            REM Lbl_Errori.Text = "Non posso eliminare vi sono registrazioni con questo conto"
            cn.Close()
            Exit Sub
        End If
        cn.Close()



        x.Mastro = Mastro
        x.Conto = Conto
        x.Sottoconto = Sottoconto

        x.Elimina(Session("DC_GENERALE"))

        Response.Redirect("ElencoPianoConti.aspx")
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        'Response.Redirect("ElencoPianoConti.aspx")

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoPianoConti.aspx")
        Else
            Response.Redirect("ElencoPianoConti.aspx?TIPO=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
        
    End Sub

    Protected Sub TxT_Mastro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxT_Mastro.TextChanged
        Dim k As New Cls_Pianodeiconti
        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = 0
        k.Sottoconto = 0
        k.Decodfica(Session("DC_GENERALE"))
        LblMastro.Text = k.Descrizione
    End Sub

    Protected Sub Txt_Conto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Conto.TextChanged
        Dim k As New Cls_Pianodeiconti
        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = Val(Txt_Conto.Text)
        k.Sottoconto = 0
        k.Decodfica(Session("DC_GENERALE"))
        LblConto.Text = k.Descrizione
    End Sub

    Protected Sub Txt_Sottoconto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Sottoconto.TextChanged
        Dim k As New Cls_Pianodeiconti
        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = Val(Txt_Conto.Text)
        k.Sottoconto = Val(Txt_Sottoconto.Text)
        k.Decodfica(Session("DC_GENERALE"))
        LblSottoconto.Text = k.Descrizione
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Dim k As New Cls_Pianodeiconti
        k.Mastro = Val(TxT_Mastro.Text)
        k.Conto = Val(Txt_Conto.Text)
        k.Sottoconto = Val(Txt_Sottoconto.Text)
        If k.MaxSottoconto(Session("DC_GENERALE")) > 0 Then
            Txt_Sottoconto.Text = k.MaxSottoconto(Session("DC_GENERALE"))
            Txt_Sottoconto.Enabled = True
            Txt_Descrizione.Text = ""
            LblSottoconto.Text = ""
        Else
            Call Pulisci()
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("Pianoconti.aspx")
    End Sub
End Class

