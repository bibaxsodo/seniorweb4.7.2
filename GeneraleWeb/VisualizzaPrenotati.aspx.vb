﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_VisualizzaPrenotati
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If




        Dim MyTable As New System.Data.DataTable("tabella")
        Dim MyDataSet As New System.Data.DataSet()
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim i As Integer


        Dim ConnectionString As String = Session("DC_GENERALE")


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim CommandColonne As New OleDbCommand("Select * From TipoBudget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?", cn)

        CommandColonne.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Anno").Value = Request.Item("Anno")

        CommandColonne.Parameters.Add("@Livello1", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Livello1").Value = Request.Item("Livello1")

        CommandColonne.Parameters.Add("@Livello2", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Livello2").Value = Request.Item("Livello2")


        CommandColonne.Parameters.Add("@Livello3", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Livello3").Value = Request.Item("Livello3")


        Dim myColonne As OleDbDataReader = CommandColonne.ExecuteReader()
        If myColonne.Read() Then
            Lbl_ContoBudget.Text = myColonne.Item("DESCRIZIONE").ToString
        End If
        myColonne.Close()
        CommandColonne.Dispose()


        Dim CommandCol As New OleDbCommand("Select * From ColonneBudget Where Anno = ? And Livello1 = ? ", cn)

        CommandCol.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandCol.Parameters("@Anno").Value = Request.Item("Anno")

        CommandCol.Parameters.Add("@Livello1", OleDbType.Integer, 8)
        CommandCol.Parameters("@Livello1").Value = Request.Item("Colonna")


        Dim myCol As OleDbDataReader = CommandCol.ExecuteReader()
        If myCol.Read() Then
            Lbl_ContoBudget.Text = Lbl_ContoBudget.Text & " / " & myCol.Item("DESCRIZIONE").ToString
        End If
        myCol.Close()
        CommandCol.Dispose()


        Txt_Livello1.Text = Request.Item("Livello1")
        Txt_Livello2.Text = Request.Item("Livello2")
        Txt_Livello3.Text = Request.Item("Livello3")
        Txt_Colonna.Text = Request.Item("Colonna")



        MySql = "   (Select sum(VariazioniPrenotazioni.importo) From VariazioniPrenotazioni Where VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno And VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero And VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga ) as impvar, "
        MySql = MySql & "   (Select sum(PrenotazioniUsate.importo) From PrenotazioniUsate  Where PrenotazioniUsate.AnnoPrenotazione = PrenotazioniRiga.Anno And PrenotazioniUsate.NumeroPrenotazione = PrenotazioniRiga.Numero And PrenotazioniUsate.RigaPrenotazione = PrenotazioniRiga.Riga ) as impusa"
        Dim CommandImpPrenotazioniCol As New OleDbCommand("Select Numero,Descrizione,Importo," & MySql & " From PrenotazioniRiga Where PrenotazioniRiga.Anno = ? And Livello1 = ? And Livello2 = ? And Livello3= ? And Colonna = ?", cn)

        CommandImpPrenotazioniCol.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandImpPrenotazioniCol.Parameters("@Anno").Value = Request.Item("Anno")

        CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO1", OleDbType.Integer, 8)
        CommandImpPrenotazioniCol.Parameters("@LIVELLO1").Value = Request.Item("Livello1")
        CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO2", OleDbType.Integer, 8)
        CommandImpPrenotazioniCol.Parameters("@LIVELLO2").Value = Request.Item("Livello2")
        CommandImpPrenotazioniCol.Parameters.Add("@LIVELLO3", OleDbType.Integer, 8)
        CommandImpPrenotazioniCol.Parameters("@LIVELLO3").Value = Request.Item("Livello3")

        CommandImpPrenotazioniCol.Parameters.Add("@COLONNA", OleDbType.Integer, 8)
        CommandImpPrenotazioniCol.Parameters("@COLONNA").Value = Request.Item("Colonna")
        Dim myPrenotazioniCol As OleDbDataReader = CommandImpPrenotazioniCol.ExecuteReader()


        MyTable.Columns.Add("Numero", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Variazioni", GetType(String))
        MyTable.Columns.Add("Usato", GetType(String))
        MyDataSet.Tables.Add(MyTable)

        Do While myPrenotazioniCol.Read()
            Dim RigaPrenotato As System.Data.DataRow = MyTable.NewRow()
            RigaPrenotato(0) = myPrenotazioniCol.Item("Numero")
            RigaPrenotato(1) = myPrenotazioniCol.Item("Descrizione")
            If Not IsDBNull(myPrenotazioniCol.Item("Importo")) Then
                RigaPrenotato(2) = Format(myPrenotazioniCol.Item("Importo"), "#,##0.00")
            Else
                RigaPrenotato(2) = Format(0, "#,##0.00")
            End If

            If Not IsDBNull(myPrenotazioniCol.Item("impvar")) Then
                RigaPrenotato(3) = Format(myPrenotazioniCol.Item("impvar"), "#,##0.00")
            Else
                RigaPrenotato(3) = Format(0, "#,##0.00")
            End If

            If Not IsDBNull(myPrenotazioniCol.Item("impusa")) Then
                RigaPrenotato(4) = Format(myPrenotazioniCol.Item("impusa"), "#,##0.00")
            Else
                RigaPrenotato(4) = Format(0, "#,##0.00")
            End If

            MyTable.Rows.Add(RigaPrenotato)

        Loop
        myPrenotazioniCol.Close()
        CommandImpPrenotazioniCol.Dispose()
        cn.Close()

        DaGrid.AutoGenerateColumns = True
        DaGrid.DataSource = MyDataSet
        DaGrid.DataMember = "tabella"
        DaGrid.DataBind()

        For i = 0 To DaGrid.Rows.Count - 1
            DaGrid.Rows.Item(i).HorizontalAlign = HorizontalAlign.Right

            DaGrid.Rows(i).Cells(0).HorizontalAlign = HorizontalAlign.Left
            DaGrid.Rows(i).Cells(1).HorizontalAlign = HorizontalAlign.Left
        Next i


    End Sub
End Class
