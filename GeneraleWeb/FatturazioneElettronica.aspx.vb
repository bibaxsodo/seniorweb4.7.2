﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class GeneraleWeb_FatturazioneElettronica
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click        
        Dim NumeroRegistrazione As Long
        Dim MaxID As Long


        For MaxID = 0 To Chk_Registrazioni.Items.Count - 1
            If Chk_Registrazioni.Items(MaxID).Selected = True Then
                NumeroRegistrazione = Chk_Registrazioni.Items(MaxID).Value
            End If
        Next

        Dim x As New Cls_TabellaSocieta


        x.Leggi(Session("DC_TABELLE"))





        Dim cn As OleDbConnection
        Dim cnOsp As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        cnOsp = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOsp.Open()


        'MyRs.Open("Select max(Id) From FatturaElettronica", GeneraleDb, adOpenKeyset, adLockBatchOptimistic)
        'If Not MyRs.EOF Then
        '    MaxID = MoveFromDb(MyRs.Fields(0))
        'End If
        'MyRs.Close()

        Dim cmd As New OleDbCommand
        cmd.CommandText = "Select max(Progressivo) From FatturaElettronica"
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxID = campodbN(myPOSTreader.Item(0)) + 1
        End If
        myPOSTreader.Close()

        Session("Progressivo") = 0
        Dim cmdProgressivo As New OleDbCommand

        cmdProgressivo.CommandText = "Select * From FatturaElettronica Where NumeroRegistrazione = " & NumeroRegistrazione
        cmdProgressivo.Connection = cn
        Dim ReadProgressivo As OleDbDataReader = cmdProgressivo.ExecuteReader()
        If ReadProgressivo.Read() Then
            Session("Progressivo") = campodbN(ReadProgressivo.Item("Progressivo"))
            MaxID = Session("Progressivo")
        End If
        ReadProgressivo.Close()

        If Session("Progressivo") = 0 Then

            Dim CNG As New OleDbCommand("INSERT INTO FatturaElettronica (UTENTE,DATAORA,XMLORIGINALE,XMLMODIFICATO,NUMEROREGISTRAZIONE,Progressivo) VALUES (?,?,?,?,?,?)", cn)
            CNG.Parameters.AddWithValue("@Utente", Session("UTENTE"))
            CNG.Parameters.AddWithValue("@DataORa", Now)
            CNG.Parameters.AddWithValue("@XMLORIGINALE", "")
            CNG.Parameters.AddWithValue("@XMLMODIFICATO", "")
            CNG.Parameters.AddWithValue("@NUMEROREGISTRAZIONE", NumeroRegistrazione)
            CNG.Parameters.AddWithValue("@Progressivo", MaxID)
            CNG.ExecuteNonQuery()
            Session("Progressivo") = MaxID
        End If



        Dim CodiceDestinatario As String = ""
        Dim CFCOMMITTENTE As String = ""
        Dim DENOMINAZIONECOMMITTENTE As String = ""
        Dim DENOMINAZIONEINDIRIZZO As String = ""
        Dim DENOMINAZIONECAP As String = ""
        Dim DENOMINAZIONECOMUNE As String = ""
        Dim DENOMINAZIONEPROV As String = ""
        Dim PIVACOMMITTENTE As String = ""
        Dim CodiceCup As String = ""
        Dim CodiceCig As String = ""
        Dim Iban As String = ""
        Dim IstitutoFinanziario As String = ""
        Dim xModalitaPagamento As String = ""
        Dim ModalitaPagamento As String = ""
        Dim IdDocumento As String = ""
        Dim RiferimentoAmministrazione As String = ""
        Dim NumeroFatturaDDT As Integer = 0
        Dim RIGANOTE As String        
        Dim NoteFattura As String = ""
        Dim MovimentiCT As New Cls_MovimentoContabile
        Dim TIPINVIO As String = ""

        MovimentiCT.NumeroRegistrazione = NumeroRegistrazione
        MovimentiCT.Leggi(Session("DC_GENERALE"), MovimentiCT.NumeroRegistrazione)


        Dim Tipologia As String
        Dim tipodoc As String
        Dim DataDoc As String
        Dim XCodiceRegione As String = ""
        Dim XCodiceProvincia As String = ""
        Dim XCodiceComune As String = ""

        Tipologia = MovimentiCT.Tipologia

        Dim CauCon As New Cls_CausaleContabile

        CauCon.Codice = MovimentiCT.CausaleContabile
        CauCon.Leggi(Session("DC_TABELLE"), CauCon.Codice)

        tipodoc = CauCon.TipoDocumento
        DataDoc = Format(MovimentiCT.DataRegistrazione, "yyyy-MM-dd")


        Session("tipodoc") = tipodoc
        Session("DataDoc") = DataDoc
        Session("IDDataDoc") = DataDoc
        Session("NumeroDocumento") = MovimentiCT.NumeroProtocollo

        Dim RegistroIva As New Cls_RegistroIVA


        RegistroIva.Leggi(Session("DC_TABELLE"), MovimentiCT.RegistroIVA)

        If RegistroIva.IndicatoreRegistro <> "" Then
            Session("NumeroDocumento") = Session("NumeroDocumento") & "/" & RegistroIva.IndicatoreRegistro
        End If

        If Mid(Tipologia & Space(10), 1, 1) = "R" Then
            XCodiceRegione = Mid(Tipologia & Space(10), 2, 4)
            Tipologia = "R"
        End If
        If Mid(Tipologia & Space(10), 1, 1) = "C" Then
            XCodiceProvincia = Mid(Tipologia & Space(10), 2, 3)
            XCodiceComune = Mid(Tipologia & Space(10), 5, 3)
            Tipologia = "C"
        End If
        If Mid(Tipologia & Space(10), 1, 1) = "J" Then
            XCodiceProvincia = Mid(Tipologia & Space(10), 2, 3)
            XCodiceComune = Mid(Tipologia & Space(10), 5, 3)
            Tipologia = "J"
        End If
        If Tipologia = "R" Then
            Dim Regioni As New ClsUSL

            Regioni.CodiceRegione = XCodiceRegione
            Regioni.Leggi(Session("DC_OSPITE"))


            CodiceDestinatario = Regioni.CodiceDestinatario
            CFCOMMITTENTE = Regioni.CodiceFiscale
            PIVACOMMITTENTE = Format(Regioni.PARTITAIVA, "00000000000")
            DENOMINAZIONECOMMITTENTE = Regioni.Nome
            DENOMINAZIONEINDIRIZZO = Regioni.RESIDENZAINDIRIZZO1
            DENOMINAZIONECAP = Regioni.RESIDENZACAP1

            Dim ksCom As New ClsComune

            ksCom.Provincia = Regioni.RESIDENZAPROVINCIA1
            ksCom.Comune = Regioni.RESIDENZACOMUNE1
            ksCom.DecodficaComune(Session("DC_OSPITE"))

            DENOMINAZIONECOMUNE = ksCom.Descrizione


            TIPINVIO = Regioni.EGo
            ksCom.Provincia = Regioni.RESIDENZAPROVINCIA1
            ksCom.Comune = ""
            ksCom.Leggi(Session("DC_OSPITE"))

            DENOMINAZIONEPROV = ksCom.CodificaProvincia
            CodiceCup = Regioni.CodiceCup
            CodiceCig = Regioni.CodiceCig
            Iban = AdattaLunghezza(Regioni.IntCliente, 2) & AdattaLunghezzaNumero(Regioni.NumeroControlloCliente, 2) & AdattaLunghezza(Regioni.CINCLIENTE, 1) & AdattaLunghezzaNumero(Regioni.ABICLIENTE, 5) & AdattaLunghezzaNumero(Regioni.CABCLIENTE, 5) & AdattaLunghezzaNumero(Regioni.CCBANCARIOCLIENTE, 12)
            IstitutoFinanziario = Regioni.BancaCliente

            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Regioni.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))

            xModalitaPagamento = ModPAg.ModalitaPagamento

            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                ModalitaPagamento = ModPAgGen.CampoFE
            End If

            IdDocumento = Regioni.IdDocumento
            RiferimentoAmministrazione = Regioni.RiferimentoAmministrazione
            NumeroFatturaDDT = Regioni.NumeroFatturaDDT

            RIGANOTE = Regioni.Note

            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = MovimentiCT.CentroServizio
            k.CodiceRegione = XCodiceRegione
            k.LeggiRegioneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceRegione)
            NoteFattura = k.NoteUp
        End If

        If Tipologia = "C" Then
            Dim Comune As New ClsComune

            Comune.Provincia = XCodiceProvincia
            Comune.Comune = XCodiceComune
            Comune.Leggi(Session("DC_OSPITE"))


            CodiceDestinatario = Comune.CodiceDestinatario
            CFCOMMITTENTE = Comune.CodiceFiscale
            PIVACOMMITTENTE = Format(Comune.PartitaIVA, "00000000000")

            DENOMINAZIONECOMMITTENTE = Comune.Descrizione
            DENOMINAZIONEINDIRIZZO = Comune.RESIDENZAINDIRIZZO1
            DENOMINAZIONECAP = Comune.RESIDENZACAP1

            TIPINVIO = Comune.EGo

            If Comune.IdDocumentoData <> "" Then
                Session("IDDataDoc") = Comune.IdDocumentoData
            End If

            Dim ksCom As New ClsComune

            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = Comune.RESIDENZACOMUNE1
            ksCom.DecodficaComune(Session("DC_OSPITE"))

            DENOMINAZIONECOMUNE = ksCom.Descrizione


            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = ""
            ksCom.Leggi(Session("DC_OSPITE"))

            DENOMINAZIONEPROV = ksCom.CodificaProvincia
            CodiceCup = Comune.CodiceCup
            CodiceCig = Comune.CodiceCig            
            Iban = AdattaLunghezza(Comune.IntCliente, 2) & AdattaLunghezzaNumero(Comune.NumeroControlloCliente, 2) & AdattaLunghezza(Comune.CINCLIENTE, 1) & AdattaLunghezzaNumero(Comune.ABICLIENTE, 5) & AdattaLunghezzaNumero(Comune.CABCLIENTE, 5) & AdattaLunghezzaNumero(Comune.CCBANCARIOCLIENTE, 12)
            IstitutoFinanziario = Comune.BANCACLIENTE

            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Comune.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))

            xModalitaPagamento = ModPAg.ModalitaPagamento

            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                ModalitaPagamento = ModPAgGen.CampoFE
            End If

            IdDocumento = Comune.IdDocumento
            RiferimentoAmministrazione = Comune.RiferimentoAmministrazione
            NumeroFatturaDDT = Comune.NumeroFatturaDDT

            RIGANOTE = Comune.Note

            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = MovimentiCT.CentroServizio
            k.CodiceProvincia = XCodiceProvincia
            k.CodiceComune = XCodiceComune
            k.LeggiComuneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceProvincia, k.CodiceComune)
            NoteFattura = k.NoteUp
        End If

        If Tipologia = "J" Then
            Dim Comune As New ClsComune

            Comune.Provincia = XCodiceProvincia
            Comune.Comune = XCodiceComune
            Comune.Leggi(Session("DC_OSPITE"))


            CodiceDestinatario = Comune.CodiceDestinatario
            CFCOMMITTENTE = Comune.CodiceFiscale
            PIVACOMMITTENTE = Format(Comune.PartitaIVA, "00000000000")

            DENOMINAZIONECOMMITTENTE = Comune.Descrizione
            DENOMINAZIONEINDIRIZZO = Comune.RESIDENZAINDIRIZZO1
            DENOMINAZIONECAP = Comune.RESIDENZACAP1

            TIPINVIO = Comune.EGo

            Dim ksCom As New ClsComune

            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = Comune.RESIDENZACOMUNE1
            ksCom.DecodficaComune(Session("DC_OSPITE"))

            DENOMINAZIONECOMUNE = ksCom.Descrizione


            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = ""
            ksCom.Leggi(Session("DC_OSPITE"))

            DENOMINAZIONEPROV = ksCom.CodificaProvincia
            CodiceCup = Comune.CodiceCup
            CodiceCig = Comune.CodiceCig
            REM Iban = Format(Comune.IntCliente, "##") & Format(Comune.NumeroControlloCliente, "00") & Format(Comune.CINCLIENTE, "#") & Format(Comune.ABICLIENTE, "00000") & Format(Comune.CABCLIENTE, "00000") & Format(Comune.CCBANCARIOCLIENTE, "000000000000")
            Iban = AdattaLunghezza(Comune.IntCliente, 2) & AdattaLunghezzaNumero(Comune.NumeroControlloCliente, 2) & AdattaLunghezza(Comune.CINCLIENTE, 1) & AdattaLunghezzaNumero(Comune.ABICLIENTE, 5) & AdattaLunghezzaNumero(Comune.CABCLIENTE, 5) & AdattaLunghezzaNumero(Comune.CCBANCARIOCLIENTE, 12)
            IstitutoFinanziario = Comune.BANCACLIENTE

            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Comune.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))

            xModalitaPagamento = ModPAg.ModalitaPagamento

            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                ModalitaPagamento = ModPAgGen.CampoFE
            End If

            IdDocumento = Comune.IdDocumento
            RiferimentoAmministrazione = Comune.RiferimentoAmministrazione
            NumeroFatturaDDT = Comune.NumeroFatturaDDT

            RIGANOTE = Comune.Note

            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = MovimentiCT.CentroServizio
            k.CodiceProvincia = XCodiceProvincia
            k.CodiceComune = XCodiceComune
            k.LeggiComuneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceProvincia, k.CodiceComune)
            NoteFattura = k.NoteUp
        End If

        If Tipologia = "" Then
            Dim cmdTipologiaCF As New OleDbCommand
            Dim MastroPartita As Long = 0
            Dim ContoPartita As Long = 0
            Dim SottocontoPartita As Long = 0

            Try
                MastroPartita = MovimentiCT.Righe(0).MastroPartita
                ContoPartita = MovimentiCT.Righe(0).ContoPartita
                SottocontoPartita = MovimentiCT.Righe(0).SottocontoPartita
            Catch ex As Exception

            End Try
            

            cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroCliente = " & MastroPartita & " And ContoCliente = " & ContoPartita & " And SottocontoCliente  = " & SottocontoPartita
            cmdTipologiaCF.Connection = cnOsp
            Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
            If ReadCliFor.Read() Then
                CodiceDestinatario = campodb(ReadCliFor.Item("CodiceDESTINATARIO"))
                CFCOMMITTENTE = campodb(ReadCliFor.Item("CodiceFiscale"))
                PIVACOMMITTENTE = Format(campodb(ReadCliFor.Item("PARTITAIVA")), "00000000000")


                DENOMINAZIONECOMMITTENTE = campodb(ReadCliFor.Item("Nome"))
                DENOMINAZIONEINDIRIZZO = campodb(ReadCliFor.Item("RESIDENZAINDIRIZZO1"))
                DENOMINAZIONECAP = campodb(ReadCliFor.Item("RESIDENZACAP1"))

                Dim ksCom As New ClsComune

                ksCom.Provincia = campodb(ReadCliFor.Item("RESIDENZAPROVINCIA1"))
                ksCom.Comune = campodb(ReadCliFor.Item("RESIDENZACOMUNE1"))
                ksCom.DecodficaComune(Session("DC_OSPITE"))

                DENOMINAZIONECOMUNE = ksCom.Descrizione
                DENOMINAZIONEPROV = ksCom.CodificaProvincia
                CodiceCup = campodb(ReadCliFor.Item("CodiceCup"))
                CodiceCig = campodb(ReadCliFor.Item("CodiceCig"))
                Try

                    Iban = Format(campodb(ReadCliFor.Item("IntCliente")), "##") & Format(campodbN(ReadCliFor.Item("NumeroControlloCliente")), "00") & Format(campodb(ReadCliFor.Item("CinCliente")), "#") & Format(campodb(ReadCliFor.Item("ABICliente")), "00000") & Format(campodb(ReadCliFor.Item("CABCliente")), "00000") & Format(campodbN(ReadCliFor.Item("CCBancarioCliente")), "000000000000")
                Catch ex As Exception

                End Try

                IstitutoFinanziario = campodb(ReadCliFor.Item("BancaCliente"))

                IdDocumento = campodb(ReadCliFor.Item("IdDocumento"))
                RiferimentoAmministrazione = campodb(ReadCliFor.Item("RiferimentoAmministrazione"))
                REM NumeroFatturaDDT = campodb(ReadCliFor.Item("NumeroFatturaDDT"))
                RIGANOTE = campodb(ReadCliFor.Item("Note"))
            End If
            ReadProgressivo.Close()

            NoteFattura = MovimentiCT.Descrizione
        End If

        cnOsp.Close()
        Session("BOLLO") = 0

        For i = 0 To MovimentiCT.Righe.Length - 1

            If Not IsNothing(MovimentiCT.Righe(i)) Then
                If MovimentiCT.Righe(i).RigaDaCausale = 9 Then
                    Session("BOLLO") = MovimentiCT.Righe(i).Importo
                End If
            End If
        Next



        Dim TabellaSocieta As New Cls_TabellaSocieta

        TabellaSocieta.Leggi(Session("DC_TABELLE"))


        If TabellaSocieta.IBAN <> "" Then
            Iban = TabellaSocieta.IBAN
        End If

        Session("TIPINVIO") = TIPINVIO
        Session("Tipologia") = Tipologia
        Session("CodiceDestinatario") = CodiceDestinatario
        Session("CFCOMMITTENTE") = CFCOMMITTENTE
        Session("PIVACOMMITTENTE") = PIVACOMMITTENTE

        Session("DENOMINAZIONECOMMITTENTE") = DENOMINAZIONECOMMITTENTE
        Session("DENOMINAZIONEINDIRIZZO") = DENOMINAZIONEINDIRIZZO
        Session("DENOMINAZIONECAP") = DENOMINAZIONECAP
        Session("DENOMINAZIONECOMUNE") = DENOMINAZIONECOMUNE
        Session("DENOMINAZIONEPROV") = DENOMINAZIONEPROV
        Session("CodiceCup") = CodiceCup
        Session("CodiceCig") = CodiceCig
        Session("Iban") = Iban
        Session("IstitutoFinanziario") = IstitutoFinanziario
        Session("xModalitaPagamento") = xModalitaPagamento
        Session("ModalitaPagamento") = ModalitaPagamento
        Session("IdDocumento") = IdDocumento
        Session("RiferimentoAmministrazione") = RiferimentoAmministrazione
        Session("NumeroFatturaDDT") = NumeroFatturaDDT
        Session("NumeroRegistrazione") = MovimentiCT.NumeroRegistrazione

        Session("CausaleContablie") = MovimentiCT.CausaleContabile
        Session("RIGANOTE") = RIGANOTE
        Session("NoteFattura") = NoteFattura
        Session("PERIODO") = Dd_Mese.Text & "/" & Txt_Anno.Text



        Response.Redirect("FatturaElettronica_1.aspx")

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub GeneraleWeb_FatturazioneElettronica_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)


        Dim MyReg As New Cls_RegistroIVA

        MyReg.UpDateDropBox(Session("DC_TABELLE"), DD_RegIVa)


        If Request.Item("CHIAMATO") = "1" Then
            Txt_Anno.Text = Session("XMLANNO")
            Dd_Mese.SelectedValue = Session("XMLMESE")
            DD_RegIVa.SelectedValue = Session("XMLREGIVA")
            Call Visualizza()
        End If
    End Sub

    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click

        Call Visualizza()
        
    End Sub
    Private Sub Visualizza()


        If Val(DD_RegIVa.SelectedValue) = 0 Then

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specifica Registro IVA');", True)
            Exit Sub
        End If

        Session("XMLANNO") = Txt_Anno.Text
        Session("XMLMESE") = Dd_Mese.SelectedValue
        Session("XMLREGIVA") = DD_RegIVa.SelectedValue

        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmdSaldo As New OleDbCommand()
        cmdSaldo.CommandText = "SELECT * FROM MovimentiContabiliTesta " & _
            " WHERE MeseCompetenza = " & Dd_Mese.SelectedValue & _
            " AND AnnoCompetenza = " & Val(Txt_Anno.Text) & " And RegistroIVA = " & DD_RegIVa.SelectedValue
        cmdSaldo.Connection = cn

        Chk_Registrazioni.Items.Clear()

        Dim ReadSaldo As OleDbDataReader = cmdSaldo.ExecuteReader()
        Do While ReadSaldo.Read
            Dim Mov As New Cls_MovimentoContabile
            Dim FatturaElettronica As String = ""

            Mov.NumeroRegistrazione = ReadSaldo.Item("NumeroRegistrazione")
            Mov.Leggi(Session("DC_GENERALE"), Mov.NumeroRegistrazione)

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = Mov.Righe(0).MastroPartita
            PianoConti.Conto = Mov.Righe(0).ContoPartita
            PianoConti.Sottoconto = Mov.Righe(0).SottocontoPartita
            PianoConti.Decodfica(Session("DC_GENERALE"))


            Dim cmdRiga As New OleDbCommand()
            cmdRiga.CommandText = "Select * From FatturaElettronica where NumeroRegistrazione =" & Mov.NumeroRegistrazione
            cmdRiga.Connection = cn
            Dim ReadRiga As OleDbDataReader = cmdRiga.ExecuteReader()
            If ReadRiga.Read Then
                FatturaElettronica = " (Importata : " & campodb(ReadRiga.Item("DataOra")) & " progressivo " & campodb(ReadRiga.Item("Progressivo")) & ")"
            End If
            ReadRiga.Close()

            Chk_Registrazioni.Items.Add(ReadSaldo.Item("NumeroRegistrazione") & " " & ReadSaldo.Item("DataRegistrazione") & " " & ReadSaldo.Item("AnnoProtocollo") & " " & ReadSaldo.Item("NumeroProtocollo") & " " & PianoConti.Descrizione & " " & FatturaElettronica)
            Chk_Registrazioni.Items(Chk_Registrazioni.Items.Count - 1).Value = ReadSaldo.Item("NumeroRegistrazione")
        Loop
        ReadSaldo.Close()

        cn.Close()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../OspitiWeb/Menu_Export.aspx")
    End Sub

    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function

End Class



