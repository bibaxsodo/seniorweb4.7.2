﻿
Partial Class GeneraleWeb_contabilita
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        REM **************************************************************
        REM * SOLO PER DEBUG
        REM **************************************************************
        'Dim k As New Cls_Login

        'k.Utente = "SODO"
        'k.Chiave = "MASTER"

        'Session("UTENTE") = k.Utente
        'k.Leggi(Application("SENIOR"))

        'Session("DC_OSPITE") = k.Ospiti
        'Session("DC_TABELLE") = k.TABELLE
        'Session("DC_GENERALE") = k.Generale

        'Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        REM **************************************************************
        REM * FINE SOLO PER DEBUG
        REM **************************************************************
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<GG>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        If Val(Request.Item("AzzeraRegistrazione")) > 0 Then
            Session("NumeroRegistrazione") = 0
        End If

        If Val(Session("NumeroRegistrazione")) > 0 Then
            Dim Xt As New Cls_MovimentoContabile
            Xt.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))
            Dim XCausale As New Cls_CausaleContabile

            XCausale.Leggi(Session("DC_TABELLE"), Xt.CausaleContabile)

            Lbl_Registrazione.Text = "<div style=""background-color:#00FFFF; "" ><font style=""font-size: small; font-family: Arial;background-color: #00FFFF; ""><a href=""#"" class=""commentato"">Registrazione : " & Val(Session("NumeroRegistrazione")) & " - Data " & Xt.DataRegistrazione & "<div>Causale :" & XCausale.Descrizione & "<br>Protocollo : " & Xt.NumeroProtocollo & "/" & Xt.AnnoProtocollo & " </div></a> - <a href=""contabilita.aspx?AzzeraRegistrazione=1"" rel=""external"">Esci</a></font></div>"
        End If

        Label1.Text = ""
        lbl_container.Text = "<h1>Gestione Contabilità Web<br/></h1><table><tr><td><img src=images\qdocumenti.jpg></td><td bgcolor=#808080></td><td valign=top align=right>Gestione Documenti<br />Prima Nota<br />Incassi Pagamenti<br />Bilancio<br /></td><tr><td bgcolor=#C0C0C0></td><td><img src=images\qincassipagamenti.jpg></td><td bgcolor=#808080></td></tr><tr><td valign=bottom>Sodo Informatica S.r.l.<br/><a href=http://www.sodo.it>www.sodo.it</a></td><td bgcolor=#C0C0C0></td><td><img src=images\qmastrini.jpg></td></tr></table>"

        If Request.Item("TIPO") = "PrimaNota" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PrimaNota"" data-role=""button"" data-theme=""b""  rel=""external"">Prima Nota</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PrimaNota"" data-role=""button"" rel=""external"">Prima Nota</a> "
        End If
        If Request.Item("TIPO") = "Documenti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Documenti"" data-role=""button"" data-theme=""b""  rel=""external"">Documenti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Documenti"" data-role=""button"" rel=""external"">Documenti</a> "
        End If
        If Request.Item("TIPO") = "IncassiPagamenti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncassiPagamenti"" data-role=""button"" data-theme=""b""  rel=""external"">Incassi/Pagamenti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncassiPagamenti"" data-role=""button"" rel=""external"">Incassi/Pagamenti</a> "
        End If
        If Request.Item("TIPO") = "IncPagScade" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncPagScade"" data-role=""button"" data-theme=""b""  rel=""external"">Inc/Pag Scadenz.</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=IncPagScade"" data-role=""button"" rel=""external"">Inc/Pag Scadenz.</a> "
        End If
        If Request.Item("TIPO") = "PianoConti" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PianoConti"" data-role=""button"" data-theme=""b""  rel=""external"">Piano Conti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=PianoConti"" data-role=""button"" rel=""external"">Piano Conti</a> "
        End If
        If Request.Item("TIPO") = "Anagrafica" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Anagrafica"" data-role=""button"" data-theme=""b""  rel=""external"">Anagrafica Cli/For</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Anagrafica"" data-role=""button"" rel=""external"">Anagrafica Cli/For</a> "
        End If
        If Request.Item("TIPO") = "Ricerca" Then
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Ricerca"" data-role=""button"" data-theme=""b""  rel=""external"">Ricerca</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Ricerca"" data-role=""button"" rel=""external"">Ricerca</a> "
        End If


        If Request.Item("TIPO") = "Budget" Then
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Budget</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx?Tipo=Budget"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Budget</a>  "
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<BUDGET>") >= 0 And Request.Item("TIPO") = "Budget" Or Request.Item("TIPO") = "VisualizzaBudget" Or Request.Item("TIPO") = "InserireBudegt" Or Request.Item("TIPO") = "RicePren" _
            Or Request.Item("TIPO") = "GestPrenotazione" Or Request.Item("TIPO") = "ContiBudget" Or Request.Item("TIPO") = "ColonneBudget" Then
            If Request.Item("TIPO") = "VisualizzaBudget" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=VisualizzaBudget"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Visualizza Budget</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=VisualizzaBudget"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Visualizza Budget</a> "
            End If
            If Request.Item("TIPO") = "InserireBudegt" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=InserireBudegt"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Inserisci Budget</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=InserireBudegt"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Inserisci Budget</a> "
            End If


            If Request.Item("TIPO") = "RicePren" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=RicePren"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Ricerca Pren.</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=RicePren"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Ricerca Pren.</a> "
            End If


            If Request.Item("TIPO") = "GestPrenotazione" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=GestPrenotazione"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Prenotazioni</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=GestPrenotazione"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Prenotazioni</a> "
            End If
            If Request.Item("TIPO") = "ContiBudget" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ContiBudget"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Conti Budget</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ContiBudget"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Conti Budget</a> "
            End If
            If Request.Item("TIPO") = "ColonneBudget" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ColonneBudget"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Colonne Budget</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ColonneBudget"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Colonne Budget</a> "
            End If


        End If

        If Request.Item("TIPO") = "Tabelle" Then
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Tabelle</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx?Tipo=Tabelle"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Tabelle</a>  "
        End If

        If Request.Item("TIPO") = "Tabelle" Or Request.Item("TIPO") = "CausaliContabili" Or
           Request.Item("TIPO") = "TipoRegistro" Or Request.Item("TIPO") = "RegistriIVA" Or
           Request.Item("TIPO") = "ModalitaPagamento" Or Request.Item("TIPO") = "Detraibilita" Or
           Request.Item("TIPO") = "Aliquote" Then
            If Request.Item("TIPO") = "CausaliContabili" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=CausaliContabili"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Causali Contabili</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=CausaliContabili"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Causali Contabili</a>  "
            End If
            If Request.Item("TIPO") = "TipoRegistro" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=TipoRegistro"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Tipo Registro</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=TipoRegistro"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Tipo Registro</a>  "
            End If
            If Request.Item("TIPO") = "RegistriIVA" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=RegistriIVA"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Registri IVA</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=RegistriIVA"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Registri IVA</a>  "
            End If
            If Request.Item("TIPO") = "ModalitaPagamento" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ModalitaPagamento"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Modalita Pagamento</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=ModalitaPagamento"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Modalita Pagamento</a>  "
            End If
            If Request.Item("TIPO") = "Detraibilita" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Detraibilita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Detraibilita</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Detraibilita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Detraibilita</a>  "
            End If
            If Request.Item("TIPO") = "Aliquote" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Aliquote"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Aliquota IVA</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Aliquote"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Aliquota IVA</a>  "
            End If

        End If

        If Request.Item("TIPO") = "Stampe" Then
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Stampe</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx?Tipo=Stampe"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Stampe</a>  "
        End If

        If Request.Item("TIPO") = "Stampe" Or Request.Item("TIPO") = "STMASTRINO" Or Request.Item("TIPO") = "StGiornaleContabilita" Or
           Request.Item("TIPO") = "STBILAN" Or Request.Item("TIPO") = "StRegistriIVA" Or
           Request.Item("TIPO") = "StLiquidazione" Or Request.Item("TIPO") = "StPianoConti" Then
            If Request.Item("TIPO") = "STMASTRINO" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=STMASTRINO"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Stampa Mastrino</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=STMASTRINO"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Stampa Mastrino</a> "
            End If
            If Request.Item("TIPO") = "STBILAN" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=STBILAN"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Bilancio</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=STBILAN"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Bilancio</a> "
            End If
            If Request.Item("TIPO") = "StRegistriIVA" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StRegistriIVA"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Stampa Registri IVA</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StRegistriIVA"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Stampa Registri IVA</a> "
            End If
            If Request.Item("TIPO") = "StLiquidazione" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StLiquidazione"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Stampa Liquidazione</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StLiquidazione"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Stampa Liquidazione</a> "
            End If
            If Request.Item("TIPO") = "StPianoConti" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StPianoConti"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Stampa Piano Conti</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StPianoConti"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Stampa Piano Conti</a> "
            End If
            If Request.Item("TIPO") = "StGiornaleContabilita" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StGiornaleContabilita"" data-role=""button"" data-icon=""arrow-r"" data-theme=""b""  rel=""external"">Giornale Contabilita</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=StGiornaleContabilita"" data-role=""button"" data-icon=""arrow-r"" rel=""external"">Giornale Contabilita</a> "
            End If
        End If


        If Request.Item("TIPO") = "Visualiz" Then
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Visualizzazioni</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx?Tipo=Visualiz"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Visualizzazioni</a>  "
        End If
        If Request.Item("TIPO") = "Visualiz" Or Request.Item("TIPO") = "DocumScoper" Or
           Request.Item("TIPO") = "Scadenzario" Or Request.Item("TIPO") = "Spesometro" Or Request.Item("TIPO") = "VisPeriodContr" Then
            If Request.Item("TIPO") = "DocumScoper" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=DocumScoper"" data-icon=""arrow-r"" data-role=""button"" data-theme=""b""  rel=""external"">Documenti Scoperti</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=DocumScoper"" data-icon=""arrow-r"" data-role=""button"" rel=""external"">Documenti Scoperti</a> "
            End If

            If Request.Item("TIPO") = "Scadenzario" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Scadenzario"" data-icon=""arrow-r"" data-role=""button"" data-theme=""b""  rel=""external"">Scadenzario</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Scadenzario"" data-icon=""arrow-r"" data-role=""button"" rel=""external"">Scadenzario</a> "
            End If


            If Request.Item("TIPO") = "Spesometro" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Spesometro"" data-icon=""arrow-r"" data-role=""button"" data-theme=""b""  rel=""external"">Spesometro</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Spesometro"" data-icon=""arrow-r"" data-role=""button"" rel=""external"">Spesometro</a> "
            End If

            If Request.Item("TIPO") = "VisPeriodContr" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=VisPeriodContr"" data-icon=""arrow-r"" data-role=""button"" data-theme=""b""  rel=""external"">Periodi a confronto</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=VisPeriodContr"" data-icon=""arrow-r"" data-role=""button"" rel=""external"">Periodi a confronto</a> "
            End If

        End If


        If Request.Item("TIPO") = "Servizi" Then
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Servizi</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""contabilita.aspx?Tipo=Servizi"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Servizi</a>  "
        End If
        If Request.Item("TIPO") = "Servizi" Or Request.Item("TIPO") = "Chiusure" Then
            If Request.Item("TIPO") = "Chiusure" Then
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Chiusure"" data-icon=""arrow-r"" data-role=""button"" data-theme=""b""  rel=""external"">Chiusure</a> "
            Else
                Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Chiusure"" data-icon=""arrow-r"" data-role=""button"" rel=""external"">Chiusure</a> "
            End If
        End If

        Label1.Text = Label1.Text & "<a href=""#"" onclick=""CaricaAltroFrame();"" data-role=""button""   rel=""external"">EasyViewer</a> "


        If Request.Item("TIPO") = "Chiusure" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""ChiusureContabili.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "ModalitaPagamento" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""modalitapagamento.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "DocumScoper" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""DocumentiScoperti.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "StPianoConti" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""StPianoConti.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Scadenzario" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""VisualizzaScadenzario.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Ricerca" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""Ricerca.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "PrimaNota" And Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") >= 0 Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;""  src=""primanota.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "STMASTRINO" Then
            lbl_container.Text = "<iframe id=""output"" style=""border-width:0;""  src=""mastrino.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "STBILAN" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""Bilancio.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Documenti" And Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") >= 0 Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""documenti.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "IncassiPagamenti" And Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") >= 0 Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""incassipagamenti.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "IncPagScade" And Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") >= 0 Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""incassipagamenti.aspx?TIPO=SCADENZARIO"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "PianoConti" And Session("ABILITAZIONI").ToString.IndexOf("<PIANOCONTI>") >= 0 Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""PianoConti.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "Anagrafica" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""AnagraficaClientiFornitori.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "CausaliContabili" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""causalicontabili.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Aliquote" Then
            lbl_container.Text = "<iframe id=""output"" style=""border-width:0;""  src=""AliquotaIVA.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "TipoRegistro" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""TipoRegistro.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "RegistriIVA" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""RegistroIVA.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "StRegistriIVA" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""stamparegistriiva.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "StLiquidazione" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""LiquidazioneIVA.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "Detraibilita" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""Detraibilita.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "VisPeriodContr" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""VisualizzaBilanciPeriodoConfronto.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "VisualizzaBudget" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""VisualizzaBudget.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "InserireBudegt" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""InserimentoBudget.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "StGiornaleContabilita" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""StampaGiornaleContabilita.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "GestPrenotazione" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""GestionePrenotazione.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "ContiBudget" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""contibudget.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "ColonneBudget" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""GestioneColonneBudget.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "RicePren" Then
            lbl_container.Text = "<iframe id=""output""  style=""border-width:0;"" src=""RicercaPrenotazione.aspx"" width=""100%"" data-theme=""b""   height=""600px""></iframe> "
        End If

        Label1.Text = Label1.Text & "<a href=""contabilita.aspx?TIPO=Menu"" data-role=""button"" rel=""external"">Menù</a> "
        If Request.Item("TIPO") = "Menu" Then
            System.Web.HttpContext.Current.Response.Redirect("../MainMenu.aspx")
        End If



    End Sub
End Class
