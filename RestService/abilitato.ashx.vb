﻿Public Class abilitato
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim Password As String = context.Request.QueryString("Password")
        Dim Funzione As String = context.Request.QueryString("Funzione")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer


        Dim DbC As New Cls_LoginParente

        DbC.Utente = UTENTE
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            context.Response.Write("0")
            Exit Sub
        End If


        If Funzione = "Denaro" Then
            If DbC.Denaro = 1 Then
                context.Response.Write("1")
                Exit Sub
            End If
        End If
        If Funzione = "Stanza" Then
            If DbC.Stanza = 1 Then
                context.Response.Write("1")
                Exit Sub
            End If
        End If
        If Funzione = "Contabilita" Then
            If DbC.Contabilita = 1 Then
                context.Response.Write("1")
                Exit Sub
            End If
        End If
        If Funzione = "Diurno" Then
            If DbC.Diurno = 1 Then
                context.Response.Write("1")
                Exit Sub
            End If
        End If

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class