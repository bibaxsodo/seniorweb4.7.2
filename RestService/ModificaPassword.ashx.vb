﻿Public Class ModificaPassword1
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim OldPassword As String = context.Request.QueryString("OldPassword")
        Dim NewPassword As String = context.Request.QueryString("NewPassword")
        Dim ConfermaPassword As String = context.Request.QueryString("ConfermaPassword")


        If UTENTE = "" Then Exit Sub
        If OldPassword = "" Then Exit Sub


        If NewPassword <> ConfermaPassword Then
            context.Response.Write("PASSWORD NON COINCIDENTI")
            Exit Sub
        End If


        Dim DbC As New Cls_LoginParente

        DbC.Utente = UTENTE
        DbC.Password = OldPassword
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            context.Response.Write("PASSWORD ERRATA")
            Exit Sub
        End If


        DbC.Password = NewPassword
        DbC.ModificaPassword(context.Application("SENIOR"))

        context.Response.Write("PASSWORD MODIFICATA")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class