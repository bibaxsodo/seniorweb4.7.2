﻿Public Class Diurno1
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim Password As String = context.Request.QueryString("Password")
        Dim mese As String = context.Request.QueryString("mese")
        Dim anno As String = context.Request.QueryString("anno")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim js As New Script.Serialization.JavaScriptSerializer


        Dim DbC As New Cls_LoginParente

        DbC.Utente = UTENTE
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            Exit Sub
        End If

        Dim UlM As New Cls_Movimenti

        UlM.CodiceOspite = DbC.CodiceOspite
        UlM.UltimaData(DbC.Ospiti, UlM.CodiceOspite)

        Dim MovD As New Cls_Diurno

        MovD.CENTROSERVIZIO = UlM.CENTROSERVIZIO
        MovD.CodiceOspite = DbC.CodiceOspite

        MovD.Leggi(DbC.Ospiti, MovD.CodiceOspite, MovD.CENTROSERVIZIO, Val(anno), Val(mese))

        If MovD.Mese = 0 And MovD.Anno = 0 Then
            context.Response.Write("Presenza diurna non caricata")
            Exit Sub
        End If
        Dim Appoggio As String
        Appoggio = ""
        Appoggio = Appoggio & "<b>1°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno1, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>2°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno2, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>3°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno3, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>4°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno4, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>5°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno5, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>6°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno6, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>7°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno7, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>8°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno8, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>9°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno9, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>10°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno10, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>11°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno11, DbC.Ospiti) & "<br /><br />"


        Appoggio = Appoggio & "<b>12°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno12, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>13°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno13, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>14°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno14, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>15°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno15, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>16°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno16, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>17°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno17, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>18°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno18, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>19°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno19, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>20°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno20, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>21°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno21, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>22°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno22, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>23°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno23, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>24°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno24, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>25°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno25, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>26°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno26, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>27°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno27, DbC.Ospiti) & "<br /><br />"

        Appoggio = Appoggio & "<b>28°</b><br />"
        Appoggio = Appoggio & SingoloGiorno(MovD.Giorno28, DbC.Ospiti) & "<br /><br />"

        If GiorniMese(MovD.Mese, MovD.Anno) >= 29 Then
            Appoggio = Appoggio & "<b>29°</b><br />"
            Appoggio = Appoggio & SingoloGiorno(MovD.Giorno29, DbC.Ospiti) & "<br /><br />"

            If GiorniMese(MovD.Mese, MovD.Anno) >= 30 Then
                Appoggio = Appoggio & "<b>30°</b><br />"
                Appoggio = Appoggio & SingoloGiorno(MovD.Giorno30, DbC.Ospiti) & "<br /><br />"
                If GiorniMese(MovD.Mese, MovD.Anno) >= 31 Then
                    Appoggio = Appoggio & "<b>31°</b><br />"
                    Appoggio = Appoggio & SingoloGiorno(MovD.Giorno31, DbC.Ospiti) & "<br /><br />"
                End If
            End If
        End If

        context.Response.Write(Appoggio)

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Function SingoloGiorno(ByVal Causale As String, ByVal Connessione As String) As String
        SingoloGiorno = ""
        If Causale = "" Then
            SingoloGiorno = "Presente (senza variazioni)"
        Else
            Dim CauEnt As New Cls_CausaliEntrataUscita

            CauEnt.Codice = Causale
            CauEnt.LeggiCausale(Connessione)
            SingoloGiorno = CauEnt.Descrizione
        End If
    End Function

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class