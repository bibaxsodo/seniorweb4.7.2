﻿Public Class BarraSeniorElencoProgrammi
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CodiceBarra As String = ""
        Dim Pagina As String = context.Request.QueryString("Pagina")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim k As New Cls_Login

        k.Utente = context.Session("UTENTE")
        k.LeggiSP(context.Application("SENIOR"))


        If Pagina.ToUpper <> "Menu_Societa_aspx".ToUpper And Not IsNothing(k.ABILITAZIONI) Then
            If k.ABILITAZIONI.IndexOf("<LOGPRIVACY>") >= 0 Then
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""IdMenuOspiti"">LOG</li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuOspiti"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/seniorweb/privacy/StatistichePrivacy.aspx"">UTENTI SENIOR</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/seniorweb/privacy/StatistichePrivacyUtente.aspx"">ANAGRAFICHE</a></li>"
                CodiceBarra = CodiceBarra & "</ul>"
            End If
            If k.ABILITAZIONI.IndexOf("<GO>") >= 0 And k.ABILITAZIONI.IndexOf("<NONOSPITI>") = -1 Then
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""IdMenuOspiti"">Ospiti</li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuOspiti"">"
                If k.ABILITAZIONI.IndexOf("<ANAGRAFICA>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/seniorweb/OspitiWeb/RicercaAnagrafica.aspx"">OSPITE</a></li>"
                End If
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/MovimentiDiurniCentroServizio.aspx"">PRE.DIURNO CSERV</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/MovimentiDomiciliariCentroServizio.aspx"">DOMICILIARI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/AddebitiAccreditiMultipli.aspx"">ADDEBITI MULTIPLI</a></li>"
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idStatisticheOspiti"" >STATISTICHE</a></li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuStatisticheOspiti"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ProspettoOspitiPresenti.aspx"">OSPITI PRES.</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Prospetto.aspx"">PROSP. PRESENZE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VerificaRette.aspx"">VERIF. RETTE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/RicercaAnagrafica.aspx?TIPO=DOCOSPPAR"">SIT.CONTABILE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VisualizzaMovimenti.aspx"">VIS. MOVIMENTI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VisualizzazioneAddebitoAccredito.aspx"">VIS. ADD./ACR.</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/RicercaAnagrafica.aspx?TIPO=EstrattoContoDenaro"">ESTRATTO CONTO OSPITE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/EstrattoContoDenaro.aspx"">ESTRATTO CONTO</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StanzeLibere.aspx"">LETTI LIBERI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StanzeOccupate.aspx"">LETTI OCCUPATI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ProspettoPresenzeAssenzeStanzaMovimenti.aspx"">MOVIMENTI LETTI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/FlussoOspiti.aspx"">FLUSSO OSPITI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StatisticaDomiciliari.aspx"">DOMICILIARI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StatistiDomiciliareSchema.aspx"">DOM.SCHEMA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Frm_EstrazionePer730.aspx"">EST.730</a></li>  "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VisualizzaImpegnative.aspx"">IMPEGN.</a></li>     "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VisualizzaISE.aspx"">ISE</a></li> "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/SituazioneContabileCentroServizio.aspx"">SIT. CONT. CSERV</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/VisualizzazioneQuote.aspx"">CRONOLOGIA QUOTE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ProspettoConguagliRettaMese.aspx"">CONGUAGLIO ANTICIPATI</a></li>"
                'CONGUAGLIO ANTICIPATI

                CodiceBarra = CodiceBarra & "</ul>"
                If k.ABILITAZIONI.IndexOf("<CALCOLO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Calcolo.aspx"">CALCOLO</a></li>"
                End If

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Rendiconto.aspx"">RENDICONTO</a></li>"
                If k.ABILITAZIONI.IndexOf("<EMISSIONE>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/EmissioneRette.aspx"">EMISSIONE</a></li>"
                End If

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StampaDocumenti.aspx"">STAMPA DOCUMENTI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ricalcolo.aspx"">RICALCOLO</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/StampaFattureDettagliate.aspx"">FATTURE DETTAGLIATE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/SeniorWeb/OspitiWeb/ElencoCentroServizio.aspx"">FATTURE DETTAGLIATE</a></li>"

                If k.ABILITAZIONI.IndexOf("<TABELLA>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""IdTabelleOspiti"" >TABELLE</li>          "
                    CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuTabelleOspiti"">"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoCentroServizio.aspx"">CENTRO SER.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoCausali.aspx"">CAUSALI E/U</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoComuni.aspx"">COMUNI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoRegioni.aspx"">REGIONE</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoTipoAddebito.aspx"">TIPO ADD/ACR</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/TabelleDescrittive.aspx"">PARTAB.DESCR.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Parametri.aspx"">PARAMETRI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoMedici.aspx"">MEDICI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoTipoOperazione.aspx"">TIPO OPER.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_Stanze.aspx"">STANZE</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/TabelleDescrittiveOspitiAccessori.aspx"">TAB.DESCR.STANZE</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoModalitapagamento.aspx"">TAB.MOD.PAG.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoExtraFissi.aspx"">EXTRA FISSI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoCausaliEpersonam.aspx"">SENIOR-EPERSONAM</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoNoteFatture.aspx"">NOTE FATTURE</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoTipoRetta.aspx"">TAB.TIPO.RETTA</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_TipoDomiciliare.aspx"">TAB.TIPO.DOMIC.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_Operatori.aspx"">OPERATORI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_TabellaTipoImportoRegione.aspx"">TIPO IMP.</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoTipoSconto.aspx"">TIPO SCONTO</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_Banche.aspx"">BANCHE</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ElencoNucleiCentroServizi.aspx"">NUCLEI-CSERV</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Elenco_Listino.aspx"">LISTINI</a></li>"

                    'NULCEI-CSERV
                    CodiceBarra = CodiceBarra & "</ul>"
                End If

                If k.ABILITAZIONI.IndexOf("<SERVIZI>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idEPERSONAM"" >EPERSONAM</li>          "
                    CodiceBarra = CodiceBarra & "<ul class=""custom-menu-listSmall2"" id=""MenuEPERSONAM"">"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/AllineaDbInterfaccia.aspx"">E-PERSONAM ALLINEA</a></li>        "
                    'If <EPERSONAM-AUT>
                    If k.ABILITAZIONI.IndexOf("<EPERSONAM-AUT>") >= 0 Then
                        CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/importomovimenti_new.aspx"">E-PERSONAM MOVIM</a></li>        "
                    Else
                        CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ImportOspiti.aspx"">E-PERSONAM OSPITI</a></li>        "
                    End If
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ImportDiurnoNew.aspx"">E-PERSONAM DIURNO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ImportDomiciliare.aspx"">E-PERSONAM DOMIC</a></li>  "
                    CodiceBarra = CodiceBarra & "</ul>"
                End If

                If k.ABILITAZIONI.IndexOf("<SERVIZI>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idEXPORT"" >EXPORT</li>"
                    CodiceBarra = CodiceBarra & "<ul class=""custom-menu-listSmall2"" id=""MenuEXPORT"">"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/FatturazioneElettronica.aspx"">FAT.XML</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Export_Documenti.aspx"">EXPORT DOCUMENTI</a></li>"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ImportExcelToXml.aspx"">730 XML</a></li>  "
                    CodiceBarra = CodiceBarra & "<li><a href=""http://www.Sodo.it/INvio730TS/Invio730TS.aspx?Utente=" & k.Utente & "&Societa=" & k.RagioneSociale & "&Data=" & Format(Now, "dd/MM/yyyy") & "&Url=" & Pagina
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Export_RID.aspx"">EXPORT RID</a></li>  "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Export_MAV.aspx"">EXPORT MAV</a></li>  "

                    CodiceBarra = CodiceBarra & "</ul>"
                End If

                If k.ABILITAZIONI.IndexOf("<SERVIZI>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idStrumenti"" >STRUMENTI</li>          "
                    CodiceBarra = CodiceBarra & "<ul class=""custom-menu-listSmall2"" id=""MenuStrumenti"">"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/EliminaFatturazione.aspx"">ELIMINA FAT.</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/Controllapianoconti.aspx"">CONTR. PIANOCONTI</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/AggiornaRette.aspx"">AGG. RETTA TOTALE</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/IncassiDaRid.aspx"">INCASSI RID</a></li>        "
                    'IncassiDaRid.aspx
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/FatturaDaAddebitiAccrediti.aspx"">FATT. AD/AC</a>   </li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/ImportAddebitiDaFileExcel.aspx"">IMPORT ADD.ACR</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/IncassoDaBollettino.aspx"">INC. DA BOLLETTINO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/RicercaAnagrafica.aspx?TIPO=MODIFICARETTA"">MODIFICA RETTA</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/OspitiWeb/RicercaAnagrafica.aspx?TIPO=MODIFICACSERV"">MODIFICA CSERV</a></li>        "


                    CodiceBarra = CodiceBarra & "</ul>"
                End If

                CodiceBarra = CodiceBarra & "</ul>"
            End If

            If k.ABILITAZIONI.IndexOf("<GG>") >= 0 And k.ABILITAZIONI.IndexOf("<NONGENERALE>") < 0 Then
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""IdMenuGenerale"">Generale</li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuGenerale"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/UltimiMovimenti.aspx?TIPO=PRNT"">PRIMA NOTA</a></li>        "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/UltimiMovimenti.aspx?TIPO=INC"">INCASSI E PAGAMENTI</a></li>            "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/UltimiMovimenti.aspx?TIPO=DOC"">DOCUMENTI</a></li>            "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/UltimiMovimenti.aspx?TIPO=INCS"">INC. PAG. SCAD.</a></li>            "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoClientiFornitori.aspx"">CLI. FOR.</a></li>            "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoPianoconti.aspx"">PIANO DEI CONTI</a></li>            "

                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idBudget"" >BUDGET</li>          "
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuBudget"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/RicercaPrenotazione.aspx"">RICERCA PREN.</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/VisualizzaBudget.aspx"">VISUALIZZA BUDGET</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoPrenotazioni.aspx"">GEST. PRENOTAZIONI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoVariazioniBudget.aspx"">VARIAZIONE BUDGET</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/InserimentoBudget.aspx"">INSERIMENTO BUDGET</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoContiBudget.aspx"">CONTI BUDGET</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoColonneBudget.aspx"">COLONNE BUDGET</a></li>    "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/UltimiMovimenti.aspx?TIPO=MANREV"">REV.MAN.</a></li>"
                CodiceBarra = CodiceBarra & "</ul>"
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idStampeGenerale"" >STAMPE</li>          "
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuStampeGenerale"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StPianoconti.aspx"">PIANO DEI CONTI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaPrimaNota.aspx"">PRIMA NOTA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/mastrino.aspx"">MASTRINO</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Bilancio.aspx"">BILANCIO</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaGiornaleContabilita.aspx"">GIORNALE CONTABILITA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/stamparegistriiva.aspx"">REGISTRI IVA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/LiquidazioneIVA.aspx"">LIQUIDAZIONE IVA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/BilancioRiclassificato.aspx"">RICLASSIFICATO</a></li>        "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaClientiFornitori.aspx"">STAMPA CLIENTI/FORNITORI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaSaldi.aspx"">SALDI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaReversali.aspx"">REVERSALI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaMandati.aspx"">MANDATI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaDistinte.aspx"">DISTINTE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/StampaFatture.aspx"">FATTURE</a></li>        "
                CodiceBarra = CodiceBarra & "</ul>"
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idVisualizzazione"" >VISUALIZZAZIONI</li>          "
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-listSmall"" id=""MenuVisualizzazione"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/DocumentiScoperti.aspx"">DOCUMENTI SCOPERTI</a></li>"
                If k.ABILITAZIONI.IndexOf("<PERIODOCONFRONTO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/VisualizzaBilanciPeriodoConfronto.aspx"">PERIODO A CONFRONTO</a></li>"
                End If

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/VisualizzaScadenzario.aspx"">SCADENZARIO</a></li>"
                CodiceBarra = CodiceBarra & "</ul>"

                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idTabelleGenerale"" >TABELLE</li>          "
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuTabelleGenerale"">"
                If k.ABILITAZIONI.IndexOf("<CAUSALICONTABILI>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoCausaliContabili.aspx"">CAUSALI CONT.</a></li>"
                End If

                If k.ABILITAZIONI.IndexOf("<MODALITAPAGAMENTO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoModalitapagamento.aspx"">MOD. PAGAMENTO</a></li>"
                End If

                If k.ABILITAZIONI.IndexOf("<TIPOREGISTRO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoTipoRegistro.aspx"">TIPO REGISTRO</a></li>"
                End If

                If k.ABILITAZIONI.IndexOf("<REGISTROIVA>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoRegistriIVA.aspx"">REGISTRI IVA</a></li>"
                End If

                If k.ABILITAZIONI.IndexOf("<DETRAIBILITA>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoDetraibilita.aspx"">DETRAIBILITA</a></li>"
                End If

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoAliquotaIVA.aspx"">ALIQUOTE IVA</a></li>"

                'RITENUTE
                If k.ABILITAZIONI.IndexOf("<RITENUTE>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ElencoRitenute.aspx"">RITENUTE</a></li>"
                End If

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Elenco_NoteFatture.aspx"">NOTE FATTURE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Tabella_DatiGenerali.aspx"">DATI GENERALI</a></li>"

                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Elenco_TabellaTrascodifica.aspx"">TAB.TRASCODIFICA</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Elenco_Tributi.aspx"">TRIBUTI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Elenco_FEALIQUOTA.aspx"">FE ALIQUOTE IVA</a></li>"
                CodiceBarra = CodiceBarra & "</ul>"


                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idStrumentiGenerale"" >STRUMENTI</li>          "
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-listSmall"" id=""MenuStrumentiGenerale"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ChiusureContabili.aspx"">CHIUSURE CONT.</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Parametri.aspx"">PARAMETRI</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ControlloRegistrazioni.aspx"">CONTROL.REG.</a></li>"

                If k.ABILITAZIONI.IndexOf("<SPESOMETRO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/Spesometro.aspx"">SPESOMETRO</a></li>   "
                End If


                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/XMLLiquidazioneIVATrimestrale.aspx"">LIQUIDAZIONE XML</a></li>   "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/DatiFatture.aspx"">DATI FATTURE</a></li>"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/GeneraleWeb/ImportRegistrazioni.aspx"">IMP. REG. STIPENDI</a></li>"

                CodiceBarra = CodiceBarra & "</ul>"

                CodiceBarra = CodiceBarra & "</ul>"

                '
                '<PROTOCOLLO>
                If k.ABILITAZIONI.IndexOf("<PROTOCOLLO>") >= 0 Then
                    CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idMenuProtocollo"">Protocollo</li>"
                    CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuProtocollo"">"
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Elenco_Protocollo.aspx"">PROTOCOLLO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Stmp_Protocollo.aspx"">STAMPA</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/StampaRegistro_Protocollo.aspx"">REGISTRO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Menu_Rubrica.aspx"">RUBRICA</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/ImportaMetodo.aspx"">IMPORTA DA METODO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_Titolo.aspx"">TABELLA TITOLO</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_Classe.aspx"">TABELLA CLASSE</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_Indice.aspx"">TABELLA INDICE</a></li>        "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_Mezzo.aspx"">TABELLA MEZZO</a></li>   "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_UfficioDestinatario.aspx"">TABELLA UFF.DEST.</a></li>   "
                    CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Protocollo/Tabella_Fascicolo.aspx"">TABELLA FASCICOLO</a></li>           "
                    CodiceBarra = CodiceBarra & "</ul>"
                End If



                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idMenuMailingList"">Mailing List</li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuMailingList"">"
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/MailingList/Elenco_Mail.aspx"">MAIL</a></li>        "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/MailingList/Elenco_MailingList.aspx"">MAIL LIST</a></li>        "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/MailingList/InvioMail.aspx"">INVIO MAIL</a></li>        "
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/MailingList/InviaFatture.aspx"">INVIO FATTURE</a></li>                "
                CodiceBarra = CodiceBarra & "</ul>"

            End If

            If k.Utente.IndexOf("<") >= 0 Or context.Session("UTENTEIMPER") <> "" Then
                CodiceBarra = CodiceBarra & "<li class=""context-menu-submenu"" id=""idMenuMailingList"">Società</li>"
                CodiceBarra = CodiceBarra & "<ul class=""custom-menu-list"" id=""MenuMailingList""  style=""top:60px"">"
                CodiceBarra = CodiceBarra & context.Session("SOCIETALI")
                CodiceBarra = CodiceBarra & "</ul>"
                'CaricaSocieta

            End If
        End If

        context.Response.Write(CodiceBarra)
    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class