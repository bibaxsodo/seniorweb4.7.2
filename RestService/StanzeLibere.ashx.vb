﻿Imports System.Data.OleDb

Public Class StanzeLibere
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim Password As String = context.Request.QueryString("Password")
        Dim CodiceOspite As String = context.Request.QueryString("CodiceOspite")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer


        Dim DbC As New Cls_Login

        DbC.Utente = UTENTE
        DbC.Chiave = Password
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            Exit Sub
        End If

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.OspitiAccessori)

        cn.Open()

        Dim PaginaHTML As String
        PaginaHTML = ""


        Dim cmd As New OleDbCommand()


        cmd.CommandText = "Select * From Stanze Order by Villa,Reparto,Piano,Stanza,Letto"

        cmd.Connection = cn

        Dim OldStruttura As String = ""
        Dim OldReparto As String = ""
        PaginaHTML = "<ul class=""comment-list"">"

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If Not LettoOccupato(campodb(myPOSTreader.Item("Villa")), campodb(myPOSTreader.Item("Reparto")), campodb(myPOSTreader.Item("Piano")), campodb(myPOSTreader.Item("Stanza")), campodb(myPOSTreader.Item("Letto")), DbC.OspitiAccessori) = True Then

                Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

                XVilla.TipoTabella = "VIL"
                XVilla.CodiceTabella = campodb(myPOSTreader.Item("Villa"))
                XVilla.Leggi(DbC.OspitiAccessori)

                Dim XRep As New Cls_TabelleDescrittiveOspitiAccessori

                XRep.TipoTabella = "REP"
                XRep.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
                XRep.Leggi(DbC.OspitiAccessori)

                PaginaHTML = PaginaHTML & "<li class=""comment"">"
                PaginaHTML = PaginaHTML & "<div>"
                PaginaHTML = PaginaHTML & "<div class=""comment-meta"">"
                PaginaHTML = PaginaHTML & "<p class=""author""><a href=""#"">" & XVilla.Descrizione & "</a></p>"
                PaginaHTML = PaginaHTML & "<p class=""date"">" & XRep.Descrizione & "</p>"
                PaginaHTML = PaginaHTML & "</div>"

                PaginaHTML = PaginaHTML & "<div class=""comment-wrap""><p>"
                PaginaHTML = PaginaHTML & "Piano :" & campodb(myPOSTreader.Item("Piano")) & "<br/>"
                PaginaHTML = PaginaHTML & "Stanza :" & campodb(myPOSTreader.Item("Stanza")) & "<br/>"
                PaginaHTML = PaginaHTML & "Letto :" & campodb(myPOSTreader.Item("Letto")) & "<br/>"

                Dim k As New Cls_TabelleDescrittiveOspitiAccessori

                k.TipoTabella = "TST"
                k.CodiceTabella = campodb(myPOSTreader.Item("Tipologia"))
                k.Leggi(DbC.OspitiAccessori)
                PaginaHTML = PaginaHTML & "Tipologia :" & k.Descrizione & "<br/>"

                k.Descrizione = ""

                k.TipoTabella = "TAR"
                k.CodiceTabella = campodb(myPOSTreader.Item("TIPOARREDO"))
                k.Leggi(DbC.OspitiAccessori)

                PaginaHTML = PaginaHTML & "Arredo :" & k.Descrizione & "<br/>"

                PaginaHTML = PaginaHTML & "</div>"
                PaginaHTML = PaginaHTML & "</div>"
                PaginaHTML = PaginaHTML & "</li>"
            End If
        Loop
        myPOSTreader.Close()
        PaginaHTML = PaginaHTML & "</ul>"



        context.Response.Write(PaginaHTML)
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Function LettoOccupato(ByVal Villa As String, ByVal Reparto As String, ByVal Piano As String, ByVal Stanza As String, ByVal Letto As String, ByVal Connessione As String) As Boolean

        Dim MySql As String
        Dim OldCserv As String = ""
        Dim OldCodOsp As Long = 0

        LettoOccupato = False


        If Piano = "NON INDICATO" And Stanza = "NON INDICATO" Then
            MySql = "SELECT * FROM Movimenti " &
                    " WHERE VILLA = '" & Villa & "'" &
                    " AND REPARTO = '" & Reparto & "'" &
                    " AND LETTO = '" & Letto & "'" &
                    " ORDER BY  Data DESC,ID Desc"
        Else
            MySql = "SELECT * FROM Movimenti " &
                    " WHERE VILLA = '" & Villa & "'" &
                    " AND REPARTO = '" & Reparto & "'" &
                    " AND PIANO = '" & Piano & "'" &
                    " AND STANZA = '" & Stanza & "'" &
                    " AND LETTO = '" & Letto & "'" &
                    " ORDER BY Data DESC,ID Desc"
        End If
        Dim cn As OleDbConnection
        Dim Vuoto As Boolean = False


        cn = New Data.OleDb.OleDbConnection(Connessione)

        cn.Open()

        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                LettoOccupato = True
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class