﻿Public Class Stanza
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim Password As String = context.Request.QueryString("Password")
        Dim Utente As String = context.Request.QueryString("Utente")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If Utente = "" Then Exit Sub
        If Password = "" Then Exit Sub


        Dim DbC As New Cls_LoginParente

        DbC.Utente = Utente
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))

        Dim KS As New Cls_MovimentiStanze

        KS.CodiceOspite = DbC.CodiceOspite
        KS.UltimoMovimentoOspiteSenza(DbC.OspitiAccessori)

        Dim Appoggio As String

        Dim XVilla As New Cls_TabelleDescrittiveOspitiAccessori

        XVilla.TipoTabella = "VIL"
        XVilla.CodiceTabella = KS.Villa
        XVilla.Leggi(DbC.OspitiAccessori)

        Dim XRep As New Cls_TabelleDescrittiveOspitiAccessori

        XRep.TipoTabella = "REP"
        XRep.CodiceTabella = KS.Reparto
        XRep.Leggi(DbC.OspitiAccessori)
        Appoggio = ""
        Appoggio = Appoggio & "<b>Villa :</b><i>" & XVilla.Descrizione & "</i><br/>"
        Appoggio = Appoggio & "<b>Reparto :</b><i>" & XRep.Descrizione & "</i><br/>"


        Appoggio = Appoggio & "<b>Piano :</b><i>" & KS.Piano & "</i><br/>"
        Appoggio = Appoggio & "<b>Stanza :</b><i>" & KS.Stanza & "</i><br/>"
        Appoggio = Appoggio & "<b>Letto :</b><i>" & KS.Letto & "</i><br/>"


        If DbC.Ospiti <> "" Then
            context.Response.Write(Appoggio)
        Else
            context.Response.Write("FALSE")
        End If
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class