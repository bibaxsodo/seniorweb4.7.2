﻿Imports System.Data.OleDb

Public Class OspitiPresenti
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim Password As String = context.Request.QueryString("Password")
        Dim CodiceOspite As String = context.Request.QueryString("CodiceOspite")
        Dim Stringa As String = ""
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim MyTable As New System.Data.DataTable("tabella")

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer


        Dim DbC As New Cls_Login

        DbC.Utente = UTENTE
        DbC.Chiave = Password
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            Exit Sub
        End If

        Dim MySql As String = "SELECT     CENTROSERVIZIO, CODICEOSPITE FROM   MOVIMENTI WHERE  ((SELECT  TOP (1) TIPOMOV FROM    MOVIMENTI AS k WHERE     (CENTROSERVIZIO = MOVIMENTI.CENTROSERVIZIO) AND (CODICEOSPITE = MOVIMENTI.CODICEOSPITE) AND (DATA < ?) ORDER BY DATA DESC, Id) <> '13') GROUP BY CENTROSERVIZIO, CODICEOSPITE"

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()


        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("CentroServizio", GetType(String))
        MyTable.Columns.Add("CodiceOspite", GetType(String))
        MyTable.Columns.Add("Nome", GetType(String))



        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@Data", Now)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim kOsp As New ClsOspite

            kOsp.CodiceOspite = campodb(myPOSTreader.Item("CODICEOSPITE"))
            kOsp.Leggi(DbC.Ospiti, kOsp.CodiceOspite)

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            myriga(1) = kOsp.CodiceOspite
            myriga(2) = kOsp.Nome

            MyTable.Rows.Add(myriga)
        Loop
        cn.Close()

        Stringa = "<ul data-role=""listview"" data-filter=""true"" data-filter-placeholder=""Cerca Ospite..."" data-inset=""true"">"

        Dim foundRowsR As System.Data.DataRow()
        foundRowsR = MyTable.Select("", "Nome")

        Dim i As Integer

        For i = 0 To foundRowsR.Length - 1
            Stringa = Stringa & "<li><a href=""#"" onclick=""apriospite(" & foundRowsR(i).Item("CodiceOspite") & "," & foundRowsR(i).Item("CentroServizio") & ");"">" & foundRowsR(i).Item("Nome") & "</a></li>"
        Next

        Stringa = Stringa & "</ul>"

        context.Response.Write(Stringa)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


End Class