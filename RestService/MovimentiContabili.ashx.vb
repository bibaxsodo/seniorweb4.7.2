﻿Imports System.Data.OleDb

Public Class MovimentiContabili1
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim Password As String = context.Request.QueryString("Password")
        Dim Utente As String = context.Request.QueryString("Utente")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If Utente = "" Then Exit Sub
        If Password = "" Then Exit Sub


        Dim DbC As New Cls_LoginParente

        DbC.Utente = Utente
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))


        Dim UlM As New Cls_Movimenti

        UlM.CodiceOspite = DbC.CodiceOspite
        UlM.UltimaData(DbC.Ospiti, UlM.CodiceOspite)



        Dim KD As New Cls_CentroServizio

        KD.CENTROSERVIZIO = UlM.CENTROSERVIZIO
        KD.Leggi(DbC.Ospiti, KD.CENTROSERVIZIO)

        Dim SqlCond As String = " MastroPartita = " & KD.MASTRO & " And ContoPartita = " & KD.CONTO & " And SottocontoPartita = " & Int((DbC.CodiceOspite * 100) + DbC.CodiceParente)

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(DbC.Generale)


        Dim Appoggio As String = ""



        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn
        cmd.CommandText = "select * From MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta On  MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione Where " & SqlCond & " And  DataRegistrazione >=  ? Order by DataRegistrazione Desc"
        cmd.Parameters.AddWithValue("@Data", DateSerial(Year(Now), 1, 1))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim CauCont As New Cls_CausaleContabile
            Dim TipoDocumento As String
            Dim MySql As String
            Dim TotImp As Double

            CauCont.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            CauCont.Leggi(DbC.TABELLE, CauCont.Codice)
            If CauCont.Tipo = "P" Then
                TipoDocumento = "INC"
            End If


            Appoggio = Appoggio & "<b>Data Movimento :</b><i>" & Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy") & "</i>" & "<br />"
            Appoggio = Appoggio & "<b>Causale Movimento </b><i>" & CauCont.Descrizione & "</i><br />"

            Dim Mov As New Cls_MovimentoContabile


            Mov.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Mov.Leggi(DbC.Generale, Mov.NumeroRegistrazione)

            TotImp = Mov.ImportoDocumento(DbC.TABELLE)

            If TotImp = 0 Then
                MySql = "Select sum(Importo) as totimp From MovimentiContabiliRiga Where  DareAvere = 'D' And Numero = " & campodbn(myPOSTreader.Item("NumeroRegistrazione"))
                Dim cmdG As New OleDbCommand()
                cmdG.CommandText = (MySql)
                cmdG.Connection = cn

                Dim RdG As OleDbDataReader = cmdG.ExecuteReader()
                Do While RdG.Read
                    If IsDBNull(RdG.Item("totimp")) Then
                        TotImp = 0
                    Else
                        TotImp = RdG.Item("totimp")
                    End If
                Loop
                RdG.Close()
            End If

            Appoggio = Appoggio & "<b>Importo :</b><i>" & Format(TotImp, "#,##0.00") & "</i>" & "<br />"
            Appoggio = Appoggio & "<br />"
        Loop
        myPOSTreader.Close()
        cn.Close()


        If DbC.Ospiti <> "" Then
            context.Response.Write(Appoggio)
        Else
            context.Response.Write("FALSE")
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class