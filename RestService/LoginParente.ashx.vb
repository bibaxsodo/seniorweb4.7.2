﻿Public Class LoginParente
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim Password As String = context.Request.QueryString("Password")
        Dim Utente As String = context.Request.QueryString("Utente")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If Utente = "" Then Exit Sub
        If Password = "" Then Exit Sub


        Dim DbC As New Cls_LoginParente

        DbC.Utente = Utente
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))

        If DbC.Ospiti <> "" Then
            context.Response.Write("OK")
        Else
            context.Response.Write("FALSE")
        End If
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class