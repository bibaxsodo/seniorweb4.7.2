﻿Public Class DatiOspite
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim Password As String = context.Request.QueryString("Password")
        Dim CodiceOspite As String = context.Request.QueryString("CodiceOspite")
        Dim centroservizio As String = context.Request.QueryString("centroservizio")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer


        Dim DbC As New Cls_Login

        DbC.Utente = UTENTE
        DbC.Chiave = Password
        DbC.Leggi(context.Application("SENIOR"))
        If DbC.Ospiti = "" Then
            Exit Sub
        End If


        Dim k As New JSON_result


        Dim kOspite As New ClsOspite

        kOspite.Leggi(DbC.Ospiti, CodiceOspite)

        Dim KImp As New Cls_Impegnativa

        KImp.CODICEOSPITE = kOspite.CodiceOspite
        KImp.CENTROSERVIZIO = centroservizio
        KImp.UltimaData(DbC.Ospiti, KImp.CODICEOSPITE, KImp.CENTROSERVIZIO)

        Dim KOspMov As New Cls_MovimentiStanze


        KOspMov.CodiceOspite = kOspite.CodiceOspite
        KOspMov.CentroServizio = centroservizio
        KOspMov.StanzaOspite(DbC.OspitiAccessori)

        Dim DecodificaVilla As New Cls_TabelleDescrittiveOspitiAccessori

        DecodificaVilla.TipoTabella = "VIL"
        DecodificaVilla.CodiceTabella = KOspMov.Villa
        DecodificaVilla.Leggi(DbC.OspitiAccessori)

        Dim DecodificaReparto As New Cls_TabelleDescrittiveOspitiAccessori

        DecodificaReparto.TipoTabella = "REP"
        DecodificaReparto.CodiceTabella = KOspMov.Villa
        DecodificaReparto.Leggi(DbC.OspitiAccessori)


        k.CodiceOspite = kOspite.CodiceOspite
        k.Nome = kOspite.Nome
        If Year(KImp.DATAINIZIO) < 1910 Then
            k.Impegnativa = ""
        Else
            k.Impegnativa = "Dal " & KImp.DATAINIZIO & " Al " & KImp.DATAFINE & "<br/>" & KImp.DESCRIZIONE
        End If


        If DecodificaVilla.Descrizione = "" Then
            k.Stanza = ""
            k.Stanza2 = ""
        Else
            k.Stanza = "Struttura : " & DecodificaVilla.Descrizione & "<br/> Reparto " & DecodificaReparto.Descrizione
            k.Stanza2 = "Piano : " & KOspMov.Piano & " Stanza " & KOspMov.Stanza & " Letto " & KOspMov.Letto
        End If


        Dim kDenaro As New Cls_Denaro

        kDenaro.CodiceOspite = CodiceOspite
        k.SaldoDenaro = Format(kDenaro.Saldo(DbC.OspitiAccessori), "#,##0.00")


        Dim kMedico As New Cls_Medici

        kMedico.CodiceMedico = kOspite.CodiceMedico

        kMedico.Leggi(DbC.Ospiti)

        If IsNothing(kMedico.Nome) Then
            k.Medico = ""
        Else
            k.Medico = kMedico.Nome
        End If

        Dim MyMov As New Cls_Movimenti

        MyMov.CENTROSERVIZIO = centroservizio
        MyMov.CodiceOspite = CodiceOspite
        MyMov.UltimaDataAccoglimento(DbC.Ospiti)

        k.Accoglimento = MyMov.Data


        Dim MyUsl As New Cls_StatoAuto
        MyUsl.CENTROSERVIZIO = centroservizio
        MyUsl.CODICEOSPITE = CodiceOspite
        MyUsl.UltimaData(DbC.Ospiti, MyUsl.CODICEOSPITE, MyUsl.CENTROSERVIZIO)
        k.StatoAuto = MyUsl.STATOAUTO

        If MyUsl.USL <> "" Then
            Dim MyRegione As New ClsUSL

            MyRegione.CodiceRegione = MyUsl.USL
            MyRegione.Leggi(DbC.Ospiti)
            k.USL = MyRegione.Nome
        Else
            k.USL = ""
        End If

        Dim kParenti As New Cls_Parenti


        k.Parenti = ""
        Dim Indice As Integer

        For Indice = 1 To 5
            kParenti.Nome = ""
            kParenti.Leggi(DbC.Ospiti, CodiceOspite, Indice)
            If kParenti.Nome <> "" Then
                If k.Parenti <> "" Then
                    k.Parenti = k.Parenti & "<br/>"
                End If
                k.Parenti = k.Parenti & kParenti.Nome & " <a href=""tel:" & kParenti.Telefono1 & """>" & kParenti.Telefono1 & "</a>"
                If kParenti.Telefono2 <> "" Then
                    k.Parenti = k.Parenti & "<a href=""tel:" & kParenti.Telefono2 & """>" & kParenti.Telefono2 & "</a>"
                End If
            End If
        Next



        Dim Stringa As String = js.Serialize(k)

        context.Response.Write(Stringa)
    End Sub

    Public Class JSON_result
        Public CodiceOspite As Integer
        Public Nome As String
        Public Impegnativa As String
        Public Medico As String
        Public Stanza As String
        Public Stanza2 As String
        Public SaldoDenaro As String
        Public Accoglimento As String
        Public StatoAuto As String
        Public USL As String
        Public Parenti As String
    End Class

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class