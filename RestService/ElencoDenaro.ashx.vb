﻿Imports System.Data.OleDb

Public Class ElencoDenaro
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim Password As String = context.Request.QueryString("Password")
        Dim Utente As String = context.Request.QueryString("Utente")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If Utente = "" Then Exit Sub
        If Password = "" Then Exit Sub


        Dim DbC As New Cls_LoginParente

        DbC.Utente = Utente
        DbC.Password = Password
        DbC.Leggi(context.Application("SENIOR"))


        Dim KD As New Cls_Denaro
        KD.AnnoMovimento = Year(Now)
        KD.CodiceOspite = DbC.CodiceOspite

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(DbC.OspitiAccessori)


        Dim Appoggio As String = ""

        Appoggio = Appoggio & "<b>Saldo Inizio Anno :</b><i>" & Format(KD.SaldoInizioAnno(DbC.OspitiAccessori), "#,##0.00") & "</i>" & "<br /><br />"

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DENARO where AnnoMovimento = " & Year(Now) & " And CodiceOspite = " & KD.CodiceOspite & " Order by DataRegistrazione Desc")
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio = Appoggio & "<b>Data Movimento :</b><i>" & Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy") & "</i>" & "<br />"
            If campodb(myPOSTreader.Item("TipoOperazione")) = "P" Then
                Appoggio = Appoggio & "<b>Movimento di :</b><i>Prelievo</i>" & "<br />"
            Else
                Appoggio = Appoggio & "<b>Movimento di :</b><i>Versamento</i>" & "<br />"
            End If
            Appoggio = Appoggio & "<b>Importo :</b><i>" & Format(campodbn(myPOSTreader.Item("IMPORTO")), "#,##0.00") & "</i>" & "<br />"
            Appoggio = Appoggio & "<br />"
        Loop
        myPOSTreader.Close()
        cn.Close()


        If DbC.Ospiti <> "" Then
            context.Response.Write(Appoggio)
        Else
            context.Response.Write("FALSE")
        End If
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class