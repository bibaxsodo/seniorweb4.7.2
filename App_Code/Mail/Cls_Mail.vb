﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Mail
    Public Id As Long
    Public Descrizione As String
    Public Data As Date
    Public Oggetto As String
    Public Testo As String
    Public IdFirma As Long

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_Mail")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_Mail where ID = " & Id)
        cmd.Connection = cn
        Dim cmdIns As New OleDbCommand()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            cmdIns.CommandText = ("Insert Into Tabella_Mail (Descrizione,Data,Oggetto,Testo,IdFirma)  VALUES (?,?,?,?,?) ")
        Else
            cmdIns.CommandText = ("UPDATE Tabella_Mail SET Descrizione =?,Data= ?,Oggetto = ?,Testo= ?,IdFirma= ? Where ID = " & Id)
        End If


        cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdIns.Parameters.AddWithValue("@Data", Data)
        cmdIns.Parameters.AddWithValue("@Oggetto", Oggetto)
        cmdIns.Parameters.AddWithValue("@Testo", Testo)
        cmdIns.Parameters.AddWithValue("@IdFirma", IdFirma)

        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_Mail where ID = " & Id)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Oggetto = campodb(myPOSTreader.Item("Oggetto"))
            Data = campodb(myPOSTreader.Item("Data"))
            Testo = campodb(myPOSTreader.Item("Testo"))
            IdFirma = Val(campodb(myPOSTreader.Item("IdFirma")))
        End If
        myPOSTreader.Close()
        cn.Close()


    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Tabella_Mail where ID = " & Id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()


    End Sub

End Class
