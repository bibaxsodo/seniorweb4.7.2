﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Tabella_MailingListRiga
    Public Id As Long
    Public IdMailList As Long
    Public CodiceOspite As Long
    Public CodiceParente As Long
    Public CodicePorvincia As String
    Public CodiceComune As String
    Public Regione As String
    Public CodiceDebitoreCreditore As Long
    Public CodiceMedico As String
    Public Mail As String
    Public Utente As String



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function Scrivi(ByRef cn As OleDbConnection) As Boolean
        'Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False



        Dim cmdIns As New OleDbCommand()
        MySql = "INSERT INTO Tabella_MailingLista_Righe (IdMailingList,CodiceOspite,CodiceParente,CodicePorvincia,CodiceComune,Regione,CodiceDebitoreCreditore,CodiceMedico,Mail,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
        cmdIns.CommandText = MySql
        cmdIns.Parameters.AddWithValue("@IdMailList", IdMailList)
        cmdIns.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmdIns.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmdIns.Parameters.AddWithValue("@CodicePorvincia", CodicePorvincia)
        cmdIns.Parameters.AddWithValue("@CodiceComune", CodiceComune)
        cmdIns.Parameters.AddWithValue("@Regione", Regione)
        cmdIns.Parameters.AddWithValue("@CodiceDebitoreCreditore", CodiceDebitoreCreditore)
        cmdIns.Parameters.AddWithValue("@CodiceMedico", CodiceMedico)
        cmdIns.Parameters.AddWithValue("@Mail", Mail)
        cmdIns.Parameters.AddWithValue("@Utente", Utente)
        cmdIns.Parameters.AddWithValue("@IdAnagrafica", Now)        
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        'cn.Close()
        Return True
    End Function

End Class
