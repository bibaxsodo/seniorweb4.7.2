﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Tabella_MailingListTesta
    Public Id As Long
    Public Descrizione As String
    Public Utente As String
    Public DataModifica As Date
    Public Righe(4000) As Cls_Tabella_MailingListRiga




    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_MailingList")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("Descrizione")))
            appoggio.Items(appoggio.Items.Count - 1).Value = campodbN(myPOSTreader.Item("ID"))
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_MailingList where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@ID", Id)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If Not VerReader.Read Then
            Id = 0
            VerReader.Close()
            cn.Close()
            Exit Sub
        End If
        VerReader.Close()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

        End If


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from Tabella_MailingLista_Righe where " & _
                           " IdMailingList = ? Order By ID")
        cmd1.Parameters.AddWithValue("@ID", Id)
        cmd1.Connection = cn


        Dim Indice As Long = 0

        Dim myrd As OleDbDataReader = cmd1.ExecuteReader()
        Do While myrd.Read
            Righe(Indice) = New Cls_Tabella_MailingListRiga
            Righe(Indice).Id = Val(campodb(myrd.Item("ID")))
            Righe(Indice).CodiceOspite = Val(campodb(myrd.Item("CodiceOspite")))
            Righe(Indice).CodiceParente = Val(campodb(myrd.Item("CodiceParente")))
            Righe(Indice).CodicePorvincia = campodb(myrd.Item("CodicePorvincia"))
            Righe(Indice).CodiceComune = campodb(myrd.Item("CodiceComune"))
            Righe(Indice).Regione = campodb(myrd.Item("Regione"))
            Righe(Indice).CodiceDebitoreCreditore = Val(campodb(myrd.Item("CodiceDebitoreCreditore")))
            Righe(Indice).CodiceMedico = campodb(myrd.Item("CodiceMedico"))

            Righe(Indice).Mail = campodb(myrd.Item("Mail"))
            Indice = Indice + 1
        Loop
        myrd.Close()
        cn.Close()
    End Sub



    Sub Delete(ByVal StringaConnessione As String, ByVal Id As Long)

        Dim cn As OleDbConnection


        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_MailingList where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@ID", Id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from Tabella_MailingLista_Righe where " & _
                           "IdMailingList = ? ")
        cmd1.Parameters.AddWithValue("@ID", Id)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()


        cn.Close()

    End Sub
    Function Scrivi(ByVal StringaConnessione As String, ByVal iNNumeroRegistrazione As Long) As Boolean

        Dim cn As OleDbConnection


        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()



        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_MailingList where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@ID", Id)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then

            MySql = "UPDATE Tabella_MailingList SET " & _
             " Descrizione = ?, " & _
             " Utente = ?, " & _
             " DataAggiornamento = ? " & _
             " Where Id= " & Id
        Else

            MySql = "INSERT INTO Tabella_MailingList  (Descrizione,Utente,DataAggiornamento) VALUES (?,?,?)"

        End If


        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = MySql

        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@DataAggiornamento", Now)

        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        If Id = 0 Then
            Dim cmd5 As New OleDbCommand()
            cmd5.CommandText = ("select max(id) as TotID from Tabella_MailingList  where " & _
                               "Utente = ? ")
            cmd5.Parameters.AddWithValue("@Utente", Utente)
            cmd5.Connection = cn
            Dim VerReader1 As OleDbDataReader = cmd5.ExecuteReader()
            If VerReader1.Read Then
                Id = Val(campodb(VerReader1.Item("TotID")))
            End If
            VerReader1.Close()
        End If

        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = ("Delete from Tabella_MailingLista_Righe where " & _
                           "IdMailingList = ? ")
        cmdDelete.Parameters.AddWithValue("@ID", Id)
        cmdDelete.Connection = cn
        cmdDelete.ExecuteNonQuery()


        Dim x As Integer

        For x = 0 To Righe.Length - 1
            If Not IsNothing(Righe(x)) Then
                Righe(x).IdMailList = Id
                If Righe(x).Scrivi(cn) = False Then
                    cn.Close()
                    Return False
                    Exit Function
                End If
            Else
                Exit For
            End If
        Next
        cn.Close()

        Return True

    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbN(ByVal oggetto As Object) As Long
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

End Class
