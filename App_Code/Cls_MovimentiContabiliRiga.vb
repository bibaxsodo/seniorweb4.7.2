Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

<Serializable()> Public Class Cls_MovimentiContabiliRiga
    Public Id As Long
    Public Numero As Long
    Public MastroPartita As Long
    Public ContoPartita As Long
    Public SottocontoPartita As Long
    Public MastroContropartita As Long
    Public ContoContropartita As Long
    Public SottocontoContropartita As Long
    Public Descrizione As String
    Public Importo As Double
    Public DareAvere As String
    Public Segno As String
    Public Tipo As String
    Public CodiceIVA As String
    Public Imponibile As Double
    Public Detraibile As String
    Public Scadenza As Date
    Public Prorata As String
    Public RibaltamentoManuale As String
    Public RigaBollato As Long
    Public RigaDaCausale As Long
    Public CodiceRitenuta As String
    Public Quantita As Long
    Public CausaleDescrittiva As String
    Public MeseRiferimento As Long
    Public AnnoRiferimento As Long
    Public DestinatoVendita As String
    Public Data As Date
    Public RigaRegistrazione As Long
    Public Utente As String
    Public Invisibile As Integer

    Public CentroServizio As String
    Public TipoExtra As String



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Sub New()
        Numero = 0
        MastroPartita = 0
        ContoPartita = 0
        SottocontoPartita = 0
        MastroContropartita = 0
        ContoContropartita = 0
        SottocontoContropartita = 0
        Descrizione = ""

        Importo = 0
        DareAvere = ""
        Segno = ""
        Tipo = ""
        CodiceIVA = ""

        Imponibile = 0

        Detraibile = ""


        Scadenza = Nothing

        Prorata = ""
        RibaltamentoManuale = ""
        RigaBollato = 0
        RigaDaCausale = 0
        CodiceRitenuta = ""
        Quantita = 0
        CausaleDescrittiva = ""
        MeseRiferimento = 0
        AnnoRiferimento = 0
        DestinatoVendita = ""
        Data = Nothing
        RigaRegistrazione = 0
        Id = 0
        Invisibile = 0

        CentroServizio = ""
        TipoExtra = ""
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                           "NumeroRegistrazione = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Numero = Val(campodb(myPOSTreader.Item("Numero")))
            MastroPartita = Val(campodb(myPOSTreader.Item("MastroPartita")))
            ContoPartita = Val(campodb(myPOSTreader.Item("ContoPartita")))
            SottocontoPartita = Val(campodb(myPOSTreader.Item("SottocontoPartita")))
            MastroContropartita = Val(campodb(myPOSTreader.Item("MastroContropartita")))
            ContoContropartita = Val(campodb(myPOSTreader.Item("ContoContropartita")))
            SottocontoContropartita = Val(campodb(myPOSTreader.Item("SottocontoContropartita")))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

            If campodb(myPOSTreader.Item("Importo")) = "" Then
                Importo = 0
            Else
                Importo = CDbl(campodbN(myPOSTreader.Item("Importo")))
            End If
            DareAvere = campodb(myPOSTreader.Item("DareAvere"))
            Segno = campodb(myPOSTreader.Item("Segno"))
            Tipo = campodb(myPOSTreader.Item("Segno"))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))

            If campodb(myPOSTreader.Item("Imponibile")) = "" Then
                Imponibile = 0
            Else
                Imponibile = CDbl(campodbN(myPOSTreader.Item("Imponibile")))
            End If

            Detraibile = campodb(myPOSTreader.Item("Detraibile"))

            If campodb(myPOSTreader.Item("Scadenza")) <> "" Then
                Scadenza = campodb(myPOSTreader.Item("Scadenza"))
            Else
                Scadenza = Nothing
            End If

            Prorata = campodb(myPOSTreader.Item("Prorata"))
            RibaltamentoManuale = campodb(myPOSTreader.Item("RibaltamentoManuale"))
            RigaBollato = Val(campodb(myPOSTreader.Item("RigaBollato")))
            RigaDaCausale = Val(campodb(myPOSTreader.Item("RigaDaCausale")))
            CodiceRitenuta = campodb(myPOSTreader.Item("CodiceRitenuta"))
            Quantita = Val(campodb(myPOSTreader.Item("Quantita")))
            CausaleDescrittiva = campodb(myPOSTreader.Item("CausaleDescrittiva"))
            MeseRiferimento = Val(campodb(myPOSTreader.Item("MeseRiferimento")))
            AnnoRiferimento = Val(campodb(myPOSTreader.Item("AnnoRiferimento")))
            DestinatoVendita = campodb(myPOSTreader.Item("DestinatoVendita"))
            If campodb(myPOSTreader.Item("Data")) <> "" Then
                Data = campodb(myPOSTreader.Item("Data"))
            Else
                Data = Nothing
            End If

            RigaRegistrazione = Val(campodb(myPOSTreader.Item("RigaRegistrazione")))
            Id = Val(campodb(myPOSTreader.Item("Id")))

            Invisibile = Val(campodb(myPOSTreader.Item("Invisibile")))

            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            TipoExtra = campodb(myPOSTreader.Item("TipoExtra"))
        End If
        cn.Close()
    End Sub


    Function Scrivi(ByRef cn As OleDbConnection, ByRef Trans As OleDbTransaction, Optional ByVal ArchivioTemp As Boolean = False) As Boolean
        'Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        'cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        'cn.Open()

        Dim APPOGGIO As String = ""
        Dim I As Integer
        For I = 1 To 10
            APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
        Next
        Dim cmdIns As New OleDbCommand()
        If ArchivioTemp = False Then
            MySql = "INSERT INTO MovimentiContabiliRiga (Numero,UTENTE) VALUES (?,'" & APPOGGIO & "')"
        Else
            MySql = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,UTENTE) VALUES (?,'" & APPOGGIO & "')"
        End If
        cmdIns.CommandText = MySql
        cmdIns.Parameters.AddWithValue("@Numero", Numero)
        cmdIns.Connection = cn
        cmdIns.Transaction = Trans
        cmdIns.ExecuteNonQuery()

        Dim cmdT As New OleDbCommand()
        If ArchivioTemp = False Then
            cmdT.CommandText = ("select MAX(id) from MovimentiContabiliRiga WHERE UTENTE = '" & APPOGGIO & "' And Numero = ?")
        Else
            cmdT.CommandText = ("select MAX(id) from Temp_MovimentiContabiliRiga WHERE UTENTE = '" & APPOGGIO & "' And Numero = ?")
        End If

        cmdT.Parameters.AddWithValue("@Numero", Numero)
        cmdT.Connection = cn
        cmdT.Transaction = Trans
        Dim myR As OleDbDataReader = cmdT.ExecuteReader()
        If myR.Read Then
            Id = myR.Item(0)
        End If
        myR.Close()


        If ArchivioTemp = False Then
            MySql = "UPDATE MovimentiContabiliRiga SET "
        Else
            MySql = "UPDATE Temp_MovimentiContabiliRiga SET "
        End If
        MySql = MySql & "MastroPartita = ?," & _
                "ContoPartita = ?," & _
                "SottocontoPartita = ?," & _
                "MastroContropartita = ?," & _
                "ContoContropartita = ?," & _
                "SottocontoContropartita = ?," & _
                "Descrizione = ?," & _
                "Importo = ?," & _
                "DareAvere = ?," & _
                "Segno = ?," & _
                "Tipo = ?, " & _
                "CodiceIVA = ?," & _
                "Imponibile = ?," & _
                "Detraibile = ?," & _
                "Prorata = ?," & _
                "RibaltamentoManuale = ?," & _
                "RigaBollato = ?," & _
                "RigaDaCausale = ?," & _
                "CodiceRitenuta = ?," & _
                "Quantita = ?," & _
                "CausaleDescrittiva = ?," & _
                "MeseRiferimento = ?," & _
                "AnnoRiferimento = ?," & _
                "DestinatoVendita = ?," & _
                "RigaRegistrazione = ?, " & _
                "UTENTE = ?, " & _
                "DataAggiornamento = ?," & _
                "Invisibile = ?, " & _
                "CentroServizio  = ?, " & _
                "TipoExtra  = ? " & _
                " Where Numero = ? And id = ? "


        '"Scadenza = ?," & _

        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = MySql

        cmd1.Parameters.AddWithValue("@MastroPartita", MastroPartita)
        cmd1.Parameters.AddWithValue("@ContoPartita", ContoPartita)
        cmd1.Parameters.AddWithValue("@SottocontoPartita", SottocontoPartita)
        cmd1.Parameters.AddWithValue("@MastroContropartita", MastroContropartita)
        cmd1.Parameters.AddWithValue("@ContoContropartita", ContoContropartita)
        cmd1.Parameters.AddWithValue("@SottocontoContropartita", SottocontoContropartita)
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Importo", Importo)
        cmd1.Parameters.AddWithValue("@DareAvere", DareAvere)
        cmd1.Parameters.AddWithValue("@Segno", Segno)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@CodiceIVA", CodiceIVA)
        cmd1.Parameters.AddWithValue("@Imponibile", Imponibile)
        cmd1.Parameters.AddWithValue("@Detraibile", Detraibile)
        'cmd1.Parameters.AddWithValue("@Scadenza", Scadenza)
        cmd1.Parameters.AddWithValue("@Prorata", Prorata)
        cmd1.Parameters.AddWithValue("@RibaltamentoManuale", RibaltamentoManuale)
        cmd1.Parameters.AddWithValue("@RigaBollato", RigaBollato)
        cmd1.Parameters.AddWithValue("@RigaDaCausale", RigaDaCausale)
        cmd1.Parameters.AddWithValue("@CodiceRitenuta", CodiceRitenuta)
        cmd1.Parameters.AddWithValue("@Quantita", Quantita)
        cmd1.Parameters.AddWithValue("@CausaleDescrittiva", CausaleDescrittiva)
        cmd1.Parameters.AddWithValue("@MeseRiferimento", MeseRiferimento)
        cmd1.Parameters.AddWithValue("@AnnoRiferimento", AnnoRiferimento)
        cmd1.Parameters.AddWithValue("@DestinatoVendita", DestinatoVendita)
        cmd1.Parameters.AddWithValue("@RigaRegistrazione", RigaRegistrazione)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd1.Parameters.AddWithValue("@Invisibile", Invisibile)
        cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd1.Parameters.AddWithValue("@TipoExtra", TipoExtra)
        cmd1.Parameters.AddWithValue("@Numero", Numero)
        cmd1.Parameters.AddWithValue("@Id", Id)
        cmd1.Transaction = Trans
        cmd1.Connection = cn
        Try
            cmd1.ExecuteNonQuery()
        Catch ex As Exception
            cn.Close()
            Return False
            Exit Function
        End Try
        'cn.Close()
        Return True
    End Function


End Class
