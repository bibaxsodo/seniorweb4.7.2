﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Documentazione
    Public CODICEOSPITE As Long
    Public NomeFile As String
    Public DATA As Date
    Public DESCRIZIONE As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Documentazione where " & _
                           "CODICEOSPITE = ? And Nome = ?")
        cmd.Parameters.AddWithValue("@CODICEOSPITE", CODICEOSPITE)
        cmd.Parameters.AddWithValue("@NomeFile", NomeFile)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CODICEOSPITE = Val(campodb(myPOSTreader.Item("CODICEOSPITE")))
            NomeFile = campodb(myPOSTreader.Item("Nome"))
            DESCRIZIONE = campodb(myPOSTreader.Item("Descrizione"))
            DATA = Nothing
            If campodb(myPOSTreader.Item("Data")) <> "" Then
                DATA = campodb(myPOSTreader.Item("Data"))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String, ByVal MyTable As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim I As Long


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdDelete As New OleDbCommand()

        cmdDelete.CommandText = ("delete  from Documentazione where " & _
                   "CODICEOSPITE = ? ")
        cmdDelete.Parameters.AddWithValue("@CODICEOSPITE", CODICEOSPITE)
        cmdDelete.Connection = cn
        cmdDelete.ExecuteNonQuery()


        For I = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(I).Item(0)) Then
                If Trim(MyTable.Rows(I).Item(0)) <> "" And IsDate(MyTable.Rows(I).Item(2)) Then
                    Dim cmd As New OleDbCommand()

                    cmd.CommandText = ("INSERT INTO Documentazione (CODICEOSPITE,NOME,DESCRIZIONE,DATA)" & _
                                       " VALUES (?,?,?,?)")
                    cmd.Parameters.AddWithValue("@CODICEOSPITE", CODICEOSPITE)
                    cmd.Parameters.AddWithValue("@NomeFile", MyTable.Rows(I).Item(0))
                    cmd.Parameters.AddWithValue("@Descrizione", MyTable.Rows(I).Item(1))
                    Dim DataDoc As Date = MyTable.Rows(I).Item(2)
                    cmd.Parameters.AddWithValue("@Data", DataDoc)
                    cmd.Connection = cn
                    cmd.ExecuteNonQuery()
                End If
            End If
        Next

        cn.Close()
    End Sub
End Class
