﻿Imports Microsoft.VisualBasic

Public Class Cls_Grid
    Public TextMatrix(1000, 100) As String
    Public Rows As Long
    Public Row As Long
    Public Col As Long
    Public BackColor As Long
    Public ForeColor As Long

    Public Function Text() As String
        Return TextMatrix(Row, Col)
    End Function

    Public Sub Text(ByVal Testo As String)
        TextMatrix(Row, Col) = Testo
    End Sub

    Public Sub New()
        Dim x As Integer
        Dim y As Integer
        For x = 0 To 100
            For y = 0 To 100
                TextMatrix(y, x) = ""
            Next
        Next
        Rows = 0
    End Sub
    Public Sub AddItem(ByVal Lista As String)
        Dim Linea(100) As String
        Linea = SplitWords(Lista)

        Dim Ind As Long

        Rows = Rows + 1
        For Ind = 0 To Linea.Length - 1
            TextMatrix(Rows, Ind) = Linea(Ind)
        Next
    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, vbTab)
    End Function
End Class
