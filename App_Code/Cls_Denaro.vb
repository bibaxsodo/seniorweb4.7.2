﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Denaro
    Public CodiceOspite As Long
    Public AnnoMovimento As Long
    Public NumeroMovimento As Long
    Public DataRegistrazione As Date
    Public TipoOperazione As String
    Public Importo As Double
    Public Descrizione As String
    Public Ripartizione As String
    Public Parente As Long
    Public TipoAddebito As String
    Public Utente As String
    Public AnnoCompetenza As Integer
    Public MeseCompetenza As Integer



    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DENARO where AnnoMovimento = " & AnnoMovimento & " And CodiceOspite = " & CodiceOspite & " AND  NumeroMovimento =  " & NumeroMovimento)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            codiceospite = campodbn(myPOSTreader.Item("codiceospite"))
            AnnoMovimento = campodbn(myPOSTreader.Item("AnnoMovimento"))
            NumeroMovimento = campodbn(myPOSTreader.Item("NumeroMovimento"))
            DataRegistrazione = campodb(myPOSTreader.Item("DataRegistrazione"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            Importo = campodbn(myPOSTreader.Item("Importo"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))
            Parente = Val(campodb(myPOSTreader.Item("Parente")))
            TipoAddebito = campodb(myPOSTreader.Item("TipoAddebito"))

            AnnoCompetenza = Val(campodb(myPOSTreader.Item("AnnoCompetenza")))
            MeseCompetenza = Val(campodb(myPOSTreader.Item("MeseCompetenza")))
        End If
        myPOSTreader.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from DENARO where  CodiceOspite = " & CodiceOspite & " And AnnoMovimento = ? And NumeroMovimento = " & NumeroMovimento)
        cmd.Parameters.AddWithValue("@AnnoMovimento", AnnoMovimento)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Function SaldoInizioAnno(ByVal StringaConnessione As String) As Double
        Dim cn As OleDbConnection
        Dim MySql1 As String
        Dim MySql As String

        SaldoInizioAnno = 0
        MySql1 = ""

        MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                " WHERE CodiceOspite = " & CodiceOspite & _
                " AND  TipoOperazione  = 'V' And AnnoMovimento < " & AnnoMovimento

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            SaldoInizioAnno = campodbn(myPOSTreader.Item("Totale"))
        End If
        myPOSTreader.Close()

        MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                " WHERE CodiceOspite = " & CodiceOspite & _
                " AND  TipoOperazione  = 'P' And AnnoMovimento < " & AnnoMovimento

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = (MySql)
        cmd1.Connection = cn
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            SaldoInizioAnno = SaldoInizioAnno - campodbn(myPOSTreader1.Item("Totale"))
        End If
        myPOSTreader1.Close()
        cn.Close()
    End Function

    Public Function Saldo(ByVal StringaConnessione As String) As Double
        Dim cn As OleDbConnection
        Dim MySql1 As String
        Dim MySql As String

        Saldo = 0
        MySql1 = ""

        MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                " WHERE CodiceOspite = " & CodiceOspite & _
                " AND  TipoOperazione  = 'V'"

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Saldo = campodbn(myPOSTreader.Item("Totale"))
        End If
        myPOSTreader.Close()

        MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                " WHERE CodiceOspite = " & CodiceOspite & _
                " AND  TipoOperazione  = 'P'" & MySql1

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = (MySql)
        cmd1.Connection = cn
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            Saldo = Saldo - campodbn(myPOSTreader1.Item("Totale"))
        End If
        myPOSTreader1.Close()
        cn.Close()
    End Function

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DENARO where  CodiceOspite = " & CodiceOspite & "  And AnnoMovimento = " & AnnoMovimento & " And NumeroMovimento  = " & NumeroMovimento)        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE DENARO SET TipoOperazione = ?,Importo = ?,Descrizione = ?,Ripartizione = ?,Parente = ?,Utente = ?,DataAggiornamento = ?,TipoAddebito = ?,DataRegistrazione = ?,AnnoCompetenza = ?,MeseCompetenza = ?  " & _
                    " WHERE  CodiceOspite = " & CodiceOspite & " AND NumeroMovimento  = " & NumeroMovimento & " AND AnnoMovimento = " & AnnoMovimento
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
            cmdw.Parameters.AddWithValue("@Importo", Importo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Ripartizione", Ripartizione)
            cmdw.Parameters.AddWithValue("@Parente", Parente)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@TipoAddebito", TipoAddebito)
            cmdw.Parameters.AddWithValue("@DataRegistrazione", DataRegistrazione)
            cmdw.Parameters.AddWithValue("@AnnoCompetenza", AnnoCompetenza)
            cmdw.Parameters.AddWithValue("@MeseCompetenza", MeseCompetenza)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            If NumeroMovimento = 0 Then
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("select MAX(NumeroMovimento) from DENARO where  AnnoMovimento = " & AnnoMovimento)
                cmd1.Connection = cn
                Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                If myPOSTreader1.Read Then
                    If Not IsDBNull(myPOSTreader1.Item(0)) Then
                        NumeroMovimento = myPOSTreader1.Item(0) + 1
                    Else
                        NumeroMovimento = 1
                    End If
                Else
                    NumeroMovimento = 1
                End If
            End If
            MySql = "INSERT INTO DENARO (TipoOperazione,Importo,Descrizione,Ripartizione,Parente,CodiceOspite,DataRegistrazione,NumeroMovimento,AnnoMovimento,Utente,DataAggiornamento,TipoAddebito,AnnoCompetenza,MeseCompetenza) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
            cmdw.Parameters.AddWithValue("@Importo", Importo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Ripartizione", Ripartizione)
            cmdw.Parameters.AddWithValue("@Parente", Parente)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@DataRegistrazione", DataRegistrazione)
            cmdw.Parameters.AddWithValue("@NumeroMovimento", NumeroMovimento)
            cmdw.Parameters.AddWithValue("@AnnoMovimento", AnnoMovimento)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@TipoAddebito", TipoAddebito)
            cmdw.Parameters.AddWithValue("@AnnoCompetenza", AnnoCompetenza)
            cmdw.Parameters.AddWithValue("@MeseCompetenza", MeseCompetenza)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal AnnoMovimento As Long, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DENARO where AnnoMovimento = " & AnnoMovimento & " And CodiceOspite = " & codiceospite & " Order by DataRegistrazione Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DataRegistrazione", GetType(Date))
        Tabella.Columns.Add("TipoOperazione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Ripartizione", GetType(String))
        Tabella.Columns.Add("Parente", GetType(String))
        Tabella.Columns.Add("NumeroMovimento", GetType(Long))
        Tabella.Columns.Add("Periodo", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            If campodb(myPOSTreader.Item("TipoOperazione")) = "P" Then
                myriga(1) = "Prelievo"
            Else
                myriga(1) = "Versamento"
            End If
            myriga(2) = Format(campodbn(myPOSTreader.Item("IMPORTO")), "#,##0.00")
            myriga(3) = myPOSTreader.Item("Descrizione")
            myriga(4) = myPOSTreader.Item("Ripartizione")
            myriga(5) = myPOSTreader.Item("Parente")
            myriga(6) = myPOSTreader.Item("NumeroMovimento")
            myriga(7) = campodb(myPOSTreader.Item("AnnoCompetenza")) & "/" & Format(campodbn(myPOSTreader.Item("MeseCompetenza")), "00")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
        
    End Sub
End Class
