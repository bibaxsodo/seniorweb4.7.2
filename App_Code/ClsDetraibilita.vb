Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class ClsDetraibilita
    Public Codice As String
    Public Descrizione As String
    Public Aliquota As Double
    Public Automatico As String
    Public GiroACosto As String
    Public IVAGiroACostoMastro As Long
    Public IVAGiroACostoConto As Long
    Public IVAGiroACostoSottoconto As Long
    Public IVAGiroACostoAliquota As Double
    Public Prorata As Long

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Long


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Delete from Detraibilita where " & _
                           "Codice  = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)

        cmd.Connection = cn

        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub


    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from MovimentiContabiliRiga Where Detraibile = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Function MaxDetraibilita(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection
        Dim MaxRegistroVar As Integer = 1
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Codice) from Detraibilita ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxRegistroVar = Val(campodb(myPOSTreader.Item(0))) + 1
        End If
        myPOSTreader.Close()
        cn.Close()

        Return MaxRegistroVar
    End Function

    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Detraibilita where " & _
                           "Descrizione  Like ? ")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Codice = campodb(VerReader.Item("Codice"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
            Aliquota = campodb(VerReader.Item("Aliquota"))
            Automatico = campodb(VerReader.Item("Automatico"))
            GiroACosto = campodb(VerReader.Item("GiroACosto"))
            IVAGiroACostoMastro = campodbN(VerReader.Item("IVAGiroACostoMastro"))
            IVAGiroACostoConto = campodbN(VerReader.Item("IVAGiroACostoConto"))
            IVAGiroACostoSottoconto = campodbN(VerReader.Item("IVAGiroACostoSottoconto"))
            IVAGiroACostoAliquota = campodbN(VerReader.Item("IVAGiroACostoAliquota"))
            Prorata = campodbN(VerReader.Item("Prorata"))
        End If
        VerReader.Close()
        cn.Close()
    End Sub
    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Detraibilita where " & _
                           "Codice  = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Codice = campodb(VerReader.Item("Codice"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
            Aliquota = campodb(VerReader.Item("Aliquota"))
            Automatico = campodb(VerReader.Item("Automatico"))
            GiroACosto = campodb(VerReader.Item("GiroACosto"))
            IVAGiroACostoMastro = campodbN(VerReader.Item("IVAGiroACostoMastro"))
            IVAGiroACostoConto = campodbN(VerReader.Item("IVAGiroACostoConto"))
            IVAGiroACostoSottoconto = campodbN(VerReader.Item("IVAGiroACostoSottoconto"))
            IVAGiroACostoAliquota = campodbN(VerReader.Item("IVAGiroACostoAliquota"))
            Prorata = campodbN(VerReader.Item("Prorata"))
        End If
        VerReader.Close()
        cn.Close()
    End Sub
    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Esiste As Boolean

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Detraibilita where " & _
                           "Codice  = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)


        cmd.Connection = cn

        Esiste = False
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Esiste = True
        End If
        VerReader.Close()

        Dim MySql As String


        If Esiste = True Then
            MySql = "UPDATE Detraibilita SET " & _
                    " Descrizione  = ?, " & _
                    " Aliquota  = ?, " & _
                    " Automatico  = ?, " & _
                    " GiroACosto  = ?, " & _
                    " IVAGiroACostoMastro  = ?, " & _
                    " IVAGiroACostoConto  = ?, " & _
                    " IVAGiroACostoSottoconto  = ?, " & _
                    " IVAGiroACostoAliquota  = ?, " & _
                    " Prorata  = ? " & _
                    " Where Codice = ? "
        Else
            MySql = "INSERT INTO Detraibilita (Descrizione,Aliquota,Automatico,GiroACosto,IVAGiroACostoMastro,IVAGiroACostoConto,IVAGiroACostoSottoconto,IVAGiroACostoAliquota,Prorata,Codice)  VALUES " & _
                                             "(?,?,?,?,?,?,?,?,?,?)"
        End If


        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Aliquota", Aliquota)
        cmd1.Parameters.AddWithValue("@Automatico", Automatico)
        cmd1.Parameters.AddWithValue("@GiroACosto", GiroACosto)
        cmd1.Parameters.AddWithValue("@IVAGiroACostoMastro", IVAGiroACostoMastro)
        cmd1.Parameters.AddWithValue("@IVAGiroACostoConto", IVAGiroACostoConto)
        cmd1.Parameters.AddWithValue("@IVAGiroACostoSottoconto", IVAGiroACostoSottoconto)
        cmd1.Parameters.AddWithValue("@IVAGiroACostoAliquota", IVAGiroACostoAliquota)
        cmd1.Parameters.AddWithValue("@Prorata", Prorata)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub UpDateDropBoxCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Detraibilita order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub
    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Detraibilita order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub
End Class
