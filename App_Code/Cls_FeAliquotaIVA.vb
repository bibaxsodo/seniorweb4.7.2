﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_FeAliquotaIVA
    Public ALIQUOTA As String
    Public NATURA As String
    Public CODICE As String



    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Public Sub DeleteID(ByVal StringaConnessione As String, ByVal id As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from FeAliquotaIVA Where id = ?")
        cmd.Parameters.AddWithValue("@ID", id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Public Sub LeggiID(ByVal StringaConnessione As String, ByVal id As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from FeAliquotaIVA Where id = ?")
        cmd.Parameters.AddWithValue("@ID", id)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ALIQUOTA = campodb(myPOSTreader.Item("ALIQUOTA"))
            NATURA = campodb(myPOSTreader.Item("NATURA"))
            CODICE = campodb(myPOSTreader.Item("CODICE"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If NATURA = "" Then
            cmd.CommandText = ("select * from FeAliquotaIVA Where ALIQUOTA = ? AND (NATURA = '' OR NATURA IS NULL)")
            cmd.Parameters.AddWithValue("@ALIQUOTA", ALIQUOTA)
        End If
        If ALIQUOTA = "" Then
            cmd.CommandText = ("select * from FeAliquotaIVA Where NATURA =? AND (ALIQUOTA = '' OR ALIQUOTA IS NULL)")
            cmd.Parameters.AddWithValue("@NATURA", NATURA)
        End If

        If ALIQUOTA <> "" And NATURA <> "" Then
            cmd.CommandText = ("select * from FeAliquotaIVA Where ALIQUOTA = ? AND NATURA =?")
            cmd.Parameters.AddWithValue("@ALIQUOTA", ALIQUOTA)
            cmd.Parameters.AddWithValue("@NATURA", NATURA)
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CODICE = campodb(myPOSTreader.Item("CODICE"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String, ByVal id As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from FeAliquotaIVA Where id = ?")
        cmd.Parameters.AddWithValue("@ID", id)
        cmd.Connection = cn

        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Dim CmqUP As New OleDbCommand

            CmqUP.Connection = cn
            CmqUP.CommandText = "UPDATE FeAliquotaIVA SET CODICE = ?,ALIQUOTA = ?,NATURA =? WHERE ID = ?"
            CmqUP.Parameters.AddWithValue("@CODICE", CODICE)
            CmqUP.Parameters.AddWithValue("@ALIQUOTA", ALIQUOTA)
            CmqUP.Parameters.AddWithValue("@NATURA", NATURA)
            CmqUP.Parameters.AddWithValue("@ID", id)
            CmqUP.ExecuteNonQuery()
        Else
            Dim CmqADD As New OleDbCommand

            CmqADD.Connection = cn
            CmqADD.CommandText = "INSERT INTO FeAliquotaIVA  (CODICE,ALIQUOTA,NATURA) VALUES (?,?,?)"
            CmqADD.Parameters.AddWithValue("@CODICE", CODICE)
            CmqADD.Parameters.AddWithValue("@ALIQUOTA", ALIQUOTA)
            CmqADD.Parameters.AddWithValue("@NATURA", NATURA)
            CmqADD.ExecuteNonQuery()

        End If
        myPOSTreader.Close()


        cn.Close()
    End Sub


End Class
