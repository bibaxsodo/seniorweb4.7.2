﻿Imports Microsoft.VisualBasic

Public Class Cls_ImportoInLettere
    Public Function ImportoInTesto(ByVal numero As Double, ByVal MaxCar As Integer) As String
        Dim NumeriDecimali As Double
        Dim Unita(20) As String
        Dim Decine(10) As String
        Dim Molto(10) As String
        Dim Molti(10) As String
        Dim II As Long, lint As Long, i As Long
        Dim cc1 As Long
        Dim cc2 As Long
        Dim cc3 As Long
        Dim over As Boolean
        Dim Stringa As String
        Dim Decimali As String
        Dim intero As String

        numero = Math.Abs(numero)

        NumeriDecimali = Math.Round(numero - Int(numero), 2) * 100
        numero = Int(numero)
        Unita(1) = "UNO"
        Unita(2) = "DUE"
        Unita(3) = "TRE"
        Unita(4) = "QUATTRO"
        Unita(5) = "CINQUE"
        Unita(6) = "SEI"
        Unita(7) = "SETTE"
        Unita(8) = "OTTO"
        Unita(9) = "NOVE"
        Unita(10) = "DIECI"
        Unita(11) = "UNDICI"
        Unita(12) = "DODICI"
        Unita(13) = "TREDICI"
        Unita(14) = "QUATTORDICI"
        Unita(15) = "QUINDICI"
        Unita(16) = "SEDICI"
        Unita(17) = "DICIASSETTE"
        Unita(18) = "DICIOTTO"
        Unita(19) = "DICIANNOVE"

        Decine(2) = "VENTI"
        Decine(3) = "TRENTA"
        Decine(4) = "QUARANTA"
        Decine(5) = "CINQUANTA"
        Decine(6) = "SESSANTA"
        Decine(7) = "SETTANTA"
        Decine(8) = "OTTANTA"
        Decine(9) = "NOVANTA"

        Molto(1) = "UNMILIARDO"
        Molto(2) = "UNMILIONE"
        Molto(3) = "MILLE"
        Molto(4) = ""

        Molti(1) = "MILIARDI"
        Molti(2) = "MILIONI"
        Molti(3) = "MILA"
        Molti(4) = ""

        intero = Int(numero)
        lint = Len(intero)
        For i = lint To 11
            intero = "0" & intero
        Next i
        Decimali = numero - Int(numero)
        over = False
        II = 0
        For i = 1 To 12 Step 3
            II = II + 1
            cc1 = 0
            cc2 = 0
            cc3 = 0
            If over = False Then
                If Not Mid(intero, i, 3) = "000" Then
                    Stringa = ""
                    cc1 = Val(Mid(intero, i, 1))
                    If Mid(intero, i + 1, 1) < "2" Then
                        cc3 = Val(Mid(intero, i + 1, 2))
                    Else
                        cc2 = Val(Mid(intero, i + 1, 1))
                        cc3 = Val(Mid(intero, i + 2, 1))
                    End If
                    If Mid(intero, i, 3) = "001" Then
                        If Molto(II) = "" Then Stringa = Stringa & Unita(cc3)
                        Stringa = Stringa & Molto(II)
                    Else
                        If cc1 > 0 Then
                            If Not cc1 = 1 Then
                                Stringa = Stringa & Unita(cc1)
                            End If
                            Stringa = Stringa & "CENTO"
                        End If
                        If cc2 > 0 Then
                            Stringa = Stringa & Decine(cc2)
                        End If
                        If cc3 > 0 Then
                            Stringa = Stringa & Unita(cc3)
                        End If
                        Stringa = Stringa & Molti(II)
                    End If
                    If Len(ImportoInTesto) + Len(Stringa) > MaxCar Then
                        over = True
                        ImportoInTesto = ImportoInTesto & Mid(intero, i, 3)
                    Else
                        ImportoInTesto = ImportoInTesto & Stringa
                    End If
                End If
            Else
                ImportoInTesto = ImportoInTesto & Mid(intero, i, 3)
            End If
        Next i

        ImportoInTesto = ImportoInTesto & "/" & Format(NumeriDecimali, "00")
    End Function

End Class
