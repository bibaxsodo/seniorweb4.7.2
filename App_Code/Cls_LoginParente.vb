﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports BCrypt.Net

Public Class Cls_LoginParente

    Public Utente As String
    Public Password As String
    Public CodiceOspite As Long
    Public CodiceParente As Long
    Public Societa As Long

    Public Denaro As Long
    Public Contabilita As Long
    Public Stanza As Long
    Public Diurno As Long

    Public Ospiti As String
    Public TABELLE As String
    Public Generale As String
    Public Finanziaria As String
    Public OspitiAccessori As String
    Public Turni As String
    Public EMail As String


    Public Ip As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub ModificaPassword(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        If Utente = "" Then Exit Sub

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
     
        Dim cmdW As New OleDbCommand()
        cmdW.CommandText = ("update UTENTIAPP set password = ?  where " & _
                           "UTENTE = ? ")

        cmdW.Parameters.AddWithValue("@Chiave", Password)
        cmdW.Parameters.AddWithValue("@UTENTE", Utente)
        cmdW.Connection = cn
     
        cn.Close()


    End Sub

    Sub ScriviDatiParente(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        If Utente = "" Then Exit Sub

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTIAPP where " & _
                           "CodiceOspite = ? And CodiceParente = ? ")

        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            MySql = "INSERT INTO UTENTIAPP (Utente,Password,Societa,Denaro,Contabilita,Stanza,Diurno,CodiceOspite,CodiceParente) VALUES (?,?,?,?,?,?,?,?,?)"
        Else
            MySql = "UPDATE UTENTIAPP SET Utente = ?,Password = ?,Societa = ?,Denaro = ? ,Contabilita = ?,Stanza = ?,Diurno = ? WHERE  CodiceOspite = ? AND  CodiceParente = ?"
        End If
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql

        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@Password", Password)
        cmd1.Parameters.AddWithValue("@Societa", Societa)
        cmd1.Parameters.AddWithValue("@Denaro", Denaro)
        cmd1.Parameters.AddWithValue("@Contabilita", Contabilita)

        cmd1.Parameters.AddWithValue("@Stanza", Stanza)
        cmd1.Parameters.AddWithValue("@Diurno", Diurno)
        cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function PresenteUtente(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection


        PresenteUtente = True

        If Utente = "" Then Exit Function

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTIAPP where " & _
                           "Utente = ? And ( CodiceOspite <> ? or CodiceParente <> ? or Societa <> ?) ")

        cmd.Parameters.AddWithValue("@Utente", Utente)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Societa", Societa)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            PresenteUtente = True
        Else
            PresenteUtente = False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function
    Sub LeggiDatiParente(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTIAPP where " & _
                           "CodiceOspite = ? And CodiceParente = ? And Societa = ? ")

        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Societa", Societa)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Utente = campodb(myPOSTreader.Item("Utente"))
            Password = campodb(myPOSTreader.Item("Password"))
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            Societa = campodb(myPOSTreader.Item("Societa"))

            Denaro = Val(campodb(myPOSTreader.Item("Denaro")))
            Contabilita = Val(campodb(myPOSTreader.Item("Contabilita")))
            Stanza = Val(campodb(myPOSTreader.Item("Stanza")))
            Diurno = Val(campodb(myPOSTreader.Item("Diurno")))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        If Utente = "" Then Exit Sub

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTIAPP where " & _
                           "UTENTE = ? And Password = ? ")

        cmd.Parameters.AddWithValue("@UTENTE", Utente)
        cmd.Parameters.AddWithValue("@Chiave", Password)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Utente = campodb(myPOSTreader.Item("Utente"))
            Password = campodb(myPOSTreader.Item("Password"))
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            Societa = campodb(myPOSTreader.Item("Societa"))

            Denaro = Val(campodb(myPOSTreader.Item("Denaro")))
            Contabilita = Val(campodb(myPOSTreader.Item("Contabilita")))
            Stanza = Val(campodb(myPOSTreader.Item("Stanza")))
            Diurno = Val(campodb(myPOSTreader.Item("Diurno")))

            Dim cmd2 As New OleDbCommand()

            cmd2.CommandText = ("select * from ArchivioClienti where CLIENTE = ? ")

            cmd2.Parameters.AddWithValue("@CLIENTE", Societa)
            cmd2.Connection = cn

            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Ospiti = campodb(myPOSTreader2.Item("Ospiti"))
                TABELLE = campodb(myPOSTreader2.Item("TABELLE"))
                Generale = campodb(myPOSTreader2.Item("Generale"))
                Finanziaria = campodb(myPOSTreader2.Item("Finanziaria"))
                OspitiAccessori = campodb(myPOSTreader2.Item("OspitiAccessori"))
                Turni = campodb(myPOSTreader2.Item("Turni"))

            End If
            myPOSTreader2.Close()


            If Ip <> "" Then
                Dim CmdInsLog As New OleDbCommand

                CmdInsLog.CommandText = "INSERT INTO Log_Login (Login,OKAccess,Ip,Sistema,DataOra) VALUES (?,1,?,?,?)"
                CmdInsLog.Parameters.AddWithValue("@Login", Utente)
                CmdInsLog.Parameters.AddWithValue("@Ip", Ip)
                CmdInsLog.Parameters.AddWithValue("@Sistema", "APP")
                CmdInsLog.Parameters.AddWithValue("@DataOra", Now)


                CmdInsLog.Connection = cn

                CmdInsLog.ExecuteNonQuery()
            End If
        Else
            If Ip <> "" Then
                Dim CmdInsLog As New OleDbCommand

                CmdInsLog.CommandText = "INSERT INTO Log_Login (Login,OKAccess,Ip,Sistema,DataOra) VALUES (?,0,?,?,?)"
                CmdInsLog.Parameters.AddWithValue("@Login", Utente)
                CmdInsLog.Parameters.AddWithValue("@Ip", Ip)
                CmdInsLog.Parameters.AddWithValue("@Sistema", "APP")
                CmdInsLog.Parameters.AddWithValue("@DataOra", Now)
                CmdInsLog.Connection = cn
                CmdInsLog.ExecuteNonQuery()
            End If

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class


