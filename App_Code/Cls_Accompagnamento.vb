﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Accompagnamento

    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public DATA As Date
    Public Accompagnamento As Integer    
    Public UTENTE As String

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Public Sub LeggiAData(ByVal StringaConnessione As String, ByVal DataInzio As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from Accompagnamento Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@DATA", DataInzio)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CENTROSERVIZIO = campodb(myPOSTreader.Item("DATA"))
            CODICEOSPITE = campodb(myPOSTreader.Item("CODICEOSPITE"))
            DATA = campodbD(myPOSTreader.Item("DATA"))
            Accompagnamento = campodb(myPOSTreader.Item("Accompagnamento"))
            UTENTE = campodb(myPOSTreader.Item("UTENTE"))

        End If
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Accompagnamento Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Accompagnamento Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", DATA)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Accompagnamento where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO Accompagnamento (CentroServizio,CodiceOspite,DATA,Accompagnamento,UTENTE,DATAAGGIORNAMENTO) VALUES (?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                If IsDate(Tabella.Rows(i).Item(0)) Then
                    xData = Tabella.Rows(i).Item(0)
                    cmdw.Parameters.AddWithValue("@DATA", xData)
                Else
                    cmdw.Parameters.AddWithValue("@DATA", System.DBNull.Value)
                End If
     

                cmdw.Parameters.AddWithValue("@Accompagnamento", Tabella.Rows(i).Item(1))
            

                cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
                cmdw.Parameters.AddWithValue("@AGGIORNAMENTO", Now)

                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Accompagnamento where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA= ?")
        cmd.Parameters.AddWithValue("@DATA", DATA)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            MySql = "UPDATE Accompagnamento SET DATA= ?,Accompagnamento=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And DATAINIZIO = ? "

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@DATA", DATA)
            cmdw.Parameters.AddWithValue("@Accompagnamento", Accompagnamento)            
            cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            
            MySql = "INSERT INTO Accompagnamento (CentroServizio,CodiceOspite,DATA,Accompagnamento,UTENTE,DATAAGGIORNAMENTO) VALUES (?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@DATA", DATA)
            cmdw.Parameters.AddWithValue("@Accompagnamento", Accompagnamento)            
            cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    
    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Accompagnamento where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By DATA")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DATA", GetType(String))
        Tabella.Columns.Add("Accompagnamento", GetType(Integer))
        

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            If Val(campodb(myPOSTreader.Item("Accompagnamento"))) = 0 Then
                myriga(1) = 0
            Else
                myriga(1) = 1
            End If
            

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub
End Class
