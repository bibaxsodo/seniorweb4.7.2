﻿Imports Microsoft.VisualBasic
Imports SeniorWebApplication
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_StampaFatture
    Public DC_OSPITE As String
    Public DC_TABELLE As String
    Public DC_GENERALE As String

    Function quanteVolte(ByVal str1 As String, ByVal str2 As String) As Long
        Dim strArray(100) As String
        strArray = Split(str1, str2)
        quanteVolte = UBound(strArray)
    End Function

    Public Function DecodificaMese(ByVal Mese As Long) As String
        Select Case Mese
            Case 1
                DecodificaMese = "Gennaio"
            Case 2
                DecodificaMese = "Febbraio"
            Case 3
                DecodificaMese = "Marzo"
            Case 4
                DecodificaMese = "Aprile"
            Case 5
                DecodificaMese = "Maggio"
            Case 6
                DecodificaMese = "Giugno"
            Case 7
                DecodificaMese = "Luglio"
            Case 8
                DecodificaMese = "Agosto"
            Case 9
                DecodificaMese = "Settembre"
            Case 10
                DecodificaMese = "Ottobre"
            Case 11
                DecodificaMese = "Novembre"
            Case 12
                DecodificaMese = "Dicembre"
        End Select
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Sub CreaRecordStampa(ByVal TxtAnno As Long, ByVal Mese As Long, ByVal Anticipo As Boolean, ByVal DocumentoDal As Long, ByVal DocumentoAl As Long, ByVal AnnoRegistrazione As Integer, ByVal RegistroIVA As Long, ByVal Campo1 As String, ByVal Campo2 As String, ByVal Campo3 As String, ByVal TxtProva As Long, ByVal Data As Object, Optional ByVal NumeroRegistrazioneDal As Long = 0, Optional ByVal NumeroRegistrazioneAl As Long = 0, Optional ByVal MastroCliente As Integer = 0, Optional ByVal ContoCliente As Integer = 0, Optional ByVal SottoContoCliente As Integer = 0)

        Dim Rs_PdC As New ADODB.Recordset
        Dim Rs_Pagato As New ADODB.Recordset
        Dim Rs_Legame As New ADODB.Recordset
        Dim Rs_Clienti As OleDbDataReader
        Dim RsRegione As New ADODB.Recordset
        Dim WImponibile(5) As Double
        Dim WImposta(5) As Double
        Dim WAliquota(5) As String
        Dim StampeDb As New ADODB.Connection
        Dim GiorniInFattRs As New ADODB.Recordset
        Dim VImportoInTesto As Double
        Dim RsTot As New ADODB.Recordset
        Dim RsMovimenti As New ADODB.Recordset
        Dim RsTipoligia As New ADODB.Recordset
        Dim Ragr(10) As String
        Dim ConRagr(10) As Integer
        Dim ImpConRagr(10) As Double
        Dim MySql As String
        Dim Riferimento As String = ""
        Dim GeneraleDb As New ADODB.Connection
        Dim OspitiDb As New ADODB.Connection
        Dim OldRegistrazione As Long
        Dim XProv As String
        Dim XCom As String
        Dim TotaleAnticipo As Double
        Dim CausaleContabile As String
        Dim TotSql As String
        Dim ImportoPagato As Double
        Dim WTotaleMerce As Double
        Dim WImportoSconto As Double
        Dim FlagParente As Integer
        Dim DareAvere As String
        Dim Totale As Double
        Dim Fondo As String
        Dim condz As String
        Dim RegolaQuota As New Cls_CalcoloRette
        Dim FnVB6 As New Cls_FunzioniVB6
        Dim CampoTipoOperazioneAnticipo As String
        Dim RigaDaCausale As Long
        Dim QUOTAGIORNALIERA As Double
        Dim NoteProvincia As String
        Dim NoteComune As String
        Dim Regione As String
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MyStatoAuto As String, Bollette As String
        Dim importo As Double
        Dim OMastro As Long
        Dim OConto As Long
        Dim OSottoconto As Long
        Dim CodiceOspite As Long
        Dim ImportoRiga As Double
        Dim DesMese As String = DecodificaMese(Mese)
        Dim MyNQuota As String
        Dim NQuota As Double
        Dim Sotto As Long
        Dim MyQuota As String
        Dim TUscite As String
        Dim DataNascita As Date
        Dim Imponibile As Double
        Dim Indice As Long
        Dim Oldfattura As Long
        Dim mYdESC As String
        Dim PredentePA As Long
        Dim ProgressTot As Long = 0
        Dim X As Long
        Dim Anno As String
        Dim Appoggio As String
        Dim myCodiceOspite As Long
        Dim xNum As Long
        Dim CentroServizioVB As New Cls_CentroServizio
        Dim DescrizioneMagiera As Boolean = False
        Dim ClsImpLettere As New Cls_ImportoInLettere
        Dim Stampa As New StampeGenerale
        Dim MyRecordStampaT As System.Data.DataRow
        Dim MyRecordStampaR As System.Data.DataRow
        Dim NumeroRigaRegistrazione As Long
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = Data


        Dim cnGenerale As OleDbConnection
        Dim cnOspiti As OleDbConnection



        RegolaQuota.STRINGACONNESSIONEDB = SessioneTP("DC_OSPITE")

        DC_OSPITE = SessioneTP("DC_OSPITE")
        DC_TABELLE = SessioneTP("DC_TABELLE")
        DC_GENERALE = SessioneTP("DC_GENERALE")


        RegolaQuota.ApriDB(SessioneTP("DC_OSPITE"), SessioneTP("DC_OSPITIACCESSORI"))
        FnVB6.StringaOspiti = SessioneTP("DC_OSPITE")
        FnVB6.StringaTabelle = SessioneTP("DC_TABELLE")
        FnVB6.StringaGenerale = SessioneTP("DC_GENERALE")
        FnVB6.ApriDB()

        GeneraleDb.Open(SessioneTP("DC_GENERALE"))
        OspitiDb.Open(SessioneTP("DC_OSPITE"))


        cnGenerale = New Data.OleDb.OleDbConnection(SessioneTP("DC_GENERALE"))

        cnOspiti = New Data.OleDb.OleDbConnection(SessioneTP("DC_OSPITE"))

        cnOspiti.Open()
        cnGenerale.Open()
        Dim FiltroServizi As String = ""
        'Dim FiltroServizi As String = "( "


        If TxtProva = 0 Then
            Dim Param As New Cls_Parametri

            Param.LeggiParametri(DC_OSPITE)

            If Param.StampaSoloCSAttivi = 1 Then

                FiltroServizi = "( "
                Dim CmdFiltroServizi As New OleDbCommand
                CmdFiltroServizi.Connection = cnOspiti
                CmdFiltroServizi.CommandText = "Select * From TABELLACENTROSERVIZIO Where Emissione = 0"

                Dim RdFiltroServizi As OleDbDataReader = CmdFiltroServizi.ExecuteReader
                Do While RdFiltroServizi.Read
                    If FiltroServizi <> "( " Then
                        FiltroServizi = FiltroServizi & " Or "
                    End If
                    FiltroServizi = FiltroServizi & " MovimentiContabiliTesta.CentroServizio = '" & campodb(RdFiltroServizi.Item("CENTROSERVIZIO")) & "'"
                Loop
                RdFiltroServizi.Close()
                FiltroServizi = " And " & FiltroServizi & ")"
            End If
        End If

        MySql = ""
        If TxtProva = 1 Then
            MySql = "SELECT Temp_MovimentiContabiliTesta.*, Temp_MovimentiContabiliRiga.*,Temp_MovimentiContabiliRiga.Descrizione AS DescrizioneRiga,Temp_MovimentiContabiliTesta.CausaleContabile , Temp_MovimentiContabiliTesta.Descrizione AS DescrizioneTesta, Temp_MovimentiContabiliTesta.Tipologia " &
                    " FROM (Temp_MovimentiContabiliRiga INNER JOIN Temp_MovimentiContabiliTesta ON Temp_MovimentiContabiliRiga.Numero = Temp_MovimentiContabiliTesta.NumeroRegistrazione) " &
                    " WHERE Numeroregistrazione  >= " & NumeroRegistrazioneDal &
                    " AND Numeroregistrazione   <= " & NumeroRegistrazioneAl &
                    " And RigaDaCausale > 0 "

            If RegistroIVA > 0 Then
                MySql = MySql & " AND Temp_MovimentiContabiliTesta.RegistroIVA = " & RegistroIVA
            End If
            MySql = MySql & " ORDER BY Numeroregistrazione, RigaDaCausale"
        Else
            MySql = "SELECT MovimentiContabiliTesta.*, MovimentiContabiliRiga.*,MovimentiContabiliRiga.Descrizione AS DescrizioneRiga,MovimentiContabiliTesta.CausaleContabile , MovimentiContabiliTesta.Descrizione AS DescrizioneTesta, MovimentiContabiliTesta.Tipologia " &
                    " FROM (MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) " &
                    " WHERE AnnoCompetenza = " & TxtAnno &
                    " AND MeseCompetenza = " & Mese &
                    " AND NumeroProtocollo >= " & DocumentoDal &
                    " AND NumeroProtocollo <= " & DocumentoAl &
                    " AND AnnoProtocollo = " & AnnoRegistrazione &
                    " AND MovimentiContabiliTesta.RegistroIVA = " & RegistroIVA &
                    " And RigaDaCausale > 0 " & FiltroServizi
            If TxtProva = -1 Then
                MySql = "SELECT MovimentiContabiliTesta.*, MovimentiContabiliRiga.*,MovimentiContabiliRiga.Descrizione AS DescrizioneRiga,MovimentiContabiliTesta.CausaleContabile , MovimentiContabiliTesta.Descrizione AS DescrizioneTesta, MovimentiContabiliTesta.Tipologia " &
                    " FROM (MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) " &
                    " WHERE Numeroregistrazione  >= " & NumeroRegistrazioneDal &
                    " AND Numeroregistrazione   <= " & NumeroRegistrazioneAl &
                    " And RigaDaCausale > 0 "

                If RegistroIVA > 0 Then
                    MySql = MySql & " AND MovimentiContabiliTesta.RegistroIVA = " & RegistroIVA
                End If
            End If

            If MastroCliente > 0 Then
                MySql = "SELECT MovimentiContabiliTesta.*, MovimentiContabiliRiga.*,MovimentiContabiliRiga.Descrizione AS DescrizioneRiga,MovimentiContabiliTesta.CausaleContabile , MovimentiContabiliTesta.Descrizione AS DescrizioneTesta, MovimentiContabiliTesta.Tipologia " &
                        " FROM (MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) " &
                        " WHERE AnnoProtocollo = " & AnnoRegistrazione &
                        " AND (Select count(*) From MovimentiContabiliRiga as CercaUtente Where CercaUtente.Numero  = MovimentiContabiliTesta.NumeroRegistrazione And CercaUtente.MastroPartita = " & MastroCliente &
                        " And CercaUtente.ContoPartita = " & ContoCliente &
                        " And CercaUtente.SottoContoPartita = " & SottoContoCliente &
                        " ) > 0 "
            End If

            If Anticipo = True Then
                MySql = MySql & " AND (FatturaDiAnticipo <> 'S' OR FatturaDiAnticipo Is Null)"
            End If

            MySql = MySql & " ORDER BY NumeroProtocollo,NumeroRegistrazione, RigaDaCausale"
        End If


        ProgressTot = 0

        Dim CmdTot As New OleDbCommand
        CmdTot.Connection = cnGenerale
        CmdTot.CommandText = MySql

        Dim RdTot As OleDbDataReader = CmdTot.ExecuteReader
        Do While RdTot.Read
            ProgressTot = ProgressTot + 1
        Loop
        RdTot.Close()

        If ProgressTot = 0 Then
            SessioneTP("CampoErrori") = "Nessun documento estratto"
            SessioneTP("CampoProgressBar") = 999

            System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Nessun documento estratto"
            System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
            Exit Sub
        End If




        Dim CmdRs As New OleDbCommand
        CmdRs.Connection = cnGenerale
        CmdRs.CommandText = MySql

        Dim MyRs As OleDbDataReader = CmdRs.ExecuteReader

        xNum = 0

        Do While MyRs.Read
            If xNum = 0 Then
                OldRegistrazione = campodbN(MyRs.Item("NumeroRegistrazione"))

                MySql = "SELECT * FROM PianoConti " &
                      " WHERE Mastro = " & campodbN(MyRs.Item("MastroPartita")) &
                      " AND Conto = " & campodbN(MyRs.Item("ContoPartita")) &
                      " AND Sottoconto = " & campodbN(MyRs.Item("SottocontoPartita"))
                Rs_PdC.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If Rs_PdC.EOF Then
                    Rs_PdC.Close()

                    SessioneTP("CampoErrori") = "Inesistente su Piano dei Conti (1) : " & campodbN(MyRs.Item("MastroPartita")) & " / " & campodbN(MyRs.Item("ContoPartita")) & " / " & campodbN(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodbN(MyRs.Item("NumeroRegistrazione"))
                    SessioneTP("CampoProgressBar") = 999

                    System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Inesistente su Piano dei Conti (1) : " & campodbN(MyRs.Item("MastroPartita")) & " / " & campodbN(MyRs.Item("ContoPartita")) & " / " & campodbN(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodbN(MyRs.Item("NumeroRegistrazione"))
                    System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
                    MyRs.Close()
                    Exit Sub
                End If


                If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "O" Then

                    MySql = "SELECT * FROM AnagraficaComune " &
                          " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) &
                          " And Tipologia = 'O'"
                Else
                    If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "P" Then
                        MySql = "SELECT * FROM AnagraficaComune " &
                             " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) &
                             " AND CodiceParente = " & FnVB6.CampoOspiteParenteMCS("CodiceParente", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))

                    Else
                        If Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 1, 1) = "C" Then
                            XProv = Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 2, 3)
                            XCom = Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 5, 3)


                            MySql = "SELECT * FROM AnagraficaComune " &
                                    " WHERE CodiceProvincia = '" & XProv & "'" &
                                    " AND CodiceComune = '" & XCom & "'" &
                                    " And  Tipologia = 'C'"

                        Else
                            If Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 1, 1) = "R" Then
                                MySql = "SELECT * FROM AnagraficaComune " &
                                       " WHERE MastroCliente = " & campodbN(MyRs.Item("MastroPartita")) &
                                       " AND ContoCliente = " & campodbN(MyRs.Item("ContoPartita")) &
                                       " AND SottocontoCliente = " & campodbN(MyRs.Item("SottocontoPartita")) & " And  Tipologia = 'R' "
                                If Len(campodb(MyRs.Item("Tipologia")) & Space(3)) >= 5 Then
                                    MySql = MySql & " AND CodiceRegione = '" & Trim(Mid(campodb(MyRs.Item("Tipologia")) & Space(3), 2, 4)) & "'"
                                End If
                            Else
                                MySql = "SELECT * FROM AnagraficaComune " &
                                        " WHERE MastroCliente = " & campodbN(MyRs.Item("MastroPartita")) &
                                        " AND ContoCliente = " & campodbN(MyRs.Item("ContoPartita")) &
                                        " AND SottocontoCliente = " & campodbN(MyRs.Item("SottocontoPartita")) & " And  Tipologia <> 'D'"

                            End If
                        End If
                    End If
                End If
                Rs_PdC.Close()

                Dim MyCmdClienti As New OleDbCommand

                MyCmdClienti.Connection = cnOspiti
                MyCmdClienti.CommandText = MySql


                Rs_Clienti = MyCmdClienti.ExecuteReader

                If Not Rs_Clienti.Read Then
                    Rs_Clienti.Close()

                    SessioneTP("CampoErrori") = "Inesistente su Anagrafica Comune (1) : " & campodb(MyRs.Item("Tipologia")) & " --- " & campodbN(MyRs.Item("MastroPartita")) & " / " & campodbN(MyRs.Item("ContoPartita")) & " / " & campodbN(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodbN(MyRs.Item("NumeroRegistrazione"))
                    SessioneTP("CampoProgressBar") = 999


                    System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Inesistente su Anagrafica Comune (1) : " & campodb(MyRs.Item("Tipologia")) & " --- " & campodb(MyRs.Item("MastroPartita")) & " / " & campodb(MyRs.Item("ContoPartita")) & " / " & campodb(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodb(MyRs.Item("NumeroRegistrazione"))
                    System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
                    MyRs.Close()
                    Exit Sub
                End If
                For I = 1 To 5
                    WImponibile(I) = 0
                    WAliquota(I) = ""
                    WImposta(I) = 0
                Next I

                Dim xAPPO As String = campodb(MyRs.Item("CausaleContabile"))


                MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow


                NumeroRigaRegistrazione = 0
                TotaleAnticipo = 0
                MyRecordStampaT.Item("TipoDocumento") = TipoDocumento(campodb(MyRs.Item("CausaleContabile")))
                MyRecordStampaT.Item("RegistroIVA") = campodbN(MyRs.Item("RegistroIVA"))

                Dim DCrEG As New Cls_RegistroIVA

                DCrEG.Tipo = campodbN(MyRs.Item("RegistroIVA"))
                DCrEG.Leggi(SessioneTP("DC_TABELLE"), DCrEG.Tipo)
                MyRecordStampaT.Item("IndicatoreRegistro") = DCrEG.IndicatoreRegistro


                MyRecordStampaT.Item("Campo1") = Campo1
                MyRecordStampaT.Item("Campo2") = Campo2
                MyRecordStampaT.Item("Campo3") = Campo3
                MyRecordStampaT.Item("Intestatario") = ""

                CausaleContabile = campodb(MyRs.Item("CausaleContabile"))
            End If


            If CentroServizioVB.CENTROSERVIZIO <> campodb(MyRs.Item("CentroServizio")) Then
                CentroServizioVB.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                CentroServizioVB.Leggi(SessioneTP("DC_OSPITE"), CentroServizioVB.CENTROSERVIZIO)
            End If

            Riferimento = CentroServizioVB.DescrizioneRigaInFattura

            SessioneTP("CampoProgressBar") = Math.Round(100 / ProgressTot * xNum, 2)
            System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = Math.Round(100 / ProgressTot * xNum, 2)

            xNum = xNum + 1
            If OldRegistrazione <> campodbN(MyRs.Item("NumeroRegistrazione")) Then
                DescrizioneMagiera = False
                If FnVB6.CampoCausaleContabile(CausaleContabile, "TipoDocumento") = "CR" Then
                    ImportoPagato = 0

                    Dim M As New Cls_Legami

                    MyRecordStampaT.Item("ImportoPagato") = M.TotaleLegame(DC_GENERALE, OldRegistrazione)

                End If

                MyRecordStampaT.Item("Imponibile1") = WImponibile(1)
                If WAliquota(1) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA1") = WAliquota(1) & " - " & FnVB6.CampoIVA(WAliquota(1), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta1") = WImposta(1)

                MyRecordStampaT.Item("Imponibile2") = WImponibile(2)
                If WAliquota(2) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA2") = WAliquota(2) & " - " & FnVB6.CampoIVA(WAliquota(2), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta2") = WImposta(2)

                MyRecordStampaT.Item("Imponibile3") = WImponibile(3)
                If WAliquota(3) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA3") = WAliquota(3) & " - " & FnVB6.CampoIVA(WAliquota(3), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta3") = WImposta(3)

                MyRecordStampaT.Item("Imponibile4") = WImponibile(4)
                If WAliquota(4) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA4") = WAliquota(4) & " - " & FnVB6.CampoIVA(WAliquota(4), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta4") = WImposta(4)

                MyRecordStampaT.Item("Imponibile5") = WImponibile(5)
                If WAliquota(5) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA5") = WAliquota(5) & " - " & FnVB6.CampoIVA(WAliquota(5), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta5") = Math.Abs(WImposta(5))
                VImportoInTesto = WImponibile(1) + WImponibile(2) + WImponibile(3) + WImponibile(4) + WImposta(1) + WImposta(2) + WImposta(3) + WImposta(4)


                MyRecordStampaT.Item("ImportoInTesto") = ClsImpLettere.ImportoInTesto(VImportoInTesto, 50)
                MyRecordStampaT.Item("ImportoAnticipo") = TotaleAnticipo
                MyRecordStampaT.Item("CampoOrdinamento") = Mid(MyRecordStampaT.Item("RagioneSociale") & Space(50), 1, 50) & MyRecordStampaT.Item("NumeroFattura")

                Dim Log As New Cls_LogPrivacy


                Log.LogPrivacy(SessioneTP("SENIOR"), SessioneTP("CLIENTE"), SessioneTP("UTENTE"), SessioneTP("UTENTEIMPER"), "OSPITIWEB_STAMPADOCUMENTI_ASPX", 0, 0, "", "", MyRecordStampaT.Item("NumeroRegistrazione"), "", "", "", "")

                Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)
                OldRegistrazione = campodb(MyRs.Item("NumeroRegistrazione"))
                WTotaleMerce = 0
                WImportoSconto = 0
                MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow


                NumeroRigaRegistrazione = 0
                MyRecordStampaT.Item("Campo1") = Campo1
                MyRecordStampaT.Item("Campo2") = Campo2
                MyRecordStampaT.Item("Campo3") = Campo3

                TotaleAnticipo = 0
                MyRecordStampaT.Item("TipoDocumento") = TipoDocumento(campodb(MyRs.Item("CausaleContabile")))
                MyRecordStampaT.Item("RegistroIVA") = campodbN(MyRs.Item("RegistroIVA"))


                Dim DCrEG As New Cls_RegistroIVA

                DCrEG.Tipo = campodbN(MyRs.Item("RegistroIVA"))
                DCrEG.Leggi(SessioneTP("DC_TABELLE"), DCrEG.Tipo)
                MyRecordStampaT.Item("IndicatoreRegistro") = DCrEG.IndicatoreRegistro

                CausaleContabile = campodb(MyRs.Item("CausaleContabile"))

                MySql = "SELECT * FROM PianoConti " &
                        " WHERE Mastro = " & campodbN(MyRs.Item("MastroPartita")) &
                        " AND Conto = " & campodbN(MyRs.Item("ContoPartita")) &
                        " AND Sottoconto = " & campodbN(MyRs.Item("SottocontoPartita"))
                Rs_PdC.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If Rs_PdC.EOF Then
                    Rs_PdC.Close()


                    SessioneTP("CampoErrori") = "Inesistente su Piano dei Conti : " & campodb(MyRs.Item("MastroPartita")) & " / " & campodb(MyRs.Item("ContoPartita")) & " / " & campodb(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodb(MyRs.Item("NumeroRegistrazione"))
                    SessioneTP("CampoProgressBar") = 999

                    System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Inesistente su Piano dei Conti : " & campodb(MyRs.Item("MastroPartita")) & " / " & campodb(MyRs.Item("ContoPartita")) & " / " & campodb(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodb(MyRs.Item("NumeroRegistrazione"))
                    System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
                    MyRs.Close()
                    Exit Sub
                End If
                FlagParente = 0
                If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "O" Then
                    MySql = "SELECT * FROM AnagraficaComune " &
                            " WHERE CodiceOspite = " & Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))) &
                            " And Tipologia = 'O'"
                Else
                    If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "P" Then

                        MySql = "SELECT * FROM AnagraficaComune " &
                                " WHERE CodiceOspite = " & Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))) &
                                " AND CodiceParente = " & Val(FnVB6.CampoOspiteParenteMCS("CodiceParente", campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))))

                        FlagParente = 1
                    Else
                        If Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 1, 1) = "C" Or Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 1, 1) = "J" Then
                            XProv = Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 2, 3)
                            XCom = Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 5, 3)

                            MySql = "SELECT * FROM AnagraficaComune " &
                                    " WHERE CodiceProvincia = '" & XProv & "'" &
                                    " AND CodiceComune = '" & XCom & "'" &
                                    " And  Tipologia = 'C'"
                        Else
                            If Mid(campodb(MyRs.Item("Tipologia")) & Space(1), 1, 1) = "R" Then
                                MySql = "SELECT * FROM AnagraficaComune " &
                                        " WHERE MastroCliente = " & campodbN(MyRs.Item("MastroPartita")) &
                                        " AND ContoCliente = " & campodbN(MyRs.Item("ContoPartita")) &
                                        " AND SottocontoCliente = " & campodbN(MyRs.Item("SottocontoPartita")) & " And  Tipologia = 'R'"
                                If Len(campodb(MyRs.Item("Tipologia")) & Space(3)) >= 5 Then
                                    MySql = MySql & " AND CodiceRegione = '" & Trim(Mid(campodb(MyRs.Item("Tipologia")) & Space(3), 2, 4)) & "'"
                                End If
                            Else
                                MySql = "SELECT * FROM AnagraficaComune " &
                                        " WHERE MastroCliente = " & campodbN(MyRs.Item("MastroPartita")) &
                                        " AND ContoCliente = " & campodbN(MyRs.Item("ContoPartita")) &
                                        " AND SottocontoCliente = " & campodbN(MyRs.Item("SottocontoPartita")) & " And  Tipologia <> 'D'"
                            End If
                        End If
                    End If
                End If
                Rs_PdC.Close()


                Dim MyCmdClienti As New OleDbCommand

                MyCmdClienti.Connection = cnOspiti
                MyCmdClienti.CommandText = MySql


                Rs_Clienti = MyCmdClienti.ExecuteReader

                If Not Rs_Clienti.Read Then
                    Rs_Clienti.Close()
                    Try
                        SessioneTP("CampoErrori") = "Inesistente su Anagrafica Comune (2) : " & campodbN(MyRs.Item("Tipologia")) & " --- " & campodbN(MyRs.Item("MastroPartita")) & " / " & campodbN(MyRs.Item("ContoPartita")) & " / " & campodbN(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodbN(MyRs.Item("NumeroRegistrazione")) & " <br /> " & MySql
                        SessioneTP("CampoProgressBar") = 999


                        System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Inesistente su Anagrafica Comune (2) : " & campodbN(MyRs.Item("Tipologia")) & " --- " & campodbN(MyRs.Item("MastroPartita")) & " / " & campodbN(MyRs.Item("ContoPartita")) & " / " & campodbN(MyRs.Item("SottocontoPartita")) & " Registrazione : " & campodbN(MyRs.Item("NumeroRegistrazione")) & " <br /> " & MySql
                        System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999

                    Catch ex As Exception

                    End Try
                    MyRs.Close()
                    Exit Sub
                End If
                For I = 1 To 5
                    WImponibile(I) = 0
                    WAliquota(I) = ""
                    WImposta(I) = 0
                Next I
            End If
            If campodb(MyRs.Item("Tipo")) = "CF" Then
                DareAvere = campodb(MyRs.Item("DareAvere"))
                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                    If campodb(Rs_Clienti.Item("Specializazione")) = "S" Then
                        MyRecordStampaT.Item("RagioneSociale") = campodb(Rs_Clienti.Item("Nome"))
                    Else
                        MyRecordStampaT.Item("RagioneSociale") = "COMUNE DI " & campodb(Rs_Clienti.Item("Nome"))
                    End If
                Else
                    MyRecordStampaT.Item("RagioneSociale") = campodb(Rs_Clienti.Item("Nome"))
                End If
                SessioneTP("RagioneSocialeFattura") = MyRecordStampaT.Item("RagioneSociale")
                System.Web.HttpRuntime.Cache("RagioneSocialeFattura" + SessioneTP.SessionID) = MyRecordStampaT.Item("RagioneSociale")

                If campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                    MyRecordStampaT.Item("CodiceCliente") = campodbN(Rs_Clienti.Item("CodiceOspite"))

                    MyRecordStampaT.Item("OspiteRiferimento") = FnVB6.CampoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), "NOME")
                    MyRecordStampaT.Item("CodiceFiscaleOspiteRiferimento") = FnVB6.CampoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), "CODICEFISCALE")


                    MyRecordStampaT.Item("StatoAuto") = FnVB6.StatoAutoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), campodbD(MyRs.Item("DataRegistrazione")))


                    MyRecordStampaT.Item("EMail") = campodb(Rs_Clienti.Item("RESIDENZATELEFONO1"))

                    MyRecordStampaT.Item("TipologiaOspite") = ""
                    Dim CmdTO As New OleDbCommand
                    CmdTO.Connection = cnOspiti
                    CmdTO.CommandText = "Select * From TipologiaOspite Where CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite"))

                    Dim RdTipoligia As OleDbDataReader = CmdTO.ExecuteReader
                    If RdTipoligia.Read Then
                        MyRecordStampaT.Item("TipologiaOspite") = IIf(IsNothing(FnVB6.DecodificaTabella("TPU", campodb(RdTipoligia.Item("Tipologia")))), "", FnVB6.DecodificaTabella("TPU", campodb(RdTipoligia.Item("Tipologia"))))
                    End If
                    RdTipoligia.Close()

                    Dim SMEstesaP As New ClsModalitaPagamento

                    SMEstesaP.DESCRIZIONE = ""
                    SMEstesaP.ModalitaPagamento = campodb(MyRs.Item("CODICEPAGAMENTO"))
                    SMEstesaP.LeggiModalitaFattura(DC_OSPITE)

                    If SMEstesaP.DESCRIZIONE = "" Then
                        SMEstesaP.Codice = campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))
                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio


                        KCs.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                        KCs.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                        KCs.CodiceParente = campodbN(Rs_Clienti.Item("CodiceParente"))
                        KCs.Leggi(DC_OSPITE)
                        If Trim(KCs.ModalitaPagamento) <> "" Then
                            SMEstesaP.Codice = KCs.ModalitaPagamento
                        End If


                        SMEstesaP.Leggi(DC_OSPITE)
                    End If

                    Dim MImpoP As New Cls_ImportoParente

                    MImpoP.CODICEOSPITE = campodbN(Rs_Clienti.Item("CodiceOspite"))
                    MImpoP.CODICEPARENTE = campodbN(Rs_Clienti.Item("CodiceParente"))
                    MImpoP.TIPOIMPORTO = ""
                    MImpoP.UltimaData(DC_OSPITE, MImpoP.CODICEOSPITE, campodb(MyRs.Item("CentroServizio")), MImpoP.CODICEPARENTE)

                    MyRecordStampaT.Item("TipoRettaOspiteParente") = MImpoP.TIPOIMPORTO

                    MyRecordStampaT.Item("TIPOOPERAZIONE") = campodb(Rs_Clienti.Item("TIPOOPERAZIONE"))

                    Dim MOspite As New Cls_DatiOspiteParenteCentroServizio

                    MOspite.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                    MOspite.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                    MOspite.CodiceParente = campodbN(Rs_Clienti.Item("CodiceParente"))
                    MOspite.Leggi(DC_OSPITE)
                    If MOspite.TipoOperazione <> "" Then
                        MyRecordStampaT.Item("TIPOOPERAZIONE") = MOspite.TipoOperazione
                    End If


                    MyRecordStampaT.Item("ModalitaPagamento") = SMEstesaP.DESCRIZIONE

                    If SMEstesaP.DESCRIZIONE <> "" Then
                        MyRecordStampaT.Item("ModalitaPagamentoEstesa") = SMEstesaP.DescrizioneEstesa
                    End If

                End If
                If campodb(Rs_Clienti.Item("Tipologia")) = "O" Then
                    MyRecordStampaT.Item("CodiceCliente") = campodbN(Rs_Clienti.Item("CodiceOspite"))

                    MyRecordStampaT.Item("OspiteRiferimento") = campodb(Rs_Clienti.Item("Nome"))
                    MyRecordStampaT.Item("CodiceFiscaleOspiteRiferimento") = campodb(Rs_Clienti.Item("CODICEFISCALE"))
                    MyRecordStampaT.Item("StatoAuto") = FnVB6.StatoAutoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), campodbD(MyRs.Item("DataRegistrazione")))

                    MyRecordStampaT.Item("EMail") = campodb(Rs_Clienti.Item("RESIDENZATELEFONO3"))
                    If Trim(campodb(Rs_Clienti.Item("TELEFONO4"))).IndexOf("@") > 0 Then
                        MyRecordStampaT.Item("EMail") = Trim(campodb(Rs_Clienti.Item("TELEFONO4")))
                    End If

                    If campodb(Rs_Clienti.Item("Nome")) = "Martinelli Giovanni" Then
                        Dim Prova As String


                        Prova = MyRecordStampaT.Item("EMail")
                    End If

                    MyRecordStampaT.Item("TipologiaOspite") = ""
                    Dim CmdTO As New OleDbCommand
                    CmdTO.Connection = cnOspiti
                    CmdTO.CommandText = "Select * From TipologiaOspite Where CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite"))

                    Dim RdTipoligia As OleDbDataReader = CmdTO.ExecuteReader
                    If RdTipoligia.Read Then
                        MyRecordStampaT.Item("TipologiaOspite") = IIf(IsNothing(FnVB6.DecodificaTabella("TPU", campodb(RdTipoligia.Item("Tipologia")))), "", FnVB6.DecodificaTabella("TPU", campodb(RdTipoligia.Item("Tipologia"))))
                    End If
                    RdTipoligia.Close()


                    Dim MImpoO As New Cls_ImportoOspite

                    MImpoO.CODICEOSPITE = campodbN(Rs_Clienti.Item("CodiceOspite"))
                    MImpoO.Tipo = ""
                    MImpoO.UltimaData(DC_OSPITE, MImpoO.CODICEOSPITE, campodb(MyRs.Item("CentroServizio")))

                    MyRecordStampaT.Item("TipoRettaOspiteParente") = MImpoO.Tipo

                    Dim SMEstesa As New ClsModalitaPagamento

                    SMEstesa.DESCRIZIONE = ""
                    SMEstesa.ModalitaPagamento = campodb(MyRs.Item("CODICEPAGAMENTO"))
                    SMEstesa.LeggiModalitaFattura(DC_OSPITE)

                    If SMEstesa.DESCRIZIONE = "" Then
                        SMEstesa.Codice = campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))
                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio


                        KCs.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                        KCs.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                        KCs.CodiceParente = 0
                        KCs.Leggi(DC_OSPITE)
                        If Trim(KCs.ModalitaPagamento) <> "" Then
                            SMEstesa.Codice = KCs.ModalitaPagamento
                        End If

                        SMEstesa.Leggi(DC_OSPITE)

                    End If


                    MyRecordStampaT.Item("ModalitaPagamento") = SMEstesa.DESCRIZIONE
                    If SMEstesa.DESCRIZIONE <> "" Then
                        MyRecordStampaT.Item("ModalitaPagamentoEstesa") = SMEstesa.DescrizioneEstesa
                    End If

                    MyRecordStampaT.Item("TIPOOPERAZIONE") = campodb(Rs_Clienti.Item("TIPOOPERAZIONE"))


                    Dim MOspite As New Cls_DatiOspiteParenteCentroServizio

                    MOspite.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                    MOspite.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                    MOspite.CodiceParente = 0
                    MOspite.Leggi(DC_OSPITE)
                    If MOspite.TipoOperazione <> "" Then
                        MyRecordStampaT.Item("TIPOOPERAZIONE") = MOspite.TipoOperazione
                    End If

                End If

                MyRecordStampaT.Item("Indirizzo") = campodb(Rs_Clienti.Item("RESIDENZAINDIRIZZO1"))
                MyRecordStampaT.Item("Cap") = campodb(Rs_Clienti.Item("RESIDENZACAP1"))
                MyRecordStampaT.Item("Localita") = FnVB6.DecodificaComune(campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1")), campodb(Rs_Clienti.Item("RESIDENZACOMUNE1")))



                Dim AppCom As New ClsComune

                AppCom.Provincia = campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1"))
                AppCom.Comune = ""
                AppCom.Leggi(SessioneTP("DC_OSPITE"))

                MyRecordStampaT.Item("Provincia") = AppCom.CodificaProvincia

                If Trim(campodb(Rs_Clienti.Item("RECAPITONOME"))) <> "" And campodb(Rs_Clienti.Item("Tipologia")) <> "R" And campodb(Rs_Clienti.Item("Tipologia")) <> "C" Then
                    MyRecordStampaT.Item("RagioneSociale") = campodb(Rs_Clienti.Item("RECAPITONOME"))
                    MyRecordStampaT.Item("Indirizzo") = campodb(Rs_Clienti.Item("RECAPITOINDIRIZZO"))
                    MyRecordStampaT.Item("Cap") = campodb(Rs_Clienti.Item("RESIDENZACAP4"))
                    MyRecordStampaT.Item("Localita") = FnVB6.DecodificaComune(campodb(Rs_Clienti.Item("RECAPITOPROVINCIA")), campodb(Rs_Clienti.Item("RECAPITOCOMUNE")))

                    Dim AppCom1 As New ClsComune

                    AppCom1.Provincia = campodb(Rs_Clienti.Item("RECAPITOPROVINCIA"))
                    AppCom1.Comune = ""
                    AppCom1.Leggi(SessioneTP("DC_OSPITE"))

                    MyRecordStampaT.Item("Provincia") = AppCom1.CodificaProvincia

                    MyRecordStampaT.Item("Intestatario") = campodb(Rs_Clienti.Item("Nome"))
                    MyRecordStampaT.Item("IntestatarioCap") = campodb(Rs_Clienti.Item("RESIDENZACAP1"))
                    MyRecordStampaT.Item("IntestatarioIndirizzo") = campodb(Rs_Clienti.Item("RESIDENZAINDIRIZZO1"))
                    MyRecordStampaT.Item("IntestatarioComune") = FnVB6.DecodificaComune(campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1")), campodb(Rs_Clienti.Item("RESIDENZACOMUNE1")))


                    AppCom1.Provincia = campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1"))
                    AppCom1.Comune = ""
                    AppCom1.Leggi(SessioneTP("DC_OSPITE"))

                    MyRecordStampaT.Item("IntestatarioProvincia") = AppCom1.CodificaProvincia
                End If
                If campodb(MyRs.Item("EnteDestinatario")) <> "" Then
                    Dim AppoggioEnteDestinatario As String

                    AppoggioEnteDestinatario = campodb(MyRs.Item("EnteDestinatario")) & Space(7)
                    If Mid(AppoggioEnteDestinatario, 1, 1) = "C" Then
                        Dim Comune As New ClsComune

                        Comune.Provincia = Mid(AppoggioEnteDestinatario, 2, 3)
                        Comune.Comune = Mid(AppoggioEnteDestinatario, 5, 3)
                        Comune.Leggi(SessioneTP("DC_OSPITE"))

                        MyRecordStampaT.Item("RagioneSociale") = Comune.Descrizione
                        MyRecordStampaT.Item("Indirizzo") = Comune.RESIDENZAINDIRIZZO1
                        MyRecordStampaT.Item("Cap") = Comune.RESIDENZACAP1
                        MyRecordStampaT.Item("Localita") = FnVB6.DecodificaComune(Comune.RESIDENZAPROVINCIA1, Comune.RESIDENZACOMUNE1)

                        Dim AppCom1 As New ClsComune

                        AppCom1.Provincia = Comune.RESIDENZAPROVINCIA1
                        AppCom1.Comune = ""
                        AppCom1.Leggi(SessioneTP("DC_OSPITE"))

                        MyRecordStampaT.Item("Provincia") = AppCom1.CodificaProvincia

                        MyRecordStampaT.Item("Intestatario") = campodb(Rs_Clienti.Item("Nome"))
                        MyRecordStampaT.Item("IntestatarioCap") = campodb(Rs_Clienti.Item("RESIDENZACAP1"))
                        MyRecordStampaT.Item("IntestatarioIndirizzo") = campodb(Rs_Clienti.Item("RESIDENZAINDIRIZZO1"))
                        MyRecordStampaT.Item("IntestatarioComune") = FnVB6.DecodificaComune(campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1")), campodb(Rs_Clienti.Item("RESIDENZACOMUNE1")))


                        AppCom1.Provincia = campodb(Rs_Clienti.Item("RESIDENZAPROVINCIA1"))
                        AppCom1.Comune = ""
                        AppCom1.Leggi(SessioneTP("DC_OSPITE"))

                        MyRecordStampaT.Item("IntestatarioProvincia") = AppCom1.CodificaProvincia

                        MyRecordStampaT.Item("EMail") = Comune.RESIDENZATELEFONO3
                    End If


                End If


                MyRecordStampaT.Item("Referente") = campodb(Rs_Clienti.Item("RecapitoNome"))
                MyRecordStampaT.Item("Attenzione") = campodb(Rs_Clienti.Item("Attenzione"))
                MyRecordStampaT.Item("NumeroFattura") = campodbN(MyRs.Item("NumeroProtocollo"))
                MyRecordStampaT.Item("Bis") = campodb(MyRs.Item("Bis"))

                MyRecordStampaT.Item("NumeroRegistrazione") = campodbN(MyRs.Item("NumeroRegistrazione"))
                MyRecordStampaT.Item("DataFattura") = campodbD(MyRs.Item("DataRegistrazione"))
                MyRecordStampaT.Item("RegistroIva") = campodbN(MyRs.Item("RegistroIVA"))


                Dim DCrEG As New Cls_RegistroIVA

                DCrEG.Tipo = campodbN(MyRs.Item("RegistroIVA"))
                DCrEG.Leggi(SessioneTP("DC_TABELLE"), DCrEG.Tipo)
                MyRecordStampaT.Item("IndicatoreRegistro") = DCrEG.IndicatoreRegistro

                MyRecordStampaT.Item("CodiceEsporatazion") = campodb(Rs_Clienti.Item("CONTOPERESATTO"))
                MyRecordStampaT.Item("CodicePagamento") = campodb(MyRs.Item("CODICEPAGAMENTO"))
                MyRecordStampaT.Item("ContoRicavoEsportazione") = Val(CentroServizioVB.CampoPerEsatto)
                MyRecordStampaT.Item("TipologiaCliente") = Mid(campodb(MyRs.Item("Tipologia")) & Space(10), 1, 1)
                MyRecordStampaT.Item("CodiceCig") = campodb(Rs_Clienti.Item("CodiceCig"))
                MyRecordStampaT.Item("DataScadenza") = DateAdd("M", 6, campodbD(MyRs.Item("DataRegistrazione")))

                Dim ImportoScadenza As Double = 0

                MyRecordStampaT.Item("SCADENZAPAGAMENTO") = Format(LeggiScadenza(TxtProva, campodbN(MyRs.Item("NumeroRegistrazione")), ImportoScadenza), "dd/MM/yyyy")

                'ImportoScadenza

                MyRecordStampaT.Item("ImportoScadenza") = ImportoScadenza


                MyRecordStampaT.Item("Identificatore") = campodb(Rs_Clienti.Item("CONTOPERESATTO"))



                If campodb(Rs_Clienti.Item("Tipologia")) = "R" Then
                    MyRecordStampaT.Item("CodiceCliente") = campodb(Rs_Clienti.Item("CodiceRegione"))

                    MyRecordStampaT.Item("EMail") = campodb(Rs_Clienti.Item("RESIDENZATELEFONO3"))

                    Dim SMEstesaR As New ClsModalitaPagamento


                    SMEstesaR.DESCRIZIONE = ""
                    SMEstesaR.ModalitaPagamento = campodb(MyRs.Item("CODICEPAGAMENTO"))
                    SMEstesaR.LeggiModalitaFattura(DC_OSPITE)


                    If SMEstesaR.DESCRIZIONE = "" Then
                        SMEstesaR.Codice = campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))
                        SMEstesaR.Leggi(DC_OSPITE)
                    End If


                    MyRecordStampaT.Item("ModalitaPagamento") = SMEstesaR.DESCRIZIONE

                    If SMEstesaR.DESCRIZIONE <> "" Then
                        MyRecordStampaT.Item("ModalitaPagamentoEstesa") = SMEstesaR.DescrizioneEstesa
                    End If

                    If campodbN(MyRs.Item("IdProgettoODV")) > 0 Then
                        Dim m As New Cls_AppaltiAnagrafica

                        m.Id = campodbN(MyRs.Item("IdProgettoODV"))
                        m.Leggi(SessioneTP("DC_TABELLE"))

                        MyRecordStampaT.Item("CodiceCig") = m.Cig

                    End If
                End If
                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                    MyRecordStampaT.Item("CodiceCliente") = campodb(Rs_Clienti.Item("CodiceProvincia")) & campodb(Rs_Clienti.Item("CodiceComune"))

                    MyRecordStampaT.Item("EMail") = campodb(Rs_Clienti.Item("RESIDENZATELEFONO3"))

                    Dim SMEstesaC As New ClsModalitaPagamento


                    SMEstesaC.DESCRIZIONE = ""
                    SMEstesaC.ModalitaPagamento = campodb(MyRs.Item("CODICEPAGAMENTO"))
                    SMEstesaC.LeggiModalitaFattura(DC_OSPITE)


                    If SMEstesaC.DESCRIZIONE = "" Then
                        SMEstesaC.Codice = campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))
                        SMEstesaC.Leggi(DC_OSPITE)
                    End If

                    MyRecordStampaT.Item("ModalitaPagamento") = SMEstesaC.DESCRIZIONE

                    If SMEstesaC.DESCRIZIONE <> "" Then
                        MyRecordStampaT.Item("ModalitaPagamentoEstesa") = SMEstesaC.DescrizioneEstesa
                    End If
                End If

                'Dim RSCServ As New Cls_CentroServizio

                'RSCServ.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                'RSCServ.Leggi(DC_OSPITE, RSCServ.CENTROSERVIZIO)

                If Trim(campodb(Rs_Clienti.Item("CCBANCARIOCLIENTE"))) = "" Then


                    MyRecordStampaT.Item("Banca") = CentroServizioVB.Banca
                    MyRecordStampaT.Item("CAB") = Format(Val(CentroServizioVB.CAB), "00000")
                    MyRecordStampaT.Item("ABI") = Format(Val(CentroServizioVB.ABI), "00000")
                    MyRecordStampaT.Item("CIN") = ""
                    MyRecordStampaT.Item("BancaAppoggio") = ""
                    MyRecordStampaT.Item("CCBancario") = ""
                    MyRecordStampaT.Item("CCBancario") = campodb(Rs_Clienti.Item("CCBANCARIOCLIENTE"))


                    If Trim(campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))) <> "" Then

                        Dim ModPAg As New ClsModalitaPagamento

                        ModPAg.Codice = campodb(Rs_Clienti.Item("MODALITAPAGAMENTO"))
                        ModPAg.Leggi(DC_OSPITE)


                        MyRecordStampaT.Item("Banca") = ModPAg.BancaCliente
                        MyRecordStampaT.Item("CAB") = Format(Val(ModPAg.BancaCab), "00000")
                        MyRecordStampaT.Item("ABI") = Format(Val(ModPAg.BancaAbi), "00000")
                        MyRecordStampaT.Item("BancaAppoggio") = ModPAg.BancaCliente
                        MyRecordStampaT.Item("CCBancario") = ModPAg.CCBancario
                        MyRecordStampaT.Item("CIN") = ModPAg.BancaCin
                        MyRecordStampaT.Item("Int") = ModPAg.IntCliente
                        MyRecordStampaT.Item("NumeroControlloCliente") = Format(Val(ModPAg.NumeroControlloCliente), "00")
                    End If

                    If campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                        Dim MODPAG As New Cls_DatiPagamento

                        MODPAG.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                        MODPAG.CodiceParente = campodbN(Rs_Clienti.Item("CodiceParente"))
                        MODPAG.LeggiUltimaDataData(DC_OSPITE, campodbD(MyRs.Item("DataRegistrazione")))
                        If MODPAG.CodiceInt <> "" Then

                            MyRecordStampaT.Item("Banca") = MODPAG.Banca
                            MyRecordStampaT.Item("Int") = MODPAG.CodiceInt
                            MyRecordStampaT.Item("NumeroControlloCliente") = Format(Val(MODPAG.NumeroControllo), "00")

                            MyRecordStampaT.Item("CAB") = MODPAG.Cab
                            MyRecordStampaT.Item("ABI") = MODPAG.Abi
                            MyRecordStampaT.Item("CIN") = MODPAG.Cin
                            MyRecordStampaT.Item("BancaAppoggio") = MODPAG.Banca
                            MyRecordStampaT.Item("CCBancario") = MODPAG.CCBancario
                        End If
                    End If
                Else

                    MyRecordStampaT.Item("Banca") = ""
                    MyRecordStampaT.Item("Int") = campodb(Rs_Clienti.Item("IntCliente"))
                    MyRecordStampaT.Item("NumeroControlloCliente") = Val(campodbN(Rs_Clienti.Item("NumeroControlloCliente")))

                    MyRecordStampaT.Item("CAB") = Format(campodb(Rs_Clienti.Item("CABCLIENTE")), "00000")
                    MyRecordStampaT.Item("ABI") = Format(campodb(Rs_Clienti.Item("ABICLIENTE")), "00000")
                    MyRecordStampaT.Item("CIN") = campodb(Rs_Clienti.Item("CINCLIENTE"))
                    MyRecordStampaT.Item("BancaAppoggio") = campodb(Rs_Clienti.Item("BANCACLIENTE"))
                    MyRecordStampaT.Item("CCBancario") = campodb(Rs_Clienti.Item("CCBANCARIOCLIENTE"))

                    If campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                        Dim MODPAG As New Cls_DatiPagamento

                        MODPAG.CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))
                        MODPAG.CodiceParente = campodbN(Rs_Clienti.Item("CodiceParente"))
                        MODPAG.LeggiUltimaDataData(DC_OSPITE, campodbD(MyRs.Item("DataRegistrazione")))
                        If MODPAG.CodiceInt <> "" Then

                            MyRecordStampaT.Item("Banca") = MODPAG.Banca
                            MyRecordStampaT.Item("Int") = MODPAG.CodiceInt
                            MyRecordStampaT.Item("NumeroControlloCliente") = Val(MODPAG.NumeroControllo)


                            MyRecordStampaT.Item("CAB") = MODPAG.Cab
                            MyRecordStampaT.Item("ABI") = MODPAG.Abi
                            MyRecordStampaT.Item("CIN") = MODPAG.Cin
                            MyRecordStampaT.Item("BancaAppoggio") = MODPAG.Banca
                            MyRecordStampaT.Item("CCBancario") = MODPAG.CCBancario
                        End If
                    End If
                End If

                MyRecordStampaT.Item("CentroServizio") = CentroServizioVB.DESCRIZIONE
                Dim MovCont As New Cls_MovimentoContabile

                If TxtProva = 1 Then
                    If MovCont.PiuCentroServizi(DC_GENERALE, campodbN(MyRs.Item("NumeroRegistrazione")), True) Then
                        MyRecordStampaT.Item("CentroServizio") = ""
                    End If
                Else
                    If MovCont.PiuCentroServizi(DC_GENERALE, campodbN(MyRs.Item("NumeroRegistrazione")), False) Then
                        MyRecordStampaT.Item("CentroServizio") = ""
                    End If
                End If


                Dim MCl As New Cls_TabelleDescrittiveOspitiAccessori

                MCl.TipoTabella = "VIL"
                MCl.CodiceTabella = CentroServizioVB.Villa
                MCl.Leggi(SessioneTP("DC_OSPITIACCESSORI"))

                MyRecordStampaT.Item("Struttura") = MCl.Descrizione
                MyRecordStampaT.Item("CausaleContabile") = campodb(MyRs.Item("CausaleContabile"))
                If campodbN(MyRs.Item("IdProgettoODV")) > 0 Then
                    Dim Appalto As New Cls_AppaltiAnagrafica


                    Appalto.Id = campodbN(MyRs.Item("IdProgettoODV"))
                    Appalto.Leggi(SessioneTP("DC_TABELLE"))
                    MyRecordStampaT.Item("Appalto") = Appalto.Descrizione
                End If

                If campodb(Rs_Clienti.Item("Tipologia")) = "O" Then
                    MyRecordStampaT.Item("QUOTAGIORNALIERA") = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), campodbN(Rs_Clienti.Item("CodiceOspite")), "O", 0, campodbD(MyRs.Item("DataRegistrazione")))
                    MyRecordStampaT.Item("ImportoE") = FnVB6.ImportoExtraOSpite(campodbN(Rs_Clienti.Item("CodiceOspite")), campodbD(MyRs.Item("DataRegistrazione")))

                    Totale = 0
                    Dim Retot As New ADODB.Recordset

                    '    Retot.Open("Select sum(Importo) From RETTEOSPITE Where ELEMENTO <> 'ACC' AND Mese = " & campodbn(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbn(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbn(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    '    If Not Retot.EOF Then
                    '        Totale = MoveFromDb(Retot.Fields(0))
                    '    End If
                    '    Retot.Close()

                    '    Retot.Open("Select sum(Importo) From RETTEPARENTE Where ELEMENTO <> 'ACC' AND  Mese = " & campodbn(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbn(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbn(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    '    If Not Retot.EOF Then
                    '        Totale = Totale + MoveFromDb(Retot.Fields(0))
                    '    End If
                    '    Retot.Close()

                    '    Retot.Open("Select sum(Importo) From RETTEOSPITE Where ELEMENTO = 'ACC' AND Mese = " & campodbn(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbn(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbn(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    '    If Not Retot.EOF Then
                    '        Totale = Totale - MoveFromDb(Retot.Fields(0))
                    '    End If
                    '    Retot.Close()

                    '    Retot.Open("Select sum(Importo) From RETTEPARENTE Where ELEMENTO = 'ACC' AND  Mese = " & campodbn(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbn(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbn(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    '    If Not Retot.EOF Then
                    '        Totale = Totale - MoveFromDb(Retot.Fields(0))
                    '    End If
                    'Retot.Close()
                    Dim SAppoggio As String

                    SAppoggio = "Select "
                    SAppoggio = SAppoggio & "(Select sum(Importo) From RETTEOSPITE Where ELEMENTO <> 'ACC' AND Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "') AS TOT1,"
                    SAppoggio = SAppoggio & "(Select sum(Importo) From RETTEPARENTE Where ELEMENTO <> 'ACC' AND  Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "') AS TOT2,"
                    SAppoggio = SAppoggio & "(Select sum(Importo) From RETTEOSPITE Where ELEMENTO = 'ACC' AND Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "') AS TOT3,"
                    SAppoggio = SAppoggio & "(Select sum(Importo) From RETTEPARENTE Where ELEMENTO = 'ACC' AND  Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "') AS TOT4"

                    Retot.Open(SAppoggio, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not Retot.EOF Then
                        Totale = MoveFromDb(Retot.Fields("TOT1")) + MoveFromDb(Retot.Fields("TOT2")) - MoveFromDb(Retot.Fields("TOT3")) - MoveFromDb(Retot.Fields("TOT4"))
                    End If
                    Retot.Close()

                    MyRecordStampaT.Item("TotOspPar") = Totale
                End If
                If campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                    MyRecordStampaT.Item("QUOTAGIORNALIERA") = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), campodbN(Rs_Clienti.Item("CodiceOspite")), "P", campodbN(Rs_Clienti.Item("CodiceParente")), campodbD(MyRs.Item("DataRegistrazione")))
                    MyRecordStampaT.Item("ImportoE") = FnVB6.ImportoExtraParente(campodbN(Rs_Clienti.Item("CodiceOspite")), campodbN(Rs_Clienti.Item("CodiceParente")), campodbD(MyRs.Item("DataRegistrazione")))

                    Totale = 0
                    Dim Retot2 As New ADODB.Recordset

                    Retot2.Open("Select sum(Importo) From RETTEOSPITE Where Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not Retot2.EOF Then
                        Totale = MoveFromDb(Retot2.Fields(0))
                    End If
                    Retot2.Close()

                    Retot2.Open("Select sum(Importo) From RETTEPARENTE Where Mese = " & campodbN(MyRs.Item("MeseCompetenza")) & " And Anno = " & campodbN(MyRs.Item("AnnoCompetenza")) & " And CodiceOspite = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not Retot2.EOF Then
                        Totale = Totale + MoveFromDb(Retot2.Fields(0))
                    End If
                    Retot2.Close()
                    MyRecordStampaT.Item("TotOspPar") = Totale
                End If

                If campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                    Dim Retot1 As New ADODB.Recordset
                    Fondo = ""

                    Dim FondoImpegnativa As New Cls_Impegnativa

                    FondoImpegnativa.CODICEOSPITE = campodbN(Rs_Clienti.Item("CodiceOspite"))
                    FondoImpegnativa.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                    FondoImpegnativa.LeggiDaAl(DC_OSPITE, FondoImpegnativa.CODICEOSPITE, FondoImpegnativa.CENTROSERVIZIO, DateSerial(campodbN(MyRs.Item("AnnoCompetenza")), campodbN(MyRs.Item("MeseCompetenza")), 1), DateSerial(campodbN(MyRs.Item("AnnoCompetenza")), campodbN(MyRs.Item("MeseCompetenza")), GiorniMese(campodbN(MyRs.Item("MeseCompetenza")), campodbN(MyRs.Item("AnnoCompetenza")))))

                    MyRecordStampaT.Item("Fondo") = FondoImpegnativa.DESCRIZIONE
                End If
                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                    MyRecordStampaT.Item("QUOTAGIORNALIERA") = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), campodbN(Rs_Clienti.Item("CodiceOspite")), "C", 0, campodbD(MyRs.Item("DataRegistrazione")))

                End If
                If campodb(Rs_Clienti.Item("Tipologia")) = "R" Then
                    MySql = "Select * From ImportoRegioni Where CODICEREGIONE = '" & campodb(Rs_Clienti.Item("CodiceRegione")) & "' and  DATA <= {ts '" & Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") & " 00:00:00'}  Order by Data"
                    RsRegione.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not RsRegione.EOF Then
                        RsRegione.MoveLast()
                        QUOTAGIORNALIERA = MoveFromDbWC(RsRegione, "Importo")
                    End If
                    RsRegione.Close()
                    MyRecordStampaT.Item("QUOTAGIORNALIERA") = QUOTAGIORNALIERA
                End If

                Dim CmdGiorniInFatt As New OleDbCommand
                CmdGiorniInFatt.Connection = cnGenerale

                If TxtProva = 1 Then
                    CmdGiorniInFatt.CommandText = "Select sum(Quantita) As Giorni From Temp_MovimentiContabiliRiga Where Numero = " & campodbN(MyRs.Item("Numero"))
                Else
                    CmdGiorniInFatt.CommandText = "Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where Numero = " & campodbN(MyRs.Item("Numero"))
                End If
                Dim RdGiorniInFatt As OleDbDataReader = CmdGiorniInFatt.ExecuteReader
                If RdGiorniInFatt.Read Then
                    MyRecordStampaT.Item("GIORNIPRESENZA") = campodbN(RdGiorniInFatt.Item("Giorni"))
                End If
                RdGiorniInFatt.Close()


                If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                    MyRecordStampaT.Item("CodiceOspite") = campodbN(Rs_Clienti.Item("CodiceOspite"))
                End If

                If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") And campodb(Rs_Clienti.Item("FattAnticipata")) <> "S" Then
                    If campodb(Rs_Clienti.Item("Tipologia")) = "O" Then

                        Dim CmdGiorniCalcolo As New OleDbCommand

                        CmdGiorniCalcolo.Connection = cnOspiti
                        CmdGiorniCalcolo.CommandText = "Select sum(Giorni) From RETTEOSPITE Where CODICEOSPITE = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And Mese = " & Mese & " And Anno = " & TxtAnno & " And ELEMENTO = 'RGP'"
                        Dim RdGiorniCalcolo As OleDbDataReader = CmdGiorniCalcolo.ExecuteReader
                        If RdGiorniCalcolo.Read Then
                            MyRecordStampaT.Item("GIORNIPRESENZA") = campodbN(RdGiorniCalcolo.Item(0))
                        End If
                        RdGiorniCalcolo.Close()


                        Dim CmdGiorniCalcoloAssenza As New OleDbCommand

                        CmdGiorniCalcoloAssenza.Connection = cnOspiti
                        CmdGiorniCalcoloAssenza.CommandText = "Select sum(Giorni) From RETTEOSPITE Where CODICEOSPITE = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And Mese = " & Mese & " And Anno = " & TxtAnno & " And ELEMENTO = 'RGA'"
                        Dim RdGiorniCalcoloAssenza As OleDbDataReader = CmdGiorniCalcoloAssenza.ExecuteReader
                        If RdGiorniCalcoloAssenza.Read Then
                            MyRecordStampaT.Item("GIORNIASSENZA") = campodbN(RdGiorniCalcoloAssenza.Item(0))
                        End If
                        RdGiorniCalcoloAssenza.Close()
                    End If

                    If campodb(Rs_Clienti.Item("Tipologia")) = "P" Then


                        Dim CmdGiorniCalcolo As New OleDbCommand

                        CmdGiorniCalcolo.Connection = cnOspiti
                        CmdGiorniCalcolo.CommandText = "Select sum(Giorni) From RETTEPARENTE Where CODICEOSPITE = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And Mese = " & Mese & " And Anno = " & TxtAnno & " And ELEMENTO = 'RGP' And CODICEPARENTE = " & campodbN(Rs_Clienti.Item("CodiceParente"))
                        Dim RdGiorniCalcolo As OleDbDataReader = CmdGiorniCalcolo.ExecuteReader
                        If RdGiorniCalcolo.Read Then
                            MyRecordStampaT.Item("GIORNIPRESENZA") = campodbN(RdGiorniCalcolo.Item(0))
                        End If
                        RdGiorniCalcolo.Close()

                        Dim CmdGiorniCalcoloAssenza As New OleDbCommand

                        CmdGiorniCalcoloAssenza.Connection = cnOspiti
                        CmdGiorniCalcoloAssenza.CommandText = "Select sum(Giorni) From RETTEPARENTE Where CODICEOSPITE = " & campodbN(Rs_Clienti.Item("CodiceOspite")) & " And Mese = " & Mese & " And Anno = " & TxtAnno & " And ELEMENTO = 'RGA'  And CODICEPARENTE = " & campodbN(Rs_Clienti.Item("CodiceParente"))
                        Dim RdGiorniCalcoloAssenza As OleDbDataReader = CmdGiorniCalcoloAssenza.ExecuteReader
                        If RdGiorniCalcoloAssenza.Read Then
                            MyRecordStampaT.Item("GIORNIASSENZA") = campodbN(RdGiorniCalcoloAssenza.Item(0))
                        End If
                        RdGiorniCalcoloAssenza.Close()

                    End If
                End If

                If campodb(Rs_Clienti.Item("PERIODO")) = "M" Then
                    CampoTipoOperazioneAnticipo = campodb(Rs_Clienti.Item("FattAnticipata"))

                    If CampoTipoOperazioneAnticipo = "S" Then
                        If FnVB6.CampoCausaleContabile(campodb(MyRs.Item("CausaleContabile")), "TIPODOCUMENTO") = "NC" Then
                            MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                        Else
                            Dim MesePeriodoStampa As Integer = campodbN(MyRs.Item("MeseCompetenza"))
                            Dim AnnoPeriodoStampa As Integer = campodbN(MyRs.Item("AnnoCompetenza"))

                            If MesePeriodoStampa < 12 Then
                                MesePeriodoStampa = MesePeriodoStampa + 1
                            Else
                                MesePeriodoStampa = 1
                                AnnoPeriodoStampa = AnnoPeriodoStampa + 1
                            End If
                            Dim CmdGiorniCalcoloAssenza As New OleDbCommand

                            CmdGiorniCalcoloAssenza.Connection = cnGenerale

                            If TxtProva = 1 Then
                                CmdGiorniCalcoloAssenza.CommandText = "Select * From Temp_MovimentiContabiliRiga Where Numero = " & campodbN(MyRs.Item("NumeroRegistrazione")) & " And MeseRiferimento = " & MesePeriodoStampa & " And AnnoRiferimento = " & AnnoPeriodoStampa
                            Else
                                CmdGiorniCalcoloAssenza.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & campodbN(MyRs.Item("NumeroRegistrazione")) & " And MeseRiferimento = " & MesePeriodoStampa & " And AnnoRiferimento = " & AnnoPeriodoStampa
                            End If

                            Dim RdGiorniCalcoloAssenza As OleDbDataReader = CmdGiorniCalcoloAssenza.ExecuteReader
                            If RdGiorniCalcoloAssenza.Read Then
                                MyRecordStampaT.Item("Periodo") = DecodificaMese(MesePeriodoStampa) & " / " & AnnoPeriodoStampa
                            Else
                                MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                            End If
                            RdGiorniCalcoloAssenza.Close()

                        End If
                    Else
                        MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                    End If
                End If
                MyRecordStampaT.Item("Mese") = campodbN(MyRs.Item("MeseCompetenza"))
                MyRecordStampaT.Item("Anno") = campodbN(MyRs.Item("AnnoCompetenza"))

                If campodb(Rs_Clienti.Item("PERIODO")) = "B" Then
                    MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza")) - 1) & " / " & campodbN(MyRs.Item("AnnoCompetenza")) & " - " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                End If
                If campodb(Rs_Clienti.Item("PERIODO")) = "T" Then
                    MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza")) - 2) & " / " & campodbN(MyRs.Item("AnnoCompetenza")) & " - " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza")) - 1) & " / " & campodbN(MyRs.Item("AnnoCompetenza")) & " - " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                End If
                If campodb(Rs_Clienti.Item("PERIODO")) = "S" Then
                    MyRecordStampaT.Item("Periodo") = DecodificaMese(campodbN(MyRs.Item("MeseCompetenza")) - 6) & " / " & campodbN(MyRs.Item("AnnoCompetenza")) & " - " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & campodbN(MyRs.Item("AnnoCompetenza"))
                End If

                MyRecordStampaT.Item("TipoDocumento") = TipoDocumento(campodb(MyRs.Item("CausaleContabile")))
                If Len(campodb(MyRs.Item("TIPOLOGIA"))) > 1 Then
                    Regione = campodb(Rs_Clienti.Item("CodiceRegione"))
                    If Mid(campodb(MyRs.Item("TIPOLOGIA")), 1, 1) = "R" Then
                        Regione = Mid(campodb(MyRs.Item("TIPOLOGIA")), 2, 4)
                    End If
                    NoteProvincia = campodb(Rs_Clienti.Item("CodiceProvincia"))
                    NoteComune = campodb(Rs_Clienti.Item("CodiceComune"))

                    If Mid(campodb(MyRs.Item("TIPOLOGIA")), 1, 1) = "J" Then
                        NoteProvincia = Mid(campodb(MyRs.Item("TIPOLOGIA")), 2, 3)
                        NoteComune = Mid(campodb(MyRs.Item("TIPOLOGIA")), 5, 3)
                    End If

                    MyRecordStampaT.Item("DescrizioneUp") = FnVB6.LeggiNote(campodb(MyRs.Item("CentroServizio")), NoteProvincia, NoteComune, Regione, True, OldRegistrazione, campodbN(Rs_Clienti.Item("CodiceOspite")), SessioneTP("DC_OSPITE"))
                    MyRecordStampaT.Item("DescrizioneDown") = Trim(FnVB6.LeggiNote(campodb(MyRs.Item("CentroServizio")), NoteProvincia, NoteComune, campodb(Rs_Clienti.Item("CodiceRegione")), False, OldRegistrazione, campodbN(Rs_Clienti.Item("CodiceOspite")), SessioneTP("DC_OSPITE")))
                Else
                    MyRecordStampaT.Item("DescrizioneUp") = FnVB6.LeggiNote(campodb(MyRs.Item("CentroServizio")), campodb(Rs_Clienti.Item("CodiceProvincia")), campodb(Rs_Clienti.Item("CodiceComune")), campodb(Rs_Clienti.Item("CodiceRegione")), True, OldRegistrazione, campodbN(Rs_Clienti.Item("CodiceOspite")), SessioneTP("DC_OSPITE"))
                    MyRecordStampaT.Item("DescrizioneDown") = Trim(FnVB6.LeggiNote(campodb(MyRs.Item("CentroServizio")), campodb(Rs_Clienti.Item("CodiceProvincia")), campodb(Rs_Clienti.Item("CodiceComune")), campodb(Rs_Clienti.Item("CodiceRegione")), False, OldRegistrazione, campodbN(Rs_Clienti.Item("CodiceOspite")), SessioneTP("DC_OSPITE")))
                End If

                If Trim(campodb(Rs_Clienti.Item("CodiceFiscale"))) <> "" Then
                    MyRecordStampaT.Item("PartitaIVACodiceFiscale") = campodb(Rs_Clienti.Item("CodiceFiscale"))
                    If campodbN(Rs_Clienti.Item("PartitaIVA")) > 0 Then
                        MyRecordStampaT.Item("PartitaIVACodiceFiscale") = Format(campodbN(Rs_Clienti.Item("PartitaIVA")), "00000000000")
                    End If
                Else
                    If campodbN(Rs_Clienti.Item("PartitaIVA")) > 0 Then
                        MyRecordStampaT.Item("PartitaIVACodiceFiscale") = Format(campodbN(Rs_Clienti.Item("PartitaIVA")), "00000000000")
                    End If
                End If
                'If Trim(FnVB6.DecodificaModalita(campodb(Rs_Clienti.Item("MODALITAPAGAMENTO")))) <> "" Then
                '    MyRecordStampaT.Item("ModalitaPagamento") = FnVB6.DecodificaModalita(campodb(Rs_Clienti.Item("MODALITAPAGAMENTO")))
                'Else
                '    MyRecordStampaT.Item("ModalitaPagamento") = FnVB6.CampoTipoPagamento(campodb(Rs_Clienti.Item("MODALITAPAGAMENTO")), "Descrizione")
                'End If
                MyRecordStampaT.Item("Dichiarazione") = campodb(Rs_Clienti.Item("Dichiarazione"))
            End If

            If FnVB6.CampoCausaleContabile(campodb(MyRs.Item("CausaleContabile")), "TIPO") = "R" Then
                RigaDaCausale = campodbN(MyRs.Item("RigaDaCausale"))

                If RigaDaCausale = 8 Then
                    TotaleAnticipo = TotaleAnticipo + campodbN(MyRs.Item("Importo"))
                End If



                'If RigaDaCausale = 4 Then
                '    If Val(CentroServizioVB.ExtraFissiRetta) = 1 Then
                '        RigaDaCausale = 0
                '    End If
                'End If
                ImportoRiga = campodbN(MyRs.Item("Importo"))
                If RigaDaCausale = 12 Then


                    MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow
                    NumeroRigaRegistrazione = NumeroRigaRegistrazione + 1

                    MyRecordStampaR.Item("NumeroRiga") = NumeroRigaRegistrazione
                    MyRecordStampaR.Item("NumeroFattura") = campodbN(MyRs.Item("NumeroProtocollo"))
                    MyRecordStampaR.Item("NumeroRegistrazione") = campodbN(MyRs.Item("NumeroRegistrazione"))
                    MyRecordStampaR.Item("DataFattura") = campodbD(MyRs.Item("DataRegistrazione"))

                    MyRecordStampaR.Item("RigaDaCausale") = campodbN(MyRs.Item("RigaDaCausale"))

                    Sottoconto = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                    MyRecordStampaR.Item("CodiceCigSuOspite") = FnVB6.CampoOspite(Sottoconto, "CodiceCig")


                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                    If Riferimento = "Invita" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                        End If
                    End If

                    ImportoRiga = campodbN(MyRs.Item("Importo"))
                    If campodb(MyRs.Item("DareAvere")) = DareAvere Then
                        importo = ImportoRiga * -1
                    Else
                        importo = ImportoRiga
                    End If

                    MyRecordStampaR.Item("Importo") = importo
                    MyRecordStampaR.Item("CodiceIva") = campodb(MyRs.Item("CodiceIva"))
                    MyRecordStampaR.Item("RegistroIVA") = campodbN(MyRs.Item("RegistroIVA"))


                    Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)

                    WTotaleMerce = WTotaleMerce - ImportoRiga
                    WImportoSconto = ImportoRiga
                End If


                If (RigaDaCausale >= 3 And RigaDaCausale <= 6) Or (RigaDaCausale = 9 Or RigaDaCausale = 8 Or RigaDaCausale = 13 Or RigaDaCausale = 14) Then

                    If RigaDaCausale = 3 Then
                        If Val(CentroServizioVB.ExtraFissiRetta) = 1 Then
                            ImportoRiga = ImportoRiga + FnVB6.ImportoTuttiExtrDoc(campodbN(MyRs.Item("NumeroRegistrazione")))
                        End If
                    End If



                    MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow
                    NumeroRigaRegistrazione = NumeroRigaRegistrazione + 1


                    If RigaDaCausale = 3 Then
                        Dim sTATO As New Cls_StatoAuto

                        sTATO.CODICEOSPITE = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                        sTATO.UltimaData(DC_OSPITE, sTATO.CODICEOSPITE, campodb(MyRs.Item("CentroServizio")))

                        If Not IsNothing(sTATO.TipoRetta) Then
                            Dim MDecTipoRetta As New Cls_TabellaTipoImportoRegione

                            MDecTipoRetta.Codice = sTATO.TipoRetta
                            MDecTipoRetta.Leggi(DC_OSPITE, MDecTipoRetta.Codice)
                            MyRecordStampaR.Item("TipoRettaSanitario") = MDecTipoRetta.Descrizione
                        Else
                            MyRecordStampaR.Item("TipoRettaSanitario") = ""
                        End If


                    End If

                    MyRecordStampaR.Item("NumeroRiga") = NumeroRigaRegistrazione
                    MyRecordStampaR.Item("NumeroFattura") = campodbN(MyRs.Item("NumeroProtocollo"))
                    MyRecordStampaR.Item("NumeroRegistrazione") = campodbN(MyRs.Item("NumeroRegistrazione"))
                    MyRecordStampaR.Item("DataFattura") = campodbD(MyRs.Item("DataRegistrazione"))

                    MyRecordStampaR.Item("RigaDaCausale") = campodbN(MyRs.Item("RigaDaCausale"))

                    Sottoconto = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                    MyRecordStampaR.Item("CodiceCigSuOspite") = FnVB6.CampoOspite(Sottoconto, "CodiceCig")

                    If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                        MyRecordStampaR.Item("QUOTAGIORNALIERA") = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodb(MyRs.Item("MastroControPartita")), campodb(MyRs.Item("ContoControPartita")), campodb(MyRs.Item("SottocontoContropartita")))), "C", 0, campodbD(MyRs.Item("DataRegistrazione")))
                        MyRecordStampaR.Item("ImportoE") = FnVB6.ImportoExtraComune(Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodb(MyRs.Item("MastroControPartita")), campodb(MyRs.Item("ContoControPartita")), campodb(MyRs.Item("SottocontoControPartita")))), campodbD(MyRs.Item("DataRegistrazione")))
                    End If

                    If Not campodbN(MyRs.Item("RigaDaCausale")) = 9 Then
                        MyRecordStampaR.Item("Quantita") = campodbN(MyRs.Item("Quantita"))

                        If Not IsDBNull(campodbN(MyRs.Item("Quantita"))) And campodbN(MyRs.Item("Quantita")) <> 0 Then
                            MyRecordStampaR.Item("Prezzo") = ImportoRiga / campodbN(MyRs.Item("Quantita"))
                        Else
                            MyRecordStampaR.Item("Prezzo") = 0
                        End If
                    End If

                    If Riferimento = "ODA" Then
                        Dim QuotaInizioMese As Double
                        Dim QuotaFineMese As Double

                        If campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R" Then
                            OMastro = campodbN(MyRs.Item("MastroControPartita"))
                            OConto = campodbN(MyRs.Item("ContoControPartita"))
                            OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                            CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))

                        Else
                            CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))
                        End If

                        QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0)
                        QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, DateSerial(TxtAnno, Mese, 1))
                        If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                            NQuota = QuotaInizioMese
                        Else
                            NQuota = QuotaFineMese
                        End If

                        For I = 1 To 5
                            QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I)
                            QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I, DateSerial(TxtAnno, Mese, 1))
                            If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                                NQuota = NQuota + QuotaInizioMese
                            Else
                                NQuota = NQuota + QuotaFineMese
                            End If
                        Next


                        MyRecordStampaR.Item("QuotaOspitie") = Math.Round(NQuota, 2)
                    End If



                    If Riferimento = "Mese Compentenza - Stato Autosufficenza - Fatt. Riferimento" Then
                        If campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                            If FnVB6.StatoAutoOspite(MyRecordStampaT.Item("CodiceOspite"), Now) = "A" Then
                                MyStatoAuto = "Autosufficente"
                            Else
                                MyStatoAuto = "Non Autosufficente"
                            End If
                            Bollette = ""
                            Appoggio = ""
                            If TxtProva = 1 Then
                                Appoggio = FnVB6.CampoTempMovimentiContabiliTesta(campodbN(MyRs.Item("Numero")), "FatturaDiAnticipo")
                            Else
                                Appoggio = FnVB6.CampoMovimentiContabiliTesta(campodbN(MyRs.Item("Numero")), "FatturaDiAnticipo")
                            End If
                            If Appoggio = "S" Then
                                Rs_Legame.Open("Select * From TabellaLegami Where CodiceDocumento = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                                Do While Not Rs_Legame.EOF
                                    If TxtProva = 1 Then
                                        Bollette = Bollette & FnVB6.CampoTempMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "NumeroBolletta") & " " & FnVB6.CampoTempMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "DataBolletta") & " "
                                    Else
                                        Bollette = Bollette & FnVB6.CampoMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "NumeroBolletta") & " " & FnVB6.CampoMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "DataBolletta") & " "
                                    End If

                                    Rs_Legame.MoveNext()
                                Loop
                                Rs_Legame.Close()
                            End If
                            Anticipo = ""
                            If campodbN(MyRs.Item("RigaDaCausale")) = 8 Then
                                Mastro = FnVB6.CampoParametri("MASTRO")
                                Conto = campodbN(MyRs.Item("ContoPartita"))
                                Sottoconto = campodbN(MyRs.Item("SottocontoPartita"))

                                If TxtProva = 1 Then
                                    MySql = "SELECT Temp_MovimentiContabiliTesta.AnnoProtocollo, Temp_MovimentiContabiliTesta.NumeroProtocollo,Temp_MovimentiContabiliRiga.Importo " &
                                           " FROM Temp_MovimentiContabiliTesta INNER JOIN Temp_MovimentiContabiliRiga ON Temp_MovimentiContabiliTesta.NumeroRegistrazione = Temp_MovimentiContabiliRiga.Numero " &
                                            " WHERE (((Temp_MovimentiContabiliRiga.MastroPartita)= " & Mastro & ") AND ((Temp_MovimentiContabiliRiga.ContoPartita)= " & Conto & ") AND ((Temp_MovimentiContabiliRiga.SottocontoPartita)= " & Sottoconto & ") AND ((Temp_MovimentiContabiliRiga.RigaDaCausale)=1) AND ((Temp_MovimentiContabiliTesta.FatturaDiAnticipo)='S')) " &
                                            " And Temp_MovimentiContabiliRiga.Numero < " & campodbN(MyRs.Item("Numero")) & "  Order by Temp_MovimentiContabiliTesta.NumeroRegistrazione DESC"

                                Else
                                    MySql = "SELECT MovimentiContabiliTesta.AnnoProtocollo, MovimentiContabiliTesta.NumeroProtocollo,MovimentiContabiliRiga.Importo " &
                                           " FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " &
                                            " WHERE (((MovimentiContabiliRiga.MastroPartita)= " & Mastro & ") AND ((MovimentiContabiliRiga.ContoPartita)= " & Conto & ") AND ((MovimentiContabiliRiga.SottocontoPartita)= " & Sottoconto & ") AND ((MovimentiContabiliRiga.RigaDaCausale)=1) AND ((MovimentiContabiliTesta.FatturaDiAnticipo)='S')) " &
                                            " And MovimentiContabiliRiga.Numero < " & campodbN(MyRs.Item("Numero")) & "  Order by MovimentiContabiliTesta.NumeroRegistrazione DESC"

                                End If


                                Rs_Legame.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                                importo = 0
                                Do While Not Rs_Legame.EOF
                                    If Anticipo <> "" Then
                                        Anticipo = "  e Fattura n. " & Anticipo
                                    End If

                                    Anticipo = Anticipo & MoveFromDbWC(Rs_Legame, "AnnoProtocollo") & " " & MoveFromDbWC(Rs_Legame, "NumeroProtocollo")
                                    importo = importo + Rs_Legame.Fields("Importo").Value
                                    If importo >= campodbN(MyRs.Item("Importo")) Then
                                        Exit Do
                                    End If
                                Loop
                                Rs_Legame.Close()
                                If Anticipo <> "" Then
                                    Anticipo = " Fattura n. " & Anticipo
                                End If
                            End If
                            If campodbN(MyRs.Item("RigaDaCausale")) = 8 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = "Anticipo " & DesMese & " " & TxtAnno & " " & MyStatoAuto & " " & Bollette & " " & Anticipo
                            Else
                                If campodbN(MyRs.Item("RigaDaCausale")) = 9 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & DesMese & " " & TxtAnno & " " & MyStatoAuto & " " & Bollette & " " & Anticipo
                                End If

                            End If
                        Else
                            If FnVB6.StatoAutoOspite(MyRecordStampaT.Item("CodiceOspite"), Now) = "A" Then
                                MyStatoAuto = "Autosufficente"
                            Else
                                MyStatoAuto = "Non Autosufficente"
                            End If
                            Bollette = ""
                            If TxtProva = 1 Then
                                Appoggio = FnVB6.CampoTempMovimentiContabiliTesta(campodbN(MyRs.Item("Numero")), "FatturaDiAnticipo")
                            Else
                                Appoggio = FnVB6.CampoMovimentiContabiliTesta(campodbN(MyRs.Item("Numero")), "FatturaDiAnticipo")
                            End If
                            If Appoggio = "S" Then
                                Rs_Legame.Open("Select * From TabellaLegami Where CodiceDocumento = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                                Do While Not Rs_Legame.EOF
                                    If TxtProva = 1 Then
                                        Bollette = Bollette & FnVB6.CampoTempMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "NumeroBolletta") & " " & FnVB6.CampoMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "DataBolletta") & " "
                                    Else
                                        Bollette = Bollette & FnVB6.CampoMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "NumeroBolletta") & " " & FnVB6.CampoMovimentiContabiliTesta(Rs_Legame.Fields("CodicePagamento").Value, "DataBolletta") & " "
                                    End If
                                    Rs_Legame.MoveNext()
                                Loop
                                Rs_Legame.Close()
                            End If
                            Anticipo = ""
                            If campodbN(MyRs.Item("RigaDaCausale")) = 8 Then
                                Mastro = FnVB6.CampoParametri("MASTRO")
                                Conto = campodbN(MyRs.Item("ContoPartita"))
                                Sottoconto = campodbN(MyRs.Item("SottocontoPartita"))

                                If TxtProva = 1 Then
                                    MySql = "SELECT Temp_MovimentiContabiliTesta.AnnoProtocollo, Temp_MovimentiContabiliTesta.NumeroProtocollo,Temp_MovimentiContabiliRiga.Importo " &
                                            " FROM Temp_MovimentiContabiliTesta INNER JOIN Temp_MovimentiContabiliRiga ON Temp_MovimentiContabiliTesta.NumeroRegistrazione = Temp_MovimentiContabiliRiga.Numero " &
                                            " WHERE (((Temp_MovimentiContabiliRiga.MastroPartita)= " & Mastro & ") AND ((Temp_MovimentiContabiliRiga.ContoPartita)= " & Conto & ") AND ((Temp_MovimentiContabiliRiga.SottocontoPartita)= " & Sottoconto & ") AND ((Temp_MovimentiContabiliRiga.RigaDaCausale)=1) AND ((Temp_MovimentiContabiliTesta.FatturaDiAnticipo)='S')) " &
                                            " And Temp_MovimentiContabiliRiga.Numero < " & campodbN(MyRs.Item("Numero")) & "  Order by Temp_MovimentiContabiliTesta.NumeroRegistrazione DESC"
                                Else
                                    MySql = "SELECT MovimentiContabiliTesta.AnnoProtocollo, MovimentiContabiliTesta.NumeroProtocollo,MovimentiContabiliRiga.Importo " &
                                            " FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " &
                                            " WHERE (((MovimentiContabiliRiga.MastroPartita)= " & Mastro & ") AND ((MovimentiContabiliRiga.ContoPartita)= " & Conto & ") AND ((MovimentiContabiliRiga.SottocontoPartita)= " & Sottoconto & ") AND ((MovimentiContabiliRiga.RigaDaCausale)=1) AND ((MovimentiContabiliTesta.FatturaDiAnticipo)='S')) " &
                                            " And MovimentiContabiliRiga.Numero < " & campodbN(MyRs.Item("Numero")) & "  Order by MovimentiContabiliTesta.NumeroRegistrazione DESC"

                                End If

                                Rs_Legame.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                                importo = 0
                                Do While Not Rs_Legame.EOF
                                    If Anticipo <> "" Then
                                        Anticipo = "  e Fattura n. " & Anticipo
                                    End If

                                    Anticipo = Anticipo & MoveFromDbWC(Rs_Legame, "AnnoProtocollo") & " " & MoveFromDbWC(Rs_Legame, "NumeroProtocollo")
                                    importo = importo + Rs_Legame.Fields("Importo").Value
                                    If importo >= campodbN(MyRs.Item("Importo")) Then
                                        Exit Do
                                    End If
                                Loop
                                Rs_Legame.Close()
                                If Anticipo <> "" Then
                                    Anticipo = " Fattura n. " & Anticipo
                                End If
                            End If

                            If campodbN(MyRs.Item("RigaDaCausale")) = 8 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = "Anticipo " & DesMese & " " & TxtAnno & " " & MyStatoAuto & " " & Anticipo
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita"))) & " " & DesMese & " " & TxtAnno & "  " & MyStatoAuto & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If

                        End If
                    Else
                        If Riferimento = "Descrizione" Then
                            MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If campodbN(MyRs.Item("RigaDaCausale")) = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                            Else
                                Sotto = Int(campodbN(MyRs.Item("SottocontoContropartita")) / 100) * 100
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), Sotto) & " " & campodb(MyRs.Item("DescrizioneRiga"))

                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), Sotto) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                End If
                            End If
                        End If
                    End If
                    If Riferimento = "Prealpina" Then
                        If campodbN(MyRs.Item("RigaDaCausale")) = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                        Else
                            Sotto = Int(campodbN(MyRs.Item("SottocontoContropartita")) / 100) * 100
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), Sotto) & " " & campodb(MyRs.Item("DescrizioneRiga"))

                            If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), Sotto) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                            End If
                        End If
                    End If

                    If Riferimento = "Lusan" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                        If campodbN(MyRs.Item("RigaDaCausale")) = 4 Or campodbN(MyRs.Item("RigaDaCausale")) = 5 Or campodbN(MyRs.Item("RigaDaCausale")) = 6 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                            If campodbN(MyRs.Item("RigaDaCausale")) = 3 Then
                                Dim MR As New Cls_rettatotale

                                MR.CODICEOSPITE = CodiceOspite
                                MR.UltimaData(DC_OSPITE, CodiceOspite, campodb(MyRs.Item("CentroServizio")))
                                If MR.TipoRetta <> "" Then
                                    Dim Xs As New Cls_TipoRetta

                                    Xs.Tipo = MR.TipoRetta
                                    Xs.Leggi(DC_OSPITE, Xs.Tipo)
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & Xs.Descrizione & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            Dim ST As New Cls_rettatotale

                            ST.CODICEOSPITE = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                            ST.Data = campodbD(MyRs.Item("DataRegistrazione"))
                            ST.RettaTotaleADataSenzaCSERV(DC_OSPITE, ST.CODICEOSPITE, ST.Data)
                            If ST.TipoRetta <> "" Then
                                Dim Xs As New Cls_TipoRetta

                                Xs.Codice = ST.TipoRetta
                                Xs.Leggi(DC_OSPITE, Xs.Codice)
                                If campodb(Rs_Clienti.Item("Tipologia")) = "R" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Retta ASL " & Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita")))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Retta " & Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If

                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = "Retta  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita")))
                            End If
                        End If
                    End If

                    If Riferimento = "Stato Autosufficenza - gg - Retta Ospite" Then
                        OMastro = campodbN(MyRs.Item("MastroControPartita"))
                        OConto = campodbN(MyRs.Item("ContoControPartita"))
                        OSottoconto = campodbN(MyRs.Item("SottoContoControPartita"))
                        CodiceOspite = FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto)
                        If FnVB6.StatoAutoOspite(CodiceOspite, Now) = "A" Then
                            MyStatoAuto = "Autosufficente"
                        Else
                            MyStatoAuto = "Non Autosufficente"
                        End If
                        MyNQuota = "" & vbNewLine
                        NQuota = 0
                        For I = 1 To GiorniMese(Mese, TxtAnno)
                            If NQuota <> RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, DateSerial(TxtAnno, Mese, I)) Then
                                NQuota = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, DateSerial(TxtAnno, Mese, I))
                                MyNQuota = MyNQuota & "In data " & Format(DateSerial(TxtAnno, Mese, I), "dd/MM/yyyy") & " " & NQuota & " "
                            End If
                        Next

                        If NQuota = 0 Then
                            MyNQuota = "" & vbNewLine
                            NQuota = 0
                            For I = 1 To GiorniMese(Mese, TxtAnno)
                                If NQuota <> RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", 1, DateSerial(TxtAnno, Mese, I)) Then
                                    NQuota = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", 1, DateSerial(TxtAnno, Mese, I))
                                    MyNQuota = MyNQuota & "In data " & Format(DateSerial(TxtAnno, Mese, I), "dd/MM/yyyy") & " " & NQuota & " "
                                End If
                            Next
                            MyQuota = " Quota Parente : " & MyNQuota
                        Else
                            MyQuota = " Quota Ospite : " & MyNQuota
                        End If

                        TUscite = ""
                        RsMovimenti.Open("Select * From Movimenti Where CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And month(Data) = " & Mese & " And year(Data) = " & TxtAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Do While Not RsMovimenti.EOF
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 13 Then
                                TUscite = TUscite & " Ud : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy") & " " & FnVB6.DecodificaCausaleEntrataUscita(MoveFromDb(RsMovimenti.Fields("Causale")))
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 5 Then
                                TUscite = TUscite & " Ac : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy")
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 3 Then
                                TUscite = TUscite & " U : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy") & " " & FnVB6.DecodificaCausaleEntrataUscita(MoveFromDb(RsMovimenti.Fields("Causale")))
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 4 Then
                                TUscite = TUscite & " E : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy")
                            End If
                            RsMovimenti.MoveNext()
                        Loop
                        RsMovimenti.Close()

                        If TUscite <> "" Then
                            TUscite = vbNewLine & TUscite
                        End If
                        If campodb(MyRs.Item("DescrizioneRiga")) = "Presenze" Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & MyStatoAuto & MyQuota & TUscite
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroContropartita")), campodbN(MyRs.Item("ContoContropartita")), campodbN(MyRs.Item("SottocontoContropartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If

                    End If

                    If Riferimento = "Conto Descrizione CodiceFiscale" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                Dim AppoCognome As String = FnVB6.CampoOspiteParenteMCS("CognomeOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim AppoNome As String = FnVB6.CampoOspiteParenteMCS("NomeOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim AppoCodiceFiscale As String = FnVB6.CampoOspiteParenteMCS("CODICEFISCALE", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim AppoDataNascita As String = FnVB6.CampoOspiteParenteMCS("DataNascita", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim UltLiv As New Cls_rettatotale

                                UltLiv.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                UltLiv.CODICEOSPITE = FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                UltLiv.UltimaData(DC_OSPITE, UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                                Dim DecT As New Cls_TipoRetta

                                If UltLiv.TipoRetta <> "" Then
                                    DecT.Codice = UltLiv.TipoRetta
                                    DecT.Leggi(DC_OSPITE, DecT.Codice)
                                End If

                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " - " & AppoCognome & " " & AppoNome & " " & AppoCodiceFiscale & " " & DecT.Descrizione

                            End If
                        End If
                    End If

                    If Riferimento = "Diurno Cittadella" Then
                        If RigaDaCausale <> 3 And RigaDaCausale <> 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                        If RigaDaCausale = 3 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                            Else
                                Dim xAnno As Integer = campodbN(MyRs.Item("AnnoCompetenza"))
                                Dim xMese As Integer = campodbN(MyRs.Item("MeseCompetenza"))


                                MySql = ""

                                OMastro = campodbN(MyRs.Item("MastroControPartita"))
                                OConto = campodbN(MyRs.Item("ContoControPartita"))
                                OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                                CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))
                                CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))

                                Dim DiurnoString As String = ""
                                Dim K As New Cls_Diurno

                                K.Anno = xAnno
                                K.Mese = xMese
                                K.CodiceOspite = CodiceOspite
                                K.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))

                                Dim AppoggioMese As String = ""
                                AppoggioMese = K.LeggiMese(DC_OSPITE, K.CodiceOspite, K.CENTROSERVIZIO, K.Anno, K.Mese)

                                AppoggioMese = Mid(AppoggioMese, 1, GiorniMese(K.Mese, K.Anno) * 2)


                                Dim TrasportoDiurno As Integer = 0


                                TrasportoDiurno = quanteVolte(AppoggioMese, "T")

                                If TrasportoDiurno > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Trasporti :" & TrasportoDiurno
                                End If


                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & DiurnoString
                            End If
                        End If
                    End If


                    If Riferimento = "Residenza Paradiso" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                    End If

                    If Riferimento = "Residenziali Asp Charitas" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "J" Or campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                Dim AppoCognome As String = FnVB6.CampoOspiteParenteMCS("CognomeOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim AppoNome As String = FnVB6.CampoOspiteParenteMCS("NomeOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim AppoDataNascita As String = FnVB6.CampoOspiteParenteMCS("DataNascita", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                Dim UltLiv As New Cls_rettatotale

                                UltLiv.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                UltLiv.CODICEOSPITE = FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                UltLiv.UltimaData(DC_OSPITE, UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                                Dim DecT As New Cls_TipoRetta

                                If UltLiv.TipoRetta <> "" Then
                                    DecT.Codice = UltLiv.TipoRetta
                                    DecT.Leggi(DC_OSPITE, DecT.Codice)
                                End If

                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " - " & Mid(AppoCognome, 1, 1) & ". " & Mid(AppoNome, 1, 1) & ". " & AppoDataNascita & " " & DecT.Descrizione

                            End If
                        End If
                    End If

                    If Riferimento = "Residenziale Magiera" Then


                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))

                    End If

                    If Riferimento = "Asp Siena" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))

                    End If



                    If Riferimento = "Diurno Soresina" Then
                        If RigaDaCausale <> 3 And RigaDaCausale <> 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                        If RigaDaCausale = 3 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                            Else
                                Dim xAnno As Integer = campodbN(MyRs.Item("AnnoCompetenza"))
                                Dim xMese As Integer = campodbN(MyRs.Item("MeseCompetenza"))


                                MySql = ""

                                OMastro = campodbN(MyRs.Item("MastroControPartita"))
                                OConto = campodbN(MyRs.Item("ContoControPartita"))
                                OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                                CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))
                                CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))

                                Dim DiurnoString As String = ""
                                Dim K As New Cls_Diurno

                                K.Anno = xAnno
                                K.Mese = xMese
                                K.CodiceOspite = CodiceOspite
                                K.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))

                                Dim AppoggioMese As String = ""
                                AppoggioMese = K.LeggiMese(DC_OSPITE, K.CodiceOspite, K.CENTROSERVIZIO, K.Anno, K.Mese)

                                AppoggioMese = Mid(AppoggioMese, 1, GiorniMese(K.Mese, K.Anno) * 2)


                                Dim TrasportoDiurno1 As Integer = 0
                                Dim TrasportoDiurno2 As Integer = 0
                                Dim PresenzeFullTime As Integer = 0
                                Dim PresenzeMattina As Integer = 0
                                Dim PresenzePomeriggio As Integer = 0


                                TrasportoDiurno1 = quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "P1")
                                TrasportoDiurno2 = quanteVolte(AppoggioMese, "t2")
                                PresenzeFullTime = quanteVolte(AppoggioMese, "  ") + quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "t2")
                                PresenzeMattina = quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "PM")
                                PresenzePomeriggio = quanteVolte(AppoggioMese, "P1") + quanteVolte(AppoggioMese, "PP")

                                If PresenzeFullTime > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Full Time (21,00 Euro) :" & PresenzeFullTime
                                End If
                                If PresenzeMattina > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Mattina (15,00 Euro) :" & PresenzeMattina
                                End If
                                If PresenzePomeriggio > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Pomeriggio (9,00 Euro) :" & PresenzePomeriggio
                                End If

                                If TrasportoDiurno1 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "1° Trasporto :" & TrasportoDiurno1
                                End If
                                If TrasportoDiurno2 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "2° Trasporti :" & TrasportoDiurno2
                                End If


                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & DiurnoString
                            End If
                        End If
                    End If


                    If Riferimento = "Diurno Magiera" Then
                        If RigaDaCausale <> 5 And RigaDaCausale <> 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                        If RigaDaCausale = 5 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                            Else
                                'Competenza Dicembre/2017
                                If campodb(MyRs.Item("DescrizioneRiga")).ToUpper.IndexOf("COMPETENZA") >= 0 Or campodb(MyRs.Item("DescrizioneRiga")).ToUpper.IndexOf("TELESOCCORSO") >= 0 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                Else
                                    Dim xAnno As Integer = campodbN(MyRs.Item("AnnoCompetenza"))
                                    Dim xMese As Integer = campodbN(MyRs.Item("MeseCompetenza"))


                                    MySql = ""

                                    OMastro = campodbN(MyRs.Item("MastroControPartita"))
                                    OConto = campodbN(MyRs.Item("ContoControPartita"))
                                    OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                                    CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))
                                    CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))

                                    Dim DiurnoString As String = ""
                                    Dim K As New Cls_Diurno

                                    K.Anno = xAnno
                                    K.Mese = xMese
                                    K.CodiceOspite = CodiceOspite
                                    K.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))

                                    K.Leggi(DC_OSPITE, K.CodiceOspite, K.CENTROSERVIZIO, K.Anno, K.Mese)

                                    Dim TrasportoDiurno As Integer = 0
                                    Dim TrasportoDiurnoASP As Integer = 0
                                    Dim CenaDiurno As Integer = 0


                                    If K.Giorno1 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno2 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno3 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno4 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno5 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno6 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno7 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno8 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno9 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno10 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno11 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno12 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno13 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno14 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno15 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno16 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno17 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno18 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno19 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno20 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno21 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno22 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno23 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno24 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno25 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno26 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno27 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno28 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno29 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno30 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1
                                    If K.Giorno31 = "D5" Then TrasportoDiurno = TrasportoDiurno + 1

                                    If K.Giorno1 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno2 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno3 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno4 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno5 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno6 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno7 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno8 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno9 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno10 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno11 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno12 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno13 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno14 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno15 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno16 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno17 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno18 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno19 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno20 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno21 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno22 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno23 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno24 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno25 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno26 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno27 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno28 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno29 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno30 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1
                                    If K.Giorno31 = "D7" Then TrasportoDiurnoASP = TrasportoDiurnoASP + 1

                                    If K.Giorno1 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno2 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno3 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno4 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno5 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno6 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno7 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno8 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno9 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno10 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno11 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno12 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno13 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno14 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno15 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno16 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno17 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno18 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno19 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno20 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno21 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno22 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno23 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno24 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno25 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno26 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno27 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno28 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno29 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno30 = "D1" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno31 = "D1" Then CenaDiurno = CenaDiurno + 1


                                    If K.Giorno1 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno2 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno3 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno4 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno5 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno6 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno7 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno8 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno9 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno10 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno11 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno12 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno13 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno14 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno15 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno16 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno17 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno18 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno19 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno20 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno21 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno22 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno23 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno24 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno25 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno26 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno27 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno28 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno29 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno30 = "D8" Then CenaDiurno = CenaDiurno + 1
                                    If K.Giorno31 = "D8" Then CenaDiurno = CenaDiurno + 1

                                    If CenaDiurno > 0 Then
                                        DiurnoString = "Cene Diurno :" & CenaDiurno
                                    End If
                                    If TrasportoDiurno > 0 Then
                                        If DiurnoString = "" Then
                                            DiurnoString = DiurnoString & vbNewLine
                                        End If
                                        DiurnoString = DiurnoString & "Trasporto Diurno Rolo :" & TrasportoDiurno
                                    End If
                                    If TrasportoDiurnoASP > 0 Then
                                        If DiurnoString = "" Then
                                            DiurnoString = DiurnoString & vbNewLine
                                        End If
                                        DiurnoString = DiurnoString & "Servizio Trasporto CD ASP :" & TrasportoDiurnoASP
                                    End If

                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & DiurnoString
                                End If
                            End If
                        End If
                    End If
                    If Riferimento = "Domiciliare Magiera" Then
                        If RigaDaCausale <> 5 And RigaDaCausale <> 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                        If RigaDaCausale = 5 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                End If
                            Else
                                If DescrizioneMagiera = False Then
                                    Dim DomiciliareString As String = ""
                                    Dim xAnno As Integer = campodbN(MyRs.Item("AnnoCompetenza"))
                                    Dim xMese As Integer = campodbN(MyRs.Item("MeseCompetenza"))

                                    Dim cn As OleDbConnection

                                    MySql = ""

                                    OMastro = campodbN(MyRs.Item("MastroControPartita"))
                                    OConto = campodbN(MyRs.Item("ContoControPartita"))
                                    OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                                    CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))
                                    CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))

                                    cn = New Data.OleDb.OleDbConnection(DC_OSPITE)

                                    cn.Open()
                                    Dim cmd As New OleDbCommand()
                                    Dim TimeMinutes As Long = 0

                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '01' and Operatore = 1 And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + DateDiff(DateInterval.Minute, myPOSTreader.Item("OraInizio"), myPOSTreader.Item("OraFine"))
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Accreditato 1 Operatore  : " & Int(TimeMinutes / 60) & ":" & Format(TimeMinutes - (Int(TimeMinutes / 60) * 60), "00")

                                    End If

                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '01' and Operatore = 2 And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + DateDiff(DateInterval.Minute, myPOSTreader.Item("OraInizio"), myPOSTreader.Item("OraFine"))
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Accreditato 2 Operatori  : " & Int(TimeMinutes / 60) & ":" & Format(TimeMinutes - (Int(TimeMinutes / 60) * 60), "00")
                                    End If


                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '02' and Operatore = 1 And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + DateDiff(DateInterval.Minute, myPOSTreader.Item("OraInizio"), myPOSTreader.Item("OraFine"))
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Autorizzato 1 Operatore  : " & Int(TimeMinutes / 60) & ":" & Format(TimeMinutes - (Int(TimeMinutes / 60) * 60), "00")
                                    End If

                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '02' and Operatore = 2 And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + DateDiff(DateInterval.Minute, myPOSTreader.Item("OraInizio"), myPOSTreader.Item("OraFine"))
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Autorizzato 2 Operatori  : " & Int(TimeMinutes / 60) & ":" & Format(TimeMinutes - (Int(TimeMinutes / 60) * 60), "00")
                                    End If


                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '03' And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + 1
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Pasto/i  : " & TimeMinutes & " (9,50 Euro Pasto)"
                                    End If

                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '04' And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + 1
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Pasto + Cena Fredda  : " & TimeMinutes & " (10,50 Euro Pasto + Cena Fredda)"
                                    End If

                                    TimeMinutes = 0
                                    cmd.CommandText = ("select * from MovimentiDomiciliare where Tipologia = '05' and  CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And  CodiceOspite = " & CodiceOspite & "  And Data >= ? And Data <= ? ")
                                    cmd.Parameters.Clear()
                                    cmd.Parameters.AddWithValue("@DataMin", DateSerial(xAnno, xMese, 1))
                                    cmd.Parameters.AddWithValue("@DataMax", DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))
                                    cmd.Connection = cn
                                    myPOSTreader = cmd.ExecuteReader()
                                    Do While myPOSTreader.Read
                                        TimeMinutes = TimeMinutes + DateDiff(DateInterval.Minute, myPOSTreader.Item("OraInizio"), myPOSTreader.Item("OraFine"))
                                    Loop
                                    myPOSTreader.Close()
                                    If TimeMinutes > 0 Then
                                        DomiciliareString = DomiciliareString & vbNewLine
                                        DomiciliareString = DomiciliareString & " Tutoring : " & Int(TimeMinutes / 60) & ":" & Format(TimeMinutes - (Int(TimeMinutes / 60) * 60), "00")
                                    End If


                                    If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese) & " " & DomiciliareString
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & DomiciliareString
                                    End If
                                End If
                            End If
                            DescrizioneMagiera = True
                        End If
                    End If

                    If Riferimento = "Don Gnocchi" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Or RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = "Retta mensile " & DecodificaMese(campodbN(MyRs.Item("MeseRiferimento"))) & "/" & campodbN(MyRs.Item("AnnoRiferimento")) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                MyRecordStampaT.Item("Campo3") = campodb(MyRecordStampaT.Item("Campo3")) & " " & DecodificaMese(campodbN(MyRs.Item("MeseRiferimento"))) & "/" & campodbN(MyRs.Item("AnnoRiferimento"))
                            Else
                                'FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " &
                                If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                                If campodb(MyRs.Item("TipoExtra")) = "A01" Or campodb(MyRs.Item("TipoExtra")) = "A19" Then
                                    If campodb(MyRs.Item("DescrizioneRiga")).IndexOf("Competenza") >= 0 Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = "Conguaglio retta " & campodb(MyRs.Item("DescrizioneRiga"))
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = "Conguaglio retta"
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Don Gnocchi Ronzoni" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Or RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = "Retta mensile " & DecodificaMese(campodbN(MyRs.Item("MeseRiferimento"))) & "/" & campodbN(MyRs.Item("AnnoRiferimento")) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                MyRecordStampaT.Item("Campo3") = campodb(MyRecordStampaT.Item("Campo3")) & " " & DecodificaMese(campodbN(MyRs.Item("MeseRiferimento"))) & "/" & campodbN(MyRs.Item("AnnoRiferimento"))
                            Else
                                'FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " &
                                If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))

                                    If campodb(MyRs.Item("TipoExtra")) = "A01" Or campodb(MyRs.Item("TipoExtra")) = "A19" Then
                                        If campodb(MyRs.Item("DescrizioneRiga")).IndexOf("Competenza") >= 0 Then
                                            MyRecordStampaR.Item("DescrizioneRiga") = "Rimborso retta " & campodb(MyRs.Item("DescrizioneRiga"))
                                        Else
                                            MyRecordStampaR.Item("DescrizioneRiga") = "Rimborso retta"
                                        End If
                                    End If
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If

                            End If
                        End If
                    End If

                    If Riferimento = "Virginia Borgheri" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Or RigaDaCausale = 5 Or RigaDaCausale = 6 Or RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                    End If

                    If Riferimento = "Residenziale Cittadella" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Or RigaDaCausale = 5 Or RigaDaCausale = 6 Or RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            Dim P As New Cls_CalcoloRette
                            CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))
                            Dim ComuneCittadella As New Cls_ImportoComune
                            ComuneCittadella.CODICEOSPITE = CodiceOspite
                            ComuneCittadella.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                            ComuneCittadella.UltimaData(DC_OSPITE, ComuneCittadella.CODICEOSPITE, ComuneCittadella.CENTROSERVIZIO)

                            If ComuneCittadella.PROV <> "" And ComuneCittadella.COMUNE <> "" Then
                                Dim MyCodificaComumeCittadella As New ClsComune
                                MyCodificaComumeCittadella.Provincia = ComuneCittadella.PROV
                                MyCodificaComumeCittadella.Comune = ComuneCittadella.COMUNE
                                MyCodificaComumeCittadella.Leggi(DC_OSPITE)

                                If ComuneCittadella.Importo > 0 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione & " Euro " & Format(ComuneCittadella.Importo, "#,##0.00") & " " & ComuneCittadella.Tipo
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione
                                End If

                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If

                        End If
                    End If

                    If Riferimento = "Villa Jole" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") > Format(DateSerial(2017, 6, 26), "yyyy-MM-dd") Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        Else
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If RigaDaCausale = 3 And FnVB6.CampoCausaleContabile(campodb(MyRs.Item("CausaleContabile")), "TIPODOCUMENTO") <> "NC" Then
                                    If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = "Importo Presenze"
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                    End If
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Villa Jole" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") < Format(DateSerial(2017, 6, 26), "yyyy-MM-dd") Then

                        Dim GiorniPresenza As Integer
                        Dim ImportoPresenza As Double
                        'RigaDaCausale = 3 And campodbn(MyRs.Item("MastroPartita")) = 58 And campodbn(MyRs.Item("ContoPartita")) = 20 And campodbn(MyRs.Item("SottocontoPartita")) = 2 
                        If TxtProva = 1 Then
                            GiorniInFattRs.Open("Select sum(Quantita) As Giorni From Temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null) And  SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Else
                            GiorniInFattRs.Open("Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        End If
                        If Not GiorniInFattRs.EOF Then
                            GiorniPresenza = MoveFromDbWC(GiorniInFattRs, "Giorni")
                        End If
                        GiorniInFattRs.Close()


                        If TxtProva = 1 Then
                            GiorniInFattRs.Open("Select sum(Importo) As Totale From Temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null) And  SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Else
                            GiorniInFattRs.Open("Select sum(Importo) As Totale From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        End If
                        If Not GiorniInFattRs.EOF Then
                            ImportoPresenza = MoveFromDbWC(GiorniInFattRs, "Totale")
                        End If
                        GiorniInFattRs.Close()

                        Dim UltModalita As New Cls_Modalita

                        UltModalita.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                        UltModalita.CodiceOspite = CodiceOspite
                        UltModalita.UltimaData(DC_OSPITE, UltModalita.CodiceOspite, UltModalita.CentroServizio)

                        Dim UltStato As New Cls_StatoAuto

                        UltStato.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                        UltStato.CODICEOSPITE = CodiceOspite
                        UltStato.UltimaData(DC_OSPITE, UltModalita.CodiceOspite, UltModalita.CentroServizio)


                        Dim MRj As New Cls_rettatotale

                        MRj.CODICEOSPITE = CodiceOspite
                        MRj.UltimaData(DC_OSPITE, CodiceOspite, campodb(MyRs.Item("CentroServizio")))

                        Dim Xs As New Cls_TipoRetta

                        Xs.Tipo = MRj.TipoRetta
                        Xs.Leggi(DC_OSPITE, Xs.Tipo)

                        If Xs.Descrizione <> "HCP" Then
                            If campodb(MyRs.Item("CentroServizio")) = "2" Then
                                If Trim(UltStato.USL) <> "" Then
                                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.84, 2)
                                    Else
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.84 / 100, 2)
                                    End If
                                Else
                                    If UltStato.STATOAUTO = "N" Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (5.84 + 30.93), 2)
                                    End If
                                End If
                            Else
                                If Trim(UltStato.USL) <> "" Then
                                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                                    Else
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.35 / 100, 2)
                                    End If
                                Else
                                    If UltStato.STATOAUTO = "N" Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (9.73 + 52.32), 2)
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Villa Amelia" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") < Format(DateSerial(2017, 7, 1), "yyyy-MM-dd") Then

                        Dim GiorniPresenza As Integer
                        Dim ImportoPresenza As Double
                        'RigaDaCausale = 3 And campodbn(MyRs.Item("MastroPartita")) = 58 And campodbn(MyRs.Item("ContoPartita")) = 20 And campodbn(MyRs.Item("SottocontoPartita")) = 2 
                        If TxtProva = 1 Then
                            GiorniInFattRs.Open("Select sum(Quantita) As Giorni From Temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null) And  SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Else
                            GiorniInFattRs.Open("Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        End If
                        If Not GiorniInFattRs.EOF Then
                            GiorniPresenza = MoveFromDbWC(GiorniInFattRs, "Giorni")
                        End If
                        GiorniInFattRs.Close()


                        If TxtProva = 1 Then
                            GiorniInFattRs.Open("Select sum(Importo) As Totale From Temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null) And  SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Else
                            GiorniInFattRs.Open("Select sum(Importo) As Totale From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Descrizione like '%Presen%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        End If
                        If Not GiorniInFattRs.EOF Then
                            ImportoPresenza = MoveFromDbWC(GiorniInFattRs, "Totale")
                        End If
                        GiorniInFattRs.Close()

                        Dim UltModalita As New Cls_Modalita

                        UltModalita.CentroServizio = campodb(MyRs.Item("CentroServizio"))
                        UltModalita.CodiceOspite = CodiceOspite
                        UltModalita.UltimaData(DC_OSPITE, UltModalita.CodiceOspite, UltModalita.CentroServizio)

                        Dim UltStato As New Cls_StatoAuto

                        UltStato.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                        UltStato.CODICEOSPITE = CodiceOspite
                        UltStato.UltimaData(DC_OSPITE, UltModalita.CodiceOspite, UltModalita.CentroServizio)


                        If campodb(MyRs.Item("CentroServizio")) = "2" Then
                            If Trim(UltStato.USL) <> "" Then
                                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                                Else
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 16.56 / 100, 2)
                                End If
                            Else
                                If UltStato.STATOAUTO = "N" Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (5.83 + 31.4), 2)
                                Else
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                                End If
                            End If
                        Else
                            If Trim(UltStato.USL) <> "" Then
                                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                                Else
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.47 / 100, 2)
                                End If
                            Else
                                If UltStato.STATOAUTO = "N" Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (9.73 + 52.32), 2)
                                Else
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                                End If
                            End If
                        End If
                    End If


                    If Riferimento = "Villa Amelia" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") >= Format(DateSerial(2017, 7, 1), "yyyy-MM-dd") Then

                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If

                        Else
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If RigaDaCausale = 3 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Importo Presenze"
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If

                        End If
                    End If
                    'IlFaro
                    If Riferimento = "IlFaro" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If
                    'Libera
                    If Riferimento = "Libera" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If
                    'DonMoschetta
                    If Riferimento = "DonMoschetta" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If
                    'OspizioMarino
                    If Riferimento = "OspizioMarino" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If
                    'IstitutoCiechi
                    If Riferimento = "IstitutoCiechi" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If


                    If Riferimento = "ANFFAS Carrara" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If

                    If Riferimento = "CoopSanLorenzo" Then
                        If RigaDaCausale = 9 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If

                    If Riferimento = "MezzaSelva Res" Then

                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If

                        Else
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If RigaDaCausale = 3 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If

                        End If
                    End If

                    If Riferimento = "San Martino" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") >= Format(DateSerial(2019, 1, 1), "yyyy-MM-dd") Then

                        If (campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R") Then
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If

                        Else
                            If RigaDaCausale = 9 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If RigaDaCausale = 3 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Importo Presenze"
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If

                        End If
                    End If



                    If Riferimento = "GG - Retta Ospite" Then

                        If campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R" Then
                            OMastro = campodbN(MyRs.Item("MastroControPartita"))
                            OConto = campodbN(MyRs.Item("ContoControPartita"))
                            OSottoconto = campodbN(MyRs.Item("SottocontoControPartita"))
                            CodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))

                        Else
                            CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))
                        End If


                        If FnVB6.StatoAutoOspite(CodiceOspite, Now) = "A" Then
                            MyStatoAuto = "Autosufficente"
                        Else
                            MyStatoAuto = "Non Autosufficente"
                        End If

                        Dim QuotaInizioMese As Double
                        Dim QuotaFineMese As Double


                        QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0)
                        QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, DateSerial(TxtAnno, Mese, 1))
                        If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                            NQuota = QuotaInizioMese
                        Else
                            NQuota = QuotaFineMese
                        End If

                        For I = 1 To 5
                            QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I)
                            QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I, DateSerial(TxtAnno, Mese, 1))
                            If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                                NQuota = NQuota + QuotaInizioMese
                            Else
                                NQuota = NQuota + QuotaFineMese
                            End If
                        Next


                        MyRecordStampaR.Item("QuotaOspitie") = Math.Round(NQuota, 2)

                        If campodb(MyRs.Item("DescrizioneRiga")) = "Assenze" Then
                            REM NQuota = FnVB6.QuoteGiornaliereAssenza(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, Me)

                            MyRecordStampaR.Item("QuotaOspitie") = NQuota
                        End If

                        TUscite = ""
                        RsMovimenti.Open("Select * From Movimenti Where CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & campodb(MyRs.Item("CentroServizio")) & "' And month(Data) = " & Mese & " And year(Data) = " & TxtAnno, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Do While Not RsMovimenti.EOF
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 13 Then
                                TUscite = TUscite & " Ud : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy") & " " & FnVB6.DecodificaCausaleEntrataUscita(MoveFromDb(RsMovimenti.Fields("Causale")))
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 5 Then
                                TUscite = TUscite & " Ac : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy")
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 3 Then
                                TUscite = TUscite & " U : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy") & " " & FnVB6.DecodificaCausaleEntrataUscita(MoveFromDb(RsMovimenti.Fields("Causale")))
                            End If
                            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = 4 Then
                                TUscite = TUscite & " E : " & Format(MoveFromDb(RsMovimenti.Fields("Data")), "dd/MM/yy")
                            End If
                            RsMovimenti.MoveNext()
                        Loop
                        RsMovimenti.Close()

                        If TUscite <> "" Then
                            TUscite = vbNewLine & TUscite
                        End If
                        If RigaDaCausale <> 9 Then
                            If campodb(MyRs.Item("DescrizioneRiga")) = "Presenze" Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & TUscite
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If

                    If Riferimento = "(O) Descrizione - (E) Conto-contropartita-Descrizione" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Chiara Luce" Then
                        If RigaDaCausale <> 3 And RigaDaCausale <> 13 Then
                            If RigaDaCausale = 5 Or RigaDaCausale = 4 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                        End If
                    End If


                    If Riferimento = "Gabbiano" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                    End If


                    If Riferimento = "Uscita Sicurezza" Then
                        If RigaDaCausale <> 3 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        Else
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & FnVB6.CampoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), "NOME") & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If



                    If Riferimento = "Conto Retta - contropartita - Descrizione" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    Dim DescrizioneConto As String

                                    DescrizioneConto = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                    If DescrizioneConto.ToUpper.IndexOf(UCase(campodb(MyRs.Item("DescrizioneRiga")))) > 0 Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = DescrizioneConto & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = DescrizioneConto & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                    End If
                                End If
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If
                    If Riferimento = "Italcliniche" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    If RigaDaCausale = 4 Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                    End If
                                End If
                                If RigaDaCausale = 3 Then
                                    Dim TpMov As New Cls_Movimenti
                                    Dim TestoAppoggio As String = ""

                                    TpMov.CodiceOspite = CodiceOspite
                                    TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    TpMov.UltimaDataAccoglimento(DC_OSPITE)

                                    If Year(TpMov.Data) = campodb(MyRs.Item("AnnoCompetenza")) And Month(TpMov.Data) = campodb(MyRs.Item("MeseCompetenza")) Then
                                        TestoAppoggio = " Accolto il " & Format(TpMov.Data, "dd/MM/yyyy")
                                    End If


                                    TpMov.Data = Nothing
                                    TpMov.CodiceOspite = CodiceOspite
                                    TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    TpMov.UltimaDataUscitaDefinitiva(DC_OSPITE)

                                    If Year(TpMov.Data) = campodb(MyRs.Item("AnnoCompetenza")) And Month(TpMov.Data) = campodb(MyRs.Item("MeseCompetenza")) Then
                                        TestoAppoggio = TestoAppoggio & " Uscito il " & Format(TpMov.Data, "dd/MM/yyyy")
                                    End If

                                    MyRecordStampaR.Item("DescrizioneRiga") = "Degenza Rsa a carico dell'ospite " & campodb(MyRs.Item("DescrizioneRiga")) & " " & MyRecordStampaT.Item("OspiteRiferimento") & TestoAppoggio
                                End If

                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If


                    If Riferimento = "Don Gnocchi Tricarico" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                End If
                                If RigaDaCausale = 3 Then
                                    Dim TpMov As New Cls_Movimenti
                                    Dim TestoAppoggio As String = ""
                                    Dim AnnoCompetenza As Integer = campodb(MyRs.Item("AnnoRiferimento"))
                                    Dim MeseCompetenza As Integer = campodb(MyRs.Item("MeseRiferimento"))

                                    TpMov.CodiceOspite = CodiceOspite
                                    TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    TpMov.UltimaDataAccoglimento(DC_OSPITE)

                                    Dim DataAccoglimento As Date = TpMov.Data
                                    TestoAppoggio = " Accolto il " & Format(TpMov.Data, "dd/MM/yyyy")


                                    Dim TpRetta As New Cls_rettatotale

                                    If campodb(MyRs.Item("TipoExtra")) = "" Then
                                        TpRetta.CODICEOSPITE = CodiceOspite
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.RettaTotaleAData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO, DateSerial(AnnoCompetenza, MeseCompetenza, GiorniMese(MeseCompetenza, AnnoCompetenza)))
                                    End If
                                    If campodb(MyRs.Item("TipoExtra")) = "1P" Then
                                        TpRetta.CODICEOSPITE = CodiceOspite
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.RettaTotaleAData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO, DateSerial(AnnoCompetenza, MeseCompetenza, 1))
                                    End If
                                    If campodb(MyRs.Item("TipoExtra")) = "2P" Then
                                        TpRetta.CODICEOSPITE = CodiceOspite
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.RettaTotaleAData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO, DateSerial(AnnoCompetenza, MeseCompetenza, GiorniMese(MeseCompetenza, AnnoCompetenza)))
                                    End If
                                    If campodb(MyRs.Item("TipoExtra")) = "1A" Then
                                        TpRetta.CODICEOSPITE = CodiceOspite
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.RettaTotaleAData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO, DateSerial(AnnoCompetenza, MeseCompetenza, 1))
                                    End If
                                    If campodb(MyRs.Item("TipoExtra")) = "2A" Then
                                        TpRetta.CODICEOSPITE = CodiceOspite
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.RettaTotaleAData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO, DateSerial(AnnoCompetenza, MeseCompetenza, GiorniMese(MeseCompetenza, AnnoCompetenza)))
                                    End If


                                    Dim TipoRetta As New Cls_TipoRetta
                                    TipoRetta.Descrizione = ""
                                    If TpRetta.TipoRetta <> "" Then
                                        TipoRetta.Codice = TpRetta.TipoRetta
                                        TipoRetta.Leggi(DC_OSPITE, TipoRetta.Codice)
                                    End If


                                    TpMov.Data = Nothing
                                    TpMov.CodiceOspite = CodiceOspite
                                    TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    TpMov.UltimaDataUscitaDefinitiva(DC_OSPITE)

                                    If Format(DataAccoglimento, "yyyyMMdd") < Format(TpMov.Data, "yyyyMMdd") Then
                                        TestoAppoggio = TestoAppoggio & " Uscito il " & Format(TpMov.Data, "dd/MM/yyyy")
                                    End If

                                    MyRecordStampaR.Item("DescrizioneRiga") = MyRecordStampaT.Item("OspiteRiferimento") & " " & TipoRetta.Descrizione & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & TestoAppoggio
                                End If

                                If RigaDaCausale = 4 Or RigaDaCausale = 6 Then
                                    If campodb(MyRs.Item("TipoExtra")) = "A01" Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = "Conguaglio retta"
                                    End If
                                End If

                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    If RigaDaCausale = 3 Then
                                        Dim TpMov As New Cls_Movimenti
                                        Dim TestoAppoggio As String = ""

                                        TpMov.CodiceOspite = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                        TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpMov.UltimaDataAccoglimento(DC_OSPITE)

                                        If Year(TpMov.Data) = campodb(MyRs.Item("AnnoCompetenza")) And Month(TpMov.Data) = campodb(MyRs.Item("MeseCompetenza")) Then
                                            TestoAppoggio = " Accolto il " & Format(TpMov.Data, "dd/MM/yyyy")
                                        End If

                                        Dim TpRetta As New Cls_rettatotale

                                        TpRetta.CODICEOSPITE = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                        TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpRetta.UltimaData(DC_OSPITE, TpRetta.CODICEOSPITE, TpRetta.CENTROSERVIZIO)

                                        Dim TipoRetta As New Cls_TipoRetta

                                        TipoRetta.Codice = TpRetta.TipoRetta
                                        TipoRetta.Leggi(DC_OSPITE, TipoRetta.Codice)



                                        TpMov.Data = Nothing
                                        TpMov.CodiceOspite = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                        TpMov.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                        TpMov.UltimaDataUscitaDefinitiva(DC_OSPITE)

                                        If Year(TpMov.Data) = campodb(MyRs.Item("AnnoCompetenza")) And Month(TpMov.Data) = campodb(MyRs.Item("MeseCompetenza")) Then
                                            TestoAppoggio = TestoAppoggio & " Uscito il " & Format(TpMov.Data, "dd/MM/yyyy")
                                        End If

                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & TestoAppoggio
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                    End If
                                End If
                            End If
                        End If
                    End If


                    If Riferimento = "ANNISERENI" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") < Format(DateSerial(2019, 2, 20), "yyyy-MM-dd") Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga")))
                                End If
                                If RigaDaCausale = 4 Or RigaDaCausale = 5 Or RigaDaCausale = 6 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If
                    If Riferimento = "ANNISERENI" And Format(campodbD(MyRs.Item("DataRegistrazione")), "yyyy-MM-dd") >= Format(DateSerial(2019, 2, 20), "yyyy-MM-dd") Then
                        MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                    End If


                    If Riferimento = "Moscati" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                End If
                                Dim TpRetta As New Cls_rettatotale

                                TpRetta.CODICEOSPITE = CodiceOspite
                                TpRetta.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))

                                If campodbN(MyRs.Item("AnnoRiferimento")) > 0 Then
                                    TpRetta.RettaTotaleADataSenzaCSERV(DC_OSPITE, TpRetta.CODICEOSPITE, DateSerial(campodb(MyRs.Item("AnnoRiferimento")), campodb(MyRs.Item("MeseRiferimento")), GiorniMese(campodb(MyRs.Item("MeseRiferimento")), campodb(MyRs.Item("AnnoRiferimento")))))
                                Else
                                    TpRetta.RettaTotaleADataSenzaCSERV(DC_OSPITE, TpRetta.CODICEOSPITE, DateSerial(campodb(MyRs.Item("AnnoCompetenza")), campodb(MyRs.Item("MeseCompetenza")), GiorniMese(campodb(MyRs.Item("MeseCompetenza")), campodb(MyRs.Item("AnnoCompetenza")))))
                                End If

                                Dim TipoRetta As New Cls_TipoRetta

                                TipoRetta.Codice = TpRetta.TipoRetta
                                TipoRetta.Leggi(DC_OSPITE, TipoRetta.Codice)



                                If TpRetta.TipoRetta = "01" And RigaDaCausale = 3 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = MyRecordStampaR.Item("DescrizioneRiga") & " Eccedenza economica non a carico del FASI saldata in proprio dall'iscritto/assistito (€ " & TpRetta.Importo & ")"
                                End If
                                If RigaDaCausale = 3 And TipoRetta.Descrizione <> "" And TpRetta.TipoRetta <> "01" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = TipoRetta.Descrizione & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                                If RigaDaCausale = 3 And TipoRetta.Descrizione <> "" And TpRetta.TipoRetta <> "01" And Val(campodb(MyRs.Item("CodiceOspite"))) > 0 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = TipoRetta.Descrizione & " Contributo Cidis " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                                If RigaDaCausale = 5 Or RigaDaCausale = 4 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                End If

                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    Dim Xs As New Cls_TipoRetta
                                    Dim MR As New Cls_rettatotale

                                    Xs.Descrizione = ""
                                    If Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100) = Math.Round(campodbN(MyRs.Item("SottocontoControPartita")) / 100, 2) And RigaDaCausale = 3 Then
                                        MR.CODICEOSPITE = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                        MR.RettaTotaleADataSenzaCSERV(DC_OSPITE, MR.CODICEOSPITE, DateSerial(campodb(MyRs.Item("AnnoCompetenza")), campodb(MyRs.Item("MeseCompetenza")), GiorniMese(campodb(MyRs.Item("MeseCompetenza")), campodb(MyRs.Item("AnnoCompetenza")))))
                                        If MR.TipoRetta <> "" Then


                                            Xs.Codice = MR.TipoRetta
                                            Xs.Leggi(DC_OSPITE, Xs.Codice)
                                        End If
                                    End If

                                    If RigaDaCausale = 3 Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                    End If
                                Else
                                    Dim Xs As New Cls_TipoRetta
                                    Dim MR As New Cls_rettatotale

                                    Xs.Descrizione = ""
                                    If Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100) = Math.Round(campodbN(MyRs.Item("SottocontoControPartita")) / 100, 2) And RigaDaCausale = 3 Then
                                        MR.CODICEOSPITE = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                        MR.RettaTotaleADataSenzaCSERV(DC_OSPITE, MR.CODICEOSPITE, DateSerial(campodb(MyRs.Item("AnnoCompetenza")), campodb(MyRs.Item("MeseCompetenza")), GiorniMese(campodb(MyRs.Item("MeseCompetenza")), campodb(MyRs.Item("AnnoCompetenza")))))
                                        If MR.TipoRetta <> "" Then


                                            Xs.Codice = MR.TipoRetta
                                            Xs.Leggi(DC_OSPITE, Xs.Codice)
                                        End If
                                    End If
                                    If RigaDaCausale = 3 Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & Xs.Descrizione & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                    End If
                                End If
                            End If
                        End If
                    End If


                    If Riferimento = "Virginia" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                If FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) = UCase(campodb(MyRs.Item("DescrizioneRiga"))) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento")
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & UCase(campodb(MyRs.Item("DescrizioneRiga"))) & " " & MyRecordStampaT.Item("OspiteRiferimento")
                                End If
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "R/C Contropartita - Descrizione O/P Retta Descrizione" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "R/C Contropartita - Descrizione O/P Retta Descrizione - DataNascita" Then
                        If RigaDaCausale <> 9 Then
                            If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese) & " - Data Nascita : " & FnVB6.CampoOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), "DATANASCITA")
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Conto Retta - descrizione" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))

                        If campodb(MyRs.Item("DescrizioneRiga")).IndexOf("Assenza") < 0 Then
                            Dim QuotaInizioMese As Double
                            Dim QuotaFineMese As Double

                            Dim AnnoRegistrazione1 As Integer = 0
                            Dim MeseRegistrazione1 As Integer = 0


                            CodiceOspite = campodbN(Rs_Clienti.Item("CodiceOspite"))

                            AnnoRegistrazione1 = Year(campodbD(MyRs.Item("DataRegistrazione")))
                            MeseRegistrazione1 = Month(campodbD(MyRs.Item("DataRegistrazione")))
                            QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0)
                            QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "O", 0, DateSerial(AnnoRegistrazione1, MeseRegistrazione1, 1))
                            If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                                NQuota = QuotaInizioMese
                            Else
                                NQuota = QuotaFineMese
                            End If

                            If campodb(Rs_Clienti.Item("Tipologia")) = "P" Then
                                NQuota = 0
                                Dim I As Integer = 0

                                I = campodbN(Rs_Clienti.Item("CodiceParente"))
                                QuotaFineMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I, DateSerial(AnnoRegistrazione1, MeseRegistrazione1, GiorniMese(MeseRegistrazione1, AnnoRegistrazione1)))
                                QuotaInizioMese = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), CodiceOspite, "P", I, DateSerial(AnnoRegistrazione1, MeseRegistrazione1, 1))
                                If QuotaInizioMese > 0 And QuotaInizioMese < QuotaFineMese Then
                                    NQuota = NQuota + QuotaInizioMese
                                Else
                                    NQuota = NQuota + QuotaFineMese
                                End If
                            End If


                            MyRecordStampaR.Item("QuotaOspitie") = Math.Round(NQuota, 2)
                        End If
                    End If


                    If Riferimento = "Med Service" Then
                        If campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R" Or campodb(Rs_Clienti.Item("Tipologia")) = "J" Then
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = "Retta " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))  ' & " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = "Retta " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) '& " " & FnVB6.DescrizioneRigaOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), TxtAnno, Mese)
                            End If
                        Else
                            If RigaDaCausale = 3 Then
                                If campodbN(MyRs.Item("SottocontoPartita")) = 5800400012 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800300012 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Retta " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100) * 100) & " Alzheimer " & campodb(MyRs.Item("DescrizioneRiga"))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = "Retta " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            Else
                                If RigaDaCausale = 9 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                Else
                                    If campodbN(MyRs.Item("SottocontoPartita")) = 5800300017 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800300016 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800300014 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800300015 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800400016 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800400012 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800400014 Or campodbN(MyRs.Item("SottocontoPartita")) = 5800400015 Then
                                        If campodb(MyRs.Item("DescrizioneRiga")).ToString.IndexOf("Alzheimer") < 0 Then
                                            MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga")) & " Alzheimer" '& " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                        Else
                                            MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                                        End If
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga")) '& " " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                        If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottoContoPartita")))
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Sissa" Then
                        If RigaDaCausale = 3 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = Trim(FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Mese di " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & campodbN(MyRs.Item("AnnoCompetenza")))
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = Trim(FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")))
                        End If
                    End If

                    If Riferimento = "NEFESH" Then
                        Dim NefeshMese As Integer
                        Dim NefeshAnno As Integer

                        NefeshMese = campodbN(MyRs.Item("MeseCompetenza"))
                        NefeshAnno = campodbN(MyRs.Item("AnnoCompetenza"))


                        If campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R" Or campodb(Rs_Clienti.Item("Tipologia")) = "J" Then
                            If RigaDaCausale = 3 Then
                                Dim NefeshOspite As New ClsOspite

                                NefeshOspite.CodiceOspite = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                NefeshOspite.Leggi(DC_OSPITE, NefeshOspite.CodiceOspite)

                                If campodb(MyRs.Item("DescrizioneRiga")).ToString.IndexOf("Assenze") >= 0 Then
                                    Dim UltMov As New Cls_Movimenti
                                    Dim KAssezz As New Cls_Movimenti
                                    Dim Uscita As String = ""

                                    KAssezz.CENTROSERVIZIO = campodb(MyRs.Item("CENTROSERVIZIO"))
                                    KAssezz.CodiceOspite = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                    KAssezz.UltimaDataUscita(DC_OSPITE, KAssezz.CodiceOspite, KAssezz.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))
                                    UltMov.CENTROSERVIZIO = KAssezz.CENTROSERVIZIO
                                    UltMov.CodiceOspite = KAssezz.CodiceOspite
                                    UltMov.UltimaMovimentoPrimaData(DC_OSPITE, UltMov.CodiceOspite, UltMov.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))

                                    If KAssezz.Causale <> "" And ((Month(KAssezz.Data) = NefeshMese And Year(KAssezz.Data) = NefeshAnno) Or UltMov.TipoMov = "03" Or (UltMov.TipoMov = "04" And Day(UltMov.Data) > 1 And Month(UltMov.Data) = NefeshMese And Year(UltMov.Data) = NefeshAnno)) Then

                                        Dim TipoCausale As New Cls_CausaliEntrataUscita


                                        TipoCausale.Codice = KAssezz.Causale
                                        TipoCausale.LeggiCausale(DC_OSPITE)

                                        Uscita = "(" & TipoCausale.Descrizione & ")"
                                    End If

                                    MyRecordStampaR.Item("DescrizioneRiga") = Trim(FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & Mid(NefeshOspite.CognomeOspite & Space(10), 1, 1) & ". " & Mid(NefeshOspite.NomeOspite & Space(10), 1, 1)) & ". " & Uscita
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = Trim(FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & Mid(NefeshOspite.CognomeOspite & Space(10), 1, 1) & ". " & Mid(NefeshOspite.NomeOspite & Space(10), 1, 1)) & ". " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = Trim(FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")))
                            End If
                        Else
                            If RigaDaCausale = 3 Then
                                If campodb(MyRs.Item("DescrizioneRiga")).ToString.IndexOf("Assenze") >= 0 Then

                                    Dim KAssezz As New Cls_Movimenti
                                    Dim UltMov As New Cls_Movimenti
                                    Dim Uscita As String = ""

                                    KAssezz.CENTROSERVIZIO = campodb(MyRs.Item("CENTROSERVIZIO"))
                                    KAssezz.CodiceOspite = Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100)
                                    KAssezz.UltimaDataUscita(DC_OSPITE, KAssezz.CodiceOspite, KAssezz.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))

                                    UltMov.CENTROSERVIZIO = KAssezz.CENTROSERVIZIO
                                    UltMov.CodiceOspite = KAssezz.CodiceOspite
                                    UltMov.UltimaMovimentoPrimaData(DC_OSPITE, UltMov.CodiceOspite, UltMov.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))

                                    If KAssezz.Causale <> "" And ((Month(KAssezz.Data) = NefeshMese And Year(KAssezz.Data) = NefeshAnno) Or UltMov.TipoMov = "03" Or (UltMov.TipoMov = "04" And Day(UltMov.Data) > 1 And Month(UltMov.Data) = NefeshMese And Year(UltMov.Data) = NefeshAnno)) Then
                                        Dim TipoCausale As New Cls_CausaliEntrataUscita


                                        TipoCausale.Codice = KAssezz.Causale
                                        TipoCausale.LeggiCausale(DC_OSPITE)

                                        Uscita = "(" & TipoCausale.Descrizione & ")"
                                    End If

                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga")) & Uscita
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & " " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Conto Retta - descrizione - Betulle" Then
                        If campodb(Rs_Clienti.Item("Tipologia")) = "C" Or campodb(Rs_Clienti.Item("Tipologia")) = "R" Or campodb(Rs_Clienti.Item("Tipologia")) = "J" Then
                            If RigaDaCausale = 3 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                            Else
                                If RigaDaCausale = 9 Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                End If
                            End If
                        Else
                            If RigaDaCausale = 3 Then
                                Dim MeseBetulle As Integer
                                Dim AnnoBetulle As Integer
                                MeseBetulle = campodbN(MyRs.Item("MeseRiferimento"))
                                AnnoBetulle = campodbN(MyRs.Item("AnnoRiferimento"))



                                Dim UltLiv As New Cls_rettatotale

                                UltLiv.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                UltLiv.CODICEOSPITE = FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                UltLiv.UltimaData(DC_OSPITE, UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                                Dim Xs As New Cls_TipoRetta

                                Xs.Tipo = UltLiv.TipoRetta
                                Xs.Leggi(DC_OSPITE, Xs.Tipo)
                                If Xs.Descrizione = "PROVVISORIO" Then
                                    Dim MIng As New Cls_Movimenti

                                    MIng.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    MIng.CodiceOspite = FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                    MIng.UltimaDataAccoglimento(DC_OSPITE)

                                    Dim MUsc As New Cls_Movimenti

                                    MUsc.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                                    MUsc.CodiceOspite = FnVB6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                                    MUsc.UltimaDataUscitaDefinitiva(DC_OSPITE)


                                    If RigaDaCausale = 3 And campodbN(MyRs.Item("MastroPartita")) = 58 And ((campodbN(MyRs.Item("ContoPartita")) = 20 And campodbN(MyRs.Item("SottocontoPartita")) = 2) Or (campodbN(MyRs.Item("ContoPartita")) = 2 Or campodbN(MyRs.Item("SottocontoPartita")) = 5)) Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                    Else
                                        Dim aPPOGGIOVerificaAssistenza As String = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))

                                        If aPPOGGIOVerificaAssistenza.IndexOf("Assistenza") > 0 Then
                                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/MM/yyyy")
                                        Else
                                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                        End If

                                    End If
                                Else
                                    If RigaDaCausale = 3 And campodbN(MyRs.Item("MastroPartita")) = 58 And ((campodbN(MyRs.Item("ContoPartita")) = 20 And campodbN(MyRs.Item("SottocontoPartita")) = 2) Or (campodbN(MyRs.Item("ContoPartita")) = 2 Or campodbN(MyRs.Item("SottocontoPartita")) = 5)) Then
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Mese di " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & campodbN(MyRs.Item("AnnoCompetenza"))
                                    Else
                                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Mese di " & DecodificaMese(MeseBetulle) & AnnoBetulle
                                        If campodb(MyRs.Item("DareAvere")) = DareAvere And TipoDocumento(campodb(MyRs.Item("CausaleContabile"))) = "NC" Then
                                            MyRecordStampaR.Item("DescrizioneRiga") = "Reso " & FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Mese di " & DecodificaMese(MeseBetulle) & AnnoBetulle
                                        End If
                                    End If

                                End If

                            Else
                                Dim AppoggioDescrizione As String = campodb(MyRs.Item("DescrizioneRiga"))
                                '58 20 2 
                                If RigaDaCausale = 5 And campodbN(MyRs.Item("MastroPartita")) = 58 And ((campodbN(MyRs.Item("ContoPartita")) = 20 And campodbN(MyRs.Item("SottocontoPartita")) = 2) Or (campodbN(MyRs.Item("ContoPartita")) = 2 Or campodbN(MyRs.Item("SottocontoPartita")) = 5)) Then
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Mese di " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & campodbN(MyRs.Item("AnnoCompetenza"))
                                Else
                                    MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                                End If
                            End If
                        End If
                    End If


                    If Riferimento = "Conto Retta - Commento - Competenza" Then
                        If RigaDaCausale = 3 Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga")) & " Periodo : " & DecodificaMese(campodbN(MyRs.Item("MeseCompetenza"))) & " / " & campodbN(MyRs.Item("AnnoCompetenza"))
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                    End If

                    If Riferimento = "Conto Retta" Then
                        MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita")))
                    End If


                    If Riferimento = "Conto Retta - ! descrizione" Then
                        If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                        End If
                    End If

                    If Riferimento = "Invita" Then
                        If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                            If campodb(MyRs.Item("DescrizioneRiga")) = "" Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        Else
                            If campodbN(MyRs.Item("RigaDaCausale")) = 12 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaCommentoConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita")))
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & "  " & campodb(MyRs.Item("DescrizioneRiga"))
                            End If
                        End If
                    End If


                    myCodiceOspite = Val(FnVB6.CampoOspiteParenteMCS("CodiceOspite", Val(campodbN(MyRs.Item("MastroControPartita"))), Val(campodbN(MyRs.Item("ContoControPartita"))), Val(campodbN(MyRs.Item("SottocontoControPartita")))))
                    If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                        MyRecordStampaR.Item("ComuneResidenza") = FnVB6.DecodificaComune(FnVB6.CampoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), "RESIDENZAPROVINCIA1"), FnVB6.CampoOspite(campodbN(Rs_Clienti.Item("CodiceOspite")), "RESIDENZACOMUNE1"))
                    Else
                        MyRecordStampaR.Item("ComuneResidenza") = FnVB6.DecodificaComune(FnVB6.CampoOspite(Val(myCodiceOspite), "RESIDENZAPROVINCIA1"), FnVB6.CampoOspite(Val(myCodiceOspite), "RESIDENZACOMUNE1"))
                    End If



                    If Riferimento = "Conto Retta - ControPartita - Dt Nas - TabAnagrafica" Then
                        DataNascita = ""
                        If Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100) = (campodbN(MyRs.Item("SottocontoControPartita")) / 100) Then
                            If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                DataNascita = "Nato il " & FnVB6.CampoOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), "DATANASCITA")
                            End If
                            If FnVB6.CampoOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), "CodiceTabellareUsl") <> "" And campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                                DataNascita = DataNascita & " - " & FnVB6.DecodificaTabella("CUS", FnVB6.CampoOspite(Int(campodbN(MyRs.Item("SottocontoControPartita")) / 100), "CodiceTabellareUsl"))
                            End If
                        End If

                        If (campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P") Then
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & MyRecordStampaT.Item("OspiteRiferimento") & "  " & DataNascita
                        Else
                            MyRecordStampaR.Item("DescrizioneRiga") = FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroPartita")), campodbN(MyRs.Item("ContoPartita")), campodbN(MyRs.Item("SottocontoPartita"))) & "  " & FnVB6.DecodificaConto(campodbN(MyRs.Item("MastroControPartita")), campodbN(MyRs.Item("ContoControPartita")), campodbN(MyRs.Item("SottocontoControPartita"))) & "  " & DataNascita
                        End If
                    End If


                    If campodb(MyRs.Item("DareAvere")) = DareAvere Then
                        importo = ImportoRiga * -1
                    Else
                        importo = ImportoRiga
                    End If
                    MyRecordStampaR.Item("Importo") = importo
                    MyRecordStampaR.Item("CodiceIva") = campodb(MyRs.Item("CodiceIva"))
                    MyRecordStampaR.Item("RegistroIVA") = campodbN(MyRs.Item("RegistroIVA"))

                    Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)

                    WTotaleMerce = WTotaleMerce + importo
                    WImportoSconto = 0
                End If

                If campodb(MyRs.Item("Tipo")) = "IV" Then
                    If FnVB6.CampoCausaleContabile(campodb(MyRs.Item("CausaleContabile")), "TIPODOCUMENTO") = "NC" Then
                        If campodb(MyRs.Item("DareAvere")) = "D" Then
                            Imponibile = campodbN(MyRs.Item("Imponibile"))
                            importo = ImportoRiga
                        Else
                            Imponibile = campodbN(MyRs.Item("Imponibile")) * -1
                            importo = ImportoRiga * -1
                        End If
                    Else
                        If campodb(MyRs.Item("DareAvere")) = "A" Then
                            Imponibile = campodbN(MyRs.Item("Imponibile"))
                            importo = ImportoRiga
                        Else
                            Imponibile = campodbN(MyRs.Item("Imponibile")) * -1
                            importo = ImportoRiga * -1
                        End If
                    End If
                    For I = 1 To 5
                        If WAliquota(I) = campodb(MyRs.Item("CodiceIva")) Then
                            WImponibile(I) = WImponibile(I) + Imponibile
                            WImposta(I) = WImposta(I) + importo
                            Exit For
                        End If
                        If WAliquota(I) = "" And Math.Abs(Imponibile) > 0 Then
                            WAliquota(I) = campodb(MyRs.Item("CodiceIva"))
                            WImponibile(I) = Imponibile
                            WImposta(I) = importo
                            Exit For
                        End If
                    Next I
                End If
            Else
                If (campodbN(MyRs.Item("RigaDaCausale")) = 3 Or campodbN(MyRs.Item("RigaDaCausale")) = 4) And Trim(campodb(MyRs.Item("Tipo"))) = "" Then
                    MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow

                    NumeroRigaRegistrazione = NumeroRigaRegistrazione + 1
                    MyRecordStampaR.Item("NumeroRiga") = NumeroRigaRegistrazione


                    MyRecordStampaR.Item("NumeroFattura") = campodbN(MyRs.Item("NumeroProtocollo"))
                    MyRecordStampaR.Item("DataFattura") = campodbD(MyRs.Item("DataRegistrazione"))
                    MyRecordStampaR.Item("NumeroRegistrazione") = campodbN(MyRs.Item("NumeroRegistrazione"))


                    MyRecordStampaR.Item("TipoRiga") = campodbN(MyRs.Item("RigaDaCausale"))





                    MyRecordStampaR.Item("RigaDaCausale") = campodbN(MyRs.Item("RigaDaCausale"))
                    MyRecordStampaR.Item("DescrizioneRiga") = campodb(MyRs.Item("DescrizioneRiga")) '& " " & MoveFromDbWC(MyRs,"DescrizioneRiga")
                    MyRecordStampaR.Item("Quantita") = 0
                    MyRecordStampaR.Item("Prezzo") = 0
                    MyRecordStampaR.Item("Importo") = campodbN(MyRs.Item("Imponibile"))
                    MyRecordStampaR.Item("CodiceIva") = campodb(MyRs.Item("CodiceIva"))

                    If campodb(Rs_Clienti.Item("Tipologia")) = "C" Then
                        MyRecordStampaT.Item("QUOTAGIORNALIERA") = RegolaQuota.QuoteGiornaliere(campodb(MyRs.Item("CentroServizio")), campodbN(Rs_Clienti.Item("CodiceOspite")), "C", 0, campodbD(MyRs.Item("DataRegistrazione")))
                        MyRecordStampaT.Item("ImportoE") = FnVB6.ImportoExtraComune(campodbN(Rs_Clienti.Item("CodiceOspite")), campodbD(MyRs.Item("DataRegistrazione")))
                    End If
                    MyRecordStampaR.Item("RegistroIVA") = campodbN(MyRs.Item("RegistroIVA"))




                    Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
                    WTotaleMerce = WTotaleMerce + campodbN(MyRs.Item("Imponibile"))
                    WImportoSconto = 0
                    For I = 1 To 5
                        If WAliquota(I) = campodb(MyRs.Item("CodiceIva")) Then
                            WImponibile(I) = WImponibile(I) + campodbN(MyRs.Item("Imponibile"))
                            WImposta(I) = WImposta(I) + ImportoRiga
                            Exit For
                        End If
                        If WAliquota(I) = "" Then
                            WAliquota(I) = campodb(MyRs.Item("CodiceIva"))
                            WImponibile(I) = campodbN(MyRs.Item("Imponibile"))
                            WImposta(I) = ImportoRiga
                            Exit For
                        End If
                    Next I
                End If
            End If
            MyRecordStampaT.Item("TotaleMerce") = WTotaleMerce
            MyRecordStampaT.Item("ImportoSconto") = WImportoSconto
            MyRecordStampaT.Item("NettoMerce") = WTotaleMerce - WImportoSconto

            If campodb(Rs_Clienti.Item("Tipologia")) = "O" Or campodb(Rs_Clienti.Item("Tipologia")) = "P" Then

                CodiceOspite = Val(MyRecordStampaT.Item("CodiceOspite"))

                If campodbN(MyRs.Item("RigaDaCausale")) = 1 Then
                    Dim MR As New Cls_rettatotale

                    MR.CODICEOSPITE = CodiceOspite
                    MR.UltimaData(DC_OSPITE, CodiceOspite, campodb(MyRs.Item("CentroServizio")))


                    If MR.TipoRetta <> "" Then
                        Dim Xs As New Cls_TipoRetta

                        Xs.Tipo = MR.TipoRetta
                        Xs.Leggi(DC_OSPITE, Xs.Tipo)


                        If UCase(campodb(MyRecordStampaT.Item("Descrizione"))) = UCase(campodb(campodb(MyRs.Item("DescrizioneTesta")))) Then
                            If Riferimento = "Virginia" Or Riferimento = "Asp Siena" Then
                                MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Tipo Retta " & Xs.Descrizione
                            Else
                                MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta"))
                            End If
                        Else
                            If Riferimento = "Virginia" Or Riferimento = "Asp Siena" Then
                                MyRecordStampaT.Item("Descrizione") = MyRecordStampaT.Item("Descrizione") & campodb(MyRs.Item("DescrizioneTesta")) & vbNewLine & "Tipo Retta " & Xs.Descrizione
                            Else
                                MyRecordStampaT.Item("Descrizione") = MyRecordStampaT.Item("Descrizione") & campodb(MyRs.Item("DescrizioneTesta"))
                            End If
                        End If
                    Else
                        If UCase(campodb(MyRecordStampaT.Item("Descrizione"))) = UCase(campodb(MyRs.Item("DescrizioneTesta"))) Then
                            MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta"))
                        Else
                            MyRecordStampaT.Item("Descrizione") = MyRecordStampaT.Item("Descrizione") & campodb(MyRs.Item("DescrizioneTesta"))
                        End If
                    End If
                End If


                If Riferimento = "Conto Retta - descrizione - Betulle" Then
                    Dim GiorniPresenza As Integer

                    If TxtProva = 1 Then
                        GiorniInFattRs.Open("Select sum(Quantita) As Giorni From Temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And (Not Descrizione like '%Sconto%' Or Descrizione is Null) And  SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Else
                        GiorniInFattRs.Open("Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Not Descrizione like '%Sconto%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(MyRs.Item("Numero")), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    End If
                    If Not GiorniInFattRs.EOF Then
                        GiorniPresenza = MoveFromDbWC(GiorniInFattRs, "Giorni")
                    End If
                    GiorniInFattRs.Close()

                    If campodb(MyRs.Item("CentroServizio")) = "CD" Then
                    Else
                        Dim MeseComp As Integer = campodbN(MyRs.Item("MeseCompetenza"))
                        Dim AnnoComp As Integer = campodbN(MyRs.Item("AnnoCompetenza"))

                        If MeseComp < 12 Then
                            MeseComp = MeseComp + 1
                        Else
                            MeseComp = 1
                            AnnoComp = AnnoComp + 1
                        End If


                        Dim UltLiv As New Cls_rettatotale

                        UltLiv.CENTROSERVIZIO = campodb(MyRs.Item("CentroServizio"))
                        UltLiv.CODICEOSPITE = CodiceOspite
                        UltLiv.UltimaData(DC_OSPITE, UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                        Dim Xs As New Cls_TipoRetta

                        Xs.Tipo = UltLiv.TipoRetta
                        Xs.Leggi(DC_OSPITE, Xs.Tipo)
                        If Xs.Descrizione = "PROVVISORIO" Then
                            Dim UltAcc As New Cls_Movimenti

                            UltAcc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltAcc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltAcc.UltimaDataAccoglimento(DC_OSPITE)

                            Dim UltUsc As New Cls_Movimenti

                            UltUsc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltUsc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltUsc.UltimaDataUscitaDefinitiva(DC_OSPITE)

                            GiorniPresenza = DateDiff(DateInterval.Day, UltAcc.Data, UltUsc.Data)
                            If GiorniPresenza > 0 And Not IsNothing(MyRecordStampaR) Then
                                If MyRecordStampaR.Item("DescrizioneRiga").ToString.ToUpper.IndexOf("Alberg".ToUpper) >= 0 Then
                                    If campodb(MyRs.Item("CentroServizio")) = "NA" Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro " & Format(GiorniPresenza * 7.24, "0.00")
                                    Else
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro " & Format(GiorniPresenza * 14.14, "0.00")
                                    End If
                                End If
                            Else
                                MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta"))
                            End If
                        Else
                            If campodb(MyRs.Item("CentroServizio")) = "NA" Then
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro 220,00"
                                Else
                                    If GiorniPresenza > 0 Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro " & Format(GiorniPresenza * 7.24, "0.00")
                                    Else
                                        MyRecordStampaT.Item("Descrizione") = MyRecordStampaT.Item("Descrizione") & campodb(MyRs.Item("DescrizioneTesta"))
                                    End If
                                End If
                            Else
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro 430,00"
                                Else
                                    If GiorniPresenza > 0 Then
                                        MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta")) & "L’importo dell’assistenza specifica di base alla persona, garantita da personale qualificato, e ricompresa nella quota alberghiera è quantificato in Euro " & Format(GiorniPresenza * 14.14, "0.00")
                                    Else
                                        MyRecordStampaT.Item("Descrizione") = MyRecordStampaT.Item("Descrizione") & campodb(MyRs.Item("DescrizioneTesta"))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else

                MyRecordStampaT.Item("Descrizione") = campodb(MyRs.Item("DescrizioneTesta"))
            End If


            'If TipoDataBaseGenerale <> TipoOracle Then            
        Loop
        If FnVB6.CampoCausaleContabile(CausaleContabile, "TipoDocumento") = "CR" Then

            ImportoPagato = 0
            MySql = "SELECT * FROM TabellaLegami WHERE CodiceDocumento = " & OldRegistrazione
            Rs_Pagato.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do Until Rs_Pagato.EOF
                ImportoPagato = ImportoPagato + MoveFromDbWC(Rs_Pagato, "Importo")
                Rs_Pagato.MoveNext()
            Loop
            MyRecordStampaT.Item("ImportoPagato") = ImportoPagato
            Rs_Pagato.Close()
        End If
        MyRecordStampaT.Item("Imponibile1") = WImponibile(1)
        If WAliquota(1) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA1") = WAliquota(1) & " - " & FnVB6.CampoIVA(WAliquota(1), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta1") = WImposta(1)
        MyRecordStampaT.Item("Imponibile2") = WImponibile(2)
        If WAliquota(2) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA2") = WAliquota(2) & " - " & FnVB6.CampoIVA(WAliquota(2), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta2") = WImposta(2)
        MyRecordStampaT.Item("Imponibile3") = WImponibile(3)
        If WAliquota(3) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA3") = WAliquota(3) & " - " & FnVB6.CampoIVA(WAliquota(3), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta3") = WImposta(3)
        MyRecordStampaT.Item("Imponibile4") = WImponibile(4)
        If WAliquota(4) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA4") = WAliquota(4) & " - " & FnVB6.CampoIVA(WAliquota(4), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta4") = WImposta(4)
        MyRecordStampaT.Item("Imponibile5") = WImponibile(5)
        If WAliquota(5) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA5") = WAliquota(5) & " - " & FnVB6.CampoIVA(WAliquota(5), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta5") = WImposta(5)
        VImportoInTesto = WImponibile(1) + WImponibile(2) + WImponibile(3) + WImponibile(4) + WImposta(1) + WImposta(2) + WImposta(3) + WImposta(4)
        MyRecordStampaT.Item("ImportoInTesto") = ClsImpLettere.ImportoInTesto(VImportoInTesto, 50)
        MyRecordStampaT.Item("ImportoAnticipo") = TotaleAnticipo

        MyRecordStampaT.Item("CampoOrdinamento") = Mid(MyRecordStampaT.Item("RagioneSociale") & Space(50), 1, 50) & MyRecordStampaT.Item("NumeroFattura")

        Dim Log2 As New Cls_LogPrivacy

        Log2.LogPrivacy(SessioneTP("SENIOR"), SessioneTP("CLIENTE"), SessioneTP("UTENTE"), SessioneTP("UTENTEIMPER"), "OSPITIWEB_STAMPADOCUMENTI_ASPX", 0, 0, "", "", MyRecordStampaT.Item("NumeroRegistrazione"), "", "", "", "")

        Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)

        MyRs.Close()


        '
        '  Ordina Numero Riga
        '
        Indice = 1
        Oldfattura = 0
        mYdESC = ""
        Dim XO As Integer

        Dim foundRows As System.Data.DataRow()
        foundRows = Stampa.Tables("FatturaRighe").Select("", "NumeroFattura,ComuneResidenza,DescrizioneRiga")

        For XO = 0 To foundRows.Length - 1
            If Oldfattura <> foundRows(XO).Item("NumeroFattura") Then
                Indice = 0
            End If


            If Not IsDBNull(foundRows(XO).Item("DescrizioneRiga")) Then
                PredentePA = InStr(foundRows(XO).Item("DescrizioneRiga"), "Presenze") - 1
                If PredentePA <= 0 Then
                    PredentePA = InStr(foundRows(XO).Item("DescrizioneRiga"), "Assenze") - 1
                End If
                If PredentePA <= 0 Then
                    PredentePA = Len(foundRows(XO).Item("DescrizioneRiga")) - 1
                End If

                If PredentePA < 0 Then
                    PredentePA = 0
                End If
                If Mid(foundRows(XO).Item("DescrizioneRiga"), 1, PredentePA) <> mYdESC Then
                    Indice = Indice + 1
                End If
                mYdESC = Mid(foundRows(XO).Item("DescrizioneRiga"), 1, PredentePA)
            End If


            foundRows(XO).Item("NumeroRiga") = Indice

            Oldfattura = foundRows(XO).Item("NumeroFattura")
        Next


        '
        '
        '


        Dim Periodo(1000) As String
        Dim Riga(1000) As String
        Dim XP As Integer

        Stampa.Tables("FatturaTesta").Select("", "NumeroFattura")
        Dim foundRowsT As System.Data.DataRow()
        foundRowsT = Stampa.Tables("FatturaTesta").Select("", "NumeroFattura")
        For XO = 0 To foundRowsT.Length - 1


            X = 0
            Dim XMese As String


            Dim foundRowsR As System.Data.DataRow()
            foundRowsR = Stampa.Tables("FatturaRighe").Select("NUMEROFATTURA =   " & foundRowsT(XO).Item("NumeroFattura"), "ComuneResidenza,DescrizioneRiga")


            For XP = 0 To foundRowsR.Length - 1
                X = X + 1
                Anno = ""
                XMese = ""
                Riga(X) = foundRowsR(XP).Item("NumeroRiga")
                If Not IsDBNull(foundRowsR(XP).Item("DescrizioneRiga")) Then
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2013") > 0 Then
                        Anno = "2013"
                    End If
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2014") > 0 Then
                        Anno = "2014"
                    End If
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2015") > 0 Then
                        Anno = "2015"
                    End If
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2016") > 0 Then
                        Anno = "2016"
                    End If
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2017") > 0 Then
                        Anno = "2017"
                    End If

                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2018") > 0 Then
                        Anno = "2018"
                    End If
                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2019") > 0 Then
                        Anno = "2019"
                    End If

                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2020") > 0 Then
                        Anno = "2020"
                    End If

                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2021") > 0 Then
                        Anno = "2021"
                    End If


                    If InStr(1, foundRowsR(XP).Item("DescrizioneRiga"), "2022") > 0 Then
                        Anno = "2022"
                    End If

                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Gennaio")) > 0 Then
                        XMese = "01"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Febbraio")) > 0 Then
                        XMese = "02"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Marzo")) > 0 Then
                        XMese = "03"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Aprile")) > 0 Then
                        XMese = "04"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Maggio")) > 0 Then
                        XMese = "05"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Giugno")) > 0 Then
                        XMese = "06"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Luglio")) > 0 Then
                        XMese = "07"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Agosto")) > 0 Then
                        XMese = "08"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Settembre")) > 0 Then
                        XMese = "09"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Ottobre")) > 0 Then
                        XMese = "10"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Novembre")) > 0 Then
                        XMese = "11"
                    End If
                    If InStr(1, UCase(foundRowsR(XP).Item("DescrizioneRiga")), UCase("Dicembre")) > 0 Then
                        XMese = "12"
                    End If
                End If
                Periodo(X) = Anno & XMese
            Next


            For V = 1 To X
                For T = V + 1 To X
                    If Periodo(V) > Periodo(T) Then
                        Appoggio = Periodo(V)
                        Periodo(V) = Periodo(T)
                        Periodo(T) = Appoggio

                        Appoggio = Riga(V)
                        Riga(V) = Riga(T)
                        Riga(T) = Appoggio
                    End If
                Next
            Next
            Dim CreaSQL As String

            CreaSQL = ""


            If Stampa.Tables("FatturaTesta").Rows(XO).Item("NumeroFattura") = 967 Then
                CreaSQL = ""
            End If
            Dim MInd As Integer
            For V = 1 To X
                Stampa.Tables("FatturaRighe").Select("NUMEROFATTURA =   " & foundRowsT(XO).Item("NumeroFattura") & " And NumeroRiga = " & Riga(V), "DescrizioneRiga")

                For MInd = 0 To Stampa.Tables("FatturaRighe").Rows.Count - 1
                    If Stampa.Tables("FatturaRighe").Rows(MInd).Item("NUMEROFATTURA") = foundRowsT(XO).Item("NumeroFattura") And Stampa.Tables("FatturaRighe").Rows(MInd).Item("NumeroRiga") = Riga(V) Then
                        Stampa.Tables("FatturaRighe").Rows(MInd).Item("NumeroRiga") = V + 1000
                    End If
                Next MInd
            Next
        Next



        GeneraleDb.Close()
        OspitiDb.Close()

        RegolaQuota.ChiudiDB()
        SessioneTP("stampa") = Stampa
        SessioneTP("CampoProgressBar") = 101

        System.Web.HttpRuntime.Cache("stampa" + SessioneTP.SessionID) = Stampa
        System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 101


    End Sub


    Private Function LeggiScadenza(ByVal Prova As Integer, ByVal NumeroRegistrazioneContabile As Long, ByRef ImportoScadenze As Double) As Date
        Dim cn As OleDbConnection
        Dim Max As Long
        ImportoScadenze = 0
        cn = New Data.OleDb.OleDbConnection(DC_GENERALE)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Prova = 1 Then
            cmd.CommandText = ("select * from Temp_Scadenzario where " & _
                                   "NumeroRegistrazioneContabile = ? ")
        Else
            cmd.CommandText = ("select * from Scadenzario where " & _
                                   "NumeroRegistrazioneContabile = ? ")
        End If
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            If IsDBNull(VerReader.Item("DataScadenza")) Then
                LeggiScadenza = Nothing
            Else
                LeggiScadenza = VerReader.Item("DataScadenza")
            End If
            If campodb(VerReader.Item("Descrizione")) <> "IMPONIBILE" Then
                ImportoScadenze = ImportoScadenze + campodbN(VerReader.Item("Importo"))
            End If
        Loop
        VerReader.Close()
        cn.Close()
    End Function


    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function



    Private Function TipoDocumento(ByVal CausaleContabile As String) As String
        Dim WTipo As String
        Dim FnVB6 As New Cls_FunzioniVB6
        FnVB6.StringaOspiti = DC_OSPITE
        FnVB6.StringaTabelle = DC_TABELLE
        FnVB6.StringaGenerale = DC_GENERALE
        FnVB6.ApriDB()

        WTipo = FnVB6.CampoCausaleContabile(CausaleContabile, "TipoDocumento")
        If WTipo = "FA" Or WTipo = "RE" Then
            TipoDocumento = "FATTURA"
        End If
        If WTipo = "NC" Then
            TipoDocumento = "NOTA CREDITO"
        End If
        If WTipo = "ND" Then
            TipoDocumento = "NOTA DEBITO"
        End If
        If WTipo = "CR" Then
            TipoDocumento = "CORRISPETTIVO"
        End If

        FnVB6.ChiudiDB()
    End Function

    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
