Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipoOperazione
    Public Codice As String
    Public Descrizione As String
    Public CausaleRetta As String
    Public CausaleAccredito As String
    Public CausaleAddebito As String
    Public CausaleIncasso As String
    Public CausaleStorno As String
    Public SoggettaABollo As String
    Public StampaBollettinoPostale As String
    Public StampaFattura As String
    Public CausaleDocumentoAnticipo As String
    Public Anticipo As String
    Public SoloAnticipo As String
    Public CausaleAnticipo As String
    Public ParentiOspiti As Long
    Public DocumentoZero As String
    Public NonContoAnticipi As String
    Public GirocontoAnticipo As String
    Public SCORPORAIVA As Long
    Public CausaleDeposito As String
    Public CausaleNcDeposito As String
    Public CausaleCaparraConfirmatoria As String
    Public NcCausaleCaparraConfirmatoria As String
    Public CodiceIVACaparra As String
    Public RegolaDeposito As String
    Public ImportoDeposito As Double
    Public TipoAddebitoAccredito As String
    Public TipoAddebitoAccredito2 As String
    Public PercentualeDivisione As Double
    Public CENTROSERVIZIO As String
    Public Struttura As String
    Public TIPOFILTRO As String
    Public NonBolloConIVA As Integer
    Public BolloVirtuale As Integer

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from AnagraficaComune Where TipoOperazione = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Sub UpDateDropBoxCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoOperazione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Public Sub UpDateDropBoxCentroServizio(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal CentroServizio As String, ByVal Tipo As String)
        Dim cn As OleDbConnection



        Dim CS As New Cls_CentroServizio

        CS.CENTROSERVIZIO = CentroServizio
        CS.Leggi(StringaConnessione, CS.CENTROSERVIZIO)

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Tipo = "O" Then
            cmd.CommandText = ("select * from TipoOperazione WHERE (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?))  And  (TIPOFILTRO = 'O' OR (TIPOFILTRO ='' OR TIPOFILTRO IS NULL)) Order by Descrizione")
        End If
        If Tipo = "E" Then
            cmd.CommandText = ("select * from TipoOperazione WHERE (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?))  And  (TIPOFILTRO = 'E' OR (TIPOFILTRO ='' OR TIPOFILTRO IS NULL)) Order by Descrizione")
        End If
        If Tipo = "" Then
            cmd.CommandText = ("select * from TipoOperazione WHERE (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?))  Order by Descrizione")
        End If

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd.Parameters.AddWithValue("@Struttura", CS.Villa)

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Public Sub UpDateDropBoxTipo(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal Tipo As String)
        Dim cn As OleDbConnection



        Dim CS As New Cls_CentroServizio

        CS.CENTROSERVIZIO = CentroServizio
        CS.Leggi(StringaConnessione, CS.CENTROSERVIZIO)

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Tipo = "O" Then
            cmd.CommandText = ("select * from TipoOperazione WHERE  (TIPOFILTRO = 'O' OR (TIPOFILTRO ='' OR TIPOFILTRO IS NULL)) Order by Descrizione")
        End If
        If Tipo = "E" Then
            cmd.CommandText = ("select * from TipoOperazione WHERE  (TIPOFILTRO = 'E' OR (TIPOFILTRO ='' OR TIPOFILTRO IS NULL)) Order by Descrizione")
        End If
        If Tipo = "" Then
            cmd.CommandText = ("select * from TipoOperazione Order by Descrizione")
        End If

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd.Parameters.AddWithValue("@Struttura", CS.Villa)

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoOperazione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoOperazione where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoOperazione where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TipoOperazione (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE TipoOperazione SET " & _
                "  Descrizione = ?," & _
                "  CausaleRetta = ?," & _
                "  CausaleAccredito = ?," & _
                "  CausaleAddebito = ?," & _
                "  CausaleIncasso = ?," & _
                "  CausaleStorno = ?," & _
                "  SoggettaABollo = ?," & _
                "  StampaBollettinoPostale = ?," & _
                "  StampaFattura = ?," & _
                "  CausaleDocumentoAnticipo = ?," & _
                "  Anticipo = ?," & _
                "  SoloAnticipo = ?," & _
                "  CausaleAnticipo = ?," & _
                "  ParentiOspiti = ?," & _
                "  DocumentoZero = ?," & _
                "  NonContoAnticipi = ?," & _
                "  GirocontoAnticipo = ?," & _
                "  SCORPORAIVA = ?," & _
                "  CausaleDeposito = ?," & _
                "  CausaleNcDeposito = ?, " & _
                "  CausaleCaparraConfirmatoria = ?, " & _
                "  NcCausaleCaparraConfirmatoria = ?, " & _
                "  CodiceIVACaparra = ?, " & _
                "  RegolaDeposito = ?, " & _
                "  ImportoDeposito = ?, " & _
                " TipoAddebitoAccredito = ?, " & _
                " TipoAddebitoAccredito2 = ?, " & _
                " PercentualeDivisione = ?, " & _
                " CENTROSERVIZIO = ?, " & _
                " Struttura= ?, " & _
                " TIPOFILTRO = ?, " & _
                " NonBolloConIVA = ?, " & _
                " BolloVirtuale = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql        
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@CausaleRetta", CausaleRetta)
        cmd1.Parameters.AddWithValue("@CausaleAccredito", CausaleAccredito)
        cmd1.Parameters.AddWithValue("@CausaleAddebito", CausaleAddebito)
        cmd1.Parameters.AddWithValue("@CausaleIncasso", CausaleIncasso)
        cmd1.Parameters.AddWithValue("@CausaleStorno", CausaleStorno)
        cmd1.Parameters.AddWithValue("@SoggettaABollo", SoggettaABollo)
        cmd1.Parameters.AddWithValue("@StampaBollettinoPostale", StampaBollettinoPostale)
        cmd1.Parameters.AddWithValue("@StampaFattura", StampaFattura)
        cmd1.Parameters.AddWithValue("@CausaleDocumentoAnticipo", CausaleDocumentoAnticipo)
        cmd1.Parameters.AddWithValue("@Anticipo", Anticipo)
        cmd1.Parameters.AddWithValue("@SoloAnticipo", SoloAnticipo)
        cmd1.Parameters.AddWithValue("@CausaleAnticipo", CausaleAnticipo)
        cmd1.Parameters.AddWithValue("@ParentiOspiti", ParentiOspiti)
        cmd1.Parameters.AddWithValue("@DocumentoZero", DocumentoZero)
        cmd1.Parameters.AddWithValue("@NonContoAnticipi", NonContoAnticipi)
        cmd1.Parameters.AddWithValue("@GirocontoAnticipo", GirocontoAnticipo)
        cmd1.Parameters.AddWithValue("@SCORPORAIVA", SCORPORAIVA)
        cmd1.Parameters.AddWithValue("@CausaleDeposito", CausaleDeposito)
        cmd1.Parameters.AddWithValue("@CausaleNcDeposito", CausaleNcDeposito)

        cmd1.Parameters.AddWithValue("@CausaleCaparraConfirmatoria", CausaleCaparraConfirmatoria)
        cmd1.Parameters.AddWithValue("@NcCausaleCaparraConfirmatoria", NcCausaleCaparraConfirmatoria)
        cmd1.Parameters.AddWithValue("@CodiceIVACaparra", CodiceIVACaparra)
        cmd1.Parameters.AddWithValue("@RegolaDeposito", RegolaDeposito)
        cmd1.Parameters.AddWithValue("@ImportoDeposito", ImportoDeposito)
        cmd1.Parameters.AddWithValue("@TipoAddebitoAccredito", TipoAddebitoAccredito)
        cmd1.Parameters.AddWithValue("@TipoAddebitoAccredito2", TipoAddebitoAccredito2)
        cmd1.Parameters.AddWithValue("@PercentualeDivisione", PercentualeDivisione)

        cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
        cmd1.Parameters.AddWithValue("@Struttura", Struttura)
        cmd1.Parameters.AddWithValue("@TIPOFILTRO", TIPOFILTRO)

        cmd1.Parameters.AddWithValue("@NonBolloConIVA", NonBolloConIVA)

        cmd1.Parameters.AddWithValue("@BolloVirtuale", BolloVirtuale)



        'NonBolloConIVA

        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal XDescrizione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoOperazione where " & _
                           " Descrizione = ?")
        cmd.Parameters.AddWithValue("@XDescrizione", XDescrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Leggi(StringaConnessione, Codice)
        End If
        cn.Close()
    End Sub

    Function MaxTipoOperazione(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoOperazione = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END)  from TipoOperazione ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If


            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal XCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoOperazione where " & _
                           " Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", XCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            CausaleRetta = campodb(myPOSTreader.Item("CausaleRetta"))
            CausaleAccredito = campodb(myPOSTreader.Item("CausaleAccredito"))
            CausaleAddebito = campodb(myPOSTreader.Item("CausaleAddebito"))
            CausaleIncasso = campodb(myPOSTreader.Item("CausaleIncasso"))
            CausaleStorno = campodb(myPOSTreader.Item("CausaleStorno"))
            SoggettaABollo = campodb(myPOSTreader.Item("SoggettaABollo"))
            StampaBollettinoPostale = campodb(myPOSTreader.Item("StampaBollettinoPostale"))
            StampaFattura = campodb(myPOSTreader.Item("StampaFattura"))
            CausaleDocumentoAnticipo = campodb(myPOSTreader.Item("CausaleDocumentoAnticipo"))
            Anticipo = campodb(myPOSTreader.Item("Anticipo"))
            SoloAnticipo = campodb(myPOSTreader.Item("SoloAnticipo"))
            CausaleAnticipo = campodb(myPOSTreader.Item("CausaleAnticipo"))
            ParentiOspiti = Val(campodb(myPOSTreader.Item("ParentiOspiti")))
            DocumentoZero = campodb(myPOSTreader.Item("DocumentoZero"))
            NonContoAnticipi = campodb(myPOSTreader.Item("NonContoAnticipi"))
            GirocontoAnticipo = campodb(myPOSTreader.Item("GirocontoAnticipo"))
            SCORPORAIVA = Val(campodb(myPOSTreader.Item("SCORPORAIVA")))
            CausaleDeposito = campodb(myPOSTreader.Item("CausaleDeposito"))
            CausaleNcDeposito = campodb(myPOSTreader.Item("CausaleNcDeposito"))

            CausaleCaparraConfirmatoria = campodb(myPOSTreader.Item("CausaleCaparraConfirmatoria"))
            NcCausaleCaparraConfirmatoria = campodb(myPOSTreader.Item("NcCausaleCaparraConfirmatoria"))
            CodiceIVACaparra = campodb(myPOSTreader.Item("CodiceIVACaparra"))
            RegolaDeposito = campodb(myPOSTreader.Item("RegolaDeposito"))
            ImportoDeposito = campodbn(myPOSTreader.Item("ImportoDeposito"))

            TipoAddebitoAccredito = campodb(myPOSTreader.Item("TipoAddebitoAccredito"))
            TipoAddebitoAccredito2 = campodb(myPOSTreader.Item("TipoAddebitoAccredito2"))

            PercentualeDivisione = campodbn(myPOSTreader.Item("PercentualeDivisione"))

            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Struttura = campodb(myPOSTreader.Item("Struttura"))

            TIPOFILTRO = campodb(myPOSTreader.Item("TIPOFILTRO"))

            NonBolloConIVA = campodbn(myPOSTreader.Item("NonBolloConIVA"))

            BolloVirtuale = campodbn(myPOSTreader.Item("BolloVirtuale"))
        End If
        cn.Close()
    End Sub
End Class
