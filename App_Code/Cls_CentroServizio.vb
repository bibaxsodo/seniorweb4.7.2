Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CentroServizio
    Public CENTROSERVIZIO As String
    Public DESCRIZIONE As String
    Public TIPOCENTROSERVIZIO As String
    Public MASTRO As Long
    Public CONTO As Long
    Public SETTIMANA As String
    Public GIORNO0101 As String
    Public GIORNO0601 As String
    Public GIORNO1903 As String
    Public GIORNO2504 As String
    Public GIORNO0105 As String
    Public GIORNO0206 As String
    Public GIORNO2906 As String
    Public GIORNO1508 As String
    Public GIORNO0111 As String
    Public GIORNO0411 As String
    Public GIORNO0812 As String
    Public GIORNO2512 As String
    Public GIORNO2612 As String
    Public GIORNO1 As String
    Public GIORNO1ATTIVO As String
    Public GIORNO2 As String
    Public GIORNO2ATTIVO As String
    Public GIORNO3 As String
    Public GIORNO3ATTIVO As String
    Public GIORNO4 As String
    Public GIORNO4ATTIVO As String
    Public GIORNO5 As String
    Public GIORNO5ATTIVO As String
    Public Regole As String
    Public Emissione As String
    Public Banca As String
    Public CAB As String
    Public ABI As String
    Public VerificaImpegnativa As String
    Public DescrizioneRigaInFattura As String
    Public ExtraFissiRetta As String
    Public Mese1 As String
    Public Mese2 As String
    Public Mese3 As String
    Public Mese4 As String
    Public Mese5 As String
    Public Convenzione As String
    Public EmissioneOP As Long
    Public Utente As String
    Public Villa As String
    Public ImportoMensile As Integer
    Public EPersonam As Long
    Public EPersonamN As Long
    Public CausaleTempoPieno As String
    Public CausaleTempoPienoAss As String
    Public CausaleTempoPienoAss1 As String
    Public CausaleTempoPienoAss2 As String
    Public CausalePartTime As String
    Public CausalePartTimeAss As String
    Public CausalePartTimeAss1 As String
    Public CausalePartTimeAss2 As String
    Public CausaleChiuso As String

    Public NonCalcolare As Integer

    Public CODREGIONE As String
    Public CODASL As String
    Public CODSSA As String
    Public CampoPerEsatto As String
    Public IncrementaNumeroCartella As Integer
    Public MeseFatturazione As Integer
    Public AnnoFatturazione As Integer


    Public Sub New()
        CENTROSERVIZIO = ""
        DESCRIZIONE = ""
        TIPOCENTROSERVIZIO = ""
        MASTRO = 0
        CONTO = 0
        SETTIMANA = ""
        GIORNO0101 = ""
        GIORNO0601 = ""
        GIORNO1903 = ""
        GIORNO2504 = ""
        GIORNO0105 = ""
        GIORNO0206 = ""
        GIORNO2906 = ""
        GIORNO1508 = ""
        GIORNO0111 = ""
        GIORNO0411 = ""
        GIORNO0812 = ""
        GIORNO2512 = ""
        GIORNO2612 = ""
        GIORNO1 = ""
        GIORNO1ATTIVO = ""
        GIORNO2 = ""
        GIORNO2ATTIVO = ""
        GIORNO3 = ""
        GIORNO3ATTIVO = ""
        GIORNO4 = ""
        GIORNO4ATTIVO = ""
        GIORNO5 = ""
        GIORNO5ATTIVO = ""
        Regole = ""
        Emissione = ""
        Banca = ""
        CAB = ""
        ABI = ""
        VerificaImpegnativa = ""
        DescrizioneRigaInFattura = ""
        ExtraFissiRetta = ""
        Mese1 = ""
        Mese2 = ""
        Mese3 = ""
        Mese4 = ""
        Mese5 = ""
        Convenzione = ""
        EmissioneOP = 0
        Utente = ""
        Villa = ""
        ImportoMensile = 0
        EPersonam = 0
        EPersonamN = 0
        CausaleTempoPieno = ""
        CausaleTempoPienoAss = ""
        CausaleTempoPienoAss1 = ""
        CausaleTempoPienoAss2 = ""
        CausalePartTime = ""
        CausalePartTimeAss = ""
        CausalePartTimeAss1 = ""
        CausalePartTimeAss2 = ""
        CausaleChiuso = ""
        NonCalcolare = 0
        CODREGIONE = ""
        CODASL = ""
        CODSSA = ""
        CampoPerEsatto = ""

        IncrementaNumeroCartella = 0
        MeseFatturazione = 0
        AnnoFatturazione = 0
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Integer
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub Elimina(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TABELLACENTROSERVIZIO Where  CENTROSERVIZIO = '" & Codice & "'")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
    End Sub

    Sub LeggiEpersonam(ByVal StringaConnessione As String, ByVal Codice As String, ByVal conv As Boolean)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If conv = True Then
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where  EPersonam = " & EPersonam)
        Else
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where  EPersonamN = " & EPersonam)
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))

            Call Leggi(StringaConnessione, CENTROSERVIZIO)
        End If
        cn.Close()


    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where  CENTROSERVIZIO = '" & Codice & "'")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            TIPOCENTROSERVIZIO = campodb(myPOSTreader.Item("TIPOCENTROSERVIZIO"))
            MASTRO = myPOSTreader.Item("MASTRO")
            CONTO = myPOSTreader.Item("CONTO")
            SETTIMANA = campodb(myPOSTreader.Item("SETTIMANA"))
            GIORNO0101 = campodb(myPOSTreader.Item("GIORNO0101"))
            GIORNO0601 = campodb(myPOSTreader.Item("GIORNO0601"))
            GIORNO1903 = campodb(myPOSTreader.Item("GIORNO1903"))
            GIORNO2504 = campodb(myPOSTreader.Item("GIORNO2504"))
            GIORNO0105 = campodb(myPOSTreader.Item("GIORNO0105"))
            GIORNO0206 = campodb(myPOSTreader.Item("GIORNO0206"))
            GIORNO2906 = campodb(myPOSTreader.Item("GIORNO2906"))
            GIORNO1508 = campodb(myPOSTreader.Item("GIORNO1508"))
            GIORNO0111 = campodb(myPOSTreader.Item("GIORNO0111"))
            GIORNO0411 = campodb(myPOSTreader.Item("GIORNO0411"))
            GIORNO0812 = campodb(myPOSTreader.Item("GIORNO0812"))
            GIORNO2512 = campodb(myPOSTreader.Item("GIORNO2512"))
            GIORNO2612 = campodb(myPOSTreader.Item("GIORNO2612"))
            GIORNO1 = campodb(myPOSTreader.Item("GIORNO1"))
            GIORNO1ATTIVO = campodb(myPOSTreader.Item("GIORNO1ATTIVO"))
            GIORNO2 = campodb(myPOSTreader.Item("GIORNO2"))
            GIORNO2ATTIVO = campodb(myPOSTreader.Item("GIORNO2ATTIVO"))
            GIORNO3 = campodb(myPOSTreader.Item("GIORNO3"))
            GIORNO3ATTIVO = campodb(myPOSTreader.Item("GIORNO3ATTIVO"))
            GIORNO4 = campodb(myPOSTreader.Item("GIORNO4"))
            GIORNO4ATTIVO = campodb(myPOSTreader.Item("GIORNO4ATTIVO"))
            GIORNO5 = campodb(myPOSTreader.Item("GIORNO5"))
            GIORNO5ATTIVO = campodb(myPOSTreader.Item("GIORNO5ATTIVO"))
            Regole = campodb(myPOSTreader.Item("Regole"))
            Emissione = campodb(myPOSTreader.Item("Emissione"))
            Banca = campodb(myPOSTreader.Item("Banca"))
            CAB = campodb(myPOSTreader.Item("CAB"))
            ABI = campodb(myPOSTreader.Item("ABI"))
            VerificaImpegnativa = campodb(myPOSTreader.Item("VerificaImpegnativa"))
            DescrizioneRigaInFattura = campodb(myPOSTreader.Item("DescrizioneRigaInFattura"))
            ExtraFissiRetta = campodb(myPOSTreader.Item("ExtraFissiRetta"))
            Mese1 = campodb(myPOSTreader.Item("Mese1"))
            Mese2 = campodb(myPOSTreader.Item("Mese2"))
            Mese3 = campodb(myPOSTreader.Item("Mese3"))
            Mese4 = campodb(myPOSTreader.Item("Mese4"))
            Mese5 = campodb(myPOSTreader.Item("Mese5"))
            Convenzione = campodb(myPOSTreader.Item("Convenzione"))
            EmissioneOP = Val(campodb(myPOSTreader.Item("EmissioneOP")))
            Villa = campodb(myPOSTreader.Item("Villa"))
            ImportoMensile = Val(campodb(myPOSTreader.Item("ImportoMensile")))
            EPersonam = Val(campodb(myPOSTreader.Item("EPersonam")))
            EPersonamN = Val(campodb(myPOSTreader.Item("EPersonamN")))

            CausaleTempoPieno = campodb(myPOSTreader.Item("CausaleTempoPieno"))
            CausaleTempoPienoAss = campodb(myPOSTreader.Item("CausaleTempoPienoAss"))
            CausaleTempoPienoAss1 = campodb(myPOSTreader.Item("CausaleTempoPienoAss1"))
            CausaleTempoPienoAss2 = campodb(myPOSTreader.Item("CausaleTempoPienoAss2"))
            CausalePartTime = campodb(myPOSTreader.Item("CausalePartTime"))
            CausalePartTimeAss = campodb(myPOSTreader.Item("CausalePartTimeAss"))
            CausalePartTimeAss1 = campodb(myPOSTreader.Item("CausalePartTimeAss1"))
            CausalePartTimeAss2 = campodb(myPOSTreader.Item("CausalePartTimeAss2"))
            CausaleChiuso = campodb(myPOSTreader.Item("CausaleChiuso"))
            NonCalcolare = Val(campodb(myPOSTreader.Item("NonCalcolare")))

            CODREGIONE = campodb(myPOSTreader.Item("CODREGIONE"))
            CODASL = campodb(myPOSTreader.Item("CODASL"))
            CODSSA = campodb(myPOSTreader.Item("CODSSA"))
            CampoPerEsatto = campodb(myPOSTreader.Item("CampoPerEsatto"))

            IncrementaNumeroCartella = campodbn(myPOSTreader.Item("IncrementaNumeroCartella"))

            MeseFatturazione = campodbn(myPOSTreader.Item("MeseFatturazione"))
            AnnoFatturazione = campodbn(myPOSTreader.Item("AnnoFatturazione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where  DESCRIZIONE =?")
        cmd.Parameters.AddWithValue("@Descrizione", DESCRIZIONE)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            TIPOCENTROSERVIZIO = campodb(myPOSTreader.Item("TIPOCENTROSERVIZIO"))
            MASTRO = myPOSTreader.Item("MASTRO")
            CONTO = myPOSTreader.Item("CONTO")
            SETTIMANA = campodb(myPOSTreader.Item("SETTIMANA"))
            GIORNO0101 = campodb(myPOSTreader.Item("GIORNO0101"))
            GIORNO0601 = campodb(myPOSTreader.Item("GIORNO0601"))
            GIORNO1903 = campodb(myPOSTreader.Item("GIORNO1903"))
            GIORNO2504 = campodb(myPOSTreader.Item("GIORNO2504"))
            GIORNO0105 = campodb(myPOSTreader.Item("GIORNO0105"))
            GIORNO0206 = campodb(myPOSTreader.Item("GIORNO0206"))
            GIORNO2906 = campodb(myPOSTreader.Item("GIORNO2906"))
            GIORNO1508 = campodb(myPOSTreader.Item("GIORNO1508"))
            GIORNO0111 = campodb(myPOSTreader.Item("GIORNO0111"))
            GIORNO0411 = campodb(myPOSTreader.Item("GIORNO0411"))
            GIORNO0812 = campodb(myPOSTreader.Item("GIORNO0812"))
            GIORNO2512 = campodb(myPOSTreader.Item("GIORNO2512"))
            GIORNO2612 = campodb(myPOSTreader.Item("GIORNO2612"))
            GIORNO1 = campodb(myPOSTreader.Item("GIORNO1"))
            GIORNO1ATTIVO = campodb(myPOSTreader.Item("GIORNO1ATTIVO"))
            GIORNO2 = campodb(myPOSTreader.Item("GIORNO2"))
            GIORNO2ATTIVO = campodb(myPOSTreader.Item("GIORNO2ATTIVO"))
            GIORNO3 = campodb(myPOSTreader.Item("GIORNO3"))
            GIORNO3ATTIVO = campodb(myPOSTreader.Item("GIORNO3ATTIVO"))
            GIORNO4 = campodb(myPOSTreader.Item("GIORNO4"))
            GIORNO4ATTIVO = campodb(myPOSTreader.Item("GIORNO4ATTIVO"))
            GIORNO5 = campodb(myPOSTreader.Item("GIORNO5"))
            GIORNO5ATTIVO = campodb(myPOSTreader.Item("GIORNO5ATTIVO"))
            Regole = campodb(myPOSTreader.Item("Regole"))
            Emissione = campodb(myPOSTreader.Item("Emissione"))
            Banca = campodb(myPOSTreader.Item("Banca"))
            CAB = campodb(myPOSTreader.Item("CAB"))
            ABI = campodb(myPOSTreader.Item("ABI"))
            VerificaImpegnativa = campodb(myPOSTreader.Item("VerificaImpegnativa"))
            DescrizioneRigaInFattura = campodb(myPOSTreader.Item("DescrizioneRigaInFattura"))
            ExtraFissiRetta = campodb(myPOSTreader.Item("ExtraFissiRetta"))
            Mese1 = campodb(myPOSTreader.Item("Mese1"))
            Mese2 = campodb(myPOSTreader.Item("Mese2"))
            Mese3 = campodb(myPOSTreader.Item("Mese3"))
            Mese4 = campodb(myPOSTreader.Item("Mese4"))
            Mese5 = campodb(myPOSTreader.Item("Mese5"))
            Convenzione = campodb(myPOSTreader.Item("Convenzione"))
            EmissioneOP = Val(campodb(myPOSTreader.Item("EmissioneOP")))
            Villa = campodb(myPOSTreader.Item("Villa"))
            ImportoMensile = Val(campodb(myPOSTreader.Item("ImportoMensile")))
            EPersonam = Val(campodb(myPOSTreader.Item("EPersonam")))
            EPersonamN = Val(campodb(myPOSTreader.Item("EPersonamN")))

            CausaleTempoPieno = campodb(myPOSTreader.Item("CausaleTempoPieno"))
            CausaleTempoPienoAss = campodb(myPOSTreader.Item("CausaleTempoPienoAss"))
            CausaleTempoPienoAss1 = campodb(myPOSTreader.Item("CausaleTempoPienoAss1"))
            CausaleTempoPienoAss2 = campodb(myPOSTreader.Item("CausaleTempoPienoAss2"))
            CausalePartTime = campodb(myPOSTreader.Item("CausalePartTime"))
            CausalePartTimeAss = campodb(myPOSTreader.Item("CausalePartTimeAss"))
            CausalePartTimeAss1 = campodb(myPOSTreader.Item("CausalePartTimeAss1"))
            CausalePartTimeAss2 = campodb(myPOSTreader.Item("CausalePartTimeAss2"))
            CausaleChiuso = campodb(myPOSTreader.Item("CausaleChiuso"))
            NonCalcolare = Val(campodb(myPOSTreader.Item("NonCalcolare")))

            CODREGIONE = campodb(myPOSTreader.Item("CODREGIONE"))
            CODASL = campodb(myPOSTreader.Item("CODASL"))
            CODSSA = campodb(myPOSTreader.Item("CODSSA"))

            CampoPerEsatto = campodb(myPOSTreader.Item("CampoPerEsatto"))

            IncrementaNumeroCartella = campodbn(myPOSTreader.Item("IncrementaNumeroCartella"))

            MeseFatturazione = campodbn(myPOSTreader.Item("MeseFatturazione"))
            AnnoFatturazione = campodbn(myPOSTreader.Item("AnnoFatturazione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, Optional ByVal Tipo As String = "")
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Tipo = "" Then
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Order by Descrizione")
        Else
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where [TIPOCENTROSERVIZIO] = '" & Tipo & "' Order by Descrizione")
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CENTROSERVIZIO")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub UpDateDropBoxStruttura(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal Struttura As String, Optional ByVal Tipo As String = "")
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Tipo = "" Then
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where Villa = ? Order by Descrizione")
        Else
            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO Where Villa = ? And [TIPOCENTROSERVIZIO] = '" & Tipo & "' Order by Descrizione")
        End If
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Villa", Struttura)
        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CENTROSERVIZIO")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLACENTROSERVIZIO  ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CENTROSERVIZIO", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("CENTROSERVIZIO")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub ScriviCentroServizio(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from TABELLACENTROSERVIZIO Where  CENTROSERVIZIO = '" & CENTROSERVIZIO & "'")
        cmd1.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TABELLACENTROSERVIZIO  SET " & _
                    "DESCRIZIONE= ?," & _
                    "TIPOCENTROSERVIZIO= ?," & _
                    "MASTRO= ?," & _
                    "CONTO= ?," & _
                    "SETTIMANA= ?," & _
                    "GIORNO0101= ?," & _
                    "GIORNO0601= ?," & _
                    "GIORNO1903= ?," & _
                    "GIORNO2504= ?," & _
                    "GIORNO0105= ?," & _
                    "GIORNO0206= ?," & _
                    "GIORNO2906= ?," & _
                    "GIORNO1508= ?," & _
                    "GIORNO0111= ?," & _
                    "GIORNO0411= ?," & _
                    "GIORNO0812= ?," & _
                    "GIORNO2512= ?," & _
                    "GIORNO2612= ?," & _
                    "GIORNO1= ?," & _
                    "GIORNO1ATTIVO= ?," & _
                    "GIORNO2= ?," & _
                    "GIORNO2ATTIVO= ?," & _
                    "GIORNO3= ?," & _
                    "GIORNO3ATTIVO= ?," & _
                    "GIORNO4= ?," & _
                    "GIORNO4ATTIVO= ?," & _
                    "GIORNO5= ?," & _
                    "GIORNO5ATTIVO= ?," & _
                    "Regole= ?," & _
                    "Emissione= ?," & _
                    "Banca= ?," & _
                    "CAB= ?," & _
                    "ABI= ?," & _
                    "VerificaImpegnativa= ?," & _
                    "DescrizioneRigaInFattura= ?," & _
                    "ExtraFissiRetta= ?," & _
                    "Mese1= ?," & _
                    "Mese2= ?," & _
                    "Mese3= ?," & _
                    "Mese4= ?," & _
                    "Mese5= ?," & _
                    "Convenzione= ?, " & _
                    "Villa= ?, " & _
                    "Utente= ?, " & _
                    "DataAggiornamento= ?, " & _
                    "ImportoMensile = ?, " & _
                    "EPersonam = ?," & _
                    "EPersonamN = ?," & _
                    "CausaleTempoPieno = ?," & _
                    "CausaleTempoPienoAss = ?," & _
                    "CausaleTempoPienoAss1 = ?," & _
                    "CausaleTempoPienoAss2 = ?," & _
                    "CausalePartTime = ?," & _
                    "CausalePartTimeAss = ?, " & _
                    "CausalePartTimeAss1 = ?, " & _
                    "CausalePartTimeAss2 = ?, " & _
                    "CausaleChiuso = ?, " & _
                    "NonCalcolare = ?, " & _
                    "CODREGIONE = ?, " & _
                    "CODASL = ?, " & _
                    "CODSSA = ?,  " & _
                    "CampoPerEsatto = ?, " & _
                    "EmissioneOP = ?, " & _
                    "IncrementaNumeroCartella =?, " & _
                    "AnnoFatturazione =?, " & _
                    "MeseFatturazione =? " & _
                    " WHERE CENTROSERVIZIO = ? "
        Else
            MySql = "INSERT INTO TABELLACENTROSERVIZIO " & _
                    "( " & _
                    "DESCRIZIONE, " & _
                    "TIPOCENTROSERVIZIO, " & _
                    "MASTRO, " & _
                    "CONTO, " & _
                    "SETTIMANA, " & _
                    "GIORNO0101, " & _
                    "GIORNO0601, " & _
                    "GIORNO1903, " & _
                    "GIORNO2504, " & _
                    "GIORNO0105, " & _
                    "GIORNO0206, " & _
                    "GIORNO2906, " & _
                    "GIORNO1508, " & _
                    "GIORNO0111, " & _
                    "GIORNO0411, " & _
                    "GIORNO0812, " & _
                    "GIORNO2512, " & _
                    "GIORNO2612, " & _
                    "GIORNO1, " & _
                    "GIORNO1ATTIVO, " & _
                    "GIORNO2, " & _
                    "GIORNO2ATTIVO, " & _
                    "GIORNO3, " & _
                    "GIORNO3ATTIVO, " & _
                    "GIORNO4, " & _
                    "GIORNO4ATTIVO, " & _
                    "GIORNO5, " & _
                    "GIORNO5ATTIVO, " & _
                    "Regole, " & _
                    "Emissione, " & _
                    "Banca, " & _
                    "CAB, " & _
                    "ABI, " & _
                    "VerificaImpegnativa, " & _
                    "DescrizioneRigaInFattura, " & _
                    "ExtraFissiRetta, " & _
                    "Mese1, " & _
                    "Mese2, " & _
                    "Mese3, " & _
                    "Mese4, " & _
                    "Mese5, " & _
                    "Convenzione, " & _
                    "Villa, " & _
                    "Utente, " & _
                    "DataAggiornamento,ImportoMensile,EPersonam,EPersonamN," & _
                    "CausaleTempoPieno," & _
                    "CausaleTempoPienoAss," & _
                    "CausaleTempoPienoAss1," & _
                    "CausaleTempoPienoAss2," & _
                    "CausalePartTime," & _
                    "CausalePartTimeAss, " & _
                    "CausalePartTimeAss1, " & _
                    "CausalePartTimeAss2, " & _
                    "CausaleChiuso, " & _
                    "NonCalcolare ," & _
                    "CODREGIONE, " & _
                    "CODASL, " & _
                    "CODSSA, " & _
                    "CampoPerEsatto, " & _
                    "EmissioneOP, " & _
                    "IncrementaNumeroCartella," & _
                    "AnnoFatturazione," & _
                    "MeseFatturazione," & _
                    "CENTROSERVIZIO) VALUES (" & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?) "

        End If
        myPOSTreader.Close()
        Dim cmd As New OleDbCommand()

        cmd.Connection = cn
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@DESCRIZIONE", DESCRIZIONE)
        cmd.Parameters.AddWithValue("@TIPOCENTROSERVIZIO", TIPOCENTROSERVIZIO)
        cmd.Parameters.AddWithValue("@MASTRO", MASTRO)
        cmd.Parameters.AddWithValue("@CONTO", CONTO)
        cmd.Parameters.AddWithValue("@SETTIMANA", SETTIMANA)
        cmd.Parameters.AddWithValue("@GIORNO0101", GIORNO0101)
        cmd.Parameters.AddWithValue("@GIORNO0601", GIORNO0601)
        cmd.Parameters.AddWithValue("@GIORNO1903", GIORNO1903)
        cmd.Parameters.AddWithValue("@GIORNO2504", GIORNO2504)
        cmd.Parameters.AddWithValue("@GIORNO0105", GIORNO0105)
        cmd.Parameters.AddWithValue("@GIORNO0206", GIORNO0206)
        cmd.Parameters.AddWithValue("@GIORNO2906", GIORNO2906)
        cmd.Parameters.AddWithValue("@GIORNO1508", GIORNO1508)
        cmd.Parameters.AddWithValue("@GIORNO0111", GIORNO0111)
        cmd.Parameters.AddWithValue("@GIORNO0411", GIORNO0411)
        cmd.Parameters.AddWithValue("@GIORNO0812", GIORNO0812)
        cmd.Parameters.AddWithValue("@GIORNO2512", GIORNO2512)
        cmd.Parameters.AddWithValue("@GIORNO2612", GIORNO2612)
        cmd.Parameters.AddWithValue("@GIORNO1", GIORNO1)
        cmd.Parameters.AddWithValue("@GIORNO1ATTIVO", GIORNO1ATTIVO)
        cmd.Parameters.AddWithValue("@GIORNO2", GIORNO2)
        cmd.Parameters.AddWithValue("@GIORNO2ATTIVO", GIORNO2ATTIVO)
        cmd.Parameters.AddWithValue("@GIORNO3", GIORNO3)
        cmd.Parameters.AddWithValue("@GIORNO3ATTIVO", GIORNO3ATTIVO)
        cmd.Parameters.AddWithValue("@GIORNO4", GIORNO4)
        cmd.Parameters.AddWithValue("@GIORNO4ATTIVO", GIORNO4ATTIVO)
        cmd.Parameters.AddWithValue("@GIORNO5", GIORNO5)
        cmd.Parameters.AddWithValue("@GIORNO5ATTIVO", GIORNO5ATTIVO)
        cmd.Parameters.AddWithValue("@Regole", Regole)
        cmd.Parameters.AddWithValue("@Emissione", Emissione)
        cmd.Parameters.AddWithValue("@Banca", Banca)
        cmd.Parameters.AddWithValue("@CAB", CAB)
        cmd.Parameters.AddWithValue("@ABI", ABI)
        cmd.Parameters.AddWithValue("@VerificaImpegnativa", VerificaImpegnativa)
        cmd.Parameters.AddWithValue("@DescrizioneRigaInFattura", DescrizioneRigaInFattura)
        cmd.Parameters.AddWithValue("@ExtraFissiRetta", ExtraFissiRetta)
        cmd.Parameters.AddWithValue("@Mese1", Mese1)
        cmd.Parameters.AddWithValue("@Mese2", Mese2)
        cmd.Parameters.AddWithValue("@Mese3", Mese3)
        cmd.Parameters.AddWithValue("@Mese4", Mese4)
        cmd.Parameters.AddWithValue("@Mese5", Mese5)
        cmd.Parameters.AddWithValue("@Convenzione", Convenzione)
        cmd.Parameters.AddWithValue("@Villa", Villa)
        cmd.Parameters.AddWithValue("@Utente", Utente)
        cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd.Parameters.AddWithValue("@ImportoMensile", ImportoMensile)
        cmd.Parameters.AddWithValue("@EPersonam", EPersonam)
        cmd.Parameters.AddWithValue("@EPersonamN", EPersonamN)

        cmd.Parameters.AddWithValue("@CausaleTempoPieno", CausaleTempoPieno)
        cmd.Parameters.AddWithValue("@CausaleTempoPienoAss", CausaleTempoPienoAss)
        cmd.Parameters.AddWithValue("@CausaleTempoPienoAss1", CausaleTempoPienoAss1)
        cmd.Parameters.AddWithValue("@CausaleTempoPienoAss2", CausaleTempoPienoAss2)
        cmd.Parameters.AddWithValue("@CausalePartTime", CausalePartTime)
        cmd.Parameters.AddWithValue("@CausalePartTimeAss", CausalePartTimeAss)
        cmd.Parameters.AddWithValue("@CausalePartTimeAss1", CausalePartTimeAss1)
        cmd.Parameters.AddWithValue("@CausalePartTimeAss2", CausalePartTimeAss2)
        cmd.Parameters.AddWithValue("@CausaleChiuso", CausaleChiuso)

        cmd.Parameters.AddWithValue("@NonCalcolare", NonCalcolare)

        cmd.Parameters.AddWithValue("@CODREGIONE", CODREGIONE)
        cmd.Parameters.AddWithValue("@CODASL", CODASL)
        cmd.Parameters.AddWithValue("@CODSSA", CODSSA)
        cmd.Parameters.AddWithValue("@CampoPerEsatto", CampoPerEsatto)
        cmd.Parameters.AddWithValue("@EmissioneOP", EmissioneOP)
        cmd.Parameters.AddWithValue("@IncrementaNumeroCartella", IncrementaNumeroCartella)
        cmd.Parameters.AddWithValue("@AnnoFatturazione", AnnoFatturazione)

        cmd.Parameters.AddWithValue("@MeseFatturazione", MeseFatturazione)

        cmd.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub
End Class
