﻿Imports Microsoft.VisualBasic

Public Class Cls_CampiExport
    Public Tipodoc As String
    Public Numdoc As String
    Public seziva As String
    Public datadoc As String
    Public totDoc As String
    Public codiva1 As String
    Public codiva2 As String
    Public codiva3 As String
    Public codiva4 As String
    Public codiva5 As String
    Public codiva6 As String
    Public codiva7 As String
    Public codiva8 As String
    Public codiva9 As String
    Public codiva10 As String
    Public imponib1 As String
    Public imponib2 As String
    Public imponib3 As String
    Public imponib4 As String
    Public imponib5 As String
    Public imponib6 As String
    Public imponib7 As String
    Public imponib8 As String
    Public imponib9 As String
    Public imponib10 As String
    Public iva1 As String
    Public iva2 As String
    Public iva3 As String
    Public iva4 As String
    Public iva5 As String
    Public iva6 As String
    Public iva7 As String
    Public iva8 As String
    Public iva9 As String
    Public iva10 As String
    Public contoric1 As String
    Public contoric2 As String
    Public contoric3 As String
    Public contoric4 As String
    Public contoric5 As String
    Public contoric6 As String
    Public contoric7 As String
    Public contoric8 As String
    Public contoric9 As String
    Public contoric10 As String
    Public ricavo1 As String
    Public ricavo2 As String
    Public ricavo3 As String
    Public ricavo4 As String
    Public ricavo5 As String
    Public ricavo6 As String
    Public ricavo7 As String
    Public ricavo8 As String
    Public ricavo9 As String
    Public ricavo10 As String
    Public Scadpag As String
    Public Modpag As String
    Public datascad1 As String
    Public datascad2 As String
    Public datascad3 As String
    Public datascad4 As String
    Public codcliente As String
    Public denomin1 As String
    Public denomin2 As String
    Public vian As String
    Public cap As String
    Public citta As String
    Public prov As String
    Public piva As String
    Public codfisc As String
    Public abi As String
    Public cab As String
    Public TipoConto1 As String
    Public TipoConto2 As String
    Public TipoConto3 As String
    Public TipoConto4 As String
    Public TipoConto5 As String
    Public TipoConto6 As String
    Public TipoConto7 As String
    Public TipoConto8 As String
    Public TipoConto9 As String
    Public TipoConto10 As String
    Public ContoCredito As String
    Public CassaBanca As String
    Public Datanascita As String
    Public Cittanascita As String
    Public Provnascita As String
    Public Capnascita As String
    Public Protocolloiva As String
    Public Causale As String
    Public Dataincasso As String
    Public Descr1 As String
    Public Descr2 As String
    Public Causaledoc As String
    Public Descrizionedoc As String
    Public Dataregistr As String
    Public NumDocpartic As String
    Public DataCompetIniziale As String
    Public DataCompetFinale As String
    Public Contoanalit1 As String
    Public Contoanalit2 As String
    Public Contoanalit3 As String
    Public Contoanalit4 As String
    Public Contoanalit5 As String
    Public Contoanalit6 As String
    Public Contoanalit7 As String
    Public Contoanalit8 As String
    Public Contoanalit9 As String
    Public Contoanalit10 As String
    Public Centrocosto1 As String
    Public Centrocosto2 As String
    Public Centrocosto3 As String
    Public Centrocosto4 As String
    Public Centrocosto5 As String
    Public Centrocosto6 As String
    Public Centrocosto7 As String
    Public Centrocosto8 As String
    Public Centrocosto9 As String
    Public Centrocosto10 As String
    Public ImpAnalit1 As String
    Public ImpAnalit2 As String
    Public ImpAnalit3 As String
    Public ImpAnalit4 As String
    Public ImpAnalit5 As String
    Public ImpAnalit6 As String
    Public ImpAnalit7 As String
    Public ImpAnalit8 As String
    Public ImpAnalit9 As String
    Public ImpAnalit10 As String
    Public AnnoIncasso As String
    Public IDIncasso As String
    Public FlagOpposiz As String
    Public Codregione As String
    Public CodASL As String
    Public CodStruttura As String
    Public TipoSpesa As String
    Public RifNumero As String    
    Public RifData As String



    Public Function ExportStringa() As String
        ExportStringa = Tipodoc & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(Numdoc, 20) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(seziva, 4) & "#"
        ExportStringa = ExportStringa & datadoc & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(totDoc, 20) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva1, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva2, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva3, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva4, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva5, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva6, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva7, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva8, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva9, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codiva10, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib1, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib2, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib3, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib4, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib5, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib6, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib7, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib8, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib9, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(imponib10, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva1, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva2, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva3, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva4, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva5, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva6, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva7, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva8, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva9, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(iva10, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric1, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric2, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric3, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric4, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric5, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric6, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric7, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric8, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric9, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(contoric10, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo1, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo2, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo3, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo4, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo5, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo6, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo7, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo8, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo9, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ricavo10, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(Scadpag, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(Modpag, 8) & "#"
        ExportStringa = ExportStringa & datascad1 & "#"
        ExportStringa = ExportStringa & datascad2 & "#"
        ExportStringa = ExportStringa & datascad3 & "#"
        ExportStringa = ExportStringa & datascad4 & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(codcliente, 8) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(denomin1, 35) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(denomin2, 35) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(vian, 32) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(cap, 5) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(citta, 23) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(prov, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(piva, 11) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(codfisc, 16) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(abi, 5) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(cab, 5) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto1, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto2, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto3, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto4, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto5, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto6, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto7, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto8, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto9, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoConto10, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ContoCredito, 9) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(CassaBanca, 9) & "#"
        ExportStringa = ExportStringa & Datanascita & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Cittanascita, 40) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Provnascita, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Capnascita, 5) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Protocolloiva, 20) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(Causale, 5) & "#"
        ExportStringa = ExportStringa & Dataincasso & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Descr1, 50) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Descr2, 50) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Causaledoc, 2) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Descrizionedoc, 50) & "#"
        ExportStringa = ExportStringa & Dataregistr & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(NumDocpartic, 20) & "#"
        ExportStringa = ExportStringa & DataCompetIniziale & "#"
        ExportStringa = ExportStringa & DataCompetFinale & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit1, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit2, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit3, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit4, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit5, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit6, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit7, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit8, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit9, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Contoanalit10, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto1, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto2, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto3, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto4, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto5, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto6, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto7, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto8, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto9, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Centrocosto10, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit1, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit2, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit3, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit4, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit5, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit6, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit7, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit8, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit9, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(ImpAnalit10, 12) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(AnnoIncasso, 5) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaNumero(IDIncasso, 20) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(FlagOpposiz, 1) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(Codregione, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(CodASL, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(CodStruttura, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezza(TipoSpesa, 10) & "#"
        ExportStringa = ExportStringa & AdattaLunghezzaTestoComeNumero(RifNumero, 20) & "#"
        ExportStringa = ExportStringa & RifData & "#"


    End Function

    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function


    Function AdattaLunghezzaTestoComeNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezzaTestoComeNumero = Space(Lunghezza - Len(Testo)) & Testo

        If Len(AdattaLunghezzaTestoComeNumero) > Lunghezza Then
            AdattaLunghezzaTestoComeNumero = Mid(AdattaLunghezzaTestoComeNumero, 1, Lunghezza)
        End If

    End Function

End Class
