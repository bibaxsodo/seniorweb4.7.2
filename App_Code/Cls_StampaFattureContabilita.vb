﻿Imports Microsoft.VisualBasic
Imports SeniorWebApplication
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_StampaFattureContabilita
    Public DC_OSPITE As String
    Public DC_TABELLE As String
    Public DC_GENERALE As String

    Public Sub CreaStampa(ByVal Txt_DataInizioText As Date, ByVal Txt_DataFineText As Date, ByVal Txt_NumeroInizioText As Long, ByVal Txt_NumeroFineText As Long, ByVal RegistroIVA As Long, ByVal Data As Object, ByVal TestoCommento As String)
        Dim Rs_PdC As New ADODB.Recordset
        Dim Rs_Clienti As New ADODB.Recordset
        Dim MyRs As New ADODB.Recordset
        Dim WImponibile(5) As Double
        Dim WImposta(5) As Double
        Dim WAliquota(5) As String
        Dim StampeDb As New ADODB.Connection
        Dim Oldfattura As Long
        Dim Condizione As String
        Dim CreaRecordStampa As Boolean
        Dim MySql As String
        Dim GeneraleDb As New ADODB.Connection
        Dim OspitiDb As New ADODB.Connection

        Dim Importo As Double
        Dim Imponibile As Double

        Dim MyRecordStampaT As System.Data.DataRow

        Dim MyRecordStampaR As System.Data.DataRow
        Dim FnVB6 As New Cls_FunzioniVB6


        Dim WTotaleMerce As Double
        Dim WImportoSconto As Double
        Dim Appoggio As String


        Dim Stampa As New StampeGenerale

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = Data

        System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 1


        GeneraleDb.Open(SessioneTP("DC_GENERALE"))
        OspitiDb.Open(SessioneTP("DC_OSPITE"))


        DC_OSPITE = SessioneTP("DC_OSPITE")
        DC_TABELLE = SessioneTP("DC_TABELLE")
        DC_GENERALE = SessioneTP("DC_GENERALE")

        FnVB6.StringaGenerale = SessioneTP("DC_GENERALE")
        FnVB6.StringaOspiti = SessioneTP("DC_OSPITE")
        FnVB6.StringaTabelle = SessioneTP("DC_TABELLE")
        FnVB6.ApriDB()



        Condizione = ""

        CreaRecordStampa = False

        MySql = "SELECT MovimentiContabiliRiga.*, MovimentiContabiliRiga.Descrizione AS DescrizioneRiga, MovimentiContabiliTesta.*, MovimentiContabiliTesta.Descrizione AS DescrizioneTesta " &
              " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " &
              " WHERE RegistroIVA = " & RegistroIVA &
              " AND DataRegistrazione >= {ts '" & Format(Txt_DataInizioText, "yyyy-MM-dd") & " 00:00:00'}" &
              " AND DataRegistrazione <= {ts '" & Format(Txt_DataFineText, "yyyy-MM-dd") & " 00:00:00'}" &
              " AND NumeroProtocollo >= " & Txt_NumeroInizioText &
              " AND NumeroProtocollo <= " & Txt_NumeroFineText &
              Condizione &
              " ORDER BY NumeroProtocollo, RigaDaCausale, RigaRegistrazione"

        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            MyRs.Close()
            SessioneTP("CampoErrori") = "Nessun documento estratto"
            SessioneTP("CampoProgressBar") = 999

            System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = "Nessun documento estratto"
            System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
            Exit Sub
        End If



        Oldfattura = MoveFromDbWC(MyRs, "NumeroProtocollo")
        MySql = "SELECT * FROM PianoConti " &
              " WHERE Mastro = " & MoveFromDbWC(MyRs, "MastroPartita") &
              " AND Conto = " & MoveFromDbWC(MyRs, "ContoPartita") &
              " AND Sottoconto = " & MoveFromDbWC(MyRs, "SottocontoPartita")
        Rs_PdC.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Rs_PdC.EOF Then
            Rs_PdC.Close()
            SessioneTP("CampoErrori") = SessioneTP("CampoErrori") & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sul piano dei conti " & vbNewLine
            System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sul piano dei conti " & vbNewLine
            MyRs.Close()
            SessioneTP("CampoProgressBar") = 999
            System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
            Exit Sub
        End If
        If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "O" Then

            MySql = "SELECT * FROM AnagraficaComune " &
                  " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita")) &
                  " And Tipologia = 'O'"
        Else

            If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "P" Then
                MySql = "SELECT * FROM AnagraficaComune " &
                " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita")).ToString &
                " AND CodiceParente = " & FnVB6.CampoOspiteParenteMCS("CodiceParente", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita")).ToString
            Else
                MySql = "SELECT * FROM AnagraficaComune " &
                      " WHERE MastroCliente = " & MoveFromDbWC(MyRs, "MastroPartita") &
                      " AND ContoCliente = " & MoveFromDbWC(MyRs, "ContoPartita") &
                      " AND SottocontoCliente = " & MoveFromDbWC(MyRs, "SottocontoPartita")
            End If
        End If
        Rs_PdC.Close()
        Rs_Clienti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Rs_Clienti.EOF Then
            Rs_Clienti.Close()
            SessioneTP("CampoErrori") = SessioneTP("CampoErrori") & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sull'anagrafico clienti " & vbNewLine
            SessioneTP("CampoProgressBar") = 999

            System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sull'anagrafico clienti " & vbNewLine
            System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
            MyRs.Close()
            Exit Sub
        End If
        For I = 1 To 5
            WImponibile(I) = 0
            WAliquota(I) = ""
            WImposta(I) = 0
        Next I
        MySql = "Select * From FatturaTesta"
        MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow

        MyRecordStampaT.Item("TipoDocumento") = TipoDocumento(MoveFromDbWC(MyRs, "CausaleContabile"))
        MyRecordStampaT.Item("RegistroIVA") = MoveFromDbWC(MyRs, "RegistroIVA")

        Do Until MyRs.EOF
            If Oldfattura <> MoveFromDbWC(MyRs, "NumeroProtocollo") Then
                SessioneTP("CampoProgressBar") = SessioneTP("CampoProgressBar") + 1
                MyRecordStampaT.Item("Imponibile1") = WImponibile(1)
                If WAliquota(1) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA1") = WAliquota(1) & " - " & FnVB6.CampoIVA(WAliquota(1), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta1") = WImposta(1)
                MyRecordStampaT.Item("Imponibile2") = WImponibile(2)
                If WAliquota(2) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA2") = WAliquota(2) & " - " & FnVB6.CampoIVA(WAliquota(2), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta2") = WImposta(2)
                MyRecordStampaT.Item("Imponibile3") = WImponibile(3)
                If WAliquota(3) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA3") = WAliquota(3) & " - " & FnVB6.CampoIVA(WAliquota(3), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta3") = WImposta(3)
                MyRecordStampaT.Item("Imponibile4") = WImponibile(4)
                If WAliquota(4) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA4") = WAliquota(4) & " - " & FnVB6.CampoIVA(WAliquota(4), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta4") = WImposta(4)
                MyRecordStampaT.Item("Imponibile5") = WImponibile(5)
                If WAliquota(5) <> "" Then
                    MyRecordStampaT.Item("AliquotaIVA5") = WAliquota(5) & " - " & FnVB6.CampoIVA(WAliquota(5), "Descrizione")
                End If
                MyRecordStampaT.Item("Imposta5") = WImposta(5)
                Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)
                MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow


                Oldfattura = MoveFromDbWC(MyRs, "NumeroProtocollo")
                WTotaleMerce = 0
                WImportoSconto = 0

                MyRecordStampaT = Stampa.Tables("FatturaTesta").NewRow
                MyRecordStampaT.Item("TipoDocumento") = TipoDocumento(MoveFromDbWC(MyRs, "CausaleContabile"))
                MyRecordStampaT.Item("RegistroIVA") = MoveFromDbWC(MyRs, "RegistroIVA")

                Rs_Clienti.Close()
                MySql = "SELECT * FROM PianoConti " &
                        " WHERE Mastro = " & MoveFromDbWC(MyRs, "MastroPartita") &
                        " AND Conto = " & MoveFromDbWC(MyRs, "ContoPartita") &
                        " AND Sottoconto = " & MoveFromDbWC(MyRs, "SottocontoPartita")
                Rs_PdC.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If Rs_PdC.EOF Then
                    Rs_PdC.Close()
                    SessioneTP("CampoErrori") = SessioneTP("CampoErrori") & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sul piano dei conti " & vbNewLine
                    MyRs.Close()
                    SessioneTP("CampoProgressBar") = 999
                    Exit Sub
                End If
                If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "O" Then

                    MySql = "SELECT * FROM AnagraficaComune " &
                            " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita")) &
                            " And Tipologia = 'O'"
                Else
                    If MoveFromDbWC(Rs_PdC, "TipoAnagrafica") = "P" Then

                        MySql = "SELECT * FROM AnagraficaComune " &
                                " WHERE CodiceOspite = " & FnVB6.CampoOspiteParenteMCS("CodiceOspite", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita")) &
                                " AND CodiceParente = " & FnVB6.CampoOspiteParenteMCS("CodiceParente", MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita"))

                    Else
                        MySql = "SELECT * FROM AnagraficaComune " &
                                " WHERE MastroCliente = " & MoveFromDbWC(MyRs, "MastroPartita") &
                                " AND ContoCliente = " & MoveFromDbWC(MyRs, "ContoPartita") &
                                " AND SottocontoCliente = " & MoveFromDbWC(MyRs, "SottocontoPartita")
                    End If
                End If
                Rs_PdC.Close()
                Rs_Clienti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If Rs_Clienti.EOF Then
                    Rs_Clienti.Close()
                    SessioneTP("CampoErrori") = SessioneTP("CampoErrori") & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sull'anagrafico clienti " & vbNewLine
                    System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) = System.Web.HttpRuntime.Cache("CampoErrori" + SessioneTP.SessionID) & "Mastro: " & MoveFromDbWC(MyRs, "MastroPartita") & " Conto: " & MoveFromDbWC(MyRs, "ContoPartita") & " Sottoconto: " & MoveFromDbWC(MyRs, "SottocontoPartita") & " non trovato sull'anagrafico clienti " & vbNewLine
                    System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 999
                    MyRs.Close()
                    Exit Sub
                End If

                For I = 1 To 5
                    WImponibile(I) = 0
                    WAliquota(I) = ""
                    WImposta(I) = 0
                Next I
            End If

            If MoveFromDbWC(MyRs, "Tipo") = "CF" Then
                MyRecordStampaT.Item("RagioneSociale") = MoveFromDbWC(Rs_Clienti, "Nome")
                MyRecordStampaT.Item("Indirizzo") = MoveFromDbWC(Rs_Clienti, "RESIDENZAINDIRIZZO1")
                MyRecordStampaT.Item("Cap") = MoveFromDbWC(Rs_Clienti, "RESIDENZACAP1")
                MyRecordStampaT.Item("Localita") = FnVB6.DecodificaComune(MoveFromDbWC(Rs_Clienti, "RESIDENZAPROVINCIA1"), MoveFromDbWC(Rs_Clienti, "RESIDENZACOMUNE1"))
                Dim k As New ClsComune

                k.Provincia = MoveFromDbWC(Rs_Clienti, "RESIDENZAPROVINCIA1")
                k.Comune = MoveFromDbWC(Rs_Clienti, "RESIDENZACOMUNE1")
                k.Leggi(SessioneTP("DC_OSPITE"))


                MyRecordStampaT.Item("Provincia") = k.CodificaProvincia
                MyRecordStampaT.Item("Attenzione") = MoveFromDbWC(Rs_Clienti, "Attenzione")
                MyRecordStampaT.Item("NumeroFattura") = MoveFromDbWC(MyRs, "NumeroProtocollo")
                Dim KRegIVa As New Cls_RegistroIVA

                KRegIVa.Tipo = MoveFromDbWC(MyRs, "RegistroIva")
                KRegIVa.Leggi(SessioneTP("DC_TABELLE"), KRegIVa.Tipo)


                MyRecordStampaT.Item("IndicatoreRegistro") = KRegIVa.IndicatoreRegistro

                MyRecordStampaT.Item("CodiceCIG") = MoveFromDbWC(MyRs, "CodiceCig")
                MyRecordStampaT.Item("BISIDENTIFICATORE") = MoveFromDbWC(MyRs, "BIS")
                MyRecordStampaT.Item("DataFattura") = MoveFromDbWC(MyRs, "DataRegistrazione")
                MyRecordStampaT.Item("DataScadenza") = LeggiScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                MyRecordStampaT.Item("ImportoScadenza") = ImportoScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                If Val(MoveFromDbWC(Rs_Clienti, "PartitaIVA")) > 0 Then
                    MyRecordStampaT.Item("PartitaIVACodiceFiscale") = Format(MoveFromDbWC(Rs_Clienti, "PartitaIVA"), "00000000000")
                Else
                    MyRecordStampaT.Item("PartitaIVACodiceFiscale") = MoveFromDbWC(Rs_Clienti, "CodiceFiscale")
                End If
                MyRecordStampaT.Item("PIVA") = Format(Val(MoveFromDbWC(Rs_Clienti, "PartitaIVA")), "00000000000")
                MyRecordStampaT.Item("CODICEFISCALE") = MoveFromDbWC(Rs_Clienti, "CodiceFiscale")

                Dim kTipPag As New Cls_TipoPagamento
                kTipPag.Codice = MoveFromDbWC(MyRs, "CodicePagamento")
                kTipPag.Leggi(SessioneTP("DC_TABELLE"))
                MyRecordStampaT.Item("ModalitaPagamento") = kTipPag.Descrizione
                MyRecordStampaT.Item("Dichiarazione") = MoveFromDbWC(Rs_Clienti, "Dichiarazione")


                Dim TGem As New Cls_Legami

                MyRecordStampaT.Item("ImportoPagato") = TGem.TotaleLegame(SessioneTP("DC_GENERALE"), MoveFromDbWC(MyRs, "NumeroRegistrazione"))
            End If
            Dim CausaleContabile As New Cls_CausaleContabile
            CausaleContabile.Codice = MoveFromDbWC(MyRs, "CausaleContabile")
            CausaleContabile.Leggi(SessioneTP("DC_TABELLE"), CausaleContabile.Codice)


            If CausaleContabile.Tipo = "R" Then
                If CausaleContabile.TipoDocumento = "NC" Then
                    If MoveFromDbWC(MyRs, "DareAvere") = "D" Then
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile")
                        Importo = MoveFromDbWC(MyRs, "Importo")
                    Else
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile") * -1
                        Importo = MoveFromDbWC(MyRs, "Importo") * -1
                    End If
                Else
                    If MoveFromDbWC(MyRs, "DareAvere") = "A" Then
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile")
                        Importo = MoveFromDbWC(MyRs, "Importo")
                    Else
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile") * -1
                        Importo = MoveFromDbWC(MyRs, "Importo") * -1
                    End If
                End If

                If ((MoveFromDbWC(MyRs, "RigaDaCausale") >= 3 And MoveFromDbWC(MyRs, "RigaDaCausale") <= 6) Or MoveFromDbWC(MyRs, "RigaDaCausale") = 9 Or MoveFromDbWC(MyRs, "RigaDaCausale") = 13) And Trim(MoveFromDbWC(MyRs, "Tipo")) = "" Then
                    MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow

                    MyRecordStampaR.Item("NumeroRiga") = MoveFromDbWC(MyRs, "RigaRegistrazione")
                    MyRecordStampaR.Item("NumeroFattura") = MoveFromDbWC(MyRs, "NumeroProtocollo")
                    MyRecordStampaR.Item("DataFattura") = MoveFromDbWC(MyRs, "DataRegistrazione")
                    MyRecordStampaT.Item("DataScadenza") = LeggiScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                    MyRecordStampaT.Item("ImportoScadenza") = ImportoScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                    MyRecordStampaR.Item("TipoRiga") = MoveFromDbWC(MyRs, "RigaDaCausale")


                    If Not MoveFromDbWC(MyRs, "RigaDaCausale") = 9 Then

                        If Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")) <> "" Then
                            Dim DecConto As New Cls_Pianodeiconti
                            DecConto.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                            DecConto.Conto = MoveFromDbWC(MyRs, "ContoPartita")
                            DecConto.Sottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
                            DecConto.Decodfica(SessioneTP("DC_GENERALE"))
                            Appoggio = Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")) ' & " " & DecConto.Descrizione
                            If Len(Appoggio) < 120 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = Appoggio
                                MyRecordStampaR.Item("DescrizioneRiga1") = ""
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = ""
                                MyRecordStampaR.Item("DescrizioneRiga1") = Appoggio
                            End If
                        Else
                            Dim DecConto As New Cls_Pianodeiconti

                            DecConto.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                            DecConto.Conto = MoveFromDbWC(MyRs, "ContoPartita")
                            DecConto.Sottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
                            DecConto.Decodfica(SessioneTP("DC_GENERALE"))
                            Appoggio = DecConto.Descrizione
                            If Len(Appoggio) < 120 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = Appoggio
                                MyRecordStampaR.Item("DescrizioneRiga1") = ""
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = ""
                                MyRecordStampaR.Item("DescrizioneRiga1") = Appoggio
                            End If
                        End If

                        MyRecordStampaR.Item("Quantita") = MoveFromDbWC(MyRs, "Quantita")
                        If Not IsDBNull(MyRs.Fields("Quantita")) And MoveFromDbWC(MyRs, "Quantita") <> 0 Then
                            MyRecordStampaR.Item("Prezzo") = MoveFromDbWC(MyRs, "Importo") / MoveFromDbWC(MyRs, "Quantita")
                        Else
                            MyRecordStampaR.Item("Prezzo") = 0
                        End If
                    End If
                    MyRecordStampaR.Item("Importo") = Importo
                    MyRecordStampaR.Item("CodiceIva") = MoveFromDbWC(MyRs, "CodiceIva")
                    MyRecordStampaR.Item("RegistroIVA") = MoveFromDbWC(MyRs, "RegistroIVA")

                    Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
                    WTotaleMerce = WTotaleMerce + Importo
                    WImportoSconto = 0
                End If
                If MoveFromDbWC(MyRs, "Tipo") = "IV" Then
                    For I = 1 To 5
                        If WAliquota(I) = MoveFromDbWC(MyRs, "CodiceIva") Then
                            WImponibile(I) = WImponibile(I) + Imponibile
                            WImposta(I) = WImposta(I) + Importo
                            Exit For
                        End If
                        If WAliquota(I) = "" Then
                            WAliquota(I) = MoveFromDbWC(MyRs, "CodiceIva")
                            WImponibile(I) = Imponibile
                            WImposta(I) = Importo
                            Exit For
                        End If
                    Next I
                End If
            Else
                If CausaleContabile.TipoDocumento = "NC" Then
                    If MoveFromDbWC(MyRs, "DareAvere") = "D" Then
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile")
                        Importo = MoveFromDbWC(MyRs, "Importo")
                    Else
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile") * -1
                        Importo = MoveFromDbWC(MyRs, "Importo") * -1
                    End If
                Else
                    If MoveFromDbWC(MyRs, "DareAvere") = "A" Then
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile")
                        Importo = MoveFromDbWC(MyRs, "Importo")
                    Else
                        Imponibile = MoveFromDbWC(MyRs, "Imponibile") * -1
                        Importo = MoveFromDbWC(MyRs, "Importo") * -1
                    End If
                End If
                If MoveFromDbWC(MyRs, "Tipo") = "IV" Then
                    WImportoSconto = 0
                    WTotaleMerce = WTotaleMerce + Imponibile
                    If MoveFromDbWC(MyRs, "Tipo") = "IV" Then
                        For I = 1 To 5
                            If WAliquota(I) = MoveFromDbWC(MyRs, "CodiceIva") Then
                                WImponibile(I) = WImponibile(I) + Imponibile
                                WImposta(I) = WImposta(I) + Importo
                                Exit For
                            End If
                            If WAliquota(I) = "" Then
                                WAliquota(I) = MoveFromDbWC(MyRs, "CodiceIva")
                                WImponibile(I) = Imponibile
                                WImposta(I) = Importo
                                Exit For
                            End If
                        Next I
                    End If
                End If
                If MoveFromDbWC(MyRs, "Tipo") = "" Then
                    If MoveFromDbWC(MyRs, "Importo") >= 0 Then
                        MyRecordStampaR = Stampa.Tables("FatturaRighe").NewRow

                        MyRecordStampaR.Item("NumeroRiga") = MoveFromDbWC(MyRs, "RigaRegistrazione")
                        MyRecordStampaR.Item("NumeroFattura") = MoveFromDbWC(MyRs, "NumeroProtocollo")
                        MyRecordStampaR.Item("DataFattura") = MoveFromDbWC(MyRs, "DataRegistrazione")
                        MyRecordStampaT.Item("DataScadenza") = LeggiScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                        MyRecordStampaT.Item("ImportoScadenza") = ImportoScadenza(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                        MyRecordStampaR.Item("TipoRiga") = MoveFromDbWC(MyRs, "RigaDaCausale")

                        If Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")) <> "" Then
                            Appoggio = Trim(MoveFromDbWC(MyRs, "DescrizioneRiga"))
                            If Len(Appoggio) < 120 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = Appoggio
                                MyRecordStampaR.Item("DescrizioneRiga1") = ""
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = ""
                                MyRecordStampaR.Item("DescrizioneRiga1") = Appoggio
                            End If
                        Else
                            Dim DecConto As New Cls_Pianodeiconti

                            DecConto.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                            DecConto.Conto = MoveFromDbWC(MyRs, "ContoPartita")
                            DecConto.Sottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
                            DecConto.Decodfica(SessioneTP("DC_GENERALE"))
                            Appoggio = DecConto.Descrizione
                            If Len(Appoggio) < 120 Then
                                MyRecordStampaR.Item("DescrizioneRiga") = Appoggio
                                MyRecordStampaR.Item("DescrizioneRiga1") = ""
                            Else
                                MyRecordStampaR.Item("DescrizioneRiga") = ""
                                MyRecordStampaR.Item("DescrizioneRiga1") = Appoggio
                            End If
                        End If


                        MyRecordStampaR.Item("Quantita") = MoveFromDbWC(MyRs, "Quantita")
                        If Not IsDBNull(MoveFromDbWC(MyRs, "Quantita")) And MoveFromDbWC(MyRs, "Quantita") <> 0 Then
                            MyRecordStampaR.Item("Prezzo") = MoveFromDbWC(MyRs, "Importo") / MoveFromDbWC(MyRs, "Quantita")
                        Else
                            MyRecordStampaR.Item("Prezzo") = 0
                        End If
                        MyRecordStampaR.Item("Importo") = Importo
                        MyRecordStampaR.Item("CodiceIva") = MoveFromDbWC(MyRs, "CodiceIva")
                        MyRecordStampaR.Item("RegistroIVA") = MoveFromDbWC(MyRs, "RegistroIVA")
                        Stampa.Tables("FatturaRighe").Rows.Add(MyRecordStampaR)
                    End If
                End If


            End If
            MyRecordStampaT.Item("TotaleMerce") = WTotaleMerce
            MyRecordStampaT.Item("ImportoSconto") = WImportoSconto
            MyRecordStampaT.Item("NettoMerce") = WTotaleMerce - WImportoSconto
            MyRecordStampaT.Item("Descrizione") = MoveFromDbWC(MyRs, "DescrizioneTesta")
            MyRecordStampaT.Item("DescrizioneDown") = TestoCommento
            MyRecordStampaT.Item("OspiteRiferimento") = "" 'Txt_Bis.Text            
            MyRs.MoveNext()
        Loop
        MyRecordStampaT.Item("Imponibile1") = WImponibile(1)
        If WAliquota(1) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA1") = WAliquota(1) & " - " & FnVB6.CampoIVA(WAliquota(1), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta1") = WImposta(1)
        MyRecordStampaT.Item("Imponibile2") = WImponibile(2)
        If WAliquota(2) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA2") = WAliquota(2) & " - " & FnVB6.CampoIVA(WAliquota(2), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta2") = WImposta(2)
        MyRecordStampaT.Item("Imponibile3") = WImponibile(3)
        If WAliquota(3) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA3") = WAliquota(3) & " - " & FnVB6.CampoIVA(WAliquota(3), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta3") = WImposta(3)
        MyRecordStampaT.Item("Imponibile4") = WImponibile(4)
        If WAliquota(4) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA4") = WAliquota(4) & " - " & FnVB6.CampoIVA(WAliquota(4), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta4") = WImposta(4)
        MyRecordStampaT.Item("Imponibile5") = WImponibile(5)
        If WAliquota(5) <> "" Then
            MyRecordStampaT.Item("AliquotaIVA5") = WAliquota(5) & " - " & FnVB6.CampoIVA(WAliquota(5), "Descrizione")
        End If
        MyRecordStampaT.Item("Imposta5") = WImposta(5)



        Stampa.Tables("FatturaTesta").Rows.Add(MyRecordStampaT)

        '  Rs_Clienti.Close
        MyRs.Close()
        SessioneTP("stampa") = Stampa
        SessioneTP("CampoProgressBar") = 101

        System.Web.HttpRuntime.Cache("CampoProgressBar" + SessioneTP.SessionID) = 101
        System.Web.HttpRuntime.Cache("stampa" + SessioneTP.SessionID) = Stampa

        CreaRecordStampa = True
    End Sub




    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Private Function TipoDocumento(ByVal CausaleContabile As String) As String
        Dim WTipo As String
        Dim FnVB6 As New Cls_FunzioniVB6
        FnVB6.StringaOspiti = DC_OSPITE
        FnVB6.StringaTabelle = DC_TABELLE
        FnVB6.StringaGenerale = DC_GENERALE
        FnVB6.ApriDB()

        WTipo = FnVB6.CampoCausaleContabile(CausaleContabile, "TipoDocumento")
        If WTipo = "FA" Or WTipo = "RE" Then
            TipoDocumento = "FATTURA"
        End If
        If WTipo = "NC" Then
            TipoDocumento = "NOTA CREDITO"
        End If
        If WTipo = "ND" Then
            TipoDocumento = "NOTA DEBITO"
        End If
        If WTipo = "CR" Then
            TipoDocumento = "CORRISPETTIVO"
        End If

        FnVB6.ChiudiDB()
    End Function


    Private Function LeggiScadenza(ByVal NumeroRegistrazioneContabile As Long) As Date
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(DC_GENERALE)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Scadenzario where " &
                                   "NumeroRegistrazioneContabile = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            If IsDBNull(VerReader.Item("DataScadenza")) Then
                LeggiScadenza = Nothing
            Else
                LeggiScadenza = VerReader.Item("DataScadenza")
            End If
        End If
        VerReader.Close()
        cn.Close()
    End Function

    Private Function ImportoScadenza(ByVal NumeroRegistrazioneContabile As Long) As Double
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(DC_GENERALE)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select sum(Importo) from Scadenzario where " & _
                                   "NumeroRegistrazioneContabile = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            If IsDBNull(VerReader.Item(0)) Then
                ImportoScadenza = 0
            Else
                ImportoScadenza = VerReader.Item(0)
            End If
        End If
        VerReader.Close()
        cn.Close()
    End Function

End Class
