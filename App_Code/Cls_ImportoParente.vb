Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ImportoParente
    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public CODICEPARENTE As Long
    Public DATA As Date
    Public PERCENTUALE As Double
    Public IMPORTO As Double
    Public TIPOIMPORTO As String
    Public IMPORTO_2 As Double
    Public IMPORTO1 As Double
    Public IMPORTO2 As Double
    Public IMPORTO3 As Double
    Public IMPORTO4 As Double
    Public MensileFisso As Integer
    Public Utente As String
    Public DataAggiornamento As Date
    Public TIPOOPERAZIONE As String


    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOPARENTI where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And CODICEPARENTE = " & CODICEPARENTE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub PrimaDataMese(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CodiceParente As Long, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Integer)
        Dim cn As OleDbConnection
        Dim MovimentiMese As Integer = 0


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdV As New OleDbCommand()
        cmdV.CommandText = ("select count(*) from IMPORTOPARENTI where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And CodiceParente =   " & CodiceParente & " And  Year(Data) = ? And month(Data) = ?")
        cmdV.Connection = cn
        cmdV.Parameters.AddWithValue("@Anno", Anno)
        cmdV.Parameters.AddWithValue("@Mese", Mese)
        Dim RsV As OleDbDataReader = cmdV.ExecuteReader()
        If RsV.Read Then
            MovimentiMese = campodbN(RsV.Item(0))
        End If
        RsV.Close()

        If MovimentiMese >= 1 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And CodiceParente =   " & CodiceParente & " And  Data <= ? Order by Data Desc")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, 1))
            Dim Appoggio As String = ""


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then

                CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
                IMPORTO = campodbN(myPOSTreader.Item("Importo"))
                TIPOIMPORTO = campodb(myPOSTreader.Item("TIPOIMPORTO"))
                DATA = campodb(myPOSTreader.Item("Data"))
                IMPORTO_2 = campodbN(myPOSTreader.Item("Importo_2"))
                IMPORTO1 = campodbN(myPOSTreader.Item("Importo1"))
                IMPORTO2 = campodbN(myPOSTreader.Item("Importo2"))
                IMPORTO3 = campodbN(myPOSTreader.Item("Importo3"))
                IMPORTO4 = campodbN(myPOSTreader.Item("Importo4"))
                MensileFisso = campodbN(myPOSTreader.Item("MensileFisso"))

                TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            Else
                Appoggio = "NonT"
            End If
            myPOSTreader.Close()

            If Appoggio = "NonT" Then
                Dim cmd2 As New OleDbCommand()
                cmd2.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And CodiceParente =   " & CodiceParente & " And  Data > ? Order by Data")                
                cmd2.Connection = cn
                cmd2.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, 1))


                Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                If myPOSTreader2.Read Then
                    CodiceOspite = campodb(myPOSTreader2.Item("CodiceOspite"))
                    CodiceParente = campodb(myPOSTreader2.Item("CodiceParente"))
                    IMPORTO = campodbN(myPOSTreader2.Item("Importo"))
                    TIPOIMPORTO = campodb(myPOSTreader2.Item("TIPOIMPORTO"))
                    DATA = campodb(myPOSTreader2.Item("Data"))
                    IMPORTO_2 = campodbN(myPOSTreader2.Item("Importo_2"))
                    IMPORTO1 = campodbN(myPOSTreader2.Item("Importo1"))
                    IMPORTO2 = campodbN(myPOSTreader2.Item("Importo2"))
                    IMPORTO3 = campodbN(myPOSTreader2.Item("Importo3"))
                    IMPORTO4 = campodbN(myPOSTreader2.Item("Importo4"))
                    MensileFisso = campodbN(myPOSTreader2.Item("MensileFisso"))

                    TIPOOPERAZIONE = campodb(myPOSTreader2.Item("TIPOOPERAZIONE"))
                End If
                myPOSTreader2.Close()
            End If
        End If

        cn.Close()

    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal CodiceParente As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And CodiceParente = " & CodiceParente & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            IMPORTO = campodbN(myPOSTreader.Item("Importo"))
            TIPOIMPORTO = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            DATA = campodb(myPOSTreader.Item("Data"))
            PERCENTUALE = campodb(myPOSTreader.Item("PERCENTUALE"))
            IMPORTO_2 = campodbN(myPOSTreader.Item("IMPORTO_2"))
            IMPORTO1 = campodbN(myPOSTreader.Item("Importo1"))
            IMPORTO2 = campodbN(myPOSTreader.Item("Importo2"))
            IMPORTO3 = campodbN(myPOSTreader.Item("Importo3"))
            IMPORTO4 = campodbN(myPOSTreader.Item("Importo4"))
            MensileFisso = campodbN(myPOSTreader.Item("MensileFisso"))

            TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaDataAData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal CodiceParente As Long, ByVal Data As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And CodiceParente = " & CodiceParente & " And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", DATA)


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            IMPORTO = campodbN(myPOSTreader.Item("Importo"))
            TIPOIMPORTO = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            DATA = campodb(myPOSTreader.Item("Data"))
            PERCENTUALE = campodb(myPOSTreader.Item("PERCENTUALE"))
            IMPORTO_2 = campodbN(myPOSTreader.Item("IMPORTO_2"))
            IMPORTO1 = campodbN(myPOSTreader.Item("Importo1"))
            IMPORTO2 = campodbN(myPOSTreader.Item("Importo2"))
            IMPORTO3 = campodbN(myPOSTreader.Item("Importo3"))
            IMPORTO4 = campodbN(myPOSTreader.Item("Importo4"))
            MensileFisso = campodbN(myPOSTreader.Item("MensileFisso"))

            TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOPARENTI where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And CODICEPARENTE = " & CODICEPARENTE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTOPARENTI where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And CODICEPARENTE = " & CODICEPARENTE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO IMPORTOPARENTI (CentroServizio,CodiceOspite,CODICEPARENTE,Data,IMPORTO,TIPOIMPORTO,IMPORTO_2,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4,PERCENTUALE,MensileFisso,TIPOOPERAZIONE,Utente,DataAggiornamento) VALUES " & _
                                                    "(            ?,           ?,             ?,  ?,     ?,           ?,        ?,       ?,       ?,       ?,       ?,          ?,           ?,            ?,      ?,                ?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                cmdw.Parameters.AddWithValue("@CODICEPARENTE", CODICEPARENTE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@IMPORTO", CDbl(Tabella.Rows(i).Item(1)))
                cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tabella.Rows(i).Item(2))
                cmdw.Parameters.AddWithValue("@IMPORTO_2", campodbN(Tabella.Rows(i).Item(4)))
                cmdw.Parameters.AddWithValue("@IMPORTO1", campodbN(Tabella.Rows(i).Item(5)))
                cmdw.Parameters.AddWithValue("@IMPORTO2", campodbN(Tabella.Rows(i).Item(6)))
                cmdw.Parameters.AddWithValue("@IMPORTO3", campodbN(Tabella.Rows(i).Item(7)))
                cmdw.Parameters.AddWithValue("@IMPORTO4", campodbN(Tabella.Rows(i).Item(8)))
                cmdw.Parameters.AddWithValue("@PERCENTUALE", campodbN(Tabella.Rows(i).Item(3)) / 100)
                cmdw.Parameters.AddWithValue("@MensileFisso", campodbN(Tabella.Rows(i).Item(9)))
                cmdw.Parameters.AddWithValue("@TIPOOPERAZIONE", campodb(Tabella.Rows(i).Item(10)))

                cmdw.Parameters.AddWithValue("@Utente", Utente)
                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                

                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And CODICEPARENTE = " & CODICEPARENTE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE IMPORTOPARENTI SET PERCENTUALE= ?,IMPORTO = ?,TIPOIMPORTO=?, " & _
                    " IMPORTO_2=?,IMPORTO1=?,IMPORTO2=?,IMPORTO3=?,IMPORTO4=?,MensileFisso = ?,TIPOOPERAZIONE=  ?,Utente  = ?,DataAggiornamento = ? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And CODICEPARENTE = " & CODICEPARENTE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@PERCENTUALE", PERCENTUALE)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", TIPOIMPORTO)
            cmdw.Parameters.AddWithValue("@IMPORTO_2", IMPORTO_2)
            cmdw.Parameters.AddWithValue("@IMPORTO1", IMPORTO1)
            cmdw.Parameters.AddWithValue("@IMPORTO3", IMPORTO2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", IMPORTO3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", IMPORTO4)
            cmdw.Parameters.AddWithValue("@MensileFisso", MensileFisso)
            cmdw.Parameters.AddWithValue("@TIPOOPERAZIONE", TIPOOPERAZIONE)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Data", DATA)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO IMPORTOPARENTI (CentroServizio,CodiceOspite,CODICEPARENTE,Data,PERCENTUALE,IMPORTO,TIPOIMPORTO,IMPORTO_2,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4,MensileFisso,TIPOOPERAZIONE,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@CODICEPARENTE", CODICEPARENTE)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@PERCENTUALE", PERCENTUALE)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", TIPOIMPORTO)
            cmdw.Parameters.AddWithValue("@IMPORTO_2", IMPORTO_2)
            cmdw.Parameters.AddWithValue("@IMPORTO1", IMPORTO1)
            cmdw.Parameters.AddWithValue("@IMPORTO3", IMPORTO2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", IMPORTO3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", IMPORTO4)
            cmdw.Parameters.AddWithValue("@MensileFisso", MensileFisso)
            cmdw.Parameters.AddWithValue("@TIPOOPERAZIONE", TIPOOPERAZIONE)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto        
        End If
    End Function
    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal codiceparente As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOPARENTI where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And codiceparente = " & codiceparente & " Order By Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Percentuale", GetType(String))
        Tabella.Columns.Add("Importo_2", GetType(String))
        Tabella.Columns.Add("Importo1", GetType(String))
        Tabella.Columns.Add("Importo2", GetType(String))
        Tabella.Columns.Add("Importo3", GetType(String))
        Tabella.Columns.Add("Importo4", GetType(String))
        Tabella.Columns.Add("MensileFisso", GetType(String))
        Tabella.Columns.Add("TIPOOPERAZIONE", GetType(String))
        'TIPOOPERAZIONE

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            myriga(1) = Format(campodbN(myPOSTreader.Item("Importo")), "#,##0.00")
            myriga(2) = myPOSTreader.Item("TIPOIMPORTO")
            myriga(3) = Format(campodbN(myPOSTreader.Item("Percentuale")) * 100, "#,##0.00")
            myriga(4) = Format(campodbN(myPOSTreader.Item("IMPORTO_2")), "#,##0.00")
            myriga(5) = Format(campodbN(myPOSTreader.Item("IMPORTO1")), "#,##0.00")
            myriga(6) = Format(campodbN(myPOSTreader.Item("IMPORTO2")), "#,##0.00")
            myriga(7) = Format(campodbN(myPOSTreader.Item("IMPORTO3")), "#,##0.00")
            myriga(8) = Format(campodbN(myPOSTreader.Item("IMPORTO4")), "#,##0.00")
            myriga(9) = campodbN(myPOSTreader.Item("MensileFisso"))
            myriga(10) = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))



            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myriga(0) = Mov.Data

            myriga(1) = 0
            myriga(2) = "G"
            myriga(3) = 0
            myriga(4) = 0
            myriga(5) = 0
            myriga(6) = 0
            myriga(7) = 0
            myriga(8) = 0
            myriga(9) = 0
            myriga(10) = ""
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub
End Class
