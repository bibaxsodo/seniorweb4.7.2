﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DatiComnueRegioneServizio
    Public CodiceRegione As String
    Public CodiceProvincia As String
    Public CodiceComune As String
    Public CentroServizio As String
    Public TipoOperazione As String
    Public AliquotaIva As String
    Public ModalitaPagamento As String
    Public Compensazione As String    



    Public Sub Pulisci()
        CodiceRegione = ""
        CodiceProvincia = ""
        CodiceComune = ""
        CentroServizio = ""
        TipoOperazione = ""
        AliquotaIva = ""        
        ModalitaPagamento = ""
        Compensazione = ""


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CodiceRegione <> "" Then
            cmd.CommandText = "select * from DatiComnueRegioneServizio where " & _
                               "CodiceRegione = ? And CentroServizio = ? "
            cmd.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        Else
            cmd.CommandText = "select * from DatiComnueRegioneServizio where " & _
                                       "CodiceProvincia = ? And CodiceComune = ? And CentroServizio = ? "

            cmd.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmd.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        End If


        

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceRegione = Val(campodb(myPOSTreader.Item("CodiceRegione")))
            CodiceProvincia = Val(campodb(myPOSTreader.Item("CodiceProvincia")))
            CodiceComune = Val(campodb(myPOSTreader.Item("CodiceComune")))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            AliquotaIva = campodb(myPOSTreader.Item("AliquotaIva"))            
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            Compensazione = campodb(myPOSTreader.Item("Compensazione"))            
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CodiceRegione <> "" Then
            cmd.CommandText = ("Delete from DatiComnueRegioneServizio where CODICEREGIONE = '" & CodiceRegione & "'")
        End If
        If CodiceProvincia <> "" Then
            cmd.CommandText = ("Delete from DatiComnueRegioneServizio where CodiceProvincia = '" & CodiceProvincia & "' And CodiceComune = '" & CodiceComune & "'")
        End If

        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CodiceRegione <> "" Then
            cmd.CommandText = "select * from DatiComnueRegioneServizio where " & _
                               "CodiceRegione = ? And CentroServizio = ? "
            cmd.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        Else
            cmd.CommandText = "select * from DatiComnueRegioneServizio where " & _
                                       "CodiceProvincia = ? And CodiceComune = ? And CentroServizio = ? "

            cmd.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmd.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "update DatiComnueRegioneServizio set TipoOperazione = ?, AliquotaIva = ?,ModalitaPagamento = ?,Compensazione = ?  where CodiceProvincia = ?  And CodiceComune = ?  And CodiceRegione = ? And CentroServizio = ?"
        Else
            MySql = "INSERT INTO DatiComnueRegioneServizio (TipoOperazione , AliquotaIva,ModalitaPagamento,Compensazione,CodiceProvincia,CodiceComune ,CodiceRegione,CentroServizio ) Values (?,?,?,?,?,?,?,?)"
        End If
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
        cmd1.Parameters.AddWithValue("@AliquotaIva", AliquotaIva)


        cmd1.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd1.Parameters.AddWithValue("@Compensazione", Compensazione)


        cmd1.Parameters.AddWithValue("@CodiceProvincia", campodb(CodiceProvincia))
        cmd1.Parameters.AddWithValue("@CodiceComune", campodb(CodiceComune))
        cmd1.Parameters.AddWithValue("@CodiceRegione", campodb(CodiceRegione))
        cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()


        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CodiceRegione <> "" Then
            cmd.CommandText = ("select * from DatiComnueRegioneServizio where CODICEREGIONE = '" & CodiceRegione & "' ORDER BY CentroServizio  ")
        Else
            cmd.CommandText = ("select * from DatiComnueRegioneServizio where CodiceProvincia = '" & CodiceProvincia & "' And CodiceComune = '" & CodiceComune & "' ORDER BY CentroServizio  ")
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Servizio", GetType(String))
        Tabella.Columns.Add("TipoOperazione", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            
            myriga(0) = campodb(myPOSTreader.Item("CentroServizio"))
            myriga(1) = campodb(myPOSTreader.Item("TipoOperazione"))
            myriga(2) = campodb(myPOSTreader.Item("AliquotaIva"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = ""
            myriga(1) = 0
            myriga(2) = "G"
            Tabella.Rows.Add(myriga)
        End If

        cn.Close()
    End Sub

End Class
