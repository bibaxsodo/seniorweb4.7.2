﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq



Public Class Cls_Epersonam
    Private Connessione As String

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Public Function RendiWardid(ByVal ward_id As Integer, ByVal Connessione As String) As Integer
        Dim cn As OleDbConnection
        Dim FlagTabellaPreacaricata As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Connessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NucleiCentroServizi")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            FlagTabellaPreacaricata = True
        End If
        myPOSTreader.Close()


        If FlagTabellaPreacaricata = True Then
            cmd.CommandText = ("select * from NucleiCentroServizi where IdEpersonam = ?")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@IdEpersonam", ward_id)
            Dim MyReadID As OleDbDataReader = cmd.ExecuteReader()
            If MyReadID.Read Then
                ward_id = campodbn(MyReadID.Item("IdSenior"))
            End If
            MyReadID.Close()

            Return ward_id
            Exit Function
        End If


        'SAADUN
        If ward_id = 1846 Then
            ward_id = 1845
        End If
        If ward_id = 1847 Then
            ward_id = 1845
        End If

        'VILLA RACHELE
        If ward_id = 3202 Then
            ward_id = 3200
        End If

        If ward_id = 3201 Then
            ward_id = 3200
        End If


        'NEFESH
        If ward_id = 3196 Then
            ward_id = 3187
        End If
        If ward_id = 3195 Then
            ward_id = 3187
        End If
        If ward_id = 3194 Then
            ward_id = 3187
        End If
        If ward_id = 3198 Then
            ward_id = 3187
        End If
        If ward_id = 3197 Then
            ward_id = 3187
        End If


        If ward_id = 3192 Then
            ward_id = 3188
        End If
        If ward_id = 3193 Then
            ward_id = 3188
        End If

        If ward_id = 3191 Then
            ward_id = 3186
        End If


        If ward_id = 3190 Then
            ward_id = 3189
        End If


        'ODA
        If ward_id = 2722 Then
            ward_id = 1774
        End If
        If ward_id = 2723 Then
            ward_id = 1774
        End If


        If ward_id = 1970 Then
            ward_id = 1972
        End If

        If ward_id = 3521 Then
            ward_id = 1772
        End If

        If ward_id = 1971 Then
            ward_id = 1772
        End If

        If ward_id = 1885 Then
            ward_id = 1773
        End If
        If ward_id = 1884 Then
            ward_id = 1773
        End If

        If ward_id = 3653 Then
            ward_id = 1773
        End If

        If ward_id = 3679 Then
            ward_id = 1773
        End If


        If ward_id = 3691 Then
            ward_id = 1773
        End If


        If ward_id = 3676 Then
            ward_id = 1773
        End If

        If ward_id = 3668 Then
            ward_id = 1773
        End If

        If ward_id = 3674 Then
            ward_id = 1773
        End If

        If ward_id = 3654 Then
            ward_id = 1773
        End If


        If ward_id = 3670 Then
            ward_id = 1773
        End If


        If ward_id = 3671 Then
            ward_id = 1773
        End If


        If ward_id = 2614 Then
            ward_id = 1773
        End If


        If ward_id = 3649 Then
            ward_id = 1773
        End If


        If ward_id = 1885 Then
            ward_id = 1773
        End If


        If ward_id = 3667 Then
            ward_id = 1773
        End If

        If ward_id = 3678 Then
            ward_id = 1773
        End If


        If ward_id = 3659 Then
            ward_id = 1773
        End If

        If ward_id = 3672 Then
            ward_id = 1773
        End If


        If ward_id = 1884 Then
            ward_id = 1773
        End If

        If ward_id = 3677 Then
            ward_id = 1773
        End If

        If ward_id = 3666 Then
            ward_id = 1773
        End If

        If ward_id = 3650 Then
            ward_id = 1773
        End If

        If ward_id = 3675 Then
            ward_id = 1773
        End If

        If ward_id = 3669 Then
            ward_id = 1773
        End If

        If ward_id = 3662 Then
            ward_id = 1773
        End If

        If ward_id = 3673 Then
            ward_id = 1773
        End If



        If ward_id = 2940 Then
            ward_id = 2939
        End If

        'ASP MAGIERA 
        If ward_id = 1154 Then
            ward_id = 1155
        End If

        If ward_id = 1156 Then
            ward_id = 1157
        End If
        If ward_id = 1161 Then
            ward_id = 1162
        End If

        If ward_id = 1164 Then
            ward_id = 1167
        End If

        If ward_id = 1159 Then
            ward_id = 1160
        End If

        If ward_id = 1408 Then
            ward_id = 1409
        End If

        If ward_id = 1159 Then
            ward_id = 1160
        End If
        If ward_id = 1165 Then
            ward_id = 1166
        End If




        ' ASP CARITAS - Personalizzazione
        If ward_id = 1071 Or ward_id = 1072 Or ward_id = 1073 Or ward_id = 1074 Then
            ward_id = 1071
        End If
        If ward_id = 1168 Or ward_id = 1169 Then
            ward_id = 1168
        End If

        If ward_id = 1498 Or ward_id = 1499 Or ward_id = 1500 Or ward_id = 1501 Then
            ward_id = 1498
        End If

        'Virginia Borgheri

        If ward_id = 1562 Or ward_id = 1564 Then
            ward_id = 1561
        End If


        'Cittadella
        If ward_id = 12 Or ward_id = 13 Or ward_id = 11 Or ward_id = 10 Or ward_id = 9 Or ward_id = 8 Or ward_id = 3006 Then
            ward_id = 8
        End If

        If ward_id = 23 Or ward_id = 24 Then
            ward_id = 23
        End If


        If ward_id = 17 Or ward_id = 19 Or ward_id = 20 Or ward_id = 985 Or ward_id = 987 Or ward_id = 988 Or ward_id = 989 Then
            ward_id = 989
        End If

        If ward_id = 999 Or ward_id = 21 Or ward_id = 22 Or ward_id = 998 Or ward_id = 2678 Then
            ward_id = 999
        End If

        If ward_id = 14 Or ward_id = 984 Then
            ward_id = 14
        End If

        'Virginia Borgheri
        If ward_id = 1562 Then
            ward_id = 1561
        End If


        ' COPERATIVA CREA


        ' COPERATIVA CREA


        If ward_id = 2032 Then
            ward_id = 2035
        End If

        If ward_id = 2033 Then
            ward_id = 2036
        End If
        If ward_id = 2031 Then
            ward_id = 2037
        End If
        If ward_id = 2034 Then
            ward_id = 2038
        End If
        If ward_id = 2041 Then
            ward_id = 2042
        End If
        If ward_id = 2034 Then
            ward_id = 2038
        End If
        If ward_id = 2040 Then
            ward_id = 2039
        End If




        'Prealpina
        If ward_id = 2098 Or ward_id = 2099 Or ward_id = 2100 Or ward_id = 2101 Then
            ward_id = 2096
        End If

        'VILLA TOMASI
        If ward_id = 3066 Then
            ward_id = 3065
        End If
        If ward_id = 3067 Then
            ward_id = 3065
        End If
        If ward_id = 3068 Then
            ward_id = 3065
        End If

        'binotto
        If ward_id = 2102 Or ward_id = 2103 Then
            ward_id = 2097
        End If

        'BELVEDERE
        '2366
        If ward_id = 2366 Then
            ward_id = 2367
        End If


        REM *****************************************************************************************

        'DON GNOCCHI

        If ward_id = 3048 Or ward_id = 3049 Then
            ward_id = 3046
        End If

        If ward_id = 3050 Or ward_id = 3051 Then
            ward_id = 3047
        End If


        REM **************************************************************************************

        'SAN GIUSEPPE

        If ward_id = 1436 Or ward_id = 1438 Or ward_id = 1437 Then
            ward_id = 1441
        End If

        If ward_id = 1435 Then
            ward_id = 1442
        End If

        If ward_id = 1446 Then
            ward_id = 1443
        End If

        REM ****************************MED SERVICE*******************
        REM RSA RESIDENZA DOMIZIANA 
        If ward_id = 2387 Then
            ward_id = 2389
        End If
        If ward_id = 2388 Then
            ward_id = 2389
        End If
        If ward_id = 2390 Then
            ward_id = 2389
        End If

        If ward_id = 2391 Then
            ward_id = 2389
        End If
        If ward_id = 2392 Then
            ward_id = 2389
        End If

        If ward_id = 2222 Then
            ward_id = 2389
        End If
        If ward_id = 2221 Then
            ward_id = 2389
        End If
        REM MEZZALUNA

        If ward_id = 2224 Then
            ward_id = 2484
        End If
        If ward_id = 2226 Then
            ward_id = 2484
        End If
        If ward_id = 2225 Then
            ward_id = 2484
        End If
        '2484
        REM SORESINA

        If ward_id = 2427 Then
            ward_id = 2445
        End If
        If ward_id = 2428 Then
            ward_id = 2445
        End If
        If ward_id = 2429 Then
            ward_id = 2445
        End If
        If ward_id = 2430 Then
            ward_id = 2445
        End If
        If ward_id = 2431 Then
            ward_id = 2445
        End If
        If ward_id = 2432 Then
            ward_id = 2445
        End If
        If ward_id = 2433 Then
            ward_id = 2445
        End If
        If ward_id = 2434 Then
            ward_id = 2445
        End If
        If ward_id = 2435 Then
            ward_id = 2445
        End If

        If ward_id = 2436 Then
            ward_id = 2445
        End If

        If ward_id = 2437 Then
            ward_id = 2445
        End If

        If ward_id = 2439 Then
            ward_id = 2445
        End If

        If ward_id = 2444 Then
            ward_id = 2438
        End If


        REM ****************NICHELINO/SAN MATTEO*******************************


        If ward_id = 3238 Then
            ward_id = 3225
        End If
        If ward_id = 3239 Then
            ward_id = 3225
        End If
        If ward_id = 3240 Then
            ward_id = 3225
        End If
        If ward_id = 3241 Then
            ward_id = 3225
        End If

        REM DIURNO

        If ward_id = 3237 Then
            ward_id = 3236
        End If

        REM ****************************MED SERVICE*******************

        REM ****************************ILENIA*******************
        If ward_id = 1101 Then
            ward_id = 1100
        End If
        REM ****************************ILENIA*******************

        REM ****************************DEMO*******************

        If ward_id = 855 Then
            ward_id = 852
        End If
        If ward_id = 856 Then
            ward_id = 852
        End If


        If ward_id = 1945 Then
            ward_id = 1946
        End If
        If ward_id = 2393 Then
            ward_id = 1946
        End If
        If ward_id = 2394 Then
            ward_id = 1946
        End If
        If ward_id = 2395 Then
            ward_id = 1946
        End If
        If ward_id = 2395 Then
            ward_id = 1946
        End If
        If ward_id = 2440 Then
            ward_id = 1946
        End If

        REM ****************************DEMO*******************

        REM ****************************LE BETULLE*******************

        If ward_id = 1660 Then
            ward_id = 1659
        End If

        If ward_id = 1655 Then
            ward_id = 1654
        End If

        If ward_id = 1656 Then
            ward_id = 1654
        End If


        If ward_id = 1656 Then
            ward_id = 1654
        End If

        REM ****************************LE BETULLE*******************

        REM ****************************LIETO*******************

        If ward_id = 2481 Then
            ward_id = 2480
        End If

        If ward_id <> 2480 And ward_id <> 2482 Then
            REM  ward_id = 2480
        End If
        REM ****************************LE BETULLE*******************

        REM *****************************
        REM * S.S. CONCEZIONE
        REM *****************************

        If ward_id = 2458 Then
            ward_id = 2219
        End If

        If ward_id = 2461 Then
            ward_id = 2219
        End If

        If ward_id = 2460 Then
            ward_id = 2219
        End If

        If ward_id = 2457 Then
            ward_id = 2219
        End If

        REM *****************************
        REM * S.S. CONCEZIONE
        REM *****************************


        REM ****************************NAVIGLI*******************

        If ward_id = 2540 Then
            ward_id = 2537
        End If
        If ward_id = 2541 Then
            ward_id = 2537
        End If
        If ward_id = 2542 Then
            ward_id = 2537
        End If
        If ward_id = 2545 Then
            ward_id = 2537
        End If

        If ward_id = 2544 Then
            ward_id = 2537
        End If

        If ward_id = 2543 Then
            ward_id = 2537
        End If

        If ward_id = 2538 Then
            ward_id = 2539
        End If



        If ward_id = 2918 Then
            ward_id = 2892
        End If

        If ward_id = 2926 Then
            ward_id = 2892
        End If

        If ward_id = 3024 Then
            ward_id = 2892
        End If

        If ward_id = 2918 Then
            ward_id = 2892
        End If



        REM ****************************NAVIGLI*******************


        REM ****************************BAGGIO*******************
        If ward_id = 2559 Then
            ward_id = 2550
        End If
        If ward_id = 2553 Then
            ward_id = 2550
        End If

        If ward_id = 2554 Then
            ward_id = 2550
        End If
        If ward_id = 2555 Then
            ward_id = 2550
        End If

        If ward_id = 2556 Then
            ward_id = 2550
        End If
        If ward_id = 2557 Then
            ward_id = 2550
        End If

        If ward_id = 2558 Then
            ward_id = 2550
        End If


        If ward_id = 2574 Then
            ward_id = 2552
        End If
        If ward_id = 2575 Then
            ward_id = 2552
        End If

        If ward_id = 2576 Then
            ward_id = 2552
        End If

        If ward_id = 2579 Then
            ward_id = 2552
        End If

        If ward_id = 2578 Then
            ward_id = 2552
        End If

        If ward_id = 2577 Then
            ward_id = 2552
        End If
        REM ****************************BAGGIO*******************
        REM ****************************VILLA CARPANEDA*******************



        If ward_id = 3288 Then
            ward_id = 2582
        End If

        If ward_id = 2587 Then
            ward_id = 2582
        End If
        If ward_id = 2588 Then
            ward_id = 2582
        End If

        If ward_id = 2589 Then
            ward_id = 2582
        End If
        If ward_id = 2590 Then
            ward_id = 2582
        End If

        If ward_id = 2591 Then
            ward_id = 2582
        End If

        If ward_id = 2793 Then
            ward_id = 2582
        End If

        If ward_id = 2592 Then
            ward_id = 2582
        End If

        REM *RODIGO

        If ward_id = 2566 Then
            ward_id = 2581
        End If
        If ward_id = 2569 Then
            ward_id = 2581
        End If

        If ward_id = 2570 Then
            ward_id = 2581
        End If
        If ward_id = 2571 Then
            ward_id = 2581
        End If

        If ward_id = 2572 Then
            ward_id = 2581
        End If

        If ward_id = 2568 Then
            ward_id = 2581
        End If

        REM ****************************VILLA CARPANEDA*******************


        REM *****************************USCITE SICUREZZA***************
        REM 2016-05-06 17:21:34.697	RSC	RSA COSTA ARGENTO	1890	1890
        REM NULL	NULL	CDC	CD COSTA ARGENTO	1891	1891
        REM NULL	NULL	RSF	RSA FERRUCCI	1894	1894
        REM NULL	NULL	RSS	RSA LE SUGHERE	1896	1896
        REM NULL	NULL	RSD	RSD IL SOLE	1898	1898

        REM RSA COSTA ARGENTO
        If ward_id = 2502 Then
            ward_id = 1890
        End If
        If ward_id = 2503 Then
            ward_id = 1890
        End If
        If ward_id = 2504 Then
            ward_id = 1890
        End If
        If ward_id = 2506 Then
            ward_id = 1890
        End If
        If ward_id = 2507 Then
            ward_id = 1890
        End If
        If ward_id = 2509 Then
            ward_id = 1890
        End If
        If ward_id = 2502 Then
            ward_id = 1890
        End If

        REM ****CD COSTA ARGENTO
        If ward_id = 2512 Then
            ward_id = 1891
        End If
        If ward_id = 2513 Then
            ward_id = 1891
        End If

        If ward_id = 1891 Then
            ward_id = 2513
        End If

        REM ****FERRUCCI
        If ward_id = 1902 Then
            ward_id = 1894
        End If
        If ward_id = 2022 Then
            ward_id = 1894
        End If
        If ward_id = 2023 Then
            ward_id = 1894
        End If
        If ward_id = 2045 Then
            ward_id = 1894
        End If
        If ward_id = 1903 Then
            ward_id = 1894
        End If
        REM LE SEGURE
        If ward_id = 1897 Then
            ward_id = 1896
        End If
        If ward_id = 2510 Then
            ward_id = 1896
        End If
        If ward_id = 2511 Then
            ward_id = 1896
        End If
        REM RSD IL SOLE
        If ward_id = 1899 Then
            ward_id = 1898
        End If
        REM *****************************

        REM *****************************
        REM * VILLA JOLE
        REM *****************************

        If ward_id = 2526 Then
            ward_id = 2525
        End If

        If ward_id = 2527 Then
            ward_id = 2525
        End If

        If ward_id = 2528 Then
            ward_id = 2525
        End If

        If ward_id = 2529 Then
            ward_id = 2525
        End If

        If ward_id = 2530 Then
            ward_id = 2525
        End If

        If ward_id = 2531 Then
            ward_id = 2525
        End If

        If ward_id = 2532 Then
            ward_id = 2525
        End If




        REM *****************************
        REM * VILLA JOLE
        REM *****************************


        REM *****************************
        REM * Invita s.r.l.
        REM *****************************

        If ward_id = 2683 Then
            ward_id = 2682
        End If
        If ward_id = 2684 Then
            ward_id = 2682
        End If


        If ward_id = 2686 Then
            ward_id = 2685
        End If
        If ward_id = 2687 Then
            ward_id = 2685
        End If
        If ward_id = 2688 Then
            ward_id = 2685
        End If

        REM *****************************
        REM * Invita s.r.l.
        REM *****************************


        REM *****************************
        REM * SAN DOMENICO
        REM *****************************


        If ward_id = 2711 Then
            ward_id = 2700
        End If
        If ward_id = 2712 Then
            ward_id = 2700
        End If
        If ward_id = 2713 Then
            ward_id = 2700
        End If
        If ward_id = 2714 Then
            ward_id = 2700
        End If

        REM *****************************
        REM * SAN DOMENICO
        REM *****************************




        REM *****************************
        REM * OIC DEMO
        REM *****************************


        If ward_id = 1826 Then
            ward_id = 1825
        End If
        If ward_id = 1828 Then
            ward_id = 1825
        End If
        If ward_id = 1857 Then
            ward_id = 1825
        End If
        If ward_id = 1858 Then
            ward_id = 1825
        End If
        If ward_id = 1864 Then
            ward_id = 1825
        End If
        If ward_id = 1862 Then
            ward_id = 1825
        End If

        REM *****************************
        REM * OIC DEMO
        REM *****************************


        REM *****************************
        REM * FONDAZIONE PIETRO SISSA
        REM *****************************

        '2856
        If ward_id = 2857 Then
            ward_id = 2856
        End If
        If ward_id = 2858 Then
            ward_id = 2856
        End If
        If ward_id = 2859 Then
            ward_id = 2856
        End If
        If ward_id = 2860 Then
            ward_id = 2856
        End If


        'REM *****************************
        'REM * ASP SIENA
        'REM *****************************
        'REM 1471 = CAMPANSI
        'If ward_id = 1472 Then
        '    ward_id = 1471
        'End If
        'If ward_id = 1473 Then
        '    ward_id = 1471
        'End If
        'If ward_id = 1477 Then
        '    ward_id = 1471
        'End If
        'If ward_id = 1475 Then
        '    ward_id = 1471
        'End If
        'If ward_id = 1476 Then
        '    ward_id = 1471
        'End If
        'If ward_id = 1478 Then
        '    ward_id = 1471
        'End If
        'REM 1480 = CACCIALUPI
        'If ward_id = 1481 Then
        '    ward_id = 1480
        'End If
        'If ward_id = 1482 Then
        '    ward_id = 1480
        'End If
        REM *****************************
        REM * VILLAGGIO SAN FRANCESCO
        REM *****************************
        REM 2909 = SAN FRANCESCO
        If ward_id = 2897 Then
            ward_id = 2909
        End If
        If ward_id = 2898 Then
            ward_id = 2909
        End If
        If ward_id = 2899 Then
            ward_id = 2909
        End If

        If ward_id = 2900 Then
            ward_id = 2909
        End If
        If ward_id = 2901 Then
            ward_id = 2909
        End If
        If ward_id = 2903 Then
            ward_id = 2909
        End If



        If ward_id = 2905 Then
            ward_id = 2909
        End If
        If ward_id = 2906 Then
            ward_id = 2909
        End If
        REM RSD ELISA
        If ward_id = 2902 Then
            ward_id = 2910
        End If



        RendiWardid = ward_id
    End Function

    Public Sub VisuallizzaAccoglimentoOspite(ByVal Token As String, ByVal ConnesioneOspiti As String, ByVal Chk_RicaricaDati As Boolean, ByVal context As HttpContext, ByVal codicefiscale As String, ByRef CServ As String, ByRef DataAccoglimento As String, Optional ByVal ParametroBusinessUnit As Integer = 0)

        Dim ConnectionString As String = ConnesioneOspiti
        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(ConnesioneOspiti)
        If Param.MeseFatturazione = 1 Then
            Param.MeseFatturazione = 12
            Param.AnnoFatturazione = Param.AnnoFatturazione - 1
        Else
            Param.MeseFatturazione = Param.MeseFatturazione - 1
        End If

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If

        Dim rawresp As String = ""
        Dim NonDati As Integer = 0
        Try
            If IsNothing(context.Session("LETTOMOVIMENTI")) Then
                NonDati = 1
            Else
                If context.Session("LETTOMOVIMENTI") = "" Then
                    NonDati = 1
                End If
            End If
        Catch ex As Exception
            NonDati = 1
        End Try

        If NonDati = 1 Then
            If ParametroBusinessUnit = 0 Then
                ParametroBusinessUnit = BusinessUnit(Token, context)
            End If
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & ParametroBusinessUnit & "/guests/movements/from/" & DataFatt)




            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())



            rawresp = reader.ReadToEnd()

            Dim SalvaDb As New OleDbCommand

            SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = 0 And Periodo = ? And Tipo = 'M'"
            SalvaDb.Parameters.AddWithValue("@Periodo", DataFatt)
            SalvaDb.Connection = cn

            Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
            If VerificaDB.Read Then
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'M'"
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            Else
                Dim UpDb As New OleDbCommand
                UpDb.Connection = cn
                UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo) values (?,?,?,?,?,'M') "
                UpDb.Parameters.AddWithValue("@Dati", rawresp)
                UpDb.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
                UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
                UpDb.Parameters.AddWithValue("@Pagina", 0)
                UpDb.Parameters.AddWithValue("@Periodo", DataFatt)
                UpDb.ExecuteNonQuery()
            End If
            VerificaDB.Close()

            context.Session("LETTOMOVIMENTI") = rawresp
        Else
            rawresp = context.Session("LETTOMOVIMENTI")
        End If



        Dim jResults As JArray = JArray.Parse(rawresp)
        Dim LastMov As String

        Dim Ospite As New ClsOspite

        Ospite.CODICEFISCALE = codicefiscale
        Ospite.LeggiPerCodiceFiscale(ConnesioneOspiti, Ospite.CODICEFISCALE)



        For Each jTok As JToken In jResults
            rawresp = rawresp & jTok.Item("guests_movements").ToString()
            For Each jTok1 As JToken In jTok.Item("guests_movements").Children

                Dim ward_id As Integer


                ward_id = jTok1.Item("ward_id").ToString()

                If codicefiscale = jTok1.Item("cf").ToString() Then
                    LastMov = ""
                    For Each jTok2 As JToken In jTok1.Item("movements").Children
                        Dim IdEpersonam As String
                        Dim Descrizione As String
                        IdEpersonam = jTok2.Item("id").ToString

                        Try
                            Descrizione = jTok2.Item("description").ToString
                        Catch ex As Exception

                        End Try

                        If (jTok2.Item("type").ToString = "8" And (Descrizione = "ENTER" Or Descrizione = "")) Or jTok2.Item("type").ToString <> "8" Then

                            If LastMov = "11" And jTok2.Item("type").ToString = "8" Then

                            Else

                                LastMov = jTok2.Item("type").ToString

                                Dim cmd As New OleDbCommand()

                                If jTok2.Item("type").ToString = "11" Then
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  CodiceOspite = " & Ospite.CodiceOspite & " And Data = ?")
                                    cmd.Parameters.AddWithValue("@Data", Mid(jTok2.Item("date").ToString, 1, 10))

                                Else
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  IdEpersonam = " & IdEpersonam)
                                End If

                                cmd.Connection = cn

                                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                If Not myPOSTreader.Read Then
                                    Dim Tipo As String = ""
                                    Dim DataMov As String = ""

                                    If jTok2.Item("type").ToString = "11" Then
                                        Tipo = "05"
                                    End If
                                    If jTok2.Item("type").ToString = "9" Then
                                        Tipo = "03"
                                    End If
                                    If jTok2.Item("type").ToString = "8" Then
                                        Tipo = "04"
                                    End If
                                    If jTok2.Item("type").ToString = "12" Then
                                        Tipo = "13"
                                    End If

                                    If Tipo = "05" Then
                                        DataMov = jTok2.Item("date").ToString

                                        ward_id = RendiWardid(ward_id, ConnesioneOspiti)

                                        Dim CS As New Cls_CentroServizio
                                        CS.DESCRIZIONE = ""

                                        CS.Convenzione = ""
                                        Try
                                            CS.Convenzione = jTok2.Item("conv").ToString
                                        Catch ex As Exception

                                        End Try

                                        CS.EPersonam = ward_id
                                        If CS.Convenzione.ToUpper.Trim = "TRUE" Then
                                            CS.LeggiEpersonam(ConnesioneOspiti, CS.EPersonam, True)
                                        Else
                                            CS.LeggiEpersonam(ConnesioneOspiti, CS.EPersonam, False)
                                        End If

                                        DataAccoglimento = jTok2.Item("date").ToString
                                        If CS.CENTROSERVIZIO = "" Then
                                            DataAccoglimento = DataAccoglimento & " " & ward_id
                                        End If
                                        CServ = CS.CENTROSERVIZIO
                                        If CServ <> "" Then
                                            myPOSTreader.Close()
                                            Trovato = True
                                        End If
                                    End If

                                    Dim DataVerifica As Date

                                    Try
                                        DataVerifica = jTok2.Item("date").ToString
                                    Catch ex As Exception

                                    End Try

                                    If Tipo = "13" And Format(DataVerifica, "yyyyMMdd") < Format(DateSerial(Param.AnnoFatturazione, Param.MeseFatturazione, 1), "yyyyMMdd") Then
                                        Trovato = False
                                        CServ = ""
                                        DataAccoglimento = ""
                                    End If

                                End If
                                myPOSTreader.Close()
                                End If
                        End If
                    Next
                End If
                If Trovato = True Then
                    Exit For
                End If
            Next
            If Trovato = True Then
                Exit For
            End If
        Next

        cn.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Function ElencoSottoStrutture(ByVal Token As String, ByVal context As HttpContext, ByVal businessID As Integer) As Integer
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/dictionary/business_units/" & businessID)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()
        Try
            Dim jResults As JObject = JObject.Parse(rawresp)

            For Each jTok2 As JToken In jResults.Item("structures").Children
                If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                    ElencoSottoStrutture = Val(jTok2.Item("id").ToString)
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try

    End Function

    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            Else

                Dim BU As Integer = Val(jTok2.Item("id").ToString)

                BusinessUnit = ElencoSottoStrutture(Token, context, BU)

                If context.Session("NomeEPersonam") = "PROVA" Then
                    BusinessUnit = 1825
                End If
            End If
        Next



    End Function


    Public Function DecodificaGradoParente(ByVal CodEpersonam As String, ByVal DecodificaEpersonam As String, ByVal Connessione As String) As String
        Dim DecTab As New Cls_Tabelle


        DecTab.TipTab = "EGP"
        DecTab.Codice = CodEpersonam
        DecTab.Leggi(Connessione)
        If DecTab.Descrizione = "" Then
            DecTab.TipTab = "GPA"
            DecTab.Codice = CodEpersonam
            DecTab.Descrizione = DecodificaEpersonam
            DecTab.Scrivi(Connessione)
        Else
            Return DecTab.Descrizione
            Exit Function
        End If

        Return CodEpersonam
    End Function

    Public Function DecodificaStatoCivile(ByVal CodEpersonam As String, ByVal DecodificaEpersonam As String, ByVal Connessione As String) As String
        Dim DecTab As New Cls_Tabelle


        DecTab.TipTab = "EST"
        DecTab.Codice = CodEpersonam
        DecTab.Leggi(Connessione)
        If DecTab.Descrizione = "" Then
            DecTab.TipTab = "STC"
            DecTab.Codice = CodEpersonam
            DecTab.Descrizione = DecodificaEpersonam
            DecTab.Scrivi(Connessione)
        Else
            Return DecTab.Descrizione
            Exit Function
        End If

        Return CodEpersonam
    End Function
End Class
