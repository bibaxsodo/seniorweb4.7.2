﻿Imports Microsoft.VisualBasic

Public Class ClsCodiceFiscale
    Public Function ControlloIntegritaCodiceFiscale(ByVal stringa As String) As Boolean
        Dim Somma As Double
        Dim ChEl As String
        Dim NumEl As String
        Dim cars As String
        Dim MySomma As Double
        Dim LastLettera As String
        Dim i As Integer

        If Len(stringa) <> 16 Then ControlloIntegritaCodiceFiscale = False : Exit Function

        ControlloIntegritaCodiceFiscale = True
        Somma = 0
        For I = 2 To 14 Step 2
            If Mid(stringa, I, 1) <= "9" And Mid(stringa, I, 1) >= "0" Then
                Somma = Somma + Mid(stringa, I, 1)
            Else
                Somma = Somma + Asc(Mid(stringa, I, 1)) - 65
            End If
        Next I

        ChEl = "A01B00C05D07E09F13G15H17I19J21K02L04M18N20O11P03Q06R08S12T14U16V10W22X25Y24Z23"
        NumEl = "000507091315171921"

        For I = 1 To 15 Step 2
            If Mid(stringa, I, 1) <= "9" And Mid(stringa, I, 1) >= "0" Then
                If Mid(stringa, I, 1) = 0 Then
                    Somma = Somma + 1
                Else
                    Somma = Somma + Mid(NumEl, (Val(Mid(stringa, I, 1)) * 2) - 1, 2)
                End If
            Else
                cars = InStr(1, ChEl, Mid(stringa, I, 1))
                If cars <> 0 Then
                    Somma = Somma + Val(Mid(ChEl, cars + 1, 2))
                End If
            End If
        Next I
        MySomma = Int(Somma / 26)
        MySomma = Somma - (MySomma * 26)
        LastLettera = Chr(MySomma + 65)

        If LastLettera <> Mid(stringa, 16, 1) Then
            ControlloIntegritaCodiceFiscale = False
        End If
    End Function


End Class
