﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data.OleDb

Public Class Cls_CalcoloOspitiFineMese
    Public StringaDegliErrori As String =""

    REM ********************************

    Public Class Giorni
        Public Data As Date
        Public Causale As String
        Public TabellaDettaglioGiorno As List(Of DettaglioGiorno) = New List(Of DettaglioGiorno)
    End Class


    Public Class DettaglioGiorno
        Public Data As Date
        Public Pagante As String
        Public TipoImporto As String
        Public TipoOperazione As String
        Public CodiceEnte As String
        Public CodiceParente As Integer
        Public Percentuale As Double
        Public TipoRetta As String
        Public CodiceExtra As String
        Public SpecificaImporto As String
        Public Importo As Double
        Public ExtraFissi As List(Of String) = New List(Of String)

    End Class


    Dim TabellaGiorni As List(Of Giorni) = New List(Of Giorni)

    Public Class RaggruppamentoQuote
        Public Pagante As String
        Public Elemento As String
        Public Importo As Double
        Public Giorni As Integer
        Public CodiceExtra As String
        Public Anno As Integer
        Public Mese As Integer

        Public CodiceParente As Integer

        Public ImportoUnitario As Double
        Public TipoOperazione As String


        Public CodiceRegione As String

        Public CodiceComune As String

        Public CodiceJolly As String


    End Class

    Dim TabellaRaggruppamentoQuote As List(Of RaggruppamentoQuote) = New List(Of RaggruppamentoQuote)

    Public Class ImportiGiornoParente
        Public CodiceParente As Integer = 0
        Public ImportoRettaParente1 As Double = 0
        Public ImportoRettaParente2 As Double = 0
        Public ImportoRettaParenteExt1 As Double = 0
        Public ImportoRettaParenteExt2 As Double = 0
        Public ImportoRettaParenteExt3 As Double = 0
        Public ImportoRettaParenteExt4 As Double = 0
        Public Percentuale As Double = 0
        Public TipoRettaParente As String = ""
        Public TipoOperazioneParente As String = ""
    End Class


    Public Class ImportiGiorno
        Public ImportoRettaOspite1 As Double = 0
        Public ImportoRettaOspite2 As Double = 0
        Public ImportoRettaOspiteExt1 As Double = 0
        Public ImportoRettaOspiteExt2 As Double = 0
        Public ImportoRettaOspiteExt3 As Double = 0
        Public ImportoRettaOspiteExt4 As Double = 0
        Public TipoRettaOspite As String = ""
        Public TipoOperazioneOspite As String = ""
        Public Modalita As String = ""


        Public ImportoComune As Double = 0
        Public CodiceComune As String = ""
        Public ImportoComuneExt1 As Double = 0
        Public ImportoComuneExt2 As Double = 0
        Public ImportoComuneExt3 As Double = 0
        Public ImportoComuneExt4 As Double = 0
        Public TipoRettaComune As String = ""


        Public ImportoRettaTotale As Double = 0
        Public TipoRettaTotale As String = ""


        Public ImportoRettaRegione As Double = 0
        Public TipoRettaRegione As String = ""
        Public CodiceRegione As String = ""
        Public TipoImportoRegione As String = ""



        Public ImportoJolly As Double = 0
        Public CodiceJolly As String = ""
        Public ImportoJollyExt1 As Double = 0
        Public ImportoJollyExt2 As Double = 0
        Public ImportoJollyExt3 As Double = 0
        Public ImportoJollyExt4 As Double = 0
        Public TipoRettaJolly As String = ""

        Public Parente As List(Of ImportiGiornoParente) = New List(Of ImportiGiornoParente)
        Public ExtraFissi As List(Of String) = New List(Of String)
    End Class

    Dim TabellaImportiGiorno As New ImportiGiorno

    Public Function OspiteDimessonelMese(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Date
        Dim Movimenti As New Cls_Movimenti

        Movimenti.CodiceOspite = CodOsp
        Movimenti.CENTROSERVIZIO = Cserv
        Movimenti.Data = Nothing
        Movimenti.UltimaDataUscitaDefinitivaMese(ConnessioniOspiti, Anno, Mese)
        OspiteDimessonelMese = Movimenti.Data

    End Function


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function MovimentiPeriodoOspite(ByVal Cserv As String, ByVal CodOsp As Long, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String) As List(Of Giorni)
        Dim cn As OleDbConnection
        Dim ListaGiorni As List(Of Giorni) = New List(Of Giorni)

        cn = New Data.OleDb.OleDbConnection(ConnessioniOspiti)

        cn.Open()

        Dim Parametri As New Cls_Parametri

        Parametri.LeggiParametri(ConnessioniOspiti)



        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where centroservizio = ? And CodiceOspite = ?  And Data <= ? Order by Data DEsc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@Data", UscitaDefinitiva)

        Dim ReadMovimenti As OleDbDataReader = cmd.ExecuteReader
        Dim UltimaData As Date = Nothing
        Dim UltimaCausale As String = ""

        Do While ReadMovimenti.Read
            Dim DataLettura As Date = campodbd(ReadMovimenti.Item("Data"))
            If IsNothing(UltimaData) Or Year(UltimaData) < 1900 Then
                If campodb(ReadMovimenti.Item("TipoMov")) = "13" And Parametri.GIORNOUSCITA = "" Then
                    Dim Accoglimento As New Cls_Movimenti

                    Accoglimento.CodiceOspite = CodOsp
                    Accoglimento.CENTROSERVIZIO = Cserv
                    Accoglimento.Data = DataLettura
                    Accoglimento.UltimaDataAccoglimentoAData(ConnessioniOspiti)

                    If Format(Accoglimento.Data, "yyyyMMdd") < Format(DataLettura, "yyyyMMdd") Then
                        DataLettura = DateAdd(DateInterval.Day, -1, DataLettura)
                    End If
                End If
                ListaGiorni.Add(New Giorni With {.Causale = campodb(ReadMovimenti.Item("Causale")), .Data = DataLettura})
            Else
                Dim DataElaborazione As Date = DateAdd(DateInterval.Day, -1, UltimaData)
                Do While DataElaborazione >= campodb(ReadMovimenti.Item("Data"))
                    If campodb(ReadMovimenti.Item("TipoMov")) = "03" Then
                        ListaGiorni.Add(New Giorni With {.Causale = campodb(ReadMovimenti.Item("Causale")), .Data = DataElaborazione})
                    Else
                        ListaGiorni.Add(New Giorni With {.Causale = "", .Data = DataElaborazione})
                    End If

                    DataElaborazione = DataElaborazione.AddDays(-1)
                Loop
            End If

            If campodb(ReadMovimenti.Item("TipoMov")) = "05" Then
                Exit Do
            End If

            UltimaData = DataLettura
            UltimaCausale = campodb(ReadMovimenti.Item("Causale"))
        Loop


        cn.Close()

        Return ListaGiorni
    End Function

    Public Sub ImportiPeriodo(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataAccoglimento As Date, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnessioniOspiti)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTORETTA where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadImportoTotale As OleDbDataReader = cmd.ExecuteReader
        Do While ReadImportoTotale.Read()

            If Format(campodbd(ReadImportoTotale.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadImportoTotale.Item("Data"))

                Dim Extra As List(Of String) = New List(Of String)

                Dim cmdRead As New OleDbCommand()
                cmdRead.CommandText = "Select * From EXTRAOSPITE where  centroservizio = ? And CodiceOspite = ? And Data = ?"
                cmdRead.Connection = cn
                cmdRead.Parameters.AddWithValue("@CentroServizio", Cserv)
                cmdRead.Parameters.AddWithValue("@CodiceOspite", CodOsp)
                cmdRead.Parameters.AddWithValue("@Data", compareValue)
                Dim ReadExtra As OleDbDataReader = cmdRead.ExecuteReader
                Do While ReadExtra.Read()
                    Extra.Add(ReadExtra.Item("CODICEEXTRA"))
                Loop
                ReadExtra.Close()



                Try

                     TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .Importo = campodb(ReadImportoTotale.Item("Importo")), .Pagante = "T", .SpecificaImporto = "R1", _
                                        .TipoOperazione = "", .TipoImporto = campodb(ReadImportoTotale.Item("TIPORETTA")), .ExtraFissi = Extra})

                Catch ex As Exception
                    StringaDegliErrori =StringaDegliErrori & "Periodo non elaborato per ospite " & CodOsp
                End Try
           
      
            Else

                compareValue = DataAccoglimento
                Dim Extra As List(Of String) = New List(Of String)

                Dim cmdRead As New OleDbCommand()
                cmdRead.CommandText = "Select * From EXTRAOSPITE where  centroservizio = ? And CodiceOspite = ? And Data = ?"
                cmdRead.Connection = cn
                cmdRead.Parameters.AddWithValue("@CentroServizio", Cserv)
                cmdRead.Parameters.AddWithValue("@CodiceOspite", CodOsp)
                cmdRead.Parameters.AddWithValue("@Data", compareValue)
                Dim ReadExtra As OleDbDataReader = cmdRead.ExecuteReader
                Do While ReadExtra.Read()
                    Extra.Add(ReadExtra.Item("CODICEEXTRA"))
                Loop
                ReadExtra.Close()

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                            .Importo = campodb(ReadImportoTotale.Item("Importo")), .Pagante = "T", .SpecificaImporto = "R1", _
                                            .TipoOperazione = "", .TipoImporto = campodb(ReadImportoTotale.Item("TIPORETTA")), .ExtraFissi = Extra})



                Exit Do
            End If
        Loop
        ReadImportoTotale.Close()



        cmd.CommandText = ("select * from ImportoOspite where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadImportoOspite As OleDbDataReader = cmd.ExecuteReader
        Do While ReadImportoOspite.Read()

            If Format(campodbd(ReadImportoOspite.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadImportoOspite.Item("Data"))

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                             .Importo = campodb(ReadImportoOspite.Item("Importo")), .Pagante = "O", .SpecificaImporto = "R1", _
                                             .TipoOperazione = campodb(ReadImportoOspite.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoOspite.Item("TIPORETTA"))})

                If campodbN(ReadImportoOspite.Item("IMPORTO_2")) > 0 Then
                    TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                             .Importo = campodb(ReadImportoOspite.Item("IMPORTO_2")), .Pagante = "O", .SpecificaImporto = "R2", _
                                             .TipoOperazione = campodb(ReadImportoOspite.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoOspite.Item("TIPORETTA"))})
                End If
            Else

                compareValue = DataAccoglimento

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                             .Importo = campodb(ReadImportoOspite.Item("Importo")), .Pagante = "O", .SpecificaImporto = "R1", _
                                             .TipoOperazione = campodb(ReadImportoOspite.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoOspite.Item("TIPORETTA"))})
                If campodbN(ReadImportoOspite.Item("IMPORTO_2")) > 0 Then
                    TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                                 .Importo = campodb(ReadImportoOspite.Item("IMPORTO_2")), .Pagante = "O", .SpecificaImporto = "R2", _
                                                 .TipoOperazione = campodb(ReadImportoOspite.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoOspite.Item("TIPORETTA"))})
                End If
                Exit Do
            End If
        Loop
        ReadImportoOspite.Close()


        cmd.CommandText = ("select * from IMPORTOCOMUNE where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadImportoComune As OleDbDataReader = cmd.ExecuteReader
        Do While ReadImportoComune.Read()

            If Format(campodbd(ReadImportoComune.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadImportoComune.Item("Data"))

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .Importo = campodb(ReadImportoComune.Item("Importo")), .Pagante = "C", .SpecificaImporto = "R1", _
                                        .TipoOperazione = "", .TipoImporto = campodb(ReadImportoComune.Item("TIPORETTA")), _
                                        .CodiceEnte = campodb(ReadImportoComune.Item("PROV")) & campodb(ReadImportoComune.Item("COMUNE"))})
            Else

                compareValue = DataAccoglimento

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .Importo = campodb(ReadImportoComune.Item("Importo")), .Pagante = "C", .SpecificaImporto = "R1", _
                                        .TipoOperazione = "", .TipoImporto = campodb(ReadImportoComune.Item("TIPORETTA")), _
                                        .CodiceEnte = campodb(ReadImportoComune.Item("PROV")) & campodb(ReadImportoComune.Item("COMUNE"))})
                Exit Do
            End If
        Loop
        ReadImportoComune.Close()



        cmd.CommandText = ("select * from IMPORTOJOLLY where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadImportoJolly As OleDbDataReader = cmd.ExecuteReader
        Do While ReadImportoJolly.Read()

            If Format(campodbd(ReadImportoJolly.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadImportoJolly.Item("Data"))

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .Importo = campodb(ReadImportoJolly.Item("Importo")), .Pagante = "J", .SpecificaImporto = "R1", _
                                        .TipoOperazione = "", .TipoImporto = campodb(ReadImportoJolly.Item("TIPORETTA")), _
                                        .CodiceEnte = campodb(ReadImportoJolly.Item("PROV")) & campodb(ReadImportoJolly.Item("COMUNE"))})
            Else

                compareValue = DataAccoglimento

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .Importo = campodb(ReadImportoJolly.Item("Importo")), .Pagante = "J", .SpecificaImporto = "R1", _
                                        .TipoOperazione = "", .TipoImporto = campodb(ReadImportoJolly.Item("TIPORETTA")), _
                                        .CodiceEnte = campodb(ReadImportoJolly.Item("PROV")) & campodb(ReadImportoJolly.Item("COMUNE"))})
                Exit Do
            End If
        Loop
        ReadImportoJolly.Close()


        Dim cmdParente As New OleDbCommand
        cmdParente.CommandText = ("select CodiceParente from IMPORTOPARENTI where centroservizio = ? And CodiceOspite = ? And Data <= ? group by CodiceParente")
        cmdParente.Connection = cn
        cmdParente.Parameters.Clear()
        cmdParente.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdParente.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmdParente.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)
        Dim ReadParenti As OleDbDataReader = cmdParente.ExecuteReader
        Do While ReadParenti.Read()


            cmd.CommandText = ("select * from IMPORTOPARENTI where centroservizio = ? And CodiceOspite = ? And CodiceParente = ? And Data <= ? Order by  Data Desc")
            cmd.Connection = cn
            cmd.Parameters.Clear()
            cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
            cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
            cmd.Parameters.AddWithValue("@CodiceParente", campodbN(ReadParenti.Item("CodiceParente")))
            cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

            Dim ReadImportoParente As OleDbDataReader = cmd.ExecuteReader
            Do While ReadImportoParente.Read()

                If Format(campodbd(ReadImportoParente.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                    compareValue = campodbd(ReadImportoParente.Item("Data"))

                    TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                            .Importo = campodb(ReadImportoParente.Item("Importo")), .Pagante = "P", .SpecificaImporto = "R1", _
                                            .TipoOperazione = campodb(ReadImportoParente.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoParente.Item("TIPOIMPORTO")), _
                                            .CodiceParente = campodb(ReadImportoParente.Item("CODICEPARENTE")), .Percentuale = campodb(ReadImportoParente.Item("PERCENTUALE"))})
                    If campodb(ReadImportoParente.Item("importo_2")) > 0 Then
                        TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                                .Importo = campodb(ReadImportoParente.Item("importo_2")), .Pagante = "P", .SpecificaImporto = "R1", _
                                                .TipoOperazione = campodb(ReadImportoParente.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoParente.Item("TIPOIMPORTO")), _
                                                .CodiceParente = campodb(ReadImportoParente.Item("CODICEPARENTE")), .Percentuale = campodb(ReadImportoParente.Item("PERCENTUALE"))})

                    End If
                Else

                    compareValue = DataAccoglimento

                    TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                            .Importo = campodb(ReadImportoParente.Item("Importo")), .Pagante = "P", .SpecificaImporto = "R1", _
                                            .TipoOperazione = campodb(ReadImportoParente.Item("TIPOOPERAZIONE")), .TipoImporto = campodb(ReadImportoParente.Item("TIPOIMPORTO")), _
                                            .CodiceParente = campodb(ReadImportoParente.Item("CODICEPARENTE")), .Percentuale = campodb(ReadImportoParente.Item("PERCENTUALE"))})
                    Exit Do
                End If
            Loop
            ReadImportoParente.Close()
        Loop
        ReadParenti.Close()


        cmd.CommandText = ("select * from MODALITA where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadImportoMODALITA As OleDbDataReader = cmd.ExecuteReader
        Do While ReadImportoMODALITA.Read()

            If Format(campodbd(ReadImportoMODALITA.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadImportoMODALITA.Item("Data"))

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .TipoImporto = campodb(ReadImportoMODALITA.Item("MODALITA")), .Pagante = "M"})
            Else

                compareValue = DataAccoglimento

                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .TipoImporto = campodb(ReadImportoMODALITA.Item("MODALITA")), .Pagante = "M"})
                Exit Do
            End If
        Loop
        ReadImportoMODALITA.Close()


        cmd.CommandText = ("select * from STATOAUTO where centroservizio = ? And CodiceOspite = ? And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@UscitaDefinitiva", UscitaDefinitiva)

        Dim ReadSTATOAUTO As OleDbDataReader = cmd.ExecuteReader
        Do While ReadSTATOAUTO.Read()

            If Format(campodbd(ReadSTATOAUTO.Item("Data")), "yyyyMMdd") > Format(DataAccoglimento, "yyyyMMdd") Then
                compareValue = campodbd(ReadSTATOAUTO.Item("Data"))

                Dim ImportoRegione As New Cls_ImportoRegione

                ImportoRegione.Codice = campodb(ReadSTATOAUTO.Item("USL"))
                ImportoRegione.TipoRetta = campodb(ReadSTATOAUTO.Item("TIPORETTA"))
                ImportoRegione.Data = compareValue
                ImportoRegione.LeggiAData(ConnessioniOspiti, ImportoRegione.Codice, ImportoRegione.TipoRetta)



                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .CodiceEnte = campodb(ReadSTATOAUTO.Item("USL")), .TipoRetta = campodb(ReadSTATOAUTO.Item("TIPORETTA")), _
                                        .Importo = ImportoRegione.Importo, .TipoImporto = ImportoRegione.Tipo, .Pagante = "R"})
            Else

                compareValue = DataAccoglimento

                Dim ImportoRegione As New Cls_ImportoRegione

                ImportoRegione.Codice = campodb(ReadSTATOAUTO.Item("USL"))
                ImportoRegione.TipoRetta = campodb(ReadSTATOAUTO.Item("TIPORETTA"))
                ImportoRegione.Data = compareValue
                ImportoRegione.LeggiAData(ConnessioniOspiti, ImportoRegione.Codice, ImportoRegione.TipoRetta)


                TabellaGiorni.FindLast(AddressOf FindData).TabellaDettaglioGiorno.Add(New DettaglioGiorno With {.Data = compareValue, _
                                        .CodiceEnte = campodb(ReadSTATOAUTO.Item("USL")), .TipoRetta = campodb(ReadSTATOAUTO.Item("TIPORETTA")), _
                                        .Importo = ImportoRegione.Importo, .TipoImporto = ImportoRegione.Tipo, .Pagante = "R"})
                Exit Do
            End If
        Loop
        ReadImportoMODALITA.Close()
        cn.Close()

    End Sub

    Private compareValue As Date

    Private Function FindData(ByVal value As Giorni) As Boolean
        If value.Data = compareValue Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub LetturaDatiGiorni(ByVal Indice As Integer)
        If Not IsNothing(TabellaGiorni(Indice).TabellaDettaglioGiorno) Then
            For Each item As DettaglioGiorno In TabellaGiorni(Indice).TabellaDettaglioGiorno
                If item.Pagante = "M" Then
                    TabellaImportiGiorno.Modalita = item.TipoImporto
                End If

                If item.Pagante = "O" Then
                    TabellaImportiGiorno.TipoRettaOspite = item.TipoImporto
                    TabellaImportiGiorno.TipoOperazioneOspite = item.TipoOperazione
                    If item.SpecificaImporto = "R1" Then
                        TabellaImportiGiorno.ImportoRettaOspite1 = item.Importo
                    End If
                    If item.SpecificaImporto = "R2" Then
                        TabellaImportiGiorno.ImportoRettaOspite2 = item.Importo
                    End If
                End If

                If item.Pagante = "C" Then
                    TabellaImportiGiorno.TipoRettaComune = item.TipoImporto
                    TabellaImportiGiorno.CodiceComune = item.CodiceEnte
                    If item.SpecificaImporto = "R1" Then
                        TabellaImportiGiorno.ImportoComune = item.Importo
                    End If
                End If


                If item.Pagante = "J" Then
                    TabellaImportiGiorno.TipoRettaComune = item.TipoImporto
                    TabellaImportiGiorno.CodiceComune = item.CodiceEnte
                    If item.SpecificaImporto = "R1" Then
                        TabellaImportiGiorno.ImportoJolly = item.Importo
                    End If
                End If


                If item.Pagante = "R" Then
                    TabellaImportiGiorno.TipoImportoRegione = item.TipoImporto
                    TabellaImportiGiorno.CodiceRegione = item.CodiceEnte
                    TabellaImportiGiorno.TipoRettaRegione = item.TipoRetta
                    TabellaImportiGiorno.ImportoRettaRegione = item.Importo
                End If


                If item.Pagante = "T" Then
                    TabellaImportiGiorno.TipoRettaTotale = item.TipoImporto
                    If item.SpecificaImporto = "R1" Then
                        TabellaImportiGiorno.ImportoRettaTotale = item.Importo

                        TabellaImportiGiorno.ExtraFissi = item.ExtraFissi
                    End If
                End If

                If item.Pagante = "P" Then
                    Dim Trovato As Boolean = False
                    For Each Parente As ImportiGiornoParente In TabellaImportiGiorno.Parente
                        If Parente.CodiceParente = item.CodiceParente Then
                            Parente.TipoRettaParente = item.TipoRetta
                            Parente.Percentuale = item.Percentuale
                            If item.SpecificaImporto = "R1" Then
                                Parente.ImportoRettaParente1 = item.Importo
                            End If
                            If item.SpecificaImporto = "R2" Then
                                Parente.ImportoRettaParente2 = item.Importo
                            End If
                            Trovato = True
                        End If
                    Next

                    If Not Trovato Then
                        If item.SpecificaImporto = "R1" Then
                            TabellaImportiGiorno.Parente.Add(New ImportiGiornoParente With {.CodiceParente = item.CodiceParente, _
                                                        .TipoRettaParente = item.TipoRetta, .Percentuale = item.Percentuale, .ImportoRettaParente1 = item.Importo})
                        End If
                        If item.SpecificaImporto = "R2" Then
                            TabellaImportiGiorno.Parente.Add(New ImportiGiornoParente With {.CodiceParente = item.CodiceParente, _
                                                        .TipoRettaParente = item.TipoRetta, .Percentuale = item.Percentuale, .ImportoRettaParente1 = item.Importo})
                        End If

                    End If
                End If
            Next
        End If
    End Sub

    Private Sub ScriviAddebiti(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String, ByVal MScriviOspite As Boolean, ByVal MScriviParente As Boolean, ByVal MScriviComune As Boolean, ByVal MScriviRegione As Boolean)
        Dim Indice As Integer = 0
        Dim ClasseRette As New ClassRetteOspiti

        Dim OspitiCon As OleDbConnection

        OspitiCon = New Data.OleDb.OleDbConnection(ConnessioniOspiti)
        OspitiCon.Open()

        ClasseRette.ApriDB(ConnessioniOspiti)

        Dim cmdAddebiti As New OleDbCommand()
        cmdAddebiti.CommandText = "Select * From ADDACR Where (Elaborato =0 or Elaborato IS Null) And CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And month(Data) = " & Mese & " And year(Data) = " & Anno
        cmdAddebiti.Connection = OspitiCon
        Dim RDAddebiti As OleDbDataReader = cmdAddebiti.ExecuteReader()
        Do While RDAddebiti.Read
            Dim Elemeto As String = "ADD"
            Dim Importo As Double = campodbN(RDAddebiti.Item("IMPORTO"))
            Dim CodiceAddebito As String = campodb(RDAddebiti.Item("CodiceIva"))
            If campodb(RDAddebiti.Item("TIPOMOV")) = "AC" Then
                Elemeto = "ACC"
            End If

            If campodb(RDAddebiti.Item("RIFERIMENTO")) = "O" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.CODICEPARENTE = 0
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = Elemeto
                ClasseRette.Giorni = campodbN(RDAddebiti.Item("Quantita"))
                ClasseRette.Descrizione = campodb(RDAddebiti.Item("DESCRIZIONE"))
                ClasseRette.CODICEEXTRA = CodiceAddebito
                ClasseRette.Importo = Importo
                ClasseRette.MESECOMPETENZA = 0
                ClasseRette.ANNOCOMPETENZA = 0
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If

            If campodb(RDAddebiti.Item("RIFERIMENTO")) = "P" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.CODICEPARENTE = campodbN(RDAddebiti.Item("PARENTE"))
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = Elemeto
                ClasseRette.Giorni = 0
                ClasseRette.CODICEEXTRA = CodiceAddebito
                ClasseRette.Importo = Importo
                ClasseRette.MESECOMPETENZA = 0
                ClasseRette.ANNOCOMPETENZA = 0
                If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
            End If


            If campodb(RDAddebiti.Item("RIFERIMENTO")) = "C" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = Elemeto
                ClasseRette.Provincia = campodb(RDAddebiti.Item("PROVINCIA"))
                ClasseRette.Comune = campodb(RDAddebiti.Item("COMUNE"))
                ClasseRette.Giorni = 0
                ClasseRette.CODICEEXTRA = CodiceAddebito
                ClasseRette.Importo = Importo
                ClasseRette.MESECOMPETENZA = 0
                ClasseRette.ANNOCOMPETENZA = 0
                If MScriviOspite = True Then ClasseRette.ScriviRettaComune()
            End If


            If campodb(RDAddebiti.Item("RIFERIMENTO")) = "J" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = Elemeto
                ClasseRette.Provincia = campodb(RDAddebiti.Item("PROVINCIA"))
                ClasseRette.Comune = campodb(RDAddebiti.Item("COMUNE"))
                ClasseRette.Giorni = 0
                ClasseRette.CODICEEXTRA = CodiceAddebito
                ClasseRette.Importo = Importo
                ClasseRette.MESECOMPETENZA = 0
                ClasseRette.ANNOCOMPETENZA = 0
                If MScriviOspite = True Then ClasseRette.ScriviRettaJolly()
            End If


            If campodb(RDAddebiti.Item("RIFERIMENTO")) = "R" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.Regione = campodb(RDAddebiti.Item("REGIONE"))
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = Elemeto
                ClasseRette.Giorni = 0
                ClasseRette.CODICEEXTRA = CodiceAddebito
                ClasseRette.Importo = Importo
                ClasseRette.MESECOMPETENZA = 0
                ClasseRette.ANNOCOMPETENZA = 0
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()
            End If

        Loop
        RDAddebiti.Close()


        OspitiCon.Close()

    End Sub
    Private Sub SaveTabellaRaggruppamentoQuote(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String, ByRef SC1 As Object)
        Dim Indice As Integer = 0
        Dim ClasseRette As New ClassRetteOspiti
        Dim MScriviOspite As Boolean = True
        Dim MScriviParente As Boolean = True
        Dim MScriviComune As Boolean = True
        Dim MScriviRegione As Boolean = True
        Dim OspitiCon As OleDbConnection

        OspitiCon = New Data.OleDb.OleDbConnection(ConnessioniOspiti)
        OspitiCon.Open()

        ClasseRette.ApriDB(ConnessioniOspiti)

        Dim cmdRettaOspite As New OleDbCommand()
        cmdRettaOspite.CommandText = "Select count(*) From RETTEOSPITE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaOspite.Connection = OspitiCon
        Dim RDRettaOspite As OleDbDataReader = cmdRettaOspite.ExecuteReader()
        If RDRettaOspite.Read() Then
            If campodb(RDRettaOspite.Item(0)) > 0 Then
                MScriviOspite = False
            End If
        End If
        RDRettaOspite.Close()



        Dim cmdRettaParenti As New OleDbCommand()
        cmdRettaParenti.CommandText = "Select count(*) From RETTEPARENTE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaParenti.Connection = OspitiCon
        Dim RDRettaParenti As OleDbDataReader = cmdRettaParenti.ExecuteReader()
        If RDRettaParenti.Read() Then
            If campodb(RDRettaParenti.Item(0)) > 0 Then
                MScriviParente = False
            End If
        End If
        RDRettaParenti.Close()


        Dim cmdRettaComune As New OleDbCommand()
        cmdRettaComune.CommandText = "Select count(*) From RETTECOMUNE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaComune.Connection = OspitiCon
        Dim RDRettaComune As OleDbDataReader = cmdRettaComune.ExecuteReader()
        If RDRettaComune.Read() Then
            If campodb(RDRettaComune.Item(0)) > 0 Then
                MScriviComune = False
            End If
        End If
        RDRettaParenti.Close()



        Dim cmdRettaRegione As New OleDbCommand()
        cmdRettaRegione.CommandText = "Select count(*) From RETTEREGIONE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaRegione.Connection = OspitiCon
        Dim RDRettaRegione As OleDbDataReader = cmdRettaRegione.ExecuteReader()
        If RDRettaRegione.Read() Then
            If campodb(RDRettaRegione.Item(0)) > 0 Then
                MScriviRegione = False
            End If
        End If
        RDRettaRegione.Close()


        For Indice = 0 To TabellaRaggruppamentoQuote.Count - 1
            If TabellaRaggruppamentoQuote(Indice).Pagante = "O" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.CODICEPARENTE = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.CODICEEXTRA = TabellaRaggruppamentoQuote(Indice).CodiceExtra
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = TabellaRaggruppamentoQuote(Indice).Elemento
                ClasseRette.Giorni = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Importo = Modulo.MathRound(TabellaRaggruppamentoQuote(Indice).Importo, 2)
                ClasseRette.MESECOMPETENZA = TabellaRaggruppamentoQuote(Indice).Mese
                ClasseRette.ANNOCOMPETENZA = TabellaRaggruppamentoQuote(Indice).Anno
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If
            If TabellaRaggruppamentoQuote(Indice).Pagante = "P" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.CODICEEXTRA = TabellaRaggruppamentoQuote(Indice).CodiceExtra
                ClasseRette.CODICEPARENTE = TabellaRaggruppamentoQuote(Indice).CodiceParente
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = TabellaRaggruppamentoQuote(Indice).Elemento
                ClasseRette.Giorni = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Importo = Modulo.MathRound(TabellaRaggruppamentoQuote(Indice).Importo, 2)
                ClasseRette.MESECOMPETENZA = TabellaRaggruppamentoQuote(Indice).Mese
                ClasseRette.ANNOCOMPETENZA = TabellaRaggruppamentoQuote(Indice).Anno
                If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
            End If
            If TabellaRaggruppamentoQuote(Indice).Pagante = "C" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"

                ClasseRette.Provincia = Mid(TabellaRaggruppamentoQuote(Indice).CodiceComune & Space(10), 1, 3)
                ClasseRette.Comune = Mid(TabellaRaggruppamentoQuote(Indice).CodiceComune & Space(10), 4, 3)

                ClasseRette.ELEMENTO = TabellaRaggruppamentoQuote(Indice).Elemento
                ClasseRette.Giorni = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Importo = Modulo.MathRound(TabellaRaggruppamentoQuote(Indice).Importo, 2)
                ClasseRette.MESECOMPETENZA = TabellaRaggruppamentoQuote(Indice).Mese
                ClasseRette.ANNOCOMPETENZA = TabellaRaggruppamentoQuote(Indice).Anno
                If MScriviOspite = True Then ClasseRette.ScriviRettaComune()
            End If

            If TabellaRaggruppamentoQuote(Indice).Pagante = "J" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"

                ClasseRette.Provincia = Mid(TabellaRaggruppamentoQuote(Indice).CodiceJolly & Space(10), 1, 3)
                ClasseRette.Comune = Mid(TabellaRaggruppamentoQuote(Indice).CodiceJolly & Space(10), 4, 3)

                ClasseRette.ELEMENTO = TabellaRaggruppamentoQuote(Indice).Elemento
                ClasseRette.Giorni = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Importo = Modulo.MathRound(TabellaRaggruppamentoQuote(Indice).Importo, 2)
                ClasseRette.MESECOMPETENZA = TabellaRaggruppamentoQuote(Indice).Mese
                ClasseRette.ANNOCOMPETENZA = TabellaRaggruppamentoQuote(Indice).Anno
                If MScriviOspite = True Then ClasseRette.ScriviRettaJolly()
            End If
            If TabellaRaggruppamentoQuote(Indice).Pagante = "R" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.Regione = TabellaRaggruppamentoQuote(Indice).CodiceRegione
                ClasseRette.ELEMENTO = TabellaRaggruppamentoQuote(Indice).Elemento
                ClasseRette.Giorni = TabellaRaggruppamentoQuote(Indice).Giorni
                ClasseRette.Importo = Modulo.MathRound(TabellaRaggruppamentoQuote(Indice).Importo, 2)
                ClasseRette.MESECOMPETENZA = TabellaRaggruppamentoQuote(Indice).Mese
                ClasseRette.ANNOCOMPETENZA = TabellaRaggruppamentoQuote(Indice).Anno
                ClasseRette.CODICEEXTRA =TabellaRaggruppamentoQuote(Indice).CodiceExtra
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()
            End If

        Next

        OspitiCon.Close()
        ClasseRette.ChiudiDB()

        Call ScriviAddebiti(Cserv, CodOsp, Mese, Anno, ConnessioniOspiti, MScriviOspite, MScriviParente, MScriviComune, MScriviRegione)
    End Sub

    Private Sub AllineamentoMensile(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String, ByRef SC1 As Object)
        For Indice = 0 To TabellaRaggruppamentoQuote.Count - 1
            If TabellaRaggruppamentoQuote(Indice).Pagante = "O" Then

            End If
        Next
    End Sub

    Public Sub GeneraAddebitiDomiciliare(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal Accoglimento As Date, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String)
        Dim Calcolo As New Cls_CalcoloRette
        Dim Sql As String
        Dim OspitiCon As OleDbConnection

        OspitiCon = New Data.OleDb.OleDbConnection(ConnessioniOspiti)
        OspitiCon.Open()
        Calcolo.ApriDB(ConnessioniOspiti, ConnessioniOspiti)
        Calcolo.STRINGACONNESSIONEDB = ConnessioniOspiti

        Dim VettoreCodice(50) As String
        Dim VettoreImporto(50) As Double
        Dim VettoreDescrizione(50) As String
        Dim VettoreTipo(50) As String
        Dim VettoreQuantita(50) As Double

        Dim MaxAddebito As Integer
        Dim OldConsideraIngresso As Integer

        Sql = "Select * From MovimentiDomiciliare Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data >= ? And Data <= ? Order by Data,OraInizio"
        Dim OldData As Date

        Dim cmd As New OleDbCommand()
        cmd.CommandText = Sql
        cmd.Parameters.AddWithValue("@Data", Accoglimento)
        cmd.Parameters.AddWithValue("@Data", UscitaDefinitiva)
        cmd.Connection = OspitiCon
        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()
        Do While ReaderAssenzeCentroDiurno.Read

            Dim Minuti As Integer
            Minuti = DateDiff("n", campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio")), campodbd(ReaderAssenzeCentroDiurno.Item("OraFine")))

            Dim PrimoIngresso As Boolean = True
            If OldData = campodbd(ReaderAssenzeCentroDiurno.Item("Data")) And OldConsideraIngresso = 1 Then
                PrimoIngresso = False
            End If

            Calcolo.CreaAddebitiDomicliari(campodb(ReaderAssenzeCentroDiurno.Item("Tipologia")), Minuti, VettoreTipo, VettoreCodice, VettoreDescrizione, VettoreImporto, MaxAddebito, campodbN(ReaderAssenzeCentroDiurno.Item("CreatoDocumentoOspiti")), Cserv, CodOsp, campodbd(ReaderAssenzeCentroDiurno.Item("Data")), Val(campodb(ReaderAssenzeCentroDiurno.Item("Operatore"))), PrimoIngresso, Val(campodb(ReaderAssenzeCentroDiurno.Item("ID"))), VettoreQuantita, 0)
            OldData = campodbd(ReaderAssenzeCentroDiurno.Item("Data"))

            Dim MP As New Cls_TipoDomiciliare

            MP.Codice = campodb(ReaderAssenzeCentroDiurno.Item("Tipologia"))
            MP.Leggi(ConnessioniOspiti, MP.Codice)
            OldConsideraIngresso = MP.ConsideraIngresso
        Loop
        ReaderAssenzeCentroDiurno.Close()


        Dim X As Double
        Dim Comune As String = ""
        Dim Jolly As String = ""
        Dim Regione As String = ""
        For X = 1 To MaxAddebito
            If VettoreTipo(X) = "C" Then
                Dim ComuneS As New Cls_ImportoComune

                ComuneS.CENTROSERVIZIO = Cserv
                ComuneS.CODICEOSPITE = CodOsp
                ComuneS.UltimaDataAData(ConnessioniOspiti, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO, DateSerial(Anno, Mese, 1))

                Comune = ComuneS.PROV & ComuneS.COMUNE

                If Comune = "" Then
                    ComuneS.CENTROSERVIZIO = Cserv
                    ComuneS.CODICEOSPITE = CodOsp
                    ComuneS.UltimaDataAData(ConnessioniOspiti, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

                    Comune = ComuneS.PROV & ComuneS.COMUNE

                End If
            End If
            If VettoreTipo(X) = "J" Then
                Dim ComuneS As New Cls_ImportoJolly

                ComuneS.CENTROSERVIZIO = Cserv
                ComuneS.CODICEOSPITE = CodOsp
                ComuneS.UltimaData(ConnessioniOspiti, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO)

                Jolly = ComuneS.PROV & ComuneS.COMUNE
            End If
            If VettoreTipo(X) = "R" Then
                Dim RegS As New Cls_StatoAuto

                RegS.CENTROSERVIZIO = Cserv
                RegS.CODICEOSPITE = CodOsp
                RegS.UltimaData(ConnessioniOspiti, RegS.CODICEOSPITE, RegS.CENTROSERVIZIO)

                Regione = RegS.USL
            End If
        Next

        For X = 1 To MaxAddebito

            If VettoreTipo(X) = "O" Or VettoreTipo(X) = "P" Then
                Dim RettaTotale As New Cls_rettatotale

                RettaTotale.CODICEOSPITE = CodOsp
                RettaTotale.CENTROSERVIZIO = Cserv
                RettaTotale.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                RettaTotale.RettaTotaleAData(ConnessioniOspiti, CodOsp, Cserv, RettaTotale.Data)

                If RettaTotale.TipoRetta <> "" Then
                    Dim TipoRetta As New Cls_TipoRetta

                    TipoRetta.Codice = RettaTotale.TipoRetta
                    TipoRetta.Leggi(ConnessioniOspiti, TipoRetta.Codice)

                    If TipoRetta.MassimaleDomiciliari > 0 Then

                        If TipoRetta.MassimaleDomiciliari < VettoreImporto(X) And (TipoRetta.TipoAddebitoMassimale = "" Or (TipoRetta.TipoAddebitoMassimale = VettoreCodice(X))) Then
                            VettoreImporto(X) = TipoRetta.MassimaleDomiciliari
                        End If
                    End If

                End If

            End If

            Dim AddM As New Cls_AddebitiAccrediti

            AddM.CENTROSERVIZIO = Cserv
            AddM.CodiceOspite = CodOsp
            AddM.chiaveselezione = X
            AddM.MESECOMPETENZA = Mese
            AddM.ANNOCOMPETENZA = Anno
            AddM.TipoMov = "AD"

            AddM.CodiceIva = VettoreCodice(X)
            AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
            AddM.Descrizione = VettoreDescrizione(X)
            AddM.IMPORTO = VettoreImporto(X)
            AddM.Quantita = VettoreQuantita(X)
            AddM.PARENTE = 0
            AddM.PROVINCIA = ""
            AddM.COMUNE = ""
            AddM.REGIONE = ""
            AddM.RIFERIMENTO = VettoreTipo(X)
            If AddM.RIFERIMENTO = "C" Then
                AddM.PROVINCIA = Mid(Comune & Space(6), 1, 3)
                AddM.COMUNE = Mid(Comune & Space(6), 4, 3)
            End If
            If AddM.RIFERIMENTO = "J" Then
                AddM.PROVINCIA = Mid(Jolly & Space(6), 1, 3)
                AddM.COMUNE = Mid(Jolly & Space(6), 4, 3)
            End If
            If AddM.RIFERIMENTO = "R" Then
                AddM.REGIONE = Mid(Regione & Space(6), 1, 4)
            End If
            AddM.AggiornaDB(ConnessioniOspiti)
        Next

        OspitiCon.Close()
        Calcolo.ChiudiDB()
    End Sub

    Public Sub GeneraPeriodo(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal UscitaDefinitiva As Date, ByVal ConnessioniOspiti As String, ByRef SC1 As Object)

        Dim Accoglimento As Date

        Dim Xs As New Cls_CalcoloRette
        Xs.STRINGACONNESSIONEDB = ConnessioniOspiti


        Dim DelAdb As New Cls_AddebitiAccrediti

        DelAdb.DeleteChiave(ConnessioniOspiti, CodOsp, Cserv, Mese, Anno)



        TabellaGiorni = MovimentiPeriodoOspite(Cserv, CodOsp, UscitaDefinitiva, ConnessioniOspiti)

        Dim TabCserv As New Cls_CentroServizio

        TabCserv.CENTROSERVIZIO = Cserv
        TabCserv.Leggi(ConnessioniOspiti, TabCserv.CENTROSERVIZIO)

  

        Accoglimento = TabellaGiorni(TabellaGiorni.Count - 1).Data

        ImportiPeriodo(Cserv, CodOsp, Accoglimento, UscitaDefinitiva, ConnessioniOspiti)


        Dim Indice As Integer

        GeneraAddebitiDomiciliare(Cserv, CodOsp, Mese, Anno, Accoglimento, UscitaDefinitiva, ConnessioniOspiti)



        For Indice = TabellaGiorni.Count - 1 To 0 Step -1

            LetturaDatiGiorni(Indice)

            Dim TotaleImporto1Parenti As Double = 0
            Dim IndiceP As Integer = 0

            For IndiceP = 0 To TabellaImportiGiorno.Parente.Count - 1

                TotaleImporto1Parenti = TotaleImporto1Parenti + TabellaImportiGiorno.Parente(IndiceP).ImportoRettaParente1

            Next



            If TabellaImportiGiorno.Modalita = "O" Then
                TabellaImportiGiorno.ImportoRettaOspite1 = TabellaImportiGiorno.ImportoRettaTotale - TabellaImportiGiorno.ImportoComune - TabellaImportiGiorno.ImportoRettaRegione - TabellaImportiGiorno.ImportoJolly - TotaleImporto1Parenti - TabellaImportiGiorno.ImportoJolly
            End If
            If TabellaImportiGiorno.Modalita = "C" Then
                TabellaImportiGiorno.ImportoComune = TabellaImportiGiorno.ImportoRettaTotale - TabellaImportiGiorno.ImportoRettaOspite1 - TabellaImportiGiorno.ImportoRettaOspite2 - TabellaImportiGiorno.ImportoRettaRegione - TabellaImportiGiorno.ImportoJolly - TotaleImporto1Parenti - TabellaImportiGiorno.ImportoJolly
            End If


            Dim ImportoOspite As Double = TabellaImportiGiorno.ImportoRettaOspite1
            Dim ImportoOspite2 As Double = TabellaImportiGiorno.ImportoRettaOspite2
            Dim ImportoComune As Double = TabellaImportiGiorno.ImportoComune
            Dim ImportoJolly As Double = TabellaImportiGiorno.ImportoJolly
            Dim CodiceComune As String = TabellaImportiGiorno.CodiceComune
            Dim CodiceRegione As String = TabellaImportiGiorno.CodiceRegione
            Dim CodiceJolly As String = TabellaImportiGiorno.CodiceJolly
            Dim TipoImportoRegione As String = TabellaImportiGiorno.TipoRettaRegione
            Dim ImportoRettaRegione As Double = TabellaImportiGiorno.ImportoRettaRegione
            Dim TabImportoParente As List(Of ImportiGiornoParente) = New List(Of ImportiGiornoParente)
            Dim TabExtraGiorno As List(Of String) = New List(Of String)


            TabImportoParente = TabellaImportiGiorno.Parente
            TabExtraGiorno = TabellaImportiGiorno.ExtraFissi


            Dim Presenza As Boolean = True

            If TabellaGiorni(Indice).Causale <> "" Then
                Presenza = False
                SC1.Reset()

                Dim Causale As New Cls_CausaliEntrataUscita


                Causale.Codice = TabellaGiorni(Indice).Causale
                Causale.LeggiCausale(ConnessioniOspiti)

                SC1.ExecuteStatement("Dim SalvaParente(10)")

                SC1.ExecuteStatement("SalvaComune = " & Replace(ImportoComune, ",", "."))
                SC1.ExecuteStatement("SalvaRegione = " & Replace(ImportoRettaRegione, ",", "."))
                SC1.ExecuteStatement("SalvaOspite = " & Replace(ImportoOspite, ",", "."))
                SC1.ExecuteStatement("SalvaOspite2 = " & Replace(ImportoOspite2, ",", "."))
                For IndiceP = 0 To TabellaImportiGiorno.Parente.Count - 1
                    SC1.ExecuteStatement("SalvaParente(" & IndiceP & ") = " & Replace(TabellaImportiGiorno.Parente(IndiceP).ImportoRettaParente1, ",", "."))
                Next


                SC1.AddCode(Causale.Regole)

                Try
                    If Not IsDBNull(Causale.Regole) And Causale.Regole <> "" Then SC1.Run("Calcolo")
                Catch ex As Exception
                    ' StringaDegliErrori = StringaDegliErrori & " Errore in Regola Causale " & MyTabCausale(I) & "-" & ex.Message
                End Try

                ImportoComune = SC1.Eval("SalvaComune")
                ImportoRettaRegione = SC1.Eval("SalvaRegione")
                ImportoOspite = SC1.Eval("SalvaOspite")
                ImportoOspite2 = SC1.Eval("SalvaOspite2")
                For IndiceP = 0 To TabellaImportiGiorno.Parente.Count - 1
                    ImportoOspite2 = SC1.Eval("SalvaOspite2")
                Next

                Dim PrimoGiorno As Boolean = True
                If Indice > 0 Then
                    If TabellaGiorni(Indice - 1).Causale <> TabellaGiorni(Indice).Causale Then
                        PrimoGiorno = False
                    End If
                End If



                If PrimoGiorno Then
                    If Causale.GiornoUscita = "P" Then
                        Presenza = True
                    End If
                Else
                    If Causale.TIPOMOVIMENTO = "P" Then
                        Presenza = True
                    End If
                End If
            End If


            Dim Trovato As Boolean = False
            Dim Elemento As String = ""
            Dim Elemento2 As String = ""
            If Presenza Then
                Elemento = "RGP"
                Elemento2 = "R2P"
            Else
                Elemento = "RGA"
                Elemento2 = "R2A"
            End If



            For Each CodiceExtra As String In TabExtraGiorno
                Trovato = False

                Dim ExtraFissi As New Cls_TipoExtraFisso
                Dim Parente As Integer = 0
                ExtraFissi.CODICEEXTRA = CodiceExtra
                ExtraFissi.Leggi(ConnessioniOspiti)

                If ExtraFissi.Ripartizione = "P" Then

                    For IndiceP = 0 To TabellaImportiGiorno.Parente.Count - 1

                        If TabellaImportiGiorno.Parente(IndiceP).ImportoRettaParente1 > 0 Then
                            Parente = TabellaImportiGiorno.Parente(IndiceP).CodiceParente
                        End If
                    Next
                End If


                REM Caricamento extra
                For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                    If item.Pagante = ExtraFissi.Ripartizione And item.Elemento = "E00" And item.CodiceParente = Parente Then
                        If item.CodiceExtra = CodiceExtra And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                            item.Importo = item.Importo + ExtraFissi.IMPORTO
                            item.Giorni = item.Giorni + 1
                            Trovato = True
                        End If
                    End If
                Next

                If Not Trovato And ExtraFissi.IMPORTO > 0 Then
                    TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = "E00", .CodiceExtra = CodiceExtra, .Giorni = 1, _
                                                   .Importo = ExtraFissi.IMPORTO, .ImportoUnitario = ExtraFissi.IMPORTO, _
                                                   .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                                   .Pagante = ExtraFissi.Ripartizione, .CodiceParente = Parente, .TipoOperazione = TabellaImportiGiorno.TipoOperazioneOspite})

                End If
            Next

            Trovato = False
            REM Caricamento Ospite
            For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                If item.Pagante = "O" And item.Elemento = Elemento And item.TipoOperazione = TabellaImportiGiorno.TipoOperazioneOspite Then
                    If item.ImportoUnitario = ImportoOspite And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                        item.Importo = item.Importo + ImportoOspite
                        item.Giorni = item.Giorni + 1
                        Trovato = True
                    End If
                End If
                If item.Pagante = "O" And item.Elemento = Elemento2 And item.TipoOperazione = TabellaImportiGiorno.TipoOperazioneOspite Then
                    If item.ImportoUnitario = ImportoOspite2 And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                        item.Importo = item.Importo + ImportoOspite2
                        item.Giorni = item.Giorni + 1
                        Trovato = True
                    End If
                End If
            Next

            If Not Trovato And ImportoOspite > 0 Then
                TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = Elemento, .CodiceExtra = "", .Giorni = 1, _
                                               .Importo = ImportoOspite, .ImportoUnitario = ImportoOspite, _
                                               .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                               .Pagante = "O", .TipoOperazione = TabellaImportiGiorno.TipoOperazioneOspite})

            End If

            If Not Trovato And ImportoOspite2 > 0 Then
                TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = Elemento2, .CodiceExtra = "", .Giorni = 1, _
                                               .Importo = ImportoOspite2, .ImportoUnitario = ImportoOspite2, _
                                               .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                               .Pagante = "O", .TipoOperazione = TabellaImportiGiorno.TipoOperazioneOspite})

            End If

            Trovato = False
            REM Caricamento Comune
            For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                If item.Pagante = "C" And item.Elemento = Elemento And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                    If item.ImportoUnitario = ImportoComune Then
                        item.Importo = item.Importo + ImportoComune
                        item.Giorni = item.Giorni + 1
                        Trovato = True
                    End If
                End If
            Next

            If Not Trovato And ImportoComune > 0 Then
                TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = Elemento, .CodiceExtra = "", .Giorni = 1, _
                                               .Importo = ImportoComune, .ImportoUnitario = ImportoComune, _
                                               .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                               .Pagante = "C", .CodiceComune = CodiceComune})

            End If


            Trovato = False
            REM Caricamento Jolly
            For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                If item.Pagante = "J" And item.Elemento = Elemento And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                    If item.ImportoUnitario = ImportoJolly Then
                        item.Importo = item.Importo + ImportoJolly
                        item.Giorni = item.Giorni + 1
                        Trovato = True
                    End If
                End If
            Next

            If Not Trovato And ImportoJolly > 0 Then
                TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = Elemento, .CodiceExtra = "", .Giorni = 1, _
                                               .Importo = ImportoJolly, .ImportoUnitario = ImportoJolly, _
                                               .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                               .Pagante = "J", .CodiceJolly = CodiceComune})

            End If



            Trovato = False
            REM Caricamento Regione
            For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                If  item.Pagante = "R" And item.Elemento = Elemento And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                    If item.ImportoUnitario = ImportoRettaRegione Then
                        item.Importo = item.Importo + ImportoRettaRegione
                        item.Giorni = item.Giorni + 1
                        Trovato = True
                    End If
                End If
            Next

            If Not Trovato And ImportoRettaRegione > 0 Then
                TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With { .Elemento = Elemento, .CodiceExtra = TipoImportoRegione, .Giorni = 1, _
                                               .Importo = ImportoRettaRegione, .ImportoUnitario = ImportoRettaRegione, _
                                               .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                               .Pagante = "R", .CodiceRegione = CodiceRegione})

            End If

            Trovato = False
            REM Caricamento Parente

            For Each Parente As ImportiGiornoParente In TabImportoParente
                For Each item As RaggruppamentoQuote In TabellaRaggruppamentoQuote
                    If item.Pagante = "P" And item.Elemento = Elemento And item.Mese = Month(TabellaGiorni(Indice).Data) And item.Anno = Year(TabellaGiorni(Indice).Data) Then
                        If item.ImportoUnitario = Parente.ImportoRettaParente1 And item.CodiceParente = Parente.CodiceParente Then
                            item.Importo = item.Importo + Parente.ImportoRettaParente1
                            item.Giorni = item.Giorni + 1
                            Trovato = True
                        End If
                    End If
                Next

                If Not Trovato And Parente.ImportoRettaParente1 > 0 Then
                    TabellaRaggruppamentoQuote.Add(New RaggruppamentoQuote With {.Elemento = Elemento, .CodiceExtra = "", .Giorni = 1, _
                                                   .Importo = Parente.ImportoRettaParente1, .ImportoUnitario = Parente.ImportoRettaParente1, _
                                                   .Mese = Month(TabellaGiorni(Indice).Data), .Anno = Year(TabellaGiorni(Indice).Data), _
                                                   .Pagante = "P", .CodiceParente = Parente.CodiceParente})

                End If
            Next
        Next


        SaveTabellaRaggruppamentoQuote(Cserv, CodOsp, Mese, Anno, UscitaDefinitiva, ConnessioniOspiti, SC1)


    End Sub

End Class
