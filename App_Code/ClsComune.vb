Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class ClsComune

    Public Provincia As String
    Public Comune As String
    Public Descrizione As String

    Public RESIDENZAINDIRIZZO1 As String
    Public Attenzione As String
    Public RESIDENZACAP1 As String
    Public PartitaIVA As Long
    Public CodiceFiscale As String
    Public RESIDENZAPROVINCIA1 As String
    Public RESIDENZACOMUNE1 As String
    Public CABCLIENTE As String
    Public ABICLIENTE As String
    Public CODXCODF As String

    Public CINCLIENTE As String
    Public CCBANCARIOCLIENTE As String
    Public BANCACLIENTE As String
    Public IntCliente As String
    Public NumeroControlloCliente As Long
    Public CodiceCig As String

    Public ImportoSconto As Double

    Public NomeConiuge As String
    Public ImportoAZero As Double
    Public SeparaInFattura As String

    Public Specializazione As String
    Public TipoOperazione As String
    Public ModalitaPagamento As String
    Public CodiceIva As String
    Public Compensazione As String
    Public CONTOPERESATTO As String

    Public PERIODO As String
    Public Vincolo As String


    Public MastroCliente As String
    Public ContoCliente As String
    Public SottocontoCliente As String

    Public CodiceDestinatario As String
    Public CodificaProvincia As String
    Public IdEpersonam As Long
    Public RotturaOspite As Integer

    Public CodiceCup As String
    Public IdDocumento As String
    Public RiferimentoAmministrazione As String
    Public NumeroFatturaDDT As Integer
    Public Note As String
    Public IdDocumentoData As String
    Public RotturaCserv As String
    Public EGo As String
    Public RESIDENZATELEFONO3 As String


    Public RecapitoNome As String
    Public RecapitoIndirizzo As String
    Public RESIDENZATELEFONO4 As String
    Public IvaSospesa As String
    Public FattAnticipata As String
    Public OspiteDestinatario As Integer
    Public OspiteIntestatario As Integer

    Public NumItem As String
    Public CodiceCommessaConvezione As String
    Public PEC As String
    Public Privato As Integer
    Public TIPOENTE As String
    Public RaggruppaInExport As Integer
    Public MeseFatturazione As Integer
    Public AnnoFatturazione As Integer
    Public Causale As String
    Public SoggettoABollo As String
    Public DescrizioneXML As String
    Public NonInUso As String
    Public RaggruppaInElaborazione As Integer
    Public RIFERIMENTOREGIONE As String



    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        ',[PROV],[COMUNE]

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from ImportoComune Where PROV = ?  And Comune = ?")
        cmd.Parameters.AddWithValue("@PROV", Provincia)
        cmd.Parameters.AddWithValue("@Comune", Comune)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Provincia) Or IsNothing(Comune) Then
            Exit Sub
        End If
        If Provincia = "" Or Comune = "" Then
            Exit Sub
        End If
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("delete from AnagraficaComune where " & _
                           "CodiceProvincia = ? And CodiceComune = ?")

        cmd.Parameters.AddWithValue("@CodiceProvincia", Provincia)
        cmd.Parameters.AddWithValue("@CodiceComune", Comune)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub DecodficaComune(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Provincia) Or IsNothing(Comune) Then
            Exit Sub
        End If
        If Provincia = "" Then
            Exit Sub
        End If
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Comune = "" Then
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                                       "CodiceProvincia = ? And (CodiceComune IS NULL OR CodiceComune = '')")

            cmd.Parameters.AddWithValue("@CodiceProvincia", Provincia)            
        Else
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                               "CodiceProvincia = ? And CodiceComune = ?")

            cmd.Parameters.AddWithValue("@CodiceProvincia", Provincia)
            cmd.Parameters.AddWithValue("@CodiceComune", Comune)
        End If

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("NOME"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodb0(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function MaxComune(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection

        MaxComune = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CAST(codicecomune AS Int)) as Max from AnagraficaComune Where TIPOLOGIA = 'C' And CodiceProvincia = '888'")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxComune = Val(campodb(myPOSTreader.Item("Max"))) + 1
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function


    Sub LeggiCodiceXCOFIS(ByVal StringaConnessione As String, ByVal CodFisc As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'C' And CODXCODF = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodFisc", campodb(CodFisc))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Provincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            Comune = campodb(myPOSTreader.Item("CodiceComune"))
            If Provincia <> "" And Comune <> "" Then
                Leggi(StringaConnessione)
            End If
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)

        If campodb(Comune) = "" And campodb(Provincia) = "" Then
            Exit Sub
        End If
        If campodb(Provincia) = "" Then
            Exit Sub
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If campodb(Comune) = "" Then
            cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'C' And CodiceProvincia = ? And (CodiceComune IS NULL OR CodiceComune   ='')")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Provincia", campodb(Provincia))
        Else
            cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'C' And CodiceProvincia = ? And CodiceComune = ?")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Provincia", campodb(Provincia))
            cmd.Parameters.AddWithValue("@Comune", campodb(Comune))
        End If

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Provincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            Comune = campodb(myPOSTreader.Item("CodiceComune"))
            Descrizione = campodb(myPOSTreader.Item("Nome"))
            NomeConiuge = campodb(myPOSTreader.Item("NomeConiuge")) ' NomeConiuge
            REM Tipologia = campodb(myPOSTreader.Item("Tipologia"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BANCACLIENTE = campodb(myPOSTreader.Item("BancaCliente"))


            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            Attenzione = campodb(myPOSTreader.Item("Attenzione"))
            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            PartitaIVA = myPOSTreader.Item("PARTITAIVA")
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))


            CODXCODF = campodb(myPOSTreader.Item("CODXCODF"))
            CodiceCig = campodb(myPOSTreader.Item("CodiceCig"))
            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb0(myPOSTreader.Item("NumeroControlloCliente"))

            ImportoAZero = campodb0(myPOSTreader.Item("ImportoAZero"))
            ImportoSconto = campodb0(myPOSTreader.Item("ImportoSconto"))
            PERIODO = campodb(myPOSTreader.Item("Periodo"))
            Specializazione = campodb(myPOSTreader.Item("Specializazione"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))

            SeparaInFattura = campodb0(myPOSTreader.Item("SeparaInFattura"))
            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            MastroCliente = campodb0(myPOSTreader.Item("MastroCliente"))
            ContoCliente = campodb0(myPOSTreader.Item("ContoCliente"))
            SottocontoCliente = campodb0(myPOSTreader.Item("SottoContoCliente"))

            Vincolo = campodb(myPOSTreader.Item("Vincolo"))
            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            CodificaProvincia = campodb(myPOSTreader.Item("CodificaProvincia"))
            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))

            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))
            IdDocumento = campodb(myPOSTreader.Item("IdDocumento"))
            RiferimentoAmministrazione = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            NumeroFatturaDDT = campodb0(myPOSTreader.Item("NumeroFatturaDDT"))
            Note = campodb(myPOSTreader.Item("Note"))
            IdDocumentoData = campodb(myPOSTreader.Item("IdDocumentoData"))

            RotturaCserv = campodb(myPOSTreader.Item("RotturaCserv"))
            RESIDENZATELEFONO3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))


            RecapitoNome = campodb(myPOSTreader.Item("RecapitoNome"))
            RecapitoIndirizzo = campodb(myPOSTreader.Item("RecapitoIndirizzo"))
            RESIDENZATELEFONO4 = campodb(myPOSTreader.Item("RESIDENZATELEFONO4"))

            IvaSospesa = campodb(myPOSTreader.Item("IvaSospesa"))


            FattAnticipata = campodb(myPOSTreader.Item("FattAnticipata"))

            EGo = campodb(myPOSTreader.Item("EGo"))

            OspiteDestinatario = Val(campodb(myPOSTreader.Item("OspiteDestinatario")))
            OspiteIntestatario = Val(campodb(myPOSTreader.Item("OspiteIntestatario")))


            NumItem = campodb(myPOSTreader.Item("NumItem"))
            CodiceCommessaConvezione = campodb(myPOSTreader.Item("CodiceCommessaConvezione"))

            PEC = campodb(myPOSTreader.Item("PEC"))

            Privato = Val(campodb(myPOSTreader.Item("Privato")))

            RaggruppaInExport = Val(campodb(myPOSTreader.Item("RaggruppaInExport")))

            TIPOENTE = campodb(myPOSTreader.Item("TIPOENTE"))
            Causale = campodb(myPOSTreader.Item("Causale"))

            AnnoFatturazione = Val(campodb(myPOSTreader.Item("AnnoFatturazione")))
            MeseFatturazione = Val(campodb(myPOSTreader.Item("MeseFatturazione")))


            SoggettoABollo = campodb(myPOSTreader.Item("SoggettoABollo"))

            DescrizioneXML = campodb(myPOSTreader.Item("DescrizioneXML"))


            NonInUso = campodb(myPOSTreader.Item("NonInUso"))

            RaggruppaInElaborazione = Val(campodb(myPOSTreader.Item("RaggruppaInElaborazione")))

            RIFERIMENTOREGIONE = campodb(myPOSTreader.Item("RIFERIMENTOREGIONE"))
        End If
        cn.Close()


    End Sub

    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'C' And Nome = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Nome", Descrizione)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Provincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            Comune = campodb(myPOSTreader.Item("CodiceComune"))
            Descrizione = campodb(myPOSTreader.Item("Nome"))
            NomeConiuge = campodb(myPOSTreader.Item("NomeConiuge")) ' NomeConiuge
            REM Tipologia = campodb(myPOSTreader.Item("Tipologia"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BANCACLIENTE = campodb(myPOSTreader.Item("BancaCliente"))


            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            Attenzione = campodb(myPOSTreader.Item("Attenzione"))
            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            PartitaIVA = myPOSTreader.Item("PARTITAIVA")
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))


            CODXCODF = campodb(myPOSTreader.Item("CODXCODF"))
            CodiceCig = campodb(myPOSTreader.Item("CodiceCig"))
            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb0(myPOSTreader.Item("NumeroControlloCliente"))

            ImportoAZero = campodb0(myPOSTreader.Item("ImportoAZero"))
            ImportoSconto = campodb0(myPOSTreader.Item("ImportoSconto"))
            PERIODO = campodb(myPOSTreader.Item("Periodo"))
            Specializazione = campodb(myPOSTreader.Item("Specializazione"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))

            SeparaInFattura = campodb0(myPOSTreader.Item("SeparaInFattura"))
            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            MastroCliente = campodb0(myPOSTreader.Item("MastroCliente"))
            ContoCliente = campodb0(myPOSTreader.Item("ContoCliente"))
            SottocontoCliente = campodb0(myPOSTreader.Item("SottoContoCliente"))

            Vincolo = campodb(myPOSTreader.Item("Vincolo"))

            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            CodificaProvincia = campodb(myPOSTreader.Item("CodificaProvincia"))

            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))

            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))
            IdDocumento = campodb(myPOSTreader.Item("IdDocumento"))
            RiferimentoAmministrazione = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            NumeroFatturaDDT = campodb0(myPOSTreader.Item("NumeroFatturaDDT"))
            Note = campodb(myPOSTreader.Item("Note"))
            IdDocumentoData = campodb(myPOSTreader.Item("IdDocumentoData"))
            RESIDENZATELEFONO3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))

            RotturaCserv = campodb(myPOSTreader.Item("RotturaCserv"))
            IvaSospesa = campodb(myPOSTreader.Item("IvaSospesa"))

            FattAnticipata = campodb(myPOSTreader.Item("FattAnticipata"))
            EGo = campodb(myPOSTreader.Item("EGo"))
            OspiteDestinatario = Val(campodb(myPOSTreader.Item("OspiteDestinatario")))
            OspiteIntestatario = Val(campodb(myPOSTreader.Item("OspiteIntestatario")))


            NumItem = campodb(myPOSTreader.Item("NumItem"))
            CodiceCommessaConvezione = campodb(myPOSTreader.Item("CodiceCommessaConvezione"))
            PEC = campodb(myPOSTreader.Item("PEC"))

            Privato = Val(campodb(myPOSTreader.Item("Privato")))

            TIPOENTE = campodb(myPOSTreader.Item("TIPOENTE"))

            RaggruppaInExport = Val(campodb(myPOSTreader.Item("RaggruppaInExport")))


            AnnoFatturazione = Val(campodb(myPOSTreader.Item("AnnoFatturazione")))
            MeseFatturazione = Val(campodb(myPOSTreader.Item("MeseFatturazione")))
            Causale = campodb(myPOSTreader.Item("Causale"))

            SoggettoABollo = campodb(myPOSTreader.Item("SoggettoABollo"))

            DescrizioneXML = campodb(myPOSTreader.Item("DescrizioneXML"))

            NonInUso = campodb(myPOSTreader.Item("NonInUso"))

            RaggruppaInElaborazione = Val(campodb(myPOSTreader.Item("RaggruppaInElaborazione")))

            RIFERIMENTOREGIONE = campodb(myPOSTreader.Item("RIFERIMENTOREGIONE"))
        End If
        cn.Close()


    End Sub

    Sub ScriviComune(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        If Provincia = "" Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'C' And CodiceProvincia = ? And CodiceComune = ?")
        cmd1.Connection = cn
        cmd1.Parameters.AddWithValue("@Provincia", Provincia)
        cmd1.Parameters.AddWithValue("@Comune", Comune)

        Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE AnagraficaComune SET " & _
                    "Nome = ?," & _
                    "RESIDENZAINDIRIZZO1 = ?," & _
                    "Attenzione = ?," & _
                    "RESIDENZACAP1 = ?," & _
                    "PartitaIVA = ?," & _
                    "CodiceFiscale = ?," & _
                    "RESIDENZAPROVINCIA1 = ?," & _
                    "RESIDENZACOMUNE1 = ?," & _
                    "CABCLIENTE = ?," & _
                    "ABICLIENTE = ?," & _
                    "CODXCODF = ?," & _
                    "CINCLIENTE = ?," & _
                    "CCBANCARIOCLIENTE = ?, " & _
                    "BANCACLIENTE = ?, " & _
                    "IntCliente = ?, " & _
                    "NumeroControlloCliente = ?, " & _
                    "CodiceCig = ?, " & _
                    "ImportoSconto = ?, " & _
                    "NomeConiuge = ?, " & _
                    "ImportoAZero = ?, " & _
                    "SeparaInFattura = ?, " & _
                    "Specializazione = ?, " & _
                    "TipoOperazione = ?, " & _
                    "ModalitaPagamento = ?, " & _
                    "CodiceIva = ?, " & _
                    "Compensazione = ?, " & _
                    "CONTOPERESATTO = ?, " & _
                    "PERIODO = ?, " & _
                    "Vincolo = ?, " & _
                    "MastroCliente = ?," & _
                    "ContoCliente = ?, " & _
                    "SottocontoCliente = ?, " & _
                    "CodiceDestinatario = ?, " & _
                    "CodificaProvincia = ?, " & _
                    "RotturaOspite = ?, " & _
                    "CodiceCup = ?," & _
                    "IdDocumento = ?," & _
                    "RiferimentoAmministrazione = ?," & _
                    "NumeroFatturaDDT = ?, " & _
                    "Note = ?, " & _
                    "IdDocumentoData  = ?, " & _
                    "RotturaCserv = ?," & _
                    "RESIDENZATELEFONO3 = ?, " & _
                    "RecapitoNome = ?,  " & _
                    "RecapitoIndirizzo = ?,  " & _
                    "RESIDENZATELEFONO4 = ?,  " & _
                    "IvaSospesa = ?, " & _
                    "FattAnticipata = ?, " & _
                    "Ego = ?, " & _
                    "OspiteDestinatario = ?, " & _
                    "OspiteIntestatario = ?, " & _
                    "NumItem = ?,  " & _
                    "CodiceCommessaConvezione = ?,  " & _
                    "PEC = ?,  " & _
                    "Privato = ?, " & _
                    "TIPOENTE = ?, " & _
                    "RaggruppaInExport  = ?, " & _
                    "AnnoFatturazione = ?, " & _
                    "MeseFatturazione = ?, " & _
                    "Causale = ?, " & _
                    "SoggettoABollo = ?, " & _
                    "DescrizioneXML = ?, " & _
                    "NonInUso = ?, " & _
                    "RaggruppaInElaborazione = ?, " & _
                    "RIFERIMENTOREGIONE= ? " & _
                    "WHERE CodiceProvincia = ? and CodiceComune = ? "
        Else
            MySql = "INSERT INTO AnagraficaComune " & _
                    "(Nome, " & _
                    "RESIDENZAINDIRIZZO1, " & _
                    "Attenzione, " & _
                    "RESIDENZACAP1, " & _
                    "PartitaIVA, " & _
                    "CodiceFiscale, " & _
                    "RESIDENZAPROVINCIA1, " & _
                    "RESIDENZACOMUNE1, " & _
                    "CABCLIENTE, " & _
                    "ABICLIENTE, " & _
                    "CODXCODF, " & _
                    "CINCLIENTE, " & _
                    "CCBANCARIOCLIENTE, " & _
                    "BANCACLIENTE, " & _
                    "IntCliente, " & _
                    "NumeroControlloCliente, " & _
                    "CodiceCig, " & _
                    "ImportoSconto, " & _
                    "NomeConiuge, " & _
                    "ImportoAZero, " & _
                    "SeparaInFattura, " & _
                    "Specializazione, " & _
                    "TipoOperazione, " & _
                    "ModalitaPagamento, " & _
                    "CodiceIva, " & _
                    "Compensazione, " & _
                    "CONTOPERESATTO, " & _
                    "PERIODO, " & _
                    "Vincolo, " & _
                    "MastroCliente, " & _
                    "ContoCliente, " & _
                    "SottocontoCliente, " & _
                    "CodiceDestinatario, " & _
                    "CodificaProvincia," & _
                    "RotturaOspite, " & _
                    "CodiceCup," & _
                    "IdDocumento," & _
                    "RiferimentoAmministrazione," & _
                    "NumeroFatturaDDT," & _
                    "Note, " & _
                    "IdDocumentoData, " & _
                    "RotturaCserv," & _
                    "RESIDENZATELEFONO3, " & _
                    "RecapitoNome,  " & _
                    "RecapitoIndirizzo,  " & _
                    "RESIDENZATELEFONO4," & _
                    "IvaSospesa," & _
                    "FattAnticipata," & _
                    "Ego," & _
                    "OspiteDestinatario," & _
                    "OspiteIntestatario," & _
                    "NumItem," & _
                    "CodiceCommessaConvezione," & _
                    "PEC," & _
                    "Privato, " & _
                    "TIPOENTE," & _
                    "RaggruppaInExport," & _
                    "AnnoFatturazione," & _
                    "MeseFatturazione," & _
                    "Causale, " & _
                    "SoggettoABollo, " & _
                    "DescrizioneXML, " & _
                     "NonInUso," & _
                     "RaggruppaInElaborazione," & _
                     "RIFERIMENTOREGIONE," & _
                    "CodiceProvincia, " & _
                    "CodiceComune,TIPOLOGIA) VALUES (" & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?, " & _
                    "?,'C')"

        End If
        myPOSTreader.Close()
        Dim cmd As New OleDbCommand()


        cmd.Connection = cn
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@Nome", Descrizione)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@Attenzione", Attenzione)
        cmd.Parameters.AddWithValue("@RESIDENZACAP1", RESIDENZACAP1)
        cmd.Parameters.AddWithValue("@PartitaIVA", PartitaIVA)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", RESIDENZAPROVINCIA1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", RESIDENZACOMUNE1)
        cmd.Parameters.AddWithValue("@CABCLIENTE", CABCLIENTE)
        cmd.Parameters.AddWithValue("@ABICLIENTE", ABICLIENTE)
        cmd.Parameters.AddWithValue("@CODXCODF", CODXCODF)
        cmd.Parameters.AddWithValue("@CINCLIENTE", CINCLIENTE)
        cmd.Parameters.AddWithValue("@CCBANCARIOCLIENTE", CCBANCARIOCLIENTE)
        cmd.Parameters.AddWithValue("@BANCACLIENTE", BANCACLIENTE)
        cmd.Parameters.AddWithValue("@IntCliente", IntCliente)
        cmd.Parameters.AddWithValue("@NumeroControlloCliente", NumeroControlloCliente)
        cmd.Parameters.AddWithValue("@CodiceCig", CodiceCig)
        cmd.Parameters.AddWithValue("@ImportoSconto", ImportoSconto)
        cmd.Parameters.AddWithValue("@NomeConiuge", NomeConiuge)
        cmd.Parameters.AddWithValue("@ImportoAZero", ImportoAZero)
        cmd.Parameters.AddWithValue("@SeparaInFattura", SeparaInFattura)
        cmd.Parameters.AddWithValue("@Specializazione", Specializazione)
        cmd.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
        cmd.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd.Parameters.AddWithValue("@CodiceIva", CodiceIva)
        cmd.Parameters.AddWithValue("@Compensazione", Compensazione)
        cmd.Parameters.AddWithValue("@CONTOPERESATTO", CONTOPERESATTO)
        cmd.Parameters.AddWithValue("@PERIODO", PERIODO)
        cmd.Parameters.AddWithValue("@Vincolo", Vincolo)
        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottocontoCliente", SottocontoCliente)

        cmd.Parameters.AddWithValue("@CodiceDestinatario", CodiceDestinatario)
        cmd.Parameters.AddWithValue("@CodificaProvincia", CodificaProvincia)
        cmd.Parameters.AddWithValue("@RotturaOspite", RotturaOspite)

        cmd.Parameters.AddWithValue("@CodiceCup", CodiceCup)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@RiferimentoAmministrazione", RiferimentoAmministrazione)
        cmd.Parameters.AddWithValue("@NumeroFatturaDDT", NumeroFatturaDDT)
        cmd.Parameters.AddWithValue("@Note", Note)
        cmd.Parameters.AddWithValue("@IdDocumentoData", IdDocumentoData)
        cmd.Parameters.AddWithValue("@RotturaCserv", RotturaCserv)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO3", RESIDENZATELEFONO3)
        cmd.Parameters.AddWithValue("@RecapitoNome", RecapitoNome)
        cmd.Parameters.AddWithValue("@RecapitoIndirizzo", RecapitoIndirizzo)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO4", RESIDENZATELEFONO4)
        cmd.Parameters.AddWithValue("@IvaSospesa", IvaSospesa)
        cmd.Parameters.AddWithValue("@FattAnticipata", FattAnticipata)
        cmd.Parameters.AddWithValue("@Ego", EGo)
        cmd.Parameters.AddWithValue("@OspiteDestinatario", OspiteDestinatario)
        cmd.Parameters.AddWithValue("@OspiteIntestatario", OspiteIntestatario)


        cmd.Parameters.AddWithValue("@NumItem", NumItem)
        cmd.Parameters.AddWithValue("@CodiceCommessaConvezione", CodiceCommessaConvezione)
        cmd.Parameters.AddWithValue("@PEC", PEC)
        cmd.Parameters.AddWithValue("@Privato", Privato)
        cmd.Parameters.AddWithValue("@TIPOENTE", TIPOENTE)
        cmd.Parameters.AddWithValue("@RaggruppaInExport", RaggruppaInExport)

        cmd.Parameters.AddWithValue("@AnnoFatturazione", AnnoFatturazione)
        cmd.Parameters.AddWithValue("@MeseFatturazione", MeseFatturazione)

        cmd.Parameters.AddWithValue("@Causale", Causale)
        cmd.Parameters.AddWithValue("@SoggettoABollo", SoggettoABollo)

        cmd.Parameters.AddWithValue("@DescrizioneXML", DescrizioneXML)

        cmd.Parameters.AddWithValue("@NonInUso", NonInUso)

        cmd.Parameters.AddWithValue("@RaggruppaInElaborazione", RaggruppaInElaborazione)

        cmd.Parameters.AddWithValue("@RIFERIMENTOREGIONE", RIFERIMENTOREGIONE)

        'RIFERIMENTOREGIONE

        'RaggruppaInExport
        cmd.Parameters.AddWithValue("@Provincia", Provincia)
        cmd.Parameters.AddWithValue("@Comune", Comune)
        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select CodiceProvincia,CodiceComune,Nome,NomeConiuge from AnagraficaComune Where  TIPOLOGIA = 'C' And Not TipoOperazione IS Null And TipoOperazione <> '' And (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')  Order By Nome,NomeConiuge")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim AppoggioCom As String

            AppoggioCom = campodb(myPOSTreader.Item("NomeConiuge"))
            If Trim(AppoggioCom) <> "" Then
                AppoggioCom = " (" & AppoggioCom & ")"
            End If

            appoggio.Items.Add(myPOSTreader.Item("Nome") & AppoggioCom)
            appoggio.Items(appoggio.Items.Count - 1).Value = campodb(myPOSTreader.Item("CodiceProvincia")) & " " & campodb(myPOSTreader.Item("CodiceComune"))
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal CODICEREGIONE As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where RIFERIMENTOREGIONE = '" & CODICEREGIONE & "' And Tipologia = 'C' ORDER BY Nome")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Nome", GetType(String))
        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("CodiceProvincia")) & " " & campodb(myPOSTreader.Item("CodiceComune")) & " " & campodb(myPOSTreader.Item("Nome"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = ""

            Tabella.Rows.Add(myriga)
        End If

        cn.Close()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("update AnagraficaComune set RIFERIMENTOREGIONE = '' where RIFERIMENTOREGIONE = '" & RIFERIMENTOREGIONE & "' And Tipologia = 'C'")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If campodb(Tabella.Rows(i).Item(0)) <> "" Then
                Dim Comune As New ClsComune
                Dim Vettore(100) As String

                Vettore = SplitWords(campodb(Tabella.Rows(i).Item(0)))

                Comune.Provincia = Vettore(0)
                Comune.Comune = Vettore(1)
                Comune.Leggi(StringaConnessione)

                Comune.RIFERIMENTOREGIONE = RIFERIMENTOREGIONE
                Comune.ScriviComune(StringaConnessione)

            End If

        Next
        cn.Close()

    End Sub

End Class

