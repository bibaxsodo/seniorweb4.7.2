﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RegoleBudget
    Public id As Long
    Public Anno As Long
    Public Colonna As Long
    Public Livello1 As Long
    Public Livello2 As Long
    Public Livello3 As Long
    Public Importo As Double
    Public MastroPianoDeiConti As Long
    Public ContoPianoDeiConti As Long
    Public SottoContoPianoDeiConti As Long
    Public CentroServizio As String

    Public ImportoFisso As Double
    Public Percentuale As Double



    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from RegoleBudget where " & _
                             "id  = ?")

        cmd.Parameters.AddWithValue("@id", id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Sub Decodfica(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from RegoleBudget where " & _
                             "id  = ?")

        cmd.Parameters.AddWithValue("@id", id)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Anno = campodb(myPOSTreader.Item("Anno"))
            Colonna = campodb(myPOSTreader.Item("Colonna"))
            Livello1 = campodb(myPOSTreader.Item("Livello1"))
            Livello2 = campodb(myPOSTreader.Item("Livello2"))
            Livello3 = campodb(myPOSTreader.Item("Livello3"))
            MastroPianoDeiConti = campodb(myPOSTreader.Item("MastroPianoDeiConti"))
            ContoPianoDeiConti = campodb(myPOSTreader.Item("ContoPianoDeiConti"))
            SottoContoPianoDeiConti = campodb(myPOSTreader.Item("SottoContoPianoDeiConti"))

            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            ImportoFisso = campodbN(myPOSTreader.Item("ImportoFisso"))

            Percentuale = campodbN(myPOSTreader.Item("Percentuale"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from RegoleBudget where " & _
                                     "id  = ?")

        cmd.Parameters.AddWithValue("@id", id)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim cmdIns As New OleDbCommand()
            MySql = "INSERT INTO RegoleBudget (Anno,Livello1,Livello2,Livello3,MastroPianoDeiConti,ContoPianoDeiConti,SottoContoPianoDeiConti,Colonna,ImportoFisso,Percentuale,CentroServizio) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            cmdIns.CommandText = MySql
            cmdIns.Parameters.AddWithValue("@Anno", Anno)
            cmdIns.Parameters.AddWithValue("@Livello1", Livello1)
            cmdIns.Parameters.AddWithValue("@Livello2", Livello2)
            cmdIns.Parameters.AddWithValue("@Livello3", Livello3)

            cmdIns.Parameters.AddWithValue("@MastroPianoDeiConti", MastroPianoDeiConti)
            cmdIns.Parameters.AddWithValue("@ContoPianoDeiConti", ContoPianoDeiConti)
            cmdIns.Parameters.AddWithValue("@SottoContoPianoDeiConti", SottoContoPianoDeiConti)
            cmdIns.Parameters.AddWithValue("@Colonna", Colonna)
            cmdIns.Parameters.AddWithValue("@ImportoFisso", ImportoFisso)
            cmdIns.Parameters.AddWithValue("@Percentuale", Percentuale)
            cmdIns.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        Else

            MySql = "UPDATE RegoleBudget  SET Anno = ?,Livello1 = ?,Livello2 = ?,Livello3 = ?,MastroPianoDeiConti = ?,ContoPianoDeiConti = ?,SottoContoPianoDeiConti = ?,Colonna = ?,ImportoFisso = ?,Percentuale = ?,CentroServizio= ? " & _
                    " Where ID = " & id
            Dim cmd1 As New OleDbCommand()
            cmd1.Parameters.AddWithValue("@Anno", Anno)
            cmd1.Parameters.AddWithValue("@Livello1", Livello1)
            cmd1.Parameters.AddWithValue("@Livello2", Livello2)
            cmd1.Parameters.AddWithValue("@Livello3", Livello3)

            cmd1.Parameters.AddWithValue("@MastroPianoDeiConti", MastroPianoDeiConti)
            cmd1.Parameters.AddWithValue("@ContoPianoDeiConti", ContoPianoDeiConti)
            cmd1.Parameters.AddWithValue("@SottoContoPianoDeiConti", SottoContoPianoDeiConti)
            cmd1.Parameters.AddWithValue("@Colonna", Colonna)
            cmd1.Parameters.AddWithValue("@ImportoFisso", ImportoFisso)
            cmd1.Parameters.AddWithValue("@Percentuale", Percentuale)
            cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmd1.CommandText = MySql
            cmd1.Connection = cn
            cmd1.ExecuteNonQuery()
        End If

        cn.Close()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Function ApplicaRegola(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Integer) As Boolean
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim CommandVerificaRegistrazione As New OleDbCommand("Select *  From LegameBudgetRegistrazione  Where NumeroRegistrazione = " & NumeroRegistrazione, cn)
        Dim ReadVerifica As OleDbDataReader = CommandVerificaRegistrazione.ExecuteReader()
        If ReadVerifica.Read Then

            ReadVerifica.Close()
            cn.Close()
            Return False
            Exit Function
        End If

        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.NumeroRegistrazione = NumeroRegistrazione
        Registrazione.Leggi(StringaConnessione, Registrazione.NumeroRegistrazione, False)


        Dim Inserito As Boolean = False

        Dim CommandLegato As New OleDbCommand("Select * From RegoleBudget Where Anno = ? ", cn)

        CommandLegato.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandLegato.Parameters("@Anno").Value = Year(Registrazione.DataRegistrazione)

        Dim myLegato As OleDbDataReader = CommandLegato.ExecuteReader()

        Do While myLegato.Read()
            Dim Somma As Double = 0
            Dim Tpc As New Cls_TipoBudget

            Tpc.Anno = campodbN(myLegato.Item("Anno"))
            Tpc.Livello1 = campodbN(myLegato.Item("Livello1"))
            Tpc.Livello2 = campodbN(myLegato.Item("Livello2"))
            Tpc.Livello3 = campodbN(myLegato.Item("Livello3"))
            Tpc.Decodfica(StringaConnessione)
            If Tpc.Tipo = "U" Then
                MySql = "SELECT CASE WHEN DareAvere = 'D' THEN Importo ELSE Importo *-1 END,RigaRegistrazione FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                        " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione WHERE " & _
                        " NumeroRegistrazione = ? " & _
                        " And MastroPartita = ?"
            Else
                MySql = "SELECT CASE WHEN DareAvere = 'D' THEN Importo * -1 ELSE Importo  END,RigaRegistrazione FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                        " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione WHERE " & _
                        " NumeroRegistrazione = ? " & _
                        " And MastroPartita = ?"
            End If
            If campodbN(myLegato.Item("ContoPianoDeiConti")) <> 0 Then
                MySql = MySql & " And ContoPartita = " & campodbN(myLegato.Item("ContoPianoDeiConti"))
            End If
            If campodbN(myLegato.Item("SottoContoPianoDeiConti")) <> 0 Then
                MySql = MySql & " And SottoContoPartita  = " & campodbN(myLegato.Item("SottoContoPianoDeiConti"))
            End If

            Dim CommandBdReg As New OleDbCommand(MySql, cn)
            Dim Importob As Double = 0

            CommandBdReg.Parameters.Add("@NumeroRegistrazione", OleDbType.Integer, 8)
            CommandBdReg.Parameters("@NumeroRegistrazione").Value = NumeroRegistrazione

          
            CommandBdReg.Parameters.Add("@MASTROPARTITA", OleDbType.Integer, 8)
            CommandBdReg.Parameters("@MASTROPARTITA").Value = campodbN(myLegato.Item("MastroPianoDeiConti"))

            Dim myBdReg As OleDbDataReader = CommandBdReg.ExecuteReader()
            Do While myBdReg.Read
                If campodbN(myLegato.Item("ImportoFisso")) > 0 Then
                    Somma = campodbN(myLegato.Item("ImportoFisso"))
                Else
                    Importob = campodbN(myBdReg.Item(0))
                    Somma = Math.Round(Importob * campodbN(myLegato.Item("Percentuale")) / 100, 2)
                End If

                If Somma <> 0 Then

                    MySql = "INSERT INTO LegameBudgetRegistrazione (Anno,Livello1,Livello2,Livello3,Colonna,Importo,NumeroRegistrazione,RigaRegistrazione,AnnoCompetenza,MeseCompetenza,Automatico) VALUES (?,?,?,?,?,?,?,?,?,?,?)"

                    Dim cmdUs As New OleDbCommand()
                    cmdUs.CommandText = MySql
                    cmdUs.Connection = cn

                    cmdUs.Parameters.AddWithValue("@Anno", campodbN(myLegato.Item("Anno")))
                    cmdUs.Parameters.AddWithValue("@Livello1", Tpc.Livello1)
                    cmdUs.Parameters.AddWithValue("@Livello2", Tpc.Livello2)
                    cmdUs.Parameters.AddWithValue("@Livello3", Tpc.Livello3)
                    cmdUs.Parameters.AddWithValue("@Colonna", campodbN(myLegato.Item("Colonna")))
                    cmdUs.Parameters.AddWithValue("@Importo", Somma)
                    cmdUs.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
                    cmdUs.Parameters.AddWithValue("@RigaRegistrazione", campodbN(myBdReg.Item("RigaRegistrazione")))
                    cmdUs.Parameters.AddWithValue("@AnnoCompetenza", campodbN(myLegato.Item("Anno")))
                    cmdUs.Parameters.AddWithValue("@MeseCompetenza", Month(Registrazione.DataRegistrazione))
                    cmdUs.Parameters.AddWithValue("@Automatico", 1)
                    cmdUs.ExecuteNonQuery()
                    Inserito = True
                End If                
            Loop
            myBdReg.Close()
        Loop
        myLegato.Close()
        CommandLegato.Dispose()

        cn.Close()

        Return Inserito

    End Function


End Class
