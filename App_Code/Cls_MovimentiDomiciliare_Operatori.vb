﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_MovimentiDomiciliare_Operatori
    Public Id As Long
    Public IdMovimento As Long
    Public CodiceOperatore As Integer


    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function LegggiTipoOperatore(ByVal StringaConnessione As String, ByVal TipoOperatore As String) As Boolean
        Dim cn As OleDbConnection

        LegggiTipoOperatore = False
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = " SELECT * FROM MovimentiDomiciliare_Operatori inner join AnagraficaComune on CodiceOperatore = CodiceMedico And Tipologia ='A'  Where MovimentiDomiciliare_Operatori.IdMovimento = " & IdMovimento & " And TipoOperazione = ?"
        cmd.Parameters.AddWithValue("@TipoOperatore", TipoOperatore)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            LegggiTipoOperatore = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function


    Sub leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from MovimentiDomiciliare_Operatori where Id = " & Id
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Id = campodbn(myPOSTreader.Item("id"))
            IdMovimento = campodbn(myPOSTreader.Item("IdMovimento"))
            CodiceOperatore = campodbn(myPOSTreader.Item("CodiceOperatore"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from MovimentiDomiciliare_Operatori where IdMovimento = " & IdMovimento)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from MovimentiDomiciliare_Operatori where Id = " & Id
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE MovimentiDomiciliare_Operatori SET IdMovimento=?,CodiceOperatore  = ?  WHERE  ID = " & Id

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@IdMovimento", IdMovimento)
            cmdw.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
            
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            'OraInizio,OraFine

            MySql = "INSERT INTO MovimentiDomiciliare_Operatori (IdMovimento,CodiceOperatore) VALUES (?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IdMovimento", IdMovimento)
            cmdw.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
