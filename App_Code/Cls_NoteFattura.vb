﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_NoteFattura
    Public Codice As String
    Public Descrizione As String
    Public Note As String

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFattura Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFattura Where Codice = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Note = campodb(myPOSTreader.Item("Note"))
      
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Public Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFattura Where Descrizione Like ? ")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Call Leggi(StringaConnessione)
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub

    Function MaxNoteFatture(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection


        MaxNoteFatture = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Codice) from NoteFattura ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As Long

            MassimoLetto = Val(campodb(MaxAddebito.Item(0))) + 1

            Return MassimoLetto
        Else
            Return 1
        End If
        cn.Close()

    End Function

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from NoteFattura Where Codice = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub
    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFattura where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO NoteFattura (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE NoteFattura SET " & _
                " Descrizione = ?, " & _
                " [Note] = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Aliquota", Note)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class

