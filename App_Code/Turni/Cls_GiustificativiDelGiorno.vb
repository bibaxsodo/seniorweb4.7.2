Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_GiustificativiDelGiorno
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public CodiceDipendente As Integer
    Public Orario As Date
    Public Giustificativo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        CodiceDipendente = 0
        Orario = Nothing
        Giustificativo = ""
    End Sub
End Class
