Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Dipendenti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Cognome As String
    Public Nome As String
    Public DataNascita As Date
    Public ComuneNascita As String
    Public ProvinciaNascita As String
    Public Nazionalita As String
    Public IndirizzoResidenza As String
    Public ComuneResidenza As String
    Public CAPResidenza As Integer
    Public ProvinciaResidenza As String
    Public IndirizzoDomicilio As String
    Public ComuneDomicilio As String
    Public CAPDomicilio As Integer
    Public ProvinciaDomicilio As String
    Public Telefono As String
    Public CodiceFiscale As String
    Public Sesso As String
    Public AnagraficaAttiva As String
    Public TelefonoVisibile As String
    Public Foto As String
    Public Aliax As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Cognome = ""
        Nome = ""
        DataNascita = Nothing
        ComuneNascita = ""
        ProvinciaNascita = ""
        Nazionalita = ""
        IndirizzoResidenza = ""
        ComuneResidenza = ""
        CAPResidenza = 0
        ProvinciaResidenza = ""
        IndirizzoDomicilio = ""
        ComuneDomicilio = ""
        CAPDomicilio = 0
        ProvinciaDomicilio = ""
        Telefono = ""
        CodiceFiscale = ""
        Sesso = ""
        AnagraficaAttiva = ""
        TelefonoVisibile = ""
        Foto = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Dipendenti WHERE CodiceDipendente = ?")
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Dipendenti WHERE CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Dipendenti SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Cognome = ?," & _
                    " Nome = ?, " & _
                    " DataNascita = ?, " & _
                    " ComuneNascita = ?, " & _
                    " ProvinciaNascita = ?, " & _
                    " Nazionalita = ?, " & _
                    " IndirizzoResidenza = ?, " & _
                    " ComuneResidenza = ?, " & _
                    " CAPResidenza = ?, " & _
                    " ProvinciaResidenza = ?, " & _
                    " IndirizzoDomicilio = ?, " & _
                    " ComuneDomicilio = ?, " & _
                    " CAPDomicilio = ?, " & _
                    " ProvinciaDomicilio = ?, " & _
                    " Telefono = ?, " & _
                    " CodiceFiscale = ?, " & _
                    " Sesso = ?, " & _
                    " AnagraficaAttiva = ?, " & _
                    " TelefonoVisibile = ?, " & _
                    " Foto = ?, " & _
                    " Alias = ? " & _
                    " WHERE CodiceDipendente = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Cognome", Cognome)
            cmdw.Parameters.AddWithValue("@Nome", Nome)
            cmdw.Parameters.AddWithValue("@DataNascita", DataNascita)
            cmdw.Parameters.AddWithValue("@ComuneNascita", ComuneNascita)
            cmdw.Parameters.AddWithValue("@ProvinciaNascita", ProvinciaNascita)
            cmdw.Parameters.AddWithValue("@Nazionalita", Nazionalita)
            cmdw.Parameters.AddWithValue("@IndirizzoResidenza", IndirizzoResidenza)
            cmdw.Parameters.AddWithValue("@ComuneResidenza", ComuneResidenza)
            cmdw.Parameters.AddWithValue("@CAPResidenza", CAPResidenza)
            cmdw.Parameters.AddWithValue("@ProvinciaResidenza", ProvinciaResidenza)
            cmdw.Parameters.AddWithValue("@IndirizzoDomicilio", IndirizzoDomicilio)
            cmdw.Parameters.AddWithValue("@ComuneDomicilio", ComuneDomicilio)
            cmdw.Parameters.AddWithValue("@CAPDomicilio", CAPDomicilio)
            cmdw.Parameters.AddWithValue("@ProvinciaDomicilio", ProvinciaDomicilio)
            cmdw.Parameters.AddWithValue("@Telefono", Telefono)
            cmdw.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
            cmdw.Parameters.AddWithValue("@Sesso", Sesso)
            cmdw.Parameters.AddWithValue("@AnagraficaAttiva", AnagraficaAttiva)
            cmdw.Parameters.AddWithValue("@TelefonoVisibile", TelefonoVisibile)
            cmdw.Parameters.AddWithValue("@Foto", Foto)
            cmdw.Parameters.AddWithValue("@Alias", Aliax)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Dipendenti (Utente, DataAggiornamento, CodiceDipendente, Cognome, Nome, DataNascita, ComuneNascita, ProvinciaNascita, Nazionalita, IndirizzoResidenza, ComuneResidenza, CAPResidenza, ProvinciaResidenza, IndirizzoDomicilio, ComuneDomicilio, CAPDomicilio, ProvinciaDomicilio, Telefono, CodiceFiscale, Sesso, AnagraficaAttiva, TelefonoVisibile, Foto, Alias) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@Cognome", Cognome)
            cmdw.Parameters.AddWithValue("@Nome", Nome)
            cmdw.Parameters.AddWithValue("@DataNascita", IIf(Year(DataNascita) > 1, DataNascita, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ComuneNascita", ComuneNascita)
            cmdw.Parameters.AddWithValue("@ProvinciaNascita", ProvinciaNascita)
            cmdw.Parameters.AddWithValue("@Nazionalita", Nazionalita)
            cmdw.Parameters.AddWithValue("@IndirizzoResidenza", IndirizzoResidenza)
            cmdw.Parameters.AddWithValue("@ComuneResidenza", ComuneResidenza)
            cmdw.Parameters.AddWithValue("@CAPResidenza", CAPResidenza)
            cmdw.Parameters.AddWithValue("@ProvinciaResidenza", ProvinciaResidenza)
            cmdw.Parameters.AddWithValue("@IndirizzoDomicilio", IndirizzoDomicilio)
            cmdw.Parameters.AddWithValue("@ComuneDomicilio", ComuneDomicilio)
            cmdw.Parameters.AddWithValue("@CAPDomicilio", CAPDomicilio)
            cmdw.Parameters.AddWithValue("@ProvinciaDomicilio", ProvinciaDomicilio)
            cmdw.Parameters.AddWithValue("@Telefono", Telefono)
            cmdw.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
            cmdw.Parameters.AddWithValue("@Sesso", Sesso)
            cmdw.Parameters.AddWithValue("@AnagraficaAttiva", AnagraficaAttiva)
            cmdw.Parameters.AddWithValue("@TelefonoVisibile", TelefonoVisibile)
            cmdw.Parameters.AddWithValue("@Foto", Foto)
            cmdw.Parameters.AddWithValue("@Alias", Aliax)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Validita As String, ByVal Gruppo As String, ByVal xCognome As String, ByVal xNome As String, ByVal Qualifica As String, ByVal Tabella As System.Data.DataTable)
        Dim Extr_Gruppo As String
        Dim Sel_Gruppo As String
        Dim Extr_Qualifica As String
        Dim Sel_Qualifica As String
        Dim Sel_Cognome As String
        Dim Sel_Nome As String
        Dim Extr_Matricola As String
        Dim Extr_Equipaggio As String
        Dim Sel_Equipaggio As String
        Dim MySql As String

        Dim p As New Cls_ParametriTurni
        Dim g As New Cls_GruppiDipendenti

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        ' Roba Mia

        p.Leggi(StringaConnessione)

        Extr_Matricola = "(SELECT TOP 1 Matricola FROM Matricola" & _
                         " WHERE Matricola.CodiceDipendente = Dipendenti.CodiceDipendente" & _
                         " AND Matricola.DataAssunzione <= ?" & _
                         " ORDER BY Matricola.DataAssunzione DESC, Id DESC) AS Matricola"
        Extr_Gruppo = ""
        Sel_Gruppo = ""
        Extr_Qualifica = ""
        Sel_Qualifica = ""
        Sel_Cognome = ""
        Sel_Nome = ""
        Extr_Equipaggio = "' ' AS Equip"
        Sel_Equipaggio = ""
        If Gruppo <> "" Then
            Extr_Gruppo = "? AS Gruppo"
            Sel_Gruppo = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
                         " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
                         " AND DatiVariabili.CodiceVariabile = ?" & _
                         " AND DatiVariabili.Validita <= ?" & _
                         " ORDER BY DatiVariabili.Validita DESC, Id DESC) = ?"
        Else
            Extr_Gruppo = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
                         " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
                         " AND DatiVariabili.CodiceVariabile = ?" & _
                         " AND DatiVariabili.Validita <= ?" & _
                         " ORDER BY DatiVariabili.Validita DESC, Id DESC) AS Gruppo"
            'If TurniClient = True Then
            ' Sel_Gruppo = "("
            ' For i = 0 To Cmb_Gruppo.ListCount - 1
            ' If Sel_Gruppo <> "(" Then Sel_Gruppo = Sel_Gruppo & " Or "
            ' Sel_Gruppo = Sel_Gruppo & "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
            '              " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
            '              " AND DatiVariabili.CodiceVariabile = '" & p.CodiceAnagraficoGruppo & "'" & _
            '              " AND DatiVariabili.Validita <= {ts '" & Format(Validita, "yyyy-mm-dd") & " 00:00:00'}" & _
            '              " ORDER BY DatiVariabili.Validita DESC, Id DESC) = '" & Vt_Gruppi(i) & "'"
            ' Next i
            ' Sel_Gruppo = Sel_Gruppo & ")"
            'End If
        End If
        If Qualifica <> "" Then
            Extr_Qualifica = "? AS Qualifica"
            Sel_Qualifica = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
                            " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
                            " AND DatiVariabili.CodiceVariabile = ?" & _
                            " AND DatiVariabili.Validita <= ?" & _
                            " ORDER BY DatiVariabili.Validita DESC, Id DESC) = ?"
        Else
            Extr_Qualifica = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
                            " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
                            " AND DatiVariabili.CodiceVariabile = ?" & _
                            " AND DatiVariabili.Validita <= ?" & _
                            " ORDER BY DatiVariabili.Validita DESC, Id DESC) AS Qualifica"
        End If
        If xCognome <> "" Then
            Sel_Cognome = "Cognome LIKE ?"
        End If
        If xNome <> "" Then
            Sel_Nome = "Nome LIKE ?"
        End If
        'If Chk_Equipaggi.Value = 1 Then
        ' Extr_Equipaggio = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
        '                   " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
        '                   " AND DatiVariabili.CodiceVariabile = '" & p.CodiceAnagraficoEquipaggio & "'" & _
        '                   " AND DatiVariabili.Validita <= {ts '" & Format(Validita, "yyyy-mm-dd") & " 00:00:00'}" & _
        '                   " ORDER BY DatiVariabili.Validita DESC, Id DESC) AS Equip"
        ' Sel_Equipaggio = "(SELECT TOP 1 ContenutoTesto FROM DatiVariabili" & _
        '                  " WHERE DatiVariabili.CodiceDipendente = Dipendenti.CodiceDipendente" & _
        '                  " AND DatiVariabili.CodiceVariabile = '" & p.CodiceAnagraficoEquipaggio & "'" & _
        '                  " AND DatiVariabili.Validita <= {ts '" & Format(Validita, "yyyy-mm-dd") & " 00:00:00'}" & _
        '                  " ORDER BY DatiVariabili.Validita DESC, Id DESC) <> ''"
        ' End If

        MySql = "SELECT " & Extr_Gruppo & ", " & Extr_Equipaggio & ", CodiceDipendente, Cognome, Nome, " & Extr_Matricola & _
                " FROM Dipendenti" & _
                " WHERE AnagraficaAttiva <> 'N'"
        If Sel_Gruppo <> "" Then
            MySql = MySql & " AND " & Sel_Gruppo
        End If
        If Sel_Qualifica <> "" Then
            MySql = MySql & " AND " & Sel_Qualifica
        End If
        If xCognome <> "" Then
            MySql = MySql & " AND " & Sel_Cognome
        End If
        If xNome <> "" Then
            MySql = MySql & " AND " & Sel_Nome
        End If
        'If Sel_Gruppo <> "" And Sel_Qualifica = "" And Trim(Cognome) = "" And Trim(Nome) = "" And Chk_Equipaggi.Value = 1 Then
        ' MySql = MySql & " AND " & Sel_Equipaggio
        'End If
        MySql = MySql & " ORDER BY Cognome, Nome"

        ' Fine Roba

        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql

        If Gruppo <> "" Then
            cmd.Parameters.AddWithValue("@Gruppo", Gruppo)
        Else
            cmd.Parameters.AddWithValue("@CodiceAnagraficoGruppo", p.CodiceAnagraficoGruppo)
            cmd.Parameters.AddWithValue("@Validita", Validita)
        End If
        cmd.Parameters.AddWithValue("@Validita", Validita)
        If Gruppo <> "" Then
            cmd.Parameters.AddWithValue("@CodiceAnagraficoGruppo", p.CodiceAnagraficoGruppo)
            cmd.Parameters.AddWithValue("@Validita", Validita)
            cmd.Parameters.AddWithValue("@Gruppo", Gruppo)
        End If
        If Qualifica <> "" Then
            cmd.Parameters.AddWithValue("@CodiceAnagraficoQualifica", p.CodiceAnagraficoQualifica)
            cmd.Parameters.AddWithValue("@Validita", Validita)
            cmd.Parameters.AddWithValue("@Qualifica", Qualifica)
        End If
        If xCognome <> "" Then
            cmd.Parameters.AddWithValue("@Cognome", xCognome)
        End If
        If xNome <> "" Then
            cmd.Parameters.AddWithValue("@Nome", xNome)
        End If

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Gruppo", GetType(String))
        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("CodiceDipendente", GetType(String))
        Tabella.Columns.Add("Matricola", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            g.Leggi(StringaConnessione, myPOSTreader.Item("Gruppo"))
            myriga(0) = g.Descrizione
            myriga(1) = myPOSTreader.Item("Cognome")
            myriga(2) = myPOSTreader.Item("Nome")
            myriga(3) = myPOSTreader.Item("CodiceDipendente")
            myriga(4) = myPOSTreader.Item("Matricola")
            '            myriga(5) = Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * From Dipendenti WHERE " & _
                           "CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            Cognome = StringaDb(myPOSTreader.Item("Cognome"))
            Nome = StringaDb(myPOSTreader.Item("Nome"))
            DataNascita = DataDb(myPOSTreader.Item("DataNascita"))
            ComuneNascita = StringaDb(myPOSTreader.Item("ComuneNascita"))
            ProvinciaNascita = StringaDb(myPOSTreader.Item("ProvinciaNascita"))
            Nazionalita = StringaDb(myPOSTreader.Item("Nazionalita"))
            IndirizzoResidenza = StringaDb(myPOSTreader.Item("IndirizzoResidenza"))
            ComuneResidenza = StringaDb(myPOSTreader.Item("ComuneResidenza"))
            CAPResidenza = NumeroDb(myPOSTreader.Item("CAPResidenza"))
            ProvinciaResidenza = StringaDb(myPOSTreader.Item("ProvinciaResidenza"))
            IndirizzoDomicilio = StringaDb(myPOSTreader.Item("IndirizzoDomicilio"))
            ComuneDomicilio = StringaDb(myPOSTreader.Item("ComuneDomicilio"))
            CAPDomicilio = NumeroDb(myPOSTreader.Item("CAPDomicilio"))
            ProvinciaDomicilio = StringaDb(myPOSTreader.Item("ProvinciaDomicilio"))
            Telefono = StringaDb(myPOSTreader.Item("Telefono"))
            CodiceFiscale = StringaDb(myPOSTreader.Item("CodiceFiscale"))
            Sesso = StringaDb(myPOSTreader.Item("Sesso"))
            AnagraficaAttiva = StringaDb(myPOSTreader.Item("AnagraficaAttiva"))
            TelefonoVisibile = StringaDb(myPOSTreader.Item("TelefonoVisibile"))
            Foto = StringaDb(myPOSTreader.Item("Foto"))
            Aliax = StringaDb(myPOSTreader.Item("Alias"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceDipendente = ?"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Dipendenti" & _
                          " WHERE CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function EsisteCognomeNome(ByVal StringaConnessione As String, ByVal xCognome As String, ByVal xNome As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Dipendenti" & _
                          " WHERE Cognome = ?" & _
                          " AND Nome = ?"
        cmd.Parameters.AddWithValue("@Cognome", xCognome)
        cmd.Parameters.AddWithValue("@Nome", xNome)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function MassimoCodice(ByVal StringaConnessione As String) As Integer
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT MAX(CodiceDipendente) FROM Dipendenti")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            If Not IsDBNull(myPOSTreader.Item(0)) Then
                Return myPOSTreader.Item(0) + 1
            Else
                Return 1
            End If
        Else
            Return 1
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Gruppo_Dipendente(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xValidita As Date) As Object
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim p As New Cls_ParametriTurni
        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoGruppo)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Select Case NumeroDb(myPOSTreader.Item("Tipo"))
                Case 0
                    Return myPOSTreader.Item("ContenutoTesto")
                Case 100
                    Return myPOSTreader.Item("ContenutoNumero")
                Case Else
                    Return ""
            End Select
        Else
            Return ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DipendenteAssunto(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xDataMin As Date, ByVal xDataMax As Date) As Boolean
        Dim cn As OleDbConnection

        DipendenteAssunto = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM Matricola " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND DataAssunzione <= ?" & _
                          " ORDER BY DataAssunzione DESC, Id"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", xDataMax)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If DataDb(myPOSTreader.Item("DataLicenziamento")) <> Nothing Then
                If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") >= Format(xDataMin, "yyyyMMdd") Then
                    DipendenteAssunto = True
                End If
            Else
                DipendenteAssunto = True
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function CampoDipendenti(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        CampoDipendenti = ""

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT " & xcampo & " FROM Dipendenti WHERE CodiceDipendente = ?"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoDipendenti = myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function
End Class
