Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_StrutturaNotti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Descrizione As String
    Public GiornoOrario(6, 4) As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Descrizione = ""
        For x = 0 To 6
            For y = 0 To 4
                GiornoOrario(x, y) = ""
            Next
        Next
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * FROM StrutturaNotti WHERE " & _
                           " Codice = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE StrutturaNotti SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?," & _
                    " Giorno1Orario1 = ?, " & _
                    " Giorno1Orario2 = ?, " & _
                    " Giorno1Orario3 = ?, " & _
                    " Giorno1Orario4 = ?, " & _
                    " Giorno1Orario5 = ?, " & _
                    " Giorno2Orario1 = ?, " & _
                    " Giorno2Orario2 = ?, " & _
                    " Giorno2Orario3 = ?, " & _
                    " Giorno2Orario4 = ?, " & _
                    " Giorno2Orario5 = ?, " & _
                    " Giorno3Orario1 = ?, " & _
                    " Giorno3Orario2 = ?, " & _
                    " Giorno3Orario3 = ?, " & _
                    " Giorno3Orario4 = ?, " & _
                    " Giorno3Orario5 = ?, " & _
                    " Giorno4Orario1 = ?, " & _
                    " Giorno4Orario2 = ?, " & _
                    " Giorno4Orario3 = ?, " & _
                    " Giorno4Orario4 = ?, " & _
                    " Giorno4Orario5 = ?, " & _
                    " Giorno5Orario1 = ?, " & _
                    " Giorno5Orario2 = ?, " & _
                    " Giorno5Orario3 = ?, " & _
                    " Giorno5Orario4 = ?, " & _
                    " Giorno5Orario5 = ?, " & _
                    " Giorno6Orario1 = ?, " & _
                    " Giorno6Orario2 = ?, " & _
                    " Giorno6Orario3 = ?, " & _
                    " Giorno6Orario4 = ?, " & _
                    " Giorno6Orario5 = ?, " & _
                    " Giorno7Orario1 = ?, " & _
                    " Giorno7Orario2 = ?, " & _
                    " Giorno7Orario3 = ?, " & _
                    " Giorno7Orario4 = ?, " & _
                    " Giorno7Orario5 = ?" & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Giorno1Orario1", GiornoOrario(0, 0))
            cmdw.Parameters.AddWithValue("@Giorno1Orario2", GiornoOrario(0, 1))
            cmdw.Parameters.AddWithValue("@Giorno1Orario3", GiornoOrario(0, 2))
            cmdw.Parameters.AddWithValue("@Giorno1Orario4", GiornoOrario(0, 3))
            cmdw.Parameters.AddWithValue("@Giorno1Orario5", GiornoOrario(0, 4))
            cmdw.Parameters.AddWithValue("@Giorno2Orario1", GiornoOrario(1, 0))
            cmdw.Parameters.AddWithValue("@Giorno2Orario2", GiornoOrario(1, 1))
            cmdw.Parameters.AddWithValue("@Giorno2Orario3", GiornoOrario(1, 2))
            cmdw.Parameters.AddWithValue("@Giorno2Orario4", GiornoOrario(1, 3))
            cmdw.Parameters.AddWithValue("@Giorno2Orario5", GiornoOrario(1, 4))
            cmdw.Parameters.AddWithValue("@Giorno3Orario1", GiornoOrario(2, 0))
            cmdw.Parameters.AddWithValue("@Giorno3Orario2", GiornoOrario(2, 1))
            cmdw.Parameters.AddWithValue("@Giorno3Orario3", GiornoOrario(2, 2))
            cmdw.Parameters.AddWithValue("@Giorno3Orario4", GiornoOrario(2, 3))
            cmdw.Parameters.AddWithValue("@Giorno3Orario5", GiornoOrario(2, 4))
            cmdw.Parameters.AddWithValue("@Giorno4Orario1", GiornoOrario(3, 0))
            cmdw.Parameters.AddWithValue("@Giorno4Orario2", GiornoOrario(3, 1))
            cmdw.Parameters.AddWithValue("@Giorno4Orario3", GiornoOrario(3, 2))
            cmdw.Parameters.AddWithValue("@Giorno4Orario4", GiornoOrario(3, 3))
            cmdw.Parameters.AddWithValue("@Giorno4Orario5", GiornoOrario(3, 4))
            cmdw.Parameters.AddWithValue("@Giorno5Orario1", GiornoOrario(4, 0))
            cmdw.Parameters.AddWithValue("@Giorno5Orario2", GiornoOrario(4, 1))
            cmdw.Parameters.AddWithValue("@Giorno5Orario3", GiornoOrario(4, 2))
            cmdw.Parameters.AddWithValue("@Giorno5Orario4", GiornoOrario(4, 3))
            cmdw.Parameters.AddWithValue("@Giorno5Orario5", GiornoOrario(4, 4))
            cmdw.Parameters.AddWithValue("@Giorno6Orario1", GiornoOrario(5, 0))
            cmdw.Parameters.AddWithValue("@Giorno6Orario2", GiornoOrario(5, 1))
            cmdw.Parameters.AddWithValue("@Giorno6Orario3", GiornoOrario(5, 2))
            cmdw.Parameters.AddWithValue("@Giorno6Orario4", GiornoOrario(5, 3))
            cmdw.Parameters.AddWithValue("@Giorno6Orario5", GiornoOrario(5, 4))
            cmdw.Parameters.AddWithValue("@Giorno7Orario1", GiornoOrario(6, 0))
            cmdw.Parameters.AddWithValue("@Giorno7Orario2", GiornoOrario(6, 1))
            cmdw.Parameters.AddWithValue("@Giorno7Orario3", GiornoOrario(6, 2))
            cmdw.Parameters.AddWithValue("@Giorno7Orario4", GiornoOrario(6, 3))
            cmdw.Parameters.AddWithValue("@Giorno7Orario5", GiornoOrario(6, 4))
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO StrutturaNotti (Utente, DataAggiornamento, Codice, Validita, Descrizione, Giorno1Orario1, Giorno1Orario2, Giorno1Orario3, Giorno1Orario4, Giorno1Orario5, Giorno2Orario1, Giorno2Orario2, Giorno2Orario3, Giorno2Orario4, Giorno2Orario5, Giorno3Orario1, Giorno3Orario2, Giorno3Orario3, Giorno3Orario4, Giorno3Orario5, Giorno4Orario1, Giorno4Orario2, Giorno4Orario3, Giorno4Orario4, Giorno4Orario5, Giorno5Orario1, Giorno5Orario2, Giorno5Orario3, Giorno5Orario4, Giorno5Orario5, Giorno6Orario1, Giorno6Orario2, Giorno6Orario3, Giorno6Orario4, Giorno6Orario5, Giorno7Orario1, Giorno7Orario2, Giorno7Orario3, Giorno7Orario4, Giorno7Orario5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Giorno1Orario1", GiornoOrario(0, 0))
            cmdw.Parameters.AddWithValue("@Giorno1Orario2", GiornoOrario(0, 1))
            cmdw.Parameters.AddWithValue("@Giorno1Orario3", GiornoOrario(0, 2))
            cmdw.Parameters.AddWithValue("@Giorno1Orario4", GiornoOrario(0, 3))
            cmdw.Parameters.AddWithValue("@Giorno1Orario5", GiornoOrario(0, 4))
            cmdw.Parameters.AddWithValue("@Giorno2Orario1", GiornoOrario(1, 0))
            cmdw.Parameters.AddWithValue("@Giorno2Orario2", GiornoOrario(1, 1))
            cmdw.Parameters.AddWithValue("@Giorno2Orario3", GiornoOrario(1, 2))
            cmdw.Parameters.AddWithValue("@Giorno2Orario4", GiornoOrario(1, 3))
            cmdw.Parameters.AddWithValue("@Giorno2Orario5", GiornoOrario(1, 4))
            cmdw.Parameters.AddWithValue("@Giorno3Orario1", GiornoOrario(2, 0))
            cmdw.Parameters.AddWithValue("@Giorno3Orario2", GiornoOrario(2, 1))
            cmdw.Parameters.AddWithValue("@Giorno3Orario3", GiornoOrario(2, 2))
            cmdw.Parameters.AddWithValue("@Giorno3Orario4", GiornoOrario(2, 3))
            cmdw.Parameters.AddWithValue("@Giorno3Orario5", GiornoOrario(2, 4))
            cmdw.Parameters.AddWithValue("@Giorno4Orario1", GiornoOrario(3, 0))
            cmdw.Parameters.AddWithValue("@Giorno4Orario2", GiornoOrario(3, 1))
            cmdw.Parameters.AddWithValue("@Giorno4Orario3", GiornoOrario(3, 2))
            cmdw.Parameters.AddWithValue("@Giorno4Orario4", GiornoOrario(3, 3))
            cmdw.Parameters.AddWithValue("@Giorno4Orario5", GiornoOrario(3, 4))
            cmdw.Parameters.AddWithValue("@Giorno5Orario1", GiornoOrario(4, 0))
            cmdw.Parameters.AddWithValue("@Giorno5Orario2", GiornoOrario(4, 1))
            cmdw.Parameters.AddWithValue("@Giorno5Orario3", GiornoOrario(4, 2))
            cmdw.Parameters.AddWithValue("@Giorno5Orario4", GiornoOrario(4, 3))
            cmdw.Parameters.AddWithValue("@Giorno5Orario5", GiornoOrario(4, 4))
            cmdw.Parameters.AddWithValue("@Giorno6Orario1", GiornoOrario(5, 0))
            cmdw.Parameters.AddWithValue("@Giorno6Orario2", GiornoOrario(5, 1))
            cmdw.Parameters.AddWithValue("@Giorno6Orario3", GiornoOrario(5, 2))
            cmdw.Parameters.AddWithValue("@Giorno6Orario4", GiornoOrario(5, 3))
            cmdw.Parameters.AddWithValue("@Giorno6Orario5", GiornoOrario(5, 4))
            cmdw.Parameters.AddWithValue("@Giorno7Orario1", GiornoOrario(6, 0))
            cmdw.Parameters.AddWithValue("@Giorno7Orario2", GiornoOrario(6, 1))
            cmdw.Parameters.AddWithValue("@Giorno7Orario3", GiornoOrario(6, 2))
            cmdw.Parameters.AddWithValue("@Giorno7Orario4", GiornoOrario(6, 3))
            cmdw.Parameters.AddWithValue("@Giorno7Orario5", GiornoOrario(6, 4))
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * FROM StrutturaNotti WHERE " & _
                           " Codice = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            For x = 0 To 6
                For y = 0 To 4
                    GiornoOrario(x, y) = StringaDb(myPOSTreader.Item("Giorno" & x + 1 & "Orario" & y + 1))
                Next
            Next
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub Trova(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select TOP 1 * FROM StrutturaNotti WHERE " & _
                          " Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id DESC"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            For x = 0 To 6
                For y = 0 To 4
                    GiornoOrario(x, y) = StringaDb(myPOSTreader.Item("Giorno" & x + 1 & "Orario" & y + 1))
                Next
            Next
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * FROM StrutturaNotti WHERE " & _
                           " Codice = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM StrutturaNotti WHERE Codice = ? AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub LoadDati(ByVal ConnectionString As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From StrutturaNotti Order By Codice, Validita")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Giorno_1_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_1_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_1_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_1_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_1_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_2_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_2_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_2_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_2_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_2_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_3_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_3_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_3_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_3_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_3_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_4_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_4_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_4_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_4_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_4_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_5_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_5_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_5_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_5_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_5_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_6_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_6_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_6_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_6_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_6_Orario_5", GetType(String))
        Tabella.Columns.Add("Giorno_7_Orario_1", GetType(String))
        Tabella.Columns.Add("Giorno_7_Orario_2", GetType(String))
        Tabella.Columns.Add("Giorno_7_Orario_3", GetType(String))
        Tabella.Columns.Add("Giorno_7_Orario_4", GetType(String))
        Tabella.Columns.Add("Giorno_7_Orario_5", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Validita")
            myriga(2) = myPOSTreader.Item("Descrizione")
            myriga(3) = myPOSTreader.Item("Giorno1Orario1")
            myriga(4) = myPOSTreader.Item("Giorno1Orario2")
            myriga(5) = myPOSTreader.Item("Giorno1Orario3")
            myriga(6) = myPOSTreader.Item("Giorno1Orario4")
            myriga(7) = myPOSTreader.Item("Giorno1Orario5")
            myriga(8) = myPOSTreader.Item("Giorno2Orario1")
            myriga(9) = myPOSTreader.Item("Giorno2Orario2")
            myriga(10) = myPOSTreader.Item("Giorno2Orario3")
            myriga(11) = myPOSTreader.Item("Giorno2Orario4")
            myriga(12) = myPOSTreader.Item("Giorno2Orario5")
            myriga(13) = myPOSTreader.Item("Giorno3Orario1")
            myriga(14) = myPOSTreader.Item("Giorno3Orario2")
            myriga(15) = myPOSTreader.Item("Giorno3Orario3")
            myriga(16) = myPOSTreader.Item("Giorno3Orario4")
            myriga(17) = myPOSTreader.Item("Giorno3Orario5")
            myriga(18) = myPOSTreader.Item("Giorno4Orario1")
            myriga(19) = myPOSTreader.Item("Giorno4Orario2")
            myriga(20) = myPOSTreader.Item("Giorno4Orario3")
            myriga(21) = myPOSTreader.Item("Giorno4Orario4")
            myriga(22) = myPOSTreader.Item("Giorno4Orario5")
            myriga(23) = myPOSTreader.Item("Giorno5Orario1")
            myriga(24) = myPOSTreader.Item("Giorno5Orario2")
            myriga(25) = myPOSTreader.Item("Giorno5Orario3")
            myriga(26) = myPOSTreader.Item("Giorno5Orario4")
            myriga(27) = myPOSTreader.Item("Giorno5Orario5")
            myriga(28) = myPOSTreader.Item("Giorno6Orario1")
            myriga(29) = myPOSTreader.Item("Giorno6Orario2")
            myriga(30) = myPOSTreader.Item("Giorno6Orario3")
            myriga(31) = myPOSTreader.Item("Giorno6Orario4")
            myriga(32) = myPOSTreader.Item("Giorno6Orario5")
            myriga(33) = myPOSTreader.Item("Giorno7Orario1")
            myriga(34) = myPOSTreader.Item("Giorno7Orario2")
            myriga(35) = myPOSTreader.Item("Giorno7Orario3")
            myriga(36) = myPOSTreader.Item("Giorno7Orario4")
            myriga(37) = myPOSTreader.Item("Giorno7Orario5")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub
End Class
