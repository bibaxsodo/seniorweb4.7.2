Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaVariazioni
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Riga As Short
    Public Servizio As String
    Public ServizioPerGruppo As String
    Public Commento As String
    Public Ordine As Short
    Public PaginaNuova As String
    Public Canale As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Riga = 0
        Servizio = ""
        ServizioPerGruppo = ""
        Commento = ""
        Ordine = 0
        PaginaNuova = ""
        Canale = ""
    End Sub
End Class
