Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaVariazioniOrario
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Riga As Short
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public PrimoGiornoSuccessivo As String
    Public SecondoGiornoSuccessivo As String
    Public TerzoGiornoSuccessivo As String
    Public QuartoGiornoSuccessivo As String
    Public QuintoGiornoSuccessivo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Riga = 0
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        PrimoGiornoSuccessivo = ""
        SecondoGiornoSuccessivo = ""
        TerzoGiornoSuccessivo = ""
        QuartoGiornoSuccessivo = ""
        QuintoGiornoSuccessivo = ""
    End Sub

End Class
