Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Festivita
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Anno As Integer
    Public G0101 As String
    Public G0601 As String
    Public G1903 As String
    Public G2504 As String
    Public G0105 As String
    Public G0206 As String
    Public G2906 As String
    Public G1508 As String
    Public G0111 As String
    Public G0411 As String
    Public G0812 As String
    Public G2512 As String
    Public G2612 As String
    Public PasquaGG As Byte
    Public PasquaMM As Byte
    Public Pasqua As String
    Public LunediPasquaGG As Byte
    Public LunediPasquaMM As Byte
    Public LunediPasqua As String
    Public AscensioneGG As Byte
    Public AscensioneMM As Byte
    Public Ascensione As String
    Public DominiGG As Byte
    Public DominiMM As Byte
    Public Domini As String
    Public PatronoGG As Byte
    Public PatronoMM As Byte
    Public Patrono As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Anno = 0
        G0101 = ""
        G0601 = ""
        G1903 = ""
        G2504 = ""
        G0105 = ""
        G0206 = ""
        G2906 = ""
        G1508 = ""
        G0111 = ""
        G0411 = ""
        G0812 = ""
        G2512 = ""
        G2612 = ""
        PasquaGG = 0
        PasquaMM = 0
        Pasqua = ""
        LunediPasquaGG = 0
        LunediPasquaMM = 0
        LunediPasqua = ""
        AscensioneGG = 0
        AscensioneMM = 0
        Ascensione = ""
        DominiGG = 0
        DominiMM = 0
        Domini = ""
        PatronoGG = 0
        PatronoMM = 0
        Patrono = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xAnno As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Festivita WHERE " & _
                           "Anno = ?")
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Festivita WHERE Anno = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Festivita SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " G0101  = ?," & _
                    " G0601  = ?," & _
                    " G1903  = ?," & _
                    " G2504  = ?," & _
                    " G0105  = ?," & _
                    " G0206  = ?," & _
                    " G2906  = ?," & _
                    " G1508  = ?," & _
                    " G0111  = ?," & _
                    " G0411  = ?," & _
                    " G0812  = ?," & _
                    " G2512  = ?," & _
                    " G2612  = ?," & _
                    " PasquaGG  = ?," & _
                    " PasquaMM  = ?," & _
                    " Pasqua  = ?," & _
                    " LunediPasquaGG  = ?," & _
                    " LunediPasquaMM  = ?," & _
                    " LunediPasqua  = ?," & _
                    " AscensioneGG  = ?," & _
                    " AscensioneMM  = ?," & _
                    " Ascensione  = ?," & _
                    " DominiGG  = ?," & _
                    " DominiMM  = ?," & _
                    " Domini  = ?," & _
                    " PatronoGG  = ?," & _
                    " PatronoMM  = ?," & _
                    " Patrono  = ?," & _
                    " WHERE Anno = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@G0101", G0101)
            cmdw.Parameters.AddWithValue("@G0601", G0601)
            cmdw.Parameters.AddWithValue("@G1903", G1903)
            cmdw.Parameters.AddWithValue("@G2504", G2504)
            cmdw.Parameters.AddWithValue("@G0105", G0105)
            cmdw.Parameters.AddWithValue("@G0206", G0206)
            cmdw.Parameters.AddWithValue("@G2906", G2906)
            cmdw.Parameters.AddWithValue("@G1508", G1508)
            cmdw.Parameters.AddWithValue("@G0111", G0111)
            cmdw.Parameters.AddWithValue("@G0411", G0411)
            cmdw.Parameters.AddWithValue("@G0812", G0812)
            cmdw.Parameters.AddWithValue("@G2512", G2512)
            cmdw.Parameters.AddWithValue("@G2612", G2612)
            cmdw.Parameters.AddWithValue("@PasquaGG", PasquaGG)
            cmdw.Parameters.AddWithValue("@PasquaMM", PasquaMM)
            cmdw.Parameters.AddWithValue("@Pasqua", Pasqua)
            cmdw.Parameters.AddWithValue("@LunediPasquaGG", LunediPasquaGG)
            cmdw.Parameters.AddWithValue("@LunediPasquaMM", LunediPasquaMM)
            cmdw.Parameters.AddWithValue("@LunediPasqua", LunediPasqua)
            cmdw.Parameters.AddWithValue("@AscensioneGG", AscensioneGG)
            cmdw.Parameters.AddWithValue("@AscensioneMM", AscensioneMM)
            cmdw.Parameters.AddWithValue("@Ascensione", Ascensione)
            cmdw.Parameters.AddWithValue("@DominiGG", DominiGG)
            cmdw.Parameters.AddWithValue("@DominiMM", DominiMM)
            cmdw.Parameters.AddWithValue("@Domini", Domini)
            cmdw.Parameters.AddWithValue("@PatronoGG", PatronoGG)
            cmdw.Parameters.AddWithValue("@PatronoMM", PatronoMM)
            cmdw.Parameters.AddWithValue("@Patrono", Patrono)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Festivita (Utente, DataAggiornamento, Anno, G0101, G0601, G1903, G2504, G0105, G0206, G2906, G1508, G0111, G0411, G0812, G2512, G2612, PasquaGG, PasquaMM, Pasqua, LunediPasquaGG, LunediPasquaMM, LunediPasqua, AscensioneGG, AscensioneMM, Ascensione, DominiGG, DominiMM, Domini, PatronoGG, PatronoMM, Patrono) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Parameters.AddWithValue("@G0101", G0101)
            cmdw.Parameters.AddWithValue("@G0601", G0601)
            cmdw.Parameters.AddWithValue("@G1903", G1903)
            cmdw.Parameters.AddWithValue("@G2504", G2504)
            cmdw.Parameters.AddWithValue("@G0105", G0105)
            cmdw.Parameters.AddWithValue("@G0206", G0206)
            cmdw.Parameters.AddWithValue("@G2906", G2906)
            cmdw.Parameters.AddWithValue("@G1508", G1508)
            cmdw.Parameters.AddWithValue("@G0111", G0111)
            cmdw.Parameters.AddWithValue("@G0411", G0411)
            cmdw.Parameters.AddWithValue("@G0812", G0812)
            cmdw.Parameters.AddWithValue("@G2512", G2512)
            cmdw.Parameters.AddWithValue("@G2612", G2612)
            cmdw.Parameters.AddWithValue("@PasquaGG", PasquaGG)
            cmdw.Parameters.AddWithValue("@PasquaMM", PasquaMM)
            cmdw.Parameters.AddWithValue("@Pasqua", Pasqua)
            cmdw.Parameters.AddWithValue("@LunediPasquaGG", LunediPasquaGG)
            cmdw.Parameters.AddWithValue("@LunediPasquaMM", LunediPasquaMM)
            cmdw.Parameters.AddWithValue("@LunediPasqua", LunediPasqua)
            cmdw.Parameters.AddWithValue("@AscensioneGG", AscensioneGG)
            cmdw.Parameters.AddWithValue("@AscensioneMM", AscensioneMM)
            cmdw.Parameters.AddWithValue("@Ascensione", Ascensione)
            cmdw.Parameters.AddWithValue("@DominiGG", DominiGG)
            cmdw.Parameters.AddWithValue("@DominiMM", DominiMM)
            cmdw.Parameters.AddWithValue("@Domini", Domini)
            cmdw.Parameters.AddWithValue("@PatronoGG", PatronoGG)
            cmdw.Parameters.AddWithValue("@PatronoMM", PatronoMM)
            cmdw.Parameters.AddWithValue("@Patrono", Patrono)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From Festivita Order By Anno")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("01/01", GetType(String))
        Tabella.Columns.Add("06/01", GetType(String))
        Tabella.Columns.Add("19/03", GetType(String))
        Tabella.Columns.Add("25/04", GetType(String))
        Tabella.Columns.Add("01/05", GetType(String))
        Tabella.Columns.Add("02/06", GetType(String))
        Tabella.Columns.Add("29/06", GetType(String))
        Tabella.Columns.Add("15/08", GetType(String))
        Tabella.Columns.Add("01/11", GetType(String))
        Tabella.Columns.Add("04/11", GetType(String))
        Tabella.Columns.Add("08/12", GetType(String))
        Tabella.Columns.Add("25/12", GetType(String))
        Tabella.Columns.Add("26/12", GetType(String))
        Tabella.Columns.Add("Pasqua", GetType(String))
        Tabella.Columns.Add("LunPasq", GetType(String))
        Tabella.Columns.Add("Ascensione", GetType(String))
        Tabella.Columns.Add("Corpus Domini", GetType(String))
        Tabella.Columns.Add("Patrono", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Anno")
            myriga(1) = myPOSTreader.Item("G0101")
            myriga(2) = myPOSTreader.Item("G0601")
            myriga(3) = myPOSTreader.Item("G1903")
            myriga(4) = myPOSTreader.Item("G2504")
            myriga(5) = myPOSTreader.Item("G0105")
            myriga(6) = myPOSTreader.Item("G0206")
            myriga(7) = myPOSTreader.Item("G2906")
            myriga(8) = myPOSTreader.Item("G1508")
            myriga(9) = myPOSTreader.Item("G0111")
            myriga(10) = myPOSTreader.Item("G0411")
            myriga(11) = myPOSTreader.Item("G0812")
            myriga(12) = myPOSTreader.Item("G2512")
            myriga(13) = myPOSTreader.Item("G2612")
            myriga(14) = myPOSTreader.Item("Pasqua")
            myriga(15) = myPOSTreader.Item("LunediPasqua")
            myriga(16) = myPOSTreader.Item("Ascensione")
            myriga(17) = myPOSTreader.Item("Domini")
            myriga(18) = myPOSTreader.Item("Patrono")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xAnno As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Festivita WHERE " & _
                           "Anno = ?")
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Anno = NumeroDb(myPOSTreader.Item("Anno"))
            G0101 = StringaDb(myPOSTreader.Item("G0101"))
            G0601 = StringaDb(myPOSTreader.Item("G0601"))
            G1903 = StringaDb(myPOSTreader.Item("G1903"))
            G2504 = StringaDb(myPOSTreader.Item("G2504"))
            G0105 = StringaDb(myPOSTreader.Item("G0105"))
            G0206 = StringaDb(myPOSTreader.Item("G0206"))
            G2906 = StringaDb(myPOSTreader.Item("G2906"))
            G1508 = StringaDb(myPOSTreader.Item("G1508"))
            G0111 = StringaDb(myPOSTreader.Item("G0111"))
            G0411 = StringaDb(myPOSTreader.Item("G0411"))
            G0812 = StringaDb(myPOSTreader.Item("G0812"))
            G2512 = StringaDb(myPOSTreader.Item("G2512"))
            G2612 = StringaDb(myPOSTreader.Item("G2612"))
            PasquaGG = NumeroDb(myPOSTreader.Item("PasquaGG"))
            PasquaMM = NumeroDb(myPOSTreader.Item("PasquaMM"))
            Pasqua = StringaDb(myPOSTreader.Item("Pasqua"))
            LunediPasquaGG = NumeroDb(myPOSTreader.Item("LunediPasquaGG"))
            LunediPasquaMM = NumeroDb(myPOSTreader.Item("LunediPasquaMM"))
            LunediPasqua = StringaDb(myPOSTreader.Item("LunediPasqua"))
            AscensioneGG = NumeroDb(myPOSTreader.Item("AscensioneGG"))
            AscensioneMM = NumeroDb(myPOSTreader.Item("AscensioneMM"))
            Ascensione = StringaDb(myPOSTreader.Item("Ascensione"))
            DominiGG = NumeroDb(myPOSTreader.Item("DominiGG"))
            DominiMM = NumeroDb(myPOSTreader.Item("DominiMM"))
            Domini = StringaDb(myPOSTreader.Item("Domini"))
            PatronoGG = NumeroDb(myPOSTreader.Item("PatronoGG"))
            PatronoMM = NumeroDb(myPOSTreader.Item("PatronoMM"))
            Patrono = StringaDb(myPOSTreader.Item("Patrono"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
