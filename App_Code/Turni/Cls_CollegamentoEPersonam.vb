Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CollegamentoEPersonam
    Public Id As Long
    Public DataRegistrazione As Date
    Public Dipendente As String
    Public Nucleo As String
    Public Dalle As Date
    Public Alle As Date
    Public OreMin As Date
    Public Giustificativo As String
    Public Note As String
    Public ModificaManuale As Integer

    Public Sub Pulisci()
        Id = 0
        DataRegistrazione = Nothing
        Dipendente = ""
        Nucleo = ""
        Dalle = Nothing
        Alle = Nothing
        OreMin = Nothing
        Giustificativo = ""
        Note = ""
        ModificaManuale = 0
    End Sub
End Class
