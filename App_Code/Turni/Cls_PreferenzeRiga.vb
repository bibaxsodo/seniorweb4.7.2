Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_PreferenzeRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Validita As Date
    Public Riga As Short
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public PrimoGiustificativo As String
    Public SecondoGiustificativo As String
    Public TerzoGiustificativo As String
    Public QuartoGiustificativo As String
    Public QuintoGiustificativo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Validita = Nothing
        Riga = 0
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        PrimoGiustificativo = ""
        SecondoGiustificativo = ""
        TerzoGiustificativo = ""
        QuartoGiustificativo = ""
        QuintoGiustificativo = ""
    End Sub
End Class
