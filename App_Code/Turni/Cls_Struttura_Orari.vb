﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Orari
    Public GgSett(41) As Byte
    Public Richiesti(41) As String
    Public Variati(41) As String
    Public Modificati(41) As String
    Public Azzerati(41) As String
    Public OrdiniServizio(41) As String
    Public TipoFlessibilita(41) As String  ' Impostato da Orario Flessibile (P = Timbratura Presenza ; F = Orario Flessibile ; X = Orario Flessibile con flessibilità dell'intervallo)
    Public FasceOrarie(41) As String
    Public PrimoOrario(41) As String
    Public SecondoOrario(41) As String
    Public TerzoOrario(41) As String
    Public QuartoOrario(41) As String
    Public QuintoOrario(41) As String
    Public Dalle(9, 41) As String
    Public Alle(9, 41) As String
    Public DalleAlto(9, 41) As String
    Public AlleAlto(9, 41) As String
    Public DalleBasso(9, 41) As String
    Public AlleBasso(9, 41) As String
    Public DalleObbligo(9, 41) As String
    Public AlleObbligo(9, 41) As String
    Public Pausa(9, 41) As String
    Public Giustificativo(9, 41) As String
    Public NelGruppo(9, 41) As String
    Public Familiare(9, 41) As Byte
    Public Tipo(9, 41) As String
    Public TipoServizio(9, 41) As String
    Public GiornoSuccessivo(9, 41) As String

    Public Sub Pulisci()

        For ii = 0 To 41
            GgSett(ii) = 0
            Richiesti(ii) = ""
            Variati(ii) = ""
            Modificati(ii) = ""
            Azzerati(ii) = ""
            OrdiniServizio(ii) = ""
            TipoFlessibilita(ii) = ""
            FasceOrarie(ii) = ""
            PrimoOrario(ii) = ""
            SecondoOrario(ii) = ""
            TerzoOrario(ii) = ""
            QuartoOrario(ii) = ""
            QuintoOrario(ii) = ""
            For i = 0 To 9
                If i <= 5 Then NelGruppo(i, ii) = ""
                Dalle(i, ii) = ""
                Alle(i, ii) = ""
                DalleAlto(i, ii) = ""
                AlleAlto(i, ii) = ""
                DalleBasso(i, ii) = ""
                AlleBasso(i, ii) = ""
                DalleObbligo(i, ii) = ""
                AlleObbligo(i, ii) = ""
                Pausa(i, ii) = ""
                Giustificativo(i, ii) = ""
                Familiare(i, ii) = 0
                Tipo(i, ii) = ""
                TipoServizio(i, ii) = ""
                GiornoSuccessivo(i, ii) = ""
            Next i
        Next ii
    End Sub

End Class
