Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_DatiVariabili
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public CodiceVariabile As String
    Public Validita As Date
    Public ContenutoTesto As String
    Public ContenutoData As Date
    Public ContenutoNumero As Double
    Public Tipo As Integer
    Public TipoNumericoVirgola As String
    Public Note As String
    Public Provenienza As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        CodiceVariabile = ""
        Validita = Nothing
        ContenutoTesto = ""
        ContenutoData = Nothing
        ContenutoNumero = 0
        Tipo = 0
        TipoNumericoVirgola = ""
        Note = ""
        Provenienza = ""
    End Sub

    Public Sub Elimina(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM DatiVariabili WHERE " & _
                          " CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaDalAl(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xValiditaDal As Date, ByVal xValiditaAl As Date, ByVal xProvenienza As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM DatiVariabili WHERE " & _
                          " CodiceDipendente = ?" & _
                          " AND Validita >= ?" & _
                          " AND Validita <= ?" & _
                          " AND Provenienza = ?"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Validita", xValiditaDal)
        cmd.Parameters.AddWithValue("@Validita", xValiditaAl)
        cmd.Parameters.AddWithValue("@Provenienza", xProvenienza)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal ConnectionString As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM DatiVariabili WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", CodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE DatiVariabili SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " ContenutoTesto = ?," & _
                    " ContenutoData = ?," & _
                    " ContenutoNumero = ?," & _
                    " Tipo  = ?," & _
                    " TipoNumericoVirgola = ?," & _
                    " Note = ?," & _
                    " Provenienza  = ?" & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND CodiceVariabile = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@ContenutoTesto", ContenutoTesto)
            cmdw.Parameters.AddWithValue("@ContenutoData", IIf(Year(ContenutoData) > 1, ContenutoData, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ContenutoNumero", ContenutoNumero)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Parameters.AddWithValue("@Provenienza", Provenienza)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@CodiceVariabile", CodiceVariabile)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO DatiVariabili (Utente, DataAggiornamento, CodiceDipendente, CodiceVariabile, Validita, ContenutoTesto, ContenutoData, ContenutoNumero, Tipo, TipoNumericoVirgola, Note, Provenienza) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@CodiceVariabile", CodiceVariabile)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@ContenutoTesto", ContenutoTesto)
            cmdw.Parameters.AddWithValue("@ContenutoData", IIf(Year(ContenutoData) > 1, ContenutoData, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ContenutoNumero", ContenutoNumero)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Parameters.AddWithValue("@Provenienza", Provenienza)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal ConnectionString As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From DatiVariabili Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Disponibile", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Disponibile")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From DatiVariabili WHERE " & _
                           " CodiceDipendente = ?" & _
                           " AND CodiceVariabile = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            CodiceVariabile = StringaDb(myPOSTreader.Item("CodiceVariabile"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            ContenutoTesto = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            ContenutoData = DataDb(myPOSTreader.Item("ContenutoData"))
            ContenutoNumero = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            Tipo = NumeroDb(myPOSTreader.Item("Tipo"))
            TipoNumericoVirgola = StringaDb(myPOSTreader.Item("TipoNumericoVirgola"))
            Note = StringaDb(myPOSTreader.Item("Note"))
            Provenienza = StringaDb(myPOSTreader.Item("Provenienza"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub Trova(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select TOP 1 * FROM DatiVariabili WHERE " & _
                          " CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id DESC"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            CodiceVariabile = StringaDb(myPOSTreader.Item("CodiceVariabile"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            ContenutoTesto = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            ContenutoData = DataDb(myPOSTreader.Item("ContenutoData"))
            ContenutoNumero = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            Tipo = NumeroDb(myPOSTreader.Item("Tipo"))
            TipoNumericoVirgola = StringaDb(myPOSTreader.Item("TipoNumericoVirgola"))
            Note = StringaDb(myPOSTreader.Item("Note"))
            Provenienza = StringaDb(myPOSTreader.Item("Provenienza"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Familiari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
