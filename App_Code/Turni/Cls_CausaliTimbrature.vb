Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CausaliTimbrature
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As Integer
    Public Descrizione As String
    Public Tipo As String
    Public Qualita As String
    Public Propagazione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Descrizione = ""
        Tipo = ""
        Qualita = ""
        Propagazione = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM CausaliTimbrature WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM CausaliTimbrature WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE CausaliTimbrature SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?, " & _
                    " Tipo  = ?, " & _
                    " Qualita  = ?, " & _
                    " Propagazione  = ? " & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Qualita", Qualita)
            cmdw.Parameters.AddWithValue("@Propagazione", Propagazione)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO CausaliTimbrature (Utente, DataAggiornamento, Codice, Descrizione, Tipo, Qualita, Propagazione) VALUES (?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Qualita", Qualita)
            cmdw.Parameters.AddWithValue("@Propagazione", Propagazione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From CausaliTimbrature Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Qualita", GetType(String))
        Tabella.Columns.Add("Propagazione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Tipo")
            myriga(3) = myPOSTreader.Item("Qualita")
            myriga(4) = myPOSTreader.Item("Propagazione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From CausaliTimbrature WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = NumeroDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Tipo = StringaDb(myPOSTreader.Item("Tipo"))
            Qualita = StringaDb(myPOSTreader.Item("Qualita"))
            Propagazione = StringaDb(myPOSTreader.Item("Propagazione"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As Integer) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Count(*) FROM TimbratureOriginali" & _
                          " WHERE Causale = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "Select Count(*) FROM TimbratureManipolate" & _
                          " WHERE Causale = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Codice", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "Select Count(*) FROM FormuleCausaliTimbrature" & _
                          " WHERE Codice = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Codice", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function CampoCausaliTimbrature(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        CampoCausaliTimbrature = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & xCampo & " FROM CausaliTimbrature WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM CausaliTimbrature" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM CausaliTimbrature" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
