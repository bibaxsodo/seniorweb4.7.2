Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaOrdineComandanti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public GruppoComandante As String
    Public TipoComandante As String
    Public Riga As Short
    Public Ordine As Short
    Public Specchio As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        GruppoComandante = ""
        TipoComandante = ""
        Riga = 0
        Ordine = 0
        Specchio = ""
    End Sub
End Class
