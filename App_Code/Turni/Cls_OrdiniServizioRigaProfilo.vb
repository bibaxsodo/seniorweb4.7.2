Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaProfilo
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Riga As Short
    Public Dalle As Date
    Public Alle As Date
    Public Pausa As Date
    Public Giustificativo As String
    Public GiornoSuccessivo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Riga = 0
        Dalle = Nothing
        Alle = Nothing
        Pausa = Nothing
        Giustificativo = ""
        GiornoSuccessivo = ""
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

End Class
