Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_SchedaRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceScheda As String
    Public CodiceTipoDatoAnagrafico As String
    Public Ordine As Integer

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceScheda = ""
        CodiceTipoDatoAnagrafico = ""
        Ordine = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceScheda As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM SchedaRiga WHERE " & _
                           "CodiceScheda = ?")
        cmd.Parameters.AddWithValue("@CodiceScheda", xCodiceScheda)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM SchedaRiga WHERE CodiceScheda = ?"
        cmd.Parameters.AddWithValue("@CodiceScheda", CodiceScheda)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE SchedaRiga SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " CodiceTipoDatoAnagrafico = ?, " & _
                    " Ordine  = ? " & _
                    " WHERE CodiceScheda = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceTipoDatoAnagrafico", CodiceTipoDatoAnagrafico)
            cmdw.Parameters.AddWithValue("@Ordine", Ordine)
            cmdw.Parameters.AddWithValue("@CodiceScheda", CodiceScheda)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO SchedaRiga (Utente, DataAggiornamento, CodiceScheda, CodiceTipoDatoAnagrafico, Ordine) VALUES (?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceScheda", CodiceScheda)
            cmdw.Parameters.AddWithValue("@CodiceTipoDatoAnagrafico", CodiceTipoDatoAnagrafico)
            cmdw.Parameters.AddWithValue("@Ordine", Ordine)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From SchedaRiga" & _
                           " WHERE CodiceScheda = ?" & _
                           " Order By CodiceTipoDatoAnagrafico")
        cmd.Parameters.AddWithValue("@CodiceScheda", xCodice)

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceScheda", GetType(String))
        Tabella.Columns.Add("CodiceTipoDatoAnagrafico", GetType(String))
        Tabella.Columns.Add("Ordine", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("CodiceScheda")
            myriga(1) = myPOSTreader.Item("CodiceTipoDatoAnagrafico")
            myriga(2) = myPOSTreader.Item("Ordine")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceScheda As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From SchedaRiga WHERE " & _
                           "CodiceScheda = ?")
        cmd.Parameters.AddWithValue("@CodiceScheda", xCodiceScheda)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceScheda = StringaDb(myPOSTreader.Item("CodiceScheda"))
            CodiceTipoDatoAnagrafico = StringaDb(myPOSTreader.Item("CodiceTipoDatoAnagrafico"))
            Ordine = NumeroDb(myPOSTreader.Item("Ordine"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT CodiceScheda, CodiceTipoDatoAnagrafico FROM SchedaRiga ORDER BY CodiceTipoDatoAnagrafico")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("CodiceTipoDatoAnagrafico"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceScheda")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

End Class
