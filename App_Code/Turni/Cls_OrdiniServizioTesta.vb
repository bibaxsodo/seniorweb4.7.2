Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioTesta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Commento As String
    Public Stato As String
    Public ProfiliAggiornatiUtente As String
    Public ProfiliAggiornatiDataAggiornamento As Date
    Public Canale As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Commento = ""
        Stato = ""
        ProfiliAggiornatiUtente = ""
        ProfiliAggiornatiDataAggiornamento = Nothing
        Canale = ""
    End Sub
End Class
