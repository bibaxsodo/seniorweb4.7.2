Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_StrutturaProfiliOrariRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Riga As Short
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public PrimoGiustificativo As String
    Public SecondoGiustificativo As String
    Public TerzoGiustificativo As String
    Public QuartoGiustificativo As String
    Public QuintoGiustificativo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Riga = 0
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        PrimoGiustificativo = ""
        SecondoGiustificativo = ""
        TerzoGiustificativo = ""
        QuartoGiustificativo = ""
        QuintoGiustificativo = ""
    End Sub

    Function Conta(ByVal StringaConnessione As String, ByVal xCodice As String) As Long
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT COUNT (*) FROM StrutturaProfiliOrariRiga" & _
                          " WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read() Then
            Conta = NumeroDb(myPOSTreader.Item(0))
        Else
            Conta = 0
        End If
        cn.Close()
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function
End Class
