Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TimbratureManipolate
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Data As Date
    Public Orario As Date
    Public EntrataUscita As String
    Public Causale As Short
    Public Rilevatore As Short
    Public Manuale As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Data = Nothing
        Orario = Nothing
        EntrataUscita = ""
        Causale = 0
        Rilevatore = 0
        Manuale = ""
    End Sub

End Class
