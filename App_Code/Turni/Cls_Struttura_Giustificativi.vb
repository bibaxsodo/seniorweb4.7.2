﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Giustificativi
    Public Quantita(2, 31) As Double
    Public Orario(2, 31) As String
    Public Giustificativo(2, 31) As String

    Public Sub Pulisci()

        For i = 0 To 2
            For g = 0 To 31
                Quantita(i, g) = 0
                Orario(i, g) = ""
                Giustificativo(i, g) = ""
            Next g
        Next i
    End Sub
End Class
