Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_MovimentiApparati
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Gruppo As String
    Public Validita As Date

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Gruppo = ""
        Validita = Nothing
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xGruppo As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM MovimentiApparati WHERE Codice = ? And Gruppo = ? And Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM MovimentiApparati WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM MovimentiApparati WHERE Codice = ? AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE MovimentiApparati SET " & _
                                " Utente = ?, " & _
                                " DataAggiornamento  = ?," & _
                                " Gruppo = ? " & _
                                " WHERE Codice = ?" & _
                                " AND Validita = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO MovimentiApparati (Utente, DataAggiornamento, Codice, Gruppo, Validita) VALUES (?,?,?,?,?)"
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("SELECT * From MovimentiApparati WHERE Codice = ?" & _
                " Order By Validita")

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", xCodice)

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Gruppo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("Validita"), "dd/MM/yyyy")
            myriga(1) = myPOSTreader.Item("Gruppo")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xGruppo As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT * From MovimentiApparati WHERE " & _
                           "Codice = ?") & _
                           " Gruppo  = ?" & _
                           " Validita  = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xGruppo As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Count(*) FROM OrdiniServizioRigaApparati" & _
                          " WHERE Apparato = ?" & _
                          " AND Gruppo = ?" & _
                          " AND Data >= ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Data", xValidita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
