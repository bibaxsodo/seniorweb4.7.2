﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Timbratura
    Public Dalle As String
    Public Dalle_Causale As String
    Public Dalle_Colore As Long
    Public Alle As String
    Public Alle_Causale As String
    Public Alle_Colore As Long
    Public Proseguimento As String

    Public Sub Pulisci()
        Dalle = ""
        Dalle_Causale = ""
        Dalle_Colore = 0
        Alle = ""
        Alle_Causale = ""
        Alle_Colore = 0
        Proseguimento = ""
    End Sub
End Class
