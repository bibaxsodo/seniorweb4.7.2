Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProfiliFerieRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Riga As Short
    Public PrimoIntervallo As String
    Public SecondoIntervallo As String
    Public TerzoIntervallo As String
    Public QuartoIntervallo As String
    Public QuintoIntervallo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Riga = 0
        PrimoIntervallo = ""
        SecondoIntervallo = ""
        TerzoIntervallo = ""
        QuartoIntervallo = ""
        QuintoIntervallo = ""
    End Sub

    Function Conta(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date) As Long
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT COUNT (*) FROM ProfiliFerieRiga" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Conta = NumeroDb(myPOSTreader.Item(0))
        Else
            Conta = 0
        End If
        cn.Close()
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function
End Class
