Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CondizioniContrattuali
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public OrarioDiurnoDalle As Date
    Public OrarioDiurnoAlle As Date
    Public ControlloOrario As String
    Public CalcoloPresenzeMaggiorazioni As String
    Public TimbratureProfili As String
    Public ControlloTimbrature As String
    Public FestivitaSettimanale As Byte
    Public FestivitaInfrasettimanali As String
    Public OrarioLunedi As Date
    Public OrarioMartedi As Date
    Public OrarioMercoledi As Date
    Public OrarioGiovedi As Date
    Public OrarioVenerdi As Date
    Public OrarioSabato As Date
    Public OrarioDomenica As Date
    Public PartimeLunedi As String
    Public PartimeMartedi As String
    Public PartimeMercoledi As String
    Public PartimeGiovedi As String
    Public PartimeVenerdi As String
    Public PartimeSabato As String
    Public PartimeDomenica As String
    Public PercentualePartime As Single
    Public PercentualePartimeMese As Single
    Public CodiceProfiloOrario As String
    Public PercentualePartimeGiorni As Single
    Public PercentualePartimeGiorniMese As Single
    Public PercentualePartimeOre As Single
    Public PercentualePartimeOreMese As Single
    Public RiposoOgni As Byte
    Public TipoPartime As String
    Public RiposoFestivoSettimane As Byte
    Public OrarioContrattuale As Date
    Public TraDueRiposi As Single

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        OrarioDiurnoDalle = Nothing
        OrarioDiurnoAlle = Nothing
        ControlloOrario = ""
        CalcoloPresenzeMaggiorazioni = ""
        TimbratureProfili = ""
        ControlloTimbrature = ""
        FestivitaSettimanale = 0
        FestivitaInfrasettimanali = ""
        OrarioLunedi = Nothing
        OrarioMartedi = Nothing
        OrarioMercoledi = Nothing
        OrarioGiovedi = Nothing
        OrarioVenerdi = Nothing
        OrarioSabato = Nothing
        OrarioDomenica = Nothing
        PartimeLunedi = ""
        PartimeMartedi = ""
        PartimeMercoledi = ""
        PartimeGiovedi = ""
        PartimeVenerdi = ""
        PartimeSabato = ""
        PartimeDomenica = ""
        PercentualePartime = 0
        PercentualePartimeMese = 0
        CodiceProfiloOrario = ""
        PercentualePartimeGiorni = 0
        PercentualePartimeGiorniMese = 0
        PercentualePartimeOre = 0
        PercentualePartimeOreMese = 0
        RiposoOgni = 0
        TipoPartime = ""
        RiposoFestivoSettimane = 0
        OrarioContrattuale = Nothing
        TraDueRiposi = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM CondizioniContrattuali WHERE Codice = ? AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)

        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM CondizioniContrattuali WHERE Codice = ? AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            cmd.CommandText = "UPDATE CondizioniContrattuali SET " & _
                              " Utente = ?, " & _
                              " DataAggiornamento = ?," & _
                              " OrarioDiurnoDalle = ?," & _
                              " OrarioDiurnoAlle = ?," & _
                              " ControlloOrario = ?," & _
                              " CalcoloPresenzeMaggiorazioni  = ?," & _
                              " TimbratureProfili = ?," & _
                              " ControlloTimbrature = ?," & _
                              " FestivitaSettimanale = ?," & _
                              " FestivitaInfrasettimanali = ?," & _
                              " OrarioLunedi = ?," & _
                              " OrarioMartedi = ?," & _
                              " OrarioMercoledi = ?," & _
                              " OrarioGiovedi = ?," & _
                              " OrarioVenerdi = ?," & _
                              " OrarioSabato = ?," & _
                              " OrarioDomenica = ?," & _
                              " PartimeLunedi = ?," & _
                              " PartimeMartedi = ?," & _
                              " PartimeMercoledi = ?," & _
                              " PartimeGiovedi = ?," & _
                              " PartimeVenerdi = ?," & _
                              " PartimeSabato = ?," & _
                              " PartimeDomenica = ?," & _
                              " PercentualePartime = ?," & _
                              " PercentualePartimeMese = ?," & _
                              " CodiceProfiloOrario = ?," & _
                              " PercentualePartimeGiorni = ?," & _
                              " PercentualePartimeGiorniMese = ?," & _
                              " PercentualePartimeOre = ?," & _
                              " PercentualePartimeOreMese = ?," & _
                              " RiposoOgni = ?," & _
                              " TipoPartime = ?," & _
                              " RiposoFestivoSettimane = ?" & _
                              " OrarioCotrattuale = ?" & _
                              " TraDueRiposi = ?" & _
                              " WHERE Codice = ?" & _
                              " AND Validita = ?"

            cmd.Parameters.AddWithValue("@Utente", Utente)
            cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmd.Parameters.AddWithValue("@OrarioDiurnoDalle", OrarioDiurnoDalle)
            cmd.Parameters.AddWithValue("@OrarioDiurnoAlle", OrarioDiurnoAlle)
            cmd.Parameters.AddWithValue("@ControlloOrario", ControlloOrario)
            cmd.Parameters.AddWithValue("@CalcoloPresenzeMaggiorazioni", CalcoloPresenzeMaggiorazioni)
            cmd.Parameters.AddWithValue("@TimbratureProfili", TimbratureProfili)
            cmd.Parameters.AddWithValue("@ControlloTimbrature", ControlloTimbrature)
            cmd.Parameters.AddWithValue("@FestivitaSettimanale", FestivitaSettimanale)
            cmd.Parameters.AddWithValue("@FestivitaInfrasettimanali", FestivitaInfrasettimanali)
            cmd.Parameters.AddWithValue("@OrarioLunedi", OrarioLunedi)
            cmd.Parameters.AddWithValue("@OrarioMartedi", OrarioMartedi)
            cmd.Parameters.AddWithValue("@OrarioMercoledi", OrarioMercoledi)
            cmd.Parameters.AddWithValue("@OrarioGiovedi", OrarioGiovedi)
            cmd.Parameters.AddWithValue("@OrarioVenerdi", OrarioVenerdi)
            cmd.Parameters.AddWithValue("@OrarioSabato", OrarioSabato)
            cmd.Parameters.AddWithValue("@OrarioDomenica", OrarioDomenica)
            cmd.Parameters.AddWithValue("@PartimeLunedi", PartimeLunedi)
            cmd.Parameters.AddWithValue("@PartimeMartedi", PartimeMartedi)
            cmd.Parameters.AddWithValue("@PartimeMercoledi", PartimeMercoledi)
            cmd.Parameters.AddWithValue("@PartimeGiovedi", PartimeGiovedi)
            cmd.Parameters.AddWithValue("@PartimeVenerdi", PartimeVenerdi)
            cmd.Parameters.AddWithValue("@PartimeSabato", PartimeSabato)
            cmd.Parameters.AddWithValue("@PartimeDomenica", PartimeDomenica)
            cmd.Parameters.AddWithValue("@PercentualePartime", PercentualePartime)
            cmd.Parameters.AddWithValue("@PercentualePartimeMese", PercentualePartimeMese)
            cmd.Parameters.AddWithValue("@CodiceProfiloOrario", CodiceProfiloOrario)
            cmd.Parameters.AddWithValue("@PercentualePartimeGiorni", PercentualePartimeGiorni)
            cmd.Parameters.AddWithValue("@PercentualePartimeGiorniMese", PercentualePartimeGiorniMese)
            cmd.Parameters.AddWithValue("@PercentualePartimeOre", PercentualePartimeOre)
            cmd.Parameters.AddWithValue("@PercentualePartimeOreMese", PercentualePartimeOreMese)
            cmd.Parameters.AddWithValue("@RiposoOgni", RiposoOgni)
            cmd.Parameters.AddWithValue("@TipoPartime", TipoPartime)
            cmd.Parameters.AddWithValue("@RiposoFestivoSettimane", RiposoFestivoSettimane)
            cmd.Parameters.AddWithValue("@OrarioContrattuale", OrarioContrattuale)
            cmd.Parameters.AddWithValue("@TraDueRiposi", TraDueRiposi)
            cmd.Parameters.AddWithValue("@Codice", Codice)
            cmd.Parameters.AddWithValue("@Validita", Validita)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()
        Else
            cmd.CommandText = "INSERT INTO CondizioniContrattuali (Utente, DataAggiornamento, Codice, Validita, OrarioDiurnoDalle, OrarioDiurnoAlle, ControlloOrario, CalcoloPresenzeMaggiorazioni, TimbratureProfili, ControlloTimbrature, FestivitaSettimanale, FestivitaInfrasettimanali, OrarioLunedi, OrarioMartedi, OrarioMercoledi, OrarioGiovedi, OrarioVenerdi, OrarioSabato, OrarioDomenica, PartimeLunedi, PartimeMartedi, PartimeMercoledi, PartimeGiovedi, PartimeVenerdi, PartimeSabato, PartimeDomenica, PercentualePartime, PercentualePartimeMese, CodiceProfiloOrario, PercentualePartimeGiorni, PercentualePartimeGiorniMese, PercentualePartimeOre, PercentualePartimeOreMese, RiposoOgni, TipoPartime, RiposoFestivoSettimane, OrarioContrattuale, TraDueRiposi) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmd.Parameters.AddWithValue("@Utente", Utente)
            cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmd.Parameters.AddWithValue("@Codice", Codice)
            cmd.Parameters.AddWithValue("@Validita", Validita)
            cmd.Parameters.AddWithValue("@OrarioDiurnoDalle", IIf(Year(OrarioDiurnoDalle) > 1, OrarioDiurnoDalle, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioDiurnoAlle", IIf(Year(OrarioDiurnoAlle) > 1, OrarioDiurnoAlle, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@ControlloOrario", ControlloOrario)
            cmd.Parameters.AddWithValue("@CalcoloPresenzeMaggiorazioni", CalcoloPresenzeMaggiorazioni)
            cmd.Parameters.AddWithValue("@TimbratureProfili", TimbratureProfili)
            cmd.Parameters.AddWithValue("@ControlloTimbrature", ControlloTimbrature)
            cmd.Parameters.AddWithValue("@FestivitaSettimanale", FestivitaSettimanale)
            cmd.Parameters.AddWithValue("@FestivitaInfrasettimanali", FestivitaInfrasettimanali)
            cmd.Parameters.AddWithValue("@OrarioLunedi", IIf(Year(OrarioLunedi) > 1, OrarioLunedi, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioMartedi", IIf(Year(OrarioMartedi) > 1, OrarioMartedi, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioMercoledi", IIf(Year(OrarioMercoledi) > 1, OrarioMercoledi, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioGiovedi", IIf(Year(OrarioGiovedi) > 1, OrarioGiovedi, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioVenerdi", IIf(Year(OrarioVenerdi) > 1, OrarioVenerdi, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioSabato", IIf(Year(OrarioSabato) > 1, OrarioSabato, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@OrarioDomenica", IIf(Year(OrarioDomenica) > 1, OrarioDomenica, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@PartimeLunedi", PartimeLunedi)
            cmd.Parameters.AddWithValue("@PartimeMartedi", PartimeMartedi)
            cmd.Parameters.AddWithValue("@PartimeMercoledi", PartimeMercoledi)
            cmd.Parameters.AddWithValue("@PartimeGiovedi", PartimeGiovedi)
            cmd.Parameters.AddWithValue("@PartimeVenerdi", PartimeVenerdi)
            cmd.Parameters.AddWithValue("@PartimeSabato", PartimeSabato)
            cmd.Parameters.AddWithValue("@PartimeDomenica", PartimeDomenica)
            cmd.Parameters.AddWithValue("@PercentualePartime", PercentualePartime)
            cmd.Parameters.AddWithValue("@PercentualePartimeMese", PercentualePartimeMese)
            cmd.Parameters.AddWithValue("@CodiceProfiloOrario", CodiceProfiloOrario)
            cmd.Parameters.AddWithValue("@PercentualePartimeGiorni", PercentualePartimeGiorni)
            cmd.Parameters.AddWithValue("@PercentualePartimeGiorniMese", PercentualePartimeGiorniMese)
            cmd.Parameters.AddWithValue("@PercentualePartimeOre", PercentualePartimeOre)
            cmd.Parameters.AddWithValue("@PercentualePartimeOreMese", PercentualePartimeOreMese)
            cmd.Parameters.AddWithValue("@RiposoOgni", RiposoOgni)
            cmd.Parameters.AddWithValue("@TipoPartime", TipoPartime)
            cmd.Parameters.AddWithValue("@RiposoFestivoSettimane", RiposoFestivoSettimane)
            cmd.Parameters.AddWithValue("@OrarioContrattuale", IIf(Year(OrarioContrattuale) > 1, OrarioContrattuale, System.DBNull.Value))
            cmd.Parameters.AddWithValue("@TraDueRiposi", TraDueRiposi)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xDescrizione As String, ByVal Tabella As System.Data.DataTable)

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("OrarioDiurnoDalle", GetType(String))
        Tabella.Columns.Add("OrarioDiurnoAlle", GetType(String))
        Tabella.Columns.Add("ControlloOrario", GetType(String))
        Tabella.Columns.Add("CalcoloPresenzeMaggiorazioni", GetType(String))
        Tabella.Columns.Add("TimbratureProfili", GetType(String))
        Tabella.Columns.Add("ControlloTimbrature", GetType(String))
        Tabella.Columns.Add("FestivitaSettimanale", GetType(String))
        Tabella.Columns.Add("FestivitaInfrasettimanali", GetType(String))
        Tabella.Columns.Add("OrarioLunedi", GetType(String))
        Tabella.Columns.Add("OrarioMartedi", GetType(String))
        Tabella.Columns.Add("OrarioMercoledi", GetType(String))
        Tabella.Columns.Add("OrarioGiovedi", GetType(String))
        Tabella.Columns.Add("OrarioVenerdi", GetType(String))
        Tabella.Columns.Add("OrarioSabato", GetType(String))
        Tabella.Columns.Add("OrarioDomenica", GetType(String))
        Tabella.Columns.Add("PartimeLunedi", GetType(String))
        Tabella.Columns.Add("PartimeMartedi", GetType(String))
        Tabella.Columns.Add("PartimeMercoledi", GetType(String))
        Tabella.Columns.Add("PartimeGiovedi", GetType(String))
        Tabella.Columns.Add("PartimeVenerdi", GetType(String))
        Tabella.Columns.Add("PartimeSabato", GetType(String))
        Tabella.Columns.Add("PartimeDomenica", GetType(String))
        Tabella.Columns.Add("PercentualePartime", GetType(String))
        Tabella.Columns.Add("PercentualePartimeMese", GetType(String))
        Tabella.Columns.Add("CodiceProfiloOrario", GetType(String))
        Tabella.Columns.Add("PercentualePartimeGiorni", GetType(String))
        Tabella.Columns.Add("PercentualePartimeGiorniMese", GetType(String))
        Tabella.Columns.Add("PercentualePartimeOre", GetType(String))
        Tabella.Columns.Add("PercentualePartimeOreMese", GetType(String))
        Tabella.Columns.Add("RiposoOgni", GetType(String))
        Tabella.Columns.Add("TipoPartime", GetType(String))
        Tabella.Columns.Add("RiposoFestivoSettimane", GetType(String))
        Tabella.Columns.Add("OrarioContrattuale", GetType(String))
        Tabella.Columns.Add("TraDueRiposi", GetType(String))

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()

        If xDescrizione <> "" Then
            cmd.CommandText = "SELECT CondizioniContrattuali.*, Contratti.Descrizione FROM CondizioniContrattuali INNER JOIN Contratti ON CondizioniContrattuali.Codice = Contratti.Codice WHERE Contratti.Descrizione LIKE '" & Duplica_Accenti(xDescrizione) & "' ORDER BY Descrizione, Validita"
        Else
            cmd.CommandText = "SELECT CondizioniContrattuali.*, Contratti.Descrizione FROM CondizioniContrattuali INNER JOIN Contratti ON CondizioniContrattuali.Codice = Contratti.Codice ORDER BY Descrizione, Validita"
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = Format(myPOSTreader.Item("Validita"), "dd/MM/yyyy")
            myriga(3) = Format(myPOSTreader.Item("OrarioDiurnoDalle"), "HH.mm")
            myriga(4) = Format(myPOSTreader.Item("OrarioDiurnoAlle"), "HH.mm")
            myriga(5) = myPOSTreader.Item("ControlloOrario")
            myriga(6) = myPOSTreader.Item("CalcoloPresenzeMaggiorazioni")
            myriga(7) = myPOSTreader.Item("TimbratureProfili")
            myriga(8) = myPOSTreader.Item("ControlloTimbrature")
            myriga(9) = myPOSTreader.Item("FestivitaSettimanale")
            myriga(10) = myPOSTreader.Item("FestivitaInfrasettimanali")
            myriga(11) = Format(myPOSTreader.Item("OrarioLunedi"), "HH.mm")
            myriga(12) = Format(myPOSTreader.Item("OrarioMartedi"), "HH.mm")
            myriga(13) = Format(myPOSTreader.Item("OrarioMercoledi"), "HH.mm")
            myriga(14) = Format(myPOSTreader.Item("OrarioGiovedi"), "HH.mm")
            myriga(15) = Format(myPOSTreader.Item("OrarioVenerdi"), "HH.mm")
            myriga(16) = Format(myPOSTreader.Item("OrarioSabato"), "HH.mm")
            myriga(17) = Format(myPOSTreader.Item("OrarioDomenica"), "HH.mm")
            myriga(18) = myPOSTreader.Item("PartimeLunedi")
            myriga(19) = myPOSTreader.Item("PartimeMartedi")
            myriga(20) = myPOSTreader.Item("PartimeMercoledi")
            myriga(21) = myPOSTreader.Item("PartimeGiovedi")
            myriga(22) = myPOSTreader.Item("PartimeVenerdi")
            myriga(23) = myPOSTreader.Item("PartimeSabato")
            myriga(24) = myPOSTreader.Item("PartimeDomenica")
            myriga(25) = Format(NumeroDb(myPOSTreader.Item("PercentualePartime")) * 100, "0.000")
            myriga(26) = Format(NumeroDb(myPOSTreader.Item("PercentualePartimeMese")) * 100, "0.000")
            myriga(27) = myPOSTreader.Item("CodiceProfiloOrario")
            myriga(28) = Format(NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni")) * 100, "0.000")
            myriga(29) = Format(NumeroDb(myPOSTreader.Item("PercentualePartimeGiorniMese")) * 100, "0.000")
            myriga(30) = Format(NumeroDb(myPOSTreader.Item("PercentualePartimeOre")) * 100, "0.000")
            myriga(31) = Format(NumeroDb(myPOSTreader.Item("PercentualePartimeOreMese")) * 100, "0.000")
            myriga(32) = myPOSTreader.Item("RiposoOgni")
            myriga(33) = myPOSTreader.Item("TipoPartime")
            myriga(34) = myPOSTreader.Item("RiposoFestivoSettimane")
            myriga(35) = Format(myPOSTreader.Item("OrarioContrattuale"), "HH.mm")
            myriga(36) = Format(NumeroDb(myPOSTreader.Item("TraDueRiposi")) * 100, "0.000")
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From CondizioniContrattuali WHERE Codice = ? AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            OrarioDiurnoDalle = DataDb(myPOSTreader.Item("OrarioDiurnoDalle"))
            OrarioDiurnoAlle = DataDb(myPOSTreader.Item("OrarioDiurnoAlle"))
            ControlloOrario = StringaDb(myPOSTreader.Item("ControlloOrario"))
            CalcoloPresenzeMaggiorazioni = StringaDb(myPOSTreader.Item("CalcoloPresenzeMaggiorazioni"))
            TimbratureProfili = StringaDb(myPOSTreader.Item("TimbratureProfili"))
            ControlloTimbrature = StringaDb(myPOSTreader.Item("ControlloTimbrature"))
            FestivitaSettimanale = NumeroDb(myPOSTreader.Item("FestivitaSettimanale"))
            FestivitaInfrasettimanali = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
            OrarioLunedi = DataDb(myPOSTreader.Item("OrarioLunedi"))
            OrarioMartedi = DataDb(myPOSTreader.Item("OrarioMartedi"))
            OrarioMercoledi = DataDb(myPOSTreader.Item("OrarioMercoledi"))
            OrarioGiovedi = DataDb(myPOSTreader.Item("OrarioGiovedi"))
            OrarioVenerdi = DataDb(myPOSTreader.Item("OrarioVenerdi"))
            OrarioSabato = DataDb(myPOSTreader.Item("OrarioSabato"))
            OrarioDomenica = DataDb(myPOSTreader.Item("OrarioDomenica"))
            PartimeLunedi = StringaDb(myPOSTreader.Item("PartimeLunedi"))
            PartimeMartedi = StringaDb(myPOSTreader.Item("PartimeMartedi"))
            PartimeMercoledi = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
            PartimeGiovedi = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
            PartimeVenerdi = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
            PartimeSabato = StringaDb(myPOSTreader.Item("PartimeSabato"))
            PartimeDomenica = StringaDb(myPOSTreader.Item("PartimeDomenica"))
            PercentualePartime = NumeroDb(myPOSTreader.Item("PercentualePartime"))
            PercentualePartimeMese = NumeroDb(myPOSTreader.Item("PercentualePartimeMese"))
            CodiceProfiloOrario = StringaDb(myPOSTreader.Item("CodiceProfiloOrario"))
            PercentualePartimeGiorni = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
            PercentualePartimeGiorniMese = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorniMese"))
            PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
            PercentualePartimeOreMese = NumeroDb(myPOSTreader.Item("PercentualePartimeOreMese"))
            RiposoOgni = NumeroDb(myPOSTreader.Item("RiposoOgni"))
            TipoPartime = StringaDb(myPOSTreader.Item("TipoPartime"))
            RiposoFestivoSettimane = NumeroDb(myPOSTreader.Item("RiposoFestivoSettimane"))
            OrarioContrattuale = DataDb(myPOSTreader.Item("OrarioContrattuale"))
            TraDueRiposi = NumeroDb(myPOSTreader.Item("TraDueRiposi"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Trova(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM CondizioniContrattuali" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            OrarioDiurnoDalle = DataDb(myPOSTreader.Item("OrarioDiurnoDalle"))
            OrarioDiurnoAlle = DataDb(myPOSTreader.Item("OrarioDiurnoAlle"))
            ControlloOrario = StringaDb(myPOSTreader.Item("ControlloOrario"))
            CalcoloPresenzeMaggiorazioni = StringaDb(myPOSTreader.Item("CalcoloPresenzeMaggiorazioni"))
            TimbratureProfili = StringaDb(myPOSTreader.Item("TimbratureProfili"))
            ControlloTimbrature = StringaDb(myPOSTreader.Item("ControlloTimbrature"))
            FestivitaSettimanale = NumeroDb(myPOSTreader.Item("FestivitaSettimanale"))
            FestivitaInfrasettimanali = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
            OrarioLunedi = DataDb(myPOSTreader.Item("OrarioLunedi"))
            OrarioMartedi = DataDb(myPOSTreader.Item("OrarioMartedi"))
            OrarioMercoledi = DataDb(myPOSTreader.Item("OrarioMercoledi"))
            OrarioGiovedi = DataDb(myPOSTreader.Item("OrarioGiovedi"))
            OrarioVenerdi = DataDb(myPOSTreader.Item("OrarioVenerdi"))
            OrarioSabato = DataDb(myPOSTreader.Item("OrarioSabato"))
            OrarioDomenica = DataDb(myPOSTreader.Item("OrarioDomenica"))
            PartimeLunedi = StringaDb(myPOSTreader.Item("PartimeLunedi"))
            PartimeMartedi = StringaDb(myPOSTreader.Item("PartimeMartedi"))
            PartimeMercoledi = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
            PartimeGiovedi = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
            PartimeVenerdi = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
            PartimeSabato = StringaDb(myPOSTreader.Item("PartimeSabato"))
            PartimeDomenica = StringaDb(myPOSTreader.Item("PartimeDomenica"))
            PercentualePartime = NumeroDb(myPOSTreader.Item("PercentualePartime"))
            PercentualePartimeMese = NumeroDb(myPOSTreader.Item("PercentualePartimeMese"))
            CodiceProfiloOrario = StringaDb(myPOSTreader.Item("CodiceProfiloOrario"))
            PercentualePartimeGiorni = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
            PercentualePartimeGiorniMese = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorniMese"))
            PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
            PercentualePartimeOreMese = NumeroDb(myPOSTreader.Item("PercentualePartimeOreMese"))
            RiposoOgni = NumeroDb(myPOSTreader.Item("RiposoOgni"))
            TipoPartime = StringaDb(myPOSTreader.Item("TipoPartime"))
            RiposoFestivoSettimane = NumeroDb(myPOSTreader.Item("RiposoFestivoSettimane"))
            OrarioContrattuale = DataDb(myPOSTreader.Item("OrarioContrattuale"))
            TraDueRiposi = NumeroDb(myPOSTreader.Item("TraDueRiposi"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function Campo(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xCampo As String) As Object

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM CondizioniContrattuali" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Select Case xCampo
                Case "Codice"
                    Campo = StringaDb(myPOSTreader.Item("Codice"))
                Case "Validita"
                    Campo = DataDb(myPOSTreader.Item("Validita"))
                Case "OrarioDiurnoDalle"
                    Campo = DataDb(myPOSTreader.Item("OrarioDiurnoDalle"))
                Case "OrarioDiurnoAlle"
                    Campo = DataDb(myPOSTreader.Item("OrarioDiurnoAlle"))
                Case "ControlloOrario"
                    Campo = StringaDb(myPOSTreader.Item("ControlloOrario"))
                Case "CalcoloPresenzeMaggiorazioni"
                    Campo = StringaDb(myPOSTreader.Item("CalcoloPresenzeMaggiorazioni"))
                Case "TimbratureProfili"
                    Campo = StringaDb(myPOSTreader.Item("TimbratureProfili"))
                Case "ControlloTimbrature"
                    Campo = StringaDb(myPOSTreader.Item("ControlloTimbrature"))
                Case "FestivitaSettimanale"
                    Campo = NumeroDb(myPOSTreader.Item("FestivitaSettimanale"))
                Case "FestivitaInfrasettimanali"
                    Campo = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
                Case "OrarioLunedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioLunedi"))
                Case "OrarioMartedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioMartedi"))
                Case "OrarioMercoledi"
                    Campo = DataDb(myPOSTreader.Item("OrarioMercoledi"))
                Case "OrarioGiovedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioGiovedi"))
                Case "OrarioVenerdi"
                    Campo = DataDb(myPOSTreader.Item("OrarioVenerdi"))
                Case "OrarioSabato"
                    Campo = DataDb(myPOSTreader.Item("OrarioSabato"))
                Case "OrarioDomenica"
                    Campo = DataDb(myPOSTreader.Item("OrarioDomenica"))
                Case "PartimeLunedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeLunedi"))
                Case "PartimeMartedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeMartedi"))
                Case "PartimeMercoledi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
                Case "PartimeGiovedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
                Case "PartimeVenerdi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
                Case "PartimeSabato"
                    Campo = StringaDb(myPOSTreader.Item("PartimeSabato"))
                Case "PartimeDomenica"
                    Campo = StringaDb(myPOSTreader.Item("PartimeDomenica"))
                Case "PercentualePartime"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartime"))
                Case "PercentualePartimeMese"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeMese"))
                Case "CodiceProfiloOrario"
                    Campo = StringaDb(myPOSTreader.Item("CodiceProfiloOrario"))
                Case "PercentualePartimeGiorni"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
                Case "PercentualePartimeGiorniMese"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorniMese"))
                Case "PercentualePartimeOre"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
                Case "PercentualePartimeOreMese"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeOreMese"))
                Case "RiposoOgni"
                    Campo = NumeroDb(myPOSTreader.Item("RiposoOgni"))
                Case "TipoPartime"
                    Campo = StringaDb(myPOSTreader.Item("TipoPartime"))
                Case "RiposoFestivoSettimane"
                    Campo = NumeroDb(myPOSTreader.Item("RiposoFestivoSettimane"))
                Case "OrarioContrattuale"
                    Campo = DataDb(myPOSTreader.Item("OrarioContrattuale"))
                Case "TraDueRiposi"
                    Campo = NumeroDb(myPOSTreader.Item("TraDueRiposi"))
                Case Else
                    Campo = ""
            End Select
        Else
            Campo = ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Private Function Duplica_Accenti(ByVal xRiga As String) As String
        Dim Appoggio As String = ""
        For i = 1 To Len(xRiga)
            If Mid(xRiga, i, 1) <> "'" Then
                Appoggio = Appoggio + Mid(xRiga, i, 1)
            Else
                Appoggio = Appoggio + Mid(xRiga, i, 1) + Mid(xRiga, i, 1)
            End If
        Next i
        Duplica_Accenti = Appoggio
    End Function

End Class
