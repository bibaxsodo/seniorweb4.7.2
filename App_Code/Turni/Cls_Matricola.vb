Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Matricola
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Matricola As String
    Public DataAssunzione As Date
    Public DataLicenziamento As Date
    Public Note As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Matricola = ""
        DataAssunzione = Nothing
        DataLicenziamento = Nothing
        Note = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xDataAssunzione As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM Matricola WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND DataAssunzione = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", xDataAssunzione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM Matricola WHERE" & _
                           " CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Matricola WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND DataAssunzione = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", DataAssunzione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Matricola SET" & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Matricola = ?" & _
                    " DataLicenziamento = ?, " & _
                    " Note  = ? " & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND DataAssunzione = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Matricola", Matricola)
            cmdw.Parameters.AddWithValue("@DataLicenziamento", DataLicenziamento)
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@DataAssunzione", DataAssunzione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Matricola (Utente, DataAggiornamento, CodiceDipendente, Matricola, DataAssunzione, DataLicenziamento, Note) VALUES (?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@Matricola", Matricola)
            cmdw.Parameters.AddWithValue("@DataAssunzione", IIf(Year(DataAssunzione) > 1, DataAssunzione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@DataLicenziamento", IIf(Year(DataLicenziamento) > 1, DataLicenziamento, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From Matricola WHERE CodiceDipendente = ? Order By DataAssunzione DESC"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Matricola", GetType(String))
        Tabella.Columns.Add("Data Assunzione", GetType(String))
        Tabella.Columns.Add("Data Licenziamento", GetType(String))
        Tabella.Columns.Add("Note", GetType(String))
        Tabella.Columns.Add("DataAssunzione", GetType(Date))
        Tabella.Columns.Add("DataLicenziamento", GetType(Date))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim MyRiga As System.Data.DataRow = Tabella.NewRow()
            MyRiga(0) = myPOSTreader.Item("Matricola")
            MyRiga(1) = myPOSTreader.Item("DataAssunzione")
            MyRiga(2) = myPOSTreader.Item("DataLicenziamento")
            MyRiga(3) = myPOSTreader.Item("Note")
            If IsDate(myPOSTreader.Item("DataAssunzione")) Then
                MyRiga(4) = myPOSTreader.Item("DataAssunzione")
            Else
                MyRiga(4) = System.DBNull.Value
            End If
            If IsDate(myPOSTreader.Item("DataLicenziamento")) Then
                MyRiga(5) = myPOSTreader.Item("DataLicenziamento")
            Else
                MyRiga(5) = System.DBNull.Value
            End If
            Tabella.Rows.Add(MyRiga)
        Else
            Dim MyRiga As System.Data.DataRow = Tabella.NewRow()
            MyRiga(0) = ""
            MyRiga(1) = ""
            MyRiga(2) = ""
            MyRiga(3) = ""
            MyRiga(4) = Nothing
            MyRiga(5) = Nothing

            Tabella.Rows.Add(MyRiga)
        End If

        Do While myPOSTreader.Read

            Dim MyRiga As System.Data.DataRow = Tabella.NewRow()
            MyRiga(0) = myPOSTreader.Item("Matricola")
            MyRiga(1) = myPOSTreader.Item("DataAssunzione")
            MyRiga(2) = myPOSTreader.Item("DataLicenziamento")
            MyRiga(3) = myPOSTreader.Item("Note")
            If IsDate(myPOSTreader.Item("DataAssunzione")) Then
                MyRiga(4) = myPOSTreader.Item("DataAssunzione")
            Else
                MyRiga(4) = Nothing
            End If
            If IsDate(myPOSTreader.Item("DataLicenziamento")) Then
                MyRiga(5) = myPOSTreader.Item("DataLicenziamento")
            Else
                MyRiga(5) = Nothing
            End If
            Tabella.Rows.Add(MyRiga)
        Loop

        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xDataAssunzione As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Matricola WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND DataAssunzione = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", xDataAssunzione)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            Matricola = StringaDb(myPOSTreader.Item("Matricola"))
            DataAssunzione = DataDb(myPOSTreader.Item("DataAssunzione"))
            DataLicenziamento = DataDb(myPOSTreader.Item("DataLicenziamento"))
            Note = StringaDb(myPOSTreader.Item("Note"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Trova(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xData As Date, ByVal xTipo As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM Matricola" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND DataAssunzione " & xTipo & " ?" & _
                          " ORDER BY DataAssunzione DESC, Id"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", xData)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            Matricola = StringaDb(myPOSTreader.Item("Matricola"))
            DataAssunzione = DataDb(myPOSTreader.Item("DataAssunzione"))
            DataLicenziamento = DataDb(myPOSTreader.Item("DataLicenziamento"))
            Note = StringaDb(myPOSTreader.Item("Note"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function TrovaCodiceDipendente(ByVal StringaConnessione As String, ByVal xMatricola As String) As Integer
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Matricola" & _
                          " WHERE Matricola = ?"
        cmd.Parameters.AddWithValue("@Matricola", xMatricola)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return NumeroDb(myPOSTreader.Item("CodiceDipendente"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub Ultima(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM Matricola" & _
                          " WHERE CodiceDipendente = ?" & _
                          " ORDER BY DataAssunzione DESC, Id"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            Matricola = StringaDb(myPOSTreader.Item("Matricola"))
            DataAssunzione = DataDb(myPOSTreader.Item("DataAssunzione"))
            DataLicenziamento = DataDb(myPOSTreader.Item("DataLicenziamento"))
            Note = StringaDb(myPOSTreader.Item("Note"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
