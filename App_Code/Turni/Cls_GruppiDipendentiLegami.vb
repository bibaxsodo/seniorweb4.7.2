Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_GruppiDipendentiLegami
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public LegatoA As String
    Public NelServizio As String
    Public OrdiniServizio As String
    Public Comandante As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        LegatoA = ""
        NelServizio = ""
        OrdiniServizio = ""
        Comandante = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM GruppiDipendentiLegami" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * from GruppiDipendentiLegami" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE GruppiDipendentiLegami SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " LegatoA = ?," & _
                               " NelServizio = ?," & _
                               " OrdiniServizio = ?," & _
                               " Comandante = ?" & _
                               " WHERE Codice = ?" & _
                               " AND Validita = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@LegatoA", LegatoA)
            cmdw.Parameters.AddWithValue("@NelServizio", NelServizio)
            cmdw.Parameters.AddWithValue("@OrdiniServizio", OrdiniServizio)
            cmdw.Parameters.AddWithValue("@Comandante", Comandante)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO GruppiDipendentiLegami (Utente, DataAggiornamento, Codice, Validita, LegatoA, NelServizio, OrdiniServizio, Comandante) VALUES (?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@LegatoA", LegatoA)
            cmdw.Parameters.AddWithValue("@NelServizio", NelServizio)
            cmdw.Parameters.AddWithValue("@OrdiniServizio", OrdiniServizio)
            cmdw.Parameters.AddWithValue("@Comandante", Comandante)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim d As New Cls_GruppiDipendenti

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("LegatoA", GetType(String))
        Tabella.Columns.Add("NelServizio", GetType(String))
        Tabella.Columns.Add("OrdiniServizio", GetType(String))
        Tabella.Columns.Add("Comandante", GetType(String))
        Tabella.Columns.Add("DescrizioneCodice", GetType(String))
        Tabella.Columns.Add("DescrizioneLegatoA", GetType(String))
        Tabella.Columns.Add("DescrizioneNelServizio", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From GruppiDipendentiLegami Order By Codice, Validita")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = StringaDb(myPOSTreader.Item("Codice"))
            myriga(1) = Format(DataDb(myPOSTreader.Item("Validita")), "dd/MM/yyyy")
            myriga(2) = StringaDb(myPOSTreader.Item("LegatoA"))
            myriga(3) = StringaDb(myPOSTreader.Item("NelServizio"))
            myriga(4) = StringaDb(myPOSTreader.Item("OrdiniServizio"))
            myriga(5) = StringaDb(myPOSTreader.Item("Comandante"))
            myriga(6) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Codice")))
            myriga(7) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("LegatoA")))
            myriga(8) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("NelServizio")))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From GruppiDipendentiLegami" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            LegatoA = StringaDb(myPOSTreader.Item("LegatoA"))
            NelServizio = StringaDb(myPOSTreader.Item("NelServizio"))
            OrdiniServizio = StringaDb(myPOSTreader.Item("OrdiniServizio"))
            Comandante = StringaDb(myPOSTreader.Item("Comandante"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From GruppiDipendentiLegami" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
