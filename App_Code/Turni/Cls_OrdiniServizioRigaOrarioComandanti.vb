Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaOrarioComandanti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Tipo As String
    Public Riga As Short
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public PrimoGiornoSuccessivo As String
    Public SecondoGiornoSuccessivo As String
    Public TerzoGiornoSuccessivo As String
    Public QuartoGiornoSuccessivo As String
    Public QuintoGiornoSuccessivo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Tipo = ""
        Riga = 0
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        PrimoGiornoSuccessivo = ""
        SecondoGiornoSuccessivo = ""
        TerzoGiornoSuccessivo = ""
        QuartoGiornoSuccessivo = ""
        QuintoGiornoSuccessivo = ""
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal xData As Date, ByVal xGruppo As String, ByVal xTipo As String, ByVal xRiga As Short)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrdiniServizioRigaOrarioComandanti" & _
                        " WHERE Data = ?" & _
                        " AND Gruppo = ?" & _
                        " AND Tipo = ?" & _
                        " AND Riga = ?"
        cmd.Parameters.AddWithValue("@Data", xData)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Tipo", xTipo)
        cmd.Parameters.AddWithValue("@Riga", xRiga)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = StringaDb(myPOSTreader.Item("Id"))
            Utente = StringaDb(myPOSTreader.Item("Utente"))
            DataAggiornamento = DataDb(myPOSTreader.Item("DataAggiornamento"))
            Data = DataDb(myPOSTreader.Item("Data"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            Tipo = StringaDb(myPOSTreader.Item("Tipo"))
            Riga = NumeroDb(myPOSTreader.Item("Riga"))
            PrimoOrario = StringaDb(myPOSTreader.Item("PrimoOrario"))
            SecondoOrario = StringaDb(myPOSTreader.Item("SecondoOrario"))
            TerzoOrario = StringaDb(myPOSTreader.Item("TerzoOrario"))
            QuartoOrario = StringaDb(myPOSTreader.Item("QuartoOrario"))
            QuintoOrario = StringaDb(myPOSTreader.Item("QuintoOrario"))
            PrimoGiornoSuccessivo = StringaDb(myPOSTreader.Item("PrimoGiornoSuccessivo"))
            SecondoGiornoSuccessivo = StringaDb(myPOSTreader.Item("SecondoGiornoSuccessivo"))
            TerzoGiornoSuccessivo = StringaDb(myPOSTreader.Item("TerzoGiornoSuccessivo"))
            QuartoGiornoSuccessivo = StringaDb(myPOSTreader.Item("QuartoGiornoSuccessivo"))
            QuintoGiornoSuccessivo = StringaDb(myPOSTreader.Item("QuintoGiornoSuccessivo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

End Class
