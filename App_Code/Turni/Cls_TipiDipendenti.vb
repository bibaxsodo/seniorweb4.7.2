Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipiDipendenti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public PresenzaMinima1 As Short
    Public PresenzaMassima1 As Short
    Public PresenzaMinima2 As Short
    Public PresenzaMassima2 As Short
    Public PresenzaMinima3 As Short
    Public PresenzaMassima3 As Short
    Public PresenzaMinima4 As Short
    Public PresenzaMassima4 As Short

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        PresenzaMinima1 = 0
        PresenzaMassima1 = 0
        PresenzaMinima2 = 0
        PresenzaMassima2 = 0
        PresenzaMinima3 = 0
        PresenzaMassima3 = 0
        PresenzaMinima4 = 0
        PresenzaMassima4 = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM TipiDipendenti WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM TipiDipendenti WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TipiDipendenti SET " & _
                    " Utente = ?," & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?," & _
                    " PresenzaMinima1 = ?," & _
                    " PresenzaMassima1 = ?," & _
                    " PresenzaMinima2 = ?," & _
                    " PresenzaMassima2 = ?," & _
                    " PresenzaMinima3 = ?," & _
                    " PresenzaMassima3 = ?," & _
                    " PresenzaMinima4 = ?," & _
                    " PresenzaMassima4 = ?," & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@PresenzaMinima1", PresenzaMinima1)
            cmdw.Parameters.AddWithValue("@PresenzaMassima1", PresenzaMassima1)
            cmdw.Parameters.AddWithValue("@PresenzaMinima2", PresenzaMinima2)
            cmdw.Parameters.AddWithValue("@PresenzaMassima2", PresenzaMassima2)
            cmdw.Parameters.AddWithValue("@PresenzaMinima3", PresenzaMinima3)
            cmdw.Parameters.AddWithValue("@PresenzaMassima3", PresenzaMassima3)
            cmdw.Parameters.AddWithValue("@PresenzaMinima4", PresenzaMinima4)
            cmdw.Parameters.AddWithValue("@PresenzaMassima4", PresenzaMassima4)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TipiDipendenti (Utente, DataAggiornamento, Codice, Descrizione, PresenzaMinima1, PresenzaMassima1 PresenzaMinima2, PresenzaMassima2 PresenzaMinima3, PresenzaMassima3 PresenzaMinima4, PresenzaMassima4) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@PresenzaMinima1", PresenzaMinima1)
            cmdw.Parameters.AddWithValue("@PresenzaMassima1", PresenzaMassima1)
            cmdw.Parameters.AddWithValue("@PresenzaMinima2", PresenzaMinima2)
            cmdw.Parameters.AddWithValue("@PresenzaMassima2", PresenzaMassima2)
            cmdw.Parameters.AddWithValue("@PresenzaMinima3", PresenzaMinima3)
            cmdw.Parameters.AddWithValue("@PresenzaMassima3", PresenzaMassima3)
            cmdw.Parameters.AddWithValue("@PresenzaMinima4", PresenzaMinima4)
            cmdw.Parameters.AddWithValue("@PresenzaMassima4", PresenzaMassima4)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From TipiDipendenti Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("PresenzaMinima1", GetType(String))
        Tabella.Columns.Add("PresenzaMassima1", GetType(String))
        Tabella.Columns.Add("PresenzaMinima2", GetType(String))
        Tabella.Columns.Add("PresenzaMassima2", GetType(String))
        Tabella.Columns.Add("PresenzaMinima3", GetType(String))
        Tabella.Columns.Add("PresenzaMassima3", GetType(String))
        Tabella.Columns.Add("PresenzaMinima4", GetType(String))
        Tabella.Columns.Add("PresenzaMassima4", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("PresenzaMinima1")
            myriga(3) = myPOSTreader.Item("PresenzaMassima1")
            myriga(4) = myPOSTreader.Item("PresenzaMinima2")
            myriga(5) = myPOSTreader.Item("PresenzaMassima2")
            myriga(6) = myPOSTreader.Item("PresenzaMinima3")
            myriga(7) = myPOSTreader.Item("PresenzaMassima3")
            myriga(8) = myPOSTreader.Item("PresenzaMinima4")
            myriga(9) = myPOSTreader.Item("PresenzaMassima4")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From TipiDipendenti WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            PresenzaMinima1 = NumeroDb(myPOSTreader.Item("PresenzaMinima1"))
            PresenzaMassima1 = NumeroDb(myPOSTreader.Item("PresenzaMassima1"))
            PresenzaMinima2 = NumeroDb(myPOSTreader.Item("PresenzaMinima2"))
            PresenzaMassima2 = NumeroDb(myPOSTreader.Item("PresenzaMassima2"))
            PresenzaMinima3 = NumeroDb(myPOSTreader.Item("PresenzaMinima3"))
            PresenzaMassima3 = NumeroDb(myPOSTreader.Item("PresenzaMassima3"))
            PresenzaMinima4 = NumeroDb(myPOSTreader.Item("PresenzaMinima4"))
            PresenzaMassima4 = NumeroDb(myPOSTreader.Item("PresenzaMassima4"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM Qualifica" & _
                          " WHERE TipoDipendente = ?" 

        cmd.Parameters.AddWithValue("@TipoDipendente", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM TipiDipendenti ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub


    Function Esiste_Codice(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM TipiDipendenti" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste_Descrizione(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM TipiDipendenti" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If StringaDb(myPOSTreader.Item("Codice")) = xCodice Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
