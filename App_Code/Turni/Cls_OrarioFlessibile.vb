Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrarioFlessibile
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Descrizione As String
    Public InAlto As Date
    Public InBasso As Date
    Public GestioneTimbrature As String
    Public FlessibilitaIntervallo As String
    Public AbbuonoFlessibilita As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Descrizione = ""
        InAlto = Nothing
        InBasso = Nothing
        GestioneTimbrature = ""
        FlessibilitaIntervallo = ""
        AbbuonoFlessibilita = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM OrarioFlessibile" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioFlessibile" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE OrarioFlessibile SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?, " & _
                    " InAlto  = ? " & _
                    " InBasso  = ? " & _
                    " GestioneTimbrature  = ? " & _
                    " FlessibilitaIntervallo  = ? " & _
                    " AbbuonoFlessibilita  = ? " & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@InAlto", InAlto)
            cmdw.Parameters.AddWithValue("@InBasso", InBasso)
            cmdw.Parameters.AddWithValue("@GestioneTimbrature", GestioneTimbrature)
            cmdw.Parameters.AddWithValue("@FlessibilitaIntervallo", FlessibilitaIntervallo)
            cmdw.Parameters.AddWithValue("@AbbuonoFlessibilita", AbbuonoFlessibilita)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO OrarioFlessibile (Utente, DataAggiornamento, Codice, Validita, Descrizione, InAlto, InBasso, GestioneTimbrature, FlessibilitaIntervallo, AbbuonoFlessibilita) VALUES (?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@InAlto", InAlto)
            cmdw.Parameters.AddWithValue("@InBasso", InBasso)
            cmdw.Parameters.AddWithValue("@GestioneTimbrature", GestioneTimbrature)
            cmdw.Parameters.AddWithValue("@FlessibilitaIntervallo", FlessibilitaIntervallo)
            cmdw.Parameters.AddWithValue("@AbbuonoFlessibilita", AbbuonoFlessibilita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From OrarioFlessibile Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("In Alto", GetType(String))
        Tabella.Columns.Add("In Basso", GetType(String))
        Tabella.Columns.Add("Gestione Timbrature", GetType(String))
        Tabella.Columns.Add("Flessibilita Intervallo", GetType(String))
        Tabella.Columns.Add("Abbuono Flessibilita", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = Format(myPOSTreader.Item("Validita"), "dd/MM/yyyy")
            myriga(2) = myPOSTreader.Item("Descrizione")
            myriga(3) = Format(myPOSTreader.Item("InAlto"), "HH.mm")
            myriga(4) = Format(myPOSTreader.Item("InBasso"), "HH.mm")
            myriga(5) = myPOSTreader.Item("GestioneTimbrature")
            myriga(6) = myPOSTreader.Item("FlessibilitaIntervallo")
            myriga(7) = myPOSTreader.Item("AbbuonoFlessibilita")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioFlessibile" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            InAlto = DataDb(myPOSTreader.Item("InAlto"))
            InBasso = DataDb(myPOSTreader.Item("InBasso"))
            GestioneTimbrature = StringaDb(myPOSTreader.Item("GestioneTimbrature"))
            FlessibilitaIntervallo = StringaDb(myPOSTreader.Item("FlessibilitaIntervallo"))
            AbbuonoFlessibilita = StringaDb(myPOSTreader.Item("AbbuonoFlessibilita"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Trova(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM OrarioFlessibile" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            InAlto = DataDb(myPOSTreader.Item("InAlto"))
            InBasso = DataDb(myPOSTreader.Item("InBasso"))
            GestioneTimbrature = StringaDb(myPOSTreader.Item("GestioneTimbrature"))
            FlessibilitaIntervallo = StringaDb(myPOSTreader.Item("FlessibilitaIntervallo"))
            AbbuonoFlessibilita = StringaDb(myPOSTreader.Item("AbbuonoFlessibilita"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim P As New Cls_ParametriTurni

        P.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceVariabile = ?" & _
                          " And ContenutoTesto = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", P.CodiceAnagraficoFlessibilita)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioFlessibile" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM OrarioFlessibile" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM OrarioFlessibile ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True
    End Sub

End Class
