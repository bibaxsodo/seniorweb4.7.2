Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_FasceOrarie
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Orario(10) As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Orario(10) = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM FasceOrarie WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM FasceOrarie WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE FasceOrarie SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?, " & _
                    " Orario1  = ? " & _
                    " Orario2  = ? " & _
                    " Orario3  = ? " & _
                    " Orario4  = ? " & _
                    " Orario5  = ? " & _
                    " Orario6  = ? " & _
                    " Orario7  = ? " & _
                    " Orario8  = ? " & _
                    " Orario9  = ? " & _
                    " Orario10  = ? " & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Orario1", Orario(1))
            cmdw.Parameters.AddWithValue("@Orario2", Orario(2))
            cmdw.Parameters.AddWithValue("@Orario3", Orario(3))
            cmdw.Parameters.AddWithValue("@Orario4", Orario(4))
            cmdw.Parameters.AddWithValue("@Orario5", Orario(5))
            cmdw.Parameters.AddWithValue("@Orario6", Orario(6))
            cmdw.Parameters.AddWithValue("@Orario7", Orario(7))
            cmdw.Parameters.AddWithValue("@Orario8", Orario(8))
            cmdw.Parameters.AddWithValue("@Orario9", Orario(9))
            cmdw.Parameters.AddWithValue("@Orario10", Orario(10))
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO FasceOrarie (Utente, DataAggiornamento, Codice, Descrizione, Orario(1), Orario(2), Orario(3), Orario(4), Orario(5), Orario(6), Orario(7), Orario(8), Orario(9), Orario(10)) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Orario1", Orario(1))
            cmdw.Parameters.AddWithValue("@Orario2", Orario(2))
            cmdw.Parameters.AddWithValue("@Orario3", Orario(3))
            cmdw.Parameters.AddWithValue("@Orario4", Orario(4))
            cmdw.Parameters.AddWithValue("@Orario5", Orario(5))
            cmdw.Parameters.AddWithValue("@Orario6", Orario(6))
            cmdw.Parameters.AddWithValue("@Orario7", Orario(7))
            cmdw.Parameters.AddWithValue("@Orario8", Orario(8))
            cmdw.Parameters.AddWithValue("@Orario9", Orario(9))
            cmdw.Parameters.AddWithValue("@Orario10", Orario(10))
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From FasceOrarie Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Orario 1", GetType(String))
        Tabella.Columns.Add("Orario 2", GetType(String))
        Tabella.Columns.Add("Orario 3", GetType(String))
        Tabella.Columns.Add("Orario 4", GetType(String))
        Tabella.Columns.Add("Orario 5", GetType(String))
        Tabella.Columns.Add("Orario 6", GetType(String))
        Tabella.Columns.Add("Orario 7", GetType(String))
        Tabella.Columns.Add("Orario 8", GetType(String))
        Tabella.Columns.Add("Orario 9", GetType(String))
        Tabella.Columns.Add("Orario 10", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Orario1")
            myriga(3) = myPOSTreader.Item("Orario2")
            myriga(4) = myPOSTreader.Item("Orario3")
            myriga(5) = myPOSTreader.Item("Orario4")
            myriga(6) = myPOSTreader.Item("Orario5")
            myriga(7) = myPOSTreader.Item("Orario6")
            myriga(8) = myPOSTreader.Item("Orario7")
            myriga(9) = myPOSTreader.Item("Orario8")
            myriga(10) = myPOSTreader.Item("Orario9")
            myriga(11) = myPOSTreader.Item("Orario10")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From FasceOrarie WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            For i = 1 To 10
                Orario(i) = StringaDb(myPOSTreader.Item("Orario" & i))
            Next i
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceVariabile = ?" & _
                          " And ContenutoTesto =  = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoFasceOrarie)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM FasceOrarie ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM FasceOrarie" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM FasceOrarie" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
