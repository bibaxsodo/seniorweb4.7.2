Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProfiliFerieTesta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public OrarioGiornoFestivo As String
    Public ControlloNotte As String
    Public ControlloSmontoNotte As String
    Public ControlloRiposo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        OrarioGiornoFestivo = ""
        ControlloNotte = ""
        ControlloSmontoNotte = ""
        ControlloRiposo = ""
    End Sub

    Sub Trova(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM ProfiliFerieTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            OrarioGiornoFestivo = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
            ControlloNotte = StringaDb(myPOSTreader.Item("ControlloNotte"))
            ControlloSmontoNotte = StringaDb(myPOSTreader.Item("ControlloSmontoNotte"))
            ControlloRiposo = StringaDb(myPOSTreader.Item("ControlloRiposo"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM ProfiliFerieTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM ProfiliFerieTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE ProfiliFerieTesta SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " OrarioGiornoFestivo = ?," & _
                    " ControlloNotte = ?," & _
                    " ControlloSmontoNotte = ?," & _
                    " ControlloRiposo = ?" & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivo", OrarioGiornoFestivo)
            cmdw.Parameters.AddWithValue("@ControlloNotte", ControlloNotte)
            cmdw.Parameters.AddWithValue("@ControlloSmontoNotte", ControlloSmontoNotte)
            cmdw.Parameters.AddWithValue("@ControlloRiposo", ControlloRiposo)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO ProfiliFerieTesta (Utente, DataAggiornamento, Codice, Validita, OrarioGiornoFestivo, ControlloNotte, ControlloSmontoNotte, ControlloRiposo) VALUES (?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivo", OrarioGiornoFestivo)
            cmdw.Parameters.AddWithValue("@ControlloNotte", ControlloNotte)
            cmdw.Parameters.AddWithValue("@ControlloSmontoNotte", ControlloSmontoNotte)
            cmdw.Parameters.AddWithValue("@ControlloRiposo", ControlloRiposo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From ProfiliFerieTesta" & _
                          " WHERE Codice = ?" & _
                          " Order By Validita"
        cmd.Parameters.AddWithValue("@Codice", XCodice)
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Orario Giorno Festivo", GetType(String))
        Tabella.Columns.Add("Controllo Notte", GetType(String))
        Tabella.Columns.Add("Controllo Smonto", GetType(String))
        Tabella.Columns.Add("Controllo Riposo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("Validita"), "dd/MM/yyyy")
            myriga(1) = myPOSTreader.Item("OrarioGiornoFestivo")
            myriga(2) = myPOSTreader.Item("ControlloNotte")
            myriga(3) = myPOSTreader.Item("ControlloSmontoNotte")
            myriga(4) = myPOSTreader.Item("ControlloRiposo")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM ProfiliFerieTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = StringaDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            OrarioGiornoFestivo = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
            ControlloNotte = StringaDb(myPOSTreader.Item("ControlloNotte"))
            ControlloSmontoNotte = StringaDb(myPOSTreader.Item("ControlloSmontoNotte"))
            ControlloRiposo = StringaDb(myPOSTreader.Item("ControlloRiposo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Validita FROM ProfiliFerieTesta ORDER BY Validita")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Validita"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Function Conta(ByVal StringaConnessione As String, ByVal xCodice As String) As Integer
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM ProfiliFerieTesta" & _
                          " WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return myPOSTreader.Item(0)
        Else
            Return 0
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function
End Class
