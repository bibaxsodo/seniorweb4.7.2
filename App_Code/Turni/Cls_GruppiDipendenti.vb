Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_GruppiDipendenti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public CodiceDescrittivo As String
    Public Descrizione As String
    Public Indirizzo As String
    Public Telefono As String
    Public IndirizzoPostaElettronica As String
    Public PropostaTurni As String
    Public Attivo As String
    Public DataNonAttivo As Date
    Public DataTurnoAB As Date
    Public TurnoAB1 As String
    Public TurnoAB2 As String
    Public Dalle1 As Date
    Public Alle1 As Date
    Public Canale1 As String
    Public Dalle2 As Date
    Public Alle2 As Date
    Public Canale2 As String
    Public Dalle3 As Date
    Public Alle3 As Date
    Public Canale3 As String
    Public Dalle4 As Date
    Public Alle4 As Date
    Public Canale4 As String
    Public Dalle5 As Date
    Public Alle5 As Date
    Public Canale5 As String
    Public Dalle6 As Date
    Public Alle6 As Date
    Public Canale6 As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        CodiceDescrittivo = ""
        Descrizione = ""
        Indirizzo = ""
        Telefono = ""
        IndirizzoPostaElettronica = ""
        PropostaTurni = ""
        Attivo = ""
        DataNonAttivo = Nothing
        DataTurnoAB = Nothing
        TurnoAB1 = ""
        TurnoAB2 = ""
        Dalle1 = Nothing
        Alle1 = Nothing
        Canale1 = ""
        Dalle2 = Nothing
        Alle2 = Nothing
        Canale2 = ""
        Dalle3 = Nothing
        Alle3 = Nothing
        Canale3 = ""
        Dalle4 = Nothing
        Alle4 = Nothing
        Canale4 = ""
        Dalle5 = Nothing
        Alle5 = Nothing
        Canale5 = ""
        Dalle6 = Nothing
        Alle6 = Nothing
        Canale6 = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM GruppiDipendenti where Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmdw As New OleDbCommand()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM GruppiDipendenti WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            cmdw.CommandText = "UPDATE GruppiDipendenti SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " CodiceDescrittivo  = ?," & _
                               " Descrizione = ?," & _
                               " Indirizzo = ?," & _
                               " Telefono = ?," & _
                               " IndirizzoPostaElettronica = ?," & _
                               " PropostaTurni  = ?," & _
                               " Attivo  = ?," & _
                               " DataNonAttivo  = ?," & _
                               " DataTurnoAB  = ?," & _
                               " TurnoAB1 = ?," & _
                               " TurnoAB2 = ?," & _
                               " Dalle1 = ?," & _
                               " Alle1 = ?," & _
                               " Canale1 = ?," & _
                               " Dalle2 = ?," & _
                               " Alle2 = ?," & _
                               " Canale2 = ?," & _
                               " Dalle3 = ?," & _
                               " Alle3 = ?," & _
                               " Canale3 = ?," & _
                               " Dalle4 = ?," & _
                               " Alle4 = ?," & _
                               " Canale4 = ?," & _
                               " Dalle5 = ?," & _
                               " Alle5 = ?," & _
                               " Canale5 = ?," & _
                               " Dalle6 = ?," & _
                               " Alle6 = ?," & _
                               " Canale6 = ?" & _
                               " WHERE Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@CodiceDescrittivo", CodiceDescrittivo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Indirizzo", Indirizzo)
            cmdw.Parameters.AddWithValue("@Telefono", Telefono)
            cmdw.Parameters.AddWithValue("@IndirizzoPostaElettronica", IndirizzoPostaElettronica)
            cmdw.Parameters.AddWithValue("@PropostaTurni", PropostaTurni)
            cmdw.Parameters.AddWithValue("@Attivo", Attivo)
            cmdw.Parameters.AddWithValue("@DataNonAttivo", DataNonAttivo)
            cmdw.Parameters.AddWithValue("@DataTurnoAB", DataTurnoAB)
            cmdw.Parameters.AddWithValue("@TurnoAB1", TurnoAB1)
            cmdw.Parameters.AddWithValue("@TurnoAB2", TurnoAB2)
            cmdw.Parameters.AddWithValue("@Dalle1", Dalle1)
            cmdw.Parameters.AddWithValue("@Alle1", Alle1)
            cmdw.Parameters.AddWithValue("@Canale1", Canale1)
            cmdw.Parameters.AddWithValue("@Dalle2", Dalle2)
            cmdw.Parameters.AddWithValue("@Alle2", Alle2)
            cmdw.Parameters.AddWithValue("@Canale2", Canale2)
            cmdw.Parameters.AddWithValue("@Dalle3", Dalle3)
            cmdw.Parameters.AddWithValue("@Alle3", Alle3)
            cmdw.Parameters.AddWithValue("@Canale3", Canale3)
            cmdw.Parameters.AddWithValue("@Dalle4", Dalle4)
            cmdw.Parameters.AddWithValue("@Alle4", Alle4)
            cmdw.Parameters.AddWithValue("@Canale4", Canale4)
            cmdw.Parameters.AddWithValue("@Dalle5", Dalle5)
            cmdw.Parameters.AddWithValue("@Alle5", Alle5)
            cmdw.Parameters.AddWithValue("@Canale5", Canale5)
            cmdw.Parameters.AddWithValue("@Dalle6", Dalle6)
            cmdw.Parameters.AddWithValue("@Alle6", Alle6)
            cmdw.Parameters.AddWithValue("@Canale6", Canale6)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            cmdw.CommandText = "INSERT INTO GruppiDipendenti (Utente, DataAggiornamento, Codice, CodiceDescrittivo, Descrizione, Indirizzo, Telefono, IndirizzoPostaElettronica, PropostaTurni, Attivo, DataNonAttivo, DataTurnoAB, TurnoAB1, TurnoAB2, Dalle1, Alle1, Canale1, Dalle2, Alle2, Canale2, Dalle3, Alle3, Canale3, Dalle4, Alle4, Canale4, Dalle5, Alle5, Canale5, Dalle6, Alle6, Canale6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@CodiceDescrittivo", CodiceDescrittivo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Indirizzo", Indirizzo)
            cmdw.Parameters.AddWithValue("@Telefono", Telefono)
            cmdw.Parameters.AddWithValue("@IndirizzoPostaElettronica", IndirizzoPostaElettronica)
            cmdw.Parameters.AddWithValue("@PropostaTurni", PropostaTurni)
            cmdw.Parameters.AddWithValue("@Attivo", Attivo)
            cmdw.Parameters.AddWithValue("@DataNonAttivo", IIf(Year(DataNonAttivo) > 1, DataNonAttivo, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@DataTurnoAB", IIf(Year(DataTurnoAB) > 1, DataTurnoAB, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@TurnoAB1", TurnoAB1)
            cmdw.Parameters.AddWithValue("@TurnoAB2", TurnoAB2)
            cmdw.Parameters.AddWithValue("@Dalle1", Dalle1)
            cmdw.Parameters.AddWithValue("@Alle1", Alle1)
            cmdw.Parameters.AddWithValue("@Canale1", Canale1)
            cmdw.Parameters.AddWithValue("@Dalle2", Dalle2)
            cmdw.Parameters.AddWithValue("@Alle2", Alle2)
            cmdw.Parameters.AddWithValue("@Canale2", Canale2)
            cmdw.Parameters.AddWithValue("@Dalle3", Dalle3)
            cmdw.Parameters.AddWithValue("@Alle3", Alle3)
            cmdw.Parameters.AddWithValue("@Canale3", Canale3)
            cmdw.Parameters.AddWithValue("@Dalle4", Dalle4)
            cmdw.Parameters.AddWithValue("@Alle4", Alle4)
            cmdw.Parameters.AddWithValue("@Canale4", Canale4)
            cmdw.Parameters.AddWithValue("@Dalle5", Dalle5)
            cmdw.Parameters.AddWithValue("@Alle5", Alle5)
            cmdw.Parameters.AddWithValue("@Canale5", Canale5)
            cmdw.Parameters.AddWithValue("@Dalle6", Dalle6)
            cmdw.Parameters.AddWithValue("@Alle6", Alle6)
            cmdw.Parameters.AddWithValue("@Canale6", Canale6)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Public Sub LoadDati(ByVal StringaConnessione As String, ByVal xDescrizione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("CodiceDescrittivo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Indirizzo", GetType(String))
        Tabella.Columns.Add("Telefono", GetType(String))
        Tabella.Columns.Add("IndirizzoPostaElettronica", GetType(String))
        Tabella.Columns.Add("PropostaTurni", GetType(String))
        Tabella.Columns.Add("Attivo", GetType(String))
        Tabella.Columns.Add("DataNonAttivo", GetType(String))
        Tabella.Columns.Add("DataTurnoAB", GetType(String))
        Tabella.Columns.Add("TurnoAB1", GetType(String))
        Tabella.Columns.Add("TurnoAB2", GetType(String))
        Tabella.Columns.Add("Dalle1", GetType(String))
        Tabella.Columns.Add("Alle1", GetType(String))
        Tabella.Columns.Add("Canale1", GetType(String))
        Tabella.Columns.Add("Dalle2", GetType(String))
        Tabella.Columns.Add("Alle2", GetType(String))
        Tabella.Columns.Add("Canale2", GetType(String))
        Tabella.Columns.Add("Dalle3", GetType(String))
        Tabella.Columns.Add("Alle3", GetType(String))
        Tabella.Columns.Add("Canale3", GetType(String))
        Tabella.Columns.Add("Dalle4", GetType(String))
        Tabella.Columns.Add("Alle4", GetType(String))
        Tabella.Columns.Add("Canale4", GetType(String))
        Tabella.Columns.Add("Dalle5", GetType(String))
        Tabella.Columns.Add("Alle5", GetType(String))
        Tabella.Columns.Add("Canale5", GetType(String))
        Tabella.Columns.Add("Dalle6", GetType(String))
        Tabella.Columns.Add("Alle6", GetType(String))
        Tabella.Columns.Add("Canale6", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If xDescrizione <> "" Then
            cmd.CommandText = "Select * From GruppiDipendenti WHERE Descrizione LIKE '" & Duplica_Accenti(xDescrizione) & "' Order By Descrizione"
        Else
            cmd.CommandText = "Select * From GruppiDipendenti Order By Descrizione"
        End If

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("CodiceDescrittivo")
            myriga(2) = myPOSTreader.Item("Descrizione")
            myriga(3) = myPOSTreader.Item("Indirizzo")
            myriga(4) = myPOSTreader.Item("Telefono")
            myriga(5) = myPOSTreader.Item("IndirizzoPostaElettronica")
            myriga(6) = myPOSTreader.Item("PropostaTurni")
            myriga(7) = myPOSTreader.Item("Attivo")
            myriga(8) = DataDb(myPOSTreader.Item("DataNonAttivo")).ToString("dd/MM/yyyy")
            myriga(9) = DataDb(myPOSTreader.Item("DataTurnoAB")).ToString("dd/MM/yyyy")
            myriga(10) = myPOSTreader.Item("TurnoAB1")
            myriga(11) = myPOSTreader.Item("TurnoAB2")
            myriga(12) = DataDb(myPOSTreader.Item("Dalle1")).ToString("HH.mm")
            myriga(13) = DataDb(myPOSTreader.Item("Alle1")).ToString("HH.mm")
            myriga(14) = myPOSTreader.Item("Canale1")
            myriga(15) = DataDb(myPOSTreader.Item("Dalle2")).ToString("HH.mm")
            myriga(16) = DataDb(myPOSTreader.Item("Alle2")).ToString("HH.mm")
            myriga(17) = myPOSTreader.Item("Canale2")
            myriga(18) = DataDb(myPOSTreader.Item("Dalle3")).ToString("HH.mm")
            myriga(19) = DataDb(myPOSTreader.Item("Alle3")).ToString("HH.mm")
            myriga(20) = myPOSTreader.Item("Canale3")
            myriga(21) = DataDb(myPOSTreader.Item("Dalle4")).ToString("HH.mm")
            myriga(22) = DataDb(myPOSTreader.Item("Alle4")).ToString("HH.mm")
            myriga(23) = myPOSTreader.Item("Canale4")
            myriga(24) = DataDb(myPOSTreader.Item("Dalle5")).ToString("HH.mm")
            myriga(25) = DataDb(myPOSTreader.Item("Alle5")).ToString("HH.mm")
            myriga(26) = myPOSTreader.Item("Canale5")
            myriga(27) = DataDb(myPOSTreader.Item("Dalle6")).ToString("HH.mm")
            myriga(28) = DataDb(myPOSTreader.Item("Alle6")).ToString("HH.mm")
            myriga(29) = myPOSTreader.Item("Canale6")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub CaricaGrigliaGruppo(ByVal StringaConnessione As String, ByVal xValidita As Date, ByRef Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Fine As Boolean
        Dim r As Integer
        Dim i As Integer
        Dim ii As Integer

        Tabella.Clear()
        Tabella.Columns.Add("Seleziona", GetType(String))
        Tabella.Columns.Add("LegatoA", GetType(String))
        Tabella.Columns.Add("NelServizio", GetType(String))
        Tabella.Columns.Add("OrdiniServizio", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Chiave", GetType(String))
        Tabella.Columns.Add("Livello", GetType(String))
        Tabella.Columns.Add("Livello1", GetType(String))
        Tabella.Columns.Add("Livello2", GetType(String))
        Tabella.Columns.Add("Livello3", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT (SELECT TOP 1 LegatoA FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS LegatoA, (SELECT TOP 1 NelServizio FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS NelServizio, (SELECT TOP 1 OrdiniServizio FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS OrdiniServizio, Codice, Descrizione + ' (' + Attivo + ')' As Descrizione1 FROM GruppiDipendenti " & _
          " WHERE Attivo = 'S'" & _
          " OR (Attivo = 'N' AND DataNonAttivo > ?)" & _
          " ORDER BY Codice"

        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(1) = myPOSTreader.Item("LegatoA")
            myriga(2) = myPOSTreader.Item("NelServizio")
            myriga(3) = myPOSTreader.Item("OrdiniServizio")
            myriga(4) = myPOSTreader.Item("Codice")
            myriga(6) = 0
            myriga(7) = myPOSTreader.Item("Descrizione1")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

        Fine = False
        Do Until Fine = True
            Fine = True
            For r = 0 To Tabella.Rows.Count - 1
                If IsDBNull(Tabella.Rows(r).Item(1)) Then
                    Tabella.Rows(r).Item(5) = Tabella.Rows(r).Item(4)
                    Tabella.Rows(r).Item(6) = 1
                Else
                    For i = 0 To Tabella.Rows.Count - 1
                        If Tabella.Rows(r).Item(1) = Tabella.Rows(i).Item(4) Then
                            If Not IsDBNull(Tabella.Rows(i).Item(5)) Then
                                If IsDBNull(Tabella.Rows(r).Item(5)) Then
                                    Tabella.Rows(r).Item(5) = Tabella.Rows(i).Item(5) + Tabella.Rows(r).Item(4)
                                    Tabella.Rows(r).Item(6) = Tabella.Rows(i).Item(6) + 1
                                    ii = Tabella.Rows(r).Item(6) + 7
                                    '                                    If Tabella.Columns.Count < ii Then
                                    '                                       Tabella.Columns.Add("Livello - " & Tabella.Rows(r).Item(6), GetType(String))
                                    '                                    End If
                                    Tabella.Rows(r).Item(ii - 1) = Tabella.Rows(r).Item(7)
                                    Tabella.Rows(r).Item(7) = ""
                                End If
                            Else
                                Fine = False
                            End If
                            Exit For
                        End If
                    Next i
                End If
            Next r
        Loop

        Dim dv As System.Data.DataView = Tabella.DefaultView
        dv.Sort = "Chiave"
        Tabella = dv.ToTable
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Public Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From GruppiDipendenti Where " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            CodiceDescrittivo = StringaDb(myPOSTreader.Item("CodiceDescrittivo"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Indirizzo = StringaDb(myPOSTreader.Item("Indirizzo"))
            Telefono = StringaDb(myPOSTreader.Item("Telefono"))
            IndirizzoPostaElettronica = StringaDb(myPOSTreader.Item("IndirizzoPostaElettronica"))
            PropostaTurni = StringaDb(myPOSTreader.Item("PropostaTurni"))
            Attivo = StringaDb(myPOSTreader.Item("Attivo"))
            DataNonAttivo = DataDb(myPOSTreader.Item("DataNonAttivo"))
            DataTurnoAB = DataDb(myPOSTreader.Item("DataTurnoAB"))
            TurnoAB1 = StringaDb(myPOSTreader.Item("TurnoAB1"))
            TurnoAB2 = StringaDb(myPOSTreader.Item("TurnoAB2"))
            Dalle1 = DataDb(myPOSTreader.Item("Dalle1"))
            Alle1 = DataDb(myPOSTreader.Item("Alle1"))
            Canale1 = StringaDb(myPOSTreader.Item("Canale1"))
            Dalle2 = DataDb(myPOSTreader.Item("Dalle2"))
            Alle2 = DataDb(myPOSTreader.Item("Alle2"))
            Canale2 = StringaDb(myPOSTreader.Item("Canale2"))
            Dalle3 = DataDb(myPOSTreader.Item("Dalle3"))
            Alle3 = DataDb(myPOSTreader.Item("Alle3"))
            Canale3 = StringaDb(myPOSTreader.Item("Canale3"))
            Dalle4 = DataDb(myPOSTreader.Item("Dalle4"))
            Alle4 = DataDb(myPOSTreader.Item("Alle4"))
            Canale4 = StringaDb(myPOSTreader.Item("Canale4"))
            Dalle5 = DataDb(myPOSTreader.Item("Dalle5"))
            Alle5 = DataDb(myPOSTreader.Item("Alle5"))
            Canale5 = StringaDb(myPOSTreader.Item("Canale5"))
            Dalle6 = DataDb(myPOSTreader.Item("Dalle6"))
            Alle6 = DataDb(myPOSTreader.Item("Alle6"))
            Canale6 = StringaDb(myPOSTreader.Item("Canale6"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Function Decodifica(ByVal StringaConnessione As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Descrizione, Attivo FROM GruppiDipendenti" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Decodifica = StringaDb(myPOSTreader.Item(0)) & " (" & StringaDb(myPOSTreader.Item(1)) & ")"
        Else
            Decodifica = ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE (CodiceVariabile = ?" & _
                          " Or CodiceVariabile = ?)" & _
                          " And ContenutoTesto = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoGruppo)
        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoSuperGruppo)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM GruppiDipendentiGestiti " & _
                          " WHERE CodiceGruppo = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CodiceGruppo", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM GruppiDipendentiLegami " & _
                          " WHERE Codice = ?" & _
                          " OR LegatoA = ?" & _
                          " OR NelServizio = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@LegatoA", xCodice)
        cmd.Parameters.AddWithValue("@NelServizio", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Esiste(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From GruppiDipendenti WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione + ' (' + Attivo + ')' As Descrizione1 FROM GruppiDipendenti ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione1"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Public Sub UpDateDropBoxGruppo(ByVal StringaConnessione As String, ByVal xValidita As Date, ByRef Appoggio As DropDownList)
        Dim Tabella As New System.Data.DataTable("Tabella")

        Dim r As Integer
        Appoggio.Items.Clear()

        Call CaricaGrigliaGruppo(StringaConnessione, xValidita, Tabella)

        For r = 0 To Tabella.Rows.Count - 1
            If StringaDb(Tabella.Rows(r).Item(7)) <> "" Then
                Appoggio.Items.Add(Tabella.Rows(r).Item(7))
                Appoggio.Items(Appoggio.Items.Count - 1).Value = Tabella.Rows(r).Item(4)
            Else
                If StringaDb(Tabella.Rows(r).Item(8)) <> "" Then
                    Appoggio.Items.Add(".........." & Tabella.Rows(r).Item(8))
                    Appoggio.Items(Appoggio.Items.Count - 1).Value = Tabella.Rows(r).Item(4)
                Else
                    If StringaDb(Tabella.Rows(r).Item(9)) <> "" Then
                        Appoggio.Items.Add("...................." & Tabella.Rows(r).Item(9))
                        Appoggio.Items(Appoggio.Items.Count - 1).Value = Tabella.Rows(r).Item(4)
                    End If
                End If
            End If
        Next
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True
    End Sub

    Private Function Duplica_Accenti(ByVal xRiga As String) As String
        Dim Appoggio As String = ""
        For i = 1 To Len(xRiga)
            If Mid(xRiga, i, 1) <> "'" Then
                Appoggio = Appoggio + Mid(xRiga, i, 1)
            Else
                Appoggio = Appoggio + Mid(xRiga, i, 1) + Mid(xRiga, i, 1)
            End If
        Next i
        Duplica_Accenti = Appoggio
    End Function

End Class
