Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DatiVariabiliStruttura
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceStruttura As String
    Public CodiceVariabile As String
    Public Validita As Date
    Public ContenutoTesto As String
    Public ContenutoData As Date
    Public ContenutoNumero As Double
    Public Tipo As Integer
    Public TipoNumericoVirgola As String
    Public Note As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceStruttura = ""
        CodiceVariabile = ""
        Validita = Nothing
        ContenutoTesto = ""
        ContenutoData = Nothing
        ContenutoNumero = 0
        Tipo = 0
        TipoNumericoVirgola = ""
        Note = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceStruttura As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM DatiVariabiliStruttura WHERE " & _
                           " CodiceStruttura = ?" & _
                           " AND CodiceVariabile = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@CodiceStruttura", xCodiceStruttura)
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM DatiVariabiliStruttura WHERE CodiceStruttura = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@CodiceStruttura", CodiceStruttura)
        cmd.Parameters.AddWithValue("@Codicevariabile", CodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE DatiVariabiliStruttura SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " ContenutoTesto = ?," & _
                    " ContenutoData  = ?," & _
                    " ContenutoNumero  = ?," & _
                    " Tipo  = ?," & _
                    " TipoNumericoVirgola  = ?," & _
                    " Note  = ? " & _
                    " WHERE CodiceStruttura = ?" & _
                    " AND CodiceVariabile = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@ContenutoTesto", ContenutoTesto)
            cmdw.Parameters.AddWithValue("@ContenutoData", IIf(Year(ContenutoData) > 1, ContenutoData, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ContenutoNumero", ContenutoNumero)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO DatiVariabiliStruttura (Utente, DataAggiornamento, CodiceStruttura, CodiceVariabile, Validita, ContenutoTesto, ContenutoData, ContenutoNumero, Tipo, TipoNumericoVirgola, Note) VALUES (?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceStruttura", CodiceStruttura)
            cmdw.Parameters.AddWithValue("@CodiceVariabile", CodiceVariabile)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@ContenutoTesto", ContenutoTesto)
            cmdw.Parameters.AddWithValue("@ContenutoData", IIf(Year(ContenutoData) > 1, ContenutoData, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ContenutoNumero", ContenutoNumero)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From DatiVariabiliStruttura Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Disponibile", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Disponibile")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceStruttura As Integer, ByVal xCodiceVariabile As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From DatiVariabiliStruttura WHERE " & _
                           " CodiceStruttura = ?" & _
                           " AND CodiceVariabile = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@CodiceStruttura", xCodiceStruttura)
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodiceVariabile)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceStruttura = StringaDb(myPOSTreader.Item("CodiceStruttura"))
            CodiceVariabile = StringaDb(myPOSTreader.Item("CodiceVariabile"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            ContenutoTesto = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            ContenutoData = DataDb(myPOSTreader.Item("ContenutoData"))
            ContenutoNumero = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            Tipo = NumeroDb(myPOSTreader.Item("Tipo"))
            TipoNumericoVirgola = StringaDb(myPOSTreader.Item("TipoNumericoVirgola"))
            Note = StringaDb(myPOSTreader.Item("Note"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

End Class
