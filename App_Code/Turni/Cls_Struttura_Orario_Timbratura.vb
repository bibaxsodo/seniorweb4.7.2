﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Orario_Timbratura
    Public Dalle As String
    Public Alle As String
    Public Pausa As String
    Public Causale As String
    Public Colore As Long
    Public Giustificativo As String
    Public NelGruppo As String
    Public Familiare As Byte
    Public Tipo As String
    Public Qualita As String
    Public TipoServizio As String
    Public GiornoSuccessivo As String

    Public Sub Pulisci()
        Dalle = "00.00"
        Alle = "00.00"
        Pausa = "00.00"
        Causale = ""
        Colore = RGB(255, 255, 255)
        Giustificativo = ""
        NelGruppo = ""
        Familiare = 0
        Tipo = ""
        Qualita = ""
        TipoServizio = ""
        GiornoSuccessivo = ""
    End Sub
End Class
