﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_ClsXScriptTurni
    Public ConnectionString As String
    Public Vettore_Calcolo_Orari As New Cls_Struttura_Calcolo_Orari
    Public Vettore_Orari_Timbrature As New Cls_Struttura_Orari_Timbrature
    Public Utente As String

    Public Sub AggiornaMatrice(ByVal Familiare As Object, ByVal Giustificativo As Object, ByVal Giorno As Object, ByVal Segno As Object, ByVal Contenuto As Object)
        Call Mod_AggiornaMatrice(Vettore_Calcolo_Orari, Familiare, Giustificativo, Giorno, Segno, Contenuto)
    End Sub

    Public Function LeggiMatrice(ByVal Familiare As Object, ByVal Giustificativo As Object, ByVal Giorno As Object) As Object
        LeggiMatrice = Mod_LeggiMatrice(Vettore_Calcolo_Orari, Familiare, Giustificativo, Giorno)
    End Function

    Public Sub CambiaGiustificativoMatrice(ByVal Familiare As Object, ByVal GiustificativoOld As Object, ByVal GiustificativoNew As Object, ByVal Giorno As Object)
        Call Mod_CambiaGiustificativoMatrice(Vettore_Calcolo_Orari, Familiare, GiustificativoOld, GiustificativoNew, Giorno)
    End Sub

    Public Function Calcolo_TempoStandardDalAl(ByVal DataDal As Object, ByVal DataAl As Object, ByVal SuperGruppo As Object, ByVal Contratto As Object) As Object
        Calcolo_TempoStandardDalAl = Calcolo_TempoStandard(DataDal, DataAl, SuperGruppo, Contratto)
    End Function

    Public Function OreContrattuali(ByVal Dipendente As Object, ByVal Anno As Object, ByVal Mese As Object, ByVal SuperGruppo As Object, ByVal Contratto As Object) As Object
        OreContrattuali = Mod_OreContrattuali(Dipendente, Anno, Mese, SuperGruppo, Contratto)
    End Function

    Public Function OreContrattualiNelGiorno(ByVal Data As Object, ByVal SuperGruppo As Object, ByVal Contratto As Object) As Object
        OreContrattualiNelGiorno = Mod_OreContrattualiNelGiorno(Data, SuperGruppo, Contratto)
    End Function

    Public Function MinutiInCentesimi(ByVal Primo As Object) As Object
        MinutiInCentesimi = Mod_MinutiInCentesimi(Primo)
    End Function

    Public Function OperaConMinuti(ByVal Primo As Object, ByVal Segno As Object, ByVal Secondo As Object, Optional ByVal TipoSecondo As String = "hh.nn") As Object
        OperaConMinuti = Mod_OperaConMinuti(Primo, Segno, Secondo, TipoSecondo)
    End Function

    Public Function OperazioneInMinuti(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Segno As Object, ByVal Contenuto As Object, ByVal Data As Object) As Object
        OperazioneInMinuti = Mod_OperazioneInMinuti(Dipendente, Codice, Data, Contenuto, Segno)
    End Function

    Public Function OperazioneInNumeri(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Operatore As Object, ByVal Contenuto As Object, ByVal Data As Object) As Object
        OperazioneInNumeri = Mod_OperazioneInNumeri(Dipendente, Codice, Data, Contenuto, Operatore)
    End Function

    Public Function OperazioneInTesto(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Contenuto As Object, ByVal Data As Object) As Object
        OperazioneInTesto = Mod_OperazioneInTesto(Dipendente, Codice, Data, Contenuto)
    End Function

    Public Function Leggi_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Leggi_DatiVariabili = Mod_Leggi_DatiVariabili(Dipendente, Codice, Modo, Data)
    End Function

    Public Function Scrivi_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Contenuto As Object, ByVal Data As Object, Optional ByVal Provenienza As String = "A") As Object
        Scrivi_DatiVariabili = Mod_Scrivi_DatiVariabili(Dipendente, Codice, Data, Contenuto, Provenienza)
    End Function

    Public Function Cancella_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object) As Object
        Cancella_DatiVariabili = Mod_Cancella_DatiVariabili(Dipendente, Codice, Data)
    End Function

    Public Function Leggi_CampoGiustificativi(ByVal Codice As Object, ByVal Campo As Object) As Object
        Dim cg As New Cls_Giustificativi

        Leggi_CampoGiustificativi = cg.CampoGiustificativi(ConnectionString, Codice, Campo)
    End Function

    Public Function LeggiDipendente_DatiVariabili(ByVal Contenuto As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        LeggiDipendente_DatiVariabili = Mod_LeggiDipendente_DatiVariabili(Contenuto, Codice, Modo, Data)
    End Function

    Public Function LeggiSesso(ByVal Dipendente As Object) As Object
        Dim k As New Cls_Dipendenti

        k.Leggi(ConnectionString, Dipendente)
        LeggiSesso = k.Sesso
    End Function

    Public Function LeggiDataNascita(ByVal Dipendente As Object) As Object
        Dim k As New Cls_Dipendenti

        k.Leggi(ConnectionString, Dipendente)
        LeggiDataNascita = k.DataNascita
    End Function

    Public Function LeggiDataAssunzione(ByVal Dipendente As Object, ByVal Data As Object) As Object
        Dim cm As New Cls_Matricola

        cm.Trova(ConnectionString, Dipendente, Data, "<=")
        If cm.DataAssunzione = Nothing Then
            LeggiDataAssunzione = DateSerial(1899, 12, 30)
        Else
            LeggiDataAssunzione = cm.DataAssunzione
        End If
    End Function

    Public Function LeggiDataLicenziamento(ByVal Dipendente As Object, ByVal Data As Object) As Object
        Dim cm As New Cls_Matricola

        cm.Trova(ConnectionString, Dipendente, Data, "<=")
        If cm.DataLicenziamento = Nothing Then
            LeggiDataLicenziamento = DateSerial(1899, 12, 30)
        Else
            LeggiDataLicenziamento = cm.DataLicenziamento
        End If
    End Function

    Public Function Leggi_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Leggi_DatiVariabiliStruttura = Mod_Leggi_DatiVariabiliStruttura(Struttura, Codice, Modo, Data)
    End Function
    Public Function Somma_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Somma_DatiVariabiliStruttura = Mod_Somma_DatiVariabiliStruttura(Struttura, Codice, Modo, Data)
    End Function

    Public Function Conta_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Conta_DatiVariabiliStruttura = Mod_Conta_DatiVariabiliStruttura(Struttura, Codice, Modo, Data)
    End Function

    Public Function Somma_DatiVariabiliNelPeriodo(ByVal Dipendente As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Somma_DatiVariabiliNelPeriodo = Mod_Somma_DatiVariabiliNelPeriodo(Dipendente, Codice, DataDal, DataAl)
    End Function

    Public Function DataAdozione(ByVal Dipendente As Object, ByVal Data As Object) As Object
        DataAdozione = Mod_DataAdozione(Dipendente, Data)
    End Function

    Public Function DataParto(ByVal Dipendente As Object, ByVal Data As Object) As Object
        DataParto = Mod_DataParto(Dipendente, Data)
    End Function

    Public Function Delta_DatiVariabiliNelPeriodo(ByVal Dipendente As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Delta_DatiVariabiliNelPeriodo = Mod_Delta_DatiVariabiliNelPeriodo(Dipendente, Codice, DataDal, DataAl)
    End Function

    Public Function Somma_GiustificativiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object) As Object
        Somma_GiustificativiNelMese = Mod_Somma_GiustificativiNelMese(Familiare, Codice, GiornoDal, GiornoAl, Vettore_Calcolo_Orari)
    End Function

    Public Function Cerca_SuOrariTimbrature(ByVal Giorno As Object, ByVal Campo As Object, ByVal Valore As Object) As Object
        Cerca_SuOrariTimbrature = Mod_Cerca_SuOrariTimbrature(Giorno, Campo, Valore, Vettore_Orari_Timbrature)
    End Function

    Public Function ContinuazioneDiOrario(ByVal Data As Object) As Object
        ContinuazioneDiOrario = Mod_ContinuazioneDiOrario(Data, Vettore_Orari_Timbrature)
    End Function

    Public Function Conta_GiustificativiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object) As Object
        Conta_GiustificativiNelMese = Mod_Conta_GiustificativiNelMese(Familiare, Codice, GiornoDal, GiornoAl, Vettore_Calcolo_Orari)
    End Function

    Public Function Conta_GiustificativiContiguiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal CodiceAlternativo As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object) As Object
        Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese(Familiare, Codice, CodiceAlternativo, GiornoDal, GiornoAl, Vettore_Calcolo_Orari)
    End Function

    Public Function Conta_GiustificativiMalattiaContiguiNelMese(ByVal Familiare As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object) As Object
        Conta_GiustificativiMalattiaContiguiNelMese = Mod_Conta_GiustificativiMalattiaContiguiNelMese(Familiare, GiornoDal, GiornoAl, Vettore_Calcolo_Orari)
    End Function

    Public Function Somma_GiustificativiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Somma_GiustificativiNelPeriodo = Mod_Somma_GiustificativiNelPeriodo(Dipendente, Familiare, Codice, DataDal, DataAl)
    End Function

    Public Function Conta_GiustificativiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Conta_GiustificativiNelPeriodo = Mod_Conta_GiustificativiNelPeriodo(Dipendente, Familiare, Codice, DataDal, DataAl)
    End Function

    Public Function Conta_GiustificativiContiguiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal CodiceAlternativo As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Conta_GiustificativiContiguiNelPeriodo = Mod_Conta_GiustificativiContiguiNelPeriodo(Dipendente, Familiare, Codice, CodiceAlternativo, DataDal, DataAl)
    End Function

    Public Function Conta_GiustificativiMalattiaContiguiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Conta_GiustificativiMalattiaContiguiNelPeriodo = Mod_Conta_GiustificativiMalattiaContiguiNelPeriodo(Dipendente, Familiare, DataDal, DataAl)
    End Function

    Public Function Conta_TipoGiustificativoNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Tipo As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Conta_TipoGiustificativoNelPeriodo = Mod_Conta_TipoGiustificativoNelPeriodo(Dipendente, Familiare, Tipo, DataDal, DataAl)
    End Function

    Public Function Somma_DatiVariabiliInNumeri(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Somma_DatiVariabiliInNumeri = Mod_Somma_DatiVariabili(Struttura, Codice, Modo, Data, False)
    End Function

    Public Function Somma_DatiVariabiliInMinuti(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Somma_DatiVariabiliInMinuti = Mod_Somma_DatiVariabili(Struttura, Codice, Modo, Data, True)
    End Function

    Public Function LeggiProvenienza_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        LeggiProvenienza_DatiVariabili = Mod_LeggiProvenienza_DatiVariabili(Dipendente, Codice, Modo, Data)
    End Function

    Public Function LeggiValidita_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        LeggiValidita_DatiVariabili = Mod_LeggiValidita_DatiVariabili(Dipendente, Codice, Modo, Data)
    End Function

    Public Function LeggiValidita_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        LeggiValidita_DatiVariabiliStruttura = Mod_LeggiValidita_DatiVariabiliStruttura(Struttura, Codice, Modo, Data)
    End Function

    Public Function Formato(ByVal Contenuto As Object, ByVal Forma As Object) As Object
        Forma = Sostituisci(Forma, "m", "M")
        Forma = Sostituisci(Forma, "n", "m")
        Formato = Format(Contenuto, Forma)
    End Function

    Public Function Sostituisci_Da_A(ByVal Stringa As Object, ByVal Da As Object, ByVal a As Object) As Object
        Sostituisci_Da_A = Sostituisci(Stringa, Da, a)
    End Function

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    '------------------------------------------------------------
    ' Trova l'ultimo giorno del mese
    '------------------------------------------------------------
    Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
          Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    '------------------------------------------------------------
    ' Sostituisce in "MiaStringa" la stringa "Da" con la stringa "a"
    '------------------------------------------------------------
    Public Function Sostituisci(ByVal MiaStringa As String, ByVal Da As String, ByVal a As String) As String
        Dim x As Integer

        If MiaStringa <> "" Then
            Do
                If InStr(1, MiaStringa, Da) > 0 Then
                    x = InStr(1, MiaStringa, Da)
                    MiaStringa = Mid(MiaStringa, 1, x - 1) & a & Mid(MiaStringa, x + 1, Len(MiaStringa) - x + 1)
                End If
            Loop While InStr(1, MiaStringa, Da) > 0
        End If
        Sostituisci = MiaStringa
    End Function

    Public Sub Mod_AggiornaMatrice(ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari, ByVal Familiare As Object, ByVal Giustificativo As Object, ByVal Giorno As Object, ByVal Operatore As Object, ByVal Contenuto As Object)
        Dim c As Byte = 0
        Select Case Giustificativo
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
            Case "Ore Pausa"
                c = 44
            Case "Ore Lavorate"
                c = 45
            Case "Ore Diurne"
                c = 46
            Case "Ore Notturne"
                c = 47
            Case "Ore Festive Diurne"
                c = 48
            Case "Ore Festive Notturne"
                c = 49
            Case "Ore Retribuite"
                c = 50
        End Select
        If c = 0 Then
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Vettore_Calcolo_Orari.Elemento(c, 0) = Giustificativo & "/" & Familiare
                If Vettore_Calcolo_Orari.Elemento(c, 0) = Giustificativo & "/" & Familiare Then Exit For
            Next c
        End If
        Dim a As String = Sostituisci(Vettore_Calcolo_Orari.Elemento(c, Giorno), ".", ",")
        Dim b As String = Format(Mod_OperaConMinuti(a, Operatore, Contenuto), "00.00")
        Vettore_Calcolo_Orari.Elemento(c, Giorno) = Sostituisci(b, ",", ".")
    End Sub

    Public Function Mod_LeggiMatrice(ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari, ByVal Familiare As Object, ByVal Giustificativo As Object, ByVal Giorno As Object) As Object

        Mod_LeggiMatrice = ""
        Dim c As Byte = 0
        Dim Trovato As Boolean = False
        Select Case Giustificativo
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
                Trovato = True
            Case "Ore Pausa"
                c = 44
                Trovato = True
            Case "Ore Lavorate"
                c = 45
                Trovato = True
            Case "Ore Diurne"
                c = 46
                Trovato = True
            Case "Ore Notturne"
                c = 47
                Trovato = True
            Case "Ore Festive Diurne"
                c = 48
                Trovato = True
            Case "Ore Festive Notturne"
                c = 49
                Trovato = True
            Case "Ore Retribuite"
                c = 50
                Trovato = True
        End Select
        If c = 0 Then
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit Function
                If Vettore_Calcolo_Orari.Elemento(c, 0) = Giustificativo & "/" & Familiare Then
                    Trovato = True
                    Exit For
                End If
            Next c
        End If
        If Trovato = True Then
            Mod_LeggiMatrice = Vettore_Calcolo_Orari.Elemento(c, Giorno)
            Mod_LeggiMatrice = Val(Sostituisci(Mod_LeggiMatrice, ".", ","))
        End If
    End Function

    Public Sub Mod_CambiaGiustificativoMatrice(ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari, ByVal Familiare As Object, ByVal GiustificativoOld As Object, ByVal GiustificativoNew As Object, ByVal Giorno As Object)
        Dim a As String
        Dim b As String
        Dim c As Byte = 0
        Select Case GiustificativoOld
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
            Case "Ore Pausa"
                c = 44
            Case "Ore Lavorate"
                c = 45
            Case "Ore Diurne"
                c = 46
            Case "Ore Notturne"
                c = 47
            Case "Ore Festive Diurne"
                c = 48
            Case "Ore Festive Notturne"
                c = 49
            Case "Ore Retribuite"
                c = 50
        End Select
        If c <> 0 Then Exit Sub
        For c = 0 To 40
            If Vettore_Calcolo_Orari.Elemento(c, 0) = GiustificativoOld & "/" & Familiare Then Exit For
        Next c
        If c = 0 Then Exit Sub
        Dim O As Byte = c
        c = 0
        Select Case GiustificativoNew
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
            Case "Ore Pausa"
                c = 44
            Case "Ore Lavorate"
                c = 45
            Case "Ore Diurne"
                c = 46
            Case "Ore Notturne"
                c = 47
            Case "Ore Festive Diurne"
                c = 48
            Case "Ore Festive Notturne"
                c = 49
            Case "Ore Retribuite"
                c = 50
        End Select
        If c <> 0 Then Exit Sub
        For c = 0 To 40
            If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Vettore_Calcolo_Orari.Elemento(c, 0) = GiustificativoNew & "/" & Familiare
            If Vettore_Calcolo_Orari.Elemento(c, 0) = GiustificativoNew & "/" & Familiare Then Exit For
        Next c
        Dim n As Byte = c
        If Vettore_Calcolo_Orari.Elemento(O, Giorno) = "" Then
            a = 0
        Else
            a = Sostituisci(Vettore_Calcolo_Orari.Elemento(O, Giorno), ".", ",")
        End If
        If Vettore_Calcolo_Orari.Elemento(n, Giorno) = "" Then
            b = 0
        Else
            b = Sostituisci(Vettore_Calcolo_Orari.Elemento(n, Giorno), ".", ",")
        End If
        c = Format(Mod_OperaConMinuti(a, "+", b), "00.00")
        Vettore_Calcolo_Orari.Elemento(n, Giorno) = Sostituisci(c, ",", ".")
        Vettore_Calcolo_Orari.Elemento(O, Giorno) = "00.00"
    End Sub

    Public Function Mod_MinutiInFasciaObbligatoria(ByVal Dipendente As Long, ByVal Data As Date, ByVal Dalle As String, ByVal Alle As String, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno, ByRef Vettore_Timbrature_Giorno As Cls_Struttura_Timbrature_Giorno) As Object
        Dim ceo As New Cls_ElaboraOrari
        Dim Risultato As String
        Dim TstDalle As String
        Dim TstAlle As String

        Mod_MinutiInFasciaObbligatoria = 0
        Risultato = ceo.SviluppoTurniOrdiniServizioSingoloGiorno(ConnectionString, Dipendente, Data, "S", Vettore_Orari_Giorno, Vettore_Timbrature_Giorno, "S", "N")

        For i = 0 To 14
            If Vettore_Orari_Giorno.Dalle(i) = "00.00" And Vettore_Orari_Giorno.Alle(i) = "00.00" Then Exit For
            If Vettore_Orari_Giorno.DalleObbligo(i) <> "00.00" And Vettore_Orari_Giorno.AlleObbligo(i) <> "00.00" Then
                If Vettore_Orari_Giorno.DalleObbligo(i) <= Dalle Then
                    TstDalle = Dalle
                    If Vettore_Orari_Giorno.AlleObbligo(i) > Dalle Then
                        If Vettore_Orari_Giorno.AlleObbligo(i) < Alle Then
                            Dalle = Vettore_Orari_Giorno.AlleObbligo(i)
                            TstAlle = Vettore_Orari_Giorno.AlleObbligo(i)
                        Else
                            TstAlle = Alle
                        End If
                        Mod_MinutiInFasciaObbligatoria = Mod_MinutiInFasciaObbligatoria + DateDiff("n", TstDalle, TstAlle)
                    End If
                Else
                    If Vettore_Orari_Giorno.DalleObbligo(i) < Alle Then
                        TstDalle = Vettore_Orari_Giorno.DalleObbligo(i)
                        If Vettore_Orari_Giorno.AlleObbligo(i) < Alle Then
                            Dalle = Vettore_Orari_Giorno.AlleObbligo(i)
                            TstAlle = Vettore_Orari_Giorno.AlleObbligo(i)
                        Else
                            TstAlle = Alle
                        End If
                        Mod_MinutiInFasciaObbligatoria = Mod_MinutiInFasciaObbligatoria + DateDiff("n", TstDalle, TstAlle)
                    End If
                End If
            End If
        Next i
    End Function

    Public Function Mod_MinutiInCentesimi(ByVal Primo As Object) As Object
        Dim Ore As Double
        Dim Minuti As Double
        Dim Valore As Double

        If InStr(1, Primo, ".") > 0 Then Primo = Sostituisci(Primo, ".", ",")
        If Not IsNumeric(Primo) Then Primo = 0
        Ore = Int(Primo)
        Minuti = Math.Round(Primo - Int(Primo), 2) * 100
        Minuti = Math.Round(Minuti / 60 * 100, 0)
        Valore = Math.Round(Ore + Minuti / 100, 2)
        Mod_MinutiInCentesimi = Valore
    End Function

    Public Function Mod_OperaConMinuti(ByVal Primo As Object, ByVal Operatore As Object, ByVal Secondo As Object, Optional ByVal TipoSecondo As String = "hh.nn") As Object
        Dim p As Double
        Dim s As Double
        Dim Ore As Double
        Dim Minuti As Double
        Dim Valore As String
        Dim v As Integer

        If InStr(1, Primo, ".") > 0 Then Primo = Sostituisci(Primo, ".", ",")
        If Not IsNumeric(Primo) Then Primo = 0
        If InStr(1, Secondo, ".") > 0 Then Secondo = Sostituisci(Secondo, ".", ",")
        If Not IsNumeric(Secondo) Then Secondo = 0
        If Primo < 0 Then
            Primo = Math.Round(Primo * -1, 2)
            p = Math.Round(Int(Primo) * 60, 0) + Math.Round((Primo - Int(Primo)) * 100, 0)
            p = Math.Round(p * -1, 0)
        Else
            p = Math.Round(Int(Primo) * 60, 0) + Math.Round((Primo - Int(Primo)) * 100, 0)
        End If
        If Operatore = "-" Or Operatore = "+" Then
            If TipoSecondo = "hh.nn" Then
                If Secondo < 0 Then
                    Secondo = Math.Round(Secondo * -1, 2)
                    s = Math.Round(Int(Secondo) * 60, 0) + Math.Round((Secondo - Int(Secondo)) * 100, 0)
                    s = Math.Round(s * -1, 0)
                Else
                    s = Math.Round(Int(Secondo) * 60, 0) + Math.Round((Secondo - Int(Secondo)) * 100, 0)
                End If
            Else
                s = Secondo
            End If
        Else
            s = Secondo
        End If
        If Operatore = "-" Then Minuti = p - s
        If Operatore = "+" Then Minuti = p + s
        If Operatore = "*" Then Minuti = Math.Round(p * s, 0)
        If Operatore = "/" Then Minuti = Math.Round(p / s, 0)
        If Minuti < 0 Then
            Minuti = Math.Round(Minuti * -1, 0)
            Ore = Int(Math.Round(Minuti / 60, 2))
            Minuti = Minuti - Math.Round(Ore * 60, 0)
            Valore = Math.Round(Ore + Minuti / 100, 2)
            Valore = Format(Math.Round(Valore * -1, 2), "#0.00-")
        Else
            Ore = Int(Math.Round(Minuti / 60, 2))
            Minuti = Minuti - Math.Round(Ore * 60, 0)
            Valore = Format(Math.Round(Ore + Minuti / 100, 2), "#0.00")
        End If
        Mod_OperaConMinuti = Valore
    End Function

    Public Function Mod_OperazioneInMinuti(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object, ByVal Contenuto As Object, ByVal Operatore As Object, Optional ByVal Provenienza As String = "A") As Object
        Dim Valore As Object
        Dim Risultato As Object

        Valore = Mod_Leggi_DatiVariabili(Dipendente, Codice, "<=", Data)
        Risultato = Mod_OperaConMinuti(Valore, Operatore, Contenuto)
        Mod_OperazioneInMinuti = Mod_Scrivi_DatiVariabili(Dipendente, Codice, Data, Risultato, Provenienza)
    End Function

    Public Function Mod_OperazioneInNumeri(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object, ByVal Contenuto As Object, ByVal Operatore As Object, Optional ByVal Provenienza As String = "A") As Object
        Dim Decimali As Object = 0
        Dim Valore As Object
        Dim t As New Cls_TipiDatiAnagrafici

        t.Leggi(ConnectionString, Codice)
        If t.Tipo = 200 Then Decimali = 2
        Valore = Leggi_DatiVariabili(Dipendente, Codice, "<=", Data)
        If Operatore = "+" Then Valore = Math.Round(Valore + Contenuto, Decimali)
        If Operatore = "-" Then Valore = Math.Round(Valore - Contenuto, Decimali)
        If Operatore = "*" Then Valore = Math.Round(Valore * Contenuto, Decimali)
        If Operatore = "/" Then Valore = Math.Round(Valore / Contenuto, Decimali)
        Mod_OperazioneInNumeri = Mod_Scrivi_DatiVariabili(Dipendente, Codice, Data, Valore, Provenienza)
    End Function

    Public Function Mod_OperazioneInTesto(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object, ByVal Contenuto As Object, Optional ByVal Provenienza As String = "A") As Object
        Mod_OperazioneInTesto = Mod_Scrivi_DatiVariabili(Dipendente, Codice, Data, Contenuto, Provenienza)
    End Function

    Public Function Mod_OreContrattuali(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal CodiceSuperGruppo As String, ByVal Contratto As String) As Single
        Dim comp As New Cls_OrarioMensilePatrono
        Dim com As New Cls_OrarioMensile

        Mod_OreContrattuali = 0
        If CodiceSuperGruppo <> "" Then
            comp.Leggi(ConnectionString, Anno, Contratto, CodiceSuperGruppo)
            If comp.Id <> 0 Then
                If Mese = comp.Mese Then
                    Mod_OreContrattuali = comp.TempoLavoro
                End If
                If Mese = comp.MeseStandard Then
                    Mod_OreContrattuali = comp.TempoLavoroStandard
                End If
            End If
        End If
        If Mod_OreContrattuali = 0 Then
            com.Leggi(ConnectionString, Anno, Contratto)
            If com.Id <> 0 Then
                Select Case Mese
                    Case 1
                        Mod_OreContrattuali = com.TempoLavoro1
                    Case 2
                        Mod_OreContrattuali = com.TempoLavoro2
                    Case 3
                        Mod_OreContrattuali = com.TempoLavoro3
                    Case 4
                        Mod_OreContrattuali = com.TempoLavoro4
                    Case 5
                        Mod_OreContrattuali = com.TempoLavoro5
                    Case 6
                        Mod_OreContrattuali = com.TempoLavoro6
                    Case 7
                        Mod_OreContrattuali = com.TempoLavoro7
                    Case 8
                        Mod_OreContrattuali = com.TempoLavoro8
                    Case 9
                        Mod_OreContrattuali = com.TempoLavoro9
                    Case 10
                        Mod_OreContrattuali = com.TempoLavoro10
                    Case 11
                        Mod_OreContrattuali = com.TempoLavoro11
                    Case 12
                        Mod_OreContrattuali = com.TempoLavoro12
                End Select
            End If
        End If
    End Function

    Public Function Mod_OreContrattualiNelGiorno(ByVal Data As Date, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String) As Single
        Dim cf As New Cls_Festivita
        Dim ccc As New Cls_CondizioniContrattuali
        Dim ceo As New Cls_ElaboraOrari
        Dim ccpm As New Cls_CondizioniPartimeMese

        Dim ProfiloOrario As String = ""
        Dim Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno
        Dim GiornoDal As Byte
        Dim GiornoAl As Byte
        Dim Orari(7) As Date
        Dim Orario As Date
        Dim TipoPartime As String
        Dim PercentualePartimeOre As Single
        Dim OreMinuti As Single
        Dim Ore As Integer
        Dim Minuti As Integer
        Dim DataXDif As Date = DateSerial(1899, 12, 30)
        Dim Risultato As String

        GiornoDal = 0
        GiornoAl = 0

        Mod_OreContrattualiNelGiorno = 0
        If Not IsDate(Data) Then Exit Function
        If CodiceContratto = "" Then Exit Function

        cf.Leggi(ConnectionString, Year(Data))
        If cf.Id = 0 Then Exit Function

        ccc.Trova(ConnectionString, CodiceContratto, Data)
        If ccc.Id = 0 Then Exit Function
        Orari(1) = ccc.OrarioDomenica
        Orari(2) = ccc.OrarioLunedi
        Orari(3) = ccc.OrarioMartedi
        Orari(4) = ccc.OrarioMercoledi
        Orari(5) = ccc.OrarioGiovedi
        Orari(6) = ccc.OrarioVenerdi
        Orari(7) = ccc.OrarioSabato
        ProfiloOrario = ccc.CodiceProfiloOrario
        TipoPartime = ccc.TipoPartime
        PercentualePartimeOre = ccc.PercentualePartimeOre
        Orario = Orari(Weekday(Data))

        If ProfiloOrario <> "" Then
            Risultato = ceo.SviluppoProfiloOrarioSingoloGiorno(ConnectionString, CodiceSuperGruppo, CodiceContratto, ProfiloOrario, Data, Vettore_Orari_Giorno)

            OreMinuti = 0
            For r = 0 To 14
                If Vettore_Orari_Giorno.Tipo(r) = "P" Then
                    OreMinuti = Mod_OperaConMinuti(OreMinuti, "+", Calcolo_OreMinuti(Vettore_Orari_Giorno.Dalle(r), Vettore_Orari_Giorno.Alle(r)))
                End If
            Next r
            Ore = Int(OreMinuti)
            Minuti = (OreMinuti - Ore) * 100
            Orario = Format(Ore, "00") & "." & Format(Minuti, "00")

        Else

            ccpm.Trova(ConnectionString, CodiceContratto, Data, Month(Data))
            If ccpm.Id > 0 Then
                GiornoDal = ccpm.GiornoDal
                GiornoAl = ccpm.GiornoAl
                Orari(1) = ccpm.OrarioDomenica
                Orari(2) = ccpm.OrarioLunedi
                Orari(3) = ccpm.OrarioMartedi
                Orari(4) = ccpm.OrarioMercoledi
                Orari(5) = ccpm.OrarioGiovedi
                Orari(6) = ccpm.OrarioVenerdi
                Orari(7) = ccpm.OrarioSabato
                If (GiornoDal = 0 Or GiornoDal <= Day(Data)) And (GiornoAl = 0 Or GiornoAl >= Day(Data)) Then
                    Orario = Orari(Weekday(Data))
                End If
            End If
        End If

        Minuti = 0
        If GiornoFestivo(Data, CodiceSuperGruppo) = False Then
            Minuti = Minuti + DateDiff("n", DataXDif, Orario)
        End If

        If TipoPartime = "PAO" Then
            Minuti = Math.Round(Minuti * PercentualePartimeOre)
        End If
        Ore = Int(Math.Round(Minuti / 60, 2))
        Minuti = Minuti - Math.Round(Ore * 60, 0)
        Mod_OreContrattualiNelGiorno = Math.Round(Ore + Minuti / 100, 2)
    End Function

    Public Function Mod_Leggi_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object, Optional ByVal Decodificato As Boolean = False) As Object

        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim cpt As New Cls_ParametriTurni

        Dim Inizio As Date
        Dim Risultato As Object

        Mod_Leggi_DatiVariabili = ""

        ctda.Leggi(ConnectionString, Codice)

        If ctda.Formula <> "" Then
            Dim cct As New Cls_CalcoloTurni
            cct.ConnectionString = ConnectionString
            Risultato = cct.Esegui_FormulaDatiVariabili(Dipendente, Data, ctda.Formula)
            If InStr(Risultato, "(E)") = 0 Then
                Mod_Leggi_DatiVariabili = Risultato
            End If
            Exit Function
        End If

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()

        If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If ctda.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If ctda.Scadenza = "G" Then Inizio = Data
        If ctda.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND CodiceVariabile = ?" & _
                    " AND Validita " & Modo & " ?" & _
                    " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND CodiceVariabile = ?" & _
                    " AND Validita >= ?" & _
                    " AND Validita " & Modo & " ?" & _
                    " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Select Case ctda.Tipo
                Case cpt.Cst_Testo
                    Mod_Leggi_DatiVariabili = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                Case cpt.Cst_Data
                    Mod_Leggi_DatiVariabili = DataDb(myPOSTreader.Item("ContenutoData"))
                Case cpt.Cst_DataOra
                    Mod_Leggi_DatiVariabili = DataDb(myPOSTreader.Item("ContenutoData"))
                Case cpt.Cst_Orario
                    Mod_Leggi_DatiVariabili = Format(DataDb(myPOSTreader.Item("ContenutoData")), "HH.mm")
                Case cpt.Cst_VeroFalso
                    If NumeroDb(myPOSTreader.Item("ContenutoNumero")) = 1 Then
                        Mod_Leggi_DatiVariabili = "Vero"
                    Else
                        Mod_Leggi_DatiVariabili = "Falso"
                    End If
                Case cpt.Cst_SiNo
                    If NumeroDb(myPOSTreader.Item("ContenutoNumero")) = 1 Then
                        Mod_Leggi_DatiVariabili = "Si"
                    Else
                        Mod_Leggi_DatiVariabili = "No"
                    End If
                Case cpt.Cst_Percentuale
                    Mod_Leggi_DatiVariabili = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
                Case cpt.Cst_NumericoVirgola
                    Mod_Leggi_DatiVariabili = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
                Case cpt.Cst_NumericoIntero
                    Mod_Leggi_DatiVariabili = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            End Select

        Else

            Select Case ctda.Tipo
                Case cpt.Cst_Testo
                    Mod_Leggi_DatiVariabili = ""
                Case cpt.Cst_Data
                    Mod_Leggi_DatiVariabili = DateSerial(1899, 12, 30)
                Case cpt.Cst_DataOra
                    Mod_Leggi_DatiVariabili = DateSerial(1899, 12, 30)
                Case cpt.Cst_Orario
                    Mod_Leggi_DatiVariabili = "00.00"
                Case cpt.Cst_VeroFalso
                    Mod_Leggi_DatiVariabili = "Falso"
                Case cpt.Cst_SiNo
                    Mod_Leggi_DatiVariabili = "No"
                Case cpt.Cst_Percentuale
                    Mod_Leggi_DatiVariabili = 0
                Case cpt.Cst_NumericoVirgola
                    Mod_Leggi_DatiVariabili = 0
                Case cpt.Cst_NumericoIntero
                    Mod_Leggi_DatiVariabili = 0
            End Select
            Exit Function
        End If
        myPOSTreader.Close()
        cn.Close()
        If Decodificato = True And ctda.TabellaDescrittiva <> "" Then
            Dim ctdr As New Cls_TabelleDescrittiveRiga
            Mod_Leggi_DatiVariabili = ctdr.Decodifica(ConnectionString, ctda.TabellaDescrittiva, Mod_Leggi_DatiVariabili)
        End If
    End Function

    Public Function Mod_Scrivi_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object, ByVal Contenuto As Object, ByVal Provenienza As Object) As Object

        Dim k As New Cls_TipiDatiAnagrafici
        Dim cpt As New Cls_ParametriTurni

        Dim a As Byte = 0
        If Codice = "COAC" Then
            a = a
        End If

        k.Leggi(ConnectionString, Codice)
        If k.Id = 0 Then
            Mod_Scrivi_DatiVariabili = "Il Codice '" & Codice & "' NON è stato scritto perchè NON esiste nella Tabella 'TipiDatiAnagrafici'"
            Exit Function
        End If

        Dim d As New Cls_DatiVariabili

        d.Leggi(ConnectionString, Dipendente, Codice, Data)
        If d.Id = 0 Then
            d.Utente = Utente
            d.CodiceDipendente = Dipendente
            d.CodiceVariabile = Codice
            d.Validita = Data
            d.Tipo = k.Tipo
        Else
            If d.Provenienza <> Provenienza Then
                Mod_Scrivi_DatiVariabili = "Il Codice '" & Codice & "' NON è stato scritto perchè ne esiste uno di altra provenienza"
                Exit Function
            End If
        End If

        d.Provenienza = Provenienza

        Select Case d.Tipo
            Case cpt.Cst_Testo
                d.ContenutoTesto = Contenuto
            Case cpt.Cst_Data
                d.ContenutoData = Contenuto
            Case cpt.Cst_DataOra
                d.ContenutoData = Contenuto
            Case cpt.Cst_Orario
                d.ContenutoData = Format(Contenuto, "HH.mm")
            Case cpt.Cst_VeroFalso
                If Contenuto = "Vero" Then
                    d.ContenutoNumero = 1
                Else
                    d.ContenutoNumero = 0
                End If
            Case cpt.Cst_SiNo
                If Contenuto = "Si" Then
                    d.ContenutoNumero = 1
                Else
                    d.ContenutoNumero = 0
                End If
            Case cpt.Cst_Percentuale
                d.ContenutoNumero = Contenuto
            Case cpt.Cst_NumericoVirgola
                d.ContenutoNumero = Contenuto
            Case cpt.Cst_NumericoIntero
                d.ContenutoNumero = Contenuto
        End Select
        d.Aggiorna(ConnectionString)
        Mod_Scrivi_DatiVariabili = ""
    End Function

    Public Function Mod_Cancella_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Data As Object) As Object
        Dim d As New Cls_DatiVariabili

        Mod_Cancella_DatiVariabili = False

        d.Leggi(ConnectionString, Dipendente, Codice, Data)
        If d.Id > 0 Then
            Mod_Cancella_DatiVariabili = True
            d.Elimina(ConnectionString, Dipendente, Codice, Data)
        End If
    End Function

    Public Function Mod_LeggiDipendente_DatiVariabili(ByVal Contenuto As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Long

        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim cpt As New Cls_ParametriTurni

        Dim Condizione As String
        Dim Inizio As Date

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        ctda.Leggi(ConnectionString, Codice)

        Condizione = ""
        Select Case ctda.Tipo
            Case cpt.Cst_Testo
                Condizione = " AND ContenutoTesto = ?"
            Case cpt.Cst_Data
                Condizione = " AND ContenutoData = ?"
            Case cpt.Cst_DataOra
                Condizione = " AND ContenutoData = ?"
            Case cpt.Cst_Orario
                Condizione = " AND ContenutoData = ?"
            Case cpt.Cst_VeroFalso
                Condizione = " AND ContenutoNumero = ?"
            Case cpt.Cst_SiNo
                Condizione = " AND ContenutoNumero = ?"
            Case cpt.Cst_Percentuale
                Condizione = " AND ContenutoNumero = ?"
            Case cpt.Cst_NumericoVirgola
                Condizione = " AND ContenutoNumero = ?"
            Case cpt.Cst_NumericoIntero
                Condizione = " AND ContenutoNumero = ?"
        End Select

        Dim cmd As New OleDbCommand()

        If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If ctda.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If ctda.Scadenza = "G" Then Inizio = Data
        If ctda.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceVariabile = ?" & _
                                Condizione & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@Codice", Codice)
            cmd.Parameters.AddWithValue("@Contenuto", Contenuto)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceVariabile = '" & Codice & "'" & _
                                Condizione & _
                              " AND Validita >= ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@Codice", Codice)
            cmd.Parameters.AddWithValue("@Contenuto", Contenuto)
            cmd.Parameters.AddWithValue("@Inizio", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_LeggiDipendente_DatiVariabili = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Leggi_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object

        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim cpt As New Cls_ParametriTurni

        Dim Inizio As Date
        Dim Risultato As Object

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        ctda.Leggi(ConnectionString, Codice)

        If ctda.Formula <> "" Then
            Dim cr As New Cls_CalcoloTurni
            cr.ConnectionString = ConnectionString
            Risultato = cr.Esegui_FormulaDatiVariabili(Struttura, Data, ctda.Formula)
            If InStr(Risultato, "(E)") = 0 Then
                Mod_Leggi_DatiVariabiliStruttura = Risultato
            End If
            Exit Function
        End If

        Dim cmd As New OleDbCommand()

        If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If ctda.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If ctda.Scadenza = "G" Then Inizio = Data
        If ctda.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                              " WHERE CodiceStruttura = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceStruttura", Struttura)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                              " WHERE CodiceStruttura = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceStruttura", Struttura)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Inizio", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Select Case ctda.Tipo
                Case cpt.Cst_Testo
                    Mod_Leggi_DatiVariabiliStruttura = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                Case cpt.Cst_Data
                    Mod_Leggi_DatiVariabiliStruttura = DataDb(myPOSTreader.Item("ContenutoData"))
                Case cpt.Cst_DataOra
                    Mod_Leggi_DatiVariabiliStruttura = DataDb(myPOSTreader.Item("ContenutoData"))
                Case cpt.Cst_Orario
                    Mod_Leggi_DatiVariabiliStruttura = Format(DataDb(myPOSTreader.Item("ContenutoData")), "HH.mm")
                Case cpt.Cst_VeroFalso
                    If NumeroDb(myPOSTreader.Item("ContenutoNumero")) = 1 Then
                        Mod_Leggi_DatiVariabiliStruttura = "Vero"
                    Else
                        Mod_Leggi_DatiVariabiliStruttura = "Falso"
                    End If
                Case cpt.Cst_SiNo
                    If NumeroDb(myPOSTreader.Item("ContenutoNumero")) = 1 Then
                        Mod_Leggi_DatiVariabiliStruttura = "Si"
                    Else
                        Mod_Leggi_DatiVariabiliStruttura = "No"
                    End If
                Case cpt.Cst_Percentuale
                    Mod_Leggi_DatiVariabiliStruttura = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
                Case cpt.Cst_NumericoVirgola
                    Mod_Leggi_DatiVariabiliStruttura = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
                Case cpt.Cst_NumericoIntero
                    Mod_Leggi_DatiVariabiliStruttura = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            End Select

        Else

            Select Case ctda.Tipo
                Case cpt.Cst_Testo
                    Mod_Leggi_DatiVariabiliStruttura = ""
                Case cpt.Cst_Data
                    Mod_Leggi_DatiVariabiliStruttura = DateSerial(1899, 12, 30)
                Case cpt.Cst_DataOra
                    Mod_Leggi_DatiVariabiliStruttura = DateSerial(1899, 12, 30)
                Case cpt.Cst_Orario
                    Mod_Leggi_DatiVariabiliStruttura = "00.00"
                Case cpt.Cst_VeroFalso
                    Mod_Leggi_DatiVariabiliStruttura = "Falso"
                Case cpt.Cst_SiNo
                    Mod_Leggi_DatiVariabiliStruttura = "No"
                Case cpt.Cst_Percentuale
                    Mod_Leggi_DatiVariabiliStruttura = 0
                Case cpt.Cst_NumericoVirgola
                    Mod_Leggi_DatiVariabiliStruttura = 0
                Case cpt.Cst_NumericoIntero
                    Mod_Leggi_DatiVariabiliStruttura = 0
            End Select
            Exit Function
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Somma_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date
        Dim c As Integer

        Mod_Somma_DatiVariabiliStruttura = 0
        If Struttura = "" Then Exit Function
        c = Len(Struttura)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        ctda.Leggi(ConnectionString, Codice)

        If ctda.Tipo <> 200 And ctda.Tipo <> 100 Then Exit Function

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice FROM ParagrafoDiStruttura" & _
                          " WHERE Codice >= ?" & _
                          " ORDER BY Codice"
        cmd.Parameters.AddWithValue("@Codice", Struttura)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim cmd1 As New OleDbCommand()
            If Left(StringaDb(myPOSTreader.Item("Codice")), c) <> Struttura Then Exit Do
            If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
            If ctda.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
            If ctda.Scadenza = "G" Then Inizio = Data
            If ctda.Scadenza = "N" Then
                cmd1.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                        " WHERE CodiceStruttura = ?" & _
                        " AND CodiceVariabile = ?" & _
                        " AND Validita " & Modo & " ?" & _
                        " ORDER BY Validita DESC, Id"
                cmd1.Parameters.AddWithValue("@CodiceStruttura", StringaDb(myPOSTreader.Item("Codice")))
                cmd1.Parameters.AddWithValue("@CodiceVariabile", Codice)
                cmd1.Parameters.AddWithValue("@Validita", Data)
            Else
                cmd1.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                        " WHERE CodiceStruttura = ?" & _
                        " AND CodiceVariabile = ?" & _
                        " AND Validita >= ?" & _
                        " AND Validita " & Modo & " ?" & _
                        " ORDER BY Validita DESC, Id"
                cmd1.Parameters.AddWithValue("@CodiceStruttura", StringaDb(myPOSTreader.Item("Codice")))
                cmd1.Parameters.AddWithValue("@CodiceVariabile", Codice)
                cmd1.Parameters.AddWithValue("@Validita", Inizio)
                cmd1.Parameters.AddWithValue("@Validita", Data)
            End If

            cmd1.Connection = cn
            Dim myPOSTreader_1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader_1.Read Then
                Mod_Somma_DatiVariabiliStruttura = Mod_Somma_DatiVariabiliStruttura + NumeroDb(myPOSTreader_1.Item("ContenutoNumero"))
            End If
            myPOSTreader_1.Close()
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Conta_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date
        Dim c As Integer

        Mod_Conta_DatiVariabiliStruttura = 0
        If Struttura = "" Then Exit Function
        c = Len(Struttura)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        ctda.Leggi(ConnectionString, Codice)

        If ctda.Tipo <> 200 And ctda.Tipo <> 100 Then Exit Function

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice FROM ParagrafoDiStruttura" & _
                          " WHERE Codice >= ?" & _
                          " ORDER BY Codice"
        cmd.Parameters.AddWithValue("@Codice", Struttura)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim cmd1 As New OleDbCommand()
            If Left(StringaDb(myPOSTreader.Item("Codice")), c) <> Struttura Then Exit Do
            If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
            If ctda.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
            If ctda.Scadenza = "G" Then Inizio = Data
            If ctda.Scadenza = "N" Then
                cmd1.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                        " WHERE CodiceStruttura = ?" & _
                        " AND CodiceVariabile = ?" & _
                        " AND Validita " & Modo & " ?" & _
                        " ORDER BY Validita DESC, Id"
                cmd1.Parameters.AddWithValue("@CodiceStruttura", StringaDb(myPOSTreader.Item("Codice")))
                cmd1.Parameters.AddWithValue("@CodiceVariabile", Codice)
                cmd1.Parameters.AddWithValue("@Validita", Data)
            Else
                cmd1.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                        " WHERE CodiceStruttura = ?" & _
                        " AND CodiceVariabile = ?" & _
                        " AND Validita >= ?" & _
                        " AND Validita " & Modo & " ?" & _
                        " ORDER BY Validita DESC, Id"
                cmd1.Parameters.AddWithValue("@CodiceStruttura", StringaDb(myPOSTreader.Item("Codice")))
                cmd1.Parameters.AddWithValue("@CodiceVariabile", Codice)
                cmd1.Parameters.AddWithValue("@Validita", Inizio)
                cmd1.Parameters.AddWithValue("@Validita", Data)
            End If

            cmd1.Connection = cn
            Dim myPOSTreader_1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader_1.Read Then
                Mod_Conta_DatiVariabiliStruttura = Mod_Conta_DatiVariabiliStruttura + 1
            End If
            myPOSTreader_1.Close()
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Cerca_SuOrariTimbrature(ByVal Giorno As Object, ByVal Campo As Object, ByVal Valore As Object, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As Boolean

        Mod_Cerca_SuOrariTimbrature = False
        Dim g As Byte = Giorno

        For i = 0 To 14
            Select Case Campo
                Case "Dalle"
                    If Vettore_Orari_Timbrature.Dalle(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Alle"
                    If Vettore_Orari_Timbrature.Alle(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Pausa"
                    If Vettore_Orari_Timbrature.Pausa(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Causale"
                    If Vettore_Orari_Timbrature.Causale(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Colore"
                    If Vettore_Orari_Timbrature.Colore(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Giustificativo"
                    If Vettore_Orari_Timbrature.Giustificativo(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Tipo"
                    If Vettore_Orari_Timbrature.Tipo(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "Qualita"
                    If InStr(1, Vettore_Orari_Timbrature.Qualita(i, g), Valore) > 0 Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "TipoServizio"
                    If Vettore_Orari_Timbrature.TipoServizio(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
                Case "GiornoSuccessivo"
                    If Vettore_Orari_Timbrature.GiornoSuccessivo(i, g) = Valore Then
                        Mod_Cerca_SuOrariTimbrature = True
                        Exit For
                    End If
            End Select
        Next i
    End Function

    Public Function Mod_ContinuazioneDiOrario(ByVal Data As Object, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As Boolean
        Dim Vettore_Orari_Giorno As New Cls_Struttura_Orari_Giorno
        Dim cg As New Cls_Giustificativi

        Mod_ContinuazioneDiOrario = False
        Dim g As Byte = Day(Data)
        If Vettore_Orari_Timbrature.Dalle(0, g) <> "00.00" Then Exit Function
        g = g - 1
        For i = 0 To 9
            If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "Tipo") = "P" Then
                Mod_ContinuazioneDiOrario = True
                Exit Function
            End If
        Next i
    End Function

    Public Function Mod_Somma_DatiVariabiliNelPeriodo(ByVal Dipendente As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim k As New Cls_TipiDatiAnagrafici

        Mod_Somma_DatiVariabiliNelPeriodo = 0
        If Dipendente = 0 Then Exit Function

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        k.Leggi(ConnectionString, Codice)

        If k.Tipo <> 200 And k.Tipo <> 100 Then Exit Function

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM DatiVariabili" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita >= ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
        cmd.Parameters.AddWithValue("@Validita", DataDal)
        cmd.Parameters.AddWithValue("@Validita", DataAl)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If k.TipoNumericoVirgola = "T" Then
                Mod_Somma_DatiVariabiliNelPeriodo = Mod_OperaConMinuti(Mod_Somma_DatiVariabiliNelPeriodo, "+", NumeroDb(myPOSTreader.Item("ContenutoNumero")))
            Else
                Mod_Somma_DatiVariabiliNelPeriodo = Mod_Somma_DatiVariabiliNelPeriodo + NumeroDb(myPOSTreader.Item("ContenutoNumero"))
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_DataAdozione(ByVal CodiceDipendente As Object, ByVal Validita As Date) As Object

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT MAX(DataAdozione) FROM Familiari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Parentela = 'F'" & _
                          " AND DataAdozione <= ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@DataAdozione", Validita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_DataAdozione = DataDb(myPOSTreader.Item("DataAdozione"))
        Else
            Mod_DataAdozione = DateSerial(1899, 12, 30)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_DataParto(ByVal CodiceDipendente As Object, ByVal Validita As Date) As Object

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT MAX(DataNascita) FROM Familiari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Parentela = 'F'" & _
                          " AND DataNascita <= ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@DataNascita", Validita)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_DataParto = DataDb(myPOSTreader.Item("DataNascita"))
        Else
            Mod_DataParto = DateSerial(1899, 12, 30)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Delta_DatiVariabiliNelPeriodo(ByVal Dipendente As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim ctda As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date
        Dim ValoreDal As Object = 0
        Dim ValoreAl As Object = 0

        Mod_Delta_DatiVariabiliNelPeriodo = 0
        If Dipendente = 0 Then Exit Function

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        ctda.Leggi(ConnectionString, Codice)

        Dim cmd As New OleDbCommand()
        If ctda.Tipo <> 200 And ctda.Tipo <> 100 Then Exit Function
        If ctda.Scadenza = "M" Or ctda.Scadenza = "G" Then Exit Function
        If ctda.Scadenza = "A" Then Inizio = DateSerial(Year(DataDal), 1, 1)
        If ctda.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", DataDal)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Inizio", Inizio)
            cmd.Parameters.AddWithValue("@Validita", DataDal)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ValoreDal = NumeroDb(myPOSTreader.Item("ContenutoNumero"))
        End If
        myPOSTreader.Close()

        If ctda.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", DataAl)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Inizio", Inizio)
            cmd.Parameters.AddWithValue("@Validita", DataAl)
        End If

        Dim myPOSTreader_1 As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader_1.Read Then
            ValoreAl = NumeroDb(myPOSTreader_1.Item("ContenutoNumero"))
        End If

        If ctda.TipoNumericoVirgola = "T" Then
            Mod_Delta_DatiVariabiliNelPeriodo = Mod_OperaConMinuti(ValoreAl, "-", ValoreDal)
        Else
            Mod_Delta_DatiVariabiliNelPeriodo = ValoreAl - ValoreDal
        End If
        myPOSTreader_1.Close()
        cn.Close()
    End Function

    Public Function Mod_Somma_GiustificativiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As Object
        Mod_Somma_GiustificativiNelMese = 0
        If GiornoDal > GiornoAl Then Exit Function

        Dim c As Integer = 0
        Select Case Codice
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
            Case "Ore Pausa"
                c = 44
            Case "Ore Lavorate"
                c = 45
            Case "Ore Diurne"
                c = 46
            Case "Ore Notturne"
                c = 47
            Case "Ore Festive Diurne"
                c = 48
            Case "Ore Festive Notturne"
                c = 49
            Case "Ore Retribuite"
                c = 50
        End Select

        If c = 0 Then
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit Function
                If Vettore_Calcolo_Orari.Elemento(c, 0) = Codice & "/" & Familiare Then
                    For g = GiornoDal To GiornoAl
                        If Vettore_Calcolo_Orari.Elemento(c, g) <> "" And Vettore_Calcolo_Orari.Elemento(c, g) <> "00.00" Then
                            Mod_Somma_GiustificativiNelMese = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelMese, "+", Sostituisci(Vettore_Calcolo_Orari.Elemento(c, g), ".", ","))
                        End If
                    Next g
                End If
            Next c
        Else
            For g = GiornoDal To GiornoAl
                If Vettore_Calcolo_Orari.Elemento(c, g) <> "00.00" Then
                    Mod_Somma_GiustificativiNelMese = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelMese, "+", Sostituisci(Vettore_Calcolo_Orari.Elemento(c, g), ".", ","))
                End If
            Next g
        End If
    End Function

    Public Function Mod_Conta_GiustificativiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As Object
        Mod_Conta_GiustificativiNelMese = 0
        If GiornoDal > GiornoAl Then Exit Function

        Dim c As Integer = 0
        Select Case Codice
            Case "Ore Work_1"
                c = 41
            Case "Ore Work_2"
                c = 42
            Case "Ore Previste"
                c = 43
            Case "Ore Pausa"
                c = 44
            Case "Ore Lavorate"
                c = 45
            Case "Ore Diurne"
                c = 46
            Case "Ore Notturne"
                c = 47
            Case "Ore Festive Diurne"
                c = 48
            Case "Ore Festive Notturne"
                c = 49
            Case "Ore Retribuite"
                c = 50
        End Select

        If c = 0 Then
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit Function
                If Vettore_Calcolo_Orari.Elemento(c, 0) = Codice & "/" & Familiare Then
                    For g = GiornoDal To GiornoAl
                        If Vettore_Calcolo_Orari.Elemento(c, g) <> "" Then
                            Mod_Conta_GiustificativiNelMese = Mod_Conta_GiustificativiNelMese + 1
                        End If
                    Next g
                End If
            Next c
        Else
            For g = GiornoDal To GiornoAl
                If Vettore_Calcolo_Orari.Elemento(c, g) <> "00.00" Then
                    Mod_Conta_GiustificativiNelMese = Mod_Conta_GiustificativiNelMese + 1
                End If
            Next g
        End If
    End Function

    Public Function Mod_Conta_GiustificativiContiguiNelMese(ByVal Familiare As Object, ByVal Codice As Object, ByVal CodiceAlternativo As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As Object
        Mod_Conta_GiustificativiContiguiNelMese = 0
        If GiornoDal > GiornoAl Then Exit Function

        Dim Indice As Integer = 0
        Select Case Codice
            Case "Ore Work_1"
                Indice = 41
            Case "Ore Work_2"
                Indice = 42
            Case "Ore Previste"
                Indice = 43
            Case "Ore Pausa"
                Indice = 44
            Case "Ore Lavorate"
                Indice = 45
            Case "Ore Diurne"
                Indice = 46
            Case "Ore Notturne"
                Indice = 47
            Case "Ore Festive Diurne"
                Indice = 48
            Case "Ore Festive Notturne"
                Indice = 49
            Case "Ore Retribuite"
                Indice = 50
        End Select

        Dim IndiceAlternativo As Integer = 0
        Select Case CodiceAlternativo
            Case "Ore Work_1"
                IndiceAlternativo = 41
            Case "Ore Work_2"
                IndiceAlternativo = 42
            Case "Ore Previste"
                IndiceAlternativo = 43
            Case "Ore Pausa"
                IndiceAlternativo = 44
            Case "Ore Lavorate"
                IndiceAlternativo = 45
            Case "Ore Diurne"
                IndiceAlternativo = 46
            Case "Ore Notturne"
                IndiceAlternativo = 47
            Case "Ore Festive Diurne"
                IndiceAlternativo = 48
            Case "Ore Festive Notturne"
                IndiceAlternativo = 49
            Case "Ore Retribuite"
                IndiceAlternativo = 50
        End Select

        If Indice = 0 Then
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit Function
                If Vettore_Calcolo_Orari.Elemento(c, 0) = Codice & "/" & Familiare Then
                    For g = GiornoDal To GiornoAl
                        If Vettore_Calcolo_Orari.Elemento(c, g) = "" Then
                            If CodiceAlternativo = "" Then
                                Exit Function
                            Else
                                If IndiceAlternativo = 0 Then
                                    For ca = 0 To 40
                                        If Vettore_Calcolo_Orari.Elemento(ca, 0) = "" Then Exit Function
                                        If Vettore_Calcolo_Orari.Elemento(ca, 0) = CodiceAlternativo & "/" & Familiare Then
                                            If Vettore_Calcolo_Orari.Elemento(ca, g) = "" Then Exit Function
                                            Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                                            Exit For
                                        End If
                                    Next ca
                                Else
                                    If Vettore_Calcolo_Orari.Elemento(IndiceAlternativo, g) = "00.00" Then Exit Function
                                    Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                                End If
                            End If
                        Else
                            Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                        End If
                    Next g
                End If
            Next c

        Else

            For g = GiornoDal To GiornoAl
                If Vettore_Calcolo_Orari.Elemento(Indice, g) = "00.00" Then
                    If CodiceAlternativo = "" Then
                        Exit Function
                    Else
                        If IndiceAlternativo = 0 Then
                            For ca = 0 To 40
                                If Vettore_Calcolo_Orari.Elemento(ca, 0) = "" Then Exit Function
                                If Vettore_Calcolo_Orari.Elemento(ca, 0) = CodiceAlternativo & "/" & Familiare Then
                                    If Vettore_Calcolo_Orari.Elemento(ca, g) = "" Then Exit Function
                                    Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                                    Exit For
                                End If
                            Next ca
                        Else
                            If Vettore_Calcolo_Orari.Elemento(IndiceAlternativo, g) = "00.00" Then Exit Function
                            Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                        End If
                    End If
                Else
                    Mod_Conta_GiustificativiContiguiNelMese = Mod_Conta_GiustificativiContiguiNelMese + 1
                End If
            Next g
        End If
    End Function

    Public Function Mod_Conta_GiustificativiMalattiaContiguiNelMese(ByVal Familiare As Object, ByVal GiornoDal As Object, ByVal GiornoAl As Object, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As Object
        Dim cg As New Cls_Giustificativi

        Mod_Conta_GiustificativiMalattiaContiguiNelMese = 0
        Dim Sw_Ok As Boolean = True
        For g = GiornoAl To GiornoDal Step -1
            If Sw_Ok = False Then Exit For
            Sw_Ok = False
            For c = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit For
                Dim Barra As String = InStr(1, Vettore_Calcolo_Orari.Elemento(c, 0), "/")
                Dim CodGiu As String = Mid(Vettore_Calcolo_Orari.Elemento(c, 0), 1, Barra - 1)
                Dim CodFam As Byte = Val(Mid(Vettore_Calcolo_Orari.Elemento(c, 0), Barra + 1, Len(Vettore_Calcolo_Orari.Elemento(c, 0)) - Barra))
                If Familiare = CodFam Then
                    If cg.CampoGiustificativi(ConnectionString, CodGiu, "Malattia") = "S" Then
                        If Vettore_Calcolo_Orari.Elemento(c, g) <> "" Then
                            Mod_Conta_GiustificativiMalattiaContiguiNelMese = Mod_Conta_GiustificativiMalattiaContiguiNelMese + 1
                            Sw_Ok = True
                            Exit For
                        End If
                    End If
                End If
            Next c
        Next g
    End Function

    Public Function Mod_Conta_GiustificativiMalattiaContiguiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim Vettore_Calcolo_Orari As New Cls_Struttura_Calcolo_Orari
        Dim cg As New Cls_Giustificativi
        Dim cct As New Cls_CalcoloTurni
        cct.ConnectionString = ConnectionString

        Mod_Conta_GiustificativiMalattiaContiguiNelPeriodo = 0
        Dim Sw_MeseAl As Boolean = False
        If Not IsDate(DataDal) Then Exit Function
        If Not IsDate(DataAl) Then Exit Function
        If Format(DataDal, "yyyymmdd") > Format(DataAl, "yyyymmdd") Then Exit Function

        Dim Mesi As Integer = DateDiff("m", DataDal, DataAl)
        Dim Anno As Integer = Year(DataAl)
        Dim Mese As Byte = Month(DataAl)
        Dim GiornoAl As Byte = Day(DataAl)
        Dim GiornoDal As Byte
        Dim Barra As Integer
        Dim CodGiu As String
        Dim CodFam As String

        Dim Sw_Ok As Boolean = True
        For m = 0 To Mesi
            If Sw_Ok = False Then Exit For
            cct.Carica_VettoreCalcoloOrari(Dipendente, Anno, Mese, Vettore_Calcolo_Orari)
            If m = Mesi Then
                GiornoDal = Day(DataDal)
            Else
                GiornoDal = 1
            End If
            For g = GiornoAl To GiornoDal Step -1
                If Sw_Ok = False Then Exit For
                Sw_Ok = False
                For c = 0 To 40
                    If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit For
                    Barra = InStr(1, Vettore_Calcolo_Orari.Elemento(c, 0), "/")
                    CodGiu = Mid(Vettore_Calcolo_Orari.Elemento(c, 0), 1, Barra - 1)
                    CodFam = Mid(Vettore_Calcolo_Orari.Elemento(c, 0), Barra + 1, Len(Vettore_Calcolo_Orari.Elemento(c, 0)) - Barra)
                    If Familiare = CodFam Then
                        If cg.CampoGiustificativi(ConnectionString, CodGiu, "Malattia") = "S" Then
                            If Vettore_Calcolo_Orari.Elemento(c, g) <> "" Then
                                Mod_Conta_GiustificativiMalattiaContiguiNelPeriodo = Mod_Conta_GiustificativiMalattiaContiguiNelPeriodo + 1
                                Sw_Ok = True
                                Exit For
                            End If
                        End If
                    End If
                Next c
            Next g
            Mese = Mese - 1
            If Mese = 0 Then
                Anno = Anno - 1
                Mese = 12
            End If
        Next m
    End Function

    Public Function Mod_Somma_GiustificativiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim Indice As Integer
        Dim AnnoDal As Integer
        Dim MeseDal As Byte
        Dim GiornoDal As Byte
        Dim AnnoAl As Integer
        Dim MeseAl As Byte
        Dim GiornoAl As Byte


        Mod_Somma_GiustificativiNelPeriodo = 0
        If Not IsDate(DataDal) Then Exit Function
        If Not IsDate(DataAl) Then Exit Function
        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then Exit Function

        Indice = 0
        Select Case Codice
            Case "Ore Work_1"
                Indice = 41
            Case "Ore Work_2"
                Indice = 42
            Case "Ore Previste"
                Indice = 43
            Case "Ore Pausa"
                Indice = 44
            Case "Ore Lavorate"
                Indice = 45
            Case "Ore Diurne"
                Indice = 46
            Case "Ore Notturne"
                Indice = 47
            Case "Ore Festive Diurne"
                Indice = 48
            Case "Ore Festive Notturne"
                Indice = 49
            Case "Ore Retribuite"
                Indice = 50
        End Select

        AnnoDal = Year(DataDal)
        MeseDal = Month(DataDal)
        GiornoDal = Day(DataDal)
        AnnoAl = Year(DataAl)
        MeseAl = Month(DataAl)
        GiornoAl = Day(DataAl)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        If Indice = 0 Then
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Elemento_0 = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Elemento_0", Codice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        Else
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Indice = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Indice", Indice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        End If


        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If NumeroDb(myPOSTreader.Item("Anno")) = AnnoDal And NumeroDb(myPOSTreader.Item("Mese")) = MeseDal Then
                If NumeroDb(myPOSTreader.Item("Anno")) = AnnoAl And NumeroDb(myPOSTreader.Item("Mese")) = MeseAl Then
                    If AnnoDal = AnnoAl And MeseDal = MeseAl Then
                        For g = GiornoDal To GiornoAl
                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00" Then
                                Mod_Somma_GiustificativiNelPeriodo = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelPeriodo, "+", Sostituisci(StringaDb(myPOSTreader.Item("Elemento_" & g)), ".", ","))
                            End If
                        Next g
                    Else
                        For g = 1 To GiornoAl
                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00" Then
                                Mod_Somma_GiustificativiNelPeriodo = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelPeriodo, "+", Sostituisci(StringaDb(myPOSTreader.Item("Elemento_" & g)), ".", ","))
                            End If
                        Next g
                    End If
                Else
                    For g = GiornoDal To 31
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00" Then
                            Mod_Somma_GiustificativiNelPeriodo = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelPeriodo, "+", Sostituisci(StringaDb(myPOSTreader.Item("Elemento_" & g)), ".", ","))
                        End If
                    Next g
                End If
            Else
                If NumeroDb(myPOSTreader.Item("Anno")) = AnnoAl And NumeroDb(myPOSTreader.Item("Mese")) = MeseAl Then
                    For g = 1 To GiornoAl
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00" Then
                            Mod_Somma_GiustificativiNelPeriodo = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelPeriodo, "+", Sostituisci(StringaDb(myPOSTreader.Item("Elemento_" & g)), ".", ","))
                        End If
                    Next g
                Else
                    For g = 1 To 31
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00" Then
                            Mod_Somma_GiustificativiNelPeriodo = Mod_OperaConMinuti(Mod_Somma_GiustificativiNelPeriodo, "+", Sostituisci(StringaDb(myPOSTreader.Item("Elemento_" & g)), ".", ","))
                        End If
                    Next g
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Conta_GiustificativiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim Indice As Integer
        Dim AnnoDal As Integer
        Dim MeseDal As Byte
        Dim GiornoDal As Byte
        Dim AnnoAl As Integer
        Dim MeseAl As Byte
        Dim GiornoAl As Byte


        Mod_Conta_GiustificativiNelPeriodo = 0
        If Not IsDate(DataDal) Then Exit Function
        If Not IsDate(DataAl) Then Exit Function
        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then Exit Function

        Indice = 0
        Select Case Codice
            Case "Ore Work_1"
                Indice = 41
            Case "Ore Work_2"
                Indice = 42
            Case "Ore Previste"
                Indice = 43
            Case "Ore Pausa"
                Indice = 44
            Case "Ore Lavorate"
                Indice = 45
            Case "Ore Diurne"
                Indice = 46
            Case "Ore Notturne"
                Indice = 47
            Case "Ore Festive Diurne"
                Indice = 48
            Case "Ore Festive Notturne"
                Indice = 49
            Case "Ore Retribuite"
                Indice = 50
        End Select

        AnnoDal = Year(DataDal)
        MeseDal = Month(DataDal)
        GiornoDal = Day(DataDal)
        AnnoAl = Year(DataAl)
        MeseAl = Month(DataAl)
        GiornoAl = Day(DataAl)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        If Indice = 0 Then
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Elemento_0 = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Elemento_0", Codice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        Else
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Indice = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Indice", Indice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If NumeroDb(myPOSTreader.Item("Anno")) = AnnoDal And NumeroDb(myPOSTreader.Item("Mese")) = MeseDal Then
                For g = GiornoDal To 31
                    If (Indice = 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "") Or (Indice <> 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00") Then
                        Mod_Conta_GiustificativiNelPeriodo = Mod_Conta_GiustificativiNelPeriodo + 1
                    End If
                Next g
            Else
                If NumeroDb(myPOSTreader.Item("Anno")) = AnnoAl And NumeroDb(myPOSTreader.Item("Mese")) = MeseAl Then
                    For g = 1 To GiornoAl
                        If (Indice = 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "") Or (Indice <> 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00") Then
                            Mod_Conta_GiustificativiNelPeriodo = Mod_Conta_GiustificativiNelPeriodo + 1
                        End If
                    Next g
                Else
                    For g = 1 To 31
                        If (Indice = 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "") Or (Indice <> 0 And StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "00.00") Then
                            Mod_Conta_GiustificativiNelPeriodo = Mod_Conta_GiustificativiNelPeriodo + 1
                        End If
                    Next g
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Conta_GiustificativiContiguiNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal CodiceAlternativo As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim Indice As Integer
        Dim AnnoDal As Integer
        Dim MeseDal As Byte
        Dim GiornoDal As Byte
        Dim AnnoAl As Integer
        Dim MeseAl As Byte
        Dim GiornoAl As Byte
        Dim Sw_MeseAl As Boolean = False
        Dim g As Integer

        Mod_Conta_GiustificativiContiguiNelPeriodo = 0
        If Not IsDate(DataDal) Then Exit Function
        If Not IsDate(DataAl) Then Exit Function
        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then Exit Function

        Indice = 0
        Select Case Codice
            Case "Ore Work_1"
                Indice = 41
            Case "Ore Work_2"
                Indice = 42
            Case "Ore Previste"
                Indice = 43
            Case "Ore Pausa"
                Indice = 44
            Case "Ore Lavorate"
                Indice = 45
            Case "Ore Diurne"
                Indice = 46
            Case "Ore Notturne"
                Indice = 47
            Case "Ore Festive Diurne"
                Indice = 48
            Case "Ore Festive Notturne"
                Indice = 49
            Case "Ore Retribuite"
                Indice = 50
        End Select

        AnnoDal = Year(DataDal)
        MeseDal = Month(DataDal)
        GiornoDal = Day(DataDal)
        AnnoAl = Year(DataAl)
        MeseAl = Month(DataAl)
        GiornoAl = Day(DataAl)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        If Indice = 0 Then
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Elemento_0 = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Elemento_0", Codice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        Else
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Indice = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Indice", Indice)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)

            cmd.Parameters.AddWithValue("@Anno", AnnoAl)
            cmd.Parameters.AddWithValue("@Mese", MeseAl)
            cmd.Parameters.AddWithValue("@Anno", AnnoDal)
            cmd.Parameters.AddWithValue("@Mese", MeseDal)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If NumeroDb(myPOSTreader.Item("Anno")) = AnnoAl And NumeroDb(myPOSTreader.Item("Mese")) = MeseAl Then
                Sw_MeseAl = True
                For g = GiornoAl To 1 Step -1
                    If Indice = 0 Then
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "" Then
                            If CodiceAlternativo = "" Then
                                Exit Do
                            Else
                                If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                            End If
                        End If

                    Else

                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "00.00" Then
                            If CodiceAlternativo = "" Then
                                Exit Do
                            Else
                                If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                            End If
                        End If
                    End If
                    Mod_Conta_GiustificativiContiguiNelPeriodo = Mod_Conta_GiustificativiContiguiNelPeriodo + 1
                Next g

            Else

                If Sw_MeseAl = False Then Exit Do
                If NumeroDb(myPOSTreader.Item("Anno")) = AnnoDal And NumeroDb(myPOSTreader.Item("Mese")) = MeseDal Then
                    For g = GiorniMese(MeseDal, AnnoDal) To GiornoDal Step -1
                        If Indice = 0 Then
                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "" Then
                                If CodiceAlternativo = "" Then
                                    Exit Do
                                Else
                                    If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                                End If
                            End If

                        Else

                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "00.00" Then
                                If CodiceAlternativo = "" Then
                                    Exit Do
                                Else
                                    If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                                End If
                            End If
                        End If
                        Mod_Conta_GiustificativiContiguiNelPeriodo = Mod_Conta_GiustificativiContiguiNelPeriodo + 1
                    Next g

                Else

                    For g = GiorniMese(MeseDal, AnnoDal) To 1 Step -1
                        If Indice = 0 Then
                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "" Then
                                If CodiceAlternativo = "" Then
                                    Exit Do
                                Else
                                    If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                                End If
                            End If

                        Else

                            If StringaDb(myPOSTreader.Item("Elemento_" & g)) = "00.00" Then
                                If CodiceAlternativo = "" Then
                                    Exit Do
                                Else
                                    If Mod_Leggi_GiustificativoSalvaCalcoloOrari(Dipendente, Familiare, CodiceAlternativo, NumeroDb(myPOSTreader.Item("Anno")), NumeroDb(myPOSTreader.Item("Mese")), g) = 0 Then Exit Do
                                End If
                            End If
                        End If
                        Mod_Conta_GiustificativiContiguiNelPeriodo = Mod_Conta_GiustificativiContiguiNelPeriodo + 1
                    Next g
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Leggi_GiustificativoSalvaCalcoloOrari(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Codice As Object, ByVal Anno As Object, ByVal Mese As Object, ByVal Giorno As Object) As Object
        Dim Indice As Integer

        Mod_Leggi_GiustificativoSalvaCalcoloOrari = 0

        Indice = 0
        Select Case Codice
            Case "Ore Work_1"
                Indice = 41
            Case "Ore Work_2"
                Indice = 42
            Case "Ore Previste"
                Indice = 43
            Case "Ore Pausa"
                Indice = 44
            Case "Ore Lavorate"
                Indice = 45
            Case "Ore Diurne"
                Indice = 46
            Case "Ore Notturne"
                Indice = 47
            Case "Ore Festive Diurne"
                Indice = 48
            Case "Ore Festive Notturne"
                Indice = 49
            Case "Ore Retribuite"
                Indice = 50
        End Select

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        If Indice = 0 Then
            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Elemento_0 = ?" & _
                              " AND Anno = ? AND Mese = ?"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
            cmd.Parameters.AddWithValue("@Elemento_0", Codice)
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
        Else

            cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And Indice = ?" & _
                              " AND Anno = ? AND Mese = ?"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@Indice", Indice)
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
        End If

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            If Indice = 0 Then
                If StringaDb(myPOSTreader.Item("Elemento_" & Giorno)) <> "" Then
                    Mod_Leggi_GiustificativoSalvaCalcoloOrari = 1
                End If
            Else
                If StringaDb(myPOSTreader.Item("Elemento_" & Giorno)) = "00.00" Then
                    Mod_Leggi_GiustificativoSalvaCalcoloOrari = 1
                End If
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Conta_TipoGiustificativoNelPeriodo(ByVal Dipendente As Object, ByVal Familiare As Object, ByVal Tipo As Object, ByVal DataDal As Object, ByVal DataAl As Object) As Object
        Dim AnnoDal As Integer
        Dim MeseDal As Byte
        Dim GiornoDal As Byte
        Dim AnnoAl As Integer
        Dim MeseAl As Byte
        Dim GiornoAl As Byte


        Mod_Conta_TipoGiustificativoNelPeriodo = 0
        If Not IsDate(DataDal) Then Exit Function
        If Not IsDate(DataAl) Then Exit Function
        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then Exit Function

        AnnoDal = Year(DataDal)
        MeseDal = Month(DataDal)
        GiornoDal = Day(DataDal)
        AnnoAl = Year(DataAl)
        MeseAl = Month(DataAl)
        GiornoAl = Day(DataAl)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari INNER JOIN Giustificativi ON SalvaCalcoloOrari.Elemento_0 = Giustificativi.Codice" & _
                              " WHERE CodiceDipendente = ?" & _
                              " And CodiceFamiliare = ?" & _
                              " And Tipo = ?" & _
                              " AND ((Anno > ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno < ?)" & _
                              " OR (Anno = ? AND Mese >= ? AND Anno = ? AND Mese <= ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno > ?)" & _
                              " OR (Anno = ? AND Mese <= ? AND Anno = ? AND Mese >= ?))" & _
                              " ORDER BY Anno, Mese"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", Familiare)
        cmd.Parameters.AddWithValue("@Elemento_0", Tipo)
        cmd.Parameters.AddWithValue("@Anno", AnnoDal)
        cmd.Parameters.AddWithValue("@Anno", AnnoAl)

        cmd.Parameters.AddWithValue("@Anno", AnnoDal)
        cmd.Parameters.AddWithValue("@Mese", MeseDal)
        cmd.Parameters.AddWithValue("@Anno", AnnoAl)

        cmd.Parameters.AddWithValue("@Anno", AnnoDal)
        cmd.Parameters.AddWithValue("@Mese", MeseDal)
        cmd.Parameters.AddWithValue("@Anno", AnnoAl)
        cmd.Parameters.AddWithValue("@Mese", MeseAl)

        cmd.Parameters.AddWithValue("@Anno", AnnoAl)
        cmd.Parameters.AddWithValue("@Mese", MeseAl)
        cmd.Parameters.AddWithValue("@Anno", AnnoDal)

        cmd.Parameters.AddWithValue("@Anno", AnnoAl)
        cmd.Parameters.AddWithValue("@Mese", MeseAl)
        cmd.Parameters.AddWithValue("@Anno", AnnoDal)
        cmd.Parameters.AddWithValue("@Mese", MeseDal)


        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If NumeroDb(myPOSTreader.Item("Anno")) = AnnoDal And NumeroDb(myPOSTreader.Item("Mese")) = MeseDal Then
                For g = GiornoDal To 31
                    If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" Then
                        Mod_Conta_TipoGiustificativoNelPeriodo = Mod_Conta_TipoGiustificativoNelPeriodo + 1
                    End If
                Next g
            Else
                If NumeroDb(myPOSTreader.Item("Anno")) = AnnoAl And NumeroDb(myPOSTreader.Item("Mese")) = MeseAl Then
                    For g = 1 To GiornoAl
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" Then
                            Mod_Conta_TipoGiustificativoNelPeriodo = Mod_Conta_TipoGiustificativoNelPeriodo + 1
                        End If
                    Next g
                Else
                    For g = 1 To 31
                        If StringaDb(myPOSTreader.Item("Elemento_" & g)) <> "" Then
                            Mod_Conta_TipoGiustificativoNelPeriodo = Mod_Conta_TipoGiustificativoNelPeriodo + 1
                        End If
                    Next g
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_Somma_DatiVariabili(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object, ByRef Tempo As Boolean) As Object
        Dim k As New Cls_TipiDatiAnagrafici
        Dim p As New Cls_ParametriTurni
        Dim Inizio As Date
        Dim c As Integer
        Dim OldDipendente As Long

        Mod_Somma_DatiVariabili = 0
        If Struttura = "" Then Exit Function
        c = Len(Struttura)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        p.Leggi(ConnectionString)
        k.Leggi(ConnectionString, Codice)

        If k.Tipo <> 200 And k.Tipo <> 100 Then Exit Function

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice FROM ParagrafoDiStruttura WHERE Codice >= ? ORDER BY Codice"
        cmd.Parameters.AddWithValue("@Codice", Struttura)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Left(StringaDb(myPOSTreader.Item("Codice")), c) <> Struttura Then Exit Do
            OldDipendente = 0
            cmd.CommandText = "SELECT * FROM DatiVariabili" & _
                                " WHERE ContenutoTesto = ?" & _
                                " AND CodiceVariabile = ?" & _
                                " AND Validita <= ?" & _
                                " ORDER BY CodiceDipendente, Validita"
            cmd.Parameters.AddWithValue("@Codice", Struttura)
            cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoPiantaOrganica)
            cmd.Parameters.AddWithValue("@Validita", Data)

            cmd.Connection = cn
            Dim myPOSTreader_1 As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader_1.Read
                If OldDipendente <> 0 And OldDipendente <> NumeroDb(myPOSTreader_1.Item("CodiceDipendente")) Then
                    If Struttura = Mod_Leggi_DatiVariabili(OldDipendente, p.CodiceAnagraficoPiantaOrganica, "<=", Data) Then
                        If k.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
                        If k.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
                        If k.Scadenza = "G" Then Inizio = Data
                        If k.Scadenza = "N" Then
                            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                                                " WHERE CodiceDipendente = ?" & _
                                                " AND CodiceVariabile = ?" & _
                                                " AND Validita " & Modo & " ?" & _
                                                " ORDER BY Validita DESC, Id"
                            cmd.Parameters.AddWithValue("@CodiceDipendente", OldDipendente)
                            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
                            cmd.Parameters.AddWithValue("@Validita", Data)
                        Else
                            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                                                " WHERE CodiceDipendente = ?" & _
                                                " AND CodiceVariabile = ?" & _
                                                " AND Validita >= ?" & _
                                                " AND Validita " & Modo & " ?" & _
                                                " ORDER BY Validita DESC, Id"
                            cmd.Parameters.AddWithValue("@CodiceDipendente", OldDipendente)
                            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
                            cmd.Parameters.AddWithValue("@Validita", Inizio)
                            cmd.Parameters.AddWithValue("@Validita", Data)
                        End If

                        cmd.Connection = cn
                        Dim myPOSTreader_2 As OleDbDataReader = cmd.ExecuteReader()
                        If myPOSTreader_2.Read Then
                            If Tempo = True Then
                                Mod_Somma_DatiVariabili = Mod_OperaConMinuti(Mod_Somma_DatiVariabili, "+", NumeroDb(myPOSTreader_2.Item("ContenutoNumero")))
                            Else
                                Mod_Somma_DatiVariabili = Mod_Somma_DatiVariabili + NumeroDb(myPOSTreader_2.Item("ContenutoNumero"))
                            End If
                        End If
                        myPOSTreader_2.Close()
                    End If
                End If
                OldDipendente = NumeroDb(myPOSTreader_1.Item("CodiceDipendente"))
            Loop
            myPOSTreader_1.Close()
            If OldDipendente <> 0 Then
                If Struttura = Mod_Leggi_DatiVariabili(OldDipendente, p.CodiceAnagraficoPiantaOrganica, "<=", Data) Then
                    If k.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
                    If k.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
                    If k.Scadenza = "G" Then Inizio = Data
                    If k.Scadenza = "N" Then
                        cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                                            " WHERE CodiceDipendente = ?" & _
                                            " AND CodiceVariabile = ?" & _
                                            " AND Validita " & Modo & " ?" & _
                                            " ORDER BY Validita DESC, Id"
                        cmd.Parameters.AddWithValue("@CodiceDipendente", OldDipendente)
                        cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
                        cmd.Parameters.AddWithValue("@Validita", Data)
                    Else
                        cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                                            " WHERE CodiceDipendente = ?" & _
                                            " AND CodiceVariabile = ?" & _
                                            " AND Validita >= ?" & _
                                            " AND Validita " & Modo & " ?" & _
                                            " ORDER BY Validita DESC, Id"
                        cmd.Parameters.AddWithValue("@CodiceDipendente", OldDipendente)
                        cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
                        cmd.Parameters.AddWithValue("@Validita", Inizio)
                        cmd.Parameters.AddWithValue("@Validita", Data)
                    End If

                    cmd.Connection = cn
                    Dim myPOSTreader_2 As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader_2.Read Then
                        If Tempo = True Then
                            Mod_Somma_DatiVariabili = Mod_OperaConMinuti(Mod_Somma_DatiVariabili, "+", NumeroDb(myPOSTreader_2.Item("ContenutoNumero")))
                        Else
                            Mod_Somma_DatiVariabili = Mod_Somma_DatiVariabili + NumeroDb(myPOSTreader_2.Item("ContenutoNumero"))
                        End If
                    End If
                    myPOSTreader_2.Close()
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_LeggiProvenienza_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Dim k As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date

        Mod_LeggiProvenienza_DatiVariabili = ""

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        k.Leggi(ConnectionString, Codice)

        Dim cmd As New OleDbCommand()

        If k.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If k.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If k.Scadenza = "G" Then Inizio = Data
        If k.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_LeggiProvenienza_DatiVariabili = StringaDb(myPOSTreader.Item("Provenienza"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_LeggiValidita_DatiVariabili(ByVal Dipendente As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Dim k As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date

        Mod_LeggiValidita_DatiVariabili = DateSerial(1899, 12, 30)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        k.Leggi(ConnectionString, Codice)

        Dim cmd As New OleDbCommand()

        If k.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If k.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If k.Scadenza = "G" Then Inizio = Data
        If k.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabili" & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_LeggiValidita_DatiVariabili = DataDb(myPOSTreader.Item("Validita"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Mod_LeggiValidita_DatiVariabiliStruttura(ByVal Struttura As Object, ByVal Codice As Object, ByVal Modo As Object, ByVal Data As Object) As Object
        Dim k As New Cls_TipiDatiAnagrafici
        Dim Inizio As Date

        Mod_LeggiValidita_DatiVariabiliStruttura = DateSerial(1899, 12, 30)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        k.Leggi(ConnectionString, Codice)

        Dim cmd As New OleDbCommand()

        If k.Scadenza = "A" Then Inizio = DateSerial(Year(Data), 1, 1)
        If k.Scadenza = "M" Then Inizio = DateSerial(Year(Data), Month(Data), 1)
        If k.Scadenza = "G" Then Inizio = Data
        If k.Scadenza = "N" Then
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                              " WHERE CodiceStruttura = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceStruttura", Struttura)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Data)
        Else
            cmd.CommandText = "SELECT TOP 1 * FROM DatiVariabiliStruttura" & _
                              " WHERE CodiceStruttura = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita >= ?" & _
                              " AND Validita " & Modo & " ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@CodiceStruttura", Struttura)
            cmd.Parameters.AddWithValue("@CodiceVariabile", Codice)
            cmd.Parameters.AddWithValue("@Validita", Inizio)
            cmd.Parameters.AddWithValue("@Validita", Data)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mod_LeggiValidita_DatiVariabiliStruttura = DataDb(myPOSTreader.Item("Validita"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function Calcolo_TempoContrattuale(ByVal Data As Object, ByVal Contratto As Object) As Object

        Dim Orari(7) As String
        Dim n As Byte
        Dim Minuti As Integer
        Dim Ore As Byte
        Dim Valore As Single
        Dim TipoPartime As String = ""
        Dim PercentualePartimeOre As Single = 0

        Calcolo_TempoContrattuale = 0
        If Not IsDate(Data) Then Exit Function

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM CondizioniContrattuali" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", Contratto)
        cmd.Parameters.AddWithValue("@Validita", Data)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Orari(1) = Format(DataDb(myPOSTreader.Item("OrarioDomenica")), "HH.mm")
            Orari(2) = Format(DataDb(myPOSTreader.Item("OrarioLunedi")), "HH.mm")
            Orari(3) = Format(DataDb(myPOSTreader.Item("OrarioMartedi")), "HH.mm")
            Orari(4) = Format(DataDb(myPOSTreader.Item("OrarioMercoledi")), "HH.mm")
            Orari(5) = Format(DataDb(myPOSTreader.Item("OrarioGiovedi")), "HH.mm")
            Orari(6) = Format(DataDb(myPOSTreader.Item("OrarioVenerdi")), "HH.mm")
            Orari(7) = Format(DataDb(myPOSTreader.Item("OrarioSabato")), "HH.mm")
            TipoPartime = StringaDb(myPOSTreader.Item("TipoPartime"))
            PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
        End If
        myPOSTreader.Close()
        cn.Close()

        n = 0
        Minuti = 0
        For i = 1 To 7
            If Orari(i) <> "00.00" Then
                n = n + 1
                Minuti = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Val(Mid(Orari(i), 1, 2)), Val(Mid(Orari(i), 4, 2)), 0)) + Minuti
            End If
        Next i
        If n > 0 Then
            Minuti = Math.Round(Minuti / n)
        End If
        If TipoPartime = "PAO" Then
            Minuti = Math.Round(Minuti * PercentualePartimeOre)
        End If
        Ore = Int(Math.Round(Minuti / 60, 2))
        Minuti = Minuti - Math.Round(Ore * 60, 0)
        Calcolo_TempoContrattuale = Math.Round(Ore + Minuti / 100, 2)
    End Function

    Public Function Calcolo_TempoStandard(ByVal xDataDal As Date, ByVal xDataAl As Date, ByVal xCodiceSuperGruppo As String, ByVal xContratto As String) As Single
        Dim ceo As New Cls_ElaboraOrari

        Dim ProfiloOrario As String
        Dim TipoPartime As String = ""
        Dim PercentualePartimeOre As Single
        Dim Vettore_Orari As New Cls_Struttura_Orari
        Dim GiornoDal As Byte
        Dim GiornoAl As Byte
        Dim Orari(7) As Date
        Dim Validita As Date
        Dim Orari_M(31) As Date
        Dim CC_Validita(31) As Date
        Dim FestivitaInfrasettimanali(31) As String
        Dim W_Mese As Byte
        Dim OreMinuti As Single
        Dim DataTest As Date
        Dim Ore As Integer
        Dim Minuti As Integer

        Dim g As Byte
        Dim a As String

        Dim cf As New Cls_Festivita
        Dim cg As New Cls_Giustificativi

        GiornoDal = 0
        GiornoAl = 0

        Calcolo_TempoStandard = 0
        If Not IsDate(xDataDal) Then Exit Function
        If Not IsDate(xDataAl) Then Exit Function
        If Format(xDataDal, "yyyyMMdd") > Format(xDataAl, "yyyyMMdd") Then Exit Function
        If xContratto = "" Then Exit Function

        cf.Leggi(ConnectionString, Year(xDataDal))
        If cf.Id = 0 Then Exit Function

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM CondizioniContrattuali" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@Codice", xContratto)
        cmd.Parameters.AddWithValue("@Validita", xDataAl)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") <= Format(xDataDal, "yyyyMMdd") Then
                For i = 1 To 31
                    CC_Validita(i) = DataDb(myPOSTreader.Item("Validita"))
                    FestivitaInfrasettimanali(i) = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
                Next i
            Else
                g = DateDiff("d", xDataDal, DataDb(myPOSTreader.Item("Validita"))) + 2
                For i = g To 31
                    CC_Validita(i) = DataDb(myPOSTreader.Item("Validita"))
                    FestivitaInfrasettimanali(i) = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
                Next i
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim W_CC_Validita As Date = DateSerial(1899, 12, 30)
        For i = 1 To 31
            If W_CC_Validita <> CC_Validita(i) Then
                W_CC_Validita = CC_Validita(i)
                cmd.CommandText = "SELECT * FROM CondizioniContrattuali" & _
                                  " WHERE Codice = ?" & _
                                  " AND Validita = ?"
                cmd.Parameters.AddWithValue("@Codice", xContratto)
                cmd.Parameters.AddWithValue("@Validita", W_CC_Validita)

                cmd.Connection = cn
                myPOSTreader = cmd.ExecuteReader()
                If Not myPOSTreader.Read Then
                    Exit Function
                Else
                    Orari(1) = DataDb(myPOSTreader.Item("OrarioDomenica"))
                    Orari(2) = DataDb(myPOSTreader.Item("OrarioLunedi"))
                    Orari(3) = DataDb(myPOSTreader.Item("OrarioMartedi"))
                    Orari(4) = DataDb(myPOSTreader.Item("OrarioMercoledi"))
                    Orari(5) = DataDb(myPOSTreader.Item("OrarioGiovedi"))
                    Orari(6) = DataDb(myPOSTreader.Item("OrarioVenerdi"))
                    Orari(7) = DataDb(myPOSTreader.Item("OrarioSabato"))
                    Validita = DataDb(myPOSTreader.Item("Validita"))
                    ProfiloOrario = StringaDb(myPOSTreader.Item("CodiceProfiloOrario"))
                    TipoPartime = StringaDb(myPOSTreader.Item("TipoPartime"))
                    PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
                End If
                myPOSTreader.Close()
                cmd.Parameters.Clear()

                If ProfiloOrario <> "" Then
                    a = ceo.SviluppoProfiliOrari(ConnectionString, xCodiceSuperGruppo, xContratto, ProfiloOrario, xDataDal, Vettore_Orari)

                    For ii = i To 31
                        OreMinuti = 0
                        For r = 0 To 4
                            If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(r, ii - 1), "Tipo") = "P" Then
                                OreMinuti = Mod_OperaConMinuti(OreMinuti, "+", Calcolo_OreMinuti(Vettore_Orari.Dalle(r, ii - 1), Vettore_Orari.Alle(r, ii - 1)))
                            End If
                        Next r
                        Ore = Int(OreMinuti)
                        Minuti = (OreMinuti - Ore) * 100
                        Orari_M(ii) = Format(Ore, "00") & "." & Format(Minuti, "00")
                    Next ii
                    Exit For

                Else

                    DataTest = DateAdd("d", i - 1, xDataDal)
                    For ii = i To 31
                        Orari_M(ii) = Orari(Weekday(DataTest))
                        DataTest = DateAdd("d", 1, DataTest)
                    Next ii

                    DataTest = DateAdd("d", i - 1, xDataDal)
                    W_Mese = Month(DataTest)

                    Dim ccpm As New Cls_CondizioniPartimeMese

                    ccpm.Leggi(ConnectionString, xContratto, W_CC_Validita, W_Mese)
                    If ccpm.Id > 0 Then
                        GiornoDal = ccpm.GiornoDal
                        GiornoAl = ccpm.GiornoAl
                        Orari(1) = ccpm.OrarioDomenica
                        Orari(2) = ccpm.OrarioLunedi
                        Orari(3) = ccpm.OrarioMartedi
                        Orari(4) = ccpm.OrarioMercoledi
                        Orari(5) = ccpm.OrarioGiovedi
                        Orari(6) = ccpm.OrarioVenerdi
                        Orari(7) = ccpm.OrarioSabato

                        If GiornoDal = 0 Then GiornoDal = 1
                        If GiornoAl = 0 Then GiornoAl = 31

                        For ii = i To 31
                            If W_Mese = Month(DataTest) Then
                                If ii >= GiornoDal And ii <= GiornoAl Then
                                    Orari_M(ii) = Orari(Weekday(DataTest))
                                    DataTest = DateAdd("d", 1, DataTest)
                                End If
                            End If
                        Next ii
                    End If
                End If
            End If
        Next i

        Minuti = 0
        Dim DataXDif As Date = DateSerial(1899, 12, 30)
        DataTest = xDataDal
        For i = 1 To 31
            If Format(DataTest, "yyyyMMdd") > Format(xDataAl, "yyyyMMdd") Then Exit For
            If FestivitaInfrasettimanali(i) = "N" Or (FestivitaInfrasettimanali(i) = "S" And GiornoFestivo(DataTest, xCodiceSuperGruppo) = False) Then
                Minuti = Minuti + DateDiff("n", DataXDif, Orari_M(i))
                '      Minuti = DateDiff("n", DataXDif, Orari_M(i))
                '      Ore = Int(math.round(Minuti / 60, 2))
                '      Minuti = Minuti - math.round(Ore * 60, 0)
                '      Valore = math.round(Ore + Minuti / 100, 2)
                '      Calcolo_TempoStandard = Mod_OperaConMinuti(Calcolo_TempoStandard, "+", Valore)
            End If
            DataTest = DateAdd("d", 1, DataTest)
        Next i

        If TipoPartime = "PAO" Then
            Minuti = Math.Round(Minuti * PercentualePartimeOre)
        End If
        Ore = Int(Math.Round(Minuti / 60, 2))
        Minuti = Minuti - Math.Round(Ore * 60, 0)
        Calcolo_TempoStandard = Math.Round(Ore + Minuti / 100, 2)
    End Function

    Public Function Calcolo_GiorniStandard(ByVal xDataDal As Date, ByVal xDataAl As Date, ByVal xCodiceSuperGruppo As String, ByVal xContratto As String) As Integer
        Dim ceo As New Cls_ElaboraOrari

        Dim ProfiloOrario As String
        Dim TipoPartime As String = ""
        Dim PercentualePartimeOre As Single
        Dim Vettore_Orari As New Cls_Struttura_Orari
        Dim GiornoDal As Byte
        Dim GiornoAl As Byte
        Dim Orari(7) As Date
        Dim Validita As Date
        Dim Orari_M(31) As Date
        Dim CC_Validita(31) As Date
        Dim FestivitaInfrasettimanali(31) As String
        Dim W_Mese As Byte
        Dim OreMinuti As Single
        Dim DataTest As Date
        Dim Ore As Integer
        Dim Minuti As Integer

        Dim g As Byte
        Dim a As String

        Dim cf As New Cls_Festivita
        Dim cg As New Cls_Giustificativi

        GiornoDal = 0
        GiornoAl = 0

        Calcolo_GiorniStandard = 0
        If Not IsDate(xDataDal) Then Exit Function
        If Not IsDate(xDataAl) Then Exit Function
        If Format(xDataDal, "yyyyMMdd") > Format(xDataAl, "yyyyMMdd") Then Exit Function
        If xContratto = "" Then Exit Function

        cf.Leggi(ConnectionString, Year(xDataDal))
        If cf.Id = 0 Then Exit Function

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM CondizioniContrattuali" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@Codice", xContratto)
        cmd.Parameters.AddWithValue("@Validita", xDataAl)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") <= Format(xDataDal, "yyyyMMdd") Then
                For i = 1 To 31
                    CC_Validita(i) = DataDb(myPOSTreader.Item("Validita"))
                    FestivitaInfrasettimanali(i) = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
                Next i
            Else
                g = DateDiff("d", xDataDal, DataDb(myPOSTreader.Item("Validita"))) + 2
                For i = g To 31
                    CC_Validita(i) = DataDb(myPOSTreader.Item("Validita"))
                    FestivitaInfrasettimanali(i) = StringaDb(myPOSTreader.Item("FestivitaInfrasettimanali"))
                Next i
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim W_CC_Validita As Date = DateSerial(1899, 12, 30)
        For i = 1 To 31
            If W_CC_Validita <> CC_Validita(i) Then
                W_CC_Validita = CC_Validita(i)
                cmd.CommandText = "SELECT * FROM CondizioniContrattuali" & _
                                  " WHERE Codice = ?" & _
                                  " AND Validita = ?"
                cmd.Parameters.AddWithValue("@Codice", xContratto)
                cmd.Parameters.AddWithValue("@Validita", W_CC_Validita)

                cmd.Connection = cn
                myPOSTreader = cmd.ExecuteReader()
                If Not myPOSTreader.Read Then
                    Exit Function
                Else
                    Orari(1) = DataDb(myPOSTreader.Item("OrarioDomenica"))
                    Orari(2) = DataDb(myPOSTreader.Item("OrarioLunedi"))
                    Orari(3) = DataDb(myPOSTreader.Item("OrarioMartedi"))
                    Orari(4) = DataDb(myPOSTreader.Item("OrarioMercoledi"))
                    Orari(5) = DataDb(myPOSTreader.Item("OrarioGiovedi"))
                    Orari(6) = DataDb(myPOSTreader.Item("OrarioVenerdi"))
                    Orari(7) = DataDb(myPOSTreader.Item("OrarioSabato"))
                    Validita = DataDb(myPOSTreader.Item("Validita"))
                    ProfiloOrario = StringaDb(myPOSTreader.Item("CodiceProfiloOrario"))
                    TipoPartime = StringaDb(myPOSTreader.Item("TipoPartime"))
                    PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
                End If
                myPOSTreader.Close()
                cmd.Parameters.Clear()

                If ProfiloOrario <> "" Then
                    a = ceo.SviluppoProfiliOrari(ConnectionString, xCodiceSuperGruppo, xContratto, ProfiloOrario, xDataDal, Vettore_Orari)

                    For ii = i To 31
                        OreMinuti = 0
                        For r = 0 To 4
                            If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(r, ii - 1), "Tipo") = "P" Then
                                OreMinuti = Mod_OperaConMinuti(OreMinuti, "+", Calcolo_OreMinuti(Vettore_Orari.Dalle(r, ii - 1), Vettore_Orari.Alle(r, ii - 1)))
                            End If
                        Next r
                        Ore = Int(OreMinuti)
                        Minuti = (OreMinuti - Ore) * 100
                        Orari_M(ii) = Format(Ore, "00") & "." & Format(Minuti, "00")
                    Next ii
                    Exit For

                Else

                    DataTest = DateAdd("d", i - 1, xDataDal)
                    For ii = i To 31
                        Orari_M(ii) = Orari(Weekday(DataTest))
                        DataTest = DateAdd("d", 1, DataTest)
                    Next ii

                    DataTest = DateAdd("d", i - 1, xDataDal)
                    W_Mese = Month(DataTest)

                    Dim ccpm As New Cls_CondizioniPartimeMese

                    ccpm.Leggi(ConnectionString, xContratto, W_CC_Validita, W_Mese)
                    If ccpm.Id > 0 Then
                        GiornoDal = ccpm.GiornoDal
                        GiornoAl = ccpm.GiornoAl
                        Orari(1) = ccpm.OrarioDomenica
                        Orari(2) = ccpm.OrarioLunedi
                        Orari(3) = ccpm.OrarioMartedi
                        Orari(4) = ccpm.OrarioMercoledi
                        Orari(5) = ccpm.OrarioGiovedi
                        Orari(6) = ccpm.OrarioVenerdi
                        Orari(7) = ccpm.OrarioSabato

                        If GiornoDal = 0 Then GiornoDal = 1
                        If GiornoAl = 0 Then GiornoAl = 31

                        For ii = i To 31
                            If W_Mese = Month(DataTest) Then
                                If ii >= GiornoDal And ii <= GiornoAl Then
                                    Orari_M(ii) = Orari(Weekday(DataTest))
                                    DataTest = DateAdd("d", 1, DataTest)
                                End If
                            End If
                        Next ii
                    End If
                End If
            End If
        Next i

        Minuti = 0
        Dim DataXDif As Date = DateSerial(1899, 12, 30)
        DataTest = xDataDal
        For i = 1 To 31
            If Format(DataTest, "yyyyMMdd") > Format(xDataAl, "yyyyMMdd") Then Exit For
            If FestivitaInfrasettimanali(i) = "N" Or (FestivitaInfrasettimanali(i) = "S" And GiornoFestivo(DataTest, xCodiceSuperGruppo) = False) Then
                Minuti = Minuti + DateDiff("n", DataXDif, Orari_M(i))
                If Minuti > 0 Then Calcolo_GiorniStandard = Calcolo_GiorniStandard + 1
            End If
            DataTest = DateAdd("d", 1, DataTest)
        Next i
    End Function

    Private Function Calcolo_OreMinuti(ByVal Dalle As String, ByVal Alle As String) As Single
        Dim Piu1 As Byte
        Dim Ore As Integer
        Dim Minuti As Integer

        Calcolo_OreMinuti = 0
        Piu1 = 0
        If Dalle < "00.00" Or Dalle > "23.59" Then Exit Function
        If Alle < "00.00" Or Alle > "23.59" Then Exit Function
        If Alle = "00.00" Then
            Alle = "23.59"
            Piu1 = 1
        End If

        Minuti = DateDiff("n", TimeSerial(Val(Mid(Dalle, 1, 2)), Val(Mid(Dalle, 4, 2)), 0), TimeSerial(Val(Mid(Alle, 1, 2)), Val(Mid(Alle, 4, 2)), 0)) + Piu1
        Ore = Int(Math.Round(Minuti / 60, 2))
        Minuti = Minuti - Math.Round(Ore * 60, 0)
        Calcolo_OreMinuti = Math.Round(Ore + Minuti / 100, 2)
    End Function

    '------------------------------------------------------------
    ' Indica se il giorno è una festività Nazionale per l'anno
    '------------------------------------------------------------
    Public Function GiornoFestivo(ByVal Data As Date, ByVal CodiceSuperGruppo As String) As Boolean
        Dim YYYY As Integer
        Dim mm As Byte
        Dim gg As Byte

        GiornoFestivo = False

        YYYY = Year(Data)
        mm = Month(Data)
        gg = Day(Data)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Festivita WHERE Anno = ?"
        cmd.Parameters.AddWithValue("@Anno", YYYY)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            myPOSTreader.Close()
            cmd.Parameters.Clear()
            cn.Close()
            Exit Function
        End If

        If mm = 1 And gg = 1 And StringaDb(myPOSTreader.Item("G0101")) <> "S" Then GiornoFestivo = True
        If mm = 1 And gg = 6 And StringaDb(myPOSTreader.Item("G0601")) <> "S" Then GiornoFestivo = True
        If mm = 3 And gg = 19 And StringaDb(myPOSTreader.Item("G1903")) <> "S" Then GiornoFestivo = True
        If mm = 4 And gg = 25 And StringaDb(myPOSTreader.Item("G2504")) <> "S" Then GiornoFestivo = True
        If mm = 5 And gg = 1 And StringaDb(myPOSTreader.Item("G0105")) <> "S" Then GiornoFestivo = True
        If mm = 6 And gg = 2 And StringaDb(myPOSTreader.Item("G0206")) <> "S" Then GiornoFestivo = True
        If mm = 6 And gg = 29 And StringaDb(myPOSTreader.Item("G2906")) <> "S" Then GiornoFestivo = True
        If mm = 8 And gg = 15 And StringaDb(myPOSTreader.Item("G1508")) <> "S" Then GiornoFestivo = True
        If mm = 11 And gg = 1 And StringaDb(myPOSTreader.Item("G0111")) <> "S" Then GiornoFestivo = True
        If mm = 11 And gg = 4 And StringaDb(myPOSTreader.Item("G0411")) <> "S" Then GiornoFestivo = True
        If mm = 12 And gg = 8 And StringaDb(myPOSTreader.Item("G0812")) <> "S" Then GiornoFestivo = True
        If mm = 12 And gg = 25 And StringaDb(myPOSTreader.Item("G2512")) <> "S" Then GiornoFestivo = True
        If mm = 12 And gg = 26 And StringaDb(myPOSTreader.Item("G2612")) <> "S" Then GiornoFestivo = True
        If mm = NumeroDb(myPOSTreader.Item("PasquaMM")) And gg = NumeroDb(myPOSTreader.Item("PasquaGG")) And StringaDb(myPOSTreader.Item("Pasqua")) <> "S" Then GiornoFestivo = True
        If mm = NumeroDb(myPOSTreader.Item("LunediPasquaMM")) And gg = NumeroDb(myPOSTreader.Item("LunediPasquaGG")) And StringaDb(myPOSTreader.Item("LunediPasqua")) <> "S" Then GiornoFestivo = True
        If mm = NumeroDb(myPOSTreader.Item("AscensioneMM")) And gg = NumeroDb(myPOSTreader.Item("AscensioneGG")) And StringaDb(myPOSTreader.Item("Ascensione")) <> "S" Then GiornoFestivo = True
        If mm = NumeroDb(myPOSTreader.Item("DominiMM")) And gg = NumeroDb(myPOSTreader.Item("DominiGG")) And StringaDb(myPOSTreader.Item("Domini")) <> "S" Then GiornoFestivo = True
        If GiornoFestivo = False Then
            If mm = NumeroDb(myPOSTreader.Item("PatronoMM")) And gg = NumeroDb(myPOSTreader.Item("PatronoGG")) And StringaDb(myPOSTreader.Item("Patrono")) <> "S" Then GiornoFestivo = True
            If CodiceSuperGruppo <> "" Then
                myPOSTreader.Close()
                cmd.Parameters.Clear()
                cmd.CommandText = "SELECT * FROM GruppiDipendenti WHERE Codice = ?"
                cmd.Parameters.AddWithValue("@Codice", CodiceSuperGruppo)
                If Not myPOSTreader.Read Then
                    If NumeroDb(myPOSTreader.Item("PatronoMM")) <> 0 And NumeroDb(myPOSTreader.Item("PatronoGG")) <> 0 Then
                        GiornoFestivo = False
                        If mm = NumeroDb(myPOSTreader.Item("PatronoMM")) And gg = NumeroDb(myPOSTreader.Item("PatronoGG")) And StringaDb(myPOSTreader.Item("Patrono")) <> "S" Then GiornoFestivo = True
                    End If
                End If
            End If
        End If
        myPOSTreader.Close()
        cmd.Parameters.Clear()
        cn.Close()
    End Function

    Public Function CampoCondizioniContrattuali(ByVal Codice As Object, ByVal Validita As Object, ByVal Campo As Object) As Object
        Dim k As New Cls_CondizioniContrattuali

        CampoCondizioniContrattuali = k.Campo(ConnectionString, Codice, Validita, Campo)
    End Function

    Public Function CampoCondizioniPartimeMese(ByVal Codice As String, ByVal Validita As Date, ByVal Campo As String) As Object
        Dim cc As New Cls_CondizioniContrattuali
        Dim cpm As New Cls_CondizioniPartimeMese
        Dim Valore As Object

        cc.Trova(ConnectionString, Codice, Validita)
        If cc.Id = 0 Then
            CampoCondizioniPartimeMese = ""
        Else
            If Campo = "PercentualePartimeGiorni" Then
                CampoCondizioniPartimeMese = cc.PercentualePartimeGiorniMese
            Else
                If Campo = "PercentualePartimeOre" Then
                    CampoCondizioniPartimeMese = cc.PercentualePartimeOreMese
                Else
                    CampoCondizioniPartimeMese = cc.Campo(ConnectionString, Codice, Validita, Campo)
                End If
            End If
            Valore = cpm.Campo(ConnectionString, Codice, Validita, Month(Validita), Campo)
            If cpm.Id <> 0 Then
                CampoCondizioniPartimeMese = Valore
            End If
        End If
    End Function

End Class
