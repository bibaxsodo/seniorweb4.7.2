Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaVariazioniProfiloComandanti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Tipo As String
    Public Riga As Short
    Public Dalle As Date
    Public Alle As Date
    Public Pausa As Date
    Public Giustificativo As String
    Public GiornoSuccessivo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Tipo = ""
        Riga = 0
        Dalle = Nothing
        Alle = Nothing
        Pausa = Nothing
        Giustificativo = ""
        GiornoSuccessivo = ""
    End Sub
End Class
