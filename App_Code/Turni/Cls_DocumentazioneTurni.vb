﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DocumentazioneTurni
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public NumeroRichiesta As Long
    Public NomeFile As String
    Public Data As Date
    Public Descrizione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        NumeroRichiesta = 0
        NomeFile = ""
        Data = Nothing
        Descrizione = ""
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Documentazione WHERE " & _
                          "NumeroRichiesta = ? AND NomeFile = ?"
        cmd.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
        cmd.Parameters.AddWithValue("@NomeFile", NomeFile)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            NumeroRichiesta = NumeroDb(myPOSTreader.Item("NumeroRichiesta"))
            NomeFile = StringaDb(myPOSTreader.Item("NomeFile"))
            Data = DataDb(myPOSTreader.Item("Data"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String, ByVal MyTable As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim I As Long


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdDelete As New OleDbCommand()

        cmdDelete.CommandText = ("DELETE FROM Documentazione WHERE " & _
                   "NumeroRichiesta = ? ")
        cmdDelete.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
        cmdDelete.Connection = cn
        cmdDelete.ExecuteNonQuery()


        For I = 0 To MyTable.Rows.Count - 1
            If Trim(MyTable.Rows(I).Item(0)) <> "" Then
                Dim cmd As New OleDbCommand()

                cmd.CommandText = ("INSERT INTO Documentazione (Utente, DataAggiornamento, NumeroRichiesta, NomeFile, Descrizione, Data)" & _
                                   " VALUES (?,?,?,?,?,?)")
                cmd.Parameters.AddWithValue("@Utente", Utente)
                cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmd.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
                cmd.Parameters.AddWithValue("@NomeFile", MyTable.Rows(I).Item(0))
                cmd.Parameters.AddWithValue("@Descrizione", MyTable.Rows(I).Item(1))
                Dim DataDoc As Date = MyTable.Rows(I).Item(2)
                cmd.Parameters.AddWithValue("@Data", DataDoc)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
            End If
        Next

        cn.Close()
    End Sub
End Class
