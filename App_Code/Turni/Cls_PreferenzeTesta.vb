Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_PreferenzeTesta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Validita As Date

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Validita = Nothing
    End Sub
End Class
