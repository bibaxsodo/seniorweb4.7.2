﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Orario
    Public GgSett As Byte
    Public Richiesti As String
    Public Variati As String
    Public Modificati As String
    Public TipoFlessibilita As String      ' Impostato da Orario Flessibile (P = Timbratura Presenza ; F = Orario Flessibile ; X = Orario Flessibile con flessibilità dell'intervallo)
    Public FasceOrarie As String
    Public CodiceOrario As String
    Public Dalle As String
    Public Alle As String
    Public DalleAlto As String
    Public AlleAlto As String
    Public DalleBasso As String
    Public AlleBasso As String
    Public DalleObbligo As String
    Public AlleObbligo As String
    Public Pausa As String
    Public Giustificativo As String
    Public NelGruppo As String
    Public Familiare As Byte
    Public Tipo As String
    Public TipoServizio As String
    Public GiornoSuccessivo As String
    Public PU As String

    Public Sub Pulisci()
        GgSett = 0
        Richiesti = ""
        Variati = ""
        Modificati = ""
        TipoFlessibilita = ""
        FasceOrarie = ""
        CodiceOrario = ""
        Dalle = ""
        Alle = ""
        DalleAlto = ""
        AlleAlto = ""
        DalleBasso = ""
        AlleBasso = ""
        DalleObbligo = ""
        AlleObbligo = ""
        Pausa = ""
        Giustificativo = ""
        NelGruppo = ""
        Familiare = 0
        Tipo = ""
        TipoServizio = ""
        GiornoSuccessivo = ""
        PU = ""
    End Sub
End Class
