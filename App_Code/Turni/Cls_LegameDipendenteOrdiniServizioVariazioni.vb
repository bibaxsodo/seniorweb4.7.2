Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_LegameDipendenteOrdiniServizioVariazioni
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public GruppoComandante As String
    Public TipoComandante As String
    Public Riga As Integer
    Public CodiceDipendente As Long
    Public CodiceFamiliare As Byte
    Public Comandante As String
    Public Commento As String
    Public Qualifica As String
    Public Rosso As String
    Public Verde As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        GruppoComandante = ""
        TipoComandante = ""
        Riga = 0
        CodiceDipendente = 0
        CodiceFamiliare = 0
        Comandante = ""
        Commento = ""
        Qualifica = ""
        Rosso = ""
        Verde = ""
    End Sub
End Class
