Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Apparati
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Gruppo As String
    Public Disponibile As String

    Public Sub Pulisci()
        REM modifica apparati modifica sauro

        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Gruppo = ""
        Disponibile = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Apparati WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Apparati WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Apparati SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?, " & _
                    " Gruppo  = ? " & _
                    " Disponibile  = ? " & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Disponibile", Disponibile)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Apparati (Utente, DataAggiornamento, Codice, Descrizione, Gruppo, Disponibile) VALUES (?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Disponibile", Disponibile)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        
        cmd.CommandText = ("Select * From Apparati Order By Descrizione")        

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Gruppo", GetType(String))
        Tabella.Columns.Add("Disponibile", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Gruppo")
            myriga(3) = myPOSTreader.Item("Disponibile")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Apparati WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            Disponibile = StringaDb(myPOSTreader.Item("Disponibile"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Count(*) FROM OrdiniServizioRigaApparati" & _
                          " WHERE Apparato = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM Apparati ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Function Esiste_Codice(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Apparati" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste_Descrizione(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Apparati" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If StringaDb(myPOSTreader.Item("Codice")) = xCodice Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
