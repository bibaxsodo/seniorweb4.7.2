﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Timbrature
    Public Dalle(9, 31) As String
    Public Dalle_Causale(9, 31) As String
    Public Dalle_Colore(9, 31) As Long
    Public Alle(9, 31) As String
    Public Alle_Causale(9, 31) As String
    Public Alle_Colore(9, 31) As Long

    Public Sub Pulisci()
        For g = 0 To 31
            For i = 0 To 9
                Dalle(i, g) = ""
                Dalle_Causale(i, g) = ""
                Dalle_Colore(i, g) = RGB(255, 255, 255)
                Alle(i, g) = ""
                Alle_Causale(i, g) = ""
                Alle_Colore(i, g) = RGB(255, 255, 255)
            Next i
        Next g
    End Sub
End Class

