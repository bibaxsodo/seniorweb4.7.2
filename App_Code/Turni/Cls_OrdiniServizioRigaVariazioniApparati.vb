Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaVariazioniApparati
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public GruppoComandante As String
    Public TipoComandante As String
    Public Riga As Short
    Public Apparato As String
    Public Comandante As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        GruppoComandante = ""
        TipoComandante = ""
        Riga = 0
        Apparato = ""
        Comandante = ""
    End Sub
End Class
