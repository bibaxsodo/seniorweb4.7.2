Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CondizioniPartimeMese
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Mese As Byte
    Public OrarioLunedi As Date
    Public OrarioMartedi As Date
    Public OrarioMercoledi As Date
    Public OrarioGiovedi As Date
    Public OrarioVenerdi As Date
    Public OrarioSabato As Date
    Public OrarioDomenica As Date
    Public PartimeLunedi As String
    Public PartimeMartedi As String
    Public PartimeMercoledi As String
    Public PartimeGiovedi As String
    Public PartimeVenerdi As String
    Public PartimeSabato As String
    Public PartimeDomenica As String
    Public PercentualePartime As Single
    Public PercentualePartimeGiorni As Single
    Public PercentualePartimeOre As Single
    Public GiornoDal As Byte
    Public GiornoAl As Byte

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Mese = 0
        OrarioLunedi = Nothing
        OrarioMartedi = Nothing
        OrarioMercoledi = Nothing
        OrarioGiovedi = Nothing
        OrarioVenerdi = Nothing
        OrarioSabato = Nothing
        OrarioDomenica = Nothing
        PartimeLunedi = ""
        PartimeMartedi = ""
        PartimeMercoledi = ""
        PartimeGiovedi = ""
        PartimeVenerdi = ""
        PartimeSabato = ""
        PartimeDomenica = ""
        PercentualePartime = 0
        PercentualePartimeGiorni = 0
        PercentualePartimeOre = 0
        GiornoDal = 0
        GiornoAl = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xMese As Byte)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM CondizioniPartimeMese WHERE Codice = ? AND Validita = ? AND Mese = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Mese", xMese)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM CondizioniPartimeMese WHERE Codice = ? AND Validita = ? AND Mese = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE CondizioniPartimeMese SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " OrarioLunedi = ?," & _
                    " OrarioMartedi = ?," & _
                    " OrarioMercoledi = ?," & _
                    " OrarioGiovedi = ?," & _
                    " OrarioVenerdi = ?," & _
                    " OrarioSabato = ?," & _
                    " OrarioDomenica = ?," & _
                    " PartimeLunedi = ?," & _
                    " PartimeMartedi = ?," & _
                    " PartimeMercoledi = ?," & _
                    " PartimeGiovedi = ?," & _
                    " PartimeVenerdi = ?," & _
                    " PartimeSabato = ?," & _
                    " PartimeDomenica = ?," & _
                    " PercentualePartime = ?," & _
                    " PercentualePartimeGiorni = ?," & _
                    " PercentualePartimeOre = ?," & _
                    " GiornoDal = ?," & _
                    " GiornoAl = ?" & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?" & _
                    " AND Mese = ?"


            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@OrarioLunedi", OrarioLunedi)
            cmdw.Parameters.AddWithValue("@OrarioMartedi", OrarioMartedi)
            cmdw.Parameters.AddWithValue("@OrarioMercoledi", OrarioMercoledi)
            cmdw.Parameters.AddWithValue("@OrarioGiovedi", OrarioGiovedi)
            cmdw.Parameters.AddWithValue("@OrarioVenerdi", OrarioVenerdi)
            cmdw.Parameters.AddWithValue("@OrarioSabato", OrarioSabato)
            cmdw.Parameters.AddWithValue("@OrarioDomenica", OrarioDomenica)
            cmdw.Parameters.AddWithValue("@PartimeLunedi", PartimeLunedi)
            cmdw.Parameters.AddWithValue("@PartimeMartedi", PartimeMartedi)
            cmdw.Parameters.AddWithValue("@PartimeMercoledi", PartimeMercoledi)
            cmdw.Parameters.AddWithValue("@PartimeGiovedi", PartimeGiovedi)
            cmdw.Parameters.AddWithValue("@PartimeVenerdi", PartimeVenerdi)
            cmdw.Parameters.AddWithValue("@PartimeSabato", PartimeSabato)
            cmdw.Parameters.AddWithValue("@PartimeDomenica", PartimeDomenica)
            cmdw.Parameters.AddWithValue("@PercentualePartime", PercentualePartime)
            cmdw.Parameters.AddWithValue("@PercentualePartimeGiorni", PercentualePartimeGiorni)
            cmdw.Parameters.AddWithValue("@PercentualePartimeOre", PercentualePartimeOre)
            cmdw.Parameters.AddWithValue("@GiornoDal", GiornoDal)
            cmdw.Parameters.AddWithValue("@GiornoAl", GiornoAl)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Mese", Mese)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO CondizioniPartimeMese (Utente, DataAggiornamento, Codice, Validita, Mese, OrarioLunedi, OrarioMartedi, OrarioMercoledi, OrarioGiovedi, OrarioVenerdi, OrarioSabato, OrarioDomenica, PartimeLunedi, PartimeMartedi, PartimeMercoledi, PartimeGiovedi, PartimeVenerdi, PartimeSabato, PartimeDomenica, PercentualePartime, PercentualePartimeGiorni, PercentualePartimeOre, GiornoDal, GiornoAl) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Mese", Mese)
            cmdw.Parameters.AddWithValue("@OrarioLunedi", IIf(Year(OrarioLunedi) > 1, OrarioLunedi, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioMartedi", IIf(Year(OrarioMartedi) > 1, OrarioMartedi, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioMercoledi", IIf(Year(OrarioMercoledi) > 1, OrarioMercoledi, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioGiovedi", IIf(Year(OrarioGiovedi) > 1, OrarioGiovedi, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioVenerdi", IIf(Year(OrarioVenerdi) > 1, OrarioVenerdi, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioSabato", IIf(Year(OrarioSabato) > 1, OrarioSabato, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@OrarioDomenica", IIf(Year(OrarioDomenica) > 1, OrarioDomenica, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@PartimeLunedi", PartimeLunedi)
            cmdw.Parameters.AddWithValue("@PartimeMartedi", PartimeMartedi)
            cmdw.Parameters.AddWithValue("@PartimeMercoledi", PartimeMercoledi)
            cmdw.Parameters.AddWithValue("@PartimeGiovedi", PartimeGiovedi)
            cmdw.Parameters.AddWithValue("@PartimeVenerdi", PartimeVenerdi)
            cmdw.Parameters.AddWithValue("@PartimeSabato", PartimeSabato)
            cmdw.Parameters.AddWithValue("@PartimeDomenica", PartimeDomenica)
            cmdw.Parameters.AddWithValue("@PercentualePartime", PercentualePartime)
            cmdw.Parameters.AddWithValue("@PercentualePartimeGiorni", PercentualePartimeGiorni)
            cmdw.Parameters.AddWithValue("@PercentualePartimeOre", PercentualePartimeOre)
            cmdw.Parameters.AddWithValue("@GiornoDal", GiornoDal)
            cmdw.Parameters.AddWithValue("@GiornoAl", GiornoAl)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("SELECT * FROM CondizioniPartimeMese WHERE Codice = ? ORDER BY Mese, Validita")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Mese", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("OrarioLunedi", GetType(String))
        Tabella.Columns.Add("OrarioMartedi", GetType(String))
        Tabella.Columns.Add("OrarioMercoledi", GetType(String))
        Tabella.Columns.Add("OrarioGiovedi", GetType(String))
        Tabella.Columns.Add("OrarioVenerdi", GetType(String))
        Tabella.Columns.Add("OrarioSabato", GetType(String))
        Tabella.Columns.Add("OrarioDomenica", GetType(String))
        Tabella.Columns.Add("PartimeLunedi", GetType(String))
        Tabella.Columns.Add("PartimeMartedi", GetType(String))
        Tabella.Columns.Add("PartimeMercoledi", GetType(String))
        Tabella.Columns.Add("PartimeGiovedi", GetType(String))
        Tabella.Columns.Add("PartimeVenerdi", GetType(String))
        Tabella.Columns.Add("PartimeSabato", GetType(String))
        Tabella.Columns.Add("PartimeDomenica", GetType(String))
        Tabella.Columns.Add("PercentualePartime", GetType(String))
        Tabella.Columns.Add("PercentualePartimeGiorni", GetType(String))
        Tabella.Columns.Add("PercentualePartimeOre", GetType(String))
        Tabella.Columns.Add("GiornoDal", GetType(String))
        Tabella.Columns.Add("GiornoAl", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Mese")
            myriga(1) = Format(myPOSTreader.Item("Validita"), "dd/MM/yyyy")
            myriga(2) = Format(myPOSTreader.Item("OrarioLunedi"), "HH.mm")
            myriga(3) = Format(myPOSTreader.Item("OrarioMartedi"), "HH.mm")
            myriga(4) = Format(myPOSTreader.Item("OrarioMercoledi"), "HH.mm")
            myriga(5) = Format(myPOSTreader.Item("OrarioGiovedi"), "HH.mm")
            myriga(6) = Format(myPOSTreader.Item("OrarioVenerdi"), "HH.mm")
            myriga(7) = Format(myPOSTreader.Item("OrarioSabato"), "HH.mm")
            myriga(8) = Format(myPOSTreader.Item("OrarioDomenica"), "HH.mm")
            myriga(9) = myPOSTreader.Item("PartimeLunedi")
            myriga(10) = myPOSTreader.Item("PartimeMartedi")
            myriga(11) = myPOSTreader.Item("PartimeMercoledi")
            myriga(12) = myPOSTreader.Item("PartimeGiovedi")
            myriga(13) = myPOSTreader.Item("PartimeVenerdi")
            myriga(14) = myPOSTreader.Item("PartimeSabato")
            myriga(15) = myPOSTreader.Item("PartimeDomenica")
            myriga(16) = Format(myPOSTreader.Item("PercentualePartime") * 100, "%%0.000")
            myriga(17) = Format(myPOSTreader.Item("PercentualePartimeGiorni") * 100, "%%0.000")
            myriga(18) = Format(myPOSTreader.Item("PercentualePartimeOre") * 100, "%%0.000")
            myriga(19) = myPOSTreader.Item("GiornoDal")
            myriga(20) = myPOSTreader.Item("GiornoAl")
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xMese As Byte)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From CondizioniPartimeMese" & _
                           " WHERE Codice = ?" & _
                           " AND Validita = ?" & _
                           " AND Mese = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Mese", xMese)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = NumeroDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Mese = NumeroDb(myPOSTreader.Item("Mese"))
            OrarioLunedi = DataDb(myPOSTreader.Item("OrarioLunedi"))
            OrarioMartedi = DataDb(myPOSTreader.Item("OrarioMartedi"))
            OrarioMercoledi = DataDb(myPOSTreader.Item("OrarioMercoledi"))
            OrarioGiovedi = DataDb(myPOSTreader.Item("OrarioGiovedi"))
            OrarioVenerdi = DataDb(myPOSTreader.Item("OrarioVenerdi"))
            OrarioSabato = DataDb(myPOSTreader.Item("OrarioSabato"))
            OrarioDomenica = DataDb(myPOSTreader.Item("OrarioDomenica"))
            PartimeLunedi = StringaDb(myPOSTreader.Item("PartimeLunedi"))
            PartimeMartedi = StringaDb(myPOSTreader.Item("PartimeMartedi"))
            PartimeMercoledi = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
            PartimeGiovedi = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
            PartimeVenerdi = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
            PartimeSabato = StringaDb(myPOSTreader.Item("PartimeSabato"))
            PartimeDomenica = StringaDb(myPOSTreader.Item("PartimeDomenica"))
            PercentualePartime = NumeroDb(myPOSTreader.Item("PercentualePartime"))
            PercentualePartimeGiorni = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
            PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
            GiornoDal = NumeroDb(myPOSTreader.Item("GiornoDal"))
            GiornoAl = NumeroDb(myPOSTreader.Item("GiornoAl"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Trova(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xMese As Byte)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT TOP 1 * FROM CondizioniPartimeMese " & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " AND Mese = ?" & _
                          " ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Mese", xMese)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = NumeroDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Mese = NumeroDb(myPOSTreader.Item("Mese"))
            OrarioLunedi = DataDb(myPOSTreader.Item("OrarioLunedi"))
            OrarioMartedi = DataDb(myPOSTreader.Item("OrarioMartedi"))
            OrarioMercoledi = DataDb(myPOSTreader.Item("OrarioMercoledi"))
            OrarioGiovedi = DataDb(myPOSTreader.Item("OrarioGiovedi"))
            OrarioVenerdi = DataDb(myPOSTreader.Item("OrarioVenerdi"))
            OrarioSabato = DataDb(myPOSTreader.Item("OrarioSabato"))
            OrarioDomenica = DataDb(myPOSTreader.Item("OrarioDomenica"))
            PartimeLunedi = StringaDb(myPOSTreader.Item("PartimeLunedi"))
            PartimeMartedi = StringaDb(myPOSTreader.Item("PartimeMartedi"))
            PartimeMercoledi = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
            PartimeGiovedi = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
            PartimeVenerdi = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
            PartimeSabato = StringaDb(myPOSTreader.Item("PartimeSabato"))
            PartimeDomenica = StringaDb(myPOSTreader.Item("PartimeDomenica"))
            PercentualePartime = NumeroDb(myPOSTreader.Item("PercentualePartime"))
            PercentualePartimeGiorni = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
            PercentualePartimeOre = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
            GiornoDal = NumeroDb(myPOSTreader.Item("GiornoDal"))
            GiornoAl = NumeroDb(myPOSTreader.Item("GiornoAl"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function Campo(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xMese As Byte, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From CondizioniPartimeMese" & _
                           " WHERE Codice = ?" & _
                           " AND Validita = ?" & _
                           " AND Mese = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Mese", xMese)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Select Case xCampo
                Case "Codice"
                    Campo = NumeroDb(myPOSTreader.Item("Codice"))
                Case "Validita"
                    Campo = DataDb(myPOSTreader.Item("Validita"))
                Case "Mese"
                    Campo = NumeroDb(myPOSTreader.Item("Mese"))
                Case "OrarioLunedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioLunedi"))
                Case "OrarioMartedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioMartedi"))
                Case "OrarioMercoledi"
                    Campo = DataDb(myPOSTreader.Item("OrarioMercoledi"))
                Case "OrarioGiovedi"
                    Campo = DataDb(myPOSTreader.Item("OrarioGiovedi"))
                Case "OrarioVenerdi"
                    Campo = DataDb(myPOSTreader.Item("OrarioVenerdi"))
                Case "OrarioSabato"
                    Campo = DataDb(myPOSTreader.Item("OrarioSabato"))
                Case "OrarioDomenica"
                    Campo = DataDb(myPOSTreader.Item("OrarioDomenica"))
                Case "PartimeLunedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeLunedi"))
                Case "PartimeMartedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeMartedi"))
                Case "PartimeMercoledi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeMercoledi"))
                Case "PartimeGiovedi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeGiovedi"))
                Case "PartimeVenerdi"
                    Campo = StringaDb(myPOSTreader.Item("PartimeVenerdi"))
                Case "PartimeSabato"
                    Campo = StringaDb(myPOSTreader.Item("PartimeSabato"))
                Case "PartimeDomenica"
                    Campo = StringaDb(myPOSTreader.Item("PartimeDomenica"))
                Case "PercentualePartime"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartime"))
                Case "PercentualePartimeGiorni"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeGiorni"))
                Case "PercentualePartimeOre"
                    Campo = NumeroDb(myPOSTreader.Item("PercentualePartimeOre"))
                Case "GiornoDal"
                    Campo = NumeroDb(myPOSTreader.Item("GiornoDal"))
                Case "GiornoAl"
                    Campo = NumeroDb(myPOSTreader.Item("GiornoAl"))
                Case Else
                    Campo = ""
            End Select
        Else
            Campo = ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
