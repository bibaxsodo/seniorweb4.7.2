Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrarioMensilePatrono
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Anno As Short
    Public Codice As String
    Public Gruppo As String
    Public Mese As Byte
    Public TempoLavoro As Single
    Public GiorniLavorativi As Byte
    Public MeseStandard As Byte
    Public TempoLavoroStandard As Single
    Public GiorniLavorativiStandard As Byte

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Anno = 0
        Codice = ""
        Gruppo = ""
        Mese = 0
        TempoLavoro = 0
        GiorniLavorativi = 0
        MeseStandard = 0
        TempoLavoroStandard = 0
        GiorniLavorativiStandard = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String, ByVal xGruppo As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM OrarioMensilePatrono" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?" & _
                          " AND Gruppo = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensilePatrono" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?" & _
                          " AND Gruppo = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Gruppo", Gruppo)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE OrarioMensilePatrono SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Mese = ?," & _
                    " TempoLavoro = ?," & _
                    " GiorniLavorativi = ?," & _
                    " MeseStandard = ?," & _
                    " TempoLavoroStandard = ?," & _
                    " GiorniLavorativiStandard = ?," & _
                    " WHERE Anno = ?" & _
                    " AND Codice = ?" & _
                    " AND Gruppo = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Mese", Mese)
            cmdw.Parameters.AddWithValue("@TempoLavoro", TempoLavoro)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi", GiorniLavorativi)
            cmdw.Parameters.AddWithValue("@MeseStandard", MeseStandard)
            cmdw.Parameters.AddWithValue("@TempoLavoroStandard", TempoLavoroStandard)
            cmdw.Parameters.AddWithValue("@GiorniLavorativiStandard", GiorniLavorativiStandard)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO OrarioMensilePatrono (Utente, DataAggiornamento, Anno, Codice, Gruppo, Mese, TempoLavoro, GiorniLavorativi, MeseStandard, TempoLavoroStandard, GiorniLavorativiStandard) VALUES (?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Mese", Mese)
            cmdw.Parameters.AddWithValue("@TempoLavoro", TempoLavoro)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi", GiorniLavorativi)
            cmdw.Parameters.AddWithValue("@MeseStandard", MeseStandard)
            cmdw.Parameters.AddWithValue("@TempoLavoroStandard", TempoLavoroStandard)
            cmdw.Parameters.AddWithValue("@GiorniLavorativiStandard", GiorniLavorativiStandard)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From OrarioMensilePatrono Order By Anno, Codice, Gruppo")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Gruppo", GetType(String))
        Tabella.Columns.Add("Mese", GetType(String))
        Tabella.Columns.Add("Tempo Lavoro", GetType(String))
        Tabella.Columns.Add("Giorni Lavorativi", GetType(String))
        Tabella.Columns.Add("Mese Standard", GetType(String))
        Tabella.Columns.Add("Tempo Lavoro Standard", GetType(String))
        Tabella.Columns.Add("Giorni Lavorativi Standard", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Anno")
            myriga(1) = myPOSTreader.Item("Codice")
            myriga(2) = myPOSTreader.Item("Gruppo")
            myriga(3) = myPOSTreader.Item("Mese")
            myriga(4) = myPOSTreader.Item("TempoLavoro")
            myriga(5) = myPOSTreader.Item("GiorniLavorativi")
            myriga(6) = myPOSTreader.Item("MeseStandard")
            myriga(7) = myPOSTreader.Item("TempoLavoroStandard")
            myriga(8) = myPOSTreader.Item("GiorniLavorativiStandard")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String, ByVal xGruppo As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensilePatrono" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?" & _
                          " AND Gruppo = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Anno = NumeroDb(myPOSTreader.Item("Anno"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            Mese = NumeroDb(myPOSTreader.Item("Mese"))
            TempoLavoro = NumeroDb(myPOSTreader.Item("TempoLavoro"))
            GiorniLavorativi = NumeroDb(myPOSTreader.Item("GiorniLavorativi"))
            MeseStandard = NumeroDb(myPOSTreader.Item("MeseStandard"))
            TempoLavoroStandard = NumeroDb(myPOSTreader.Item("TempoLavoroStandard"))
            GiorniLavorativiStandard = NumeroDb(myPOSTreader.Item("GiorniLavorativiStandard"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xAnno As String, ByVal xCodice As String, ByVal xGruppo As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensilePatrono" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?" & _
                          " AND Gruppo = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
