Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_SalvacalcoloOrari
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Anno As Short
    Public Mese As Byte
    Public CodiceDipendente As Integer
    Public CodiceFamiliare As Byte
    Public Indice As Byte
    Public Elemento_0 As String
    Public Elemento_1 As String
    Public Elemento_2 As String
    Public Elemento_3 As String
    Public Elemento_4 As String
    Public Elemento_5 As String
    Public Elemento_6 As String
    Public Elemento_7 As String
    Public Elemento_8 As String
    Public Elemento_9 As String
    Public Elemento_10 As String
    Public Elemento_11 As String
    Public Elemento_12 As String
    Public Elemento_13 As String
    Public Elemento_14 As String
    Public Elemento_15 As String
    Public Elemento_16 As String
    Public Elemento_17 As String
    Public Elemento_18 As String
    Public Elemento_19 As String
    Public Elemento_20 As String
    Public Elemento_21 As String
    Public Elemento_22 As String
    Public Elemento_23 As String
    Public Elemento_24 As String
    Public Elemento_25 As String
    Public Elemento_26 As String
    Public Elemento_27 As String
    Public Elemento_28 As String
    Public Elemento_29 As String
    Public Elemento_30 As String
    Public Elemento_31 As String
    Public TempoStandard As Single
    Public TempoMese As Single
    Public FlagCollegato As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Anno = 0
        Mese = 0
        CodiceDipendente = 0
        CodiceFamiliare = 0
        Indice = 0
        Elemento_0 = ""
        Elemento_1 = ""
        Elemento_2 = ""
        Elemento_3 = ""
        Elemento_4 = ""
        Elemento_5 = ""
        Elemento_6 = ""
        Elemento_7 = ""
        Elemento_8 = ""
        Elemento_9 = ""
        Elemento_10 = ""
        Elemento_11 = ""
        Elemento_12 = ""
        Elemento_13 = ""
        Elemento_14 = ""
        Elemento_15 = ""
        Elemento_16 = ""
        Elemento_17 = ""
        Elemento_18 = ""
        Elemento_19 = ""
        Elemento_20 = ""
        Elemento_21 = ""
        Elemento_22 = ""
        Elemento_23 = ""
        Elemento_24 = ""
        Elemento_25 = ""
        Elemento_26 = ""
        Elemento_27 = ""
        Elemento_28 = ""
        Elemento_29 = ""
        Elemento_30 = ""
        Elemento_31 = ""
        TempoStandard = 0
        TempoMese = 0
        FlagCollegato = ""
    End Sub
End Class
