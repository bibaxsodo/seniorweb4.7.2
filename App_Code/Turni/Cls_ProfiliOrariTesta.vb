Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProfiliOrariTesta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Descrizione As String
    Public OrarioGiornoFestivo As String
    Public OrarioGiornoFestivoInfrasettimanale As String
    Public CodiceStruttura As String
    Public PrimoElemento As Short

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Descrizione = ""
        OrarioGiornoFestivo = ""
        OrarioGiornoFestivoInfrasettimanale = ""
        CodiceStruttura = ""
        PrimoElemento = 0
    End Sub

    Sub Trova(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select TOP 1 * FROM ProfiliOrariTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id DESC"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            OrarioGiornoFestivo = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
            OrarioGiornoFestivoInfrasettimanale = StringaDb(myPOSTreader.Item("OrarioGiornoFestivoInfrasettimanale"))
            CodiceStruttura = StringaDb(myPOSTreader.Item("CodiceStruttura"))
            PrimoElemento = NumeroDb(myPOSTreader.Item("PrimoElemento"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

End Class
