Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RichiesteAssenza
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public NumeroRichiesta As Long
    Public CodiceDipendente As String
    Public DataDal As Date
    Public DataAl As Date
    Public Dalle As Date
    Public Alle As Date
    Public Giustificativo As String
    Public Comunicazioni As String
    Public Autorizzato1 As String
    Public UtenteAutorizza1 As String
    Public DataAutorizza1 As Date
    Public Autorizzato2 As String
    Public UtenteAutorizza2 As String
    Public DataAutorizza2 As Date
    Public Autorizzato3 As String
    Public UtenteAutorizza3 As String
    Public DataAutorizza3 As Date
    Public Revocato As String
    Public UtenteRevocazione As String
    Public DataRevocazione As Date
    Public Acquisito As String
    Public UtenteAcquisizione As String
    Public DataAcquisizione As Date
    Public Annullato As String
    Public UtenteAnnullamento As String
    Public DataAnnullamento As Date

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        NumeroRichiesta = 0
        CodiceDipendente = ""
        DataDal = Nothing
        DataAl = Nothing
        Dalle = Nothing
        Alle = Nothing
        Giustificativo = ""
        Comunicazioni = ""
        Autorizzato1 = ""
        UtenteAutorizza1 = ""
        DataAutorizza1 = Nothing
        Autorizzato2 = ""
        UtenteAutorizza2 = ""
        DataAutorizza2 = Nothing
        Autorizzato3 = ""
        UtenteAutorizza3 = ""
        DataAutorizza3 = Nothing
        Revocato = ""
        UtenteRevocazione = ""
        DataRevocazione = Nothing
        Acquisito = ""
        UtenteAcquisizione = ""
        DataAcquisizione = Nothing
        Annullato = ""
        UtenteAnnullamento = ""
        DataAnnullamento = Nothing
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xNumeroRichiesta As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM RichiesteAssenza" & _
                          " WHERE NumeroRichiesta = ?"
        cmd.Parameters.AddWithValue("@NumeroRichiesta", xNumeroRichiesta)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * from RichiesteAssenza" & _
                          " WHERE NumeroRichiesta = ?"
        cmd.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE RichiesteAssenza SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento = ?," & _
                               " CodiceDipendente = ?," & _
                               " DataDal = ?," & _
                               " DataAl = ?," & _
                               " Dalle  = ?," & _
                               " Alle = ?," & _
                               " Giustificativo  = ?," & _
                               " Comunicazioni  = ?," & _
                               " Autorizzato1  = ?," & _
                               " UtenteAutorizza1  = ?," & _
                               " DataAutorizza1  = ?," & _
                               " Autorizzato2  = ?," & _
                               " UtenteAutorizza2  = ?," & _
                               " DataAutorizza2  = ?," & _
                               " Autorizzato3 = ?," & _
                               " UtenteAutorizza3  = ?," & _
                               " DataAutorizza3  = ?," & _
                               " Revocato  = ?," & _
                               " UtenteRevocazione = ?," & _
                               " DataRevocazione  = ?" & _
                               " Acquisito  = ?," & _
                               " UtenteAcquisizione = ?," & _
                               " DataAcquisizione  = ?," & _
                               " Annullato  = ?," & _
                               " UtenteAnnullamento = ?," & _
                               " DataAnnullamento  = ?" & _
                               " WHERE NumeroRichiesta = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", DataAggiornamento)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@DataDal", DataDal)
            cmdw.Parameters.AddWithValue("@DataAl", IIf(Year(DataAl) > 1, DataAl, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Dalle", Dalle)
            cmdw.Parameters.AddWithValue("@Alle", Alle)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@Comunicazioni", Comunicazioni)
            cmdw.Parameters.AddWithValue("@Autorizzato1", Autorizzato1)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza1", UtenteAutorizza1)
            cmdw.Parameters.AddWithValue("@DataAutorizza1", IIf(Year(DataAutorizza1) > 1, DataAutorizza1, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Autorizzato2", Autorizzato2)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza2", UtenteAutorizza2)
            cmdw.Parameters.AddWithValue("@DataAutorizza2", IIf(Year(DataAutorizza2) > 1, DataAutorizza2, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Autorizzato3", Autorizzato3)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza3", UtenteAutorizza3)
            cmdw.Parameters.AddWithValue("@DataAutorizza3", IIf(Year(DataAutorizza3) > 1, DataAutorizza3, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Revocato", Revocato)
            cmdw.Parameters.AddWithValue("@UtenteRevocazione", UtenteRevocazione)
            cmdw.Parameters.AddWithValue("@DataRevocazione", IIf(Year(DataRevocazione) > 1, DataRevocazione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Acquisito", Acquisito)
            cmdw.Parameters.AddWithValue("@UtenteAcquisizione", UtenteAcquisizione)
            cmdw.Parameters.AddWithValue("@DataAcquisizione", IIf(Year(DataAcquisizione) > 1, DataAcquisizione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Annullato", Annullato)
            cmdw.Parameters.AddWithValue("@UtenteAnnullamento", UtenteAnnullamento)
            cmdw.Parameters.AddWithValue("@DataAnnullamento", IIf(Year(DataAnnullamento) > 1, DataAnnullamento, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()

        Else

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = "SELECT MAX(NumeroRichiesta) FROM RichiesteAssenza"
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                If Not IsDBNull(myPOSTreader1.Item(0)) Then
                    NumeroRichiesta = myPOSTreader1.Item(0) + 1
                Else
                    NumeroRichiesta = 1
                End If
            Else
                NumeroRichiesta = 1
            End If

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO RichiesteAssenza (Utente, DataAggiornamento, NumeroRichiesta, CodiceDipendente, DataDal, DataAl, Dalle, Alle, Giustificativo, Comunicazioni, Autorizzato1, UtenteAutorizza1, DataAutorizza1, Autorizzato2, UtenteAutorizza2, DataAutorizza2, Autorizzato3, UtenteAutorizza3, DataAutorizza3, Revocato, UtenteRevocazione, DataRevocazione, Acquisito, UtenteAcquisizione, DataAcquisizione, Annullato, UtenteAnnullamento, DataAnnullamento) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@NumeroRichiesta", NumeroRichiesta)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@DataDal", DataDal)
            cmdw.Parameters.AddWithValue("@DataAl", IIf(Year(DataAl) > 1, DataAl, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Dalle", Dalle)
            cmdw.Parameters.AddWithValue("@Alle", Alle)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@Comunicazioni", Comunicazioni)
            cmdw.Parameters.AddWithValue("@Autorizzato1", Autorizzato1)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza1", UtenteAutorizza1)
            cmdw.Parameters.AddWithValue("@DataAutorizza1", IIf(Year(DataAutorizza1) > 1, DataAutorizza1, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Autorizzato2", Autorizzato2)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza2", UtenteAutorizza2)
            cmdw.Parameters.AddWithValue("@DataAutorizza2", IIf(Year(DataAutorizza2) > 1, DataAutorizza2, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Autorizzato3", Autorizzato3)
            cmdw.Parameters.AddWithValue("@UtenteAutorizza3", UtenteAutorizza3)
            cmdw.Parameters.AddWithValue("@DataAutorizza3", IIf(Year(DataAutorizza3) > 1, DataAutorizza3, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Revocato", Revocato)
            cmdw.Parameters.AddWithValue("@UtenteRevocazione", UtenteRevocazione)
            cmdw.Parameters.AddWithValue("@DataRevocazione", IIf(Year(DataRevocazione) > 1, DataRevocazione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Acquisito", Acquisito)
            cmdw.Parameters.AddWithValue("@UtenteAcquisizione", UtenteAcquisizione)
            cmdw.Parameters.AddWithValue("@DataAcquisizione", IIf(Year(DataAcquisizione) > 1, DataAcquisizione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Annullato", Annullato)
            cmdw.Parameters.AddWithValue("@UtenteAnnullamento", UtenteAnnullamento)
            cmdw.Parameters.AddWithValue("@DataAnnullamento", IIf(Year(DataAnnullamento) > 1, DataAnnullamento, System.DBNull.Value))
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim d As New Cls_Qualifica

        Tabella.Clear()
        Tabella.Columns.Add("CodiceDipendente", GetType(String))
        Tabella.Columns.Add("Dal", GetType(String))
        Tabella.Columns.Add("Al", GetType(String))
        Tabella.Columns.Add("Dalle", GetType(String))
        Tabella.Columns.Add("Alle", GetType(String))
        Tabella.Columns.Add("Giustificativo", GetType(String))
        Tabella.Columns.Add("Autorizzato 1", GetType(String))
        Tabella.Columns.Add("Autorizzato 2", GetType(String))
        Tabella.Columns.Add("Autorizzato 3", GetType(String))
        Tabella.Columns.Add("Numero", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From RichiesteAssenza Order By CodiceDipendente, Validita")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = StringaDb(myPOSTreader.Item("CodiceDipendente"))
            myriga(1) = Format(DataDb(myPOSTreader.Item("Validita")), "dd/MM/yyyy")
            myriga(2) = StringaDb(myPOSTreader.Item("Qualifica1"))
            myriga(3) = StringaDb(myPOSTreader.Item("Qualifica2"))
            myriga(4) = StringaDb(myPOSTreader.Item("Qualifica3"))
            myriga(5) = StringaDb(myPOSTreader.Item("Qualifica4"))
            myriga(6) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica1")))
            myriga(7) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica2")))
            myriga(8) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica3")))
            myriga(9) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica4")))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xNumeroRichiesta As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * From RichiesteAssenza" & _
                          " WHERE NumeroRichiesta = ?"
        cmd.Parameters.AddWithValue("@NumeroRichiesta", xNumeroRichiesta)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Utente = StringaDb(myPOSTreader.Item("Utente"))
            DataAggiornamento = DataDb(myPOSTreader.Item("DataAggiornamento"))
            NumeroRichiesta = NumeroDb(myPOSTreader.Item("NumeroRichiesta"))
            CodiceDipendente = StringaDb(myPOSTreader.Item("CodiceDipendente"))
            DataDal = DataDb(myPOSTreader.Item("DataDal"))
            DataAl = DataDb(myPOSTreader.Item("DataAl"))
            Dalle = DataDb(myPOSTreader.Item("Dalle"))
            Alle = DataDb(myPOSTreader.Item("Alle"))
            Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            Comunicazioni = StringaDb(myPOSTreader.Item("Comunicazioni"))
            Autorizzato1 = StringaDb(myPOSTreader.Item("Autorizzato1"))
            UtenteAutorizza1 = StringaDb(myPOSTreader.Item("UtenteAutorizza1"))
            DataAutorizza1 = DataDb(myPOSTreader.Item("DataAutorizza1"))
            Autorizzato2 = StringaDb(myPOSTreader.Item("Autorizzato2"))
            UtenteAutorizza2 = StringaDb(myPOSTreader.Item("UtenteAutorizza2"))
            DataAutorizza2 = DataDb(myPOSTreader.Item("DataAutorizza2"))
            Autorizzato3 = StringaDb(myPOSTreader.Item("Autorizzato3"))
            UtenteAutorizza3 = StringaDb(myPOSTreader.Item("UtenteAutorizza3"))
            DataAutorizza3 = DataDb(myPOSTreader.Item("DataAutorizza3"))
            Revocato = StringaDb(myPOSTreader.Item("Revocato"))
            UtenteRevocazione = StringaDb(myPOSTreader.Item("UtenteRevocazione"))
            DataRevocazione = DataDb(myPOSTreader.Item("DataRevocazione"))
            Acquisito = StringaDb(myPOSTreader.Item("Acquisito"))
            UtenteAcquisizione = StringaDb(myPOSTreader.Item("UtenteAcquisizione"))
            DataAcquisizione = DataDb(myPOSTreader.Item("DataAcquisizione"))
            Annullato = StringaDb(myPOSTreader.Item("Annullato"))
            UtenteAnnullamento = StringaDb(myPOSTreader.Item("UtenteAnnullamento"))
            DataAnnullamento = DataDb(myPOSTreader.Item("DataAnnullamento"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
