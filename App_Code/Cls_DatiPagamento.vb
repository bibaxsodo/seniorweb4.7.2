﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DatiPagamento
    Public CodiceOspite As Long
    Public CodiceParente As Long
    Public Data As Date
    Public IntestatarioConto As String
    Public IntestatarioCFConto As String
    Public CodiceInt As String
    Public NumeroControllo As String
    Public Cin As String
    Public Abi As String
    Public Cab As String
    Public CCBancario As String
    Public Banca As String
    Public DataMandatoDebitore As Date
    Public IdMandatoDebitore As String
    Public Utente As String
    Public TipoExtra As String
    Public First As Integer
    Public CodiceBanca As String


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DatiPagamento where " & _
                           "CodiceOspite =? And CodiceParente = ? And Data =  ?")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO DatiPagamento (CodiceOspite,CodiceParente,Data) values (?,?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdIns.Parameters.AddWithValue("@CodiceParente", CodiceParente)
            cmdIns.Parameters.AddWithValue("@Data", Data)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE DatiPagamento SET " & _
                " IntestatarioConto = ?, " & _
                " IntestatarioCFConto = ?, " & _
                " CodiceInt = ?, " & _
                " NumeroControllo = ?, " & _
                " Cin = ?, " & _
                " Abi = ?, " & _
                " Cab = ?, " & _
                " CCBancario = ?, " & _
                " Banca = ?, " & _
                " DataMandatoDebitore = ?, " & _
                " IdMandatoDebitore = ?, " & _
                " First = ?, " & _
                " CodiceBanca = ?, " & _
                " Utente = ?, " & _
                " DataModifica = ? " & _
                " Where CodiceOspite = ? And CodiceParente = ? And Data = ?  "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql

        cmd1.Parameters.AddWithValue("@IntestatarioConto", IntestatarioConto)
        cmd1.Parameters.AddWithValue("@IntestatarioCFConto", IntestatarioCFConto)
        cmd1.Parameters.AddWithValue("@CodiceInt", CodiceInt)
        cmd1.Parameters.AddWithValue("@NumeroControllo", NumeroControllo)
        cmd1.Parameters.AddWithValue("@Cin", Cin)
        cmd1.Parameters.AddWithValue("@Abi", Abi)
        cmd1.Parameters.AddWithValue("@Cab", Cab)
        cmd1.Parameters.AddWithValue("@CCBancario", CCBancario)
        cmd1.Parameters.AddWithValue("@Banca", Banca)

        If Year(DataMandatoDebitore) > 1 Then
            cmd1.Parameters.AddWithValue("@DataMandatoDebitore", DataMandatoDebitore)
        Else
            cmd1.Parameters.AddWithValue("@DataMandatoDebitore", System.DBNull.Value)
        End If

        cmd1.Parameters.AddWithValue("@IdMandatoDebitore", IdMandatoDebitore)
        cmd1.Parameters.AddWithValue("@First", First)
        cmd1.Parameters.AddWithValue("@CodiceBanca", CodiceBanca)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@DataModifica", Now)        
        cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd1.Parameters.AddWithValue("@Data", Data)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from DatiPagamento Where CodiceOspite = ? And CodiceParente = ? And Data = ?  ")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub LeggiUltimaData(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select *  from DatiPagamento Where CodiceOspite = ? And CodiceParente = ?  Order By Data Desc")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            Data = campodb(myPOSTreader.Item("Data"))
        Else
            CodiceOspite = 0
            CodiceParente = 0
        End If
        myPOSTreader.Close()

        cn.Close()

        If CodiceOspite > 0 Then
            Call Leggi(StringaConnessione)
        End If

    End Sub


    Function VerificaSeUsato(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select *  from DatiPagamento Where  IdMandatoDebitore = ? and not(CodiceOspite = ? And CodiceParente = ? And Data = ?)")
        cmd.Parameters.AddWithValue("@IdMandatoDebitore", IdMandatoDebitore)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            VerificaSeUsato = True
        Else
            VerificaSeUsato = False
        End If
        myPOSTreader.Close()

        cn.Close()


    End Function

    Sub LeggiUltimaDataData(ByVal StringaConnessione As String, ByVal UltData As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select *  from DatiPagamento Where CodiceOspite = ? And CodiceParente = ?  And Data <= ? Order By Data Desc")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@UltData", UltData)

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            Data = campodb(myPOSTreader.Item("Data"))
        Else
            CodiceOspite = 0
            CodiceParente = 0
        End If
        myPOSTreader.Close()

        cn.Close()

        If CodiceOspite > 0 Then
            Call Leggi(StringaConnessione)
        End If

    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select *  from DatiPagamento Where CodiceOspite = ? And CodiceParente = ? And Data = ?  ")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
            Data = campodb(myPOSTreader.Item("Data"))
            IntestatarioConto = campodb(myPOSTreader.Item("IntestatarioConto"))
            IntestatarioCFConto = campodb(myPOSTreader.Item("IntestatarioCFConto"))
            CodiceInt = campodb(myPOSTreader.Item("CodiceInt"))
            NumeroControllo = campodb(myPOSTreader.Item("NumeroControllo"))
            Cin = campodb(myPOSTreader.Item("Cin"))
            Abi = campodb(myPOSTreader.Item("Abi"))
            Cab = campodb(myPOSTreader.Item("Cab"))
            CCBancario = campodb(myPOSTreader.Item("CCBancario"))
            Banca = campodb(myPOSTreader.Item("Banca"))

            If Year(campodbD(myPOSTreader.Item("DataMandatoDebitore"))) < 1901 Then
                DataMandatoDebitore = Nothing
            Else
                DataMandatoDebitore = campodb(myPOSTreader.Item("DataMandatoDebitore"))
            End If
            IdMandatoDebitore = campodb(myPOSTreader.Item("IdMandatoDebitore"))
            First = campodbn(myPOSTreader.Item("First"))
            CodiceBanca = campodb(myPOSTreader.Item("CodiceBanca"))

        End If

        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function



End Class

