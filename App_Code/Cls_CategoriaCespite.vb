﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_CategoriaCespite
    Public Anticipata As Double
    Public Conto As Integer
    Public ContoContropartita As Integer
    Public Descrizione As String
    Public Legge As Double
    Public Mastro As Integer
    Public MastroContropartita As Integer
    Public Minima As Double
    Public SottoConto As Long
    Public SottoContoContropartita As Long
    Public ContoImobilizazione As Integer
    Public MastroImobilizazione As Integer
    Public SottocontoImobilizazione As Long
    Public ID As Integer


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, Optional ByVal Tipo As String = "")
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CategoriaCespite Order by Descrizione")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("id")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from CategoriaCespite Where  id = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ID", ID)
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from Cespiti Where Categoria = ? ")
        cmd.Parameters.AddWithValue("@Categoria", ID)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CategoriaCespite Where  id = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Anticipata = campodbn(myPOSTreader.Item("Anticipata"))
            Conto = campodbn(myPOSTreader.Item("Conto"))
            ContoContropartita = campodbn(myPOSTreader.Item("ContoContropartita"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Legge = campodbn(myPOSTreader.Item("Legge"))
            Mastro = campodbn(myPOSTreader.Item("Mastro"))
            MastroContropartita = campodbn(myPOSTreader.Item("MastroContropartita"))
            Minima = campodbn(myPOSTreader.Item("Minima"))
            SottoConto = campodbn(myPOSTreader.Item("SottoConto"))
            SottoContoContropartita = campodbn(myPOSTreader.Item("SottoContoContropartita"))
            ContoImobilizazione = campodbn(myPOSTreader.Item("ContoImobilizazione"))
            MastroImobilizazione = campodbn(myPOSTreader.Item("MastroImobilizazione"))
            SottocontoImobilizazione = campodbn(myPOSTreader.Item("SottocontoImobilizazione"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CategoriaCespite Where  id = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = "Insert Into CategoriaCespite (Descrizione) Values (?)"
            cmdIns.Connection = cn
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.ExecuteNonQuery()

        End If
        myPOSTreader.Close()


        MySql = "UPDATE CategoriaCespite  SET "
        MySql = MySql & " Anticipata = ?,"
        MySql = MySql & " Conto  = ?,"
        MySql = MySql & " ContoContropartita  = ?,"
        MySql = MySql & " Descrizione  = ?,"
        MySql = MySql & " Legge  = ?,"
        MySql = MySql & " Mastro  = ?,"
        MySql = MySql & " MastroContropartita  = ?,"
        MySql = MySql & " Minima  = ?,"
        MySql = MySql & " SottoConto  = ?,"
        MySql = MySql & " SottoContoContropartita  = ?,"
        MySql = MySql & " ContoImobilizazione  = ?,"
        MySql = MySql & " MastroImobilizazione  = ?,"
        MySql = MySql & " SottocontoImobilizazione  = ?"
        MySql = MySql & "  Where ID = ? "

        Dim cmdMod As New OleDbCommand()
        cmdMod.CommandText = MySql
        cmdMod.Connection = cn
        cmdMod.Parameters.AddWithValue("@Anticipata", Anticipata)
        cmdMod.Parameters.AddWithValue("@Conto", Conto)
        cmdMod.Parameters.AddWithValue("@ContoContropartita", ContoContropartita)
        cmdMod.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdMod.Parameters.AddWithValue("@Legge", Legge)
        cmdMod.Parameters.AddWithValue("@Mastro", Mastro)
        cmdMod.Parameters.AddWithValue("@MastroContropartita", MastroContropartita)
        cmdMod.Parameters.AddWithValue("@Minima", Minima)
        cmdMod.Parameters.AddWithValue("@SottoConto", SottoConto)
        cmdMod.Parameters.AddWithValue("@SottoContoContropartita", SottoContoContropartita)
        cmdMod.Parameters.AddWithValue("@ContoImobilizazione", ContoImobilizazione)
        cmdMod.Parameters.AddWithValue("@MastroImobilizazione", MastroImobilizazione)
        cmdMod.Parameters.AddWithValue("@SottocontoImobilizazione", SottocontoImobilizazione)
        cmdMod.Parameters.AddWithValue("@ID", ID)
        cmdMod.ExecuteNonQuery()
    End Sub
End Class
