﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Listino

    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public DATA As Date
    Public CodiceListino As String

    Public CodiceListinoPrivatoSanitario As String
    Public CodiceListinoSanitario As String
    Public CodiceListinoSociale As String
    Public CodiceListinoJolly As String
    Public UTENTE As String

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Public Sub LeggiAData(ByVal StringaConnessione As String, ByVal DataInzio As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from Listino Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@DATA", DataInzio)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CENTROSERVIZIO = campodb(myPOSTreader.Item("DATA"))
            CODICEOSPITE = campodb(myPOSTreader.Item("CODICEOSPITE"))
            DATA = campodbD(myPOSTreader.Item("DATA"))
            CodiceListino = campodb(myPOSTreader.Item("CodiceListino"))

            CodiceListinoPrivatoSanitario = campodb(myPOSTreader.Item("CodiceListinoPrivatoSanitario"))
            CodiceListinoSanitario = campodb(myPOSTreader.Item("CodiceListinoSanitario"))
            CodiceListinoSociale = campodb(myPOSTreader.Item("CodiceListinoSociale"))
            CodiceListinoJolly = campodb(myPOSTreader.Item("CodiceListinoJolly"))
            UTENTE = campodb(myPOSTreader.Item("UTENTE"))

        End If
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Listino Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Listino Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA = ?")
        cmd.Parameters.AddWithValue("@DATA", DATA)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Listino where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATA= ?")
        cmd.Parameters.AddWithValue("@DATA", DATA)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            MySql = "UPDATE Listino SET DATA= ?,CodiceListino=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And DATA = ? "

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@DATA", DATA)
            cmdw.Parameters.AddWithValue("@CodiceListino", CodiceListino)            
            cmdw.Parameters.AddWithValue("@DATA", DATA)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else

            MySql = "INSERT INTO Listino (CentroServizio,CodiceOspite,DATA,CodiceListino,CodiceListinoPrivatoSanitario,CodiceListinoSanitario,CodiceListinoSociale,CodiceListinoJolly,UTENTE,DATAAGGIORNAMENTO) VALUES (?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@DATA", DATA)
            cmdw.Parameters.AddWithValue("@CodiceListino", CodiceListino)
            cmdw.Parameters.AddWithValue("@CodiceListinoPrivatoSanitario", CodiceListinoPrivatoSanitario)
            cmdw.Parameters.AddWithValue("@CodiceListinoSanitario", CodiceListinoSanitario)
            cmdw.Parameters.AddWithValue("@CodiceListinoSociale", CodiceListinoSociale)
            cmdw.Parameters.AddWithValue("@CodiceListinoJolly", CodiceListinoJolly)
            cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Listino where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By DATA")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DATA", GetType(String))
        Tabella.Columns.Add("Listino", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")

            Dim TabellaListi As New Cls_Tabella_Listino

            TabellaListi.Codice = campodb(myPOSTreader.Item("CodiceListino"))
            TabellaListi.Descrizione = ""
            TabellaListi.LeggiCausale(StringaConnessione)
            myriga(1) = TabellaListi.Descrizione
            If campodb(myPOSTreader.Item("CodiceListinoPrivatoSanitario")) <> "" Then
                TabellaListi.Codice = campodb(myPOSTreader.Item("CodiceListinoPrivatoSanitario"))
                TabellaListi.Descrizione = ""
                TabellaListi.LeggiCausale(StringaConnessione)
                myriga(1) = myriga(1) & "," & TabellaListi.Descrizione
            End If
            If campodb(myPOSTreader.Item("CodiceListinoSanitario")) <> "" Then
                TabellaListi.Codice = campodb(myPOSTreader.Item("CodiceListinoSanitario"))
                TabellaListi.Descrizione = ""
                TabellaListi.LeggiCausale(StringaConnessione)
                myriga(1) = myriga(1) & "," & TabellaListi.Descrizione
            End If
            If campodb(myPOSTreader.Item("CodiceListinoSociale")) <> "" Then
                TabellaListi.Codice = campodb(myPOSTreader.Item("CodiceListinoSociale"))
                TabellaListi.Descrizione = ""
                TabellaListi.LeggiCausale(StringaConnessione)
                myriga(1) = myriga(1) & "," & TabellaListi.Descrizione
            End If

            If campodb(myPOSTreader.Item("CodiceListinoJolly")) <> "" Then
                TabellaListi.Codice = campodb(myPOSTreader.Item("CodiceListinoJolly"))
                TabellaListi.Descrizione = ""
                TabellaListi.LeggiCausale(StringaConnessione)
                myriga(1) = myriga(1) & "," & TabellaListi.Descrizione
            End If
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub InserisciRette(ByVal ConnessioneOspiti As String, ByVal CodiceServizio As String, ByVal CodiceOspite As String, ByVal CodiceListino As String, ByVal DataInserimento As Date, Optional ByVal CodiceListinoPrivatoSanitario As String = "", Optional ByVal CodiceListinoSanitario As String = "", Optional ByVal CodiceListinoSociale As String = "", Optional ByVal CodiceListinoJolly As String = "")
        Dim ListinoOspiteIns As New Cls_Listino

        ListinoOspiteIns.CENTROSERVIZIO = CodiceServizio
        ListinoOspiteIns.CODICEOSPITE = CodiceOspite
        ListinoOspiteIns.DATA = DataInserimento
        ListinoOspiteIns.CodiceListino = CodiceListino
        ListinoOspiteIns.CodiceListinoPrivatoSanitario = CodiceListinoPrivatoSanitario
        ListinoOspiteIns.CodiceListinoSanitario = CodiceListinoSanitario
        ListinoOspiteIns.CodiceListinoJolly = CodiceListinoJolly
        ListinoOspiteIns.CodiceListinoSociale = CodiceListinoSociale
        ListinoOspiteIns.AggiornaDB(ConnessioneOspiti)



        Dim Listino As New Cls_Tabella_Listino
        Dim ParenteIntestatario As Integer = 0


        Listino.Codice = CodiceListino
        Listino.LeggiCausale(ConnessioneOspiti)

        If Listino.MODALITA = "P" Then
            Dim Parente As New Cls_Parenti
            ParenteIntestatario = Parente.ParenteIntestatario(ConnessioneOspiti, CodiceOspite)

        End If


        If CodiceListinoPrivatoSanitario <> "" Then
            Dim ListinoPrivatoSanitario As New Cls_Tabella_Listino

            ListinoPrivatoSanitario.Codice = CodiceListinoPrivatoSanitario
            ListinoPrivatoSanitario.LeggiCausale(ConnessioneOspiti)

            Listino.IMPORTORETTTAOSPITE2 = ListinoPrivatoSanitario.IMPORTORETTTAOSPITE2

            Listino.IMPORTORETTATOTALE = Listino.IMPORTORETTATOTALE + ListinoPrivatoSanitario.IMPORTORETTTAOSPITE2
        End If

        If CodiceListinoSanitario <> "" Then
            Dim UslSan As New Cls_ImportoRegione
            Dim ListinoSanitario As New Cls_Tabella_Listino


            ListinoSanitario.Codice = CodiceListinoSanitario
            ListinoSanitario.LeggiCausale(ConnessioneOspiti)

            If ListinoSanitario.USL = "" Then
                UslSan.TipoRetta = ListinoSanitario.TIPORETTAUSL
                UslSan.UltimaDataSenzaRegione(ConnessioneOspiti, ListinoSanitario.TIPORETTAUSL)
            Else
                UslSan.Codice = ListinoSanitario.USL
                UslSan.TipoRetta = ListinoSanitario.TIPORETTAUSL
                UslSan.UltimaData(ConnessioneOspiti, ListinoSanitario.USL, ListinoSanitario.TIPORETTAUSL)
            End If
            
            Listino.IMPORTORETTATOTALE = Listino.IMPORTORETTATOTALE + UslSan.Importo
            Listino.USL = ListinoSanitario.USL
            Listino.TIPORETTAUSL = ListinoSanitario.TIPORETTAUSL
        End If

        If CodiceListinoSociale <> "" Then
            Dim ListinoSociale As New Cls_Tabella_Listino


            ListinoSociale.Codice = CodiceListinoSociale
            ListinoSociale.LeggiCausale(ConnessioneOspiti)
            Listino.IMPORTORETTTASOCIALE = ListinoSociale.IMPORTORETTTASOCIALE
            Listino.TIPORETTASOCIALE = ListinoSociale.TIPORETTASOCIALE
            Listino.SOCIALECOMUNE = ListinoSociale.SOCIALECOMUNE
            Listino.SOCIALEPROVINCIA = ListinoSociale.SOCIALEPROVINCIA
        End If


        If CodiceListinoJolly <> "" Then
            Dim ListinoJolly As New Cls_Tabella_Listino


            ListinoJolly.Codice = CodiceListinoJolly
            ListinoJolly.LeggiCausale(ConnessioneOspiti)
            Listino.IMPORTORETTTAJOLLY = ListinoJolly.IMPORTORETTTAJOLLY
            Listino.TIPORETTAJOLLY = ListinoJolly.TIPORETTAJOLLY
            Listino.JOLLYCOMUNE = ListinoJolly.JOLLYCOMUNE
            Listino.JOLLYPROVINCIA = ListinoJolly.JOLLYPROVINCIA
        End If

        Dim RettaTotale As New Cls_rettatotale


        RettaTotale.CENTROSERVIZIO = CodiceServizio
        RettaTotale.CODICEOSPITE = CodiceOspite
        RettaTotale.Data = DataInserimento
        RettaTotale.Importo = Listino.IMPORTORETTATOTALE
        RettaTotale.Tipo = Listino.TIPORETTATOTALE
        RettaTotale.TipoRetta = Listino.TipoRetta
        RettaTotale.AggiornaDB(ConnessioneOspiti)

        If Listino.CODICEEXTRA1 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA1

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If


        If Listino.CODICEEXTRA2 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA2

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If



        If Listino.CODICEEXTRA3 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA3

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If



        If Listino.CODICEEXTRA4 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA4

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If


        If Listino.CODICEEXTRA5 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA5

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If


        If Listino.CODICEEXTRA6 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA6

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If



        If Listino.CODICEEXTRA7 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA7

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If



        If Listino.CODICEEXTRA8 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA8

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If



        If Listino.CODICEEXTRA9 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA9

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If


        If Listino.CODICEEXTRA10 <> "" Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = CodiceServizio
            Extra.CODICEOSPITE = CodiceOspite
            Extra.Data = DataInserimento
            Extra.CODICEEXTRA = Listino.CODICEEXTRA10

            Dim TipoExtra As New Cls_TipoExtraFisso

            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(ConnessioneOspiti)
            Extra.RIPARTIZIONE = TipoExtra.Ripartizione
            Extra.AggiornaDB_Extra(ConnessioneOspiti)
        End If

        Dim StatoAuto As New Cls_StatoAuto

        StatoAuto.CENTROSERVIZIO = CodiceServizio
        StatoAuto.CODICEOSPITE = CodiceOspite
        StatoAuto.Data = DataInserimento

        If Listino.USL = "" Then
            StatoAuto.STATOAUTO = "A"
        Else
            StatoAuto.STATOAUTO = "N"
        End If

        If Listino.USLDACOMUNERESIDENZA = 1 Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(ConnessioneOspiti, Ospite.CodiceOspite)

            If campodb(Ospite.RESIDENZACOMUNE1) <> "" Then
                Dim ComuneResidenza As New ClsComune

                ComuneResidenza.Comune = Ospite.RESIDENZACOMUNE1
                ComuneResidenza.Provincia = Ospite.RESIDENZAPROVINCIA1
                ComuneResidenza.Leggi(ConnessioneOspiti)

                If ComuneResidenza.RIFERIMENTOREGIONE.Trim <> "" Then
                    Listino.USL = ComuneResidenza.RIFERIMENTOREGIONE
                End If
            End If

        End If

        StatoAuto.USL = Listino.USL
        StatoAuto.TipoRetta = Listino.TIPORETTAUSL
        StatoAuto.AggiornaDB(ConnessioneOspiti)



        Dim Modalita As New Cls_Modalita

        Modalita.CentroServizio = CodiceServizio
        Modalita.CodiceOspite = CodiceOspite
        Modalita.Data = DataInserimento
        Modalita.MODALITA = Listino.MODALITA
        Modalita.AggiornaDB(ConnessioneOspiti)


        If Listino.TIPOOPERAIZONE <> "" And ((Listino.TIPOLISTINO = "" And Listino.MODALITA <> "P") Or (Listino.TIPOLISTINO = "S" And Listino.MODALITA = "O")) Then
            Dim AzzeraParenti As New Cls_ImportoParente
            Dim IndiceParente As Integer

            For IndiceParente = 1 To 10

                AzzeraParenti.IMPORTO = 0
                AzzeraParenti.PERCENTUALE = 0
                AzzeraParenti.CODICEOSPITE = CodiceOspite
                AzzeraParenti.CODICEPARENTE = IndiceParente
                AzzeraParenti.CENTROSERVIZIO = CodiceServizio
                AzzeraParenti.UltimaData(ConnessioneOspiti, AzzeraParenti.CODICEOSPITE, AzzeraParenti.CENTROSERVIZIO, AzzeraParenti.CODICEPARENTE)
                If AzzeraParenti.IMPORTO > 0 Or AzzeraParenti.PERCENTUALE > 0 Then
                    Dim RettaParenteAzzeramento As New Cls_ImportoParente

                    RettaParenteAzzeramento.CENTROSERVIZIO = CodiceServizio
                    RettaParenteAzzeramento.CODICEOSPITE = CodiceOspite
                    RettaParenteAzzeramento.CODICEPARENTE = IndiceParente
                    RettaParenteAzzeramento.DATA = DataInserimento
                    RettaParenteAzzeramento.IMPORTO = 0
                    RettaParenteAzzeramento.IMPORTO_2 = 0
                    RettaParenteAzzeramento.PERCENTUALE = 0
                    RettaParenteAzzeramento.TIPOIMPORTO = "G"
                    RettaParenteAzzeramento.AggiornaDB(ConnessioneOspiti)
                End If
            Next

            Dim RettaOSpiteCserv As New Cls_DatiOspiteParenteCentroServizio


            RettaOSpiteCserv.CentroServizio = CodiceServizio
            RettaOSpiteCserv.CodiceOspite = CodiceOspite
            RettaOSpiteCserv.Leggi(ConnessioneOspiti)
            RettaOSpiteCserv.CentroServizio = CodiceServizio
            RettaOSpiteCserv.CodiceOspite = CodiceOspite
            RettaOSpiteCserv.TipoOperazione = Listino.TIPOOPERAIZONE
            RettaOSpiteCserv.AliquotaIva = Listino.CODICEIVA
            RettaOSpiteCserv.Anticipata = Listino.ANTICIPATA
            RettaOSpiteCserv.Settimana = Listino.SETTIMANA
            RettaOSpiteCserv.Compensazione = Listino.COMPENSAZIONE
           
            If Listino.MODALITAPAGAMENTO <>"" then
                RettaOSpiteCserv.ModalitaPagamento = Listino.MODALITAPAGAMENTO
            End If

            RettaOSpiteCserv.Scrivi(ConnessioneOspiti)

            Dim AnagraficaUtente As New ClsOspite



            AnagraficaUtente.CodiceOspite = CodiceOspite
            AnagraficaUtente.Leggi(ConnessioneOspiti, AnagraficaUtente.CodiceOspite)
            AnagraficaUtente.TIPOOPERAZIONE = Listino.TIPOOPERAIZONE
            AnagraficaUtente.CODICEIVA = Listino.CODICEIVA
            AnagraficaUtente.FattAnticipata = Listino.ANTICIPATA
            AnagraficaUtente.SETTIMANA = Listino.SETTIMANA
            AnagraficaUtente.Compensazione = Listino.COMPENSAZIONE
            AnagraficaUtente.PERIODO = "M"
            If Listino.PeriodoFatturazione <> "" Then
                AnagraficaUtente.PERIODO = Listino.PeriodoFatturazione
            End If
            If Listino.MODALITAPAGAMENTO <>"" then
                AnagraficaUtente.MODALITAPAGAMENTO = Listino.MODALITAPAGAMENTO
            End If
            AnagraficaUtente.ScriviOspite(ConnessioneOspiti)


            Dim RettaOspite As New Cls_ImportoOspite

            RettaOspite.CENTROSERVIZIO = CodiceServizio
            RettaOspite.CODICEOSPITE = CodiceOspite
            RettaOspite.Data = DataInserimento
            RettaOspite.Importo = Listino.IMPORTORETTTAOSPITE1
            RettaOspite.Importo_2 = Listino.IMPORTORETTTAOSPITE2
            RettaOspite.Tipo = Listino.TIPORETTTAOSPITE
            RettaOspite.TIPOOPERAZIONE = Listino.TIPOOPERAIZONE
            RettaOspite.AggiornaDB(ConnessioneOspiti)
        Else
            Dim VerRettaOspite As New Cls_ImportoComune

            VerRettaOspite.CENTROSERVIZIO = CodiceServizio
            VerRettaOspite.CODICEOSPITE = CodiceOspite
            VerRettaOspite.UltimaData(ConnessioneOspiti, CodiceOspite, CodiceServizio)

            If VerRettaOspite.Importo > 0 Then
                Dim RettaOspite As New Cls_ImportoOspite

                RettaOspite.CENTROSERVIZIO = CodiceServizio
                RettaOspite.CODICEOSPITE = CodiceOspite
                RettaOspite.Data = DataInserimento
                RettaOspite.Importo = 0
                RettaOspite.Importo_2 = 0
                RettaOspite.Tipo = "G"
                RettaOspite.AggiornaDB(ConnessioneOspiti)
            End If
        End If

        If Listino.TIPOOPERAIZONE <> "" And Listino.MODALITA = "P" And ParenteIntestatario > 0 Then
            Dim AzzeraParenti As New Cls_ImportoParente
            Dim IndiceParente As Integer

            For IndiceParente = 1 To 10
                If IndiceParente <> ParenteIntestatario Then

                    AzzeraParenti.IMPORTO = 0
                    AzzeraParenti.PERCENTUALE = 0
                    AzzeraParenti.CODICEOSPITE = CodiceOspite
                    AzzeraParenti.CODICEPARENTE = IndiceParente
                    AzzeraParenti.CENTROSERVIZIO = CodiceServizio
                    AzzeraParenti.UltimaData(ConnessioneOspiti, AzzeraParenti.CODICEOSPITE, AzzeraParenti.CENTROSERVIZIO, AzzeraParenti.CODICEPARENTE)
                    If AzzeraParenti.IMPORTO > 0 Or AzzeraParenti.PERCENTUALE > 0 Then
                        Dim RettaParenteAzzeramento As New Cls_ImportoParente

                        RettaParenteAzzeramento.CENTROSERVIZIO = CodiceServizio
                        RettaParenteAzzeramento.CODICEOSPITE = CodiceOspite
                        RettaParenteAzzeramento.CODICEPARENTE = IndiceParente
                        RettaParenteAzzeramento.DATA = DataInserimento
                        RettaParenteAzzeramento.IMPORTO = 0
                        RettaParenteAzzeramento.IMPORTO_2 = 0
                        RettaParenteAzzeramento.PERCENTUALE = 0
                        RettaParenteAzzeramento.TIPOIMPORTO = "G"
                        RettaParenteAzzeramento.AggiornaDB(ConnessioneOspiti)
                    End If
                End If
            Next


            Dim RettaParenteCserv As New Cls_DatiOspiteParenteCentroServizio


            RettaParenteCserv.CentroServizio = CodiceServizio
            RettaParenteCserv.CodiceOspite = CodiceOspite
            RettaParenteCserv.CodiceParente = ParenteIntestatario
            RettaParenteCserv.Leggi(ConnessioneOspiti)


            RettaParenteCserv.CentroServizio = CodiceServizio
            RettaParenteCserv.CodiceOspite = CodiceOspite
            RettaParenteCserv.CodiceParente = ParenteIntestatario
            RettaParenteCserv.TipoOperazione = Listino.TIPOOPERAIZONE
            RettaParenteCserv.AliquotaIva = Listino.CODICEIVA
            RettaParenteCserv.Anticipata = Listino.ANTICIPATA
            RettaParenteCserv.Settimana = Listino.SETTIMANA
            RettaParenteCserv.Compensazione = Listino.COMPENSAZIONE
            If Listino.MODALITAPAGAMENTO <>"" then
                RettaParenteCserv.ModalitaPagamento = Listino.MODALITAPAGAMENTO
            End if
            RettaParenteCserv.Scrivi(ConnessioneOspiti)

            Dim AnagraficaUtente As New Cls_Parenti



            AnagraficaUtente.CodiceOspite = CodiceOspite
            AnagraficaUtente.CodiceParente = ParenteIntestatario
            AnagraficaUtente.Leggi(ConnessioneOspiti, AnagraficaUtente.CodiceOspite, AnagraficaUtente.CodiceParente)
            AnagraficaUtente.TIPOOPERAZIONE = Listino.TIPOOPERAIZONE
            AnagraficaUtente.CODICEIVA = Listino.CODICEIVA
            AnagraficaUtente.FattAnticipata = Listino.ANTICIPATA
            AnagraficaUtente.Compensazione = Listino.COMPENSAZIONE
            AnagraficaUtente.PERIODO = "M"

            If Listino.MODALITAPAGAMENTO <>"" then
                AnagraficaUtente.MODALITAPAGAMENTO = Listino.MODALITAPAGAMENTO
            End If
            AnagraficaUtente.ScriviParente(ConnessioneOspiti)


            Dim RettaParente As New Cls_ImportoParente

            RettaParente.CENTROSERVIZIO = CodiceServizio
            RettaParente.CODICEOSPITE = CodiceOspite
            RettaParente.CODICEPARENTE = ParenteIntestatario
            RettaParente.DATA = DataInserimento
            RettaParente.IMPORTO = Listino.IMPORTORETTTAOSPITE1
            RettaParente.IMPORTO_2 = Listino.IMPORTORETTTAOSPITE2
            RettaParente.PERCENTUALE = 1
            RettaParente.TIPOIMPORTO = Listino.TIPORETTTAOSPITE
            RettaParente.TIPOOPERAZIONE = Listino.TIPOOPERAIZONE
            RettaParente.AggiornaDB(ConnessioneOspiti)

        End If

        If Listino.SOCIALECOMUNE <> "" Then
            Dim comune As New Cls_ImportoComune

            comune.CENTROSERVIZIO = CodiceServizio
            comune.CODICEOSPITE = CodiceOspite
            comune.Data = DataInserimento
            comune.Importo = Listino.IMPORTORETTTASOCIALE
            comune.COMUNE = Listino.SOCIALECOMUNE
            comune.PROV = Listino.SOCIALEPROVINCIA
            comune.Tipo = Listino.TIPORETTTAOSPITE
            comune.AggiornaDB(ConnessioneOspiti)
        Else
            Dim comune As New Cls_ImportoComune

            comune.CENTROSERVIZIO = CodiceServizio
            comune.CODICEOSPITE = CodiceOspite
            comune.UltimaData(ConnessioneOspiti, comune.CODICEOSPITE, comune.CENTROSERVIZIO)

            If comune.Importo > 0 Then
                comune.CENTROSERVIZIO = CodiceServizio
                comune.CODICEOSPITE = CodiceOspite
                comune.Data = DataInserimento
                comune.Importo = 0
                comune.Tipo = "G"
                comune.AggiornaDB(ConnessioneOspiti)
            End If
        End If


        If Listino.JOLLYCOMUNE <> "" Then
            Dim jolly As New Cls_ImportoJolly

            jolly.CENTROSERVIZIO = CodiceServizio
            jolly.CODICEOSPITE = CodiceOspite
            jolly.Data = DataInserimento
            jolly.Importo = Listino.IMPORTORETTTAJOLLY
            jolly.COMUNE = Listino.JOLLYCOMUNE
            jolly.PROV = Listino.JOLLYPROVINCIA
            jolly.Tipo = Listino.TIPORETTAJOLLY
            jolly.AggiornaDB(ConnessioneOspiti)
        Else
            Dim jolly As New Cls_ImportoJolly

            jolly.CENTROSERVIZIO = CodiceServizio
            jolly.CODICEOSPITE = CodiceOspite
            jolly.UltimaData(ConnessioneOspiti, jolly.CODICEOSPITE, jolly.CENTROSERVIZIO)

            If jolly.Importo > 0 Then
                jolly.CENTROSERVIZIO = CodiceServizio
                jolly.CODICEOSPITE = CodiceOspite
                jolly.Data = DataInserimento
                jolly.Importo = 0
                jolly.Tipo = "G"
                jolly.AggiornaDB(ConnessioneOspiti)
            End If
        End If


    End Sub

End Class
