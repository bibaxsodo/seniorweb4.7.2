Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class ClsUSL
    Public CodiceRegione As String
    Public Nome As String
    Public NonInVisualizzazione As String ' NomeConiuge
    Public Tipologia As String
    Public CINCLIENTE As String
    Public CCBANCARIOCLIENTE As String
    Public BancaCliente As String


    Public RESIDENZAPROVINCIA1 As String
    Public RESIDENZACOMUNE1 As String
    Public RESIDENZAINDIRIZZO1 As String
    Public Attenzione As String
    Public RESIDENZACAP1 As String
    Public PARTITAIVA As Long
    Public CodiceFiscale As String
    Public CONTOPERESATTO As String
    Public FuoriRegione As Integer


    Public CodiceCig As String
    Public CABCLIENTE As String
    Public ABICLIENTE As String
    Public IntCliente As String
    Public NumeroControlloCliente As String
    Public Periodo As String

    Public ImportoAZero As Integer

    Public ImportoSconto As Double

    Public TipoOperazione As String
    Public ModalitaPagamento As String
    Public CodiceIva As String
    Public Raggruppamento As String
    Public SeparaInFattura As Integer
    Public Compensazione As String
    Public MastroCliente As Long
    Public ContoCliente As Long
    Public SottoContoCliente As Long
    Public RotturaCserv As String
    Public RotturaOspite As Long

    Public Vincolo As String
    Public CodiceDestinatario As String
    Public CodiceCup As String
    Public IdDocumento As String
    Public RiferimentoAmministrazione As String
    Public NumeroFatturaDDT As Integer
    Public Note As String

    Public RESIDENZATELEFONO3 As String

    Public RecapitoNome As String
    Public RecapitoIndirizzo As String
    Public RESIDENZATELEFONO4 As String
    Public EGo As String


    Public NumItem As String
    Public CodiceCommessaConvezione As String
    Public PEC As String
    Public IdDocumentoData As String
    Public RaggruppaInExport As Integer
    Public MeseFatturazione As Integer
    Public AnnoFatturazione As Integer
    Public DescrizioneXML As String
    Public NonInUso As String
    Public RaggruppaInElaborazione As Integer


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from STATOAUTO Where USL = ? ")
        cmd.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function



    Sub EliminaRegione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection        

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("Delete from AnagraficaComune Where TIPOLOGIA = 'R' And CodiceRegione = ?")
        cmd1.Connection = cn
        cmd1.Parameters.AddWithValue("@Codice", CodiceRegione)
        cmd1.ExecuteNonQuery()

        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = ("Delete from IMPORTOREGIONI Where  CodiceRegione = ?")
        cmd2.Connection = cn
        cmd2.Parameters.AddWithValue("@Codice", CodiceRegione)
        cmd2.ExecuteNonQuery()

        cn.Close()

    End Sub

    Sub ScriviRegione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        If CodiceRegione = "" Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' And CodiceRegione = ?")
        cmd1.Connection = cn
        cmd1.Parameters.AddWithValue("@Codice", CodiceRegione)

        Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE AnagraficaComune SET " & _
                    "Nome  = ?," & _
                    "NomeConiuge  = ?, " & _
                    "Tipologia  = ?," & _
                    "CINCLIENTE  = ?," & _
                    "CCBANCARIOCLIENTE  = ?," & _
                    "BancaCliente  = ?," & _
                    "RESIDENZAPROVINCIA1  = ?," & _
                    "RESIDENZACOMUNE1  = ?," & _
                    "RESIDENZAINDIRIZZO1  = ?," & _
                    "Attenzione  = ?," & _
                    "RESIDENZACAP1  = ?," & _
                    "PARTITAIVA = ?," & _
                    "CodiceFiscale  = ?," & _
                    "CONTOPERESATTO  = ?," & _
                    "FuoriRegione = ?," & _
                    "CodiceCig  = ?," & _
                    "CABCLIENTE  = ?," & _
                    "ABICLIENTE  = ?," & _
                    "IntCliente  = ?," & _
                    "NumeroControlloCliente  = ?," & _
                    "Periodo  = ?," & _
                    "ImportoAZero = ?," & _
                    "ImportoSconto = ?," & _
                    "TipoOperazione  = ?," & _
                    "ModalitaPagamento  = ?," & _
                    "CodiceIva  = ?," & _
                    "Raggruppamento  = ?," & _
                    "SeparaInFattura = ?," & _
                    "Compensazione  = ?," & _
                    "MastroCliente = ?," & _
                    "ContoCliente = ?," & _
                    "SottoContoCliente = ?," & _
                    "RotturaCserv  = ?, " & _
                    "CodiceDestinatario=?, " & _
                    "RotturaOspite  = ?, " & _
                    "CodiceCup  = ?, " & _
                    "IdDocumento = ?, " & _
                    "RiferimentoAmministrazione = ?, " & _
                    "NumeroFatturaDDT = ?,  " & _
                    "Note = ?,  " & _
                    "RESIDENZATELEFONO3 = ?,  " & _
                    " RecapitoNome = ?,  " & _
                    " RecapitoIndirizzo = ?,  " & _
                    " RESIDENZATELEFONO4 = ?,  " & _
                    " Ego = ?,  " & _
                    " NumItem = ?,  " & _
                    " CodiceCommessaConvezione = ? , " & _
                    " PEC = ?,  " & _
                    " IdDocumentoData = ?, " & _
                    " RaggruppaInExport = ?, " & _
                    " MeseFatturazione = ?, " & _
                    " AnnoFatturazione = ?, " & _
                    " DescrizioneXML = ?, " & _
                    " NonInUso = ?, " & _
                    " RaggruppaInElaborazione = ? " & _
                    "WHERE CODICEREGIONE = ?"
        Else
            MySql = "INSERT INTO AnagraficaComune " & _
                    "(Nome  ," & _
                    "NomeConiuge  , " & _
                    "Tipologia  ," & _
                    "CINCLIENTE  ," & _
                    "CCBANCARIOCLIENTE  ," & _
                    "BancaCliente  ," & _
                    "RESIDENZAPROVINCIA1  ," & _
                    "RESIDENZACOMUNE1  ," & _
                    "RESIDENZAINDIRIZZO1  ," & _
                    "Attenzione  ," & _
                    "RESIDENZACAP1  ," & _
                    "PARTITAIVA ," & _
                    "CodiceFiscale  ," & _
                    "CONTOPERESATTO  ," & _
                    "FuoriRegione, " & _
                    "CodiceCig  ," & _
                    "CABCLIENTE  ," & _
                    "ABICLIENTE  ," & _
                    "IntCliente  ," & _
                    "NumeroControlloCliente  ," & _
                    "Periodo  ," & _
                    "ImportoAZero ," & _
                    "ImportoSconto ," & _
                    "TipoOperazione  ," & _
                    "ModalitaPagamento  ," & _
                    "CodiceIva  ," & _
                    "Raggruppamento  ," & _
                    "SeparaInFattura ," & _
                    "Compensazione  ," & _
                    "MastroCliente ," & _
                    "ContoCliente ," & _
                    "SottoContoCliente ," & _
                    "RotturaCserv  ," & _
                    "CodiceDestinatario ," & _
                    "RotturaOspite, " & _
                    "CodiceCup, " & _
                    "IdDocumento, " & _
                    "RiferimentoAmministrazione, " & _
                    "NumeroFatturaDDT, " & _
                    "Note, " & _
                    "RESIDENZATELEFONO3,  " & _
                    "RecapitoNome,  " & _
                    "RecapitoIndirizzo,  " & _
                    "RESIDENZATELEFONO4," & _
                    "Ego," & _
                    "NumItem," & _
                    "CodiceCommessaConvezione," & _
                    "PEC," & _
                    "IdDocumentoData, " & _
                    "RaggruppaInExport, " & _
                    "MeseFatturazione, " & _
                    "AnnoFatturazione, " & _
                    "DescrizioneXML, " & _
                    "NonInUso," & _
                    "RaggruppaInElaborazione," & _
                    "CODICEREGIONE ) VALUES " & _
                    "(?  ," & _
                    "?, " & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?," & _
                    "?) "
        End If
        myPOSTreader.Close()
        Dim cmd As New OleDbCommand()


        cmd.Connection = cn
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@NonInVisualizzazione", NonInVisualizzazione)
        cmd.Parameters.AddWithValue("@Tipologia", "R")
        cmd.Parameters.AddWithValue("@CINCLIENTE", CINCLIENTE)
        cmd.Parameters.AddWithValue("@CCBANCARIOCLIENTE", CCBANCARIOCLIENTE)
        cmd.Parameters.AddWithValue("@BancaCliente", BancaCliente)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", RESIDENZAPROVINCIA1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", RESIDENZACOMUNE1)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@Attenzione", Attenzione)
        cmd.Parameters.AddWithValue("@RESIDENZACAP1", RESIDENZACAP1)
        cmd.Parameters.AddWithValue("@PARTITAIVA", PARTITAIVA)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Parameters.AddWithValue("@CONTOPERESATTO", CONTOPERESATTO)
        cmd.Parameters.AddWithValue("@FuoriRegione", FuoriRegione)
        cmd.Parameters.AddWithValue("@CodiceCig", CodiceCig)
        cmd.Parameters.AddWithValue("@CABCLIENTE", CABCLIENTE)
        cmd.Parameters.AddWithValue("@ABICLIENTE", ABICLIENTE)
        cmd.Parameters.AddWithValue("@IntCliente", IntCliente)
        cmd.Parameters.AddWithValue("@NumeroControlloCliente", NumeroControlloCliente)
        cmd.Parameters.AddWithValue("@Periodo", Periodo)
        cmd.Parameters.AddWithValue("@ImportoAZero", ImportoAZero)
        cmd.Parameters.AddWithValue("@ImportoSconto", ImportoSconto)
        cmd.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
        cmd.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd.Parameters.AddWithValue("@CodiceIva", CodiceIva)
        cmd.Parameters.AddWithValue("@Raggruppamento", Raggruppamento)
        cmd.Parameters.AddWithValue("@SeparaInFattura", SeparaInFattura)
        cmd.Parameters.AddWithValue("@Compensazione", Compensazione)
        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottoContoCliente", SottoContoCliente)
        cmd.Parameters.AddWithValue("@RotturaCserv", RotturaCserv)
        cmd.Parameters.AddWithValue("@CodiceDestinatario", CodiceDestinatario)
        cmd.Parameters.AddWithValue("@RotturaOspite", RotturaOspite)
        cmd.Parameters.AddWithValue("@CodiceCup", CodiceCup)

        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@RiferimentoAmministrazione", RiferimentoAmministrazione)
        cmd.Parameters.AddWithValue("@NumeroFatturaDDT", NumeroFatturaDDT)
        cmd.Parameters.AddWithValue("@Note", Note)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO3", RESIDENZATELEFONO3)

        cmd.Parameters.AddWithValue("@RecapitoNome", RecapitoNome)
        cmd.Parameters.AddWithValue("@RecapitoIndirizzo", RecapitoIndirizzo)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO4", RESIDENZATELEFONO4)
        cmd.Parameters.AddWithValue("@Ego", EGo)

        cmd.Parameters.AddWithValue("@NumItem", NumItem)
        cmd.Parameters.AddWithValue("@CodiceCommessaConvezione", CodiceCommessaConvezione)
        cmd.Parameters.AddWithValue("@PEC", PEC)
        cmd.Parameters.AddWithValue("@IdDocumentoData", IdDocumentoData)
        cmd.Parameters.AddWithValue("@RaggruppaInExport", RaggruppaInExport)

        cmd.Parameters.AddWithValue("@MeseFatturazione", MeseFatturazione)

        cmd.Parameters.AddWithValue("@AnnoFatturazione", AnnoFatturazione)
        cmd.Parameters.AddWithValue("@DescrizioneXML", DescrizioneXML)

        cmd.Parameters.AddWithValue("@NonInUso", NonInUso)

        cmd.Parameters.AddWithValue("@RaggruppaInElaborazione", RaggruppaInElaborazione)

        'RaggruppaInElaborazione
        'RaggruppaInExport
        cmd.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)

        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub

    Function RegionePadre(ByVal StringaConnessione As String, ByVal RegioneRaggruppamento As String) As Boolean
        Dim cn As OleDbConnection

        RegionePadre = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' And Raggruppamento = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@RegioneRaggruppamento", RegioneRaggruppamento)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RegionePadre = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub LeggiNome(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' And Nome = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", Nome)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            Leggi(StringaConnessione)
        End If
        myPOSTreader.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' And CodiceRegione = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", CodiceRegione)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            NonInVisualizzazione = campodb(myPOSTreader.Item("NomeConiuge")) ' NomeConiuge
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))


            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            Attenzione = campodb(myPOSTreader.Item("Attenzione"))
            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            PARTITAIVA = myPOSTreader.Item("PARTITAIVA")
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))
            FuoriRegione = myPOSTreader.Item("FuoriRegione")


            CodiceCig = campodb(myPOSTreader.Item("CodiceCig"))
            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb(myPOSTreader.Item("NumeroControlloCliente"))

            ImportoAZero = campodbn(myPOSTreader.Item("ImportoAZero"))
            ImportoSconto = campodbn(myPOSTreader.Item("ImportoSconto"))
            Periodo = campodb(myPOSTreader.Item("Periodo"))

            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Raggruppamento = campodb(myPOSTreader.Item("Raggruppamento"))
            SeparaInFattura = myPOSTreader.Item("SeparaInFattura")
            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            MastroCliente = myPOSTreader.Item("MastroCliente")
            ContoCliente = myPOSTreader.Item("ContoCliente")
            SottoContoCliente = myPOSTreader.Item("SottoContoCliente")
            RotturaCserv = campodb(myPOSTreader.Item("RotturaCserv"))

            Vincolo = campodb(myPOSTreader.Item("Vincolo"))

            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))
            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))


            IdDocumento = campodb(myPOSTreader.Item("IdDocumento"))
            RiferimentoAmministrazione = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            NumeroFatturaDDT = campodbn(myPOSTreader.Item("NumeroFatturaDDT"))
            Note = campodb(myPOSTreader.Item("Note"))
            RESIDENZATELEFONO3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))

            RecapitoNome = campodb(myPOSTreader.Item("RecapitoNome"))
            RecapitoIndirizzo = campodb(myPOSTreader.Item("RecapitoIndirizzo"))
            RESIDENZATELEFONO4 = campodb(myPOSTreader.Item("RESIDENZATELEFONO4"))
            EGo = campodb(myPOSTreader.Item("EGo"))

            NumItem = campodb(myPOSTreader.Item("NumItem"))
            CodiceCommessaConvezione = campodb(myPOSTreader.Item("CodiceCommessaConvezione"))
            PEC = campodb(myPOSTreader.Item("PEC"))

            IdDocumentoData = campodb(myPOSTreader.Item("IdDocumentoData"))

            RaggruppaInExport = campodbn(myPOSTreader.Item("RaggruppaInExport"))

            AnnoFatturazione = campodbn(myPOSTreader.Item("AnnoFatturazione"))
            MeseFatturazione = campodbn(myPOSTreader.Item("MeseFatturazione"))

            DescrizioneXML = campodb(myPOSTreader.Item("DescrizioneXML"))

            NonInUso = campodb(myPOSTreader.Item("NonInUso"))

            RaggruppaInElaborazione = campodbn(myPOSTreader.Item("RaggruppaInElaborazione"))
        End If
        cn.Close()




    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' Order by NOME")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceRegione", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("CodiceRegione")
            myriga(1) = myPOSTreader.Item("Nome")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Function MaxRegione(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection

        MaxRegione = 0

        Try

            cn = New Data.OleDb.OleDbConnection(StringaConnessione)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select max(CAST(codiceregione AS Int)) as Max from AnagraficaComune Where TIPOLOGIA = 'R'")
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                MaxRegione = campodbn(myPOSTreader.Item("Max")) + 1
            End If
            myPOSTreader.Close()
            cn.Close()
        Catch ex As Exception

        End Try

    End Function

    Sub DecodificaRegione(ByVal StringaConnessione As String, ByVal CodiceRegione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune Where TIPOLOGIA = 'R' and codiceregione = '" & CodiceRegione & "'")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Nome = myPOSTreader.Item("Nome")
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select Nome,NomeConiuge,CodiceRegione,(Select top 1 Importo from IMPORTOREGIONI where IMPORTOREGIONI.[CODICEREGIONE] = AnagraficaComune.CodiceRegione Order By Data Desc) as ImportoUsl,(Select top 1 TipoRetta from IMPORTOREGIONI where IMPORTOREGIONI.[CODICEREGIONE] = AnagraficaComune.CodiceRegione Order By Data Desc) as MyTipoRetta from AnagraficaComune Where TIPOLOGIA = 'R' And (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N') Order By Nome, ImportoUsl")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim AppoggioCom As String

            AppoggioCom = campodb(myPOSTreader.Item("NomeConiuge"))
            If Trim(AppoggioCom) <> "" Then
                AppoggioCom = " (" & AppoggioCom & ")"
            End If

            If campodb(myPOSTreader.Item("MyTipoRetta")) = "" Then
                appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")) & AppoggioCom & " - " & Format(campodbn(myPOSTreader.Item("ImportoUsl")), "#,##0.00") & " �")
            Else
                appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")) & AppoggioCom)
            End If
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceRegione")
        Loop
        myPOSTreader.Close()
        cn.Close()
        

    End Sub


    Sub UpDateDropBoxCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select Nome,CodiceRegione,(Select top 1 Importo from IMPORTOREGIONI where IMPORTOREGIONI.[CODICEREGIONE] = AnagraficaComune.CodiceRegione Order By Data Desc) as ImportoUsl from AnagraficaComune Where TIPOLOGIA = 'R' And (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N') Order By Nome, ImportoUsl")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            appoggio.Items.Add(campodb(myPOSTreader.Item("CodiceRegione")) & " " & campodb(myPOSTreader.Item("Nome")) & " - " & Format(campodbn(myPOSTreader.Item("ImportoUsl")), "#,##0.00") & " �")
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceRegione")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
