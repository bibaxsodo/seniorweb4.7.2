﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_DatiOspiteParenteCentroServizio
    Public CodiceOspite As Long
    Public CodiceParente As Long
    Public CentroServizio As String
    Public TipoOperazione As String
    Public AliquotaIva As String
    Public Anticipata As String
    Public ModalitaPagamento As String
    Public Compensazione As String
    Public Settimana As String
    Public TipoAddebito1 As String
    Public TipoAddebito2 As String
    Public TipoAddebito3 As String
    Public TipoAddebito4 As String
    Public NonCalcoloComune As Integer


    Public Sub Pulisci()
        CodiceOspite = 0
        CodiceParente = 0
        CentroServizio = ""
        TipoOperazione = ""
        AliquotaIva = ""
        Anticipata = ""
        ModalitaPagamento = ""
        Compensazione = ""
        Settimana = ""
        TipoAddebito1 = ""
        TipoAddebito2 = ""
        TipoAddebito3 = ""
        TipoAddebito4 = ""
        NonCalcoloComune = 0
    End Sub
    Function campodbn(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DatiOspiteParenteCentroServizio where " & _
                           "CodiceOspite = ? And " & _
                           "CentroServizio = ? And CodiceParente  = ?")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            AliquotaIva = campodb(myPOSTreader.Item("AliquotaIva"))
            Anticipata = campodb(myPOSTreader.Item("Anticipata"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))

            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            Settimana = campodb(myPOSTreader.Item("Settimana"))

            TipoAddebito1 = campodb(myPOSTreader.Item("TipoAddebito1"))
            TipoAddebito2 = campodb(myPOSTreader.Item("TipoAddebito2"))
            TipoAddebito3 = campodb(myPOSTreader.Item("TipoAddebito3"))
            TipoAddebito4 = campodb(myPOSTreader.Item("TipoAddebito4"))

            NonCalcoloComune = campodbn(myPOSTreader.Item("NonCalcoloComune"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DatiOspiteParenteCentroServizio where " & _
                           "CodiceOspite = ? And " & _
                           "CentroServizio = ? And CodiceParente  = ?")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "update DatiOspiteParenteCentroServizio set TipoOperazione = ?, AliquotaIva = ?,Anticipata = ?,ModalitaPagamento = ?,Compensazione = ?,Settimana = ?, TipoAddebito1 = ?, TipoAddebito2 = ?, TipoAddebito3 = ?, TipoAddebito4  =?,NonCalcoloComune =?  where CodiceOspite = ? And CentroServizio = ? And CodiceParente = ?"
        Else
            MySql = "INSERT INTO DatiOspiteParenteCentroServizio (TipoOperazione , AliquotaIva,Anticipata,ModalitaPagamento,Compensazione,Settimana,TipoAddebito1,TipoAddebito2,TipoAddebito3,TipoAddebito4,NonCalcoloComune,CodiceOspite  ,CentroServizio, CodiceParente ) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        End If
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
        cmd1.Parameters.AddWithValue("@AliquotaIva", AliquotaIva)

        cmd1.Parameters.AddWithValue("@Anticipata", Anticipata)
        cmd1.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd1.Parameters.AddWithValue("@Compensazione", Compensazione)
        cmd1.Parameters.AddWithValue("@Settimana", Settimana)
        cmd1.Parameters.AddWithValue("@TipoAddebito1", TipoAddebito1)
        cmd1.Parameters.AddWithValue("@TipoAddebito2", TipoAddebito2)
        cmd1.Parameters.AddWithValue("@TipoAddebito3", TipoAddebito3)
        cmd1.Parameters.AddWithValue("@TipoAddebito4", TipoAddebito4)
        cmd1.Parameters.AddWithValue("@NonCalcoloComune", NonCalcoloComune)




        cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd1.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()


        cn.Close()
    End Sub


End Class


