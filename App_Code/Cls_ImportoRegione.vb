Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ImportoRegione
    Public Codice As String
    Public Data As Date
    Public Importo As Double
    Public Tipo As String
    Public TipoRetta As String
    Public Utente As String
    Public DataAggiornamento As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOREGIONI where CODICEREGIONE = '" & Codice & "'")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Cancella(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOREGIONI where CODICEREGIONE = '" & Codice & "' And Data = ?")
        cmd.Parameters.AddWithValue("@DataValidita", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Trim(TipoRetta) = "" Then
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Codice & "' AND Data = ?")
        Else
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Codice & "' AND Data = ? And TipoRetta = '" & TipoRetta & "'")
        End If
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE IMPORTOREGIONI SET IMPORTO = ?,TIPOIMPORTO=?,TipoRetta =?,Utente = ?,DataAggiornamento = ? " & _
                    " WHERE  CODICEREGIONE = '" & Codice & "' AND Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Data", Data)

            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO IMPORTOREGIONI (CODICEREGIONE,Data,IMPORTO,TIPOIMPORTO,TipoRetta,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CODICEREGIONE", Codice)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal Regione As String, ByVal TipoImporto As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If TipoImporto = "" Then
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' Order by Data Desc")
        Else
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' And TIPORETTA = '" & TipoImporto & "' Order by Data Desc")
        End If

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("CODICEREGIONE"))
            Importo = Math.Round(CDbl(campodbN(myPOSTreader.Item("Importo"))), 2)
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub



    Public Sub LeggiAData(ByVal StringaConnessione As String, ByVal Regione As String, ByVal TipoImporto As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If TipoImporto = "" Then
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' And Data <= ? Order by Data Desc")
        Else
            cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' And TIPORETTA = '" & TipoImporto & "' And Data <= ? Order by Data Desc")
        End If

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", Data)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("CODICEREGIONE"))
            Importo = Math.Round(CDbl(campodbN(myPOSTreader.Item("Importo"))), 2)
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub UltimaDataSenzaRegione(ByVal StringaConnessione As String, ByVal TipoImporto As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        
        cmd.CommandText = ("select * from IMPORTOREGIONI where  TIPORETTA = '" & TipoImporto & "' Order by Data Desc")
        
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("CODICEREGIONE"))
            Importo = Math.Round(CDbl(campodbN(myPOSTreader.Item("Importo"))), 2)
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Function UltimiImporti(ByVal StringaConnessione As String, ByVal Regione As String) As String
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' Order by Data Desc")
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Data = campodb(myPOSTreader.Item("Data"))
        End If
        myPOSTreader.Close()

        Dim cmdT As New OleDbCommand()
        cmdT.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & Regione & "' And Data = ?")
        cmdT.Connection = cn
        cmdT.Parameters.AddWithValue("@Data", Data)

        UltimiImporti = ""
        Dim RdRead As OleDbDataReader = cmdT.ExecuteReader()
        Do While RdRead.Read
            If UltimiImporti <> "" Then
                UltimiImporti = UltimiImporti & " - "
            End If
            UltimiImporti = UltimiImporti & campodb(RdRead.Item("Importo"))
        Loop
        RdRead.Close()



        cn.Close()

    End Function
    Sub loaddati(ByVal StringaConnessione As String, ByVal CODICEREGIONE As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOREGIONI where CODICEREGIONE = '" & CODICEREGIONE & "' ORDER BY DATA,TIPORETTA  ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            myriga(1) = Format(Math.Round(CDbl(campodbN(myPOSTreader.Item("Importo"))), 2), "#,##0.00")
            myriga(2) = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            myriga(3) = campodb(myPOSTreader.Item("TipoRetta"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = ""
            myriga(1) = 0
            myriga(2) = "G"
            myriga(3) = ""
            Tabella.Rows.Add(myriga)
        End If

        cn.Close()
    End Sub
End Class
