Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipoPagamento
    Public Codice As String
    Public Descrizione As String
    Public Scadenze As Long    
    Public Tipo As String
    Public GiorniPrima As Long
    Public GiorniSeconda As Long
    Public GiorniAltre As Long
    Public Spese As Long
    Public Per As String
    Public CampoFE As String
    Public AperturaGestioneScadenze As Integer
    Public IvaImponibile As String

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoPagamento Where Codice = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", Codice)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Scadenze = campodbN(myPOSTreader.Item("Scadenze"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            GiorniPrima = campodbN(myPOSTreader.Item("GiorniPrima"))
            GiorniSeconda = campodbN(myPOSTreader.Item("GiorniSeconda"))
            GiorniAltre = campodbN(myPOSTreader.Item("GiorniAltre"))
            Spese = campodbN(myPOSTreader.Item("Spese"))
            Per = campodb(myPOSTreader.Item("Per"))
            CampoFE = campodb(myPOSTreader.Item("CampoFE"))
            AperturaGestioneScadenze = campodbN(myPOSTreader.Item("AperturaGestioneScadenze"))

            IvaImponibile = campodb(myPOSTreader.Item("IvaImponibile"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from MovimentiContabiliTesta Where CodicePagamento = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoPagamento Where Descrizione = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Call Leggi(StringaConnessione)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub UpDateDropBoxConCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoPagamento order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoPagamento order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoPagamento where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TipoPagamento (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE TipoPagamento SET " & _
                "  Descrizione  = ?," & _
                "  Scadenze  = ?," & _
                "  Tipo  = ?," & _
                "  GiorniPrima  = ?," & _
                "  GiorniSeconda  = ?," & _
                "  GiorniAltre  = ?," & _
                "  Spese = ?," & _
                "  Per  = ?, " & _
                "  CampoFE  = ?, " & _
                " AperturaGestioneScadenze = ?, " & _
                " IvaImponibile = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Scadenze", Scadenze)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@GiorniPrima", GiorniPrima)
        cmd1.Parameters.AddWithValue("@GiorniSeconda", GiorniSeconda)
        cmd1.Parameters.AddWithValue("@GiorniAltre", GiorniAltre)
        cmd1.Parameters.AddWithValue("@Spese", Spese)
        cmd1.Parameters.AddWithValue("@Per", Per)
        cmd1.Parameters.AddWithValue("@CampoFE", CampoFE)
        cmd1.Parameters.AddWithValue("@AperturaGestioneScadenze", AperturaGestioneScadenze)
        cmd1.Parameters.AddWithValue("@IvaImponibile", IvaImponibile)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()


    End Sub



    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoPagamento Where Codice = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Function MaxTipoPagamento(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoPagamento = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END)  from TipoPagamento ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function
End Class
