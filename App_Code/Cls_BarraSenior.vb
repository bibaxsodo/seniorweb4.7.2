﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_BarraSenior

    Public Function CodiceBarra(ByVal Connessione As String, ByVal Utente As String, ByVal Pagina As Page, Optional ByVal ParametriRicerca As Cls_SqlString = Nothing) As String
        Dim k As New Cls_Login
        Dim NecessitaAutoComplete As Boolean = False
        Dim NecessitaAutoCompleteDipendenti As Boolean = False

        Dim RicercaOspite As Boolean = False
        Dim RicercaDipendenti As Boolean = False
        Dim RicercaRegistrazione As Boolean = False

        Dim Gdpr As Boolean = False

        Dim PosizioneUrl as String  =""


        If Pagina.GetType.Name.ToUpper.IndexOf("GeneraleWeb_".ToUpper)  >= 0 then
            PosizioneUrl =""
        end if

        If Utente = "" Then
            CodiceBarra = "<div class=""Barra"">"
            CodiceBarra = CodiceBarra & "<table width=""100%"">"
            CodiceBarra = CodiceBarra & "<td style=""width:100px;"">"
            CodiceBarra = CodiceBarra & "<img src=""/images/senior.png"" /> "
            CodiceBarra = CodiceBarra & "</td>"
            CodiceBarra = CodiceBarra & "<td style=""text-align:left; width:60%;"">"
            CodiceBarra = CodiceBarra & "<span class=""BenvenutoText""><font size=""2"">Utente non connesso</font></span>"
            CodiceBarra = CodiceBarra & "</asp:Label>"
            CodiceBarra = CodiceBarra & "</td>"
            CodiceBarra = CodiceBarra & "<td style=""text-align:right;"">" & vbNewLine
            CodiceBarra = CodiceBarra & "<span class=""BenvenutoText"">"
            CodiceBarra = CodiceBarra & ", "
            CodiceBarra = CodiceBarra & Format(Now, "D")
            CodiceBarra = CodiceBarra & "</span>" & vbNewLine
            CodiceBarra = CodiceBarra & "</td>" & vbNewLine
            CodiceBarra = CodiceBarra & "<td style=""width:100px; text-align:right;"">" & vbNewLine
            CodiceBarra = CodiceBarra & "<a href=""/Login.aspx?Logout=TRUE"" ><span class=""LinkBarra"" style=""margin-right: 5px;"" onmouseover=""this.style.color='orange';"" onmouseout=""this.style.color='white';""><img src=""/images/logout.png"" style=""padding-top:3px;"" width=""26px""></span></a>" & vbNewLine
            CodiceBarra = CodiceBarra & "</td>"
            CodiceBarra = CodiceBarra & "</table>"
            CodiceBarra = CodiceBarra & "</div>"
            CodiceBarra = CodiceBarra & ""
            Exit Function
        End If

        'If Not IsNothing(Pagina.Session("DatiPresenti")) Then
        '    If Pagina.Session("DatiPresenti") = "SI" Then
        '        CodiceBarra = Pagina.Session("CodiceBarra")
        '        Exit Function
        '    End If
        'End If



        k.Utente = Utente
        k.ABILITAZIONI = ""
        k.LeggiSP(Connessione)


        Dim PaginaDaLog As Boolean = False
        Dim CodiceOspite As Integer = 0
        Dim CodiceParente As Integer = 0
        Dim NumeroRegistrazione As Integer = 0
        Dim Struttura As String = ""
        Dim CentroServizio As String = ""
        Dim CODIFICA As String = ""
        If Pagina.ClientQueryString.ToUpper.IndexOf("CODICEOSPITE=") >= 0 Then
            PaginaDaLog = True
            Dim PosizioneCodOspite As Integer = 0
            Dim FineOAnd As Integer = 0
            PosizioneCodOspite = Pagina.ClientQueryString.ToUpper.IndexOf("CODICEOSPITE=")

            FineOAnd = Pagina.ClientQueryString.ToUpper.IndexOf("&", PosizioneCodOspite)
            If FineOAnd <= 0 Then
                FineOAnd = Pagina.ClientQueryString.ToUpper.Length
            End If

            CodiceOspite = Val(Mid(Pagina.ClientQueryString, PosizioneCodOspite + 1, FineOAnd - PosizioneCodOspite).ToUpper.Replace("CODICEOSPITE=", ""))
            If Pagina.ClientQueryString.ToUpper.IndexOf("CODICEPARENTE=") >= 0 Then
                Dim PosizioneCodParente As Integer = 0
                PosizioneCodParente = Pagina.ClientQueryString.ToUpper.IndexOf("CODICEPARENTE=")

                FineOAnd = Pagina.ClientQueryString.ToUpper.IndexOf("&", PosizioneCodParente)
                If FineOAnd <= 0 Then
                    FineOAnd = Pagina.ClientQueryString.ToUpper.Length
                End If

                CodiceParente = Val(Mid(Pagina.ClientQueryString, PosizioneCodParente + 1, FineOAnd - PosizioneCodParente).ToUpper.Replace("CODICEPARENTE=", ""))
            End If
        End If
        If Pagina.GetType.Name.ToUpper.IndexOf("GeneraleWeb_".ToUpper) >= 0 Then
            If Not IsNothing(Pagina.Session("NumeroRegistrazione")) Then
                If Pagina.Session("NumeroRegistrazione") > 0 Then
                    PaginaDaLog = True
                    NumeroRegistrazione = Pagina.Session("NumeroRegistrazione")
                End If
            Else
                NumeroRegistrazione = 0
            End If

            If Pagina.ClientQueryString.ToUpper.IndexOf("NUMEROREGISTRAZIONE=") >= 0 Then
                PaginaDaLog = True
                Dim PosizioneNumeroRegistrazione As Integer = 0
                PosizioneNumeroRegistrazione = Pagina.ClientQueryString.ToUpper.IndexOf("NUMEROREGISTRAZIONE=")
                Dim FineOAnd As Integer = 0
                FineOAnd = Pagina.ClientQueryString.ToUpper.IndexOf("&", PosizioneNumeroRegistrazione)
                If FineOAnd <= 0 Then
                    FineOAnd = Pagina.ClientQueryString.ToUpper.Length
                End If

                NumeroRegistrazione = Val(Mid(Pagina.ClientQueryString, PosizioneNumeroRegistrazione + 1, FineOAnd - PosizioneNumeroRegistrazione).ToUpper.Replace("NUMEROREGISTRAZIONE=", ""))
            End If
        End If
        If PaginaDaLog = False Then
            Try
                If Pagina.Session("CODICEOSPITE") > 0 Then
                    CodiceOspite = Pagina.Session("CODICEOSPITE")
                    PaginaDaLog = True
                End If
            Catch ex As Exception

            End Try
            Try
                If Pagina.Session("CODICEPARENTE") > 0 Then
                    CodiceOspite = Pagina.Session("CODICEPARENTE")
                    PaginaDaLog = True
                End If
            Catch ex As Exception

            End Try
        End If

        If PaginaDaLog = True Then

            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Connessione, k.Cliente, Utente, Pagina.Session("UTENTEIMPER"), Pagina.GetType.Name.ToUpper, CodiceOspite, CodiceParente, Struttura, CentroServizio, NumeroRegistrazione, CODIFICA, "V", "", "")
        End If

        Dim PulisciUtente As String = Utente
        Dim i As Integer
        For i = 1 To 20
            PulisciUtente = PulisciUtente.Replace("<" & i & ">", "")
        Next

        If Pagina.GetType.Name.ToUpper.IndexOf("ospitiweb_".ToUpper) >= 0 Then
            If k.ABILITAZIONI.IndexOf("<NONIMPORTI>") > 0 Then
                If Pagina.GetType.Name.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_Elenco_DatiPagamento_aspx".ToUpper Then
                    Pagina.Response.Redirect("Menu_Ospiti.aspx")
                    Exit Function
                End If
            End If

            If k.ABILITAZIONI.IndexOf("<NONTABELLE>") > 0 Then
                If Pagina.GetType.Name.ToUpper = "ospitiweb_Menu_Tabelle_aspx".ToUpper Then
                    Pagina.Response.Redirect("Menu_Ospiti.aspx")
                    Exit Function
                End If
            End If

            If k.ABILITAZIONI.IndexOf("<NONFATTURAZIONE>") > 0 Then
                If Pagina.GetType.Name.ToUpper = "ospitiweb_RiCalcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_EmissioneRette_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_DepositoCauzionaleCaparra_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_CreaDocumentoRetta_aspx".ToUpper Or _
                   Pagina.GetType.Name.ToUpper = "ospitiweb_Menu_Strumenti_aspx".ToUpper Or Pagina.GetType.Name.ToUpper.IndexOf("GeneraleWeb".ToUpper) >= 0 Then
                    Pagina.Response.Redirect("Menu_Ospiti.aspx")
                    Exit Function
                End If
            End If
        End If


        CodiceBarra = "<div class=""Barra""  width=""100%"">"
        CodiceBarra = CodiceBarra & "<table width=""100%"">"
        CodiceBarra = CodiceBarra & "<td style=""width:100px;"">"
        '<a href="#"  onclick="$('#TastiSenior').toggle(100);">Apri</a>
        'CodiceBarra = CodiceBarra & "<a href=""#""  onmouseover=""$('#TastiSenior').toggle(100);""><img src=""/images/senior.png"" /> </a> "
        CodiceBarra = CodiceBarra & "<a href=""#""  onmouseover=""BarraSeniorElencoProgrammi();""><img src=""/images/senior.png"" /> </a> "
        CodiceBarra = CodiceBarra & "</td>"
        CodiceBarra = CodiceBarra & "<td id=""barraseniorwidth"" style=""text-align:left; width:40%;"">"
        CodiceBarra = CodiceBarra & "</td>"


        If Pagina.GetType.Name.ToUpper.IndexOf("TurniWeb_".ToUpper) >= 0 Then

            NecessitaAutoCompleteDipendenti = True
            RicercaDipendenti = True
            CodiceBarra = CodiceBarra & "<td style=""width:150px;"">"
            CodiceBarra = CodiceBarra & "<ul id=""menu""> "
            CodiceBarra = CodiceBarra & "<li ><a href=""#"" id=""MenuOspitiLnk""  style=""background: #007DC4; padding: 0px; color:White;"">Dipendenti</a>"
            CodiceBarra = CodiceBarra & "<ul class=""sub-menu"" id=""menuolga"">"
            CodiceBarra = CodiceBarra & "<li  class=""aQli"">"
            CodiceBarra = CodiceBarra & "&nbsp;<input type=""text"" id=""Txt_NOMEDIPENDENTEBARRA"" style=""width:300px"" />&nbsp"
            CodiceBarra = CodiceBarra & "<input type=""button""  style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('CaricaDipendente.aspx?DIPENDENTE=' + $('#Txt_NOMEDIPENDENTEBARRA').val());"" />"
            CodiceBarra = CodiceBarra & "</li>"
            CodiceBarra = CodiceBarra & "</ul>"
            CodiceBarra = CodiceBarra & "</li>"
            CodiceBarra = CodiceBarra & "</ul>"
            CodiceBarra = CodiceBarra & "</td>"
        End If

        If Pagina.GetType.Name.ToUpper.IndexOf("OspitiWeb_".ToUpper) >= 0 Then
            NecessitaAutoComplete = True
            RicercaOspite = True

            CodiceBarra = CodiceBarra & "<td style=""width:150px;"">"
            CodiceBarra = CodiceBarra & "<ul id=""menu"" >"
            CodiceBarra = CodiceBarra & "<li><a href=""#"" onmouseover=""BarraSeniorElencoOspite();"" id=""MenuOspitiLnk"" style=""background: #007DC4; padding: 0px; color:White;"">Ospiti</a>"
            CodiceBarra = CodiceBarra & "<ul class=""sub-menu"" id=""menuolga"">"
            CodiceBarra = CodiceBarra & "<li  class=""aQli"">"
            CodiceBarra = CodiceBarra & "&nbsp;<input type=""text"" id=""Txt_NOMEOSPITEBARRA"" style=""width:300px"" />&nbsp"



            If Not Pagina.GetType.Name.ToUpper.IndexOf("MENU_EXPORT_ASPX") > 0 And (Pagina.GetType.Name.ToUpper.IndexOf("APIV1") > 0 Or Pagina.GetType.Name.ToUpper.IndexOf("PUSH") > 0 Or Pagina.GetType.Name.ToUpper.IndexOf("EXPORT") > 0) Then
                If Pagina.ClientQueryString.ToUpper = "TIPO=EstrattoContoDenaro".ToUpper Then
                    CodiceBarra = CodiceBarra & "<input type=""button""  style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('../caricaospite.aspx?OSPITE=' + $('#Txt_NOMEOSPITEBARRA').val() + '&PAGINA=ospitiweb_EstrattoContoDenaro_aspx');"" />"
                Else
                    CodiceBarra = CodiceBarra & "<input type=""button""  style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('../caricaospite.aspx?OSPITE=' + $('#Txt_NOMEOSPITEBARRA').val() + '&PAGINA=" & Pagina.GetType.Name & "&PARAMETRI=" & Pagina.ClientQueryString.ToUpper & "');"" />"
                End If
            Else
                If Pagina.ClientQueryString.ToUpper = "TIPO=EstrattoContoDenaro".ToUpper Then
                    CodiceBarra = CodiceBarra & "<input type=""button""  style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('caricaospite.aspx?OSPITE=' + $('#Txt_NOMEOSPITEBARRA').val() + '&PAGINA=ospitiweb_EstrattoContoDenaro_aspx');"" />"
                Else
                    CodiceBarra = CodiceBarra & "<input type=""button""  style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('caricaospite.aspx?OSPITE=' + $('#Txt_NOMEOSPITEBARRA').val() + '&PAGINA=" & Pagina.GetType.Name & "&PARAMETRI=" & Pagina.ClientQueryString.ToUpper & "');"" />"
                End If
            End If
            CodiceBarra = CodiceBarra & "</li>"


            InserisciOspiteINRicerca(Connessione, Utente, ParametriRicerca, Pagina.GetType.Name.ToUpper, Pagina)

            CodiceBarra = CodiceBarra & "<div id=""BarraSeniorElencoOspite""></div>"


            CodiceBarra = CodiceBarra & ""


            CodiceBarra = CodiceBarra & "</ul>"
            CodiceBarra = CodiceBarra & "</li>"
            CodiceBarra = CodiceBarra & "</ul>"
            CodiceBarra = CodiceBarra & "</td>"

        End If



        If Pagina.GetType.Name.ToUpper.IndexOf("GeneraleWeb_".ToUpper) >= 0 Then
            CodiceBarra = CodiceBarra & "<td style=""width:150px;"">" & vbNewLine
            CodiceBarra = CodiceBarra & "<ul id=""menu"" >" & vbNewLine
            CodiceBarra = CodiceBarra & "<li><a href=""#"" id=""MenuRegistrazioniLnk""  onmouseover=""BarraSeniorElencoRegistrazione();"" style=""background: #007DC4; padding: 0px;  color:White;"">Registrazioni</a>" & vbNewLine
            CodiceBarra = CodiceBarra & "<ul class=""sub-menu"" id=""menuolga"">" & vbNewLine

            CodiceBarra = CodiceBarra & "<li  class=""aQli"">" & vbNewLine
            CodiceBarra = CodiceBarra & "&nbsp;<input type=""text"" id=""Txt_NumeroRegistrazioneBarra"" style=""width:100px"" />&nbsp;" & vbNewLine
            CodiceBarra = CodiceBarra & "<input type=""button"" style=""height:30px; width:70;"" value=""CERCA"" onClick=""window.location.replace('caricaregistrazione.aspx?REGISTRAZIONE=' + $('#Txt_NumeroRegistrazioneBarra').val() + '&PAGINA=" & Pagina.GetType.Name & "');"" />" & vbNewLine
            CodiceBarra = CodiceBarra & "</li>" & vbNewLine

            Try
                CodiceBarra = CodiceBarra & InserisciRegistrazioneINRicerca(Connessione, Utente, ParametriRicerca, Pagina.GetType.Name.ToUpper, Pagina)
            Catch ex As Exception

            End Try
            CodiceBarra = CodiceBarra & "<div id=""BarraSeniorElencoRegistrazione""></div>"

            CodiceBarra = CodiceBarra & "</ul>" & vbNewLine
            CodiceBarra = CodiceBarra & "</li>" & vbNewLine
            CodiceBarra = CodiceBarra & "</ul>" & vbNewLine
            CodiceBarra = CodiceBarra & "</td>" & vbNewLine
            RicercaRegistrazione = True
        End If

        'CodiceBarra = CodiceBarra & "<td style=""width:100px;"" id=""IdMailLuminosa"">" & vbNewLine
        'If k.RagioneSociale <> "" Then
        ' CodiceBarra = CodiceBarra & Scadenze(Connessione, Utente)
        'End If
        'CodiceBarra = CodiceBarra & "</td>" & vbNewLine

        'If k.RagioneSociale <> "" And Pagina.GetType.Name.ToUpper.IndexOf("Menu_Societa".ToUpper) < 0 Then
        '    CodiceBarra = CodiceBarra & "<td style=""width:100px;"">" & vbNewLine
        '    CodiceBarra = CodiceBarra & "<span class=""BenvenutoText"">" & k.RagioneSociale & "</span>" & vbNewLine
        '    CodiceBarra = CodiceBarra & "</td>" & vbNewLine
        'Else
        '    CodiceBarra = CodiceBarra & "<td style=""width:100px;"">" & vbNewLine
        '    CodiceBarra = CodiceBarra & "<span class=""BenvenutoText""></span>" & vbNewLine
        '    CodiceBarra = CodiceBarra & "</td>" & vbNewLine
        'End If

        CodiceBarra = CodiceBarra & "<td style=""text-align:right;"">" & vbNewLine
        CodiceBarra = CodiceBarra & "<span class=""BenvenutoText"">"
        CodiceBarra = CodiceBarra & System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(PulisciUtente) & ", "
        CodiceBarra = CodiceBarra & Format(Now, "D") & " - "
        CodiceBarra = CodiceBarra & "<b>" & k.RagioneSociale & "</b>"
        CodiceBarra = CodiceBarra & "</span>" & vbNewLine
        CodiceBarra = CodiceBarra & "</td>" & vbNewLine


        'CodiceBarra = CodiceBarra & "<td style=""width:200px;"">" & vbNewLine
        'If k.RagioneSociale <> "" And Pagina.GetType.Name.ToUpper.IndexOf("Menu_Societa".ToUpper) < 0 Then
        '    CodiceBarra = CodiceBarra & "<a href=""/Strumenti.aspx?URL=" & Pagina.Request.Url.AbsoluteUri & """ ><span class=""LinkBarra"" onmouseover=""this.style.color='orange';"" onmouseout=""this.style.color='white';"">IMPOSTAZIONI</span></a>" & vbNewLine
        'End If
        'CodiceBarra = CodiceBarra & "</td>" & vbNewLine
        'CodiceBarra = CodiceBarra & "<td style=""width:100px;"">" & vbNewLine
        'CodiceBarra = CodiceBarra & "<a href=""https://senior.e-personam.com/manuale/"" target=""_blank"" ><span class=""LinkBarra"" onmouseover=""this.style.color='orange';"" onmouseout=""this.style.color='white';"">?</span></a>" & vbNewLine
        'CodiceBarra = CodiceBarra & "</td>" & vbNewLine
        CodiceBarra = CodiceBarra & "<td style=""width:100px; text-align:right;"">" & vbNewLine
        CodiceBarra = CodiceBarra & "<a href=""/Login.aspx?Logout=TRUE"" ><span class=""LinkBarra"" style=""margin-right: 5px;"" onmouseover=""this.style.color='orange';"" onmouseout=""this.style.color='white';""><img src=""/images/logout.png"" style=""padding-top:3px;"" width=""26px""></span></a>" & vbNewLine
        CodiceBarra = CodiceBarra & "</td>" & vbNewLine
        CodiceBarra = CodiceBarra & "</table>" & vbNewLine
        CodiceBarra = CodiceBarra & "</div>" & vbNewLine
        If k.RagioneSociale <> "" Then
            REM CodiceBarra = CodiceBarra & "<div id=""IdMessaggiScadenze"" class=""MESSAGGIO""  style=""position:absolute; text-indent:20px; top:37px;  visibility:collapse; width:400px;height:300px;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;border:1px solid #000000;background-color:#FFFFFF;-webkit-box-shadow: #A8A8A8 8px 8px 8px;-moz-box-shadow: #A8A8A8 8px 8px 8px; box-shadow: #A8A8A8 8px 8px 8px;""><br/>" & ListaScadenze(Connessione, Utente) & "</div>" & vbNewLine
        End If


        CodiceBarra = CodiceBarra & "<div id=""IdTimeOut"" style=""position:absolute; left:45%; top:20%;  visibility:collapse; width:400px;height:200px;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;border:1px solid #000000;background-color:#FF0000;-webkit-box-shadow: #A8A8A8 8px 8px 8px;-moz-box-shadow: #A8A8A8 8px 8px 8px; box-shadow: #A8A8A8 8px 8px 8px;  ""><font color=white>Warning!!</font><br/><br/><font color=yellow>Se nei prossimi 3 minuti, non fai nessuna operazione, il sistema tornerà alla pagina di login. Salva i tuoi dati.</font><br/><br /><a href=""#"" onclick=""document.getElementById('IdTimeOut').style.visibility='collapse';"">Chiudi</a></div>" & vbNewLine


        CodiceBarra = CodiceBarra & vbNewLine & "<script type=""text/javascript"">" & vbNewLine & "$(document).ready(function() {" & vbNewLine


        CodiceBarra = CodiceBarra & "  if (window.innerWidth < 1100) { " & vbNewLine & "   $(""#barraseniorwidth"").css(""width"",""10%""); " & vbNewLine & " }  "




        CodiceBarra = CodiceBarra & "$('body').click(function() {" & vbNewLine
        CodiceBarra = CodiceBarra & "$(""#TastiSenior"").css(""display"", ""none"");" & vbNewLine
        CodiceBarra = CodiceBarra & "});" & vbNewLine

        If NecessitaAutoComplete Then
            If Pagina.GetType.Name.ToUpper.IndexOf("APIV1") > 0 Or Pagina.GetType.Name.ToUpper.IndexOf("PUSH") > 0 Or (Pagina.GetType.Name.ToUpper.IndexOf("EXPORT") > 0 And Pagina.GetType.Name.ToUpper.IndexOf("EXPORT_ASPX") < 0) Then
                CodiceBarra = CodiceBarra & " $(""#Txt_NOMEOSPITEBARRA"").autocomplete('../AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Utente & "&CSERV=2', {delay:5,minChars:3 });" & vbNewLine
            Else
                CodiceBarra = CodiceBarra & " $(""#Txt_NOMEOSPITEBARRA"").autocomplete('AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Utente & "&CSERV=2', {delay:5,minChars:3 });" & vbNewLine
            End If

            REM (CodiceBarra = CodiceBarra & " $(""#Txt_NOMEOSPITEBARRA"").change(function() { var appoggio = $(""#Txt_NOMEOSPITEBARRA"").val(); appoggio = appoggio.replace(""<b>"",""""); appoggio = appoggio.replace(""</b>"","""");  $(""#Txt_NOMEOSPITEBARRA"").val(appoggio);  alert(appoggio);} );")


            CodiceBarra = CodiceBarra & " $(""#Txt_NOMEOSPITEBARRA"").focus(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & " $(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & " });" & vbNewLine

            CodiceBarra = CodiceBarra & " $(""#TXT_NOTEOSPITE"").focus(function() { " & vbNewLine
            CodiceBarra = CodiceBarra & " $(""#menunote"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & " });" & vbNewLine

            CodiceBarra = CodiceBarra & "$('body').click(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#TastiSenior"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menunote"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine


            CodiceBarra = CodiceBarra & "$(""#MenuNote"").mouseover(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menunote"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine

            CodiceBarra = CodiceBarra & "$(""#MenuOspitiLnk"").mouseover(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menunote"").css(""display"", ""none"");" & vbNewLine
            If RicercaOspite Then
                CodiceBarra = CodiceBarra & " $(""#Txt_NOMEOSPITEBARRA"").focus();" & vbNewLine
            End If
            CodiceBarra = CodiceBarra & "});" & vbNewLine



            'CodiceBarra = CodiceBarra & "});" & vbNewLine
        End If

        Dim msgSession As String
        msgSession = "Warning: Se nei prossimi 3 minuti, non fai nessuna operazione, il sistema tornerà alla pagina di login. Salva i tuoi dati."


        Dim int_MilliSecondsTimeReminder As Integer
        Dim int_MilliSecondsTimeOut As Integer




        int_MilliSecondsTimeReminder = ((Pagina.Session.Timeout - 3) * 50 * 1000)


        int_MilliSecondsTimeOut = ((Pagina.Session.Timeout) * 50 * 1000)

        If Pagina.GetType.Name.ToUpper <> "ospitiweb_MovimentiDiurniCentroServizio_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "ospitiweb_ApiV1Epersonam_ImportDomiciliareV1_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "ospitiweb_StampaFattureProva_aspx".ToUpper And _
           Pagina.GetType.Name.ToUpper <> "ospitiweb_RiCalcolo_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "ospitiweb_Calcolo_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "ospitiweb_EmissioneRette_aspx".ToUpper And _
           Pagina.GetType.Name.ToUpper <> "ospitiweb_VerificaRette_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "ospitiweb_StatisticaDomiciliari_aspx".ToUpper And _
           Pagina.GetType.Name.ToUpper <> "ospitiweb_AddebitiAccreditiMultipli_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "generaleweb_Spesometro_aspx".ToUpper And _
           Pagina.GetType.Name.ToUpper <> "MailingList_InvioMail_Step2_aspx" And Pagina.GetType.Name.ToUpper <> "generaleweb_Bilancio_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "generaleweb_ChiusureContabili_aspx".ToUpper And _
           Pagina.GetType.Name.ToUpper <> "ospitiweb_StampaGiornaleContabilita_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "generaleweb_ImportXML_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "Appalti_importdomiciliare_aspx".ToUpper Then
            CodiceBarra = CodiceBarra & " var myTimeReminder, myTimeOut; " & vbNewLine &
                " clearTimeout(myTimeReminder); " & vbNewLine &
                " clearTimeout(myTimeOut); " & vbNewLine &
                "var sessionTimeReminder = " & int_MilliSecondsTimeReminder.ToString() & ";" & vbNewLine &
                "var sessionTimeout = " & int_MilliSecondsTimeOut.ToString() & ";" & vbNewLine &
                "function doReminder(){ alert('" + msgSession & "'); }" & vbNewLine &
                "function doRedirect(){ window.location.href='/Login.aspx'; }" & vbNewLine &
                " window.myTimeReminder=setTimeout('document.getElementById(""IdTimeOut"").style.visibility= ""visible"";',sessionTimeReminder); window.myTimeOut=setTimeout('window.location.href=""/Login.aspx"";',sessionTimeout); " & vbNewLine
        End If


        If NecessitaAutoCompleteDipendenti Then
            CodiceBarra = CodiceBarra & " $(""#Txt_NOMEDIPENDENTEBARRA"").autocomplete('AutocompleteDipendenti.ashx?Utente=" & Utente & "', {delay:5,minChars:3});" & vbNewLine

            CodiceBarra = CodiceBarra & " $(""#Txt_NOMEDIPENDENTEBARRA"").focus(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & " $(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & " });" & vbNewLine
            CodiceBarra = CodiceBarra & "$('body').click(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#TastiSenior"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine

            CodiceBarra = CodiceBarra & "$(""#MenuOspitiLnk"").mouseover(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""block"");" & vbNewLine

            CodiceBarra = CodiceBarra & "});" & vbNewLine
        End If



        CodiceBarra = CodiceBarra & "$("".aQliNote"").mouseover(function(){ " & vbNewLine
        CodiceBarra = CodiceBarra & " BarraSeniorElencoOspite(); " & vbNewLine
        CodiceBarra = CodiceBarra & "$(this).css({ ""background-color"" : ""transparent"", ""cursor"" : ""default""  });    " & vbNewLine
        If RicercaOspite Then
            CodiceBarra = CodiceBarra & " $(""#TXT_NOTEOSPITE"").focus();" & vbNewLine
        End If
        CodiceBarra = CodiceBarra & "});" & vbNewLine


        'CodiceBarra = CodiceBarra & "$("".aQli"").mouseover(function(){" & vbNewLine
        'CodiceBarra = CodiceBarra & "$(this).css({ ""background-color"" : ""transparent"", ""cursor"" : ""default""  });    " & 
        'If RicercaDipendenti Then
        '    CodiceBarra = CodiceBarra & " $(""#Txt_NOMEDIPENDENTEBARRA"").focus();" & vbNewLine
        'End If
        If RicercaRegistrazione Then

            CodiceBarra = CodiceBarra & " $(""#Txt_NumeroRegistrazioneBarra"").focus(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & " $(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & " });" & vbNewLine
            CodiceBarra = CodiceBarra & "$('body').click(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#TastiSenior"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine



            CodiceBarra = CodiceBarra & "$(""#MenuOspitiLnk"").mouseover(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menunote"").css(""display"", ""none"");" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine


            CodiceBarra = CodiceBarra & "$(""#MenuRegistrazioniLnk"").mouseover(function() {" & vbNewLine
            CodiceBarra = CodiceBarra & "$(""#menuolga"").css(""display"", ""block"");" & vbNewLine
            CodiceBarra = CodiceBarra & " $(""#Txt_NumeroRegistrazioneBarra"").focus();" & vbNewLine
            CodiceBarra = CodiceBarra & "});" & vbNewLine

        End If
        'CodiceBarra = CodiceBarra & "});" & vbNewLine

        CodiceBarra = CodiceBarra & "$("".aQli"").mouseout(function(){" & vbNewLine
        CodiceBarra = CodiceBarra & "$(this).css({ ""background-color"" : ""transparent"", ""cursor"" : ""default""  });  " & vbNewLine
        CodiceBarra = CodiceBarra & "});" & vbNewLine
        CodiceBarra = CodiceBarra & "});" & vbNewLine

        CodiceBarra = CodiceBarra & "    function BarraSeniorElencoProgrammi() {" & vbNewLine
        CodiceBarra = CodiceBarra & "$.ajax({" & vbNewLine
        CodiceBarra = CodiceBarra & "type: ""POST""," & vbNewLine
        CodiceBarra = CodiceBarra & "url: ""/RestService/BarraSeniorElencoProgrammi.ashx?Pagina=" & Pagina.GetType.Name.ToUpper & ""","
        CodiceBarra = CodiceBarra & "dataType: ""html""," & vbNewLine
        CodiceBarra = CodiceBarra & "success: function(risposta) {" & vbNewLine
        CodiceBarra = CodiceBarra & "$('#TastiSenior').html(risposta);" & vbNewLine
        CodiceBarra = CodiceBarra & "$('#TastiSenior').toggle(100);" & vbNewLine
        CodiceBarra = CodiceBarra & "}," & vbNewLine
        CodiceBarra = CodiceBarra & "error: function() {" & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine
        CodiceBarra = CodiceBarra & "}); " & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine


        CodiceBarra = CodiceBarra & "    function BarraSeniorElencoOspite() {" & vbNewLine
        CodiceBarra = CodiceBarra & "$.ajax({" & vbNewLine
        CodiceBarra = CodiceBarra & "type: ""POST""," & vbNewLine
        CodiceBarra = CodiceBarra & "url: ""/RestService/BarraSeniorElencoOspite.ashx?Pagina=" & Pagina.GetType.Name.ToUpper & "&ClientQueryString=" & Pagina.ClientQueryString.ToUpper & ""","
        CodiceBarra = CodiceBarra & "dataType: ""html""," & vbNewLine
        CodiceBarra = CodiceBarra & "success: function(risposta) {" & vbNewLine
        CodiceBarra = CodiceBarra & "$('#BarraSeniorElencoOspite').html(risposta);" & vbNewLine
        CodiceBarra = CodiceBarra & "}," & vbNewLine
        CodiceBarra = CodiceBarra & "error: function() {" & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine
        CodiceBarra = CodiceBarra & "}); " & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine

        'BarraSeniorElencoRegistrazione

        CodiceBarra = CodiceBarra & "    function BarraSeniorElencoRegistrazione() {" & vbNewLine
        CodiceBarra = CodiceBarra & "$.ajax({" & vbNewLine
        CodiceBarra = CodiceBarra & "type: ""POST""," & vbNewLine
        CodiceBarra = CodiceBarra & "url: ""/RestService/BarraSeniorElencoRegistrazione.ashx?Pagina=" & Pagina.GetType.Name.ToUpper & "&ClientQueryString=" & Pagina.ClientQueryString.ToUpper & ""","
        CodiceBarra = CodiceBarra & "dataType: ""html""," & vbNewLine
        CodiceBarra = CodiceBarra & "success: function(risposta) {" & vbNewLine
        CodiceBarra = CodiceBarra & "$('#BarraSeniorElencoRegistrazione').html(risposta);" & vbNewLine
        CodiceBarra = CodiceBarra & "}," & vbNewLine
        CodiceBarra = CodiceBarra & "error: function() {" & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine
        CodiceBarra = CodiceBarra & "}); " & vbNewLine
        CodiceBarra = CodiceBarra & "}" & vbNewLine
        CodiceBarra = CodiceBarra & "</script>" & vbNewLine


        CodiceBarra = CodiceBarra & "<ul class=""custom-menu"" id=""TastiSenior"">"
        CodiceBarra = CodiceBarra & "</ul>"



        ' Codice barra



        If k.GDPRAttivo = 0 Then
            CodiceBarra = CodiceBarra & "<div class=""ClsGdprBarra""><a href=""#"" onclick=""$('#IdGDPR').css('visibility', 'visible');"" class=""ClsGdpr"">ATTENZIONE: non hai ancora attivato il kit GDPR compliance.</a></div>"
            CodiceBarra = CodiceBarra & "<div id=""IdGDPR""  style=""visibility:hidden;position:absolute; left:45%; top:20%; color:#000000;  visibility:collapse; width:400px;height:200px;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;border:1px solid #000000;background-color:#FFFFFF;-webkit-box-shadow: #A8A8A8 8px 8px 8px;-moz-box-shadow: #A8A8A8 8px 8px 8px; box-shadow: #A8A8A8 8px 8px 8px; text-align:center;"">ATTENZIONE: Non hai ancora attivato il Kit GDPR Compliance.<br/><br/>Per attivare il KIT GDPR seguire le istruzioni riportate nella Circolare informativa o contattare l'ufficio Commerciale.<br/><br /><br/><a href=""#"" class=""SeniorButton"" onclick=""document.getElementById('IdGDPR').style.visibility='collapse';"">Chiudi</a></div>" & vbNewLine
        End If

        If Pagina.GetType.Name.ToUpper <> "Strumenti_aspx".ToUpper And Pagina.GetType.Name.ToUpper <> "Menu_Societa_aspx".ToUpper Then

            CodiceBarra = CodiceBarra & "<div style=""position: absolute; float:right; right: 5px; padding-top:5px;"">" & vbNewLine
            CodiceBarra = CodiceBarra & "<a href=""/Strumenti.aspx?URL=" & Pagina.Request.Url.AbsoluteUri & """><img style=""width:32px;"" src=""/images/setting.png"" /></a>" & vbNewLine
            CodiceBarra = CodiceBarra & "</div>"
        End If



        CodiceBarra = CodiceBarra & "<div style=""position: absolute; float:right; right: 30px; padding-top:5px;"">" & vbNewLine
        CodiceBarra = CodiceBarra & "<a href=""https://senior.e-personam.com/manuale/"" target=""_blank""><img style=""width:32px;"" src=""/images/help.png"" /></a>" & vbNewLine
        CodiceBarra = CodiceBarra & "</div>"

        'https://senior.e-personam.com/manuale/

        If Pagina.GetType.Name.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "OSPITIWEB_ELENCO_MOVIMENTIDOMICILIARE_ASPX".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
              Pagina.GetType.Name.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or Pagina.GetType.Name.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then
            If Val(Pagina.Session("CODICEOSPITE")) > 0 Then
                If Val(Pagina.Session("CODICEOSPITE")) > 0 Then
                    Dim kNote As New Cls_NoteOspite

                    kNote.CodiceOspite = Val(Pagina.Session("CODICEOSPITE"))
                    kNote.Leggi(Pagina.Session("DC_OSPITE"), Val(Pagina.Session("CODICEOSPITE")))

                    CodiceBarra = CodiceBarra & "<div style=""float:right; position: absolute; right:50px;"">" & vbNewLine
                    CodiceBarra = CodiceBarra & "<ul id=""postitospite"" >" & vbNewLine
                    If kNote.Note.Trim <> "" Then
                        CodiceBarra = CodiceBarra & "<li ><a href=""#"" class=""iconanote""  id=""MenuNote"" ><img style=""width:32px;"" src=""/images/notifichepresenti.png?ver=2"" /></a>" & vbNewLine
                    Else
                        CodiceBarra = CodiceBarra & "<li ><a href=""#"" class=""iconanote"" id=""MenuNote"" ><img style=""width:32px;"" src=""/images/senzanotifiche.png?ver=2"" /></a>" & vbNewLine
                    End If

                    CodiceBarra = CodiceBarra & "<ul class=""sub-postitospite""  id=""menunote"" style=""background-color: transparent; border:0px; -webkit-box-shadow: #A8A8A8 0px 0px 0px; -moz-box-shadow: #A8A8A8 0px 0px 0px; box-shadow: #A8A8A8 0px 0px 0px; "">" & vbNewLine
                    CodiceBarra = CodiceBarra & "<li  class=""aQliNote""  style=""background-color: transparent; border:0px; -webkit-box-shadow: #A8A8A8 0px 0px 0px; -moz-box-shadow: #A8A8A8 0px 0px 0px; box-shadow: #A8A8A8 0px 0px 0px;"">" & vbNewLine
                    CodiceBarra = CodiceBarra & "<div style=""background-image: url('/images/postit.png'); width:320px; height:362px;"" >" & vbNewLine
                    If kNote.Note.Trim <> "" Then
                        CodiceBarra = CodiceBarra & "<textarea  id=""TXT_NOTEOSPITE"" style=""width:250px; height:200px; resize: none; position: relative; left:20px; top:50px; border:0px; overflow:hidden; background-color: transparent;"" >" & kNote.Note & "</textarea><br />" & vbNewLine
                    Else
                        CodiceBarra = CodiceBarra & "<textarea  id=""TXT_NOTEOSPITE"" style=""width:250px; height:200px; resize: none; position: relative; left:20px; top:50px; border:0px; overflow:hidden; background-color: transparent;"" ></textarea><br />" & vbNewLine
                    End If
                    Dim AppoggioLeg As String = ""


                    AppoggioLeg = AppoggioLeg & "$.ajax({ url: 'notefattura.ashx?CODICEOSPITE=" & Val(Pagina.Session("CODICEOSPITE")) & "&NOTE=' + escape($('#TXT_NOTEOSPITE').val()), success: function(data, stato) { alert('modificato'); }, error: function(richiesta, stato, errori) { alert(stato); } });" & vbNewLine


                    CodiceBarra = CodiceBarra & "<a href=""#""><img src=""/Images/save.png"" style=""height:30px; width:70; position: relative; left:20px; top:80px;"" value=""Salva"" onClick=""" & AppoggioLeg & """ /></a>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</div>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</li>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</ul>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</li>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</ul>" & vbNewLine
                    CodiceBarra = CodiceBarra & "</div>" & vbNewLine
                End If
            End If
        End If


        Pagina.Session("CodiceBarra") = CodiceBarra
        Pagina.Session("DatiPresenti") = "SI"
        'left:1000px; 
    End Function


    Private Sub InserisciOspiteINRicerca(ByVal Connessione As String, ByVal Utente As String, ByVal ParametriRicerca As Cls_SqlString, ByVal NomePagina As String, ByRef pagina As Page)


        Dim cn As OleDbConnection
        Dim Riga As Long = 0


        cn = New Data.OleDb.OleDbConnection(pagina.Session("DC_OSPITE"))

        cn.Open()

        If NomePagina.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
           NomePagina.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then

            If Val(pagina.Session("CodiceOspite")) > 0 Then
                Dim cmddDeleteS As New OleDbCommand()
                cmddDeleteS.CommandText = "delete FROM UltimeRicerca Where CentroServizio = ? and CodiceOspite = ? AND UTENTE = ?"
                cmddDeleteS.Parameters.AddWithValue("@CentroServizio", pagina.Session("CodiceServizio"))
                cmddDeleteS.Parameters.AddWithValue("@CodiceOspite", pagina.Session("CodiceOspite"))
                cmddDeleteS.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDeleteS.Connection = cn
                cmddDeleteS.ExecuteNonQuery()
            End If
        End If

        If Val(pagina.Session("CodiceOspite")) > 0 Then
            If cn.ServerVersion < "10" Then
                Dim cmddDelete As New OleDbCommand()
                cmddDelete.CommandText = "DELETE  FROM UltimeRicerca  where UTENTE = ? And DataOraRicerca < ?  "
                cmddDelete.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDelete.Parameters.AddWithValue("@DataOraRicerca", DateAdd(DateInterval.Day, -10, Now))
                cmddDelete.Connection = cn
                cmddDelete.ExecuteNonQuery()
            Else
                Dim cmddDelete As New OleDbCommand()
                cmddDelete.CommandText = "with todelete as (SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY DataOraRicerca dESC) NUM,* FROM UltimeRicerca WHERE UTENTE = ? ) L WHERE NUM > 10) DELETE  FROM todelete;"
                cmddDelete.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDelete.Connection = cn
                cmddDelete.ExecuteNonQuery()
            End If
        End If


        'cmdInsert.CommandText = "IF NOT EXISTS(select top 1 * from UltimeRicerca Where codiceospite = " & pagina.Session("CodiceOspite") & " and DataOraRicerca = (Select MAX(DataOraRicerca) from UltimeRicerca))   INSERT INTO UltimeRicerca ([DataOraRicerca],[CentroServizio],[CodiceOspite]) VALUES (?,?,?)"
        If pagina.GetType.Name.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then

            If Val(pagina.Session("CodiceOspite")) > 0 Then

                Dim cmdInsert As New OleDbCommand()


                cmdInsert.CommandText = "INSERT INTO UltimeRicerca ([DataOraRicerca],[CentroServizio],[CodiceOspite],Utente) VALUES (?,?,?,?)"
                cmdInsert.Connection = cn
                cmdInsert.Parameters.AddWithValue("@DataRicerca", Now)
                cmdInsert.Parameters.AddWithValue("@CentroServizio", pagina.Session("CodiceServizio"))
                cmdInsert.Parameters.AddWithValue("@CodiceOspite", pagina.Session("CodiceOspite"))
                cmdInsert.Parameters.AddWithValue("@Utente", pagina.Session("UTENTE"))

                cmdInsert.ExecuteNonQuery()


            End If
        End If

        cn.Close()
    End Sub



    Private Function InserisciRegistrazioneINRicerca(ByVal Connessione As String, ByVal Utente As String, ByVal ParametriRicerca As Cls_SqlString, ByVal NomePagina As String, ByRef pagina As Page) As String
        Dim cn As OleDbConnection
        Dim cnTB As OleDbConnection
        Dim Riga As Long = 0
        Dim MySql As String = ""

        Dim k As New Cls_Login
        Dim Indice As Integer
        Indice = 0

        k.Utente = Utente
        k.LeggiSP(Connessione)

        cn = New Data.OleDb.OleDbConnection(k.Generale)

        cn.Open()


        cnTB = New Data.OleDb.OleDbConnection(k.TABELLE)

        cnTB.Open()

        Dim cmd As New OleDbCommand()

        Dim CodiceBarra As String = ""



        If pagina.GetType.Name.ToUpper = "GeneraleWeb_PrimaNota_aspx".ToUpper Or _
            pagina.GetType.Name.ToUpper = "GeneraleWeb_Documenti_aspx".ToUpper Or _
            pagina.GetType.Name.ToUpper = "GeneraleWeb_DocumentiJs_aspx".ToUpper Or _
            pagina.GetType.Name.ToUpper = "GeneraleWeb_incassipagamenti_aspx".ToUpper Then

            If Val(pagina.Session("NumeroRegistrazione")) > 0 Then
                Dim cmddDeleteS As New OleDbCommand()
                cmddDeleteS.CommandText = "delete FROM UltimeRicerca Where NumeroRegistrazione = ? AND UTENTE = ?"
                cmddDeleteS.Parameters.AddWithValue("@NumeroRegistrazione", pagina.Session("NumeroRegistrazione"))
                cmddDeleteS.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDeleteS.Connection = cn
                cmddDeleteS.ExecuteNonQuery()
            End If
        End If

        If Val(pagina.Session("NumeroRegistrazione")) > 0 Then
            If cn.ServerVersion < "10" Then
                Dim cmddDelete As New OleDbCommand()
                cmddDelete.CommandText = "DELETE  FROM UltimeRicerca  where UTENTE = ? And DataOraRicerca < ?  "
                cmddDelete.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDelete.Parameters.AddWithValue("@DataOraRicerca", DateAdd(DateInterval.Day, -10, Now))
                cmddDelete.Connection = cn
                cmddDelete.ExecuteNonQuery()
            Else
                Dim cmddDelete As New OleDbCommand()
                cmddDelete.CommandText = "with todelete as (SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY DataOraRicerca dESC) NUM,* FROM UltimeRicerca WHERE UTENTE = ? ) L WHERE NUM > 10) DELETE  FROM todelete;"
                cmddDelete.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmddDelete.Connection = cn
                cmddDelete.ExecuteNonQuery()
            End If
        End If





        If pagina.GetType.Name.ToUpper = "GeneraleWeb_PrimaNota_aspx".ToUpper Or pagina.GetType.Name.ToUpper = "GeneraleWeb_Documenti_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "GeneraleWeb_DocumentiJS_aspx".ToUpper Or _
           pagina.GetType.Name.ToUpper = "GeneraleWeb_incassipagamenti_aspx".ToUpper Then

            If Val(pagina.Session("NumeroRegistrazione")) > 0 Then
                Dim cmdInsert As New OleDbCommand

                cmdInsert.CommandText = "INSERT INTO UltimeRicerca ([DataOraRicerca],[NumeroRegistrazione],UTENTE) VALUES (?,?,?)"
                cmdInsert.Connection = cn
                cmdInsert.Parameters.AddWithValue("@DataRicerca", Now)
                cmdInsert.Parameters.AddWithValue("@NumeroRegistrazione", pagina.Session("NumeroRegistrazione"))
                cmdInsert.Parameters.AddWithValue("@UTENTE", pagina.Session("UTENTE"))
                cmdInsert.ExecuteNonQuery()
            End If
        End If


        Return CodiceBarra
    End Function


    Private Function ListaScadenze(ByVal Connessione As String, ByVal Utente As String) As String

        Dim k As New Cls_Login

        k.Utente = Utente
        k.LeggiSP(Connessione)

        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(k.Generale)
        cnOspiti = New Data.OleDb.OleDbConnection(k.Ospiti)

        cn.Open()
        cnOspiti.Open()

        Dim CodiceHtml As String


        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select TOP 5 * from Scadenzario where " & _
                               "DataScadenza >= ? And DataScadenza <= ? ")


        Dim DataInizio As Date = Format(DateAdd(DateInterval.Day, -1, Now), "dd/MM/yyyy")
        cmd.Parameters.AddWithValue("@DataScadenza", DataInizio)
        Dim DataFine As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmd.Parameters.AddWithValue("@DataScadenza", DataFine)

        CodiceHtml = ""
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            CodiceHtml = CodiceHtml + "&nbsp;<font color=""black"" size=""2"">Registrazione " & campodb(VerReader.Item("NumeroRegistrazioneContabile")) & " Scadenza : " & campodb(VerReader.Item("DataScadenza")) & "</font><br/>"
        Loop
        VerReader.Close()

        Dim cmdImp As New OleDbCommand()


        cmdImp.CommandText = ("select * from Impegnative where " & _
                               "DataFine >= ? And DataFine <= ? ")



        cmdImp.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdImp.Parameters.AddWithValue("@DataScadenza", DataFine)


        cmdImp.Connection = cnOspiti

        Dim ImpRd As OleDbDataReader = cmdImp.ExecuteReader()
        Do While ImpRd.Read
            Dim CSOspite As New ClsOspite

            CSOspite.Leggi(k.Ospiti, Val(campodb(ImpRd.Item("CodiceOspite"))))
            CodiceHtml = CodiceHtml + "&nbsp;<font color=""black"" size=""2"">Impegnativa ospite " & CSOspite.Nome & " in scadenza : " & campodb(ImpRd.Item("DataFine")) & "</font><br/>"
        Loop
        ImpRd.Close()


        Dim cmdOspiti As New OleDbCommand()
        cmdOspiti.CommandText = ("select * from AnagraficaComune where " & _
                       "(DataScadenza >= ? and DataScadenza <= ?)  or (DataScadenza2 >= ? And DataScadenza2 <= ?)   ")
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataFine)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataFine)

        cmdOspiti.Connection = cnOspiti

        Dim OspitiRd As OleDbDataReader = cmdOspiti.ExecuteReader()
        Do While OspitiRd.Read
            Dim CSOspite As New ClsOspite

            CSOspite.Leggi(k.Ospiti, Val(campodb(OspitiRd.Item("CodiceOspite"))))
            CodiceHtml = CodiceHtml + "&nbsp;<font color=""black"" size=""2"">Ticket ospite " & CSOspite.Nome & " in scadenza </font><br/>"
        Loop
        OspitiRd.Close()


        cn.Close()
        Return CodiceHtml
    End Function
    Private Function Scadenze(ByVal Connessione As String, ByVal Utente As String) As String


        Dim k As New Cls_Login

        k.Utente = Utente
        k.LeggiSP(Connessione)

        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(k.Generale)
        cnOspiti = New Data.OleDb.OleDbConnection(k.Ospiti)
        cn.Open()
        cnOspiti.Open()

        Dim Scadenziario As Integer


        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Scadenzario where " & _
                               "DataScadenza >= ? And DataScadenza <= ? ")


        Dim DataInizio As Date = Format(DateAdd(DateInterval.Day, -1, Now), "dd/MM/yyyy")
        cmd.Parameters.AddWithValue("@DataScadenza", DataInizio)
        Dim DataFine As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmd.Parameters.AddWithValue("@DataScadenza", DataFine)


        cmd.Connection = cn
        Scadenziario = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            Scadenziario = Scadenziario + 1
        Loop
        VerReader.Close()



        Dim cmdImp As New OleDbCommand()


        cmdImp.CommandText = ("select * from Impegnative where " & _
                               "DataFine >= ? And DataFine <= ? ")



        cmdImp.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdImp.Parameters.AddWithValue("@DataScadenza", DataFine)


        cmdImp.Connection = cnOspiti
        Scadenziario = 0
        Dim ImpRd As OleDbDataReader = cmdImp.ExecuteReader()
        Do While ImpRd.Read
            Scadenziario = Scadenziario + 1
        Loop
        ImpRd.Close()



        Dim Codice As String = ""

        If Scadenziario >= 1 Then
            Codice = Codice & "<img  src=""/images/notifichepresenti.gif"" onmouseover=""document.getElementById('IdMessaggiScadenze').style.visibility= 'visible';  document.getElementById('IdMessaggiScadenze').style.left = (getOffset(document.getElementById('IdMailLuminosa')).left - 380) + 'px';"" onmouseout=""document.getElementById('IdMessaggiScadenze').style.visibility= 'collapse'; "" />"
        End If



        Dim cmdOspiti As New OleDbCommand()
        cmdOspiti.CommandText = ("select * from AnagraficaComune where " & _
                       "(DataScadenza >= ? and DataScadenza <= ?)  or (DataScadenza2 >= ? And DataScadenza2 <= ?)   ")
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataFine)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataFine)

        cmdOspiti.Connection = cnOspiti
        Scadenziario = 0
        Dim OspitiRd As OleDbDataReader = cmdOspiti.ExecuteReader()
        Do While OspitiRd.Read
            Scadenziario = Scadenziario + 1
        Loop
        OspitiRd.Close()


        cnOspiti.Close()
        cn.Close()

        Return Codice
    End Function


    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class
