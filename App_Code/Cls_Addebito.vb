Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Addebito
    Public Codice As String
    Public Descrizione As String
    Public Mastro As Long
    Public Conto As Long
    Public Sottoconto As Long
    Public CodiceIVA As String
    Public CENTROSERVIZIO As String
    Public Struttura As String
    Public DataScadenza As Date
    Public Deposito As Integer
    Public Importo As Double
    Public ModalitaPagamentoEpersonam1 As String
    Public ModalitaPagamentoEpersonam2 As String
    Public ModalitaPagamentoEpersonam3 As String
    Public ModalitaPagamentoEpersonam4 As String
    Public FuoriRetta As Integer



    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabTipoAddebito where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TabTipoAddebito (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String
        
        MySql = "UPDATE TabTipoAddebito SET " & _
                " Descrizione = ?, " & _
                " Mastro = ?, " & _
                " Conto = ?, " & _
                " Sottoconto = ?, " & _
                " CodiceIVA = ?, " & _
                " CENTROSERVIZIO = ?, " & _
                " Scadenza = ?, " & _
                " Deposito = ?, " & _
                " ModalitaPagamentoEpersonam1 = ?, " & _
                " ModalitaPagamentoEpersonam2 = ?, " & _
                " ModalitaPagamentoEpersonam3 = ?, " & _
                " ModalitaPagamentoEpersonam4 = ?, " & _
                " Importo = ?, " & _
                " Struttura= ?, " & _
                " FuoriRetta = ? " & _
                " Where Codice = ? "


        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Aliquota", Mastro)
        cmd1.Parameters.AddWithValue("@AllegatoFornitori", Conto)
        cmd1.Parameters.AddWithValue("@AllegatoClienti", Sottoconto)
        cmd1.Parameters.AddWithValue("@Tipo", CodiceIVA)
        cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
        If Year(DataScadenza) > 1 Then
            cmd1.Parameters.AddWithValue("@DataScadenza", DataScadenza)
        Else
            cmd1.Parameters.AddWithValue("@DataScadenza", System.DBNull.Value)
        End If

        cmd1.Parameters.AddWithValue("@Deposito", Deposito)

        cmd1.Parameters.AddWithValue("@ModalitaPagamentoEpersonam1", ModalitaPagamentoEpersonam1)
        cmd1.Parameters.AddWithValue("@ModalitaPagamentoEpersonam2", ModalitaPagamentoEpersonam2)
        cmd1.Parameters.AddWithValue("@ModalitaPagamentoEpersonam3", ModalitaPagamentoEpersonam3)
        cmd1.Parameters.AddWithValue("@ModalitaPagamentoEpersonam4", ModalitaPagamentoEpersonam4)

        cmd1.Parameters.AddWithValue("@Importo", Importo)
        cmd1.Parameters.AddWithValue("@Struttura", Struttura)

        cmd1.Parameters.AddWithValue("@FuoriRetta", FuoriRetta)
        'Struttura
        'Importo
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TabTipoAddebito where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal MyDescrizione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabTipoAddebito where " & _
                           "Descrizione Like ? ")
        cmd.Parameters.AddWithValue("@MyDescrizione", MyDescrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            DataScadenza = campodbD(myPOSTreader.Item("Scadenza"))
            Deposito = Val(campodb(myPOSTreader.Item("Deposito")))

            ModalitaPagamentoEpersonam1 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam1"))
            ModalitaPagamentoEpersonam2 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam2"))
            ModalitaPagamentoEpersonam3 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam3"))
            ModalitaPagamentoEpersonam4 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam4"))

            Importo = campodbn(myPOSTreader.Item("Importo"))

            Struttura = campodb(myPOSTreader.Item("Struttura"))

            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function MaxTipoAddebito(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoAddebito = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END) from TabTipoAddebito ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))
            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If
            cn.Close()
            Return MassimoLetto
        Else
            cn.Close()
            Return "01"
        End If
        cn.Close()

    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabTipoAddebito where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            DataScadenza = campodbD(myPOSTreader.Item("Scadenza"))
            Deposito = Val(campodb(myPOSTreader.Item("Deposito")))

            ModalitaPagamentoEpersonam1 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam1"))
            ModalitaPagamentoEpersonam2 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam2"))
            ModalitaPagamentoEpersonam3 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam3"))
            ModalitaPagamentoEpersonam4 = campodb(myPOSTreader.Item("ModalitaPagamentoEpersonam4"))
            Importo = campodbn(myPOSTreader.Item("Importo"))
            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))

            Struttura = campodb(myPOSTreader.Item("Struttura"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, Optional ByVal Data As Date = #1/1/1900#)
        Dim cn As OleDbConnection


        If Year(Data) = 1900 Then
            Data = Now
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from TabTipoAddebito WHERE (SCADENZA IS NULL OR SCADENZA > ?) Order by Descrizione")
        cmd.Connection = cn        
        cmd.Parameters.AddWithValue("@SCADENZA", Data)
        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub
    Public Sub UpDateDropBoxCentroServizio(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal CentroServizio As String, Optional ByVal Data As Date = #1/1/1900#)
        Dim cn As OleDbConnection


        If Year(Data) = 1900 Then
            Data = Now
        End If

        Dim CS As New Cls_CentroServizio

        CS.CENTROSERVIZIO = CentroServizio
        CS.Leggi(StringaConnessione, CS.CENTROSERVIZIO)

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from TabTipoAddebito WHERE (SCADENZA IS NULL OR SCADENZA > ?) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?))  Order by Descrizione")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@SCADENZA", Data)
        cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd.Parameters.AddWithValue("@Struttura", CS.Villa)

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub
End Class


