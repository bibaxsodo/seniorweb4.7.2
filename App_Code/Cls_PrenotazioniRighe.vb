﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_PrenotazioniRighe
    Public Anno As Long
    Public Numero As Long
    Public Riga As Long
    Public Livello1 As Long
    Public Livello2 As Long
    Public Livello3 As Long
    Public Colonna As Long
    Public Descrizione As String
    Public Importo As Double



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub New()

        Anno = 0
        Numero = 0
        Riga = 0
        Livello1 = 0
        Livello2 = 0
        Livello3 = 0
        Colonna = 0
        Descrizione = ""
        Importo = 0
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PrenotazioniRiga where " & _
                           "Anno = ? And Numero = ? And Riga = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Riga", Riga)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Livello1 = Val(campodb(myPOSTreader.Item("Livello1")))
            Livello2 = Val(campodb(myPOSTreader.Item("Livello2")))
            Livello3 = Val(campodb(myPOSTreader.Item("Livello3")))
            Colonna = Val(campodb(myPOSTreader.Item("Colonna")))
            Descrizione = Val(campodb(myPOSTreader.Item("Descrizione")))
            Importo = Val(campodb(myPOSTreader.Item("Importo")))


        End If
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        If Riga = 0 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select max(Riga) from PrenotazioniRiga where " & _
                               "Anno = ? And Numero = ? ")
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Numero", Numero)
            cmd.Connection = cn            

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Riga = campodbN(myPOSTreader.Item(0)) + 1
            End If
            myPOSTreader.Close()
        End If

        Dim APPOGGIO As String = ""
        Dim cmdIns As New OleDbCommand()
        MySql = " INSERT INTO PrenotazioniRiga (Riga,Numero,Anno) VALUES (" & Riga & "," & Numero & "," & Anno & ")"
        cmdIns.CommandText = MySql
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()



        MySql = "UPDATE PrenotazioniRiga SET " & _
                "Livello1 = ?," & _
                "Livello2 = ?," & _
                "Livello3 = ?," & _
                "Colonna = ?," & _
                "Descrizione = ?," & _
                "Importo = ? " & _
                " Where Numero = ? And Anno = ? And Riga = ?"


        '"Scadenza = ?," & _

        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = MySql

        cmd1.Parameters.AddWithValue("@Livello1", Livello1)
        cmd1.Parameters.AddWithValue("@Livello2", Livello2)
        cmd1.Parameters.AddWithValue("@Livello3", Livello3)
        cmd1.Parameters.AddWithValue("@Colonna", Colonna)
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Importo", Importo)
        cmd1.Parameters.AddWithValue("@Numero", Numero)
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Riga", Riga)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub




End Class

