﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Indice
    Public idIndice As Long
    Public Descrizione As String
    Public idTitolo As Long
    Public idClasse As Long

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaIndice Order by idTitolo,idclasse,idIndice")
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("idIndice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("idTitolo", GetType(String))
        Tabella.Columns.Add("idClasse", GetType(String))

        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("idIndice")

            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(2) = Val(campodb(myPOSTreader.Item("idTitolo")))
            myriga(3) = Val(campodb(myPOSTreader.Item("idClasse")))
            Tabella.Rows.Add(myriga)

            Entrato = True
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Entrato = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)
        End If
    End Sub

    Sub ScriviTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim transaction As OleDbTransaction

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        transaction = cn.BeginTransaction()

        Try

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("delete from TabellaIndice")
            cmd.Connection = cn
            cmd.Transaction = transaction
            cmd.ExecuteNonQuery()

            Dim i As Long

            For i = 0 To Tabella.Rows.Count - 1
                If Tabella.Rows(i).Item(1).ToString.Trim <> "" Then
                    Dim cmdIns As New OleDbCommand()
                    cmdIns.CommandText = ("Insert Into TabellaIndice (idIndice,Descrizione,IdTitolo,idClasse)  VALUES (?,?,?,?) ")

                    If Val(campodb(Tabella.Rows(i).Item(0))) > 0 Then
                        cmdIns.Parameters.AddWithValue("@IdGruppo", Val(Tabella.Rows(i).Item(0)))
                    Else
                        Dim cmdMax As New OleDbCommand()
                        cmdMax.CommandText = ("select MAX(idIndice) from TabellaIndice")
                        cmdMax.Connection = cn
                        cmdMax.Transaction = transaction
                        Dim RDMax As OleDbDataReader = cmdMax.ExecuteReader()
                        If RDMax.Read Then
                            cmdIns.Parameters.AddWithValue("@idIndice", Val(campodb(RDMax.Item(0))) + 1)
                        End If
                        RDMax.Close()
                    End If
                    cmdIns.Parameters.AddWithValue("@Descrizione", Tabella.Rows(i).Item(1))
                    cmdIns.Parameters.AddWithValue("@IdTitolo", Tabella.Rows(i).Item(2))
                    cmdIns.Parameters.AddWithValue("@idClasse", Tabella.Rows(i).Item(3))
                    cmdIns.Transaction = transaction
                    cmdIns.Connection = cn
                    cmdIns.ExecuteNonQuery()
                End If
            Next
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
        End Try

        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub UpDropDownListClasse(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal idTitolo As Integer, ByVal idClasse As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaIndice Where idTitolo = ? And idClasse = ? Order by IdIndice")
        cmd.Parameters.AddWithValue("@idTitolo", idTitolo)
        cmd.Parameters.AddWithValue("@idClasse", idClasse)

        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If myPOSTreader.Item("idIndice") > 0 Then
                appoggio.Items.Add(myPOSTreader.Item("idIndice") & " - " & myPOSTreader.Item("Descrizione"))
            Else
                appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            End If
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("idIndice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        'appoggio.Items.Add("")
        'appoggio.Items(appoggio.Items.Count - 1).Value = ""
        'appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaIndice where idTitolo = " & idTitolo & " And idClasse = " & idClasse & " And IdIndice = " & idIndice)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


End Class
