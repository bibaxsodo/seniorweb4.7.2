﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabelleFascicolo
    Public IdFascicolo As Long
    Public IdUfficioDestinatario As Long
    Public Descrizione As String
    Public Chiuso As Integer
    Public CodiceFascicolo As String
    Public DataScadenza As Date

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaFascicolo Order by IdUfficioDestinatario,IdFascicolo")
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("IdUfficioDestinatario", GetType(String))
        Tabella.Columns.Add("IdFascicolo", GetType(String))
        Tabella.Columns.Add("CodiceFascicolo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Chiuso", GetType(Integer))        
        Tabella.Columns.Add("DataScadenza", GetType(String))




        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Val(campodb(myPOSTreader.Item("IdFascicolo")))

            myriga(1) = myPOSTreader.Item("IdUfficioDestinatario")
            myriga(2) = myPOSTreader.Item("CodiceFascicolo")
            myriga(3) = myPOSTreader.Item("Descrizione")
            myriga(4) = myPOSTreader.Item("Chiuso")


            myriga(5) = myPOSTreader.Item("DataScadenza")

            Tabella.Rows.Add(myriga)

            Entrato = True
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Entrato = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)
        End If
    End Sub


    Sub ScriviTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim transaction As OleDbTransaction

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        transaction = cn.BeginTransaction()

        Try

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("delete from TabellaFascicolo")
            cmd.Connection = cn
            cmd.Transaction = transaction
            cmd.ExecuteNonQuery()

            Dim i As Long

            For i = 0 To Tabella.Rows.Count - 1
                If Tabella.Rows(i).Item(1).ToString.Trim <> "" Then
                    Dim cmdIns As New OleDbCommand()
                    'IdUfficioDestinatario,IdFascicolo
                    cmdIns.CommandText = ("Insert Into TabellaFascicolo (IdUfficioDestinatario,IdFascicolo,CodiceFascicolo,Descrizione,Chiuso,DataScadenza)  VALUES (?,?,?,?,?,?) ")

                    cmdIns.Parameters.AddWithValue("@IdUfficioDestinatario", Tabella.Rows(i).Item(1))
                    If Val(campodb(Tabella.Rows(i).Item(0))) > 0 Then
                        cmdIns.Parameters.AddWithValue("@IdGruppo", Val(Tabella.Rows(i).Item(0)))
                    Else
                        Dim cmdMax As New OleDbCommand()
                        cmdMax.CommandText = ("select MAX(IdFascicolo) from TabellaFascicolo")
                        cmdMax.Connection = cn
                        cmdMax.Transaction = transaction
                        Dim RDMax As OleDbDataReader = cmdMax.ExecuteReader()
                        If RDMax.Read Then
                            cmdIns.Parameters.AddWithValue("@IdFascicolo", Val(campodb(RDMax.Item(0))) + 1)
                        End If
                        RDMax.Close()
                    End If

                    cmdIns.Parameters.AddWithValue("@CodiceFascicolo", Tabella.Rows(i).Item(2))
                    cmdIns.Parameters.AddWithValue("@Descrizione", Tabella.Rows(i).Item(3))
                    cmdIns.Parameters.AddWithValue("@Chiuso", Tabella.Rows(i).Item(4))

                    If campodb(Tabella.Rows(i).Item(5)) = "" Then
                        cmdIns.Parameters.AddWithValue("@DataScadenza", DBNull.Value)
                    Else
                        cmdIns.Parameters.AddWithValue("@DataScadenza", Tabella.Rows(i).Item(5))
                    End If

                    cmdIns.Transaction = transaction
                    cmdIns.Connection = cn
                    cmdIns.ExecuteNonQuery()
                End If
            Next
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
        End Try

        cn.Close()
    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub UpDropDownListClasse(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal IdUfficioDestinatario As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaFascicolo Where IdUfficioDestinatario = ? order by  IdFascicolo")
        cmd.Parameters.AddWithValue("@IdUfficioDestinatario", IdUfficioDestinatario)
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("CodiceFascicolo") & " - " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("IdFascicolo")
            appoggio.Items(appoggio.Items.Count - 1).Selected = False
        Loop
        myPOSTreader.Close()
        cn.Close()


        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaFascicolo ")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If myPOSTreader.Item("IdFascicolo") > 0 Then
                appoggio.Items.Add(myPOSTreader.Item("CodiceFascicolo") & " - " & myPOSTreader.Item("Descrizione"))
            Else
                appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            End If
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("IdFascicolo")
        Loop
        myPOSTreader.Close()
        cn.Close()
        If appoggio.Items.Count > 0 Then
            appoggio.Items(0).Selected = True
        End If
        'appoggio.Items.Add("")
        'appoggio.Items(appoggio.Items.Count - 1).Value = ""
        'appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaFascicolo where IdFascicolo = " & IdFascicolo & " And IdUfficioDestinatario = " & IdUfficioDestinatario)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Chiuso = Val(campodb(myPOSTreader.Item("Chiuso")))
            DataScadenza = campodbd(myPOSTreader.Item("DataScadenza"))
            CodiceFascicolo = campodb(myPOSTreader.Item("CodiceFascicolo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
