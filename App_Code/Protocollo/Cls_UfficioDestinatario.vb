﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_UfficioDestinatario
    Public idUfficioDestinatario As Long
    Public Descrizione As String

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaUfficioDestinatario ")
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("idUfficioDestinatario", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("idUfficioDestinatario")

            myriga(1) = myPOSTreader.Item("Descrizione")
            Tabella.Rows.Add(myriga)

            Entrato = True
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Entrato = False Then
            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            myriga1(0) = 0
            myriga1(1) = ""
            Tabella.Rows.Add(myriga1)
        End If
    End Sub

    Sub ScriviTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim transaction As OleDbTransaction

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        transaction = cn.BeginTransaction()

        Try
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("delete from TabellaUfficioDestinatario")
            cmd.Connection = cn
            cmd.Transaction = transaction
            cmd.ExecuteNonQuery()

            Dim i As Long

            For i = 0 To Tabella.Rows.Count - 1
                If Tabella.Rows(i).Item(1).ToString.Trim <> "" Then
                    Dim cmdIns As New OleDbCommand()
                    cmdIns.CommandText = ("Insert Into TabellaUfficioDestinatario (idUfficioDestinatario,Descrizione)  VALUES (?,?) ")

                    If Val(campodb(Tabella.Rows(i).Item(0))) > 0 Then
                        cmdIns.Parameters.AddWithValue("@IdUfficioDestinatario", Val(Tabella.Rows(i).Item(0)))
                    Else
                        Dim cmdMax As New OleDbCommand()
                        cmdMax.CommandText = ("select MAX(idUfficioDestinatario) from TabellaUfficioDestinatario")
                        cmdMax.Transaction = transaction
                        cmdMax.Connection = cn
                        Dim RDMax As OleDbDataReader = cmdMax.ExecuteReader()
                        If RDMax.Read Then
                            cmdIns.Parameters.AddWithValue("@idUfficioDestinatario", Val(campodb(RDMax.Item(0))) + 1)
                        End If
                        RDMax.Close()
                    End If
                    cmdIns.Parameters.AddWithValue("@Descrizione", Tabella.Rows(i).Item(1))
                    cmdIns.Transaction = transaction
                    cmdIns.Connection = cn
                    cmdIns.ExecuteNonQuery()
                End If
            Next
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
        End Try
        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub UpCheckBoxList(ByVal StringaConnessione As String, ByRef appoggio As CheckBoxList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaUfficioDestinatario")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("idUfficioDestinatario")
        Loop
        myPOSTreader.Close()
        cn.Close()
        'appoggio.Items.Add("")
        'appoggio.Items(appoggio.Items.Count - 1).Value = ""
        'appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaUfficioDestinatario")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("idUfficioDestinatario")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaUfficioDestinatario where idUfficioDestinatario = " & idUfficioDestinatario)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
