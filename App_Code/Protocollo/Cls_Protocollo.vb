﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Protocollo
    Public Id As Long    
    Public Numero As Integer
    Public Tipo As String
    Public Anno As Integer
    Public Titolo As Integer
    Public TipoProtocolloPrecedente As String
    Public NumeroProtocolloPrecedente As Integer
    Public AnnoProtocolloPrecedente As Integer
    Public TipoProtocolloSuccessivo As String
    Public NumeroProtocolloSuccessivo As Integer
    Public AnnoProtocolloSuccessivo As Integer
    Public DataDocumento As Date
    Public DataArrivo As Date
    Public NumeroDocumento As String
    Public RagioneSociale As String
    Public Indirizzo As String
    Public Citta As String
    Public Cap As String
    Public Provincia As String
    Public Stato As String
    Public Mezzo As Integer
    Public Allegati As Integer
    Public UfficioDestinatario As Integer
    Public Oggetto As String
    Public Note As String
    Public Utente As String
    Public Classe As Integer
    Public Indice As Integer
    Public Fascicolo As String
    Public OraArrivo As Date
    Public FascicoloId As Integer
    Public CONSENSOINSERIMENTO As Integer
    Public CONSENSOMARKETING As Integer

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Protocollo where " & _
                           "Anno = ? And Numero = ? And Tipo = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Tipo", Tipo)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            If StringaConnessione.IndexOf("AspCharitas") > 0 Then
                Dim XSql As String

                XSql = "Declare @numero as INT;Set @numero = (Select max(Numero) + 1 From Protocollo with(updlock) Where Anno = " & Anno & " ); " & vbNewLine
                XSql = XSql & " IF @numero is null " & vbNewLine
                XSql = XSql & "   Set @numero = 1" & vbNewLine


                XSql = XSql & "INSERT INTO Protocollo (Anno,Numero,Tipo,Utente) values (?,@numero,?,?)" & vbNewLine
                Dim cmdInsT As New OleDbCommand()
                cmdInsT.CommandText = XSql
                cmdInsT.Parameters.AddWithValue("@Anno", Anno)
                cmdInsT.Parameters.AddWithValue("@Tipo", Tipo)
                cmdInsT.Parameters.AddWithValue("@Utente", Utente)
                cmdInsT.Connection = cn

                cmdInsT.ExecuteNonQuery()


                Dim CmdRd As New OleDbCommand()

                XSql = "Select Max(Numero) from Protocollo Where Anno = " & Anno & "  And Utente = ? "
                CmdRd.CommandText = XSql
                CmdRd.Parameters.AddWithValue("@Utente", Utente)
                CmdRd.Connection = cn

                Dim XR As OleDbDataReader = CmdRd.ExecuteReader()
                If XR.Read Then
                    Numero = XR.Item(0)
                End If
                XR.Close()
            Else
                Dim XSql As String

                XSql = "Declare @numero as INT;Set @numero = (Select max(Numero) + 1 From Protocollo with(updlock) Where Anno = " & Anno & " And Tipo = '" & Tipo & "'); " & vbNewLine
                XSql = XSql & " IF @numero is null " & vbNewLine
                XSql = XSql & "   Set @numero = 1" & vbNewLine


                XSql = XSql & "INSERT INTO Protocollo (Anno,Numero,Tipo,Utente) values (?,@numero,?,?)" & vbNewLine
                Dim cmdInsT As New OleDbCommand()
                cmdInsT.CommandText = XSql
                cmdInsT.Parameters.AddWithValue("@Anno", Anno)
                cmdInsT.Parameters.AddWithValue("@Tipo", Tipo)
                cmdInsT.Parameters.AddWithValue("@Utente", Utente)
                cmdInsT.Connection = cn

                cmdInsT.ExecuteNonQuery()


                Dim CmdRd As New OleDbCommand()

                XSql = "Select Max(Numero) from Protocollo Where Anno = " & Anno & "  And Tipo = '" & Tipo & "' And Utente = ? "
                CmdRd.CommandText = XSql
                CmdRd.Parameters.AddWithValue("@Utente", Utente)
                CmdRd.Connection = cn

                Dim XR As OleDbDataReader = CmdRd.ExecuteReader()
                If XR.Read Then
                    Numero = XR.Item(0)
                End If
                XR.Close()
            End If
        End If


        Dim cmdIns As New OleDbCommand()


        cmdIns.CommandText = ("UPDATE Protocollo SET Titolo = ?,NumeroProtocolloPrecedente = ?,TipoProtocolloPrecedente = ?,AnnoProtocolloPrecedente = ?,TipoProtocolloSuccessivo = ?,NumeroProtocolloSuccessivo = ?,AnnoProtocolloSuccessivo = ?,DataDocumento = ?,DataArrivo = ?,NumeroDocumento = ?,RagioneSociale = ?,Indirizzo = ?,Citta = ?,Cap = ?,Provincia = ?,Stato = ?,Mezzo = ?,Allegati = ?,UfficioDestinatario = ?,Oggetto = ?,Note = ?,Utente =?,DataModifica=?,Classe = ?,Indice = ?, Fascicolo = ?,OraArrivo = ?,FascicoloId = ?,CONSENSOINSERIMENTO = ?,CONSENSOMARKETING = ? Where Numero = " & Numero & " And Tipo = '" & Tipo & "' And Anno = " & Anno)

        'cmdIns.Parameters.AddWithValue("@Numero", Numero)
        'cmdIns.Parameters.AddWithValue("@Tipo", Tipo)
        'cmdIns.Parameters.AddWithValue("@Anno", Anno)
        cmdIns.Parameters.AddWithValue("@Titolo", Titolo)
        cmdIns.Parameters.AddWithValue("@NumeroProtocolloPrecedente", NumeroProtocolloPrecedente)
        cmdIns.Parameters.AddWithValue("@TipoProtocolloPrecedente", TipoProtocolloPrecedente)
        cmdIns.Parameters.AddWithValue("@AnnoProtocolloPrecedente", AnnoProtocolloPrecedente)
        cmdIns.Parameters.AddWithValue("@TipoProtocolloSuccessivo", TipoProtocolloSuccessivo)
        cmdIns.Parameters.AddWithValue("@NumeroProtocolloSuccessivo", NumeroProtocolloSuccessivo)
        cmdIns.Parameters.AddWithValue("@AnnoProtocolloSuccessivo", AnnoProtocolloSuccessivo)
        cmdIns.Parameters.AddWithValue("@DataDocumento", IIf(Year(DataDocumento) > 1, DataDocumento, System.DBNull.Value))
        cmdIns.Parameters.AddWithValue("@DataArrivo", DataArrivo)
        cmdIns.Parameters.AddWithValue("@NumeroDocumento", NumeroDocumento)
        cmdIns.Parameters.AddWithValue("@RagioneSociale", RagioneSociale)
        cmdIns.Parameters.AddWithValue("@Indirizzo", Trim(Mid(Indirizzo & Space(100), 1, 100)))
        cmdIns.Parameters.AddWithValue("@Citta", Trim(Mid(Citta & Space(60), 1, 49)))
        cmdIns.Parameters.AddWithValue("@Cap", Trim(Mid(Cap & Space(60), 1, 6)))
        cmdIns.Parameters.AddWithValue("@Provincia", Provincia)
        cmdIns.Parameters.AddWithValue("@Stato", Trim(Mid(Stato & Space(60), 1, 49)))
        cmdIns.Parameters.AddWithValue("@Mezzo", Mezzo)
        cmdIns.Parameters.AddWithValue("@Allegati", Allegati)
        cmdIns.Parameters.AddWithValue("@UfficioDestinatario", UfficioDestinatario)
        cmdIns.Parameters.AddWithValue("@Oggetto", Oggetto)
        cmdIns.Parameters.AddWithValue("@Note", Note)
        cmdIns.Parameters.AddWithValue("@Utente", Utente)
        cmdIns.Parameters.AddWithValue("@DataModifica", Now)
        cmdIns.Parameters.AddWithValue("@Classe", Classe)
        cmdIns.Parameters.AddWithValue("@Indice", Indice)
        cmdIns.Parameters.AddWithValue("@Fascicolo", Fascicolo)
        cmdIns.Parameters.AddWithValue("@OraArrivo", OraArrivo)
        cmdIns.Parameters.AddWithValue("@FascicoloId", FascicoloId)
        cmdIns.Parameters.AddWithValue("@CONSENSOINSERIMENTO", CONSENSOINSERIMENTO)
        cmdIns.Parameters.AddWithValue("@CONSENSOMARKETING", CONSENSOMARKETING)
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function UltimaData(ByVal StringaConnessione As String) As Date
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select max(DataArrivo) from Protocollo where " & _
                               "Anno = ? And Tipo = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Anno", Anno)

        cmd.Parameters.AddWithValue("@Tipo", Tipo)

        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            UltimaData = campodbd(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()
        cn.Close()



    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Protocollo where " & _
                               "Anno = ? And Numero = ? And Tipo = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Tipo", Tipo)



        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Numero = campodb(myPOSTreader.Item("Numero"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Anno = campodb(myPOSTreader.Item("Anno"))
            Titolo = Val(campodb(myPOSTreader.Item("Titolo")))
            TipoProtocolloPrecedente = campodb(myPOSTreader.Item("TipoProtocolloPrecedente"))
            NumeroProtocolloPrecedente = Val(campodb(myPOSTreader.Item("NumeroProtocolloPrecedente")))
            AnnoProtocolloPrecedente = Val(campodb(myPOSTreader.Item("AnnoProtocolloPrecedente")))
            TipoProtocolloSuccessivo = campodb(myPOSTreader.Item("NumeroProtocolloSuccessivo"))
            NumeroProtocolloSuccessivo = Val(campodb(myPOSTreader.Item("NumeroProtocolloSuccessivo")))
            AnnoProtocolloSuccessivo = Val(campodb(myPOSTreader.Item("AnnoProtocolloSuccessivo")))
            DataDocumento = campodbd(myPOSTreader.Item("DataDocumento"))
            DataArrivo = campodbd(myPOSTreader.Item("DataArrivo"))
            NumeroDocumento = campodb(myPOSTreader.Item("NumeroDocumento"))
            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Citta = campodb(myPOSTreader.Item("Citta"))
            Cap = campodb(myPOSTreader.Item("Cap"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Stato = campodb(myPOSTreader.Item("Stato"))
            Mezzo = Val(campodb(myPOSTreader.Item("Mezzo")))
            Allegati = Val(campodb(myPOSTreader.Item("Allegati")))
            UfficioDestinatario = Val(campodb(myPOSTreader.Item("UfficioDestinatario")))
            Oggetto = campodb(myPOSTreader.Item("Oggetto"))
            Note = campodb(myPOSTreader.Item("Note"))

            Classe = Val(campodb(myPOSTreader.Item("Classe")))
            Indice = Val(campodb(myPOSTreader.Item("Indice")))
            Fascicolo = campodb(myPOSTreader.Item("Fascicolo"))
            OraArrivo = campodbd(myPOSTreader.Item("OraArrivo"))

            FascicoloId = Val(campodb(myPOSTreader.Item("FascicoloId")))

            CONSENSOINSERIMENTO = Val(campodb(myPOSTreader.Item("CONSENSOINSERIMENTO")))
            CONSENSOMARKETING = Val(campodb(myPOSTreader.Item("CONSENSOMARKETING")))
        End If
        myPOSTreader.Close()
        cn.Close()


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function



    Sub CaricaLista(ByVal StringaConnessione As String, ByRef Lista As ListBox, ByVal Oggetto As String, ByVal DataDal As String, ByVal DataAl As Date)
        Dim cn As OleDbConnection

        Lista.Items.Clear()

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Year(DataDal) > 1900 And Year(DataAl) > 1900 And Oggetto = "" Then
            cmd.CommandText = ("select * from Protocollo where " & _
                                   "DataArrivo < ? And DataArrivo > ?")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@DataDal", DataAl)
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        Else
            cmd.CommandText = ("select * from Protocollo where " & _
                                   "Oggetto Like ?")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Oggetto", Oggetto)
        End If
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Lista.Items.Add(campodb(myPOSTreader.Item("Anno")) & " " & campodb(myPOSTreader.Item("Numero")) & " " & campodb(myPOSTreader.Item("Tipo")) & " " & campodb(myPOSTreader.Item("Oggetto")))
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from Protocollo where " & _
                               "Anno = ? And Numero = ? And Tipo = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Tipo", Tipo)
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub
End Class
