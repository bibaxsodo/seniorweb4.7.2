﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CigClientiFornitori
    Public CodiceDebitoreCreditore As Integer
    Public CodiceCig As String
    Public DataInizio As Date
    Public DataFine As Date
    Public ImportoBudget As Double
    Public Utente As String
    Public Descrizione As String
   

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Integer
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodbD(ByVal oggetto As Object) As date
        If IsDBNull(oggetto) Then
            Return  Nothing
        Else
            Return oggetto
        End If
    End Function



    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal CodiceDebitoreCreditore As Integer, ByVal DataRegistrazione As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        
        cmd.CommandText ="Select * From CigClientiFornitori where CodiceDebitoreCreditore = ? And DataInzio <= ? And DataFine >= ? "
        cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore",CodiceDebitoreCreditore)
        cmd.Parameters.AddWithValue("@DataInzio",DataRegistrazione)
        cmd.Parameters.AddWithValue("@DataFine",DataRegistrazione)
        cmd.Connection = cn
      

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("CodiceCig") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = campodb(myPOSTreader.Item("CodiceCig"))
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    
     Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from CigClientiFornitori where CodiceDebitoreCreditore = ? "
        cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore",CodiceDebitoreCreditore)
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

       
        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO CigClientiFornitori (CodiceDebitoreCreditore,DataInizio,DataFine,CodiceCig,ImportoBudget,Descrizione,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CodiceDebitoreCreditore", CodiceDebitoreCreditore)
                Dim xData As Date

                
                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@DataInizio", xData)


                xData = Tabella.Rows(i).Item(1)
                cmdw.Parameters.AddWithValue("@DataFine", xData)


                cmdw.Parameters.AddWithValue("@CodiceCig", Tabella.Rows(i).Item(2))
                
                cmdw.Parameters.AddWithValue("@ImportoBudget", CDbl(Tabella.Rows(i).Item(3)))


                cmdw.Parameters.AddWithValue("@Descrizione", Tabella.Rows(i).Item(4))

                'Descrizione


                cmdw.Parameters.AddWithValue("@Utente", Utente)

                cmdw.Parameters.AddWithValue("@DataAggiornamento", now)
                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()


            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Sub LeggiCig(ByVal StringaConnessione As String,ByVal Codice As Integer, ByVal Cig As String)
         Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CigClientiFornitori where CodiceDebitoreCreditore = ? And CodiceCig = ? Order By DataInizio,DataFine")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice",Codice)
        cmd.Parameters.AddWithValue("@Cig",Cig)
            
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            CodiceDebitoreCreditore = campodbN(myPOSTreader.Item("CodiceDebitoreCreditore"))
            CodiceCig  = campodb(myPOSTreader.Item("CodiceCig"))
            DataInizio   = campodbd(myPOSTreader.Item("DataInizio"))
            DataFine = campodbd(myPOSTreader.Item("DataFine"))
            ImportoBudget  = campodbN(myPOSTreader.Item("ImportoBudget"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

        Loop
        myPOSTreader.Close()
        

        cn.Close
    End Sub

    Function ImportoCigUsato(ByVal StringaConnessione As String, ByVal StringaConnessioneTabelle As String, ByVal Codice As Integer, ByVal CodiceCig As String, ByVal MastroCliente As Integer, ByVal ContoCliente As Integer, ByVal SottocontoCliente As Long) As Double
        Dim cn As OleDbConnection
        Dim ImportoDocumenti As Double = 0

        LeggiCig(StringaConnessione, Codice, CodiceCig)

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiContabiliTesta where CodiceCig = ? And DataRegistrazione >= ? And DataRegistrazione <= ?  And (Select count(*) From MovimentiContabiliRiga  Where Numero = NumeroRegistrazione And RigaDaCausale =1 And MastroPartita =? And ContoPartita = ? And SottocontoPartita = ?) > 0 Order By DataRegistrazione")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceCig", CodiceCig)
        cmd.Parameters.AddWithValue("@DataRegistrazioneDal", DataInizio)
        cmd.Parameters.AddWithValue("@DataRegistrazioneAl", DataFine)
        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottoContoCliente", SottocontoCliente)

        Dim ReadReg As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadReg.Read
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbN(ReadReg.Item("NumeroRegistrazione"))
            Registrazione.Leggi(StringaConnessione, Registrazione.NumeroRegistrazione)

            ImportoDocumenti = ImportoDocumenti + Registrazione.ImportoDocumento(StringaConnessioneTabelle)


        Loop
        ReadReg.Close()

        cn.Close()

        Return ImportoDocumenti
    End Function
    
    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CigClientiFornitori where CodiceDebitoreCreditore = ? Order By DataInizio,DataFine")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore",CodiceDebitoreCreditore)
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data Inizio", GetType(String))
        Tabella.Columns.Add("Data Fine", GetType(String))
        Tabella.Columns.Add("Cig", GetType(String))
        Tabella.Columns.Add("ImportoBudget", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Try

            
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Format(campodbd(myPOSTreader.Item("DataInizio")), "dd/MM/yyyy")
                myriga(1) = Format(campodbd(myPOSTreader.Item("DataFine")), "dd/MM/yyyy")
                myriga(2) = campodb( myPOSTreader.Item("CodiceCig"))
                myriga(3) = format(campodbn( myPOSTreader.Item("ImportoBudget")),"#,##0.00")
                myriga(4) = campodb(myPOSTreader.Item("Descrizione"))
                Tabella.Rows.Add(myriga)
            Catch ex As Exception

            End Try
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga1)
        End If
        cn.Close()
    End Sub



    
    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList,byval CodiceDebitoreCreditore As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from CigClientiFornitori where CodiceDebitoreCreditore = ?  Order by CodiceCig ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore",CodiceDebitoreCreditore)

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("CodiceCig")) & " " & campodb(myPOSTreader.Item("Descrizione")))
            appoggio.Items(appoggio.Items.Count - 1).Value = campodb(myPOSTreader.Item("CodiceCig"))
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub UpDateDropBoxAttivi(ByVal StringaConnessione As String, ByRef appoggio As DropDownList,byval CodiceDebitoreCreditore As Integer,byval DataValidita As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from CigClientiFornitori where CodiceDebitoreCreditore = ?  And DataInizio <= ? And DataFine >= ? order by CodiceCig ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore",CodiceDebitoreCreditore)
        cmd.Parameters.AddWithValue("@DataValidita",DataValidita)
        cmd.Parameters.AddWithValue("@DataValidita",DataValidita)

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("CodiceCig")) & " " & campodb(myPOSTreader.Item("Descrizione")))
            appoggio.Items(appoggio.Items.Count - 1).Value = campodb(myPOSTreader.Item("CodiceCig"))
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub



End Class
