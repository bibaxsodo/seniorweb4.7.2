﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DatiGenerali
    Public CausaleChiusura As String
    Public DataChiusura As Date
    Public EsercizioMese As Long
    Public EsercizioGiorno As Long
    Public ApprovazioneBilancio As Date    
    Public CausaleApertura As String
    Public BilancioChiusuraMastro As Long
    Public BilancioChiusuraConto As Long
    Public BilancioChiusuraSottoconto As Long
    Public ProfittiPerditeMastro As Long
    Public ProfittiPerditeConto As Long
    Public ProfittiPerditeSottoconto As Long
    Public BilancioAperturaMastro As Long
    Public BilancioAperturaConto As Long
    Public BilancioAperturaSottoconto As Long
    Public UtilePatrimonialeMastro As Long
    Public UtilePatrimonialeConto As Long
    Public UtilePatrimonialeSottoconto As Long
    Public PerditaPatrimonialeMastro As Long
    Public PerditaPatrimonialeConto As Long
    Public PerditaPatrimonialeSottoconto As Long
    Public UtileEconomicoMastro As Long
    Public UtileEconomicoConto As Long
    Public UtileEconomicoSottoconto As Long
    Public PerditaEconomicoMastro As Long
    Public PerditaEconomicoConto As Long
    Public PerditaEconomicoSottoconto As Long
    Public CausaleRettifica As String
    Public ArrotondaIVA As String
    Public ClientiMastro As Long    
    Public FornitoriMastro As Long
    Public IVAMastro As Long
    Public RegistroLiquidazioni As Long
    Public LiquidazioneTrimestrale As String
    Public CodiceDetraibilita As String
    Public CodiceScadenza As String
    Public CodiceRitenute As String
    Public NumeroAutomaticoClienti As String
    Public ValoreEURO As Double
    Public TipoApertura As String
    Public EconomoMastro As Long
    Public EconomoConto As Long
    Public EconomoSottoconto As Double
    Public EconomoCapitolo As Long
    Public EconomoArticolo As Long
    Public ImportoMassimo As Double
    Public CausaleGirocontoRitenute As String    
    Public ContoBollo As Long
    Public MastroBollo As Long
    Public SottoContoBollo As Long
    Public ClientiConto As Long
    Public FornitoriConto As Long
    Public StampaDaGestioneDocumenti As Integer

    Public MovimentiRitenute As Integer
    Public GirocontoRitenuta As Integer
    Public FatturazionePrivati As Integer
    Public BudgetAnalitica As Integer
    Public CausaleContabileDefault As String
    Public CausaleContabileNCDefault As String
    Public AttivaCServPrimanoIncassi As Integer
    Public ModalitaPagamentoOblDoc As Integer
    Public TestAgyo As String
    Public IdAgyo As String
    Public PasswordAgyo As String
    Public TokenAgyo As String
    Public BloccaLegameScadenzario As Integer
    Public ScadenziarioCheckChiuso As Integer
    Public TipoVisualizzazione As Integer
    Public CompetenzaObbligatiroAnalitica As Integer

    Public GestioneDocumentiNew As Integer
    Public CausaleContabileNCDefaultAttivita2 As String
    Public CausaleContabileDefaultAttivita2 As String


    'TipoVisualizzazione
    'ScadenziarioCheckChiuso


    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Sub LeggiDati(ByVal StringaConnessione As String)
        Dim cnT As OleDbConnection
        Dim MySql As String

        cnT = New Data.OleDb.OleDbConnection(StringaConnessione)

        cnT.Open()

        MySql = "SELECT * From DatiGenerali"

        Dim cmdX As New OleDbCommand()
        cmdX.CommandText = (MySql)
        cmdX.Connection = cnT
        Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX.Read Then
            CausaleChiusura = campodb(ReadDX.Item("CausaleChiusura"))
            DataChiusura = campodbD(ReadDX.Item("DataChiusura"))
            EsercizioMese = campodb(ReadDX.Item("EsercizioMese"))
            EsercizioGiorno = campodb(ReadDX.Item("EsercizioGiorno"))
            ApprovazioneBilancio = campodbD(ReadDX.Item("ApprovazioneBilancio"))
            CausaleApertura = campodb(ReadDX.Item("CausaleApertura"))
            BilancioChiusuraMastro = campodb(ReadDX.Item("BilancioChiusuraMastro"))
            BilancioChiusuraConto = campodb(ReadDX.Item("BilancioChiusuraConto"))
            BilancioChiusuraSottoconto = campodb(ReadDX.Item("BilancioChiusuraSottoconto"))
            ProfittiPerditeMastro = campodb(ReadDX.Item("ProfittiPerditeMastro"))
            ProfittiPerditeConto = campodb(ReadDX.Item("ProfittiPerditeConto"))
            ProfittiPerditeSottoconto = campodb(ReadDX.Item("ProfittiPerditeSottoconto"))
            BilancioAperturaMastro = campodb(ReadDX.Item("BilancioAperturaMastro"))
            BilancioAperturaConto = campodb(ReadDX.Item("BilancioAperturaConto"))
            BilancioAperturaSottoconto = campodb(ReadDX.Item("BilancioAperturaSottoconto"))
            UtilePatrimonialeMastro = campodb(ReadDX.Item("UtilePatrimonialeMastro"))
            UtilePatrimonialeConto = campodb(ReadDX.Item("UtilePatrimonialeConto"))
            UtilePatrimonialeSottoconto = campodb(ReadDX.Item("UtilePatrimonialeSottoconto"))
            PerditaPatrimonialeMastro = campodb(ReadDX.Item("PerditaPatrimonialeMastro"))
            PerditaPatrimonialeConto = campodb(ReadDX.Item("PerditaPatrimonialeConto"))
            PerditaPatrimonialeSottoconto = campodb(ReadDX.Item("PerditaPatrimonialeSottoconto"))
            UtileEconomicoMastro = campodb(ReadDX.Item("UtileEconomicoMastro"))
            UtileEconomicoConto = campodb(ReadDX.Item("UtileEconomicoConto"))
            UtileEconomicoSottoconto = campodb(ReadDX.Item("UtileEconomicoSottoconto"))
            PerditaEconomicoMastro = campodb(ReadDX.Item("PerditaEconomicoMastro"))
            PerditaEconomicoConto = campodb(ReadDX.Item("PerditaEconomicoConto"))
            PerditaEconomicoSottoconto = campodb(ReadDX.Item("PerditaEconomicoSottoconto"))
            CausaleRettifica = campodb(ReadDX.Item("CausaleRettifica"))
            ArrotondaIVA = campodb(ReadDX.Item("ArrotondaIVA"))
            ClientiMastro = campodb(ReadDX.Item("ClientiMastro"))
            FornitoriMastro = campodb(ReadDX.Item("FornitoriMastro"))
            IVAMastro = campodb(ReadDX.Item("IVAMastro"))
            RegistroLiquidazioni = campodb(ReadDX.Item("RegistroLiquidazioni"))
            LiquidazioneTrimestrale = campodb(ReadDX.Item("LiquidazioneTrimestrale"))
            CodiceDetraibilita = campodb(ReadDX.Item("CodiceDetraibilita"))
            CodiceScadenza = campodb(ReadDX.Item("CodiceScadenza"))
            CodiceRitenute = campodb(ReadDX.Item("CodiceRitenute"))
            NumeroAutomaticoClienti = campodb(ReadDX.Item("NumeroAutomaticoClienti"))
            ValoreEURO = campodbN(ReadDX.Item("ValoreEURO"))
            TipoApertura = campodb(ReadDX.Item("TipoApertura"))
            EconomoMastro = campodbN(ReadDX.Item("EconomoMastro"))
            EconomoConto = campodbN(ReadDX.Item("EconomoConto"))
            EconomoSottoconto = campodbN(ReadDX.Item("EconomoSottoconto"))
            EconomoCapitolo = campodbN(ReadDX.Item("EconomoCapitolo"))
            EconomoArticolo = campodbN(ReadDX.Item("EconomoArticolo"))
            ImportoMassimo = campodbN(ReadDX.Item("ImportoMassimo"))
            CausaleGirocontoRitenute = campodb(ReadDX.Item("CausaleGirocontoRitenute"))
            ContoBollo = campodbN(ReadDX.Item("ContoBollo"))
            MastroBollo = campodbN(ReadDX.Item("MastroBollo"))
            SottoContoBollo = campodbN(ReadDX.Item("SottoContoBollo"))
            ClientiConto = campodbN(ReadDX.Item("ClientiConto"))
            FornitoriConto = campodbN(ReadDX.Item("FornitoriConto"))
            StampaDaGestioneDocumenti = campodbN(ReadDX.Item("StampaDaGestioneDocumenti"))

            MovimentiRitenute = campodbN(ReadDX.Item("MovimentiRitenute"))
            GirocontoRitenuta = campodbN(ReadDX.Item("GirocontoRitenuta"))

            FatturazionePrivati = campodbN(ReadDX.Item("FatturazionePrivati"))
            BudgetAnalitica = campodbN(ReadDX.Item("BudgetAnalitica"))

            CausaleContabileDefault = campodb(ReadDX.Item("CausaleContabileDefault"))
            CausaleContabileNCDefault = campodb(ReadDX.Item("CausaleContabileNCDefault"))

            AttivaCServPrimanoIncassi = campodbN(ReadDX.Item("AttivaCServPrimanoIncassi"))

            ModalitaPagamentoOblDoc = campodbN(ReadDX.Item("ModalitaPagamentoOblDoc"))


            TestAgyo = campodb(ReadDX.Item("TestAgyo"))
            IdAgyo = campodb(ReadDX.Item("IdAgyo"))
            PasswordAgyo = campodb(ReadDX.Item("PasswordAgyo"))

            TokenAgyo = campodb(ReadDX.Item("TokenAgyo"))


            BloccaLegameScadenzario = campodbN(ReadDX.Item("BloccaLegameScadenzario"))

            ScadenziarioCheckChiuso = campodbN(ReadDX.Item("ScadenziarioCheckChiuso"))
            TipoVisualizzazione = campodbN(ReadDX.Item("TipoVisualizzazione"))

            CompetenzaObbligatiroAnalitica = campodbN(ReadDX.Item("CompetenzaObbligatiroAnalitica"))

            GestioneDocumentiNew = campodbN(ReadDX.Item("GestioneDocumentiNew"))


            CausaleContabileNCDefaultAttivita2 = campodb(ReadDX.Item("CausaleContabileNCDefaultAttivita2"))

            CausaleContabileDefaultAttivita2 = campodb(ReadDX.Item("CausaleContabileDefaultAttivita2"))

        End If
        ReadDX.Close()
        cnT.Close()
    End Sub

    Public Sub ScriviDati(ByVal StringaConnessione As String)
        Dim cnT As OleDbConnection
        Dim MySql As String

        cnT = New Data.OleDb.OleDbConnection(StringaConnessione)

        cnT.Open()

        MySql = "UPDATE DatiGenerali SET CausaleChiusura= ?,DataChiusura= ?,EsercizioMese= ?,EsercizioGiorno= ?,ApprovazioneBilancio    = ?,CausaleApertura= ?,BilancioChiusuraMastro= ?,BilancioChiusuraConto= ?,BilancioChiusuraSottoconto= ?,ProfittiPerditeMastro= ?,ProfittiPerditeConto= ?,ProfittiPerditeSottoconto= ?,BilancioAperturaMastro= ?,BilancioAperturaConto= ?," & _
        "BilancioAperturaSottoconto= ?,UtilePatrimonialeMastro= ?,UtilePatrimonialeConto= ?,UtilePatrimonialeSottoconto= ?,PerditaPatrimonialeMastro= ?,PerditaPatrimonialeConto= ?,PerditaPatrimonialeSottoconto= ?,UtileEconomicoMastro= ?,UtileEconomicoConto= ?,UtileEconomicoSottoconto= ?,PerditaEconomicoMastro= ?,PerditaEconomicoConto= ?,PerditaEconomicoSottoconto= ?,CausaleRettifica= ?," & _
        "ArrotondaIVA= ?,ClientiMastro    = ?,FornitoriMastro= ?,IVAMastro= ?,RegistroLiquidazioni= ?,LiquidazioneTrimestrale= ?,CodiceDetraibilita= ?,CodiceScadenza= ?,CodiceRitenute= ?,NumeroAutomaticoClienti= ?,ValoreEURO = ?,TipoApertura= ?,EconomoMastro= ?,EconomoConto= ?,EconomoSottoconto = ?,EconomoCapitolo= ?,EconomoArticolo= ?,ImportoMassimo = ?,CausaleGirocontoRitenute    = ?," & _
        "ContoBollo= ?,MastroBollo= ?,SottoContoBollo= ?,ClientiConto= ?,FornitoriConto= ?,StampaDaGestioneDocumenti = ?,MovimentiRitenute =?,GirocontoRitenuta = ?, FatturazionePrivati = ?,BudgetAnalitica = ?,CausaleContabileDefault = ?,CausaleContabileNCDefault = ?, AttivaCServPrimanoIncassi = ?,ModalitaPagamentoOblDoc = ?, TestAgyo = ?,IdAgyo = ?,PasswordAgyo = ?,TokenAgyo = ?,BloccaLegameScadenzario = ?,ScadenziarioCheckChiuso = ?, TipoVisualizzazione = ?,CompetenzaObbligatiroAnalitica  = ?,GestioneDocumentiNew = ?,CausaleContabileDefaultAttivita2  = ?, CausaleContabileNCDefaultAttivita2 = ?"

        Dim cmdX As New OleDbCommand()
        cmdX.CommandText = (MySql)
        cmdX.Connection = cnT
        cmdX.Parameters.AddWithValue("@CausaleChiusura", CausaleChiusura)
        cmdX.Parameters.AddWithValue("@DataChiusura", IIf(Year(DataChiusura) > 1, DataChiusura, System.DBNull.Value))
        cmdX.Parameters.AddWithValue("@EsercizioMese", EsercizioMese)
        cmdX.Parameters.AddWithValue("@EsercizioGiorno", EsercizioGiorno)
        cmdX.Parameters.AddWithValue("@ApprovazioneBilancio", IIf(Year(ApprovazioneBilancio) > 1, ApprovazioneBilancio, System.DBNull.Value))
        cmdX.Parameters.AddWithValue("@CausaleApertura", CausaleApertura)
        cmdX.Parameters.AddWithValue("@BilancioChiusuraMastro", BilancioChiusuraMastro)
        cmdX.Parameters.AddWithValue("@BilancioChiusuraConto", BilancioChiusuraConto)
        cmdX.Parameters.AddWithValue("@BilancioChiusuraSottoconto", BilancioChiusuraSottoconto)
        cmdX.Parameters.AddWithValue("@ProfittiPerditeMastro", ProfittiPerditeMastro)
        cmdX.Parameters.AddWithValue("@ProfittiPerditeConto", ProfittiPerditeConto)
        cmdX.Parameters.AddWithValue("@ProfittiPerditeSottoconto", ProfittiPerditeSottoconto)
        cmdX.Parameters.AddWithValue("@BilancioAperturaMastro", BilancioAperturaMastro)
        cmdX.Parameters.AddWithValue("@BilancioAperturaConto", BilancioAperturaConto)
        cmdX.Parameters.AddWithValue("@BilancioAperturaSottoconto", BilancioAperturaSottoconto)
        cmdX.Parameters.AddWithValue("@UtilePatrimonialeMastro", UtilePatrimonialeMastro)
        cmdX.Parameters.AddWithValue("@UtilePatrimonialeConto", UtilePatrimonialeConto)
        cmdX.Parameters.AddWithValue("@UtilePatrimonialeSottoconto", UtilePatrimonialeSottoconto)
        cmdX.Parameters.AddWithValue("@PerditaPatrimonialeMastro", PerditaPatrimonialeMastro)
        cmdX.Parameters.AddWithValue("@PerditaPatrimonialeConto", PerditaPatrimonialeConto)
        cmdX.Parameters.AddWithValue("@PerditaPatrimonialeSottoconto", PerditaPatrimonialeSottoconto)
        cmdX.Parameters.AddWithValue("@UtileEconomicoMastro", UtileEconomicoMastro)
        cmdX.Parameters.AddWithValue("@UtileEconomicoConto", UtileEconomicoConto)
        cmdX.Parameters.AddWithValue("@UtileEconomicoSottoconto", UtileEconomicoSottoconto)
        cmdX.Parameters.AddWithValue("@PerditaEconomicoMastro", PerditaEconomicoMastro)
        cmdX.Parameters.AddWithValue("@PerditaEconomicoConto", PerditaEconomicoConto)
        cmdX.Parameters.AddWithValue("@PerditaEconomicoSottoconto", PerditaEconomicoSottoconto)
        cmdX.Parameters.AddWithValue("@CausaleRettifica", CausaleRettifica)
        cmdX.Parameters.AddWithValue("@ArrotondaIVA", ArrotondaIVA)
        cmdX.Parameters.AddWithValue("@ClientiMastro", ClientiMastro)
        cmdX.Parameters.AddWithValue("@FornitoriMastro", FornitoriMastro)
        cmdX.Parameters.AddWithValue("@IVAMastro", IVAMastro)
        cmdX.Parameters.AddWithValue("@RegistroLiquidazioni", RegistroLiquidazioni)
        cmdX.Parameters.AddWithValue("@LiquidazioneTrimestrale", LiquidazioneTrimestrale)
        cmdX.Parameters.AddWithValue("@CodiceDetraibilita", CodiceDetraibilita)
        cmdX.Parameters.AddWithValue("@CodiceScadenza", CodiceScadenza)
        cmdX.Parameters.AddWithValue("@CodiceRitenute", CodiceRitenute)
        cmdX.Parameters.AddWithValue("@NumeroAutomaticoClienti", NumeroAutomaticoClienti)
        cmdX.Parameters.AddWithValue("@ValoreEURO", ValoreEURO)
        cmdX.Parameters.AddWithValue("@TipoApertura", TipoApertura)
        cmdX.Parameters.AddWithValue("@EconomoMastro", EconomoMastro)
        cmdX.Parameters.AddWithValue("@EconomoConto", EconomoConto)
        cmdX.Parameters.AddWithValue("@EconomoSottoconto", EconomoSottoconto)
        cmdX.Parameters.AddWithValue("@EconomoCapitolo", EconomoCapitolo)
        cmdX.Parameters.AddWithValue("@EconomoArticolo", EconomoArticolo)
        cmdX.Parameters.AddWithValue("@ImportoMassimo", ImportoMassimo)
        cmdX.Parameters.AddWithValue("@CausaleGirocontoRitenute", CausaleGirocontoRitenute)
        cmdX.Parameters.AddWithValue("@ContoBollo", ContoBollo)
        cmdX.Parameters.AddWithValue("@MastroBollo", MastroBollo)
        cmdX.Parameters.AddWithValue("@SottoContoBollo", SottoContoBollo)
        cmdX.Parameters.AddWithValue("@ClientiConto", ClientiConto)
        cmdX.Parameters.AddWithValue("@FornitoriConto", FornitoriConto)
        cmdX.Parameters.AddWithValue("@StampaDaGestioneDocumenti", StampaDaGestioneDocumenti)
        cmdX.Parameters.AddWithValue("@MovimentiRitenute", MovimentiRitenute)
        cmdX.Parameters.AddWithValue("@GirocontoRitenuta", GirocontoRitenuta)
        cmdX.Parameters.AddWithValue("@FatturazionePrivati", FatturazionePrivati)
        cmdX.Parameters.AddWithValue("@BudgetAnalitica", BudgetAnalitica)

        cmdX.Parameters.AddWithValue("@CausaleContabileDefault", CausaleContabileDefault)
        cmdX.Parameters.AddWithValue("@CausaleContabileNCDefault", CausaleContabileNCDefault)
        cmdX.Parameters.AddWithValue("@AttivaCServPrimanoIncassi", AttivaCServPrimanoIncassi)

        cmdX.Parameters.AddWithValue("@ModalitaPagamentoOblDoc", ModalitaPagamentoOblDoc)


        cmdX.Parameters.AddWithValue("@TestAgyo", TestAgyo)
        cmdX.Parameters.AddWithValue("@IdAgyo", IdAgyo)
        cmdX.Parameters.AddWithValue("@PasswordAgyo", PasswordAgyo)
        cmdX.Parameters.AddWithValue("@TokenAgyo", TokenAgyo)


        cmdX.Parameters.AddWithValue("@BloccaLegameScadenzario", BloccaLegameScadenzario)

        cmdX.Parameters.AddWithValue("@ScadenziarioCheckChiuso", ScadenziarioCheckChiuso)

        cmdX.Parameters.AddWithValue("@TipoVisualizzazione", TipoVisualizzazione)

        cmdX.Parameters.AddWithValue("@CompetenzaObbligatiroAnalitica", CompetenzaObbligatiroAnalitica)

        cmdX.Parameters.AddWithValue("@GestioneDocumentiNew", GestioneDocumentiNew)

        cmdX.Parameters.AddWithValue("@CausaleContabileDefaultAttivita2", CausaleContabileDefaultAttivita2)
        cmdX.Parameters.AddWithValue("@CausaleContabileNCDefaultAttivita2", CausaleContabileNCDefaultAttivita2)

        'GestioneDocumentiNew

        'CompetenzaObbligatiroAnalitica

        cmdX.ExecuteNonQuery()
        
        cnT.Close()
    End Sub
End Class
