Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


<Serializable()> Public Class Cls_MovimentoContabile
    Public NumeroRegistrazione As Long
    Public DataRegistrazione As Date
    Public CausaleContabile As String
    Public DataDocumento As Date

    Public NumeroDocumento As String
    Public Competenza As String
    Public Descrizione As String
    Public IVASospesa As String
    Public TipoDocumento As String
    Public RegistroIVA As Long
    Public AnnoProtocollo As Long
    Public NumeroProtocollo As Long
    Public CodicePagamento As String
    Public GeneraleFinanziaria As String
    Public Trasferimento As String
    Public Economato As String
    Public AnnoCompetenza As Long
    Public MeseCompetenza As Long
    Public FatturaDiAnticipo As String
    Public NumeroBolletta As String
    Public DataBolletta As Date
    Public ModalitaPagamento As String
    Public Stornato As String
    Public CentroServizio As String
    Public Tipologia As String
    Public DataValuta As Date
    Public DataDistinta As Date
    Public DataMandato As Date
    Public DataReversale As Date
    Public NumeroDistinta As Long
    Public NumeroMandato As Long
    Public NumeroReversale As Long
    Public TipoStorno As String
    Public Bis As String
    Public PeriodoRiferimentoRettaAnno As Long
    Public PeriodoRiferimentoRettaMese As Long
    Public AnnoImpegno As Long
    Public BolloMandato As Long
    Public NumeroImpegno As Long
    Public Utente As String
    Public IDODV As Long
    Public AnnoODV As Long
    Public TipoODV As Long
    Public IdProgettoODV As Long
    Public ProgressivoNumero As Long
    Public ProgressivoAnno As Integer
    Public EspTipoPagamento As String
    Public EspNumeroRegolarizzazione As Long
    Public EspSpeseMandato As Long
    Public EspNumeroElenco As String
    Public EspDataEsecuzionePagamento As Date
    Public EspCodiceSIOPE As String
    Public EspCodiceCUP As String
    Public EspCodiceDelegatoQuietanzante As Long
    Public Cofog As String
    Public CodiceCig As String
    Public CodiceOspite As Long

    Public RifNumero As String
    Public RifData As Date
    Public EnteDestinatario As String
    Public DataRid As Date
    Public idRid As String
    Public ExportDATA As Date
    Public BolloVirtuale As Integer
    Public RegistrazioneRiferimento As Integer
    Public StatoRegistrazione As Integer

    Public Righe(40000) As Cls_MovimentiContabiliRiga


    Public Function VerificaRegistroIVA(ByVal StringaConnessione As String, ByVal StringaConnessioneTabelle As String, Optional ByVal Legami As Cls_Legami = Nothing) As Boolean
        Dim cn As OleDbConnection
        Dim Documento As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim cn1 As OleDbConnection


        cn1 = New Data.OleDb.OleDbConnection(StringaConnessioneTabelle)

        cn1.Open()



        VerificaRegistroIVA = True

        Dim cmd As New OleDbCommand()


        cmd.CommandText = "Select * From TabellaLegami Where CodicePagamento = " & NumeroRegistrazione


        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read

            Documento = campodbN(myPOSTreader.Item("CodiceDocumento"))

            Dim KD As New Cls_MovimentoContabile


            KD.Leggi(StringaConnessione, Documento)


            If KD.IVASospesa = "S" Then
                If KD.RegistroIVA > 0 Then

                    Dim cmd1 As New OleDbCommand()

                    cmd1.CommandText = "Select max(DataStampa) as MaxDt From Registri Where Tipo =  " & KD.RegistroIVA
                    cmd1.Connection = cn1
                    Dim RSt As OleDbDataReader = cmd1.ExecuteReader()
                    Do While RSt.Read
                        Dim MaxData As Date
                        MaxData = campodb(RSt.Item("MaxDt"))

                        If Format(MaxData, "yyyyMMdd") >= Format(DataRegistrazione, "yyyyMMdd") Then
                            VerificaRegistroIVA = False
                        End If
                    Loop
                End If
            End If
        Loop
        If VerificaRegistroIVA = True Then
            If Not IsNothing(Legami) Then
                Dim i As Integer

                For i = 0 To Legami.Importo.Length - 1
                    If Legami.NumeroDocumento(i) > 0 Then
                        Documento = Legami.NumeroDocumento(i)

                        Dim KD As New Cls_MovimentoContabile


                        KD.Leggi(StringaConnessione, Documento)


                        If KD.IVASospesa = "S" Then
                            If KD.RegistroIVA > 0 Then

                                Dim cmd1 As New OleDbCommand()

                                cmd1.CommandText = "Select max(DataStampa) as MaxDt From Registri Where Tipo =  " & KD.RegistroIVA
                                cmd1.Connection = cn1
                                Dim RSt As OleDbDataReader = cmd1.ExecuteReader()
                                Do While RSt.Read
                                    Dim MaxData As Date
                                    MaxData = campodb(RSt.Item("MaxDt"))

                                    If Format(MaxData, "yyyyMMdd") >= Format(DataRegistrazione, "yyyyMMdd") Then
                                        VerificaRegistroIVA = False
                                    End If
                                Loop
                            End If
                        End If
                    End If
                Next
            End If
        End If

        cn.Close()
        cn1.Close()
    End Function

    Public Function VerificaValidita(ByVal StringaConnessioneGenerale As String, ByVal StringaConnessioneTabelle As String, Optional ByVal Tipo As String = "") As String


        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim MyLegami As New Cls_Legami



        XDatiGenerali.LeggiDati(StringaConnessioneTabelle)


        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(DataRegistrazione, "yyyyMMdd") Then
            VerificaValidita = "Non posso inserire o modificare registrazione,<BR/> gi� effettuato la chiusura"
            Exit Function
        End If

        If Tipo = "" Then
            If Math.Abs(Modulo.MathRound(ImportoRegistrazioneDocumento(StringaConnessioneTabelle) - ImportoRitenuta(StringaConnessioneTabelle), 2)) < Modulo.MathRound(MyLegami.TotaleLegame(StringaConnessioneGenerale, NumeroRegistrazione), 2) Then
                VerificaValidita = "L importo nette dev essere uguale o maggiore dell importo legame :" & Math.Abs(Modulo.MathRound(ImportoRegistrazioneDocumento(StringaConnessioneTabelle) - ImportoRitenuta(StringaConnessioneTabelle), 2)) & " -  " & Modulo.MathRound(MyLegami.TotaleLegame(StringaConnessioneGenerale, NumeroRegistrazione), 2)
                Exit Function
            End If
        End If

        If VerificaRegistroIVA(StringaConnessioneGenerale, StringaConnessioneTabelle) = False Then
            VerificaValidita = "Non posso inserire o modificare registro iva gi� stampato"
            Exit Function
        End If

        If Bollato(StringaConnessioneGenerale) = True Then
            VerificaValidita = "Gi� creato il giornale bollato"
            Exit Function
        End If

        VerificaValidita = "OK"
    End Function
    Public Sub New()
        NumeroRegistrazione = 0
        DataRegistrazione = Nothing
        CausaleContabile = ""
        DataDocumento = Nothing

        NumeroDocumento = ""
        Competenza = ""
        Descrizione = ""
        IVASospesa = ""
        TipoDocumento = ""
        RegistroIVA = 0
        AnnoProtocollo = 0
        NumeroProtocollo = 0
        CodicePagamento = ""
        GeneraleFinanziaria = ""
        Trasferimento = ""
        Economato = ""
        AnnoCompetenza = 0
        MeseCompetenza = 0
        FatturaDiAnticipo = ""
        NumeroBolletta = ""
        DataBolletta = Nothing
        ModalitaPagamento = ""
        Stornato = ""
        CentroServizio = ""
        Tipologia = ""
        DataValuta = Nothing
        DataDistinta = Nothing
        DataMandato = Nothing
        DataReversale = Nothing
        NumeroDistinta = 0
        NumeroMandato = 0
        NumeroReversale = 0
        TipoStorno = ""
        Bis = ""
        PeriodoRiferimentoRettaAnno = 0
        PeriodoRiferimentoRettaMese = 0
        AnnoImpegno = 0
        BolloMandato = 0
        NumeroImpegno = 0
        IDODV = 0
        AnnoODV = 0
        TipoODV = 0
        IdProgettoODV = 0
        ProgressivoNumero = 0
        ProgressivoAnno = 0

        EspTipoPagamento = ""
        EspNumeroRegolarizzazione = 0
        EspSpeseMandato = 0
        EspNumeroElenco = ""
        EspDataEsecuzionePagamento = Nothing
        EspCodiceSIOPE = ""
        EspCodiceCUP = ""
        EspCodiceDelegatoQuietanzante = 0
        Cofog = ""
        CodiceCig = 0
        CodiceOspite = 0

        RifNumero = ""
        RifData = Nothing
        EnteDestinatario = ""

        DataRid = Nothing
        idRid = ""
        ExportDATA = Nothing
        BolloVirtuale = 0

        RegistrazioneRiferimento = 0
        StatoRegistrazione = 0


    End Sub

    Public Function Bollato(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        Bollato = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand() '
        cmd.CommandText = ("Select * from MovimentiContabiliRiga where Numero = ?")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodbN(myPOSTreader.Item("RigaBollato")) > 0 Then
                Bollato = True
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function





    Function Scrivi(ByVal StringaConnessione As String, ByVal iNNumeroRegistrazione As Long, Optional ByVal ArchivioTemp As Boolean = False) As Boolean

        Dim Verifica As Integer
        Dim TotDare As Double
        Dim TotAvere As Double

        For Verifica = 0 To Righe.Length - 1

            If Not IsNothing(Righe(Verifica)) Then

                If Righe(Verifica).DareAvere = "D" Then
                    TotDare = TotDare + Righe(Verifica).Importo
                End If
                If Righe(Verifica).DareAvere = "A" Then
                    TotAvere = TotAvere + Righe(Verifica).Importo
                End If

            Else
                Exit For
            End If
        Next

        If Modulo.MathRound(TotAvere, 2) <> Modulo.MathRound(TotDare, 2) Then
            Return False
            Exit Function
        End If

        For Verifica = 0 To Righe.Length - 1

            If Not IsNothing(Righe(Verifica)) Then
                If Righe(Verifica).Tipo = "IV" Then
                    If Righe(Verifica).CodiceIVA = "" Then
                        Return False
                        Exit Function
                    End If
                End If
            End If
        Next



        Dim cn As OleDbConnection


        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        If ArchivioTemp = False Then
            Dim cmd As New OleDbCommand()


            cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                               "NumeroRegistrazione = ? ")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", iNNumeroRegistrazione)

            cmd.Connection = cn

            cmd.Transaction = Transan

            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                NumeroRegistrazione = iNNumeroRegistrazione
                Modifica = True
            End If
            myPOSTreader.Close()

            If Modifica = False Then
                Dim APPOGGIO As String = ""
                Dim I As Integer
                For I = 1 To 10
                    APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
                Next
                Dim cmdIns As New OleDbCommand()
                MySql = "DECLARE @NUMERO integer DECLARE @NUMEROPA integer" & _
                        " SET @NUMERO = (SELECT     max(NumeroRegistrazione) + 1  FROM  MovimentiContabiliTesta)" & _
                        " IF @NUMERO IS NULL SET @NUMERO = 1" & _
                        " SET @NUMEROPA = (SELECT     max(ProgressivoNumero) + 1  FROM  MovimentiContabiliTesta Where ProgressivoAnno = " & Year(DataRegistrazione) & ")" & _
                        " IF @NUMEROPA IS NULL SET @NUMEROPA = 1" & _
                        " INSERT INTO MovimentiContabiliTesta (NumeroRegistrazione,ProgressivoNumero,ProgressivoAnno,UTENTE) VALUES (@NUMERO,@NUMEROPA," & Year(DataRegistrazione) & ",'" & APPOGGIO & "')"
                cmdIns.CommandText = MySql
                cmdIns.Connection = cn
                cmdIns.Transaction = Transan
                cmdIns.ExecuteNonQuery()

                Dim cmdT As New OleDbCommand()
                cmdT.CommandText = ("select MAX(NumeroRegistrazione) from MovimentiContabiliTesta WHERE UTENTE = '" & APPOGGIO & "'")
                cmdT.Connection = cn
                cmdT.Transaction = Transan
                Dim myR As OleDbDataReader = cmdT.ExecuteReader()
                If myR.Read Then
                    NumeroRegistrazione = campodb(myR.Item(0))
                End If
                myR.Close()
            End If
        Else

            Dim cmd As New OleDbCommand()


            cmd.CommandText = ("select * from Temp_MovimentiContabiliTesta where " & _
                               "NumeroRegistrazione = ? ")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", iNNumeroRegistrazione)

            cmd.Connection = cn

            cmd.Transaction = Transan

            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                NumeroRegistrazione = iNNumeroRegistrazione
                Modifica = True
            End If
            myPOSTreader.Close()

            If Modifica = False Then
                Dim cmdUltEF As New OleDbCommand()
                cmdUltEF.CommandText = ("select MAX(NumeroRegistrazione) from MovimentiContabiliTesta")
                cmdUltEF.Connection = cn
                cmdUltEF.Transaction = Transan
                Dim myRUltEF As OleDbDataReader = cmdUltEF.ExecuteReader()
                If myRUltEF.Read Then
                    NumeroRegistrazione = campodbN(myRUltEF.Item(0)) + 1
                End If
                myRUltEF.Close()

                Dim cmdUltTemp As New OleDbCommand()
                cmdUltTemp.CommandText = ("select MAX(NumeroRegistrazione) from Temp_MovimentiContabiliTesta")
                cmdUltTemp.Connection = cn
                cmdUltTemp.Transaction = Transan
                Dim myTemp As OleDbDataReader = cmdUltTemp.ExecuteReader()
                If myTemp.Read Then
                    If NumeroRegistrazione <= campodbN(myTemp.Item(0)) Then
                        NumeroRegistrazione = campodbN(myTemp.Item(0)) + 1
                    End If
                End If
                myTemp.Close()



                Dim cmdIns As New OleDbCommand()
                MySql = " DECLARE @NUMEROPA integer " & _
                        " SET @NUMEROPA = (SELECT     max(ProgressivoNumero) + 1  FROM  MovimentiContabiliTesta Where ProgressivoAnno = " & Year(DataRegistrazione) & ")" & _
                        " IF @NUMEROPA IS NULL SET @NUMEROPA = 1" & _
                        " INSERT INTO Temp_MovimentiContabiliTesta (NumeroRegistrazione,ProgressivoNumero,ProgressivoAnno,UTENTE) VALUES (?,@NUMEROPA,?,?)"
                cmdIns.CommandText = MySql
                cmdIns.Connection = cn
                cmdIns.Transaction = Transan
                cmdIns.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
                cmdIns.Parameters.AddWithValue("@ProgressivoAnno", Year(DataRegistrazione))
                'Year(DataRegistrazione)
                cmdIns.Parameters.AddWithValue("@Utente", Utente)
                cmdIns.ExecuteNonQuery()
            End If
        End If

        If ArchivioTemp = False Then
            MySql = "UPDATE MovimentiContabiliTesta SET "
        Else
            MySql = "UPDATE Temp_MovimentiContabiliTesta SET "
        End If
        MySql = MySql & " DataRegistrazione = ?, " & _
         " CausaleContabile = ?, " & _
         " DataDocumento = ?, " & _
         " NumeroDocumento = ?, " & _
         " Competenza = ?, " & _
         " Descrizione = ?, " & _
         " IVASospesa = ?, " & _
         " TipoDocumento = ?, " & _
         " RegistroIVA = ?, " & _
         " AnnoProtocollo = ?, " & _
         " NumeroProtocollo = ?, " & _
         " CodicePagamento = ?, " & _
         " GeneraleFinanziaria = ?, " & _
         " Trasferimento = ?, " & _
         " Economato = ?, " & _
         " AnnoCompetenza = ?, " & _
         " MeseCompetenza = ?, " & _
         " FatturaDiAnticipo = ?, " & _
         " NumeroBolletta = ?, " & _
         " DataBolletta = ?, " & _
         " ModalitaPagamento = ?, " & _
         " Stornato = ?, " & _
         " CentroServizio = ?, " & _
         " Tipologia = ?, " & _
         " DataValuta = ?, " & _
         " DataDistinta = ?, " & _
         " DataMandato = ?, " & _
         " DataReversale = ?, " & _
         " NumeroDistinta = ?, " & _
         " NumeroMandato = ?, " & _
         " NumeroReversale = ?, " & _
         " TipoStorno = ?, " & _
         " Bis = ?, " & _
         " PeriodoRiferimentoRettaAnno = ?, " & _
         " PeriodoRiferimentoRettaMese = ?, " & _
         " AnnoImpegno = ?, " & _
         " BolloMandato = ?, " & _
         " NumeroImpegno = ?, " & _
         " Utente = ?, " & _
         " DataAggiornamento = ?, " & _
         " IDODV = ?, " & _
         " AnnoODV = ?, " & _
         " TipoODV = ?, " & _
         " IdProgettoODV = ?, " & _
         " EspTipoPagamento = ?, " & _
         " EspNumeroRegolarizzazione = ?, " & _
         " EspNumeroElenco = ?, " & _
         " EspSpeseMandato = ?, " & _
         " EspDataEsecuzionePagamento = ?, " & _
         " EspCodiceSIOPE = ?, " & _
         " EspCodiceCUP = ?, " & _
         " EspCodiceDelegatoQuietanzante = ?, " & _
         " Cofog = ?, " & _
         " CodiceCig = ?, " & _
         " CodiceOspite  = ?, " & _
         " RifNumero = ?, " & _
         " RifData = ?, " & _
         " DataRid = ?, " & _
         " idRid= ?, " & _
         " ExportDATA = ?, " & _
         " BolloVirtuale = ?, " & _
         " RegistrazioneRiferimento = ?, " & _
         " StatoRegistrazione = ? " & _
         " Where NumeroRegistrazione = ? "



        Dim cmd1 As New OleDbCommand()


        If NumeroProtocollo = 0 And AnnoProtocollo > 0 And RegistroIVA > 0 And ArchivioTemp = False Then
            NumeroProtocollo = MaxProtocollo(StringaConnessione, AnnoProtocollo, RegistroIVA, cn, Transan) + 1
        End If
        If NumeroProtocollo = 0 And AnnoProtocollo > 0 And RegistroIVA > 0 And ArchivioTemp = True Then
            NumeroProtocollo = MaxProtocolloTempTransazione(StringaConnessione, AnnoProtocollo, RegistroIVA, cn, Transan) + 1

            If NumeroProtocollo < MaxProtocollo(StringaConnessione, AnnoProtocollo, RegistroIVA, cn, Transan) + 1 Then
                NumeroProtocollo = MaxProtocollo(StringaConnessione, AnnoProtocollo, RegistroIVA, cn, Transan) + 1
            End If
        End If


        If NumeroDocumento = "" Then
            NumeroDocumento = NumeroProtocollo
        End If


        cmd1.CommandText = MySql
        cmd1.Transaction = Transan
        cmd1.Parameters.AddWithValue("@DataRegistrazione", IIf(Year(DataRegistrazione) > 1, DataRegistrazione, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@CausaleContabile", CausaleContabile)
        cmd1.Parameters.AddWithValue("@DataDocumento", IIf(Year(DataDocumento) > 1, DataDocumento, System.DBNull.Value))

        cmd1.Parameters.AddWithValue("@NumeroDocumento", NumeroDocumento)
        cmd1.Parameters.AddWithValue("@Competenza", Competenza)
        'cmd1.Parameters.AddWithValue("@Descrizione", IIf(Len(Descrizione) > 250, Mid(Descrizione, 1, 250), Descrizione))
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@IVASospesa", IVASospesa)
        cmd1.Parameters.AddWithValue("@TipoDocumento", TipoDocumento)
        cmd1.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd1.Parameters.AddWithValue("@AnnoProtocollo", AnnoProtocollo)



        cmd1.Parameters.AddWithValue("@NumeroProtocollo", NumeroProtocollo)
        cmd1.Parameters.AddWithValue("@CodicePagamento", CodicePagamento)
        cmd1.Parameters.AddWithValue("@GeneraleFinanziaria", GeneraleFinanziaria)
        cmd1.Parameters.AddWithValue("@Trasferimento", Trasferimento)
        cmd1.Parameters.AddWithValue("@Economato", Economato)
        cmd1.Parameters.AddWithValue("@AnnoCompetenza", AnnoCompetenza)
        cmd1.Parameters.AddWithValue("@MeseCompetenza", MeseCompetenza)
        cmd1.Parameters.AddWithValue("@FatturaDiAnticipo", FatturaDiAnticipo)
        cmd1.Parameters.AddWithValue("@NumeroBolletta", NumeroBolletta)
        cmd1.Parameters.AddWithValue("@DataBolletta", IIf(Year(DataBolletta) > 1, DataBolletta, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd1.Parameters.AddWithValue("@Stornato", Stornato)
        cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd1.Parameters.AddWithValue("@Tipologia", Tipologia)
        cmd1.Parameters.AddWithValue("@DataValuta", IIf(Year(DataValuta) > 1, DataValuta, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@DataDistinta", IIf(Year(DataDistinta) > 1, DataDistinta, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@DataMandato", IIf(Year(DataMandato) > 1, DataMandato, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@DataReversale", IIf(Year(DataReversale) > 1, DataReversale, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@NumeroDistinta", NumeroDistinta)
        cmd1.Parameters.AddWithValue("@NumeroMandato", NumeroMandato)
        cmd1.Parameters.AddWithValue("@NumeroReversale", NumeroReversale)
        cmd1.Parameters.AddWithValue("@TipoStorno", TipoStorno)
        cmd1.Parameters.AddWithValue("@Bis", Bis)
        cmd1.Parameters.AddWithValue("@PeriodoRiferimentoRettaAnno", PeriodoRiferimentoRettaAnno)
        cmd1.Parameters.AddWithValue("@PeriodoRiferimentoRettaMese", PeriodoRiferimentoRettaMese)
        cmd1.Parameters.AddWithValue("@AnnoImpegno", AnnoImpegno)
        cmd1.Parameters.AddWithValue("@BolloMandato", BolloMandato)
        cmd1.Parameters.AddWithValue("@NumeroImpegno", NumeroImpegno)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd1.Parameters.AddWithValue("@IDODV", IDODV)
        cmd1.Parameters.AddWithValue("@AnnoODV", AnnoODV)
        cmd1.Parameters.AddWithValue("@TipoODV", TipoODV)
        cmd1.Parameters.AddWithValue("@IdProgettoODV", IdProgettoODV)
        cmd1.Parameters.AddWithValue("@EspTipoPagamento", EspTipoPagamento)
        cmd1.Parameters.AddWithValue("@EspNumeroRegolarizzazione", EspNumeroRegolarizzazione)
        cmd1.Parameters.AddWithValue("@EspNumeroElenco", EspNumeroElenco)
        cmd1.Parameters.AddWithValue("@EspSpeseMandato", EspSpeseMandato)
        cmd1.Parameters.AddWithValue("@EspDataEsecuzionePagamento", IIf(Year(EspDataEsecuzionePagamento) > 1, EspDataEsecuzionePagamento, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@EspCodiceSIOPE", EspCodiceSIOPE)
        cmd1.Parameters.AddWithValue("@EspCodiceCUP", EspCodiceCUP)
        cmd1.Parameters.AddWithValue("@EspCodiceDelegatoQuietanzante", EspCodiceDelegatoQuietanzante)
        cmd1.Parameters.AddWithValue("@Cofog", Cofog)
        cmd1.Parameters.AddWithValue("@CodiceCig", CodiceCig)
        cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd1.Parameters.AddWithValue("@RifNumero", RifNumero)
        cmd1.Parameters.AddWithValue("@RifData", IIf(Year(RifData) > 1, RifData, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@DataRid", IIf(Year(DataRid) > 1, DataRid, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@idRid", idRid)
        cmd1.Parameters.AddWithValue("@ExportDATA", IIf(Year(ExportDATA) > 1, ExportDATA, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@BolloVirtuale", BolloVirtuale)
        cmd1.Parameters.AddWithValue("@RegistrazioneRiferimento", RegistrazioneRiferimento)
        cmd1.Parameters.AddWithValue("@StatoRegistrazione", StatoRegistrazione)


        'ExportDATA
        cmd1.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd1.Connection = cn
        Try
            cmd1.ExecuteNonQuery()
        Catch ex As Exception

            If Modifica = False Then
                cn.Close()
                Return False
                Exit Function
            Else
                Transan.Rollback()
                cn.Close()
                Return False
                Exit Function
            End If
        End Try

        If ArchivioTemp = False Then
            Dim cmdDelete As New OleDbCommand()
            cmdDelete.CommandText = ("Delete from MovimentiContabiliRiga where " & _
                               "Numero = ? ")
            cmdDelete.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
            cmdDelete.Connection = cn
            cmdDelete.Transaction = Transan
            cmdDelete.ExecuteNonQuery()
        Else

            Dim cmdDelete As New OleDbCommand()
            cmdDelete.CommandText = ("Delete from Temp_MovimentiContabiliRiga where " & _
                               "Numero = ? ")
            cmdDelete.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
            cmdDelete.Connection = cn
            cmdDelete.Transaction = Transan
            cmdDelete.ExecuteNonQuery()
        End If


        Dim x As Integer
        Dim RigaMax As Integer = 0
                
        For x = 0 To Righe.Length - 1
            If Not IsNothing(Righe(x)) Then
                If Righe(x).RigaRegistrazione > 0 Then
                    RigaMax = Righe(x).RigaRegistrazione
                End If
            End If
        Next

        For x = 0 To Righe.Length - 1

            If Not IsNothing(Righe(x)) Then
                Righe(x).Utente = Utente
                Righe(x).Numero = NumeroRegistrazione

                If Righe(x).RigaRegistrazione = 0 Then
                    RigaMax = RigaMax + 1
                    Righe(x).RigaRegistrazione = RigaMax                    
                End If

                If Righe(x).Scrivi(cn, Transan, ArchivioTemp) = False Then
                    cn.Close()
                    Return False
                    Exit Function
                End If
            Else
                Exit For
            End If
        Next

        Transan.Commit()
        cn.Close()

        Return True

    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Function MaxDataRegistroIVA(ByVal StringaConnessione As String, ByVal Anno As Long, ByVal RegistroIVA As Long) As Date
        Dim cn1 As OleDbConnection

        cn1 = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn1.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(DataRegistrazione) from MovimentiContabiliTesta Where AnnoProtocollo = ?  and RegistroIVA = ?")
        cmd.Parameters.AddWithValue("@AnnoProtocollo", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn1

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Date
            If campodb(myPOSTreader.Item(0)) = "" Then
                Appoggio = DateSerial(Anno, 1, 1)
            Else
                Appoggio = campodb(myPOSTreader.Item(0))
            End If

            myPOSTreader.Close()
            cn1.Close()
            Return Appoggio
        End If
        cn1.Close()
    End Function

    Function MaxProtocollo(ByVal StringaConnessione As String, ByVal Anno As Long, ByVal RegistroIVA As Long, ByRef cn As OleDbConnection, ByRef Transa As OleDbTransaction) As Long
        'Dim cn As OleDbConnection

        'cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        'cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroProtocollo) from MovimentiContabiliTesta Where AnnoProtocollo = ?  and RegistroIVA = ?")
        cmd.Parameters.AddWithValue("@AnnoProtocollo", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Transaction = Transa
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Long
            Appoggio = Val(campodb(myPOSTreader.Item(0)))
            Return Appoggio
        End If
        'cn.Close()
    End Function

    Function MaxProgressivoAnno(ByVal StringaConnessione As String, ByVal Anno As Long) As Long
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(ProgressivoNumero) from MovimentiContabiliTesta Where ProgressivoAnno = ?  ")
        cmd.Parameters.AddWithValue("@ProgressivoAnno", Anno)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Long
            Appoggio = Val(campodb(myPOSTreader.Item(0)))
            cn.Close()
            Return Appoggio
        End If
        cn.Close()
    End Function



    Function MaxProtocolloTemp(ByVal StringaConnessione As String, ByVal Anno As Long, ByVal RegistroIVA As Long) As Long
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroProtocollo) from Temp_MovimentiContabiliTesta Where AnnoProtocollo = ?  and RegistroIVA = ?")
        cmd.Parameters.AddWithValue("@AnnoProtocollo", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Long
            Appoggio = Val(campodb(myPOSTreader.Item(0)))
            cn.Close()
            Return Appoggio
        End If
        cn.Close()
    End Function

    Function MaxProtocolloTempTransazione(ByVal StringaConnessione As String, ByVal Anno As Long, ByVal RegistroIVA As Long, ByRef cn As OleDbConnection, ByRef Transa As OleDbTransaction) As Long
        'Dim cn As OleDbConnection

        'cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        'cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroProtocollo) from Temp_MovimentiContabiliTesta Where AnnoProtocollo = ?  and RegistroIVA = ?")
        cmd.Parameters.AddWithValue("@AnnoProtocollo", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Transaction = Transa
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Long
            Appoggio = Val(campodb(myPOSTreader.Item(0)))
            Return Appoggio
        End If
        'cn.Close()
    End Function

    Function MaxProtocollo(ByVal StringaConnessione As String, ByVal Anno As Long, ByVal RegistroIVA As Long) As Long
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroProtocollo) from MovimentiContabiliTesta Where AnnoProtocollo = ?  and RegistroIVA = ?")
        cmd.Parameters.AddWithValue("@AnnoProtocollo", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim Appoggio As Long
            Appoggio = Val(campodb(myPOSTreader.Item(0)))
            cn.Close()
            Return Appoggio
        End If
        cn.Close()
    End Function
    Function ImportoRitenuta(ByVal StringaConnessione As String) As Double
        Dim I As Integer
        Dim TotImp As Double
        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Leggi(StringaConnessione, CausaleContabile)


        TotImp = 0
        For I = 0 To 100
            If Not IsNothing(Righe(I)) Then
                If campodb(Righe(I).Tipo) = "RI" Then
                    If campodb(Righe(I).DareAvere) = "D" Then
                        TotImp = TotImp + Righe(I).Importo
                    End If
                End If
            End If
        Next
        Return TotImp
    End Function

    Function ImportoRegistrazioneDocumento(ByVal StringaConnessione As String) As Double
        Dim I As Integer
        Dim TotImp As Double
        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Leggi(StringaConnessione, CausaleContabile)


        TotImp = 0
        If MyCausale.Tipo = "I" Or MyCausale.Tipo = "R" Then
            For I = 0 To 600
                If Not IsNothing(Righe(I)) Then
                    If campodb(Righe(I).Tipo) = "IV" Then
                        If MyCausale.VenditaAcquisti = "V" Then
                            If Righe(I).DareAvere = "D" Then
                                TotImp = TotImp - CDbl(Righe(I).Importo + Righe(I).Imponibile)
                            Else
                                TotImp = TotImp + Righe(I).Importo + Righe(I).Imponibile
                            End If
                        Else
                            If Righe(I).DareAvere = "A" Then
                                TotImp = TotImp - CDbl(Righe(I).Importo + Righe(I).Imponibile)
                            Else
                                TotImp = TotImp + Righe(I).Importo + Righe(I).Imponibile
                            End If
                        End If
                    End If
                Else
                    Exit For
                End If
            Next
        Else
            For I = 0 To 600
                If Not IsNothing(Righe(I)) Then
                    If Righe(I).DareAvere = "D" Then
                        TotImp = TotImp + Righe(I).Importo
                    End If
                End If
            Next
        End If

        Return TotImp
    End Function
    Function ImportoDocumento(ByVal StringaConnessione As String) As Double
        Dim I As Integer
        Dim TotImp As Double
        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Leggi(StringaConnessione, CausaleContabile)


        TotImp = 0
        For I = 0 To 100
            If Not IsNothing(Righe(I)) Then
                If campodb(Righe(I).Tipo) = "IV" Then
                    If MyCausale.VenditaAcquisti = "V" Then
                        If Righe(I).DareAvere = "D" Then
                            TotImp = TotImp - (Righe(I).Importo + Righe(I).Imponibile)
                        Else
                            TotImp = TotImp + Righe(I).Importo + Righe(I).Imponibile
                        End If
                    Else
                        If Righe(I).DareAvere = "A" Then
                            TotImp = TotImp - (Righe(I).Importo + Righe(I).Imponibile)
                        Else
                            TotImp = TotImp + Righe(I).Importo + Righe(I).Imponibile
                        End If
                    End If
                End If
            Else
                Exit For
            End If
        Next
        Return TotImp
    End Function

    Public Function VerificaElimina(ByVal StringaConnessioneGenerale As String, ByVal StringaConnessioneTabelle As String) As String

        Dim Legami As New Cls_Legami

        If Legami.TotaleLegame(StringaConnessioneGenerale, NumeroRegistrazione) > 0 Then
            VerificaElimina = "Non Posso elimare esiste uno o piu incassi/pagamenti per questo documento"
            Exit Function
        End If

        Dim Lt As New Cls_CausaleContabile

        Lt.Codice = CausaleContabile
        Lt.Leggi(StringaConnessioneTabelle, CausaleContabile)


        If Lt.RegistroIVA > 0 Then
            Dim TabRegistri As New Cls_RegistriIVAanno

            TabRegistri.Anno = 0
            TabRegistri.Leggi(StringaConnessioneTabelle, Lt.RegistroIVA, AnnoProtocollo)
            If TabRegistri.Anno = 0 Then
                VerificaElimina = "Manca il Registro IVA per l'anno del protocollo IVA"
                Exit Function
            Else
                If Format(TabRegistri.DataStampa, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd") Then
                    VerificaElimina = "Data registrazione inferiore data stampa registro IVA - " & Format(TabRegistri.DataStampa, "dd/MM/yyyy")

                    Exit Function
                End If
            End If
        End If
        Dim KTDtGen As New Cls_DatiGenerali

        KTDtGen.LeggiDati(StringaConnessioneTabelle)


        If Format(KTDtGen.DataChiusura, "yyyymmdd") >= Format(DataRegistrazione, "yyyymmdd") Then
            VerificaElimina = "Non posso modificare o inserire registrazione, gi� effettuato la chiusura"
            Exit Function
        End If



        If Bollato(StringaConnessioneGenerale) = True Then
            VerificaElimina = "Gi� creato il giornale bollato"
            Exit Function
        End If


        If VerificaRegistroIVA(StringaConnessioneGenerale, StringaConnessioneTabelle) = False Then
            VerificaElimina = "Non posso eliminare registro iva gi� stampato"
            Exit Function
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessioneGenerale)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From PrenotazioniUsate Where " & _
                           "NumeroRegistrazione = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        Dim MyRead As OleDbDataReader = cmd.ExecuteReader()
        If MyRead.Read Then
            VerificaElimina = "Errore non posso eliminare registrazione legata a prenotazione"
            Exit Function
        End If
        cn.Close()
        VerificaElimina = "OK"
    End Function

    Sub EliminaRegistrazione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from MovimentiContabiliTesta where " & _
                           "NumeroRegistrazione = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim cmdR As New OleDbCommand()
        cmdR.CommandText = ("Delete  from MovimentiContabiliRiga where " & _
                           "Numero = ? ")
        cmdR.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdR.Connection = cn
        cmdR.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function PiuCentroServizi(ByVal StringaConnessione As String, ByVal xNumeroRegistrazione As Long, Optional ByVal ArchivioTemp As Boolean = False) As Boolean
        Dim cn As OleDbConnection


        PiuCentroServizi = False
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If ArchivioTemp Then
            cmd.CommandText = ("select CENTROSERVIZIO from Temp_MovimentiContabiliRiga where RigaDaCausale = 3 And  " & _
                               "Numero = ? Group by CENTROSERVIZIO")
        Else
            cmd.CommandText = ("select CENTROSERVIZIO from MovimentiContabiliRiga where RigaDaCausale = 3 And  " & _
                               "Numero = ? Group by CENTROSERVIZIO")
        End If


        cmd.Parameters.AddWithValue("@NumeroRegistrazione", xNumeroRegistrazione)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            If VerReader.Read Then
                PiuCentroServizi = True
            End If
        End If
        VerReader.Close()


    End Function

    Sub LeggiRegistrazionePerNumeroProtocollo(ByVal StringaConnessione As String, ByVal ParamRegistroIVa As Integer, ByVal ParamNumeroProtocollo As Integer, ByVal ParamAnnoProtocollo As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                           "RegistroIVA = ? And AnnoProtocollo = ? And NumeroProtocollo = ? ")
        cmd.Parameters.AddWithValue("@RegistroIVa", ParamRegistroIVa)
        cmd.Parameters.AddWithValue("@AnnoProtocollo", ParamAnnoProtocollo)
        cmd.Parameters.AddWithValue("@NumeroProtocollo", ParamNumeroProtocollo)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Leggi(StringaConnessione, campodbN(VerReader.Item("NumeroRegistrazione")))
        End If
        VerReader.Close()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal xNumeroRegistrazione As Long, Optional ByVal ArchivioTemp As Boolean = False)
        Dim cn As OleDbConnection


        NumeroRegistrazione = xNumeroRegistrazione
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If ArchivioTemp Then
            cmd.CommandText = ("select * from Temp_MovimentiContabiliTesta where " & _
                               "NumeroRegistrazione = ? ")
        Else
            cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                               "NumeroRegistrazione = ? ")
        End If


        cmd.Parameters.AddWithValue("@NumeroRegistrazione", xNumeroRegistrazione)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If Not VerReader.Read Then
            NumeroRegistrazione = 0
            VerReader.Close()
            cn.Close()
            Exit Sub
        End If
        VerReader.Close()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            NumeroRegistrazione = myPOSTreader.Item("NumeroRegistrazione")
            If IsDate(myPOSTreader.Item("DataRegistrazione")) Then
                DataRegistrazione = myPOSTreader.Item("DataRegistrazione")
            Else
                DataRegistrazione = Nothing
            End If
            CausaleContabile = campodb(myPOSTreader.Item("CausaleContabile"))
            If IsDate(myPOSTreader.Item("DataDocumento")) Then
                DataDocumento = myPOSTreader.Item("DataDocumento")
            Else
                DataDocumento = Nothing
            End If

            NumeroDocumento = campodb(myPOSTreader.Item("NumeroDocumento"))
            Competenza = campodb(myPOSTreader.Item("Competenza"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            IVASospesa = campodb(myPOSTreader.Item("IVASospesa"))
            TipoDocumento = campodb(myPOSTreader.Item("TipoDocumento"))
            If IsDBNull(myPOSTreader.Item("RegistroIVA")) Then
                RegistroIVA = 0
            Else
                RegistroIVA = myPOSTreader.Item("RegistroIVA")
            End If

            If IsDBNull(myPOSTreader.Item("AnnoProtocollo")) Then
                AnnoProtocollo = 0
            Else
                AnnoProtocollo = myPOSTreader.Item("AnnoProtocollo")
            End If
            If IsDBNull(myPOSTreader.Item("NumeroProtocollo")) Then
                NumeroProtocollo = 0
            Else
                NumeroProtocollo = myPOSTreader.Item("NumeroProtocollo")
            End If
            CodicePagamento = campodb(myPOSTreader.Item("CodicePagamento"))
            GeneraleFinanziaria = campodb(myPOSTreader.Item("GeneraleFinanziaria"))
            Trasferimento = campodb(myPOSTreader.Item("Trasferimento"))
            Economato = campodb(myPOSTreader.Item("Economato"))
            If IsDBNull(myPOSTreader.Item("AnnoCompetenza")) Then
                AnnoCompetenza = 0
            Else
                AnnoCompetenza = myPOSTreader.Item("AnnoCompetenza")
            End If
            If IsDBNull(myPOSTreader.Item("MeseCompetenza")) Then
                MeseCompetenza = 0
            Else
                MeseCompetenza = myPOSTreader.Item("MeseCompetenza")
            End If
            FatturaDiAnticipo = campodb(myPOSTreader.Item("FatturaDiAnticipo"))
            NumeroBolletta = campodb(myPOSTreader.Item("NumeroBolletta"))

            If IsDate(myPOSTreader.Item("DataBolletta")) Then
                DataBolletta = myPOSTreader.Item("DataBolletta")
            Else
                DataBolletta = Nothing
            End If

            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            Stornato = campodb(myPOSTreader.Item("Stornato"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))

            If IsDate(myPOSTreader.Item("DataValuta")) Then
                DataValuta = myPOSTreader.Item("DataValuta")
            Else
                DataValuta = Nothing
            End If

            If IsDate(myPOSTreader.Item("DataDistinta")) Then
                DataDistinta = myPOSTreader.Item("DataDistinta")
            Else
                DataDistinta = Nothing
            End If

            If IsDate(myPOSTreader.Item("DataMandato")) Then
                DataMandato = myPOSTreader.Item("DataMandato")
            Else
                DataMandato = Nothing
            End If

            If IsDate(myPOSTreader.Item("DataReversale")) Then
                DataReversale = myPOSTreader.Item("DataReversale")
            Else
                DataReversale = Nothing
            End If

            NumeroDistinta = Val(campodb(myPOSTreader.Item("NumeroDistinta")))
            NumeroMandato = Val(campodb(myPOSTreader.Item("NumeroMandato")))
            NumeroReversale = Val(campodb(myPOSTreader.Item("NumeroReversale")))
            TipoStorno = campodb(myPOSTreader.Item("TipoStorno"))
            Bis = campodb(myPOSTreader.Item("Bis"))
            PeriodoRiferimentoRettaAnno = Val(campodb(myPOSTreader.Item("PeriodoRiferimentoRettaAnno")))
            PeriodoRiferimentoRettaMese = Val(campodb(myPOSTreader.Item("PeriodoRiferimentoRettaMese")))
            AnnoImpegno = Val(campodb(myPOSTreader.Item("AnnoImpegno")))
            BolloMandato = Val(campodb(myPOSTreader.Item("BolloMandato")))
            NumeroImpegno = Val(campodb(myPOSTreader.Item("NumeroImpegno")))


            IDODV = Val(campodb(myPOSTreader.Item("IDODV")))
            AnnoODV = Val(campodb(myPOSTreader.Item("AnnoODV")))
            TipoODV = Val(campodb(myPOSTreader.Item("TipoODV")))
            IdProgettoODV = Val(campodb(myPOSTreader.Item("IdProgettoODV")))
            ProgressivoNumero = Val(campodb(myPOSTreader.Item("ProgressivoNumero")))
            ProgressivoAnno = Val(campodb(myPOSTreader.Item("ProgressivoAnno")))

            EspTipoPagamento = campodb(myPOSTreader.Item("EspTipoPagamento"))
            EspNumeroRegolarizzazione = Val(campodb(myPOSTreader.Item("EspNumeroRegolarizzazione")))
            EspNumeroElenco = campodb(myPOSTreader.Item("EspNumeroElenco"))
            EspSpeseMandato = Val(campodb(myPOSTreader.Item("EspSpeseMandato")))
            If IsDate(campodb(myPOSTreader.Item("EspDataEsecuzionePagamento"))) Then
                EspDataEsecuzionePagamento = campodb(myPOSTreader.Item("EspDataEsecuzionePagamento"))
            Else
                EspDataEsecuzionePagamento = Nothing
            End If
            EspCodiceSIOPE = campodb(myPOSTreader.Item("EspCodiceSIOPE"))
            EspCodiceCUP = campodb(myPOSTreader.Item("EspCodiceCUP"))
            EspCodiceDelegatoQuietanzante = Val(campodb(myPOSTreader.Item("EspCodiceDelegatoQuietanzante")))
            Cofog = campodb(myPOSTreader.Item("Cofog"))
            CodiceCig = campodb(myPOSTreader.Item("CodiceCig"))
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            RifNumero = campodb(myPOSTreader.Item("RifNumero"))

            If IsDate(campodb(myPOSTreader.Item("RifData"))) Then
                RifData = campodb(myPOSTreader.Item("RifData"))
            Else
                RifData = Nothing
            End If

            idRid = campodb(myPOSTreader.Item("idRid"))

            If IsDate(campodb(myPOSTreader.Item("DataRid"))) Then
                DataRid = campodb(myPOSTreader.Item("DataRid"))
            Else
                DataRid = Nothing
            End If

            EnteDestinatario = campodb(myPOSTreader.Item("EnteDestinatario"))

            If IsDate(campodb(myPOSTreader.Item("ExportDATA"))) Then
                ExportDATA = campodb(myPOSTreader.Item("ExportDATA"))
            Else
                ExportDATA = Nothing
            End If


            BolloVirtuale = campodbN(myPOSTreader.Item("BolloVirtuale"))


            RegistrazioneRiferimento = campodbN(myPOSTreader.Item("RegistrazioneRiferimento"))

            StatoRegistrazione = campodbN(myPOSTreader.Item("StatoRegistrazione"))
            'ExportDATA
        End If
        myPOSTreader.Close()

        Dim cmd1 As New OleDbCommand()

        If ArchivioTemp Then
            cmd1.CommandText = ("select * from Temp_MovimentiContabiliRiga where " & _
                               "Numero = ? Order by RigaDaCausale,RigaRegistrazione")
        Else
            cmd1.CommandText = ("select * from MovimentiContabiliRiga where " & _
                               "Numero = ? Order by RigaDaCausale,RigaRegistrazione")
        End If


        cmd1.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd1.Connection = cn

        Dim Indice As Long = 0
        Dim myrd As OleDbDataReader = cmd1.ExecuteReader()
        Do While myrd.Read
            Righe(Indice) = New Cls_MovimentiContabiliRiga
            Righe(Indice).Id = Val(campodb(myrd.Item("Id")))
            Righe(Indice).Numero = Val(campodb(myrd.Item("Numero")))
            Righe(Indice).MastroPartita = Val(campodb(myrd.Item("MastroPartita")))
            Righe(Indice).ContoPartita = Val(campodb(myrd.Item("ContoPartita")))
            Righe(Indice).SottocontoPartita = Val(campodb(myrd.Item("SottocontoPartita")))
            Righe(Indice).MastroContropartita = Val(campodb(myrd.Item("MastroContropartita")))
            Righe(Indice).ContoContropartita = Val(campodb(myrd.Item("ContoContropartita")))
            Righe(Indice).SottocontoContropartita = Val(campodb(myrd.Item("SottocontoContropartita")))
            Righe(Indice).Descrizione = campodb(myrd.Item("Descrizione"))
            Righe(Indice).Importo = CDbl(campodbN(myrd.Item("Importo")))
            Righe(Indice).DareAvere = campodb(myrd.Item("DareAvere"))
            Righe(Indice).Segno = campodb(myrd.Item("Segno"))
            Righe(Indice).Tipo = campodb(myrd.Item("Tipo"))
            Righe(Indice).CodiceIVA = campodb(myrd.Item("CodiceIVA"))
            Righe(Indice).Imponibile = CDbl(campodbN(myrd.Item("Imponibile")))
            Righe(Indice).Detraibile = campodb(myrd.Item("Detraibile"))
            If IsDate(myrd.Item("Scadenza")) Then
                Righe(Indice).Scadenza = myrd.Item("Scadenza")
            Else
                Righe(Indice).Scadenza = Nothing
            End If
            Righe(Indice).Prorata = campodb(myrd.Item("Prorata"))
            Righe(Indice).RibaltamentoManuale = campodb(myrd.Item("RibaltamentoManuale"))
            Righe(Indice).RigaBollato = Val(campodb(myrd.Item("RigaBollato")))
            Righe(Indice).RigaDaCausale = Val(campodb(myrd.Item("RigaDaCausale")))
            Righe(Indice).CodiceRitenuta = campodb(myrd.Item("CodiceRitenuta"))
            If IsDBNull(myrd.Item("Quantita")) Then
                Righe(Indice).Quantita = 0
            Else
                Righe(Indice).Quantita = campodb(myrd.Item("Quantita"))
            End If
            Righe(Indice).CausaleDescrittiva = campodb(myrd.Item("CausaleDescrittiva"))
            Righe(Indice).MeseRiferimento = Val(campodb(myrd.Item("MeseRiferimento")))
            Righe(Indice).AnnoRiferimento = Val(campodb(myrd.Item("AnnoRiferimento")))
            Righe(Indice).DestinatoVendita = campodb(myrd.Item("DestinatoVendita"))
            REM Righe(Indice).Data = myrd.Item("Data")
            Righe(Indice).RigaRegistrazione = Val(campodb(myrd.Item("RigaRegistrazione")))
            Righe(Indice).Invisibile = Val(campodb(myrd.Item("Invisibile")))
            Righe(Indice).CentroServizio = campodb(myrd.Item("CentroServizio"))
            Righe(Indice).TipoExtra = campodb(myrd.Item("TipoExtra"))


            Indice = Indice + 1
        Loop
        myrd.Close()


        cn.Close()
    End Sub
    Public Function EsisteDocumentoRendiNumero(ByVal StringaConnessione As String, ByVal Mastro As Integer, ByVal Conto As Long, ByVal SottoConto As Double, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipologia As String, ByVal AnnoRiferimento As Integer, ByVal MeseRiferimento As Byte) As Long
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim Numero As Long

        MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                " And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'" & _
                " AND MovimentiContabiliTesta.AnnoCompetenza = " & Anno & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & Mese
        If Trim(Tipologia) <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
        End If
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()


        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("FatturaDiAnticipo")) = "" Then

                Dim cmd2 As New OleDbCommand()
                cmd2.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & myPOSTreader.Item("NumeroRegistrazione") & " And AnnoRiferimento =  " & AnnoRiferimento & " And MeseRiferimento = " & MeseRiferimento
                cmd2.Connection = cn
                Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                If myPOSTreader2.Read Then
                    Numero = myPOSTreader2.Item("Numero")
                    myPOSTreader2.Close()
                    Exit Do
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        Return Numero
    End Function

End Class
