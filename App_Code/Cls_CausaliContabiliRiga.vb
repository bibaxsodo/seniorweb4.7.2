Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CausaliContabiliRiga
    Public Codice As String
    Public Riga As Long
    Public Mastro As Long
    Public Conto As Long
    Public Sottoconto As Long
    Public Segno As String
    Public DareAvere As String
    Public Utente As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Public Sub Insert(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Modifica As Boolean = False
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdIns As New OleDbCommand()
        MySql = " INSERT INTO CausaliContabiliRiga (Codice,Riga,Mastro,Conto ,Sottoconto,Segno,DareAvere,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?,?)"

        cmdIns.CommandText = MySql
        cmdIns.Connection = cn
        cmdIns.Parameters.AddWithValue("@Codice", Codice)
        cmdIns.Parameters.AddWithValue("@Riga", Riga)
        cmdIns.Parameters.AddWithValue("@Mastro", Mastro)
        cmdIns.Parameters.AddWithValue("@Conto", Conto)
        cmdIns.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmdIns.Parameters.AddWithValue("@Segno", "+")
        cmdIns.Parameters.AddWithValue("@DareAvere", DareAvere)
        cmdIns.Parameters.AddWithValue("@Utente", Utente)
        cmdIns.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdIns.ExecuteNonQuery()

        cn.Close()

    End Sub
End Class
