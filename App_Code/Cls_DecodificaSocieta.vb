Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DecodificaSocieta
    Public RagioneSociale As String
    Public Indirizzo As String
    Public CAP As String
    Public Localita As String
    Public Provincia As String
    Public Pasword As String
    Public DataScadenza As String
    Public OspitiAbilitati As String
    Public OspitiPresenti As String
    Public Attivita As String
    Public CodiceFiscale As String
    Public PartitaIVA As Double
    Public Telefono As String
    Public IBAN As String
    Public IBANRID As String
    Public EndToEndId As String
    Public PrvtId As String
    Public MmbId As String
    Public OrgId As String

    

    Public Function DecodificaSocieta(ByVal StringaConnessione As String) As String
        Dim MySql As String
        Dim cn As OleDbConnection

        Dim MyRs As New ADODB.Recordset
        MySql = "Select * From Societa"

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()


        If Not myPOSTreader.Read() Then
            DecodificaSocieta = ""
        Else
            DecodificaSocieta = campodb(myPOSTreader.Item("RagioneSociale")) & "  " & campodb(myPOSTreader.Item("Indirizzo")) & "  " & campodb(myPOSTreader.Item("Cap")) & "  " & campodb(myPOSTreader.Item("Localita")) & "  " & campodb(myPOSTreader.Item("Provincia"))
            If campodb(myPOSTreader.Item("CodiceFiscale")) <> "" Then
                DecodificaSocieta = DecodificaSocieta & "  C.F. " & campodb(myPOSTreader.Item("CodiceFiscale"))
            End If
            If campodb(myPOSTreader.Item("PartitaIVA")) <> "" Then
                DecodificaSocieta = DecodificaSocieta & "  P.I. " & Format(campodbN(myPOSTreader.Item("PartitaIVA")), "00000000000")
            End If
        End If

    End Function




    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim MySql As String
        Dim cn As OleDbConnection

        Dim MyRs As New ADODB.Recordset
        MySql = "Select * From Societa"

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read() Then

            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            CAP = campodb(myPOSTreader.Item("CAP"))
            Localita = campodb(myPOSTreader.Item("Localita"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Pasword = campodb(myPOSTreader.Item("Pasword"))
            DataScadenza = campodb(myPOSTreader.Item("DataScadenza"))
            OspitiAbilitati = campodb(myPOSTreader.Item("OspitiAbilitati"))
            OspitiPresenti = campodb(myPOSTreader.Item("OspitiPresenti"))
            Attivita = campodb(myPOSTreader.Item("Attivita"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            PartitaIVA = campodbN(myPOSTreader.Item("PartitaIVA"))
            Telefono = campodb(myPOSTreader.Item("Telefono"))
            IBAN = campodb(myPOSTreader.Item("IBAN"))

            IBANRID = campodb(myPOSTreader.Item("IBANRID"))
            EndToEndId = campodb(myPOSTreader.Item("EndToEndId"))
            PrvtId = campodb(myPOSTreader.Item("PrvtId"))
            MmbId = campodb(myPOSTreader.Item("MmbId"))
            OrgId = campodb(myPOSTreader.Item("OrgId"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub
    Function campodbN(ByVal oggetto As Object) As Long


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
