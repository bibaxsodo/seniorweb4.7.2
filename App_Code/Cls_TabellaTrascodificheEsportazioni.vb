﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabellaTrascodificheEsportazioni
    Public ID As Long
    Public TIPOTAB As String
    Public SENIOR As String
    Public EXPORT As String

    Sub LeggiByID(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTrascodificheEsportazioni where [ID] = " & ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TIPOTAB = campodb(myPOSTreader.Item("TIPOTAB"))
            SENIOR = campodb(myPOSTreader.Item("SENIOR"))
            EXPORT = campodb(myPOSTreader.Item("EXPORT"))
        End If
        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TabellaTrascodificheEsportazioni where [ID] = " & ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()        
        cn.Close()
    End Sub


    Sub LeggiDaExport(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTrascodificheEsportazioni where [TIPOTAB] = '" & TIPOTAB & "' And  [EXPORT] = '" & EXPORT & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TIPOTAB = campodb(myPOSTreader.Item("TIPOTAB"))
            SENIOR = campodb(myPOSTreader.Item("SENIOR"))
            EXPORT = campodb(myPOSTreader.Item("EXPORT"))
        End If
        cn.Close()

    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTrascodificheEsportazioni where [TIPOTAB] = '" & TIPOTAB & "' And  [SENIOR] = '" & SENIOR & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TIPOTAB = campodb(myPOSTreader.Item("TIPOTAB"))
            SENIOR = campodb(myPOSTreader.Item("SENIOR"))
            EXPORT = campodb(myPOSTreader.Item("EXPORT"))
        End If
        cn.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTrascodificheEsportazioni where id = " & id)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TabellaTrascodificheEsportazioni SET TIPOTAB = ?, SENIOR = ?,EXPORT= ? Where ID = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@TIPOTAB", TIPOTAB)
            cmdw.Parameters.AddWithValue("@SENIOR", SENIOR)
            cmdw.Parameters.AddWithValue("@EXPORT", EXPORT)
            cmdw.Parameters.AddWithValue("@ID", ID)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TabellaTrascodificheEsportazioni (TIPOTAB,SENIOR,EXPORT) VALUES (?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@TIPOTAB", TIPOTAB)
            cmdw.Parameters.AddWithValue("@SENIOR", SENIOR)
            cmdw.Parameters.AddWithValue("@EXPORT", EXPORT)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
