Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Public Class Cls_ClienteFornitore

    Public CODICEDEBITORECREDITORE As Integer
    Public Nome As String
    Public RESIDENZAINDIRIZZO1 As String
    Public RESIDENZACAP1 As String
    Public RESIDENZACOMUNE1 As String
    Public RESIDENZAPROVINCIA1 As String
    Public Telefono1 As String
    Public Telefono2 As String
    Public CodiceFiscale As String
    Public PARTITAIVA As String
    Public Contratto As Long
    Public CodiceCig As String
    Public CodiceCup As String
    Public ScadenzaDurc As String
    Public TipoOspite As String
    Public FisicaGiuridica As String
    Public Professionista As String
    Public SoggettoABollo As String
    Public Economo As String
    Public MastroCliente As Integer
    Public ContoCliente As Long
    Public SottoContoCliente As Double
    Public MastroRicavo As Integer
    Public ContoRicavo As Long
    Public SottocontoRicavo As Long
    Public ModalitaPagamento As String
    Public AllegatoCliente As String
    Public MastroClienteAllegato As Long
    Public ContoClienteAllegato As Long
    Public SottocontoClienteAllegato As Long
    Public DescrizioneCliente As String
    Public CCPostaleCliente As String
    Public ABICliente As Long
    Public CABCliente As Long
    Public CinCliente As String
    Public IntCliente As String
    Public NumeroControlloCliente As Long
    Public CCBancarioCliente As String
    Public Dichiarazione As String
    Public CausaleContabileCliente As String
    Public MastroFornitore As Long
    Public ContoFornitore As Long
    Public SottoContoFornitore As Double
    Public MastroCosto As Long
    Public ContoCosto As Long
    Public SottocontoCosto As Double
    Public ModalitaPagamentoFornitore As String
    Public AllegatoFornitore As String    
    Public IVASospesa As String
    Public IVASospesaCli As String
    Public MastroFornitoreAllegato As Long
    Public ContoFornitoreAllegato As Long
    Public SottocontoFornitoreAllegato As Double
    Public DescrizioneFornitore As String
    Public CCPostaleFornitore As String
    Public ABIFornitore As Long
    Public CABFornitore As Long
    Public CinFornitore As String
    Public IntFornitore As String
    Public NumeroControlloFornitore As String
    Public CCBancarioFornitore As String
    Public CausaleContabileFornitore As String
    Public DetrabilitaFornitore As String    
    Public CONTOPERESATTO As Long
    Public Utente As String
    Public PEC As String ' PEC
    Public CodiceDestinatario As String 'CodiceDestinatario



    Public NumItem As String
    Public CodiceCommessaConvezione As String
    Public IdDocumento As String
    Public RiferimentoAmministrazione As String
    Public NumeroFatturaDDT As Integer
    Public Note As String
    Public IdDocumentoData As String
    Public Causale As String
    Public EGO As String

    Public Cellulare As String 'RESIDENZATELEFONO2
    Public Mail As String 'RESIDENZATELEFONO3    

    Public CodiceComune As String
    Public CodiceProvincia As String
    Public CodiceRegione As String





    Sub Cancella(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from AnagraficaComune where " & _
                           "CODICEDEBITORECREDITORE = ? And TIPOLOGIA = 'D'")
        cmd.Parameters.AddWithValue("@CODICEDEBITORECREDITORE", CODICEDEBITORECREDITORE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Sub CercaCFContiAttivi(ByVal StringaConnessione As String, ByVal StringaConnessioneGenerale As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        CODICEDEBITORECREDITORE = 0

        If PARTITAIVA > 0 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                                   "PARTITAIVA = ? And TIPOLOGIA = 'D'")
            cmd.Parameters.AddWithValue("@PARTITAIVA", PARTITAIVA)
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CODICEDEBITORECREDITORE = campodbn(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
                Leggi(StringaConnessione)

                If MastroFornitore > 0 Then
                    Dim PDC As New Cls_Pianodeiconti

                    PDC.NonInUso = ""
                    PDC.Mastro = MastroFornitore
                    PDC.Conto = ContoFornitore
                    PDC.Sottoconto = SottoContoFornitore
                    PDC.Decodfica(StringaConnessioneGenerale)

                    
                    If PDC.NonInUso = "S" Then
                    Else
                        Exit Do
                    End If
                End If
            Loop
            myPOSTreader.Close()
        End If

        If CodiceFiscale <> "" Then
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = ("select * from AnagraficaComune where " & _
                                   "CodiceFiscale = ? And TIPOLOGIA = 'D'")

            cmdR.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
            cmdR.Connection = cn


            Dim MReaad As OleDbDataReader = cmdR.ExecuteReader()
            Do While MReaad.Read
                CODICEDEBITORECREDITORE = campodbn(MReaad.Item("CODICEDEBITORECREDITORE"))
                Leggi(StringaConnessione)

                If MastroFornitore > 0 Then
                    Dim PDC As New Cls_Pianodeiconti

                    PDC.NonInUso = ""
                    PDC.Mastro = MastroFornitore
                    PDC.Conto = ContoFornitore
                    PDC.Sottoconto = SottoContoFornitore
                    PDC.Decodfica(StringaConnessioneGenerale)

                    If PDC.NonInUso = "S" Then
                    Else
                        Exit Do
                    End If
                End If
            Loop
            MReaad.Close()
        End If


        cn.Close()
    End Sub

    Sub CercaCF(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        CODICEDEBITORECREDITORE = 0

        If PARTITAIVA > 0 Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                                   "PARTITAIVA = ? And TIPOLOGIA = 'D'")
            cmd.Parameters.AddWithValue("@PARTITAIVA", PARTITAIVA)
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CODICEDEBITORECREDITORE = campodbn(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
                Leggi(StringaConnessione)
            Loop
            myPOSTreader.Close()
        End If

        If CodiceFiscale <> "" Then
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = ("select * from AnagraficaComune where " & _
                                   "CodiceFiscale = ? And TIPOLOGIA = 'D'")
            cmdR.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
            cmdR.Connection = cn


            Dim MReaad As OleDbDataReader = cmdR.ExecuteReader()
            Do While MReaad.Read
                CODICEDEBITORECREDITORE = campodbn(MReaad.Item("CODICEDEBITORECREDITORE"))
                Leggi(StringaConnessione)
            Loop
            MReaad.Close()
        End If


        cn.Close()
    End Sub

    Sub RicercaPerDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                               " Nome Like ? And TIPOLOGIA = 'D'")
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CODICEDEBITORECREDITORE = campodbn(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
            Leggi(StringaConnessione)

        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        If CODICEDEBITORECREDITORE = 0 And MastroCliente = 0 And MastroFornitore = 0 Then
            Exit Sub
        End If


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CODICEDEBITORECREDITORE > 0 Then
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                               "CODICEDEBITORECREDITORE = ? And TIPOLOGIA = 'D'")
            cmd.Parameters.AddWithValue("@CODICEDEBITORECREDITORE", CODICEDEBITORECREDITORE)
        End If
        If CODICEDEBITORECREDITORE = 0 And MastroCliente > 0 Then
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                               "MastroCliente = ? And ContoCliente = ? And SottoContoCliente = ? And (TIPOLOGIA = 'D' or TIPOLOGIA = 'C' OR TIPOLOGIA = 'R')")
            cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
            cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
            cmd.Parameters.AddWithValue("@SottoContoCliente", SottoContoCliente)
        End If
        If CODICEDEBITORECREDITORE = 0 And MastroFornitore > 0 Then
            cmd.CommandText = ("select * from AnagraficaComune where " & _
                               "MastroFornitore = ? And ContoFornitore = ? And SottoContoFornitore = ? And (TIPOLOGIA = 'D' or TIPOLOGIA = 'C' OR TIPOLOGIA = 'R')")
            cmd.Parameters.AddWithValue("@MastroFornitore", MastroFornitore)
            cmd.Parameters.AddWithValue("@ContoFornitore", ContoFornitore)
            cmd.Parameters.AddWithValue("@SottoContoFornitore", SottoContoFornitore)
        End If

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CODICEDEBITORECREDITORE = campodbn(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))
            PEC = campodb(myPOSTreader.Item("PEC"))
            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            PARTITAIVA = campodb(myPOSTreader.Item("PARTITAIVA"))
            Contratto = campodbn(myPOSTreader.Item("Contratto"))
            CodiceCig = campodb(myPOSTreader.Item("CodiceCig"))
            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))
            ScadenzaDurc = campodb(myPOSTreader.Item("ScadenzaDurc"))
            TipoOspite = campodb(myPOSTreader.Item("TipoOspite"))
            FisicaGiuridica = campodb(myPOSTreader.Item("FisicaGiuridica"))
            Professionista = campodb(myPOSTreader.Item("Professionista"))
            SoggettoABollo = campodb(myPOSTreader.Item("SoggettoABollo"))
            Economo = campodb(myPOSTreader.Item("Economo"))
            MastroCliente = campodbn(myPOSTreader.Item("MastroCliente"))
            ContoCliente = campodbn(myPOSTreader.Item("ContoCliente"))
            SottoContoCliente = campodbn(myPOSTreader.Item("SottoContoCliente"))
            MastroRicavo = campodbn(myPOSTreader.Item("MastroRicavo"))
            ContoRicavo = campodbn(myPOSTreader.Item("ContoRicavo"))
            SottocontoRicavo = campodbn(myPOSTreader.Item("SottocontoRicavo"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            AllegatoCliente = campodb(myPOSTreader.Item("AllegatoCliente"))
            MastroClienteAllegato = campodbn(myPOSTreader.Item("MastroClienteAllegato"))
            ContoClienteAllegato = campodbn(myPOSTreader.Item("ContoClienteAllegato"))
            SottocontoClienteAllegato = campodbn(myPOSTreader.Item("SottocontoClienteAllegato"))
            DescrizioneCliente = campodb(myPOSTreader.Item("DescrizioneCliente"))
            CCPostaleCliente = campodb(myPOSTreader.Item("CCPostaleCliente"))
            ABICliente = campodbn(myPOSTreader.Item("ABICliente"))
            CABCliente = campodbn(myPOSTreader.Item("CABCliente"))
            CinCliente = campodb(myPOSTreader.Item("CinCliente"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodbn(myPOSTreader.Item("NumeroControlloCliente"))
            CCBancarioCliente = campodb(myPOSTreader.Item("CCBancarioCliente"))
            Dichiarazione = campodb(myPOSTreader.Item("Dichiarazione"))
            CausaleContabileCliente = campodb(myPOSTreader.Item("CausaleContabileCliente"))
            MastroFornitore = campodbn(myPOSTreader.Item("MastroFornitore"))
            ContoFornitore = campodbn(myPOSTreader.Item("ContoFornitore"))
            SottoContoFornitore = campodbn(myPOSTreader.Item("SottoContoFornitore"))
            MastroCosto = campodbn(myPOSTreader.Item("MastroCosto"))
            ContoCosto = campodbn(myPOSTreader.Item("ContoCosto"))
            SottocontoCosto = campodbn(myPOSTreader.Item("SottocontoCosto"))
            ModalitaPagamentoFornitore = campodb(myPOSTreader.Item("ModalitaPagamentoFornitore"))
            AllegatoFornitore = campodb(myPOSTreader.Item("AllegatoFornitore"))
            IVASospesa = campodb(myPOSTreader.Item("IVASospesa"))
            IVASospesaCli = campodb(myPOSTreader.Item("IVASospesaCli"))
            MastroFornitoreAllegato = campodbn(myPOSTreader.Item("MastroFornitoreAllegato"))
            ContoFornitoreAllegato = campodbn(myPOSTreader.Item("ContoFornitoreAllegato"))
            SottocontoFornitoreAllegato = campodbn(myPOSTreader.Item("SottocontoFornitoreAllegato"))
            DescrizioneFornitore = campodb(myPOSTreader.Item("DescrizioneFornitore"))
            CCPostaleFornitore = campodb(myPOSTreader.Item("CCPostaleFornitore"))
            ABIFornitore = campodbn(myPOSTreader.Item("ABIFornitore"))
            CABFornitore = campodbn(myPOSTreader.Item("CABFornitore"))
            CinFornitore = campodb(myPOSTreader.Item("CinFornitore"))
            IntFornitore = campodb(myPOSTreader.Item("IntFornitore"))
            NumeroControlloFornitore = campodbn(myPOSTreader.Item("NumeroControlloFornitore"))
            CCBancarioFornitore = campodb(myPOSTreader.Item("CCBancarioFornitore"))
            CausaleContabileFornitore = campodb(myPOSTreader.Item("CausaleContabileFornitore"))
            DetrabilitaFornitore = campodb(myPOSTreader.Item("DetrabilitaFornitore"))

            CodiceComune = campodb(myPOSTreader.Item("CodiceComune"))
            CodiceProvincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))

            IdDocumento = campodb(myPOSTreader.Item("IdDocumento"))
            RiferimentoAmministrazione = campodb(myPOSTreader.Item("RiferimentoAmministrazione"))
            NumeroFatturaDDT = campodbn(myPOSTreader.Item("NumeroFatturaDDT"))
            Note = campodb(myPOSTreader.Item("Note"))
            IdDocumentoData = campodb(myPOSTreader.Item("IdDocumentoData"))

            NumItem = campodb(myPOSTreader.Item("NumItem"))
            CodiceCommessaConvezione = campodb(myPOSTreader.Item("CodiceCommessaConvezione"))

            Causale = campodb(myPOSTreader.Item("Causale"))

            EGO = campodb(myPOSTreader.Item("EGO"))

            Cellulare = campodb(myPOSTreader.Item("RESIDENZATELEFONO2"))

            Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))            

        End If

        cn.Close()
    End Sub
    Function campodbn(ByVal oggetto As Object) As Long
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub Scrivi(ByVal StringaConnessione As String, ByVal StringaConnessioneGen As String)
        Dim cn As OleDbConnection
        Dim cnGen As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cnGen = New Data.OleDb.OleDbConnection(StringaConnessioneGen)

        cnGen.Open()

        cn.Open()

        If CODICEDEBITORECREDITORE = 0 Then
            Dim cmdRd As New OleDbCommand()

            cmdRd.Connection = cn
            MySql = "Select max(CODICEDEBITORECREDITORE) From AnagraficaComune Where TIPOLOGIA = 'D'"
            cmdRd.CommandText = MySql
            Dim MyRdT As OleDbDataReader = cmdRd.ExecuteReader()
            If MyRdT.Read Then
                If IsDBNull(MyRdT.Item(0)) Then
                    CODICEDEBITORECREDITORE = 1
                Else
                    CODICEDEBITORECREDITORE = MyRdT.Item(0) + 1
                End If
            End If
            MyRdT.Close()

            Dim cmdIns As New OleDbCommand()

            cmdIns.Connection = cn
            MySql = "INSERT INTO AnagraficaComune (CODICEDEBITORECREDITORE,Nome,TIPOLOGIA) VALUES " & _
                        "(?,?,'D')"
            cmdIns.Parameters.AddWithValue("@CODICEDEBITORECREDITORE", CODICEDEBITORECREDITORE)
            cmdIns.Parameters.AddWithValue("@Nome", Nome)
            cmdIns.CommandText = MySql

            cmdIns.ExecuteNonQuery()

        End If

        If MastroCliente > 0 And SottoContoCliente = 0 Then
            Dim cmdRd1 As New OleDbCommand()

            cmdRd1.Connection = cnGen
            MySql = "Select max(Sottoconto) From PianoConti where mastro = ? and conto = ? "
            cmdRd1.CommandText = MySql
            cmdRd1.Parameters.AddWithValue("@Mastro", MastroCliente)
            cmdRd1.Parameters.AddWithValue("@Conto", ContoCliente)
            Dim MyRdT1 As OleDbDataReader = cmdRd1.ExecuteReader()
            If MyRdT1.Read Then
                If IsDBNull(MyRdT1.Item(0)) Then
                    SottoContoCliente = 1
                Else
                    SottoContoCliente = MyRdT1.Item(0) + 1
                End If
            Else
                SottoContoCliente = 1
            End If
            MyRdT1.Close()

            Dim Xn As New Cls_Pianodeiconti

            Xn.Mastro = MastroCliente
            Xn.Conto = ContoCliente
            Xn.Sottoconto = SottoContoCliente
            Xn.Descrizione = Nome
            Xn.Tipo = "A"
            Xn.TipoAnagrafica = "D"
            Xn.Scrivi(StringaConnessioneGen)
        End If

        If MastroFornitore > 0 And SottoContoFornitore = 0 Then
            Dim cmdRd1 As New OleDbCommand()

            cmdRd1.Connection = cnGen
            MySql = "Select max(Sottoconto) From PianoConti where mastro = ? and conto = ? "
            cmdRd1.CommandText = MySql
            cmdRd1.Parameters.AddWithValue("@Mastro", MastroFornitore)
            cmdRd1.Parameters.AddWithValue("@Conto", ContoFornitore)
            Dim MyRdT1 As OleDbDataReader = cmdRd1.ExecuteReader()
            If MyRdT1.Read Then
                If IsDBNull(MyRdT1.Item(0)) Then
                    SottoContoFornitore = 1
                Else
                    SottoContoFornitore = MyRdT1.Item(0) + 1
                End If
            Else
                SottoContoFornitore = 1
            End If
            MyRdT1.Close()

            Dim Xn As New Cls_Pianodeiconti

            Xn.Mastro = MastroFornitore
            Xn.Conto = ContoFornitore
            Xn.Sottoconto = SottoContoFornitore
            Xn.Descrizione = Nome
            Xn.Tipo = "P"
            Xn.TipoAnagrafica = "D"
            Xn.Scrivi(StringaConnessioneGen)
        End If

        If CODICEDEBITORECREDITORE = 0 Then
            Exit Sub
        End If

        Dim cmd As New OleDbCommand()
        cmd.Connection = cn
        MySql = "UPDATE AnagraficaComune Set Nome= ?," _
                & "RESIDENZAINDIRIZZO1= ?," _
                & "RESIDENZACAP1= ?," _
                & "RESIDENZACOMUNE1= ?," _
                & "RESIDENZAPROVINCIA1= ?," _
                & "Telefono1= ?," _
                & "Telefono2= ?," _
                & "CodiceFiscale= ?," _
                & "PARTITAIVA= ?," _
                & "Contratto= ?," _
                & "CodiceCig= ?," _
                & "CodiceCup= ?," _
                & "ScadenzaDurc= ?," _
                & "TipoOspite= ?," _
                & "FisicaGiuridica= ?," _
                & "Professionista= ?," _
                & "SoggettoABollo= ?," _
                & "Economo= ?," _
                & "MastroCliente= ?," _
                & "ContoCliente= ?," _
                & "SottoContoCliente= ?," _
                & "MastroRicavo= ?," _
                & "ContoRicavo= ?," _
                & "SottocontoRicavo= ?," _
                & "ModalitaPagamento= ?," _
                & "AllegatoCliente= ?," _
                & "MastroClienteAllegato= ?," _
                & "ContoClienteAllegato= ?," _
                & "SottocontoClienteAllegato= ?," _
                & "DescrizioneCliente= ?," _
                & "CCPostaleCliente= ?," _
                & "ABICliente= ?," _
                & "CABCliente= ?," _
                & "CinCliente= ?," _
                & "IntCliente= ?," _
                & "NumeroControlloCliente= ?," _
                & "CCBancarioCliente= ?," _
                & "Dichiarazione= ?," _
                & "CausaleContabileCliente= ?," _
                & "MastroFornitore= ?," _
                & "ContoFornitore= ?," _
                & "SottoContoFornitore= ?," _
                & "MastroCosto= ?," _
                & "ContoCosto= ?," _
                & "SottocontoCosto= ?," _
                & "ModalitaPagamentoFornitore= ?," _
                & "AllegatoFornitore= ?," _
                & "IVASospesa= ?," _
                & "IVASospesaCli= ?," _
                & "MastroFornitoreAllegato= ?," _
                & "ContoFornitoreAllegato= ?," _
                & "SottocontoFornitoreAllegato = ?," _
                & "DescrizioneFornitore= ?," _
                & "CCPostaleFornitore= ?," _
                & "ABIFornitore= ?," _
                & "CABFornitore= ?," _
                & "CinFornitore= ?," _
                & "IntFornitore= ?," _
                & "NumeroControlloFornitore= ?," _
                & "CCBancarioFornitore= ?," _
                & "CausaleContabileFornitore= ?," _
                & "DetrabilitaFornitore = ?," _
                & "Tipologia= ?, " _
                & "Utente = ?, " _
                & "DataAggiornamento = ?, " _
                & "CodiceDestinatario = ?," _
                & "Pec = ?, " _
                & " NumItem = ? ," _
                & " CodiceCommessaConvezione = ? ," _
                & " IdDocumento = ? ," _
                & " RiferimentoAmministrazione = ? ," _
                & " NumeroFatturaDDT = ? ," _
                & " Note = ? ," _
                & " IdDocumentoData = ?, " _
                & " EGO = ?, " _
                & " Causale = ?, " _
                & " RESIDENZATELEFONO2 = ?, " _
                & " RESIDENZATELEFONO3 = ? " _
                & " Where CODICEDEBITORECREDITORE= ?"

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@RESIDENZACAP1", RESIDENZACAP1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", Mid(RESIDENZACOMUNE1 & Space(10), 1, 3))
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", Mid(RESIDENZAPROVINCIA1 & Space(10), 1, 3))
        cmd.Parameters.AddWithValue("@Telefono1", Telefono1)
        cmd.Parameters.AddWithValue("@Telefono2", Telefono2)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Parameters.AddWithValue("@PARTITAIVA", Val(PARTITAIVA))
        cmd.Parameters.AddWithValue("@Contratto", Contratto)
        cmd.Parameters.AddWithValue("@CodiceCig", CodiceCig)
        cmd.Parameters.AddWithValue("@CodiceCup", CodiceCup)
        cmd.Parameters.AddWithValue("@ScadenzaDurc", ScadenzaDurc)
        cmd.Parameters.AddWithValue("@TipoOspite", TipoOspite)
        cmd.Parameters.AddWithValue("@FisicaGiuridica", FisicaGiuridica)
        cmd.Parameters.AddWithValue("@Professionista", Professionista)
        cmd.Parameters.AddWithValue("@SoggettoABollo", SoggettoABollo)
        cmd.Parameters.AddWithValue("@Economo", Economo)
        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottoContoCliente", SottoContoCliente)
        cmd.Parameters.AddWithValue("@MastroRicavo", MastroRicavo)
        cmd.Parameters.AddWithValue("@ContoRicavo", ContoRicavo)
        cmd.Parameters.AddWithValue("@SottocontoRicavo", SottocontoRicavo)
        cmd.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmd.Parameters.AddWithValue("@AllegatoCliente", AllegatoCliente)
        cmd.Parameters.AddWithValue("@MastroClienteAllegato", MastroClienteAllegato)
        cmd.Parameters.AddWithValue("@ContoClienteAllegato", ContoClienteAllegato)
        cmd.Parameters.AddWithValue("@SottocontoClienteAllegato", SottocontoClienteAllegato)
        cmd.Parameters.AddWithValue("@DescrizioneCliente", DescrizioneCliente)
        cmd.Parameters.AddWithValue("@CCPostaleCliente", CCPostaleCliente)
        cmd.Parameters.AddWithValue("@ABICliente", ABICliente)
        cmd.Parameters.AddWithValue("@CABCliente", CABCliente)
        cmd.Parameters.AddWithValue("@CinCliente", CinCliente)
        cmd.Parameters.AddWithValue("@IntCliente", IntCliente)
        cmd.Parameters.AddWithValue("@NumeroControlloCliente", Val(NumeroControlloCliente))
        cmd.Parameters.AddWithValue("@CCBancarioCliente", CCBancarioCliente)
        cmd.Parameters.AddWithValue("@Dichiarazione", Dichiarazione)
        cmd.Parameters.AddWithValue("@CausaleContabileCliente", CausaleContabileCliente)
        cmd.Parameters.AddWithValue("@MastroFornitore", MastroFornitore)
        cmd.Parameters.AddWithValue("@ContoFornitore", ContoFornitore)
        cmd.Parameters.AddWithValue("@SottoContoFornitore", SottoContoFornitore)
        cmd.Parameters.AddWithValue("@MastroCosto", MastroCosto)
        cmd.Parameters.AddWithValue("@ContoCosto", ContoCosto)
        cmd.Parameters.AddWithValue("@SottocontoCosto", SottocontoCosto)
        cmd.Parameters.AddWithValue("@ModalitaPagamentoFornitore", ModalitaPagamentoFornitore)
        cmd.Parameters.AddWithValue("@AllegatoFornitore", AllegatoFornitore)
        cmd.Parameters.AddWithValue("@IVASospesa", IVASospesa)
        cmd.Parameters.AddWithValue("@IVASospesaCli", IVASospesaCli)
        cmd.Parameters.AddWithValue("@MastroFornitoreAllegato", MastroFornitoreAllegato)
        cmd.Parameters.AddWithValue("@ContoFornitoreAllegato", ContoFornitoreAllegato)
        cmd.Parameters.AddWithValue("@SottocontoFornitoreAllegato", SottocontoFornitoreAllegato)
        cmd.Parameters.AddWithValue("@DescrizioneFornitore", DescrizioneFornitore)
        cmd.Parameters.AddWithValue("@CCPostaleFornitore", CCPostaleFornitore)
        cmd.Parameters.AddWithValue("@ABIFornitore", ABIFornitore)
        cmd.Parameters.AddWithValue("@CABFornitore", CABFornitore)
        cmd.Parameters.AddWithValue("@CinFornitore", CinFornitore)
        cmd.Parameters.AddWithValue("@IntFornitore", IntFornitore)
        cmd.Parameters.AddWithValue("@NumeroControlloFornitore", Val(NumeroControlloFornitore))
        cmd.Parameters.AddWithValue("@CCBancarioFornitore", CCBancarioFornitore)
        cmd.Parameters.AddWithValue("@CausaleContabileFornitore", CausaleContabileFornitore)
        cmd.Parameters.AddWithValue("@DetrabilitaFornitore", DetrabilitaFornitore)
        cmd.Parameters.AddWithValue("@Tipologia", "D")
        cmd.Parameters.AddWithValue("@Utente", Utente)
        cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd.Parameters.AddWithValue("@CodiceDestinatario", CodiceDestinatario)
        cmd.Parameters.AddWithValue("@PEC", PEC)



        cmd.Parameters.AddWithValue("@NumItem ", NumItem)
        cmd.Parameters.AddWithValue("@CodiceCommessaConvezione", CodiceCommessaConvezione)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@RiferimentoAmministrazione", RiferimentoAmministrazione)
        cmd.Parameters.AddWithValue("@NumeroFatturaDDT", NumeroFatturaDDT)
        cmd.Parameters.AddWithValue("@Note", Note)
        cmd.Parameters.AddWithValue("@IdDocumentoData", IdDocumentoData)
        cmd.Parameters.AddWithValue("@EGO", EGO)
        cmd.Parameters.AddWithValue("@Causale", Causale)

        cmd.Parameters.AddWithValue("@Cellulare", Cellulare)
        cmd.Parameters.AddWithValue("@MAIL", Mail)        


        cmd.Parameters.AddWithValue("@CODICEDEBITORECREDITORE", CODICEDEBITORECREDITORE)


        cmd.ExecuteNonQuery()


        cn.Close()
        cnGen.Close()

    End Sub


    Sub UpCheckBoxList(ByVal StringaConnessione As String, ByVal Ricerca As String, ByRef appoggio As CheckBoxList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Ricerca = "" Then
            cmd.CommandText = ("select * from AnagraficaComune Where (CODICEDEBITORECREDITORE > 0 OR CodiceOspite > 0)  Order By Nome")
        Else
            cmd.CommandText = ("select * from AnagraficaComune Where (CODICEDEBITORECREDITORE > 0 OR CodiceOspite > 0) And Nome Like ? Order By Nome")
            cmd.Parameters.AddWithValue("@Nome", Ricerca)
        End If

        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")))
            If campodb(myPOSTreader.Item("Tipologia")) = "D" Then
                appoggio.Items(appoggio.Items.Count - 1).Value = "D" & campodb(myPOSTreader.Item("CODICEDEBITORECREDITORE"))
            End If
            If campodb(myPOSTreader.Item("Tipologia")) = "O" Then
                appoggio.Items(appoggio.Items.Count - 1).Value = "O" & campodb(myPOSTreader.Item("CODICEOSPITE"))
            End If
            If campodb(myPOSTreader.Item("Tipologia")) = "P" Then
                appoggio.Items(appoggio.Items.Count - 1).Value = "P" & campodb(myPOSTreader.Item("CODICEOSPITE")) & "-" & campodb(myPOSTreader.Item("CODICEPARENTE"))
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub
End Class
