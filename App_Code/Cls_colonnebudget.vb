﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_colonnebudget
    Public Anno As Long
    Public Livello1 As Long
    Public Descrizione As String


    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT * FROM ColonneBudget WHERE Anno = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Anno", Anno)

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Livello1")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Livello1) Or IsNothing(Anno) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from ColonneBudget where " & _
                           "Anno = ? And Livello1 = ? ")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub
    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ColonneBudget where " & _
                           "Anno = ? And Livello1 = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim APPOGGIO As String = ""
            Dim I As Integer
            For I = 1 To 10
                APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
            Next
            Dim cmdIns As New OleDbCommand()
            MySql = "INSERT INTO ColonneBudget (Anno,Livello1,Descrizione) VALUES (?,?,?)"
            cmdIns.CommandText = MySql
            cmdIns.Parameters.AddWithValue("@Anno", Anno)
            cmdIns.Parameters.AddWithValue("@Livello1", Livello1)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)

            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If

        MySql = "UPDATE ColonneBudget SET " & _
                "Descrizione = ? " & _
                " Where Anno = ? And Livello1 = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)        
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Livello1", Livello1)
        cmd1.CommandText = MySql
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Livello1) Or IsNothing(Anno) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ColonneBudget where " & _
                           "Anno = ? And Livello1 = ? ")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))            
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Sub DaDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        If IsNothing(Descrizione) Or IsNothing(Anno) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ColonneBudget where " & _
                           "Anno = ? And Descrizione = ? ")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Livello1 = campodb(myPOSTreader.Item("Livello1"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
