Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RegistriIVAanno
    Public Anno As Long
    Public Tipo As Long
    Public DataStampa As Date
    Public UltimaPaginaStampata As Long

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As Long, ByVal xAnno As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Registri Where  Tipo = " & Codice & " And Anno = " & xAnno)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Tipo = campodbn(myPOSTreader.Item("Tipo"))
            Anno = campodbn(myPOSTreader.Item("Anno"))
            If campodb(myPOSTreader.Item("DataStampa")) <> "" Then
                DataStampa = campodb(myPOSTreader.Item("DataStampa"))
            End If
            UltimaPaginaStampata = campodbn(myPOSTreader.Item("UltimaPaginaStampata"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String, ByVal Codice As Long, ByVal xAnno As Long)
        Dim cn As OleDbConnection
        Dim MySql As String


        MySql = ""
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Registri Where  Tipo = " & Codice & " And Anno = " & xAnno)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "Update Registri Set DataStampa = ?,UltimaPaginaStampata= ?   Where  Tipo = " & Codice & " And Anno = " & xAnno
        Else
            MySql = "INSERT INTO Registri (DataStampa,UltimaPaginaStampata,Tipo,Anno) VALUES (?,?," & Codice & "," & xAnno & ")"
        End If
        myPOSTreader.Close()

        Dim Xmd As New OleDbCommand
        Xmd.CommandText = MySql
        Xmd.Connection = cn
        Xmd.Parameters.AddWithValue("@DataStampa", DataStampa)
        Xmd.Parameters.AddWithValue("@UltimaPaginaStampata", UltimaPaginaStampata)
        Xmd.ExecuteNonQuery()

        cn.Close()
    End Sub

End Class

