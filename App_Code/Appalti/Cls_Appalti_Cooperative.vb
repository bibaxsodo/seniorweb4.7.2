﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Appalti_Cooperative
    Public ID As Integer
    Public RagioneSociale As String

    
    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Appalti_Cooperative Order by RagioneSociale")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("RagioneSociale"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection


        If IsNothing(MyCodice) Then
            MyCodice = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Cooperative where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
    
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub




    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection


        If IsNothing(MyCodice) Then
            MyCodice = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Cooperative where " & _
                           "RagioneSociale = ? ")
        cmd.Parameters.AddWithValue("@MyCodice", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("delete from Appalti_Cooperative where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Cooperative where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO Appalti_Cooperative (RagioneSociale) values (?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@RagioneSociale", RagioneSociale)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()

            Dim cmdMId As New OleDbCommand()
            cmdMId.CommandText = ("select max(id) from Appalti_Cooperative ")
            cmdMId.Connection = cn
            Dim ReadMid As OleDbDataReader = cmdMId.ExecuteReader()
            If ReadMid.Read() Then
                ID = campodbn(ReadMid.Item(0))
            End If
        End If
        myPOSTreader.Close()


        Dim MySql As String

        MySql = "UPDATE Appalti_Cooperative SET " & _
                " RagioneSociale = ? " & _
                " Where ID = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@RagioneSociale", RagioneSociale)
        cmd1.Parameters.AddWithValue("@ID", ID)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

End Class
