﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Public Class Cls_Appalti_AppaltiBudget
    Public IdAppalto As Integer
    Public IdTipoOperatore As String
    Public Ore As Double
    Public Importo As Double


    Sub loaddati(ByVal StringaConnessione As String, ByVal IdAppalti As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_AppaltiBudget  where IdAppalto = " & IdAppalti)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("IdTipoOperatore", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()



            myriga(0) = campodb(myPOSTreader.Item("IdTipoOperatore"))

            myriga(1) = campodbn(myPOSTreader.Item("Ore"))

            myriga(2) = campodbn(myPOSTreader.Item("Importo"))




            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = 0

            myriga(1) = 0

            myriga(2) = 0            

            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Appalti_AppaltiBudget where IdAppalto = " & IdAppalto
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            'If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
            MySql = "INSERT INTO Appalti_AppaltiBudget ([IdAppalto],[IdTipoOperatore],Ore,Importo) VALUES (?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IdAppalto", IdAppalto)
            cmdw.Parameters.AddWithValue("@IdTipoOperatore", Tabella.Rows(i).Item(0))
            If Val(campodb(Tabella.Rows(i).Item(1))) = 0 Then
                Tabella.Rows(i).Item(1) = 0
            End If

            cmdw.Parameters.AddWithValue("@Ore", Convert.ToDouble(Tabella.Rows(i).Item(1)))

            If Val(campodb(Tabella.Rows(i).Item(2))) = 0 Then
                Tabella.Rows(i).Item(2) = 0
            End If
            cmdw.Parameters.AddWithValue("@Importo", Convert.ToDouble(Tabella.Rows(i).Item(2)))

            cmdw.Transaction = Transan
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
            'End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
End Class
