﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Appalti_StruttureMansioni
    Public IdAppalto As Integer
    Public Struttura As Integer
    Public Mansione As Integer
    Public Regola As String
    Public importo As Double
    Public Mastro As Integer
    Public Conto As Integer
    Public Sottoconto As Integer
    Public CodiceIVA As String
    Public intypecod As String


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_StruttureMansioni where IdAppalto = " & IdAppalto & " And IdStrutture = " & Struttura & " And idMansione = " & Mansione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Regola = campodb(myPOSTreader.Item("Regola"))
            importo = campodbn(myPOSTreader.Item("importo"))
            Mastro = campodbn(myPOSTreader.Item("Mastro"))
            Conto = campodbn(myPOSTreader.Item("Conto"))
            Sottoconto = campodbn(myPOSTreader.Item("Sottoconto"))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))

            intypecod = campodb(myPOSTreader.Item("intypecod"))

        End If
        myPOSTreader.Close()

    End Sub

    Sub Leggiintypecod(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_StruttureMansioni where IdAppalto = " & IdAppalto & " And IdStrutture = " & Struttura & " And idMansione = " & Mansione & " And intypecod = '" & intypecod & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Regola = campodb(myPOSTreader.Item("Regola"))
            importo = campodbn(myPOSTreader.Item("importo"))
            Mastro = campodbn(myPOSTreader.Item("Mastro"))
            Conto = campodbn(myPOSTreader.Item("Conto"))
            Sottoconto = campodbn(myPOSTreader.Item("Sottoconto"))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))

            intypecod = campodb(myPOSTreader.Item("intypecod"))

        End If
        myPOSTreader.Close()

    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal StringaConnessioneGenerale As String, ByVal IdAppalti As Integer, ByVal Struttura As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Struttura = 0 Then
            cmd.CommandText = ("select IdMansione,Regola,importo,Mastro,Conto,Sottoconto,CodiceIVA,intypecod from Appalti_StruttureMansioni where IdAppalto = " & IdAppalti & " And ModificaDaAppalto = 1 group by IdMansione,Regola,importo,Mastro,Conto,Sottoconto,CodiceIVA,intypecod")
        Else
            cmd.CommandText = ("select * from Appalti_StruttureMansioni where IdAppalto = " & IdAppalti & " And IdStrutture = " & Struttura & " And ModificaDaAppalto = 0")
        End If

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("IdMansione", GetType(String))
        Tabella.Columns.Add("Regola", GetType(String))
        Tabella.Columns.Add("importo", GetType(String))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("CodiceIVA", GetType(String))
        Tabella.Columns.Add("intypecod", GetType(String))



        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()



            myriga(0) = campodbn(myPOSTreader.Item("IdMansione"))
            myriga(1) = campodb(myPOSTreader.Item("Regola"))
            myriga(2) = campodbn(myPOSTreader.Item("importo"))

            Dim ContoP As New Cls_Pianodeiconti
            ContoP.Mastro = campodbn(myPOSTreader.Item("Mastro"))
            ContoP.Conto = campodbn(myPOSTreader.Item("Conto"))
            ContoP.Sottoconto = campodbn(myPOSTreader.Item("Sottoconto"))
            ContoP.Decodfica(StringaConnessioneGenerale)

            myriga(3) = ContoP.Mastro & " " & ContoP.Conto & " " & ContoP.Sottoconto & " " & ContoP.Descrizione


            myriga(4) = campodb(myPOSTreader.Item("CodiceIVA"))

            myriga(5) = campodb(myPOSTreader.Item("intypecod"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = 0
            myriga(2) = 0


            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable, Optional ByVal ModificaDaAppalto As Integer = 0)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Appalti_StruttureMansioni where IdAppalto = " & IdAppalto & " And IdStrutture = " & Struttura & " And ModificaDaAppalto = " & ModificaDaAppalto
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            'If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
            MySql = "INSERT INTO Appalti_StruttureMansioni ([IdAppalto],[IdStrutture],IdMansione,Regola,Importo,Mastro,Conto,Sottoconto,CodiceIVA,ModificaDaAppalto,intypecod) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IdAppalto", IdAppalto)
            cmdw.Parameters.AddWithValue("@Struttura", Struttura)
            cmdw.Parameters.AddWithValue("@IdMansione", Convert.ToDouble(Tabella.Rows(i).Item(0)))
            cmdw.Parameters.AddWithValue("@Regola", Tabella.Rows(i).Item(1))
            cmdw.Parameters.AddWithValue("@Importo", Convert.ToDouble(Tabella.Rows(i).Item(2)))




            Dim Vettore(100) As String

            Vettore = SplitWords(campodb(Tabella.Rows(i).Item(3)))

            Mastro = 0
            Conto = 0
            Sottoconto = 0
            If Vettore.Length >= 3 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If
            cmdw.Parameters.AddWithValue("@Mastro", Mastro)
            cmdw.Parameters.AddWithValue("@Conto", Conto)
            cmdw.Parameters.AddWithValue("@Sottoconto", Sottoconto)
            cmdw.Parameters.AddWithValue("@CodiceIVA", campodb(Tabella.Rows(i).Item(4)))
            cmdw.Parameters.AddWithValue("@ModificaDaAppalto", ModificaDaAppalto)
            cmdw.Parameters.AddWithValue("@intypecod", Tabella.Rows(i).Item(5))


            cmdw.Transaction = Transan
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
            'End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




End Class
