﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_AppaltiTabellaMansione
    Public ID As Integer
    Public Descrizione As String
    Public CodificaImport As String
    Public CodificaImport2 As String
    Public Struttura As Integer
    Public Festivo As Integer


    Public Sub UpDateDropBoxSt(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal Struttura As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Appalti_Mansione Where Struttura = ? Or (Struttura = 0 or struttura is null) Order by Descrizione")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Struttura", Struttura)

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Appalti_Mansione Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection


        If IsNothing(MyCodice) Then
            MyCodice = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Mansione where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            CodificaImport = campodb(myPOSTreader.Item("CodificaImport"))

            CodificaImport2 = campodb(myPOSTreader.Item("CodificaImport2"))

            Struttura = campodbn(myPOSTreader.Item("Struttura"))
            Festivo = campodbn(myPOSTreader.Item("Festivo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiImport(ByVal StringaConnessione As String, ByVal CodificaImport As String)
        Dim cn As OleDbConnection


        If IsNothing(CodificaImport) Then
            CodificaImport = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Mansione where " & _
                           "CodificaImport=  ? or CodificaImport2 = ? ")
        cmd.Parameters.AddWithValue("@CodificaImport", CodificaImport)
        cmd.Parameters.AddWithValue("@CodificaImport2", CodificaImport)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            CodificaImport = campodb(myPOSTreader.Item("CodificaImport"))
            CodificaImport2 = campodb(myPOSTreader.Item("CodificaImport2"))
            Struttura = campodbn(myPOSTreader.Item("Struttura"))
            Festivo = campodbn(myPOSTreader.Item("Festivo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiImportFestivoFeriale(ByVal StringaConnessione As String, ByVal CodificaImport As String, ByVal Festivo As Integer)
        Dim cn As OleDbConnection


        If IsNothing(CodificaImport) Then
            CodificaImport = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Festivo = 1 Then

            cmd.CommandText = ("select * from Appalti_Mansione where " & _
                               "CodificaImport = ? And Festivo = 1")
        Else
            cmd.CommandText = ("select * from Appalti_Mansione where " & _
                           "CodificaImport = ? And Festivo = 0")

        End If

        cmd.Parameters.AddWithValue("@CodificaImport", CodificaImport)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            CodificaImport = campodb(myPOSTreader.Item("CodificaImport"))
            CodificaImport2 = campodb(myPOSTreader.Item("CodificaImport2"))
            Struttura = campodbn(myPOSTreader.Item("Struttura"))
            Festivo = campodbn(myPOSTreader.Item("Festivo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection


        If IsNothing(MyCodice) Then
            MyCodice = ""
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Mansione where " & _
                           "Descrizione = ? ")
        cmd.Parameters.AddWithValue("@MyCodice", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            CodificaImport = campodb(myPOSTreader.Item("CodificaImport"))
            CodificaImport2 = campodb(myPOSTreader.Item("CodificaImport2"))
            Struttura = campodbn(myPOSTreader.Item("Struttura"))
            Festivo = campodbn(myPOSTreader.Item("Festivo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("delete from Appalti_Mansione where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Mansione where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO Appalti_Mansione (DESCRIZIONE) values (?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()

            Dim cmdMId As New OleDbCommand()
            cmdMId.CommandText = ("select max(id) from Appalti_Mansione ")
            cmdMId.Connection = cn
            Dim ReadMid As OleDbDataReader = cmdMId.ExecuteReader()
            If ReadMid.Read() Then
                ID = campodbn(ReadMid.Item(0))
            End If
        End If
        myPOSTreader.Close()


        Dim MySql As String

        MySql = "UPDATE Appalti_Mansione SET " & _
                " Descrizione = ?, " & _
                " CodificaImport = ?, " & _
                " CodificaImport2 = ?, " & _
                " Struttura = ?, " & _
                " Festivo = ? " & _
                " Where ID = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@CodificaImport", CodificaImport)
        cmd1.Parameters.AddWithValue("@CodificaImport2", CodificaImport2)
        cmd1.Parameters.AddWithValue("@Struttura", Struttura)
        cmd1.Parameters.AddWithValue("@Festivo", Festivo)
        cmd1.Parameters.AddWithValue("@ID", ID)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
End Class
