﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_AppaltiAnagrafica
    Public Id As Integer
    Public Descrizione As String
    Public Gruppo As String
    Public Cig As String
    Public Committente As String
    Public DataInizio As Date
    Public DataFine As Date
    Public ImportoTeoricoMensile As Double
    Public ImportoTeoricoGiornaliero As Double
    Public ImportoTeoricoMensileInaddGiornaliero As Double
    Public ImportoForfait As Double
    Public CausaleContabile As String
    Public CodiceIva As String
    Public RotturaStruttura As Integer
    Public Tipo As String
    Public RaggruppaRicavi As Integer
    Public Mastro As Integer
    Public Conto As Integer
    Public Sottoconto As Integer

    Public CodiceIvaKM As String
    Public MastroKM As Integer
    Public ContoKM As Integer
    Public SottocontoKM As Integer
    Public RaggruppaSenzaPresaInCarico As Integer


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Anagrafica where " & _
                           "Id Like ? ")
        cmd.Parameters.AddWithValue("@Id", Id)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Gruppo = campodb(myPOSTreader.Item("Gruppo"))
            Cig = campodb(myPOSTreader.Item("Cig"))
            DataInizio = campodbd(myPOSTreader.Item("DataInizio"))
            DataFine = campodbd(myPOSTreader.Item("DataFine"))
            ImportoTeoricoMensile = campodbn(myPOSTreader.Item("ImportoTeoricoMensile"))
            ImportoTeoricoGiornaliero = campodbn(myPOSTreader.Item("ImportoTeoricoGiornaliero"))
            ImportoTeoricoMensileInaddGiornaliero = campodbn(myPOSTreader.Item("ImportoTeoricoMensileInaddGiornaliero"))
            ImportoForfait = campodbn(myPOSTreader.Item("ImportoForfait"))
            Committente = campodb(myPOSTreader.Item("Committente"))
            CausaleContabile = campodb(myPOSTreader.Item("CausaleContabile"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            RotturaStruttura = campodbn(myPOSTreader.Item("RotturaStruttura"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            RaggruppaRicavi = campodbn(myPOSTreader.Item("RaggruppaRicavi"))

            Mastro = campodbn(myPOSTreader.Item("Mastro"))
            Conto = campodbn(myPOSTreader.Item("Conto"))
            Sottoconto = campodbn(myPOSTreader.Item("Sottoconto"))

            CodiceIvaKM = campodb(myPOSTreader.Item("CodiceIvaKM"))

            MastroKM = campodbn(myPOSTreader.Item("MastroKM"))
            ContoKM = campodbn(myPOSTreader.Item("ContoKM"))
            SottocontoKM = campodbn(myPOSTreader.Item("SottocontoKM"))

            RaggruppaSenzaPresaInCarico = campodbn(myPOSTreader.Item("RaggruppaSenzaPresaInCarico"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Appalti_Anagrafica Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_Anagrafica where " & _
                           "Id = ? ")
        cmd.Parameters.AddWithValue("@Id", Id)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO Appalti_Anagrafica (DESCRIZIONE) values (?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql            
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()

            Dim cmdMId As New OleDbCommand()
            cmdMId.CommandText = ("select max(id) from Appalti_Anagrafica ")            
            cmdMId.Connection = cn
            Dim ReadMid As OleDbDataReader = cmdMId.ExecuteReader()
            If ReadMid.Read() Then
                Id = campodbn(ReadMid.Item(0))
            End If
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE Appalti_Anagrafica SET " & _
                " Descrizione = ?, " & _
                " Gruppo = ?, " & _
                " Cig = ?, " & _
                " DataInizio = ?,  " & _
                " DataFine = ?, " & _
                " ImportoTeoricoMensile = ?, " & _
                " ImportoTeoricoGiornaliero = ?, " & _
                " ImportoTeoricoMensileInaddGiornaliero = ?, " & _
                " ImportoForfait = ?, " & _
                " Committente = ?, " & _
                " CausaleContabile = ?, " & _
                " CodiceIVA = ?, " & _
                " RotturaStruttura =?, " & _
                " Tipo =?, " & _
                " RaggruppaRicavi = ?, " & _
                " Mastro = ? ," & _
                " Conto= ? ," & _
                " Sottoconto = ?, " & _
                " CodiceIVAKM = ?, " & _
                " MastroKM = ? ," & _
                " ContoKM= ? ," & _
                " SottocontoKM = ?, " & _
                " RaggruppaSenzaPresaInCarico = ? " & _
                " Where ID = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Gruppo", Gruppo)
        cmd1.Parameters.AddWithValue("@Cig", Cig)
        cmd1.Parameters.AddWithValue("@DataInizio", DataInizio)
        cmd1.Parameters.AddWithValue("@DataFine", DataFine)
        cmd1.Parameters.AddWithValue("@ImportoTeoricoMensile", ImportoTeoricoMensile)
        cmd1.Parameters.AddWithValue("@ImportoTeoricoGiornaliero", ImportoTeoricoGiornaliero)
        cmd1.Parameters.AddWithValue("@ImportoTeoricoMensileInaddGiornaliero", ImportoTeoricoMensileInaddGiornaliero)
        cmd1.Parameters.AddWithValue("@ImportoForfait", ImportoForfait)
        cmd1.Parameters.AddWithValue("@Committente", Committente)
        cmd1.Parameters.AddWithValue("@CausaleContabile", CausaleContabile)
        cmd1.Parameters.AddWithValue("@CodiceIVA", CodiceIva)
        cmd1.Parameters.AddWithValue("@RotturaStruttura", RotturaStruttura)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@RaggruppaRicavi", RaggruppaRicavi)
        cmd1.Parameters.AddWithValue("@Mastro", Mastro)
        cmd1.Parameters.AddWithValue("@Conto", Conto)
        cmd1.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd1.Parameters.AddWithValue("@CodiceIVAKM", CodiceIvaKM)
        cmd1.Parameters.AddWithValue("@MastroKM", MastroKM)
        cmd1.Parameters.AddWithValue("@ContoKM", ContoKM)
        cmd1.Parameters.AddWithValue("@SottocontoKM", SottocontoKM)
        cmd1.Parameters.AddWithValue("@RaggruppaSenzaPresaInCarico", RaggruppaSenzaPresaInCarico)
        cmd1.Parameters.AddWithValue("@ID", Id)

        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub
End Class

