﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Appalti_Programmazione
    Public ID As Integer
    Public Giorno As Integer
    Public IdOperatore As Integer
    Public IdAppalto As Integer
    Public IdStruttura As Integer
    Public IdMansione As Integer
    Public CodiceOspite As String
    Public Ore As Double

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub GeneraGiorno(ByVal StringaConnessione As String, ByVal StringaConnessioneOspiti As String, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdOperatore As Integer, ByVal Data As Date)
        Dim GiornoSettimana As Integer

        GiornoSettimana = Data.DayOfWeek
        If GiornoSettimana = 0 Then GiornoSettimana = 7


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_Programmazione where IdOperatore = ? And Regola =0 And Giorni Like ? And IdAppalto = ? And IdStruttura =  ?")
        cmd.Parameters.AddWithValue("@IdOperatore", IdOperatore)

        cmd.Parameters.AddWithValue("@Giorno", "%<" & GiornoSettimana & ">%")
        cmd.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmd.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmd.Connection = cn

        Dim PianificatoGiorno As OleDbDataReader = cmd.ExecuteReader()
        Do While PianificatoGiorno.Read
            Dim Prestazione As New Cls_Appalti_Prestazioni

            Prestazione.Data = Data
            Prestazione.IdAppalto = IdAppalto
            Prestazione.IdStrutture = IdStruttura
            Prestazione.CodiceOperatore = IdOperatore
            Prestazione.CodiceOspite = campodbn(PianificatoGiorno.Item("CodiceOspite"))
            Prestazione.Ore = campodbn(PianificatoGiorno.Item("Ore"))
            Prestazione.IdMansione = campodbn(PianificatoGiorno.Item("idMansione"))

            Dim TipoOspite As New ClsOspite

            TipoOspite.CodiceOspite = campodbn(PianificatoGiorno.Item("CodiceOspite"))
            TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

            If Not IsNothing(TipoOspite.TipoOspite) Then
                Dim TipoUtente As New Cls_Appalti_TipoUtente

                TipoUtente.intypecod = TipoOspite.TipoOspite
                TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                Prestazione.intypecod = TipoUtente.intypecod
                Prestazione.intypedescription = TipoUtente.intypedescription

            End If


            Prestazione.Scrivi(StringaConnessione)

        Loop
        PianificatoGiorno.Close()




        Dim cmdProgrammata As New OleDbCommand()
        cmdProgrammata.CommandText = ("select * from Appalti_Programmazione where IdOperatore = ? And Regola =1 And Giorni Like ? And IdAppalto = ? And IdStruttura =  ? And DataInizio <= ?")
        cmdProgrammata.Parameters.AddWithValue("@IdOperatore", IdOperatore)

        cmdProgrammata.Parameters.AddWithValue("@Giorno", "%<" & GiornoSettimana & ">%")
        cmdProgrammata.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdProgrammata.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmdProgrammata.Parameters.AddWithValue("@DataInizio", Data)
        cmdProgrammata.Connection = cn
        Dim PianificatoGiornoProgrammata As OleDbDataReader = cmdProgrammata.ExecuteReader()
        Do While PianificatoGiornoProgrammata.Read
            Dim DataInizio As Date
            Dim NumeroRipetizioni As Integer
            Dim Ripetuto As Integer = 0

            DataInizio = campodb(PianificatoGiornoProgrammata.Item("DataInizio"))
            NumeroRipetizioni = campodbn(PianificatoGiornoProgrammata.Item("NumeroRipetizioni"))
            If NumeroRipetizioni > 0 Then
                Dim Inidice As Integer
                For Inidice = 1 To DateDiff(DateInterval.Day, DataInizio, Data)
                    Dim DataCheck As Date

                    DataCheck = DateAdd(DateInterval.Day, Inidice, DataInizio)
                    If DataCheck.DayOfWeek = Data.DayOfWeek Then
                        Ripetuto = Ripetuto + 1
                    End If
                Next

                If Ripetuto <= NumeroRipetizioni Then
                    Dim Prestazione As New Cls_Appalti_Prestazioni

                    Prestazione.Data = Data
                    Prestazione.IdAppalto = IdAppalto
                    Prestazione.IdStrutture = IdStruttura
                    Prestazione.CodiceOperatore = IdOperatore
                    Prestazione.CodiceOspite = campodbn(PianificatoGiornoProgrammata.Item("CodiceOspite"))
                    Prestazione.Ore = campodbn(PianificatoGiornoProgrammata.Item("Ore"))
                    Prestazione.IdMansione = campodbn(PianificatoGiornoProgrammata.Item("idMansione"))

                    Dim TipoOspite As New ClsOspite

                    TipoOspite.CodiceOspite = campodbn(PianificatoGiornoProgrammata.Item("CodiceOspite"))
                    TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

                    If Not IsNothing(TipoOspite.TipoOspite) Then
                        Dim TipoUtente As New Cls_Appalti_TipoUtente

                        TipoUtente.intypecod = TipoOspite.TipoOspite
                        TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                        Prestazione.intypecod = TipoUtente.intypecod
                        Prestazione.intypedescription = TipoUtente.intypedescription

                    End If


                    Prestazione.Scrivi(StringaConnessione)

                End If

            End If


        Loop
        PianificatoGiornoProgrammata.Close()



        Dim cmdProgrammataIntervallo As New OleDbCommand()
        cmdProgrammataIntervallo.CommandText = ("select * from Appalti_Programmazione where IdOperatore = ? And Regola =2 And IdAppalto = ? And IdStruttura =  ? And DataInizio <= ?")
        cmdProgrammataIntervallo.Parameters.AddWithValue("@IdOperatore", IdOperatore)
        cmdProgrammataIntervallo.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdProgrammataIntervallo.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmdProgrammataIntervallo.Parameters.AddWithValue("@DataInizio", Data)
        cmdProgrammataIntervallo.Connection = cn
        Dim PianificatoIntervallo As OleDbDataReader = cmdProgrammataIntervallo.ExecuteReader()
        Do While PianificatoIntervallo.Read
            Dim DataInizio As Date
            Dim NumeroRipetizioni As Integer
            Dim Intervallo As Integer
            Dim Giorni As String = ""

            DataInizio = campodb(PianificatoIntervallo.Item("DataInizio"))
            NumeroRipetizioni = campodbn(PianificatoIntervallo.Item("NumeroRipetizioni"))

            Giorni = campodb(PianificatoIntervallo.Item("Giorni"))

            Intervallo = Giorni.Replace("<INTERVALLO=", "").Replace(">", "")

            If Format(Data, "yyyyMMdd") >= Format(DataInizio, "yyyyMMdd") Then
                Dim Indice As Integer
                Dim InizioData As Date = DataInizio



                If Format(Data, "yyyyMMdd") = Format(InizioData, "yyyyMMdd") Then
                    Dim Prestazione As New Cls_Appalti_Prestazioni

                    Prestazione.Data = Data
                    Prestazione.IdAppalto = IdAppalto
                    Prestazione.IdStrutture = IdStruttura
                    Prestazione.CodiceOperatore = IdOperatore
                    Prestazione.CodiceOspite = campodbn(PianificatoIntervallo.Item("CodiceOspite"))
                    Prestazione.Ore = campodbn(PianificatoIntervallo.Item("Ore"))
                    Prestazione.IdMansione = campodbn(PianificatoIntervallo.Item("idMansione"))

                    Dim TipoOspite As New ClsOspite

                    TipoOspite.CodiceOspite = campodbn(PianificatoIntervallo.Item("CodiceOspite"))
                    TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

                    If Not IsNothing(TipoOspite.TipoOspite) Then
                        Dim TipoUtente As New Cls_Appalti_TipoUtente

                        TipoUtente.intypecod = TipoOspite.TipoOspite
                        TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                        Prestazione.intypecod = TipoUtente.intypecod
                        Prestazione.intypedescription = TipoUtente.intypedescription

                    End If


                    Prestazione.Scrivi(StringaConnessione)
                End If

                For Indice = 1 To NumeroRipetizioni - 1

                    InizioData = InizioData.AddDays(Intervallo)

                    If Format(Data, "yyyyMMdd") = Format(InizioData, "yyyyMMdd") Then
                        Dim Prestazione As New Cls_Appalti_Prestazioni

                        Prestazione.Data = Data
                        Prestazione.IdAppalto = IdAppalto
                        Prestazione.IdStrutture = IdStruttura
                        Prestazione.CodiceOperatore = IdOperatore
                        Prestazione.CodiceOspite = campodbn(PianificatoIntervallo.Item("CodiceOspite"))
                        Prestazione.Ore = campodbn(PianificatoIntervallo.Item("Ore"))
                        Prestazione.IdMansione = campodbn(PianificatoIntervallo.Item("idMansione"))

                        Dim TipoOspite As New ClsOspite

                        TipoOspite.CodiceOspite = campodbn(PianificatoIntervallo.Item("CodiceOspite"))
                        TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

                        If Not IsNothing(TipoOspite.TipoOspite) Then
                            Dim TipoUtente As New Cls_Appalti_TipoUtente

                            TipoUtente.intypecod = TipoOspite.TipoOspite
                            TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                            Prestazione.intypecod = TipoUtente.intypecod
                            Prestazione.intypedescription = TipoUtente.intypedescription

                        End If


                        Prestazione.Scrivi(StringaConnessione)
                    End If

                Next
            End If
        Loop
        PianificatoIntervallo.Close()


        Dim cmdProgrammataMese As New OleDbCommand()
        cmdProgrammataMese.CommandText = ("select * from Appalti_Programmazione where IdOperatore = ? And Regola =3 And IdAppalto = ? And IdStruttura =  ? And DataInizio <= ?")
        cmdProgrammataMese.Parameters.AddWithValue("@IdOperatore", IdOperatore)
        cmdProgrammataMese.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdProgrammataMese.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmdProgrammataMese.Parameters.AddWithValue("@DataInizio", Data)
        cmdProgrammataMese.Connection = cn
        Dim PianificatoMese As OleDbDataReader = cmdProgrammataMese.ExecuteReader()
        Do While PianificatoMese.Read
            Dim DataInizio As Date
            Dim NumeroRipetizioni As Integer
            Dim Intervallo As Integer
            Dim Giorni As String = ""

            DataInizio = campodb(PianificatoMese.Item("DataInizio"))
            NumeroRipetizioni = campodbn(PianificatoMese.Item("NumeroRipetizioni"))

            Giorni = campodb(PianificatoMese.Item("Giorni"))

            Intervallo = Giorni.Replace("<MESE=", "").Replace(">", "")

            If Format(Data, "yyyyMMdd") >= Format(DataInizio, "yyyyMMdd") Then
                Dim Indice As Integer
                Dim InizioData As Date = DataInizio

                If Day(Data) = Intervallo Then
                    Dim Prestazione As New Cls_Appalti_Prestazioni

                    Prestazione.Data = Data
                    Prestazione.IdAppalto = IdAppalto
                    Prestazione.IdStrutture = IdStruttura
                    Prestazione.CodiceOperatore = IdOperatore
                    Prestazione.CodiceOspite = campodbn(PianificatoMese.Item("CodiceOspite"))
                    Prestazione.Ore = campodbn(PianificatoMese.Item("Ore"))
                    Prestazione.IdMansione = campodbn(PianificatoMese.Item("idMansione"))

                    Dim TipoOspite As New ClsOspite

                    TipoOspite.CodiceOspite = campodbn(PianificatoMese.Item("CodiceOspite"))
                    TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

                    If Not IsNothing(TipoOspite.TipoOspite) Then
                        Dim TipoUtente As New Cls_Appalti_TipoUtente

                        TipoUtente.intypecod = TipoOspite.TipoOspite
                        TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                        Prestazione.intypecod = TipoUtente.intypecod
                        Prestazione.intypedescription = TipoUtente.intypedescription

                    End If


                    Prestazione.Scrivi(StringaConnessione)
                End If

            End If
        Loop
        PianificatoMese.Close()



        Dim cmdProgrammataData As New OleDbCommand()
        cmdProgrammataData.CommandText = ("select * from Appalti_Programmazione where IdOperatore = ? And Regola =4 And IdAppalto = ? And IdStruttura =  ? And DataInizio <= ?")
        cmdProgrammataData.Parameters.AddWithValue("@IdOperatore", IdOperatore)
        cmdProgrammataData.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdProgrammataData.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmdProgrammataData.Parameters.AddWithValue("@DataInizio", Data)
        cmdProgrammataData.Connection = cn
        Dim PianificatoData As OleDbDataReader = cmdProgrammataData.ExecuteReader()
        Do While PianificatoData.Read
            Dim DataInizio As Date
            Dim NumeroRipetizioni As Integer
            Dim Intervallo As Integer
            Dim Giorni As String = ""

            DataInizio = campodb(PianificatoData.Item("DataInizio"))
            NumeroRipetizioni = campodbn(PianificatoData.Item("NumeroRipetizioni"))

            
            If Format(Data, "yyyyMMdd") >= Format(DataInizio, "yyyyMMdd") Then
                Dim Indice As Integer
                Dim InizioData As Date = DataInizio

                Dim Prestazione As New Cls_Appalti_Prestazioni

                Prestazione.Data = Data
                Prestazione.IdAppalto = IdAppalto
                Prestazione.IdStrutture = IdStruttura
                Prestazione.CodiceOperatore = IdOperatore
                Prestazione.CodiceOspite = campodbn(PianificatoData.Item("CodiceOspite"))
                Prestazione.Ore = campodbn(PianificatoData.Item("Ore"))
                Prestazione.IdMansione = campodbn(PianificatoData.Item("idMansione"))

                Dim TipoOspite As New ClsOspite

                TipoOspite.CodiceOspite = campodbn(PianificatoData.Item("CodiceOspite"))
                TipoOspite.Leggi(StringaConnessioneOspiti, TipoOspite.CodiceOspite)

                If Not IsNothing(TipoOspite.TipoOspite) Then
                    Dim TipoUtente As New Cls_Appalti_TipoUtente

                    TipoUtente.intypecod = TipoOspite.TipoOspite
                    TipoUtente.Leggi(StringaConnessione, TipoUtente.intypecod)

                    Prestazione.intypecod = TipoUtente.intypecod
                    Prestazione.intypedescription = TipoUtente.intypedescription

                End If


                Prestazione.Scrivi(StringaConnessione)
            End If
        Loop
        PianificatoData.Close()
        cn.Close()
    End Sub



    Sub loaddati(ByVal StringaConnessione As String, ByVal IdOperatore As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_Programmazione where IdOperatore = " & IdOperatore)
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Clear()
        
        Tabella.Columns.Add("Regole", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Giorni", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("Struttura", GetType(String))
        Tabella.Columns.Add("Utente", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()



            myriga(0) = campodbn(myPOSTreader.Item("Regola"))

            myriga(1) = campodb(myPOSTreader.Item("DataInizio"))
            If IsDate(campodb(myPOSTreader.Item("DataInizio"))) Then
                myriga(1) = Format(Convert.ToDateTime(myPOSTreader.Item("DataInizio")), "dd/MM/yyyy")
            End If

            myriga(2) = campodbn(myPOSTreader.Item("NumeroRipetizioni"))

            myriga(3) = campodb(myPOSTreader.Item("Giorni"))

            myriga(4) = campodbn(myPOSTreader.Item("idMansione"))
            myriga(5) = campodbn(myPOSTreader.Item("IdAppalto"))

            myriga(6) = campodbn(myPOSTreader.Item("IdStruttura"))

            myriga(7) = campodbn(myPOSTreader.Item("CodiceOspite"))

            myriga(8) = campodbn(myPOSTreader.Item("Ore"))


            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = 0

            myriga(1) = ""

            myriga(2) = 0
            myriga(3) = 0

            myriga(4) = 0
            myriga(5) = 0
            myriga(6) = 0
            myriga(7) = 0
            myriga(8) = 0

            Tabella.Rows.Add(myriga)
        End If
        cn.Close()


    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Appalti_Programmazione where IdOperatore = " & IdOperatore
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            'If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
            MySql = "INSERT INTO Appalti_Programmazione ([Regola],DataInizio,NumeroRipetizioni,Giorni,[IdOperatore],IdMansione,CodiceOspite ,Ore,IdAppalto,IdStruttura ) VALUES (?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Regola", Convert.ToDouble(Tabella.Rows(i).Item(0)))

            If Tabella.Rows(i).Item(1) <> "" Then
                cmdw.Parameters.AddWithValue("@DataInizio", Convert.ToDateTime(Tabella.Rows(i).Item(1)))
            Else
                cmdw.Parameters.AddWithValue("@DataInizio", System.DBNull.Value)
            End If
            cmdw.Parameters.AddWithValue("@NumeroRipetizioni", Convert.ToDouble(Tabella.Rows(i).Item(2)))
            cmdw.Parameters.AddWithValue("@Giorni", Convert.ToString(Tabella.Rows(i).Item(3)))
            cmdw.Parameters.AddWithValue("@IdOperatore", IdOperatore)
            cmdw.Parameters.AddWithValue("@IdMansione", Convert.ToDouble(Tabella.Rows(i).Item(4)))


            cmdw.Parameters.AddWithValue("@CodiceOspite", campodbn(Tabella.Rows(i).Item(7)))
            cmdw.Parameters.AddWithValue("@Ore", Convert.ToDouble(Tabella.Rows(i).Item(8)))
            cmdw.Parameters.AddWithValue("@IdAppalto", Convert.ToDouble(Tabella.Rows(i).Item(5)))
            cmdw.Parameters.AddWithValue("@IdStruttura", Convert.ToDouble(Tabella.Rows(i).Item(6)))
            cmdw.Transaction = Transan
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
            'End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub
End Class
