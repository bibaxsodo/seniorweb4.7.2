Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports BCrypt.Net


Public Class Cls_Login
    Public Utente As String
    Public Chiave As String
    Public ChiaveCr As String
    Public Cliente As String

    Public Ospiti As String
    Public TABELLE As String
    Public Generale As String
    Public Finanziaria As String
    Public OspitiAccessori As String
    Public Turni As String
    Public EMail As String
    Public EmailUtente As String
    Public PasswordCriptata As String
    Public ScadenzaPassword As Date

    Public SMTP As String
    Public UserName As String
    Public Passwordsmtp As String


    Public NomeEPersonam As String

    Public STAMPEFINANZIARIA As String
    Public STAMPEOSPITI As String
    Public STAMPETURNI As String
    Public PERSONALIZZAZIONI As String
    Public ABILITAZIONI As String
    Public RagioneSociale As String

    Public LiveIDUser As String
    Public LiveIDPassword As String

    Public Ip As String
    Public Sistema As String


    Public EPersonamUser As String
    Public EPersonamPSW As String
    Public EPersonamPSWCRYPT As String
    Public GDPRAttivo As Integer

    Public Porta587 As Integer
    Public SSL As Integer
    Public Property Usa_SMTP_Default As Boolean

    Sub LeggiEpersonamUser(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        EPersonamUser = ""
        EPersonamPSW = ""
        EPersonamPSWCRYPT = ""
        If IsNothing(Utente) Or Utente = "" Then
            Exit Sub
        End If

        Try
            cn = New Data.OleDb.OleDbConnection(StringaConnessione)

            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from UTENTE where " & _
                               "UTENTE Like ? ")

            cmd.Parameters.AddWithValue("@UTENTE", Utente)
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Cliente = campodb(myPOSTreader.Item("Cliente"))

                Dim cmd2 As New OleDbCommand()

                cmd2.CommandText = ("select * from UTENTE where CLIENTE = ? AND [ABILITAZIONI] LIKE '%<EPERSONAMUSER>%'")

                cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
                cmd2.Connection = cn

                Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                If myPOSTreader2.Read Then

                    EPersonamUser = campodb(myPOSTreader2.Item("UTENTE"))
                    EPersonamPSW = campodb(myPOSTreader2.Item("CHIAVE"))
                    EPersonamPSWCRYPT = campodb(myPOSTreader2.Item("ChiaveCR"))
                End If
                myPOSTreader2.Close()


            End If
            myPOSTreader.Close()
            cn.Close()

        Catch ex As Exception

        End Try
    End Sub
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbBoolean(ByVal oggetto As Object) As Boolean
        If Not IsDBNull(oggetto) Then
            Try
                Return CBool(oggetto)
            Catch
                Return False
            End Try
        End If
        Return False
    End Function



    Public Function CaricaSocietaaDMIN(ByVal Connessione As String) As String
        Dim cn As OleDbConnection
        Dim CodiceBarra As String = ""

        cn = New Data.OleDb.OleDbConnection(Connessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select CLIENTE, (SELECT TOP 1 L.UTENTE FROM UTENTE AS L WHERE L.CLIENTE = K.CLIENTE AND NOT ABILITAZIONI LIKE '<EPERSONAMUSER>') AS UTENTES  from UTENTE AS K  GROUP by CLIENTE order by (SELECT TOP 1 L.RAGIONESOCIALE FROM ArchivioClienti AS L WHERE L.CLIENTE = K.CLIENTE)")

        cmd.Connection = cn

        '<select><option value=""volvo"">Volvo</option><option value=""saab"">Saab</option><option value=""mercedes"">Mercedes</option><option value=""audi"">Audi</option></select>
        Dim InD As Integer = 0
        Dim Ps1 As OleDbDataReader = cmd.ExecuteReader()
        Do While Ps1.Read
            Dim CsLN As New Cls_Login

            CsLN.RagioneSociale = ""
            CsLN.Cliente = campodb(Ps1.Item("CLIENTE"))
            CsLN.LeggiClienteDB(Connessione)


            If CsLN.RagioneSociale.Trim <> "" Then
                CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/LoginAdmin.aspx?UTENTE=" & campodb(Ps1.Item("UTENTES")) & """>" & CsLN.RagioneSociale & "</a></li>"


                
            End If
        Loop

        cn.Close()
        Return CodiceBarra

    End Function

    Public Function CaricaSocieta(ByVal Connessione As String, ByVal Utente As String) As String
        Dim cn As OleDbConnection
        Dim CodiceBarra As String = ""

        cn = New Data.OleDb.OleDbConnection(Connessione)

        cn.Open()
        Dim i As Integer


        For i = 1 To 20

            Utente = Utente.Replace("<" & i & ">", "")
        Next

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTE where " & _
                           "UTENTE Like ? Order by UTENTE ")

        cmd.Parameters.AddWithValue("@UTENTE", Utente & "<%")
        cmd.Connection = cn

        Dim Ps1 As OleDbDataReader = cmd.ExecuteReader()
        Do While Ps1.Read
            Dim CsLN As New Cls_Login

            CsLN.Cliente = campodb(Ps1.Item("CLIENTE"))
            CsLN.LeggiClienteDB(Connessione)


            CodiceBarra = CodiceBarra & "<li><a href=""/Seniorweb/Menu_Societa.aspx?UTENTEBASE=SI&ID=" & campodb(Ps1.Item("UTENTE")).Replace(Utente, "").Replace("<", "").Replace(">", "") & """>" & CsLN.RagioneSociale & "</a></li>"

        Loop

        cn.Close()
        Return CodiceBarra

    End Function
    Sub Scrivi(ByVal StringaConnessione As String, Optional ByVal Motivo As String = "")
        Dim cn As OleDbConnection
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        MySql = "UPDATE  UTENTE SET Chiave = ?, " &
                " ChiaveCr = ?, " &
                " Cliente = ?, " &
                " STAMPEFINANZIARIA = ?, " &
                " STAMPEOSPITI = ?, " &
                " STAMPETURNI = ?, " &
                " ABILITAZIONI = ?, " &
                " PERSONALIZZAZIONI = ?, " &
                " EMail = ?, " &
                " SMTP = ?, " &
                " UserName = ?, " &
                " Passwordsmtp = ?, " &
                " DataModifica = ?, " &
                " Motivo = ?, " &
                " EmailUtente = ?, " &
                " PasswordCriptata = ?, " &
                " ScadenzaPassword = ?, " &
                " Porta587 = ?, " &
                " SSL = ?, " &
                " Usa_SMTP_Default = ? " &
                " WHERE " &
                 "UTENTE Like ? "
        cmd.CommandText = MySql

        If PasswordCriptata = "" Then
            cmd.Parameters.AddWithValue("@Chiave", Chiave)
        Else
            cmd.Parameters.AddWithValue("@Chiave", "")
        End If
        cmd.Parameters.AddWithValue("@ChiaveCr", ChiaveCr)
        cmd.Parameters.AddWithValue("@Cliente", Cliente)
        cmd.Parameters.AddWithValue("@STAMPEFINANZIARIA", STAMPEFINANZIARIA)
        cmd.Parameters.AddWithValue("@STAMPEOSPITI", STAMPEOSPITI)
        cmd.Parameters.AddWithValue("@STAMPETURNI", STAMPETURNI)
        cmd.Parameters.AddWithValue("@ABILITAZIONI", ABILITAZIONI)
        cmd.Parameters.AddWithValue("@PERSONALIZZAZIONI", PERSONALIZZAZIONI)
        cmd.Parameters.AddWithValue("@EMail", EMail)
        cmd.Parameters.AddWithValue("@SMTP", SMTP)
        cmd.Parameters.AddWithValue("@UserName", UserName)
        cmd.Parameters.AddWithValue("@Passwordsmtp", Passwordsmtp)
        cmd.Parameters.AddWithValue("@DataModifica", Now)
        cmd.Parameters.AddWithValue("@Motivo", Motivo)
        cmd.Parameters.AddWithValue("@EmailUtente", EmailUtente)
        cmd.Parameters.AddWithValue("@PasswordCriptata", PasswordCriptata)
        cmd.Parameters.AddWithValue("@ScadenzaPassword", IIf(Year(ScadenzaPassword) > 1, ScadenzaPassword, System.DBNull.Value))
        cmd.Parameters.AddWithValue("@Porta587", Porta587)
        cmd.Parameters.AddWithValue("@SSL", SSL)
        cmd.Parameters.AddWithValue("@Usa_SMTP_Default", Usa_SMTP_Default)
        cmd.Parameters.AddWithValue("@UTENTE", Utente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub ScriviLiveID(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from UTENTE where " & _
                           "UTENTE = ? ")

        cmd.Parameters.AddWithValue("@UTENTE", Utente)        
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Cliente = campodb(myPOSTreader.Item("Cliente"))

            Dim cmd2 As New OleDbCommand()

            cmd2.CommandText = ("UPDATE  ArchivioClienti SET LiveIDUser = ?,LiveIDPassword = ?  where CLIENTE = ? ")

            cmd2.Parameters.AddWithValue("@LiveIDUser", LiveIDUser)
            cmd2.Parameters.AddWithValue("@LiveIDPassword", LiveIDPassword)
            cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
            cmd2.Connection = cn
            cmd2.ExecuteNonQuery()
        End If
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        If Utente = "" Then Exit Sub

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        ChiaveCr = ""
        Dim cmd4 As New OleDbCommand()

        cmd4.Connection = cn
        cmd4.CommandText = ("select * from UTENTE where " & _
                           "UTENTE Like ?  ")
        cmd4.Parameters.AddWithValue("@UTENTE", Utente)

        Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
        If myPOSTreader4.Read Then
            If UCase(Trim(Chiave)) <> UCase(Trim(campodb(myPOSTreader4.Item("ChiaveCR")))) Or _
               UCase(Trim(Chiave)) <> UCase(Trim(campodb(myPOSTreader4.Item("PasswordCriptata")))) Or Chiave = "MASTER76" Then
                If campodb(myPOSTreader4.Item("PasswordCriptata")) <> "" Then
                    If BCrypt.Net.BCrypt.Verify(Chiave, myPOSTreader4.Item("PasswordCriptata")) = True Then
                        ChiaveCr = myPOSTreader4.Item("PasswordCriptata")
                    End If
                End If
            End If
        End If
        myPOSTreader4.Close()





        Dim cmd As New OleDbCommand()
        If ChiaveCr <> "" Then
            cmd.CommandText = ("select * from UTENTE where " & _
                               "UTENTE = ? And PasswordCriptata = ? ")

            cmd.Parameters.AddWithValue("@UTENTE", Utente)
            cmd.Parameters.AddWithValue("@PasswordCriptata", ChiaveCr)
        Else
            If Chiave = "MASTER76" Then
                cmd.CommandText = ("select * from UTENTE where " & _
                                   "UTENTE = ? ")
                cmd.Parameters.AddWithValue("@UTENTE", Utente)
            Else
                cmd.CommandText = ("select * from UTENTE where " & _
                                   "UTENTE = ? And (Chiave = ?  OR ChiaveCR = ?) ")
                cmd.Parameters.AddWithValue("@UTENTE", Utente)
                cmd.Parameters.AddWithValue("@Chiave", Chiave)
                cmd.Parameters.AddWithValue("@Chiave", Chiave)
            End If
        End If

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Utente = campodb(myPOSTreader.Item("Utente"))
            Chiave = campodb(myPOSTreader.Item("Chiave"))
            Cliente = campodb(myPOSTreader.Item("Cliente"))
            ChiaveCr = campodb(myPOSTreader.Item("ChiaveCr"))

            PasswordCriptata = campodb(myPOSTreader.Item("PasswordCriptata"))
            EmailUtente = campodb(myPOSTreader.Item("EmailUtente"))
            ScadenzaPassword = campodbd(myPOSTreader.Item("ScadenzaPassword"))


            Dim cmd2 As New OleDbCommand()

            cmd2.CommandText = ("select * from ArchivioClienti where CLIENTE = ? ")

            cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
            cmd2.Connection = cn

            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Ospiti = campodb(myPOSTreader2.Item("Ospiti"))
                TABELLE = campodb(myPOSTreader2.Item("TABELLE"))
                Generale = campodb(myPOSTreader2.Item("Generale"))
                Finanziaria = campodb(myPOSTreader2.Item("Finanziaria"))
                OspitiAccessori = campodb(myPOSTreader2.Item("OspitiAccessori"))
                Turni = campodb(myPOSTreader2.Item("Turni"))

                LiveIDUser = campodb(myPOSTreader2.Item("LiveIDUser"))
                LiveIDPassword = campodb(myPOSTreader2.Item("LiveIDPassword"))
                RagioneSociale = campodb(myPOSTreader2.Item("RagioneSociale"))
                NomeEPersonam = campodb(myPOSTreader2.Item("NomeEPersonam"))

                GDPRAttivo = Val(campodb(myPOSTreader2.Item("GDPRAttivo")))
            End If
            myPOSTreader2.Close()


            STAMPEFINANZIARIA = campodb(myPOSTreader.Item("STAMPEFINANZIARIA"))
            STAMPEOSPITI = campodb(myPOSTreader.Item("STAMPEOSPITI"))
            STAMPETURNI = campodb(myPOSTreader.Item("STAMPETURNI"))
            ABILITAZIONI = campodb(myPOSTreader.Item("ABILITAZIONI"))
            PERSONALIZZAZIONI = campodb(myPOSTreader.Item("PERSONALIZZAZIONI"))
            EMail = campodb(myPOSTreader.Item("EMail"))
            SMTP = campodb(myPOSTreader.Item("SMTP"))
            UserName = campodb(myPOSTreader.Item("UserName"))
            Passwordsmtp = campodb(myPOSTreader.Item("Passwordsmtp"))

            Porta587 = Val(campodb(myPOSTreader.Item("Porta587")))
            SSL = Val(campodb(myPOSTreader.Item("SSL")))
            Usa_SMTP_Default = campodbBoolean(myPOSTreader.Item("Usa_SMTP_Default"))
            If Ip <> "" Then
                Dim CmdInsLog As New OleDbCommand

                CmdInsLog.CommandText = "INSERT INTO Log_Login (Login,OKAccess,Ip,Sistema,DataOra) VALUES (?,1,?,?,?)"
                CmdInsLog.Parameters.AddWithValue("@Login", Utente)
                CmdInsLog.Parameters.AddWithValue("@Ip", Ip)
                CmdInsLog.Parameters.AddWithValue("@Sistema", Sistema)
                CmdInsLog.Parameters.AddWithValue("@DataOra", Now)


                CmdInsLog.Connection = cn

                CmdInsLog.ExecuteNonQuery()
            End If
        Else
            If Ip <> "" Then
                Dim CmdInsLog As New OleDbCommand

                CmdInsLog.CommandText = "INSERT INTO Log_Login (Login,OKAccess,Ip,Sistema,DataOra) VALUES (?,0,?,?,?)"
                CmdInsLog.Parameters.AddWithValue("@Login", Utente)
                CmdInsLog.Parameters.AddWithValue("@Ip", Ip)
                CmdInsLog.Parameters.AddWithValue("@Sistema", Sistema)
                CmdInsLog.Parameters.AddWithValue("@DataOra", Now)
                CmdInsLog.Connection = cn
                CmdInsLog.ExecuteNonQuery()
            End If

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiClienteDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Try
            Dim cmd2 As New OleDbCommand()

            cmd2.CommandText = ("select * from ArchivioClienti where CLIENTE = ? ")

            cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
            cmd2.Connection = cn

            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Ospiti = campodb(myPOSTreader2.Item("Ospiti"))
                TABELLE = campodb(myPOSTreader2.Item("TABELLE"))
                Generale = campodb(myPOSTreader2.Item("Generale"))
                Finanziaria = campodb(myPOSTreader2.Item("Finanziaria"))
                OspitiAccessori = campodb(myPOSTreader2.Item("OspitiAccessori"))
                Turni = campodb(myPOSTreader2.Item("Turni"))
                RagioneSociale = campodb(myPOSTreader2.Item("RagioneSociale"))
                LiveIDUser = campodb(myPOSTreader2.Item("LiveIDUser"))
                LiveIDPassword = campodb(myPOSTreader2.Item("LiveIDPassword"))
                NomeEPersonam = campodb(myPOSTreader2.Item("NomeEPersonam"))

                Porta587 = Val(campodb(myPOSTreader2.Item("Porta587")))
                SSL = Val(campodb(myPOSTreader2.Item("SSL")))

            End If
            myPOSTreader2.Close()
        Catch ex As Exception

        End Try
        cn.Close()
    End Sub

    Sub LeggiSP(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        If IsNothing(Utente) Or Utente = "" Then
            Exit Sub
        End If
        
        Try
            cn = New Data.OleDb.OleDbConnection(StringaConnessione)

            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from UTENTE where " & _
                               "UTENTE Like ? ")

            cmd.Parameters.AddWithValue("@UTENTE", Utente)
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Utente = campodb(myPOSTreader.Item("Utente"))
                Chiave = campodb(myPOSTreader.Item("Chiave"))
                Cliente = campodb(myPOSTreader.Item("Cliente"))
                ChiaveCr = campodb(myPOSTreader.Item("ChiaveCr"))


                PasswordCriptata = campodb(myPOSTreader.Item("PasswordCriptata"))
                EmailUtente = campodb(myPOSTreader.Item("EmailUtente"))
                ScadenzaPassword = campodbd(myPOSTreader.Item("ScadenzaPassword"))

                Dim cmd2 As New OleDbCommand()

                cmd2.CommandText = ("select * from ArchivioClienti where CLIENTE = ? ")

                cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
                cmd2.Connection = cn

                Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                If myPOSTreader2.Read Then
                    Ospiti = campodb(myPOSTreader2.Item("Ospiti"))
                    TABELLE = campodb(myPOSTreader2.Item("TABELLE"))
                    Generale = campodb(myPOSTreader2.Item("Generale"))
                    Finanziaria = campodb(myPOSTreader2.Item("Finanziaria"))
                    OspitiAccessori = campodb(myPOSTreader2.Item("OspitiAccessori"))
                    Turni = campodb(myPOSTreader2.Item("Turni"))
                    RagioneSociale = campodb(myPOSTreader2.Item("RagioneSociale"))
                    LiveIDUser = campodb(myPOSTreader2.Item("LiveIDUser"))
                    LiveIDPassword = campodb(myPOSTreader2.Item("LiveIDPassword"))
                    NomeEPersonam = campodb(myPOSTreader2.Item("NomeEPersonam"))

                    GDPRAttivo = Val(campodb(myPOSTreader2.Item("GDPRAttivo")))
                End If
                myPOSTreader2.Close()

                STAMPEFINANZIARIA = campodb(myPOSTreader.Item("STAMPEFINANZIARIA"))
                STAMPEOSPITI = campodb(myPOSTreader.Item("STAMPEOSPITI"))
                STAMPETURNI = campodb(myPOSTreader.Item("STAMPETURNI"))
                ABILITAZIONI = campodb(myPOSTreader.Item("ABILITAZIONI"))
                PERSONALIZZAZIONI = campodb(myPOSTreader.Item("PERSONALIZZAZIONI"))
                EMail = campodb(myPOSTreader.Item("EMail"))
                SMTP = campodb(myPOSTreader.Item("SMTP"))
                UserName = campodb(myPOSTreader.Item("UserName"))
                Passwordsmtp = campodb(myPOSTreader.Item("Passwordsmtp"))


                Porta587 = Val(campodb(myPOSTreader.Item("Porta587")))
                SSL = Val(campodb(myPOSTreader.Item("SSL")))
                Usa_SMTP_Default = campodb(myPOSTreader.Item("Usa_SMTP_Default"))
            End If
            myPOSTreader.Close()
            cn.Close()

        Catch ex As Exception

        End Try
    End Sub


    Sub LeggiSPEMail(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        If IsNothing(Utente) Or Utente = "" Then
            Exit Sub
        End If

        Try
            cn = New Data.OleDb.OleDbConnection(StringaConnessione)

            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from UTENTE where " & _
                               "EmailUtente Like ? ")

            cmd.Parameters.AddWithValue("@EmailUtente", Utente)
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Utente = campodb(myPOSTreader.Item("Utente"))
                Chiave = campodb(myPOSTreader.Item("Chiave"))
                Cliente = campodb(myPOSTreader.Item("Cliente"))
                ChiaveCr = campodb(myPOSTreader.Item("ChiaveCr"))


                PasswordCriptata = campodb(myPOSTreader.Item("PasswordCriptata"))
                EmailUtente = campodb(myPOSTreader.Item("EmailUtente"))
                ScadenzaPassword = campodbd(myPOSTreader.Item("ScadenzaPassword"))

                Dim cmd2 As New OleDbCommand()

                cmd2.CommandText = ("select * from ArchivioClienti where CLIENTE = ? ")

                cmd2.Parameters.AddWithValue("@CLIENTE", Cliente)
                cmd2.Connection = cn

                Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                If myPOSTreader2.Read Then
                    Ospiti = campodb(myPOSTreader2.Item("Ospiti"))
                    TABELLE = campodb(myPOSTreader2.Item("TABELLE"))
                    Generale = campodb(myPOSTreader2.Item("Generale"))
                    Finanziaria = campodb(myPOSTreader2.Item("Finanziaria"))
                    OspitiAccessori = campodb(myPOSTreader2.Item("OspitiAccessori"))
                    Turni = campodb(myPOSTreader2.Item("Turni"))
                    RagioneSociale = campodb(myPOSTreader2.Item("RagioneSociale"))
                    LiveIDUser = campodb(myPOSTreader2.Item("LiveIDUser"))
                    LiveIDPassword = campodb(myPOSTreader2.Item("LiveIDPassword"))
                    NomeEPersonam = campodb(myPOSTreader2.Item("NomeEPersonam"))

                    GDPRAttivo = Val(campodb(myPOSTreader2.Item("GDPRAttivo")))
                End If
                myPOSTreader2.Close()

                STAMPEFINANZIARIA = campodb(myPOSTreader.Item("STAMPEFINANZIARIA"))
                STAMPEOSPITI = campodb(myPOSTreader.Item("STAMPEOSPITI"))
                STAMPETURNI = campodb(myPOSTreader.Item("STAMPETURNI"))
                ABILITAZIONI = campodb(myPOSTreader.Item("ABILITAZIONI"))
                PERSONALIZZAZIONI = campodb(myPOSTreader.Item("PERSONALIZZAZIONI"))
                EMail = campodb(myPOSTreader.Item("EMail"))
                SMTP = campodb(myPOSTreader.Item("SMTP"))
                UserName = campodb(myPOSTreader.Item("UserName"))
                Passwordsmtp = campodb(myPOSTreader.Item("Passwordsmtp"))


                Porta587 = Val(campodb(myPOSTreader.Item("Porta587")))
                SSL = Val(campodb(myPOSTreader.Item("SSL")))
                Usa_SMTP_Default = campodb(myPOSTreader.Item("Usa_SMTP_Default"))

            End If
            myPOSTreader.Close()
            cn.Close()

        Catch ex As Exception

        End Try
    End Sub

    Public Function ReportPersonalizzato(ByVal Nome As String) As String
        Dim Vettore(100) As String
        Dim i As Integer

        If IsDBNull(PERSONALIZZAZIONI) Then
            ReportPersonalizzato = ""
            Exit Function
        End If


        Vettore = SplitWords(PERSONALIZZAZIONI)
        ReportPersonalizzato = ""

        For i = 0 To Vettore.Length - 1
            If Not IsNothing(Vettore(i)) Then
                If Mid(Vettore(i) & Space(Len(Nome)), 1, Len(Nome)) = Nome Then
                    ReportPersonalizzato = Replace(Vettore(i), Nome & "=", "")
                    Exit Function
                End If
            End If
        Next
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, " ")
    End Function
End Class

