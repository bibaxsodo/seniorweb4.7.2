Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_IVA
    Public Codice As String
    Public Descrizione As String
    Public Aliquota As Double
    Public AllegatoFornitori As Long
    Public AllegatoClienti As Long
    Public Tipo As String
    Public InLiquidazione As Long
    Public DetraibilitaDefault As String
    Public Natura As String
    Public InSpesometro As Integer
    Public NonInUso As Integer


    Public Sub New()
        Codice = ""
        Descrizione = ""
        Aliquota = 0
        AllegatoFornitori = 0
        AllegatoClienti = 0
        Tipo = ""
        InLiquidazione = 0
        DetraibilitaDefault = ""
        Natura = ""
        InSpesometro = 0
        NonInUso = 0
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IVA where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO IVA (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE IVA SET " & _
                " Descrizione = ?, " & _
                " Aliquota = ?, " & _
                " AllegatoFornitori = ?, " & _
                " AllegatoClienti = ?, " & _
                " Tipo = ?, " & _
                " InLiquidazione = ?, " & _
                " DetraibilitaDefault = ?, " & _
                " InSpesometro = ?, " & _
                " Natura = ?, " & _
                " NonInUso = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Aliquota", Aliquota)
        cmd1.Parameters.AddWithValue("@AllegatoFornitori", AllegatoFornitori)
        cmd1.Parameters.AddWithValue("@AllegatoClienti", AllegatoClienti)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@InLiquidazione", InLiquidazione)
        cmd1.Parameters.AddWithValue("@DetraibilitaDefault", DetraibilitaDefault)
        cmd1.Parameters.AddWithValue("@InSpesometro", InSpesometro)
        cmd1.Parameters.AddWithValue("@Natura", Natura)
        cmd1.Parameters.AddWithValue("@NonInUso", NonInUso)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub



    Sub LeggiPerAliquota(ByVal StringaConnessione As String, ByVal RcAliquota As Double)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA where " & _
                           "Aliquota = ? ")
        cmd.Parameters.AddWithValue("@Aliquota", RcAliquota)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Aliquota = CDbl(campodb(myPOSTreader.Item("Aliquota")))
            AllegatoFornitori = Val(campodb(myPOSTreader.Item("AllegatoFornitori")))
            AllegatoClienti = Val(campodb(myPOSTreader.Item("AllegatoClienti")))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            InLiquidazione = Val(campodb(myPOSTreader.Item("InLiquidazione")))
            InSpesometro = Val(campodb(myPOSTreader.Item("InSpesometro")))
            DetraibilitaDefault = campodb(myPOSTreader.Item("DetraibilitaDefault"))
            Natura = campodb(myPOSTreader.Item("Natura"))
            NonInUso = Val(campodb(myPOSTreader.Item("NonInUso")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiPerNatura(ByVal StringaConnessione As String, ByVal RcNatura As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA where " & _
                           "Natura = ? ")
        cmd.Parameters.AddWithValue("@Natura", RcNatura)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Aliquota = CDbl(campodb(myPOSTreader.Item("Aliquota")))
            AllegatoFornitori = Val(campodb(myPOSTreader.Item("AllegatoFornitori")))
            AllegatoClienti = Val(campodb(myPOSTreader.Item("AllegatoClienti")))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            InLiquidazione = Val(campodb(myPOSTreader.Item("InLiquidazione")))
            InSpesometro = Val(campodb(myPOSTreader.Item("InSpesometro")))
            DetraibilitaDefault = campodb(myPOSTreader.Item("DetraibilitaDefault"))
            Natura = campodb(myPOSTreader.Item("Natura"))
            NonInUso = Val(campodb(myPOSTreader.Item("NonInUso")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal Descrizione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA where " & _
                           "Descrizione Like ? ")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Call Leggi(StringaConnessione, Codice)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Aliquota = CDbl(campodb(myPOSTreader.Item("Aliquota")))
            AllegatoFornitori = Val(campodb(myPOSTreader.Item("AllegatoFornitori")))
            AllegatoClienti = Val(campodb(myPOSTreader.Item("AllegatoClienti")))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            InLiquidazione = Val(campodb(myPOSTreader.Item("InLiquidazione")))
            InSpesometro = Val(campodb(myPOSTreader.Item("InSpesometro")))
            DetraibilitaDefault = campodb(myPOSTreader.Item("DetraibilitaDefault"))
            Natura = campodb(myPOSTreader.Item("Natura"))
            NonInUso = Val(campodb(myPOSTreader.Item("NonInUso")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub UpDateDropBoxSoloInUso(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA Where NonInUso Is Null Or NonInUso = 0 Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()

        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IVA Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()

        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub


End Class

