﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ParametriCespiti
    Public CausaleContabile As String
    Public CausaleContabileDismissione As String



    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Sub LeggiParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ParametriCespiti ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CausaleContabile = campodb(myPOSTreader.Item("CausaleContabile"))
            CausaleContabileDismissione = campodb(myPOSTreader.Item("CausaleContabileDismissione"))
         
        End If
        myPOSTreader.Close()


        cn.Close()
    End Sub


    Public Sub ScriviParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE ParametriCespiti SET CausaleContabile=?,CausaleContabileDismissione=?"



        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@CausaleContabile", CausaleContabile)
        cmdw.Parameters.AddWithValue("@CausaleContabileDismissione", CausaleContabileDismissione)
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()
        cn.Close()
    End Sub
End Class
