Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class ClassRetteOspiti
    Public Utente As String
    Public DataAggiornamento As Date
    Public CentroServizio As String
    Public CodiceOspite As Long
    Public Mese As Long
    Public Anno As Long
    Public CODICEPARENTE As Long
    Public Provincia As String
    Public Comune As String
    Public Regione As String
    Public STATOCONTABILE As String
    Public ELEMENTO As String
    Public Giorni As Long
    Public CODICEEXTRA As String
    Public Importo As Double
    Public Descrizione As String
    Public ANNOCOMPETENZA As Long
    Public MESECOMPETENZA As Long
    Public NumeroRegistrazione As Long
    Private OspitiDb As New ADODB.Connection

    Private OspitiCon As OleDbConnection

    Public Sub ChiudiDB()        
        OspitiCon.Close()
    End Sub

    Public Sub ApriDB(ByVal stringaconnessione As String)
        'OspitiDb.Open(stringaconnessione)
        OspitiCon = New Data.OleDb.OleDbConnection(stringaconnessione)

        OspitiCon.Open()

    End Sub

    Public Sub Pulisci()
        Utente = ""
        DataAggiornamento = Now
        CentroServizio = ""
        CodiceOspite = 0
        Mese = 0
        Anno = 0
        CODICEPARENTE = 0
        Provincia = ""
        Comune = ""
        Regione = ""
        STATOCONTABILE = ""
        ELEMENTO = ""
        Giorni = 0
        CODICEEXTRA = ""
        Importo = 0
        Descrizione = ""
        ANNOCOMPETENZA = 0
        MESECOMPETENZA = 0
        NumeroRegistrazione = 0
    End Sub
    Public Sub ScriviRettaComune()

        Dim Campi As String
        Dim Valori As String

        Dim MySql As String

        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "PROVINCIA," : Valori = Valori & "'" & Provincia & "',"
        Campi = Campi & "COMUNE," : Valori = Valori & "'" & Comune & "',"
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "CODICEEXTRA," : Valori = Valori & "'" & CODICEEXTRA & "',"
        Campi = Campi & "IMPORTO," : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".") & ","
        Campi = Campi & "DESCRIZIONE" : Valori = Valori & "'" & Replace(Descrizione, "'", "''") & "'"

        MySql = "INSERT INTO RetteComune (" & Campi & ")  VALUES (" & Valori & ")"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()
    End Sub


    Public Sub ScriviRettaParente()
        Dim Campi As String
        Dim Valori As String


        Dim MySql As String
        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "CodiceParente," : Valori = Valori & CODICEPARENTE & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "CODICEEXTRA," : Valori = Valori & "'" & CODICEEXTRA & "',"
        Campi = Campi & "IMPORTO," : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".") & ","
        Campi = Campi & "DESCRIZIONE," : Valori = Valori & "'" & Replace(Descrizione, "'", "''") & "'" & ","
        Campi = Campi & "ANNOCOMPETENZA," : Valori = Valori & ANNOCOMPETENZA & ","
        Campi = Campi & "MESECOMPETENZA," : Valori = Valori & MESECOMPETENZA & ","
        Campi = Campi & "NumeroRegistrazione" : Valori = Valori & NumeroRegistrazione

        MySql = "INSERT INTO RetteParente (" & Campi & ")  VALUES (" & Valori & ")"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()

    End Sub


    Public Sub ScriviRettaOspite()

        Dim Campi As String
        Dim Valori As String

        Dim MySql As String
        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "CODICEEXTRA," : Valori = Valori & "'" & CODICEEXTRA & "',"
        Campi = Campi & "IMPORTO," : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".") & ","
        Campi = Campi & "DESCRIZIONE," : Valori = Valori & "'" & Replace(Descrizione, "'", "''") & "'" & ","
        Campi = Campi & "ANNOCOMPETENZA," : Valori = Valori & ANNOCOMPETENZA & ","
        Campi = Campi & "MESECOMPETENZA," : Valori = Valori & MESECOMPETENZA & ","
        Campi = Campi & "NumeroRegistrazione" : Valori = Valori & NumeroRegistrazione


        MySql = "INSERT INTO RetteOspite (" & Campi & ")  VALUES (" & Valori & ")"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()

    End Sub

    Public Sub ScriviRettaRegione()

        Dim Campi As String
        Dim Valori As String


        Dim MySql As String
        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "Regione," : Valori = Valori & "'" & Regione & "',"
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "CODICEEXTRA," : Valori = Valori & "'" & CODICEEXTRA & "',"
        Campi = Campi & "IMPORTO," : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".") & ","
        Campi = Campi & "DESCRIZIONE" : Valori = Valori & "'" & Replace(Descrizione, "'", "''") & "'" 
        MySql = "INSERT INTO RetteRegione (" & Campi & ")  VALUES (" & Valori & ")"


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()

    End Sub



    Public Sub ScriviRettaEnte()

        Dim Campi As String
        Dim Valori As String


        Dim MySql As String
        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "IMPORTO" : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".")

        MySql = "INSERT INTO RetteEnte (" & Campi & ")  VALUES (" & Valori & ")"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()

    End Sub



    Public Sub ScriviRettaJolly()
        Dim MySql As String

        Dim Campi As String
        Dim Valori As String


        Campi = "Utente," : Valori = "'" & Utente & "',"
        Campi = Campi & "DataAggiornamento," : Valori = Valori & "{ts '" & Format(DataAggiornamento, "yyyy-MM-dd") & " 00:00:00'}" & ","
        Campi = Campi & "CentroServizio ," : Valori = Valori & "'" & CentroServizio & "',"
        Campi = Campi & "CodiceOspite," : Valori = Valori & CodiceOspite & ","
        Campi = Campi & "MESE," : Valori = Valori & Mese & ","
        Campi = Campi & "ANNO," : Valori = Valori & Anno & ","
        Campi = Campi & "PROVINCIA," : Valori = Valori & "'" & Provincia & "',"
        Campi = Campi & "COMUNE," : Valori = Valori & "'" & Comune & "',"
        Campi = Campi & "STATOCONTABILE," : Valori = Valori & "'" & STATOCONTABILE & "',"
        Campi = Campi & "ELEMENTO," : Valori = Valori & "'" & ELEMENTO & "',"
        Campi = Campi & "GIORNI," : Valori = Valori & Giorni & ","
        Campi = Campi & "CODICEEXTRA," : Valori = Valori & "'" & CODICEEXTRA & "',"
        Campi = Campi & "IMPORTO," : Valori = Valori & Replace(Modulo.MathRound(Importo, 2).ToString, ",", ".") & ","
        Campi = Campi & "DESCRIZIONE," : Valori = Valori & "'" & Replace(Descrizione, "'", "''") & "'" & ","
        Campi = Campi & "ANNOCOMPETENZA," : Valori = Valori & ANNOCOMPETENZA & ","
        Campi = Campi & "MESECOMPETENZA," : Valori = Valori & MESECOMPETENZA & ","
        Campi = Campi & "NumeroRegistrazione" : Valori = Valori & NumeroRegistrazione

        MySql = "INSERT INTO RetteJolly (" & Campi & ")  VALUES (" & Valori & ")"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        cmd.ExecuteNonQuery()

    End Sub


End Class
