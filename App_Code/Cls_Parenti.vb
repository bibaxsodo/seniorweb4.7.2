Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Parenti
    Public CodiceOspite As Long
    Public CodiceParente As Long
    Public Nome As String

    Public NomeParente As String
    Public CognomeParente As String

    Public Sesso As String
    Public DataNascita As Date
    Public ComuneDiNascita As String
    Public ProvinciaDiNascita As String
    Public CODICEFISCALE As String

    Public Telefono1 As String
    Public Telefono2 As String
    Public Telefono3 As String
    Public Telefono4 As String

    Public GradoParentela As String

    Public RESIDENZAINDIRIZZO1 As String
    Public RESIDENZACAP1 As String

    Public RESIDENZAPROVINCIA1 As String
    Public RESIDENZACOMUNE1 As String

    Public CODICEIVA As String
    Public TIPOOPERAZIONE As String
    Public MODALITAPAGAMENTO As String
    Public FattAnticipata As String
    Public PERIODO As String
    Public Compensazione As String
    Public MesiAnticipo As Long

    Public MastroCliente As Integer
    Public ContoCliente As Integer
    Public SottoContoCliente As Integer
    Public CONTOPERESATTO As String
    Public Utente As String

    Public RECAPITONOME As String
    Public RECAPITOCOMUNE As String
    Public RECAPITOPROVINCIA As String
    Public RECAPITOINDIRIZZO As String
    Public RESIDENZACAP4 As String
    Public CABCLIENTE As String
    Public ABICLIENTE As String
    Public IntCliente As String
    Public NumeroControlloCliente As String
    Public CINCLIENTE As String
    Public CCBANCARIOCLIENTE As String
    Public BancaCliente As String
    Public IntestatarioCC As String
    Public CodiceFiscaleCC As String
    Public IdEpersonam As Integer
    Public ParenteIndirizzo As Integer
    Public NOTE As String
    Public RotturaOspite As String
    Public ImportoSconto As Double
    Public TipoSconto As String
    Public Opposizione730 As Integer
    Public CONSENSOINSERIMENTO As Integer
    Public CONSENSOMARKETING As Integer
    Public CodiceCup As String
    Public ExtraFuoriRetta As Long

    Public Intestatario As Integer 'OspiteIntestatario

    Public Sub Pulisci()
        CodiceOspite = 0
        CodiceParente = 0
        Nome = ""
        Sesso = ""
        DataNascita = Nothing
        ComuneDiNascita = ""
        ProvinciaDiNascita = ""
        CODICEFISCALE = ""

        Telefono1 = ""
        Telefono2 = ""
        Telefono3 = ""
        Telefono4 = ""

        GradoParentela = ""

        RESIDENZAINDIRIZZO1 = ""
        RESIDENZACAP1 = ""

        RESIDENZAPROVINCIA1 = ""
        RESIDENZACOMUNE1 = ""

        CODICEIVA = ""
        TIPOOPERAZIONE = ""
        MODALITAPAGAMENTO = ""
        FattAnticipata = ""
        PERIODO = ""
        Compensazione = ""

        MesiAnticipo = 0
        MastroCliente = 0
        ContoCliente = 0
        SottoContoCliente = 0
        CONTOPERESATTO = ""
        NomeParente = ""
        CognomeParente = ""
        IntestatarioCC = ""
        CodiceFiscaleCC = ""
        ParenteIndirizzo = 0
        NOTE = ""
        RotturaOspite = 0
        ImportoSconto = 0
        IdEpersonam = 0

        Opposizione730 = 0
        TipoSconto = ""

        CONSENSOINSERIMENTO = 0
        CONSENSOMARKETING = 0

        Intestatario = 0
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function ParenteIntestatario(ByVal StringaConnessione As String, ByVal CodiceOspite As Integer) As Integer
        Dim cn As OleDbConnection


        ParenteIntestatario = 0
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And TIPOLOGIA = 'P' and OspiteIntestatario  = 1")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ParenteIntestatario = Val(campodb(myPOSTreader.Item("CodiceParente")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub LeggiPerIdEpersonam(ByVal StringaConnessione As String, ByVal CodiceFiscale As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                           "IdEpersonam = ? And TIPOLOGIA = 'P'")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@IdEpersonam", IdEpersonam)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            Leggi(StringaConnessione, CodiceOspite, CodiceParente)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiPerCodiceFiscale(ByVal StringaConnessione As String, ByVal CodiceFiscale As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                           "CodiceFiscale = ? And TIPOLOGIA = 'P'")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            Leggi(StringaConnessione, CodiceOspite, CodiceParente)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiPerCognomeNome(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                           "NomeOspite = ? And  CognomeOspite = ?  And TIPOLOGIA = 'P'")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@NomeParente", NomeParente)
        cmd.Parameters.AddWithValue("@CognomeParente", CognomeParente)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            Leggi(StringaConnessione, CodiceOspite, CodiceParente)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiPerCodiceFiscaleSenzaOspite(ByVal StringaConnessione As String, ByVal CodiceFiscale As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceFiscale = ? And TIPOLOGIA = 'P'")        
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            Leggi(StringaConnessione, CodiceOspite, CodiceParente)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function ParenteDestinatario(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        ParenteDestinatario = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                           "CodiceParente > 0 And ParenteIndirizzo = 1")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)        
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ParenteDestinatario = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function AltroDestinatario(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        AltroDestinatario = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                           "CodiceParente <> ? And ParenteIndirizzo = 1")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            AltroDestinatario = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub Elimina(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CodiceParenti As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from AnagraficaComune where " & _
                           "CodiceOspite = ? And " & _
                           "CodiceParente = ?")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParenti)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

    End Sub
    Sub Leggi(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CodiceParenti As Long)
        Dim cn As OleDbConnection




        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceOspite = ? And " & _
                           "CodiceParente = ?")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParenti)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = CodiceOspite


            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))
            Telefono3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO1"))
            Telefono4 = campodb(myPOSTreader.Item("RESIDENZATELEFONO2"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))

            CODICEFISCALE = campodb(myPOSTreader.Item("CODICEFISCALE"))
            CODICEIVA = campodb(myPOSTreader.Item("CODICEIVA"))
            TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MODALITAPAGAMENTO = campodb(myPOSTreader.Item("MODALITAPAGAMENTO"))

            MastroCliente = Val(campodb(myPOSTreader.Item("MastroCliente")))
            ContoCliente = Val(campodb(myPOSTreader.Item("ContoCliente")))
            SottoContoCliente = Val(campodb(myPOSTreader.Item("SottoContoCliente")))
            Nome = campodb(myPOSTreader.Item("Nome"))

            NomeParente = campodb(myPOSTreader.Item("NomeOspite"))
            CognomeParente = campodb(myPOSTreader.Item("CognomeOspite"))
            If NomeParente = "" And CognomeParente = "" Then
                Dim Seperatore As Integer

                Seperatore = Nome.IndexOf(" ")

                If Seperatore > 0 Then
                    CognomeParente = Mid(Nome, 1, Seperatore).Trim
                    NomeParente = Mid(Nome, Seperatore + 1, Len(Nome) - Seperatore).Trim
                Else
                    CognomeParente = Nome
                End If
            End If
            Sesso = campodb(myPOSTreader.Item("Sesso"))
            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            ComuneDiNascita = campodb(myPOSTreader.Item("ComuneDiNascita"))
            ProvinciaDiNascita = campodb(myPOSTreader.Item("ProvinciaDiNascita"))

            If campodb(myPOSTreader.Item("DataNascita")) <> "" Then
                DataNascita = campodb(myPOSTreader.Item("DataNascita"))
            Else
                DataNascita = Nothing
            End If

            PERIODO = campodb(myPOSTreader.Item("PERIODO"))

            FattAnticipata = campodb(myPOSTreader.Item("FattAnticipata"))
            GradoParentela = campodb(myPOSTreader.Item("GradoParentela"))
            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))
            MesiAnticipo = Val(campodb(myPOSTreader.Item("MesiAnticipo")))

            RECAPITONOME = campodb(myPOSTreader.Item("RECAPITONOME"))
            RECAPITOCOMUNE = campodb(myPOSTreader.Item("RECAPITOCOMUNE"))
            RECAPITOPROVINCIA = campodb(myPOSTreader.Item("RECAPITOPROVINCIA"))
            RECAPITOINDIRIZZO = campodb(myPOSTreader.Item("RECAPITOINDIRIZZO"))
            RESIDENZACAP4 = campodb(myPOSTreader.Item("RESIDENZACAP4"))



            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb(myPOSTreader.Item("NumeroControlloCliente"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))

            IntestatarioCC = campodb(myPOSTreader.Item("IntestatarioCC"))
            CodiceFiscaleCC = campodb(myPOSTreader.Item("CodiceFiscaleCC"))
            NOTE = campodb(myPOSTreader.Item("NOTE"))
            IdEpersonam = Val(campodb(myPOSTreader.Item("IdEpersonam")))

            ParenteIndirizzo = Val(campodb(myPOSTreader.Item("ParenteIndirizzo")))
            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))

            ImportoSconto = campodbN(myPOSTreader.Item("ImportoSconto"))

            IdEpersonam = campodbN(myPOSTreader.Item("IdEpersonam"))

            TipoSconto = campodb(myPOSTreader.Item("TipoSconto"))

            Opposizione730 = campodbN(myPOSTreader.Item("Opposizione730"))

            CONSENSOINSERIMENTO = campodbN(myPOSTreader.Item("CONSENSOINSERIMENTO"))
            CONSENSOMARKETING = campodbN(myPOSTreader.Item("CONSENSOMARKETING"))

            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))

            ExtraFuoriRetta = campodbN(myPOSTreader.Item("ExtraFuoriRetta"))

            Intestatario = campodbN(myPOSTreader.Item("OspiteIntestatario"))
        End If
        cn.Close()
    End Sub

    Sub VerificaPrimaScrivereParente(ByVal StringaConnessione As String)

        If CodiceParente = 0 Then
            Dim cn As OleDbConnection



            cn = New Data.OleDb.OleDbConnection(StringaConnessione)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                               "CodiceFiscale = ? And TIPOLOGIA = 'P'")
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmd.Parameters.AddWithValue("@CodiceFiscale", CODICEFISCALE)
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
            End If
            myPOSTreader.Close()
            cn.Close()
        End If
        ScriviParente(StringaConnessione)
    End Sub


    Sub ScriviParente(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String


        If Len(Nome) > 150 Then
            Nome = Mid(Nome, 1, 150)
        End If
        If Len(RESIDENZAINDIRIZZO1) > 150 Then
            RESIDENZAINDIRIZZO1 = Mid(RESIDENZAINDIRIZZO1, 1, 150)
        End If
        If Len(RECAPITOINDIRIZZO) > 50 Then
            RECAPITOINDIRIZZO = Mid(RECAPITOINDIRIZZO, 1, 50)
        End If

        If Len(RECAPITONOME) > 50 Then
            RECAPITONOME = Mid(RECAPITONOME, 1, 50)
        End If


        If Len(Telefono1) > 130 Then
            Telefono1 = Mid(Telefono1, 1, 130)
        End If

        If Len(Telefono2) > 130 Then
            Telefono2 = Mid(Telefono2, 1, 130)
        End If
        If Len(Telefono3) > 130 Then
            Telefono3 = Mid(Telefono3, 1, 130)
        End If
        If Len(Telefono4) > 130 Then
            Telefono4 = Mid(Telefono4, 1, 130)
        End If



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.Connection = cn

        If CodiceParente = 0 Then
            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select MAX(CodiceParente) from AnagraficaComune where " & _
                               "CodiceOspite = ? ")
            cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmd1.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader.Read Then
                If IsDBNull(myPOSTreader.Item(0)) Then
                    CodiceParente = 1
                Else
                    CodiceParente = myPOSTreader.Item(0) + 1
                End If
            Else
                CodiceParente = 1
            End If
            myPOSTreader.Close()

            MySql = "INSERT INTO AnagraficaComune " & _
                    " (Nome,CodiceOspite,CodiceParente,TIPOLOGIA ) Values " & _
                    " (?,?,?,'P')"
            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = MySql
            cmd2.Parameters.AddWithValue("@Nome", Nome)
            cmd2.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmd2.Parameters.AddWithValue("@CodiceParente", CodiceParente)
            cmd2.Connection = cn
            cmd2.ExecuteNonQuery()
        End If

        If CodiceOspite = 0 Then
            Exit Sub
        End If
        If CodiceParente = 0 Then
            Exit Sub
        End If


        MySql = "UPDATE AnagraficaComune SET " & _
                " Nome  = ?," & _
                 "Sesso = ?, " & _
                 "DataNascita = ?, " & _
                 "ComuneDiNascita = ?, " & _
                 "ProvinciaDiNascita = ?, " & _
                 "CODICEFISCALE = ?, " & _
                 "Telefono1 = ?, " & _
                 "Telefono2 = ?, " & _
                 "RESIDENZATELEFONO1 = ?," & _
                 "RESIDENZATELEFONO2 = ?," & _
                 "GradoParentela = ?, " & _
                 "RESIDENZAINDIRIZZO1 = ?, " & _
                 "RESIDENZACAP1 = ?, " & _
                 "RESIDENZAPROVINCIA1 = ?, " & _
                 "RESIDENZACOMUNE1 = ?, " & _
                 "CODICEIVA = ?, " & _
                 "TIPOOPERAZIONE = ?, " & _
                 "MODALITAPAGAMENTO = ?, " & _
                 "FattAnticipata = ?, " & _
                 "PERIODO = ?, " & _
                 "Compensazione = ?, " & _
                 "MastroCliente = ?, " & _
                 "ContoCliente = ?, " & _
                 "SottoContoCliente = ?, " & _
                 "CONTOPERESATTO= ?, " & _
                 "CognomeOspite= ?, " & _
                 "NomeOspite= ?,  " & _
                " RECAPITONOME= ?, " & _
                " RECAPITOCOMUNE= ?, " & _
                " RECAPITOPROVINCIA = ?, " & _
                " RECAPITOINDIRIZZO= ?, " & _
                " RESIDENZACAP4= ?, " & _
                " CABCLIENTE = ?, " & _
                " ABICLIENTE = ?, " & _
                " IntCliente = ?, " & _
                " NumeroControlloCliente = ?, " & _
                " CINCLIENTE = ?, " & _
                " CCBANCARIOCLIENTE = ?, " & _
                " BancaCliente = ?, " & _
                " IntestatarioCC = ?, " & _
                " CodiceFiscaleCC = ?, " & _
                " IdEpersonam  = ?, " & _
                " ParenteIndirizzo = ?," & _
                " [NOTE] = ?," & _
                " RotturaOspite  = ?, " & _
                " ImportoSconto = ?, " & _
                " Utente = ?," & _
                " DataAggiornamento = ?, " & _
                " TipoSconto = ?, " & _
                " Opposizione730 = ?, " & _
                " CONSENSOINSERIMENTO = ?, " & _
                " CONSENSOMARKETING = ?, " & _
                " OspiteIntestatario =?, " & _
                " CodiceCup = ?, " & _
                " ExtraFuoriRetta = ? " & _
                 " Where CodiceOSpite = ? And CodiceParente = ?"


        'PERIODO
        '" NOTE  = @NOTE, " & _
        cmd.CommandText = MySql

        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@Sesso", Sesso)
        cmd.Parameters.AddWithValue("@DataNascita", IIf(Year(DataNascita) > 1, DataNascita, System.DBNull.Value))
        cmd.Parameters.AddWithValue("@ComuneDiNascita", ComuneDiNascita)
        cmd.Parameters.AddWithValue("@ProvinciaDiNascita", ProvinciaDiNascita)
        cmd.Parameters.AddWithValue("@CODICEFISCALE", CODICEFISCALE)
        cmd.Parameters.AddWithValue("@Telefono1", Telefono1)
        cmd.Parameters.AddWithValue("@Telefono2", Telefono2)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO1", Telefono3)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO2", Telefono4)
        cmd.Parameters.AddWithValue("@GradoParentela", GradoParentela)

        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@RESIDENZACAP1", RESIDENZACAP1)

        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", RESIDENZAPROVINCIA1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", RESIDENZACOMUNE1)

        cmd.Parameters.AddWithValue("@CODICEIVA", CODICEIVA)
        cmd.Parameters.AddWithValue("@TIPOOPERAZIONE", TIPOOPERAZIONE)
        cmd.Parameters.AddWithValue("@MODALITAPAGAMENTO", MODALITAPAGAMENTO)
        cmd.Parameters.AddWithValue("@FattAnticipata", FattAnticipata)
        cmd.Parameters.AddWithValue("@PERIODO", PERIODO)
        cmd.Parameters.AddWithValue("@Compensazione", Compensazione)


        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottoContoCliente", SottoContoCliente)
        cmd.Parameters.AddWithValue("@CONTOPERESATTO", CONTOPERESATTO)
        cmd.Parameters.AddWithValue("@CognomeOspite", CognomeParente)
        cmd.Parameters.AddWithValue("@NomeOspite", NomeParente)


        cmd.Parameters.AddWithValue("@RECAPITONOME", RECAPITONOME)
        cmd.Parameters.AddWithValue("@RECAPITOCOMUNE", RECAPITOCOMUNE)
        cmd.Parameters.AddWithValue("@RECAPITOPROVINCIA", RECAPITOPROVINCIA)
        cmd.Parameters.AddWithValue("@RECAPITOINDIRIZZO", RECAPITOINDIRIZZO)
        cmd.Parameters.AddWithValue("@RESIDENZACAP4", RESIDENZACAP4)
        cmd.Parameters.AddWithValue("@CABCLIENTE", CABCLIENTE)
        cmd.Parameters.AddWithValue("@ABICLIENTE", ABICLIENTE)
        cmd.Parameters.AddWithValue("@IntCliente", IntCliente)
        cmd.Parameters.AddWithValue("@NumeroControlloCliente", NumeroControlloCliente)
        cmd.Parameters.AddWithValue("@CINCLIENTE", CINCLIENTE)
        cmd.Parameters.AddWithValue("@CCBANCARIOCLIENTE", CCBANCARIOCLIENTE)
        cmd.Parameters.AddWithValue("@BancaCliente", BancaCliente)

        cmd.Parameters.AddWithValue("@IntestatarioCC", IntestatarioCC)
        cmd.Parameters.AddWithValue("@CodiceFiscaleCC", CodiceFiscaleCC)
        cmd.Parameters.AddWithValue("@IdEpersonam", IdEpersonam)
        cmd.Parameters.AddWithValue("@ParenteIndirizzo", ParenteIndirizzo)
        cmd.Parameters.AddWithValue("@Note", NOTE)
        cmd.Parameters.AddWithValue("@RotturaOspite", RotturaOspite)
        cmd.Parameters.AddWithValue("@ImportoSconto", ImportoSconto)
        cmd.Parameters.AddWithValue("@Utente", Utente)
        cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd.Parameters.AddWithValue("@TipoSconto", TipoSconto)
        cmd.Parameters.AddWithValue("@Opposizione730", Opposizione730)

        cmd.Parameters.AddWithValue("@CONSENSOINSERIMENTO", CONSENSOINSERIMENTO)
        cmd.Parameters.AddWithValue("@CONSENSOMARKETING", CONSENSOMARKETING)

        cmd.Parameters.AddWithValue("@OspiteIntestatario", Intestatario)
        cmd.Parameters.AddWithValue("@CodiceCup", CodiceCup)
        cmd.Parameters.AddWithValue("@ExtraFuoriRetta", ExtraFuoriRetta)


        'OspiteIntestatario
        'Opposizione730
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceParente", CodiceParente)


        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select CodiceParente,  Nome, ParenteIndirizzo, OspiteIntestatario  from AnagraficaComune where AnagraficaComune.CodiceParente > 0 And AnagraficaComune.CodiceOspite = " & codiceospite & " ORDER BY Nome")


        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceParente", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("Des. Fat.", GetType(String))
        Tabella.Columns.Add("Int. Fat.", GetType(String))

        Dim oldCodiceServizio As String
        Dim OldCodiceOspite As Long
        oldCodiceServizio = ""
        OldCodiceOspite = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
   

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbN(myPOSTreader.Item("CodiceParente"))
            myriga(1) = campodb(myPOSTreader.Item("Nome"))



            myriga(2) = "NO"
            If campodbN(myPOSTreader.Item("ParenteIndirizzo")) = 1 Then
                myriga(2) = "SI"
            End If

            myriga(3) = "NO"
            If campodbN(myPOSTreader.Item("OspiteIntestatario")) = 1 Then
                myriga(3) = "SI"
            End If




            Tabella.Rows.Add(myriga)
            
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByVal codiceospite As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select CodiceParente,  Nome from AnagraficaComune where AnagraficaComune.CodiceParente > 0 And AnagraficaComune.CodiceOspite = " & codiceospite & " ORDER BY Nome")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("NOME"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceParente")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub
End Class

