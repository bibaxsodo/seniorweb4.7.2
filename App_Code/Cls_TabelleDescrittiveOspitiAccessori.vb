﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabelleDescrittiveOspitiAccessori
    Public TipoTabella As String
    Public CodiceTabella As String
    Public Descrizione As String


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLE where TipoTabella = '" & TipoTabella & "' And  CodiceTabella = '" & CodiceTabella & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TipoTabella = campodb(myPOSTreader.Item("TipoTabella"))
            CodiceTabella = campodb(myPOSTreader.Item("CodiceTabella"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
        End If
        cn.Close()
    End Sub

    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLE where TipoTabella = '" & TipoTabella & "' And  CodiceTabella = '" & CodiceTabella & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TABELLE SET Descrizione = ? WHERE  TipoTabella = '" & TipoTabella & "' And  CodiceTabella = '" & CodiceTabella & "'"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TABELLE (TipoTabella,CodiceTabella,Descrizione) VALUES (?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@TipoTabella", TipoTabella)
            cmdw.Parameters.AddWithValue("@CodiceTabella", CodiceTabella)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal TipTab As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabelle where TipoTabella = '" & TipTab & "' ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        REM Tabella.Columns.Add("TipTab", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()            
            myriga(0) = campodb(myPOSTreader.Item("CodiceTabella"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Sub UpDateDropBox(ByVal StringaConnessione As String, ByVal TipTab As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabelle where TipoTabella = '" & TipTab & "' ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceTabella")
        Loop
        myPOSTreader.Close()
        cn.Close()


    End Sub


End Class
