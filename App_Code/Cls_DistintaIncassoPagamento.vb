﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_DistintaIncassoPagamento
    Public Id As Integer
    Public DataDistinta As Date
    Public Descrizione As String
    Public TipoAV As String
    Public Generato As Integer
    Public Riga(1000) As Cls_DistintaIncassoPagamentoRiga
    Public Max As Integer = 0


    Public Sub LeggiID(ByVal StringaConnessione As String, ByVal IdDistinta As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DistintaIncassoPagamento where " & _
                           " ID = ? ")
        cmd.Parameters.AddWithValue("@ID", IdDistinta)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then

            Id = campodbn(myPOSTreader.Item("ID"))
            DataDistinta = campodbD(myPOSTreader.Item("DataDistinta"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            TipoAV = campodb(myPOSTreader.Item("TipoAV"))
            Generato = campodbn(myPOSTreader.Item("Generato"))

            LeggiRighe(StringaConnessione, IdDistinta)
        End If
        myPOSTreader.Close()

        cn.Close()

    End Sub


    Public Sub LeggiRighe(ByVal StringaConnessione As String, ByVal IdDistinta As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        Max = 0

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DistintaIncassoPagamentoRiga where " & _
                           " IdDistinta = ? ")
        cmd.Parameters.AddWithValue("@IdDistinta", IdDistinta)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Riga(Max).IdDistinta = campodbn(myPOSTreader.Item("IdDistinta"))
            Riga(Max).NumeroRegistrazioneDocumento = campodbn(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            Riga(Max).IdScadenza = campodbn(myPOSTreader.Item("IdScadenza"))
            Riga(Max).Importo = campodbn(myPOSTreader.Item("Importo"))
            Max = Max + 1
        Loop
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub Scrivi(ByVal StringaConnessione As String, ByVal IdDistinta As Integer)
        Dim cn As OleDbConnection
        Dim IdMAX As Integer


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)


        cn.Open()
        Dim cmd As New OleDbCommand()

        If IdDistinta = 0 Then
            cmd.CommandText = ("INSERT INTO DistintaIncassoPagamento " & _
                               " (DataDistinta,Descrizione,TipoAV ,Generato) values (?,?,?,?)")
            cmd.Parameters.AddWithValue("@DataDistinta", DataDistinta)
            cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmd.Parameters.AddWithValue("@TipoAV", TipoAV)
            cmd.Parameters.AddWithValue("@Generato", Generato)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()

            cmd.Parameters.Clear()
            cmd.CommandText = ("select max(id) as TotId from DistintaIncassoPagamento")
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                IdDistinta = campodbn(myPOSTreader.Item("TotId"))
                Id = IdDistinta
            End If
            myPOSTreader.Close()
        Else
            cmd.CommandText = ("UPDATE DistintaIncassoPagamento SET " & _
                   " DataDistinta = ?,Descrizione  = ?,TipoAV = ?,Generato = ? Where IdDistinta = ? ")
            cmd.Parameters.AddWithValue("@DataDistinta", DataDistinta)
            cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmd.Parameters.AddWithValue("@TipoAV", TipoAV)
            cmd.Parameters.AddWithValue("@Generato", Generato)
            cmd.Parameters.AddWithValue("@IdDistinta", IdDistinta)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()
        End If


        Dim Transan As OleDbTransaction = cn.BeginTransaction()


        For IdMAX = 0 To  Max -1
            Dim cmdTr As New OleDbCommand()

            cmdTr.CommandText = ("INSERT INTO DistintaIncassoPagamentoRiga ([IdDistinta],[NumeroRegistrazioneDocumento],[IdScadenza],[Importo]) values (?,?,?,?)")
            cmdTr.Parameters.AddWithValue("@IdDistinta", IdDistinta)
            cmdTr.Parameters.AddWithValue("@NumeroRegistrazioneDocumento", Riga(IdMAX).NumeroRegistrazioneDocumento)
            cmdTr.Parameters.AddWithValue("@IdScadenza", Riga(IdMAX).IdScadenza)
            cmdTr.Parameters.AddWithValue("@Importo", Riga(IdMAX).Importo)                        
            cmdTr.Connection = cn
            cmdTr.Transaction = Transan
            cmdTr.ExecuteNonQuery()
        Next

        Transan.Commit()

        cn.Close()

    End Sub



End Class
