﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RitenuteAcconto
    Public ID As Long
    Public NumeroRegistrazioneDocumento As Long
    Public CodiceTributo As String
    Public ImponibileRitenuta As Double
    Public TipoRitenuta As String
    Public ImportoRitenuta As Double
    Public NumeroRegistrazionePagamento As Integer
    Public NumeroRegistrazionePagamentoRitenuta As Integer
    Public ImportoPagamento As Double
    Public ImportoPagamentoRitenuta As Double



    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public Function RitenutePagamento(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long) As Boolean
        Dim cn As OleDbConnection

        RitenutePagamento = False
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazionePagamento = ? "
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", RegistrazionePagamento)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RitenutePagamento = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function RitenutePagateSuPagamento(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long) As Boolean
        Dim cn As OleDbConnection

        RitenutePagateSuPagamento = False
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazionePagamento = ? And ImportoPagamentoRitenuta > 0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", RegistrazionePagamento)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RitenutePagateSuPagamento = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function RitenutePagateSuPagamentoRitenuta(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long) As Boolean
        Dim cn As OleDbConnection

        RitenutePagateSuPagamentoRitenuta = False
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazionePagamentoRitenuta = ? And ImportoPagamentoRitenuta > 0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", RegistrazionePagamento)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RitenutePagateSuPagamentoRitenuta = True
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function


    Public Function RitenuteSuDocumento(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long, ByVal ID As Long) As Double
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        RitenuteSuDocumento = 0

        cmd.CommandText = "Select SUM(ImportoRitenuta) From MovimentiRitenute where NumeroRegistrazioneDocumento = ? And ID <> ?"
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", RegistrazionePagamento)
        cmd.Parameters.AddWithValue("@ID", ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RitenuteSuDocumento = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function


    Public Function RitenuteSuDocumentoImponibile(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long, ByVal ID As Long) As Double
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        RitenuteSuDocumentoImponibile = 0

        cmd.CommandText = "Select SUM(ImponibileRitenuta) From MovimentiRitenute where NumeroRegistrazioneDocumento = ? And ID <> 0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneDocumento", RegistrazionePagamento)        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RitenuteSuDocumentoImponibile = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Sub EliminaPagamento(ByVal StringaConnessione As String, ByVal RegistrazionePagamento As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "update MovimentiRitenute set NumeroRegistrazionePagamento = 0, ImportoPagamento = 0  where NumeroRegistrazionePagamento = ? And ImportoPagamentoRitenuta = 0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", RegistrazionePagamento)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Public Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "delete  From MovimentiRitenute where id = ?"
        cmd.Parameters.AddWithValue("@ID", ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaMovimentoRitenutaDocumento(ByVal StringaConnessione As String, ByVal RegistrazioneDocumento As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "delete  From MovimentiRitenute where NumeroRegistrazioneDocumento = ?"
        cmd.Parameters.AddWithValue("@RegistrazioneDocumento", RegistrazioneDocumento)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From MovimentiRitenute where id = ?"
        cmd.Parameters.AddWithValue("@ID", ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            NumeroRegistrazioneDocumento = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            CodiceTributo = campodb(myPOSTreader.Item("CodiceTributo"))
            ImponibileRitenuta = campodbN(myPOSTreader.Item("ImponibileRitenuta"))
            TipoRitenuta = campodb(myPOSTreader.Item("TipoRitenuta"))
            ImportoRitenuta = campodbN(myPOSTreader.Item("ImportoRitenuta"))
            NumeroRegistrazionePagamento = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            ImportoPagamento = campodbN(myPOSTreader.Item("ImportoPagamento"))
            NumeroRegistrazionePagamentoRitenuta = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamentoRitenuta"))
            ImportoPagamentoRitenuta = campodbN(myPOSTreader.Item("ImportoPagamentoRitenuta"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub




    Public Sub PagaRitenutaDelDocumento(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "update  MovimentiRitenute Set ImportoPagamento = ImportoRitenuta,NumeroRegistrazionePagamento = ? where NumeroRegistrazioneDocumento = ? And ImportoPagamento =0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", NumeroRegistrazionePagamento)
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneDocumento", NumeroRegistrazioneDocumento)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub



    Public Sub LeggiMovimentiRitenutaDocumentoNonPagati(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazioneDocumento = ? And ImportoPagamento =0"
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneDocumento", NumeroRegistrazioneDocumento)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodbN(myPOSTreader.Item("ID"))
            NumeroRegistrazioneDocumento = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            CodiceTributo = campodb(myPOSTreader.Item("CodiceTributo"))
            ImponibileRitenuta = campodbN(myPOSTreader.Item("ImponibileRitenuta"))
            TipoRitenuta = campodb(myPOSTreader.Item("TipoRitenuta"))
            ImportoRitenuta = campodbN(myPOSTreader.Item("ImportoRitenuta"))
            NumeroRegistrazionePagamento = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            ImportoPagamento = campodbN(myPOSTreader.Item("ImportoPagamento"))
            NumeroRegistrazionePagamentoRitenuta = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamentoRitenuta"))
            ImportoPagamentoRitenuta = campodbN(myPOSTreader.Item("ImportoPagamentoRitenuta"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()


        Dim cmdV As New OleDbCommand()

        cmdV.CommandText = "Select * From MovimentiRitenute where id = ?"
        cmdV.Parameters.AddWithValue("@ID", ID)
        cmdV.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmdV.ExecuteReader()
        If myPOSTreader.Read Then
            cmd.CommandText = "UPDATE MovimentiRitenute SET NumeroRegistrazioneDocumento = ?, CodiceTributo = ?, ImponibileRitenuta = ?, TipoRitenuta = ?, ImportoRitenuta = ?, NumeroRegistrazionePagamento = ?, ImportoPagamento  = ?,NumeroRegistrazionePagamentoRitenuta = ?, ImportoPagamentoRitenuta  = ? WHERE ID = " & ID
        Else
            cmd.CommandText = "INSERT INTO MovimentiRitenute (NumeroRegistrazioneDocumento, CodiceTributo, ImponibileRitenuta, TipoRitenuta, ImportoRitenuta, NumeroRegistrazionePagamento, ImportoPagamento,NumeroRegistrazionePagamentoRitenuta, ImportoPagamentoRitenuta) values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        End If
        myPOSTreader.Close()
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneDocumento", NumeroRegistrazioneDocumento)
        cmd.Parameters.AddWithValue("@CodiceTributo", CodiceTributo)
        cmd.Parameters.AddWithValue("@ImponibileRitenuta", ImponibileRitenuta)
        cmd.Parameters.AddWithValue("@TipoRitenuta", TipoRitenuta)
        cmd.Parameters.AddWithValue("@ImportoRitenuta", ImportoRitenuta)
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", NumeroRegistrazionePagamento)
        cmd.Parameters.AddWithValue("@ImportoPagamento", ImportoPagamento)
        cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamentoRitenuta", NumeroRegistrazionePagamentoRitenuta)
        cmd.Parameters.AddWithValue("@ImportoPagamentoRitenuta", ImportoPagamentoRitenuta)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub LeggiMovimentiDaRegistrazione(ByVal StringaConnessione As String, ByVal StringaConnessioneTabelle As String, ByVal RegistrazioneDocumento As Integer, ByVal RegistrazionePagamento As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Tipo Doc.", GetType(String))
        Tabella.Columns.Add("N. Regis.", GetType(String))
        Tabella.Columns.Add("Num.Doc.", GetType(String))
        Tabella.Columns.Add("Data Doc.", GetType(String))
        Tabella.Columns.Add("Cod. Tributo", GetType(String))
        Tabella.Columns.Add("Tributo", GetType(String))
        Tabella.Columns.Add("Imponibile Rit.", GetType(String))
        Tabella.Columns.Add("Tipo Rit.", GetType(String))
        Tabella.Columns.Add("Imp. Ritenuta", GetType(String))
        Tabella.Columns.Add("Pagamento", GetType(String))
        Tabella.Columns.Add("Data Pagamento", GetType(String))
        Tabella.Columns.Add("Imp. Pagamento", GetType(String))
        Tabella.Columns.Add("Reg. Pag. Ritenuta", GetType(String))
        Tabella.Columns.Add("Imp. Pag. Ritenuta", GetType(String))
        Tabella.Columns.Add("Id", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()

        If RegistrazioneDocumento = 0 Then
            cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazionePagamento = ?"
            cmd.Parameters.AddWithValue("@RegistrazionePagamento", RegistrazionePagamento)
        Else
            cmd.CommandText = "Select * From MovimentiRitenute where NumeroRegistrazioneDocumento = ?"
            cmd.Parameters.AddWithValue("@RegistrazioneDocumento", RegistrazioneDocumento)
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            Dim RegistrazioneContabile As New Cls_MovimentoContabile

            RegistrazioneContabile.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            RegistrazioneContabile.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = RegistrazioneContabile.CausaleContabile
            CausaleContabile.Leggi(StringaConnessioneTabelle, CausaleContabile.Codice)


            myriga(0) = CausaleContabile.TipoDocumento
            myriga(1) = RegistrazioneContabile.NumeroRegistrazione

            myriga(2) = RegistrazioneContabile.NumeroDocumento

            myriga(3) = Format(RegistrazioneContabile.DataDocumento, "dd/MM/yyyy")

            myriga(4) = campodb(myPOSTreader.Item("CodiceTributo"))

            Dim Tributo As New Cls_TabellaTributo

            Tributo.Codice = campodb(myPOSTreader.Item("CodiceTributo"))
            Tributo.Leggi(StringaConnessioneTabelle, Tributo.Codice)

            myriga(5) = Tributo.Descrizione

            myriga(6) = Format(campodbN(myPOSTreader.Item("ImponibileRitenuta")), "#,##0.00")

            Dim TipoRitenuta As New ClsRitenuta

            TipoRitenuta.Codice = campodb(myPOSTreader.Item("TipoRitenuta"))
            TipoRitenuta.Leggi(StringaConnessioneTabelle)

            myriga(7) = TipoRitenuta.Descrizione

            myriga(8) = Format(campodbN(myPOSTreader.Item("ImportoRitenuta")), "#,##0.00")


            Dim RegistrazioneContabilePagamento As New Cls_MovimentoContabile

            RegistrazioneContabilePagamento.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            RegistrazioneContabilePagamento.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            myriga(9) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))


            If campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento")) = 0 Then
                myriga(10) = ""
            Else

                myriga(10) = Format(RegistrazioneContabilePagamento.DataRegistrazione, "dd/MM/yyyy")
            End If
            myriga(11) = Format(campodbN(myPOSTreader.Item("ImportoPagamento")), "#,##0.00")
            myriga(12) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamentoRitenuta"))
            myriga(13) = Format(campodbN(myPOSTreader.Item("ImportoPagamentoRitenuta")), "#,##0.00")

            myriga(14) = campodbN(myPOSTreader.Item("ID"))
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub



End Class
