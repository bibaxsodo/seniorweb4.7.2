﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Solleciti
    Public Id As Long
    Public Descrizione As String
    Public DataScadenza As Date
    Public Testo As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_SOLLECITI")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_SOLLECITI where ID = " & Id)
        cmd.Connection = cn
        Dim cmdIns As New OleDbCommand()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            cmdIns.CommandText = ("Insert Into Tabella_SOLLECITI (Descrizione,DataScadenza,Testo)  VALUES (?,?,?) ")
        Else
            cmdIns.CommandText = ("UPDATE Tabella_SOLLECITI SET Descrizione =?,DataScadenza= ?,Testo=?  Where ID = " & Id)
        End If


        cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdIns.Parameters.AddWithValue("@DataScadenza", IIf(Year(DataScadenza) > 1, DataScadenza, System.DBNull.Value))
        cmdIns.Parameters.AddWithValue("@Testo", Testo)

        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabella_SOLLECITI where ID = " & Id)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

            DataScadenza = campodbD(myPOSTreader.Item("DataScadenza"))

            Testo = campodb(myPOSTreader.Item("Testo"))
        End If
        myPOSTreader.Close()
        cn.Close()


    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Tabella_SOLLECITI where ID = " & Id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()


    End Sub
End Class
