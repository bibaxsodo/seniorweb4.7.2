Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Parametri
    Public Errori As String
    Public PercEsec As Double
    Public CodiceOspite As Long
    Public AnnoFatturazione As Long
    Public MeseFatturazione As Long
    Public CodiceIva As String
    Public TipoOperazione As String
    Public ModalitaPagamento As String
    Public Anticipato As String
    Public ImportoMensile As Long
    Public NCDeposito As String

    Public GIORNOUSCITA As String
    Public GIORNOENTRATA As String
    Public CAUSALEACCOGLIMENTO As String
    Public MastroAnticipo As Double
    Public AddebitiAccreditiComulati As Long
    Public EmissioneComuni As Long
    Public EmissioneRegione As Long
    Public EmissioneJolly As Long
    Public EmissioneOspite As Long
    Public EmissioneParente As Long
    Public Calcolato As String

    Public Mastro As Double
    Public ContoComune As Double
    Public ContoRegione As Double
    Public Deposito As String
    Public ControllaInserimentoDeposito As Long
    Public AzzeraSeNegativo As Long
    Public ConguaglioMinimo As Double
    Public DataCheckEpersonam As Date
    Public CausaleCambioServizioSenzaCognuaglio As String
    Public OrdinaPerDestinatario As Integer
    Public DescrizioneDefaultDenaro As String
    Public NonAllineareMensileAdifferenza As Integer
    Public RaggruppaRicavi As Integer
    Public NonInAtteasa As Integer
    Public NonUsciteTemporanee As Integer
    Public ModalitaPagamentoObligatoria As Integer
    Public AlberghieroAssistenziale As Integer    
    Public IdMandatoDaCF As Integer
    Public MandatoSIA As String
    Public SocialeSanitario As Integer
    Public TipoAddebitoFatturePrivati As Integer
    Public TipoExport As String
    Public DocumentiIncassiBloccato As Integer
    Public DocumentiIncassiMesiPrecedenti As Integer
    Public DocumentiIncassiNonPeriodo As Integer
    Public DocumentiIncassiUltimaUscitaDefinitiva As Integer
    Public HideDefaultDiurnoOspite As Integer

    Public ApiV1 As Integer
    Public SoloModAnagraficaDaEpersonam As Integer
    Public DebugV1 As Integer
    Public SeparaPeriodoOspiteParente As Integer
    Public CheckIdMandato As Integer
    Public V1MovimentiEpersonam As Integer
    Public ProgressivoIDMandato As Integer
    Public OraSuMovimenti As Integer

    Public FE_RaggruppaRicavi As Integer
    Public FE_IndicaMastroPartita As Integer
    Public FE_Competenza As Integer
    Public FE_CentroServizio As Integer
    Public FE_GiorniInDescrizione As Integer
    Public FE_TipoRetta As Integer
    Public FE_SoloIniziali As Integer
    Public FE_NoteFatture As Integer
    Public AllineaAnag As Integer
    Public TabPresenze As Integer
    Public AllineaMovimenti As Integer
    Public DataSosia As Integer
    Public ImportListino As Integer
    Public DescrizioneAutomaticaIncassi As Integer
    Public IndirizzoObbligatorioInt As Integer
    Public DataOraElaborazione As Date
    Public SplitSuEnti As Integer
    Public FattureNCNuovo As Integer

    Public DenaroDaPeriodo As Integer
    Public OrdinamentoCR As Integer
    Public MovimentiStanze As Integer
    Public CartellaEpersonam As Integer
    Public ModificaPeriodoFatturaNC As Integer
    Public AbilitaQrCodeAddAcc As Integer
    Public AddebitiRettaInRendiconto As String

    Public CausaleContabileSeparaIVA As String

    Public ParenteIntestatarioUnico As Integer
    Public XMLSEZIONALEPRIMAPROTOCOLLO As Integer
    Public SoloDescrizioneRigaFattura As Integer
    Public PeriodoFatturazioneSuServizio As Integer
    Public StampaSoloCSAttivi As Integer

    'SplitSuEnti

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Function InElaborazione(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaParametri ")
        cmd.Connection = cn

        InElaborazione = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If DateDiff(DateInterval.Second, campodbD(myPOSTreader.Item("DataOraElaborazione")), Now) < 30 Then
                InElaborazione = True
            Else
                InElaborazione = False
            End If
        End If
        cn.Close()
    End Function

    Public Function ChiudiPeriodoDaServizi(ByVal StringaConnessione As String, ByVal Mese As Integer, ByVal Anno As Integer) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLACENTROSERVIZIO ")
        cmd.Connection = cn
        Dim Chiudi As Boolean = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If (campodbN(myPOSTreader.Item("AnnoFatturazione")) = Anno And campodbN(myPOSTreader.Item("MeseFatturazione")) >= MeseFatturazione) Or campodbN(myPOSTreader.Item("AnnoFatturazione")) > Anno Then
            Else
                Chiudi = False
            End If
        Loop
        myPOSTreader.Close()

        cn.Close()
        If Chiudi = True Then
            LeggiParametri(StringaConnessione)

            MeseFatturazione = Mese
            AnnoFatturazione = AnnoFatturazione

            ScriviParametri(StringaConnessione)
        End If
    End Function


    Public Sub LeggiParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection           

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaParametri ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Errori = campodb(myPOSTreader.Item("Errori"))
            PercEsec = campodbN(myPOSTreader.Item("PercEsec"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            AnnoFatturazione = myPOSTreader.Item("AnnoFatturazione")
            MeseFatturazione = myPOSTreader.Item("MeseFatturazione")

            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            TipoOperazione = campodb(myPOSTreader.Item("TipoOperazione"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            Anticipato = campodb(myPOSTreader.Item("Anticipato"))

            GIORNOUSCITA = campodb(myPOSTreader.Item("GIORNOUSCITA"))
            GIORNOENTRATA = campodb(myPOSTreader.Item("GIORNOENTRATA"))
            CAUSALEACCOGLIMENTO = campodb(myPOSTreader.Item("CAUSALEACCOGLIMENTO"))
            MastroAnticipo = campodbN(myPOSTreader.Item("MastroAnticipo"))

            Mastro = campodbN(myPOSTreader.Item("Mastro"))
            ContoComune = campodbN(myPOSTreader.Item("ContoComune"))
            ContoRegione = campodbN(myPOSTreader.Item("ContoRegione"))

            AddebitiAccreditiComulati = campodbN(myPOSTreader.Item("AddebitiAccreditiComulati"))
            NCDeposito = campodb(myPOSTreader.Item("NCDeposito"))
            ImportoMensile = campodb(myPOSTreader.Item("ImportoMensile"))
            EmissioneComuni = campodbN(myPOSTreader.Item("EmissioneComuni"))
            EmissioneRegione = campodbN(myPOSTreader.Item("EmissioneRegione"))
            EmissioneJolly = campodbN(myPOSTreader.Item("EmissioneJolly"))
            EmissioneOspite = campodbN(myPOSTreader.Item("EmissioneOspite"))
            EmissioneParente = campodbN(myPOSTreader.Item("EmissioneParente"))

            Calcolato = campodb(myPOSTreader.Item("Calcolato"))
            Deposito = campodb(myPOSTreader.Item("Deposito"))
            ControllaInserimentoDeposito = campodbN(myPOSTreader.Item("ControllaInserimentoDeposito"))

            AzzeraSeNegativo = campodbN(myPOSTreader.Item("AzzeraSeNegativo"))

            ConguaglioMinimo = campodbN(myPOSTreader.Item("ConguaglioMinimo"))

            DataCheckEpersonam = campodbD(myPOSTreader.Item("DataCheckEpersonam"))

            CausaleCambioServizioSenzaCognuaglio = campodb(myPOSTreader.Item("CausaleCambioServizioSenzaCognuaglio"))

            OrdinaPerDestinatario = campodbN(myPOSTreader.Item("OrdinaPerDestinatario"))

            DescrizioneDefaultDenaro = campodb(myPOSTreader.Item("DescrizioneDefaultDenaro"))

            NonAllineareMensileAdifferenza = campodbN(myPOSTreader.Item("NonAllineareMensileAdifferenza"))

            RaggruppaRicavi = campodbN(myPOSTreader.Item("RaggruppaRicavi"))
            NonInAtteasa = campodbN(myPOSTreader.Item("NonInAtteasa"))

            NonUsciteTemporanee = campodbN(myPOSTreader.Item("NonUsciteTemporanee"))

            ModalitaPagamentoObligatoria = campodbN(myPOSTreader.Item("ModalitaPagamentoObligatoria"))

            AlberghieroAssistenziale = campodbN(myPOSTreader.Item("AlberghieroAssistenziale"))

            IdMandatoDaCF = campodbN(myPOSTreader.Item("IdMandatoDaCF"))
            MandatoSIA = campodb(myPOSTreader.Item("MandatoSIA"))

            SocialeSanitario = campodbN(myPOSTreader.Item("SocialeSanitario"))

            TipoAddebitoFatturePrivati = campodbN(myPOSTreader.Item("TipoAddebitoFatturePrivati"))


            TipoExport = campodb(myPOSTreader.Item("TipoExport"))

            DocumentiIncassiBloccato = campodbN(myPOSTreader.Item("DocumentiIncassiBloccato"))

            DocumentiIncassiMesiPrecedenti = campodbN(myPOSTreader.Item("DocumentiIncassiMesiPrecedenti"))

            DocumentiIncassiNonPeriodo = campodbN(myPOSTreader.Item("DocumentiIncassiNonPeriodo"))

            DocumentiIncassiUltimaUscitaDefinitiva = campodbN(myPOSTreader.Item("DocumentiIncassiUltimaUscitaDefinitiva"))

            HideDefaultDiurnoOspite = campodbN(myPOSTreader.Item("HideDefaultDiurnoOspite"))

            SoloModAnagraficaDaEpersonam = campodbN(myPOSTreader.Item("SoloModAnagraficaDaEpersonam"))
            ApiV1 = campodbN(myPOSTreader.Item("ApiV1"))

            DebugV1 = campodbN(myPOSTreader.Item("DebugV1"))

            SeparaPeriodoOspiteParente = campodbN(myPOSTreader.Item("SeparaPeriodoOspiteParente"))

            CheckIdMandato = campodbN(myPOSTreader.Item("CheckIdMandato"))

            V1MovimentiEpersonam = campodbN(myPOSTreader.Item("V1MovimentiEpersonam"))

            ProgressivoIDMandato = campodbN(myPOSTreader.Item("ProgressivoIDMandato"))

            OraSuMovimenti = campodbN(myPOSTreader.Item("OraSuMovimenti"))


            FE_RaggruppaRicavi = campodbN(myPOSTreader.Item("FE_RaggruppaRicavi"))
            FE_IndicaMastroPartita = campodbN(myPOSTreader.Item("FE_IndicaMastroPartita"))
            FE_Competenza = campodbN(myPOSTreader.Item("FE_Competenza"))
            FE_CentroServizio = campodbN(myPOSTreader.Item("FE_CentroServizio"))

            FE_GiorniInDescrizione = campodbN(myPOSTreader.Item("FE_GiorniInDescrizione"))

            FE_TipoRetta = campodbN(myPOSTreader.Item("FE_TipoRetta"))

            FE_SoloIniziali = campodbN(myPOSTreader.Item("FE_SoloIniziali"))

            FE_NoteFatture = campodbN(myPOSTreader.Item("FE_NoteFatture"))

            AllineaAnag = campodbN(myPOSTreader.Item("AllineaAnag"))
            TabPresenze = campodbN(myPOSTreader.Item("TabPresenze"))

            AllineaMovimenti = campodbN(myPOSTreader.Item("AllineaMovimenti"))

            DataSosia = campodbN(myPOSTreader.Item("DataSosia"))

            ImportListino = campodbN(myPOSTreader.Item("ImportListino"))

            DescrizioneAutomaticaIncassi = campodbN(myPOSTreader.Item("DescrizioneAutomaticaIncassi"))

            IndirizzoObbligatorioInt = campodbN(myPOSTreader.Item("IndirizzoObbligatorioInt"))

            DataOraElaborazione = campodbD(myPOSTreader.Item("DataOraElaborazione"))

            SplitSuEnti = campodbN(myPOSTreader.Item("SplitSuEnti"))

            FattureNCNuovo = campodbN(myPOSTreader.Item("FattureNCNuovo"))

            DenaroDaPeriodo = campodbN(myPOSTreader.Item("DenaroDaPeriodo"))

            OrdinamentoCR = campodbN(myPOSTreader.Item("OrdinamentoCR"))

            MovimentiStanze = campodbN(myPOSTreader.Item("MovimentiStanze"))


            CartellaEpersonam = campodbN(myPOSTreader.Item("CartellaEpersonam"))

            ModificaPeriodoFatturaNC = campodbN(myPOSTreader.Item("ModificaPeriodoFatturaNC"))


            AbilitaQrCodeAddAcc = campodbN(myPOSTreader.Item("AbilitaQrCodeAddAcc"))


            AddebitiRettaInRendiconto = campodb(myPOSTreader.Item("AddebitiRettaInRendiconto"))

            CausaleContabileSeparaIVA = campodb(myPOSTreader.Item("CausaleContabileSeparaIVA"))


            ParenteIntestatarioUnico = campodbN(myPOSTreader.Item("ParenteIntestatarioUnico"))

            XMLSEZIONALEPRIMAPROTOCOLLO = campodbN(myPOSTreader.Item("XMLSEZIONALEPRIMAPROTOCOLLO"))


            SoloDescrizioneRigaFattura = campodbN(myPOSTreader.Item("SoloDescrizioneRigaFattura"))

            PeriodoFatturazioneSuServizio = campodbN(myPOSTreader.Item("PeriodoFatturazioneSuServizio"))

            StampaSoloCSAttivi = campodbN(myPOSTreader.Item("StampaSoloCSAttivi"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub




    Public Sub Elaborazione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE TabellaParametri SET DataOraElaborazione=?  "
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@DataOraElaborazione", Now)
        cmdw.ExecuteNonQuery()
        cn.Close()

    End Sub

    Public Sub FineElaborazione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE TabellaParametri SET DataOraElaborazione= ?  "
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@DataOraElaborazione", System.DBNull.Value)
        cmdw.ExecuteNonQuery()
        cn.Close()

    End Sub


    Public Sub ScriviParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE TabellaParametri SET Errori=?,PercEsec=?,CodiceOspite=?,AnnoFatturazione=?,MeseFatturazione=?, " & _
            " CodiceIva = ?, TipoOperazione = ?,  ModalitaPagamento = ?,Anticipato = ?, ImportoMensile = ?, NCDeposito = ?, GIORNOUSCITA = ?," & _
            " GIORNOENTRATA = ?, CAUSALEACCOGLIMENTO = ?,MastroAnticipo = ?,AddebitiAccreditiComulati = ?,EmissioneComuni = ?," & _
             " EmissioneRegione = ?, EmissioneJolly = ?,EmissioneOspite = ?, EmissioneParente = ?, Calcolato = ?, Mastro = ?, " & _
            " ContoComune = ?, ContoRegione = ?, Deposito = ?,ControllaInserimentoDeposito=?,AzzeraSeNegativo = ?,ConguaglioMinimo = ?," & _
            " DataCheckEpersonam =?,CausaleCambioServizioSenzaCognuaglio = ?,OrdinaPerDestinatario  =?,DescrizioneDefaultDenaro = ?," & _
            " NonAllineareMensileAdifferenza = ?,RaggruppaRicavi = ?,NonInAtteasa = ?,NonUsciteTemporanee=?,ModalitaPagamentoObligatoria = ?," & _
            " AlberghieroAssistenziale = ?,IdMandatoDaCF = ?, MandatoSIA = ?, SocialeSanitario = ?, TipoAddebitoFatturePrivati   = ?,TipoExport = ?," & _
            " DocumentiIncassiBloccato = ?,DocumentiIncassiMesiPrecedenti = ?,DocumentiIncassiNonPeriodo =?, DocumentiIncassiUltimaUscitaDefinitiva =?, " & _
            " HideDefaultDiurnoOspite = ?,SoloModAnagraficaDaEpersonam =?,ApiV1= ?,DebugV1 = ?, SeparaPeriodoOspiteParente = ?, CheckIdMandato = ?,V1MovimentiEpersonam = ?,ProgressivoIDMandato = ?,OraSuMovimenti = ?, " & _
            " FE_RaggruppaRicavi = ? ,FE_IndicaMastroPartita = ? , FE_Competenza = ? ,FE_CentroServizio = ? , FE_GiorniInDescrizione = ? ,FE_TipoRetta = ? , FE_SoloIniziali = ?,FE_NoteFatture =?, " & _
            " AllineaAnag = ?, TabPresenze = ?,AllineaMovimenti=?,DataSosia=?,ImportListino =?,DescrizioneAutomaticaIncassi =?,IndirizzoObbligatorioInt= ?,DataOraElaborazione = ?,SplitSuEnti = ?,FattureNCNuovo= ?,DenaroDaPeriodo = ?,OrdinamentoCR  = ?,MovimentiStanze = ?,CartellaEpersonam = ?,ModificaPeriodoFatturaNC = ?,AbilitaQrCodeAddAcc =?,AddebitiRettaInRendiconto =?, CausaleContabileSeparaIVA = ?,ParenteIntestatarioUnico =?,XMLSEZIONALEPRIMAPROTOCOLLO = ?,SoloDescrizioneRigaFattura = ?,PeriodoFatturazioneSuServizio = ?,StampaSoloCSAttivi = ? "



        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@Errore", Errori)
        cmdw.Parameters.AddWithValue("@PercEsec", PercEsec)
        cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmdw.Parameters.AddWithValue("@AnnoFatturazione", AnnoFatturazione)
        cmdw.Parameters.AddWithValue("@MeseFatturazione", MeseFatturazione)


        cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
        cmdw.Parameters.AddWithValue("@TipoOperazione", TipoOperazione)
        cmdw.Parameters.AddWithValue("@ModalitaPagamento", ModalitaPagamento)
        cmdw.Parameters.AddWithValue("@Anticipato", Anticipato)


        cmdw.Parameters.AddWithValue("@ImportoMensile", ImportoMensile)
        cmdw.Parameters.AddWithValue("@NCDeposito", NCDeposito)
        cmdw.Parameters.AddWithValue("@GIORNOUSCITA", GIORNOUSCITA)
        cmdw.Parameters.AddWithValue("@GIORNOENTRATA", GIORNOENTRATA)
        cmdw.Parameters.AddWithValue("@CAUSALEACCOGLIMENTO", CAUSALEACCOGLIMENTO)
        cmdw.Parameters.AddWithValue("@MastroAnticipo", MastroAnticipo)


        cmdw.Parameters.AddWithValue("@AddebitiAccreditiComulati", AddebitiAccreditiComulati)

        cmdw.Parameters.AddWithValue("@EmissioneComuni", EmissioneComuni)
        cmdw.Parameters.AddWithValue("@EmissioneRegione", EmissioneRegione)
        cmdw.Parameters.AddWithValue("@EmissioneJolly", EmissioneJolly)
        cmdw.Parameters.AddWithValue("@EmissioneOspite", EmissioneOspite)
        cmdw.Parameters.AddWithValue("@EmissioneParente", EmissioneParente)

        cmdw.Parameters.AddWithValue("@Calcolato", Calcolato)

        cmdw.Parameters.AddWithValue("@Mastro", Mastro)
        cmdw.Parameters.AddWithValue("@ContoComune", ContoComune)
        cmdw.Parameters.AddWithValue("@ContoRegione", ContoRegione)

        cmdw.Parameters.AddWithValue("@Deposito", Deposito)
        cmdw.Parameters.AddWithValue("@ControllaInserimentoDeposito", ControllaInserimentoDeposito)
        cmdw.Parameters.AddWithValue("@AzzeraSeNegativo", AzzeraSeNegativo)
        cmdw.Parameters.AddWithValue("@ConguaglioMinimo", ConguaglioMinimo)



        If Year(DataCheckEpersonam) > 1 Then
            cmdw.Parameters.AddWithValue("@DataCheckEperonam", DataCheckEpersonam)
        Else
            cmdw.Parameters.AddWithValue("@DataCheckEpersonam", System.DBNull.Value)
        End If

        cmdw.Parameters.AddWithValue("@CausaleCambioServizioSenzaCognuaglio", CausaleCambioServizioSenzaCognuaglio)        
        cmdw.Parameters.AddWithValue("@OrdinaPerDestinatario", OrdinaPerDestinatario)
        cmdw.Parameters.AddWithValue("@DescrizioneDefaultDenaro", DescrizioneDefaultDenaro)        
        cmdw.Parameters.AddWithValue("@NonAllineareMensileAdifferenza", NonAllineareMensileAdifferenza)
        cmdw.Parameters.AddWithValue("@RaggruppaRicavi", RaggruppaRicavi)
        cmdw.Parameters.AddWithValue("@NonInAtteasa", NonInAtteasa)
        cmdw.Parameters.AddWithValue("@NonUsciteTemporanee", NonUsciteTemporanee)

        cmdw.Parameters.AddWithValue("@ModalitaPagamentoObligatoria", ModalitaPagamentoObligatoria)
        cmdw.Parameters.AddWithValue("@AlberghieroAssistenziale", AlberghieroAssistenziale)


        cmdw.Parameters.AddWithValue("@IdMandatoDaCF", IdMandatoDaCF)
        cmdw.Parameters.AddWithValue("@MandatoSIA", MandatoSIA)

        cmdw.Parameters.AddWithValue("@SocialeSanitario", SocialeSanitario)
        cmdw.Parameters.AddWithValue("@TipoAddebitoFatturePrivati", TipoAddebitoFatturePrivati)
        'TipoExport
        cmdw.Parameters.AddWithValue("@TipoExport", TipoExport)

        cmdw.Parameters.AddWithValue("@DocumentiIncassiBloccato", DocumentiIncassiBloccato)
        cmdw.Parameters.AddWithValue("@DocumentiIncassiMesiPrecedenti", DocumentiIncassiMesiPrecedenti)

        cmdw.Parameters.AddWithValue("@DocumentiIncassiNonPeriodo", DocumentiIncassiNonPeriodo)

        cmdw.Parameters.AddWithValue("@DocumentiIncassiUltimaUscitaDefinitiva", DocumentiIncassiUltimaUscitaDefinitiva)
        cmdw.Parameters.AddWithValue("@HideDefaultDiurnoOspite", HideDefaultDiurnoOspite)


        cmdw.Parameters.AddWithValue("@SoloModAnagraficaDaEpersonam", SoloModAnagraficaDaEpersonam)
        cmdw.Parameters.AddWithValue("@ApiV1", ApiV1)

        cmdw.Parameters.AddWithValue("@DebugV1", DebugV1)

        cmdw.Parameters.AddWithValue("@SeparaPeriodoOspiteParente", SeparaPeriodoOspiteParente)


        cmdw.Parameters.AddWithValue("@CheckIdMandato", CheckIdMandato)

        cmdw.Parameters.AddWithValue("@V1MovimentiEpersonam", V1MovimentiEpersonam)

        cmdw.Parameters.AddWithValue("@ProgressivoIDMandato", ProgressivoIDMandato)

        cmdw.Parameters.AddWithValue("@OraSuMovimenti", OraSuMovimenti)

        cmdw.Parameters.AddWithValue("@FE_RaggruppaRicavi", FE_RaggruppaRicavi)
        cmdw.Parameters.AddWithValue("@FE_IndicaMastroPartita", FE_IndicaMastroPartita)
        cmdw.Parameters.AddWithValue("@FE_Competenza", FE_Competenza)
        cmdw.Parameters.AddWithValue("@FE_CentroServizio", FE_CentroServizio)
        cmdw.Parameters.AddWithValue("@FE_GiorniInDescrizione", FE_GiorniInDescrizione)
        cmdw.Parameters.AddWithValue("@FE_TipoRetta", FE_TipoRetta)
        cmdw.Parameters.AddWithValue("@FE_SoloIniziali", FE_SoloIniziali)

        cmdw.Parameters.AddWithValue("@FE_NoteFatture", FE_NoteFatture)

        cmdw.Parameters.AddWithValue("@AllineaAnag", AllineaAnag)

        cmdw.Parameters.AddWithValue("@TabPresenze", TabPresenze)

        cmdw.Parameters.AddWithValue("@AllineaMovimenti", AllineaMovimenti)

        cmdw.Parameters.AddWithValue("@DataSosia", DataSosia)

        cmdw.Parameters.AddWithValue("@ImportListino", ImportListino)


        cmdw.Parameters.AddWithValue("@DescrizioneAutomaticaIncassi", DescrizioneAutomaticaIncassi)

        cmdw.Parameters.AddWithValue("@IndirizzoObbligatorioInt", IndirizzoObbligatorioInt)



        If Year(DataOraElaborazione) > 1 Then
            cmdw.Parameters.AddWithValue("@DataOraElaborazione", DataOraElaborazione)
        Else
            cmdw.Parameters.AddWithValue("@DataOraElaborazione", System.DBNull.Value)
        End If

        cmdw.Parameters.AddWithValue("@SplitSuEnti", SplitSuEnti)

        cmdw.Parameters.AddWithValue("@FattureNCNuovo", FattureNCNuovo)

        cmdw.Parameters.AddWithValue("@DenaroDaPeriodo", DenaroDaPeriodo)


        cmdw.Parameters.AddWithValue("@OrdinamentoCR", OrdinamentoCR)

        cmdw.Parameters.AddWithValue("@MovimentiStanze", MovimentiStanze)
        'OrdinamentoCR

        cmdw.Parameters.AddWithValue("@CartellaEpersonam", CartellaEpersonam)

        cmdw.Parameters.AddWithValue("@ModificaPeriodoFatturaNC", ModificaPeriodoFatturaNC)

        cmdw.Parameters.AddWithValue("@AbilitaQrCodeAddAcc", AbilitaQrCodeAddAcc)


        cmdw.Parameters.AddWithValue("@AddebitiRettaInRendiconto", AddebitiRettaInRendiconto)


        cmdw.Parameters.AddWithValue("@CausaleContabileSeparaIVA", CausaleContabileSeparaIVA)

        cmdw.Parameters.AddWithValue("@ParenteIntestatarioUnico", ParenteIntestatarioUnico)

        cmdw.Parameters.AddWithValue("@XMLSEZIONALEPRIMAPROTOCOLLO", XMLSEZIONALEPRIMAPROTOCOLLO)

        cmdw.Parameters.AddWithValue("@SoloDescrizioneRigaFattura", SoloDescrizioneRigaFattura)

        cmdw.Parameters.AddWithValue("@PeriodoFatturazioneSuServizio", PeriodoFatturazioneSuServizio)

        cmdw.Parameters.AddWithValue("@StampaSoloCSAttivi", StampaSoloCSAttivi)

        'StampaSoloCSAttivi


        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()
        cn.Close()
    End Sub
End Class
