﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Public Class Cls_MovimentiCespite
    Public CodiceCespite As Integer
    Public Importo As Double
    Public TipoMov As String
    Public DataRegistrazione As Date
    Public ID As Integer


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiCespite Where  id = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceCespite = campodbn(myPOSTreader.Item("CodiceCespite"))
            Importo = campodbn(myPOSTreader.Item("Importo"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            DataRegistrazione = campodbd(myPOSTreader.Item("DataRegistrazione"))
          
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiCespite Where  id = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = "Insert Into MovimentiCespite (CodiceCespite) Values (?)"
            cmdIns.Connection = cn
            cmdIns.Parameters.AddWithValue("@CodiceCespite", CodiceCespite)
            cmdIns.ExecuteNonQuery()

            Dim cmdID As New OleDbCommand()
            cmdID.CommandText = ("select max(id) from MovimentiCespite ")
            cmdID.Connection = cn
            Dim MyID As OleDbDataReader = cmdID.ExecuteReader()
            If MyID.Read Then
                ID = campodbn(MyID.Item(0))
            End If
            MyID.Close()
        End If
        myPOSTreader.Close()


        MySql = "UPDATE MovimentiCespite  SET "
        MySql = MySql & " CodiceCespite = ?,"
        MySql = MySql & " Importo  = ?,"
        MySql = MySql & " TipoMov  = ?,"
        MySql = MySql & " DataRegistrazione  = ? "
        MySql = MySql & "  Where ID = ? "

        Dim cmdMod As New OleDbCommand()
        cmdMod.CommandText = MySql
        cmdMod.Connection = cn
        cmdMod.Parameters.AddWithValue("@CodiceCespite", CodiceCespite)
        cmdMod.Parameters.AddWithValue("@Importo", Importo)
        cmdMod.Parameters.AddWithValue("@TipoMov", TipoMov)
        cmdMod.Parameters.AddWithValue("@DataRegistrazione", DataRegistrazione)
        cmdMod.Parameters.AddWithValue("@ID", ID)
        cmdMod.ExecuteNonQuery()
    End Sub



End Class
