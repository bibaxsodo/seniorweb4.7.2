﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Stanze
    Public Id As Long
    Public Villa As String
    Public Reparto As String
    Public Piano As String
    Public Stanza As String
    Public Letto As String    
    Public Tipologia As String
    Public NumeroTelefono As String
    Public NumeroBiancheria As String
    Public TipoArredo As String
    Public Utente As String



    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Stanze where  Id = " & Id)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Stanze SET Villa = ?,Reparto = ?,Piano = ?,Stanza = ?,Letto = ?,Tipologia = ?,NumeroTelefono = ?,NumeroBiancheria = ?,TipoArredo = ?,Utente = ?,DataAggiornamento = ? " & _
                    " WHERE  ID = " & Id
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@Villa", Villa)
            cmdw.Parameters.AddWithValue("@Reparto", Reparto)
            cmdw.Parameters.AddWithValue("@Piano", Piano)
            cmdw.Parameters.AddWithValue("@Stanza", Stanza)
            cmdw.Parameters.AddWithValue("@Letto", Letto)            
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@NumeroTelefono", NumeroTelefono)
            cmdw.Parameters.AddWithValue("@NumeroBiancheria", NumeroBiancheria)
            cmdw.Parameters.AddWithValue("@TipoArredo", TipoArredo)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else

            MySql = "INSERT INTO Stanze (Villa ,Reparto ,Piano ,Stanza ,Letto ,Tipologia,NumeroTelefono ,NumeroBiancheria ,TipoArredo ,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Villa", Villa)
            cmdw.Parameters.AddWithValue("@Reparto", Reparto)
            cmdw.Parameters.AddWithValue("@Piano", Piano)
            cmdw.Parameters.AddWithValue("@Stanza", Stanza)
            cmdw.Parameters.AddWithValue("@Letto", Letto)
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@NumeroTelefono", NumeroTelefono)
            cmdw.Parameters.AddWithValue("@NumeroBiancheria", NumeroBiancheria)
            cmdw.Parameters.AddWithValue("@TipoArredo", TipoArredo)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Stanze where Id =  " & Id)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))

            NumeroTelefono = campodb(myPOSTreader.Item("NumeroTelefono"))
            NumeroBiancheria = campodb(myPOSTreader.Item("NumeroBiancheria"))
            TipoArredo = campodb(myPOSTreader.Item("TipoArredo"))

            Id = campodb(myPOSTreader.Item("Id"))
        End If
        myPOSTreader.Close()
    End Sub


    Public Sub LeggiLetto(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Piano = "" And Stanza = "" Then
            cmd.CommandText = ("select * from Stanze where Villa = '" & Villa & "' And Reparto = '" & Reparto & "' And Letto = " & Letto)
        Else
            cmd.CommandText = ("select * from Stanze where Villa = '" & Villa & "' And Reparto = '" & Reparto & "' And Piano = '" & Piano & "' And Stanza = '" & Stanza & "' And Letto = " & Letto)
        End If
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))

            NumeroTelefono = campodb(myPOSTreader.Item("NumeroTelefono"))
            NumeroBiancheria = campodb(myPOSTreader.Item("NumeroBiancheria"))
            TipoArredo = campodb(myPOSTreader.Item("TipoArredo"))

            Id = campodb(myPOSTreader.Item("Id"))
        End If
        myPOSTreader.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Movimenti where    Id =  " & Id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


End Class
