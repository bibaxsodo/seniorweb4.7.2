Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Scadenziario
    Public Tipo As String
    Public Numero As Long
    Public NumeroRegistrazioneContabile As Long
    Public DataScadenza As Date
    Public Importo As Double
    Public Descrizione As String
    Public Chiusa As Integer
    Public RegistrazioneIncasso As Integer

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public Function ControllaScadenza(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Long, ByVal ImportoNetto As Double) As Boolean
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim TotaleImp As Double
        Dim Xps As Boolean
        Dim NumeroScadenze As Long

        ControllaScadenza = True
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        MySql = "Select sum(Importo) as TotImp From Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione

        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TotaleImp = campodbN(myPOSTreader.Item("TotImp"))
        End If
        myPOSTreader.Close()

        If ImportoNetto <> TotaleImp Then
            MySql = "Select count(*) as NScad From Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione

            Dim cmd1 As New OleDbCommand()

            cmd1.CommandText = MySql
            cmd1.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazione)
            cmd1.Connection = cn
            Dim MyRd1 As OleDbDataReader = cmd1.ExecuteReader()
            If MyRd1.Read Then
                NumeroScadenze = campodbN(MyRd1.Item("NScad"))
            End If
            MyRd1.Close()


            If NumeroScadenze > 1 Then
                'pi� di una scadenza
                ControllaScadenza = False
            Else
                MySql = "update Scadenzario set Importo = ? Where NumeroRegistrazioneContabile = " & NumeroRegistrazione

                Dim cmdSave As New OleDbCommand()

                cmdSave.CommandText = MySql
                cmdSave.Parameters.AddWithValue("@Importo", ImportoNetto)
                cmdSave.Connection = cn
                cmdSave.ExecuteNonQuery()
                ControllaScadenza = True
            End If
        End If

        cn.Close()
    End Function


    Sub EliminaScadenze(ByVal StringaConnessione As String, ByVal NumeroRegistrazioneContabile As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Delete  from Scadenzario where " & _
                             "NumeroRegistrazioneContabile = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function VerificaScadenzaChiuso(ByVal StringaConnessione As String, ByVal NumeroRegistrazioneContabile As Integer) As Boolean
        Dim cn As OleDbConnection

        VerificaScadenzaChiuso = True
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Scadenzario where " & _
                             "NumeroRegistrazioneContabile = ? and Chiusa = 1 ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn

        Dim ScadenzeReader As OleDbDataReader = cmd.ExecuteReader()
        If ScadenzeReader.Read Then
            VerificaScadenzaChiuso = False
        End If
        ScadenzeReader.Close()
        cn.Close()

    End Function

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Xps As Boolean

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Scadenzario where " & _
                             "NumeroRegistrazioneContabile = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("DataScadenza", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Chiusa", GetType(String))
        Tabella.Columns.Add("Numero", GetType(Long))
        Tabella.Columns.Add("RegistrazioneIncasso", GetType(Long))
        Tabella.Columns.Add("IdScadenza", GetType(Long))

        Xps = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("DataScadenza"))
            myriga(1) = campodbN(myPOSTreader.Item("Importo"))
            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))
            If Val(campodbN(myPOSTreader.Item("Chiusa"))) = 1 Then
                myriga(3) = "Chiusa"
            Else
                myriga(3) = "Aperta"
            End If
            myriga(4) = campodbN(myPOSTreader.Item("Numero"))

            myriga(5) = campodbN(myPOSTreader.Item("RegistrazioneIncasso"))
            myriga(6) = campodbN(myPOSTreader.Item("ID"))

            Tabella.Rows.Add(myriga)
            Xps = True
        Loop
        myPOSTreader.Close()
        cn.Close()
        If Xps = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
    End Sub
    Sub LeggiAData(ByVal StringaConnessione As String, ByVal Data As Date)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Scadenzario where " & _
                               "DataScadenza = ? ")
        cmd.Parameters.AddWithValue("@DataScadenza", Data)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            Tipo = campodb(VerReader.Item("Tipo"))
            Numero = campodbN(VerReader.Item("Numero"))
            NumeroRegistrazioneContabile = campodbN(VerReader.Item("NumeroRegistrazioneContabile"))
            DataScadenza = campodb(VerReader.Item("DataScadenza"))
            Importo = campodbN(VerReader.Item("Importo"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
            Chiusa = campodbN(VerReader.Item("Chiusa"))
            RegistrazioneIncasso = campodbN(VerReader.Item("RegistrazioneIncasso"))
            Max = Max + 1
        Loop
        VerReader.Close()
        cn.Close()
    End Sub
    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Numero > 0 Then
            cmd.CommandText = ("select * from Scadenzario where " & _
                               "Numero = ? And NumeroRegistrazioneContabile = ? ")
            cmd.Parameters.AddWithValue("@Numero", Numero)
            cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        Else
            cmd.CommandText = ("select * from Scadenzario where " & _
                               "NumeroRegistrazioneContabile = ? ")
            cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        End If

        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            Tipo = campodb(VerReader.Item("Tipo"))
            Numero = campodbN(VerReader.Item("Numero"))
            NumeroRegistrazioneContabile = campodbN(VerReader.Item("NumeroRegistrazioneContabile"))
            DataScadenza = campodbd(VerReader.Item("DataScadenza"))
            Importo = campodbN(VerReader.Item("Importo"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
            Chiusa = campodbN(VerReader.Item("Chiusa"))
            RegistrazioneIncasso = campodbN(VerReader.Item("RegistrazioneIncasso"))
            Max = Max + 1
        Loop
        VerReader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Max As Long
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Delete from Scadenzario where " & _
                             "NumeroRegistrazioneContabile = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Connection = cn

        cmd.ExecuteNonQuery()

        For Max = 0 To Tabella.Rows.Count - 1
            If Not IsDBNull(Tabella.Rows(Max).Item(1)) Then
                If CDbl(Tabella.Rows(Max).Item(1)) <> 0 Then
                    MySql = "INSERT INTO Scadenzario (NumeroRegistrazioneContabile,DataScadenza,Importo,Descrizione,Chiusa,Numero,RegistrazioneIncasso) VALUES (?,?,?,?,?,?,?)"
                    Dim cmdw As New OleDbCommand()
                    cmdw.CommandText = (MySql)

                    cmdw.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)

                    Dim DataAppoggio As Date

                    Try
                        DataAppoggio = Tabella.Rows(Max).Item(0)
                    Catch ex As Exception
                        DataAppoggio = Nothing
                    End Try


                    cmdw.Parameters.AddWithValue("@DataScadenza", DataAppoggio)
                    cmdw.Parameters.AddWithValue("@Importo", CDbl(Tabella.Rows(Max).Item(1)))
                    cmdw.Parameters.AddWithValue("@Descrizione", Tabella.Rows(Max).Item(2))
                    If Tabella.Rows(Max).Item(3).ToString = "Chiusa" Then
                        cmdw.Parameters.AddWithValue("@Chiusa", 1)
                    Else
                        cmdw.Parameters.AddWithValue("@Chiusa", 0)
                    End If
                    cmdw.Parameters.AddWithValue("@Numero", MaxScadenza(StringaConnessione) + 1)

                    cmdw.Parameters.AddWithValue("@RegistrazioneIncasso", Val(Tabella.Rows(Max).Item(5)))

                    cmdw.Connection = cn
                    cmdw.ExecuteNonQuery()
                End If
            End If
        Next

        cn.Close()
    End Sub
    Function MaxScadenza(ByVal StringaConnessione As String, Optional ByVal temp As Boolean = False) As Long
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Not temp Then
            cmd.CommandText = ("select max(Numero) from Scadenzario Where NumeroRegistrazioneContabile = ? ")
        Else
            cmd.CommandText = ("select max(Numero) from Temp_Scadenzario Where NumeroRegistrazioneContabile = ? ")
        End If
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        Max = 0
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Max = campodbN(VerReader.Item(0))
        End If
        VerReader.Close()
        cn.Close()
        Return Max
    End Function

    Sub ScriviScadenza(ByVal StringaConnessione As String, Optional ByVal TempDB As Boolean = False)
        Dim cn As OleDbConnection
        Dim Max As Long
        Dim MySql As String
        Dim Inserimento As Boolean = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If TempDB = False Then
            cmd.CommandText = ("select * from Scadenzario where " & _
                               "Numero = ? And NumeroRegistrazioneContabile = ? ")
        Else

            cmd.CommandText = ("select * from Temp_Scadenzario where " & _
                               "Numero = ? And NumeroRegistrazioneContabile = ? ")
        End If
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If TempDB = False Then
            If VerReader.Read Then
                MySql = "UPDATE Scadenzario SET  DataScadenza = ? ,Importo = ?,Descrizione = ? ,Chiusa = ?,RegistrazioneIncasso = ? Where NumeroRegistrazioneContabile  = ? And Numero = ?"
            Else
                MySql = "INSERT INTO Scadenzario (DataScadenza,Importo,Descrizione,Chiusa,RegistrazioneIncasso,NumeroRegistrazioneContabile,Numero) VALUES (?,?,?,?,?,?,?)"
                Inserimento = True
            End If
        Else
            If VerReader.Read Then
                MySql = "UPDATE Temp_Scadenzario SET  DataScadenza = ? ,Importo = ?,Descrizione = ? ,Chiusa = ?,RegistrazioneIncasso = ? Where NumeroRegistrazioneContabile  = ? And Numero = ?"
            Else
                MySql = "INSERT INTO Temp_Scadenzario (DataScadenza,Importo,Descrizione,Chiusa,RegistrazioneIncasso,NumeroRegistrazioneContabile,Numero) VALUES (?,?,?,?,?,?,?)"
                Inserimento = True
            End If

        End If

        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)

        cmdw.Parameters.AddWithValue("@DataScadenza", DataScadenza)
        cmdw.Parameters.AddWithValue("@Importo", Importo)
        cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdw.Parameters.AddWithValue("@Chiusa", Chiusa)
        cmdw.Parameters.AddWithValue("@RegistrazioneIncasso", RegistrazioneIncasso)
        cmdw.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        If Inserimento = True Then
            cmdw.Parameters.AddWithValue("@Numero", MaxScadenza(StringaConnessione) + 1)
        Else
            cmdw.Parameters.AddWithValue("@Numero", Numero)
        End If

        'RegistrazioneIncasso
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()


        cn.Close()
    End Sub


    Sub LeggiDocumentoIncasso(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = ("select * from Scadenzario where " & _
                           "NumeroRegistrazioneContabile = ? And RegistrazioneIncasso = ?")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Parameters.AddWithValue("@RegistrazioneIncasso", RegistrazioneIncasso)

        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            Tipo = campodb(VerReader.Item("Tipo"))
            Numero = campodbN(VerReader.Item("Numero"))
            NumeroRegistrazioneContabile = campodbN(VerReader.Item("NumeroRegistrazioneContabile"))
            DataScadenza = campodbd(VerReader.Item("DataScadenza"))
            Importo = campodbN(VerReader.Item("Importo"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
            Chiusa = campodbN(VerReader.Item("Chiusa"))
            RegistrazioneIncasso = campodbN(VerReader.Item("RegistrazioneIncasso"))
            Max = Max + 1
        Loop
        VerReader.Close()
        cn.Close()
    End Sub


    Sub ApriScadenzaDocumentoIncasso(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = ("update  Scadenzario  set Chiusa = 0,RegistrazioneIncasso = 0 where " & _
                             "NumeroRegistrazioneContabile = ? And RegistrazioneIncasso = ?")
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", NumeroRegistrazioneContabile)
        cmd.Parameters.AddWithValue("@RegistrazioneIncasso", RegistrazioneIncasso)

        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
End Class
