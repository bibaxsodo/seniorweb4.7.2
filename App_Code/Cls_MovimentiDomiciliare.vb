﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_MovimentiDomiciliare
    Public ID As Long
    Public CENTROSERVIZIO As String
    Public CodiceOspite As Long
    Public Data As Date
    Public Descrizione As String
    Public OraInizio As Date
    Public OraFine As Date
    Public Operatore As String
    Public Tipologia As String
    Public Utente As String
    Public CreatoDocumentoOspiti As Integer
    Public Righe(60) As Cls_MovimentiDomiciliare_Operatori


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim i As Integer = 0
        For I = 0 To 40
            Righe(i) = Nothing
        Next
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & CENTROSERVIZIO & "' And CodiceOspite = " & CodiceOspite & " And ID = " & ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            ID = Val(campodb(myPOSTreader.Item("id")))
            Data = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("centroservizio"))
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            OraInizio = campodbd(myPOSTreader.Item("OraInizio"))
            OraFine = campodbd(myPOSTreader.Item("OraFine"))
            Operatore = campodb(myPOSTreader.Item("Operatore"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))

            CreatoDocumentoOspiti = campodbn(myPOSTreader.Item("CreatoDocumentoOspiti"))

        End If
        myPOSTreader.Close()

        i = 0
        Dim cmdRighe As New OleDbCommand()
        cmdRighe.CommandText = ("select * from MovimentiDomiciliare_Operatori where IdMovimento = " & ID)
        cmdRighe.Connection = cn
        Dim ReadRighe As OleDbDataReader = cmdRighe.ExecuteReader()
        Do While ReadRighe.Read
            Righe(i) = New Cls_MovimentiDomiciliare_Operatori
            Righe(i).Id = campodbn(ReadRighe.Item("Id"))
            Righe(i).CodiceOperatore = campodbn(ReadRighe.Item("CodiceOperatore"))
            Righe(i).IdMovimento = campodbn(ReadRighe.Item("IdMovimento"))
            i = i + 1
        Loop
        ReadRighe.Close()
        cn.Close()
    End Sub



    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from MovimentiDomiciliare where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And ID = " & ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        Dim cmdEl As New OleDbCommand()
        cmdEl.CommandText = ("Delete from MovimentiDomiciliare_Operatori where ID = " & ID)
        cmdEl.Connection = cn
        cmdEl.ExecuteNonQuery()

        cn.Close()
    End Sub


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiDomiciliare where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Id = " & ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE MovimentiDomiciliare SET Descrizione=?," & _
                    " Tipologia  = ?,Data = ?,OraInizio=?,OraFine=?,Operatore  =?,Utente = ?, DataAggiornamento = ? , CreatoDocumentoOspiti  = ?" & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And ID = " & ID
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@OraInizio", OraInizio)
            cmdw.Parameters.AddWithValue("@OraFine", OraFine)
            cmdw.Parameters.AddWithValue("@Operatore", Operatore)
            cmdw.Parameters.AddWithValue("@Utente", utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CreatoDocumentoOspiti", CreatoDocumentoOspiti)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            'OraInizio,OraFine

            MySql = "INSERT INTO MovimentiDomiciliare (CentroServizio,CodiceOspite,Descrizione,Tipologia,Data,OraInizio,OraFine,Operatore,Utente,DataAggiornamento,CreatoDocumentoOspiti) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@OraInizio", OraInizio)
            cmdw.Parameters.AddWithValue("@OraFine", OraFine)
            cmdw.Parameters.AddWithValue("@Operatore", Operatore)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CreatoDocumentoOspiti", CreatoDocumentoOspiti)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()



        If ID = 0 Then
            Dim cmdR As New OleDbCommand()
            cmdR.CommandText = ("select max(iD) from MovimentiDomiciliare where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite)
            cmdR.Connection = cn
            Dim RdReg As OleDbDataReader = cmdR.ExecuteReader()
            If RdReg.Read Then
                ID = campodbn(RdReg.Item(0))
            End If
            RdReg.Close()
        End If

        Dim RigaEl As New Cls_MovimentiDomiciliare_Operatori
        RigaEl.IdMovimento = ID
        RigaEl.Elimina(StringaConnessione)

        Dim i As Integer

        For i = 0 To 40
            If Not IsNothing(Righe(i)) Then
                Dim Riga As New Cls_MovimentiDomiciliare_Operatori

                Riga.Id = 0
                Riga.IdMovimento = ID
                Riga.CodiceOperatore = Righe(i).CodiceOperatore
                Riga.AggiornaDB(StringaConnessione)
            End If
        Next

        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable, Optional ByVal DataDal As Date = Nothing, Optional ByVal DataAl As Date = Nothing)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If IsNothing(DataDal) Or Year(DataDal) < 1920 Then
            cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order by Data Desc")
        Else
            cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And Data >= ? And Data <= ? Order by Data Desc")
            cmd.Parameters.AddWithValue("@Data", DataDal)
            cmd.Parameters.AddWithValue("@Data", DataAl)
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("ID", GetType(Long))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("OraInizio", GetType(String))
        Tabella.Columns.Add("OraFine", GetType(String))
        Tabella.Columns.Add("Ore(centesimi)", GetType(String))
        Tabella.Columns.Add("Numero Operatori", GetType(String))
        Tabella.Columns.Add("Tipologia", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Elaborato", GetType(String))
    
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbn(myPOSTreader.Item("ID"))
            myriga(1) = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
            myriga(2) = Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm")
            myriga(3) = Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm")

            myriga(4) = Math.Round(DateDiff(DateInterval.Minute, campodbd(myPOSTreader.Item("OraInizio")), campodbd(myPOSTreader.Item("OraFine"))) / 60, 2)


            'Dim Oper As New Cls_Operatore

            'Oper.CodiceMedico = Val(campodb(myPOSTreader.Item("Operatore")))
            'Oper.Leggi(StringaConnessione)

            myriga(5) = Val(campodb(myPOSTreader.Item("Operatore"))) ' Oper.Nome

            Dim Tipo As New Cls_TipoDomiciliare

            Tipo.Codice = campodb(myPOSTreader.Item("Tipologia"))
            Tipo.Leggi(StringaConnessione, Tipo.Codice)
            myriga(6) = Tipo.Descrizione

            myriga(7) = campodb(myPOSTreader.Item("Descrizione"))

            If campodbn(myPOSTreader.Item("CreatoDocumentoOspiti")) = 1 Then
                myriga(8) = "SI"
            Else
                myriga(8) = "NO"
            End If

      
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
