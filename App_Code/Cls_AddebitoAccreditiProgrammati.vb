﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_AddebitoAccreditiProgrammati
    Public ID As Long
    Public CentroServizio As String
    Public CodiceOspite As Long
    Public DataInizio As Date
    Public DataFine As Date
    Public Riferimento As String
    Public Parente As Long
    Public CodiceRegione As String
    Public Provincia As String
    Public Comune As String
    Public TipoAddebito As String
    Public Tipo As String
    Public Importo As Double
    Public Descrizione As String
    Public Ripetizione As String
    Public Rendiconto As Integer


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("delete from AddebitoAccreditiProgrammati where centroservizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And ID = ?")
        cmd.Parameters.AddWithValue("@id", ID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

    End Sub

    Sub leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AddebitoAccreditiProgrammati where centroservizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And ID = ?")
        cmd.Parameters.AddWithValue("@id", ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            CodiceOspite = campodbn(myPOSTreader.Item("CodiceOspite"))
            DataInizio = campodbd(myPOSTreader.Item("DataInizio"))
            DataFine = campodbd(myPOSTreader.Item("DataFine"))
            Riferimento = campodb(myPOSTreader.Item("Riferimento"))
            Parente = campodb(myPOSTreader.Item("Parente"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Comune = campodb(myPOSTreader.Item("Comune"))
            TipoAddebito = campodb(myPOSTreader.Item("TipoAddebito"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Importo = campodbn(myPOSTreader.Item("Importo"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

            Ripetizione = campodb(myPOSTreader.Item("Ripetizione"))

            Rendiconto = campodbn(myPOSTreader.Item("Rendiconto"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub UltimaData(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AddebitoAccreditiProgrammati where centroservizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " Order by DataInizio desc")        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            CodiceOspite = campodbn(myPOSTreader.Item("CodiceOspite"))
            DataInizio = campodbd(myPOSTreader.Item("DataInizio"))
            DataFine = campodbd(myPOSTreader.Item("DataFine"))
            Riferimento = campodb(myPOSTreader.Item("Riferimento"))
            Parente = campodb(myPOSTreader.Item("Parente"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Comune = campodb(myPOSTreader.Item("Comune"))
            TipoAddebito = campodb(myPOSTreader.Item("TipoAddebito"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Importo = campodbn(myPOSTreader.Item("Importo"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))

            Ripetizione = campodb(myPOSTreader.Item("Ripetizione"))

            Rendiconto = campodbn(myPOSTreader.Item("Rendiconto"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AddebitoAccreditiProgrammati where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And ID = " & ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE AddebitoAccreditiProgrammati SET DataFine= ?,Riferimento= ?,Parente= ?,CodiceRegione= ?,Provincia= ?,Comune= ?,TipoAddebito= ?,Tipo= ?,Importo= ?,Descrizione= ?,DataInizio= ?,Ripetizione = ?,Rendiconto = ? " & _
                    " WHERE  CentroServizio =?  And  CodiceOspite = ? And ID = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@DataFine", DataFine)
            cmdw.Parameters.AddWithValue("@Riferimento", Riferimento)
            cmdw.Parameters.AddWithValue("@Parente", Parente)
            cmdw.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmdw.Parameters.AddWithValue("@Provincia", Provincia)
            cmdw.Parameters.AddWithValue("@Comune", Comune)
            cmdw.Parameters.AddWithValue("@TipoAddebito", TipoAddebito)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Importo", Importo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DataInizio", DataInizio)
            cmdw.Parameters.AddWithValue("@Ripetizione", Ripetizione)
            cmdw.Parameters.AddWithValue("@Rendiconto", Rendiconto)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@ID", ID)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else

            MySql = "INSERT INTO AddebitoAccreditiProgrammati (CentroServizio,CodiceOspite,DataFine,Riferimento,Parente,CodiceRegione,Provincia,Comune,TipoAddebito,Tipo,Importo,Descrizione,DataInizio,Ripetizione,Rendiconto) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@DataFine", DataFine)
            cmdw.Parameters.AddWithValue("@Riferimento", Riferimento)
            cmdw.Parameters.AddWithValue("@Parente", Parente)
            cmdw.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmdw.Parameters.AddWithValue("@Provincia", Provincia)
            cmdw.Parameters.AddWithValue("@Comune", Comune)
            cmdw.Parameters.AddWithValue("@TipoAddebito", TipoAddebito)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Importo", Importo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DataInizio", DataInizio)
            cmdw.Parameters.AddWithValue("@Ripetizione", Ripetizione)
            cmdw.Parameters.AddWithValue("@Rendiconto", Rendiconto)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


End Class

