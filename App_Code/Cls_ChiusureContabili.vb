﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_ChiusureContabili
    Private GeneraleDB As New ADODB.Connection
    Private TabelleDB As New ADODB.Connection

    Private GeneraleCn As OleDbConnection
    Public StringaConnessione As String
    Public StringaConnessioneTabelle As String


    Private Rs_DatiGenerali As ADODB.Recordset
    Public Tabella As New System.Data.DataTable("tabella")
    Public Chk_Effettiva As Long
    Public DataAperture As Date    
    Public DataInizio As Date
    Public DataFine As Date
    Public DataFineNext As Date
    Public Totale As Double
    Public NumeroRegistrazione As Long
    Public DareAvere As String
    Public ProfittiPerdite As Double
    Public UtileEconomico As Double
    Public PerditaEconomico As Double
    Public AttivatoSimulazione As Boolean

    Public Sub Chiudidb()
        GeneraleDB.Close()

        GeneraleCn.Close()        
    End Sub
    Public Sub Apridb(ByVal Connessione As String)
        GeneraleDB.Open(Connessione)        

        StringaConnessione = Connessione
        GeneraleCn = New Data.OleDb.OleDbConnection(Connessione)

        GeneraleCn.Open()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("DareAvere", GetType(String))
        Tabella.Columns.Add("Totale", GetType(String))
        Tabella.Columns.Add("MastroPartita", GetType(String))
        Tabella.Columns.Add("ContoPartita", GetType(String))
        Tabella.Columns.Add("SottocontoPartita", GetType(String))
        Tabella.Columns.Add("DecodficaPartita", GetType(String))
        Tabella.Columns.Add("MastroContropartita", GetType(String))
        Tabella.Columns.Add("ContoContropartita", GetType(String))
        Tabella.Columns.Add("SottocontoContropartita", GetType(String))
        Tabella.Columns.Add("DecodficaContropartita", GetType(String))
    End Sub

    Private Sub RiempiGriglia(ByVal Registrazione As Long, ByVal Data As Date, ByVal DareAvere As String, ByVal Totale As Double, ByVal MastroPartita As Integer, ByVal ContoPartita As Integer, ByVal SottocontoPartita As Double, ByVal MastroContropartita As Integer, ByVal ContoContropartita As Integer, ByVal SottocontoContropartita As Double, ByVal Descrizione As String)
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = Registrazione
        myriga(1) = Format(Data, "dd/MM/yyyy")
        myriga(2) = DareAvere
        myriga(3) = Totale
        myriga(4) = MastroPartita
        myriga(5) = ContoPartita
        myriga(6) = SottocontoPartita
        Dim x As New Cls_Pianodeiconti
        x.Mastro = MastroPartita
        x.Conto = ContoPartita
        x.Sottoconto = SottocontoPartita
        x.Decodfica(StringaConnessione)

        myriga(7) = x.Descrizione
        myriga(8) = MastroContropartita
        myriga(9) = ContoContropartita

        Dim x1 As New Cls_Pianodeiconti
        x1.Mastro = MastroContropartita
        x1.Conto = ContoContropartita
        x1.Sottoconto = SottocontoContropartita
        x1.Decodfica(StringaConnessione)

        myriga(10) = x1.Descrizione
        Tabella.Rows.Add(myriga)
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Function MaxRegistrazione() As Long
        Dim Rs_MaxF As New ADODB.Recordset

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Max(NumeroRegistrazione) From MovimentiContabiliTesta"
        cmd.Connection = GeneraleCn
        Dim Rs_Read As OleDbDataReader = cmd.ExecuteReader()
        If Rs_Read.Read Then
            MaxRegistrazione = campodbn(Rs_Read.Item(0)) + 1
        End If
        Rs_Read.Close()
        cmd.Dispose()

    End Function


    Public Sub CreaChiusura(ByVal tipo As String)        
        Dim RS_MovimentiContabiliTesta As New ADODB.Recordset
        Dim RS_MovimentiContabiliRiga As New ADODB.Recordset
        Dim MySql As String
        Dim Totale As Double
        Dim DareAvere As String
        Dim ProfittiPerdite As Double
        Dim DatiGenerali As New Cls_DatiGenerali
        Dim NumeroRegistrazione As Long


        DatiGenerali.LeggiDati(StringaConnessioneTabelle)

        MySql = "SELECT * FROM PianoConti WHERE Sottoconto <> 0 AND Tipo = '" & tipo & "'  ORDER BY Mastro, Conto, Sottoconto"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = GeneraleCn
        Dim Rs_Read As OleDbDataReader = cmd.ExecuteReader()
        Do Until Not Rs_Read.Read

            Totale = totaleSottoConto(campodbN(Rs_Read.Item("Mastro")), campodbN(Rs_Read.Item("Conto")), campodbN(Rs_Read.Item("SottoConto")))


            If Totale <> 0 Then
                If Totale < 0 Then
                    DareAvere = "D"
                    Totale = Totale * -1
                Else
                    DareAvere = "A"
                End If
                If Chk_Effettiva = 0 Then
                    If tipo = "A" Or tipo = "P" Or tipo = "O" Then
                        Call RiempiGriglia(0, DataFine, DareAvere, Totale, campodbN(Rs_Read.Item("Mastro")), campodbN(Rs_Read.Item("Conto")), campodbN(Rs_Read.Item("SottoConto")), DatiGenerali.BilancioChiusuraMastro, DatiGenerali.BilancioChiusuraConto, DatiGenerali.BilancioChiusuraSottoconto, "Chiusura")
                        If DareAvere = "D" Then
                            DareAvere = "A"
                        Else
                            DareAvere = "D"
                        End If
                        Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.BilancioChiusuraMastro, DatiGenerali.BilancioChiusuraConto, DatiGenerali.BilancioChiusuraSottoconto, campodbN(Rs_Read.Item("Mastro")), campodbN(Rs_Read.Item("Conto")), campodbN(Rs_Read.Item("SottoConto")), "Chiusura")
                    Else
                        Call RiempiGriglia(0, DataFine, DareAvere, Totale, campodbN(Rs_Read.Item("Mastro")), campodbN(Rs_Read.Item("Conto")), campodbN(Rs_Read.Item("SottoConto")), DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoconto, "Chiusura")
                        If DareAvere = "D" Then
                            ProfittiPerdite = Math.Round(ProfittiPerdite - Totale, 2)
                            DareAvere = "A"
                        Else
                            ProfittiPerdite = Math.Round(ProfittiPerdite + Totale, 2)
                            DareAvere = "D"
                        End If
                        Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoconto, campodbN(Rs_Read.Item("Mastro")), campodbN(Rs_Read.Item("Conto")), campodbN(Rs_Read.Item("Sottoconto")), "Chiusura")
                    End If
                Else
                    MySql = "SELECT * FROM MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
                    RS_MovimentiContabiliTesta.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If RS_MovimentiContabiliTesta.EOF Then
                        RS_MovimentiContabiliTesta.AddNew()
                    End If
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroRegistrazione"), MaxRegistrazione())
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("DataRegistrazione"), DataFine)
                    'MoveToDb(RS_MovimentiContabiliTesta.Fields("DataDocumento"), System.DBNull.Value)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroDocumento"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Competenza"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("IVASospesa"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("CodicePagamento"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("RegistroIva"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroProtocollo"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("AnnoProtocollo"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("CausaleContabile"), DatiGenerali.CausaleChiusura)

                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Utente"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("DataAggiornamento"), Now)
                    RS_MovimentiContabiliTesta.Update()
                    NumeroRegistrazione = MoveFromDbWC(RS_MovimentiContabiliTesta, "NumeroRegistrazione")
                    RS_MovimentiContabiliTesta.Close()
                    MySql = "Select * from MovimentiContabiliRiga where Numero = " & NumeroRegistrazione
                    RS_MovimentiContabiliRiga.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Do Until RS_MovimentiContabiliRiga.EOF
                        RS_MovimentiContabiliRiga.Delete()
                        RS_MovimentiContabiliRiga.MoveNext()
                    Loop
                    RS_MovimentiContabiliRiga.AddNew()
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), DareAvere)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), campodbN(Rs_Read.Item("Mastro")))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), campodbN(Rs_Read.Item("Conto")))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), campodbN(Rs_Read.Item("Sottoconto")))
                    If tipo = "A" Or tipo = "P" Or tipo = "O" Then
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), DatiGenerali.BilancioChiusuraMastro)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), DatiGenerali.BilancioChiusuraConto)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), DatiGenerali.BilancioChiusuraSottoconto)
                    Else
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), DatiGenerali.ProfittiPerditeMastro)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), DatiGenerali.ProfittiPerditeConto)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), DatiGenerali.ProfittiPerditeSottoconto)
                    End If
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 1)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 1)

                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)
                    Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Chiusura")
                    RS_MovimentiContabiliRiga.Update()
                    RS_MovimentiContabiliRiga.AddNew()
                    If DareAvere = "A" Then
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "A")
                    End If
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                    If tipo = "A" Or tipo = "P" Or tipo = "O" Then
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.BilancioChiusuraMastro)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.BilancioChiusuraConto)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.BilancioChiusuraSottoconto)
                    Else
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.ProfittiPerditeMastro)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.ProfittiPerditeConto)
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.ProfittiPerditeSottoconto)
                    End If
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), campodbN(Rs_Read.Item("Mastro")))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), campodbN(Rs_Read.Item("Conto")))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), campodbN(Rs_Read.Item("SottoConto")))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 2)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 2)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)
                    Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Chiusura")
                    RS_MovimentiContabiliRiga.Update()
                    RS_MovimentiContabiliRiga.Close()
                End If
            End If


        Loop
        Rs_Read.Close()
    End Sub


    Public Sub CreaApertura(ByVal tipo As String)
        Dim MyRs As New ADODB.Recordset
        Dim RS_MovimentiContabiliTesta As New ADODB.Recordset
        Dim RS_MovimentiContabiliRiga As New ADODB.Recordset
        Dim MySql As String
        Dim Totale As Double
        Dim DareAvere As String                
        Dim Rs_DatiGenerali As New ADODB.Recordset
        Dim NumeroRegistrazione As Long
        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(StringaConnessioneTabelle)


        MySql = "SELECT * FROM PianoConti WHERE Sottoconto <> 0 AND Tipo = '" & tipo & "' ORDER BY Mastro, Conto, Sottoconto"
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do Until MyRs.EOF
            Totale = totaleSottoConto(MoveFromDbWC(MyRs, "Mastro"), MoveFromDbWC(MyRs, "Conto"), MoveFromDbWC(MyRs, "SottoConto"))
            If Totale <> 0 Then
                If Totale < 0 Then
                    DareAvere = "A"
                    Totale = Totale * -1
                Else
                    DareAvere = "D"
                End If
                If Chk_Effettiva = 0 Then
                    Call RiempiGriglia(0, DataAperture, DareAvere, Totale, MoveFromDbWC(MyRs, "Mastro"), MoveFromDbWC(MyRs, "Conto"), MoveFromDbWC(MyRs, "SottoConto"), DatiGenerali.BilancioAperturaMastro, DatiGenerali.BilancioAperturaConto, DatiGenerali.BilancioAperturaSottoConto, "Apertura")
                    If DareAvere = "D" Then
                        DareAvere = "A"
                    Else
                        DareAvere = "D"
                    End If
                    Call RiempiGriglia(0, DataAperture, DareAvere, Totale, DatiGenerali.BilancioAperturaMastro, DatiGenerali.BilancioAperturaConto, DatiGenerali.BilancioAperturaSottoConto, MoveFromDbWC(MyRs, "Mastro"), MoveFromDbWC(MyRs, "Conto"), MoveFromDbWC(MyRs, "SottoConto"), "Apertura")
                Else
                    MySql = "SELECT * FROM MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
                    RS_MovimentiContabiliTesta.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    If RS_MovimentiContabiliTesta.EOF Then
                        RS_MovimentiContabiliTesta.AddNew()
                    End If
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroRegistrazione"), MaxRegistrazione())
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("DataRegistrazione"), DataAperture)
                    'MoveToDb(RS_MovimentiContabiliTesta.Fields("DataDocumento"), System.DBNull.Value)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroDocumento"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Competenza"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("IVASospesa"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("CodicePagamento"), "")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("RegistroIva"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroProtocollo"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("AnnoProtocollo"), 0)
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("CausaleContabile"), DatiGenerali.CausaleApertura)

                    MoveToDb(RS_MovimentiContabiliTesta.Fields("Utente"), "Chiusure")
                    MoveToDb(RS_MovimentiContabiliTesta.Fields("DataAggiornamento"), Now)

                    RS_MovimentiContabiliTesta.Update()
                    NumeroRegistrazione = MoveFromDbWC(RS_MovimentiContabiliTesta, "NumeroRegistrazione")
                    RS_MovimentiContabiliTesta.Close()
                    MySql = "Select * from MovimentiContabiliRiga where Numero = " & NumeroRegistrazione
                    RS_MovimentiContabiliRiga.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Do Until RS_MovimentiContabiliRiga.EOF
                        RS_MovimentiContabiliRiga.Delete()
                        RS_MovimentiContabiliRiga.MoveNext()
                    Loop
                    RS_MovimentiContabiliRiga.AddNew()
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), DareAvere)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), MoveFromDbWC(MyRs, "Mastro"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), MoveFromDbWC(MyRs, "Conto"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), MoveFromDbWC(MyRs, "SottoConto"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), DatiGenerali.BilancioAperturaMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), DatiGenerali.BilancioAperturaConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), DatiGenerali.BilancioAperturaSottoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 1)
                    Call RiempiGriglia(NumeroRegistrazione, DataAperture, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Apertura")

                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 1)

                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                    RS_MovimentiContabiliRiga.Update()
                    RS_MovimentiContabiliRiga.AddNew()
                    If DareAvere = "A" Then
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "A")
                    End If
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.BilancioAperturaMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.BilancioAperturaConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.BilancioAperturaSottoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), MoveFromDbWC(MyRs, "Mastro"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), MoveFromDbWC(MyRs, "Conto"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), MoveFromDbWC(MyRs, "SottoConto"))
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 2)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 2)

                    Call RiempiGriglia(NumeroRegistrazione, DataAperture, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Apertura")
                    RS_MovimentiContabiliRiga.Update()
                    RS_MovimentiContabiliRiga.Close()
                End If
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
    End Sub

    Public Sub RilevaUtilePerdita()
        Dim Totale As Double
        Dim DareAvere As String
        Dim UtileEconomico As Double
        Dim RS_MovimentiContabiliTesta As New ADODB.Recordset
        Dim RS_MovimentiContabiliRiga As New ADODB.Recordset
        Dim MySql As String
        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(StringaConnessioneTabelle)

        '
        ' Crea Registrazione da Profitti e Perdite a Utile Economico o Perdita Economico
        '
        If Chk_Effettiva = 0 Then
            Totale = ProfittiPerdite
        Else
            Totale = totaleSottoConto(DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoConto)
        End If
        If Totale <> 0 Then
            If Totale < 0 Then
                DareAvere = "D"
                Totale = Totale * -1
            Else
                DareAvere = "A"
            End If
            If Chk_Effettiva = 0 Then
                If DareAvere = "D" Then
                    Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoConto, DatiGenerali.UtileEconomicoMastro, DatiGenerali.UtileEconomicoConto, DatiGenerali.UtileEconomicoSottoConto, "Utile Economico")
                    DareAvere = "A"
                    Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.UtileEconomicoMastro, DatiGenerali.UtileEconomicoConto, DatiGenerali.UtileEconomicoSottoConto, DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoConto, "Utile Economico")
                    UtileEconomico = Math.Round(UtileEconomico - Totale, 2)
                Else
                    Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoConto, DatiGenerali.PerditaEconomicoMastro, DatiGenerali.PerditaEconomicoConto, DatiGenerali.PerditaEconomicoSottoConto, "Perdita Economico")
                    DareAvere = "D"
                    Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.PerditaEconomicoMastro, DatiGenerali.PerditaEconomicoConto, DatiGenerali.PerditaEconomicoSottoConto, DatiGenerali.ProfittiPerditeMastro, DatiGenerali.ProfittiPerditeConto, DatiGenerali.ProfittiPerditeSottoConto, "Perdita Economico")
                    PerditaEconomico = Math.Round(PerditaEconomico + Totale, 2)
                End If
            Else
                MySql = "SELECT * FROM MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
                RS_MovimentiContabiliTesta.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If RS_MovimentiContabiliTesta.EOF Then
                    RS_MovimentiContabiliTesta.AddNew()
                End If
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroRegistrazione"), MaxRegistrazione())
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataRegistrazione"), DataFine)
                'MoveToDb(RS_MovimentiContabiliTesta.Fields("DataDocumento"), System.DBNull.Value)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroDocumento"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Competenza"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("IVASospesa"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CodicePagamento"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("RegistroIva"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("AnnoProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataAggiornamento"), Now)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CausaleContabile"), DatiGenerali.CausaleChiusura)
                RS_MovimentiContabiliTesta.Update()
                NumeroRegistrazione = MoveFromDbWC(RS_MovimentiContabiliTesta, "NumeroRegistrazione")
                RS_MovimentiContabiliTesta.Close()
                MySql = "Select * from MovimentiContabiliRiga where Numero = " & NumeroRegistrazione
                RS_MovimentiContabiliRiga.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do Until RS_MovimentiContabiliRiga.EOF
                    RS_MovimentiContabiliRiga.Delete()
                    RS_MovimentiContabiliRiga.MoveNext()
                Loop
                RS_MovimentiContabiliRiga.AddNew()
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), DareAvere)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.ProfittiPerditeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.ProfittiPerditeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.ProfittiPerditeSottoConto)
                If DareAvere = "D" Then
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), DatiGenerali.UtileEconomicoMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), DatiGenerali.UtileEconomicoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), DatiGenerali.UtileEconomicoSottoConto)
                Else
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroContropartita"), DatiGenerali.PerditaEconomicoMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoContropartita"), DatiGenerali.PerditaEconomicoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoContropartita"), DatiGenerali.PerditaEconomicoSottoConto)
                End If
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 1)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 1)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Utile/Perdita")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.AddNew()
                If DareAvere = "A" Then
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "D")
                Else
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "A")
                End If
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                If DareAvere = "D" Then
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.UtileEconomicoMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.UtileEconomicoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.UtileEconomicoSottoConto)
                Else
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.PerditaEconomicoMastro)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.PerditaEconomicoConto)
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.PerditaEconomicoSottoConto)
                End If
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroControPartita"), DatiGenerali.ProfittiPerditeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoControPartita"), DatiGenerali.ProfittiPerditeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoControPartita"), DatiGenerali.ProfittiPerditeSottoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 2)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 2)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Utile/Perdita")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.Close()
            End If
        End If
        '
        ' Crea Registrazione da Utile Economico a Utile Patrimoniale
        '
        If Chk_Effettiva = 0 Then
            Totale = UtileEconomico
        Else
            Totale = totaleSottoConto(DatiGenerali.UtileEconomicoMastro, DatiGenerali.UtileEconomicoConto, DatiGenerali.UtileEconomicoSottoConto)
        End If
        If Totale <> 0 Then
            If Totale < 0 Then
                DareAvere = "D"
                Totale = Totale * -1
            Else
                DareAvere = "A"
            End If
            If Chk_Effettiva = 0 Then
                Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.UtileEconomicoMastro, DatiGenerali.UtileEconomicoConto, DatiGenerali.UtileEconomicoSottoConto, DatiGenerali.UtilePatrimonialeMastro, DatiGenerali.UtilePatrimonialeConto, DatiGenerali.UtilePatrimonialeSottoConto, "Giroconto Utile")
                If DareAvere = "D" Then
                    DareAvere = "A"
                Else
                    DareAvere = "D"
                End If
                Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.UtilePatrimonialeMastro, DatiGenerali.UtilePatrimonialeConto, DatiGenerali.UtilePatrimonialeSottoConto, DatiGenerali.UtileEconomicoMastro, DatiGenerali.UtileEconomicoConto, DatiGenerali.UtileEconomicoSottoConto, "Giroconto Utile")
            Else
                MySql = "SELECT * FROM MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
                RS_MovimentiContabiliTesta.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If RS_MovimentiContabiliTesta.EOF Then
                    RS_MovimentiContabiliTesta.AddNew()
                End If
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroRegistrazione"), MaxRegistrazione())
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataRegistrazione"), DataFine)
                'MoveToDb(RS_MovimentiContabiliTesta.Fields("DataDocumento"), System.DBNull.Value)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroDocumento"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Competenza"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("IVASospesa"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CodicePagamento"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("RegistroIva"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("AnnoProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CausaleContabile"), DatiGenerali.CausaleChiusura)

                MoveToDb(RS_MovimentiContabiliTesta.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataAggiornamento"), Now)
                RS_MovimentiContabiliTesta.Update()
                NumeroRegistrazione = MoveFromDbWC(RS_MovimentiContabiliTesta, "NumeroRegistrazione")
                RS_MovimentiContabiliTesta.Close()
                MySql = "Select * from MovimentiContabiliRiga where Numero = " & NumeroRegistrazione
                RS_MovimentiContabiliRiga.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do Until RS_MovimentiContabiliRiga.EOF
                    RS_MovimentiContabiliRiga.Delete()
                    RS_MovimentiContabiliRiga.MoveNext()
                Loop
                RS_MovimentiContabiliRiga.AddNew()
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), DareAvere)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.UtileEconomicoMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.UtileEconomicoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.UtileEconomicoSottoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroControPartita"), DatiGenerali.UtilePatrimonialeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoControPartita"), DatiGenerali.UtilePatrimonialeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoControPartita"), DatiGenerali.UtilePatrimonialeSottoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 1)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 1)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Giroconto Utile")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.AddNew()
                If DareAvere = "A" Then
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "D")
                Else
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "A")
                End If
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.UtilePatrimonialeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.UtilePatrimonialeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.UtilePatrimonialeSottoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroControPartita"), DatiGenerali.UtileEconomicoMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoControPartita"), DatiGenerali.UtileEconomicoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoControPartita"), DatiGenerali.UtileEconomicoSottoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 2)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 2)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Giroconto Utile")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.Close()
            End If
        End If
        '
        ' Crea Registrazione da Perdita Economico a Perdita Patrimoniale
        '
        If Chk_Effettiva = 0 Then
            Totale = PerditaEconomico
        Else
            Totale = totaleSottoConto(DatiGenerali.PerditaEconomicoMastro, DatiGenerali.PerditaEconomicoConto, DatiGenerali.PerditaEconomicoSottoconto)
        End If
        If Totale <> 0 Then
            If Totale < 0 Then
                DareAvere = "D"
                Totale = Totale * -1
            Else
                DareAvere = "A"
            End If
            If Chk_Effettiva = 0 Then
                Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.PerditaEconomicoMastro, DatiGenerali.PerditaEconomicoConto, DatiGenerali.PerditaEconomicoSottoconto, DatiGenerali.PerditaPatrimonialeMastro, DatiGenerali.PerditaPatrimonialeConto, DatiGenerali.PerditaPatrimonialeSottoconto, "Giroconto Perdita")
                If DareAvere = "D" Then
                    DareAvere = "A"
                Else
                    DareAvere = "D"
                End If
                Call RiempiGriglia(0, DataFine, DareAvere, Totale, DatiGenerali.PerditaPatrimonialeMastro, DatiGenerali.PerditaPatrimonialeConto, DatiGenerali.PerditaPatrimonialeSottoconto, DatiGenerali.PerditaEconomicoMastro, DatiGenerali.PerditaEconomicoConto, DatiGenerali.PerditaEconomicoSottoconto, "Giroconto Perdita")
            Else
                MySql = "SELECT * FROM MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
                RS_MovimentiContabiliTesta.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If RS_MovimentiContabiliTesta.EOF Then
                    RS_MovimentiContabiliTesta.AddNew()
                End If
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroRegistrazione"), MaxRegistrazione())
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataRegistrazione"), DataFine)
                'MoveToDb(RS_MovimentiContabiliTesta.Fields("DataDocumento"), System.DBNull.Value)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroDocumento"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Competenza"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("IVASospesa"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CodicePagamento"), "")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("RegistroIva"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("NumeroProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("AnnoProtocollo"), 0)
                MoveToDb(RS_MovimentiContabiliTesta.Fields("CausaleContabile"), DatiGenerali.CausaleChiusura)

                MoveToDb(RS_MovimentiContabiliTesta.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliTesta.Fields("DataAggiornamento"), Now)

                RS_MovimentiContabiliTesta.Update()
                NumeroRegistrazione = MoveFromDbWC(RS_MovimentiContabiliTesta, "NumeroRegistrazione")
                RS_MovimentiContabiliTesta.Close()
                MySql = "Select * from MovimentiContabiliRiga where Numero = " & NumeroRegistrazione
                RS_MovimentiContabiliRiga.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do Until RS_MovimentiContabiliRiga.EOF
                    RS_MovimentiContabiliRiga.Delete()
                    RS_MovimentiContabiliRiga.MoveNext()
                Loop
                RS_MovimentiContabiliRiga.AddNew()
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), DareAvere)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.PerditaEconomicoMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.PerditaEconomicoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.PerditaEconomicoSottoconto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroControPartita"), DatiGenerali.PerditaPatrimonialeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoControPartita"), DatiGenerali.PerditaPatrimonialeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoControPartita"), DatiGenerali.PerditaPatrimonialeSottoconto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 1)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 1)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Giroconto Perdita")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.AddNew()
                If DareAvere = "A" Then
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "D")
                Else
                    MoveToDb(RS_MovimentiContabiliRiga.Fields("DareAvere"), "A")
                End If
                MoveToDb(RS_MovimentiContabiliRiga.Fields("numero"), NumeroRegistrazione)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroPartita"), DatiGenerali.PerditaPatrimonialeMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoPartita"), DatiGenerali.PerditaPatrimonialeConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoPartita"), DatiGenerali.PerditaPatrimonialeSottoconto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("MastroControPartita"), DatiGenerali.PerditaEconomicoMastro)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("ContoControPartita"), DatiGenerali.PerditaEconomicoConto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("SottocontoControPartita"), DatiGenerali.PerditaEconomicoSottoconto)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Descrizione"), "")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Importo"), Totale)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("Segno"), "+")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaDaCausale"), 2)
                MoveToDb(RS_MovimentiContabiliRiga.Fields("RigaRegistrazione"), 2)

                MoveToDb(RS_MovimentiContabiliRiga.Fields("Utente"), "Chiusure")
                MoveToDb(RS_MovimentiContabiliRiga.Fields("DataAggiornamento"), Now)

                Call RiempiGriglia(NumeroRegistrazione, DataFine, MoveFromDbWC(RS_MovimentiContabiliRiga, "DareAvere"), Totale, MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoPartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "MastroContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "ContoContropartita"), MoveFromDbWC(RS_MovimentiContabiliRiga, "SottocontoContropartita"), "Giroconto Perdita")
                RS_MovimentiContabiliRiga.Update()
                RS_MovimentiContabiliRiga.Close()
            End If
        End If
    End Sub


    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long) As Double
        Dim MCRs As New ADODB.Recordset
        Dim MySql As String
        Dim Condizione As String
        Dim Dare As Double
        Dim Avere As Double



        Condizione = " And (Competenza IS NULL OR Competenza = '') "



        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'D' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione <= {ts'" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & Condizione
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Connection = GeneraleCn
        Dim Rs_Read1 As OleDbDataReader = cmd1.ExecuteReader()
        If Rs_Read1.Read Then
            Dare = campodbN(Rs_Read1.Item("Totale"))
        End If
        Rs_Read1.Close()


        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'A' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione <= {ts'" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & Condizione


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = GeneraleCn
        Dim Rs_Read As OleDbDataReader = cmd.ExecuteReader()
        If Rs_Read.Read Then
            Avere = campodbN(Rs_Read.Item("Totale"))
        End If
        Rs_Read.Close()

        totaleSottoConto = Math.Round(Dare - Avere, 2)

    End Function


    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub
End Class
