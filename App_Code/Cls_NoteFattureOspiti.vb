﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_NoteFattureOspiti
    Public id As Long
    Public Utente As String
    Public DataAggiornamento As String
    Public CodiceServizio As String
    Public CodiceProvincia As String
    Public CodiceComune As String
    Public CodiceRegione As String
    Public OspitiParentiComuniRegioni As String
    Public NoteUp As String
    Public NoteDown As String
    Public NumeroRegistrazione As Long
    Public CODICIOSPITI As String


    Sub Delete(ByVal StringaConnessione As String, ByVal MyID As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from NoteFatture where " & _
                           " Id = ? ")
        cmd.Parameters.AddWithValue("@CodiceOspite", MyID)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub LeggiRegioneCserv(ByVal StringaConnessione As String, ByVal Cserv As String, ByVal Reg As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFatture where " & _
                           " CodiceServizio= ? And CodiceRegione = ?  ")
        cmd.Parameters.AddWithValue("@Cserv", Cserv)
        cmd.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Utente = campodb(myPOSTreader.Item("Utente"))
            DataAggiornamento = campodb(myPOSTreader.Item("DataAggiornamento"))
            CodiceServizio = campodb(myPOSTreader.Item("CodiceServizio"))
            CodiceProvincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            CodiceComune = campodb(myPOSTreader.Item("CodiceComune"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            OspitiParentiComuniRegioni = campodb(myPOSTreader.Item("OspitiParentiComuniRegioni"))
            NoteUp = campodb(myPOSTreader.Item("NoteUp"))
            NoteDown = campodb(myPOSTreader.Item("NoteDown"))
            NumeroRegistrazione = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            CODICIOSPITI = campodb(myPOSTreader.Item("CODICIOSPITI"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiComuneCserv(ByVal StringaConnessione As String, ByVal Cserv As String, ByVal CdProv As String, ByVal CdCom As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFatture where " & _
                           " CodiceServizio= ? And CodiceProvincia = ?  And  CodiceComune = ? ")
        cmd.Parameters.AddWithValue("@Cserv", Cserv)
        cmd.Parameters.AddWithValue("@CodiceProvincia", CdProv)
        cmd.Parameters.AddWithValue("@CodiceComune", CdCom)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Utente = campodb(myPOSTreader.Item("Utente"))
            DataAggiornamento = campodb(myPOSTreader.Item("DataAggiornamento"))
            CodiceServizio = campodb(myPOSTreader.Item("CodiceServizio"))
            CodiceProvincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            CodiceComune = campodb(myPOSTreader.Item("CodiceComune"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            OspitiParentiComuniRegioni = campodb(myPOSTreader.Item("OspitiParentiComuniRegioni"))
            NoteUp = campodb(myPOSTreader.Item("NoteUp"))
            NoteDown = campodb(myPOSTreader.Item("NoteDown"))
            NumeroRegistrazione = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            CODICIOSPITI = campodb(myPOSTreader.Item("CODICIOSPITI"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyID As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFatture where " & _
                           " Id = ? ")
        cmd.Parameters.AddWithValue("@CodiceOspite", MyID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            Utente = campodb(myPOSTreader.Item("Utente"))
            DataAggiornamento = campodb(myPOSTreader.Item("DataAggiornamento"))
            CodiceServizio = campodb(myPOSTreader.Item("CodiceServizio"))
            CodiceProvincia = campodb(myPOSTreader.Item("CodiceProvincia"))
            CodiceComune = campodb(myPOSTreader.Item("CodiceComune"))
            CodiceRegione = campodb(myPOSTreader.Item("CodiceRegione"))
            OspitiParentiComuniRegioni = campodb(myPOSTreader.Item("OspitiParentiComuniRegioni"))
            NoteUp = campodb(myPOSTreader.Item("NoteUp"))
            NoteDown = campodb(myPOSTreader.Item("NoteDown"))
            NumeroRegistrazione = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            CODICIOSPITI = campodb(myPOSTreader.Item("CODICIOSPITI"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from NoteFatture where " & _
                           "Id = ? ")
        cmd.Parameters.AddWithValue("@Id", id)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO NoteFatture (CodiceServizio,CodiceProvincia,CodiceComune,CodiceRegione,OspitiParentiComuniRegioni,NoteUp,NoteDown,NumeroRegistrazione,CODICIOSPITI,Utente,DataAggiornamento) values " & _
                                            "(?,?,?,?,?,?,?,?,?,?,?) "

            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql

            cmdIns.Parameters.AddWithValue("@CodiceServizio", CodiceServizio)
            cmdIns.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdIns.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdIns.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmdIns.Parameters.AddWithValue("@OspitiParentiComuniRegioni", OspitiParentiComuniRegioni)
            cmdIns.Parameters.AddWithValue("@NoteUp", NoteUp)
            cmdIns.Parameters.AddWithValue("@NoteDown", NoteDown)
            cmdIns.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmdIns.Parameters.AddWithValue("@CODICIOSPITI", CODICIOSPITI)
            cmdIns.Parameters.AddWithValue("@Utente", Utente)
            cmdIns.Parameters.AddWithValue("@DataAggiornamento", DataAggiornamento)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        Else
            Dim XSql As String
            XSql = "UPDATE NoteFatture SET CodiceServizio = ?,CodiceProvincia = ?,CodiceComune = ?,CodiceRegione = ?,OspitiParentiComuniRegioni = ?,NoteUp = ?,NoteDown = ?,NumeroRegistrazione = ?,CODICIOSPITI = ?,Utente = ?,DataAggiornamento= ? WHERE ID = ? "


            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql

            cmdIns.Parameters.AddWithValue("@CodiceServizio", CodiceServizio)
            cmdIns.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdIns.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdIns.Parameters.AddWithValue("@CodiceRegione", CodiceRegione)
            cmdIns.Parameters.AddWithValue("@OspitiParentiComuniRegioni", OspitiParentiComuniRegioni)
            cmdIns.Parameters.AddWithValue("@NoteUp", NoteUp)
            cmdIns.Parameters.AddWithValue("@NoteDown", NoteDown)
            cmdIns.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmdIns.Parameters.AddWithValue("@CODICIOSPITI", CODICIOSPITI)
            cmdIns.Parameters.AddWithValue("@Utente", Utente)
            cmdIns.Parameters.AddWithValue("@DataAggiornamento", DataAggiornamento)
            cmdIns.Parameters.AddWithValue("@ID", id)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        

        cn.Close()
    End Sub
End Class

