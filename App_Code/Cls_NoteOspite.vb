﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_NoteOspite
    Public CodiceOspite As Long
    Public Note As String


    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As Long)
        Dim cn As OleDbConnection

        CodiceOspite = 0
        Note = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PostIT where " & _
                           "CodiceOspite = ? ")
        cmd.Parameters.AddWithValue("@CodiceOspite", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = MyCodice
            Note = campodb(myPOSTreader.Item("Note"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PostIT where " & _
                           "CodiceOspite = ? ")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO PostIT (CodiceOspite,Note) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdIns.Parameters.AddWithValue("@Note", Note)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE PostIT SET " & _
                " Note = ? " & _
                " Where CodiceOspite = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Note", Note)
        cmd1.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)        
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub
End Class
