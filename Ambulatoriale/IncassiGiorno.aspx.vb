﻿Imports System.Data.OleDb

Public Class IncassiGiorno
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

 Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_GENERALE")
        Dim cn As OleDbConnection
        Dim cnTabelle As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim Totale As Double =0


        cnTabelle = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnTabelle.Open()
        Dim cmd As New OleDbCommand()

        Condizione = ""
        cmd.CommandText = "Select * From CausaliContabiliTesta where TIPO = 'P' AND VenditaAcquisti = 'V'"
        cmd.Connection = cnTabelle
        Dim RdCauCont As OleDbDataReader = cmd.ExecuteReader()
        Do While RdCauCont.Read
            If Condizione <> "" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " CausaleContabile = '" & campodb(RdCauCont.Item("Codice")) & "'"

        Loop
        RdCauCont.Close()
        cnTabelle.Close()

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Data Registrazione", GetType(String))
        MyTable.Columns.Add("Causale", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("ContoCassa", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Utente Inserimento/Modifica", GetType(String))

        Dim cmd1 As New OleDbCommand()

        Dim AppoggioUtente As String = Session("UTENTE")

        For i = 1 To 20
            AppoggioUtente = AppoggioUtente.Replace("<" & i & ">", "")
        Next i


        If Chk_SoloUtente.Checked = False Then
            If DD_CausaleContabile.SelectedValue = "" Then
                MySql = "Select * From MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And (" & Condizione & ")"
            Else
                MySql = "Select * From MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
            End If
        Else
            If DD_CausaleContabile.SelectedValue = "" Then
                MySql = "Select * From MovimentiContabiliTesta Where Utente like '" & AppoggioUtente & "%' And DataRegistrazione >= ? And DataRegistrazione <= ? And (" & Condizione & ")"
            Else
                MySql = "Select * From MovimentiContabiliTesta Where Utente like '" & AppoggioUtente & "%' And DataRegistrazione >= ? And DataRegistrazione <= ? And CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
            End If
        End If
        cmd1.CommandText = MySql
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd1.Parameters.AddWithValue("@DataDal", DataDal)
        cmd1.Parameters.AddWithValue("@DataAl", DataAl)
        cmd1.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga.Item(0) = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            myriga.Item(1) = campodb(myPOSTreader.Item("DataRegistrazione"))

            Dim CauContabile As New Cls_CausaleContabile

            CauContabile.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            CauContabile.Leggi(Session("DC_TABELLE"), CauContabile.Codice)
            myriga.Item(2) = CauContabile.Descrizione
            Dim MovimentoContabile As New Cls_MovimentoContabile


            MovimentoContabile.NumeroRegistrazione = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            MovimentoContabile.Leggi(Session("DC_GENERALE"), MovimentoContabile.NumeroRegistrazione, False)

            Dim DecPDC As New Cls_Pianodeiconti

            DecPDC.Mastro = MovimentoContabile.Righe(0).MastroPartita
            DecPDC.Conto = MovimentoContabile.Righe(0).ContoPartita
            DecPDC.Sottoconto = MovimentoContabile.Righe(0).SottocontoPartita
            DecPDC.Decodfica(Session("DC_GENERALE"))

            myriga.Item(3) = DecPDC.Descrizione
            myriga.Item(4) = campodb(myPOSTreader.Item("Descrizione"))


            DecPDC.Descrizione = ""
            DecPDC.Mastro = MovimentoContabile.Righe(1).MastroPartita
            DecPDC.Conto = MovimentoContabile.Righe(1).ContoPartita
            DecPDC.Sottoconto = MovimentoContabile.Righe(1).SottocontoPartita
            DecPDC.Decodfica(Session("DC_GENERALE"))

            myriga.Item(5) = DecPDC.Descrizione

            myriga.Item(6) = Format(MovimentoContabile.ImportoRegistrazioneDocumento(Session("DC_TABELLE")),"#,##0.00")

            AppoggioUtente = campodb(myPOSTreader.Item("UTENTE"))

            For i = 1 To 20
                AppoggioUtente = AppoggioUtente.Replace("<" & i & ">", "")
            Next i
            myriga.Item(7) = AppoggioUtente

            MyTable.Rows.Add(myriga)

            Totale = Totale  + CDbl(myriga.Item(6))
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()

        myriga1.Item(5) = "Totale : "

        myriga1.Item(6) = Format(Totale,"#,##0.00")

        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Dim Causale As New Cls_CausaleContabile

        Causale.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleContabile)

        Call EseguiJS()
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Call CaricaTabella()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click

        Call CaricaTabella()
        Call EseguiJS()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Movimenti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Call CaricaTabella()


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

        Call EseguiJS()
    End Sub
End Class