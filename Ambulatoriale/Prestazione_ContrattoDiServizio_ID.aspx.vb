﻿Imports Senior.Entities
Imports Senior.BIZ

Public Class Prestazione_ContrattoDiServizio_ID
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Codice.Enabled =False
        Txt_Codice.Text="0"

        If val(Request.Item("ID_Operatore")) > 0 then
            Dim ContrattoManager As New  Prestazione_ContrattoDiServizioBiz(Session("DC_TABELLE"))
            Dim Controatto As New Prestazione_ContrattoDiServizio            

            Controatto =ContrattoManager.FindByID(Request.Item("ID_Operatore"))
        
            Txt_Codice.Text= Controatto.ID_ContrattoDiServizio
            Txt_Descrizione.Text = Controatto.Descrizione_Interna

        End if

    End Sub

    Private Sub Btn_Modifica_Click(sender As Object, e As ImageClickEventArgs) Handles Btn_Modifica.Click
        If Val(Txt_Codice.Text) = 0 Then
            Dim ContrattoManager As New  Prestazione_ContrattoDiServizioBiz(Session("DC_TABELLE"))
            Dim Controatto As New Prestazione_ContrattoDiServizio
                      
            Controatto.ID_ContrattoDiServizio =0
            Controatto.Descrizione_Interna = Txt_Descrizione.Text
            Controatto.Descrizione=""
            Controatto.Chiave_Import=""
            Controatto.Convenzionato =0
            ContrattoManager.Add(Controatto)
           Else
            Dim ContrattoManager As New  Prestazione_ContrattoDiServizioBiz(Session("DC_TABELLE"))
            Dim Controatto As New Prestazione_ContrattoDiServizio
                      
            Controatto.ID_ContrattoDiServizio =Txt_Codice.Text

            Controatto = ContrattoManager.FindByID(Txt_Codice.Text)

            Controatto.ID_ContrattoDiServizio =Txt_Codice.Text
            Controatto.Descrizione_Interna = Txt_Descrizione.Text
            Controatto.Descrizione=""
            Controatto.Chiave_Import=""
            Controatto.Convenzionato =0
            ContrattoManager.Update(Controatto)
        End If

        Response.Redirect("ContrattoServizio_Elenco.aspx")
    End Sub

    Private Sub Btn_Esci_Click(sender As Object, e As ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ContrattoServizio_Elenco.aspx")
    End Sub
End Class