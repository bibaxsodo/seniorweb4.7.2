﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports Senior.Entities
Imports Senior.BIZ

Partial Class Ambulatoriale_Prestazioni_Tipologie_Elenco
    Inherits System.Web.UI.Page

    Private ID_Branca As Integer = 0
    Private StringaRicerca As String = ""
    Private SoloPrestazioniAttive As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()

        If Not Page.IsPostBack Then



            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            btn_Nuovo.Visible = True
            FillSearchOptions()

            LoadPrestazioniTipologie()
        End If

        ReadSearchParametersFromUI()
    End Sub

    Private Sub FillSearchOptions()
        Dim MyListaBranche As New Branca_List
        If Session("ElencoBranche") Is Nothing Then
            Dim BracheManager As New BrancaBiz(Session("DC_TABELLE"))
            MyListaBranche = BracheManager.List
            Session("ElencoBranche") = MyListaBranche
        Else
            MyListaBranche = CType(Session("ElencoBranche"), Branca_List)
        End If

        ddl_Brache.DataSource = MyListaBranche
        ddl_Brache.DataBind()
    End Sub

    Private Sub ReadSearchParametersFromUI()
        ID_Branca = ddl_Brache.SelectedValue
        StringaRicerca = Txt_Ricerca.Text
        SoloPrestazioniAttive = cb_Attive.Checked
    End Sub

    Private Sub LoadPrestazioniTipologie()
        Dim PrestazioniTipologieManager As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
        grd_ElencoTipologiePrestazioni.DataSource = PrestazioniTipologieManager.RicercaPerElenco(ID_Branca, StringaRicerca, SoloPrestazioniAttive)
        grd_ElencoTipologiePrestazioni.DataBind()
    End Sub

    Protected Sub grd_ElencoTipologiePrestazioni_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd_ElencoTipologiePrestazioni.PageIndexChanging
        grd_ElencoTipologiePrestazioni.PageIndex = e.NewPageIndex
        LoadPrestazioniTipologie()
    End Sub

    Protected Sub grd_ElencoTipologiePrestazioni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd_ElencoTipologiePrestazioni.RowCommand
        Select Case e.CommandName
            Case "Archivia"
                'e.CommandArgument
            Case "Listino"
                Response.Redirect("Prestazioni_Tipologie_Listino.aspx?ID_Prestazione_Tipologia=" & e.CommandArgument)
        End Select
    End Sub

    Protected Sub btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Ricerca.Click
        LoadPrestazioniTipologie()
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Nuovo.Click

        Response.Redirect("Prestazioni_Tipologie_Listino.aspx?ID_Prestazione_Tipologia=0")

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine

        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
