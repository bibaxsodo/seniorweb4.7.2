﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports Senior.Entities
Imports Senior.BIZ
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq


Partial Class Ambulatoriale_Prestazioni_Tipologie_Listino
    Inherits System.Web.UI.Page



    Private ReadOnly Property ID_Prestazione_Tipologia As Integer
        Get
            Dim MyInt As Integer
            If Not Integer.TryParse(Request.QueryString("ID_Prestazione_Tipologia"), MyInt) Then
                Return 0
            End If
            Return Request.QueryString("ID_Prestazione_Tipologia")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()

        If Not Page.IsPostBack Then

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If


            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            btn_Salva.Visible = True
            Fill_UI()

        End If
    End Sub

    Private Sub Fill_UI()
        If Not Page.IsPostBack Then
            ddl_ContrattoDiServizio.DataSource = GetListaContrattoDiServizio()
            ddl_ContrattoDiServizio.DataBind()

            ddl_FasciaIse.DataSource = GetListaFasciaIse()
            ddl_FasciaIse.DataBind()

            ddl_Branche1.DataSource = GetListaBranche()
            ddl_Branche1.DataBind()
            ddl_Branche2.DataSource = GetListaBranche()
            ddl_Branche2.DataBind()
            ddl_Branche3.DataSource = GetListaBranche()
            ddl_Branche3.DataBind()
            ddl_Branche4.DataSource = GetListaBranche()
            ddl_Branche4.DataBind()
            ddl_Branche5.DataSource = GetListaBranche()
            ddl_Branche5.DataBind()

            ddl_ePersonam_exams.DataSource = GetListaEsami_ePersonam()
            ddl_ePersonam_exams.DataBind()
        End If

        If ID_Prestazione_Tipologia <> 0 Then

            btn_AggiungiListino.Enabled = True

            Dim MyPrestazioneTipologia = GetPrestazioneTipologia()
            lbl_TitoloTab.Text = MyPrestazioneTipologia.Descrizione
            txt_Descrizione.Text = MyPrestazioneTipologia.Descrizione

            If MyPrestazioneTipologia.Branche IsNot Nothing Then
                If MyPrestazioneTipologia.Branche.Count > 0 Then
                    ddl_Branche1.SelectedValue = MyPrestazioneTipologia.Branche.ElementAtOrDefault(0).ID_Branca
                End If
                If MyPrestazioneTipologia.Branche.Count > 1 Then
                    ddl_Branche2.SelectedValue = MyPrestazioneTipologia.Branche.ElementAtOrDefault(1).ID_Branca
                End If
                If MyPrestazioneTipologia.Branche.Count > 2 Then
                    ddl_Branche3.SelectedValue = MyPrestazioneTipologia.Branche.ElementAtOrDefault(2).ID_Branca
                End If
                If MyPrestazioneTipologia.Branche.Count > 3 Then
                    ddl_Branche4.SelectedValue = MyPrestazioneTipologia.Branche.ElementAtOrDefault(3).ID_Branca
                End If
                If MyPrestazioneTipologia.Branche.Count > 4 Then
                    ddl_Branche5.SelectedValue = MyPrestazioneTipologia.Branche.ElementAtOrDefault(4).ID_Branca
                End If
            End If

            txt_Codice_Prestazione.Text = MyPrestazioneTipologia.Codice_Prestazione
            txt_Note.Text = MyPrestazioneTipologia.Note
            txt_Chiave_Import.Text = MyPrestazioneTipologia.Chiave_Import

            grd_ListinoTipologiePrestazioni.DataSource = GetListinoForPrestazione()
            grd_ListinoTipologiePrestazioni.DataBind()

            grid_Prestazioni_ePersonam.DataSource = GetPrestazioni_ePersonam()
            grid_Prestazioni_ePersonam.DataBind()
        Else
            btn_AggiungiListino.Enabled = False
        End If

    End Sub

#Region "Prestazioni_Tipologie"

    Private Function GetListaBranche() As Branca_List
        Dim MyListaBranche As New Branca_List
        If Session("ElencoBranche") Is Nothing Then
            Dim BracheManager As New BrancaBiz(Session("DC_TABELLE"))
            MyListaBranche = BracheManager.List
            Session("ElencoBranche") = MyListaBranche
        Else
            MyListaBranche = CType(Session("ElencoBranche"), Branca_List)
        End If
        Return MyListaBranche
    End Function

    Private Function GetPrestazioneTipologia() As Prestazione_Tipologia
        Dim PrestazioniTipologieManager As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
        Return PrestazioniTipologieManager.FindByID_Prestazione_Tipologia(ID_Prestazione_Tipologia)
    End Function

    Protected Sub Btn_Salva_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Salva.Click

        Dim PrestazioniTipologieManager As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
        If ID_Prestazione_Tipologia = 0 Then
            Dim MyPrestazioneTipologia As Prestazione_Tipologia = PrestazioniTipologieManager.Add(Fill_Entity_from_UI())
            Response.Redirect("Prestazioni_Tipologie_Listino.aspx?ID_Prestazione_Tipologia=" & MyPrestazioneTipologia.ID_Prestazione_Tipologia)
        Else
            PrestazioniTipologieManager.Update(Fill_Entity_from_UI())
        End If

    End Sub

    Private Function Fill_Entity_from_UI() As Prestazione_Tipologia
        Dim MyPrestazioneTipologia As New Prestazione_Tipologia
        MyPrestazioneTipologia.Descrizione = txt_Descrizione.Text
        MyPrestazioneTipologia.Branche = New List(Of Branca)
        If ddl_Branche1.SelectedValue <> 0 Then
            MyPrestazioneTipologia.Branche.Add(New Branca With {.ID_Branca = ddl_Branche1.SelectedValue})
        End If
        If ddl_Branche2.SelectedValue <> 0 Then
            MyPrestazioneTipologia.Branche.Add(New Branca With {.ID_Branca = ddl_Branche2.SelectedValue})
        End If
        If ddl_Branche3.SelectedValue <> 0 Then
            MyPrestazioneTipologia.Branche.Add(New Branca With {.ID_Branca = ddl_Branche3.SelectedValue})
        End If
        If ddl_Branche4.SelectedValue <> 0 Then
            MyPrestazioneTipologia.Branche.Add(New Branca With {.ID_Branca = ddl_Branche4.SelectedValue})
        End If
        If ddl_Branche5.SelectedValue <> 0 Then
            MyPrestazioneTipologia.Branche.Add(New Branca With {.ID_Branca = ddl_Branche5.SelectedValue})
        End If

        MyPrestazioneTipologia.Codice_Prestazione = txt_Codice_Prestazione.Text
        MyPrestazioneTipologia.Note = txt_Note.Text
        MyPrestazioneTipologia.Chiave_Import = txt_Chiave_Import.Text
        MyPrestazioneTipologia.ID_Prestazione_Tipologia = ID_Prestazione_Tipologia

        Return MyPrestazioneTipologia
    End Function

#End Region

#Region "Importi Listino"
    Private Function GetListaFasciaIse() As Tabella_FasciaISE_List
        Dim MyListaFasceISEE As New Tabella_FasciaISE_List
        If Session("ElencoFasceISEE") Is Nothing Then
            Dim FasceISEEManager As New Tabella_FasciaISEBiz(Session("DC_TABELLE"))
            MyListaFasceISEE = FasceISEEManager.List_For_DropDown
            Session("ElencoFasceISEE") = MyListaFasceISEE
        Else
            MyListaFasceISEE = CType(Session("ElencoFasceISEE"), Tabella_FasciaISE_List)
        End If
        Return MyListaFasceISEE
    End Function

    Private Function GetListaContrattoDiServizio() As Prestazione_ContrattoDiServizio_List
        Dim MyListaContrattoDiServizio As New Prestazione_ContrattoDiServizio_List
        If Session("ElencoContrattoDiServizio") Is Nothing Then
            Dim ContrattoDiServizioManager As New Prestazione_ContrattoDiServizioBiz(Session("DC_TABELLE"))
            MyListaContrattoDiServizio = ContrattoDiServizioManager.List
            Session("ElencoContrattoDiServizio") = MyListaContrattoDiServizio
        Else
            MyListaContrattoDiServizio = CType(Session("ElencoContrattoDiServizio"), Prestazione_ContrattoDiServizio_List)
        End If
        Return MyListaContrattoDiServizio
    End Function

    Private Function GetListinoForPrestazione() As Prestazione_Listino_List
        Dim ListinoManager As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
        Return ListinoManager.List_By_Prestazione_Tipologia(ID_Prestazione_Tipologia)
    End Function

    Private Sub btn_AggiungiListino_Click(sender As Object, e As EventArgs) Handles btn_AggiungiListino.Click
        Dim PrestazioniListinoManager As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
        PrestazioniListinoManager.Add(RecordListinoFromUI())
        grd_ListinoTipologiePrestazioni.DataSource = GetListinoForPrestazione()
        grd_ListinoTipologiePrestazioni.DataBind()
    End Sub

    Protected Sub grd_ListinoTipologiePrestazioni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd_ListinoTipologiePrestazioni.RowCommand
        Select Case e.CommandName
            Case "Elimina"
                Dim Prestazione_ListinoManager As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
                Prestazione_ListinoManager.Delete(e.CommandArgument)
                grd_ListinoTipologiePrestazioni.DataSource = GetListinoForPrestazione()
                grd_ListinoTipologiePrestazioni.DataBind()
        End Select
    End Sub

    Private Function RecordListinoFromUI() As Prestazione_Listino
        Dim MyPrestazione_Listino As New Prestazione_Listino With {
            .ID_ContrattoDiServizio = ddl_ContrattoDiServizio.SelectedValue,
            .ID_FasciaISEE = ddl_FasciaIse.SelectedValue,
            .ID_Prestazione_Tipologia = ID_Prestazione_Tipologia,
            .Importo = txt_Importo.Text}

        Return MyPrestazione_Listino
    End Function
#End Region

#Region "ePersonam"

    Private Function GetPrestazioni_ePersonam() As Prestazione_Tipologia__ePersonam_exams_List
        Dim PrestazioniEPersonamManager As New Prestazione_Tipologia__ePersonam_examsBiz(Session("DC_TABELLE"))
        Return PrestazioniEPersonamManager.List_By_ID_Prestazione_Tipologia(ID_Prestazione_Tipologia)
    End Function

    Private Function GetListaEsami_ePersonam() As ePersonam_exams_List
        Dim ePersonamManager As New ePersonamBiz("super", "Advenias202002", 851,0)
        Return ePersonamManager.ElencoEsami
    End Function
    Private Sub btn_AddPrestazione_ePersonam_Click(sender As Object, e As EventArgs) Handles btn_AddPrestazione_ePersonam.Click
        Dim PrestazioniEPersonamManager As New Prestazione_Tipologia__ePersonam_examsBiz(Session("DC_TABELLE"))
        PrestazioniEPersonamManager.Add(RecordEPersonamFromUI())
        grid_Prestazioni_ePersonam.DataSource = GetPrestazioni_ePersonam()
        grid_Prestazioni_ePersonam.DataBind()
    End Sub

    Protected Sub grid_Prestazioni_ePersonam_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid_Prestazioni_ePersonam.RowCommand
        Select Case e.CommandName
            Case "Elimina"
                Dim PrestazioniEPersonamManager As New Prestazione_Tipologia__ePersonam_examsBiz(Session("DC_TABELLE"))
                PrestazioniEPersonamManager.Delete(e.CommandArgument)
                grid_Prestazioni_ePersonam.DataSource = GetPrestazioni_ePersonam()
                grid_Prestazioni_ePersonam.DataBind()
        End Select
    End Sub


    Private Function RecordEPersonamFromUI() As Prestazione_Tipologia__ePersonam_exams
        Dim MyPrestazione_ePersonam As New Prestazione_Tipologia__ePersonam_exams With {
            .Codice_ePersonam = ddl_ePersonam_exams.SelectedValue,
            .Descrizione_ePersonam = ddl_ePersonam_exams.SelectedItem.Text,
            .ID_Prestazione_Tipologia = ID_Prestazione_Tipologia,
            .Tipologia_ePersonam = ePersonamBiz.DettagliEsame(ddl_ePersonam_exams.SelectedValue).Tipologia_ePersonam
        }

        Return MyPrestazione_ePersonam
    End Function
#End Region



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine

        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
