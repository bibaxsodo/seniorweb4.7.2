﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Ambulatoriale_Appuntamenti_Elenco" CodeFile="Appuntamenti_Elenco.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elenco Operatori</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=1" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ambulatoriale - Elenco Operatori</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="label_Ricerca" AssociatedControlID="Txt_Ricerca" Text="Nome: " runat="server" />
                                    <asp:TextBox ID="Txt_Ricerca" Width="450px" runat="server"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:Label ID="label1" AssociatedControlID="txt_CF" Text="Codice Fiscale: " runat="server" />
                                    <asp:TextBox ID="txt_CF" Width="150px" runat="server"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:Label ID="label2" AssociatedControlID="txt_Data" Text="Data Appuntamento: " runat="server" />
                                    <asp:TextBox ID="Txt_Data" Width="100px" runat="server"  ></asp:TextBox>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" CssClass="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ambulatoriale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" />
                        </a>
                        <br />                        
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="Btn_Nuovo" runat="server" ImageUrl="../images/nuovo.png" CssClass="EffettoBottoniTondi" ToolTip="Aggiungi" />
                        </div>
                        <asp:GridView ID="grd_ElencoAppuntamenti" runat="server" CellPadding="3" ItemType="Senior.Entities.DettaglioAppuntamento"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" AutoGenerateColumns="false"
                            BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                            ShowFooter="True" PageSize="20">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" CssClass="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%# Item.CodiceOperazione  %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>         
                                <asp:BoundField HeaderText="Data" DataField="Data" DataFormatString="{0:d}" />
                                <asp:BoundField HeaderText="Ora" DataField="Ora" DataFormatString="{0:t}" />
                                <asp:BoundField HeaderText="Operatore" DataField="Operatore" />
                                <asp:BoundField HeaderText="Cognome Utente" DataField="Utente.Cognome" />
                                <asp:BoundField HeaderText="Nome Utente" DataField="Utente.Nome" />
                                <asp:BoundField HeaderText="Cod. Fiscale" DataField="Utente.CodiceFiscale" />
                                <asp:BoundField HeaderText="Contratto" DataField="Contratto" />
                                <asp:BoundField HeaderText="Elenco Prestazioni" DataField="ElencoPrestazioni" />
                                <asp:BoundField HeaderText="Elenco Prestazioni" DataField="Fattura" />
                               

                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
