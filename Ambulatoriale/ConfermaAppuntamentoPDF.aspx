﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConfermaAppuntamentoPDF.aspx.vb" Inherits="ConfermaAppuntamentoPDF" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xsau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ambulatoriale - Imp. PDF appuntamenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=1" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <style>
        th {
            font-weight: normal;
        }

        .tabella {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        .miotr {
            background: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        .miacella {
            padding: .625em;
            text-align: center;
        }

        .miaintestazione {
            padding: .625em;
            text-align: center;
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }
                .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 480px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="36000" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Ambulatoriale - Imp. PDF appuntamenti<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">                            
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="ImgMenu" ImageUrl="../images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />                        
                        <br />
                        <br />
                        <br />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xsau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xsau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Imp. PDF appuntamenti
                                </HeaderTemplate>
                                <ContentTemplate>
                                  <div class="Row">
                                      <div class="Column">
                                        <div class="cornicegrigia">
                                            <label class="LabelCampo">Codice Operazione :</label>
                                            <asp:TextBox ID="Txt_CodiceOperazione" runat="server" Width="100px" ></asp:TextBox><br />                                            
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Data Appuntamento:</label>
                                            <asp:TextBox ID="Txt_Data" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox><br />                                            
                                            <br />
                                            <label class="LabelCampo">Ora Appuntamento:</label>
                                            <asp:TextBox ID="TXt_Ora" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox><br />                                            
                                            <br />
                                            <label class="LabelCampo">Codice Operatore :</label>
                                            <asp:TextBox ID="Txt_CodiceOperatore" runat="server" Width="100px" ></asp:TextBox><br />                                            
                                            <br />
                                            <label class="LabelCampo">Tipo contatto :</label>
                                            <asp:DropDownList ID="DD_TipoContatto" class="chosen-select" runat="server">
                                                <asp:ListItem Text="SPO" Value="SPO"></asp:ListItem>
                                                <asp:ListItem Text="SPO" Value="SPO"></asp:ListItem>
                                            </asp:DropDownList><br />                                           
                                            <br />
                                            <label class="LabelCampo">Provenienza  :</label>
                                            <asp:DropDownList ID="DD_Provenienza" class="chosen-select" runat="server">
                                                <asp:ListItem Text="EST" Value="EST"></asp:ListItem>
                                                <asp:ListItem Text="EST" Value="EST"></asp:ListItem>
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Codice Impegnativa :</label>
                                            <asp:TextBox ID="Txt_CodiceImpegnativa" runat="server" Width="140px" ></asp:TextBox><br />                                            
                                            <br />
                                            <label class="LabelCampo">Contratto :</label>
                                            <asp:DropDownList ID="DD_CONTRATTO" class="chosen-select" runat="server">
                                                <asp:ListItem Text="Accesso SSR - Esente" Value="Accesso SSR - Esente"></asp:ListItem>
                                                <asp:ListItem Text="Accesso SSR" Value="Accesso SSR"></asp:ListItem>
                                                <asp:ListItem Text="Libera professione" Value="Libera professione"></asp:ListItem>
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Priorita :</label>
                                            <asp:DropDownList ID="DD_Priorita"  runat="server">
                                                <asp:ListItem Text="P" Value="P"></asp:ListItem>
                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                <asp:ListItem Text="U" Value="U"></asp:ListItem>                                                
                                                <asp:ListItem Text="" Value=""></asp:ListItem>         
                                            </asp:DropDownList><br />
                                            <br />                                                                         
                                            <label class="LabelCampo">Testo quesito :</label>
                                            <asp:TextBox ID="Txt_Testo" runat="server" Width="400px" ></asp:TextBox><br />                                            
                                            <br />                                            
                                        </div>
                                       </div>

                                    <div class="Column">
                                    <div class="cornicegrigia">
                                    <br />
                                    <label class="LabelCampo">Operatore :</label>
                                    <asp:DropDownList ID="Dd_Operatore" class="chosen-select-large" Style="border-color: Red;" runat="server" Width="300px"  DataTextField="CognomeNome" DataValueField="Codice_Operatore" ></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Cognome :</label>
                                    <asp:TextBox ID="Txt_Cognome" runat="server" Width="300px" Style="border-color: Red;"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Nome :</label>
                                    <asp:TextBox ID="Txt_Nome" runat="server" Width="300px" Style="border-color: Red;"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Sesso :</label>
                                    <asp:DropDownList ID="DD_Sesso" runat="server">
                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                    <asp:ListItem Text="M" Value="M"></asp:ListItem>
                                    </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Data Nascita :</label>
                                    <asp:TextBox ID="Txt_DataNascita" runat="server" Width="100px" Style="border-color: Red;" ></asp:TextBox><br />
                                    <br />                              
                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" runat="server" Width="150px" ></asp:TextBox><br />
                                    <br />      
                                    <label class="LabelCampo">Codice Fiscale  :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" Width="190px" ></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" runat="server" Width="300px" ></asp:TextBox><br />
                                    <br />                                    

                                    <label class="LabelCampo">Cap :</label>
                                    <asp:TextBox ID="Txt_Cap" runat="server" Width="50px" ></asp:TextBox> <br />
                                     <br />
                                     <label class="LabelCampo">comune PDF:</label>                                   
                                     <asp:Label ID="lbl_ComuneLetto" runat="server" Text=""></asp:Label><br /><br />
                                     <label class="LabelCampo">Comune :</label>                                                                           
                                    <asp:TextBox ID="Txt_Comune" runat="server" Width="300px" ></asp:TextBox>
                                        
                                     <br />
                                    <br />                                    

                                    </div>
                                    </div>
                                      
                                    </div>
                                    <div class="Row">
                                    <div class="Column">
                                        <div class="cornicegrigia">
                                            <br />
                                            <asp:label id="lbl_Visita1" runat="server" Text="" ForeColor="Red"></asp:label>
                                            <label class="LabelCampo">Visita :</label>
                                            <asp:DropDownList ID="DD_VISITA1" class="chosen-select-large"  Style="border-color: Red;" runat="server" Width="70%" DataTextField="Descrizione" DataValueField="ID_Prestazione_Tipologia"></asp:DropDownList><br />        
                                            <br />
                                            <asp:label id="lbl_Visita2" runat="server" Text="" ForeColor="Red"></asp:label>
                                            <label class="LabelCampo">Visita :</label>
                                            <asp:DropDownList ID="DD_VISITA2" class="chosen-select-large" runat="server" Width="70%"  DataTextField="Descrizione" DataValueField="ID_Prestazione_Tipologia"></asp:DropDownList><br />        
                                            <br />
                                            <asp:label id="lbl_Visita3" runat="server" Text="" ForeColor="Red"></asp:label>
                                            <label class="LabelCampo">Visita :</label>
                                            <asp:DropDownList ID="DD_VISITA3" class="chosen-select-large"  runat="server" Width="70%"  DataTextField="Descrizione" DataValueField="ID_Prestazione_Tipologia"></asp:DropDownList><br />        
                                            <br />
                                            <asp:label id="lbl_Visita4" runat="server" Text="" ForeColor="Red"></asp:label>
                                            <label class="LabelCampo">Visita :</label>
                                            <asp:DropDownList ID="DD_VISITA4" class="chosen-select-large" runat="server" Width="70%"  DataTextField="Descrizione" DataValueField="ID_Prestazione_Tipologia"></asp:DropDownList><br />     
                                            <br />
                                            <asp:label id="lbl_Visita5" runat="server" Text="" ForeColor="Red"></asp:label>
                                            <label class="LabelCampo">Visita :</label>
                                            <asp:DropDownList ID="DD_VISITA5" class="chosen-select-large" runat="server" Width="70%"  DataTextField="Descrizione" DataValueField="ID_Prestazione_Tipologia"></asp:DropDownList><br />                                                                                             
                                            <br />
                                            <br />                                            
                                            <div style="position: absolute; right: 30px;">
                                                <asp:Button ID="Img_Modifica" runat="server" Height="60px" Width="400px" BackColor="Red"  ForeColor="White" Text="Conferma" />                                                
                                            </div>

                                        </div>                                      
                                    </div>                                      
                                    </div>
                                    <asp:Label ID="Lbl_Errori" runat="server" Text=""></asp:Label>


                                </ContentTemplate>
                            </xsau:TabPanel>
                        </xsau:TabContainer>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="GridExcel" runat="server">
            </asp:GridView>
        </div>
    </form>

</body>
</html>
