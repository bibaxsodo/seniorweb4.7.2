﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Ambulatoriale_Menu_Ambulatoriale" CodeFile="Menu_Ambulatoriale.aspx.vb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Ambulatoriale</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <script src="https://kit.fontawesome.com/2d6fbbbf2d.js" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;">
                    <td>
                        <div class="Titolo">Ambulatoriale</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 1024px;">
                            <tr>
                                <td style="text-align: center; width: 150px;"><a href="ImportPDF_SSR.aspx" id="A1"><i class="fas fa-cloud-upload-alt fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="NuovoAppuntamentoManuale.aspx" id="A1"><i class="far fa-calendar-check fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Appuntamenti_Elenco.aspx" id="A1"><i class="far fa-calendar-check fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="IncassiGiorno.aspx" id="A1"><i class="fas fa-money-check-alt fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="ContrattoServizio_Elenco.aspx" id="A1"><i class="fas fa-file-signature fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Prestazioni_Tipologie_Elenco.aspx" id="A1"><i class="fas fa-euro-sign fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Prestazioni_Tipologie_Listino.aspx" id="A1"><i class="fas fa-hospital-user fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Operatori_Elenco.aspx" id="A1"><i class="fas fa-user-md fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT PDF</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">NUOVO APPUNTAMENTO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">APPUNTAMNETI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">SITUAZIONE INCASSO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTRATTI DI SERVIZIO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">LISTINO PRESTAZIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">UTENTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">OPERATORI</span></td>                                
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>


                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>
