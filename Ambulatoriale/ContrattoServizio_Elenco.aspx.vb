﻿Imports Senior.Entities
Imports Senior.BIZ

Public Class ContrattoServizio_Elenco
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Btn_Nuovo.Visible = True
        If Not Page.IsPostBack Then
            Session("Ambulatoriale_ListaOperatori") = Nothing
            grd_ElencoOperatori.DataSource = GetOperatori()
            grd_ElencoOperatori.DataBind()
        End If
    End Sub

    Private Sub grd_ElencoOperatori_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grd_ElencoOperatori.RowCommand
        
        If e.CommandName = "Seleziona" Then
            Response.Redirect("Prestazione_ContrattoDiServizio_ID.aspx?ID_Operatore=" & e.CommandArgument)
        End If

    End Sub

    Private Sub Btn_Nuovo_Click(sender As Object, e As ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("Prestazione_ContrattoDiServizio_ID.aspx?ID_Operatore=0")
    End Sub

    Private Sub Btn_Esci_Click(sender As Object, e As ImageClickEventArgs) Handles Btn_Esci.Click
       Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub

    Private Function GetOperatori() As Prestazione_ContrattoDiServizio_List
        Dim OperatoreManager As New  Prestazione_ContrattoDiServizioBiz(Session("DC_TABELLE"))
        Dim MyListaOperatori As New Prestazione_ContrattoDiServizio_List

        If String.IsNullOrEmpty(Txt_Ricerca.Text) Then
            MyListaOperatori = OperatoreManager.List()
        Else
            MyListaOperatori = OperatoreManager.List()
        End If
        Session("Ambulatoriale_ListaOperatori") = MyListaOperatori
        Return MyListaOperatori
    End Function

End Class