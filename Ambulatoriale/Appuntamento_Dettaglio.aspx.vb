﻿Imports System.Configuration
Imports System.Text
Imports System.Web
Imports System.Web.Security
Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class Appuntamento_Dettaglio
    Inherits System.Web.UI.Page




    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Private Function LeggiFattura(ByVal Numero As Integer)  As Integer
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        LeggiFattura=0

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From MovimentiContabiliTesta where IdProgettoODV = ? And NumeroProtocollo > 0"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Numero", numero)
        
        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read then
            LeggiFattura = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
        End if
        myPOSTreader.Close()
        cn.Close()
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
              

         Dim Barra As New Cls_BarraSenior
         Dim K1 As New Cls_SqlString

         If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
         End If

         Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        EseguiJS()

         If Page.IsPostBack = True Then Exit Sub

        Dim OperatoreManager As New  OperatoreBiz(Session("DC_OSPITE"))

        Dd_Operatore.DataSource = OperatoreManager.List_For_DropDown()
        Dd_Operatore.DataBind



        Dim FascieISEManager As New Tabella_FasciaISEBiz(Session("DC_TABELLE"))
        Dim FasciaISE As New  Tabella_FasciaISE
        Dim ListaFasciaISE As New Tabella_FasciaISE_List

        ListaFasciaISE  =FascieISEManager.List_For_DropDown()

        FasciaISE.Descrizione ="Non Indicato"
        FasciaISE.ID_FasciaISEE =0

        ListaFasciaISE.Add(FasciaISE)

        DD_FASCIAISE.DataSource = ListaFasciaISE
        DD_FASCIAISE.Items.Add("")
        DD_FASCIAISE.DataBind
        
        Dim Iva As New Cls_IVA


        Iva.UpDateDropBox(Session("DC_TABELLE"),DDIvaCr)



        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)
        
        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))
            
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("ID_Operatore")

        Dim MyFilteredList As List(Of DettaglioAppuntamento)

        MyFilteredList = AppuntamentiFilePDF.Where(Function(appuntamento) ( appuntamento.CodiceOperazione = CodiceOperazione)).ToList




        Try
            Txt_Cognome.Text = MyFilteredList.Item(0).Utente.Cognome.Trim
        Catch ex As Exception

        End Try
            
            
        Try
            Dd_Operatore.SelectedValue = MyFilteredList.Item(0).OperatoreMedico
        Catch ex As Exception

        End Try

        Try
            Txt_Nome.Text = MyFilteredList.Item(0).Utente.Nome.Trim
        Catch ex As Exception

        End Try

        Try
            Txt_CodiceFiscale.Text =MyFilteredList.Item(0).Utente.CodiceFiscale.Trim
        Catch ex As Exception

        End Try
                                  

        Try
            DD_Sesso.SelectedValue = MyFilteredList.Item(0).Utente.Sesso
        Catch ex As Exception

        End Try

        Try
            If Year(MyFilteredList.Item(0).Utente.DataNascita) > 1900 then
                Txt_DataNascita.Text =format(MyFilteredList.Item(0).Utente.DataNascita,"dd/MM/yyyy")
            Else
                Txt_DataNascita.Text =""
            End If
        Catch ex As Exception

        End Try

        Try
            Txt_Telefono.Text = MyFilteredList.Item(0).Utente.Telefono1.Trim
        Catch ex As Exception

        End Try

        Try
                Txt_Indirizzo.Text = MyFilteredList.Item(0).Utente.Indirizzo.Trim
        Catch ex As Exception

        End Try

            
        Try
                Txt_cap.Text = MyFilteredList.Item(0).Utente.CAP.Trim
        Catch ex As Exception

        End Try

            
        Try
                Txt_Comune.Text = MyFilteredList.Item(0).Utente.Comune.Trim
        Catch ex As Exception

        End Try


        Try
            DD_FASCIAISE.SelectedValue = MyFilteredList.Item(0).Utente.IDFasciaIsee
        Catch ex As Exception

        End Try
                        

        Try
            Txt_Data.Text  = format(MyFilteredList.Item(0).Data,"dd/MM/yyyy")
        Catch ex As Exception

        End Try
            
        Try
            TXt_Ora.Text   = format(MyFilteredList.Item(0).Ora,"HH:mm")
        Catch ex As Exception

        End Try

        Try
            Txt_CodiceOperatore.Text = MyFilteredList.Item(0).Operatore
        Catch ex As Exception

        End Try
            


            

        Try
            DD_Provenienza.SelectedValue = MyFilteredList.Item(0).Provenienza
        Catch ex As Exception

        End Try

        Try
            If not IsNothing(MyFilteredList.Item(0).CodiceImpegnativa) Then
                Txt_CodiceImpegnativa.Text = MyFilteredList.Item(0).CodiceImpegnativa.Trim
            Else
                Txt_CodiceImpegnativa.Text = ""
            End If                
        Catch ex As Exception

        End Try

            
        Try
            If Not IsNothing(Txt_Testo.Text = MyFilteredList.Item(0).TestoQuesito) then
                Txt_Testo.Text = MyFilteredList.Item(0).TestoQuesito.Trim
            End if
        Catch ex As Exception

        End Try


        Try
            DD_TipoContatto.SelectedValue= MyFilteredList.Item(0).TipoContatto
        Catch ex As Exception

        End Try

        


        Try
            DD_CONTRATTO.SelectedValue= MyFilteredList.Item(0).Contratto.Trim
        Catch ex As Exception

        End Try

        Try
            DD_Priorita.SelectedValue =MyFilteredList.Item(0).Priorita
        Catch ex As Exception

        End Try


        Dim LeggiParametri As New Cls_Amb_Parametri

        LeggiParametri.LeggiParametri(Session("DC_TABELLE"))


        If MyFilteredList.Item(0).MastroExtra =0 Then
                Dim Pdc As New Cls_Pianodeiconti

                Pdc.Mastro = LeggiParametri.MastroRicavo
                Pdc.Conto = LeggiParametri.ContoRicavo
                Pdc.Sottoconto = LeggiParametri.SottoContoRicavo
                Pdc.Decodfica(Session("DC_GENERALE"))

                TxtSottocontoCR.Text = Pdc.Mastro & " " & Pdc.Conto & " " & Pdc.Sottoconto & " " & Pdc.Descrizione
          Else
            Try
                Dim Pdc As New Cls_Pianodeiconti

                Pdc.Mastro = MyFilteredList.Item(0).MastroExtra
                Pdc.Conto = MyFilteredList.Item(0).ContoExtra
                Pdc.Sottoconto = MyFilteredList.Item(0).SottoContoExtra
                Pdc.Decodfica(Session("DC_GENERALE"))

                 TxtSottocontoCR.Text = Pdc.Mastro & " " & Pdc.Conto & " " & Pdc.Sottoconto & " " & Pdc.Descrizione
            Catch ex As Exception

            End Try
        End If
        

        If MyFilteredList.Item(0).DareAvere ="D" Then
            RBDareCR.Checked =True
            RBAvereCR.Checked =False
        Else            
            RBDareCR.Checked =False
            RBAvereCR.Checked =True
        End If


        If Trim(MyFilteredList.Item(0).CodiceIVA) ="" then
            DDIvaCr.SelectedValue = LeggiParametri.CodiceIVA
        Else
            DDIvaCr.SelectedValue = MyFilteredList.Item(0).CodiceIVA
        End if

        TxtImportoCR.Text = Format(MyFilteredList.Item(0).Importo, "#,##0.00")

        TxtDescrizioneCr.Text = MyFilteredList.Item(0).Descrizione
            

        Try

            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()


            
            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)
                
            DD_VISITA1.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA1.DataBind()
            DD_VISITA1.SelectedValue = 0

                                

            If MyFilteredList.Item(0).Prestazioni.count > 0 Then
                If val(MyFilteredList.Item(0).Prestazioni.Item(0).Codice_Prestazione) > 0 Then
                    lbl_Visita1.Text=""
                    DD_VISITA1.SelectedValue = MyFilteredList.Item(0).Prestazioni.Item(0).Codice_Prestazione
                Else
                    DD_VISITA1.SelectedValue =""
                    lbl_Visita1.Text=MyFilteredList.Item(0).Prestazioni.Item(0).Descrizione
                End If                    
            Else
                DD_VISITA1.SelectedValue =""                    
            End If


            If  MyFilteredList.Item(0).Prestazioni.Item(0).ConsensoFirmato =True Then
                Chk_ConsensoInformato1.Checked =True
            Else
                Chk_ConsensoInformato1.Checked =false
            End If
        Catch ex As Exception

        End Try
        Try

            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)


            DD_VISITA2.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA2.DataBind()            
            DD_VISITA2.SelectedValue = 0


            If MyFilteredList.Item(0).Prestazioni.count > 1 Then
                If val(MyFilteredList.Item(0).Prestazioni.Item(1).Codice_Prestazione) > 0 Then
                    lbl_Visita2.Text=""
                    DD_VISITA2.SelectedValue = MyFilteredList.Item(0).Prestazioni.Item(1).Codice_Prestazione
                Else
                    If MyFilteredList.Item(0).Prestazioni.Item(1).Descrizione <> "Non Indicato" Then
                        DD_VISITA2.SelectedValue =""
                        lbl_Visita2.Text=MyFilteredList.Item(0).Prestazioni.Item(1).Descrizione
                    End If
                End If                    
            Else
                DD_VISITA2.SelectedValue =""                    
            End If 

            If  MyFilteredList.Item(0).Prestazioni.Item(1).ConsensoFirmato =True Then
                Chk_ConsensoInformato2.Checked =True
            Else
                Chk_ConsensoInformato2.Checked =false
            End If
        Catch ex As Exception

        End Try
        Try

            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)


            DD_VISITA3.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA3.DataBind()            
            DD_VISITA3.SelectedValue = 0

            If MyFilteredList.Item(0).Prestazioni.count > 2 Then
                If val(MyFilteredList.Item(0).Prestazioni.Item(2).Codice_Prestazione) > 0 Then
                    lbl_Visita3.Text=""
                    DD_VISITA3.SelectedValue = MyFilteredList.Item(0).Prestazioni.Item(2).Codice_Prestazione
                Else
                    If MyFilteredList.Item(0).Prestazioni.Item(2).Descrizione <> "Non Indicato" Then
                        DD_VISITA3.SelectedValue =""
                        lbl_Visita3.Text=MyFilteredList.Item(0).Prestazioni.Item(2).Descrizione
                    End If
                End If                    
            Else
                DD_VISITA3.SelectedValue =""                    
            End If     

            If  MyFilteredList.Item(0).Prestazioni.Item(2).ConsensoFirmato =True Then
                Chk_ConsensoInformato2.Checked =True
            Else
                Chk_ConsensoInformato2.Checked =false
            End If
        Catch ex As Exception

        End Try
            
        Try

            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)


            DD_VISITA4.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA4.DataBind()
            DD_VISITA4.SelectedValue = 0

            If MyFilteredList.Item(0).Prestazioni.count > 3 Then
                If val(MyFilteredList.Item(0).Prestazioni.Item(3).Codice_Prestazione) > 0 Then
                    lbl_Visita4.Text=""
                    DD_VISITA4.SelectedValue = MyFilteredList.Item(0).Prestazioni.Item(3).Codice_Prestazione
                Else
                    If MyFilteredList.Item(0).Prestazioni.Item(3).Descrizione <> "Non Indicato" Then
                        DD_VISITA4.SelectedValue =""
                        lbl_Visita4.Text=MyFilteredList.Item(0).Prestazioni.Item(3).Descrizione
                    End If
                End If                    
            Else
                DD_VISITA4.SelectedValue =""                    
            End If     

            If  MyFilteredList.Item(0).Prestazioni.Item(3).ConsensoFirmato =True Then
                Chk_ConsensoInformato3.Checked =True
            Else
                Chk_ConsensoInformato3.Checked =false
            End If
        Catch ex As Exception

        End Try

       Try

            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)


            DD_VISITA5.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA5.DataBind()            
            DD_VISITA5.SelectedValue = 0

            If MyFilteredList.Item(0).Prestazioni.count > 4 Then
                If val(MyFilteredList.Item(0).Prestazioni.Item(4).Codice_Prestazione) > 0 Then
                    lbl_Visita5.Text=""
                    DD_VISITA5.SelectedValue = MyFilteredList.Item(0).Prestazioni.Item(4).Codice_Prestazione
                Else
                    If MyFilteredList.Item(0).Prestazioni.Item(4).Descrizione <> "Non Indicato" Then
                        DD_VISITA5.SelectedValue =""
                        lbl_Visita5.Text=MyFilteredList.Item(0).Prestazioni.Item(4).Descrizione
                    End if 
                End If                    
            Else
                DD_VISITA5.SelectedValue =""                    
            End If     


            If  MyFilteredList.Item(0).Prestazioni.Item(4).ConsensoFirmato =True Then
                Chk_ConsensoInformato4.Checked =True
            Else
                Chk_ConsensoInformato4.Checked =false
            End If
        Catch ex As Exception

        End Try

        Dim CodiceOperazioneApp As Integer

        Try
            CodiceOperazioneApp= MyFilteredList.Item(0).CodiceOperazione
        Catch ex As Exception

        End Try

        if LeggiFattura(CodiceOperazioneApp) > 0 Then
            

            Txt_Cognome.Enabled=False   
            Dd_Operatore.Enabled=False   
            Txt_Nome.Enabled=False   
            Txt_CodiceFiscale.Enabled=False   
            DD_Sesso.Enabled=False
            Txt_DataNascita.Enabled =False
            Txt_Telefono.Enabled =False
            Txt_Indirizzo.Enabled =False
            Txt_Cap.Enabled =False
            Txt_Comune.Enabled =False
            Txt_Data.Enabled =False
            TXt_Ora.Enabled =False  
            Txt_CodiceOperatore.Enabled =False
            DD_Provenienza.Enabled =False
            DD_TipoContatto.Enabled= False
            

            Txt_CodiceImpegnativa.Enabled =False 
            Txt_Testo.Enabled =False

            DD_CONTRATTO.Enabled=False
            DD_Priorita.Enabled=False
            DD_VISITA1.Enabled =False
            DD_VISITA2.Enabled =False
            DD_VISITA3.Enabled =False
            DD_VISITA4.Enabled =False            
            DD_VISITA5.Enabled =False  


            TxtImportoCR.Enabled =False
            DDIvaCr.Enabled =False
            TxtDescrizioneCr.Enabled =False
            TxtSottocontoCR.Enabled =False
            RBAvereCR.Enabled =False    
            RBDareCR.Enabled =False
        End If


    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtSottocontoIVA')!= null) || (appoggio.match('TxtSottocontoCR')!= null) || (appoggio.match('TxtSottocontoControPartitaCR')!= null) || (appoggio.match('TxtSottocontoDareRT')!= null) || (appoggio.match('TxtSottocontoAvereRT')!= null)) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ( (appoggio.match('TxtImpostaIVA')!= null) || (appoggio.match('TxtImportoCR')!= null) || (appoggio.match('TxtQuantitaCR')!= null) || (appoggio.match('TxtImponibileRT')!= null) || (appoggio.match('TxtRitenutaRT')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap);  });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$("".chosen-select-large"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""50%"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click
       Response.Redirect("Appuntamenti_Elenco.aspx")
    End Sub

    Private Sub Img_Modifica_Click(sender As Object, e As EventArgs) Handles Img_Modifica.Click
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("ID_Operatore")


        If Txt_Data.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If not isdate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If TXt_Ora.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_CONTRATTO.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare tipo contratto');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If DD_Priorita.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare priorità');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Dim CodiceFiscale As New Cls_CodiceFiscale

        
        If not CodiceFiscale.Check_CodiceFiscale(Txt_CodiceFiscale.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Fiscale formalmente errato');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Dd_Operatore.SelectedValue.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Operatore non indicato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Cognome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Cognome');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Nome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Nome');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Telefono.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il numero Telefono');", True)
            Call EseguiJS()
            Exit Sub
        End If


        
        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)
        Dim SalvaAppuntamento as new Cls_DettaglioAppuntamentoSave

        
        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))


        
        Dim Indice As Integer

        
        For Indice =0 To AppuntamentiFilePDF.Count-1
            If AppuntamentiFilePDF.Item(Indice).CodiceOperazione = CodiceOperazione Then
                 Exit For
            End If
        Next

                
        
        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Cognome= Txt_Cognome.Text 
        Catch ex As Exception

        End Try
            
            
        Try
            AppuntamentiFilePDF.Item(Indice).OperatoreMedico =Dd_Operatore.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Nome =Txt_Nome.Text 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.CodiceFiscale = Txt_CodiceFiscale.Text
        Catch ex As Exception

        End Try
                                  

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Sesso = DD_Sesso.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.DataNascita =Txt_DataNascita.Text 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Telefono1 = Txt_Telefono.Text
        Catch ex As Exception

        End Try

        Try
                AppuntamentiFilePDF.Item(Indice).Utente.Indirizzo = Txt_Indirizzo.Text
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).Utente.CAP =Txt_cap.Text
        Catch ex As Exception

        End Try


        Try
             AppuntamentiFilePDF.Item(Indice).Utente.IDFasciaIsee = DD_FASCIAISE.SelectedValue 
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).Utente.Comune =Txt_Comune.Text 
        Catch ex As Exception

        End Try
                        

        Try
            AppuntamentiFilePDF.Item(Indice).Data =Txt_Data.Text  
        Catch ex As Exception

        End Try
            
        Try
            Dim TempOre As String
            Dim TempMinuti As String

            TempOre = Mid(TXt_Ora.Text,1,2)
            TempMinuti= Mid(TXt_Ora.Text,4,2)


            AppuntamentiFilePDF.Item(Indice).Ora = TimeSerial(TempOre,TempMinuti,0)
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Operatore = Txt_CodiceOperatore.Text 
        Catch ex As Exception

        End Try
                              

        Try
            AppuntamentiFilePDF.Item(Indice).Provenienza = DD_Provenienza.SelectedValue 
        Catch ex As Exception

        End Try

        Try
                AppuntamentiFilePDF.Item(Indice).CodiceImpegnativa =Txt_CodiceImpegnativa.Text 
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).TestoQuesito =Txt_Testo.Text
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).TipoContatto  =DD_TipoContatto.SelectedValue 
        Catch ex As Exception

        End Try
        

        Try
            AppuntamentiFilePDF.Item(Indice).Contratto  =DD_CONTRATTO.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Priorita =DD_Priorita.SelectedValue
        Catch ex As Exception

        End Try



        Try             
             Dim Vettore(100) As String

             Vettore = SplitWords(TxtSottocontoCR.Text)

             If Vettore.Length > 2 Then
                AppuntamentiFilePDF.Item(Indice).MastroExtra = Vettore(0)
                AppuntamentiFilePDF.Item(Indice).ContoExtra = Vettore(1)
                AppuntamentiFilePDF.Item(Indice).SottoContoExtra = Vettore(2)
             End If            
        Catch ex As Exception

        End Try


        Try
            AppuntamentiFilePDF.Item(Indice).Importo = TxtImportoCR.Text
        Catch ex As Exception

        End Try
        
        Try
            AppuntamentiFilePDF.Item(Indice).Descrizione = TxtDescrizioneCr.Text
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).CodiceIVA = DDIvaCr.SelectedValue
        Catch ex As Exception

        End Try

        Try
            If RBAvereCR.Checked =True Then
                AppuntamentiFilePDF.Item(Indice).DareAvere = "A"
            Else
                AppuntamentiFilePDF.Item(Indice).DareAvere = "D"
            End if
        Catch ex As Exception

        End Try



        Try
            If AppuntamentiFilePDF.Item(Indice).Prestazioni.Count =0 Then
                Dim Prestazione As New Prestazione

                Prestazione.Descrizione =DD_VISITA1.SelectedItem.Text
                Prestazione.Codice_Prestazione = DD_VISITA1.SelectedItem.Value

                AppuntamentiFilePDF.Item(Indice).Prestazioni.Add(Prestazione)
            Else
               AppuntamentiFilePDF.Item(Indice).Prestazioni(0).Descrizione = DD_VISITA1.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(0).Codice_Prestazione= DD_VISITA1.SelectedItem.Value
            End If

            If Chk_ConsensoInformato1.Checked = True Then 
                AppuntamentiFilePDF.Item(Indice).Prestazioni(0).ConsensoFirmato = True
            Else
                AppuntamentiFilePDF.Item(Indice).Prestazioni(0).ConsensoFirmato = False    
            End If
        Catch ex As Exception

        End Try
        Try            
            If AppuntamentiFilePDF.Item(Indice).Prestazioni.Count < 2 Then
                Dim Prestazione As New Prestazione

                Prestazione.Descrizione =DD_VISITA2.SelectedItem.Text
                Prestazione.Codice_Prestazione = DD_VISITA2.SelectedItem.Value

                AppuntamentiFilePDF.Item(Indice).Prestazioni.Add(Prestazione)
            Else
               AppuntamentiFilePDF.Item(Indice).Prestazioni(1).Descrizione = DD_VISITA2.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(1).Codice_Prestazione= DD_VISITA2.SelectedItem.Value
            End If

            If Chk_ConsensoInformato2.Checked = True Then 
                AppuntamentiFilePDF.Item(Indice).Prestazioni(1).ConsensoFirmato = True
            Else
                AppuntamentiFilePDF.Item(Indice).Prestazioni(1).ConsensoFirmato = False    
            End If
        Catch ex As Exception

        End Try
        Try            
            If AppuntamentiFilePDF.Item(Indice).Prestazioni.Count < 3 Then
                Dim Prestazione As New Prestazione

                Prestazione.Descrizione =DD_VISITA3.SelectedItem.Text
                Prestazione.Codice_Prestazione = DD_VISITA3.SelectedItem.Value

                AppuntamentiFilePDF.Item(Indice).Prestazioni.Add(Prestazione)
            Else
               AppuntamentiFilePDF.Item(Indice).Prestazioni(2).Descrizione = DD_VISITA3.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(2).Codice_Prestazione= DD_VISITA3.SelectedItem.Value
            End If

            
            If Chk_ConsensoInformato3.Checked = True Then 
                AppuntamentiFilePDF.Item(Indice).Prestazioni(2).ConsensoFirmato = True
            Else
                AppuntamentiFilePDF.Item(Indice).Prestazioni(2).ConsensoFirmato = False    
            End If
        Catch ex As Exception

        End Try

        Try            
            If AppuntamentiFilePDF.Item(Indice).Prestazioni.Count < 4 Then
                Dim Prestazione As New Prestazione

                Prestazione.Descrizione =DD_VISITA4.SelectedItem.Text
                Prestazione.Codice_Prestazione = DD_VISITA4.SelectedItem.Value

                AppuntamentiFilePDF.Item(Indice).Prestazioni.Add(Prestazione)
            Else
               AppuntamentiFilePDF.Item(Indice).Prestazioni(3).Descrizione = DD_VISITA4.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(3).Codice_Prestazione= DD_VISITA4.SelectedItem.Value
            End If

            
            If Chk_ConsensoInformato4.Checked = True Then 
                AppuntamentiFilePDF.Item(Indice).Prestazioni(3).ConsensoFirmato = True
            Else
                AppuntamentiFilePDF.Item(Indice).Prestazioni(3).ConsensoFirmato = False    
            End If
        Catch ex As Exception

        End Try

        Try            
            If AppuntamentiFilePDF.Item(Indice).Prestazioni.Count < 5 Then
                Dim Prestazione As New Prestazione

                Prestazione.Descrizione =DD_VISITA5.SelectedItem.Text
                Prestazione.Codice_Prestazione = DD_VISITA5.SelectedItem.Value

                AppuntamentiFilePDF.Item(Indice).Prestazioni.Add(Prestazione)
            Else
               AppuntamentiFilePDF.Item(Indice).Prestazioni(4).Descrizione = DD_VISITA5.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(4).Codice_Prestazione= DD_VISITA5.SelectedItem.Value
            End If

            
            If Chk_ConsensoInformato5.Checked = True Then 
                AppuntamentiFilePDF.Item(Indice).Prestazioni(4).ConsensoFirmato = True
            Else
                AppuntamentiFilePDF.Item(Indice).Prestazioni(4).ConsensoFirmato = False    
            End If
        Catch ex As Exception

        End Try
        
        if CreateGuest() = -1 Then
            Exit Sub
        End If


        
        SalvaAppuntamento.ScriviDB(Session("DC_TABELLE"),AppuntamentiFilePDF.Item(Indice),Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page)

        Session("AppuntamentiFilePDF") = AppuntamentiFilePDF 

        


        

        Response.Redirect("EmissioneFattura.aspx?CodiceOperazione=" & CodiceOperazione)
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Function IdEpersonamOperatore(ByVal CodiceOperatore As Integer)
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        IdEpersonamOperatore=0

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From AnagraficaComune where CODICEDEBITORECREDITORE  = ? "
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CODICEDEBITORECREDITORE ", CodiceOperatore)
                

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read then
            IdEpersonamOperatore = campodbn(myPOSTreader.Item("IdEpersonam"))
        End if
        myPOSTreader.Close()
        cn.Close()

    End Function

    Private Function CreateGuest() As Integer
        'Dim ePersonamManager As New ePersonamBiz(Session("EPersonamUser"), Session("EPersonamPSW"), 3611)
        
        Dim Parametri As New  Cls_Amb_Parametri

        Parametri.LeggiParametri(Session("DC_TABELLE"))
      
        Dim ePersonamManager As New ePersonamBiz("super", "Advenias202002",  IdEpersonamOperatore(Dd_Operatore.SelectedValue),Parametri.IdEpersonamStruttura)
        Dim MyCliente As New Cliente With {
            .DataNascita = Txt_DataNascita.Text,
            .CodiceFiscale = Txt_CodiceFiscale.Text,
            .CAP = Txt_Cap.Text,
            .Comune = Txt_Comune.Text,
            .Indirizzo = Txt_Indirizzo.Text,
            .Nome = Txt_Nome.Text,
            .Cognome = Txt_Cognome.Text,
            .Sesso = DD_Sesso.SelectedValue,
            .Telefono1 = Txt_Telefono.Text}

        Try
            ePersonamManager.CreateGuest(MyCliente, Txt_Data.Text)
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore dialogo con ePersonam');", True)
            Call EseguiJS()
            Return -1
            Exit Function
        End Try
        


        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)
        
        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))



        Dim Indice As Integer

        
        For Indice =0 To AppuntamentiFilePDF.Count-1
            If AppuntamentiFilePDF.Item(Indice).CodiceOperazione =  Request.Item("ID_Operatore") Then
                 Exit For
            End If
        Next

        'Try
            ePersonamManager.CreateEsami(AppuntamentiFilePDF.Item(Indice))
        'Catch ex As Exception
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore dialogo con ePersonam');", True)
        '    Call EseguiJS()
        '    Return -1
        '    Exit Function
        'End Try
        
    End Function

End Class