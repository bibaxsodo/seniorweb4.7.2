﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports iTextSharp.text.pdf.codec

Public Class NuovoAppuntamentoManuale
    Inherits System.Web.UI.Page
    Private Function NuovoCodiceManuale()  As Integer
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        
        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select max(CodiceOperazione) as Totale From Amb_Appuntamento "
        cmd.Connection = cn
        
        
        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read then
            NuovoCodiceManuale = campodbn(myPOSTreader.Item("Totale"))
        End if
        myPOSTreader.Close()
        cn.Close()


        If NuovoCodiceManuale < 1000000000 Then
            NuovoCodiceManuale = 1000000000 
        End If
    End Function


    
     Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
   Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
   Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         Dim Barra As New Cls_BarraSenior
         Dim K1 As New Cls_SqlString

         If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
         End If

         Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        EseguiJS()

         If Page.IsPostBack = True Then Exit Sub

        Dim OperatoreManager As New  OperatoreBiz(Session("DC_OSPITE"))

        Dd_Operatore.DataSource = OperatoreManager.List_For_DropDown()
        Dd_Operatore.DataBind


        

        Dim FascieISEManager As New Tabella_FasciaISEBiz(Session("DC_TABELLE"))
        Dim FasciaISE As New  Tabella_FasciaISE
        Dim ListaFasciaISE As New Tabella_FasciaISE_List

        ListaFasciaISE  =FascieISEManager.List_For_DropDown()

        FasciaISE.Descrizione ="Non Indicato"
        FasciaISE.ID_FasciaISEE =0

        ListaFasciaISE.Add(FasciaISE)

        DD_FASCIAISE.DataSource = ListaFasciaISE
        DD_FASCIAISE.Items.Add("")
        DD_FASCIAISE.DataBind
        




        Try
            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

            Dim ApPrestazione_Tipologia As New   Prestazione_Tipologia


            ApPrestazione_Tipologia.Descrizione ="Non Indicato"
            ApPrestazione_Tipologia.ID_Prestazione_Tipologia =0

            MyListaPrestazione_Tipologia.Add(ApPrestazione_Tipologia)
            

            DD_VISITA1.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA1.DataBind()
            DD_VISITA1.SelectedValue = 0


            
            DD_VISITA2.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA2.DataBind()
            DD_VISITA2.SelectedValue = 0


            
            DD_VISITA3.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA3.DataBind()
            DD_VISITA3.SelectedValue = 0

            
            DD_VISITA4.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA4.DataBind()
            DD_VISITA4.SelectedValue = 0


            
            DD_VISITA5.DataSource = MyListaPrestazione_Tipologia 
            DD_VISITA5.DataBind()
            DD_VISITA5.SelectedValue = 0

        Catch ex As Exception

        End Try        
        
    End Sub
     Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('TXt_Ora')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99:99"");"        
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) ) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "$("".chosen-select-large"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""70%"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub Img_Modifica_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Modifica.Click
        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)
        Dim SalvaAppuntamento as New Cls_DettaglioAppuntamentoSave





        If Txt_Data.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If not isdate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If TXt_Ora.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_CONTRATTO.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare tipo contratto');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If DD_Priorita.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare priorità');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If Dd_Operatore.SelectedValue.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Operatore non indicato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Cognome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Cognome');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Nome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Nome');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Txt_DataNascita.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare la data nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If not isdate(Txt_DataNascita.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento la data nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim CodiceFiscale As New Cls_CodiceFiscale

        
        If not CodiceFiscale.Check_CodiceFiscale(Txt_CodiceFiscale.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Fiscale formalmente errato');", True)
            Call EseguiJS()
            Exit Sub
        End If



        If Txt_Telefono.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il numero Telefono');", True)
            Call EseguiJS()
            Exit Sub
        End If




        If DD_VISITA1.SelectedValue ="" And lbl_Visita1.Visible = True And lbl_Visita1.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If DD_VISITA2.SelectedValue ="" And lbl_Visita2.Visible = True And lbl_Visita2.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_VISITA3.SelectedValue ="" And lbl_Visita3.Visible = True And lbl_Visita3.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If DD_VISITA4.SelectedValue ="" And lbl_Visita4.Visible = True And lbl_Visita4.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If
        

        If DD_VISITA5.SelectedValue ="" And lbl_Visita5.Visible = True And lbl_Visita5.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If not IsDate(Txt_DataNascita.Text) then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare data di nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        
        Dim NuovoAppuntamento As New  DettaglioAppuntamento

        Dim Indice As  Integer = VAL(Request.Item("NUMERO")) -1


        NuovoAppuntamento.Utente = New Cliente

        NuovoAppuntamento.CodiceOperazione = NuovoCodiceManuale()


        Try
            NuovoAppuntamento.Utente.Cognome= Txt_Cognome.Text 
        Catch ex As Exception

        End Try
            
            
        Try
            NuovoAppuntamento.OperatoreMedico =Dd_Operatore.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Utente.Nome =Txt_Nome.Text 
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Utente.CodiceFiscale = Txt_CodiceFiscale.Text
        Catch ex As Exception

        End Try
                                  

        Try
            NuovoAppuntamento.Utente.Sesso = DD_Sesso.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Utente.DataNascita =Txt_DataNascita.Text 
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Utente.Telefono1 = Txt_Telefono.Text
        Catch ex As Exception

        End Try

        Try
                NuovoAppuntamento.Utente.Indirizzo = Txt_Indirizzo.Text
        Catch ex As Exception

        End Try

            
        Try
                NuovoAppuntamento.Utente.CAP =Txt_cap.Text
        Catch ex As Exception

        End Try

            
        Try
                NuovoAppuntamento.Utente.Comune =Txt_Comune.Text 
        Catch ex As Exception

        End Try
                        

        Try
            NuovoAppuntamento.Data =Txt_Data.Text  
        Catch ex As Exception

        End Try
            
        Try
            Dim TempOre As String
            Dim TempMinuti As String

            TempOre = Mid(TXt_Ora.Text,1,2)
            TempMinuti= Mid(TXt_Ora.Text,4,2)


            NuovoAppuntamento.Ora = TimeSerial(TempOre,TempMinuti,0)
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Operatore = Txt_CodiceOperatore.Text 
        Catch ex As Exception

        End Try
                              

        Try
            NuovoAppuntamento.Provenienza = DD_Provenienza.SelectedValue 
        Catch ex As Exception

        End Try

        Try
                NuovoAppuntamento.CodiceImpegnativa =Txt_CodiceImpegnativa.Text 
        Catch ex As Exception

        End Try

            
        Try
                NuovoAppuntamento.TestoQuesito =Txt_Testo.Text
        Catch ex As Exception

        End Try


        Try
             NuovoAppuntamento.DataDomanda= Txt_Data.Text
        Catch ex As Exception

        End Try


        Try
             NuovoAppuntamento.TipoContatto = DD_TIPOCONTATO.SelectedValue
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Contratto  =DD_CONTRATTO.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            NuovoAppuntamento.Priorita =DD_Priorita.SelectedValue
        Catch ex As Exception

        End Try
            

        Try
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
            Dim ModificaTipologia As New Prestazione_Tipologia
            
            If lbl_Visita1.Text <>"" THEN
                ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA1.SelectedItem.Value)
                ModificaTipologia.Chiave_Import=  lbl_Visita1.Text                       
                Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
            End If

            NuovoAppuntamento.Prestazioni(0).Descrizione = DD_VISITA1.SelectedItem.Text
            NuovoAppuntamento.Prestazioni(0).Codice_Prestazione = DD_VISITA1.SelectedItem.Value
        Catch ex As Exception

        End Try

        If DD_VISITA2.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita2.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA2.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita2.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

                NuovoAppuntamento.Prestazioni(1).Descrizione = DD_VISITA2.SelectedItem.Text
                NuovoAppuntamento.Prestazioni(1).Codice_Prestazione = DD_VISITA2.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End If
        If DD_VISITA3.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita3.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA3.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita3.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

               NuovoAppuntamento.Prestazioni(2).Descrizione = DD_VISITA3.SelectedItem.Text
               NuovoAppuntamento.Prestazioni(2).Codice_Prestazione = DD_VISITA3.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End If

        If DD_VISITA4.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita4.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA4.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita4.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

               NuovoAppuntamento.Prestazioni(3).Descrizione = DD_VISITA4.SelectedItem.Text
               NuovoAppuntamento.Prestazioni(3).Codice_Prestazione = DD_VISITA4.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End if

        SalvaAppuntamento.ScriviDB(Session("DC_TABELLE"),NuovoAppuntamento,Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page)

        Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub

    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub
End Class