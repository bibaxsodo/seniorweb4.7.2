﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EmissioneFattura.aspx.vb" Inherits="EmissioneFattura" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xsau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ambulatoriale - Fatturazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=1" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <style>
        th {
            font-weight: normal;
        }

        .tabella {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        .miotr {
            background: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        .miacella {
            padding: .625em;
            text-align: center;
        }

        .miaintestazione {
            padding: .625em;
            text-align: center;
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }
                .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 425px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
        .cornicegrigia2 {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 125px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });

        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="36000" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Ambulatoriale - Fatturazione<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Img_Elimina" runat="server" Height="38px" ImageUrl="~/images/cancella.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Img_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <a href="Appuntamenti_Elenco.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto">
                        </a>
                        <asp:ImageButton ID="ImgMenu" ImageUrl="../images/Menu_Indietro.png" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />                        
                        <br />
                        <br />
                        <br />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xsau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xsau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Fatturazione
                                </HeaderTemplate>
                                <ContentTemplate>
                                  <div class="Row">
                                      <div class="Column">
                                        <div class="cornicegrigia">
                                            <br />
                                            Dati Appuntamento:
                                            <asp:label id="Lbl_DatiAppuntamenti"  runat="server" Text=""  ></asp:label>    
                                            <br />                                            
                                            <label class="LabelCampo"><asp:label id="Lbl_TotaleFattura"  runat="server" Text="Totale Fattura :"  ></asp:label></label>
                                            <asp:label id="TxtImportoTotale"  runat="server" Text="0"  ></asp:label>
                                            <br />              
                                            <br />              
                                            <label class="LabelCampo"><asp:label id="Lbl_DataFattura"  runat="server" Text="Data Fattura :"  ></asp:label></label>
                                            <asp:TextBox ID="Txt_DataFattura" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox><br />                                            
                                            <br />
                                            <label class="LabelCampo"><asp:label id="Lbl_Causale"  runat="server" Text="Causale :"  ></asp:label></label>
                                            <asp:DropDownList ID="DD_CausaleFattura" runat="server" Style="border-color: Red;"></asp:DropDownList><br />                                            
                                            <br />
                                            <br />
                                            <br />
                                            <center>
                                            <asp:label id="Lbl_DatiFattura"  runat="server" Text=""  ></asp:label>
                                            </center>
                                        </div>
                                       </div>
                                    <div class="Column">
                                    <div class="cornicegrigia">
                                           <table>
                                                <tr>
                                                    <td>
                                                        Data Incasso
                                                    </td>
                                                    <td>
                                                        Causale
                                                    </td>
                                                    <td>
                                                        Importo
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>                                                    
                                                    <td>
                                                        <asp:TextBox ID="Txt_DataIncasso" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DD_Causale" runat="server" ></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Importo"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  autocomplete="off" Width="100px" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button runat="server" ID="Btn_Add" Text="+" />
                                                    </td>
                                                </tr>
                                                 <tr id="riga2" style="display:none,">                                                    
                                                    <td>
                                                        <asp:TextBox ID="Txt_DataIncasso1" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DD_Causale1" runat="server" ></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Importo1"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  autocomplete="off" Width="100px" runat="server"></asp:TextBox>
                                                    </td>
                                                     <td>
                                                        <asp:Button runat="server" ID="Btn_Add1" Text="+" />
                                                    </td>
                                                </tr>
                                               <tr id="riga2" style="display:none,">                                                    
	                                                <td>
		                                                <asp:TextBox ID="Txt_DataIncasso2" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox>
	                                                </td>
	                                                <td>
		                                                <asp:DropDownList ID="DD_Causale2" runat="server" ></asp:DropDownList>
	                                                </td>
	                                                <td>
		                                                <asp:TextBox ID="Txt_Importo2"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  autocomplete="off" Width="100px" runat="server"></asp:TextBox>
	                                                </td>
	                                                 <td>
		                                                <asp:Button runat="server" ID="Btn_Add2" Text="+" />
	                                                </td>
                                                </tr>
                                                <tr id="riga2" style="display:none,">                                                    
	                                                <td>
		                                                <asp:TextBox ID="Txt_DataIncasso3" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox>
	                                                </td>
	                                                <td>
		                                                <asp:DropDownList ID="DD_Causale3" runat="server" ></asp:DropDownList>
	                                                </td>
	                                                <td>
		                                                <asp:TextBox ID="Txt_Importo3"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  autocomplete="off" Width="100px" runat="server"></asp:TextBox>
	                                                </td>
	                                                 <td>
		                                                <asp:Button runat="server" ID="Btn_Add3" Text="+" />
	                                                </td>
                                                </tr>
                                                <tr id="riga2" style="display:none,">                                                    
	                                                <td>
		                                                <asp:TextBox ID="Txt_DataIncasso4" runat="server" Width="100px" Style="border-color: Red;"></asp:TextBox>
	                                                </td>
	                                                <td>
		                                                <asp:DropDownList ID="DD_Causale4" runat="server" ></asp:DropDownList>
	                                                </td>
	                                                <td>
		                                                <asp:TextBox ID="Txt_Importo4"  style="text-align:right;"   onkeypress="return handleEnter(this, event)"  autocomplete="off" Width="100px" runat="server"></asp:TextBox>
	                                                </td>
	                                                 <td>
		                                                <asp:Button runat="server" ID="Btn_Add4" Text="+" />
	                                                </td>
                                                </tr>
                                            </table>
                                    </div>
                                    </div>
                                      
                                    </div>
                                    <div class="Row">
                                    <div class="Column">
                                        <div class="cornicegrigia2">
                                            Note Fattura :
                                            <asp:TextBox ID="Txt_Descrizione" runat="server" Width="95%"  TextMode="MultiLine"  Height="100px" ></asp:TextBox>
                                        </div>                                      
                                    </div>                                      
                                    </div>
                                    <asp:Label ID="Lbl_Errori" runat="server" Text=""></asp:Label>


                                </ContentTemplate>
                            </xsau:TabPanel>
                        </xsau:TabContainer>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="GridExcel" runat="server">
            </asp:GridView>
        </div>
    </form>

</body>
</html>
