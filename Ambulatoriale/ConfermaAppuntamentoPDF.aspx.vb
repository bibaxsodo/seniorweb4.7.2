﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports iTextSharp.text.pdf.codec

Public Class ConfermaAppuntamentoPDF
    Inherits System.Web.UI.Page

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
         

         Dim Barra As New Cls_BarraSenior
         Dim K1 As New Cls_SqlString

         If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
         End If

         Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        EseguiJS()

         If Page.IsPostBack = True Then Exit Sub

        Dim OperatoreManager As New  OperatoreBiz(Session("DC_OSPITE"))

        Dd_Operatore.DataSource = OperatoreManager.List_For_DropDown()
        Dd_Operatore.DataBind

        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)

        if VAL(Request.Item("NUMERO")) >  0 then            
            AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))
            
            Dim Indice As  Integer = VAL(Request.Item("NUMERO")) -1

            Try
                Txt_CodiceOperazione.Text = AppuntamentiFilePDF.Item(Indice).CodiceOperazione
            Catch ex As Exception

            End Try

            Try
                Txt_Cognome.Text = AppuntamentiFilePDF.Item(Indice).Utente.Cognome.Trim
            Catch ex As Exception

            End Try
            
            
            Try
                Dd_Operatore.SelectedValue = AppuntamentiFilePDF.Item(Indice).OperatoreMedico
            Catch ex As Exception

            End Try

            Try
                Txt_Nome.Text = AppuntamentiFilePDF.Item(Indice).Utente.Nome.Trim
            Catch ex As Exception

            End Try

            Try
                Txt_CodiceFiscale.Text =AppuntamentiFilePDF.Item(Indice).Utente.CodiceFiscale.Trim
            Catch ex As Exception

            End Try
                                  

            Try
                DD_Sesso.SelectedValue = AppuntamentiFilePDF.Item(Indice).Utente.Sesso
            Catch ex As Exception

            End Try

            Try
                Txt_DataNascita.Text =format(AppuntamentiFilePDF.Item(Indice).Utente.DataNascita,"dd/MM/yyyy")
            Catch ex As Exception

            End Try

            Try
                Txt_Telefono.Text = AppuntamentiFilePDF.Item(Indice).Utente.Telefono1.Trim
            Catch ex As Exception

            End Try

            Try
                 Txt_Indirizzo.Text = AppuntamentiFilePDF.Item(Indice).Utente.Indirizzo.Trim
            Catch ex As Exception

            End Try

            
            Try
                 Txt_cap.Text = AppuntamentiFilePDF.Item(Indice).Utente.CAP.Trim
            Catch ex As Exception

            End Try

            
            Try
                 lbl_ComuneLetto.Text =AppuntamentiFilePDF.Item(Indice).Utente.Comune.Trim
                 Dim LeggiComune As New ClsComune

                 LeggiComune.Descrizione = lbl_ComuneLetto.Text
                 LeggiComune.LeggiDescrizione(Session("DC_OSPITE"))

                 Txt_Comune.Text = LeggiComune.Provincia & " " & LeggiComune.Comune & " " &  LeggiComune.Descrizione
            Catch ex As Exception

            End Try
                        

            Try
                Txt_Data.Text  = format(AppuntamentiFilePDF.Item(Indice).Data,"dd/MM/yyyy")
            Catch ex As Exception

            End Try
            
            Try
                TXt_Ora.Text   = format(AppuntamentiFilePDF.Item(Indice).Ora,"HH:mm")
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(AppuntamentiFilePDF.Item(Indice).Operatore) then
                    Txt_CodiceOperatore.Text = AppuntamentiFilePDF.Item(Indice).Operatore
                End if
            Catch ex As Exception

            End Try
            
            Try
                DD_TipoContatto.SelectedValue = AppuntamentiFilePDF.Item(Indice).TipoContatto
            Catch ex As Exception

            End Try
            

            Try
                DD_Provenienza.SelectedValue = AppuntamentiFilePDF.Item(Indice).Provenienza
            Catch ex As Exception

            End Try

            Try
                If not IsNothing(AppuntamentiFilePDF.Item(Indice).CodiceImpegnativa) Then
                    Txt_CodiceImpegnativa.Text = AppuntamentiFilePDF.Item(Indice).CodiceImpegnativa.Trim
                End If                 
            Catch ex As Exception

            End Try

            
            Try
                If Not IsNothing(AppuntamentiFilePDF.Item(Indice).TestoQuesito) then
                    Txt_Testo.Text = AppuntamentiFilePDF.Item(Indice).TestoQuesito.Trim
                End if
            Catch ex As Exception

            End Try

            Try
                If not IsNothing(AppuntamentiFilePDF.Item(Indice).Contratto) then
                    DD_CONTRATTO.SelectedValue= AppuntamentiFilePDF.Item(Indice).Contratto.Trim
                End if
            Catch ex As Exception

            End Try

            Try
                If not IsNothing(AppuntamentiFilePDF.Item(Indice).Priorita) then
                    DD_Priorita.SelectedValue =AppuntamentiFilePDF.Item(Indice).Priorita
                End if
            Catch ex As Exception
                DD_Priorita.SelectedValue =""
            End Try

            

            Try

                Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

                
                DD_VISITA1.DataSource = MyListaPrestazione_Tipologia 
                DD_VISITA1.DataBind()
                DD_VISITA1.Items.Add("")

                                

                If AppuntamentiFilePDF.Item(Indice).Prestazioni.count > 0 Then
                    If AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(0).Codice_Prestazione > 0 Then
                        lbl_Visita1.Text=""
                        DD_VISITA1.SelectedValue = AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(0).Codice_Prestazione
                    Else
                        DD_VISITA1.SelectedValue =""
                        lbl_Visita1.Text=AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(0).Descrizione
                    End If                    
                Else
                    DD_VISITA1.SelectedValue =""                    
                End If
            Catch ex As Exception

            End Try
            Try

                Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

                DD_VISITA2.DataSource = MyListaPrestazione_Tipologia 
                DD_VISITA2.DataBind()
                DD_VISITA2.Items.Add("")


                If AppuntamentiFilePDF.Item(Indice).Prestazioni.count > 1 Then
                    If AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(1).Codice_Prestazione > 0 Then
                        lbl_Visita2.Text=""
                        DD_VISITA2.SelectedValue = AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(1).Codice_Prestazione
                    Else
                        DD_VISITA2.SelectedValue =""
                        lbl_Visita2.Text=AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(1).Descrizione
                    End If                    
                Else
                    DD_VISITA2.SelectedValue =""                    
                End If 
            Catch ex As Exception

            End Try
            Try

                Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

                DD_VISITA3.DataSource = MyListaPrestazione_Tipologia 
                DD_VISITA3.DataBind()
                DD_VISITA3.Items.Add("")

                If AppuntamentiFilePDF.Item(Indice).Prestazioni.count > 2 Then
                    If AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(2).Codice_Prestazione > 0 Then
                        lbl_Visita3.Text=""
                        DD_VISITA3.SelectedValue = AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(2).Codice_Prestazione
                    Else
                        DD_VISITA3.SelectedValue =""
                        lbl_Visita3.Text=AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(2).Descrizione
                    End If                    
                Else
                    DD_VISITA3.SelectedValue =""                    
                End If     
            Catch ex As Exception

            End Try
            
            Try

                Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

                DD_VISITA4.DataSource = MyListaPrestazione_Tipologia 
                DD_VISITA4.DataBind()
                DD_VISITA4.Items.Add("")

                If AppuntamentiFilePDF.Item(Indice).Prestazioni.count > 3 Then
                    If AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(3).Codice_Prestazione > 0 Then
                        lbl_Visita4.Text=""
                        DD_VISITA4.SelectedValue = AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(3).Codice_Prestazione
                    Else
                        DD_VISITA4.SelectedValue =""
                        lbl_Visita4.Text=AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(3).Descrizione
                    End If                    
                Else
                    DD_VISITA4.SelectedValue =""                    
                End If     
            Catch ex As Exception

            End Try

             Try
                Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.List_For_DropDown()

                DD_VISITA5.DataSource = MyListaPrestazione_Tipologia 
                DD_VISITA5.DataBind()
                DD_VISITA5.Items.Add("")

                If AppuntamentiFilePDF.Item(Indice).Prestazioni.count > 4 Then
                    If AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(4).Codice_Prestazione > 0 Then
                        lbl_Visita5.Text=""
                        DD_VISITA5.SelectedValue = AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(4).Codice_Prestazione
                    Else
                        DD_VISITA5.SelectedValue =""
                        lbl_Visita5.Text=AppuntamentiFilePDF.Item(Indice).Prestazioni.Item(4).Descrizione
                    End If                    
                Else
                    DD_VISITA5.SelectedValue =""                    
                End If     
            Catch ex As Exception

            End Try


        End If

        

    
    End Sub

     Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) ) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "$("".chosen-select-large"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""70%"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Sub Modifica()
        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)
        Dim SalvaAppuntamento as New Cls_DettaglioAppuntamentoSave



        If Txt_CodiceOperazione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice operazione non indicato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Data.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If not isdate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If TXt_Ora.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora appuntamento non indicata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_CONTRATTO.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare tipo contratto');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If DD_Priorita.SelectedValue.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare priorità');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If Dd_Operatore.SelectedValue.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Operatore non indicato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Cognome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Cognome');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_Nome.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserire Nome');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Txt_DataNascita.Text.Trim="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare la data nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If not isdate(Txt_DataNascita.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data appuntamento la data nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim CodiceFiscale As New Cls_CodiceFiscale

        
        If not CodiceFiscale.Check_CodiceFiscale(Txt_CodiceFiscale.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Fiscale formalmente errato');", True)
            Call EseguiJS()
            Exit Sub
        End If



        If Txt_Telefono.Text.Trim ="" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il numero Telefono');", True)
            Call EseguiJS()
            Exit Sub
        End If




        If DD_VISITA1.SelectedValue ="" And lbl_Visita1.Visible = True And lbl_Visita1.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If DD_VISITA2.SelectedValue ="" And lbl_Visita2.Visible = True And lbl_Visita2.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_VISITA3.SelectedValue ="" And lbl_Visita3.Visible = True And lbl_Visita3.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If

        
        If DD_VISITA4.SelectedValue ="" And lbl_Visita4.Visible = True And lbl_Visita4.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If
        

        If DD_VISITA5.SelectedValue ="" And lbl_Visita5.Visible = True And lbl_Visita5.Text <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare il tipo visita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If not IsDate(Txt_DataNascita.Text) then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare data di nascita');", True)
            Call EseguiJS()
            Exit Sub
        End If


        
        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))

        Dim Indice As  Integer = VAL(Request.Item("NUMERO")) -1

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Cognome= Txt_Cognome.Text 
        Catch ex As Exception

        End Try
            
            
        Try
            AppuntamentiFilePDF.Item(Indice).OperatoreMedico =Dd_Operatore.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Nome =Txt_Nome.Text 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.CodiceFiscale = Txt_CodiceFiscale.Text
        Catch ex As Exception

        End Try
                                  

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Sesso = DD_Sesso.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.DataNascita =Txt_DataNascita.Text 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Utente.Telefono1 = Txt_Telefono.Text
        Catch ex As Exception

        End Try

        Try
                AppuntamentiFilePDF.Item(Indice).Utente.Indirizzo = Txt_Indirizzo.Text
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).Utente.CAP =Txt_cap.Text
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).Utente.Comune =Txt_Comune.Text 
        Catch ex As Exception

        End Try
                        

        Try
            AppuntamentiFilePDF.Item(Indice).Data =Txt_Data.Text  
        Catch ex As Exception

        End Try
            
        Try
            Dim TempOre As String
            Dim TempMinuti As String

            TempOre = Mid(TXt_Ora.Text,1,2)
            TempMinuti= Mid(TXt_Ora.Text,4,2)


            AppuntamentiFilePDF.Item(Indice).Ora = TimeSerial(TempOre,TempMinuti,0)
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Operatore = Txt_CodiceOperatore.Text 
        Catch ex As Exception

        End Try
                              

        Try
            AppuntamentiFilePDF.Item(Indice).Provenienza = DD_Provenienza.SelectedValue 
        Catch ex As Exception

        End Try

        Try
                AppuntamentiFilePDF.Item(Indice).CodiceImpegnativa =Txt_CodiceImpegnativa.Text 
        Catch ex As Exception

        End Try

            
        Try
                AppuntamentiFilePDF.Item(Indice).TestoQuesito =Txt_Testo.Text
        Catch ex As Exception

        End Try

        Try
              AppuntamentiFilePDF.Item(Indice).TipoContatto = DD_TipoContatto.SelectedValue 
        Catch ex As Exception

        End Try


        Try
            AppuntamentiFilePDF.Item(Indice).Contratto  =DD_CONTRATTO.SelectedValue 
        Catch ex As Exception

        End Try

        Try
            AppuntamentiFilePDF.Item(Indice).Priorita =DD_Priorita.SelectedValue
        Catch ex As Exception

        End Try
            

        
        

        Try
            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
            Dim ModificaTipologia As New Prestazione_Tipologia
            
            If lbl_Visita1.Text <>"" THEN
                ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA1.SelectedItem.Value)
                ModificaTipologia.Chiave_Import=  lbl_Visita1.Text                       
                Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
            End If

            AppuntamentiFilePDF.Item(Indice).Prestazioni(0).Descrizione = DD_VISITA1.SelectedItem.Text
            AppuntamentiFilePDF.Item(Indice).Prestazioni(0).Codice_Prestazione = DD_VISITA1.SelectedItem.Value
        Catch ex As Exception

        End Try

        If DD_VISITA2.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita2.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA2.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita2.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

                AppuntamentiFilePDF.Item(Indice).Prestazioni(1).Descrizione = DD_VISITA2.SelectedItem.Text
                AppuntamentiFilePDF.Item(Indice).Prestazioni(1).Codice_Prestazione = DD_VISITA2.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End If
        If DD_VISITA3.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita3.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA3.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita3.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

               AppuntamentiFilePDF.Item(Indice).Prestazioni(2).Descrizione = DD_VISITA3.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(2).Codice_Prestazione = DD_VISITA3.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End If

        If DD_VISITA4.SelectedItem.Text  <> "" then
            Try 
                Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))
                Dim ModificaTipologia As New Prestazione_Tipologia
            
                If lbl_Visita4.Text <>"" THEN
                    ModificaTipologia  =Tipologia_PPrestazione_Tipologia.FindByID_Prestazione_Tipologia(DD_VISITA4.SelectedItem.Value)
                    ModificaTipologia.Chiave_Import=  lbl_Visita4.Text                       
                    Tipologia_PPrestazione_Tipologia.Update(ModificaTipologia)
                End If

               AppuntamentiFilePDF.Item(Indice).Prestazioni(3).Descrizione = DD_VISITA4.SelectedItem.Text
               AppuntamentiFilePDF.Item(Indice).Prestazioni(3).Codice_Prestazione = DD_VISITA4.SelectedItem.Value       
            Catch ex As Exception

            End Try
        End if

        Dim IdAppuntamento As Integer = SalvaAppuntamento.GetIDAppuntamento(Session("DC_TABELLE"),AppuntamentiFilePDF.Item(Indice).CodiceOperazione) 
        If IdAppuntamento> 0 Then        
            SalvaAppuntamento.CancellaPrestazioni(Session("DC_TABELLE"),IdAppuntamento)
        End if

        SalvaAppuntamento.ScriviDB(Session("DC_TABELLE"),AppuntamentiFilePDF.Item(Indice),Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page)

        Session("AppuntamentiFilePDF") = AppuntamentiFilePDF 


        If VAL(Request.Item("NUMERO"))  < AppuntamentiFilePDF.Count  then
              Response.Redirect ("ConfermaAppuntamentoPDF.aspx?NUMERO=" & VAL(Request.Item("NUMERO")) + 1)
        Else
            Response.Redirect ("ElencoAppuntameti.aspx")
        End if
    End Sub
    

    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub

    Private Sub Img_Modifica_Click(sender As Object, e As EventArgs) Handles Img_Modifica.Click
        Call Modifica
    End Sub
End Class