﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports System.Web.Script.Serialization

Public Class EmissioneFattura
    Inherits System.Web.UI.Page

    Private Function LeggiFattura(ByVal Numero As Integer)  As Integer
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        LeggiFattura=0

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From MovimentiContabiliTesta where IdProgettoODV = ? And NumeroProtocollo > 0"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Numero", numero)
        
        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read then
            LeggiFattura = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
        End if
        myPOSTreader.Close()
        cn.Close()
    End Function




    
    Private sub LeggiIncassi(ByVal Numero As Integer)  
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
                
        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From MovimentiContabiliTesta where IdProgettoODV = ? And NumeroProtocollo = 0"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Numero", numero)
        
        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read  then
            Txt_DataIncasso.Text = campodbD(myPOSTreader.Item("DataRegistrazione"))
            DD_Causale.SelectedValue = campodb(myPOSTreader.Item("CausaleContabile"))
            
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Txt_Importo.Text = Format(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")),"#,##0.00")

            Txt_DataIncasso.Enabled =False
            Txt_Importo.Enabled =False
            DD_Causale.Enabled =False

            Btn_Add.Enabled =True
        End if
        if myPOSTreader.Read  then
            Txt_DataIncasso1.Text = campodbD(myPOSTreader.Item("DataRegistrazione"))
            DD_Causale1.SelectedValue = campodb(myPOSTreader.Item("CausaleContabile"))
            
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Txt_Importo1.Text = format(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")),"#,##0.00")

            Txt_DataIncasso1.Enabled =False
            Txt_Importo1.Enabled =False
            DD_Causale1.Enabled =False

            Txt_DataIncasso1.Visible =True
            Txt_Importo1.Visible =True
            DD_Causale1.Visible =True

            Btn_Add.Enabled =False
            Btn_Add1.Visible=True
        End if
        if myPOSTreader.Read  then
            Txt_DataIncasso2.Text = campodbD(myPOSTreader.Item("DataRegistrazione"))
            DD_Causale2.SelectedValue = campodb(myPOSTreader.Item("CausaleContabile"))
            
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Txt_Importo2.Text = format(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")),"#,##0.00")


            Txt_DataIncasso2.Enabled =False
            Txt_Importo2.Enabled =False
            DD_Causale2.Enabled =False

            Txt_DataIncasso2.Visible =True
            Txt_Importo2.Visible =True
            DD_Causale2.Visible =True

            Btn_Add1.Enabled =False
            Btn_Add2.Visible =True
        End if
        if myPOSTreader.Read  then
            Txt_DataIncasso3.Text = campodbD(myPOSTreader.Item("DataRegistrazione"))
            DD_Causale3.SelectedValue = campodb(myPOSTreader.Item("CausaleContabile"))
            
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Txt_Importo3.Text = format(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")),"#,##0.00")


            Txt_DataIncasso3.Enabled =False
            Txt_Importo3.Enabled =False
            DD_Causale3.Enabled =False

            Txt_DataIncasso3.Visible =True
            Txt_Importo3.Visible =True
            DD_Causale3.Visible =True

            Btn_Add2.Enabled =False
            Btn_Add3.Visible =True
        End if
        myPOSTreader.Close()
        cn.Close()
    End sub

     Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
   Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
   Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Call EseguiJS

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)

        'CodiceOperazione

        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")

        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))

        Dim MyFilteredList As List(Of DettaglioAppuntamento)

        Txt_DataIncasso.Text = Format(Now,"dd/MM/yyyy")

        Txt_DataIncasso1.Visible =False
        DD_Causale1.Visible=False
        Txt_Importo1.visible=False
        Btn_Add1.Visible =False
        
        Txt_DataIncasso2.Visible =False
        DD_Causale2.Visible=False
        Txt_Importo2.visible=False
        Btn_Add2.Visible =False

        
        Txt_DataIncasso3.Visible =False
        DD_Causale3.Visible=False
        Txt_Importo3.visible=False
        Btn_Add3.Visible =False

        Txt_DataIncasso4.Visible =False
        DD_Causale4.Visible=False
        Txt_Importo4.visible=False
        Btn_Add4.Visible =False



        MyFilteredList = AppuntamentiFilePDF.Where(Function(appuntamento) ( appuntamento.CodiceOperazione = CodiceOperazione)).ToList

        Lbl_DatiAppuntamenti.Text ="<br/>Congome : <b>" & MyFilteredList.Item(0).Utente.Cognome & "</b>"
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>Nome: <b>" & MyFilteredList.Item(0).Utente.Nome & "</b>"
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>Codice Fiscale: <b>" & MyFilteredList.Item(0).Utente.CodiceFiscale & "</b>"
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>Telefono: " & MyFilteredList.Item(0).Utente.Telefono1
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/><br/><br/>"
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "Prestazioni :"        

        Dim ImportoTotale As Double =0 

        For Each Visita In MyFilteredList.Item(0).Prestazioni            
            Dim PrestazioniContrattoServizio As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
            Dim ImportoListino As New Prestazione_Listino

            
            If Visita.Codice_Prestazione.Trim <> "" And Val(Visita.Codice_Prestazione) > 0 Then
                ImportoListino= PrestazioniContrattoServizio.GetByContrattoPrestazione( MyFilteredList.Item(0).Contratto.Trim, Visita.Codice_Prestazione, MyFilteredList.Item(0).Utente.IDFasciaIsee )
                If Not IsNothing(ImportoListino) then
                    Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>" & Visita.Descrizione & ": <b>" & format(ImportoListino.Importo,"#,##0.00") & "</b>"
                    ImportoTotale = ImportoTotale + ImportoListino.Importo
                Else
                     Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>" & Visita.Descrizione 
                End if 
            End if                             
        Next


        If  MyFilteredList.Item(0).Importo > 0 Then
            Dim ImportoExtra As Double =0
            Dim IVA As New Cls_IVA

            IVA.Codice = MyFilteredList.Item(0).CodiceIVA
            IVA.Leggi(Session("DC_TABELLE"),IVA.Codice)
            If IVA.Aliquota =0 then
                ImportoExtra = MyFilteredList.Item(0).Importo 
            Else
                ImportoExtra = MyFilteredList.Item(0).Importo  +  (MyFilteredList.Item(0).Importo  * IVA.Aliquota)
            End if


            Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>" & MyFilteredList.Item(0).Descrizione & ": <b>" & format(ImportoExtra ,"#,##0.00") & "</b>"
            ImportoTotale = ImportoTotale + ImportoExtra 
        End If

        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>" 

        If ImportoTotale =0 Then
            Lbl_DataFattura.Visible=False
            Lbl_Causale.Visible =False
            Lbl_TotaleFattura.Visible =False
            TxtImportoTotale.Visible =False
        Else
            TxtImportoTotale.Text = "<b>" & format(ImportoTotale,"#,##0.00") & "</b>"

        End If

        
        Lbl_DatiAppuntamenti.Text = Lbl_DatiAppuntamenti.Text  & "<br/>"
                       
        Dim CausaleContabile As New Cls_CausaleContabile


        Txt_DataFattura.Text = Format(Now,"dd/MM/yyyy")

        CausaleContabile.UpDateDropBoxDoc(Session("DC_TABELLE"),DD_CausaleFattura)
        
        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale)
        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale1)
        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale2)
        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale3)


        Dim Registrazione As New Cls_MovimentoContabile


        
        Registrazione.NumeroRegistrazione = LeggiFattura(CodiceOperazione)

        If Registrazione.NumeroRegistrazione  > 0 THEN
            Registrazione.Leggi(Session("DC_GENERALE"),Registrazione.NumeroRegistrazione)


            Txt_DataFattura.Text = Format(Registrazione.DataRegistrazione,"dd/MM/yyyy")

            Txt_DataFattura.Enabled =False

            DD_CausaleFattura.SelectedValue = Registrazione.CausaleContabile

            DD_CausaleFattura.Enabled=False


            Lbl_DatiFattura.Text = " Numero Protocollo <b>" & Registrazione.NumeroProtocollo & "/" & Registrazione.AnnoProtocollo & "</b>"
            Lbl_DatiFattura.Text = Lbl_DatiFattura.Text & " Importo : <b>" & Format(Registrazione.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00") & "</b>"

            Txt_Descrizione.Text  = Registrazione.Descrizione

            Txt_Descrizione.Enabled =False
            LeggiIncassi(CodiceOperazione)
        Else
            Txt_Importo.Text = Format(ImportoTotale,"#,##0.00")
        End IF

        if ImportoTotale = 0 Then
            
            DD_CausaleFattura.Visible  =False
            DD_Causale.Visible =False
            Txt_DataFattura.Visible=False
            Txt_DataIncasso.Visible=False
            Txt_Importo.Visible=False

            Btn_Add.Visible =False
            Exit Sub
        End If        
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub Btn_Add_Click(sender As Object, e As EventArgs) Handles Btn_Add.Click
        Txt_DataIncasso1.Visible =true
        DD_Causale1.Visible=true
        Txt_Importo1.visible=true        
        Btn_Add1.Visible =True

        
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale1)

    End Sub

    Private Sub Btn_Add2_Click(sender As Object, e As EventArgs) Handles Btn_Add2.Click
        Txt_DataIncasso2.Visible =true
        DD_Causale2.Visible=true
        Txt_Importo2.visible=true        
        Btn_Add2.Visible =True

        
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale2)
    End Sub

    Private Sub Btn_Add3_Click(sender As Object, e As EventArgs) Handles Btn_Add3.Click
        Txt_DataIncasso3.Visible =true
        DD_Causale3.Visible=true
        Txt_Importo3.visible=true        
        Btn_Add3.Visible =True

        
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale3)
    End Sub

    Private Sub Btn_Add4_Click(sender As Object, e As EventArgs) Handles Btn_Add4.Click
        Txt_DataIncasso4.Visible =true
        DD_Causale4.Visible=true
        Txt_Importo4.visible=true        
        Btn_Add4.Visible =True

        
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale4)
    End Sub

    Private Sub Img_Modifica_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Modifica.Click
        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)

        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")

        AppuntamentiFilePDF = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))

        Dim MyFilteredList As List(Of DettaglioAppuntamento)

        MyFilteredList = AppuntamentiFilePDF.Where(Function(appuntamento) ( appuntamento.CodiceOperazione = CodiceOperazione)).ToList
      

        Dim MyDettaglio As New DettaglioAppuntamento
         
        Dim ImportoTotale As Double =0

        For Each Visita In MyFilteredList.Item(0).Prestazioni
            Dim PrestazioniContrattoServizio As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
            Dim ImportoListino As New Prestazione_Listino
            
            ImportoListino= PrestazioniContrattoServizio.GetByContrattoPrestazione( MyFilteredList.Item(0).Contratto.Trim, Visita.Codice_Prestazione, MyFilteredList.Item(0).Utente.IDFasciaIsee)
            
            If not IsNothing(ImportoListino) Then
                ImportoTotale = ImportoTotale + ImportoListino.Importo            
            End if
        Next
        

        If  MyFilteredList.Item(0).Importo > 0 Then
            Dim ImportoExtra As Double =0
            Dim IVA As New Cls_IVA

            IVA.Codice = MyFilteredList.Item(0).CodiceIVA
            IVA.Leggi(Session("DC_TABELLE"),IVA.Codice)
            If IVA.Aliquota =0 then
                ImportoExtra = MyFilteredList.Item(0).Importo 
            Else
                ImportoExtra = MyFilteredList.Item(0).Importo  +  (MyFilteredList.Item(0).Importo  * IVA.Aliquota)
            End if
            
            ImportoTotale = ImportoTotale + ImportoExtra 
        End If


        Dim TotaleIncasso As Double =0
        If Txt_Importo.Text <>"" Then
            If val(Txt_Importo.Text) > 0 Then
                TotaleIncasso  =TotaleIncasso  + CDbl(Txt_Importo.Text)
            End If
        Else
            Txt_Importo.Text = "0"
        End If
        If Txt_Importo1.Text <>"" Then
            If val(Txt_Importo1.Text) > 0 Then
                TotaleIncasso  =TotaleIncasso  + CDbl(Txt_Importo1.Text)
            End If
          Else
            Txt_Importo1.Text = "0"
        End If
        If Txt_Importo2.Text <>"" Then
            If val(Txt_Importo2.Text) > 0 Then
                TotaleIncasso  =TotaleIncasso  + CDbl(Txt_Importo2.Text)
            End If
          Else
            Txt_Importo2.Text = "0"
        End If
        If Txt_Importo3.Text <>"" Then
            If val(Txt_Importo3.Text) > 0 Then
                TotaleIncasso  =TotaleIncasso  + CDbl(Txt_Importo3.Text)
            End If
         Else
            Txt_Importo3.Text = "0"
        End If

        If ImportoTotale  = 0 Then
            Exit Sub
        End If

        If TotaleIncasso > ImportoTotale Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Importo incassato maggiore del fatturato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_DataIncasso.Text= "" And val(Txt_Importo.Text) > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserito importo su incasso , inserire data   ');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_DataIncasso1.Text= "" And val(Txt_Importo1.Text) > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserito importo su incasso , inserire data   ');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_DataIncasso2.Text= "" And val(Txt_Importo2.Text) > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserito importo su incasso , inserire data   ');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_DataIncasso3.Text= "" And val(Txt_Importo3.Text) > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserito importo su incasso , inserire data   ');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Txt_DataIncasso4.Text= "" And val(Txt_Importo4.Text) > 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Inserito importo su incasso , inserire data   ');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Txt_DataFattura.Enabled = true Then
            Dim CausaleContabile As New Cls_CausaleContabile


            CausaleContabile.Codice = DD_CausaleFattura.SelectedValue
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

            Dim MovimentoContabile As New Cls_MovimentoContabile

            If format(MovimentoContabile.MaxDataRegistroIVA(Session("DC_GENERALE"), Year(Txt_DataFattura.Text),  CausaleContabile.RegistroIVA),"yyyyMMdd") > Format(Txt_DataFattura.Text,"yyyyMMdd")  Then                          
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data registrazione precedente ad ultima fattura inserita');", True)
                Call EseguiJS()
                Exit Sub
            End If  
        End If

        
        Dim Mastro As Integer =0
        Dim Conto As Integer=0
        Dim Sottoconto As Integer=0


        ScriviCliente(Mastro,Conto,Sottoconto, MyFilteredList.Item(0))


        If Txt_DataFattura.Enabled = true Then
            SCriviFattura(Mastro,Conto,Sottoconto,MyFilteredList.Item(0))
        End if

        If Txt_Importo.Enabled = True Then
            If CDbl(Txt_Importo.Text) > 0 And DD_Causale.SelectedValue.Trim <> ""  Then
                ScriviIncasso(Txt_DataIncasso.Text,DD_Causale.SelectedValue,CDbl(Txt_Importo.Text),Mastro,Conto,Sottoconto, MyFilteredList.Item(0))

                Txt_DataIncasso.Enabled= False
                DD_Causale.Enabled= False
                Txt_Importo.Enabled=False
            End If
        End If
        If Txt_Importo1.Visible = True And Txt_Importo1.Enabled = True Then
            If CDbl(Txt_Importo1.Text) > 0 And DD_Causale1.SelectedValue.Trim <> ""  Then
                ScriviIncasso(Txt_DataIncasso1.Text,DD_Causale1.SelectedValue,CDbl(Txt_Importo1.Text),Mastro,Conto,Sottoconto, MyFilteredList.Item(0))

                Txt_DataIncasso1.Enabled= False
                DD_Causale1.Enabled= False
                Txt_Importo1.Enabled=False
            End If
        End If
        If Txt_Importo2.Visible= True And Txt_Importo2.Enabled = True  Then
            If CDbl(Txt_Importo2.Text) > 0  And DD_Causale2.SelectedValue.Trim <> ""  Then
                ScriviIncasso(Txt_DataIncasso2.Text,DD_Causale2.SelectedValue,CDbl(Txt_Importo2.Text),Mastro,Conto,Sottoconto, MyFilteredList.Item(0))

                Txt_DataIncasso2.Enabled= False
                DD_Causale2.Enabled= False
                Txt_Importo2.Enabled=False
            End If
        End If
        If Txt_Importo3.Visible = True And Txt_Importo3.Enabled = True  Then
            If CDbl(Txt_Importo3.Text) > 0  And DD_Causale3.SelectedValue.Trim <> ""  Then
                ScriviIncasso(Txt_DataIncasso3.Text,DD_Causale3.SelectedValue,CDbl(Txt_Importo3.Text),Mastro,Conto,Sottoconto, MyFilteredList.Item(0))

                Txt_DataIncasso3.Enabled= False
                DD_Causale3.Enabled= False
                Txt_Importo3.Enabled=False
            End If
        End If
        
    End Sub

    
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function




    Private Sub ScriviCliente( ByRef  Mastro As Integer, ByRef Conto As Integer,  ByRef Sottoconto As Integer, ByVal MyDettaglio As DettaglioAppuntamento)
        'GetSingle

        Dim ClienteManager As New ClienteBiz(Session("DC_OSPITE"))
        Dim MyCliente As New Cliente


        MyCliente = ClienteManager.FindByCF(MyDettaglio.Utente.CodiceFiscale)
        
        If IsNothing(MyCliente) Then
            Dim InsCliente As New Cliente    
            InsCliente.Cognome = MyDettaglio.Utente.Cognome
            InsCliente.Nome = MyDettaglio.Utente.Nome
            InsCliente.CognomeNome = MyDettaglio.Utente.Cognome & " "  & MyDettaglio.Utente.Nome
            InsCliente.CodiceFiscale = MyDettaglio.Utente.CodiceFiscale

            InsCliente.Indirizzo = MyDettaglio.Utente.Indirizzo
            InsCliente.CAP = MyDettaglio.Utente.CAP

            Dim StringaComune As String

            StringaComune  =MyDettaglio.Utente.Comune

            Dim Vettore(100) as String

            Vettore = SplitWords(StringaComune)
            
            If Vettore.Length >= 3 Then
                InsCliente.Provincia= Vettore(0)
                InsCliente.Comune = Vettore(1)
            End If
            
            InsCliente.DataNascita = MyDettaglio.Utente.DataNascita
            ClienteManager.Add(InsCliente)

            Dim Log As New Cls_LogPrivacy

            Dim serializer As JavaScriptSerializer
            serializer = New JavaScriptSerializer()
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "I", "EMISSIONEFATTURA", serializer.Serialize(InsCliente))


            Mastro = InsCliente.MastroCliente
            Conto = InsCliente.ContoCliente
            Sottoconto = InsCliente.SottoContoCliente

            Dim PianoConti As NEW Cls_Pianodeiconti

            PianoConti.Mastro = Mastro
            PianoConti.Conto = Conto
            PianoConti.Sottoconto = Sottoconto
            PianoConti.Descrizione = InsCliente.CognomeNome
            PianoConti.Scrivi(Session("DC_GENERALE"))
           Else
            Mastro = MyCliente.MastroCliente
            Conto = MyCliente.ContoCliente
            Sottoconto = MyCliente.SottoContoCliente
        End If
                      

    End Sub
    Private Sub ScriviIncasso(byval DataIncasso As Date,byval CausaleIncasso As String, byval Importo As Double, byval  Mastro As Integer, ByVal Conto As Integer,  ByVal Sottoconto As Integer, ByVal MyDettaglio As DettaglioAppuntamento)
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")        
        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Registrazione As New Cls_MovimentoContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"),CausaleIncasso)

        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")


        Registrazione.DataRegistrazione = DataIncasso
        Registrazione.DataDocumento = DataIncasso
        Registrazione.NumeroDocumento = ""
        Registrazione.CodicePagamento = ""        
        Registrazione.CausaleContabile = CausaleIncasso      
        Registrazione.AnnoCompetenza = 0
        Registrazione.MeseCompetenza = 0
        Registrazione.CentroServizio = ""

        Registrazione.RegistroIVA = 0
        Registrazione.AnnoProtocollo =0
        Registrazione.NumeroProtocollo = 0
        Registrazione.IdProgettoODV = CodiceOperazione 
        
        Registrazione.Descrizione = Txt_Descrizione.Text       


        Registrazione.Utente = Session("UTENTE")


        Dim CentroServizio As New Cls_CentroServizio

        Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(0).MastroPartita = Mastro
        Registrazione.Righe(0).ContoPartita = conto
        Registrazione.Righe(0).SottocontoPartita = Sottoconto        
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = CausaleContabile.Righe(0).Segno

        Registrazione.Righe(0).Importo = Math.Abs(Importo )



        Registrazione.Righe(0).Tipo = "CF"
        Registrazione.Righe(0).RigaDaCausale = 1
        Registrazione.Righe(0).Utente = Session("UTENTE")


        Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere      
        Registrazione.Righe(1).Segno = CausaleContabile.Righe(1).Segno
        Registrazione.Righe(1).Importo = Importo 
        Registrazione.Righe(1).Imponibile = 0
        Registrazione.Righe(1).CodiceIVA = ""
        Registrazione.Righe(1).Tipo = ""
        Registrazione.Righe(1).RigaDaCausale = 2
        Registrazione.Righe(1).Utente = Session("UTENTE")

        Registrazione.Scrivi(Session("DC_GENERALE"),0)

        
        Dim Legame As New Cls_Legami
        Dim FatturaNumeroRegistrazione As Integer
        
                
        FatturaNumeroRegistrazione = LeggiFattura(CodiceOperazione)

        Legame.Leggi(Session("DC_GENERALE"), FatturaNumeroRegistrazione ,0 )

        Dim i As Integer

        For i = 0 To 100
            If Legame.NumeroPagamento(i)  = 0 Then
                Legame.NumeroPagamento(i)= Registrazione.NumeroRegistrazione
                Legame.NumeroDocumento(i)= FatturaNumeroRegistrazione
                Legame.Importo(i)= Importo
                Exit For
            End If
        Next
        Legame.Scrivi(Session("DC_GENERALE"),FatturaNumeroRegistrazione,0)

        

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Registrazione.NumeroRegistrazione, "", "I", "EMISSIONEFATTURA", serializer.Serialize(Registrazione))


    End Sub


    Private Sub ScriviFattura(byval  Mastro As Integer, ByVal Conto As Integer,  ByVal Sottoconto As Integer, ByVal MyDettaglio As DettaglioAppuntamento)
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Registrazione As New Cls_MovimentoContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), DD_Causalefattura.SelectedValue)

        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")

        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date

        Try
            DataAppo = Txt_DataFattura.Text
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella data di registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End Try

        RegIva.Tipo = CausaleContabile.RegistroIVA
        RegIva.Anno = Year(Txt_DataFattura.Text)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)

        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registro iva chiuso prima della dataregistrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Format(Registrazione.MaxDataRegistroIVA(Session("DC_GENERALE"), Year(DataAppo), CausaleContabile.RegistroIVA), "yyyyMMdd") > Format(DataAppo, "yyyyMMdd") Then            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già registrato una documento con data successiva');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim Vettore(100) as String
        Dim VettoreImporto(100) as Double
        Dim Righe As Integer =0
        Dim ImportoTotale As Double =0
        Dim ImponibileExtra As Double =0

        For Each Visita In MyDettaglio.Prestazioni
            If Visita.Codice_Prestazione.Trim <> "" And Val(Visita.Codice_Prestazione) > 0 Then
                Dim PrestazioniContrattoServizio As New Prestazione_ListinoBiz(Session("DC_TABELLE"))
                Dim ImportoListino As New Prestazione_Listino

            
                ImportoListino= PrestazioniContrattoServizio.GetByContrattoPrestazione( MyDettaglio.Contratto.Trim, Visita.Codice_Prestazione, MyDettaglio.Utente.IDFasciaIsee)

                If Not IsNothing(ImportoListino) then
                    Vettore(Righe) = Visita.Descrizione
                    VettoreImporto(Righe) = ImportoListino.Importo


                    ImportoTotale = ImportoTotale + ImportoListino.Importo

                    ImponibileExtra= ImponibileExtra + ImportoListino.Importo
                End If
            End If
            Righe =Righe +1
        Next

        
        

        If  MyDettaglio.Importo > 0 Then
            Dim ImportoExtra As Double =0
            Dim IVA As New Cls_IVA

            IVA.Codice = MyDettaglio.CodiceIVA
            IVA.Leggi(Session("DC_TABELLE"),IVA.Codice)
            If IVA.Aliquota =0 then
                ImportoExtra = MyDettaglio.Importo 
            Else
                ImportoExtra = MyDettaglio.Importo  +  (MyDettaglio.Importo  * IVA.Aliquota)
            End if

            
            ImportoTotale = ImportoTotale + ImportoExtra 
        End If




        Registrazione.DataRegistrazione = Txt_DataFattura.Text
        Registrazione.DataDocumento = Txt_DataFattura.Text
        Registrazione.NumeroDocumento = Registrazione.MaxProtocollo(ConnectionStringGenerale, Year(Txt_DataFattura.Text), CausaleContabile.RegistroIVA) + 1


        Registrazione.CodicePagamento = ""
        
        Registrazione.CausaleContabile = DD_CausaleFattura.SelectedValue
        
        Registrazione.AnnoCompetenza = Year(DataAppo)
        Registrazione.MeseCompetenza = Month(DataAppo)
        Registrazione.CentroServizio = ""

        Registrazione.RegistroIVA = CausaleContabile.RegistroIVA
        Registrazione.AnnoProtocollo =Year(DataAppo)
        Registrazione.NumeroProtocollo = Registrazione.NumeroDocumento
        Registrazione.IdProgettoODV = CodiceOperazione 

        
        Registrazione.Descrizione = Txt_Descrizione.Text
        


        Registrazione.Utente = Session("UTENTE")


        Dim CentroServizio As New Cls_CentroServizio

        Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(0).MastroPartita = Mastro
        Registrazione.Righe(0).ContoPartita = conto
        Registrazione.Righe(0).SottocontoPartita = Sottoconto        
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = CausaleContabile.Righe(0).Segno

        Registrazione.Righe(0).Importo = Math.Abs(ImportoTotale )



        Registrazione.Righe(0).Tipo = "CF"
        Registrazione.Righe(0).RigaDaCausale = 1
        Registrazione.Righe(0).Utente = Session("UTENTE")


        Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere      
        Registrazione.Righe(1).Segno = CausaleContabile.Righe(1).Segno
        Registrazione.Righe(1).Importo = 0
        Registrazione.Righe(1).Imponibile = ImponibileExtra 
        Registrazione.Righe(1).CodiceIVA = "10" ' codice iva
        Registrazione.Righe(1).Tipo = "IV"
        Registrazione.Righe(1).RigaDaCausale = 2
        Registrazione.Righe(1).Utente = Session("UTENTE")


        Dim I As Integer
        Dim RigaIndice As Integer =2
        If  MyDettaglio.Importo > 0 Then
            Dim ImportoExtra As Double =0
            Dim IVA As New Cls_IVA

            IVA.Codice = MyDettaglio.CodiceIVA
            IVA.Leggi(Session("DC_TABELLE"),IVA.Codice)

            ImportoExtra  = MyDettaglio.Importo

            Registrazione.Righe(RigaIndice) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(RigaIndice).MastroPartita = CausaleContabile.Righe(1).Mastro
            Registrazione.Righe(RigaIndice).ContoPartita = CausaleContabile.Righe(1).Conto
            Registrazione.Righe(RigaIndice).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
            Registrazione.Righe(RigaIndice).DareAvere =MyDettaglio.DareAvere      
            Registrazione.Righe(RigaIndice).Segno = CausaleContabile.Righe(1).Segno
            Registrazione.Righe(RigaIndice).Importo =  Math.Round(ImportoExtra  * IVA.Aliquota ,2)
            Registrazione.Righe(RigaIndice).Imponibile = ImportoExtra  
            Registrazione.Righe(RigaIndice).CodiceIVA = MyDettaglio.CodiceIVA
            Registrazione.Righe(RigaIndice).Tipo = "IV"
            Registrazione.Righe(RigaIndice).RigaDaCausale = 2
            Registrazione.Righe(RigaIndice).Utente = Session("UTENTE")
            RigaIndice= RigaIndice+1
        End If


        For i = 0 To Righe
            If VettoreImporto(I) > 0 Then
                Registrazione.Righe(RigaIndice) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(RigaIndice).MastroPartita = CausaleContabile.Righe(2).Mastro
                Registrazione.Righe(RigaIndice).ContoPartita = CausaleContabile.Righe(2).Conto
                Registrazione.Righe(RigaIndice).SottocontoPartita = CausaleContabile.Righe(2).Sottoconto
                Registrazione.Righe(RigaIndice).DareAvere = CausaleContabile.Righe(2).DareAvere      
                Registrazione.Righe(RigaIndice).Segno = CausaleContabile.Righe(2).Segno
                Registrazione.Righe(RigaIndice).Importo = VettoreImporto(i)
                Registrazione.Righe(RigaIndice).Descrizione = Vettore(I)
                Registrazione.Righe(RigaIndice).CodiceIVA = "10" ' codice iva
                Registrazione.Righe(RigaIndice).Tipo = ""
                Registrazione.Righe(RigaIndice).RigaDaCausale = 3
                Registrazione.Righe(RigaIndice).Utente = Session("UTENTE")
                RigaIndice= RigaIndice+1
            End If
        Next


        If  MyDettaglio.Importo > 0 Then
            Dim ImportoExtra As Double =0
            Dim IVA As New Cls_IVA

            IVA.Codice = MyDettaglio.CodiceIVA
            IVA.Leggi(Session("DC_TABELLE"),IVA.Codice)

            ImportoExtra  = MyDettaglio.Importo

            Registrazione.Righe(RigaIndice) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(RigaIndice).MastroPartita = MyDettaglio.MastroExtra
            Registrazione.Righe(RigaIndice).ContoPartita = MyDettaglio.ContoExtra
            Registrazione.Righe(RigaIndice).SottocontoPartita = MyDettaglio.SottoContoExtra
            Registrazione.Righe(RigaIndice).DareAvere = MyDettaglio.DareAvere
            Registrazione.Righe(RigaIndice).Segno = "+"
            Registrazione.Righe(RigaIndice).Importo = MyDettaglio.Importo 
            Registrazione.Righe(RigaIndice).Descrizione = MyDettaglio.Descrizione
            Registrazione.Righe(RigaIndice).CodiceIVA = MyDettaglio.CodiceIVA
            Registrazione.Righe(RigaIndice).Tipo = ""
            Registrazione.Righe(RigaIndice).RigaDaCausale = 3
            Registrazione.Righe(RigaIndice).Utente = Session("UTENTE")
            RigaIndice= RigaIndice+1
        End If

            
        Registrazione.Scrivi(Session("DC_GENERALE"),0)

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Registrazione.NumeroRegistrazione, "", "I", "EMISSIONEFATTURA", serializer.Serialize(Registrazione))



        DD_CausaleFattura.Enabled =False
        Txt_DataFattura.Enabled =False
        Txt_Descrizione.Enabled =False

        Lbl_DatiFattura.Text = " Numero Protocollo " & Registrazione.NumeroProtocollo & "/" & Registrazione.AnnoProtocollo
        Lbl_DatiFattura.Text = Lbl_DatiFattura.Text & " Importo :" & Format(Registrazione.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")
    End Sub

    Private Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        If Lbl_TotaleFattura.Visible = false then
            If cdbl(TxtImportoTotale.Text) = 0 Then
                Exit Sub
            End If
        End if
        
        Dim x As New Cls_StampaFattureContabilita

        Dim NumeroFatture As Integer =0
        
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")


        NumeroFatture = LeggiFattura(CodiceOperazione)

        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.NumeroRegistrazione = LeggiFattura(CodiceOperazione)
        Registrazione.Leggi(Session("DC_GENERALE"),NumeroFatture)

        x.CreaStampa(Txt_DataFattura.Text,Txt_DataFattura.Text,Registrazione.NumeroProtocollo,Registrazione.NumeroProtocollo,Registrazione.RegistroIVA,Session,"")
        
            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPAFATTURACONTABILITA&PRINTERKEY=ON','Stampe1','width=800,height=600');", True)        
    End Sub

    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")


        Response.Redirect("Appuntamento_Dettaglio.aspx?ID_Operatore=" & CodiceOperazione)

    End Sub

    Private Sub Img_Modifica_Command(sender As Object, e As CommandEventArgs) Handles Img_Modifica.Command

    End Sub

    Private Sub Btn_Add1_Click(sender As Object, e As EventArgs) Handles Btn_Add1.Click
        Txt_DataIncasso2.Visible =true
        DD_Causale2.Visible=true
        Txt_Importo2.visible=true        
        Btn_Add2.Visible =True

        
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_Causale2)
    End Sub

    Private Sub Img_Elimina_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Elimina.Click

        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")


        Response.Redirect("EliminaRegistrazioni.aspx?CodiceOperazione=" & CodiceOperazione)
    End Sub
End Class
