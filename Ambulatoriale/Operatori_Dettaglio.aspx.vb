﻿
Partial Class Ambulatoriale_Operatori_Dettaglio
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim TpOpe As New Cls_TipoOperatore

        TpOpe.UpDateDropBox(Session("DC_OSPITE"), DD_TipoOperatore)

        Dim RsRagr As New Cls_RaggruppamentoOperatori

        RsRagr.UpDateDropBox(Session("DC_OSPITE"), DD_Raggruppamento)

        If Val(Request.Item("CODICE")) = 0 Then

            Dim MaxOperatore As New Cls_Operatore


            Txt_CodiceMedico.Text = MaxOperatore.MaxOperatore(Session("DC_OSPITE"))

            Exit Sub
        End If


        Dim Med As New Cls_Operatore

        Med.CodiceMedico = Request.Item("CODICE")
        Med.Leggi(Session("DC_OSPITE"))
        Txt_CodiceMedico.Text = Med.CodiceMedico
        Txt_Nome.Text = Med.Nome
        Txt_CodiceFiscale.Text = Med.CodiceFiscale
        Txt_Indirizzo.Text = Med.RESIDENZAINDIRIZZO1

        Dim DeCom As New ClsComune
        DeCom.Comune = Med.RESIDENZACOMUNE1
        DeCom.Provincia = Med.RESIDENZAPROVINCIA1
        DeCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = DeCom.Descrizione

        Txt_Telefono.Text = Med.Telefono1
        Txt_TelefonoCellulare.Text = Med.Telefono2
        Txt_Specializzazione.Text = Med.Specializazione
        Txt_Note.Text = Med.Note

        DD_TipoOperatore.SelectedValue = Med.TipoOperatore

        DD_Raggruppamento.SelectedValue = Med.RaggruppamentoOperatori

        Dim DecConto As New Cls_Pianodeiconti

        DecConto.Mastro = Med.MastroFornitore
        DecConto.Conto = Med.ContoFornitore
        DecConto.Sottoconto = Med.SottoContoFornitore
        DecConto.Decodfica(Session("DC_GENERALE"))

        Txt_Conto.Text = DecConto.Mastro & " " & DecConto.Conto & " " & DecConto.Sottoconto & " " & DecConto.Descrizione

        If Med.EmOspiteParente = 1 Then
            Chk_EmOspiteParente.Checked = True
        Else
            Chk_EmOspiteParente.Checked = False
        End If

        Txt_CodiceMedico.Enabled = False


        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "V", "OPERATORE", "")


    End Sub




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Med As New Cls_Operatore
        If Val(Txt_CodiceMedico.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice, codice deve essere numerico');", True)
            Exit Sub
        End If
        If Txt_Nome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare cognome nome');", True)
            Exit Sub
        End If


        If Request.Item("CODICE") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "I", "OPERATORE", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_Operatore

            OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
            OldDatiPar.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "M", "OPERATORE", AppoggioJS)

        End If

        Med.CodiceMedico = Txt_CodiceMedico.Text
        Med.Nome = Txt_Nome.Text
        Med.CodiceFiscale = Txt_CodiceFiscale.Text
        Med.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text

        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                Med.RESIDENZAPROVINCIA1 = Vettore(0)
                Med.RESIDENZACOMUNE1 = Vettore(1)
            End If
        End If

        If Txt_Conto.Text <> "" Then
            Vettore = SplitWords(Txt_Conto.Text)
            If Vettore.Length > 2 Then
                Med.MastroFornitore = Vettore(0)
                Med.ContoFornitore = Vettore(1)
                Med.SottoContoFornitore = Vettore(2)
            End If
        End If


        Med.Telefono1 = Txt_Telefono.Text
        Med.Telefono2 = Txt_TelefonoCellulare.Text
        Med.Specializazione = Txt_Specializzazione.Text
        Med.Note = Txt_Note.Text
        Med.TipoOperatore = DD_TipoOperatore.SelectedValue

        Med.RaggruppamentoOperatori = DD_Raggruppamento.SelectedValue

        If Chk_EmOspiteParente.Checked = True Then
            Med.EmOspiteParente = 1
        Else
            Med.EmOspiteParente = 0
        End If
        Med.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_CodiceMedico.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        Dim MyMed As New Cls_Operatore
        MyMed.CodiceMedico = Txt_CodiceMedico.Text

        If MyMed.VerificaElimina(Session("DC_OSPITE")) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso utilizzare codice usate');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Operatore

        OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
        OldDatiPar.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "D", "OPERATORE", AppoggioJS)

        MyMed.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub
    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Operatori.aspx")
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComuneIndirizzo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class
