﻿Imports System.Web.Hosting
Imports Senior.Entities
Imports Senior.BIZ
Public Class ImportPDF_SSR
    Inherits System.Web.UI.Page

    Private Sub LeggiPdf(ByVal StringaPDF As String)
        Dim RighePDF() As String = Regex.Split(StringaPDF, vbLf)
        Dim I As Integer
        Dim DettagliAppuntamenti As DettaglioAppuntamento


        If RighePDF(I).IndexOf(vbLf) > 0 Then
            I = 10
        End If
        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)

        Dim IntestazioneCampi as Integer = 0

        For I = 0 To RighePDF.Count - 1

            If RighePDF(I).IndexOf(":") = 2 and RighePDF(I).IndexOf("/") = 8 Then
                IntestazioneCampi  = I
                exit For
            End If
            If RighePDF(I).IndexOf("appuntamenti") > 0 Then
                Exit For
            End If
        Next
       

        Dim InizioAppuntamenti As Integer = I + 1


        if IntestazioneCampi > 0 then
            InizioAppuntamenti = IntestazioneCampi  
        End If


        Dim RigaElaborazione As Integer = 1



        Dim Appuntamento As New DettaglioAppuntamento


        Dim InizioPrestazioni As Boolean = False

        For I = InizioAppuntamenti To RighePDF.Count - 1

            If I = 2820 Then
                I = 2820
            End If
            Dim StringaGlb As String

            StringaGlb = RighePDF(I)


            If Appuntamento.Priorita ="" then
                 If StringaGlb.IndexOf("D -") >= 0 Then
                    Appuntamento.Priorita ="D"
                End If
                If StringaGlb.IndexOf("P -") >= 0 Then
                    Appuntamento.Priorita ="P"
                End If
                If StringaGlb.IndexOf("B -") >= 0 Then
                    Appuntamento.Priorita ="B"
                End If
                If StringaGlb.IndexOf("U -") >= 0 Then
                    Appuntamento.Priorita ="U"
                End If
            End if
            If Appuntamento.TestoQuesito = "" Then
                If StringaGlb.IndexOf("Note:") >= 0 Then
                    Appuntamento.TestoQuesito = StringaGlb.Replace("Note:","")                            
                End If                
                If StringaGlb.IndexOf("Testo quesito:") >= 0 Then
                    Appuntamento.TestoQuesito = StringaGlb.Replace("Testo quesito:","")                            
                End If     
            End If                
            If Appuntamento.Contratto ="" Then
                If StringaGlb.IndexOf("CON:") >= 0 Then
                    Appuntamento.Contratto = replace(StringaGlb,"CON:","")

                    If StringaGlb.IndexOf("IN:") >= 0 Then
                        Appuntamento.Contratto = Trim(mid(StringaGlb,1,StringaGlb.IndexOf("IN:")).Replace("CON:",""))
                    End If                    
                End If
            End If
            IF Appuntamento.CodiceImpegnativa ="" Then                   
                If StringaGlb.IndexOf("CI:") >= 0 Then
                    Appuntamento.CodiceImpegnativa= mid(StringaGlb,4,15)
                End If    
            End If

            IF Appuntamento.Operatore = 0 Then                   
                If StringaGlb.IndexOf("Op:") >= 0 Then
                    Appuntamento.Operatore = replace(StringaGlb,"Op:","")
                End If
            End If

            If RigaElaborazione = 12 Then
                Dim Stringa As String

                Stringa = RighePDF(I)

                 If Stringa.IndexOf("D -") >= 0 Then
                    Appuntamento.Priorita ="D"
                End If
                If Stringa.IndexOf("P -") >= 0 Then
                    Appuntamento.Priorita ="P"
                End If
                If Stringa.IndexOf("B -") >= 0 Then
                    Appuntamento.Priorita ="B"
                End If
                If Stringa.IndexOf("U -") >= 0 Then
                    Appuntamento.Priorita ="U"
                End If

                If InizioPrestazioni = True Then
                    If Stringa.IndexOf(":") >= 0 And Stringa.IndexOf("Tel") >= 0 Then
                        InizioPrestazioni = False

                        AppuntamentiFilePDF.Add(Appuntamento)

                        Appuntamento =New DettaglioAppuntamento

                        RigaElaborazione = 1
                    Else
                        If Stringa.IndexOf("Pagina") < 0 then                        
                            Dim Tipologia_PPrestazione_Tipologia As New Prestazione_TipologiaBiz(Session("DC_TABELLE"))

                            Dim MyListaPrestazione_Tipologia As New Prestazione_Tipologia_List


                            MyListaPrestazione_Tipologia =Tipologia_PPrestazione_Tipologia.FindByChiave_Importazione(Stringa)

                            If MyListaPrestazione_Tipologia.Count = 0 Then
                                Appuntamento.Prestazioni.Add(New Prestazione With {.Descrizione = Stringa, .Codice_Prestazione = 0})
                            Else
                                Appuntamento.Prestazioni.Add(New Prestazione With {.Descrizione = Stringa, .Codice_Prestazione = MyListaPrestazione_Tipologia.Item(0).ID_Prestazione_Tipologia})
                            End If                                               
                        End If                               
                    End If
                End If

                If Stringa.IndexOf("Prestazioni non fatturate:") >= 0 Then
                    InizioPrestazioni  =True
                End If
                If Stringa.IndexOf("appuntamenti") >= 0  Then
                    InizioPrestazioni = False

                    AppuntamentiFilePDF.Add(Appuntamento)

                    Appuntamento =New DettaglioAppuntamento

                    RigaElaborazione = 1

                    I = I+1
                    RigaElaborazione=1
                End If
            End If

            If StringaGlb.IndexOf("Prestazioni non fatturate:") >= 0 Then
                 InizioPrestazioni  =True
                 RigaElaborazione  = 12
            End If


            If RigaElaborazione  = 11 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)
                
                If Stringa.IndexOf("CI:") >= 0 Then
                    Appuntamento.CodiceImpegnativa= mid(Stringa,4,15)
                End If                
                If Stringa.IndexOf("D -") >= 0 Then
                    Appuntamento.Priorita ="D"
                End If
                If Stringa.IndexOf("P -") >= 0 Then
                    Appuntamento.Priorita ="P"
                End If
                If Stringa.IndexOf("B -") >= 0 Then
                    Appuntamento.Priorita ="B"
                End If
                If Stringa.IndexOf("U -") >= 0 Then
                    Appuntamento.Priorita ="U"
                End If

                
                If Stringa.IndexOf("Prestazioni non fatturate:") >= 0 Then
                    InizioPrestazioni  =True
                End If

                RigaElaborazione  = 12
            End If

             If RigaElaborazione  = 10 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("Testo quesito:") >= 0 Then
                    Appuntamento.TestoQuesito = replace(Stringa,"Testo quesito:","")
                End If                
                RigaElaborazione  = 11
            End If
         

            If RigaElaborazione  = 9 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("CON:") >= 0 Then
                    Appuntamento.Contratto = replace(Stringa,"CON:","")
                    If Stringa.IndexOf("IN:") >= 0 Then
                        Appuntamento.Contratto = Trim(mid(Stringa,1,Stringa.IndexOf("IN:")).Replace("CON:",""))
                    End If       
                End If                
                RigaElaborazione  = 10
            End If

            If RigaElaborazione  = 8 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)
                
                Try
                    Appuntamento.DataDomanda = Stringa                
                Catch ex As Exception

                End Try
                
                RigaElaborazione  = 9
            End If

            If RigaElaborazione  = 7 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)
                 
                If Stringa.IndexOf("Note:") < 0 Then
                    Appuntamento.CodiceOperazione = Stringa                

                    RigaElaborazione  = 8                
                End If                
            End If
            If RigaElaborazione  = 6 Then
                 Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("TC:") >= 0 Then
                    Appuntamento.TipoContatto = mid(Stringa,4,3)
                End If
                RigaElaborazione  = 7
            End If
            If RigaElaborazione  = 5 Then
                Dim Stringa As String 

                 Stringa = RighePDF(I)

                Dim Indirizzo As String

                Try
                    Indirizzo = mid(Stringa,1, Stringa.IndexOf("-"))
                Catch ex As Exception
                    Indirizzo = ""
                End Try
                
                 
                Dim Cap As String

                Stringa = trim(Replace(Stringa, mid(Stringa,1, Stringa.IndexOf("-") + 1),""))

                Cap = mid(Stringa,1,5)

                Dim Comune As String 

                Stringa= Trim(Replace(Stringa,Cap,""))


                Dim FineComune As Integer =0

                FineComune  = Stringa.IndexOf("(")

                If FineComune < 0 Then
                    FineComune = Stringa.Length
                End If

                Try
                    Comune = Mid(Stringa,1,FineComune)
                Catch ex As Exception
                    Comune = ""
                End Try
                

                Appuntamento.Utente.Indirizzo = Indirizzo
                Appuntamento.Utente.CAP = Cap
                Appuntamento.Utente.Comune = Comune

                RigaElaborazione  = 6
            End If


            If RigaElaborazione  = 4 Then
                Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("CF:") >= 0 Then
                    Appuntamento.Utente.CodiceFiscale = replace(Stringa,"CF:","")
                
                    If Appuntamento.Utente.CodiceFiscale.Trim = "GSSMHL48M15A669U" Then
                        Appuntamento.Utente.CodiceFiscale = "GSSMHL48M15A669U" 
                    End If
                End If

                
                RigaElaborazione  = 5
            End If

            If RigaElaborazione  = 3 Then
                Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("CRA:") >= 0 Then
                    Appuntamento.CodiceSanitario = replace(Stringa,"CRA:","")
                End If
                RigaElaborazione  = 4
            End If

            
            If RigaElaborazione  = 2 Then
                Dim Stringa As String 

                 Stringa = RighePDF(I)

                If Stringa.IndexOf("Op:") >= 0 Then
                    Appuntamento.Operatore = replace(Stringa,"Op:","")
                End If
                RigaElaborazione  = 3
            End If



            If RigaElaborazione  = 1 Then
                Dim Stringa As String 

                 Stringa = RighePDF(I)
                 
                 If Stringa.IndexOf("Pagina") >= 0 Then
                    Dim CercaOra As Integer

                    CercaOra = Stringa.IndexOf(":") - 1

                    Stringa = mid(Stringa,CercaOra, Len(Stringa) - CercaOra)                 
                End If


                If Stringa.IndexOf("Unità Offerente")  >= 0 Then
                    Do 
                      I =I +1 
                    Loop While RighePDF(i).IndexOf("appuntamenti") = -1   

                    I =I +1 
                    Stringa = RighePDF(i)                    
                End If

                If Stringa.IndexOf("appuntamenti") = -1 Then
                                   

                    Dim TempOre As String
                    Dim TempMinuti As String

                    TempOre = Mid(Stringa,1,2)
                    TempMinuti= Mid(Stringa,4,2)
                           
                
                    Appuntamento.OperatoreMedico = val(Dd_Operatore.SelectedValue)
                

                    Try
                        Appuntamento.Ora  = TimeSerial(TempOre,TempMinuti,0)
                    Catch ex As Exception
                        Appuntamento.Ora  = Nothing
                    End Try
                    
            
                    Dim Giorno As String
                    Dim Mese As String
                    Dim Anno As String

                    Giorno =Mid(Stringa,7,2)
                    Mese =Mid(Stringa,10,2)
                    Anno ="20" & Mid(Stringa,13,2)


                    Try
                        Appuntamento.Data = DateSerial(Anno,Mese,Giorno)
                    Catch ex As Exception
                        Appuntamento.Data  = Nothing
                    End Try
                    

                    Dim Testo As String =""
                    Dim TrovaDataNascita As Integer = 0


                    If Stringa.IndexOf("(") > 0 Then
                        TrovaDataNascita  =Stringa.IndexOf("(") 
                    Else
                        If Stringa.IndexOf("Tel.") > 0 Then
                            TrovaDataNascita  =Stringa.IndexOf("Tel.") 
                        End If
                    End If
                    

                    Try
                        Testo = Mid(Stringa,16, TrovaDataNascita -16)
                    Catch ex As Exception
                        Testo = Stringa
                    End Try
                    

                    Dim CognomeNome As String

                    CognomeNome = Testo

                    Appuntamento.Utente = New Cliente

                    Appuntamento.Utente.Cognome = Mid(CognomeNome,1, CognomeNome.IndexOf(" "))
                    Appuntamento.Utente.Nome = CognomeNome.Replace(Appuntamento.Utente.Cognome,"")


                    If Appuntamento.Utente.Cognome =  "TRAMONTINA" Then
                        Appuntamento.Utente.Cognome =  "TRAMONTINA"
                    End If

                    Dim InizioData As Integer

                    InizioData= Stringa.IndexOf("(") +  2

                    If InizioData > 0 then
                
                        Testo =Mid(Stringa,InizioData, 10) 
                
                        Giorno =Mid(Testo,1,2)
                        Mese =Mid(Testo,4,2)
                        Anno = Mid(Testo,7,4)

                        Try
                            Appuntamento.Utente.DataNascita = DateSerial(Anno,Mese,Giorno)
                        Catch ex As Exception
                            Appuntamento.Utente.DataNascita = Nothing
                        End Try
                        
                    Else
                        Appuntamento.Utente.DataNascita = Nothing
                    End If

                    Dim Appoggio As String =""

                    Appoggio = Mid(Stringa, 1, InizioData + 10)
                
                    Appoggio = Replace(Stringa, Appoggio,"")

                    Dim InizioGenere As Integer

                    InizioGenere = Appoggio.IndexOf("(") +2
                    If InizioGenere > 0 Then
                        Appuntamento.Utente.Sesso = Mid(Appoggio,InizioGenere ,1)
                    Else
                        Appuntamento.Utente.Sesso =""
                    End If

                    Dim InizioTel As Integer

                    InizioTel = Appoggio.IndexOf("Tel.") + 6
                    
                    Try
                        Appuntamento.Utente.Telefono1 = Mid(Appoggio, InizioTel, Len(Appoggio) - InizioTel + 1)
                    Catch ex As Exception
                        Appuntamento.Utente.Telefono1 = ""
                    End Try
                    

                End If

                RigaElaborazione = RigaElaborazione + 1
            End if

        Next
        


        If RigaElaborazione = 12 Then
           AppuntamentiFilePDF.Add(Appuntamento)
        End If
            
        

        Session("AppuntamentiFilePDF") = AppuntamentiFilePDF

        Response.Redirect("ConfermaAppuntamentoPDF.ASPX?NUMERO=1")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Barra As New Cls_BarraSenior
        Dim K1 As New Cls_SqlString

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim OperatoreManager As New  OperatoreBiz(Session("DC_OSPITE"))

        Dd_Operatore.DataSource = OperatoreManager.List_For_DropDown()
        Dd_Operatore.DataBind

        
    End Sub


    Public Shared Function GetTextFromPDF(PdfFileName As String) As String
            Dim oReader As New iTextSharp.text.pdf.PdfReader(PdfFileName)

            Dim sOut = ""

            For i = 1 To oReader.NumberOfPages
                Dim its As New iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy

                sOut &= iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(oReader, i, its)
            Next

            Return sOut
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub Img_Import_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Import.Click
        Dim CodiceFile As  Integer = Now.Minute * Now.Hour * Now.Second


        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Import" & CodiceFile &  ".PDF")

        Dim ContenutoPDF As String = GetTextFromPDF(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Import" & CodiceFile &  ".PDF")


        LeggiPdf(ContenutoPDF)
    End Sub

    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Ambulatoriale.ASPX")
    End Sub
End Class