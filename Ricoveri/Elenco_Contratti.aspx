﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Elenco_Contratti.aspx.vb" Inherits="Elenco_Contratti" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elenco Contratti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ricoveri - Elenco Operatori</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">Codice
                                    <asp:TextBox ID="Txt_Codice" Width="100px" runat="server"></asp:TextBox>
                                </td>
                                <td align="left">Descrizione
                                    <asp:TextBox ID="Txt_Descrizione" Width="350px" runat="server"></asp:TextBox>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="~/images/ricerca.png" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ricoveri.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <asp:ImageButton ID="Lnk_ToExcel" runat="server" ToolTip="Esporta griglia in excel" ImageUrl="../images/BTNExel.jpg" class="Effetto" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="Btn_Nuovo" runat="server" ImageUrl="../images/nuovo.png" class="EffettoBottoniTondi" ToolTip="Aggiungi" />
                        </div>

                        <asp:GridView ID="Grd_ImportoOspite" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                            ShowFooter="True" PageSize="20">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server" ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%#   Container.DataItemIndex  %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
