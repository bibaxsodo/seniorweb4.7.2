﻿Public Class Tabelle_DRG
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        

        If Request.Item("CODICE") = "" Then
            Call EseguiJS()

            Dim MaxTpRetta As New Cls_Ric_Contratti

            Txt_Codice.Text = MaxTpRetta.MaxTipoRetta(Session("DC_TABELLE"))
            Exit Sub
        End If


        

        Dim TpAd As New Cls_Ric_Drg


        TpAd.Codice = Request.Item("CODICE")
        TpAd.Leggi(Session("DC_TABELLE"), TpAd.Codice)


        Txt_Codice.Text = TpAd.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = TpAd.Descrizione
       
        Txt_ImportoDayHospital.Text =  Format(TpAd.DayHospital, "#,##0.00") 
        Txt_ImportoIncrementoProDie.Text =  Format(TpAd.IncrementoProDie, "#,##0.00") 
        Txt_Importogiorno1.Text =  Format(TpAd.Giorno1, "#,##0.00") 
        Txt_Importoordinari.Text =  Format(TpAd.ordinari, "#,##0.00") 
        Txt_Soglia.Text = TpAd.Soglia
        
        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function





    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If

        If Txt_ImportoDayHospital.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Day Hospital');", True)
            Exit Sub
        End If
        If Txt_ImportoIncrementoProDie.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Incremento Pro Die');", True)
            Exit Sub
        End If
        If Txt_Importogiorno1.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Giorno 1');", True)
            Exit Sub
        End If
        If Txt_Importoordinari.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice ordinari');", True)
            Exit Sub
        End If
        If Txt_Soglia.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Soglia');", True)
            Exit Sub
        End If

        Dim TpAd As New Cls_Ric_Drg
        

        TpAd.Codice = Txt_Codice.Text
        TpAd.Descrizione = Txt_Descrizione.Text
        
        TpAd.DayHospital = Txt_ImportoDayHospital.Text 
        TpAd.IncrementoProDie = Txt_ImportoIncrementoProDie.Text 
        TpAd.Giorno1  = Txt_Importogiorno1.Text 
        TpAd.ordinari = Txt_Importoordinari.Text 
        TpAd.Soglia=Txt_Soglia.Text         

        TpAd.Scrivi(Session("DC_TABELLE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If
        Dim TpAd As New Cls_Ric_Contratti

        TpAd.Codice = Txt_Codice.Text
        TpAd.Elimina(Session("DC_TABELLE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  " & vbNewLine
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); " & vbNewLine
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });" & vbNewLine
        MyJs = MyJs & "    }" & vbNewLine

        
        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_DRG.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True
        Dim MaxTpRetta As New Cls_Ric_Drg

        Txt_Codice.Text = MaxTpRetta.MaxTipoRetta(Session("DC_TABELLE"))

        Call Txt_Codice_TextChanged(sender, e)
        Call Txt_Descrizione_TextChanged(sender, e)

        Call EseguiJS()
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Ric_Drg

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_TABELLE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
        EseguiJS()
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_Ric_Drg

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione)

                If x.Codice <> "" Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
        EseguiJS()
    End Sub

End Class