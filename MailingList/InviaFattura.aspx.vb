﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Security.Cryptography


Partial Class MailingList_InviaFattura
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim XS As New Cls_Login
        Dim I As Integer
        Dim Stampa As New StampeGenerale

        Dim rpt As New ReportDocument
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then Exit Sub


        Timer1.Enabled = False


        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        Session("OGGETTOMAIL") = Session("SALVAOGGETTO")
        Session("TESTOMAIL") = Session("SALVAMAIL")
        Session("LOGMAIL") = ""



        Dim NomeSt As String = HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT"))


        If Not System.IO.File.Exists(NomeSt) Then
            Response.Redirect("Reportnonpresente.aspx?NOME=" & NomeSt)
            Exit Sub
        End If

        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato(Request.Item("REPORT")))

        Stampa = Session("stampa")

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("Numero", GetType(String))
            Tabella.Columns.Add("Data", GetType(String))
            Tabella.Columns.Add("EMail", GetType(String))
            Tabella.Columns.Add("Esito", GetType(String))

            Tabella.Columns.Add("SndEmail", GetType(String))
            Tabella.Columns.Add("SndOggetto", GetType(String))
            Tabella.Columns.Add("SndTesto", GetType(String))
            Tabella.Columns.Add("SndPath", GetType(String))

            Tabella.Columns.Add("SndCheck", GetType(String))
            Tabella.Columns.Add("SndNumeroRegistrazione", GetType(String))

            Tabella.Columns.Add("OspiteRiferimento", GetType(String))
        Catch ex As Exception

        End Try

        For I = 0 To Stampa.FatturaTesta.Count - 1
            If Stampa.FatturaTesta.Item(I).Email <> "" Then
                If Stampa.FatturaTesta.Item(I).Email.IndexOf("@") > 0 And Stampa.FatturaTesta.Item(I).Email.IndexOf(".") > 0 Then
                    If Session("INVIATE") = "NONINV" Then
                        Dim cn As OleDbConnection
                        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
                        cn.Open()

                        Dim CmdVerificaInvio As New OleDbCommand

                        CmdVerificaInvio.Connection = cn
                        CmdVerificaInvio.CommandText = "Select * From Log_MailInviate where  NumeroRegistrazione = ? And Esito = 'OK'"
                        CmdVerificaInvio.Parameters.AddWithValue("@NumeroRegistrazione", Val(campodb(Stampa.FatturaTesta.Item(I).NumeroRegistrazione)))

                        Dim ReadInvio As OleDbDataReader = CmdVerificaInvio.ExecuteReader()
                        If ReadInvio.Read Then
                            Stampa.FatturaTesta.Item(I).Email = ""
                        End If
                        ReadInvio.Close()

                        cn.Close()
                    End If
                End If
            End If


            If Stampa.FatturaTesta.Item(I).Email <> "" Then
                If Stampa.FatturaTesta.Item(I).Email.IndexOf("@") > 0 And Stampa.FatturaTesta.Item(I).Email.IndexOf(".") > 0 Then

                    rpt.RecordSelectionFormula = "{FatturaTesta.NumeroRegistrazione} = " & Stampa.FatturaTesta.Item(I).NumeroRegistrazione
                    rpt.SetDataSource(Session("stampa"))

                    Dim PathFile As String = "Temp\Fattura_" & Session("CLIENTE") & "_" & Stampa.FatturaTesta.Item(I).NumeroFattura & "_" & Format(Now, "yyyyMMddHHmmss") & ".pdf"

                    rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & PathFile)
                    Dim CognomeNome As String = ""

                    Try
                        CognomeNome = campodb(Stampa.FatturaTesta.Item(I).OspiteRiferimento)
                    Catch ex As Exception

                    End Try

                    Dim Intestatario As String = ""

                    Dim IntestatarioIndirizzo As String = ""
                    Dim IntestatarioComune As String = ""
                    Dim IntestatarioCap As String = ""

                    Try

                        If Not IsNothing(Stampa.FatturaTesta.Item(I).Intestatario) Then
                            If campodb(Stampa.FatturaTesta.Item(I).Intestatario) <> "" Then
                                Intestatario = Stampa.FatturaTesta.Item(I).Intestatario
                            End If
                        End If

                        If Not IsNothing(Stampa.FatturaTesta.Item(I).IntestatarioIndirizzo) Then
                            If campodb(Stampa.FatturaTesta.Item(I).IntestatarioIndirizzo) <> "" Then
                                IntestatarioIndirizzo = Stampa.FatturaTesta.Item(I).IntestatarioIndirizzo
                            End If
                        End If

                        If Not IsNothing(Stampa.FatturaTesta.Item(I).IntestatarioComune) Then
                            If campodb(Stampa.FatturaTesta.Item(I).IntestatarioComune) <> "" Then
                                IntestatarioComune = Stampa.FatturaTesta.Item(I).IntestatarioComune
                            End If
                        End If

                        If Not IsNothing(Stampa.FatturaTesta.Item(I).IntestatarioCap) Then
                            If campodb(Stampa.FatturaTesta.Item(I).IntestatarioCap) <> "" Then
                                IntestatarioCap = Stampa.FatturaTesta.Item(I).IntestatarioCap
                            End If
                        End If

                    Catch ex As Exception

                    End Try


                    Dim Oggetto As String = Session("SALVAOGGETTO")
                    Dim TestoMail As String = Session("SALVAMAIL")
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                    myriga(0) = Stampa.FatturaTesta.Item(I).NumeroFattura
                    myriga(1) = Stampa.FatturaTesta.Item(I).DataFattura
                    myriga(2) = Stampa.FatturaTesta.Item(I).Email
                    myriga(3) = ""

                    myriga(4) = Stampa.FatturaTesta.Item(I).Email

                    Dim Periodo As String = ""
                    If Year(Stampa.FatturaTesta.Item(I).DataFattura) > 1 Then
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 1 Then
                            Periodo = "Gennaio/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 2 Then
                            Periodo = "Febbraio/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 3 Then
                            Periodo = "Marzo/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 4 Then
                            Periodo = "Aprile/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 5 Then
                            Periodo = "Maggio/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 6 Then
                            Periodo = "Giugno/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 7 Then
                            Periodo = "Luglio/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 8 Then
                            Periodo = "Agosto/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 9 Then
                            Periodo = "Settembre/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 10 Then
                            Periodo = "Ottobre/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 11 Then
                            Periodo = "Novembre/"
                        End If
                        If Month(Stampa.FatturaTesta.Item(I).DataFattura) = 12 Then
                            Periodo = "Dicembre/"
                        End If
                        Periodo = Periodo & Year(Stampa.FatturaTesta.Item(I).DataFattura)
                    End If


                    If Not IsNothing(Oggetto) Then
                        myriga(5) = campodb(Oggetto).Replace("@COGNOMENOME", CognomeNome).Replace("@INTESTATARIO", Intestatario).Replace("@INTESTATARIOINDIRIZZO", IntestatarioIndirizzo).Replace("@INTESTATARIOCOMUNE", IntestatarioComune).Replace("@INTESTATARIOCAP", IntestatarioCap).Replace("@PERIODOCOM", Periodo)
                    End If
                    If Not IsNothing(TestoMail) Then
                        myriga(6) = campodb(TestoMail).Replace("@COGNOMENOME", CognomeNome).Replace("@INTESTATARIO", Intestatario).Replace("@INTESTATARIOINDIRIZZO", IntestatarioIndirizzo).Replace("@INTESTATARIOCOMUNE", IntestatarioComune).Replace("@INTESTATARIOCAP", IntestatarioCap).Replace("@PERIODOCOM", Periodo)
                    End If
                    myriga(7) = HostingEnvironment.ApplicationPhysicalPath() & PathFile

                    myriga(8) = ""
                    myriga(9) = Stampa.FatturaTesta.Item(I).NumeroRegistrazione

                    Try
                        myriga(10) = Stampa.FatturaTesta.Item(I).OspiteRiferimento
                    Catch ex As Exception

                    End Try


                    Tabella.Rows.Add(myriga)
                End If
            End If
        Next

        Session("InvioMail") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

    End Sub


    Public Function SendAnEmail(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal Allegato As String, ByVal NumeroRegistrazione As Long) As String

        Dim Kx As New Cls_Login

        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))

        Dim NomeSocieta As String

        NomeSocieta = Kx.RagioneSociale

        Try

            Using msg As New Net.Mail.MailMessage(MsgFrom, MsgTo, MsgSubject, MsgBody)
                msg.IsBodyHtml = True
                Dim mailClient As SmtpClient = Nothing
                If Kx.Usa_SMTP_Default Then
                    mailClient = New SmtpClient("smtp.sendgrid.net")
                    mailClient.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings("Senior_DefaultSmtpUser"), ConfigurationManager.AppSettings("Senior_DefaultSmtpPassword"))
                    mailClient.EnableSsl = False
                    mailClient.Port = 587
                Else
                    if Kx.SMTP = "smtp.gmail.com" Or Kx.SMTP = "smtp.office365.com" Or Kx.SMTP = "smtps.aruba.it" Or Kx.SMTP = "ns0.ovh.net" Or Kx.SMTP = "sicuro.nephila.it" Then
                        mailClient = New SmtpClient(Kx.SMTP, 587)  '  = local machine IP Address
                        mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)
                        mailClient.EnableSsl = True
                    Else
                        If Kx.Porta587 = 1 then
                            mailClient = New SmtpClient(Kx.SMTP, 587)  '  = local machine IP Address
                            mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)
                            If Kx.SSL = 1 Then
                                mailClient.EnableSsl = True
                            End If
                        Else
                            mailClient = New SmtpClient(Kx.SMTP)  '  = local machine IP Address
                            mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)
                            If Kx.SSL = 1 Then
                                mailClient.EnableSsl = True
                            End If
                        End If
                    End If
                End If


                GeneraAttachment(Allegato, NomeSocieta, msg)
                
                Dim Param As New Cls_Parametri
                Param.LeggiParametri(Session("DC_OSPITE"))

                If Param.AbilitaQrCodeAddAcc = 1 Then                
                   CercaAddebiti(NumeroRegistrazione, msg)
                   CercaAllegatiDocumenti(NumeroRegistrazione, msg)
                   CercaDocumentiLocal(NumeroRegistrazione, msg)
                End If

                If mailClient IsNot Nothing Then
                    mailClient.Send(msg)
                End If
            End Using

            Return "OK"

        Catch ex As FormatException
            Return (ex.Message & " :Format Exception")
        Catch ex As SmtpException
            Return (ex.Message & " :SMTP Exception")
        End Try

    End Function

    Private Sub GeneraAttachment(Allegato As String, NomeSocieta As String, ByRef msg As Net.Mail.MailMessage)
        Dim Appo As String = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati", FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        End If

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Session("SALVAMAILID"), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Session("SALVAMAILID"))
        End If

        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Session("SALVAMAILID") & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo


        aryItemsInfo = objDI.GetFileSystemInfos()

        For Each objItem In aryItemsInfo
            Dim MyAllegato As String

            MyAllegato = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Session("SALVAMAILID") & "\" & objItem.Name

            Dim at1 As New Attachment(MyAllegato)

            msg.Attachments.Add(at1)

        Next


        Dim at As New Attachment(Allegato)
        msg.Attachments.Add(at)
    End Sub

    Private Sub CercaAddebiti(ByVal NumeroRegistrazione As Long, ByRef msg As Net.Mail.MailMessage)
        


            Dim cnOspiti As OleDbConnection

            cnOspiti = New System.Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
            Dim CodiceOspite As Integer = 0
            Dim Registrazione As New Cls_MovimentoContabile
            Dim Indice As Integer

            Registrazione.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)
            For Indice = 0 To 20
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).RigaDaCausale = 1 Then
                        CodiceOspite = Int(Registrazione.Righe(Indice).SottocontoPartita / 100)
                    End If
                End If
            Next

            cnOspiti.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "SELECT * From ADDACR where NumeroRegistrazione = ?"
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmd.Connection = cnOspiti

            Dim Read As OleDbDataReader = cmd.ExecuteReader()
            Do While Read.Read

                Try
                    Dim NomeSocieta As String

                    Dim k2 As New Cls_Login

                    k2.Utente = Session("UTENTE")
                    k2.LeggiSP(Application("SENIOR"))

                    NomeSocieta = k2.RagioneSociale

                    If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & CodiceOspite & "\" & "AdAC_" & Val(campodb(Read.Item("ID"))) & "\1.jpg") <> "" Then
                        Dim at As New Attachment(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & CodiceOspite & "\" & "AdAC_" & Val(campodb(Read.Item("ID"))) & "\1.jpg")
                        msg.Attachments.Add(at)
                    End If
                Catch ex As Exception

                End Try
            Loop
            cnOspiti.Close()        
    End Sub

    
    Private Sub CercaDocumentiLocal(ByVal NumeroRegistrazione As Long, ByRef msg As Net.Mail.MailMessage)
        Dim cnOspiti As OleDbConnection

        cnOspiti = New System.Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        Dim CodiceOspite As Integer = 0
        Dim Registrazione As New Cls_MovimentoContabile
        Dim Indice As Integer = 0

        Registrazione.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)
        For Indice = 0 To 20
            If Not IsNothing(Registrazione.Righe(Indice)) Then
                If Registrazione.Righe(Indice).RigaDaCausale = 1 Then
                    CodiceOspite = Int(Registrazione.Righe(Indice).SottocontoPartita / 100)
                End If
            End If
        Next


        cnOspiti.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * From Documentazione where CodiceOspite = ? And InvioMail = 1"
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Connection = cnOspiti

        Dim Read As OleDbDataReader = cmd.ExecuteReader()
        Do While Read.Read

            Try
                Dim NomeSocieta As String


                Dim k As New Cls_Login

                k.Utente = Session("UTENTE")
                k.LeggiSP(Application("SENIOR"))

                NomeSocieta = k.RagioneSociale

                Dim NomeFile As String
                NomeFile = campodb(Read.Item("Nome"))

                If NomeFile.IndexOf("Crypt_") >= 0 Then
                    Decrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & CodiceOspite & "\" & NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & CodiceOspite & NomeFile.Replace("Crypt_", ""))
                End If


                If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & CodiceOspite & NomeFile.Replace("Crypt_", "")) <> "" Then
                    Dim at As New Attachment(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & CodiceOspite & NomeFile.Replace("Crypt_", ""))
                    msg.Attachments.Add(at)
                End If

                Dim cmdUpdate As New OleDbCommand()
                cmdUpdate.CommandText = "UPDATE   Documentazione SET DataInvio = ?,InvioMail = 2 where id= ?"
                cmdUpdate.Parameters.AddWithValue("@DataInvio", Now)
                cmdUpdate.Parameters.AddWithValue("@id", Val(campodb(Read.Item("ID"))))
                cmdUpdate.Connection = cnOspiti
                cmdUpdate.ExecuteNonQuery()

            Catch ex As Exception

            End Try
        Loop


        cnOspiti.Close()


    End Sub
    Private Sub Decrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(inputFilePath, FileMode.Open)
                Using cs As New CryptoStream(fs, encryptor.CreateDecryptor(), CryptoStreamMode.Read)
                    Using fsOutput As New FileStream(outputfilePath, FileMode.Create)
                        Dim data As Integer
                        While (Assign(data, cs.ReadByte())) <> -1
                            fsOutput.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Shared Function Assign(Of T)(ByRef source As T, ByVal value As T) As T
        source = value
        Return value
    End Function

    Private Sub CercaAllegatiDocumenti(ByVal NumeroRegistrazione As Long, ByRef msg As Net.Mail.MailMessage)

        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo

            If objItem.Name.IndexOf("Copia_") < 0 Then
                Dim Dati As String = ""
                Dim CopiaNomeFile As String = ""


                CopiaNomeFile = "Copia_" & objItem.Name

                Dim readFile As System.IO.TextReader = New StreamReader(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & objItem.Name)

                Dati = readFile.ReadToEnd()


                Dim binaryData() As Byte = Convert.FromBase64String(Dati)


                Try
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & NumeroRegistrazione & "_" & CopiaNomeFile)
                Catch ex As Exception

                End Try

                Dim fs As New FileStream(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & NumeroRegistrazione & "_" & CopiaNomeFile, FileMode.CreateNew)


                fs.Write(binaryData, 0, binaryData.Length)
                fs.Close()

                Try
                    Dim at As New Attachment(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & NumeroRegistrazione & "_" & CopiaNomeFile)

                    msg.Attachments.Add(at)
                Finally
                    Try
                        Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Temp\" & NomeSocieta & "_" & NumeroRegistrazione & "_" & CopiaNomeFile)
                    Catch ex As Exception

                    End Try
                End Try
            End If
        Next        
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Tabella = Session("InvioMail")

        Session("LOGMAIL") = "<tr class=""miotr""><th class=""miaintestazione"">Numero Documento</th><th class=""miaintestazione"">Data Documento</th><th class=""miaintestazione"">Esito</th><th class=""miaintestazione"">E-Mail</th><th class=""miaintestazione"">Oggetto</th><th class=""miaintestazione"">Data Ora Invio</th></tr>"

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkInvia"), CheckBox)
            If CheckBox.Checked = True Then
                Tabella.Rows(Riga).Item(8) = "Checked"
            End If
        Next

        lblWaiting.Text = "<center><img src=""../images/loading.gif"" /></center>"

        Session("InvioMail") = Tabella
        Timer1.Interval = 2000
        Timer1.Enabled = True
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim Riga As Integer
        Dim Entratato As Boolean = False
        Dim XS As New Cls_Login
        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Tabella = Session("InvioMail")

        If IsNothing(Tabella) Then
            Timer1.Enabled = False
            Exit Sub
        End If

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim SenzaEsito As Boolean = True
            If Not IsNothing(Tabella.Rows(Riga).Item(3)) Then
                If campodb(Tabella.Rows(Riga).Item(3)) <> "" Then
                    SenzaEsito = False
                End If
            Else
                SenzaEsito = False
            End If
            If Tabella.Rows(Riga).Item(8) = "Checked" And SenzaEsito = True Then

                Dim cn As OleDbConnection
                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
                cn.Open()

                Dim Registrazione As New Cls_MovimentoContabile

                Registrazione.NumeroRegistrazione = Val(campodb(Tabella.Rows(Riga).Item(9)))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                Dim Conto As New Cls_Pianodeiconti

                Conto.Mastro = Registrazione.Righe(0).MastroPartita
                Conto.Conto = Registrazione.Righe(0).ContoPartita
                Conto.Sottoconto = Int(Registrazione.Righe(0).SottocontoPartita / 100) * 100
                Conto.Decodfica(Session("DC_GENERALE"))

                If Conto.Descrizione.ToUpper.Trim.Replace(" ", "") = Tabella.Rows(Riga).Item(10).ToString.ToUpper.Trim.Replace(" ", "") Then

                    Dim CmdVerificaInvio As New OleDbCommand

                    CmdVerificaInvio.Connection = cn
                    CmdVerificaInvio.CommandText = "Select * From Log_MailInviate where IdMail = ? And Year(DataOra) = ? and Month(DataOra) = ? and Day(DataOra) = ? And NumeroRegistrazione = ? And Esito = 'OK'"
                    CmdVerificaInvio.Parameters.AddWithValue("@IdMail", Session("SALVAMAILID"))
                    CmdVerificaInvio.Parameters.AddWithValue("@DataOra", Year(Now))
                    CmdVerificaInvio.Parameters.AddWithValue("@DataOra", Month(Now))
                    CmdVerificaInvio.Parameters.AddWithValue("@DataOra", Day(Now))
                    CmdVerificaInvio.Parameters.AddWithValue("@NumeroRegistrazione", Val(campodb(Tabella.Rows(Riga).Item(9))))

                    Dim ReadInvio As OleDbDataReader = CmdVerificaInvio.ExecuteReader()
                    If Not ReadInvio.Read Then
                        Tabella.Rows(Riga).Item(3) = SendAnEmail(XS.EMail, campodb(Tabella.Rows(Riga).Item(4)), campodb(Tabella.Rows(Riga).Item(5)), campodb(Tabella.Rows(Riga).Item(6)), Tabella.Rows(Riga).Item(7), Val(Tabella.Rows(Riga).Item(9)))

                        Session("LOGMAIL") = Session("LOGMAIL") & "<tr class=""miotr""><td class=""miacella"">" & Tabella.Rows(Riga).Item(0) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(1) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(3) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(4)) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(5)) & "</td><td class=""miacella"">" & Format(Now, "dd/MM/yyyy hh:mm:ss") & "</td></tr>"

                        Dim cmdIns As New OleDbCommand()
                        cmdIns.CommandText = ("Insert Into Log_MailInviate ([IdMail],[CodiceOspite],[CodiceParente],[CodicePorvincia],[CodiceComune],[Regione],[CodiceDebitoreCreditore],[CodiceMedico],[IdMailingList],[Mail],[DataOra],[Utente],[Esito],[Oggetto],[CorpoMail],[MailInvio],NumeroRegistrazione)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                        cmdIns.Parameters.AddWithValue("@IdMail", Session("SALVAMAILID"))
                        cmdIns.Parameters.AddWithValue("@CodiceOspite", 0)
                        cmdIns.Parameters.AddWithValue("@CodiceParente", 0)
                        cmdIns.Parameters.AddWithValue("@CodicePorvincia", "")
                        cmdIns.Parameters.AddWithValue("@CodiceComune", "")
                        cmdIns.Parameters.AddWithValue("@Regione", "")
                        cmdIns.Parameters.AddWithValue("@CodiceDebitoreCreditore", "")
                        cmdIns.Parameters.AddWithValue("@CodiceMedico", "")
                        cmdIns.Parameters.AddWithValue("@IdMailingList", 0)

                        If Len(campodb(Tabella.Rows(Riga).Item(4))) > 100 Then
                            cmdIns.Parameters.AddWithValue("@Mail", Mid(campodb(Tabella.Rows(Riga).Item(4)), 1, 100))
                        Else
                            cmdIns.Parameters.AddWithValue("@Mail", campodb(Tabella.Rows(Riga).Item(4)))
                        End If
                        cmdIns.Parameters.AddWithValue("@DataOra", Now)
                        If Len(Session("UTENTE")) > 50 Then
                            cmdIns.Parameters.AddWithValue("@Utente", Mid(Session("UTENTE"), 1, 50))
                        Else
                            cmdIns.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                        End If
                        If Len(Tabella.Rows(Riga).Item(3)) > 50 Then
                            cmdIns.Parameters.AddWithValue("@Esito", Mid(Tabella.Rows(Riga).Item(3), 1, 50))
                        Else
                            cmdIns.Parameters.AddWithValue("@Esito", Tabella.Rows(Riga).Item(3))
                        End If
                        If Len(Tabella.Rows(Riga).Item(5)) > 100 Then
                            cmdIns.Parameters.AddWithValue("@Oggetto", Mid(campodb(Tabella.Rows(Riga).Item(5)), 1, 100))
                        Else
                            cmdIns.Parameters.AddWithValue("@Oggetto", campodb(Tabella.Rows(Riga).Item(5)))
                        End If
                        If Len(Tabella.Rows(Riga).Item(6)) > 500 Then
                            cmdIns.Parameters.AddWithValue("@CorpoMail", Mid(campodb(Tabella.Rows(Riga).Item(6)), 1, 500))
                        Else
                            cmdIns.Parameters.AddWithValue("@CorpoMail", campodb(Tabella.Rows(Riga).Item(6)))
                        End If
                        If Len(Tabella.Rows(Riga).Item(5)) > 100 Then
                            cmdIns.Parameters.AddWithValue("@MailInvio", Mid(XS.EMail, 1, 100))
                        Else
                            cmdIns.Parameters.AddWithValue("@MailInvio", XS.EMail)
                        End If
                        cmdIns.Parameters.AddWithValue("@NumeroRegistrazione", Val(campodb(Tabella.Rows(Riga).Item(9))))
                        cmdIns.Connection = cn
                        cmdIns.ExecuteNonQuery()

                        cn.Close()
                        Entratato = True
                    Else
                        Tabella.Rows(Riga).Item(3) = "ERRORE GIA' INVIATA"

                        Session("LOGMAIL") = Session("LOGMAIL") & "<tr class=""miotr""><td class=""miacella"">" & Tabella.Rows(Riga).Item(0) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(1) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(3) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(4)) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(5)) & "</td><td class=""miacella"">" & Format(Now, "dd/MM/yyyy hh:mm:ss") & "</td></tr>"
                        Entratato = True
                    End If
                    ReadInvio.Close()

                    GridView1.DataSource = Tabella
                    GridView1.DataBind()
                    Session("InvioMail") = Tabella
                    Exit For
                Else
                    Tabella.Rows(Riga).Item(3) = "ERRORE OSPITE RIFERIMENTO NON CORRISPONE CON FATTURA"

                    Session("LOGMAIL") = Session("LOGMAIL") & "<tr class=""miotr""><td class=""miacella"">" & Tabella.Rows(Riga).Item(0) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(1) & "</td><td class=""miacella"">" & Tabella.Rows(Riga).Item(3) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(4)) & "</td><td class=""miacella"">" & campodb(Tabella.Rows(Riga).Item(5)) & "</td><td class=""miacella"">" & Format(Now, "dd/MM/yyyy hh:mm:ss") & "</td></tr>"
                    Entratato = True

                    GridView1.DataSource = Tabella
                    GridView1.DataBind()
                    Session("InvioMail") = Tabella

                End If
            End If
        Next
        If Not Entratato Then
            lblWaiting.Text = ""
            Timer1.Enabled = False
            Response.Redirect("MailInviate.aspx")
        End If
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Try
            Tabella = Session("InvioMail")

            If Tabella.Rows(e.Row.RowIndex).Item(8) = "Checked" Then
                Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkInvia"), CheckBox)
                CheckBox.Checked = True

            End If
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkInvia"), CheckBox)
            CheckBox.Checked = True
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("InviaFatture.aspx")
    End Sub
End Class
