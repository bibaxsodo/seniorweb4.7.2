


<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title>numeric/jquery.numeric.js at master from SamWM/jQuery-Plugins - GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />

    
    

    <meta content="authenticity_token" name="csrf-param" />
<meta content="9ffe0277ef1c4245dcc80c4be9040b9208705f05" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/stylesheets/bundle_github.css?6b5d692977b08742ec00aa5dab2fd958e92f1410" media="screen" rel="stylesheet" type="text/css" />
    

    <script src="https://a248.e.akamai.net/assets.github.com/javascripts/bundle_jquery.js?1cda878e9113acce74087d5ff1a5e1c49fb94eeb" type="text/javascript"></script>
    <script src="https://a248.e.akamai.net/assets.github.com/javascripts/bundle_github.js?2dc50814e43ab0e16d507dc6c08dd427d1c1b301" type="text/javascript"></script>
    

      <link rel='permalink' href='/SamWM/jQuery-Plugins/blob/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a/numeric/jquery.numeric.js'>
    

    <meta name="description" content="jQuery-Plugins - Plugins I have developed for jQuery" />
  <link href="https://github.com/SamWM/jQuery-Plugins/commits/master.atom" rel="alternate" title="Recent Commits to jQuery-Plugins:master" type="application/atom+xml" />

  </head>


  <body class="logged_out page-blob windows env-production ">
    


    

    <div id="main">
      <div id="header" class="true">
          <a class="logo" href="https://github.com">
            <img alt="github" class="default svg" height="45" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov6.svg" />
            <img alt="github" class="default png" height="45" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov6.png" />
            <!--[if (gt IE 8)|!(IE)]><!-->
            <img alt="github" class="hover svg" height="45" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov6-hover.svg" />
            <img alt="github" class="hover png" height="45" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov6-hover.png" />
            <!--<![endif]-->
          </a>

        <div class="topsearch">
    <!--
      make sure to use fully qualified URLs here since this nav
      is used on error pages on other domains
    -->
    <ul class="nav logged_out">
        <li class="pricing"><a href="https://github.com/plans">Signup and Pricing</a></li>
        <li class="explore"><a href="https://github.com/explore">Explore GitHub</a></li>
      <li class="features"><a href="https://github.com/features">Features</a></li>
        <li class="blog"><a href="https://github.com/blog">Blog</a></li>
      <li class="login"><a href="https://github.com/login?return_to=%2FSamWM%2FjQuery-Plugins%2Fblob%2Fmaster%2Fnumeric%2Fjquery.numeric.js">Login</a></li>
    </ul>
</div>

      </div>

      
            <div class="site">
      <div class="pagehead repohead vis-public   instapaper_ignore readability-menu">


      <div class="title-actions-bar">
        <h1>
          <a href="/SamWM">SamWM</a> /
          <strong><a href="/SamWM/jQuery-Plugins" class="js-current-repository">jQuery-Plugins</a></strong>
        </h1>
        



            <ul class="pagehead-actions">

        <li>
            <a href="/SamWM/jQuery-Plugins/toggle_watch" class="minibutton btn-watch watch-button" data-method="post"><span><span class="icon"></span>Watch</span></a>
        </li>
            <li><a href="/SamWM/jQuery-Plugins/fork" class="minibutton btn-fork fork-button" data-method="post"><span><span class="icon"></span>Fork</span></a></li>

      <li class="repostats">
        <ul class="repo-stats">
          <li class="watchers ">
            <a href="/SamWM/jQuery-Plugins/watchers" title="Watchers" class="tooltipped downwards">
              116
            </a>
          </li>
          <li class="forks">
            <a href="/SamWM/jQuery-Plugins/network" title="Forks" class="tooltipped downwards">
              26
            </a>
          </li>
        </ul>
      </li>
    </ul>

      </div>

        

  <ul class="tabs">
    <li><a href="/SamWM/jQuery-Plugins" class="selected" highlight="repo_sourcerepo_downloadsrepo_commitsrepo_tagsrepo_branches">Code</a></li>
    <li><a href="/SamWM/jQuery-Plugins/network" highlight="repo_networkrepo_fork_queue">Network</a>
    <li><a href="/SamWM/jQuery-Plugins/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>5</span></a></li>

      <li><a href="/SamWM/jQuery-Plugins/issues" highlight="repo_issues">Issues <span class='counter'>17</span></a></li>


    <li><a href="/SamWM/jQuery-Plugins/graphs" highlight="repo_graphsrepo_contributors">Stats &amp; Graphs</a></li>

  </ul>

  
<div class="frame frame-center tree-finder" style="display:none"
      data-tree-list-url="/SamWM/jQuery-Plugins/tree-list/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a"
      data-blob-url-prefix="/SamWM/jQuery-Plugins/blob/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a"
    >

  <div class="breadcrumb">
    <b><a href="/SamWM/jQuery-Plugins">jQuery-Plugins</a></b> /
    <input class="tree-finder-input" type="text" name="query" autocomplete="off" spellcheck="false">
  </div>

    <div class="octotip">
      <p>
        <a href="/SamWM/jQuery-Plugins/dismiss-tree-finder-help" class="dismiss js-dismiss-tree-list-help" title="Hide this notice forever">Dismiss</a>
        <strong>Octotip:</strong> You've activated the <em>file finder</em>
        by pressing <span class="kbd">t</span> Start typing to filter the
        file list. Use <span class="kbd badmono">↑</span> and
        <span class="kbd badmono">↓</span> to navigate,
        <span class="kbd">enter</span> to view files.
      </p>
    </div>

  <table class="tree-browser" cellpadding="0" cellspacing="0">
    <tr class="js-header"><th>&nbsp;</th><th>name</th></tr>
    <tr class="js-no-results no-results" style="display: none">
      <th colspan="2">No matching files</th>
    </tr>
    <tbody class="js-results-list">
    </tbody>
  </table>
</div>

<div id="jump-to-line" style="display:none">
  <h2>Jump to Line</h2>
  <form>
    <input class="textfield" type="text">
    <div class="full-button">
      <button type="submit" class="classy">
        <span>Go</span>
      </button>
    </div>
  </form>
</div>


<div class="subnav-bar">

  <ul class="actions">
    
      <li class="switcher">

        <div class="context-menu-container js-menu-container">
          <span class="text">Current branch:</span>
          <a href="#"
             class="minibutton bigger switcher context-menu-button js-menu-target js-commitish-button btn-branch repo-tree"
             data-master-branch="master"
             data-ref="master">
            <span><span class="icon"></span>master</span>
          </a>

          <div class="context-pane commitish-context js-menu-content">
            <a href="javascript:;" class="close js-menu-close"></a>
            <div class="title">Switch Branches/Tags</div>
            <div class="body pane-selector commitish-selector js-filterable-commitishes">
              <div class="filterbar">
                <div class="placeholder-field js-placeholder-field">
                  <label class="placeholder" for="context-commitish-filter-field" data-placeholder-mode="sticky">Filter branches/tags</label>
                  <input type="text" id="context-commitish-filter-field" class="commitish-filter" />
                </div>

                <ul class="tabs">
                  <li><a href="#" data-filter="branches" class="selected">Branches</a></li>
                  <li><a href="#" data-filter="tags">Tags</a></li>
                </ul>
              </div>

                <div class="commitish-item branch-commitish selector-item">
                  <h4>
                      <a href="/SamWM/jQuery-Plugins/blob/master/numeric/jquery.numeric.js" data-name="master">master</a>
                  </h4>
                </div>


              <div class="no-results" style="display:none">Nothing to show</div>
            </div>
          </div><!-- /.commitish-context-context -->
        </div>

      </li>
  </ul>

  <ul class="subnav">
    <li><a href="/SamWM/jQuery-Plugins" class="selected" highlight="repo_source">Files</a></li>
    <li><a href="/SamWM/jQuery-Plugins/commits/master" highlight="repo_commits">Commits</a></li>
    <li><a href="/SamWM/jQuery-Plugins/branches" class="" highlight="repo_branches">Branches <span class="counter">1</span></a></li>
    <li><a href="/SamWM/jQuery-Plugins/tags" class="blank" highlight="repo_tags">Tags <span class="counter">0</span></a></li>
    <li><a href="/SamWM/jQuery-Plugins/downloads" class="blank" highlight="repo_downloads">Downloads <span class="counter">0</span></a></li>
  </ul>

</div>

  
  
  


        

      </div><!-- /.pagehead -->

      




  
  <p class="last-commit">Latest commit to the <strong>master</strong> branch</p>

<div class="commit commit-tease js-details-container">
  <p class="commit-title ">
      <a href="/SamWM/jQuery-Plugins/tree/master/numeric/"><a href="/SamWM/jQuery-Plugins/commit/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a" class="message">Option to prevent negative values from being entered. Fix text when it i...</a></a>
      <a href="javascript:;" class="minibutton expander-minibutton js-details-target"><span>…</span></a>
  </p>
    <div class="commit-desc"><pre>...s pasted via Ctrl+V, though not if you use the mouse (can use callback to prevent the form being submitted, or clear the box)</pre></div>
  <div class="commit-meta">
    <a href="/SamWM/jQuery-Plugins/commit/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a" class="sha-block">commit <span class="sha">014aa5a84b</span></a>

    <div class="authorship">
      <img class="gravatar" height="20" src="https://secure.gravatar.com/avatar/343d7ac92353b21993bbb9619a02e2e0?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-140.png" width="20" />
      <span class="author-name"><a href="/SamWM">SamWM</a></span>
      authored <time class="js-relative-date" datetime="2011-05-08T11:09:07-07:00" title="2011-05-08 11:09:07">May 08, 2011</time>

    </div>
  </div>
</div>


  <div id="slider">

    <div class="breadcrumb" data-path="numeric/jquery.numeric.js/">
      <b><a href="/SamWM/jQuery-Plugins/tree/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a" class="js-rewrite-sha">jQuery-Plugins</a></b> / <a href="/SamWM/jQuery-Plugins/tree/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a/numeric" class="js-rewrite-sha">numeric</a> / jquery.numeric.js       <span style="display:none" id="clippy_795" class="clippy-text">numeric/jquery.numeric.js</span>
      
      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
              width="110"
              height="14"
              class="clippy"
              id="clippy" >
      <param name="movie" value="https://a248.e.akamai.net/assets.github.com/flash/clippy.swf?v5"/>
      <param name="allowScriptAccess" value="always" />
      <param name="quality" value="high" />
      <param name="scale" value="noscale" />
      <param NAME="FlashVars" value="id=clippy_795&amp;copied=copied!&amp;copyto=copy to clipboard">
      <param name="bgcolor" value="#FFFFFF">
      <param name="wmode" value="opaque">
      <embed src="https://a248.e.akamai.net/assets.github.com/flash/clippy.swf?v5"
             width="110"
             height="14"
             name="clippy"
             quality="high"
             allowScriptAccess="always"
             type="application/x-shockwave-flash"
             pluginspage="http://www.macromedia.com/go/getflashplayer"
             FlashVars="id=clippy_795&amp;copied=copied!&amp;copyto=copy to clipboard"
             bgcolor="#FFFFFF"
             wmode="opaque"
      />
      </object>
      

    </div>

    <div class="frames">
      <div class="frame frame-center" data-path="numeric/jquery.numeric.js/" data-permalink-url="/SamWM/jQuery-Plugins/blob/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a/numeric/jquery.numeric.js" data-title="numeric/jquery.numeric.js at master from SamWM/jQuery-Plugins - GitHub" data-type="blob">
          <ul class="big-actions">
            <li><a class="file-edit-link minibutton js-rewrite-sha" href="/SamWM/jQuery-Plugins/edit/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a/numeric/jquery.numeric.js" data-method="post"><span>Edit this file</span></a></li>
          </ul>

        <div id="files">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><img alt="Txt" height="16" src="https://a248.e.akamai.net/assets.github.com/images/icons/txt.png" width="16" /></span>
                <span class="mode" title="File Mode">100644</span>
                  <span>279 lines (270 sloc)</span>
                <span>8.387 kb</span>
              </div>
              <ul class="actions">
                <li><a href="/SamWM/jQuery-Plugins/raw/master/numeric/jquery.numeric.js" id="raw-url">raw</a></li>
                  <li><a href="/SamWM/jQuery-Plugins/blame/master/numeric/jquery.numeric.js">blame</a></li>
                <li><a href="/SamWM/jQuery-Plugins/commits/master/numeric/jquery.numeric.js">history</a></li>
              </ul>
            </div>
              <div class="data type-javascript">
      <table cellpadding="0" cellspacing="0" class="lines">
        <tr>
          <td>
            <pre class="line_numbers"><span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
<span id="L97" rel="#L97">97</span>
<span id="L98" rel="#L98">98</span>
<span id="L99" rel="#L99">99</span>
<span id="L100" rel="#L100">100</span>
<span id="L101" rel="#L101">101</span>
<span id="L102" rel="#L102">102</span>
<span id="L103" rel="#L103">103</span>
<span id="L104" rel="#L104">104</span>
<span id="L105" rel="#L105">105</span>
<span id="L106" rel="#L106">106</span>
<span id="L107" rel="#L107">107</span>
<span id="L108" rel="#L108">108</span>
<span id="L109" rel="#L109">109</span>
<span id="L110" rel="#L110">110</span>
<span id="L111" rel="#L111">111</span>
<span id="L112" rel="#L112">112</span>
<span id="L113" rel="#L113">113</span>
<span id="L114" rel="#L114">114</span>
<span id="L115" rel="#L115">115</span>
<span id="L116" rel="#L116">116</span>
<span id="L117" rel="#L117">117</span>
<span id="L118" rel="#L118">118</span>
<span id="L119" rel="#L119">119</span>
<span id="L120" rel="#L120">120</span>
<span id="L121" rel="#L121">121</span>
<span id="L122" rel="#L122">122</span>
<span id="L123" rel="#L123">123</span>
<span id="L124" rel="#L124">124</span>
<span id="L125" rel="#L125">125</span>
<span id="L126" rel="#L126">126</span>
<span id="L127" rel="#L127">127</span>
<span id="L128" rel="#L128">128</span>
<span id="L129" rel="#L129">129</span>
<span id="L130" rel="#L130">130</span>
<span id="L131" rel="#L131">131</span>
<span id="L132" rel="#L132">132</span>
<span id="L133" rel="#L133">133</span>
<span id="L134" rel="#L134">134</span>
<span id="L135" rel="#L135">135</span>
<span id="L136" rel="#L136">136</span>
<span id="L137" rel="#L137">137</span>
<span id="L138" rel="#L138">138</span>
<span id="L139" rel="#L139">139</span>
<span id="L140" rel="#L140">140</span>
<span id="L141" rel="#L141">141</span>
<span id="L142" rel="#L142">142</span>
<span id="L143" rel="#L143">143</span>
<span id="L144" rel="#L144">144</span>
<span id="L145" rel="#L145">145</span>
<span id="L146" rel="#L146">146</span>
<span id="L147" rel="#L147">147</span>
<span id="L148" rel="#L148">148</span>
<span id="L149" rel="#L149">149</span>
<span id="L150" rel="#L150">150</span>
<span id="L151" rel="#L151">151</span>
<span id="L152" rel="#L152">152</span>
<span id="L153" rel="#L153">153</span>
<span id="L154" rel="#L154">154</span>
<span id="L155" rel="#L155">155</span>
<span id="L156" rel="#L156">156</span>
<span id="L157" rel="#L157">157</span>
<span id="L158" rel="#L158">158</span>
<span id="L159" rel="#L159">159</span>
<span id="L160" rel="#L160">160</span>
<span id="L161" rel="#L161">161</span>
<span id="L162" rel="#L162">162</span>
<span id="L163" rel="#L163">163</span>
<span id="L164" rel="#L164">164</span>
<span id="L165" rel="#L165">165</span>
<span id="L166" rel="#L166">166</span>
<span id="L167" rel="#L167">167</span>
<span id="L168" rel="#L168">168</span>
<span id="L169" rel="#L169">169</span>
<span id="L170" rel="#L170">170</span>
<span id="L171" rel="#L171">171</span>
<span id="L172" rel="#L172">172</span>
<span id="L173" rel="#L173">173</span>
<span id="L174" rel="#L174">174</span>
<span id="L175" rel="#L175">175</span>
<span id="L176" rel="#L176">176</span>
<span id="L177" rel="#L177">177</span>
<span id="L178" rel="#L178">178</span>
<span id="L179" rel="#L179">179</span>
<span id="L180" rel="#L180">180</span>
<span id="L181" rel="#L181">181</span>
<span id="L182" rel="#L182">182</span>
<span id="L183" rel="#L183">183</span>
<span id="L184" rel="#L184">184</span>
<span id="L185" rel="#L185">185</span>
<span id="L186" rel="#L186">186</span>
<span id="L187" rel="#L187">187</span>
<span id="L188" rel="#L188">188</span>
<span id="L189" rel="#L189">189</span>
<span id="L190" rel="#L190">190</span>
<span id="L191" rel="#L191">191</span>
<span id="L192" rel="#L192">192</span>
<span id="L193" rel="#L193">193</span>
<span id="L194" rel="#L194">194</span>
<span id="L195" rel="#L195">195</span>
<span id="L196" rel="#L196">196</span>
<span id="L197" rel="#L197">197</span>
<span id="L198" rel="#L198">198</span>
<span id="L199" rel="#L199">199</span>
<span id="L200" rel="#L200">200</span>
<span id="L201" rel="#L201">201</span>
<span id="L202" rel="#L202">202</span>
<span id="L203" rel="#L203">203</span>
<span id="L204" rel="#L204">204</span>
<span id="L205" rel="#L205">205</span>
<span id="L206" rel="#L206">206</span>
<span id="L207" rel="#L207">207</span>
<span id="L208" rel="#L208">208</span>
<span id="L209" rel="#L209">209</span>
<span id="L210" rel="#L210">210</span>
<span id="L211" rel="#L211">211</span>
<span id="L212" rel="#L212">212</span>
<span id="L213" rel="#L213">213</span>
<span id="L214" rel="#L214">214</span>
<span id="L215" rel="#L215">215</span>
<span id="L216" rel="#L216">216</span>
<span id="L217" rel="#L217">217</span>
<span id="L218" rel="#L218">218</span>
<span id="L219" rel="#L219">219</span>
<span id="L220" rel="#L220">220</span>
<span id="L221" rel="#L221">221</span>
<span id="L222" rel="#L222">222</span>
<span id="L223" rel="#L223">223</span>
<span id="L224" rel="#L224">224</span>
<span id="L225" rel="#L225">225</span>
<span id="L226" rel="#L226">226</span>
<span id="L227" rel="#L227">227</span>
<span id="L228" rel="#L228">228</span>
<span id="L229" rel="#L229">229</span>
<span id="L230" rel="#L230">230</span>
<span id="L231" rel="#L231">231</span>
<span id="L232" rel="#L232">232</span>
<span id="L233" rel="#L233">233</span>
<span id="L234" rel="#L234">234</span>
<span id="L235" rel="#L235">235</span>
<span id="L236" rel="#L236">236</span>
<span id="L237" rel="#L237">237</span>
<span id="L238" rel="#L238">238</span>
<span id="L239" rel="#L239">239</span>
<span id="L240" rel="#L240">240</span>
<span id="L241" rel="#L241">241</span>
<span id="L242" rel="#L242">242</span>
<span id="L243" rel="#L243">243</span>
<span id="L244" rel="#L244">244</span>
<span id="L245" rel="#L245">245</span>
<span id="L246" rel="#L246">246</span>
<span id="L247" rel="#L247">247</span>
<span id="L248" rel="#L248">248</span>
<span id="L249" rel="#L249">249</span>
<span id="L250" rel="#L250">250</span>
<span id="L251" rel="#L251">251</span>
<span id="L252" rel="#L252">252</span>
<span id="L253" rel="#L253">253</span>
<span id="L254" rel="#L254">254</span>
<span id="L255" rel="#L255">255</span>
<span id="L256" rel="#L256">256</span>
<span id="L257" rel="#L257">257</span>
<span id="L258" rel="#L258">258</span>
<span id="L259" rel="#L259">259</span>
<span id="L260" rel="#L260">260</span>
<span id="L261" rel="#L261">261</span>
<span id="L262" rel="#L262">262</span>
<span id="L263" rel="#L263">263</span>
<span id="L264" rel="#L264">264</span>
<span id="L265" rel="#L265">265</span>
<span id="L266" rel="#L266">266</span>
<span id="L267" rel="#L267">267</span>
<span id="L268" rel="#L268">268</span>
<span id="L269" rel="#L269">269</span>
<span id="L270" rel="#L270">270</span>
<span id="L271" rel="#L271">271</span>
<span id="L272" rel="#L272">272</span>
<span id="L273" rel="#L273">273</span>
<span id="L274" rel="#L274">274</span>
<span id="L275" rel="#L275">275</span>
<span id="L276" rel="#L276">276</span>
<span id="L277" rel="#L277">277</span>
<span id="L278" rel="#L278">278</span>
<span id="L279" rel="#L279">279</span>
</pre>
          </td>
          <td width="100%">
                <div class="highlight"><pre><div class='line' id='LC1'><span class="cm">/*</span></div><div class='line' id='LC2'><span class="cm"> *</span></div><div class='line' id='LC3'><span class="cm"> * Copyright (c) 2006-2011 Sam Collett (http://www.texotela.co.uk)</span></div><div class='line' id='LC4'><span class="cm"> * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)</span></div><div class='line' id='LC5'><span class="cm"> * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.</span></div><div class='line' id='LC6'><span class="cm"> * </span></div><div class='line' id='LC7'><span class="cm"> * Version 1.3</span></div><div class='line' id='LC8'><span class="cm"> * Demo: http://www.texotela.co.uk/code/jquery/numeric/</span></div><div class='line' id='LC9'><span class="cm"> *</span></div><div class='line' id='LC10'><span class="cm"> */</span></div><div class='line' id='LC11'><span class="p">(</span><span class="kd">function</span><span class="p">(</span><span class="nx">$</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC12'><span class="cm">/*</span></div><div class='line' id='LC13'><span class="cm"> * Allows only valid characters to be entered into input boxes.</span></div><div class='line' id='LC14'><span class="cm"> * Note: fixes value when pasting via Ctrl+V, but not when using the mouse to paste</span></div><div class='line' id='LC15'><span class="cm">  *      side-effect: Ctrl+A does not work, though you can still use the mouse to select (or double-click to select all)</span></div><div class='line' id='LC16'><span class="cm"> *</span></div><div class='line' id='LC17'><span class="cm"> * @name     numeric</span></div><div class='line' id='LC18'><span class="cm"> * @param    config      { decimal : &quot;.&quot; , negative : true }</span></div><div class='line' id='LC19'><span class="cm"> * @param    callback     A function that runs if the number is not valid (fires onblur)</span></div><div class='line' id='LC20'><span class="cm"> * @author   Sam Collett (http://www.texotela.co.uk)</span></div><div class='line' id='LC21'><span class="cm"> * @example  $(&quot;.numeric&quot;).numeric();</span></div><div class='line' id='LC22'><span class="cm"> * @example  $(&quot;.numeric&quot;).numeric(&quot;,&quot;); // use , as separater</span></div><div class='line' id='LC23'><span class="cm"> * @example  $(&quot;.numeric&quot;).numeric({ decimal : &quot;,&quot; }); // use , as separator</span></div><div class='line' id='LC24'><span class="cm"> * @example  $(&quot;.numeric&quot;).numeric({ negative : false }); // do not allow negative values</span></div><div class='line' id='LC25'><span class="cm"> * @example  $(&quot;.numeric&quot;).numeric(null, callback); // use default values, pass on the &#39;callback&#39; function</span></div><div class='line' id='LC26'><span class="cm"> *</span></div><div class='line' id='LC27'><span class="cm"> */</span></div><div class='line' id='LC28'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">config</span><span class="p">,</span> <span class="nx">callback</span><span class="p">)</span></div><div class='line' id='LC29'><span class="p">{</span></div><div class='line' id='LC30'>	<span class="k">if</span><span class="p">(</span><span class="k">typeof</span> <span class="nx">config</span> <span class="o">===</span> <span class="s1">&#39;boolean&#39;</span><span class="p">)</span></div><div class='line' id='LC31'>	<span class="p">{</span></div><div class='line' id='LC32'>		<span class="nx">config</span> <span class="o">=</span> <span class="p">{</span> <span class="nx">decimal</span><span class="o">:</span> <span class="nx">config</span> <span class="p">};</span></div><div class='line' id='LC33'>	<span class="p">}</span></div><div class='line' id='LC34'>	<span class="nx">config</span> <span class="o">=</span> <span class="nx">config</span> <span class="o">||</span> <span class="p">{};</span></div><div class='line' id='LC35'>	<span class="c1">// if config.negative undefined, set to true (default is to allow negative numbers)</span></div><div class='line' id='LC36'>	<span class="k">if</span><span class="p">(</span><span class="k">typeof</span> <span class="nx">config</span><span class="p">.</span><span class="nx">negative</span> <span class="o">==</span> <span class="s2">&quot;undefined&quot;</span><span class="p">)</span> <span class="nx">config</span><span class="p">.</span><span class="nx">negative</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC37'>	<span class="c1">// set decimal point</span></div><div class='line' id='LC38'>	<span class="kd">var</span> <span class="nx">decimal</span> <span class="o">=</span> <span class="p">(</span><span class="nx">config</span><span class="p">.</span><span class="nx">decimal</span> <span class="o">===</span> <span class="kc">false</span><span class="p">)</span> <span class="o">?</span> <span class="s2">&quot;&quot;</span> <span class="o">:</span> <span class="nx">config</span><span class="p">.</span><span class="nx">decimal</span> <span class="o">||</span> <span class="s2">&quot;.&quot;</span><span class="p">;</span></div><div class='line' id='LC39'>	<span class="c1">// allow negatives</span></div><div class='line' id='LC40'>	<span class="kd">var</span> <span class="nx">negative</span> <span class="o">=</span> <span class="p">(</span><span class="nx">config</span><span class="p">.</span><span class="nx">negative</span> <span class="o">===</span> <span class="kc">true</span><span class="p">)</span> <span class="o">?</span> <span class="kc">true</span> <span class="o">:</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC41'>	<span class="c1">// callback function</span></div><div class='line' id='LC42'>	<span class="kd">var</span> <span class="nx">callback</span> <span class="o">=</span> <span class="k">typeof</span> <span class="nx">callback</span> <span class="o">==</span> <span class="s2">&quot;function&quot;</span> <span class="o">?</span> <span class="nx">callback</span> <span class="o">:</span> <span class="kd">function</span><span class="p">(){};</span></div><div class='line' id='LC43'>	<span class="c1">// set data and methods</span></div><div class='line' id='LC44'>	<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.decimal&quot;</span><span class="p">,</span> <span class="nx">decimal</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.negative&quot;</span><span class="p">,</span> <span class="nx">negative</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.callback&quot;</span><span class="p">,</span> <span class="nx">callback</span><span class="p">).</span><span class="nx">keypress</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">keypress</span><span class="p">).</span><span class="nx">keyup</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">keyup</span><span class="p">).</span><span class="nx">blur</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">blur</span><span class="p">);</span></div><div class='line' id='LC45'><span class="p">}</span></div><div class='line' id='LC46'><br/></div><div class='line' id='LC47'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">keypress</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">e</span><span class="p">)</span></div><div class='line' id='LC48'><span class="p">{</span></div><div class='line' id='LC49'>	<span class="c1">// get decimal character and determine if negatives are allowed</span></div><div class='line' id='LC50'>	<span class="kd">var</span> <span class="nx">decimal</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.decimal&quot;</span><span class="p">);</span></div><div class='line' id='LC51'>	<span class="kd">var</span> <span class="nx">negative</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.negative&quot;</span><span class="p">);</span></div><div class='line' id='LC52'>	<span class="c1">// get the key that was pressed</span></div><div class='line' id='LC53'>	<span class="kd">var</span> <span class="nx">key</span> <span class="o">=</span> <span class="nx">e</span><span class="p">.</span><span class="nx">charCode</span> <span class="o">?</span> <span class="nx">e</span><span class="p">.</span><span class="nx">charCode</span> <span class="o">:</span> <span class="nx">e</span><span class="p">.</span><span class="nx">keyCode</span> <span class="o">?</span> <span class="nx">e</span><span class="p">.</span><span class="nx">keyCode</span> <span class="o">:</span> <span class="mi">0</span><span class="p">;</span></div><div class='line' id='LC54'>	<span class="c1">// allow enter/return key (only when in an input box)</span></div><div class='line' id='LC55'>	<span class="k">if</span><span class="p">(</span><span class="nx">key</span> <span class="o">==</span> <span class="mi">13</span> <span class="o">&amp;&amp;</span> <span class="k">this</span><span class="p">.</span><span class="nx">nodeName</span><span class="p">.</span><span class="nx">toLowerCase</span><span class="p">()</span> <span class="o">==</span> <span class="s2">&quot;input&quot;</span><span class="p">)</span></div><div class='line' id='LC56'>	<span class="p">{</span></div><div class='line' id='LC57'>		<span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC58'>	<span class="p">}</span></div><div class='line' id='LC59'>	<span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nx">key</span> <span class="o">==</span> <span class="mi">13</span><span class="p">)</span></div><div class='line' id='LC60'>	<span class="p">{</span></div><div class='line' id='LC61'>		<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC62'>	<span class="p">}</span></div><div class='line' id='LC63'>	<span class="kd">var</span> <span class="nx">allow</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC64'>	<span class="c1">// allow Ctrl+A</span></div><div class='line' id='LC65'>	<span class="k">if</span><span class="p">((</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">97</span> <span class="cm">/* firefox */</span><span class="p">)</span> <span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">65</span><span class="p">)</span> <span class="cm">/* opera */</span><span class="p">)</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC66'>	<span class="c1">// allow Ctrl+X (cut)</span></div><div class='line' id='LC67'>	<span class="k">if</span><span class="p">((</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">120</span> <span class="cm">/* firefox */</span><span class="p">)</span> <span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">88</span><span class="p">)</span> <span class="cm">/* opera */</span><span class="p">)</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC68'>	<span class="c1">// allow Ctrl+C (copy)</span></div><div class='line' id='LC69'>	<span class="k">if</span><span class="p">((</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">99</span> <span class="cm">/* firefox */</span><span class="p">)</span> <span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">67</span><span class="p">)</span> <span class="cm">/* opera */</span><span class="p">)</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC70'>	<span class="c1">// allow Ctrl+Z (undo)</span></div><div class='line' id='LC71'>	<span class="k">if</span><span class="p">((</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">122</span> <span class="cm">/* firefox */</span><span class="p">)</span> <span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">90</span><span class="p">)</span> <span class="cm">/* opera */</span><span class="p">)</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC72'>	<span class="c1">// allow or deny Ctrl+V (paste), Shift+Ins</span></div><div class='line' id='LC73'>	<span class="k">if</span><span class="p">((</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">118</span> <span class="cm">/* firefox */</span><span class="p">)</span> <span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">ctrlKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">86</span><span class="p">)</span> <span class="cm">/* opera */</span></div><div class='line' id='LC74'>	<span class="o">||</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">shiftKey</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">45</span><span class="p">))</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC75'>	<span class="c1">// if a number was not pressed</span></div><div class='line' id='LC76'>	<span class="k">if</span><span class="p">(</span><span class="nx">key</span> <span class="o">&lt;</span> <span class="mi">48</span> <span class="o">||</span> <span class="nx">key</span> <span class="o">&gt;</span> <span class="mi">57</span><span class="p">)</span></div><div class='line' id='LC77'>	<span class="p">{</span></div><div class='line' id='LC78'>		<span class="cm">/* &#39;-&#39; only allowed at start and if negative numbers allowed */</span></div><div class='line' id='LC79'>		<span class="k">if</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">indexOf</span><span class="p">(</span><span class="s2">&quot;-&quot;</span><span class="p">)</span> <span class="o">!=</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nx">negative</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="mi">45</span> <span class="o">&amp;&amp;</span> <span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">length</span> <span class="o">==</span> <span class="mi">0</span> <span class="o">||</span> <span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">getSelectionStart</span><span class="p">(</span><span class="k">this</span><span class="p">))</span> <span class="o">==</span> <span class="mi">0</span><span class="p">))</span> <span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC80'>		<span class="cm">/* only one decimal separator allowed */</span></div><div class='line' id='LC81'>		<span class="k">if</span><span class="p">(</span><span class="nx">decimal</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="nx">decimal</span><span class="p">.</span><span class="nx">charCodeAt</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">indexOf</span><span class="p">(</span><span class="nx">decimal</span><span class="p">)</span> <span class="o">!=</span> <span class="o">-</span><span class="mi">1</span><span class="p">)</span></div><div class='line' id='LC82'>		<span class="p">{</span></div><div class='line' id='LC83'>			<span class="nx">allow</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC84'>		<span class="p">}</span></div><div class='line' id='LC85'>		<span class="c1">// check for other keys that have special purposes</span></div><div class='line' id='LC86'>		<span class="k">if</span><span class="p">(</span></div><div class='line' id='LC87'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">8</span> <span class="cm">/* backspace */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC88'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">9</span> <span class="cm">/* tab */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC89'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">13</span> <span class="cm">/* enter */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC90'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">35</span> <span class="cm">/* end */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC91'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">36</span> <span class="cm">/* home */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC92'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">37</span> <span class="cm">/* left */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC93'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">39</span> <span class="cm">/* right */</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC94'>			<span class="nx">key</span> <span class="o">!=</span> <span class="mi">46</span> <span class="cm">/* del */</span></div><div class='line' id='LC95'>		<span class="p">)</span></div><div class='line' id='LC96'>		<span class="p">{</span></div><div class='line' id='LC97'>			<span class="nx">allow</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC98'>		<span class="p">}</span></div><div class='line' id='LC99'>		<span class="k">else</span></div><div class='line' id='LC100'>		<span class="p">{</span></div><div class='line' id='LC101'>			<span class="c1">// for detecting special keys (listed above)</span></div><div class='line' id='LC102'>			<span class="c1">// IE does not support &#39;charCode&#39; and ignores them in keypress anyway</span></div><div class='line' id='LC103'>			<span class="k">if</span><span class="p">(</span><span class="k">typeof</span> <span class="nx">e</span><span class="p">.</span><span class="nx">charCode</span> <span class="o">!=</span> <span class="s2">&quot;undefined&quot;</span><span class="p">)</span></div><div class='line' id='LC104'>			<span class="p">{</span></div><div class='line' id='LC105'>				<span class="c1">// special keys have &#39;keyCode&#39; and &#39;which&#39; the same (e.g. backspace)</span></div><div class='line' id='LC106'>				<span class="k">if</span><span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">keyCode</span> <span class="o">==</span> <span class="nx">e</span><span class="p">.</span><span class="nx">which</span> <span class="o">&amp;&amp;</span> <span class="nx">e</span><span class="p">.</span><span class="nx">which</span> <span class="o">!=</span> <span class="mi">0</span><span class="p">)</span></div><div class='line' id='LC107'>				<span class="p">{</span></div><div class='line' id='LC108'>					<span class="nx">allow</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC109'>					<span class="c1">// . and delete share the same code, don&#39;t allow . (will be set to true later if it is the decimal point)</span></div><div class='line' id='LC110'>					<span class="k">if</span><span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">which</span> <span class="o">==</span> <span class="mi">46</span><span class="p">)</span> <span class="nx">allow</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC111'>				<span class="p">}</span></div><div class='line' id='LC112'>				<span class="c1">// or keyCode != 0 and &#39;charCode&#39;/&#39;which&#39; = 0</span></div><div class='line' id='LC113'>				<span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">keyCode</span> <span class="o">!=</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nx">e</span><span class="p">.</span><span class="nx">charCode</span> <span class="o">==</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nx">e</span><span class="p">.</span><span class="nx">which</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span></div><div class='line' id='LC114'>				<span class="p">{</span></div><div class='line' id='LC115'>					<span class="nx">allow</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC116'>				<span class="p">}</span></div><div class='line' id='LC117'>			<span class="p">}</span></div><div class='line' id='LC118'>		<span class="p">}</span></div><div class='line' id='LC119'>		<span class="c1">// if key pressed is the decimal and it is not already in the field</span></div><div class='line' id='LC120'>		<span class="k">if</span><span class="p">(</span><span class="nx">decimal</span> <span class="o">&amp;&amp;</span> <span class="nx">key</span> <span class="o">==</span> <span class="nx">decimal</span><span class="p">.</span><span class="nx">charCodeAt</span><span class="p">(</span><span class="mi">0</span><span class="p">))</span></div><div class='line' id='LC121'>		<span class="p">{</span></div><div class='line' id='LC122'>			<span class="k">if</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">indexOf</span><span class="p">(</span><span class="nx">decimal</span><span class="p">)</span> <span class="o">==</span> <span class="o">-</span><span class="mi">1</span><span class="p">)</span></div><div class='line' id='LC123'>			<span class="p">{</span></div><div class='line' id='LC124'>				<span class="nx">allow</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC125'>			<span class="p">}</span></div><div class='line' id='LC126'>			<span class="k">else</span></div><div class='line' id='LC127'>			<span class="p">{</span></div><div class='line' id='LC128'>				<span class="nx">allow</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC129'>			<span class="p">}</span></div><div class='line' id='LC130'>		<span class="p">}</span></div><div class='line' id='LC131'>	<span class="p">}</span></div><div class='line' id='LC132'>	<span class="k">else</span></div><div class='line' id='LC133'>	<span class="p">{</span></div><div class='line' id='LC134'>		<span class="nx">allow</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC135'>	<span class="p">}</span></div><div class='line' id='LC136'>	<span class="k">return</span> <span class="nx">allow</span><span class="p">;</span></div><div class='line' id='LC137'><span class="p">}</span></div><div class='line' id='LC138'><br/></div><div class='line' id='LC139'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">keyup</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">e</span><span class="p">)</span></div><div class='line' id='LC140'><span class="p">{</span></div><div class='line' id='LC141'>	<span class="kd">var</span> <span class="nx">val</span> <span class="o">=</span> <span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">;</span></div><div class='line' id='LC142'>	<span class="k">if</span><span class="p">(</span><span class="nx">val</span><span class="p">.</span><span class="nx">length</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span></div><div class='line' id='LC143'>	<span class="p">{</span></div><div class='line' id='LC144'>		<span class="c1">// get carat (cursor) position</span></div><div class='line' id='LC145'>		<span class="kd">var</span> <span class="nx">carat</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">getSelectionStart</span><span class="p">(</span><span class="k">this</span><span class="p">);</span></div><div class='line' id='LC146'>		<span class="c1">// get decimal character and determine if negatives are allowed</span></div><div class='line' id='LC147'>		<span class="kd">var</span> <span class="nx">decimal</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.decimal&quot;</span><span class="p">);</span></div><div class='line' id='LC148'>		<span class="kd">var</span> <span class="nx">negative</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.negative&quot;</span><span class="p">);</span></div><div class='line' id='LC149'><br/></div><div class='line' id='LC150'>		<span class="c1">// prepend a 0 if necessary</span></div><div class='line' id='LC151'>		<span class="k">if</span><span class="p">(</span><span class="nx">decimal</span> <span class="o">!=</span> <span class="s2">&quot;&quot;</span><span class="p">)</span></div><div class='line' id='LC152'>		<span class="p">{</span></div><div class='line' id='LC153'>			<span class="c1">// find decimal point</span></div><div class='line' id='LC154'>			<span class="kd">var</span> <span class="nx">dot</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">indexOf</span><span class="p">(</span><span class="nx">decimal</span><span class="p">);</span></div><div class='line' id='LC155'>			<span class="c1">// if dot at start, add 0 before</span></div><div class='line' id='LC156'>			<span class="k">if</span><span class="p">(</span><span class="nx">dot</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span></div><div class='line' id='LC157'>			<span class="p">{</span></div><div class='line' id='LC158'>				<span class="k">this</span><span class="p">.</span><span class="nx">value</span> <span class="o">=</span> <span class="s2">&quot;0&quot;</span> <span class="o">+</span> <span class="nx">val</span><span class="p">;</span></div><div class='line' id='LC159'>			<span class="p">}</span></div><div class='line' id='LC160'>			<span class="c1">// if dot at position 1, check if there is a - symbol before it</span></div><div class='line' id='LC161'>			<span class="k">if</span><span class="p">(</span><span class="nx">dot</span> <span class="o">==</span> <span class="mi">1</span> <span class="o">&amp;&amp;</span> <span class="nx">val</span><span class="p">.</span><span class="nx">charAt</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span> <span class="o">==</span> <span class="s2">&quot;-&quot;</span><span class="p">)</span></div><div class='line' id='LC162'>			<span class="p">{</span></div><div class='line' id='LC163'>				<span class="k">this</span><span class="p">.</span><span class="nx">value</span> <span class="o">=</span> <span class="s2">&quot;-0&quot;</span> <span class="o">+</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="mi">1</span><span class="p">);</span></div><div class='line' id='LC164'>			<span class="p">}</span></div><div class='line' id='LC165'>			<span class="nx">val</span> <span class="o">=</span> <span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">;</span></div><div class='line' id='LC166'>		<span class="p">}</span></div><div class='line' id='LC167'><br/></div><div class='line' id='LC168'>		<span class="c1">// if pasted in, only allow the following characters</span></div><div class='line' id='LC169'>		<span class="kd">var</span> <span class="nx">validChars</span> <span class="o">=</span> <span class="p">[</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">,</span><span class="mi">4</span><span class="p">,</span><span class="mi">5</span><span class="p">,</span><span class="mi">6</span><span class="p">,</span><span class="mi">7</span><span class="p">,</span><span class="mi">8</span><span class="p">,</span><span class="mi">9</span><span class="p">,</span><span class="s1">&#39;-&#39;</span><span class="p">,</span><span class="nx">decimal</span><span class="p">];</span></div><div class='line' id='LC170'>		<span class="c1">// get length of the value (to loop through)</span></div><div class='line' id='LC171'>		<span class="kd">var</span> <span class="nx">length</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">length</span><span class="p">;</span></div><div class='line' id='LC172'>		<span class="c1">// loop backwards (to prevent going out of bounds)</span></div><div class='line' id='LC173'>		<span class="k">for</span><span class="p">(</span><span class="kd">var</span> <span class="nx">i</span> <span class="o">=</span> <span class="nx">length</span> <span class="o">-</span> <span class="mi">1</span><span class="p">;</span> <span class="nx">i</span> <span class="o">&gt;=</span> <span class="mi">0</span><span class="p">;</span> <span class="nx">i</span><span class="o">--</span><span class="p">)</span></div><div class='line' id='LC174'>		<span class="p">{</span></div><div class='line' id='LC175'>			<span class="kd">var</span> <span class="nx">ch</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">charAt</span><span class="p">(</span><span class="nx">i</span><span class="p">);</span></div><div class='line' id='LC176'>			<span class="c1">// remove &#39;-&#39; if it is in the wrong place</span></div><div class='line' id='LC177'>			<span class="k">if</span><span class="p">(</span><span class="nx">i</span> <span class="o">!=</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nx">ch</span> <span class="o">==</span> <span class="s2">&quot;-&quot;</span><span class="p">)</span></div><div class='line' id='LC178'>			<span class="p">{</span></div><div class='line' id='LC179'>				<span class="nx">val</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="nx">i</span><span class="p">)</span> <span class="o">+</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="nx">i</span> <span class="o">+</span> <span class="mi">1</span><span class="p">);</span></div><div class='line' id='LC180'>			<span class="p">}</span></div><div class='line' id='LC181'>			<span class="c1">// remove character if it is at the start, a &#39;-&#39; and negatives aren&#39;t allowed</span></div><div class='line' id='LC182'>			<span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nx">i</span> <span class="o">==</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="o">!</span><span class="nx">negative</span> <span class="o">&amp;&amp;</span> <span class="nx">ch</span> <span class="o">==</span> <span class="s2">&quot;-&quot;</span><span class="p">)</span></div><div class='line' id='LC183'>			<span class="p">{</span></div><div class='line' id='LC184'>				<span class="nx">val</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="mi">1</span><span class="p">);</span></div><div class='line' id='LC185'>			<span class="p">}</span></div><div class='line' id='LC186'>			<span class="kd">var</span> <span class="nx">validChar</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC187'>			<span class="c1">// loop through validChars</span></div><div class='line' id='LC188'>			<span class="k">for</span><span class="p">(</span><span class="kd">var</span> <span class="nx">j</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nx">j</span> <span class="o">&lt;</span> <span class="nx">validChars</span><span class="p">.</span><span class="nx">length</span><span class="p">;</span> <span class="nx">j</span><span class="o">++</span><span class="p">)</span></div><div class='line' id='LC189'>			<span class="p">{</span></div><div class='line' id='LC190'>				<span class="c1">// if it is valid, break out the loop</span></div><div class='line' id='LC191'>				<span class="k">if</span><span class="p">(</span><span class="nx">ch</span> <span class="o">==</span> <span class="nx">validChars</span><span class="p">[</span><span class="nx">j</span><span class="p">])</span></div><div class='line' id='LC192'>				<span class="p">{</span></div><div class='line' id='LC193'>					<span class="nx">validChar</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC194'>					<span class="k">break</span><span class="p">;</span></div><div class='line' id='LC195'>				<span class="p">}</span></div><div class='line' id='LC196'>			<span class="p">}</span></div><div class='line' id='LC197'>			<span class="c1">// if not a valid character, or a space, remove</span></div><div class='line' id='LC198'>			<span class="k">if</span><span class="p">(</span><span class="o">!</span><span class="nx">validChar</span> <span class="o">||</span> <span class="nx">ch</span> <span class="o">==</span> <span class="s2">&quot; &quot;</span><span class="p">)</span></div><div class='line' id='LC199'>			<span class="p">{</span></div><div class='line' id='LC200'>				<span class="nx">val</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="nx">i</span><span class="p">)</span> <span class="o">+</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="nx">i</span> <span class="o">+</span> <span class="mi">1</span><span class="p">);</span></div><div class='line' id='LC201'>			<span class="p">}</span></div><div class='line' id='LC202'>		<span class="p">}</span></div><div class='line' id='LC203'>		<span class="c1">// remove extra decimal characters</span></div><div class='line' id='LC204'>		<span class="kd">var</span> <span class="nx">firstDecimal</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">indexOf</span><span class="p">(</span><span class="nx">decimal</span><span class="p">);</span></div><div class='line' id='LC205'>		<span class="k">if</span><span class="p">(</span><span class="nx">firstDecimal</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span></div><div class='line' id='LC206'>		<span class="p">{</span></div><div class='line' id='LC207'>			<span class="k">for</span><span class="p">(</span><span class="kd">var</span> <span class="nx">i</span> <span class="o">=</span> <span class="nx">length</span> <span class="o">-</span> <span class="mi">1</span><span class="p">;</span> <span class="nx">i</span> <span class="o">&gt;</span> <span class="nx">firstDecimal</span><span class="p">;</span> <span class="nx">i</span><span class="o">--</span><span class="p">)</span></div><div class='line' id='LC208'>			<span class="p">{</span></div><div class='line' id='LC209'>				<span class="kd">var</span> <span class="nx">ch</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">charAt</span><span class="p">(</span><span class="nx">i</span><span class="p">);</span></div><div class='line' id='LC210'>				<span class="c1">// remove decimal character</span></div><div class='line' id='LC211'>				<span class="k">if</span><span class="p">(</span><span class="nx">ch</span> <span class="o">==</span> <span class="nx">decimal</span><span class="p">)</span></div><div class='line' id='LC212'>				<span class="p">{</span></div><div class='line' id='LC213'>					<span class="nx">val</span> <span class="o">=</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="nx">i</span><span class="p">)</span> <span class="o">+</span> <span class="nx">val</span><span class="p">.</span><span class="nx">substring</span><span class="p">(</span><span class="nx">i</span> <span class="o">+</span> <span class="mi">1</span><span class="p">);</span></div><div class='line' id='LC214'>				<span class="p">}</span></div><div class='line' id='LC215'>			<span class="p">}</span></div><div class='line' id='LC216'>		<span class="p">}</span></div><div class='line' id='LC217'>		<span class="c1">// set the value and prevent the cursor moving to the end</span></div><div class='line' id='LC218'>		<span class="k">this</span><span class="p">.</span><span class="nx">value</span> <span class="o">=</span> <span class="nx">val</span><span class="p">;</span></div><div class='line' id='LC219'>		<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">setSelection</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="nx">carat</span><span class="p">);</span></div><div class='line' id='LC220'>	<span class="p">}</span></div><div class='line' id='LC221'><span class="p">}</span></div><div class='line' id='LC222'><br/></div><div class='line' id='LC223'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">blur</span> <span class="o">=</span> <span class="kd">function</span><span class="p">()</span></div><div class='line' id='LC224'><span class="p">{</span></div><div class='line' id='LC225'>	<span class="kd">var</span> <span class="nx">decimal</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.decimal&quot;</span><span class="p">);</span></div><div class='line' id='LC226'>	<span class="kd">var</span> <span class="nx">callback</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="k">this</span><span class="p">,</span> <span class="s2">&quot;numeric.callback&quot;</span><span class="p">);</span></div><div class='line' id='LC227'>	<span class="kd">var</span> <span class="nx">val</span> <span class="o">=</span> <span class="k">this</span><span class="p">.</span><span class="nx">value</span><span class="p">;</span></div><div class='line' id='LC228'>	<span class="k">if</span><span class="p">(</span><span class="nx">val</span> <span class="o">!=</span> <span class="s2">&quot;&quot;</span><span class="p">)</span></div><div class='line' id='LC229'>	<span class="p">{</span></div><div class='line' id='LC230'>		<span class="kd">var</span> <span class="nx">re</span> <span class="o">=</span> <span class="k">new</span> <span class="nb">RegExp</span><span class="p">(</span><span class="s2">&quot;^\\d+$|\\d*&quot;</span> <span class="o">+</span> <span class="nx">decimal</span> <span class="o">+</span> <span class="s2">&quot;\\d+&quot;</span><span class="p">);</span></div><div class='line' id='LC231'>		<span class="k">if</span><span class="p">(</span><span class="o">!</span><span class="nx">re</span><span class="p">.</span><span class="nx">exec</span><span class="p">(</span><span class="nx">val</span><span class="p">))</span></div><div class='line' id='LC232'>		<span class="p">{</span></div><div class='line' id='LC233'>			<span class="nx">callback</span><span class="p">.</span><span class="nx">apply</span><span class="p">(</span><span class="k">this</span><span class="p">);</span></div><div class='line' id='LC234'>		<span class="p">}</span></div><div class='line' id='LC235'>	<span class="p">}</span></div><div class='line' id='LC236'><span class="p">}</span></div><div class='line' id='LC237'><br/></div><div class='line' id='LC238'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">removeNumeric</span> <span class="o">=</span> <span class="kd">function</span><span class="p">()</span></div><div class='line' id='LC239'><span class="p">{</span></div><div class='line' id='LC240'>	<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.decimal&quot;</span><span class="p">,</span> <span class="kc">null</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.negative&quot;</span><span class="p">,</span> <span class="kc">null</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s2">&quot;numeric.callback&quot;</span><span class="p">,</span> <span class="kc">null</span><span class="p">).</span><span class="nx">unbind</span><span class="p">(</span><span class="s2">&quot;keypress&quot;</span><span class="p">,</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">keypress</span><span class="p">).</span><span class="nx">unbind</span><span class="p">(</span><span class="s2">&quot;blur&quot;</span><span class="p">,</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">numeric</span><span class="p">.</span><span class="nx">blur</span><span class="p">);</span></div><div class='line' id='LC241'><span class="p">}</span></div><div class='line' id='LC242'><br/></div><div class='line' id='LC243'><span class="c1">// Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini &lt;dperini@nwbox.com&gt;)</span></div><div class='line' id='LC244'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">getSelectionStart</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">o</span><span class="p">)</span></div><div class='line' id='LC245'><span class="p">{</span></div><div class='line' id='LC246'>	<span class="k">if</span> <span class="p">(</span><span class="nx">o</span><span class="p">.</span><span class="nx">createTextRange</span><span class="p">)</span></div><div class='line' id='LC247'>	<span class="p">{</span></div><div class='line' id='LC248'>		<span class="kd">var</span> <span class="nx">r</span> <span class="o">=</span> <span class="nb">document</span><span class="p">.</span><span class="nx">selection</span><span class="p">.</span><span class="nx">createRange</span><span class="p">().</span><span class="nx">duplicate</span><span class="p">();</span></div><div class='line' id='LC249'>		<span class="nx">r</span><span class="p">.</span><span class="nx">moveEnd</span><span class="p">(</span><span class="s1">&#39;character&#39;</span><span class="p">,</span> <span class="nx">o</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">length</span><span class="p">);</span></div><div class='line' id='LC250'>		<span class="k">if</span> <span class="p">(</span><span class="nx">r</span><span class="p">.</span><span class="nx">text</span> <span class="o">==</span> <span class="s1">&#39;&#39;</span><span class="p">)</span> <span class="k">return</span> <span class="nx">o</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">length</span><span class="p">;</span></div><div class='line' id='LC251'>		<span class="k">return</span> <span class="nx">o</span><span class="p">.</span><span class="nx">value</span><span class="p">.</span><span class="nx">lastIndexOf</span><span class="p">(</span><span class="nx">r</span><span class="p">.</span><span class="nx">text</span><span class="p">);</span></div><div class='line' id='LC252'>	<span class="p">}</span> <span class="k">else</span> <span class="k">return</span> <span class="nx">o</span><span class="p">.</span><span class="nx">selectionStart</span><span class="p">;</span></div><div class='line' id='LC253'><span class="p">}</span></div><div class='line' id='LC254'><br/></div><div class='line' id='LC255'><span class="c1">// set the selection, o is the object (input), p is the position ([start, end] or just start)</span></div><div class='line' id='LC256'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">setSelection</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">o</span><span class="p">,</span> <span class="nx">p</span><span class="p">)</span></div><div class='line' id='LC257'><span class="p">{</span></div><div class='line' id='LC258'>	<span class="c1">// if p is number, start and end are the same</span></div><div class='line' id='LC259'>	<span class="k">if</span><span class="p">(</span><span class="k">typeof</span> <span class="nx">p</span> <span class="o">==</span> <span class="s2">&quot;number&quot;</span><span class="p">)</span> <span class="nx">p</span> <span class="o">=</span> <span class="p">[</span><span class="nx">p</span><span class="p">,</span> <span class="nx">p</span><span class="p">];</span></div><div class='line' id='LC260'>	<span class="c1">// only set if p is an array of length 2</span></div><div class='line' id='LC261'>	<span class="k">if</span><span class="p">(</span><span class="nx">p</span> <span class="o">&amp;&amp;</span> <span class="nx">p</span><span class="p">.</span><span class="nx">constructor</span> <span class="o">==</span> <span class="nb">Array</span> <span class="o">&amp;&amp;</span> <span class="nx">p</span><span class="p">.</span><span class="nx">length</span> <span class="o">==</span> <span class="mi">2</span><span class="p">)</span></div><div class='line' id='LC262'>	<span class="p">{</span></div><div class='line' id='LC263'>		<span class="k">if</span> <span class="p">(</span><span class="nx">o</span><span class="p">.</span><span class="nx">createTextRange</span><span class="p">)</span></div><div class='line' id='LC264'>		<span class="p">{</span></div><div class='line' id='LC265'>			<span class="kd">var</span> <span class="nx">r</span> <span class="o">=</span> <span class="nx">o</span><span class="p">.</span><span class="nx">createTextRange</span><span class="p">();</span></div><div class='line' id='LC266'>			<span class="nx">r</span><span class="p">.</span><span class="nx">collapse</span><span class="p">(</span><span class="kc">true</span><span class="p">);</span></div><div class='line' id='LC267'>			<span class="nx">r</span><span class="p">.</span><span class="nx">moveStart</span><span class="p">(</span><span class="s1">&#39;character&#39;</span><span class="p">,</span> <span class="nx">p</span><span class="p">[</span><span class="mi">0</span><span class="p">]);</span></div><div class='line' id='LC268'>			<span class="nx">r</span><span class="p">.</span><span class="nx">moveEnd</span><span class="p">(</span><span class="s1">&#39;character&#39;</span><span class="p">,</span> <span class="nx">p</span><span class="p">[</span><span class="mi">1</span><span class="p">]);</span></div><div class='line' id='LC269'>			<span class="nx">r</span><span class="p">.</span><span class="nx">select</span><span class="p">();</span></div><div class='line' id='LC270'>		<span class="p">}</span></div><div class='line' id='LC271'>		<span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nx">o</span><span class="p">.</span><span class="nx">setSelectionRange</span><span class="p">)</span></div><div class='line' id='LC272'>		<span class="p">{</span></div><div class='line' id='LC273'>			<span class="nx">o</span><span class="p">.</span><span class="nx">focus</span><span class="p">();</span></div><div class='line' id='LC274'>			<span class="nx">o</span><span class="p">.</span><span class="nx">setSelectionRange</span><span class="p">(</span><span class="nx">p</span><span class="p">[</span><span class="mi">0</span><span class="p">],</span> <span class="nx">p</span><span class="p">[</span><span class="mi">1</span><span class="p">]);</span></div><div class='line' id='LC275'>		<span class="p">}</span></div><div class='line' id='LC276'>	<span class="p">}</span></div><div class='line' id='LC277'><span class="p">}</span></div><div class='line' id='LC278'><br/></div><div class='line' id='LC279'><span class="p">})(</span><span class="nx">jQuery</span><span class="p">);</span></div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>
      </div>
    </div>

  </div>

<div class="frame frame-loading" style="display:none;" data-tree-list-url="/SamWM/jQuery-Plugins/tree-list/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a" data-blob-url-prefix="/SamWM/jQuery-Plugins/blob/014aa5a84bbef4ac293aa57a20cf0adabd3c8b1a">
  <img src="https://a248.e.akamai.net/assets.github.com/images/modules/ajax/big_spinner_336699.gif" height="32" width="32">
</div>

    </div>

    </div>

    <!-- footer -->
    <div id="footer" >
      
  <div class="upper_footer">
     <div class="site" class="clearfix">

       <!--[if IE]><h4 id="blacktocat_ie">GitHub Links</h4><![endif]-->
       <![if !IE]><h4 id="blacktocat">GitHub Links</h4><![endif]>

       <ul class="footer_nav">
         <h4>GitHub</h4>
         <li><a href="https://github.com/about">About</a></li>
         <li><a href="https://github.com/blog">Blog</a></li>
         <li><a href="https://github.com/features">Features</a></li>
         <li><a href="https://github.com/contact">Contact &amp; Support</a></li>
         <li><a href="https://github.com/training">Training</a></li>
         <li><a href="http://status.github.com/">Site Status</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Tools</h4>
         <li><a href="http://mac.github.com/">GitHub for Mac</a></li>
         <li><a href="http://mobile.github.com/">Issues for iPhone</a></li>
         <li><a href="https://gist.github.com">Gist: Code Snippets</a></li>
         <li><a href="http://fi.github.com/">Enterprise Install</a></li>
         <li><a href="http://jobs.github.com/">Job Board</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Extras</h4>
         <li><a href="http://shop.github.com/">GitHub Shop</a></li>
         <li><a href="http://octodex.github.com/">The Octodex</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Documentation</h4>
         <li><a href="http://help.github.com/">GitHub Help</a></li>
         <li><a href="http://developer.github.com/">Developer API</a></li>
         <li><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></li>
         <li><a href="http://pages.github.com/">GitHub Pages</a></li>
       </ul>

     </div><!-- /.site -->
  </div><!-- /.upper_footer -->

<div class="lower_footer">
  <div class="site" class="clearfix">
    <!--[if IE]><div id="legal_ie"><![endif]-->
    <![if !IE]><div id="legal"><![endif]>
      <ul>
          <li><a href="https://github.com/site/terms">Terms of Service</a></li>
          <li><a href="https://github.com/site/privacy">Privacy</a></li>
          <li><a href="https://github.com/security">Security</a></li>
      </ul>

      <p>&copy; 2011 <span id="_rrt" title="0.13367s from fe8.rs.github.com">GitHub</span> Inc. All rights reserved.</p>
    </div><!-- /#legal or /#legal_ie-->

      <div class="sponsor">
        <a href="http://www.rackspace.com" class="logo">
          <img alt="Dedicated Server" height="36" src="https://a248.e.akamai.net/assets.github.com/images/modules/footer/rackspace_logo.png?v2" width="38" />
        </a>
        Powered by the <a href="http://www.rackspace.com ">Dedicated
        Servers</a> and<br/> <a href="http://www.rackspacecloud.com">Cloud
        Computing</a> of Rackspace Hosting<span>&reg;</span>
      </div>
  </div><!-- /.site -->
</div><!-- /.lower_footer -->

    </div><!-- /#footer -->

    

<div id="keyboard_shortcuts_pane" class="instapaper_ignore readability-extra" style="display:none">
  <h2>Keyboard Shortcuts <small><a href="#" class="js-see-all-keyboard-shortcuts">(see all)</a></small></h2>

  <div class="columns threecols">
    <div class="column first">
      <h3>Site wide shortcuts</h3>
      <dl class="keyboard-mappings">
        <dt>s</dt>
        <dd>Focus site search</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>?</dt>
        <dd>Bring up this help dialog</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column middle" style=&#39;display:none&#39;>
      <h3>Commit list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>c <em>or</em> o <em>or</em> enter</dt>
        <dd>Open commit</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>y</dt>
        <dd>Expand URL to its canonical form</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column last" style=&#39;display:none&#39;>
      <h3>Pull request list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>o <em>or</em> enter</dt>
        <dd>Open issue</dd>
      </dl>
    </div><!-- /.columns.last -->

  </div><!-- /.columns.equacols -->

  <div style=&#39;display:none&#39;>
    <div class="rule"></div>

    <h3>Issues</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>x</dt>
          <dd>Toggle selection</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column middle">
        <dl class="keyboard-mappings">
          <dt>I</dt>
          <dd>Mark selection as read</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>U</dt>
          <dd>Mark selection as unread</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>e</dt>
          <dd>Close selection</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>y</dt>
          <dd>Remove selection from view</dd>
        </dl>
      </div><!-- /.column.middle -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>c</dt>
          <dd>Create issue</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Create label</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>i</dt>
          <dd>Back to inbox</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>u</dt>
          <dd>Back to issues</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>/</dt>
          <dd>Focus issues search</dd>
        </dl>
      </div>
    </div>
  </div>

  <div style=&#39;display:none&#39;>
    <div class="rule"></div>

    <h3>Issues Dashboard</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
      </div><!-- /.column.first -->
    </div>
  </div>

  <div style=&#39;display:none&#39;>
    <div class="rule"></div>

    <h3>Network Graph</h3>
    <div class="columns equacols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt><span class="badmono">←</span> <em>or</em> h</dt>
          <dd>Scroll left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">→</span> <em>or</em> l</dt>
          <dd>Scroll right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↑</span> <em>or</em> k</dt>
          <dd>Scroll up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↓</span> <em>or</em> j</dt>
          <dd>Scroll down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Toggle visibility of head labels</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">←</span> <em>or</em> shift h</dt>
          <dd>Scroll all the way left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">→</span> <em>or</em> shift l</dt>
          <dd>Scroll all the way right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↑</span> <em>or</em> shift k</dt>
          <dd>Scroll all the way up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↓</span> <em>or</em> shift j</dt>
          <dd>Scroll all the way down</dd>
        </dl>
      </div><!-- /.column.last -->
    </div>
  </div>

  <div >
    <div class="rule"></div>
    <div class="columns threecols">
      <div class="column first" >
        <h3>Source Code Browsing</h3>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Activates the file finder</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Jump to line</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>w</dt>
          <dd>Switch branch/tag</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>y</dt>
          <dd>Expand URL to its canonical form</dd>
        </dl>
      </div>
    </div>
  </div>
</div>

    <div id="markdown-help" class="instapaper_ignore readability-extra">
  <h2>Markdown Cheat Sheet</h2>

  <div class="cheatsheet-content">

  <div class="mod">
    <div class="col">
      <h3>Format Text</h3>
      <p>Headers</p>
      <pre>
# This is an &lt;h1&gt; tag
## This is an &lt;h2&gt; tag
###### This is an &lt;h6&gt; tag</pre>
     <p>Text styles</p>
     <pre>
*This text will be italic*
_This will also be italic_
**This text will be bold**
__This will also be bold__

*You **can** combine them*
</pre>
    </div>
    <div class="col">
      <h3>Lists</h3>
      <p>Unordered</p>
      <pre>
* Item 1
* Item 2
  * Item 2a
  * Item 2b</pre>
     <p>Ordered</p>
     <pre>
1. Item 1
2. Item 2
3. Item 3
   * Item 3a
   * Item 3b</pre>
    </div>
    <div class="col">
      <h3>Miscellaneous</h3>
      <p>Images</p>
      <pre>
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)
</pre>
     <p>Links</p>
     <pre>
http://github.com - automatic!
[GitHub](http://github.com)</pre>
<p>Blockquotes</p>
     <pre>
As Kanye West said:
> We're living the future so
> the present is our past.
</pre>
    </div>
  </div>
  <div class="rule"></div>

  <h3>Code Examples in Markdown</h3>
  <div class="col">
      <p>Syntax highlighting with <a href="http://github.github.com/github-flavored-markdown/" title="GitHub Flavored Markdown" target="_blank">GFM</a></p>
      <pre>
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```</pre>
    </div>
    <div class="col">
      <p>Or, indent your code 4 spaces</p>
      <pre>
Here is a Python code example
without syntax highlighting:

    def foo:
      if not bar:
        return true</pre>
    </div>
    <div class="col">
      <p>Inline code for comments</p>
      <pre>
I think you should use an
`&lt;addr&gt;` element here instead.</pre>
    </div>
  </div>

  </div>
</div>

    <div class="context-overlay"></div>

    
    
    
  </body>
</html>

