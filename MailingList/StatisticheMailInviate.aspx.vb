﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class MailingList_StatisticheMailInviate
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Mail", GetType(String))
        MyTable.Columns.Add("DataOra", GetType(String))
        MyTable.Columns.Add("Esito", GetType(String))
        MyTable.Columns.Add("Oggetto", GetType(String))
        MyTable.Columns.Add("Mail Invio", GetType(String))
        MyTable.Columns.Add("NumeroRegistrazione", GetType(String))
        MyTable.Columns.Add("Dati Documento", GetType(String))
        MyTable.Columns.Add("Nome Destinatario", GetType(String))
        Dim cmd As New OleDbCommand()


 
        cmd.CommandText = "Select * From [Log_MailInviate] Where [DataOra] >= ? And [DataOra] <= ?"
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataDal)
        cmd.Parameters.AddWithValue("@DataAl", DataAl)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            

            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("Mail"))
            myriga(1) = campodb(myPOSTreader.Item("DataOra"))
            myriga(2) = campodb(myPOSTreader.Item("Esito"))
            myriga(3) = campodb(myPOSTreader.Item("Oggetto"))
            myriga(4) = campodb(myPOSTreader.Item("MailInvio"))
            myriga(5) = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))

            Dim Doc As New Cls_MovimentoContabile

            Doc.Leggi(Session("DC_GENERALE"), Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))

            myriga(6) = Doc.NumeroDocumento & " " & Format(Doc.DataDocumento, "dd/MM/yyyy")
            myriga(7) = ""

            If Val(campodb(myPOSTreader.Item("CodiceParente"))) > 0 And Val(campodb(myPOSTreader.Item("CodiceOspite"))) > 0 Then
                Dim DecPAr As New Cls_Parenti

                DecPAr.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))), Val(campodb(myPOSTreader.Item("CodiceParente"))))

                myriga(7) = DecPAr.Nome
            End If

            If Val(campodb(myPOSTreader.Item("CodiceParente"))) = 0 And Val(campodb(myPOSTreader.Item("CodiceOspite"))) > 0 Then
                Dim DecOsp As New ClsOspite

                DecOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

                myriga(7) = DecOsp.Nome
            End If
            If myriga(7) = "" And Doc.NumeroRegistrazione > 0 Then
                Try
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = Doc.Righe(0).MastroPartita
                    PianoConti.Conto = Doc.Righe(0).ContoPartita
                    PianoConti.Sottoconto = Doc.Righe(0).SottocontoPartita
                    PianoConti.Decodfica(Session("DC_GENERALE"))
                    myriga(7) = PianoConti.Descrizione

                Catch ex As Exception

                End Try
    
            End If


            MyTable.Rows.Add(myriga)
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

 

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")


        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If

        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Mailing.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_MailList.aspx")
    End Sub



End Class
