﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class MailingList_Gestione_MailList
    Inherits System.Web.UI.Page

    Protected Sub Btn_Aggiungi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Aggiungi.Click
        Dim Appoggio As String
        Dim i As Integer
        Dim Vettore(400) As String

        Appoggio = Txt_Mail.Text

        If Appoggio.IndexOf(";") < -1 Then
            Lst_Mail.Items.Add("0" & " " & "L" & " " & Appoggio)
            Lst_Mail.Items(Lst_Mail.Items.Count - 1).Value = "0" & " " & "L" & " " & Appoggio
        Else

            Vettore = MySplitWords(Appoggio)
            For i = 0 To Vettore.Length - 1
                Lst_Mail.Items.Add(Vettore(i))
                Lst_Mail.Items(Lst_Mail.Items.Count - 1).Value = "0" & " " & "L" & " " & Vettore(i)
            Next
        End If
        Txt_Mail.Text = ""
    End Sub

    Private Function MySplitWords(ByVal s As String) As String()
        Return Regex.Split(s, ";")
    End Function

    Protected Sub Btn_Aggiungi_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Btn_Aggiungi.Command

    End Sub
    Protected Sub BtnInvSalvaDati_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInvSalvaDati.Click
        Dim TabExport As New System.Data.DataTable("TabExport")
        Dim i As Integer

        TabExport = Session("Export")

        If IsNothing(TabExport) Then
            Exit Sub
        End If
        For i = 0 To TabExport.Rows.Count - 1
            Lst_Mail.Items.Add(TabExport.Rows(i).Item(1) & " " & TabExport.Rows(i).Item(2))
            Lst_Mail.Items(Lst_Mail.Items.Count - 1).Value = TabExport.Rows(i).Item(0)
        Next
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Descrizione</center>');", True)
            Exit Sub
        End If


        ScriviRubrica()
        Response.Redirect("Elenco_MailingList.aspx")
    End Sub


    Private Sub ScriviRubrica()
        Dim k As New Cls_Tabella_MailingListTesta



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Dim Indice As Integer

        k.Id = Val(Txt_ID.Text)

        k.Descrizione = Txt_Descrizione.Text
        k.Utente = Session("UTENTE")


        For Indice = 0 To Lst_Mail.Items.Count - 1
            Dim Appoggio As String = Lst_Mail.Items(Indice).Value



            If Val(Lst_Mail.Items(Indice).Value) = 0 Then

                k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                k.Righe(Indice).CodiceOspite = 0
                k.Righe(Indice).CodiceParente = 0
                k.Righe(Indice).CodicePorvincia = ""
                k.Righe(Indice).CodiceComune = ""
                k.Righe(Indice).CodiceDebitoreCreditore = 0
                k.Righe(Indice).Regione = ""
                k.Righe(Indice).CodiceMedico = ""
                k.Righe(Indice).Mail = Lst_Mail.Items(Indice).Text

            Else                
                Dim cmd As New OleDbCommand()
                cmd.CommandText = "Select * From AnagraficaComune Where ID = " & Val(Lst_Mail.Items(Indice).Value)

                cmd.Connection = cn
                Dim sb As StringBuilder = New StringBuilder

                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                If myPOSTreader.Read Then
                    If campodb(myPOSTreader.Item("TIPOLOGIA")) = "O" Then
                        k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                        k.Righe(Indice).CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                        k.Righe(Indice).CodiceParente = 0
                        k.Righe(Indice).CodicePorvincia = ""
                        k.Righe(Indice).CodiceComune = ""
                        k.Righe(Indice).CodiceDebitoreCreditore = 0
                        k.Righe(Indice).Regione = ""
                        k.Righe(Indice).CodiceMedico = ""
                        k.Righe(Indice).Mail = ""
                    End If
                    If campodb(myPOSTreader.Item("TIPOLOGIA")) = "P" Then
                        k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                        k.Righe(Indice).CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                        k.Righe(Indice).CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
                        k.Righe(Indice).CodicePorvincia = ""
                        k.Righe(Indice).CodiceComune = ""
                        k.Righe(Indice).CodiceDebitoreCreditore = 0
                        k.Righe(Indice).Regione = ""
                        k.Righe(Indice).CodiceMedico = ""
                        k.Righe(Indice).Mail = ""
                    End If
                    If campodb(myPOSTreader.Item("TIPOLOGIA")) = "C" Then
                        k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                        k.Righe(Indice).CodiceOspite = 0
                        k.Righe(Indice).CodiceParente = 0
                        k.Righe(Indice).CodicePorvincia = campodb(myPOSTreader.Item("CodiceProvincia"))
                        k.Righe(Indice).CodiceComune = campodb(myPOSTreader.Item("CodiceComune"))
                        k.Righe(Indice).CodiceDebitoreCreditore = 0
                        k.Righe(Indice).Regione = ""
                        k.Righe(Indice).CodiceMedico = ""
                        k.Righe(Indice).Mail = ""
                    End If
                    If campodb(myPOSTreader.Item("TIPOLOGIA")) = "R" Then
                        k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                        k.Righe(Indice).CodiceOspite = 0
                        k.Righe(Indice).CodiceParente = 0
                        k.Righe(Indice).CodicePorvincia = ""
                        k.Righe(Indice).CodiceComune = ""
                        k.Righe(Indice).CodiceDebitoreCreditore = 0
                        k.Righe(Indice).Regione = campodb(myPOSTreader.Item("CodiceRegione"))
                        k.Righe(Indice).CodiceMedico = ""
                        k.Righe(Indice).Mail = ""
                    End If
                    If campodb(myPOSTreader.Item("TIPOLOGIA")) = "M" Then
                        k.Righe(Indice) = New Cls_Tabella_MailingListRiga
                        k.Righe(Indice).CodiceOspite = 0
                        k.Righe(Indice).CodiceParente = 0
                        k.Righe(Indice).CodicePorvincia = ""
                        k.Righe(Indice).CodiceComune = ""
                        k.Righe(Indice).CodiceDebitoreCreditore = 0
                        k.Righe(Indice).Regione = ""
                        k.Righe(Indice).CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
                        k.Righe(Indice).Mail = ""
                    End If

                End If

                myPOSTreader.Close()





            End If
        Next

        k.Scrivi(Session("DC_OSPITE"), k.Id)



        Txt_ID.Text = 0

        Txt_Descrizione.Text = ""

        Lst_Mail.Items.Clear()

        Txt_Mail.Text = ""

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub MailingList_Gestione_MailList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Call EseguiJS()

        If Val(Request.Item("ID")) > 0 Then
            Txt_ID.Text = Val(Request.Item("ID"))
            LeggiRubrica()
        End If

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Comune')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/autocompletecomune.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Cap')!= null) {  "
        MyJs = MyJs & " $(els[i]).focus(function() { if ($(els[i]).val() == '') {  __doPostBack(""Btn_RefreshPostali"", ""0""); } }); "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtConto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub LeggiRubrica()

        Dim MyID As Integer
        Dim Mail As String
        Dim Nome As String
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim k As New Cls_Tabella_MailingListTesta

        k.Id = Val(Txt_ID.Text)
        k.Leggi(Session("DC_OSPITE"))

        Txt_Descrizione.Text = k.Descrizione

        Dim Indice As Integer

        For Indice = 0 To k.Righe.Length - 1
            MyID = 0
            Mail = ""
            Nome = ""
            If Not IsNothing(k.Righe(Indice)) Then


                If k.Righe(Indice).CodiceOspite > 0 Then
                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select * From AnagraficaComune Where CodiceOspite  = " & k.Righe(Indice).CodiceOspite

                    cmd.Connection = cn
                    Dim sb As StringBuilder = New StringBuilder

                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MyID = myPOSTreader.Item("ID")
                        Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                        Nome = campodb(myPOSTreader.Item("Nome"))
                    End If
                    myPOSTreader.Close()
                End If
                If k.Righe(Indice).CodiceOspite > 0 And k.Righe(Indice).CodiceParente > 0 Then
                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select * From AnagraficaComune Where CodiceOspite  = " & k.Righe(Indice).CodiceOspite & " And CodiceParente = " & k.Righe(Indice).CodiceParente

                    cmd.Connection = cn

                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MyID = myPOSTreader.Item("ID")
                        Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                        Nome = campodb(myPOSTreader.Item("Nome"))
                    End If
                    myPOSTreader.Close()
                End If

                If k.Righe(Indice).CodicePorvincia <> "" And k.Righe(Indice).CodiceComune <> "" Then
                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select * From AnagraficaComune Where CodiceProvincia  = '" & k.Righe(Indice).CodicePorvincia & "' And CodiceComune = '" & k.Righe(Indice).CodiceComune & "'"

                    cmd.Connection = cn

                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MyID = myPOSTreader.Item("ID")
                        Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                        Nome = campodb(myPOSTreader.Item("Nome"))
                    End If
                    myPOSTreader.Close()
                End If

                If k.Righe(Indice).Regione <> "" Then
                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select * From AnagraficaComune Where CodiceRegione = '" & k.Righe(Indice).Regione & "' "

                    cmd.Connection = cn

                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MyID = myPOSTreader.Item("ID")
                        Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                        Nome = campodb(myPOSTreader.Item("Nome"))
                    End If
                    myPOSTreader.Close()
                End If


                If k.Righe(Indice).CodiceDebitoreCreditore <> 0 Then
                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select * From AnagraficaComune Where CodiceDebitoreCreditore  = '" & k.Righe(Indice).CodiceDebitoreCreditore & "' "

                    cmd.Connection = cn

                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MyID = myPOSTreader.Item("ID")
                        Mail = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                        Nome = campodb(myPOSTreader.Item("Nome"))
                    End If
                    myPOSTreader.Close()
                End If

                If MyID > 0 Then

                    Lst_Mail.Items.Add(Nome & " " & Mail)
                    Lst_Mail.Items(Lst_Mail.Items.Count - 1).Value = MyID
                Else
                    Lst_Mail.Items.Add(k.Righe(Indice).Mail)
                    Lst_Mail.Items(Lst_Mail.Items.Count - 1).Value = k.Righe(Indice).Mail
                End If
            End If

        Next


        cn.Close()
    End Sub

    Protected Sub Btn_Rimuovi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Rimuovi.Click
        Dim i As Integer

        For i = Lst_Mail.Items.Count - 1 To 0 Step -1
            If Lst_Mail.Items(i).Selected = True Then
                Lst_Mail.Items.RemoveAt(i)
            End If
        Next
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Txt_ID.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Puoi eliminare solo una mailing lista salvata</center>');", True)
            Exit Sub
        End If

        Dim k As New Cls_Tabella_MailingListTesta

        k.Delete(Session("DC_OSPITE"), Val(Txt_ID.Text))

        Response.Redirect("Elenco_MailingList.aspx")
    End Sub

    Protected Sub Btn_RimuoviUscitiDefinitivi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_RimuoviUscitiDefinitivi.Click
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim i As Integer

        For i = Lst_Mail.Items.Count - 1 To 0 Step -1
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From AnagraficaComune Where id  = '" & Lst_Mail.Items(i).Value & "' "

            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then

                If Val(campodb(myPOSTreader.Item("CodiceOspite"))) > 0 Then
                    Dim Mov As New Cls_Movimenti

                    Mov.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                    Mov.UltimaMovimentoPrimaData(Session("DC_OSPITE"), Mov.CodiceOspite, "", Now)
                    If Mov.TipoMov = "13" Then
                        Lst_Mail.Items.RemoveAt(i)
                    End If
                End If

            End If
            myPOSTreader.Close()

            
        Next
        cn.Close()
    End Sub
End Class
