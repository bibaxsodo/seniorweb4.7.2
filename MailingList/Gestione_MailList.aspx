﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="MailingList_Gestione_MailList" CodeFile="Gestione_MailList.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Mail</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


        function SalvaDati() {
            __doPostBack("BtnInvSalvaDati", "0");
        }

        function AppoggioSrc(Oggetto, url) {
            $('#' + Oggetto).attr('src', url)
        }

        function Abilita(Oggetto) {
            if ($("#" + Oggetto).css('visibility') == 'visible') {
                $("#" + Oggetto).css('visibility', 'hidden');
            }
            else {
                $("#" + Oggetto).css('visibility', 'visible');
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <asp:Button ID="BtnInvSalvaDati" runat="server" Text="" Visible="false" />
            <asp:Button ID="Btn_RefreshPostali" runat="server" Text="" Visible="false" />

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Mailing list - Gestione Mailing list</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="ImageButton1"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_MailList.aspx">
                            <img src="../images/Menu_Indietro.png" alt="Menù" title="Menù" /></a>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Mailing list" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Id :</label>
                                    <asp:TextBox ID="Txt_ID" onkeypress="return handleEnter(this, event)" Enabled="false" Width="70px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <table>
                                        <tr>
                                            <td>
                                                <a href="#" onclick=" AppoggioSrc('outputiframe','MailingList_Organizzazioni.aspx'); Abilita('redips_dialog_shade'); Abilita('redips_dialog'); Abilita('outputOrganizazioni'); ">
                                                    <img src="images/Anagrafica.jpg" /></a>
                                            </td>
                                            <td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <span class="MenuText">Anagrafiche</span>
                                            </td>
                                        </tr>
                                    </table>

                                    <div style="width: 650px; height: 200px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                        <asp:CheckBoxList ID="Lst_Mail" Width="600px" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                    <asp:LinkButton ID="Btn_Rimuovi" runat="server">Rimuovi</asp:LinkButton>
                                    <asp:LinkButton ID="Btn_RimuoviUscitiDefinitivi" runat="server">Rimuovi Usciti Definitivi</asp:LinkButton>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Mail :</label><br />
                                    <asp:TextBox ID="Txt_Mail" runat="server" TextMode="MultiLine" Width="300px"
                                        Height="120px" />
                                    <br />
                                    <asp:Button ID="Btn_Aggiungi" runat="server" Text="Aggiungi" />
                                    <br />
                                    <br />


                                    <div id="redips_dialog_shade" style="top: 0px; left: 0px; display: block; opacity: 0.6; visibility: hidden;"></div>
                                    <div id="redips_dialog" style="left: 20px; top: 10px; display: block; visibility: hidden;">
                                        <div class="redips_dialog_titlebar">
                                            <span title="Close" onclick="Abilita('redips_dialog_shade'); Abilita('redips_dialog'); Abilita('outputOrganizazioni');  SalvaDati();">✕</span>
                                        </div>
                                        <table class="redips_dialog_tbl" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" height="80%" width="100%">
                                                        <div>
                                                            <iframe id="outputiframe" height="600" width="1000"></iframe>
                                                        </div>
                                                        <div class="redips_dialog_buttons">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    </div>
                                                 
                                </ContentTemplate>
                            </AJAX:TabPanel>

                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
