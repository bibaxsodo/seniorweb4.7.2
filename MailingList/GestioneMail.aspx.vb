﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO


Partial Class GestioneMail
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Comune')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/autocompletecomune.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Cap')!= null) {  "
        MyJs = MyJs & " $(els[i]).focus(function() { if ($(els[i]).val() == '') {  __doPostBack(""Btn_RefreshPostali"", ""0""); } }); "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtConto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteContoTipo.ashx?TIPO=A&Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)



        If Val(Request.Item("ID")) > 0 Then
            Txt_ID.Text = Val(Request.Item("ID"))
            LeggiRubrica()
            Call LeggiDirectory()
        End If

        Call EseguiJS()
    End Sub


    Private Sub LeggiRubrica()

        Dim k As New Cls_Mail

        k.Id = Val(Txt_ID.Text)
        k.Leggi(Session("DC_OSPITE"))

        Txt_Descrizione.Text = k.Descrizione
        Txt_Data.Text = Format(k.Data, "dd/MM/yyyy")
        Txt_Oggetto.Text = k.Oggetto
        Txt_Testo.Content = k.Testo



    End Sub
    Private Sub ScriviRubrica()
        Dim k As New Cls_Mail

        k.Id = Val(Txt_ID.Text)

        k.Descrizione = Txt_Descrizione.Text
        k.Data = Txt_Data.Text
        k.Oggetto = Txt_Oggetto.Text
        k.Testo = Txt_Testo.Content
        k.IdFirma = 0

        k.Scrivi(Session("DC_OSPITE"))



        Txt_ID.Text = 0

        
        Txt_Descrizione.Text = ""
        Txt_Data.Text = ""
        Txt_Oggetto.Text = ""
        Txt_Testo.Content = ""

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Descrizione</center>');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_Data.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Data</center>');", True)
            Exit Sub
        End If


        ScriviRubrica()
        Response.Redirect("Elenco_Mail.aspx")
    End Sub




    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        If Val(Txt_ID.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Mail non registrata non posso eliminare</center>');", True)
            Exit Sub
        End If

        Dim M As New Cls_Mail

        M.Id = Val(Txt_ID.Text)
        M.Delete(Session("DC_OSPITE"))

        Response.Redirect("Elenco_Mail.aspx")
    End Sub



    Private Sub LeggiDirectory()
        Dim i As Long

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("NomeFile", GetType(String))


        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati", FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text)
        End If


        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Console.WriteLine(objItem.Name)

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga(0) = objItem.Name



            myriga(0) = objItem.Name
    

            MyTable.Rows.Add(myriga)

        Next

        Session("Documentazione") = MyTable
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()

    End Sub




    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpLoadFile.Click

        If Val(Txt_ID.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErrore", "VisualizzaErrore('Devi prima inserire la Mail');", True)
            Exit Sub
        End If

        If FileUpload1.FileName.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErroreFileNonSpecificato", "VisualizzaErrore('Specificare un file.');", True)
            Exit Sub
        End If

        Dim Appo As String



        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati", FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text)
        End If

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text & "\" & FileUpload1.FileName)

        Call LeggiDirectory()
    End Sub



    Private Sub BindDocumentazione()
        MyTable = Session("Documentazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()
    End Sub



    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "DOWNLOAD" Then


            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale


            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.ClearContent()
            Response.Clear()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("Content-Disposition", _
                                "attachment; filename=""" & "Copia_" & e.CommandArgument & """")
            Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text & "\" & e.CommandArgument)
            Response.End()
        End If
        If e.CommandName = "CANCELLAFILE" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale

            MyTable = Session("Documentazione")


            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Txt_ID.Text & "\" & MyTable.Rows(e.CommandArgument).Item(0))
            Catch ex As Exception

            End Try


            MyTable.Rows.RemoveAt(e.CommandArgument)

            If MyTable.Rows.Count = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                MyTable.Rows.Add(myriga)
            End If

            Session("Documentazione") = MyTable

            Call BindDocumentazione()

        End If
    End Sub

    Protected Sub Img_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Duplica.Click
        Txt_ID.Text = 0
    End Sub
End Class
