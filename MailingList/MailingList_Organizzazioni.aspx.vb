﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class MailingList_MailingList_Organizzazioni
    Inherits System.Web.UI.Page

    Dim Tab_RicercaSoci As New System.Data.DataTable("Tab_RicercaSoci")

    Private Sub CaricaUltimi10()
        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("ID", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Ragione Sociale/Nome", GetType(String))
        Tab_RicercaSoci.Columns.Add("Data Nascita", GetType(String))
        Tab_RicercaSoci.Columns.Add("Mail", GetType(String))



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        Dim MySql As String
        Dim Entrato As Boolean = False
        MySql = "select top 10 * from AnagraficaComune where (Not RESIDENZATELEFONO3 IS Null And RESIDENZATELEFONO3 <> '') And "


        If Rb_Ospiti.Checked = True Then
            MySql = MySql & " Tipologia = 'O' "
            Entrato = True
        End If
        If Rb_Parenti.Checked = True Then
            MySql = "select top 10 * from AnagraficaComune where (Not RESIDENZATELEFONO1 IS Null And RESIDENZATELEFONO1 <> '') And "
            MySql = MySql & " Tipologia = 'P' "
            Entrato = True
        End If
        If Rb_Comuni.Checked = True Then
            MySql = MySql & " Tipologia = 'C' "
            Entrato = True
        End If
        If Rb_Regioni.Checked = True Then
            MySql = MySql & " Tipologia = 'R' "
            Entrato = True
        End If
        If Rb_Medici.Checked = True Then
            MySql = MySql & " Tipologia = 'M' "
            Entrato = True
        End If
        If Rb_ClientiFornitori.Checked = True Then
            MySql = MySql & " Tipologia = 'D' "
            Entrato = True
        End If



        cmd.CommandText = MySql & "  Order By Nome"

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
            myrigaT(0) = campodb(myPOSTreader.Item("ID"))
            myrigaT(1) = campodb(myPOSTreader.Item("Nome"))
            myrigaT(2) = campodb(myPOSTreader.Item("DataNascita"))
            myrigaT(3) = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))

            Tab_RicercaSoci.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("RicercaSoci") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()

        Call EseguiJS()
    End Sub


    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click
        Tab_RicercaSoci.Clear()
        Tab_RicercaSoci.Columns.Clear()
        Tab_RicercaSoci.Columns.Add("ID", GetType(Long))
        Tab_RicercaSoci.Columns.Add("Ragione Sociale/Nome", GetType(String))
        Tab_RicercaSoci.Columns.Add("Data Nascita", GetType(String))
        Tab_RicercaSoci.Columns.Add("Mail", GetType(String))



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        Dim MySql As String
        Dim Entrato As Boolean = False
        MySql = "select * from AnagraficaComune where (Not RESIDENZATELEFONO3 IS Null And RESIDENZATELEFONO3 <> '') And "


        If Rb_Ospiti.Checked = True Then
            MySql = MySql & " Tipologia = 'O' "
            Entrato = True
        End If
        If Rb_Parenti.Checked = True Then
            MySql = "select * from AnagraficaComune where (Not RESIDENZATELEFONO1 IS Null And RESIDENZATELEFONO1 <> '') And "
            MySql = MySql & " Tipologia = 'P' "
            Entrato = True
        End If
        If Rb_Comuni.Checked = True Then
            MySql = MySql & " Tipologia = 'C' "
            Entrato = True
        End If

        If Rb_Regioni.Checked = True Then
            MySql = MySql & " Tipologia = 'R' "
            Entrato = True
        End If
        If Rb_Medici.Checked = True Then
            MySql = MySql & " Tipologia = 'M' "
            Entrato = True
        End If
        If Rb_ClientiFornitori.Checked = True Then
            MySql = MySql & " Tipologia = 'D' "
            Entrato = True
        End If

        If Txt_Cognome.Text <> "" Then
            If Entrato = True Then
                MySql = MySql & " And Nome Like ?"
            Else
                MySql = MySql & " Nome Like ?"
            End If
            cmd.Parameters.AddWithValue("@Nome", Txt_Cognome.Text & "%")
            Entrato = True
        End If


        cmd.CommandText = MySql & "  Order By Nome "

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Entra As Boolean = True

            If Rb_Ospiti.Checked = True Or Rb_Parenti.Checked = True Then
                If Chk_SoloPresenti.Checked = True Then
                    Dim Mov As New Cls_Movimenti

                    Mov.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                    Mov.UltimaMovimentoPrimaData(Session("DC_OSPITE"), Mov.CodiceOspite, "", Now)
                    If Mov.TipoMov = "13" Then
                        Entra = False
                    End If
                End If
            End If
            If Entra Then
                Dim myrigaT As System.Data.DataRow = Tab_RicercaSoci.NewRow()
                myrigaT(0) = campodb(myPOSTreader.Item("ID"))
                myrigaT(1) = campodb(myPOSTreader.Item("Nome"))
                myrigaT(2) = campodb(myPOSTreader.Item("DataNascita"))

                If Rb_Parenti.Checked = True Then
                    myrigaT(3) = campodb(myPOSTreader.Item("RESIDENZATELEFONO1"))
                Else
                    myrigaT(3) = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
                End If

                Tab_RicercaSoci.Rows.Add(myrigaT)
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("RicercaSoci") = Tab_RicercaSoci

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaSoci
        Grd_Visualizzazione.DataBind()

        Call EseguiJS()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null) || (appoggio.match('Txt_Data')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub MailingList_MailingList_Organizzazioni_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Call CaricaUltimi10()
        Session("IdAnagrafica") = 0

        Call EseguiJS()
    End Sub





    Protected Sub Btn_SelezionaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_SelezionaAll.Click
        Dim i As Integer
        Tab_RicercaSoci = ViewState("RicercaSoci")

        For i = 0 To Grd_Visualizzazione.Rows.Count - 1

            Dim Chk_Selezionato As CheckBox = DirectCast(Grd_Visualizzazione.Rows(i).FindControl("Chk_Selezionato"), CheckBox)


            Chk_Selezionato.Checked = True
        Next

    End Sub

    Protected Sub Imb_Exporta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Exporta.Click
        Dim i As Integer
        Dim TabExport As New System.Data.DataTable("TabExport")


        TabExport.Clear()
        TabExport.Columns.Clear()
        TabExport.Columns.Add("ID", GetType(Long))        
        TabExport.Columns.Add("RagioneSociale", GetType(String))
        TabExport.Columns.Add("Mail", GetType(String))


        Tab_RicercaSoci = ViewState("RicercaSoci")

        For i = 0 To Grd_Visualizzazione.Rows.Count - 1

            Dim Chk_Selezionato As CheckBox = DirectCast(Grd_Visualizzazione.Rows(i).FindControl("Chk_Selezionato"), CheckBox)

            If Chk_Selezionato.Checked = True Then
                Dim myrigaT As System.Data.DataRow = TabExport.NewRow()

                myrigaT.Item(0) = Tab_RicercaSoci.Rows(i).Item(0)                
                myrigaT.Item(1) = Tab_RicercaSoci.Rows(i).Item(1)
                myrigaT.Item(2) = Tab_RicercaSoci.Rows(i).Item(3)

                TabExport.Rows.Add(myrigaT)
            End If

        Next
        Session("Export") = TabExport

        Response.Redirect("MailingList_Export.aspx")
    End Sub

    Protected Sub Btn_InvertiSelezione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_InvertiSelezione.Click
        Dim i As Integer
        Tab_RicercaSoci = ViewState("RicercaSoci")

        For i = 0 To Grd_Visualizzazione.Rows.Count - 1

            Dim Chk_Selezionato As CheckBox = DirectCast(Grd_Visualizzazione.Rows(i).FindControl("Chk_Selezionato"), CheckBox)


            If Chk_Selezionato.Checked = True Then
                Chk_Selezionato.Checked = False
            Else
                Chk_Selezionato.Checked = True
            End If
        Next

    End Sub
End Class
