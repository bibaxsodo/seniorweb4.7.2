﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GestioneMail" CodeFile="GestioneMail.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Mail</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Mailing List - Creazione Mail</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Img_Duplica"></asp:ImageButton>&nbsp;
        <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_MailList.aspx">
                            <img src="../images/Menu_Indietro.png" alt="Menù" title="Menù" /></a>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Mail" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Id :</label>
                                    <asp:TextBox ID="Txt_ID" onkeypress="return handleEnter(this, event)" Enabled="false" Width="70px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        Descrizione :<font color="red">*</font><br />
                                        (uso interno)</label>
                                    <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        Data :<font color="red">*</font><br />
                                        (uso interno)</label>
                                    <asp:TextBox ID="Txt_Data" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Oggetto :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Oggetto" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <i>Usando @COGNOMENOME verrà sostituito con il nome dell'ospite</i><br />
                                    <i>Usando @INTESTATARIO verrà sostituito con l'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOINDIRIZZO verrà sostituito con l'indirizzo dell'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOCOMUNE verrà sostituito con il comune dell'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOCAP verrà sostituito con il CAP dell'intestario fattura</i><br />
                                    <i>Usando @PERIODOCOM periodo competenza da Data</i>
                                    <br />
                                    <br />
                                    <br />

                                    <cc2:Editor
                                        ID="Txt_Testo"
                                        Width="850px"
                                        Height="400px"
                                        runat="server" />
                                    <br />
                                    <i>Usando @COGNOMENOME verrà sostituito con il nome dell'ospite</i><br />
                                    <i>Usando @INTESTATARIO verrà sostituito con l'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOINDIRIZZO verrà sostituito con l'indirizzo dell'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOCOMUNE verrà sostituito con il comune dell'intestario fattura</i><br />
                                    <i>Usando @INTESTATARIOCAP verrà sostituito con il CAP dell'intestario fattura</i><br />
                                    <i>Usando @PERIODOCOM periodo competenza da Data</i>
                                    <br />
                                    <br />



                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Allegati" ID="TabPanel2">
                                <ContentTemplate>
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px" Width="1000px"
                                        ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="IB_Update" CommandName="Update" runat="Server" ImageUrl="~/images/aggiorna.png" />
                                                    <asp:ImageButton ID="IB_Cancel" CommandName="Cancel" runat="Server" ImageUrl="~/images/annulla.png" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Printer" CommandName="DOWNLOAD" runat="Server" ImageUrl="~/images/download.png" CommandArgument='<%#  Eval("NomeFile") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NOME FILE">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_DELETEFILE" CommandName="CANCELLAFILE" runat="Server" ImageUrl="~/images/cancella.png" CommandArgument='<%#  Container.DataItemIndex  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#58A4D5" Font-Bold="True" ForeColor="Black" />
                                    </asp:GridView>
                                    <br />
                                    Carica un file :<br />
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" /><asp:ImageButton ID="UpLoadFile" runat="server" Height="48px" ImageUrl="~/images/upload.png" Width="48px" ToolTip="Up Load File" />
                                </ContentTemplate>
                            </AJAX:TabPanel>
                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
