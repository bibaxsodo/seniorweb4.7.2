﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="AdminUtente" CodeFile="AdminUtente.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ADMIN UTENTE</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/structure.css">
    <script src="js/JSErrore.js" type="text/javascript"></script>
</head>
<body>

    <form id="form1" class="box login" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <fieldset class="boxBody">
            <label>Nome Utente</label>
            <asp:TextBox ID="Txt_Login" runat="server" TabIndex="1"></asp:TextBox>
            <asp:TextBox ID="Txt_Password" TextMode="Password" runat="server" TabIndex="2" required></asp:TextBox>
        </fieldset>
        <footer>
            <center>
	  <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/avanti.jpg"></asp:ImageButton>	  	  
	&nbsp;</center>
        </footer>
    </form>

    <footer id="main">
    </footer>
</body>
</html>
